

indLoc = [];
indObj = [];

[r,indtot] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELoc' 1 '==' 0;'IDEObj' 1 '==' 0},'sigSur',{'beta' [3 4] [1 3]});
% [r,indtotO] = get(bettyL,'Number','snr',99,'tuning',{'IDEObj' 1 '==' 0},'sigSur',{'beta' [3 4] [1 3]});
% indtot = length(unique([indtotL indtotO]));
length(indtot)
for dl = 3 : 4
[rLoc(dl),indL] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELoc' 1 '==' 0;'IDEObj' 1 '==' 0; 'IDELoc' dl '>=' 1},'sigSur',{'beta' dl [1 3]});
[rObj(dl),indO] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELoc' 1 '==' 0;'IDEObj' 1 '==' 0; 'IDEObj' dl '>=' 1},'sigSur',{'beta' dl [1 3]});
indLoc = [indLoc indL];
indObj = [indObj indO];
end

indLoc = unique(indLoc);
indObj = unique(indObj);
indBoth = intersect(indLoc,indObj);
indtuned = union(indLoc,indObj);length(indtuned)
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

figure
subplot(2,1,1)
n = hist(bothMonkeysL.data.hist(indtot,:),[1:18]);
bar(n)
subplot(2,1,2)

n2 = hist(bothMonkeysL.data.hist(indtuned,:),[1:18]);
bar(n2./n)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areas)


tuneI = {'iCueLoc' 'iCueObj'};
tt = [1 1; 1 2; 1 3; 2 1; 2 2; 2 3];    
ca = 3;parfor d = 1: 21;MTSlfp3('save','general','days',{longdays{d}},'stable','BehResp',1,tuneI{tt(ca,1)},tt(ca,2),'addToName',sprintf('%s%d',tuneI{tt(ca,1)}(5:end),tt(ca,2)),'fromfiles','surrogate','nrep',10000,'ML','RTfromML');end


objects = {'clark' 'betty' 'bettyL'}; 
for o = 1 : 3
    obj = eval(objects{o});
    [r,indLoc] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0;'IDELoc' [3 4] '>=' 1});
    [r,indObj] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0; 'IDEObj' [3 4] '>=' 1});
    ind = union(indLoc,indObj);
    titles = {sprintf('%s; surrogate',objects{o}) sprintf('%s; surrogate/stim',objects{o}) sprintf('%s; no surrogate',objects{o})};
    if o >1
        for i = 1 : 3; [signt,sign2,count] = sigValuesStim(obj,'surrogates',surargs{i},'figTit',titles{i},'ind',ind,'ML');end
    else
        for i = 1 : 3; [signt,sign2,count] = sigValuesStim(obj,'surrogates',surargs{i},'figTit',titles{i},'ind',ind);end
    end
end