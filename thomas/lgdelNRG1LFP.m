%% NRG1
clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160610' '20160914' '20160916' '20161020'  '20161020b' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807'  '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'
days = unique(days);
clear before after between mice cumg nmouse
c = 1;
select = 'hipp';
% select = 'mPFC';
for d = 1 : length(days)
    cd(days{d})
    if ~isempty(nptDir('mouse.txt'))
        mouse = textread('mouse.txt','%s');
        switch mouse{1}
            case {'lgdel'}
                lgDel = 1;
            case {'wt'}
                lgDel = 0;
            case {'lgdelPV-cre'}
                lgDel = 2;
        end
    else
        lgDel = nan;
        %         pwd
    end
    sessions = nptDir('session*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('drug.txt')) && lgDel ~= 2
            %             if ~exist('mouse','var'); mouse = textread('mouse.txt','%s'); end
            [dirs,grs] = findUnit;
            
            thegrs = unique(grs);
            timew = 35 * 60;
            cc=1;
            clear beforett betweentt aftertt
            rawfiles = nptDir('CSC*');
            
            if strcmp(select,'hipp')
                
                switch length(rawfiles)
                    case 64
                        sshank = 1;
                    case 96
                        sshank = 2;
                end
            elseif strcmp(select,'mPFC')
                switch length(rawfiles)
                    case 64
                        sshank = [];
                    case 96
                        sshank = 1;
                end
            end
            if ~isempty(sshank)
                for gg = sshank : length(thegrs)
                    
                    thefile = nptDir(sprintf('*group0%dpowergram.mat',thegrs(gg)));
                    load(thefile.name)
                    
                    tb = find(t < timew);
                    beforet = squeeze(median(median(S(tb,:,:),3),1));
                    before(c,:) = beforet;
                    beforett(cc,:) = beforet;
                    aftert = squeeze(median(median(S(end-length(tb) : end,:,:),3),1));
                    after(c,:) = aftert;
                    aftertt(cc,:) = aftert;
                    betweent = squeeze(median(median(S(tb(end) :tb(end) +length(tb),:,:),3),1));
                    between(c,:) = betweent;
                    betweentt(cc,:) = betweent;
                    mice(c) = lgDel;
                    cumg(c) = gg;
                    nmouse(c) = d;
                    c = c + 1;
                    cc = cc + 1;
                end
            end
        end
        cd ..
    end
    
    
    %     if isempty(nptDir('Drug effect.fig'))
    if exist('f','var') && exist('beforett','var')
        f1= figure;
        %     firstg = find(cumg ==1);
        subplot(3,1,1)
        plot(f,beforett,'k')
        title('baseline')
        xlim([0 40]);ylim([0 .0015])
        subplot(3,1,2)
        plot(f,betweentt,'g')
        title('30min after drug')
        xlim([0 40]);ylim([0 .0015])
        subplot(3,1,3)
        plot(f,aftertt,'r')
        title('1h00 after drug')
        xlim([0 40]);ylim([0 .0015])
        xlabel('Frequency (Hz)')
        ylabel('Power (arb. unit)')
        set(gcf,'Name',pwd)
        saveas(f1,'Drug effect')
        %         pause
        close(f1)
    end
    cd(sdir)
    clear mouse
end
[c,ia,ic] = unique(nmouse);

save('lgdelNRG1LFP.mat')

oldmice = mice;
mice = mice == 1;
figure;
subplot(3,2,1)
plot(f,before(mice,:),'r')
title(sprintf('n = %d LgDel',sum(mice(ia))))
ylabel('baseline')
subplot(3,2,2)
plot(f,before(~mice,:),'k')
title(sprintf('n = %d WT',sum(~mice(ia))))
subplot(3,2,3)

plot(f,between(mice,:),'r')

ylabel('30min after NRG1')
subplot(3,2,4)
plot(f,between(~mice,:),'k')

subplot(3,2,5)

plot(f,after(mice,:),'r')
ylabel('60min after NRG1')

subplot(3,2,6)
plot(f,after(~mice,:),'k')

for sb=1:6;subplot(3,2,sb); xlim([0 40]);ylim([0 .001500]);end

xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
set(gcf,'Name','All shanks plots')


tiles = [25 50 75];
figure;
subplot(1,3,1)
plot(f,prctile(before(mice,:),tiles,1),'r')
hold on
ylabel('baseline')

plot(f,prctile(before(~mice,:),tiles,1),'k')
title(sprintf('n = %d LgDel n = %d WT',sum(mice(ia)),sum(~mice(ia))))
subplot(1,3,2)

plot(f,prctile(between(mice,:),tiles,1),'r')
hold on
ylabel('30min after NRG1')

plot(f,prctile(between(~mice,:),tiles,1),'k')

subplot(1,3,3)

plot(f,prctile(after(mice,:),tiles,1),'r')
ylabel('60min after NRG1')
hold on

plot(f,prctile(after(~mice,:),tiles,1),'k')

for sb=1:3;subplot(1,3,sb); xlim([0 20]);ylim([0 .000800]);end

xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('25th prctile lgdel','50th prctile lgdel','75th prctile lgdel','25th prctile wt','50th prctile wt','75th prctile wt')
set(gcf,'Name','prctiles 25th 50th and 75th')

figure;
subplot(3,1,1)
plot(f,mean(before(mice,:),1),'r')
hold on
plot(f,mean(before(mice,:),1) + std(before(mice,:)) /sqrt(sum(mice)),'r')
plot(f,mean(before(mice,:),1) - std(before(mice,:)) /sqrt(sum(mice)),'r')
ylabel('baseline')

plot(f,mean(before(~mice,:),1),'k')
plot(f,mean(before(~mice,:),1) + std(before(~mice,:)) /sqrt(sum(~mice)),'k')
plot(f,mean(before(~mice,:),1) - std(before(~mice,:)) /sqrt(sum(~mice)),'k')
title(sprintf('n = %d LgDel n = %d WT',sum(mice(ia)),sum(~mice(ia))))
grid on
subplot(3,1,2)

plot(f,mean(between(mice,:),1),'r')
hold on
plot(f,mean(between(mice,:),1) + std(between(mice,:)) /sqrt(sum(mice)),'r')
plot(f,mean(between(mice,:),1) - std(between(mice,:)) /sqrt(sum(mice)),'r')
ylabel('30min after NRG1')

plot(f,mean(between(~mice,:),1),'k')
plot(f,mean(between(~mice,:),1) + std(between(~mice,:)) /sqrt(sum(~mice)),'k')
plot(f,mean(between(~mice,:),1) - std(between(~mice,:)) /sqrt(sum(~mice)),'k')
grid on
subplot(3,1,3)

plot(f,mean(after(mice,:),1),'r')
ylabel('60min after NRG1')
hold on
plot(f,mean(after(mice,:),1) + std(after(mice,:)) /sqrt(sum(mice)),'r')
plot(f,mean(after(mice,:),1) - std(after(mice,:)) /sqrt(sum(mice)),'r')
plot(f,mean(after(~mice,:),1),'k')
plot(f,mean(after(~mice,:),1) + std(after(~mice,:)) /sqrt(sum(~mice)),'k')
plot(f,mean(after(~mice,:),1) - std(after(~mice,:)) /sqrt(sum(~mice)),'k')
for sb=1:3;subplot(3,1,sb); xlim([0 90]);ylim([0 .000500]);end

xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('mean lgdel','mean+/-sem lgdel','mean+/-sem lgdel','mean wt','mean+/-sem wt','mean+/-sem wt')
set(gcf,'Name','mean +/- SEM')
grid on
%% peak analysis

allspectra{1} = before; allspectra{2} = between; allspectra{3} = after;
dsample = 30;
thetapeak = cell(3,1);
for p = 1 : 3
    down = downsample(allspectra{p}', dsample);
    
    df = downsample(f',dsample);
    xmax=findpeaks(down);
    allpeaks = [];
    
    % figure
    for ii = 1 : size(down,2)
        %     allpeaks = [allpeaks; xmax(ii).loc];
        %     plot(df,dbefore(:,ii));hold on; plot(df(xmax(ii).loc),dbefore(xmax(ii).loc,ii),'*');
        if ~isempty(find(df(xmax(ii).loc) >= 4 & df(xmax(ii).loc) <= 11))
            thetapeak{p} = [thetapeak{p} ii];
        end
    end
end

allthetaInd = unique(cat(2,thetapeak{:}));
alltheta = zeros(1,size(down,2));
alltheta(allthetaInd) = 1;
% plot(f,before(thetapeak,:))
% figure; plot(f,before)
% figure
% [n,xout] = hist(df(allpeaks),df);
% plot(xout,100*n / size(dbefore,2))
alllb = {'baseline' '30min after NRG1' '60min after NRG1'};
figure;
for p = 1 : 3
    subplot(1,3,p)
    plot(f,mean(allspectra{p}(mice & alltheta,:),1),'r')
    hold on
    plot(f,mean(allspectra{p}(mice & alltheta,:),1) + std(allspectra{p}(mice & alltheta,:)) /sqrt(sum(mice & alltheta)),'r')
    plot(f,mean(allspectra{p}(mice & alltheta,:),1) - std(allspectra{p}(mice & alltheta,:)) /sqrt(sum(mice & alltheta)),'r')
    
    plot(f,mean(allspectra{p}(~mice & alltheta,:),1),'k')
    hold on
    plot(f,mean(allspectra{p}(~mice & alltheta,:),1) + std(allspectra{p}(~mice & alltheta,:)) /sqrt(sum(~mice & alltheta)),'k')
    plot(f,mean(allspectra{p}(~mice & alltheta,:),1) - std(allspectra{p}(~mice & alltheta,:)) /sqrt(sum(~mice & alltheta)),'k')
    ylabel(alllb{p})
    title(sprintf('n = %d LgDel n = %d WT',sum(mice(ia)),sum(~mice(ia))))
    
    xlim([0 20]);ylim([0 .000800]);
    grid on
end
xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('mean lgdel','mean+/-sem lgdel','mean+/-sem lgdel','mean wt','mean+/-sem wt','mean+/-sem wt')
set(gcf,'Name','mean +/- SEM excluding non-theta peak')



%% mean in the different bands
bands = [1 4;5 8; 9 15; 16 30; 31 80];
bandN = {'delta' 'theta' 'alpha' 'beta' 'gamma'};
for bb = 1 : 5
    frange = bands(bb,:);
    allspectra{1} = before; allspectra{2} = between; allspectra{3} = after;
    alllb = {'baseline' '30min after NRG1' '60min after NRG1'};
    fs = find(f >= frange(1) & f <= frange(2));
    mspectra = cell(3,1);
    WRSpvalue = ones(3,1);
    ttestpvalue = ones(3,1);
    figure
    for p = 1 : 3
        mspectra{p} = mean(allspectra{p}(:,fs),2);
        %    WRSpvalue(p) = ranksum(mspectra{p}(mice & alltheta),mspectra{p}(~mice & alltheta));
        %    [h, ttestpvalue(p)] = ttest2(mspectra{p}(mice & alltheta),mspectra{p}(~mice & alltheta));
        
        WRSpvalue(p) = ranksum(mspectra{p}(mice),mspectra{p}(~mice));
        [h, ttestpvalue(p)] = ttest2(mspectra{p}(mice),mspectra{p}(~mice));
        subplot(1,3,p)
        boxplot(mspectra{p},mice,'labels',{'wt' 'lgdel'});
        hold on
        title(sprintf('pvalue = %f',ttestpvalue(p)))
        ylim([0 3.5*10^-4])
        grid on
        xlabel(alllb{p})
    end
    ylabel(sprintf('mean power in the %s range (arb. units)',bandN{bb}))
    
    figure;
    subplot(1,2,1)
    da = [mspectra{1}(mice) mspectra{3}(mice)]';
    plot(da,'y')
    hold on
    errorbar(mean(da,2), std(da,[],2)/sqrt(sum(mice)),'r')
    pv = signrank(mspectra{1}(mice),mspectra{3}(mice));
    title(sprintf('lgdel plus NRG1,n=%d, p=%d',sum(mice),pv))
    xlim([0 3]);
    ylabel(bandN{bb})
    subplot(1,2,2)
    da = [mspectra{1}(~mice) mspectra{3}(~mice)]';
    plot(da,'y')
    hold on
    errorbar(mean(da,2), std(da,[],2)/sqrt(sum(~mice)),'k')
    pv = signrank(mspectra{1}(~mice),mspectra{3}(~mice));
    title(sprintf('WT plus NRG1,n=%d, p=%d',sum(~mice),pv))
    xlim([0 3]);
end
%%
clear mbefore mafter mbetween micea
c=1;
for nm = unique(nmouse)
    mbefore(c,:) = mean(before(nmouse == nm,:),1);
    mafter(c,:) = mean(after(nmouse == nm,:),1);
    mbetween(c,:) = mean(between(nmouse == nm,:),1);
    micea(c) = unique(mice(nmouse ==nm));
    c=c+1;
end
figure

subplot(3,1,1)

plot(f,mean(mbefore(micea == 1,:)),'r')
hold on
plot(f,mean(mbefore(micea == 1,:)) + std(mbefore(micea == 1,:)) / sqrt(length(micea)),'r')
plot(f,mean(mbefore(micea == 1,:)) - std(mbefore(micea == 1,:)) / sqrt(length(micea)),'r')

plot(f,mean(mbefore(micea == 0,:)),'k')
hold on
plot(f,mean(mbefore(micea == 0,:)) + std(mbefore(micea == 0,:)) / sqrt(length(micea)),'k')
plot(f,mean(mbefore(micea == 0,:)) - std(mbefore(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('30 min baseline')

subplot(3,1,2)

plot(f,mean(mbetween(micea == 1,:)),'r')
hold on
plot(f,mean(mbetween(micea == 1,:)) + std(mbetween(micea == 1,:)) / sqrt(length(micea)),'r')
plot(f,mean(mbetween(micea == 1,:)) - std(mbetween(micea == 1,:)) / sqrt(length(micea)),'r')

plot(f,mean(mbetween(micea == 0,:)),'k')
hold on
plot(f,mean(mbetween(micea == 0,:)) + std(mbetween(micea == 0,:)) / sqrt(length(micea)),'k')
plot(f,mean(mbetween(micea == 0,:)) - std(mbetween(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('30 min after NRG1')

subplot(3,1,3)

plot(f,mean(mafter(micea == 1,:)),'r')
hold on
plot(f,mean(mafter(micea == 1,:)) + std(mafter(micea == 1,:)) / sqrt(length(micea)),'r')
plot(f,mean(mafter(micea == 1,:)) - std(mafter(micea == 1,:)) / sqrt(length(micea)),'r')

plot(f,mean(mafter(micea == 0,:)),'k')
hold on
plot(f,mean(mafter(micea == 0,:)) + std(mafter(micea == 0,:)) / sqrt(length(micea)),'k')
plot(f,mean(mafter(micea == 0,:)) - std(mafter(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('60 min after NRG1')

%%

figure
subplot(3,1,1)
delta = find(f > 1 & f < 4);

t = mean(before(mice == 0,delta),2);
y = mean(after(mice == 0,delta),2);
plot(t,y,'k.')

hold on
[r,m,b] = regression(t,y,'one');
line([0 max(cat(1,t,y))/tan(m)],[b max(cat(1,t,y))],'Color','k')
xlabel('power before NRG1'); ylabel('power  60 min after NRG1')
hold on

t = mean(before(mice == 1,delta),2);
y = mean(after(mice == 1,delta),2);
plot(t,y,'r.')
[r,m,b] = regression(t,y,'one');
line([0 max(cat(1,t,y))/tan(m)],[b max(cat(1,t,y))],'Color','r')
xlabel('power before NRG1'); ylabel('power  60 min after NRG1')
title('delta')

line([0 6*10^-4],[0 6*10^-4])

subplot(3,1,2)
theta = find(f >= 4 & f <= 9);
t = mean(before(mice == 0,theta),2);
y = mean(after(mice == 0,theta),2);
plot(t,y,'k.')
[r,m,b] = regression(t,y,'one');
line([0 max(cat(1,t,y))/tan(m)],[b max(cat(1,t,y))],'Color','k')
title('WT')
xlabel('power before NRG1'); ylabel('power  60 min after NRG1')

hold on

t = mean(before(mice == 1,theta),2);
y = mean(after(mice == 1,theta),2);
plot(t,y,'r.')
[r,m,b] = regression(t,y,'one');
line([0 max(cat(1,t,y))/tan(m)],[b max(cat(1,t,y))],'Color','r')
title('lgDel')
xlabel('power before NRG1'); ylabel('power  60 min after NRG1')
title('theta')
line([0 6*10^-4],[0 6*10^-4])

subplot(3,1,3)
plot(mean(before(mice == 0,theta),2),mean(before(mice == 0,delta),2),'k.')
hold on
plot(mean(before(mice == 1,theta),2),mean(before(mice == 1,delta),2),'r.')
line([0 6*10^-4],[0 6*10^-4])
legend('WT','lgDel')
xlabel('theta before NRG1'); ylabel('delta before NRG1')
%% stats
figure

pp = cell(2,1);
subplot(2,1,1)

plot(f,prctile(before(mice,:),tiles,1),'r')
hold on
plot(f,prctile(after(mice,:),tiles,1),'k')
for ff = 1 : length(f); pp{1}(ff) = signrank(before(mice,ff),after(mice,ff)); end
plot(f,pp{1} >= .005,'r*')
legend('25th prctile before NRG1','50th prctile before NRG1','75th prctile before NRG1','25th prctile 1 hour after NRG1','50th prctile 1 hour after NRG1','75th prctile 1 hour after NRG1','significance in a Wilcoxon signed rank paired test at p<.005')

title(sprintf('n = %d LgDel',sum(mice(ia))))
ylim([0 10^-3])
xlim([0 40])
subplot(2,1,2)

plot(f,prctile(before(~mice,:),tiles,1),'r')
hold on
plot(f,prctile(after(~mice,:),tiles,1),'k')
for ff = 1 : length(f); pp{2}(ff) = signrank(before(~mice,ff),after(~mice,ff)); end
title(sprintf(' n = %d WT',sum(~mice(ia))))

plot(f,pp{2} >= .005,'k*')
ylim([0 10^-3])
xlim([0 40])
xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')

