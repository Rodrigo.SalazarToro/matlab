
cd G:\thomas\Nurr1_Nr4a2project
cd G:\thomas\Gephyrin
cd G:\thomas\FMR1KO
cd G:\thomas\LgDel\Coactivations
sdir = pwd;

level1 = nptDir('Condition*');
count = 1;
for l1 = 1 : length(level1)
    cd(level1(l1).name)
    conds = nptDir('Condition*');
    sdir2 = pwd;
%         for c = fliplr([1 : length(conds)])
    for c = [1 : length(conds)]
        cd(conds(c).name)
        if isempty(nptDir('skip.txt'))
            cdir = pwd;
            exps = nptDir('1*');
            for e = 1 : length(exps)
                curAnal = [cdir filesep exps(e).name];
                cd(curAnal)
                
                if ~isempty(nptDir('calcium.mat')) && isempty(nptDir('skip.txt'))
                     try
                    count  = count + 1;
                    a = load('calcium');
                    onsets = a.handles.app.experiment.detections(1,2).onsets';
                    
                        %                     xcorrCalcOnsets(onsets,'save')
                        %                     xcorrView('save')
                        xcorrCalcOnsets(onsets,'save','addName','binSize2','nofitting');
                        xcorrView('save','addName','binSize2','loadFile','xcorrSpikesbinSize2.mat');
                         xcorrView('append','addName','binSize2','loadFile','xcorrSpikesbinSize2.mat');
                    catch
                        %                      xcorrCalcOnsets(onsets,'save','redo')
                        %                     xcorrView('save')
                    end
                    
                else
                    display(['FAIS FASSE THOMAS ' pwd])
                end
                cd(cdir)
            end
            cd(sdir2)
        end
    end
    cd(sdir)
end
