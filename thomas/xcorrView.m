function xcorrView(varargin)

Args = struct('save',0,'redo',0,'append',0,'addName',[],'loadFile','xcorrSpikes.mat','bins',[]);
Args.flags = {'save','redo','nofitting','append'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

lfilename = Args.loadFile;
sfilename = ['sigCoi' Args.addName '.mat'];
ffilename = ['xcorrOnsets' Args.addName '.fig'];

if ~isempty(nptDir(lfilename))
    if isempty(nptDir(sfilename)) || Args.redo || Args.append
        load(lfilename)
        if ~isempty(Args.bins); bins = Args.bins; end
        sigcoi = cumcoi > squeeze(nanmax(surcoi,[],1));
        allpairssigcoi = sum(sigcoi);
        if ~Args.append
            
            f1 = figure;
            subplot(3,1,1);
            imagesc(cumcoi);
            set(gca,'XTick',[1:length(bins)])
            set(gca,'XTickLabel',bins)
            colorbar;
            title(pwd)
            subplot(3,1,2);
            imagesc(squeeze(nanmax(surcoi,[],1)));
            set(gca,'XTick',[1:length(bins)])
            set(gca,'XTickLabel',bins)
            colorbar;
            subplot(3,1,3);
            imagesc(cumcoi > squeeze(nanmax(surcoi,[],1)));
            set(gca,'XTick',[1:length(bins)])
            set(gca,'XTickLabel',bins)
            xlabel('Lag (frame)')
            colorbar
            savefig(ffilename)
            clf
            save(sfilename,'sigcoi','allpairssigcoi','bins')
            display(['saving' pwd filesep sfilename])
            close(f1)
        else
            allpairssigcoiTot = sum(sum(sigcoi,2) ~= 0);
            save(sfilename,'allpairssigcoiTot','-append')
        end
        
    end
end
