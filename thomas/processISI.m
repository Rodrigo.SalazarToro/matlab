clear all
dirs = {'G:\thomas\LgDel\Coactivations\ConditionWT\ConditionCCH' 'G:\thomas\LgDel\Coactivations\ConditionWT\ConditionPTX'};
ncounts = cell(2,1);
for d = 1 : 2
    cd(dirs{d})
    exps = nptDir('1*');
    sdir = pwd;
    
    for e = 1 : length(exps)
        cd(exps(e).name)
        
        pwd
        if ~isempty(nptDir('calcium.mat'))
            a = load('calcium');
            onsets = a.handles.app.experiment.detections(1,2).onsets;
            [allcounts,bins,ncounts{d}(e,:)] = ISIcalcium(onsets,'save','redo');
        else
            display(['FAIS FASSE THOMAS ' pwd])
        end
        
        cd(sdir)
    end
    
    
end

figure
plot(bins,100*ncounts{1},'r')
hold on
plot(bins,100*ncounts{2},'k')
title('red:WT CCH & black: WT PTX')

xlabel('ISI (frames')
ylabel('% pair od spikes')