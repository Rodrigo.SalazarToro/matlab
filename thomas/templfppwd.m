
%% NRG1

days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};

clear before after between mice cumg nmouse
c = 1;
for d = 1 : length(days)
    cd(days{d})
    if ~isempty(nptDir('mouse.txt')); mouse = textread('mouse.txt','%s'); end
    sessions = nptDir('session*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('drug.txt'))
%             if ~exist('mouse','var'); mouse = textread('mouse.txt','%s'); end
            
            [dirs,grs] = findUnit;
            lgDel = strcmp(mouse{1},'lgdel');
            
            thegrs = unique(grs);
            timew = 35 * 60;
            
            for gg = 1 : length(thegrs)
                if thegrs(gg) > 1
                thefile = nptDir(sprintf('*group0%dpowergram.mat',thegrs(gg)));
                load(thefile.name)
                
                tb = find(t < timew);
                before(c,:) = squeeze(median(median(S(tb,:,:),3),1));
                try
                after(c,:) = squeeze(median(median(S(tb(end) +length(tb) : tb(end) +2*length(tb),:,:),3),1));
                catch
                 after(c,:) = squeeze(median(median(S(end-length(tb) : end,:,:),3),1));
                   
                    
                end
                between(c,:) = squeeze(median(median(S(tb(end) :tb(end) +length(tb),:,:),3),1));
                mice(c) = lgDel;
                cumg(c) = gg;
                nmouse(c) = d;
                c = c + 1;
                end
            end
        end
        cd ..
    end
    f1= figure;
%     firstg = find(cumg ==1);
    subplot(3,1,1)
    plot(f,before,'k')
    title('baseline')
    xlim([0 40]);ylim([0 .0015])
    subplot(3,1,2)
    plot(f,between,'g')
    title('30min after drug')
    xlim([0 40]);ylim([0 .0015])
    subplot(3,1,3)
    plot(f,after,'r')
    title('1h00 after drug')
    xlim([0 40]);ylim([0 .0015])
    xlabel('Frequency (Hz)')
    ylabel('Power (arb. unit)')
    set(gcf,'Name',pwd)
    saveas(f1,'Drug effect')
    close(f1)
    cd(sdir)
     clear mouse
end
[c,ia,ic] = unique(nmouse);
figure;
subplot(3,2,1)
plot(f,before(mice,:),'r')
title(sprintf('n = %d LgDel',sum(mice(ia))))
ylabel('baseline')
subplot(3,2,2)
plot(f,before(~mice,:),'k')
title(sprintf('n = %d WT',sum(~mice(ia))))
subplot(3,2,3)

plot(f,between(mice,:),'r')

ylabel('30min after NRG1')
subplot(3,2,4)
plot(f,between(~mice,:),'k')

subplot(3,2,5)

plot(f,after(mice,:),'r')
ylabel('60min after NRG1')

subplot(3,2,6)
plot(f,after(~mice,:),'k')

for sb=1:6;subplot(3,2,sb); xlim([0 40]);ylim([0 .001500]);end

xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')

tiles = [25 50 75];
figure;
subplot(3,1,1)
plot(f,prctile(before(mice,:),tiles,1),'r')
hold on
ylabel('baseline')

plot(f,prctile(before(~mice,:),tiles,1),'k')
title(sprintf('n = %d LgDel n = %d WT',sum(mice(ia)),sum(~mice(ia))))
subplot(3,1,2)

plot(f,prctile(between(mice,:),tiles,1),'r')
hold on
ylabel('30min after NRG1')

plot(f,prctile(between(~mice,:),tiles,1),'k')

subplot(3,1,3)

plot(f,prctile(after(mice,:),tiles,1),'r')
ylabel('60min after NRG1')
hold on

plot(f,prctile(after(~mice,:),tiles,1),'k')

for sb=1:3;subplot(3,1,sb); xlim([0 40]);ylim([0 .001500]);end

xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
%%
clear mbefore mafter mbetween micea
c=1;
for nm = unique(nmouse)
    mbefore(c,:) = mean(before(nmouse == nm,:),1);
    mafter(c,:) = mean(after(nmouse == nm,:),1);
    mbetween(c,:) = mean(between(nmouse == nm,:),1);
    micea(c) = unique(mice(nmouse ==nm));
    c=c+1;
end
figure

subplot(3,1,1)

plot(pw.f,mean(mbefore(micea == 1,:)),'r')
hold on
plot(pw.f,mean(mbefore(micea == 1,:)) + std(mbefore(micea == 1,:)) / sqrt(length(micea)),'r')
plot(pw.f,mean(mbefore(micea == 1,:)) - std(mbefore(micea == 1,:)) / sqrt(length(micea)),'r')

plot(pw.f,mean(mbefore(micea == 0,:)),'k')
hold on
plot(pw.f,mean(mbefore(micea == 0,:)) + std(mbefore(micea == 0,:)) / sqrt(length(micea)),'k')
plot(pw.f,mean(mbefore(micea == 0,:)) - std(mbefore(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('30 min baseline')

subplot(3,1,2)

plot(pw.f,mean(mbetween(micea == 1,:)),'r')
hold on
plot(pw.f,mean(mbetween(micea == 1,:)) + std(mbetween(micea == 1,:)) / sqrt(length(micea)),'r')
plot(pw.f,mean(mbetween(micea == 1,:)) - std(mbetween(micea == 1,:)) / sqrt(length(micea)),'r')

plot(pw.f,mean(mbetween(micea == 0,:)),'k')
hold on
plot(pw.f,mean(mbetween(micea == 0,:)) + std(mbetween(micea == 0,:)) / sqrt(length(micea)),'k')
plot(pw.f,mean(mbetween(micea == 0,:)) - std(mbetween(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('30 min after NRG1')

subplot(3,1,3)

plot(pw.f,mean(mafter(micea == 1,:)),'r')
hold on
plot(pw.f,mean(mafter(micea == 1,:)) + std(mafter(micea == 1,:)) / sqrt(length(micea)),'r')
plot(pw.f,mean(mafter(micea == 1,:)) - std(mafter(micea == 1,:)) / sqrt(length(micea)),'r')

plot(pw.f,mean(mafter(micea == 0,:)),'k')
hold on
plot(pw.f,mean(mafter(micea == 0,:)) + std(mafter(micea == 0,:)) / sqrt(length(micea)),'k')
plot(pw.f,mean(mafter(micea == 0,:)) - std(mafter(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')
title('60 min after NRG1')
%% stats
figure

pp = cell(2,1);
subplot(2,1,1)

plot(f,prctile(before(mice,:),tiles,1),'r')
hold on
plot(f,prctile(after(mice,:),tiles,1),'k')
for ff = 1 : length(f); pp{1}(ff) = signrank(before(mice,ff),after(mice,ff)); end
plot(f,pp{1} >= .005,'r*')
legend('25th prctile before NRG1','50th prctile before NRG1','75th prctile before NRG1','25th prctile 1 hour after NRG1','50th prctile 1 hour after NRG1','75th prctile 1 hour after NRG1','significance in a Wilcoxon signed rank paired test at p<.005')

title(sprintf('n = %d LgDel',sum(mice(ia))))
ylim([0 10^-3])
xlim([0 40])
subplot(2,1,2)

plot(f,prctile(before(~mice,:),tiles,1),'r')
hold on
plot(f,prctile(after(~mice,:),tiles,1),'k')
for ff = 1 : length(f); pp{2}(ff) = signrank(before(~mice,ff),after(~mice,ff)); end
title(sprintf(' n = %d WT',sum(~mice(ia))))

plot(f,pp{2} >= .005,'k*')
ylim([0 10^-3])
xlim([0 40])
xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')

