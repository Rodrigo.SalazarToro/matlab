cd F:\salazar\data\Electrophy\hippocampus
days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622' '20160914' '20160916' '20161020' '20161020b' '20161027' '20161103' '20161103b' '20161125' '20161201' '20161201b'};
days = {'20170111'};
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
parfor ii = 1 : obj.data.numSets
    cd(obj.data.setNames{ii})
    t = load('ispikes.mat');
    timeserie = t.sp.data.trial.cluster.spikes;
%     try
    [autoc,bins] = autocorr(timeserie,1000,5,'infolder','save');
    % [autoc,bins] = autocorr(1,1000,5,'infolder','save','multitaper');
    autocorr(1,1000,5,'infolder','save','multitaper','Fs',200,'spikeLFP');
    autocorr(1,1000,5,'infolder','save','multitaper','Fs',200,'spikeLFPSur');
     autocorr(1,1000,5,'infolder','save','multitaper','Fs',200,'spikeLFPSur','onlyFitSurrogate');
%     catch
%         display(sprintf('Error %s',obj.data.setNames{ii}))
%     end
end