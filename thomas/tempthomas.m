cd F:\salazar\data\thomas


load('CCh5 PV 4 1st.mat')
onsets = handles.app.experiment.detections(1,2).onsets;
ncell = length(onsets);

traces = handles.app.experiment.traces;


ncell = length(onsets);

comb = nchoosek([1: ncell],2);

a = load('example1.mat');


traces2 = a.handles.app.experiment.traces;

ntraces = (traces - repmat(mean(traces,2),1,size(traces,2))) ./ repmat(std(traces,[],2),1,size(traces,2));
dtraces = detrend(ntraces')';


maxlag = 200;
[c,lags]= xcorr(dtraces',maxlag,'coeff');
c = single(c);
% surrogate
iterations = 1000;
csur = single(ones(iterations,size(c,1),size(c,2)));
rtraces = dtraces;
for ii = 1 : iterations
    rshift = randi(size(dtraces,2),ncell,1);
    for n = 1 : ncell
        rtraces(n,:) = dtraces(n,cat(2,rshift(n):size(dtraces,2),1: rshift(n)-1));
    end
    [ct,~]= xcorr(rtraces',maxlag,'coeff');
    csur(ii,:,:) = single(ct);
end


[c2,lags]= xcorr(traces2','coeff');
figure

subplot(1,2,1)

imagesc([1 : size(c1,2)],lags,c1,[0 1])
title('CCh5 PV 4 1st.mat')

subplot(1,2,2)

imagesc([1 : size(c1,2)],lags,c2,[0 1])
title('example 1')
xlabel('pair combination')
ylabel('time point lag')

%% new attempt

cd F:\salazar\data\thomas


load('CCh5 PV 4 1st.mat')
onsets = handles.app.experiment.detections(1,2).onsets;
ncell = length(onsets);

maxtime = max(cell2mat(onsets')) + 1;

monset = zeros(ncell,length([0:maxtime]));
for n = 1 : ncell
    monset(n,:) = hist(onsets{n},[0:maxtime]);
    
end

figure; subplot(2,1,1); imagesc(monset); subplot(2,1,2); plot(sum(monset,1))
maxlag = 100;
cumsp = zeros(ncell,2*maxlag + 1);
lags = [-maxlag-1:1 : maxlag+ 1 ];
for n = 1 : ncell
    
    for sp = 1 : length(onsets{n})
       tp  =   hist(cell2mat(onsets(setdiff([1:ncell],n))') - onsets{n}(sp),lags);
         cumsp(n,:) = cumsp(n,:) + tp(2:end-1);
    end
end
figure;plot(lags(2:end-1),cumsp')

load('example1.mat')
onsets2 = handles.app.experiment.detections(1,2).onsets;
ncell = length(onsets2);

maxtime = max(cell2mat(onsets2')) + 1;

monset = zeros(ncell,length([0:maxtime]));
for n = 1 : ncell
    monset2(n,:) = hist(onsets2{n},[0:maxtime]);
    
end
figure; subplot(2,1,1); imagesc(monset2); subplot(2,1,2); plot(sum(monse2t,1))

maxlag = 100;
cumsp2 = zeros(ncell,2*maxlag + 1);
lags = [-maxlag-1 : 1 : maxlag + 1];
for n = 1 : ncell
    
    for sp = 1 : length(onsets2{n})
       tp  =   hist(cell2mat(onsets2(setdiff([1:ncell],n))') - onsets2{n}(sp),lags);
         cumsp2(n,:) = cumsp2(n,:) + tp(2:end-1);
    end
end
figure;plot(lags(2:end-1),cumsp')
