% dependencies lgdelNRG1LFP

clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160610' '20160914' '20160916' '20161020'  '20161020b' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807'  '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'
days = unique(days);

clear before after between mice cumg nmouse
c = 1;
select = 'hipp';
% select = 'mPFC';
for d = 1 : length(days)
    cd(days{d})
    if ~isempty(nptDir('mouse.txt'))
        mouse = textread('mouse.txt','%s');
        switch mouse{1}
            case {'lgdel'}
                lgDel = 1;
            case {'wt'}
                lgDel = 0;
            case {'lgdelPV-cre'}
                lgDel = 2;
        end
    else
        lgDel = nan;
        %         pwd
    end
    sessions = nptDir('session*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('drug.txt')) 
            %             if ~exist('mouse','var'); mouse = textread('mouse.txt','%s'); end
            [dirs,grs] = findUnit;
            
            thegrs = unique(grs);
            timew = 35 * 60;
            cc=1;
            clear beforett betweentt aftertt
            rawfiles = nptDir('CSC*');
            
            if strcmp(select,'hipp')
                
                switch length(rawfiles)
                    case 64
                        sshank = 1;
                    case 96
                        sshank = 2;
                end
            elseif strcmp(select,'mPFC')
                switch length(rawfiles)
                    case 64
                        sshank = [];
                    case 96
                        sshank = 1;
                end
            end
            if ~isempty(sshank)
                for gg = sshank : length(thegrs)
                    
                    thefile = nptDir(sprintf('*group0%dpowergram.mat',thegrs(gg)));
                    load(thefile.name)
                    
                    tb = find(t < timew);
                    beforet = squeeze(median(median(S(tb,:,:),3),1));
                    before(c,:) = beforet;
                    beforett(cc,:) = beforet;
                    aftert = squeeze(median(median(S(end-length(tb) : end,:,:),3),1));
                    after(c,:) = aftert;
                    aftertt(cc,:) = aftert;
                    betweent = squeeze(median(median(S(tb(end) :tb(end) +length(tb),:,:),3),1));
                    between(c,:) = betweent;
                    betweentt(cc,:) = betweent;
                    mice(c) = lgDel;
                    cumg(c) = gg;
                    nmouse(c) = d;
                    c = c + 1;
                    cc = cc + 1;
                end
            end
        end
        cd ..
    end
    
   
    cd(sdir)
    clear mouse
end

endt = 90; % 4 mice removed because of gamma noise


remove50 = find(f >= 47 & f <= 53);

before(:,remove50) = nan(size(before,1),length(remove50));
between(:,remove50) = nan(size(before,1),length(remove50));
after(:,remove50) = nan(size(before,1),length(remove50));


figure
plot(f,f.*mean(before(mice(1:endt)==0,:)),'k')
hold on;
plot(f,f.*mean(before(mice(1:endt)==0,:)) + f.*std(before(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'k')
plot(f,f.*mean(before(mice(1:endt)==0,:)) - f.*std(before(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'k')

plot(f,f.*mean(before(mice(1:endt)==1,:)),'r')
plot(f,f.*mean(before(mice(1:endt)==1,:)) + f.*std(before(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'r')
plot(f,f.*mean(before(mice(1:endt)==1,:)) - f.*std(before(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'r')


xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('WTmean','WT SEM','WT SEM','lgdel mean','lgdel SEM','lgdel SEM')

figure
subplot(3,1,1)
plot(f,f.*mean(before(mice(1:endt)==0,:)),'k')
hold on;
plot(f,f.*mean(before(mice(1:endt)==0,:)) + f.*std(before(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'k')
plot(f,f.*mean(before(mice(1:endt)==0,:)) - f.*std(before(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'k')

plot(f,f.*mean(after(mice(1:endt)==0,:)),'r')
plot(f,f.*mean(after(mice(1:endt)==0,:)) + f.*std(after(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'r')
plot(f,f.*mean(after(mice(1:endt)==0,:)) - f.*std(after(mice(1:endt)==0,:))/sqrt(length(mice(1:endt)==0)),'r')


xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('WT mean','WT SEM','WT SEM','NRG1 mean','NRG1 SEM','NRG1 SEM')

subplot(3,1,2)
plot(f,f.*mean(before(mice(1:endt)==1,:)),'k')
hold on;
plot(f,f.*mean(before(mice(1:endt)==1,:)) + f.*std(before(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'k')
plot(f,f.*mean(before(mice(1:endt)==1,:)) - f.*std(before(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'k')

plot(f,f.*mean(after(mice(1:endt)==1,:)),'r')
plot(f,f.*mean(after(mice(1:endt)==1,:)) + f.*std(after(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'r')
plot(f,f.*mean(after(mice(1:endt)==1,:)) - f.*std(after(mice(1:endt)==1,:))/sqrt(length(mice(1:endt)==1)),'r')


xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('lgdel mean','lgdel SEM','lgdel SEM','NRG1 mean','NRG1 SEM','NRG1 SEM')

subplot(3,1,3)
plot(f,f.*mean(before(mice(1:endt)==2,:)),'k')
hold on;
plot(f,f.*mean(before(mice(1:endt)==2,:)) + f.*std(before(mice(1:endt)==2,:))/sqrt(length(mice(1:endt)==2)),'k')
plot(f,f.*mean(before(mice(1:endt)==2,:)) - f.*std(before(mice(1:endt)==2,:))/sqrt(length(mice(1:endt)==2)),'k')

plot(f,f.*mean(between(mice(1:endt)==2,:)),'r')
plot(f,f.*mean(between(mice(1:endt)==2,:)) + f.*std(between(mice(1:endt)==2,:))/sqrt(length(mice(1:endt)==2)),'r')
plot(f,f.*mean(between(mice(1:endt)==2,:)) - f.*std(between(mice(1:endt)==2,:))/sqrt(length(mice(1:endt)==2)),'r')


xlabel('Frequency (Hz)')
ylabel('Power (arb. unit)')
legend('lgdelPVcre mean','lgdelPVcre SEM','lgdelPVcre SEM','CNO mean','CNO SEM','CNO SEM')


for sb = 1 : 3; subplot(3,1,sb);xlim([20 90]); ylim([0.00003 0.0005]); end 
allspectra{1} = before; allspectra{2} = between; allspectra{3} = after;

bands = [1 4;5 8; 9 15; 16 30; 31 80];
bandN = {'delta' 'theta' 'alpha' 'beta' 'gamma'};
for bb = 1 : 5
    frange = bands(bb,:);
    allspectra{1} = before; allspectra{2} = between; allspectra{3} = after;
    alllb = {'baseline' '30min after NRG1' '60min after NRG1'};
    fs = find(f >= frange(1) & f <= frange(2));
    mspectra = cell(3,1);
    WRSpvalue = ones(3,1);
    ttestpvalue = ones(3,1);
    figure
    for p = 1 : 3
        mspectra{p} = nanmean(allspectra{p}(:,fs),2);
        %    WRSpvalue(p) = ranksum(mspectra{p}(mice & alltheta),mspectra{p}(~mice & alltheta));
        %    [h, ttestpvalue(p)] = ttest2(mspectra{p}(mice & alltheta),mspectra{p}(~mice & alltheta));
        
        WRSpvalue(p) = ranksum(mspectra{p}(mice == 1),mspectra{p}(~mice));
        [h, ttestpvalue(p)] = ttest2(mspectra{p}(mice ==1),mspectra{p}(~mice));
        subplot(1,3,p)
        boxplot(mspectra{p},mice == 1,'labels',{'wt' 'lgdel'});
        hold on
        title(sprintf('pvalue = %f',ttestpvalue(p)))
        ylim([0 3.5*10^-4])
        grid on
        xlabel(alllb{p})
    end
    ylabel(sprintf('mean power in the %s range (arb. units)',bandN{bb}))
    
    figure;
    subplot(1,2,1)
    da = [mspectra{1}(mice == 1) mspectra{3}(mice == 1)]';
    plot(da,'y')
    hold on
    errorbar(mean(da,2), std(da,[],2)/sqrt(sum(mice)),'r')
    pv = signrank(mspectra{1}(mice == 1),mspectra{3}(mice == 1));
    title(sprintf('lgdel plus NRG1,n=%d, p=%d',sum(mice == 1),pv))
    xlim([0 3]);
    ylabel(bandN{bb})
    subplot(1,2,2)
    da = [mspectra{1}(~mice) mspectra{3}(~mice)]';
    plot(da,'y')
    hold on
    errorbar(mean(da,2), std(da,[],2)/sqrt(sum(~mice)),'k')
    pv = signrank(mspectra{1}(~mice),mspectra{3}(~mice));
    title(sprintf('WT plus NRG1,n=%d, p=%d',sum(~mice),pv))
    xlim([0 3]);
    
     figure;
    subplot(1,2,1)
    da = [mspectra{1}(mice == 2) mspectra{3}(mice == 2)]';
    plot(da,'y')
    hold on
    errorbar(mean(da,2), std(da,[],2)/sqrt(sum(mice)),'r')
    pv = signrank(mspectra{1}(mice == 2),mspectra{2}(mice == 2));
    title(sprintf('lgdelPV-cre plus CNO,n=%d, p=%d',sum(mice == 2),pv))
    xlim([0 3]);
    ylabel(bandN{bb})
   
end
%% CNO

