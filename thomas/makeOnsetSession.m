function [onsets,varargout] = makeOnsetSession(varargin)

Args = struct('getperiodAboveHz',0,'timePeriod',[]);
Args.flags = {'getperiodAboveHz'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

groups = nptDir('group0*');
sdir = pwd;
c = 1;
for gr = 1 : length(groups)
    cd(groups(gr).name)
    clusters = nptDir('cluster*s');
    gdir = pwd;
    for cl = 1 : length(clusters)
        cd(clusters(cl).name)
        load ispikes
        if isempty(Args.timePeriod)
            onsets{c,1} = double(sp.data.trial.cluster.spikes);
        else
            timeserie = double(sp.data.trial.cluster.spikes);
            sind = find(timeserie >= Args.timePeriod(1) & timeserie <= Args.timePeriod(2));
            onsets{c,1} = timeserie(sind);
        end
        if Args.getperiodAboveHz
            [time{c},speriod{c}] = periodAboveHz(sp);
        end
        cd(gdir)
        c = c + 1;
    end
    
    cd(sdir)
    
end
if Args.getperiodAboveHz
varargout{1} = time;
varargout{2} = speriod;
end