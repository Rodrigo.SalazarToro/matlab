
files = nptDir('*.atf');
for ff = 1 : length(files)
    
    [header time data] = getaxondata(files(ff).name);
    
    
    [prelfp,SR] = preprocessinglfp(data,'SR',header.SamplingFreq,'downSR',200,'bandP',[.1 90],'No60Hz','Zscore','detrend');
    
    params =  struct('Fs',SR,'tapers',[4 7],'fpass',[.2 20]);
    [S,t,f]=mtspecgramc(prelfp,[3 .1],params);
    
    figure; imagesc(t,f,S',[0 0.2]); set(gcf, 'Name',files(ff).name);
end

