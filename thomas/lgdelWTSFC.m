% clear all
% 
% sdir = 'F:\salazar\data\Electrophy\hippocampus';
% cd(sdir)
% days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622' '20160916' '20161021' '20161027' '20161103' '20161103b' '20161125' '20161201' '20161201b'};
% obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
% 
% mice = {'wt' 'lgdel'};
% SFC = cell(2,1);
% sigSFC =cell(2,1);
% for m = 1 : 2
%     [r,ind] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp'); % select for hipp cells
% %      [r,ind] = get(obj,'Number','mouse',mice{m},'unit','s','histo','mPFC'); % select for mPFC cells
%    
%     for ii = 1: r
%         cd(obj.data.setNames{ind(ii)})
%         if ~isempty(nptDir('mtsspikefield.mat')) && ~isempty(nptDir('mtsspikefieldSur.mat'))
%             data = load('mtsspikefield.mat');
%             sur = load('mtsspikefieldSur.mat');
%             data.C(data.C == 0) = nan;
%             data.Cerr(1,data.C == 0) = nan;
%             min30 = find(data.t <30*60);
%             if size(data.C,1) > 0 && isfield(sur,'thresh') && length(data.f) == 255
%                 SFC{m}(ii,:) = nanmean(data.C(min30,:));
%                 
%                 sigSFC{m}(ii,:) = nanmean(squeeze(data.Cerr(1,min30,:))) >0;
%                 sigSFC{m}(ii,:) = SFC{m}(c,:) > sur.thresh(:,5)';
%             end
%         end
%     end
% end
% cd(sdir)
% 
% save('lgdelWTSFC.mat')
% figure
% 
% plot(data.f, 100*sum(sigSFC{1}) / size(sigSFC{1},1),'k')
% hold on
% plot(data.f, 100*sum(sigSFC{2})/ size(sigSFC{2},1),'r')
% xlabel('Frequency (Hz)')
% ylabel('%significant SFC')
% legend(sprintf('n=%d SUA from WT',size(sigSFC{1},1)),sprintf('n=%d SUA from lgDel',size(sigSFC{2},1)))
% xlim([0 40])
%% NRG1
clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
mintimes = 5*60*1000;
maxtimes = 85*60*1000;

SFCdrug = cell(2,3);
SFCdrugsig = cell(2,3);
mice = {'wt' 'lgdel'};
FR = cell(2,3);

for m = 1 : 2
    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','hipp','minNumSpike',300,'NumSpikePeriod',[1 3]); % select for hipp cells
%    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','hipp','spikePtoTratio',{-1;'>='},'minNumSpike',500); % select for hipp cells
    %      [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','mPFC','minNumSpike',500,'NumSpikePeriod',[1 2 3],'thetapeak',[1 3]); % select for mPFC cells
    c = 1;
    for ii = 1: r
        cd(obj.data.setNames{ind(ii)})
        if ~isempty(nptDir('mtsspikefield.mat'))&& ~isempty(nptDir('mtsspikefieldSur.mat'))
            data = load('mtsspikefield.mat');
            sur = load('mtsspikefieldSur.mat');
            data.C(data.C == 0) = nan;
            min30 = find(data.t < (30*60));
            if size(data.C,1) > 0 && isfield(sur,'thresh') && length(data.f) == 255 && sum(sum(data.C)) ~= 0
                SFCdrug{m,1}(c,:) = nanmean(data.C(min30,:));
                                SFCdrugsig{m,1}(ii,:) = nanmean(squeeze(data.Cerr(1,min30,:))) >0;
%                 SFCdrugsig{m,1}(c,:) = SFCdrug{m,1}(c,:) > sur.thresh(:,5)';
                
                SFCdrug{m,2}(c,:) = nanmean(data.C(min30(end) : min30(end)+length(min30),:));
                                SFCdrugsig{m,2}(ii,:) = nanmean(squeeze(data.Cerr(1,min30(end) : min30(end)+length(min30),:))) >0;
%                 SFCdrugsig{m,2}(c,:) = SFCdrug{m,2}(c,:) > sur.thresh(:,5)';
                
                SFCdrug{m,3}(c,:) = nanmean(data.C(end - length(min30) : end,:));
                                SFCdrugsig{m,3}(ii,:) = nanmean(squeeze(data.Cerr(1,end - length(min30)  : end,:))) >0;
%                 SFCdrugsig{m,3}(c,:) = SFCdrug{m,3}(c,:) > sur.thresh(:,5)';
                
                load ispikes.mat
                spiketimes = double(sp.data.trial.cluster.spikes);
                FR{m,1}(c) = length(find(spiketimes < 30*60*1000));
                FR{m,2}(c) = length(find(spiketimes >= 30*60*1000 & spiketimes <= 60*60*1000));
                FR{m,3}(c) = length(find(spiketimes >= 60*60*1000 & spiketimes <= 90*60*1000));
                c = c + 1;
            else
                pwd
            end
        else
            pwd
        end
    end
end
cd(sdir)

save('lgdelWTSFCNRG1.mat')
% save('lgdelWTSFCNRG1thetapeak.mat')
lb = {'k' 'r'};
tits = {'30 min before NRG1' '30 min after NRG1' '60 min after NRG1'};
delta = find(data.f > 1 & data.f < 4);
theta = find(data.f >=5 & data.f <=8);
figure
for sb = 1 : 3
    for m = 1 : 2
        subplot(1,3,sb)
        plot(data.f,100*sum(SFCdrugsig{m,sb}) / size(SFCdrugsig{m,sb},1),lb{m})
        hold on
        title(tits{sb})
        ylim([0 10])
        xlim([0 40])
    end
    legend(sprintf('%d SUA sig in theta out of %d SUA in WT',length(find(sum(SFCdrugsig{1,sb}(:,theta),2) >0)),size(SFCdrugsig{1,sb},1)),sprintf('%d SUA sig in theta out of %d SUA in lgDel',length(find(sum(SFCdrugsig{2,sb}(:,theta),2) >0)),size(SFCdrugsig{2,sb},1)))
    
end
xlabel('Frequency (Hz)')
ylabel('%significant SFC')

lb ={'k' 'r'};
figure
for sb = 1 : 3
    for m = 1 : 2
        subplot(1,3,sb)
        %         plot(data.f,prctile(SFCdrug{m,sb},[10 25 50 75 90],1),lb{m})
        plot(data.f,nanmean(SFCdrug{m,sb}),lb{m})
%         plot(data.f,prctile(SFCdrug{m,sb},[25 50 75]),lb{m})
        hold on
        plot(data.f,nanmean(SFCdrug{m,sb}) + nanstd(SFCdrug{m,sb})/sqrt(size(SFCdrug{m,sb},1)),lb{m})
        plot(data.f,nanmean(SFCdrug{m,sb}) - nanstd(SFCdrug{m,sb})/sqrt(size(SFCdrug{m,sb},1)),lb{m})
        xlim([0 20])
        ylim([0.27 0.36])
        grid on
    end
    title(tits{sb})
end
xlabel('Frequency (Hz)')
ylabel('SFC')
legend('mean wt','mean+/-sem wt','mean+/-sem wt','mean lgdel','mean+/-sem lgdel','mean+/-sem lgdel')
alllb = {'baseline' '30min after NRG1' '60min after NRG1'};

frange = [5 8];
fs = find(data.f >= frange(1) & data.f <=frange(2));
mspectra = cell(3,1);
WRSpvalue = ones(3,1);
ttestpvalue = ones(3,1);
figure
for sb = 1 : 3
    
    mspectra{sb}(1:size(SFCdrug{1,sb},1)) = mean(SFCdrug{1,sb}(:,fs),2);
    mspectra{sb}(size(SFCdrug{1,sb},1)+1: size(SFCdrug{1,sb},1) + size(SFCdrug{2,sb},1)) = mean(SFCdrug{2,sb}(:,fs),2);
    mice = [zeros(1,size(SFCdrug{1,sb},1)) ones(1,size(SFCdrug{2,sb},1))];
    WRSpvalue(sb) = ranksum(mspectra{sb}(logical(mice)),mspectra{sb}(~mice));
    [h, ttestpvalue(sb)] = ttest2(mspectra{sb}(logical(mice)),mspectra{sb}(~mice));
    subplot(1,3,sb)
    boxplot(mspectra{sb},mice,'labels',{'wt' 'lgdel'});
   hold on
   title(sprintf('pvalue = %f',WRSpvalue(sb))) 
 ylim([0.2 .6])
 grid on
 xlabel(alllb{sb})
end


figure;

for sb = 1 : 3; 
    subplot(3,1,sb); 
    [n,xout] = hist(FR{1,sb},[500 :500:50000]); 
    plot(xout,n,'k'); 
    hold on
    [n,xout] = hist(FR{2,sb},[500 :500:50000]); 
    plot(xout,n,'r'); 
    p = ranksum(FR{1,sb},FR{2,sb});
    text(20000,10,['WRS test pvalue = ' num2str(p)])
    title(tits{sb})
end
xlabel('number of spikes')
ylabel('counts')
legend('wt','lgdel')
%% firing rate analysis on all stable SUA
tits = {'30 min before NRG1' '30 min after NRG1' '60 min after NRG1'};
mice = {'wt' 'lgdel'};
FR = cell(1,3);
mintimes = 5*60*1000;
maxtimes = 85*60*1000;
clear miceI
 c = 1;
for m = 1 : 2
    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','hipp','mintime',mintimes,'maxtime',maxtimes); % select for hipp cells
    %      [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','mPFC','minNumSpike',500,'NumSpikePeriod',[1 2 3],'thetapeak',[1 3]); % select for mPFC cells
   
    for ii = 1: r
        cd(obj.data.setNames{ind(ii)})
        load ispikes.mat
        spiketimes = double(sp.data.trial.cluster.spikes);
        FR{1}(c) = length(find(spiketimes < 30*60*1000));
        FR{2}(c) = length(find(spiketimes >= 30*60*1000 & spiketimes <= 60*60*1000));
        FR{3}(c) = length(find(spiketimes >= 60*60*1000 & spiketimes <= 90*60*1000));
        miceI(c) = m == 2;
        c = c + 1;
        
    end
end

figure;

for sb = 1 : 3
    subplot(2,3,sb)
   boxplot(FR{sb},miceI,'labels',mice) 
     p = ranksum(FR{sb}(miceI),FR{sb}(~miceI));
    title(['WRS test pvalue = ' num2str(p)])
    xlabel(tits{sb})
    ylim([0 85000])
    
    subplot(2,3,sb+3)
    B = cumsum(FR{sb}(miceI));
    plot(B,'k');
    hold on
    B = cumsum(FR{sb}(~miceI));
     plot(B,'r');
     legend(mice)
     ylabel('Number of spikes')
     xlabel('cell #')
end
ylabel('Number of spikes')


