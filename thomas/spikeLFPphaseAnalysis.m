
% clear all
%
% sdir = 'F:\salazar\data\Electrophy\hippocampus';
% cd(sdir)
% days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622' '20160914' '20160916' '20161021' '20161027' '20161103' '20161103b' '20161125' '20161201' '20161201b'};
%
% obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
% % band = 'Theta';
% mice = {'wt' 'lgdel'};
% ind = cell(2,1);
% nsigind = cell(2,1);
% r = zeros(2,1);
% tot = zeros(2,1);
% nsig = zeros(2,1);
%
%
% mintimes = 5*60*1000;
% maxtimes = 25*60*1000;
%
% figure;
% nspikes = cell(2,1);
% lb = {'k' 'r'};
%
%
% for m = 1 : 2;
%
%     [rt,indt] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp','mintime',mintimes,'maxtime',maxtimes,'minNumSpike',500,'NumSpikePeriod',[1 3]);
%     %     [rt,indt] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp');
%     nspikes{m} = obj.data.Index(indt,8);
%     [n,ce] = hist(nspikes{m},[0 : 1000 : 160000]);
%     plot(ce,n,lb{m})
%     hold on
%     plot(median(nspikes{m}),0,['*' lb{m}])
% end
% p = ranksum(nspikes{1},nspikes{2});
% title(sprintf('diff in medians WRS test p = %f',p))
% xlabel('# of spikes')
% ylabel('counts')
% legend(mice{1},'median',mice{2},'median')
%
%
% for m = 1 : 2
%     [r(m),ind{m}] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp','mintime',mintimes,'maxtime',maxtimes,'minNumSpike',500,'NumSpikePeriod',[1 3]);
%     bandd = eval(sprintf('obj.data.%sRayleigh',band));
%     tot(m) = sum(~isnan(bandd(ind{m})));
% end
% %     adjustedp = mafdr(obj.data.ThetaRayleigh(ind{m}),'BHFDR',true);% false discovery rate
% %     sind = find(adjustedp < 0.05);
% adjustedpvalue = 0.05 /sum(tot);
% for m = 1 : 2
%     [r(m),ind{m}] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp','mintime',mintimes,'maxtime',maxtimes,'minNumSpike',500,'NumSpikePeriod',[1 3]);
%     bandd = eval(sprintf('obj.data.%sRayleigh',band));
%     sind = find(bandd(ind{m}) < adjustedpvalue);  %      Bonferoni
%
%     nsig(m) = length(sind);
%     nsigind{m} = ind{m}(sind);
% end
%
% display(sprintf('percent SUA with sig phase in wt %f and lgdel %f',100*nsig(1)./tot(1),100*nsig(2)./tot(2)))
%
% figure
% for m = 1 : 2
%     subplot(2,2,m)
%     bandd = eval(sprintf('obj.data.%sMeanPhase',band));
%     [me nor] = circ_mean(bandd(nsigind{m},1));
%     rose(bandd(nsigind{m},1))
%     hold on
%     compass(cos(me)*nor,sin(me)*nor,'r')
%
%
%     [pvalR ~] = circ_vtest(bandd(nsigind{m},1), me);
%
%     title(sprintf('%s; pvalue %f; percent SUA with sig phase  %f',mice{m},pvalR,100*nsig(m)./tot(m)))
%
%     subplot(2,2,m+2)
%     hist(bandd(nsigind{m},2),[0:100:30000])
% end
%
% [pval, k, K] = circ_kuipertest(bandd(nsigind{2},1), bandd(nsigind{1}),1);
%
% %% per animal
% cd(sdir)
%
% fname = 'spikeLFPPhaseMouse.fig';
% for d = 1 : length(days)
%     cd(sdir)
%     objd = ProcessDays(micepsthStandard,'days',{days{d}},'NoSites');
%     cd(objd.data.setNames{1}); cd ../../..;
%     if isempty(nptDir(fname))
%         f1=figure;
%         [rd,indd] = get(objd,'Number','unit','s','histo','hipp','mintime',2*60,'maxtime',28*60);
%         bandd = eval(sprintf('obj.data.%sRayleigh',band));
%         totd = sum(~isnan(bandd(indd)));
%         sind = find(bandd(indd) < adjustedpvalue);
%         [me nor] = circ_mean(bandd(indd(sind),1));
%         compass(cos(me)*nor,sin(me)*nor,'r')
%         hold on
%         bandd = eval(sprintf('obj.data.%sMeanPhase',band));
%         rose(bandd(indd(sind),1))
%         set(gcf,'Name',sprintf('%s %s',days{d},objd.data.mouse{1}))
%         saveas(f1,fname)
%         display(['saving ' pwd filesep fname])
%         close(f1)
%     else
%         openfig(fname)
%
%     end
% end

%% NRG1

clear all

sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)



days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160610' '20160914' '20160916' '20161020'  '20161020b' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807'  '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
adjustedpvalue = 10^-4 ;
band = 'Theta';
cumphases = cell(3,2);
cumnorm = cell(3,2);
nsig = zeros(3,2);
cumnspikes = cell(3,2);
tot = zeros(3,2);
fr = cell(2,1);
allhist = cell(3,2);
mice = {'wt' 'lgdel'};
sig = cell(3,2);
for m = 1 : 2
    %     [r,ind] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp','drug',1,'minNumSpike',500,'NumSpikePeriod',[1 3]);
    [r,ind] = get(obj,'Number','mouse',mice{m},'unit','s','histo','hipp','drug',1);
    for ii = 1 : r
        cd(obj.data.setNames{ind(ii)})
        if strcmp(band,'Theta')
            sfc{1} = load('spikeLFPphases30min.mat');
            sfc{2} = load('spikeLFPphases60min.mat');
            sfc{3} = load('spikeLFPphases90min.mat');
        elseif strcmp(band,'Delta')
            sfc{1} = load('spikeLFPphasesDelta30min.mat');
            sfc{2} = load('spikeLFPphasesDelta60min.mat');
            sfc{3} = load('spikeLFPphasesDelta90min.mat');
        end
        
        for ti = 1 : 3
            if ~isempty(sfc{ti}.phases)
            if (sfc{ti}.pval < (adjustedpvalue)) %&& (length(sfc{ti}.phases) > 30*60)
                
                nsig(ti,m) = nsig(ti,m) +1 ;
                sig{ti,m} = [sig{ti,m} 1];
            else
                sig{ti,m} = [sig{ti,m} 0];
            end
            if ~isnan(sfc{ti}.mea) %&& (length(sfc{ti}.phases) > 30*60)
                tot(ti,m) = tot(ti,m) + 1;
                cumnspikes{ti,m} = [cumnspikes{ti,m} length(sfc{ti}.phases)];
                cumphases{ti,m} = [cumphases{ti,m} sfc{ti}.mea];
                %                 cumphases{ti,m} = [cumphases{ti,m} circ_median(sfc{ti}.phases,2)];
                cumnorm{ti,m} = [cumnorm{ti,m} sfc{ti}.nor/length(sfc{ti}.phases)];
            end
            [tout,rout] = rose(sfc{ti}.phases,12);
            allhist{ti,m}(ii,:) = rout/length(sfc{ti}.phases);
            end
        end
        % firing rate
        load ispikes.mat
        timeserie = sp.data.trial.cluster.spikes;
        [n,xout] = hist(timeserie,[0 : 10000: 91*60*1000]);
        fr{m}(ii,:) = n(1:end-1);
    end
end

figure
for ti = 1 : 3
    subplot(3,1,ti)
    [n,xout] = hist(cumnspikes{ti,1},[0 : 1000 : 160000]);
    plot(xout,n,'k')
    hold on
    [n,xout] = hist(cumnspikes{ti,2},[0 : 1000 : 160000]);
    plot(xout,n,'r')
    
    p = ranksum(cumnspikes{ti,2},cumnspikes{ti,1});
    title(sprintf('diff in medians spike counts WRS test p = %f',p))
    xlabel('# of spikes')
    ylabel('counts')
    legend(mice(:))
end

timeP ={'30 min' '60 min' '90min'};
for tt = 1 : 3
    display(timeP{tt})
    display(sprintf('percent SUA with sig phase in wt %f and lgdel %f',100*nsig(tt,1)./tot(tt,1),100*nsig(tt,2)./tot(tt,2)))
end

figure
for ti = 1 : 3
    subplot(2,3,ti)
    rose(cumphases{ti,1},12)
    title(sprintf('wt; percent SUA with sig phase %f',100*nsig(ti,1)./tot(ti,1)))
    subplot(2,3,ti + 3)
    rose(cumphases{ti,2},12)
    title(sprintf('lgdel; percent SUA with sig phase %f',100*nsig(ti,2)./tot(ti,2)))
    xlabel(timeP{ti})
end
set(gcf,'Name','distribution and %sig spikeLFPphase')
figure
lb = {'k' 'r'};
for ti = 1 : 3
    for m = 1 : 2
        subplot(3,2,(ti-1) * 2 +1)
        [n,xout] = hist(cumnorm{ti,m},[0:.02:.6]);
        plot(xout,100*n/length(cumnorm{ti,m}),lb{m})
        hold on
        plot(median(cumnorm{ti,m}),0,[lb{m} '*'])
        ylim([0 80])
        ylabel('%')
        xlabel('norm of the mean phase')
        grid on
    end
    p = ranksum(cumnorm{ti,1},cumnorm{ti,2});
    legend(mice{1},'median',mice{2},'median')
    title([timeP{ti} ' WRS test for norm phase p = ' num2str(p)])
    subplot(3,2,(ti-1) * 2 +2)
    boxplot([cumnorm{ti,1} cumnorm{ti,2}],[zeros(1,length(cumnorm{ti,1})) ones(1,length(cumnorm{ti,2}))],'labels',mice)
    hold on
    ylim([0 0.55])
    ylabel('norm of the mean phase')
    title(timeP{ti})
end

figure

plot([cumnorm{1,2}; cumnorm{3,2}])
