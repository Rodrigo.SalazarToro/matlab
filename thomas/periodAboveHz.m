function [time,speriod] = periodAboveHz(sp,varargin)

Args = struct('psthbin',10000,'minFR',2);
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

timeserie = double(sp.data.trial.cluster.spikes);

    maxtime =  timeserie(end) + Args.psthbin;

[nspikes,time] = hist(timeserie,[0 : Args.psthbin : maxtime]);
firingRate = 1000 * nspikes / Args.psthbin;


speriod = find(firingRate >= Args.minFR);