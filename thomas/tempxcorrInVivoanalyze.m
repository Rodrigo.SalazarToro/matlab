%% xcorr for drug in 30 min epochs

clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160610' '20160914' '20160916' '20161020'  '20161020b' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807'  '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'
days = unique(days);
% days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020' '20161020b' '20170111' '20170807' '20170808' '20170814' '20170815' '20170821' '20170822' '20170822'};


% for now. Also missing data 20160914-16

c = 1 ;
epairs = [];
files = {'xcorrSpikes30min2.mat' 'xcorrSpikes60min2.mat' 'xcorrSpikes90min2.mat'};
for d = 1 : length(days)
    cd(days{d})
    
    if ~isempty(nptDir('mouse.txt'))
        mouse = textread('mouse.txt','%s');
        switch mouse{1}
            case {'lgdel'}
                lgDel = 1;
            case {'wt'}
                lgDel = 0;
            case {'lgdelPV-cre'}
                lgDel = 2;
        end
    else
        lgDel = nan;
        %         pwd
    end
    
    
    sessions = nptDir('session0*');
    
    for ss = length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('xcorrSpikes30min2.mat')) && ~isempty(nptDir('xcorrSpikes60min2.mat')) && ~isempty(nptDir('xcorrSpikes90min2.mat'))&& ~isempty(nptDir('drug.txt'))
            pwd
            
            for ff = 1 : 3
                a = load(files{ff});
                
              tcoi = a.cumcoi(find(sum(a.cumcoi,2) ~= 0),:);
                tsur = a.surcoi(:,find(sum(a.cumcoi,2) ~= 0),:);
                ncoi(ff,c,:) = sum(a.fdrvalues(find(sum(a.cumcoi,2) ~= 0),:) < (0.001/size(tcoi,2)));
                %                 ncoi(ff,c,:) = sum(a.cumcoi > squeeze(max(a.surcoi)));
                %                 ncoi(ff,c,:) = sum(a.cumcoi > squeeze(prctile(a.surcoi,99.9,1)));
                
                %                                                     totpairs(ff,c) = size(a.cumcoi,1);
                totpairs(ff,c) = sum(sum(a.cumcoi,2) ~= 0);
                epairs = cat(1,epairs,sum(a.cumcoi,2) == 0);
                mice(c) = lgDel;
                sdays(c) = d;
            end
            c = c + 1;
            
        end
        cd ..
    end
    cd ..
end

% figure;
%
% for sb = 1 : 3
%     subplot(3,1,sb)
%
%     plot(bins, squeeze(pcrtcoi(sb,mice == 1,:)),'r')
%     hold on
%      plot(bins, squeeze(pcrtcoi(sb,mice == 0,:)),'k')
% end


%% criterion for minimum number of pairs
minPairs = 10;

oldncoi = ncoi;
oldmice = mice;
oldtotpairs = totpairs;
clear ncoi mice totpairs
ncoi= oldncoi(:,find(sum(oldtotpairs > minPairs) >= 3) ,:);
mice = oldmice(find(sum(oldtotpairs > minPairs) >= 3));
totpairs = oldtotpairs(:,find(sum(oldtotpairs > minPairs) >= 3));
%%
figure;

for sb = 1 : 3
    subplot(3,3,(sb-1)*3 +1)
    
    plot(a.bins, 100*squeeze(ncoi(sb,mice == 1,:))./repmat(totpairs(sb,mice==1)',1,61),'r')
    subplot(3,3,(sb-1)*3 +2)
    plot(a.bins, 100*squeeze(ncoi(sb,mice == 0,:))./repmat(totpairs(sb,mice==0)',1,61),'k')
    if sb < 3
        subplot(3,3,sb*3 +3)
        plot(a.bins, 100*squeeze(ncoi(sb,mice == 2,:))./repmat(totpairs(sb,mice==2)',1,61),'b')
        hold on
    end
    title(files{sb})
end
for sb = 1 : 9; subplot(3,3,sb); ylim([0 50]); xlabel('lag (ms)');  ylabel('% pairs'); end
xlabel('Time lag (ms)')
ylabel('% significant pairs')

figure;

for sb = 1 : 3
    subplot(3,1,sb)
    
    plot(a.bins, prctile(100*squeeze(ncoi(sb,mice == 1,:))./repmat(totpairs(sb,mice==1)',1,61),[25 50 75]),'r')
    hold on
    plot(a.bins,prctile( 100*squeeze(ncoi(sb,mice == 0,:))./repmat(totpairs(sb,mice==0)',1,61),[25 50 75]),'k')
    if sb < 3
        subplot(3,1,sb+1)
        plot(a.bins,prctile( 100*squeeze(ncoi(sb,mice == 2,:))./repmat(totpairs(sb,mice==2)',1,61),[25 50 75]),'b')
        hold on
    end
    title(files{sb})
    xlabel('lag (ms)')
    ylabel('% pairs')
    ylim([0 30])
end
legend('lgdelPV-cre','lgdelPV-cre','lgdelPV-cre','lgdel','lgdel','lgdel','wt','wt','wt')
theprctiles = [33 50 66];
f1 = figure('Name','mean +/- SEM');
f2 = figure('Name',sprintf('prctiles %dth %dth %dth',theprctiles(1),theprctiles(2),theprctiles(3)));

for sb = 1 : 3
    figure(f1)
    subplot(3,1,sb)
    
    me1 = nanmean(100*squeeze(ncoi(sb,mice == 1 | mice == 2,:))./repmat(totpairs(sb,mice == 1 | mice == 2)',1,61));
    sem1 = nanstd(100*squeeze(ncoi(sb,mice == 1 | mice == 2,:))./repmat(totpairs(sb,mice==1 | mice == 2)',1,61)) / sqrt(sum(mice ==1 | mice == 2)) ;
    prct1 = prctile(100*squeeze(ncoi(sb,mice == 1 | mice == 2,:))./repmat(totpairs(sb,mice == 1 | mice == 2)',1,61),theprctiles);
    
    me2 = nanmean(100*squeeze(ncoi(sb,mice == 0,:))./repmat(totpairs(sb,mice==0)',1,61));
    sem2 = nanstd(100*squeeze(ncoi(sb,mice == 0,:))./repmat(totpairs(sb,mice==0)',1,61)) / sqrt(sum(mice ==0)) ;
    prct2 = prctile(100*squeeze(ncoi(sb,mice == 0,:))./repmat(totpairs(sb,mice==0)',1,61),theprctiles);
   figure(f1)
    plot(a.bins, me1,'r');
    hold on;
    plot(a.bins, me1+sem1,'r'); plot(a.bins, me1-sem1,'r');
    
    plot(a.bins, me2,'k');
    plot(a.bins, me2+sem2,'k'); plot(a.bins, me2-sem2,'k');
    title(files{sb})
    xlabel('lag (ms)')
    ylabel('% pairs')
    ylim([0 30])
    figure(f2)
     subplot(3,1,sb)
    plot(a.bins, prct1,'r');
    hold on
     plot(a.bins, prct2,'k');
     title(files{sb})
    xlabel('lag (ms)')
    ylabel('% pairs')
end
theprctiles = [50];
figure
subplot(2,1,1)
plot(a.bins, prctile(100*squeeze(ncoi(1,mice == 1,:))./repmat(totpairs(1,mice == 1)',1,61),theprctiles),'r');
hold on
plot(a.bins, prctile(100*squeeze(ncoi(3,mice == 1,:))./repmat(totpairs(3,mice == 1)',1,61),theprctiles),'b');
plot(a.bins, prctile(100*squeeze(ncoi(1,mice == 0,:))./repmat(totpairs(1,mice == 0)',1,61),theprctiles),'k');
plot(a.bins, prctile(100*squeeze(ncoi(3,mice == 0,:))./repmat(totpairs(3,mice == 0)',1,61),theprctiles),'k--');

title('Lgdel (red) + NRG1 (blue) & WT (black) + NRG1 ((dashed black); prctiles')
xlim([-200 200]); ylim([ 0 20])
subplot(2,1,2)
plot(a.bins, prctile(100*squeeze(ncoi(1,mice == 2,:))./repmat(totpairs(1,mice == 2)',1,61),theprctiles),'c');

hold on
plot(a.bins, prctile(100*squeeze(ncoi(2,mice == 2,:))./repmat(totpairs(2,mice == 2)',1,61),theprctiles),'g');
title('Lgdel PV-cre (magenta) + CNO (green); prctiles')
xlim([-200 200]); ylim([ 0 20])
xlabel('Lags (ms)')
ylabel('Median % sign')


figure
percoi = 100*ncoi./repmat(totpairs,1,1,61);
lb = {'k','r','b'};
lagrange = [-60 60];
sbins = find(a.bins >= lagrange(1) & a.bins <= lagrange(2) );
mlines = {'wt' 'lgdel','lgdel pv-cre'};
for mm = 0:2
    subplot(1,3,mm+1)
    if mm ==2
        %         plot([1 3],squeeze(nanmean(percoi(1:2,mice == mm,sbins),3)),[lb{mm+1}])
        %         hold on
        %         errorbar(1,mean(squeeze(nanmean(percoi(1,mice == mm,sbins),3))), std(squeeze(nanmean(percoi(1,mice == mm,sbins),3)))/sqrt(sum(mice==mm)),['o' lb{mm+1}])
        %         errorbar(3,mean(squeeze(nanmean(percoi(2,mice == mm,sbins),3))), std(squeeze(nanmean(percoi(2,mice == mm,sbins),3)))/sqrt(sum(mice==mm)),['o' lb{mm+1}])
        %         p =signrank(squeeze(nanmean(percoi(1,mice == mm,sbins),3)),squeeze(nanmean(percoi(2,mice == mm,sbins),3)));
        %
        plot([1 3],squeeze(max(percoi(1:2,mice == mm,sbins),[],3)),[lb{mm+1}])
        hold on
        errorbar(1,mean(squeeze(max(percoi(1,mice == mm,sbins),[],3))), std(squeeze(max(percoi(1,mice == mm,sbins),[],3)))/sqrt(sum(mice==mm)),['o' lb{mm+1}])
        errorbar(3,mean(squeeze(max(percoi(2,mice == mm,sbins),[],3))), std(squeeze(max(percoi(2,mice == mm,sbins),[],3)))/sqrt(sum(mice==mm)),['o' lb{mm+1}])
        p =signrank(squeeze(max(percoi(1,mice == mm,sbins),[],3)),squeeze(max(percoi(2,mice == mm,sbins),[],3)));
        
        text(2,25,sprintf('drug effect; WSR p=%g',p))
    else
        %         plot([1 3],squeeze(nanmean(percoi([1 3],mice == mm,sbins),3)),[lb{mm+1}])
        %         hold on
        %         for ep = [1 3]
        %             errorbar(ep,mean(squeeze(nanmean(percoi(ep,mice == mm,sbins),3))),std(squeeze(nanmean(percoi(ep,mice == mm,sbins),3)))/sqrt(sum(mice == mm)),['o' lb{mm+1}])
        %         end
        %         p =signrank(squeeze(nanmean(percoi(1,mice == mm,sbins),3)),squeeze(nanmean(percoi(3,mice == mm,sbins),3)));
        plot([1 3],squeeze(max(percoi([1 3],mice == mm,sbins),[],3)),[lb{mm+1}])
        hold on
        for ep = [1 3]
            errorbar(ep,mean(squeeze(max(percoi(ep,mice == mm,sbins),[],3))),std(squeeze(max(percoi(ep,mice == mm,sbins),[],3)))/sqrt(sum(mice == mm)),['o' lb{mm+1}])
        end
        p =signrank(squeeze(max(percoi(1,mice == mm,sbins),[],3)),squeeze(max(percoi(3,mice == mm,sbins),[],3)));
        text(1,25,sprintf('drug effect; WSR p=%g',p))
    end
    hold on
    ylim([0 50]);
    title(mlines{mm+1})
    xlim([0 4])
    grid on
end

ylabel(sprintf('Percent pairs averaged between -%d to %d ms time lag',lagrange(2),lagrange(2)))
xlabel('Epoch (1:baseline; 2:30 min after; 3:60 min after')
% themice = mice(mice == 0 | mice == 1);
% thedata = nanmean(percoi(:,mice == 0 | mice == 1,sbins),3);
%
% g1 = repmat([1 2 3],1, size(thedata,2));
% g2=[];for nm = 1 : size(thedata,2);  g2 = cat(2,g2,themice(nm) + ones(1,3)); end
% thedata = reshape(thedata,1,size(thedata,1) * size(thedata,2)); % reshape column wise


% pLgDel_WT = ranksum([squeeze(nanmean(percoi(1,mice == 1,sbins),3)) squeeze(nanmean(percoi(1,mice == 2,sbins),3))],squeeze(nanmean(percoi(1,mice == 0,sbins),3)));
% display(sprintf('diff allLgDel (notreatment or first epoch LgDelPV-cre) and WT (no drug) p=%d',pLgDel_WT))
% 
% pLgDelNRG1 = ranksum(squeeze(nanmean(percoi(3,mice == 1,sbins),3)),squeeze(nanmean(percoi(1,mice == 0,sbins),3)));
% display(sprintf('diff WT (no drug) and LgDel + NRG1 p=%d',pLgDelNRG1))
% 
% pLgDelCNO = ranksum(squeeze(nanmean(percoi(2,mice == 2,sbins),3)),squeeze(nanmean(percoi(1,mice == 0,sbins),3)));
% display(sprintf('diff WT (no drug) and LgDelPV-cre + CNO p=%d',pLgDelCNO))

pLgDel_WT = ranksum([squeeze(max(percoi(1,mice == 1,sbins),[],3)) squeeze(max(percoi(1,mice == 2,sbins),[],3))],squeeze(max(percoi(1,mice == 0,sbins),[],3)));
display(sprintf('diff allLgDel (notreatment or first epoch LgDelPV-cre) and WT (no drug) p=%d',pLgDel_WT))

pLgDelNRG1 = ranksum(squeeze(max(percoi(3,mice == 1,sbins),[],3)),squeeze(max(percoi(1,mice == 0,sbins),[],3)));
display(sprintf('diff WT (no drug) and LgDel + NRG1 p=%d',pLgDelNRG1))

pLgDelCNO = ranksum(squeeze(max(percoi(2,mice == 2,sbins),[],3)),squeeze(max(percoi(1,mice == 0,sbins),[],3)));
display(sprintf('diff WT (no drug) and LgDelPV-cre + CNO p=%d',pLgDelCNO))

% [h,p] = ttest2(squeeze(nanmean(percoi(1,mice == 1,sbins),3)),squeeze(nanmean(percoi(1,mice == 0,sbins),3)));
% p= anovan(thedata,{g1,g2});

figure
subplot(1,3,1)
cmice = mice;
cmice(mice == 1 | mice ==2) = 1;
% boxplot(squeeze(nanmean(percoi(1,:,sbins),3)) ,cmice,'Labels',{'WT' 'allLgDel'},'Whisker',1.4);
boxplot(squeeze(max(percoi(1,:,sbins),[],3)) ,cmice,'Labels',{'WT' 'allLgDel'},'Whisker',1.4);
title('WT vs (LgDel+LgDelPV-cre) during baseline or first 30 min')

subplot(1,3,2)
% boxplot(squeeze(nanmean(percoi(2,:,sbins),3)) ,mice,'Labels',{'WT' 'lgdel' 'lgdel PV-cre'},'Whisker',1.4);
boxplot(squeeze(max(percoi(2,:,sbins),[],3)) ,mice,'Labels',{'WT' 'lgdel' 'lgdel PV-cre'},'Whisker',1.4);
title('Only use lgdel PV-cre as the +CNO condition')

subplot(1,3,3)
% boxplot(squeeze(nanmean(percoi(3,:,sbins),3)) ,mice,'Labels',{'WT' 'lgdel' 'lgdel PV-cre'},'Whisker',1.4);
boxplot(squeeze(max(percoi(3,:,sbins),[],3)) ,mice,'Labels',{'WT' 'lgdel' 'lgdel PV-cre'},'Whisker',1.4);

title('Only use lgdel as the +NRG1 condition')

ylabel('%pairs')

%% with normalization

figure
percoi = 100*ncoi./repmat(totpairs,1,1,61);
lb = {'k','r','b'};
lagrange = [-60 60];
sbins = find(a.bins >= lagrange(1) & a.bins <= lagrange(2) );
mlines = {'wt' 'lgdel','lgdel pv-cre'};
ndata = squeeze(nanmean(percoi(:,:,sbins),3));
ndata = 100*ndata ./ repmat(ndata(1,:),3,1);
for mm = 0:2
    subplot(1,3,mm+1)
    if mm ==2
        plot([1 3],ndata(1:2,mice == mm),[lb{mm+1}])
        hold on
%         errorbar(1,ndata(1,mice == mm), zeros(1,sum(mice == mm)),['o' lb{mm+1}])
        errorbar(3,mean(ndata(2,mice == mm)), std(ndata(2,mice == mm))/sqrt(sum(mice==mm)),['o' lb{mm+1}])
        p =signrank(ndata(1,mice == mm),ndata(2,mice == mm));
        text(2,500,sprintf('drug effect; WSR p=%g',p))
    else
        plot([1 3],ndata([1 3],mice == mm),[lb{mm+1}])
        hold on
        for ep = [1 3]
            if ep == 1
%                 errorbar(ep,ndata(ep,mice == mm), zeros(1,sum(mice == mm)),['o' lb{mm+1}])
                
            else
                errorbar(ep,mean(ndata(ep,mice == mm)),std(squeeze(ndata(ep,mice == mm)))/sqrt(sum(mice == mm)),['o' lb{mm+1}])
            end
        end
        p =signrank(ndata(1,mice == mm),ndata(3,mice == mm));
        text(1,400,sprintf('drug effect; WSR p=%g',p))
    end
    hold on
    ylim([50 600]);
    title(mlines{mm+1})
    xlim([0 4])
    grid on
end

ylabel(sprintf('Percent pairs averaged between -%d to %d ms time lag',lagrange(2),lagrange(2)))
xlabel('Epoch (1:baseline; 2:30 min after; 3:60 min after')