% coherence analysis

clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160914' '20161021' '20161027' '20161103' '20161125' '20161201'};
days = {'20161021' '20161027' '20161103' '20161125' '20161201'};% 20160914 has a coherence too high over all frequency

clear before after between mice cumg nmouse
c = 1;
for d = 1 : length(days)
    cd(days{d})
    if ~isempty(nptDir('mouse.txt')); mouse = textread('mouse.txt','%s'); end
    sessions = nptDir('session*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('drug.txt'))
            %             if ~exist('mouse','var'); mouse = textread('mouse.txt','%s'); end
            [dirs,grs] = findUnit;
            lgDel = strcmp(mouse{1},'lgdel');
            thegrs = unique(grs);
            timew = 35 * 60;
            cc=1;
            clear beforett betweentt aftertt
            for gg = 1 : length(thegrs)
                
                thefile = nptDir(sprintf('*cohgram_group01_*.mat',thegrs(gg)));
                for ff = 1 : length(thefile)
                    load(thefile(ff).name)
                    
                    tb = find(t < timew);
                    beforet = squeeze(median(squeeze(median(squeeze(median(C(:,:,tb,:)))))));
                    before(c,:) = beforet;
                    beforett(cc,:) = beforet;
                    aftert = squeeze(median(squeeze(median(squeeze(median(C(:,:,end-length(tb) : end,:)))))));
                    after(c,:) = aftert;
                    aftertt(cc,:) = aftert;
                    betweent = squeeze(median(squeeze(median(squeeze(median(C(:,:,tb(end) :tb(end) +length(tb),:)))))));
                    between(c,:) = betweent;
                    betweentt(cc,:) = betweent;
                    mice(c) = lgDel;
                    cumg(c) = gg;
                    nmouse(c) = d;
                    c = c + 1;
                    cc = cc + 1;
                end
            end
        end
        cd ..
    end
   cd(sdir)
    clear mouse
end
figure
subplot(3,1,1)
plot(f, before(mice ==1,:),'r')
hold on
plot(f, before(mice ==0,:),'k')

subplot(3,1,2)
plot(f, between(mice ==1,:),'r')
hold on
plot(f, between(mice ==0,:),'k')

subplot(3,1,3)
plot(f, after(mice ==1,:),'r')
hold on
plot(f, after(mice ==0,:),'k')

figure
subplot(3,1,1)
plot(f, mean(before(mice ==1,:)),'r')
hold on
plot(f, mean(before(mice ==1,:)) + std(before(mice ==1,:)) / sqrt(3*8),'r')
plot(f, mean(before(mice ==1,:)) - std(before(mice ==1,:)) / sqrt(3*8),'r')

plot(f, mean(before(mice ==0,:)),'k')
plot(f, mean(before(mice ==0,:)) + std(before(mice ==0,:)) / sqrt(3*8),'k')
plot(f, mean(before(mice ==0,:)) - std(before(mice ==0,:)) / sqrt(3*8),'k')

subplot(3,1,2)
plot(f,  mean(between(mice ==1,:)),'r')
hold on
plot(f,  mean(between(mice ==1,:)) + std(between(mice ==1,:)) / sqrt(3*8),'r')
plot(f,  mean(between(mice ==1,:)) - std(between(mice ==1,:)) / sqrt(3*8),'r')
plot(f,  mean(between(mice ==0,:)),'k')
plot(f,  mean(between(mice ==0,:)) + std(between(mice ==0,:)) / sqrt(3*8),'k')
plot(f,  mean(between(mice ==0,:)) - std(between(mice ==0,:)) / sqrt(3*8),'k')

subplot(3,1,3)
plot(f,  mean(after(mice ==1,:)),'r')
hold on
plot(f,  mean(after(mice ==1,:)) + std(after(mice ==1,:)) / sqrt(3*8),'r')
plot(f,  mean(after(mice ==1,:)) - std(after(mice ==1,:)) / sqrt(3*8),'r')

plot(f,  mean(after(mice ==0,:)) ,'k')
plot(f,  mean(after(mice ==0,:)) + std(after(mice ==0,:)) / sqrt(3*8),'k')
plot(f,  mean(after(mice ==0,:)) - std(after(mice ==0,:)) / sqrt(3*8),'k')

xlabel('Frequency (Hz)')
epochs = {'baseline 30 min' 'after NRG1 30min' 'after NRG1 60 min'};
ylabel('Coherence mPFC Hipp')
for sb = 1 : 3; subplot(3,1,sb);title(epochs{sb}); xlim([0 45]); ylim([ 0.2 .8]);end
legend('LgDel mean','lgDel SEM','lgDel SEM','wt mean','wt SEM','wt SEM');

tiles = [25 50 75];
figure
subplot(1,3,1)
plot(f,prctile(before(mice ==1,:),tiles),'r')
hold on
plot(f,prctile( before(mice ==0,:),tiles),'k')
ylim([0.2 .7])
xlim([0 45])
subplot(1,3,2)
plot(f,prctile( between(mice ==1,:),tiles),'r')
hold on
plot(f,prctile( between(mice ==0,:),tiles),'k')
ylim([0.2 .7])
xlim([0 45])
subplot(1,3,3)
plot(f, prctile(after(mice ==1,:),tiles),'r')
hold on
plot(f, prctile(after(mice ==0,:),tiles),'k')
ylim([0.2 .7])
xlim([0 45])

for mn = 1 : 6;
    bfcoh(mn,:) = median(before(nmouse == mn,:));
    btcoh(mn,:) = median(between(nmouse == mn,:));
    atcoh(mn,:) = median(after(nmouse == mn,:));
    themousei(mn) = unique(mice(nmouse == mn));
end
figure
subplot(3,1,1)
plot(f, bfcoh(themousei ==1,:),'r')
hold on
plot(f, bfcoh(themousei ==0,:),'k')

subplot(3,1,2)
plot(f, btcoh(themousei ==1,:),'r')
hold on
plot(f, btcoh(themousei ==0,:),'k')


subplot(3,1,3)
plot(f, atcoh(themousei ==1,:),'r')
hold on
plot(f, atcoh(themousei ==0,:),'k')
