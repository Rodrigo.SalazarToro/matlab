function cmTorun(cmd,varargin)

Args = struct('savefile',[],'stPopRate',0,'xcorr',0);
Args.flags = {'stPopRate','xcorr'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

diary(['diary' Args.savefile])
cd G:\thomas
sdir = pwd;

level1 = nptDir('Condition*');
count = 1;
miceLine = {'LgDel' 'WT'};
conditionType = {'CCH' 'CCHCNO' 'CCHFieldimaging' 'CCHNRG1' 'CCHPVeDREADD';'CCH' 'CCHCNO' 'CCHFieldimaging' 'CCHPViDREADD' ''};
allcond =  {'CCH' 'CCHCNO' 'CCHFieldimaging' 'CCHNRG1' 'CCHPVeDREADD' 'CCHPViDREADD'};

sumCoiPV = [];
sumCoiPVnonPV = [];
sumCoinonPV = [];
allstPopRate = [];
sigstPopRate = [];
allPV = [];
npairs = zeros(3,1);
for l1 = 1 : length(level1)
    cd(level1(l1).name)
    level2 = nptDir('Condition*');
    sdir2 = pwd;
    %         for c = fliplr([1 : length(conds)])
    for c = [1 : length(level2)]
        cd(level2(c).name)
        cdir = pwd;
        level3 = nptDir('1*');
        for e = 1 : length(level3)
            curAnal = [cdir filesep level3(e).name];
            cd(curAnal)
            
            if~isempty(nptDir('calcium.mat'))
                load('calcium');
                %                 onsets = handles.app.experiment.detections(1,2).onsets';
                try
                    eval(cmd)
                    %                     setNames{count: count + length(PV)} = repmat({pwd},length(PV),1);
                    if Args.stPopRate
                        miceL(count : count + length(PV) -1) = l1;
                        allPV(count : count + length(PV) -1) = PV;
                        %                     miceL{count : count + length(PV)} =  repmat({miceLine{l1}},length(PV),1);
                        %                     conditions{count : count + length(PV)} = repmat({conditionType{l1,c}},1,length(PV));
                        conditions(count : count + length(PV) -1) =  strmatch(conditionType{l1,c},allcond,'exact');
                        count = count + length(PV);
                    elseif Args.xcorr
                        miceL(count) = l1;
                        conditions(count) = strmatch(conditionType{l1,c},allcond,'exact');
                        npairsPV(count) = length(PVpairs);
                        npairsPVnonPVpairs(count) = length(PV_nonPVpairs);
                        npairsnonPVpairs(count) = length(nonPVpairs);
                        count = count + 1;
                    end
                    % pwd
                catch
                    
                    display(['Error ' pwd])
                end
            else
                display(['FAIS FASSE THOMAS ' pwd])
            end
            cd(cdir)
        end
        cd(sdir2)
    end
    cd(sdir)
end

if ~isempty(Args.savefile)
    if Args.stPopRate
        save(Args.savefile,'sigstPopRate','cbins','cmd','miceL','conditions','allstPopRate','allPV','allcond','miceLine')
        
    elseif Args.xcorr
        
        save(Args.savefile,'miceL','npairsPV','npairsPVnonPVpairs','npairsnonPVpairs','sumCoiPV','sumCoiPVnonPV','sumCoinonPV','conditions','allcond','miceLine')
        
    end
    display(['saving ' pwd filesep Args.savefile])
end

diary off