function [phase] = spikeLFPphase(varargin)
% to be run in the session folder
% for d = 1 : length(days);
%     cd(days{d});
%     sessions = nptDir('session*');
%     for ss = 1 : length(sessions);
%         cd(sessions(ss).name);
%         spikeLFPphase('save');
%         spikeLFPphase('save','bandpass',[1 4],'addName','Delta');
%         cd ..;
%     end;
%     cd ..;
% end
Args = struct('bandpass',[4 9],'save',0,'redo',0,'downsampled',100,'addName','Theta');
Args.flags = {'save','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

files = nptDir('*lfp_group*.mat');


for ff = 1 : length(files)
    if isempty(strfind(files(ff).name,'powergram.mat')) && isempty(strfind(files(ff).name,'LFPpeaks'))
        thefile = regexprep(files(ff).name,'lfp',sprintf('bpass%s',Args.addName));
        if isempty(nptDir(thefile)) || Args.redo
            data = load(files(ff).name);
            
            [bpass , ~]=nptLowPassFilter(median(double(data.lfp)),data.resampling,Args.downsampled,Args.bandpass(1),Args.bandpass(2));
            
            hb = hilbert(bpass);
            
            phase = angle(hb);
        else
            load(thefile)
        end
        
        if Args.save
            
            save(thefile,'phase','Args','bpass')
            display(['saving ' pwd filesep thefile])
        end
    end
end


