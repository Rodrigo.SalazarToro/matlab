%% processCond



conds = nptDir('Condition*');
sdir = pwd;
parfor c = 1 : length(conds)
    cd(conds(c).name)
    cdir = pwd;
    exps = nptDir('1*');
    for e = 1 : length(exps)
        cd(exps(e).name)
        
        pwd
        if isempty(nptDir('done.txt')) && ~isempty(nptDir('calcium.mat'))
            a = load('calcium');
            onsets = a.handles.app.experiment.detections(1,2).onsets;
            popPsth(onsets,'plot','save','savefig','redo')
        else
            display(['FAIS FASSE THOMAS ' pwd])
        end
      
        cd(cdir)
    end
    cd(sdir)
    
end