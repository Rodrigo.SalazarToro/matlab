function [dirs,grs] = findUnit(varargin)

Args = struct('unit','s');
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


groups = nptDir('group0*');
c = 1 ;
for gr = 1 : length(groups)
    cd(groups(gr).name)
    clusters = nptDir(sprintf('cluster*%s',Args.unit));
    if ~isempty(clusters)
        for uu = 1 : length(clusters)
            cd(clusters(uu).name)
            dirs{c} = pwd;
            grs(c) = str2num(groups(gr).name(6:end));
            c = c + 1;
            
            cd ..
        end
   
    end
    cd ..
end
if ~exist('dirs','var'); dirs = []; end
if ~exist('grs','var'); grs = []; end