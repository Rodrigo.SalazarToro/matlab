function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'unit',[],'ChR2',[],'mouse',[],'drug',[],'histo',[],'mintime',[],'maxtime',[],'minNumSpike',[],'maxNumSpike',[],'NumSpikePeriod',1,'thetapeak',[],'spikePtoTratio',[]);
Args.flags = {'Number','ObjectLevel','bothRules'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.unit)
        ttemp = find(obj.data.Index(:,2) == strmatch(Args.unit,{'s' 'm'}));
    else
        ttemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.ChR2)
        runits = [];
        for el = 1 : length(Args.ChR2)
           runits =  [runits; find(obj.data.Index(:,3) == Args.ChR2(el))];
        end
        ttemp = intersect(runits,ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
    if ~isempty(Args.mouse)
       mice = find(strcmp(obj.data.mouse,Args.mouse));
         ttemp = intersect(mice,ttemp);
        
    end
    if ~isempty(Args.drug)
    ttemp = intersect(ttemp, find(obj.data.Index(:,5) == Args.drug));
    end
    
    if ~isempty(Args.minNumSpike)
        for pp = 1 : length(Args.NumSpikePeriod)
            ttemp = intersect(ttemp, find(obj.data.Index(:,7+ Args.NumSpikePeriod(pp)) >= Args.minNumSpike));
        end
    end
    
    if ~isempty(Args.maxNumSpike)
        for pp = 1 : length(Args.NumSpikePeriod)
            ttemp = intersect(ttemp, find(obj.data.Index(:,7+ Args.NumSpikePeriod(pp)) <= Args.maxNumSpike));
        end
    end
    
    if ~isempty(Args.histo)
       histot = find(strcmp(obj.data.histo,Args.histo));
         ttemp = intersect(histot,ttemp);
    end
    
    if ~isempty(Args.mintime)
        mtime = find(obj.data.Index(:,6) <= Args.mintime);
        ttemp = intersect(mtime,ttemp);
    end
    
    if ~isempty(Args.maxtime)
        mtime = find(obj.data.Index(:,7) >= Args.maxtime);
        ttemp = intersect(mtime,ttemp);
    end
    
    if ~isempty(Args.thetapeak)
        thetat = [];
       for pp = 1 : length(Args.thetapeak)
         thetat = union(thetat, find(obj.data.thetapeak(:,Args.thetapeak(pp))));
        
       end 
        ttemp = intersect(thetat,ttemp);
    end
   if ~isempty(Args.spikePtoTratio)
        wv = eval(sprintf('find(obj.data.Index(:,11) %s Args.spikePtoTratio{1})', Args.spikePtoTratio{2}));
        ttemp = intersect(wv,ttemp);
    end
    varargout{1} = ttemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

