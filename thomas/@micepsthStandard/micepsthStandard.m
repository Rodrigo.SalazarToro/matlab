function obj = micepsth(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedLFP',[]);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'micepsthStandard';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mpsthst';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) && isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [~,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                && (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

end
%%
function obj = createObject(Args,varargin)
sdir = pwd;
drug = ~isempty(nptDir('drug.txt'));
cd ..

if isempty(nptDir('mouse.txt'))
    mouse = 'unknown';
else
    readt = textread('mouse.txt','%s');
    mouse = readt{1};
end
cd(sdir)

if ~isempty(nptDir('histo.mat'))
    load histo.mat
    
    
end
if ~isempty(nptDir('group0*'))
    
    groups = nptDir('group0*');
    if  ~isempty(groups)
        count = 1;
        data.Index = [];
        sortdir = pwd;
        
        for g = 1 : size(groups,1)
            thegroup = str2double(groups(g).name(6:end));
            peakfile = nptDir(sprintf('*lfp_group0%dLFPpeaksNRG1.mat',thegroup));
            if ~isempty(peakfile)
            peak = load(peakfile.name);
            else
                
                peak.peaks{1} = nan;
            end
            cd(groups(g).name)
            gdir = pwd;
            
            clusters = nptDir('cluster*');
            for c = 1 : size(clusters,1)
                cd(clusters(c).name)
                
                if ~isempty(nptDir('ChR2.txt'))
                    ChR2 = 1;
                elseif ~isempty(nptDir('NB.txt'))
                    ChR2 = 2;
                else
                    ChR2 = 0;
                end
                load ispikes.mat
                data.Index(count,1) = count;
                
                if clusters(c).name(end) == 'm';  data.Index(count,2) = 2; else  data.Index(count,2) = 1; end
                % 1 for SUA 2 for MUA
                data.Index(count,3) = ChR2;
                data.Index(count,4) = 1;
                data.Index(count,5) = drug;
                
                if isempty(nptDir('stationary.mat'));
                    stationarity('save');
                end
                load stationary.mat
                data.Index(count,6) = mintime;
                data.Index(count,7) = maxtime;
                
               
                if ~exist('histo','var')
                    data.histo{count} = 'unknown';
                else
                    data.histo{count} = histo{g};
                end
                data.mouse{count} = mouse;
                if isempty(nptDir('spikeLFPphases30min.mat'))
                    spikeLFPphasesStats('save','redo','addName','30min','timeLimit',[0 35*60*1000]);
                    spikeLFPphasesStats('save','redo','addName','60min','timeLimit',[30*60*1000 60*60*1000]);
                    spikeLFPphasesStats('save','redo','addName','90min','timeLimit',[60*60*1000 90*60*1000]);
                    
                    spikeLFPphasesStats('save','redo','band','Delta','addName','Delta30min','timeLimit',[0 35*60*1000]);
                    spikeLFPphasesStats('save','redo','band','Delta','addName','Delta60min','timeLimit',[30*60*1000 60*60*1000]);
                    spikeLFPphasesStats('save','redo','band','Delta','addName','Delta90min','timeLimit',[60*60*1000 90*60*1000]);
                    
                end
                ci = load('spikeLFPphases30min.mat');
                data.ThetaRayleigh(count,1) = ci.pval;
                data.ThetaMeanPhase(count,1)= ci.mea;
                data.ThetaMeanPhase(count,2)= ci.nor;
                data.ThetaSTD(count,1) = ci.s0;
                
                ci = load('spikeLFPphasesDelta30min.mat');
                data.DeltaRayleigh(count,1) = ci.pval;
                data.DeltaMeanPhase(count,1)= ci.mea;
                data.DeltaMeanPhase(count,2)= ci.nor;
                data.DeltaSTD(count,1) = ci.s0;
                
                ci = load('spikeLFPphases30min.mat');
                data.Index(count,8) = length(ci.phases);
                ci = load('spikeLFPphases60min.mat');
                data.Index(count,9) = length(ci.phases);
                ci = load('spikeLFPphases90min.mat');
                data.Index(count,10) = length(ci.phases);
                % odor and sound onset & discrimination
                if ~isnan(peak.peaks{1})
                for pp = 1 : 3; data.thetapeak(count,pp) = sum(peak.peaks{pp} >=4 & peak.peaks{pp} <= 11);end
                else
                    data.thetapeak(count,:) = nan(1,3);
                end
                 %% calculation of the spike peak-to trough ratio see Moore & Wehr 2013
                upsidedown = {'F:\salazar\data\Electrophy\hippocampus\20160412' 'F:\salazar\data\Electrophy\hippocampus\20161125\session01\group0006' 'F:\salazar\data\Electrophy\hippocampus\20161125\session01\group0007' 'F:\salazar\data\Electrophy\hippocampus\20161125\session01\group0008' 'F:\salazar\data\Electrophy\hippocampus\20161125\session01\group0009'}; 
                 
                if isfield(sp.data.trial.cluster,'waveform')
                   
                    if ~isempty(strfind(upsidedown,pwd))
                        sp.data.trial.cluster.waveform = -sp.data.trial.cluster.waveform;
                    end
                     mwv = squeeze(mean(sp.data.trial.cluster.waveform,2));
                    spikeA = max(mwv,[],2) + abs(min(mwv,[],2));
                    [~,bestElec] = max(spikeA);
                    ptratio = max(mwv(bestElec,:)) / min(mwv(bestElec,:));
                else
                    ptratio = nan;
                    
                end
                data.Index(count,11) = ptratio;
                %%
                data.setNames{count} = pwd;
                count = count + 1;
                cd(gdir)
            end%c = 1 : size(clusters,1)
            cd(sortdir)
        end%g = 1 : size(group,1)
        data.numSets = count-1; % nbr of trial
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('empty object \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('empty object \n');
    obj = createEmptyObject(Args);
end
cd(sdir)
end
function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.mouse = [];
data.histo = [];
data.numSets = 0;

data.ThetaRayleigh = [];
data.ThetaMeanPhase= [];
data.ThetaSTD = [];
data.thetapeak = [];

data.DeltaRayleigh = [];
data.DeltaMeanPhase= [];
data.DeltaSTD = [];

data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
end