function obj = plot(obj,varargin)


Args = struct('psthbin',10000,'autocorrlength',1000,'autocorrbin',10,'maxtime',[],'plotType','standard','caxis',[0.25 0.55],'fmax',80,'newbinraster',[],'band','Theta');
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
% display(obj.data.Index(ind,28:37))

%% plotting fct
load ispikes
timeserie = double(sp.data.trial.cluster.spikes);

cats = {'light' 'odor' 'sound' 'odor + sound' 'all' 'odor1' 'odor2' 'odor3'  'odor1+sound' 'odor2+sound' 'odor3+sound' };
set(gcf,'Name',obj.data.setNames{ind})
switch Args.plotType
    
    case {'standard' }
        if isempty(Args.maxtime)
            maxtime =  timeserie(end);
        else
            maxtime = Args.maxtime ;
        end
        
        [nspikes,time] = hist(timeserie,[0 : Args.psthbin : maxtime + Args.psthbin]);
        plot(time(1:end-1)/1000,1000*nspikes(1:end-1)/Args.psthbin)
        xlabel('time (s)')
        ylabel('Firing rate (Hz)')
        load stationary.mat
        title(sprintf('%f',pvalue));
        %         display(pvalue)
    case 'spikefield'
        
        data = load('mtsspikefield.mat');
        subplot(2,2,1)
        imagesc(data.t,data.f,data.C')
        ylim([data.f(1) Args.fmax])
        caxis(Args.caxis)
        colorbar
        xlabel('time (s)')
        ylabel('Frequency (Hz)')
        title('coherence')
        
        subplot(2,2,3)
        data.C(data.C == 0) = nan;
        data.Cerr(1,data.C == 0) = nan;
        mC = nanmedian(data.C);
        plot(data.f,mC)
        hold on
        plot(data.f,squeeze(nanmean(data.Cerr(1,:,:),2)),'r--')
        sigC = find((squeeze(nanmean(data.Cerr(1,:,:),2)) >0) ==1);
        try plot(data.f(sigC),mC(sigC),'r*' ); end
        
        sur = load('mtsspikefieldSur.mat');
        plot(sur.f,sur.thresh,'k--')
        
        
        xlabel('Frequency (Hz)')
        ylabel('Coherence (arb. unit)')
        xlim([data.f(1) Args.fmax])
        ylim(Args.caxis)
        
        subplot(2,2,2)
        imagesc(data.t,data.f,data.phi')
        ylim([data.f(1) Args.fmax])
        
        
        colorbar
        xlabel('time (s)')
        ylabel('Frequency (Hz)')
        title('Phase')
        
        subplot(2,2,4)
        data.phi(data.phi == 0) = nan;
        plot(data.f,nanmedian(data.phi))
        xlabel('Frequency (Hz)')
        ylabel('phase (rad.)')
        xlim([data.f(1) Args.fmax])
        
        
    case 'autocorr'
        try
            data =load('autocorr.mat');
            
            plot(data.range,data.allcumspikes/sum(data.allcumspikes))
            xlabel('time lag (ms)')
            ylabel('# coincidences')
            
            
        end
    case 'mtspower'
        data = load('mtsautocorr.mat');
        subplot(2,1,1)
        imagesc(data.t,data.f,data.mtspwd')
        xlabel('time (s)')
        ylabel('Frequency (Hz)')
        ylim([data.f(1) Args.fmax])
        subplot(2,1,2)
        data.mtspwd(data.mtspwd == 0) = nan;
        plot(data.f,squeeze(nanmedian(data.mtspwd)))
        xlabel('Frequency (Hz)')
        ylabel('Power (arb. unit)')
        ylim([data.f(1) Args.fmax])
        
    case 'spikeLFPphase'
        if strcmp(Args.band,'Theta')
            load 'spikeLFPphases30min.mat'
        elseif strcmp(Args.band,'Delta')
            load 'spikeLFPphasesDelta30min.mat'
        end
        polar(tout,rout);
        hold on;
        compass(max(rout)*x,max(rout)*y,'r')
        title(sprintf('Rayleigh test p-value %f',pval))
    case 'spikeLFPphase90min'
        addName = {'30min' '60min' '90min'};
        for sb = 1 : 3
            if strcmp(Args.band,'Theta')
                load(sprintf('spikeLFPphases%s.mat',addName{sb}))
            elseif strcmp(Args.band,'Delta')
                load(sprintf('spikeLFPphases%s%s.mat',Args.band,addName{sb}))
            end
            subplot(1,3,sb)
            polar(tout,rout);
            hold on;
            compass(max(rout)*x,max(rout)*y,'r')
            title(sprintf('Rayleigh test p-value %f',pval))
        end
    case 'WV'
        if isfield(sp.data.trial.cluster,'waveform')
            yl(1) = min(min(min(sp.data.trial.cluster.waveform)));
            yl(2) = max(max(max(sp.data.trial.cluster.waveform)));
            for sb = 1 : size(sp.data.trial.cluster.waveform,1)
                subplot(2,4,sb)
                plot(squeeze(sp.data.trial.cluster.waveform(sb,:,:))')
                ylim(yl)
            end
            xlabel('samples')
        end
        case 'meanWV'
        if isfield(sp.data.trial.cluster,'waveform')
            yl(1) = min(min(min(sp.data.trial.cluster.waveform)));
            yl(2) = max(max(max(sp.data.trial.cluster.waveform)));
            for sb = 1 : size(sp.data.trial.cluster.waveform,1)
                subplot(2,size(sp.data.trial.cluster.waveform,1)/2,sb)
                plot(squeeze(mean(sp.data.trial.cluster.waveform(sb,:,:)))')
                ylim(yl)
            end
            xlabel('samples')
        end
end
%%
