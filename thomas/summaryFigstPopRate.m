cd G:\thomas

% cmTorun('spikeTrigPopRate(''save'');')
% cmTorun('load(''spikeTrigPopRate.mat''); allstPopRate=[allstPopRate;stPopRate]; sigstPopRate=[sigstPopRate;ostPopRate > squeeze(max(stPopRateSur))];','stPopRate','savefile','combinedData.mat')
load combinedData
%% spiketriggered pop rate


for thecond = 1 : 6
    f1=figure
    
    for themice = 1 : 2
        for thePV = 0 : 1
            
            selind = miceL == themice & allPV == thePV & conditions == thecond;
            if sum(selind) ~= 0
                subplot(4,2,thePV*4 + themice)
                plot(cbins,allstPopRate(selind,:)')
                xlabel('lag (frame)')
                ylabel('# events')
                title(sprintf('%s PV%d',miceLine{themice},thePV))
                
                subplot(4,2,thePV*4 + themice + 2)
                bar(cbins,100*sum(sigstPopRate(selind,:))/sum(selind))
                xlabel('lag (frame)')
                ylabel('% cells with sig stPopRate')
                title(sprintf('%s PV%d',miceLine{themice},thePV))
            end
        end
    end
    set(gcf,'Name',allcond{thecond})
    saveas(f1,[allcond{thecond} '_spTrPopRate'])
    close(f1)
end

%% xcorr
% processCondxcorr
% cmTorun('splitCellType(''addName'',''binSize2'',''lname1'',''sigCoibinSize2.mat'',''lname2'',''xcorrSpikesbinSize2.mat'')')
% cmTorun('load(''cellTypexcorrbinSize2.mat''); sumCoiPV = [sumCoiPV; allpairssigcoi_PVpairs]; sumCoiPVnonPV = [sumCoiPVnonPV; allpairssigcoi_PV_nonPVpairs]; sumCoinonPV = [sumCoinonPV; allpairssigcoi_nonPVpairs];','xcorr','savefile','combinedDataXC.mat')
load combinedDataXC

for cond = 1 : 6
    figure;
   
    for m = 1 : 2
         selind = conditions == cond & miceL == m;
       subplot(3,2,(m-1)*1 + 1)
        bar([-20:2:20],100*sum(sumCoiPV(selind,:)) / sum(npairsPV(selind)))
        title(sprintf('PV pairs of %s',miceLine{m}))
        
        subplot(3,2,(m-1)*1 + 3)
        bar([-20:2:20],100*sum(sumCoiPVnonPV(selind,:)) / sum(npairsPVnonPVpairs(selind)))
         title(sprintf('PV-nonPV pairs of %s',miceLine{m}))
        
        subplot(3,2,(m-1)*1 + 5)
        bar([-20:2:20],100*sum(sumCoinonPV(selind,:)) / sum(npairsnonPVpairs(selind)))
        title('nonPV pairs')
        
        xlabel('frame lag')
        ylabel('% pairs witrh sig xcorr')
    end
    set(gcf,'Name',allcond{cond})
    saveas(f1,[allcond{cond} '_xcorr'])
    close(f1)
end

