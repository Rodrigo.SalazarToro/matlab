% get the spike waveform

clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};% lgdel NRG1
% days = {'20160610' '20161020' '20161020b' '20170111' '20170222' '20170223'};% lgdelPVcre
baddata = [];
c = 1;
allmeanwv = cell(8,1);
for d = 1 : length(days)
    cd(days{d})
    sessions = nptDir('session0*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        nch = length(nptDir('CSC*.ncs'));
        clufiles = nptDir('*.clu.*');
        kwikfiles = nptDir('*.kwik');
        
        if length(kwikfiles) == 1 && isempty(nptDir('skip.txt'))
            for clu = 1 : length(clufiles)
                if isempty(findstr(clufiles(clu).name,'spikeWaveform'))
                    shank = str2double(clufiles(clu).name(end));
                    baseclufile = clufiles(clu).name(1:end-2);
                    switch nch
                        case 64
                            CSCchannels = [shank * 8 + 1 : shank * 8 + 8];
                        case 96
                            if shank ==0
                                CSCchannels = [shank * 8 + 1 : shank * 8 + 32];
                            else
                                CSCchannels = [32 + (shank-1) * 8 + 1 : 32 + (shank-1) * 8 + 8];
                            end
                    end
                    wv = getspikewaveforms(kwikfiles.name,baseclufile,shank,CSCchannels,'save','addName',sprintf('clu%d',shank));
                    for sua = 1 : size(wv,2)
                        for sh = 1 : 8
                            allmeanwv{sh}(c,:) = nanmean(wv{sh,sua});
                        end
                        c = c + 1;
                    end
                end
            end
            files = nptDir('spikeWaveformclu*.mat');
            for ff = 1 : length(files)
                group = str2double(files(ff).name(end-4)) + 1;
                putSpikeWVintoCluster(files(ff).name,group)
            end
        elseif length(kwikfiles) > 1  && isempty(nptDir('skip.txt'))
            for clu = 1 : length(clufiles)
                if isempty(findstr(clufiles(clu).name,'spikeWaveform'))
                    shank = 0;
                    baseclufile = clufiles(clu).name(1:end-2);
                    thekwikfile = regexprep(clufiles(clu).name,'clu.0','kwik');
                    rshank = str2double(clufiles(clu).name(end-6));
                    CSCchannels = [(rshank-1) * 8 + 1 : (rshank-1) * 8 + 8];
                    try
                        wv = getspikewaveforms(thekwikfile,baseclufile,shank,CSCchannels,'save','addName',sprintf('clu%d',rshank));
                        for sua = 1 : size(wv,2)
                            for sh = 1 : 8
                                allmeanwv{sh}(c,:) = nanmean(wv{sh,sua});
                            end
                            c = c + 1;
                        end
                    catch
                        baddata = [baddata; [pwd filesep thekwikfile baseclufile]];
                    end
                end
            end
            files = nptDir('spikeWaveformclu*.mat');
            for ff = 1 : length(files)
                group = str2double(files(ff).name(end-4));
                try
                putSpikeWVintoCluster(files(ff).name,group)
                end
            end
        end
        
        cd ..
    end
    cd ..
end


spike = zeros(8,size(allmeanwv{1},1));
for sh = 1 : 8
    maxp = max(allmeanwv{sh}, [],2);
    minp = min(allmeanwv{sh}, [],2);
    spike(sh,:) = maxp - minp;
end
[~,mshank] = max(spike);

wid = zeros(size(allmeanwv{1},1),1);
ptratio = zeros(size(allmeanwv{1},1),1);
for ii = 1 : size(allmeanwv{1},1)
    [maxv,tp] = max(allmeanwv{mshank(ii)}(ii,:),[],2);
    [minv,tt] = min(allmeanwv{mshank(ii)}(ii,:),[],2);
    wid(ii) = tp - tt;
    opt = sort([maxv minv]);
    ptratio(ii) = opt(1) /opt(2);
    
end
figure; hist(ptratio,100)
xlabel('peak trough ration')
ylabel('counts')