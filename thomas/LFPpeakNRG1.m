function LFPpeakNRG1(varargin)

% for d = 1 : length(days)
%     cd(days{d})
%     sessions = nptDir('session*');
%     
%         cd(sessions(end).name)
% try
%         LFPpeakNRG1('save','addName','NRG1')
% end
%         cd ..
%    
%     cd ..
% end
    
Args = struct('save',0,'redo',0,'addName',[],'timeRange',32,'downsampling',30);
Args.flags = {'save','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});


timew = Args.timeRange * 60;
files = nptDir('*group*powergram.mat');
if ~isempty(files)
    for ff = 1 : length(files)
        load(files(ff).name)
        
        filename = regexprep(files(ff).name,'powergram',sprintf('LFPpeaks%s',Args.addName));
        if isempty(nptDir(filename)) || Args.redo
            tb = find(t < timew);
            allspectra(1,:) = squeeze(median(median(S(tb,:,:),3),1));
            allspectra(2,:) = squeeze(median(median(S(end-length(tb) : end,:,:),3),1));
            allspectra(3,:) = squeeze(median(median(S(tb(end) :tb(end) +length(tb),:,:),3),1));
            
            down = downsample(allspectra', Args.downsampling);
            df = downsample(f',Args.downsampling);
            xmax=findpeaks(down);
            peaks = cell(3,1);
            for p = 1 : 3
                peaks{p} = df(xmax(p).loc);
            end
            
            if Args.save
                
                save(filename,'peaks','allspectra','down','df','timew')
                display([pwd filesep filename])
            end
        end
        
    end
end