function [allcounts,varargout] = ISIcalcium(onsets,varargin)

Args = struct('maxframes',501,'save',0,'redo',0);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);


filename = 'ISIcalcium.mat';

if isempty(nptDir(filename)) || Args.redo
    monsets = cell2mat(onsets(:));
    nbrspikes = length(monsets);
    allcounts = zeros(1,2*Args.maxframes + 1);
    bins = [-Args.maxframes:Args.maxframes];
    for cc = 1 : length(onsets)-1
        for sp = 1 : length(onsets{cc})
            
            rsonsets = cell2mat(onsets((cc+1):length(onsets))');
            counts = hist(onsets{cc}(sp) -  rsonsets,bins);
            allcounts = allcounts + counts;
            
        end
    end
    
    allcounts = allcounts(2: end-1);
    bins = bins(2:end-1);
    ncounts = allcounts / (nbrspikes*(nbrspikes-1)/2);
    
    if Args.save
        save(filename,'bins','allcounts','ncounts')
    else
        
    end
else
    load(filename)
end
varargout{1}= bins;
varargout{2}= ncounts;