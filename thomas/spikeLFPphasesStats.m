function spikeLFPphasesStats(varargin)

% sdir = 'F:\salazar\data\Electrophy\hippocampus';
% cd(sdir)
% days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160610' '20160914' '20160916' '20161020'  '20161020b' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807'  '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'
% obj = ProcessDays(micepsthStandard,'days',days,'NoSites');

Args = struct('save',0,'redo',0,'addName',[],'band','Theta','timeLimit',[]);
Args.flags = {'save','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

filename = sprintf('spikeLFPphases%s.mat',Args.addName);

if isempty(nptDir(filename)) || Args.redo
    udir = pwd;
    load ispikes.mat
    getDataDirs('session','relative','CDNow');
    st = strfind(udir,'group');
    gr = str2double(udir( st +5 : st + 8));
    
    thefile = nptDir(sprintf('*bpass%s_group0%d.mat',Args.band,gr));
    pl = load(thefile.name);
    cd(udir)
    sp.data.trial.cluster.spikes(sp.data.trial.cluster.spikes ==0) = [];
    spiketimes = double(sp.data.trial.cluster.spikes);
    
    if ~isempty(Args.timeLimit)
        spiketimes = spiketimes((spiketimes > Args.timeLimit(1)) & (spiketimes < Args.timeLimit(2)));
    end
    
    spiketimes = spiketimes /10;  % in 100 Hz sampling rate
    if isempty(spiketimes)
        phases = [];
        mea = nan;
        s0 = nan;
        pval = nan;
        tout = nan;
        rout = nan;
        x=nan;
        y=nan;
        nor = nan;
    else
        phases = pl.phase(ceil(spiketimes));
        [mea nor] = circ_mean(phases,[],2);
        %     [pval z] = circ_rtest(phases);
        %     [pval ~] = circ_otest(phases);
        [pval ~] = circ_vtest(phases, mea);
        [tout,rout] = rose(phases);
        
        
        [s s0] = circ_std(phases, [], [], 2);
        
        x = cos(mea) * nor;
        
        y = sin(mea) * nor;
    end
end
if Args.save
    save(filename,'phases','mea','nor','s0','tout','rout','x','y','pval')
    display(['saving ' pwd filesep filename])
end
