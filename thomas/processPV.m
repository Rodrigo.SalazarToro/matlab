function processPV

load calcium
load xcorrSpikesbinSize2.mat

thePV=find(PV == 1);
mCOI = cell(length(thePV),1);
allsigPV = nan(length(thePV),size(cumcoi,2));
nallsigPV = nan(length(thePV),size(cumcoi,2));
for ii = 1 : length(thePV)
   [rr,~] = find(thePV(ii) == pcomb); 
    rr = unique(rr);
    mCOI{ii} = cumcoi(rr,:) > squeeze(surcoi(1,rr,:));
    nallsigPV(ii,:) = sum(mCOI{ii},1)/length(rr);
    allsigPV(ii,:) = sum(mCOI{ii},1);
end
save('PVcoi.mat','mCOI','allsigPV','nallsigPV')