% sessions = nptDir('session*');
% 
% for s = 1 : length(sessions)
% cd(sessions(s).name)
% day = '2017051901';
% for sh = 0 : 8
%     if ~isempty(nptDir(sprintf('%s.kwik',day))) && ~isempty(nptDir(sprintf('%s.clu.%d',day,sh)))
%     kwik2res(sprintf('%s.kwik',day),sprintf('%s.clu.%d',day,sh),'redo','channel_groups',sh);
%     end
% end
% cd ..
% end
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622' '20160916' '20161021' '20161027' '20161103' '20161103b' '20161125' '20161201'};
% removing doubtfulhistogy
% days = {'20160412' '20160429' '20160510' '20160511' '20160518' '20160616' '20160621' '20160622'};
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};

c=1;
for d = 1 : length(days)
    cd(days{d})
    sessions=nptDir('session0*');
    for s = 1 : length(sessions)
        cd(sessions(s).name)
        list{c} = pwd;
        c=c+1;
        cd ..
    end
    cd ..
end
for d = 1 : length(list);
    cd(list{d});
    if isempty(nptDir('skip.txt'))
        try
            [onsets,time,speriod] = makeOnsetSession('getperiodAboveHz');
            pcomb = nchoosek([1:length(onsets)],2);
            comperiod = zeros(size(pcomb,1),1);
            for comb = 1 : size(pcomb,1);
                comperiod(comb) = length(intersect(speriod{pcomb(comb,1)},speriod{pcomb(comb,2)}));
            end
            save('comperiod.mat','comperiod','pcomb')
            %     [cumcoi,bins,surcoi,thresholds] = xcorrCalcOnsets(onsets,'maxlagbin',30,'binsize',20,'save','iterations',1000);
            list{d}
        end
    end
end



cd(sdir)

clear spect mice allcoi totpairs dirs allsigcoi dmice
allcp = [];
c = 1 ;
cc = 1;
minctimebin = 30;
allsigcoi = [];
dmice = [];
for d = 1 : length(days)
    cd(days{d})
    
    if ~isempty(nptDir('mouse.txt')); mouse = textread('mouse.txt','%s'); end
    sessions = nptDir('session0*');
    tcoi = nan(length(sessions) ,61);
    for ss = length(sessions) 
        cd(sessions(ss).name)
        if ~isempty(nptDir('xcorrSpikes.mat')) && isempty(nptDir('sigCoi.mat')); xcorrView('save'); end
        if ~isempty(nptDir('sigCoi.mat'))
            dd = load('sigCoi.mat');
            cp = load('comperiod.mat');
            allcp = [allcp; cp.comperiod];
            allpairssigcoi = sum(dd.sigcoi(cp.comperiod >= minctimebin,:));
            allsigcoi = cat(1,allsigcoi,dd.sigcoi);
            allcoi(c,:) = allpairssigcoi;
            dmice = cat(1,dmice,repmat(strcmp(mouse,'lgdel'),size(dd.sigcoi,1),1));
            mice(cc) = strcmp(mouse,'lgdel');
%             if sum(cp.comperiod >= minctimebin) >= 30
                tcoi(ss,:) = 100* allpairssigcoi / sum(cp.comperiod >= minctimebin);
%             end
            totpairs(c) = sum(cp.comperiod >= minctimebin);
            dirs{c} = pwd;
            c = c + 1;
        end
        cd ..
    end
    prctcoi(cc,:) = nanmean(tcoi);
    cc = cc + 1;
    cd(sdir)
end
figure

sind = mice == 0;
plot(dd.bins,prctcoi(sind,:),'k')

hold on
sind = mice == 1;
plot(dd.bins,prctcoi(sind,:),'r')

xlabel('time lag (ms)')
ylabel('% pairs')

% legend('WT','WT','lgDel','lgDel','lgDel','lgDel')
title('Incidence of cross-correlation between single units')

figure

plot(dd.bins,100*sum(allsigcoi(dmice ==0,:),1)/sum(dmice ==0),'k')

hold on

plot(dd.bins,100*sum(allsigcoi(dmice ==1,:),1)/sum(dmice ==1),'r')


%%Auto-correlation analyses
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');
mice = {'wt' 'lgdel'};
ac = cell(2,1);
SFC = cell(2,1);
SFCsur = cell(2,1);
PHI = cell(2,1);
fftpwd = cell(2,1);
mtspwd = cell(2,1);
params = struct('tapers',[5 9],'Fs',1000,'fpass',[.5 20]);
mintimes = 2*60*1000;
maxtimes = 88*60*1000;
for m = 1 : 2
    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','hipp','mintime',mintimes,'maxtime',maxtimes,'minNumSpike',500,'NumSpikePeriod',[1 3]);
    for ii = 1: r
        cd(obj.data.setNames{ind(ii)})
        if ~isempty(nptDir('autocorr.mat'))
            auto = load('autocorr.mat');
            ac{m}(ii,:) = (auto.allcumspikes(1:end-1) - mean(auto.allcumspikes(1:end-1))) / std(auto.allcumspikes(1:end-1));
            ac{m}(ii,:) = lfp_detrend(vecc(ac{m}(ii,:)));
            data = load('mtsspikefield.mat');
            
            data.C(data.C == 0) = nan;
            try
            SFC{m}(ii,:) = nanmedian(data.C);
            SFCsur{m}(ii,:) = squeeze(nanmedian(data.Cerr(1,:,:),2));
            PHI{m}(ii,:) = nanmedian(data.phi);
            end
        end
    end
end
% figure
% plot(auto.range,ac{1},'k')
%
% hold on
%
% plot(auto.range,ac{2},'r')


tiles = [5 10 25 50 75 90 95];
leg = cell(length(tiles),1);
figure
for m =1 : 2
    subplot(2,1,m); plot(data.f,prctile(SFC{m},tiles))
    xlim([0 40])
    ylim([0.2 .5])
    title(mice{m})
    
end
xlabel('Frequency (Hz)')
ylabel('spike-field coherence')
for pr = 1 : length(tiles);leg{pr} = sprintf('prctile %dth',tiles(pr)); end
legend(leg(:))

figure
for m =1 : 2
    subplot(2,1,m);
    plot(data.f,PHI{m})
    title(mice{m})
    
end

lb = {'r' 'k'};
figure
for m =1 : 2
    
    plot(data.f,100*sum(SFCsur{m} > 0)/size(SFCsur{m},1),lb{m})
    hold on
    legend(mice(:))
end
xlabel('Frequency (Hz)')
ylabel('% pairs with sig. SFC')

%% autocorr from power spike multitaper

% days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622'};
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');

for ii = 1 : obj.data.numSets
    cd(obj.data.setNames{ii})
    %    [autoc,bins] = autocorr(timeserie,1000,5,'infolder','save');
    [autoc,bins] = autocorr(1,1000,5,'infolder','save','multitaper');
    
end


%% NRG1 SFC

cd F:\salazar\data\Electrophy\hippocampus
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161103b' '20161125' '20161201'};
obj = ProcessDays(micepsthStandard,'days',days,'NoSites');

mice = {'wt' 'lgdel'};
SFCb = cell(2,1);
PHIb = cell(2,1);
SFCa = cell(2,1);
PHIa = cell(2,1);
for m = 1 : 2
    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true);
    for ii = 1: r
        cd(obj.data.setNames{ind(ii)})
        if ~isempty(nptDir('mtsspikefield.mat'))
        data = load('mtsspikefield.mat');
        data.C(data.C == 0) = nan;
        if size(data.C,1) > 0
            SFCb{m}(ii,:,:) = data.C(1:17990,:);
            PHIb{m}(ii,:,:) = data.phi(1:17990,:);
            
            SFCa{m}(ii,:,:) = data.C(end-17990:end,:);
            PHIa{m}(ii,:,:) = data.phi(end-17990:end,:);
        end
        end
    end
end

mSFCb = cell(2,1);
mSFCa = cell(2,1);

for m = 2 : 2
    for ii = 1: 174
        mSFCb{m}(ii,:) = nanmedian( SFCb{m}(ii,:,:),2);
        mSFCa{m}(ii,:) = nanmedian( SFCa{m}(ii,:,:),2);
    end
end

figure
for m = 1 : 2
    subplot(2,2,m)
    plot(f,prctile(mSFCb{m},[5 10 25 50 75 90 95],1))
    
    title(mice{m})
    ylim([0.2 .45]); xlim([0 40])
    
    subplot(2,2,2 + m)
    plot(f,prctile(mSFCa{m},[5 10 25 50 75 90 95],1))
    ylim([0.2 .45]); xlim([0 40])
    
    xlabel('Frequency (Hz)')
    ylabel('SFC')
end
subplot(2,2,1); ylabel('30 min before NRG1')
subplot(2,2,3); ylabel('30 min after NRG1')

%% xcorr for drug in 30 min epochs

clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};



c = 1 ;

files = {'xcorrSpikes30min2.mat' 'xcorrSpikes60min2.mat' 'xcorrSpikes90min2.mat'};
for d = 1 : length(days)
    cd(days{d})
    
    if ~isempty(nptDir('mouse.txt')); mouse = textread('mouse.txt','%s'); end
    
    lgDel = strcmp(mouse{1},'lgdel');
    sessions = nptDir('session0*');
    tcoi = nan(length(sessions) ,61);
    for ss = length(sessions)
        cd(sessions(ss).name)
        if ~isempty(nptDir('xcorrSpikes30min2.mat')) && ~isempty(nptDir('xcorrSpikes60min2.mat')) && ~isempty(nptDir('xcorrSpikes90min2.mat'))&& ~isempty(nptDir('drug.txt')) && ~isempty(nptDir('xcorrSpikes60min.mat')) && ~isempty(nptDir('xcorrSpikes90min.mat')); 
        for ff = 1 : 3
            load(files{ff})
            ncoi(ff,c,:) = sum(cumcoi > squeeze(max(surcoi)));
            totpairs(ff,c) = size(cumcoi,1);
            mice(c) = lgDel;
%             dmice = cat(1,dmice,repmat(strcmp(mouse,'lgdel'),size(dd.sigcoi,1),1));
           
            %           dirs{c} = pwd;
        end
        c = c + 1;
        
        end
        cd ..
    end
    cd ..
end

% figure; 
% 
% for sb = 1 : 3
%     subplot(3,1,sb)
%     
%     plot(bins, squeeze(pcrtcoi(sb,mice == 1,:)),'r')
%     hold on
%      plot(bins, squeeze(pcrtcoi(sb,mice == 0,:)),'k')
% end   

figure; 

for sb = 1 : 3
    subplot(3,1,sb)
    
    plot(bins, 100*squeeze(ncoi(sb,mice == 1,:))./repmat(unique(totpairs(sb,mice==1))',1,61),'r')
    hold on
     plot(bins, 100*squeeze(ncoi(sb,mice == 0,:))./repmat(unique(totpairs(sb,mice==0))',1,61),'k')
   
     title(files{sb})
     xlabel('lag (ms)')
     ylabel('% pairs')
end   
legend('lgdel','wt')