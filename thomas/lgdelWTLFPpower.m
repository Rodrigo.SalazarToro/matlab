clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160413' '20160428' '20160429' '20160510' '20160511' '20160518' '20160616' '20160617' '20160621' '20160622' '20160916' '20161021' '20161027' '20161103' '20161103b' '20161125' '20161201' '20161201b' '20170807' '20170814'};% old selection
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20161021' '20161027' '20161103' '20161125' '20161201' '20160610' '20161020b' '20170111' '20170222' '20170223'  '20170807' '20170814' '20170815' '20170821' '20170822' '20170823' '20170829' '20170829b'}; % all mice including the ldgel outliers ('20160621') plus old mice  '20170222' '20170223'; '20170808' to exclude because mouse broke headpost


clear spect mice cumg nmouse
c = 1 ;
for d = 1 : length(days)
    cd(days{d})
    
    if ~isempty(nptDir('mouse.txt')); mouse = textread('mouse.txt','%s'); else; clear mouse; end
    sessions = nptDir('session0*');
     switch mouse{1}
            case {'lgdel'}
                lgDel = 1;
            case {'wt'}
                lgDel = 0;
            case {'lgdelPV-cre'}
                lgDel = 2;
        end
       
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
%         if ~exist('mouse','var'); mouse = textread('mouse.txt','%s'); end
       
        makepwgramPolytrode('name',[days{d} '0' num2str(ss)])
        [dirs,grs] = findUnit('unit','s');
        thegrs = unique(grs);
        pwfiles = nptDir('*powergram.mat');
        for gg = 1 : length(thegrs)
            if (length(pwfiles) == 9 && thegrs(gg) > 1) || length(pwfiles) == 8
                pw = load(pwfiles(thegrs(gg)).name);
                si = find(pw.t < 30 * 60); %first 30 min
                spect(c,:) = squeeze(median(median(pw.S(si,:,:),3),1));
                mice(c) = lgDel;
                cumg(c) = thegrs(gg);
                nmouse(c) = d;
                c = c + 1;
            end
        end
        cd ..
    end
     clear mouse
    cd(sdir)
end

save('lgdelWTLFPpower.mat')
figure
subplot(3,1,1)
plot(pw.f,spect(mice==0,:))
title('Wild type')
ylabel('Power (arb. unit)')
subplot(3,1,2)

plot(pw.f,spect(mice==1,:))
ylabel('Power (arb. unit)')
title('LgDel')

subplot(3,1,3)
plot(pw.f,prctile(spect(mice==0,:),[25 50 75],1),'k')
hold on
plot(pw.f,prctile(spect(mice==1,:),[25 50 75],1),'r')
for ff = 1 : length(pw.f); p(ff) = ranksum(spect(mice==0,ff),spect(mice==1,ff)); end

plot(pw.f,p >= .01,'r*')
ylabel('Power (arb. unit)')
% legend('WT mean','WT mean + SEM ','WT mean - SEM','LgDel mean','LgDel mean + SEM','LgDel mean - SEM')
legend('WT 25th prctile','WT 50th prctile','WT 75th prctile','LgDel 25th prctile','LgDel 50th prctile','LgDel 75th prctile','significance in a Wilcoxon rank sum test at p<.01' )

xlabel('Frequency (Hz)')
for sb = 1 : 3; subplot(3,1,sb); ylim([0 1.5*10^-3]); xlim([0 40]);end;
[c,ia,ic] = unique(nmouse);
title(sprintf(' %d mouse WT and %d mice LgDel',sum(~mice(ia)),sum(mice(ia))))


clear mspect micea
c=1;
for nm = unique(nmouse)
    mspect(c,:) = mean(spect(nmouse == nm,:),1);
    micea(c) = unique(mice(nmouse ==nm));
    c=c+1;
end
lastgood = 115;
figure

plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 0,:)),'k')
hold on
plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 0,:)) + pw.f.*std(spect(mice(1:lastgood) == 0,:))/sqrt(sum(mice(1:lastgood) ==0)),'k')
plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 0,:)) - pw.f.*std(spect(mice(1:lastgood) == 0,:))/sqrt(sum(mice(1:lastgood) ==0)),'k')

plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 1,:)),'r')
hold on
plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 1,:)) + pw.f.*std(spect(mice(1:lastgood) == 0,:))/sqrt(sum(mice(1:lastgood) ==1)),'r')
plot(pw.f,pw.f.*mean(spect(mice(1:lastgood) == 1,:)) - pw.f.*std(spect(mice(1:lastgood) == 0,:))/sqrt(sum(mice(1:lastgood) ==1)),'r')
title(sprintf('lgdel, n=%d; WT, n=%d',sum(mice(1:lastgood) ==1),sum(mice(1:lastgood) ==0)))

xlabel('Frequency (Hz)')
ylabel('Power 1/f')
legend('WT mean','WT std','WT std','lgdel mean','lgdel std','lgdel std')
figure
plot(pw.f,mspect(micea ==1,:),'r')

hold on

plot(pw.f,mspect(micea == 0,:),'k')

figure
plot(pw.f,mean(mspect(micea == 1,:)),'r')
hold on
plot(pw.f,mean(mspect(micea == 1,:)) + std(mspect(micea == 1,:)) / sqrt(length(micea)),'r')
plot(pw.f,mean(mspect(micea == 1,:)) - std(mspect(micea == 1,:)) / sqrt(length(micea)),'r')

plot(pw.f,mean(mspect(micea == 0,:)),'k')
hold on
plot(pw.f,mean(mspect(micea == 0,:)) + std(mspect(micea == 0,:)) / sqrt(length(micea)),'k')
plot(pw.f,mean(mspect(micea == 0,:)) - std(mspect(micea == 0,:)) / sqrt(length(micea)),'k')

xlabel('Frequency (Hz)'); ylabel('Power (arb. unit)')
legend(sprintf('mean %d LgDel',sum(micea == 1)), 'mean +/- SEM', 'mean +/- SEM', sprintf('mean %d WT',sum(micea == 0)), 'mean +/- SEM', 'mean +/- SEM')

figure
plot(pw.f,mspect(micea == 1,:),'r')
hold on
plot(pw.f,mspect(micea == 0,:),'k')


%%
figure

delta = find(pw.f >= 1 & pw.f <= 4);
theta = find(pw.f >= 5 & pw.f <= 8);
alpha = find(pw.f >= 9 & pw.f <= 15);
beta = find(pw.f >= 16 & pw.f <= 30);
gamma = find(pw.f >= 31 & pw.f <= 80);

bands = {'delta' 'theta' 'alpha' 'beta' 'gamma'};
mmice = mice;
mmice(mice == 1 | mice == 2) = 1;

for bb = 1 : 5
    subplot(1,5,bb)
    boxplot(mean(spect(:,eval(bands{bb})),2),mmice,'labels',{'WT' 'allctr'})
    p = ranksum(mean(spect(mmice == 0,eval(bands{bb})),2),mean(spect(mmice==1,eval(bands{bb})),2));
    %[~,tp] = ttest2(mean(spect(mmice == 0,eval(bands{bb})),2),mean(spect(mmice==1,eval(bands{bb})),2));
    title([bands{bb} ' WXp=' num2str(p) ' ttest=' num2str(tp)]); ylabel('Power (arb. unit)')
end
bb= 2; forxl = mean(spect(:,eval(bands{bb})),2);mmice = mmice';