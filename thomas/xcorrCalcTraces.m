function [c,lags,csur,upthreshold,lowthreshold,autoc,autosur,upthresholdauto,lowthresholdauto,varargout] = xcorrCalcTraces(traces,varargin)
% input:
% traces for raw calcium traces in cells x frame
% the script does the linear detrend, z-score and compute all possible
% xcorr. Finally, the surrogate for significance
%
% output:
% c : xcorr matrix
% lags: vector for frame lags
% csur : surrogate population
% threshold : estimate of significance threshold

Args = struct('iterations',500,'maxframe',300,'pvalue',[10^-3 10^-4 10^-5 10^-6 10^-7 10^-8 10^-9],'fromOnsets',0);
Args.flags = {'fromOnsets'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

% z-score
ntraces = (traces - repmat(mean(traces,2),1,size(traces,2))) ./ repmat(std(traces,[],2),1,size(traces,2));

% linear detrend
dtraces = detrend(ntraces')';

ncell = size(traces,1);

[c,lags]= xcorr(dtraces',Args.maxframe,'coeff');
colauto = [1 : size(traces,1) + 1 : size(c,2)];
count=1;
cs = 1;
selcomb = [];
selcol = nchoosek(ncell,2);
for first = 1: ncell
    for sec = 1 : ncell
        if first < sec
            selcol(cs) =count;
            selcomb = cat(1,selcomb,[first sec]);
            cs = cs + 1;
        end
        count = count + 1;
    end
end
autoc = c(:,colauto);

varargout{1} = selcomb;
c = c(:,selcol);
% organization of c (s1s1 s1s2 s1s3 ...s2s1 s2s2 s2s3 ...
c = single(c);
%% surrogate xcorr

csur = single(ones(size(c,1),size(c,2),Args.iterations));
rtraces = dtraces;
if Args.iterations * 2 < size(traces,2)
    for ii = 1 : Args.iterations
        rshift = floor(size(traces,2)/2 + randi(floor(size(traces,2)/2),ncell,1));
        for n = 1 : ncell
            rtraces(n,:) = dtraces(n,cat(2,rshift(n):size(dtraces,2),1: rshift(n)-1));
        end
        [ct,~]= xcorr(rtraces',Args.maxframe,'coeff');
        ct = ct(:,selcol);
        csur(:,:,ii) = single(ct);
    end
else
    error('recordings too short to compute stats!!!!!!!!!')
end

upthreshold = ones(size(csur,1),size(csur,2),length(Args.pvalue));
lowthreshold = ones(size(csur,1),size(csur,2),length(Args.pvalue));
for frbin = 1 : size(csur,1)
    for cb = 1 : size(csur,2)
        if unique(csur(frbin,cb,:)) ~= 1
            [~,~,upthreshold(frbin,cb,:),~] = normfitSur(csur(frbin,cb,:),1 - Args.pvalue,'twoSided');
            [~,~,lowthreshold(frbin,cb,:),~] = normfitSur(csur(frbin,cb,:),Args.pvalue,'twoSided');
        end
    end
end
%% surrogate autocor
autosur = single(ones(size(autoc,1),size(autoc,2),Args.iterations));
rtraces = dtraces;
if Args.iterations * 2 < size(traces,2)
    for ii = 1 : Args.iterations
        rshift = floor(size(traces,2)/2 + randi(floor(size(traces,2)/2),ncell,1));
        for n = 1 : ncell
            rtraces(n,:) = dtraces(n,cat(2,rshift(n):size(dtraces,2),1: rshift(n)-1));
            [autosur(:,n,ii),~]= xcorr(dtraces(n,:),rtraces(n,:),Args.maxframe,'coeff');
        end
        
    end
else
    error('recordings too short to compute stats!!!!!!!!!')
end

upthresholdauto = ones(size(autosur,1),size(autosur,2),length(Args.pvalue));
lowthresholdauto = ones(size(autosur,1),size(autosur,2),length(Args.pvalue));
for frbin = 1 : size(autosur,1)
    for cb = 1 : size(autosur,2)
        if unique(autosur(frbin,cb,:)) ~= 1
            [~,~,upthresholdauto(frbin,cb,:),~] = normfitSur(autosur(frbin,cb,:),1 - Args.pvalue,'twoSided');
            [~,~,lowthresholdauto(frbin,cb,:),~] = normfitSur(autosur(frbin,cb,:),Args.pvalue,'twoSided');
        end
    end
end
