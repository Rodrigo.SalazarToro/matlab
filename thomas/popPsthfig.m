
cd G:\thomas\LgDel\Coactivations\ConditionWT\ConditionCCH\150616S1

linewidth = .1;
a = load('calcium');
onsets = a.handles.app.experiment.detections(1,2).onsets;
[sumpop,raster,sumpopsur,thres,sigframe,~,surrogate] = popPsth(onsets,'redo');
sigpop = sumpop > max(sumpopsur);
 maxframe = max(cell2mat(onsets')) + 1;
figure;
% subplot(3,1,1)
for c = 1 : 259;
    spikes = find(raster(c,:) == 1);
    sigs = find(sigpop(spikes));
    line([spikes(sigs);spikes(sigs)],[c*ones(1,length(sigs));c*ones(1,length(sigs))+1],'Color','r','LineWidth',linewidth);
    hold on;
    nsigs = find(~sigpop(spikes));
     line([spikes(nsigs);spikes(nsigs)],[c*ones(1,length(nsigs));c*ones(1,length(nsigs))+1],'Color','k','LineWidth',linewidth);
   
end

ylim([0 259])
xlim([0 3321])
ylabel('Cell #')



for n = 1 : 259
    Sraster(n,:) = hist(surrogate{n},[0:maxframe - 1]);
    
end

figure
% subplot(3,1,2)
for c = 1 : 259;
    spikes = find(Sraster(c,:) == 1);
    sigs = find(sigpop(spikes));
    line([spikes(sigs);spikes(sigs)],[c*ones(1,length(sigs));c*ones(1,length(sigs))+1],'Color','r','LineWidth',linewidth);
    hold on;
    nsigs = find(~sigpop(spikes));
     line([spikes(nsigs);spikes(nsigs)],[c*ones(1,length(nsigs));c*ones(1,length(nsigs))+1],'Color','k','LineWidth',linewidth);
   
end
ylim([0 259])
xlim([0 3321])
ylabel('Cell #')
figure
% subplot(3,1,3)


plot(sumpop)
hold on
plot(max(sumpopsur),'r')
xlim([0 3321])
xlabel('Frame #')
ylabel('# cells')
linkedzoom onx