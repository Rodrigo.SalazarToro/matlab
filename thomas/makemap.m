function makemap(handles, varargin)


ncell = length(handles.app.experiment.centroids);

% cellImage = zeros(size(handles.app.experiment.Image.image));
f1=figure;
imagesc(handles.app.experiment.Image.image)
colormap('cool')
hold on

for nc = 1 : ncell
    y = floor(handles.app.experiment.centroids{nc}(2));
    x =  floor(handles.app.experiment.centroids{nc}(1));
%     cellImage(x,y) = cellImage(x,y) + 1;
    text(x,y,num2str(nc))
end



title(pwd)

saveas(f1,'2Dmap','fig')
display(['saving map ' pwd])
close(f1)