clear all
sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};

obj = ProcessDays(micepsthStandard,'days',days,'NoSites');

%% processing
parfor ii = 1 : obj.data.numSets
    cd(obj.data.setNames{ii})
    t = load('ispikes.mat');
    timeserie = t.sp.data.trial.cluster.spikes;
    
    [autoc,bins] = autocorr(timeserie,1000,5,'infolder','save','timelimit',[0 30*60*1000],'addName','30min');
    [autoc,bins] = autocorr(timeserie,1000,5,'infolder','save','timelimit',[30*60*1000 60*60*1000],'addName','60min');
    [autoc,bins] = autocorr(timeserie,1000,5,'infolder','save','timelimit',[60*60*1000 90*60*1000],'addName','90min');
    
end











%% analysis
allac = cell(2,1);
mice = {'wt' 'lgdel'};
timesP = {'30min' '60min' '90min'};
for m = 1 : 2
    [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','hipp','mintime',5*60*1000,'maxtime',85*60*1000,'minNumSpike',500,'NumSpikePeriod',[1 3],'spikePtoTratio',{-1;'>='}); % select for hipp cells
    %      [r,ind] = get(obj,'Number','mouse',mice{m},'drug',true,'unit','s','histo','mPFC','minNumSpike',500,'NumSpikePeriod',[1 2 3],'thetapeak',[1 3]); % select for mPFC cells
    c = 1;
    for ii = 1: r
        cd(obj.data.setNames{ind(ii)})
        for ti = 1 : 3;
            data = load(sprintf('autocorr%s.mat',timesP{ti}));
            allac{m,ti}(ii,:) = data.allcumspikes/sum(data.allcumspikes);
        end
    end
    
end
figure
lb = {'k' 'r'};
for m = 1 : 2
    for ti = 1 : 3
        subplot(3,1,ti)
        plot(median(allac{m,ti})',lb{m})
        hold on
    end
end
        