cd G:\thomas\LgDel\Coactivations\ConditionWT\ConditionCCH\150224S2


clear all

data = load('xcorrSpikesbinSize2.mat');
cumcoi = data.cumcoi(find(sum(data.cumcoi,2) ~= 0),:);
surcoi = squeeze(data.surcoi(1,find(sum(data.cumcoi,2) ~= 0),:));
for spair = 50: size(cumcoi,1)
    
%     if (sum(cumcoi(spair,:)) ~= 0) && (max(cumcoi(spair,:)) > median(surcoi(spair,:)))
        scells = data.pcomb(spair,:);
        figure;
        plot(data.bins,cumcoi(spair,:));
        hold on;
        plot(data.bins,squeeze(surcoi(spair,:)),'--');
        title(sprintf('%d %d %d',spair,scells(1), scells(2)))
        
        load calcium
        
        onsets = handles.app.experiment.detections(1,2).onsets';
        
        traces = handles.app.experiment.traces - handles.app.experiment.haloTraces;
        
        
        
        figure;
        
        for ce = 1 : 2
            htraces = detrend(traces(scells(ce),:));
            ftraces = htraces;
            %         [b,a] = butter(3,.01,'high');
            %         ftraces = filtfilt(b,a,htraces);
            subplot(2,1,ce)
            plot(30+ftraces)
            xlim([0 length(ftraces)])
            hold on
            line([onsets{scells(ce)}'; onsets{scells(ce)}'] ,[-10*ones(1,length(onsets{scells(ce)})); -20*ones(1,length(onsets{scells(ce)}))],'Color','k');
            
            xlim([0 length(traces)])
        end
        linkedzoom onx
        
       figure;
        subplot(2,1,1)
        imagesc(-cumcoi(51:100,:));
        colormap('hot')
        set(gca,'XTick',[1:length(data.bins)])
        set(gca,'XTickLabel',data.bins)
        xlabel('Lag (frame)')
        colorbar
        set(gca,'TickDir','out')
        
        subplot(2,1,2)
        imagesc(~(cumcoi(51:100,:) > squeeze(surcoi(51:100,:))));
        colormap('hot')
        set(gca,'XTick',[1:length(data.bins)])
        set(gca,'XTickLabel',data.bins)
        xlabel('Lag (frame)')
        colorbar
        set(gca,'TickDir','out')
        
        pause
        close all
%     end
end