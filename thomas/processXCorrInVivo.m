sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
% days = {'20160412' '20160428' '20160429' '20160510' '20160511' '20160518' '20160621' '20160914' '20160916' '20161021' '20161027' '20161103' '20161125' '20161201'};
days = {'20160429' '20160511'  '20160610' '20161020' '20161020b' '20170111' '20170222' '20170223'};
c=1;
for d = 1 : length(days)
    cd(days{d})
    sessions=nptDir('session0*');
    for s = length(sessions)
        cd(sessions(s).name)
        list{c} = pwd;
        c=c+1;
        cd ..
    end
    cd ..
end

periods = [0 30*60*1000; 30*60*1000 60*60*1000; 60*60*1000 90*60*1000];
Pname= {'30min2' '60min2' '90min2'};
for d = 1 : length(list);
    cd(list{d});
    if isempty(nptDir('skip.txt'))
        parfor pp = 1 : 3
            [onsets] = makeOnsetSession('timePeriod',periods(pp,:));
            pcomb = nchoosek([1:length(onsets)],2);
            
            [cumcoi,bins,surcoi,thresholds] = xcorrCalcOnsets(onsets,'maxlagbin',30,'binsize',20,'save','iterations',500,'addName',Pname{pp},'jitterSpike');
%             list{d}
        end
    end
end
%% special processing for heavy data

sdir = 'F:\salazar\data\Electrophy\hippocampus';
cd(sdir)
days = { '20170829b'};
periods = [0 30*60*1000; 30*60*1000 60*60*1000; 60*60*1000 90*60*1000];
Pname= {'30min2' '60min2' '90min2'};
for d = 1 : length(days)
    cd(days{d});
    sessions = nptDir('session0*');
    for ss = 1 : length(sessions)
        cd(sessions(ss).name)
        if isempty(nptDir('skip.txt'))
            for pp = 1 : 3
                [onsets] = makeOnsetSession('timePeriod',periods(pp,:));
                pcomb = nchoosek([1:length(onsets)],2);
                
                [cumcoi,bins,surcoi,thresholds] = xcorrCalcOnsets(onsets,'maxlagbin',30,'binsize',20,'save','iterations',500,'addName',Pname{pp},'jitterSpike','parSur');
                days{d}
            end
        end
        cd(days{d});
    end
    cd(sdir)
end
