function [mintime,maxtime,pvalue] = stationarity(varargin)
% to be run in the cluster folder
% for now only for continuous recordings
Args = struct('save',0,'redo',0,'psthbin',10000,'window',60000*30,'file','stationary.mat');
Args.flags = {'save','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

if isempty(nptDir(Args.file)) || Args.redo
    load ispikes
    timeserie = double(sp.data.trial.cluster.spikes);
    mintime = timeserie(1);
    maxtime =  timeserie(end);
    
    [nspikes,time] = hist(timeserie,[0 : Args.psthbin : maxtime] + Args.psthbin);
    
    counts = cell(ceil(timeserie(end) / Args.window) - 1);
    for wi = 0 : ceil(timeserie(end) / Args.window) - 1
        ind = find(time > (wi*Args.window) & time < ((wi+1)*Args.window));
        counts{wi+1} = nspikes(ind);
        
    end
    
    pvalue = ones(length(counts) - 1,1);
    for wi = 1 : length(counts) - 1
        pvalue(wi) = ranksum(counts{wi},counts{wi+1});
    end
    
else
    
    load(Args.file)
    
end

if Args.save
   save(Args.file,'mintime','maxtime','pvalue') 
   display(['saving ' pwd ' ' Args.file])
end