function splitCellType(varargin)

Args = struct('redo',0,'bins',[],'addName',[],'lname1','sigCoi.mat','lname2','xcorrSpikes.mat');
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

a = load('calcium');
onsets = a.handles.app.experiment.detections(1,2).onsets';
thomasCells = length(a.PV);
ncell = length(onsets);
sname = ['cellTypexcorr' Args.addName];
if thomasCells == ncell
    if Args.redo || isempty(nptDir([sname '.fig']))
        if isfield(a,'PV')
            PVcells = find(a.PV == 1);
            pcomb = nchoosek([1:ncell],2);
            PVtrue = ismember(pcomb,PVcells);
            
            nonPVpairs = find(sum(PVtrue,2) == 0);
            [nonPVpairs,dnonPVpairs] = sortCellpair(nonPVpairs,a.handles,pcomb);
            
            PVpairs = find(sum(PVtrue,2) == 2);
            [PVpairs,dPVpairs] = sortCellpair(PVpairs,a.handles,pcomb);
            
            PV_nonPVpairs = find(sum(PVtrue,2) == 1);
            [PV_nonPVpairs,dPV_nonPVpairs] = sortCellpair(PV_nonPVpairs,a.handles,pcomb);
            
            revPV_nonPVpairs = find(sum(PVtrue,2) == 1 & PVtrue(:,2) == 1);
            
            if  ~isempty(nptDir(Args.lname1)) &&  ~isempty(nptDir(Args.lname2))
                load(Args.lname1)
                load(Args.lname2)
                if ~isempty(Args.bins); bins = Args.bins; end
                h=figure;
                subplot(3,2,1)
                imagesc(sigcoi(nonPVpairs,:))
                set(gca,'XTick',[1:length(bins)])
                set(gca,'XTickLabel',bins)
                
                title('non PV pairs')
                subplot(3,2,2)
                allpairssigcoi_nonPVpairs = sum(sigcoi(nonPVpairs,:),1);
                bar(bins,allpairssigcoi_nonPVpairs)
                
                subplot(3,2,3)
                imagesc(sigcoi(PVpairs,:))
                set(gca,'XTick',[1:length(bins)])
                set(gca,'XTickLabel',bins)
                title('PV pairs')
                subplot(3,2,4)
                allpairssigcoi_PVpairs = sum(sigcoi(PVpairs,:),1);
                bar(bins,allpairssigcoi_PVpairs)
                
                sigcoi(revPV_nonPVpairs,:) = sigcoi(revPV_nonPVpairs,fliplr([ 1 : size(sigcoi,2)]));
                subplot(3,2,5)
                imagesc(sigcoi(PV_nonPVpairs,:))
                set(gca,'XTick',[1:length(bins)])
                set(gca,'XTickLabel',bins)
                title('PV_nonPVpairs')
                subplot(3,2,6)
                allpairssigcoi_PV_nonPVpairs = sum(sigcoi(PV_nonPVpairs,:),1);
                bar(bins,allpairssigcoi_PV_nonPVpairs)
                savefig([sname '.fig'])
                save([sname '.mat'],'nonPVpairs','dnonPVpairs','PVpairs','dPVpairs','PV_nonPVpairs','dPV_nonPVpairs','allpairssigcoi_nonPVpairs','allpairssigcoi_PVpairs','allpairssigcoi_PV_nonPVpairs')
                close(h)
                display(['saving' pwd filesep sname])
            else
                display([ pwd 'Files missing'])
            end
        end
    else
        openfig(sname)
    end
else
    display([pwd 'Error Thomas; mismatch # of cells'])
end

function [newseq,dseq] = sortCellpair(seq,handles,pcomb)

dseq = nan(length(seq),1);
for np = 1 : length(seq)
    x1 = floor(handles.app.experiment.centroids{pcomb(seq(np),1)}(2));
    y1 = floor(handles.app.experiment.centroids{pcomb(seq(np),1)}(1));
    
    x2 = floor(handles.app.experiment.centroids{pcomb(seq(np),2)}(2));
    y2 = floor(handles.app.experiment.centroids{pcomb(seq(np),2)}(1));
    
    dseq(np) = sqrt(((x1 - x2)^2) + ((y1 - y2)^2));
end
[dseq,I] = sort(dseq);
newseq = seq(I);
