function [stPopRate,bins,cbins,stPopRateSur] = spikeTrigPopRate(varargin)



Args = struct('save',0,'redo',0,'iterations',1000,'maxlagbin',10,'binsize',2,'seedNeurons',[],'addName',[],'cellType',[],'cellName',[],'thomas',0,'onsets',[]);
Args.flags = {'save','redo','nofitting','thomas'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

filename = ['spikeTrigPopRate' Args.addName '.mat'];
figname = ['spikeTrigPopRate' Args.addName '.fig'];
if isempty(nptDir(filename)) || Args.redo
    if isempty(Args.onsets)
    load('calcium');
    Args.cellType = PV;
    onsets = handles.app.experiment.detections(1,2).onsets';
    else
       onsets = Args.onsets; 
    end
    maxframe = ceil(Args.binsize/2) + Args.maxlagbin*Args.binsize;
    bins = [-maxframe:Args.binsize:maxframe];
    ncell = length(onsets);
    
    %% reshape the data to use the chronux psth.m this step is tricky because of teh variability of activity between condition. Hard to decide for a good STD for the kernel
    
    %     for u = 1 : ncell
    %         unit(u).times = onsets{u};
    %
    %     end
    %     [R,t] = psth(unit,10);
    %%
    
    
    stPopRate = zeros(ncell,length(bins));
    for unit = 1 : ncell
        otherunits = setdiff([1:ncell], unit);
        for spike = 1 : length(onsets{unit})
            spiketimes = cell2mat(vecc(onsets(otherunits))) - onsets{unit}(spike);
            stPopRate(unit,:) = stPopRate(unit,:) + vecr(histc(spiketimes,bins));
        end
    end
    %% surrogate
    
    maxspike = max(cell2mat(vecc(onsets)));
    stPopRateSur = zeros(Args.iterations,ncell,length(bins));
    for unit = 1 : ncell
        otherunits = setdiff([1:ncell], unit);
        for ii = 1 : Args.iterations
            shift = randi(maxspike - 2,1);
            shifttimes = mod(onsets{unit} + shift,maxspike);
            for spike = 1 : length(shifttimes)
                spiketimes = cell2mat(vecc(onsets(otherunits))) - shifttimes(spike);
                stPopRateSur(ii,unit,:) = squeeze(stPopRateSur(ii,unit,:)) + vecc(histc(spiketimes,bins));
            end
        end
        display(sprintf('unit %d over %d', unit,ncell))
    end
    if isempty(Args.cellType)
        ntype = 1;
        Args.cellType = ones(ncell,1);
    else
        ntype = length(unique(Args.cellType));
       
    end
     thetypes = unique(Args.cellType);
    nstPopRateSur = squeeze(median(stPopRateSur,1));
    %% last bin from histc is the left over, thus needs to be eliminated
    stPopRate = stPopRate(:,1:end-1);
    stPopRateSur = stPopRateSur(:,:,1:end-1);
  
    cbins = [-maxframe + Args.binsize/2 : Args.binsize : maxframe];
    %% normalization
     nstPopRateSur = squeeze(median(stPopRateSur,1));
     ostPopRate = stPopRate;
     stPopRate = stPopRate - nstPopRateSur;
     stPopRate = stPopRate ./ repmat(cellfun(@length,onsets),[1 length(cbins)]);
     %% 
    f1 = figure;
    for nt = 1 : ntype
        subplot(ntype,1,nt)
        plot(cbins,stPopRate(Args.cellType == thetypes(nt),:))
       if ~isempty(Args.cellName); title(Args.cellName{nt}); end
        
        xlabel('time lags (frame)')
        ylabel('# of events')
    end
%     if Args.thomas
%         nt=2;
%         PVstPR = stPopRate(Args.cellType == thetypes(nt),:) - nstPopRateSur(Args.cellType == thetypes(nt),:);
%         nt=1;
%         nonPVstPR = stPopRate(Args.cellType == thetypes(nt),:) - nstPopRateSur(Args.cellType == thetypes(nt),:);
%         
%         if Args.save
%              save(['justForThomas' filename],'PVstPR','bins','nonPVstPR')
%              display(['saving ' pwd])
%         end
%         
%     end
    if Args.save
        save(filename,'stPopRate','ostPopRate','bins','cbins','stPopRateSur');
        saveas(f1,figname);
        display(['saving ' pwd]);
    end
    close(f1)
end