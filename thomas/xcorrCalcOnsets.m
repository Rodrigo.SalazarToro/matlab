function [cumcoi,bins,surcoi,thresholds,varargout] = xcorrCalcOnsets(onsets,varargin)
% input:
% onsets for raw calcium traces in a cell with {neurons,trials} (time of
% onsets)
% the script does the  z-score and compute all possible
% xcorr. Finally, the surrogate for significance
% La reference est toujours le premiers neuron de pcomb
%
% output:
% c : xcorr matrix
% lags: vector for frame lags
% csur : surrogate population
% threshold : estimate of significance threshold

Args = struct('save',0,'redo',0,'iterations',2000,'maxframe',300,'pvalue',[10^-4 10^-5 10^-6 10^-7 10^-8],'nofitting',0,'maxlagbin',10,'binsize',2,'seedNeurons',[],'addName',[],'jitterTrialOnly',0,'jitterSpike',0,'parSur',0,'distr','Poisson');
Args.flags = {'save','redo','nofitting','jitterTrialOnly','jitterSpike','parSur'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

filename = ['xcorrSpikes' Args.addName '.mat'];

if isempty(nptDir(filename)) || Args.redo
    maxframe = ceil(Args.binsize/2) + Args.maxlagbin*Args.binsize;
    
    bins = [-maxframe:Args.binsize:maxframe];
    
    %     bins = [min(bins)-1 bins max(bins)+1];
    if length(size(onsets)) == 2
        ncell = length(onsets);
        ntrial = size(onsets,2);
    elseif length(size(onsets)) == 3 % for Sam
        ncell = size(onsets,1) * size(onsets,2);
        ntrial = size(onsets,3);
    end
    %% select only pairs with at least one seed pixel and the seed pixel is always first
    pcomb = nchoosek([1:ncell],2);
    if ~isempty(Args.seedNeurons)
        seedpairs = ismember(pcomb,Args.seedNeurons);
        rows = find(sum(seedpairs,2) ~=1 );
        % rows = find(sum(seedpairs,2) ==0 );
        pcomb(rows,:) = [];
        seedpairs = ismember(pcomb,Args.seedNeurons);
        shiftrow = find(seedpairs(:,2) == 1);
        pcomb(shiftrow,:) =  pcomb(shiftrow,[2 1]);
    end
    cumcoi = zeros(size(pcomb,1),length(bins) - 1);
    for ncomb = 1 : size(pcomb,1)
        for trial = 1 : ntrial
            if length(size(onsets)) == 2
                times1 = onsets{pcomb(ncomb,1),trial};
                times2 = onsets{pcomb(ncomb,2),trial};
            elseif length(size(onsets)) == 3
                times1 = onsets{floor(pcomb(ncomb,1)/size(onsets,2)) + 1,mod(pcomb(ncomb,1),size(onsets,2)) + 1,trial};
                times2 = onsets{floor(pcomb(ncomb,2)/size(onsets,2)) + 1,mod(pcomb(ncomb,2),size(onsets,2)) + 1,trial};
            end
            for nspike = 1 : length(times1)
                if ~isempty(times2)
                    nh = histc(times2 - times1(nspike),bins);
                    cumcoi(ncomb,:) = squeeze(cumcoi(ncomb,:)) + vecr((nh(1:end -1))); % the last bin is simply the last value
                end
            end
        end
    end
    %% surrogate
    maxrectime = maxframe - 2;
    surcoi = zeros(Args.iterations,size(pcomb,1),length(bins) - 1);
    if Args.parSur
        parfor ii = 1 : Args.iterations
            tfilename = sprintf('tempsurrogate%d.mat',ii);
            parsurcoi(pcomb,bins,ncell,maxrectime,ntrial,onsets,Args,tfilename);
        end
        for ii = 1 : Args.iterations
             tfilename = sprintf('tempsurrogate%d.mat',ii);
            tsur = load(tfilename);
            surcoi(ii,:,:) = tsur.surcoi;
            delete(tfilename)
        end
    else
        
        %       maxrectime = max(max(cat(1,onsets{:})));
        
        for ii = 1 : Args.iterations
            signr = randi(2,[1, ncell]);
            signr(signr == 2) = -1; % randomized the lag direction - or +
            shuffletime = signr .* randi(maxrectime,[1, ncell]);
            
            shuffletrial = randperm(ntrial);
            for ncomb = 1 : size(pcomb,1)
                
                for trial = 1 : ntrial
                    if length(size(onsets)) == 2
                        times1s = onsets{pcomb(ncomb,1),trial};
                        if Args.jitterTrialOnly
                            times2s = onsets{pcomb(ncomb,2),shuffletrial};
                        else
                            times2s = onsets{pcomb(ncomb,2),trial};
                        end
                    elseif length(size(onsets)) == 3
                        times1s = onsets{floor(pcomb(ncomb,1)/size(onsets,2)) + 1,mod(pcomb(ncomb,1),size(onsets,2)),trial};
                        times2s = onsets{floor(pcomb(ncomb,2)/size(onsets,2)) + 1,mod(pcomb(ncomb,2),size(onsets,2)),trial};
                    end
                    
                    for nspike = 1 : length(times1s)
                        if ~isempty(times2s)
                            %                     newonsets = mod(onsets{pcomb(ncomb,2)} + shuffletime(pcomb(ncomb,2)),maxrectime);
                            if Args.jitterTrialOnly
                                newonsets = times2s;
                            elseif Args.jitterSpike
                                signr = randi(2);
                                signr(signr == 2) = -1; % randomized the lag direction - or +
                                shuffletime = signr .* randi(maxrectime,length(times2s),1);
                                newonsets = times2s + shuffletime;
                            else
                                newonsets = times2s + shuffletime(pcomb(ncomb,2));
                            end
                            nh = histc(newonsets - times1s(nspike),bins);
                            surcoi(ii,ncomb,:) = squeeze(surcoi(ii,ncomb,:)) + vecc(nh(1:end - 1));
                        end
                    end
                end
                
            end
            curtime = clock;
            display(['surrogate ' num2str(ii) ' ' num2str(curtime(4)) 'h' num2str(curtime(5))]);
        end
        
        %     cumcoi = cumcoi(:,2:end-1); % when using hist instead histc
        %     bins = bins(2:end-1);  % when using hist instead histc
        
        %     surcoi = single(surcoi(:,:,2:end-1));
        surcoi = single(surcoi);
        
    end
    
    pvalue = ones( size(cumcoi,1),size(cumcoi,2));
    fdrvalues =  ones(size(cumcoi,1),size(cumcoi,2));
    for rr = 1 : size(cumcoi,2)
        for cc = 1 : size(cumcoi,1)
            pvalue(cc,rr) = 1-pvalueEstimateFromShuffle(surcoi(:,cc,rr),cumcoi(cc,rr),'distr',Args.distr);
        end
        fdrvalues(:,rr) = mafdr(pvalue(:,rr),'BHFDR',true);
    end
    thresholds = nan(length(Args.pvalue)*2,size(pcomb,1),length(bins) - 1);
    if ~Args.nofitting
        for ncomb = 1 : size(pcomb,1)
            for thebin = 1 : length(bins) -1
                if sum(~isnan(squeeze(surcoi(:,ncomb,thebin)))) ~= 0 && cumcoi(ncomb,thebin) ~= 0
                    try
                        [~,~,thresholds(:,ncomb,thebin),~] = normfitSur(squeeze(surcoi(:,ncomb,thebin)),[Args.pvalue 1-Args.pvalue],'twosided');
                    end
                end
            end
        end
        
        thresholds = single(thresholds);
    end
    %%
    bins = bins(1:end-1) + Args.binsize/2;
    if Args.save
        specvar = whos('surcoi');
        if specvar.bytes >= 10^9
            surcoi = max(surcoi,[],1);
            display('surrogate too big only saving max value')
        end
        
        save(filename,'cumcoi','surcoi','pcomb','bins','thresholds','fdrvalues','pvalue');
        display(['saving ' pwd '/' filename])
        
    end
else
    load(filename)
end
%%
function parsurcoi(pcomb,bins,ncell,maxrectime,ntrial,onsets,Args,tfilename)

if isempty(nptDir(tfilename))
    surcoi = zeros(size(pcomb,1),length(bins) - 1);
    signr = randi(2,[1, ncell]);
    signr(signr == 2) = -1; % randomized the lag direction - or +
    shuffletime = signr .* randi(maxrectime,[1, ncell]);
    shuffletrial = randperm(ntrial);
    for ncomb = 1 : size(pcomb,1)
        
        for trial = 1 : ntrial
            if length(size(onsets)) == 2
                times1s = onsets{pcomb(ncomb,1),trial};
                if Args.jitterTrialOnly
                    times2s = onsets{pcomb(ncomb,2),shuffletrial};
                else
                    times2s = onsets{pcomb(ncomb,2),trial};
                end
            elseif length(size(onsets)) == 3
                times1s = onsets{floor(pcomb(ncomb,1)/size(onsets,2)) + 1,mod(pcomb(ncomb,1),size(onsets,2)),trial};
                times2s = onsets{floor(pcomb(ncomb,2)/size(onsets,2)) + 1,mod(pcomb(ncomb,2),size(onsets,2)),trial};
            end
            
            for nspike = 1 : length(times1s)
                if ~isempty(times2s)
                    %                     newonsets = mod(onsets{pcomb(ncomb,2)} + shuffletime(pcomb(ncomb,2)),maxrectime);
                    if Args.jitterTrialOnly
                        newonsets = times2s;
                    elseif Args.jitterSpike
                        signr = randi(2);
                        signr(signr == 2) = -1; % randomized the lag direction - or +
                        shuffletime = signr .* randi(maxrectime,length(times2s),1);
                        newonsets = times2s + shuffletime;
                    else
                        newonsets = times2s + shuffletime(pcomb(ncomb,2));
                    end
                    nh = histc(newonsets - times1s(nspike),bins);
                    surcoi(ncomb,:) = squeeze(surcoi(ncomb,:)) + vecr(nh(1:end - 1));
                end
            end
        end
        
    end
    surcoi = single(surcoi);
    save(tfilename,'surcoi')
    display([pwd filesep tfilename])
end