mdirs = {'D:\workingmemory\data\monkey\clark' 'D:\workingmemory\data\monkey\betty' 'D:\workingmemory\data\monkey\betty\acute'};
monkey = {'clark' 'betty' 'betty/acute'};
for sd = 1 : 3
    cd(mdirs{sd})
    load switchdays
    
    sdir = pwd;
    
    BehResp = 1;% watch out if = 1 have to add 'stable' args
    cor = {'Incor' ''};
    for d = fliplr(1: length(days))
        %         for d = 1 : length(days)
        cd(sprintf('%s/%s',sdir,days{d}))
        
        %     for s = 2 : nses
        %         cd(['session0' num2str(s)])
        for rule = 1 : 2
            cd session99
            mts = mtstrial('auto','redosetNames');
            mkdir xcorr
            trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule,'noRejNoisyT');
            %         trials = mtsgetTrials(mts,'rule',rule,'noRejNoisyT');
            if ~isempty(trials)
                % windows solution
                fid = fopen('C:\cygwin64\home\salazarr\tempCommand1.sh','w');
                fprintf(fid,'cd /cygdrive/D/workingmemory/data/monkey/%s/%s/session99; find . -ipath ''*cluster*'' -name ''ispikes.mat'' -print',monkey{sd},days{d});
                fclose(fid);
                [st,allfiles] = system('C:\cygwin64\bin\bash --login -c "/home/salazarr/tempCommand1.sh"');
                % [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
                if ~isempty(allfiles)
                    stfiles = findstr(allfiles,'ispikes.mat');
                    cross = nchoosek([1:length(stfiles)],2);
                    auto = [(1:length(stfiles))' (1:length(stfiles))'];
                    iscomb = cat(1,cross,auto);
                    for cb = 1 : size(iscomb,1)
                        if cb <= size(cross,1)
                            fi1 = iscomb(cb,1);
                            fi2 = iscomb(cb,2);
                            cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                            sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                            sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                            ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
                            %                     ntoadd = sprintf('Rule%dAlltrials',rule);
                            cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                            
                            spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'redo','save','xcorr','binSize',500,'xcorrBin',500,'removeMean','addName',ntoadd);
                            spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'redo','save','xcorr','binSize',500,'xcorrBin',500,'addName',[ntoadd 'noMeanR']);
                            spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'save','xcorr','binSize',500,'xcorrBin',500,'removeMean','surrogate','addName',ntoadd);
                        else
                        end
                    end
                end
            end
            
            cd(sprintf('%s/%s',sdir,days{d}))
        end
    end
end


%% compare xcorr rules
mdirs = {'D:\workingmemory\data\monkey\clark' 'D:\workingmemory\data\monkey\betty' 'D:\workingmemory\data\monkey\betty\acute'};
monkey = {'clark' 'betty' 'betty/acute'};
for sd = 1 : 3
    cd(mdirs{sd})
    load switchdays
    
    sdir = pwd;
    BehResp = 1;% watch out if = 1 have to add 'stable' args
    cor = {'Incor' ''};
    nses = '99';
    % clark
    for d = 1: length(days)
        cd(sprintf('%s/%s',sdir,days{d}))
        cd(sprintf('session%s',nses))
        mts = mtstrial('auto','redosetNames');
        trials1 = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',1,'noRejNoisyT');
        trials2 = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',2,'noRejNoisyT');
        if ~isempty(trials1) || ~isempty(trials2)
            fid = fopen('C:\cygwin64\home\salazarr\tempCommand1.sh','w');
            fprintf(fid,'cd /cygdrive/D/workingmemory/data/monkey/%s/%s/session99; find . -ipath ''*cluster*'' -name ''ispikes.mat'' -print',monkey{sd},days{d});
            fclose(fid);
            [st,allfiles] = system('C:\cygwin64\bin\bash --login -c "/home/salazarr/tempCommand1.sh"');
            if ~isempty(allfiles)
                stfiles = findstr(allfiles,'ispikes.mat');
                cross = nchoosek([1:length(stfiles)],2);
                iscomb = cross;
                for cb = 1 : size(iscomb,1)
                    fi1 = iscomb(cb,1);
                    fi2 = iscomb(cb,2);
                    cd(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                    sp1 = load(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                    sp2 = load(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                    cd(sprintf('%s/%s/session%s/xcorr',sdir,days{d},nses))
                    %                [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save');
                    %                 [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save','xcorr');
                    [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save','xcorr','binSize',500,'addName','500ms');
                end
            end
        end
    end
end