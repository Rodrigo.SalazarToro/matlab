nCtot = zeros(21,3);
nFtot = zeros(21,2);
for d = 1 : 21
    cd(days{d})
    cd grams
    
    for loc = 1 : 3
        files = nptDir(sprintf('migramg*Rule1IdePerLoc%d.mat',loc));
        if ~isempty(files)
            load(files(1).name)
            
            nCtot(d,loc) = sum(cellfun(@length,trials));
        end
    end 
    
    cd ..
    cd session01
    mts = mtstrial('auto');
    nFtot(d,1) = sum(mts.data.CueObj ==55);
    cd ..
    
    if ~isempty(nptDir('session03'))
    cd session03
    mts = mtstrial('auto');
    nFtot(d,2) = mts.data.numSets;
    cd ..
    end
    
    cd ..
end

for loc = 1 : 3
ndays(loc) = sum(nCtot(:,loc) <= nFtot(:,1));
end


theday = find((nCtot(:,1) <= nFtot(:,1)) + (nCtot(:,2) <= nFtot(:,1)) + (nCtot(:,3) <= nFtot(:,1)) ==3);



for loc = 1 : 3
ndays(loc) = sum(nCtot(:,loc) <= nFtot(:,2));
end


theday = find((nCtot(:,1) <= nFtot(:,2)) + (nCtot(:,2) <= nFtot(:,2)) + (nCtot(:,3) <= nFtot(:,2)) ==3);

'091002'    '100129'    '100201'    '100203'    '100216'    '100218'


npairs = 0;
for dd = 1 : 6

indices = find(cellfun(@isempty,strfind(obj.data.setNames,days{theday(dd)})) == 0);

npairs = npairs + length(intersect(indices,ind));

end
