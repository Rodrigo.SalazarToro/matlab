
cd /Volumes/FITZGERALD/monkey/betty/090918/session01/
stims = cell(3,3);
mts = mtstrial('auto','redosetNames');
for obj = 1 : 3
    for loc = 1: 3
        stims{obj,loc} = mtsgetTrials(mts,'iCueObj',obj,'iCueLoc',loc,'stable','BehResp',1,'rule',1);
        
    end
end

cd /Volumes/FUSION/monkey/betty/090918/session01/lfp/beta

load /Volumes/FITZGERALD/monkey/betty/090918/session01/group0048/cluster01s/ispikes.mat
timing = [mts.data.CueOffset mts.data.MatchOnset];
files = nptDir('*beta*');

phases = cell(3,3);
peaks = cell(3,3);
ch = 11;
for obj = 1 : 3
    for loc = 1: 3
        
        for ff = stims{obj,loc}
            spikes = sp.data.trial(ff).cluster.spikes;
            
            selspikes = find(spikes > timing(ff,1) & spikes < timing(ff,2));
            beta = load(files(ff).name,'-mat');
            [pks1,locs1] = findpeaks(real(beta.hb(ch,:)));
            [pks2,locs2] = findpeaks(- real(beta.hb(ch,:)));
            for isp = selspikes
                [~,I1] = min(abs(locs1 - ceil(spikes(isp)/5)));
                [~,I2] = min(abs(locs2 - ceil(spikes(isp)/5)));
                
                peaks{obj,loc} = [peaks{obj,loc} [pks1(I1); pks2(I2)]];
                
                phases{obj,loc} = [phases{obj,loc} beta.hb(ch,ceil(spikes(isp)/5))]; %9
            end
            
            %                         subplot(2,1,1);
            %                         plot(beta.prelfp(ch,:))
            %                         hold on
            %                         plot(ceil(spikes(selspikes)/5),100*ones(length(selspikes),1),'r*')
            %                         subplot(2,1,2)
            %
            %                         plot(angle(beta.hb(ch,:)))
            %                         hold on
            %                         plot(ceil(spikes(selspikes)/5),peaks{obj,loc}(1,:),'+')
            %                         plot(ceil(spikes(selspikes)/5),peaks{obj,loc}(2,:),'+')
            %                         pause
            %                         clf
        end
    end
end




allpeaks = [];
for obj = 1 : 3
    for loc = 1: 3
        allpeaks = [allpeaks peaks{obj,loc}];
    end
end

lim = mean(prctile(allpeaks,66.6,2));


tout=cell(3,3);
rout=cell(3,3);
stats = cell(3,3);
maxr = [];
pval = zeros(3,3);
p = zeros(3,3);
for obj = 1 : 3
    for loc = 1: 3
        if ~isempty(lim)
            ind = find(peaks{obj,loc}(1,:) > lim & peaks{obj,loc}(2,:) > lim);
        else
            ind = [1 : length(peaks{obj,loc})];
        end
        
        [tout{obj,loc},rout{obj,loc}] = rose(angle(phases{obj,loc}(ind)));
        rout{obj,loc} = rout{obj,loc} / length(ind);
        [pval(obj,loc), z] = circ_rtest(angle(phases{obj,loc}(ind)));
        [p(obj,loc), U, UC] = circ_raotest(angle(phases{obj,loc}(ind)));
        stats{obj,loc} = circ_stats(angle(phases{obj,loc}(ind)));
        maxr = [maxr max(rout{obj,loc})];
    end
end
figure
alldata = zeros(1,80);
for obj = 1 : 3
    for loc = 1: 3
        subplot(3,3,(obj-1)*3+loc)
        polar([0 circ_mean(angle(phases{obj,loc}),[],2)],[0 max(maxr)],'r')
        
        hold on
        polar(tout{obj,loc},rout{obj,loc});
        
        title([pval(obj,loc) p(obj,loc)])
        alldata = alldata + rout{obj,loc};
    end
end
%%


files = nptDir('*HBphase.*');


allphases = cell(3,3);
allsur = cell(3,3);
ch = 37;

for obj = 1 : 3
    for loc = 1: 3
        for ff = stims{obj,loc}
            load(files(ff).name,'-mat')
            anglet = squeeze(angle(phases(1001,ch,:,:)));
            sur = squeeze(angle(phases(1:1000,ch,:,:)));
            
            if isvector(anglet)
                allphases{obj,loc} = cat(1,allphases{obj,loc},vecr(anglet));
                allsur{obj,loc} = cat(2,allsur{obj,loc},reshape(sur,[1000 1 length(f)]));
            else
                allphases{obj,loc} = cat(1,allphases{obj,loc},anglet);
                allsur{obj,loc} = cat(2,allsur{obj,loc},sur);
            end
        end
    end
end

for obj = 1 : 3
    for loc = 1: 3
        figure
        set(gcf,'Name',['obj' num2str(obj) ' loc' num2str(loc)]);
        for fi = 1 : 10
            subplot(2,5,fi)
            [tout, routd] = rose(allphases{obj,loc}(:,fi));
            polar(tout,routd)
            hold on
            for ii = 1 : 1000
                [tout,rout(ii,:)] = rose(allsur{obj,loc}(ii,:,fi));
            end
            thr = prctile(rout,99.9,1);
            sig = find(routd > thr);
            
            polar(tout,thr,'r--')
            polar(tout(sig),routd(sig),'*c')
            title(['central freq = ' num2str(f(fi))])
        end
    end
end
%%

epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
pvalues = [95 99 99.9 99.99];
sdir = pwd;
for ru = 1 : 2
    parfor d =1 : 20
        cd([sdir '/' days{d}])
        ses(d) = load('rules.mat');
        s = find(ses(d).r ==ru);
        cd([sdir '/' days{d} '/session0' num2str(s+1)])
        groups = nptDir('group0*');
        mts = mtstrial('auto','redosetNames');
        
        trials = mtsgetTrials(mts,'stable','BehResp',1);
         [estSac,measSac,unctrials] = checkFirstSac;
         trials = setdiff(trials,unctrials);
        for gr = 1 : length(groups)
%             %% SUA
%             cd([sdir '/' days{d} '/session0' num2str(s+1) '/' groups(gr).name])
%             
%             clusters = nptDir('cluster0*s');
%             for cl =1  : length(clusters)
%                 cd(clusters(cl).name)
%                 if ~isempty(nptDir('bothsessions.mat'))
%                     load('bothsessions.mat')
%                     if stable
%                         %                         for ep = 1 : 5; writeLFPspikeHBphase('timeFrame',epochs{ep}); end
%                         %                         tdir = pwd;
%                         %                         ndir = regexprep(tdir,'session02','session03');
%                         %                         othersession = clusters(cl).name;
%                         %                         cd(ndir)
%                         %                         save('bothsessions.mat','stable','othersession');
%                         %                         for ep = 1 : 5; writeLFPspikeHBphase('timeFrame',epochs{ep}); end
%                         %                         cd(tdir);
%                         
%                         cd HBphase
%                         
%                         for ep = 1 : 5; writeClusterPhases(trials,'type',epochs{ep},'addFile',['rule' num2str(ru)]); end
%                         
%                         
%                         cd ..
%                         
%                     end
%                 end
%                 
%                 cd ..
%             end
            %%
            
            %
            %             cd([sdir '/' days{d} '/session0' num2str(s+1) '/' groups(gr).name '/cluster01m/'])
            %             for ep = 1 : 5; writeLFPspikeHBphase('timeFrame',epochs{ep}); end
            %
            try
                        cd([sdir '/' days{d} '/session0' num2str(s+1) '/' groups(gr).name '/cluster01m/HBphase/'])
                        for ep = 5 : 5;writeClusterPhases(trials,'type',epochs{ep},'addFile',['rule' num2str(ru)],'redo'); end
            end
        end
        
    end
end
% for betty
pvalues = [95 99 99.9 99.99];
sdir = pwd;
epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
for ru = 1 : 2
    parfor d =11 : 15
        
        cd([sdir '/' days{d} '/session01'])
        groups = nptDir('group0*');
        mts = mtstrial('auto','redosetNames');
        
        trials = mtsgetTrials(mts,'stable','BehResp',1,'ML','rule',ru);
        %
        for gr = 1 : length(groups)
            cd([sdir '/' days{d} '/session01/' groups(gr).name '/cluster01m/'])
            for ep = 1 : 5; writeLFPspikeHBphase('timeFrame',epochs{ep}); end
            %
            %             cd([sdir '/' days{d} '/session01/' groups(gr).name '/cluster01m/HBphase/'])
            %             for ep = 1 : 5;writeClusterPhases(trials,'type',epochs{ep},'addFile',['rule' num2str(ru)]); end
            
        end
        
    end
end




types = {'samegr' 'PPCs_PFClfp' 'PPCs_PPClfp' 'PFCs_PPClfp' 'PFCs_PFClfp' };
allsig = zeros(80,5,10);
nPPC = 0;
nPFC = 0;

pairc = zeros(5,1);
for d =1 : 20
    cd(days{d})
    load rules
    s = find(r ==2);
    cd(['session0' num2str(s+1)])
    groups = nptDir('group0*');
%     mts = mtstrial('auto','redosetNames');
%     trials = mtsgetTrials(mts,'stable','BehResp',1);
    neuroinfo = NeuronalCHAssign;
    
    
    
    for gr = 1 : length(groups)
        thegr = str2double(groups(gr).name(end-3:end));
        samegr = find(neuroinfo.groups == thegr);
        
        if thegr <= 8 
            PPCs_PFClfp = find(neuroinfo.groups > 8);
            PPCs_PPClfp = setdiff(find(neuroinfo.groups <= 8),samegr);
            PFCs_PPClfp = [];
             PFCs_PFClfp = [];
             nPPC = nPPC +1;
        else
             PFCs_PPClfp = find(neuroinfo.groups <= 8);
             PFCs_PFClfp = setdiff(find(neuroinfo.groups > 8),samegr);
             PPCs_PFClfp = [];
            PPCs_PPClfp = [];
            nPFC = nPFC +1;
        end
        
        
        
        cd(groups(gr).name)
        cd cluster01m/HBphase/
        load saccadePhasesrule2.mat
%         try
        for ty = 1 : 5
            sch = eval(types{ty});
            if ~isempty(sch)
            for ch =1 : length(sch);
                for fi = 1 : 10; sig = squeeze(rout(sch(ch),fi,:)) > squeeze(thr(sch(ch),fi,:,3));allsig(:,ty,fi) = allsig(:,ty,fi) + sig;end;
                pairc(ty) = pairc(ty) +1;
            end
            end
        end
        
%         catch
%           writeClusterPhases(trials,'addFile','rule1','redo')  
%             
%         end
        cd ../../..
    end
    cd ../..
end

freq = [8 : 4 : 44];

for ty = 1 : 5
    figure
    set(gcf, 'Name',types{ty})
    %    limi = 100*max(max(allsig(:,ty,:))) / pairc(ty);
    if ty == 1; limi = 20; else; limi = 10; end
    for fi = 1 : 10
        subplot(2,5,fi)
        polar(0,limi)
        hold on
        polar(tout,100*squeeze(allsig(:,ty,fi))'/ pairc(ty))
        title(['Freq. ' int2str(freq(fi))])
    end
end


