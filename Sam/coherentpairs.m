
% for ex = 6
%     cd(sprintf('exp0%d',ex))
%     files = nptDir('*coh.mat');
%
%     parfor ff = 1 : length(files)
%         try
%             getpeaksPairs(files(ff).name)
%         catch
%             files(ff).name
%         end
%     end
% end

% load F:\salazar\data\sam\StimInfo
%
% mask = expe{6}.K(:,1:100);
% files = nptDir('*coh.mat');
% figure
% imagesc(mask)
% for ff = 1 : length(files)
%     load(files(ff).name);
%     for bb =  1
%         if ~isempty(coherent{bb})
%             spairs = find(Ccoh{bb} > 0.9);
%             if ~isempty(spairs) && spairs(end) > size(coherent{bb},3)
%                 line(squeeze(coherent{bb}(:,2,spairs(end-1))),squeeze(coherent{bb}(:,1,spairs(end-1))));
%             else
%                 line(squeeze(coherent{bb}(:,2,spairs)),squeeze(coherent{bb}(:,1,spairs)));
%             end
%         end
%         hold on
%     end
% end
makedir('coherence')
cb = [0 1; 0 2; 0 3; 0 4; 0 5; 0 6; 1 1; 1 2; 1 3; 1 4; 1 5; 1 6];

parfor cc = 1 : 12; makecoherenceSam('110901s',cb(cc,1),cb(cc,2)); end

cd F:\salazar\data\sam\coherence\

for exp = 5 : 5
    cd(['exp0' num2str(exp)])
    files = nptDir('*coh.mat');
    parfor ff = 1 : length(files)
        try
            getpeaksPairs(files(ff).name)
        catch
            files(ff).name
        end
        
    end
    cd ..
end

sdir = pwd;
conds = [3 0; 4 0 ; 5 0; 3 1; 4 1; 5 1;];
parfor cond = 1:6
    cd([sdir '\exp0' num2str(conds(cond,1))])
    calculateCohMap('110901s','stim',(conds(cond,2)),'save');
    cd ..
end


sdir = pwd;
conds = [3 0; 4 0 ; 5 0; 3 1; 4 1; 5 1;];
load 150309_listpix_S1
parfor cond = 1:6
    cd([sdir '\exp0' num2str(conds(cond,1))])
    calculateCohMap('110901s','stim',(conds(cond,2)),'seedpixel',listpix,'save','addName','seedS1');
    cd ..
end

bands = {'LF' 'delta' 'theta' 'heart beat' 'beta'};
explim = [1800 1800 200 2500 1800];
for bb = [1 2 3 4 5]
    figure
    set(gcf,'Name',bands{bb})
    for exp = 3 : 5
        for stim = 0 : 1
            cd(['exp0' num2str(exp)])
            load(sprintf('cohMapStim%d.mat',stim))
            subplot(2,3,(stim )* 3 + exp - 2)
            imagesc((squeeze(acpairs(bb,:,:)))',[0 explim(bb)])
            title(['exp0' num2str(exp)])
            colorbar
            xlim([1 100])
            ylim([1 100])
            cd ..
        end
    end
    xlabel('medial-lateral')
    ylabel('Anterior-posterior')
    subplot(2,3,1); ylabel('no stim')
    subplot(2,3,4); ylabel('with stim')
end


bands = {'delta' 'theta' 'heart beat' 'beta'};
explim = [100 50 0 100];
for bb = [1 2 3 4 5]
    figure
    set(gcf,'Name',bands{bb})
    for exp = 3 : 5
        for stim = 0 : 1
            cd(['exp0' num2str(exp)])
            load(sprintf('cohMapStim%dseedS1.mat',stim))
            subplot(2,3,(stim )* 3 + exp - 2)
            imagesc((squeeze(acpairs(bb,:,:)))',[0 explim(bb)])
            title(['exp0' num2str(exp)])
            colorbar
            xlim([1 100])
            ylim([1 100])
            cd ..
        end
    end
    xlabel('medial-lateral')
    ylabel('Anterior-posterior')
    subplot(2,3,1); ylabel('no stim')
    subplot(2,3,4); ylabel('with stim')
end

