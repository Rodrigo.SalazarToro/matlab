function [B,A1,A2,T,bool]=readMicamSam(adress,Condi)
% % output:
% B (rowx col) raw data
% A1 respiration channel
% A2 stimulus channel
% T period between frames
out=readMiCamrsh(adress);
T=out.TperF;
if T>=1
    T=T/1000;
end;
g=0;
for k=1:length(adress)
    if(strcmp(adress(k),'/'))
        g=k;
    end
end
Fold=adress(1:g);
fid=fopen(strcat(Fold,char(out.FileNames(1))),'r');
A=fread(fid,[128,inf],'int16');
fclose(fid);
N=size(A,2)/100;
for k=1:N
    A1((1+(k-1)*20):k*20)=A(13,(1+(k-1)*100):4:(80+(k-1)*100));
    A2((1+(k-1)*20):k*20)=A(15,(1+(k-1)*100):4:(80+(k-1)*100));
end;

A=A(21:120,:);
A=reshape(A,100,100,[]);
if(out.dual==3 && Condi==1)
    A=A(:,:,1:2:end);
end
if(out.dual==3 && Condi==2)
    A=A(:,end:-1:1,2:2:end);
end
B(:,:,:)=A; clear A;

if(out.NFile>1)
    for i=2:out.NFile
        fid=fopen(strcat(Fold,char(out.FileNames(i))),'r');
        A=fread(fid,[128,inf],'int16');
        fclose(fid);
        N=size(A,2)/100;        
        for k=1:N
            A1((1+(k-1)*20+(i-1)*5120):(k*20+(i-1)*5120))=A(13,(1+(k-1)*100):4:(80+(k-1)*100));
            A2((1+(k-1)*20+(i-1)*5120):(k*20+(i-1)*5120))=A(15,(1+(k-1)*100):4:(80+(k-1)*100));
        end;
        
        A=A(21:120,:);
        A=reshape(A,100,100,[]);
        if(out.dual==3 && Condi==1)
            A=A(:,:,1:2:end);
        end
        if(out.dual==3 && Condi==2)
            A=A(:,end:-1:1,2:2:end);
        end
        B=cat(3,B,A); clear A;
    end
end

B=B/out.av;
B=permute(B,[2 1 3]);

C=B(:,:,1);
if(out.stimmode)           % For the normal mode
    B(:,:,2:end)=B(:,:,2:end)+repmat(B(:,:,1),[1,1,size(B,3)-1]);
end

bool=(out.dual==3);

if bool
    ri=1:20;
    for kkk=1:((length(A1)/40)-1)
        ri=[ri [(1:20)+40*kkk]];
    end;
    A1=A1(ri);
    A2=A2(ri);
end;