function processCorDaySam(basename,exp,varargin)

Args = struct('redo',0,'Fs',500,'onlyPlot',0,'lookTrialType',0,'onlyCohMap',0);
Args.flags = {'redo','onlyPlot','lookTrialType','onlyCohMap'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

maindir = 'correlations';
sdir = pwd;
mkdir(maindir)
cd(maindir)
if ~Args.onlyPlot
    if Args.lookTrialType
        cb = cat(2,zeros(length(exp),1),exp');
    else
        cb = cat(2,cat(1,zeros(length(exp),1),ones(length(exp),1)),cat(1,exp',exp'));
    end
    if ~Args.onlyCohMap
        for e = exp
            
            mkdir([sdir filesep maindir filesep 'exp0' num2str(e)]);
            cd ..
        end
        cd(sdir)
        
        parfor cc = 1 : size(cb,1);
            makecorrelationsSam(basename,cb(cc,1),cb(cc,2),'Fs',Args.Fs,modvarargin{:});
        end
        
        cd([sdir filesep maindir])
        
        for e = exp
            
            cd(['exp0' num2str(e)])
            files = nptDir('*cor.mat');
            for ff = 1 : length(files)
                getpeaksPairs(files(ff).name,modvarargin{:})
            end
            cd ..
        end
    end
    
    %     seed = load([sdir filesep 'seedS1.mat']);
    parfor cond = 1:size(cb,1)
        cd([sdir sprintf('%s%s%sexp0%d',filesep,maindir,filesep,cb(cond,2))])
        calculateCohMap(basename,'stim',(cb(cond,1)),'save',modvarargin{:});
        %         calculateCohMap(basename,'stim',(cb(cond,1)),'seedpixel',seed.S1,'save','addName','seedS1');
        cd ..
    end
end

%% plotting
cd([sdir filesep maindir])

% explim = [4000 4000 100 4500 800];

figure
set(gcf,'Name','correlations')
c = 1;
for e = exp
   
        stims = 0;
        cd(['exp0' num2str(e)])
        load(sprintf('cohMapStim%d.mat',stims))
        for tt = 1 : size(acpairs,1)
            subplot(size(acpairs,1),length(exp),(tt - 1)* length(exp) +c)
            imagesc((squeeze(acpairs(tt,1,:,:)))',[0 400])
            title(['exp0' num2str(e) ])
            colorbar
            xlim([1 100])
            ylim([1 100])
            
        end
        c= c+1;
        cd ..
   
    xlabel('medial-lateral')
    ylabel('Anterior-posterior')
end
cd ..



