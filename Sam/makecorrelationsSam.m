function makecorrelationsSam(filename,stim,exp,varargin)


Args = struct('redo',0,'Fs',500,'reFs',500,'lowpass',80,'maxlag',50,'trials',10,'rpixels',100,'cpixels',100,'trialcut',[],'lookTrialType',0,'realignTrial',0,'minonset',248,'maxafteronset',999,'lightduration',31);
Args.flags = {'redo','lookTrialType','realignTrial'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

load StimInfo.mat % info about exp with expe{6}.K for the mask
load bv_mask.mat
ind =findExp(exp);
trials = expe{ind}.condR;
ntrial = length(trials);

[Btemp,~,~,~,~] = readMicamSam(sprintf('%s%d-%d-%d.rsh',filename,stim,exp,1),1);
normA = nan(Args.rpixels,Args.cpixels,size(Btemp,3),Args.trials);

nfile = sprintf('%s%d-%d-norm.mat',filename,stim,exp);
sdir = pwd;
if isempty(nptDir(nfile)) || Args.redo
    count = 1 ;
    for tr = 1 : ntrial
        [B,~,~,~,~] = readMicamSam(sprintf('%s%d-%d-%d.rsh',filename,stim,exp,tr),1); % cond (0:no stim; 1:stim) - exp# - trial#
        actB = B .* repmat(expe{6}.K(:,1:Args.cpixels),[1 1 size(B,3)]); % mask contour\
        actB = actB .* repmat(K2,[1 1 size(B,3)]); % blood vessel contour\
        if strcmp(trials(tr),'l'); 
            startlight = floor(Args.Fs * expe{ind-1 + strfind(expe{ind}.condIDl,'l')}.on_O(count));  
           
             for r = 1 : Args.rpixels; for c = 1 : Args.cpixels; actB(r,c,[1:startlight-1 startlight + Args.lightduration : size(actB,3)]) = detrend(squeeze( actB(r,c,[1:startlight-1 startlight + Args.lightduration : size(actB,3)])));end; end % detrend
              actB(:,:,startlight: startlight + Args.lightduration - 1) = nan(100,100, Args.lightduration);
            count = count + 1; 
        else
            
            for r = 1 : Args.rpixels; for c = 1 : Args.cpixels; actB(r,c,:) = detrend(squeeze(actB(r,c,:)));end; end % detrend
        end
        
        normA(:,:,:,tr)  = (actB - repmat(squeeze(nanmean(actB,3)),[1 1 size(B,3)])) ./ repmat(squeeze(nanstd(actB,[],3)),[1 1 size(B,3)]); % z-score
    end
    if Args.realignTrial
        realigndata = nan(size(normA,1),size(normA,2),length(-Args.minonset: Args.maxafteronset),size(normA,4));
        
        for c = 1 : length(expe{ind}.condIDl)
            onsets = expe{ind-1 +c}.on_O;
            strial = find(expe{ind}.condIDl(c) == trials);
            for tr = 1 : length(strial)
                realigndata(:,:,:,strial(tr)) = normA(:,:,Args.Fs*onsets(tr) - Args.minonset : Args.Fs*onsets(tr) + Args.maxafteronset,strial(tr));
            end
        end
        
        normA = realigndata;
    end
    normA = single(normA);
    
    save(nfile,'normA','-v7.3');
    display(['saved ' nfile])
else
    load(nfile)
end
clear B actB

cd correlations
if isempty(nptDir('pixelComb.mat')) || Args.redo
    [rgoodP,cgoodP] = find(expe{6}.K(:,1:Args.cpixels) & K2);
    comb = single(nchoosek([1:length(rgoodP)],2));
    save('pixelComb.mat','comb','rgoodP','cgoodP')
else
    load pixelComb.mat
    
end
if Args.lookTrialType
    
    condID = expe{ind}.condIDl;
    trials = expe{ind}.condR;
    for ttype = 1 : length(condID)
        tlist{ttype} = strfind(trials,condID(ttype));
    end
else
    condID = 1;
    tlist{1} = [1:ntrial];
    clear expe
end
cd(['exp0' num2str(exp)])
for p1 = 1 : length(rgoodP)
    sname = sprintf('%s%d-%d-p1r%dc%dcor.mat',filename,stim,exp,rgoodP(p1),cgoodP(p1));
    if isempty(nptDir(sname)) || Args.redo
        
        C = zeros(length(condID),length(rgoodP) - p1,Args.maxlag*2 + 1);
        
        for ttype = 1 : length(condID)
           for p2 = p1+ 1 : length(rgoodP)
                
                if isempty(Args.trialcut)
                    data1 = squeeze(normA(rgoodP(p1),cgoodP(p1),:,tlist{ttype}));
                    data2 = squeeze(normA(rgoodP(p2),cgoodP(p2),:,tlist{ttype}));
                else
                    data1 = squeeze(normA(rgoodP(p1),cgoodP(p1),Args.trialcut(1):Args.trialcut(2),tlist{ttype}));
                    data2 = squeeze(normA(rgoodP(p2),cgoodP(p2),Args.trialcut(1):Args.trialcut(2),tlist{ttype}));
                end
                data1 = double(data1);
                data2 = double(data2);
                data1 = data1(~isnan(data1));
                 data2 = data2(~isnan(data2));
                 txcor = zeros(Args.maxlag*2 + 1,size(data1,2));
                for tr = 1 : size(data1,2)
                    ldata1 = nptLowPassFilter(data1(:,tr),Args.Fs,Args.reFs,1,Args.lowpass);
                    ldata2 = nptLowPassFilter(data2(:,tr),Args.Fs,Args.reFs,1,Args.lowpass);
                    [r,lags]=xcorr(ldata1,ldata2,Args.maxlag,'coeff');
                   txcor(:,tr) =  r;
                end
                lags = lags / Args.reFs;
               C(ttype,p2-p1,:) = nanmean(txcor,2);
            end
        end
        display(['saving ' sname])
        C = single(C); p1 = single(p1); 
        save(sname,'C','lags','p1','condID','tlist')
    end
end

cd ..
cd ..
% Ex
% [C,phi,~,S1,S2,f]=coherencyc(squeeze(normA(60,62,:)),squeeze(normA(26,37,:)),params);
