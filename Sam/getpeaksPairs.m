function getpeaksPairs(filename,varargin)
% in teh exp01 folder
Args = struct('threshold',0.5,'movingAve',8,'bands',[0 2; 2 5; 5 8; 9 15; 15 25],'lookTrialType',0,'correlations',0,'nofiltering',0,'noappending',0,'notRemoveZeroLag',0);
Args.flags = {'lookTrialType','correlations','nofiltering','noappending','notRemoveZeroLag'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

if ~Args.noappending
    sdir = pwd;
    cd ..
    load pixelComb.mat
    cd(sdir)
    
    d = load(filename);
    if Args.lookTrialType
        if Args.correlations
            coherent = cell(1,size(d.C,1));
            Ccoh = cell(1,size(d.C,1));
        else
            coherent = cell(length(Args.bands),size(d.C,1));
            Ccoh = cell(length(Args.bands),size(d.C,1));
        end
        xmax = cell(size(d.C,1),1);
        allpeaks = cell(size(d.C,1),1);
        allC = cell(size(d.C,1),1);
        for ttype = 1 : size(d.C,1)
            if ~isempty(squeeze(d.C(ttype,:,:)))
                if Args.nofiltering
                    sC = double(squeeze(d.C(ttype,:,:)))';
                    if ~Args.notRemoveZeroLag
                        sC(ceil(size(sC,1)/2),:) =  0;
                    end
                else
                    sC = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,double(squeeze(d.C(ttype,:,:)))');
                end
               
                xmax{ttype} = findpeaks(sC,Args.threshold);
                if Args.correlations
                    allpeaks{ttype} = d.lags(cat(1,xmax{ttype}(:).loc))';
                else
                    allpeaks{ttype} = d.f(cat(1,xmax{ttype}(:).loc));
                end
                allC{ttype} = zeros(length(allpeaks{ttype}),1);
                c = 1;
                for pairs =  1 : length(xmax{ttype})
                    pixel(1,:) = [rgoodP(d.p1) cgoodP(d.p1)];
                    pixel(2,:) = [rgoodP(d.p1 + pairs) cgoodP(d.p1 + pairs)];
                    if Args.correlations
                        peaks = d.lags(xmax{ttype}(pairs).loc);
                      
                    else
                        peaks = d.f(xmax{ttype}(pairs).loc);
                    end
                    if Args.correlations
                        if ~isempty(peaks)
                            coherent{1,ttype} = cat(3,coherent{1,ttype},pixel);
                            
                            Ccoh{1,ttype} = cat(2,Ccoh{1,ttype},vecr(squeeze(d.C(ttype,pairs,xmax{ttype}(pairs).loc))));
                        end
                        
                        
                    else
                        for bb = [1 2 3 4 5]
                            if ~isempty(find(peaks > Args.bands(bb,1) & peaks <= Args.bands(bb,2)))
                                coherent{bb,ttype} = cat(3,coherent{bb,ttype},pixel);
                                speak = find(peaks > Args.bands(bb,1) & peaks <= Args.bands(bb,2));
                                Ccoh{bb,ttype} = cat(2,Ccoh{bb,ttype},vecr(squeeze(d.C(ttype,pairs,xmax{ttype}(pairs).loc(speak)))));
                            end
                            
                        end
                    end
                    allC{ttype}(c: c + length(xmax{ttype}(pairs).loc) - 1) = d.C(ttype,pairs,xmax{ttype}(pairs).loc);
                    c = c + length(xmax{ttype}(pairs).loc);
                end
            end
        end
    else
        coherent = cell(length(Args.bands),1);
        Ccoh = cell(length(Args.bands),1);
        sC = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d.C');
        xmax = findpeaks(sC,Args.threshold);
        allpeaks = d.f(cat(1,xmax(:).loc));
        allC = zeros(length(allpeaks),1);
        c = 1;
        for pairs =  1 : length(xmax)
            pixel(1,:) = [rgoodP(d.p1) cgoodP(d.p1)];
            pixel(2,:) = [rgoodP(d.p1 + pairs) cgoodP(d.p1 + pairs)];
            peaks = d.f(xmax(pairs).loc);
            
            for bb = [1 2 3 4 5]
                if ~isempty(find(peaks > Args.bands(bb,1) & peaks <= Args.bands(bb,2)))
                    coherent{bb} = cat(3,coherent{bb},pixel);
                    speak = find(peaks > Args.bands(bb,1) & peaks <= Args.bands(bb,2));
                    Ccoh{bb} = cat(2,Ccoh{bb},d.C(pairs,xmax(pairs).loc(speak)));
                end
                
            end
            allC(c: c + length(xmax(pairs).loc) - 1) = d.C(pairs,xmax(pairs).loc);
            c = c + length(xmax(pairs).loc);
        end
    end
    save(filename,'xmax','allpeaks','allC','coherent','Ccoh','-append')
    display(['Appending variables to ' filename])
end
