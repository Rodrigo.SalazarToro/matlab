function [ acpairs] = calculateCohMap(basename,varargin)

Args = struct('stim',0,'seedpixel',[],'save',0,'addname',[],'lookTrialType',0,'correlations',0);
Args.flags = {'save','lookTrialType','correlations'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});



if Args.lookTrialType
    if Args.correlations
        acpairs = zeros(4,1,100,100);
        files = nptDir(sprintf('%s%d*cor.mat',basename,Args.stim));
    else
        acpairs = zeros(4,5,100,100);
        files = nptDir(sprintf('%s%d*coh.mat',basename,Args.stim));
    end
else
    acpairs = zeros(5,100,100);
end
for ff = 1 : length(files)
    load(files(ff).name);
    
    if Args.lookTrialType
        for bb =  1: size(coherent,1)
            for ttype = 1 : size(coherent,2)
                if ~isempty(coherent{bb,ttype})
                    tm = zeros(100,100);
                    for pix = 1 : size(coherent{bb,ttype},3)
                        if ~isempty(Args.seedpixel)
                            if sum(ismember(Args.seedpixel,coherent{bb,ttype}(1,:,pix),'rows')) ~=0 || sum(ismember(Args.seedpixel,coherent{bb,ttype}(2,:,pix),'rows')) ~=0
                                
                                tm(squeeze(coherent{bb,ttype}(1,2,pix)),squeeze(coherent{bb,ttype}(1,1,pix))) = 1;
                                tm(squeeze(coherent{bb,ttype}(2,2,pix)),squeeze(coherent{bb,ttype}(2,1,pix))) = 1;
                            end
                        else
                            tm(squeeze(coherent{bb,ttype}(1,2,pix)),squeeze(coherent{bb,ttype}(1,1,pix))) = 1;
                            tm(squeeze(coherent{bb,ttype}(2,2,pix)),squeeze(coherent{bb,ttype}(2,1,pix))) = 1;
                        end
                        
                    end
                    acpairs(ttype,bb,:,:) = squeeze(acpairs(ttype,bb,:,:)) + tm;
                end
            end
        end
        
    else
        for bb =  1: size(coherent,1)
            if ~isempty(coherent{bb})
                tm = zeros(100,100);
                for pix = 1 : size(coherent{bb},3)
                    if ~isempty(Args.seedpixel)
                        if sum(ismember(Args.seedpixel,coherent{bb}(1,:,pix),'rows')) ~=0 || sum(ismember(Args.seedpixel,coherent{bb}(2,:,pix),'rows')) ~=0
                            
                            tm(squeeze(coherent{bb}(1,2,pix)),squeeze(coherent{bb}(1,1,pix))) = 1;
                            tm(squeeze(coherent{bb}(2,2,pix)),squeeze(coherent{bb}(2,1,pix))) = 1;
                        end
                    else
                        tm(squeeze(coherent{bb}(1,2,pix)),squeeze(coherent{bb}(1,1,pix))) = 1;
                        tm(squeeze(coherent{bb}(2,2,pix)),squeeze(coherent{bb}(2,1,pix))) = 1;
                    end
                    
                end
                acpairs(bb,:,:) = squeeze(acpairs(bb,:,:)) + tm;
            end
        end
    end
end
if Args.save
    savename = sprintf('cohMapStim%d%s.mat',Args.stim,Args.addname);
    display([pwd '\' savename])
    save(savename,'acpairs');
    
end