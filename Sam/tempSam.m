
load StimInfo.mat % info about exp with expe{6}.K for the mask
load /new-disk/salazar/matlab/Sam/bv_mask.mat
ntrial = 10;
params =struct('tapers',[5 9],'Fs',500,'trialave',1,'fpass',[0 100]);
normA = nan(100,100,2048,10);
parfor tr = 1 : ntrial
    
    [B,A1,A2,T,bool] = readMicamSam(sprintf('110901s0-3-%d.rsh',1),tr); % cond (0:no stim; 1:stim) - exp# - trial#
    
    actB = B .* repmat(expe{6}.K(:,1:100),[1 1 2048]); % mask contour\
    actB = actB .* repmat(K2,[1 1 2048]); % blood vessel contour\
    
    for r = 1 : 100; for c = 1 : 100; actB(r,c,:) = detrend(squeeze(actB(r,c,:)));end; end % detrend
    normA(:,:,:,tr)  = (actB - repmat(squeeze(mean(actB,3)),[1 1 2048])) ./ repmat(squeeze(std(actB,[],3)),[1 1 2048]); % z-score
    
    
    % clear S;co = 1; for r = 1 : 100; for c = 1 : 100; try [S(co,:),f] = mtspectrumc(squeeze(normA(r,c,:)),params); co =co + 1 ;end;end; end
    
    
    % for ti = 1 : 20148;imagesc(expe{6}.K(:,1:100) .* squeeze(normA(:,:,ti)),[-2 2]); pause(0.1);end
    
    % plot(f,real(S)')
    
    % bands
    
    
    % rS = real(S);
    
    % npt = 10; % fstep = 0.24 Hz
    % for ii = 1 : size(rS,1); sS(ii,:) = filtfilt(repmat(1/npt,1,npt),1,rS(ii,:)); end
    %
    % plot(f,sS(650,:))
    %
    %
    % xmax=findpeaks(sS',0.15*10^-3);
    % acpeaks = [];
    % for ii = 1 : 1000
    %     acpeaks = [acpeaks f(xmax(ii).loc)];
    % end
    %
    % hist(acpeaks,50)
    
end
[rgoodP,cgoodP] = find(expe{6}.K(:,1:100) & K2);
comb = nchoosek([1:length(rgoodP)],2);
parfor cb = 1 : size(comb,1)
    data1 = squeeze(normA(rgoodP(comb(cb,1)),cgoodP(comb(cb,1)),:,:));
    data2 = squeeze(normA(rgoodP(comb(cb,2)),cgoodP(comb(cb,2)),:,:));
    [C(cb,:),phi(cb,:),~,S1(cb,:),S2(cb,:),f]=coherencyc(data1,data2,params);
end



% Ex
% [C,phi,~,S1,S2,f]=coherencyc(squeeze(normA(60,62,:)),squeeze(normA(26,37,:)),params);
