function [cpeak] = plotAllpeaks(exp,varargin)
% in the coherence folder

Args = struct('bins',[0:150]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

 figure
sdir= pwd;
cpeak = cell(length(exp),1);
count = 1;
for e = exp
    cd(['exp0' num2str(e)]);
    files = nptDir('*coh.mat');
    
    for ff =  1 : length(files)
        load(files(ff).name)
        cpeak{count}= cat(2,cpeak{count},allpeaks);
    end
    cd(sdir)
    subplot(1,length(exp),e - exp(1) + 1)
    
    hist(cpeak{count},Args.bins)
    count = count + 1;
end
   
    
    
    
