function out=readMiCamrsh(FName)
out=[];
fid=fopen(FName);
i=1;
while(feof(fid)==0)
    txt(i)=cellstr(fgetl(fid));
    i=1+i;
end
fclose(fid);

assignin('base','txt',txt);
% Get stim mode (0 is no blank division, 1 is with blank division)
a=char(txt(22)); out.stimmode=str2num(a(end));
% Get frame number
a=char(txt(4)); out.framenumb=str2num(a(13:end));
% Get average number
a=char(txt(10)); out.av=str2num(a(9:end));
% Number of files
out.NFile=out.framenumb/256;
% Dual mode
a=char(txt(25)); out.dual=str2num(a(10:end));
% Number of conditions
if(out.dual==3)
    out.ConditionCount=2;
    out.framenumb=out.framenumb/2;
else
    out.ConditionCount=1;
end
% File names collection
out.FileNames=txt(end-out.NFile+1:end);
% Time per frame
a=char(txt(6));
b1=findstr(a,'=');
b2=findstr(a,'msec');
out.TperF=str2num(a(b1+1:b2-1));