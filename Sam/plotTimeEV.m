function plotTimeEV(exp,varargin)

Args = struct('time',[200 450],'pause',0.5,'caxis',[-2.5 2.5],'stim',[],'nostim',[],'data',[],'manystim',0);
Args.flags = {'manystim'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

if isempty(Args.stim) && isempty(Args.nostim) && ~Args.manystim
    stim = load(sprintf('110901s1-%d-norm.mat',exp));
    nostim = load(sprintf('110901s0-%d-norm.mat',exp));
elseif Args.manystim
    if isempty(Args.data)
    data = load(sprintf('110901s0-%d-norm.mat',exp));
    else
       data.normA = Args.data;
       Args.data = [];
    end
else
    stim.normA = Args.stim;
    nostim.normA = Args.nostim;
end

figure
if Args.manystim
   load StimInfo.mat
    ind =findExp(exp);
    trials = expe{ind}.condR;
    for ti = Args.time(1): Args.time(2);
        for c = 1 : length(expe{ind}.condIDl)
           
             strial = find(expe{ind}.condIDl(c) == trials);
           
            subplot(2, ceil(length(expe{ind}.condIDl)/2),c)
           
            imagesc(squeeze(mean(data.normA(:,:,ti,strial),4)),Args.caxis);
            title(sprintf('%s %g sec',expe{ind}.condIDl(c),ti*0.002));
        end
        pause(Args.pause);
    end
else
    for ti = Args.time(1): Args.time(2);
        subplot(1,2,1)
        imagesc(squeeze(mean(stim.normA(:,:,ti,:),4)),Args.caxis);
        title(sprintf('With stimulus; time %g sec',ti*0.002));
        
        subplot(1,2,2)
        imagesc(squeeze(mean(nostim.normA(:,:,ti,:),4)),Args.caxis);
        title(sprintf('No stimulus; time %g sec',ti*0.002));
        pause(Args.pause);
    end
end