cd F:\salazar\data\sam
bands = {'delta' 'theta' 'heart beat' 'beta'};
lims = [10^-3 2*10^-4 10^-3  10^-4];
for bb = 1 : 4
    figure
    set(gcf,'Name',bands{bb})
    for exp = 3 : 5
        for stim = 0 : 1
            subplot(2,3,(stim)*3 + exp -2)
            [mpow,peakmap]=makepowermap(sprintf('110901s%d-%d-power.mat',stim,exp));
            
            imagesc(squeeze(mpow(:,:,bb)) .* squeeze(peakmap(:,:,bb)),[0 lims(bb)]);
            title(['exp0' num2str(exp)])
            colorbar
        end
    end
    xlabel('medial-lateral')
    ylabel('Anterior-posterior')
    subplot(2,3,1); ylabel('no stim')
    subplot(2,3,4); ylabel('with stim')
end