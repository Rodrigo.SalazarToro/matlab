function make_descriptor(basename,numb_channels,varargin)

Args = struct('tetrode',0);
Args.flags = {'tetrode'};
[Args] = getOptArgs(varargin,Args,'remove',{});




fid = fopen([basename '_descriptor.txt'],'w+');

%print header
fprintf(fid,'%s\n','Preprocessed Raw Data');
fprintf(fid,'%s%d\n','Number of Channels ',numb_channels);
fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Channel Number','Group Number','Grid Number','Signal Type','RF Number','Start Depth','Rec Depth','State');

%print channel information
for ch = 1 : numb_channels
    if Args.tetrode
        
    else
        fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',ch,ch,ch,'broadband',-1,0,1,'Active');
        
    end
    %
    %         if channel < 257
    %             fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,channel,channel,'broadband',-1,0,depths(channel),'Active');
    %         else
    %             if channel == 257
    %                 fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'trigger',-1,0,0,'Inactive');
    %             elseif channel == 258
    %                 fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'vertical1',-1,0,0,'Active');
    %             elseif channel == 259
    %                 fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'horizontal1',-1,0,0,'Active');
    %             else
    %
    %             end
    %         end
end

fclose(fid);
