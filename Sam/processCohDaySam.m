function processCohDaySam(basename,exp,varargin)

Args = struct('redo',0,'Fs',500,'onlyPlot',0,'lookTrialType',0,'onlyCohMap',0);
Args.flags = {'redo','onlyPlot','lookTrialType','onlyCohMap'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


sdir = pwd;
mkdir('coherence')
cd coherence
if ~Args.onlyPlot
    if Args.lookTrialType
        cb = cat(2,zeros(length(exp),1),exp');
    else
        cb = cat(2,cat(1,zeros(length(exp),1),ones(length(exp),1)),cat(1,exp',exp'));
    end
    if ~Args.onlyCohMap
        for e = exp
            
            mkdir([sdir filesep 'coherence' filesep 'exp0' num2str(e)]);
            cd ..
        end
        cd(sdir)
        
        % cb = [0 1; 0 2; 0 3; 0 4; 0 5; 0 6; 1 1; 1 2; 1 3; 1 4; 1 5; 1 6];
        
        parfor cc = 1 : size(cb,1); makecoherenceSam(basename,cb(cc,1),cb(cc,2),'Fs',Args.Fs,modvarargin{:}); end
        
        cd([sdir filesep 'coherence'])
        
        for e = exp
            
            cd(['exp0' num2str(e)])
            files = nptDir('*coh.mat');
            parfor ff = 1 : length(files)
                %             try
                getpeaksPairs(files(ff).name,modvarargin{:})
                %             catch
                %                 display(['Bad file ' files(ff).name])
                %
                %             end
                
            end
            cd ..
        end
    end
    
    %     seed = load([sdir filesep 'seedS1.mat']);
    parfor cond = 1:size(cb,1)
        cd([sdir '\coherence\exp0' num2str(cb(cond,2))])
        calculateCohMap(basename,'stim',(cb(cond,1)),'save',modvarargin{:});
        %         calculateCohMap(basename,'stim',(cb(cond,1)),'seedpixel',seed.S1,'save','addName','seedS1');
        cd ..
    end
end

%% plotting
cd([sdir filesep 'coherence'])
bands = {'LF' 'delta' 'theta' 'heart beat' 'beta'};
explim = [4000 4000 100 4500 800];
for bb = [1 2 3 4 5]
    figure
    set(gcf,'Name',bands{bb})
    c = 1; 
    for e = exp
        if Args.lookTrialType
            stims = 0;
            cd(['exp0' num2str(e)])
            load(sprintf('cohMapStim%d.mat',stims))
            for tt = 1 : size(acpairs,1)
                
                
                subplot(size(acpairs,1),length(exp),(tt - 1)* length(exp) +c)
                imagesc((squeeze(acpairs(tt,bb,:,:)))',[0 explim(bb)])
                title(['exp0' num2str(e) ])
                colorbar
                xlim([1 100])
                ylim([1 100])
                
            end
           c= c+1;
            cd ..
        else
            stims = 0:1;
            
            for stim = stims
                cd(['exp0' num2str(e)])
                load(sprintf('cohMapStim%d.mat',stim))
                subplot(length(stims),length(exp),(stim )* length(exp) + e - exp(1) + 1)
                imagesc((squeeze(acpairs(bb,:,:)))',[0 explim(bb)])
                title(['exp0' num2str(e)])
                colorbar
                xlim([1 100])
                ylim([1 100])
                cd ..
            end
            subplot(length(stims),length(exp),1); ylabel('no stim')
            subplot(length(stims),length(exp),length(exp)+1); ylabel('with stim')
        end
          xlabel('medial-lateral')
    ylabel('Anterior-posterior')
    end
  
    
end


% cd([sdir filesep 'coherence'])
% explim = [100 100 50 200 100];
% for bb = [1 2 3 4 5]
%     figure
%     set(gcf,'Name',bands{bb})
%     for e = exp
%         for stim = 0 : 1
%             cd(['exp0' num2str(e)])
%             load(sprintf('cohMapStim%dseedS1.mat',stim))
%             subplot(2,length(exp),(stim )* length(exp) + e - exp(1) + 1)
%             imagesc((squeeze(acpairs(bb,:,:)))',[0 explim(bb)])
%             title(['exp0' num2str(e)])
%             colorbar
%             xlim([1 100])
%             ylim([1 100])
%             cd ..
%         end
%     end
%     xlabel('medial-lateral')
%     ylabel('Anterior-posterior')
%     subplot(2,length(exp),1); ylabel('no stim')
%     subplot(2,length(exp),length(exp)+1); ylabel('with stim')
% end
% cd ..
