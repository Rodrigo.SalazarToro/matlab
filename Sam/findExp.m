function N=findExp(nb)
% nb is the number of the experiment
% N is the cell number corresponding to the first stimulation modality in
% the variable 'expe'

load('StimInfo.mat');
tip=0;
N=0;

for i=1:size(expe,2)
    if isfield(expe{i},'name')
        if ~isempty(findstr(expe{i}.name,'-'))
            tp=findstr(expe{i}.name,'-');
            if strcmp(expe{i}.name((tp(1)+1):(tp(2)-1)),num2str(nb)) && tip==0
                tip=1;
                N=i;
            end;
        else
            tp=findstr(expe{i}.name,'i');
            if strcmp(expe{i}.name((tp+1):end),num2str(nb)) && tip==0
                tip=1;
                N=i;
            end;
        end;
    end;
    if isfield(expe{i},'fileName')
        tp=findstr(expe{i}.fileName,'i');
        if strcmp(expe{i}.fileName((tp+1):end),num2str(nb)) && tip==0
            tip=1;
            N=i;
        end;
    end;
end;
