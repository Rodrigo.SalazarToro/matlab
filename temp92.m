cd /Volumes/raid/data/monkey/
clear all
LOCobj = loadObject('psthLOCswitchObj.mat');
IDEobj = loadObject('psthIDEswitchObj.mat');

[~,LOCind] = get(LOCobj,'number','hist',[4 11 13]);

[~,IDEind] = get(IDEobj,'number','hist',[4 11 13]);
ttind = find(ismember(LOCobj.data.setNames(LOCind),IDEobj.data.setNames(IDEind),'rows') == 1);
tttind = find(ismember(IDEobj.data.setNames(IDEind),LOCobj.data.setNames(LOCind),'rows') == 1);
LOCind = LOCind(ttind);
IDEind = IDEind(tttind);
miss = 0;
MIide = nan(2,length(LOCind), 30);
MIloc = nan(2,length(LOCind), 30);
MIideSig = nan(2,length(LOCind), 30);
MIlocSig = nan(2,length(LOCind), 30);
match = 'MatchAlign';
time = [-1750:100:1150];
parfor ii = 1 : length(LOCind)
    cd(LOCobj.data.setNames{LOCind(ii)})
    for r = 1 : 2
        
        ide = load(sprintf('PSTHIde%srule%d.mat',match,r));
        loc = load(sprintf('PSTHLoc%srule%d.mat',match,r));
        
        MIide(r,ii,:) = ide.mi(end,:) - getExpValue(ide.mi(1:end-1,:),[1:size(ide.mi,2)]);
        MIloc(r,ii,:) = loc.mi(end,:) - getExpValue(loc.mi(1:end-1,:),[1:size(loc.mi,2)]);
        MIideSig(r,ii,:) = ide.mi(end,:) > prctile(ide.mi(1:end-1,:),99.9);
        MIlocSig(r,ii,:) = loc.mi(end,:)  > prctile(loc.mi(1:end-1,:),99.9);
        
    end
end
figure
rules = {'IDE' 'LOC'};
la = {'b' 'r'};
for r = 1 : 2;
    subplot(2,2,r);
    boxplot(squeeze(MIide(r,:,:)))
    %   title('ide MI')
    ylim([-0.05 0.20])
    subplot(2,2,2+r);
    boxplot(squeeze(MIloc(r,:,:)))
    ylim([-0.05 0.45])
end



figure
subplot(2,1,1);
boxplot(squeeze(MIide(1,:,:)) - squeeze(MIide(2,:,:)))
for tbin = 1 : 30; p1(tbin) = signtest(squeeze(MIide(1,:,tbin)) - squeeze(MIide(2,:,tbin))); end
%   title('ide MI')
title('ide MI')
ylim([-0.3 0.30])
ylabel('IDE - LOC')
subplot(2,1,2);
boxplot(squeeze(MIloc(1,:,:)) - squeeze(MIloc(2,:,:)))
for tbin = 1 : 30; p2(tbin) = signtest(squeeze(MIloc(1,:,tbin)) - squeeze(MIloc(2,:,tbin))); end
ylim([-0.3 0.30])
title('loc MI')
ylabel('IDE - LOC')

figure
subplot(2,1,1);
plot(time,prctile(squeeze(MIide(1,:,:)),[25 50 75]),'b')
hold on
plot(time,prctile(squeeze(MIide(2,:,:)),[25 50 75]),'r')

%   title('ide MI')
title('ide MI')

subplot(2,1,2);
plot(time,prctile(squeeze(MIloc(1,:,:)),[25 50 75]),'b')
hold on
plot(time,prctile(squeeze(MIloc(2,:,:)),[25 50 75]),'r')

title('loc MI')
legend('IDE','LOC')
xlabel('Time from Match (ms)')
ylabel('MI')
figure
rules = {'IDE' 'LOC'};
la = {'b' 'r'};
for r = 1 : 2;
    subplot(2,1,1);
    plot(time,nansum(squeeze(MIideSig(r,:,:))),la{r})
    hold on
    title('ide MI')
    ylim([0 8])
    subplot(2,1,2);
    plot(time,nansum(squeeze(MIlocSig(r,:,:))),la{r})
    hold on
    title('loc MI')
    ylim([0 40])
end
legend('IDE','LOC')

%%

% dirs = {'/Volumes/raid/data/monkey/clark' '/Volumes/raid/data/monkey/betty' '/Volumes/raid/data/monkey/betty/acute'};
%
% for dir = 1 : 3
%     cd(dirs{dir})
%     dd = load('switchdays.mat');
%     fname = fieldnames(dd);
%     if strcmp('days',fname{1})
%         days = dd.days;
%     else
%         days = dd.switchdays;
%     end
%     parfor d = 1 : length(days)
%         cd([dirs{dir} '/' days{d}])
%
%         psthSurDay('save','rule',1,'session99')
%         psthSurDay('save','rule',2,'session99')
%
%     end
% end





