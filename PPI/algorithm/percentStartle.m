function [PCstartle,varargout] = percentStartle(startle,infoTrial,defaultParams,chamberid,blockid,varargin)
% Calculate the % startle for different PP intensities
%
%
Args = struct('block',2,'STdB',120,'LightTrials',[],'SN',3,'excludePPwithStartle',0);
Args.flags = {'excludePPwithStartle'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

raw = find(strcmp('Prepulse Level',defaultParams) ==1);
alldB = unique(infoTrial(raw,:));

PPAmp = cell(length(alldB) - 1,1);
noPPAmp = cell(length(alldB) - 1,1);

if isempty(Args.LightTrials)
    nchamber = max(chamberid);
    
else
    nchamber = 2 * max(chamberid);
    rch =  max(chamberid);
end
PCstartle = zeros(length(alldB) - 1,nchamber,1);
STraw = find(strcmp('Startle Level',defaultParams) ==1);

startleTrial = find(infoTrial(STraw,:)  == Args.STdB);
blockTrial = find(blockid == Args.block);
for dB = 2 : length(alldB)
    for ch = 1 :nchamber
        if ch <= (nchamber/2) || isempty(Args.LightTrials)
            PPtrial = intersect(intersect(find(infoTrial(raw,:) == alldB(dB)),find(chamberid == ch)),blockTrial);% PPi trials
            noPPtrial = intersect(intersect(find(infoTrial(raw,:) == 0),find(chamberid == ch)),blockTrial);% startle ony trial
            
            PPtrial = intersect(PPtrial,startleTrial);
            noPPtrial = intersect(noPPtrial,startleTrial);
            
            PPtrial = setdiff(PPtrial,Args.LightTrials);
            noPPtrial = setdiff(noPPtrial,Args.LightTrials);
            % noPPtrial = setdiff(noPPtrial,Args.LightTrials); % when
            % pulse only trials will get light too
        elseif ch > (nchamber/2) && ~isempty(Args.LightTrials)
            therch = ch-rch;
            PPtrial = intersect(intersect(find(infoTrial(raw,:) == alldB(dB)),find(chamberid == therch)),blockTrial);% PPi trials
            noPPtrial = intersect(intersect(find(infoTrial(raw,:) == 0),find(chamberid == therch)),blockTrial);% startle ony trial
            
            PPtrial = intersect(PPtrial,startleTrial);
            noPPtrial = intersect(noPPtrial,startleTrial);
            
            PPtrial = intersect(PPtrial,Args.LightTrials);
            noPPtrial = intersect(noPPtrial,Args.LightTrials);
        end
        
        if isempty( PPtrial) || isempty(noPPtrial)
            PCstartle(dB-1,ch) = nan;
        else
            %% temporary code to fix a problem with unequal number of trial for light and no light conditions
            if length(PPtrial) == 13; PPtrial(end) = [];end
            noiseLevel =  median(max(startle.Null(PPtrial,:),[],2) + abs(min(startle.Null(PPtrial,:),[],2)));
            PPreactivity = max(startle.Prepulse(PPtrial,:),[],2) + abs(min(startle.Prepulse(PPtrial,:),[],2));
            
            PPreactif = PPreactivity > (Args.SN * noiseLevel);
            PPAmp{dB - 1}(ch,:) = nanmax(startle.Startle(PPtrial,:),[],2) + abs(nanmin(startle.Startle(PPtrial,:),[],2));
            if Args.excludePPwithStartle;PPAmp{dB - 1}(ch,PPreactif) = nan;end
            noPPAmp{dB -1 }(ch,:) = max(startle.Startle(noPPtrial,:),[],2) + abs(min(startle.Startle(noPPtrial,:),[],2));
            
            PCstartle(dB-1,ch) = 100 - 100 * (nanmean(PPAmp{dB -1 }(ch,:),2) ./ nanmean(noPPAmp{dB -1 }(ch,:),2));
        end
    end
    
end
varargout{1} = PPAmp;
varargout{2} = noPPAmp;
varargout{3} = alldB(2:end);
