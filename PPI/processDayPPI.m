function ppi = processDayPPI(varargin)
% to be run in the day directory

Args = struct('save',0,'redo',0,'plot',0,'sdB',[75 80 85 90]);
Args.flags = {'save','redo','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

sdir = pwd; % sdir prend la position de d�part dans les dossiers (acc�s � toutes les sessions d'une journ�e)
sdB = Args.sdB;
[~,basename,~]=fileparts(sdir); % fileparts renvoie normalement le chemin,
%le nom du dossier et son extension, ici on ne s'int�resse qu'au nom du dossier
savename = sprintf('%sprctPPI.mat',basename); % nom de sauvegarde

% Si la fonction n'a jamais �t� lanc�e, ou si on souhaite relancer la manip :
if Args.redo || isempty(nptDir(savename))
    sessions = nptDir('session*'); % sessions prend toutes les sessions du dossier
    ppi = [];
    for ss = 1 : length(sessions)
        cd(sessions(ss).name) % rentre dans le dossier de la session ss
        
        pct = processSessionPPI(modvarargin{:});
        ppi = cat(2,ppi,pct); %colle toutes les matrices contenant les ppi de chaque session
        
        cd(sdir) % revient dans le dossier principal
    end
    if Args.save
        save(savename,'ppi')
        display(['saving ' savename])
    end
else
    load(savename)
end

if Args.plot
    minimum= min(min(ppi));
    maximum= max(max(ppi));
    
    figure1 = figure;plot(a,ppi);xlabel('Prepulse intensities');ylabel('%PPI');ylim([minimum maximum]);title('%PPI as a function of the prepulse intensity')
    figure2 = figure;boxplot(ppi');xlabel('Prepulse intensities');ylabel('%PPI');title('%PPI for each prepulse intensity')
    figure3 = figure;plot(ppi','.');xlabel('Mice'),ylabel('%PPI');legend(num2str(sdB(:)));title('%PPI for each mice and each intensity')
    saveas(figure1,'PPICh1&2.fig')
    saveas(figure2,'PPIboxplotCh1&2.fig')
    saveas(figure3,'MiceVariabilityCh1&2.fig')
    %m�mes figures chambres s�par�es
    
    figure4 = figure;
    for ii = 1:2
        Ch=ii:2:length(ppi);
        ppiCh=ppi(:,Ch);
        subplot(2,1,ii);plot(a,ppiCh);xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([minimum maximum]);title(sprintf('Chamber %d',ii))
    end
    saveas(figure4,'PPICh1&2separated.fig')
    
    figure5 =figure;
    for ii = 1:2
        Ch=ii:2:length(ppi);
        ppiCh=ppi(:,Ch);
        subplot(2,1,ii);boxplot(ppiCh');xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([minimum maximum]);title(sprintf('Chamber %d',ii))
    end
    saveas(figure5,'PPIboxplotCh1&2separated.fig')
    
    figure6= figure;
    for ii = 1:2
        Ch=ii:2:length(ppi);
        ppiCh=ppi(:,Ch);
        subplot(2,1,ii);plot(ppiCh','.');xlabel('Mice');ylabel('%PPI');ylim([minimum maximum]);title(sprintf('Chamber %d',ii));legend('75','80','85','90')
    end
    saveas(figure6,'MiceVariabilityCh1&2separated.fig')
end