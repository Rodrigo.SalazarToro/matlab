dbstop if error;
labels = {'GFP no light' 'GFP light' 'ChR2 no light' 'ChR2 light'};
processDaysPPI({'20160526'},'20160526STD3',labels,'withLight','sdB',[74 78 82 86],'fromfile','infoTrial.mat','redo','PPperiodAlso','medianStartlefold',3)
load('20160526STD3combinedDataPPI.mat')
plotDaysPPI(ppi,doses,miceID,startleAmp,nullPeriodAmp,labels,sdB,'withLight','savefig','20160526STD4','redo')

