function [infoTrial,defaultParams,varargout] = ReadInfoPPItxtFile(filename,varargin)
% reads info from raw .TAB file saved from Startle Reflex software
% output:
%
% infoTrial: parameters x items
%
% defaultParams: parameters names
Args = struct('arduino',0,'fromfile',[]);
Args.flags = {'arduino'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


defaultParams = {'Trial #' 'Null Period' 'Prepulse Duration' 'Prepulse Level' 'Prepulse Rise/Fall' 'Prepulse Frequency' 'Prepulse aux port' 'Prepulse/Startle Delay' 'Startle Duration' 'Startle Level' 'Startle Rise/Fall' 'Startle Frequency' 'Startle aux port'};


if isempty(Args.fromfile)
    fid = fopen(filename,'r');
    a=textscan(fid,'%s');
    a = a{1};
    
    if Args.arduino
        % in procgress
        conds = [74 120; 78 120; 82 120; 86 120; 74 0; 78 0; 82 0;86 0; 0 120; 0 0];
        
        ind=strmatch('Trial',B);aa
    else
        
        
        
        %% eliminate useless lines or confusing text
        
        eliminate = find(strcmp('--------------------------------------------------------------------------',a) == 1);
        a(eliminate) = [];
        eliminate = find(strcmp('Chamber#',a) == 1) + 1;
        a(eliminate) = [];
        eliminate = find(strcmp('Chamber#',a) == 1);
        a(eliminate) = [];
        
        %%
        
        starts = find(strcmp('Delay',a) == 1) + 1;
        ends = [find(strcmp('Block#',a) == 1) - 1; length(a)];
        ends(1) = [];
        
        params = [];
        for xi = 1 : length(starts)
            params = cat(1,params,str2double(a(starts(xi):ends(xi))));
        end
        
        infoTrial = reshape(params,length(defaultParams),length(params)/length(defaultParams));
        
    end
    
else
    load(Args.fromfile)
end