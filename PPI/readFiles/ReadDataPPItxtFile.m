function [startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(filename,varargin)
% reads startle data from raw .txt file saved from Startle Reflex software
%
% Output:
%
% startle: structure with 'Null', 'Prepulse' and 'Startle' fields. Each
% with a item x time format. Item = Nbr of chambers fold number of trials
%
% chamberid: chamber number for each item
%
% blockid: block number for each item
%
% trialid: trial number for each item

% Args = struct('fromfile',[]);
% Args.flags = {};
% [Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

fid = fopen(filename,'r');
a=textscan(fid,'%s');
a = a{1};


all = find(strcmp('CHAMBER:',a) == 1);
chamberid = str2double(a(all + 1));

all = find(strcmp('BLOCK:',a) == 1);
blockid = str2double(a(all + 1));

all = find(strcmp('TRIAL:',a) == 1);
trialid = str2double(a(all + 1));

all = find(strcmp('Data:',a) == 1);
ntrial = length(all) / 3;

prepulse = find(strcmp('Prepulse',a) == 1);

startle = find(strcmp('Startle',a) == 1);

title = find(strcmp('TITLE:',a) == 1);

%% initialisation

nbin = nan(3,1);

for tr = 1 : 3
    switch a{all(tr) - 3}
        
        case 'Null'
            temp = prepulse(find(prepulse > all(tr) + 1)) - all(tr);
            nbin(1) = temp(1) - 1;
        case 'Prepulse'
            temp = startle(find(startle > all(tr) + 1)) - all(tr);
            nbin(2) = temp(1) - 1;
        case 'Startle'
            temp = title(find(title > all(tr) + 1)) - all(tr);
            nbin(3) = temp(1) - 1;
    end
end
%%

startle = struct('Null',nan(ntrial,nbin(1)),'Prepulse',nan(ntrial,nbin(2)),'Startle',nan(ntrial,nbin(3)));
count = ones(3,1);
for tr = 1 : length(all)
    switch a{all(tr) - 3}
        
        case 'Null'
            startle.Null(count(1),:) = str2double(a(all(tr) + 1 : all(tr) + nbin(1)));
            count(1) = count(1) + 1;
        case 'Prepulse'
            startle.Prepulse(count(2),:) = str2double(a(all(tr) + 1 : all(tr) + nbin(2)));
            count(2) = count(2) + 1;
        case 'Startle'
            startle.Startle(count(3),:) = str2double(a(all(tr) + 1 : all(tr) + nbin(3)));
            count(3) = count(3) + 1;
    end
    
end

% if ~isempty(Args.fromfile); load(Args.fromfile); end

