function plotDaysPPI(ppi,doses,miceID,startleAmp,nullPeriodAmp,labels,sdB,varargin)


Args = struct('nostats', 0,'withLight',0,'savefig',[],'individualDiff',0);
Args.flags = {'nostats','withLight','individualDiff'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

thedoses = unique(doses);
f1= figure;% plot the SEM
labs = {'r' 'k' 'g' 'm' 'c' 'b' 'r--' 'k--' 'g--' 'm--' 'c--' 'b--'};

if ~Args.nostats
    inten = repmat(sdB, length(ppi(:,1)), 1);
    inten = inten(:);
    dose1 = repmat(doses, size(ppi,2), 1);
    
    [p, ~, ~] = anovan(reshape(ppi,[size(ppi,1)*size(ppi,2) 1]), {dose1, inten}, 'model', 'full','display','off');
    % multcompare(stats);
end
minvalue = 1.1*min(min(ppi));
maxvalue = 1.1*max(max(ppi));
for do = 1 : length(thedoses)
    sind = doses == thedoses(do);
    subplot(length(thedoses) + 1,1,1)
    errorbar(mean(ppi(sind,:),1),std(ppi(sind,:),[],1)/sqrt(sum(sind)),labs{do})
    hold on
    xlim([0.5 length(sdB) + .5])
    set(gca,'XTick',[1 : length(sdB)])
    set(gca,'XTickLabel',sdB)
    %ylim([minvalue maxvalue])
    ylim([0 90])
end
title(sprintf('p=%f for treatment, p=%f for PP and p=%f for interaction',p(1),p(2),p(3)));
legend(labels)

for do = 1 : length(thedoses)
    sind = doses == thedoses(do);
    subplot(length(thedoses) + 1,1,1+do)
    plot(ppi(sind,:)')
    xlim([0.5 length(sdB) + .5])
    set(gca,'XTick',[1 : length(sdB)])
    title(labels{do})
    legend(miceID(sind))
    ylim([minvalue maxvalue])
end
set(gca,'XTick',[1 : length(sdB)])
set(gca,'XTickLabel',sdB)
xlabel('Prepulse intensities (dB)')
ylabel('% PPI')

if ~isempty(Args.savefig); saveas(f1,[Args.savefig '.fig']); end

f2=figure; %plot Amplitude
if Args.withLight; thedoses = thedoses(1: length(thedoses)/2); end
for do = 1 : length(thedoses)
    subplot(length(thedoses),1,do)
    sind = doses(1: size(startleAmp,1)) == thedoses(do);
    
    plot(startleAmp(sind,:),['.' labs{do}])
    xlim([0.5 sum(sind) + 0.5])
    set(gca,'XTick',[1:sum(sind)])
    set(gca,'XTickLabel',miceID(sind))
    title(labels{do})
    ylim([min(min(startleAmp)) max(max(startleAmp))])
end
xlabel('mice ID')

ylabel('Startle ampl.')
set(gcf,'Name','Startle after pulse')
if ~isempty(Args.savefig); saveas(f2,[Args.savefig 'startle.fig']); end
figure; %plot Amplitude

for do = 1 : length(thedoses)
    subplot(length(thedoses),1,do)
    sind = doses == thedoses(do);
    
    plot(nullPeriodAmp(sind,:),['.' labs{do}])
    xlim([0.5 sum(sind) + 0.5])
    set(gca,'XTick',[1:sum(sind)])
    set(gca,'XTickLabel',miceID(sind))
    title(labels{do})
    ylim([min(min(nullPeriodAmp)) max(max(nullPeriodAmp))])
end
xlabel('mice ID')

ylabel('Startle ampl.')
set(gcf,'Name','Startle during null period')
if ~isempty(Args.savefig); saveas(f2,[Args.savefig 'startleNull.fig']); end

if Args.individualDiff
    
    % careful tis code will always make a difference between the highest
    % label and the lowest label. Thus it is important to keep the order
    % between the treatment
    
    uniquemice = unique(miceID);
    diffppi = nan(length(uniquemice),size(ppi,2));
    for mm = 1 : length(uniquemice)
        sm = find(ismember(miceID,uniquemice{mm}) == 1);
        [~,ix] = sort(doses(sm));
        
        alldiffType{mm} = [labels{doses(sm(ix(2))) + 1} ' ' labels{doses(sm(ix(1))) + 1}];
        diffppi(mm,:) = diff(ppi(sm(ix),:));
        
    end
    
    diffType = unique(alldiffType);
    for dt = 1 : length(diffType)
        smice = ismember(alldiffType,diffType{dt});
        figure
        for sb = 1 : size(ppi, 2)
            subplot(size(ppi, 2),1,sb)
            b= histc(diffppi(smice,sb),[-50:5:50]);
            bar([-50:5:50],b);
            xlabel('differential %ppi')
            ylabel('# mice')
            xlim([-50 50])
            title(sprintf('Prepulse %d',sdB(sb)))
        end
        set(gcf,'Name',diffType{dt})
    end
end
