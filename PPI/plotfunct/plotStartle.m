function [f1,fch] = plotStartle(startle,infoTrial,defaultParams,chamberid,blockid,varargin)
% plot startle traces for quality control
%
Args = struct('block',2);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


limit = max(max(abs([startle.Null startle.Prepulse startle.Startle])));

%% plot the 3 periods
periods = {'Null' 'Prepulse' 'Startle'};
f1 = figure;
set(gcf,'Name','Startles for the diferent periods');
for ch = 1 : max(chamberid)
     ind = intersect(find(chamberid == ch),find(blockid == Args.block));
    subplot(2,3,(ch-1)*3 + 1); plot(startle.Null(ind,:)')
    subplot(2,3,(ch-1)*3 +2); plot(startle.Prepulse(ind,:)')
    subplot(2,3,(ch-1)*3 +3); plot(startle.Startle(ind,:)')
end
for sb = 1 : 6; subplot(2,3,sb);ylim([-limit limit]); if sb < 4; title(periods{sb});end;end

xlabel('Time (ms)')
ylabel('startle (arb. unit)')
subplot(2,3,1); ylabel('Chamber 1')
subplot(2,3,4); ylabel('Chamber 2')
%% plot the different intensities of prepulse

raw = find(strcmp('Prepulse Level',defaultParams) ==1);
for ch = 1 : max(chamberid)
    fch(ch) = figure;
    set(gcf,'Name',sprintf('Different prepulse intensities for Chamber #%d',ch)); % sprintf permet de rajouter le chiffre de la chambre utilis�e dans
    %la chaine de caract�re %
    c=1;
    alldB = unique(infoTrial(raw,:));
    for int = alldB
        
        ind = intersect(intersect(find(infoTrial(raw,:) == int),find(chamberid == ch)),find(blockid == Args.block));
        subplot(length(alldB),3,(c-1)*3 + 1)
        plot(startle.Null(ind,:)')
         ylim([-limit limit])
         subplot(length(alldB),3,(c-1)*3 + 2)
        plot(startle.Prepulse(ind,:)')
         ylim([-limit limit])
         subplot(length(alldB),3,(c-1)*3 + 3)
        plot(startle.Startle(ind,:)')
        
        ylim([-limit limit])
        title(sprintf('prepulse at %d dB',int))
        c=c+1;
    end
    xlabel('Time (ms)')
    ylabel('startle (arb. unit)')
end