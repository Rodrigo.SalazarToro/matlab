function [noPPAmpStartle,noPPAmpNull,varargout] = getTrialsPPI(startle,infoTrial,defaultParams,chamberid,blockid,varargin)
% Calculate the amplitude of the startle for the startle trials only,
% compare to the null period
% noPPAmpStartle gives the startle amplitude for each trial
%
Args = struct('block',2,'STdB',120, 'onlyPP', 0, 'dosereponse', 0,'PPperiodAlso',0);
Args.flags = {'onlyPP', 'dosereponse','PPperiodAlso'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});



STraw = find(strcmp('Startle Level',defaultParams) ==1); % Prend la position de la ligne ou se trouve l'indication de l'intensite du ST (0 ou 120)

raw = find(strcmp('Prepulse Level',defaultParams) ==1);

blockTrial = find(blockid == Args.block); % On ne s'intéresse qu'aux essais du bloc II

intensities2 = infoTrial(raw,intersect(find(chamberid == 1),intersect(find(infoTrial(raw,:)), blockTrial))); %WROOONG creates list of all prepulse intensities includings 0s (block II)
varargout{1} = intensities2;

if Args.dosereponse
    intensities = infoTrial(STraw,intersect(find(chamberid == 1),intersect(find(infoTrial(STraw,:) ~= 0), blockTrial)));
    varargout{2} = intensities;
    startleTrial = find(infoTrial(STraw,:)  ~= 0); % Trouve les essais ou il y a un ST
    for ch = 1 : max(chamberid)
        noPPtrial = intersect(intersect(startleTrial,find(chamberid == ch)),blockTrial);
        if Args.PPperiodAlso
           noPPAmpStartle(ch,:) = max([startle.Startle(noPPtrial,:) startle.Prepulse(noPPtrial,:)],[],2) + abs(min([startle.Startle(noPPtrial,:) startle.Prepulse(noPPtrial,:)],[],2));
         
        else
        noPPAmpStartle(ch,:) = max(startle.Startle(noPPtrial,:),[],2) + abs(min(startle.Startle(noPPtrial,:),[],2));
        end
        noPPAmpNull(ch,:) = max(startle.Null(noPPtrial,:),[],2) + abs(min(startle.Null(noPPtrial,:),[],2));
    end
    
    
else
   
    startleTrial = find(infoTrial(STraw,:)  == Args.STdB); % Trouve les essais ou il y a un ST
    for ch = 1 : max(chamberid)
        noPPtrial = intersect(intersect(find(infoTrial(raw,:) == 0),find(chamberid == ch)),blockTrial);% gives trials with no prepulse, in both chambers
        noPPtrial = intersect(noPPtrial,startleTrial);
        if Args.PPperiodAlso
            noPPAmpStartle(ch,:) = max([startle.Startle(noPPtrial,:) startle.Prepulse(noPPtrial,:)],[],2) + abs(min([startle.Startle(noPPtrial,:) startle.Prepulse(noPPtrial,:)],[],2));
        
        else
        noPPAmpStartle(ch,:) = max(startle.Startle(noPPtrial,:),[],2) + abs(min(startle.Startle(noPPtrial,:),[],2));
        end
        noPPAmpNull(ch,:) = max(startle.Null(noPPtrial,:),[],2) + abs(min(startle.Null(noPPtrial,:),[],2));
        
        end
end

if Args.onlyPP
    PP = find(infoTrial(raw,:)  ~= 0 ); % searches for trials with a prepulse not equal to 0
    for ch = 1 : max(chamberid)
        noPtrial = intersect(intersect(find(infoTrial(STraw,:) == 0),find(chamberid == ch)),blockTrial);% searches for trials with no startle Pulse, in each chamber, for block 2 only.
        PPonly = intersect(noPtrial, PP); %gives trials with a PP not equal to 0 and no startle pulse, in block 2, for both chambers
        PPonlyStartle(ch,:) = max(startle.Startle(PPonly,:),[],2) + abs(min(startle.Startle(PPonly,:),[],2));%amplitude of these trials
    end
    varargout{3} = PPonlyStartle;
end
