function ppi1(varargin)

Args = struct('ntrials',1,'pre_stim',0,'durationPP',.18,'durationP',.38,'iti',[0 0],'FSsound',8192,'dBW',10,'newWN',0,'gainPP',.6,'gainP',1,'delay',0);
Args.flags = {'newWN'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});


if Args.newWN || isempty(nptDir('C:\Documents and Settings\carleton\desktop\whitenoise1.mat')) || isempty(nptDir('C:\Documents and Settings\carleton\desktop\pulse.mat'))
    vl = round((Args.FSsound / 1000) * Args.durationPP);
    y = wgn(vl,1,Args.dBW);
    y = Args.gainPP * y / max(abs(y));
    save('C:\Documents and Settings\carleton\desktop\whitenoise1.mat','y')
    yPP = y;
     vl = round((Args.FSsound / 1000) * Args.durationP);
    y = wgn(vl,1,Args.dBW);
    y = Args.gainP * y / max(abs(y));
    save('C:\Documents and Settings\carleton\desktop\pulse.mat','y')
    yP = y;
else
    load('C:\Documents and Settings\carleton\desktop\whitenoise1.mat')
    yPP = y;
    load('C:\Documents and Settings\carleton\desktop\pulse.mat')
    yP = y;
end

Z=zeros(740,1);
sFinal=[yPP;Z;yP];

display('!!!!!!!!!!!!!!!!!!!!START OF SESSION!!!!!!!!!!!!!!!!!!!!!!!!!!')
for tr = 1 : Args.ntrials
    display(sprintf('%d',tr))
    if Args.pre_stim > 0
        pause(Args.pre_stim/1000)
    end
    
    sound(sFinal)
%     pause(Args.delay)
%     sound(yP)
    if Args.iti(1) > 0 && Args.iti(2) > 0
        pause((Args.iti(1) + randi(Args.iti(2) - Args.iti(1),1))/1000)
    end
end


display('!!!!!!!!!!!!!!!!!!!!END OF SESSION!!!!!!!!!!!!!!!!!!!!!!!!!!!')