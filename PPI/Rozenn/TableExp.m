%Permutations al�atoires des indices du vecteur contenant les doses �
%injecter pour faire la table d'exp�riences
doses=[.5 .05 .01 .005 .001 0];
mtable=[];

%while length(mtable)<24
    bad = true;
    while bad
    indicepermute = randperm(numel(doses));
    if isempty(find(abs(diff(doses(indicepermute))) == .45))
       bad = false; 
    end
    end
    mouse = doses(indicepermute);
    mtable=cat(1,mtable, mouse);
end   
weight=[24.3 24.1 24.4 23.9 25.1 26.6 22.7 25.3 25.8 28.1 25.8 23.8 23 26 23 25.4 24.7 22.8 25 20.8 26.1 26.4 24.8 25.1];
for i=1:length(weight)
    mtable(i,7)=weight(i)
end    
mice={'212533';'099967';'110294';'194892';'210129';'115800';'192037';'197254';'200244';'202494';'205790';'110862';'106029';'207944';'121850';'212117';'210543';'202078';'193719';'196923';'201884';'199376';'204235';'134227'};
jour={'mardi16' 'mercredi17' 'jeudi18' 'vendredi19' 'samedi20' 'dimanche21' 'Weight'};

T = table(mtable(:,1),mtable(:,2),mtable(:,3),mtable(:,4),mtable(:,5),mtable(:,6),mtable(:,7),'rowNames',mice,'VariableNames',jour)


