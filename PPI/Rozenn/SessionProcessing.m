% Fonction qui fait tourner les quatres fonctions associ�es au traitement
% de donn�es du PPi. Attention, il faut se trouver dans le bon dossier. %

function SessionProcessing
table=nptDir ('*TableTest.TAB*') % cherche le fichier ayant l'extension .TAB %
data=nptDir('*rawdata*') % cherche les donn�es brutes %
[infoTrial,defaultParams] = ReadInfoPPItxtFile(table.name);
[startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(data.name);
[f1,fch] = plotStartle(startle,infoTrial,defaultParams,chamberid,blockid)
% sauvegarde de chaque figure, comment utiliser f1, fch ? %
saveas(f1,cat(2,table.name(1:10),'AllTrials.fig'))
%saveas(fch,'Chamber1test.fig') %ne marchent pas : variable non explicit�e en dehors de la fonction ? %
%saveas(fch(2),'Chamber2test.fig')
[PCstartle] = percentStartle(startle,infoTrial,defaultParams,chamberid,blockid)
% sauve le pct avec la date %
save(cat(2,table.name(1:10),'pctPPI.mat'),'PCstartle')
end
