%% Test de la réactivité

% Comparaison de l'amplitude du startle (bloc II uniquement) selon la dose recue

fid = fopen(filename,'r');
a=textscan(fid,'%s');
a = a{1};

all = find(strcmp('BLOCK:',a) == 1);
blockid = str2double(a(all + 1));