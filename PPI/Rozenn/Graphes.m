%% Graphes
%s�paration du jdd selon la dose
ctrl=ppi(find(doses==0),:);
length(ctrl)
%II- dose 0.5
dose1=ppi(find(doses==0.5),:);
length(dose1)
%III- dose 0.05
dose2=ppi(find(doses==0.05),:);
length(dose2)
%II- dose 0.01
dose3=ppi(find(doses==0.01),:);
length(dose3)
%II- dose 0.005
dose4=ppi(find(doses==0.005),:);
length(dose4)
%II- dose 0.001
dose5=ppi(find(doses==0.001),:);
length(dose5)

abscisses=[75 80 85 90]

figure1=figure;
subplot(2,1,1);boxplot(ctrl);ylim([0 100]);title('PPI in control mice');
subplot(2,1,2);boxplot(dose1);xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('PPI in injected mice');
saveas(figure1,'1BoxplotControlDosemax.fig');
saveas(figure1,'1BoxplotControlDosemax.jpg');

figure2=figure;
A=plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
B=plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
C=plot(mean(dose1) + std(dose1)/sqrt(24),'r'); hold on;
D=plot(mean(dose1) - std(dose1)/sqrt(24),'r');xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('No effect of SalB on the PPI');
legend([A,C],'control','SalB 0.5');
saveas(figure2,'2DistributionControlDosemax.fig');
saveas(figure2,'2DistributionControlDosemax.jpg');

figure3=figure;
subplot(3,2,1);boxplot(ctrl);ylim([0 100]);title('PPI in control mice');
subplot(3,2,2);boxplot(dose1);ylim([0 100]);title('Sal B 0.5');
subplot(3,2,3);boxplot(dose2);ylim([0 100]);title('Sal B 0.05');
subplot(3,2,4);boxplot(dose3);ylim([0 100]);title('Sal B 0.01');
subplot(3,2,5);boxplot(dose4);xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('Sal B 0.005');
subplot(3,2,6);boxplot(dose5);xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('Sal B 0.001');
saveas(figure3,'3BoxplotAllDoses.fig');
saveas(figure3,'3BoxplotAllDoses.jpg');

figure4=figure;
subplot(3,2,1);A=plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,1);B=plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;title('Control alone');


subplot(3,2,2);plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,2);plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
subplot(3,2,2);plot(mean(dose1) + std(dose1)/sqrt(24),'r'); hold on;
subplot(3,2,2);plot(mean(dose1) - std(dose1)/sqrt(24),'r');ylim([0 100]);title('Sal B 0.5');


subplot(3,2,3);plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,3);plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
subplot(3,2,3);plot(mean(dose2) + std(dose2)/sqrt(24),'r'); hold on;
subplot(3,2,3);plot(mean(dose2) - std(dose2)/sqrt(24),'r');ylim([0 100]);title('Sal B 0.05');

subplot(3,2,4);plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,4);plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
subplot(3,2,4);plot(mean(dose3) + std(dose3)/sqrt(24),'r'); hold on;
subplot(3,2,4);plot(mean(dose3) - std(dose3)/sqrt(24),'r');ylim([0 100]);title('Sal B 0.01');

subplot(3,2,5);plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,5);plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
subplot(3,2,5);plot(mean(dose4) + std(dose4)/sqrt(24),'r'); hold on;
subplot(3,2,5);plot(mean(dose4) - std(dose4)/sqrt(24),'r');xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('Sal B 0.005');

subplot(3,2,6);plot(mean(ctrl) + std(ctrl)/sqrt(24)); hold on; 
subplot(3,2,6);plot(mean(ctrl) - std(ctrl)/sqrt(24)); ylim([0 100]);hold on;
subplot(3,2,6);plot(mean(dose5) + std(dose5)/sqrt(24),'r'); hold on;
subplot(3,2,6);plot(mean(dose5) - std(dose5)/sqrt(24),'r');xlabel('Prepulse intensities (dB)');ylabel('%PPI');ylim([0 100]);title('Sal B 0.001');

saveas(figure4,'4DistributionControlDosemaxComparaison.fig');
saveas(figure4,'4DistributionControlDosemaxComparaison.jpg');


%%Trouve les outliers
%contr�les
outlier=find(ctrl(:,3)==min(ctrl(:,3)))
mice0=find(doses==0)
miceID(mice0(outlier))

%% Test de r�p�tition sur plusieurs jours
%s�paration du jdd selon les jours, pour la condition controle
ctrl=ppi(find(doses==0),:);%ne prend que les contr�les
jours=Day(find(doses==0))% trouve les jours associ�s

jour1=ctrl(find(jours==20150617),:);
jour2=ctrl(find(jours==20150618),:);
jour3=ctrl(find(jours==20150619),:);
jour4=ctrl(find(jours==20150620),:);
jour5=ctrl(find(jours==20150621),:);
jour6=ctrl(find(jours==20150622),:);
jour7=ctrl(find(jours==20150623),:);

% Visualisation de la r�p�tition du PPI au cours du temps, dose contr�le
% uniquement

repetitionCtrl=figure;
subplot(3,2,1);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,1);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,1);plot(mean(jour2) + std(jour2)/sqrt(length(jour2)),'r'); hold on;
subplot(3,2,1);plot(mean(jour2) - std(jour2)/sqrt(length(jour2)),'r');title('Day 1 vs day 2');

subplot(3,2,2);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,2);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,2);plot(jour3,'r'); title('Day 1 vs day 3');

subplot(3,2,3);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,3);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,3);plot(mean(jour4) + std(jour4)/sqrt(length(jour4)),'r'); hold on;
subplot(3,2,3);plot(mean(jour4) - std(jour4)/sqrt(length(jour4)),'r');title('Day 1 vs day 4');

subplot(3,2,4);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,4);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,4);plot(jour5,'r');title('Day 1 vs day 5');

subplot(3,2,5);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,5);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,5);plot(mean(jour6) + std(jour6)/sqrt(length(jour6)),'r'); hold on;
subplot(3,2,5);plot(mean(jour6) - std(jour6)/sqrt(length(jour6)),'r');title('Day 1 vs day 6');xlabel('Prepulse intensities (dB)');ylabel('%PPI')

subplot(3,2,6);plot(mean(jour1) + std(jour1)/sqrt(length(jour1))); hold on; 
subplot(3,2,6);plot(mean(jour1) - std(jour1)/sqrt(length(jour1))); ylim([0 100]);hold on;
subplot(3,2,6);plot(mean(jour7) + std(jour7)/sqrt(length(jour7)),'r'); hold on;
subplot(3,2,6);plot(mean(jour7) - std(jour7)/sqrt(length(jour7)),'r');title('Day 1 vs day 7');xlabel('Prepulse intensities (dB)');ylabel('%PPI')


saveas(repetitionCtrl,'repetitionCtrl.fig')
saveas(repetitionCtrl,'repetitionCtrl.jpg')


%% R�p�tition, toutes les souris, 6 jours

mercredi=ppi(find(Day==20150617),:);
jeudi=ppi(find(Day==20150618),:);
vendredi=ppi(find(Day==20150619),:);
samedi=ppi(find(Day==20150620),:);
dimanche=ppi(find(Day==20150621),:);
lundi=ppi(find(Day==20150622),:);

repetition=figure;
% mercredi
A=plot(mean(mercredi) + std(mercredi)/sqrt(length(mercredi))); hold on; 
plot(mean(mercredi) - std(mercredi)/sqrt(length(mercredi))); ylim([0 70]);hold on;
% jeudi
B=plot(mean(jeudi) + std(jeudi)/sqrt(length(jeudi)),'r'); hold on; 
plot(mean(jeudi) - std(jeudi)/sqrt(length(jeudi)),'r'); ylim([0 70]);hold on;
% vendredi
C=plot(mean(vendredi) + std(vendredi)/sqrt(length(vendredi)),'g'); hold on; 
plot(mean(vendredi) - std(vendredi)/sqrt(length(vendredi)),'g'); ylim([0 70]);hold on;
% samedi
D=plot(mean(samedi) + std(samedi)/sqrt(length(samedi)),'k'); hold on; 
plot(mean(samedi) - std(samedi)/sqrt(length(samedi)),'k'); ylim([0 70]);hold on;
% dimanche
E=plot(mean(dimanche) + std(mercredi)/sqrt(length(dimanche)),'m'); hold on; 
plot(mean(dimanche) - std(mercredi)/sqrt(length(dimanche)),'m'); ylim([0 70]);hold on;
% lundi
F=plot(mean(lundi) + std(lundi)/sqrt(length(lundi)),'c'); hold on;
plot(mean(lundi) - std(lundi)/sqrt(length(lundi)),'c'); ylim([0 70]); xlabel('Prepulse intensities (dB)');ylabel('%PPI'), title('PPI mean +/- standard error')
legend([A,B,C,D,E,F],'jour 1','jour 2', 'jour 3', 'jour 4', 'jour 5', 'jour 6');

saveas(repetition,'repetition.fig')


%% R�p�tition, par souris, 6 jours
id=unique(miceID)
for m = 1:length(id)
    repetition=figure;
    A=plot(mercredi); hold on; 
    B=plot(jeudi,'r'); hold on; 
    C=plot(vendredi,'g'); hold on; 
    D=plot(samedi,'k'); hold on; 
    E=plot(dimanche,'m'); hold on; 
    F=plot(lundi,'c'); hold on;
    legend([A,B,C,D,E,F],'jour 1','jour 2', 'jour 3', 'jour 4', 'jour 5', 'jour 6');
    saveas(repetition, sprintf('%s repetition.fig',id(m)) %% Comment mettre l'ID de la souris sachant que c'est une cellule qui les contient ?
end

%% Visualisation de l'amplitude du bruit (NullPeriod) et des Startle alone
d0=nullPeriodAmp(find(doses==0),:);
d5=nullPeriodAmp(find(doses==0.5),:);
d4=nullPeriodAmp(find(doses==0.05),:);
d3=nullPeriodAmp(find(doses==0.01),:);
d2=nullPeriodAmp(find(doses==0.005),:);
d1=nullPeriodAmp(find(doses==0.001),:);

D0=startleAmp(find(doses==0),:);
D5=startleAmp(find(doses==0.5),:);
D4=startleAmp(find(doses==0.05),:);
D3=startleAmp(find(doses==0.01),:);
D2=startleAmp(find(doses==0.005),:);
D1=startleAmp(find(doses==0.001),:);

Doses=[0 0 0.001 0.001 0.005 0.005 0.01 0.01 0.05 0.05 0.5 0.5];
dd={};
tata=[1:2:12];
tonton=[2:2:12];
% Null period
dd{1}= d0;dd{3}= d1; dd{5}=d2;dd{7}= d3;dd{9}= d4; dd{11}=d5;
% Startle period
dd{2}= D0;dd{4}= D1; dd{6}=D2;dd{8}= D3;dd{10}= D4; dd{12}=D5;
nulldd = cat(3,d0,d1,d2,d3,d4,d5);
stdd = cat(3,D0,D1,D2,D3,D4,D5);
drugEffect=figure;
for sb = 2 : 6; subplot(6,2,tata(sb)); plot(dd{tata(sb)},'.'); ylim([0 1000]);ylabel(sprintf('Sal B %d', Doses(tata(sb))));end
for sb = 2 : 6; subplot(6,2,tonton(sb)); plot(dd{tonton(sb)},'.'); ylim([0 5000]);ylabel(sprintf('Sal B %d', Doses(tonton(sb))));end
A=subplot(6,2,1);plot(dd{1},'.');xlabel('Mice');title('Null Period');ylabel('Sal B 0'); ylim([0 1000]);
B=subplot(6,2,2);plot(dd{2},'.');xlabel('Mice');title('Startle Period');ylabel('Sal B 0');ylim([0 5000]);

saveas(drugEffect,'drugEffectReactivity.fig');

% Boxplot
Null=[];
St=[];

for sb = 1 : 12 ; dd{sb}=reshape(dd{sb},1,288); end;
for sb = 1:6 ; Null=cat(2,Null,log(dd{tata(sb)})); St=cat(2,St,log(dd{tonton(sb)})); end;

% vecteurs qui simulent les doses
v=[];
for n=1:288;v(n)=0;end;
for n=288:576;v(n)=0.001;end;
for n=576:864;v(n)=0.005;end;
for n=864:1152;v(n)=0.01;end;    
for n=1152:1440;v(n)=0.05;end;
for n=1440:1728;v(n)=0.5;end;

boxPlot=figure;subplot(1,2,1);boxplot(Null,v);xlabel('Sal B doses (mg per g)');ylabel('Reactivity (arbitrary units)');title('Mice reactivity during the null period (block II)');
subplot(1,2,2);boxplot(St,v);xlabel('Sal B doses (mg per g)');ylabel('Reactivity (arbitrary units)');title('Mice reactivity during the startle period (block II)');

saveas(boxPlot,'drugEffectReactivityBoxPlot.fig');

%% Boxplot on normalized data
nulldd = cat(3,d0,d1,d2,d3,d4,d5);
stdd = cat(3,D0,D1,D2,D3,D4,D5);
normnulldd = (nulldd - repmat(mean(reshape(nulldd,[24 12*6]),2),1,12,6)) ./ repmat(std(reshape(nulldd,[24 12*6]),[],2),1,12,6);
normstdd=(stdd - repmat(mean(reshape(stdd,[24 12*6]),2),1,12,6)) ./ repmat(std(reshape(stdd,[24 12*6]),[],2),1,12,6);
normBoxPlot=figure;subplot(2,1,1);boxplot(reshape(normnulldd,24*12,6));xlabel( 'Sal B doses (mg/g)' ); ylabel( 'Mice reactivity (Z score)');title('Null period');
subplot(2,1,2);boxplot(reshape(normstdd,24*12,6));xlabel( 'Sal B doses (mg/g)' ); ylabel( 'Mice reactivity (Z score)');title('Startle period');


saveas(normBoxPlot,'drugEffectReactivityNormBoxPlot.fig');

















