% Fonction qui fait tourner les quatres fonctions associ�es au traitement
% de donn�es du PPi. Attention, il faut se trouver dans le bon dossier. %

function [PCstartle,varargout] = processSessionPPI(varargin)

Args = struct('noplot',0,'sdB',[75 80 85 90],'withLight',0);
Args.flags = {'noplot','withLight'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

if isempty(nptDir('skip.txt'))
    
    table=nptDir ('*TableTest.TAB*'); % cherche le fichier ayant l'extension .TAB
    data=nptDir('*rawdata*'); % cherche les donn�es brutes
    [infoTrial,defaultParams] = ReadInfoPPItxtFile(table.name,modvarargin{:});
    [startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(data.name,modvarargin{:});
    basename = table.name(1:10); % stocke la date dans basename
    if ~Args.noplot
        [f1,fch] = plotStartle(startle,infoTrial,defaultParams,chamberid,blockid);
        % sauvegarde des figures
        saveas(f1,cat(2,basename,'rawtraces.fig')) % sauve la figure 1
        
        for fi = 1 : 2 % sauve les fig 2 et 3
            saveas(fch(fi),sprintf('%sChamber%dPrepulse.fig',basename,fi)) % %s et %d servent � pr�ciser ou mettre quoi dans le nom de la fig
        end
        % fermeture automatique des fen�tres
        close(f1)
        close(fch)
        display('Saving figures')
    end
    if Args.withLight
        load('LightTrials.mat');
        lightTrials = [lightTrials; lightTrials + size(infoTrial,2)/2]; % divide by 2 if two chambers
        [PCstartleTemp,~,~,alldB] = percentStartle(startle,infoTrial,defaultParams,chamberid,blockid,'LightTrials',lightTrials,modvarargin{:});
        
    else
        [PCstartleTemp,~,~,alldB] = percentStartle(startle,infoTrial,defaultParams,chamberid,blockid,modvarargin{:});
        
    end
    sdB = find(ismember(alldB,Args.sdB) == 1);
    PCstartle = PCstartleTemp(sdB,:);
    
    varargout{1} = alldB;
    % Sauve le pct avec la date %
    save(cat(2,basename,'pctPPI.mat'),'PCstartle')
    save(cat(2, basename, 'startle'), 'startle')
else
    PCstartle = [];
end

