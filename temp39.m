allcor = [];
allp = [];
for d =1 : 29
    cd(idedays{d})
    sessions = nptDir('session0*');
    for s = 2 : length(sessions)
        cd(sessions(s).name)
        mt = mtstrial('auto');
        if mt.data.Index(1,1) == 1
            if isempty(nptDir('PerfCohInterCor.mat'))
                clear cor pvalue
                load TrialscohInter.mat
                [perf,ti] = getperformance('window',100);
                for c = 1 : size(value.C,3)
                    for p = 1 : 4
                        for freq = 1 : 65
                            
                            [r,pv] = corrcoef(value.C(:,freq,c,p),perf);
                            cor(freq,c,p) = r(1,2);
                            pvalue(freq,c,p) = pv(1,2);
                        end
                    end
                end
                
                save PerfCohInterCor.mat cor pvalue f
                
            end
            
            load PerfCohInterCor.mat
            allcor = cat(2, allcor, cor);
            allp = cat(2, allp, pvalue);
        end
        cd ..
        
    end
    cd ..
end

