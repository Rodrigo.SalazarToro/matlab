cd /Volumes/raid/data/monkey/

type = 'beta34';
flim = [25 50];
obj = loadObject('cohInterIDEL.mat');
[r,ind] = get(obj,'Number','snr',99);
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
epoch = {'pre-sample' 'sample' 'delay1' 'delay2'};

% [r,ind] = get(obj,'Number','snr',99,'type',{'gammaneg14' [1 3]});r
histpair = unique(obj.data.hist(ind,:),'rows');
nhistpair = [];
clear nareasN
for hpair = 1 : size(histpair,1)
    [r,ind] = get(obj,'Number','snr',99,'type',{type [1 3]},'hist',histpair(hpair,:));
     npairsN(hpair) = r;
    if (r > 50  && sum(histpair(hpair,:) == 99) == 0)
        nhistpair = [nhistpair; histpair(hpair,:)];
    end
end
figure
for hpair = 1 : size(nhistpair,1) + 1
    
    
    if hpair ==  size(nhistpair,1) + 1
        [r,ind] = get(obj,'Number','snr',99,'type',{type [1 3]});
        
    elseif sum(nhistpair(hpair,:) == 99) == 0
        [r,ind] = get(obj,'Number','snr',99,'type',{type [1 3]},'hist',nhistpair(hpair,:));
        
    end
    
    sigdiffn = zeros(r,100,4);
    sigdiffp = zeros(r,100,4);
    plevels = [0.01];
    % testfig = figure;
    for i = 1 : r;
        
        cd(obj.data.setNames{ind(i)});
        cmb = obj.data.Index(ind(i),1);
        load GCcohInter.mat
        if Day.session(1).rule == 1
            s = 1;
        else
            s = 2;
        end
        
        
        Fx2y = squeeze(Day.session(s).Fx2y(:,cmb,:));
        Fy2x = squeeze(Day.session(s).Fy2x(:,cmb,:));
        load GCgeneralSur.mat
        Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
        Fy2xsur = squeeze(Day.session(s).Fx2y(:,3,:));
        siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);
        
        cd GCdirStat
        pairg = obj.data.Index(ind(i),10:11);
        pwd;
        file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),1);
        load(file,'Fdiff')
        Fdiff = Fdiff .* siggc;
        sfile = sprintf('GCintergeneralRule%d.mat',1);
        load(sfile,'pvalues','levels')
        %     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
        
        pind = find(plevel == levels);
        % Fdiff = Fx2y - Fy2x;
        sigdiffn(i,:,:) = (Fdiff <= squeeze(pvalues(:,pind(1),:))) ; % negative means PFC dominate
        sigdiffp(i,:,:) = (Fdiff >= squeeze(pvalues(:,pind(2),:))) ; % positive means PPC dominates
    end
    
    
    for p =1 : 4;
        
        subplot(size(nhistpair,1)+ 1,4, (hpair-1)* 4 +p);
        
        plot(100*sum(squeeze(sigdiffn(:,:,p)),1)/r);
        hold on
        plot(100*sum(squeeze(sigdiffp(:,:,p)),1)/r,'r');
        ylim([0 100]);xlim(flim);
        if p ==1 && hpair <size(nhistpair,1)+ 1; ylabel(sprintf('n=%d; %s %s',r,areas{nhistpair(hpair,1)},areas{nhistpair(hpair,2)})); end
    end;
    if hpair == 1; for sb = 1 : 4; subplot(size(nhistpair,1)+ 1,4,sb); title(epoch{sb}); end; end
    
    %     c = 1 ;for sb = [1 5 9];subplot(3,4,sb); ylabel(sprintf('p < %d',plevels(c))); c = c + 1;end
end
xlabel('Frequency [Hz]')
ylabel('% sign. diff.')
legend('PFC->PPC dom.','PPC->PFC dom.');
% clark PPC = x, PFC = y;
% betty PFC = x, PPC = y
cd ../../..


%% data split according to areas and not areas pair
% obj = loadObject('cohInterIDEL.mat');
[r,ind] = get(obj,'Number','snr',99);
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

% [r,ind] = get(obj,'Number','snr',99,'type',{'gammaneg14' [1 3]});r
areasN = unique(obj.data.hist(ind,:));
nareas = [];
clear nareasN
for a = 1 : length(areasN)
        [r,ind] = get(obj,'Number','snr',99,'type',{type [1 3]},'hist',[areasN(a) nan]);
%     [r,ind] = get(obj,'Number','snr',99,'hist',[areasN(a) nan]);
    nareasN(a) = r;
    if r >= 30
        nareas = [nareas; areasN(a)];
    end
end
figure
dirL = {'-' '--'};
for dir = [-1 1]
    for a = 1 : length(nareas)
        [r,ind] = get(obj,'Number','snr',99,'type',{type [1 3]},'hist',[nareas(a) nan]);
        %     [r,ind] = get(obj,'Number','snr',99,'hist',[nareas(a) nan]);
        clear sigdiff
        
        plevels = [0.01];
        % testfig = figure;
        for i = 1 : r;
            
            cd(obj.data.setNames{ind(i)});
            cmb = obj.data.Index(ind(i),1);
            load GCcohInter.mat
            if Day.session(1).rule == 1
                s = 1;
            else
                s = 2;
            end
            
            
            Fx2y = squeeze(Day.session(s).Fx2y(:,cmb,:));
            Fy2x = squeeze(Day.session(s).Fy2x(:,cmb,:));
            load GCgeneralSur.mat
            Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
            Fy2xsur = squeeze(Day.session(s).Fx2y(:,3,:));
            siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);
            
            cd GCdirStat
            pairg = obj.data.Index(ind(i),10:11);
            pwd;
            file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),1);
            load(file,'Fdiff')
            Fdiff = Fdiff .* siggc;
            sfile = sprintf('GCintergeneralRule%d.mat',1);
            load(sfile,'pvalues','levels')
            %     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
            
            pind = find(plevel == levels);
            % Fdiff = Fx2y - Fy2x;
            
            
            
            if ((nareas(a) <= 7 || nareas(a) == 17) && dir == -1) || (~(nareas(a) <= 7 || nareas(a) == 17) && dir == 1)
                sigdiff(i,:,:) = (Fdiff <= squeeze(pvalues(:,pind(1),:))) ; % negative means PFC dominate
            else
                sigdiff(i,:,:) = (Fdiff >= squeeze(pvalues(:,pind(2),:))) ; % positive means PPC dominates
            end
        end
        %     alldiff(a,:,:) = 100*squeeze(sum(sigdiff,1))/r;
        
        
        
        subplot(2,ceil(length(nareas)/2),a);
        
        plot(-dir * 100*squeeze(sum(sigdiff,1))/r);
        hold on
        ylim([-60 60]);xlim(flim);
        if nareas(a) ~= 99;title(sprintf('n=%d; %s',r,areas{nareas(a)}));end
        
        
    end
end
% figure
% for p =1 : 4;
%
%     subplot(1,4, p);
%
%     plot(squeeze(alldiff(:,:,p))');
%
%     ylim([0 100]);xlim([10 32]);
%
% end


xlabel('Frequency [Hz]')
% ylabel('% sign. diff.')
legend(epoch)
cd ../../..


