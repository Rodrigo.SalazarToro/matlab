tune = {'ide','loc'};

areas = {'PP' 'PF'};
clear tuning rules
% tuning = cell(2,3,4,2);
tuning = [];
index = [];
% rules = cell(2,3,4,2);
co = {'r' 'k' 'b'};
load 060328/cohInter.mat
% f = [0 : 100];
limitB = find(f>= 12 & f<= 25);
count = 0;

load switchdays.mat   
days = switchdays;
for d = 1 : 21
    cd(days{d})
    
        for s = 1 : 2
            cd(sprintf('session0%d/lfp',s+1))
            %          [cohPP,cohPF,cohInter,f] = checktuning('save','remoteName','anomaly','locTuning');
            %          [cohPP,cohPF,cohInter,f] = checktuning('save','remoteName','anomaly','ideTuning');
            mt = mtstrial('auto');
            r = unique(mt.data.Index(:,1));
            for t = 1 : 2
                load(sprintf('%sTuningRule%d.mat',tune{t},r))
                
                for p = 1 : 4
                    
                    for c = 1 : 3
                        data = squeeze(cohInter.coh(c).C(:,:,p));
%                         data = [squeeze(mean(powerPP.power(c).S(:,:,:,p),3)); squeeze(mean(powerPF.power(c).S(:,:,:,p),3))];
%                         me = squeeze(mean(data,3));
%                         st = squeeze(std(data,[],3));
                        %                         data = (data - repmat(me,[1 1 size(data,3)])) ./ repmat(st,[1 1 size(data,3)]);
                        if length(size(data)) == 2
                            tuning = [tuning; data];
                            %                             rules{a,c,p,t} = [rules{a,c,p,t}; repmat(r,size(data,1),1)];
                            thedata = data;
                            npairs = size(data,1);
                        
                        end
                        clear tindex
                       
                        tindex(1,:) = repmat(c,1,npairs);
                        tindex(2,:) = repmat(p,1,npairs);
                        tindex(3,:) = repmat(t,1,npairs);
                        tindex(4,:) = repmat(r,1,npairs);
                        
                        tindex(5,:) = [count + 1: count + npairs];
                        tindex(6,:) = nan(1,npairs);
                        index = [index tindex];
                        tarea(:,c) = sum(thedata(:,limitB),2);
                    end
                    index(6,isnan(index(6,:))) = abs(repmat(reshape((max(tarea,[],2) - min(tarea,[],2)) ./ min(tarea,[],2),1,size(tarea,1)),1,3));
%                      index(6,isnan(index(6,:))) = repmat(reshape((max(tarea,[],2) - min(tarea,[],2)),1,size(tarea,1)),1,3); % for phi
                    clear tarea
                end
            end
            cd ../..
        end
        
        
        count = count + npairs;
    
    cd ..
    
end
 tit = {'Pre-sample' 'Sample' 'Delay1' 'Delay2'};
lim(1) = prctile(index(6,find(index(2,:) == 1)),99);
lim(2) = prctile(index(6,find(index(2,:) == 1)),95);
maxx = max(index(6,:));
maxy = [];
figure
% for r = 1 : 2
%     figure
%     for t = 1 : 2
        for p = 1 : 4
            ind = find(index(2,:) == p); % & index(4,:) == r & index(3,:) == t);
%             subplot(4,2,(t - 1)* 4 +p)
            subplot(4,1,p)
            [n,xout] = hist(index(6,ind),[-maxx : 0.01 : maxx]);
            bar(xout,n/12)% 3 cue, 2 tuning, 2 sessions
            maxy = [maxy max(n/12)];
            hold on
            for l = 1 : 2; line([lim(l) lim(l)],[0 25],'Color','r'); hold on;end
            
        end
%     end
% end
xlabel('index tuning')
ylabel('nbr of sites')
for sb = 1 : 4; subplot(4,1,sb); title(tit{sb}); axis([ 0 maxx 0 max(maxy)]);end

% for a = 1 : 2
%     for it = 1 : size(tuning,1)
%         clf
%         maxy = [];
%         for t =1 : 2
%             for p = 1 : 4
%                 for c = 1 : 3
%                     subplot(2,4,(t-1)*4+p)
%                     thedata = tuning{a,c,p,t}(it,:);
%                     tarea(c) = sum(thedata(limitB));
%                     plot(f,thedata,co{c})
%                     maxy = [maxy max(thedata)];
%                     title(rules{a,c,p,t}(it))
%                     xlabel(tune{t})
%                     hold on
%                 end
%                 tindex{a,p,t,rules{a,c,p,t}(it)}(it) = (max(tarea) - min(tarea)) / (max(tarea) + min(tarea));
%                 text(40, max(maxy),sprintf('%0.1g',tindex{a,p,t,rules{a,c,p,t}(it)}(it)))
%             end
%         end
%         for sb = 1 : 8; subplot(2,4,sb); axis([0 50 0 max(maxy)]); end
%         % pause
%     end
%
% end
%
