for d =1 : 27
    cd(idedays{d})
    cd grams
    files = nptDir('*generalized*.mat');
    for i = 1 : length(files)
        load(files(i).name)
        clear thres
        for ti = 1 : size(C{1},2)
            [~,~,thres(ti,:,:),~] = fitSurface(squeeze(C{1}(:,ti,:)),f,'Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2);
        end
        files(i).name
        save(files(i).name,'trials','t','f','thres','C','phi')
    end
    cd ../..
end

days = {'060511' '060509' '060411' '060328'};
pairs = [3 4; 3 7; 1 7; 1 3];
sessions = {'session02' 'session02' 'session03' 'session02'};
params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1);
clear trials
for si = 1 : 4
    cd(days{si})
    cd(sessions{si});
    mts = mtstrial('auto');
    clear trials
    for i = 1 : 3; trials{i} = mtsgetTrials(mts,'stable','BehResp',1,'iCueLoc',i); end
    clear dataLoc thedata
    for i = 1 : 3; [dataLoc(i).data,lplength] = lfpPcut(trials{i},pairs(si,:),'fromFiles'); end
    for i = 1 : 3; thedata{i} = dataLoc(i).data{4}; end
    [rdiff,pvalues,levels,f,C] = compareNGroupCoh(thedata,params);
    h(si) = figure;
    set(h(si),'Name',sprintf('%s delay2 Ncomp',days{si}));
    subplot(2,1,1); plot(f,C)
    ylabel('coherence')
    subplot(2,1,2); plot(f,rdiff)
    hold on; plot(f,pvalues,'--')
    
    xlabel('Frequency [Hz]')
    ylabel('STD')
    ylim([0 0.2])
    
    
    saveas(h(si),sprintf('%s delay2 Ncomp',days{si}))
    
    for i = 1 : 3; thedataP{i} = dataLoc(i).data{1}; end
    [rdiffP,pvaluesP,levels,f,CP] = compareNGroupCoh(thedataP,params);
    h(si+4) = figure;
    set(h(si+4),'Name',sprintf('%s PreSample Ncomp',days{si}));
    subplot(2,1,1); plot(f,CP)
    ylabel('coherence')
    subplot(2,1,2); plot(f,rdiffP)
    hold on; plot(f,pvaluesP,'--')
    
    xlabel('Frequency [Hz]')
    ylabel('STD')
    ylim([0 0.2])
    saveas(h(si+4),sprintf('%s PreSample Ncomp',days{si}))
    cd ../..
end


comparisons = [1 2; 1 3; 2 3];
for si = 1 : 4
    cd(days{si})
    cd(sessions{si});
    mts = mtstrial('auto');
    
    for i = 1 : 3; trials{i} = mtsgetTrials(mts,'stable','BehResp',1,'iCueLoc',i); end
    
    for i = 1 : 3; [dataLoc(i).data,lplength] = lfpPcut(trials{i},pairs(si,:),'fromFiles'); end
    for i = 1 : 3; thedata{i} = dataLoc(i).data{4}; end
    h(si) = figure;
    set(h(si),'Name',sprintf('%s delay2 Twocomp',days{si}));
    
    for comp = 1 : 3
        [rdiff,pvalues,levels,f,C] = compareTwoGroupCoh(thedata{comparisons(comp,1)},thedata{comparisons(comp,2)},params);
        subplot(4,1,1); 
        if comp == 1; plot(f,C); hold on; end
        if comp == 3; plot(f,C(:,2),'k'); hold on;  legend('1','2','3');end
       
        ylabel('coherence')
        subplot(4,1,comp+1); plot(f,rdiff)
        hold on; plot(f,pvalues,'--')
        
        xlabel('Frequency [Hz]')
        ylabel('diff')
        ylim([-0.2 0.2])
        title(sprintf('%d %d ',comparisons(comp,1),comparisons(comp,2)))
    end
    
    saveas(h(si),sprintf('%s delay2 Twocomp',days{si}))
    
    for i = 1 : 3; thedataP{i} = dataLoc(i).data{1}; end
    h(si+4) = figure;
    set(h(si+4),'Name',sprintf('%s PreSample Twocomp',days{si}));
   
    ylabel('coherence')
   
    for comp = 1 : 3
        [rdiffP,pvaluesP,levels,f,CP] = compareTwoGroupCoh(thedataP{comparisons(comp,1)},thedataP{comparisons(comp,2)},params);
        subplot(4,1,1); 
        if comp == 1; plot(f,CP); hold on; end
        if comp == 3; plot(f,CP(:,2),'k'); hold on;  legend('1','2','3');end
        subplot(4,1,comp+1); plot(f,rdiffP)
        hold on; plot(f,pvaluesP,'--')
        
        xlabel('Frequency [Hz]')
        ylabel('diff')
        ylim([-0.2 0.2])
        
        title(sprintf('%d %d ',comparisons(comp,1),comparisons(comp,2)))
    end
    saveas(h(si+4),sprintf('%s PreSample Twocomp',days{si}))
    cd ../..
end