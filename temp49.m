tty = {'IDEObj' 'IDELoc'};
ttty =  {'Obj' 'Loc'};
epoch = 4;
[sigbeta,ind] = get(obj,'Number','snr',99,'sigSur',{'beta' epoch [1 3]});
for type = 1 : 2
    
    [r,ind] = get(obj,'Number','snr',99,'tuning',{tty{type} [1] '==' [0]; tty{type} [epoch] '>=' 1},'type',{'betapos34' [1 3]});
    inds{type} = ind;
    cc = 1;
    clear Ccomb
    for i = ind
        
        cd(obj.data.setNames{i})
        cd grams
        c = 1;
        for item = obj.data.IDEObjSeq(i,epoch,:)
            load(sprintf('cohgramg%04.0fg%04.0fRule1%s%d',obj.data.Index(i,10),obj.data.Index(i,11),ttty{type},item),'C','f','t')
            Ccomb{c,2}(cc,:,:) = C{2}(1:176,:);
            Ccomb{c,1}(cc,:,:) = C{1}(1:176,:);
            c = c + 1;
        end
        cc = cc + 1;
    end
    ff{type} = figure;
    
    set(ff{type},'Name',tty{type})
    for s = 1 : 3;
        subplot(3,2,(s-1)* 2 + 2);
        imagesc(t{2},f,squeeze(mean(Ccomb{s,2},1))',[0 0.25]);
        line([1.8 1.8],[0 40]);
        ylim([14 30]); colorbar;
        subplot(3,2,(s-1)* 2 + 1);
        imagesc(t{1},f,squeeze(mean(Ccomb{s,1},1))',[0 0.25]);
        line([1 1],[0 40]);ylim([14 30]); colorbar;
    end
    xlabel('Time [sec]; Sample Offset at 1.0 sec'); ylabel('Frequency [Hz]')
    title(sprintf('n=%d/%d',r,sigbeta))
    stim = {'unpreferred stimulus' 'intermediate stimulus' 'preferred stimulus'};
    c = 1;for sb = [2 4 6]; subplot(3,2,sb); title(stim{c}); c = c + 1;end
    xlabel('Time [sec]; Match Onset at 1.8 sec');
    
    
end