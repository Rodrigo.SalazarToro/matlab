%% cont delay

% obj = loadObject;
[r,ind] = get(obj,'type',{'betapos34' [1 3]},'sigSurPIntersect','sigSur',{'alpha' [1 : 4] [0 2]},'snr',99,'Number','delay','cont','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0});r

titD = {'delay 1sec' 'delay 2sec' 'delay 3sec'};
figure
[pair,sind,FixationSC,tvCFixS,f] = mkAveCohgram(obj,ind,'minTrials',50,'clim',0.15,'FixS');% minumum selected pairs

[pair,sind,FixationC,tvCFix,f] = mkAveCohgram(obj,sind{1},'minTrials',50,'clim',0.15,'Fix');

for nd = 1 : 3
    [pair,sind,alldata,tv,f,beforeMatch(nd)] = mkAveCohgram(obj,sind{1},'ndelay',nd);
    delayC(nd).data = alldata;
    tiC(nd).tv = tv;
    
end
h1 = figure;
set(h1,'Name','continous delays')

for nd = 1 : 3
    if ~isnan(beforeMatch);
        ref = [1 beforeMatch];
    else
        ref = [1 1.8];
    end
    for al = 1 : 2
        subplot(5,2,(nd-1)*2+al)
        
        imagesc(tiC(nd).tv{1,al},f,squeeze(median(delayC(nd).data{al},1))',[0 0.15])
        line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
        title(titD{nd})
    end
end

for al = 1 : 2
    subplot(5,2,6+al)
    
    imagesc(tvCFix{1,al},f,squeeze(median(FixationC{al},1))',[0 0.15])
    line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
    title('Fix inter')
    subplot(5,2,8+al)
    
    imagesc(tvCFixS{1,al},f,squeeze(median(FixationSC{al},1))',[0 0.15])
    line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
    title('Fix Ses')
end

%% bin delay
[r,ind] = get(obj,'type',{'betapos34' [1 3]},'sigSurPIntersect','sigSur',{'alpha' [1 : 4] [0 2]},'snr',99,'Number','delay','bin','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0});r

figure
[pair,sind,FixationB,tvBFix,f] = mkAveCohgram(obj,ind,'minTrials',50,'clim',0.15,'Fix');

[pair,sind,FixationSB,tvBFixS,f] = mkAveCohgram(obj,ind,'minTrials',50,'clim',0.15,'FixS');
for nd = 1 : 3
    [pair,sind,alldata,tv,f,beforeMatch(nd)] = mkAveCohgram(obj,ind,'minTrials',50,'clim',0.15,'ndelay',nd);
    delayB(nd).data = alldata;
    tiB(nd).tv = tv;
end


h2 = figure;
set(h2,'Name','binned delays')

for nd = 1 : 3
    if ~isnan(beforeMatch(nd));
        ref = [1 beforeMatch(nd)/1000];
    else
        ref = [1 1.8];
    end
    for al = 1 : 2
        subplot(5,2,(nd-1)*2+al)
        
        imagesc(tiB(nd).tv{1,al},f,squeeze(median(delayB(nd).data{al},1))',[0 0.2])
        line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
        title(titD{nd})
    end
end

for al = 1 : 2
    subplot(5,2,6+al)
    
    imagesc(tvBFix{1,al},f,squeeze(median(FixationB{al},1))',[0 0.2])
    line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
    title('Fix inter')
    subplot(5,2,8+al)
    
    imagesc(tvBFixS{1,al},f,squeeze(median(FixationSB{al},1))',[0 0.2])
    line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
    title('Fix Ses')
end


xlabel('Time [sec]')
ylabel('Frequency [hz]')

matchtime = [1.8 2.6 3.3];
figure
for d =1 : 3;
    subplot(3,1,d);
    plot(tiB(d).tv{1,2},mean(mean(delayB(d).data{2}(:,:,10:20),3),1));
    hold on;
    plot(tiC(d).tv{1,2},mean(mean(delayC(d).data{2}(:,:,10:20),3),1),'r'); 
    line([matchtime(d) matchtime(d)],[0.06 0.14]);
    ylim([0.06 0.14])
end
for d =1 : 3;
    subplot(3,1,d);
    plot(tiB(d).tv{1,2},mean(mean(delayB(d).data{2}(:,:,10:20),3),1)+std(mean(delayB(d).data{2}(:,:,10:20),3),1)./sqrt(size(delayB(d).data{2},1)),'--');
    hold on;
    plot(tiC(d).tv{1,2},mean(mean(delayC(d).data{2}(:,:,10:20),3),1)+std(mean(delayC(d).data{2}(:,:,10:20),3),1)./sqrt(size(delayC(d).data{2},1)),'r--');
end
for d =1 : 3;
    subplot(3,1,d); plot(tiB(d).tv{1,2},mean(mean(delayB(d).data{2}(:,:,10:20),3),1)-std(mean(delayB(d).data{2}(:,:,10:20),3),1)./sqrt(size(delayB(d).data{2},1)),'--');
    hold on;
    plot(tiC(d).tv{1,2},mean(mean(delayC(d).data{2}(:,:,10:20),3),1)-std(mean(delayC(d).data{2}(:,:,10:20),3),1)./sqrt(size(delayC(d).data{2},1)),'r--');
end
subplot(3,1,2);  line([matchtime(2)-1 matchtime(2)-1],[0.06 0.14]);
subplot(3,1,3);  line([matchtime(3)-1 matchtime(3)-1],[0.06 0.14]);line([matchtime(3)-2 matchtime(3)-2],[0.06 0.14]);