function [outdat_cor,ptsout_cor,outdat_incor,ptsout_incor,outinfo_cor,outinfo_incor]=divide_resp(indat,pts,info)

cor=find(info.BehResp==1);
incor=find(info.BehResp~=1);

outinfo_cor.CueObj=info.CueObj(cor);
outinfo_cor.CueLoc=info.CueLoc(cor);
outinfo_cor.MatchObj1=info.MatchObj1(cor);
outinfo_cor.MatchPos1=info.MatchPos1(cor);
outinfo_cor.MatchObj2=info.MatchObj2(cor);
outinfo_cor.MatchPos2=info.MatchPos2(cor);
outinfo_cor.CueOnset=info.CueOnset(cor);
outinfo_cor.CueOffset=info.CueOffset(cor);
outinfo_cor.MatchOnset=info.MatchOnset(cor);
outinfo_cor.BehResp=info.BehResp(cor);
outinfo_cor.FirstSac=info.FirstSac(cor);
outinfo_cor.LastSac=info.LastSac(cor);
outinfo_cor.NumSac=info.NumSac(cor);
outinfo_cor.Index=info.Index(cor,:);

outinfo_incor.CueObj=info.CueObj(incor);
outinfo_incor.CueLoc=info.CueLoc(incor);
outinfo_incor.MatchObj1=info.MatchObj1(incor);
outinfo_incor.MatchPos1=info.MatchPos1(incor);
outinfo_incor.MatchObj2=info.MatchObj2(incor);
outinfo_incor.MatchPos2=info.MatchPos2(incor);
outinfo_incor.CueOnset=info.CueOnset(incor);
outinfo_incor.CueOffset=info.CueOffset(incor);
outinfo_incor.MatchOnset=info.MatchOnset(incor);
outinfo_incor.BehResp=info.BehResp(incor);
outinfo_incor.FirstSac=info.FirstSac(incor);
outinfo_incor.LastSac=info.LastSac(incor);
outinfo_incor.NumSac=info.NumSac(incor);
outinfo_incor.Index=info.Index(incor,:);


c=0;
d=0;
for i=1:size(indat,2)
    if info.BehResp(i)==1;
        c=c+1;
        outdat_cor{c}=indat{i};
        ptsout_cor(c)=pts(i);
    else
        d=d+1;
        outdat_incor{d}=indat{i};
        ptsout_incor(d)=pts(i);
    end
end
        
        

    