function clust_prep_tf(monkeyname)

% prepares gray data for cluster - loads proprod m-files and formats them
% for cluster

cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end

mkdir clustdat_tf
cd clustdat_tf
fid=fopen('cogctf_com','wt');
fid2=fopen('permcogctf_com','wt');
cd ..
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'02', '03'}
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'epochs_sl_ml.mat'));
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        days(i).name
        sess
        cd ..
        cd ..
        cd ..

        cd clustdat_tf

        epochslnp=permute(epochsln,[2 1 3]);
        epochmlnp=permute(epochmln,[2 1 3]);
        epochslnpr = reshape(epochslnp(gchannels,:,:),[size(gchannels,2) size(epochslnp,2)*size(epochslnp,3)]);
        epochmlnpr = reshape(epochmlnp(gchannels,:,:),[size(gchannels,2) size(epochmlnp,2)*size(epochmlnp,3)]);
        save(strcat(monkeynamepre,'_',days(i).name,'_',sess{1},'e1','.dat'),'epochslnpr','-ascii');
        fprintf(fid,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e1',' %1.0f %1.0f %1.0f 20 11 200 100 richter\n'], size(gchannels,2), size(epochslnp,3), size(epochslnp,2))
        fprintf(fid2,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e1',' %1.0f %1.0f %1.0f 20 11 200 100 1000 richter\n'], size(gchannels,2), size(epochslnp,3), size(epochslnp,2))
        save(strcat(monkeynamepre,'_',days(i).name,'_',sess{1},'e2','.dat'),'epochmlnpr','-ascii');
        fprintf(fid,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e2',' %1.0f %1.0f %1.0f 20 11 200 100 richter\n'], size(gchannels,2), size(epochmlnp,3), size(epochmlnp,2))
        fprintf(fid2,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e2',' %1.0f %1.0f %1.0f 20 11 200 100 1000 richter\n'], size(gchannels,2), size(epochmlnp,3), size(epochmlnp,2))
        cd ..
        cd(days(i).name);
    end
    cd ..
end
fclose(fid);
fclose(fid2);
cd ..




