function [allFx2y,allFy2x] = summarizeGC(obj,ind,varargin)
% [r,ind] = get(cohC_B,'type',{'betapos34' [1 3]},'Number','rudeHist','pairhist','md');

Args = struct('file','GCcohInter.mat','freqs',[14 30],'rule',1,'prctile',[10 25 50 75 90],'figTit',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);


epoch = {'pre-sample' 'sample' 'delay 1' 'delay 2'};

frange = [Args.freqs(1) : Args.freqs(2)];


allFx2y = [];

allFy2x = [];

for c = 1 : length(ind)
    
    i = ind(c);
    pair = obj.data.Index(i,1);
    cd(obj.data.setNames{i})
    
    load(Args.file)
    
    if Args.rule == Day.session(1).rule
        s = 1;
    else
        s = 2;
    end
    
    allFx2y = cat(2,allFx2y,Day.session(s).Fx2y(:,pair,:));
    allFy2x = cat(2,allFy2x,Day.session(s).Fy2x(:,pair,:));
end

hh = figure;
if ~isempty(Args.figTit); set(hh,'Name',Args.figTit); end


for p =1 : 4
    
    subplot(2,4,p)
    
    plot(f,prctile(squeeze(allFx2y(:,:,p)),Args.prctile,2))
    
    subplot(2,4,4+p)
    plot(f,prctile(squeeze(allFy2x(:,:,p)),Args.prctile,2))
    
    
end
max1 = max(max(max([allFx2y(10:45,:,:) allFy2x(10:45,:,:)])));
for p = 1 : 4; subplot(2,4,p);title(epoch{p}); axis([10 45 0 max1]); subplot(2,4,p+4); axis([10 45 0 max1]);end
subplot(2,4,1); ylabel('PPC->PFC')
subplot(2,4,5); ylabel('PFC->PPC')
subplot(2,4,8);xlabel('Frequency [Hz]');ylabel('GC');

hh = figure;
if ~isempty(Args.figTit); set(hh,'Name',Args.figTit); end

avFx2y = squeeze(mean(allFx2y(frange,:,:),1));
avFy2x = squeeze(mean(allFy2x(frange,:,:),1));

diffav = avFx2y - avFy2x;
[h,ptest] = ttest(diffav);
subplot(2,1,1)

bar(mean(diffav));
hold on
errorbar(mean(diffav),std(diffav),'xr')
plot([1:4],h*3.*mean(diffav),'*')

set(gca,'XTickLabel',epoch)
ylabel('GC:PPC->PFC minus PFC->PPC')
%     avF = [avFx2y(:,1) avFy2x(:,1) avFx2y(:,2) avFy2x(:,2) avFx2y(:,3) avFy2x(:,3) avFx2y(:,4) avFy2x(:,4)];
avF = [mean(avFx2y,1)' mean(avFy2x,1)'];

[ptest,table] = anova2([avFx2y; avFy2x],length(ind),'off');
subplot(2,1,2)
bar(avF)
hold on
errorbar([0.85 1.85 2.85 3.85],mean(avFx2y,1),std(avFx2y,1),'xr')
errorbar([1.15 2.15 3.15 4.15],mean(avFy2x,1),std(avFy2x,1),'xr')
text(1,0.02,sprintf('pEpoch = %5.3g; pDir = %5.3g; pInter = %5.3g',ptest(1),ptest(2),ptest(3)))
legend('PPC->PFC','PFC->PPC')
set(gca,'XTickLabel',epoch)
ylabel('GC')
