%run from session directory


mtst = mtstrial('auto');
trials = mtsgetTrials(mtst,'stable','BehResp',1);
c=0;
% sample sizes to be tested
for sampsize = [25 50 75 100 125 150 175 200 225 250 275 300 325]
    c=c+1;
    % number of estimates i to be used for mean and std
    for i=1:100
        disp(sampsize)
        disp(i)
        index = ceil(rand(1,sampsize)*size(trials,2));
        dat = trials(index);
        % input channels of interest to calculate coherence
        [data,lplength] = mvar_test_lfp_prep(dat,[2 5]);
        params=struct('tapers',[2 3],'pad',2,'Fs',200,'err',0,'trialave',1);
        [Cmt(c,i,:,:),phi,S12,S1,S2,f]=coherencyc(squeeze(data(:,:,1)),squeeze(data(:,:,2)),params);
    end
end

mn = mean(Cmt,2);
mn = squeeze(mn);
vr = std(Cmt,0,2);
vr = squeeze(vr);
errorbar(mn(:,14),vr(:,14))
set(gca,'xtick',0:20);
set(gca,'xticklabel',{0 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400 425})
xlabel('resample size');
ylabel('Coherence');
title('Mean +/- std of ~20 Hz coherence')