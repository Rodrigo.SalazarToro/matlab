function clust_tf_prepro(monkeyname)
% run from data directory (contains monkey name folders)
cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'02', '03'}
        disp('processing session....')
        days(i).name
        sess
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        cd ..
        [epochmln,epochsln,epochml,epochsl] = lfp_epoch_tf(trials,1:size(neuroinfo.cortex,2),info2);
        disp('saving...')
        cd prepro
        save(strcat(days(i).name,sess{1},'epochs_sl_ml.mat'),'epochsl','epochml','epochsln','epochmln')
        cd ..
        cd ..
    end
    cd ..
end
cd ..






