function [outdat,ptsout,infoout,outrej,ptsrej,inforej,bad]=outlier_reject(info,dat,pts)


%OUTLIER_REJECT rejects trials outside graphically set limits
%
% usage: [...]=outlier_reject(info,dat,pts)
%
% Inputs:
%    info:    is a structure derived from ProcessSessionMTS.m
%    dat:     is a cell array of the lfp data created by nptReadStreamerFile.m
%    pts:     is a vector containing the number of points in each trial
%
% Outputs:
%
%    outdat:  is a cell array containing the lfp time series that did not
%    violate the visual criterion
%    ptsout:  is the vector pts with the bad trials removed
%    infoout: is the structure info with the entries for the bad trials removed
%    outrej: is a cell array of the rejected trials
%    ptsrej: is the vector of pts for the rejected trials
%    inforej: is the structure containing the sesison info for rejected trials
%    bad:  is a vecotr of the trial # of bad trials

% May 31 2007
% Craig Richter, Center for Complex Systems &
% Brain Sciences, FAU, Boca Raton, FL 33413


% for each channel, creates a plot of all trials overlayed

nchns=size(dat{1},1);
ntrls=size(dat,2);
c=0;
for i=1:size(dat{1},1)
    for k=1:size(dat,2)
        plot(dat{k}(i,:));
        hold on
    end
    % defines visual boundaries
    [X(i,:),Y(i,:)]=ginput(2);
    close
end
for i=1:nchns
    for j=1:ntrls
        if max(dat{j}(i,:))>=Y(i,1) || min(dat{j}(i,:))<=Y(i,2)
            c=c+1;
            bad(c)=j;
        end
    end
end


% determines bad trials pooled over all channels
ubad=zeros(1,size(dat,2));
bad=unique(bad);
% index of bad trials
ubad(bad)=1;

d=0;
e=0;
for i=1:ntrls
    if ubad(i)==0;
        d=d+1;
        outdat{d}=dat{i};
        ptsout(d)=pts(i);
        infoout.CueObj(d,1)=info.CueObj(i);
        infoout.CueLoc(d,1)=info.CueLoc(i);
        infoout.MatchObj1(d,1)=info.MatchObj1(i);
        infoout.MatchPos1(d,1)=info.MatchPos1(i);
        infoout.MatchObj2(d,1)=info.MatchObj2(i);
        infoout.MatchPos2(d,1)=info.MatchPos2(i);
        infoout.CueOnset(d,1)=info.CueOnset(i);
        infoout.CueOffset(d,1)=info.CueOffset(i);
        infoout.MatchOnset(d,1)=info.MatchOnset(i);
        infoout.BehResp(d,1)=info.BehResp(i);
        infoout.FirstSac(d,1)=info.FirstSac(i);
        infoout.LastSac(d,1)=info.LastSac(i);
        infoout.NumSac(d,1)=info.NumSac(i);
        infoout.Index(d,:)=info.Index(i,:);
    else
        e=e+1;
        outrej{e}=dat{i};
        ptsrej(e)=pts(i);
        inforej.CueObj(e,1)=info.CueObj(i);
        inforej.CueLoc(e,1)=info.CueLoc(i);
        inforej.MatchObj1(e,1)=info.MatchObj1(i);
        inforej.MatchPos1(e,1)=info.MatchPos1(i);
        inforej.MatchObj2(e,1)=info.MatchObj2(i);
        inforej.MatchPos2(e,1)=info.MatchPos2(i);
        inforej.CueOnset(e,1)=info.CueOnset(i);
        inforej.CueOffset(e,1)=info.CueOffset(i);
        inforej.MatchOnset(e,1)=info.MatchOnset(i);
        inforej.BehResp(e,1)=info.BehResp(i);
        inforej.FirstSac(e,1)=info.FirstSac(i);
        inforej.LastSac(e,1)=info.LastSac(i);
        inforej.NumSac(e,1)=info.NumSac(i);
        inforej.Index(e,:)=info.Index(i,:);
    end
end

