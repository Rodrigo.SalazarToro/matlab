function [cohPP,cohPF,cohInter,f] = checktuningGC(varargin)
% to be run inthe session directory

Args = struct('redo',0,'save',0,'remoteName',[],'locTuning',0,'ideTuning',0,'allCuesTuning',0,'surrogate',0,'rule',[],'porder',11,'freq',[1 : 100]);
Args.flags = {'redo','save','locTuning','ideTuning','allCuesTuning','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'redo','save','locTuning','ideTuning','allCuesTuning','surrogate'});

mtst = ProcessSession(mtstrial,'auto');
elecm = {'cohPP','cohPF','cohInter'};

rsFs = 200; % downsampling
plength = 400;
downsamp = (1000/rsFs);
lplength = plength/downsamp;

if ~isempty(Args.remoteName)
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
end

allloc = vecr(unique(mtst.data.CueLoc));
allobj = unique(mtst.data.CueObj);
if isempty(Args.rule); Args.rule = unique(mtst.data.Index(:,1)); end
if Args.locTuning
    ncues = 3;
    arg{1} = 'allloc(cues)';
    arg{2} = 'allobj';
    matfile = sprintf('locTuningRule%dGC.mat',Args.rule);
elseif Args.ideTuning
    ncues = 3;
    arg{1} = 'allloc';
    arg{2} = 'allobj(cues)';
    matfile = sprintf('ideTuningRule%dGC.mat',Args.rule);
elseif Args.allCuesTuning
    ncues = 9;
    cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    arg{1} = 'allloc(cueComb(1,cues))';
    arg{2} = 'allobj(cueComb(2,cues))';
    matfile = sprintf('allCuesTuningRule%dGC.mat',Args.rule);
else
    ncues = 1;
    arg{1} = 'allloc';
    arg{2} = 'allobj';
    matfile = sprintf('allCuesCombRule%dGC.mat',Args.rule);
end

for measure = 1 : length(elecm)
    fprintf('%s \n',elecm{measure});
    [channels,comb,CHcomb] = checkChannels(elecm{measure});
    for cues = 1 : ncues
        Trials = mtsgetTrials(mtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),modvarargin{:},'rule',Args.rule);
        ntrials{cues} = Trials;
        if ~isempty(Trials) & comb ~= -1
            [tdata,lplength] = lfpPcut(Trials,channels,'plength',plength,modvarargin{:}); % output in {periods} samples x trials x channel data
            Nr = size(tdata{1},2);
            Nl = size(tdata{1},1);
            for p = 1 : 4; data{p} = reshape(tdata{p},Nl * Nr,size(tdata{p},3))'; end
        else
            data = [];
            %             notCue = [notCue cues];
        end
        
        if comb == 2 && ~isempty(data) && length(unique(cell2mat(data))) ~= 1
            for cb = 1 : size(CHcomb,1)
                for p = 1 : 4
                    ch1 = find(CHcomb(cb,1) == channels);
                    ch2 = find(CHcomb(cb,2) == channels);
                    if ~isempty(Args.remoteName)
                        
                        data1{p+(cb-1)*4} = data{p}([ch1 ch2],:);
                        
                    else
                        [pp,cohe,Session.coh(cues).Fx2y(cb,:,p),Session.coh(cues).Fy2x(:,cb,p),Session.coh(cues).Fxy(:,cb,p),rp]= pwcausalrp(data{p}([ch1 ch2],:),size(data{p},2)/Nl,Nl,Args.porder,rsFs,Args.freq);
                        if Args.surrogate
                            [Session.coh(cues).x2yProb(cb,:,:,p),Session.coh(cues).y2xProb(:,:,cb,p),Session.coh(cues).xyProb(:,:,cb,p)] = GCcohsurrogate(data{p}([ch1 ch2],:),size(data{p},2)/Nl,Nl,Args.porder,rsFs,Args.freq);
                        end
                    end
                end
                
            end
            clear data
            if ~isempty(Args.remoteName)
                fprintf('Data sent to %s on %g %g %gth at %gh%g %g sec.\n',Args.remoteName,clock)
                [pp,cohe,Fx2y,Fy2x,Fxy,rp]= dfeval(@pwcausalrp,data1,repmat({size(data1{p},2)/Nl},1,length(data1)),repmat({Nl},1,length(data1)),repmat({Args.porder},1,length(data1)),repmat({rsFs},1,length(data1)),repmat({Args.freq},1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                if Args.surrogate
                    [x2yProb,y2xProb,xyProb] = dfeval(@GCcohsurrogate,data1,repmat({size(data1{p},2)/Nl},1,length(data1)),repmat({Nl},1,length(data1)),repmat({Args.porder},1,length(data1)),repmat({rsFs},1,length(data1)),repmat({Args.freq},1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                end
                clear data1
                if Args.surrogate
                var = {'Fx2y','Fy2x','Fxy','x2yProb','y2xProb','xyProb'};
                else
                         var = {'Fx2y','Fy2x','Fxy'};
                end
                for cb = 1 : size(CHcomb,1)
                    for p = 1 : 4
                        for v = 1 : length(var)
                            if ~isempty(findstr(var{1},'Prob'))
                                eval(sprintf('Session.coh(cues).%s(cb,:,:,p) = %s{p+(cb-1)*4};',var{v},var{v}));
                            else
                                eval(sprintf('Session.coh(cues).%s(cb,:,p) = %s{p+(cb-1)*4};',var{v},var{v}));
                            end
                        end
                        f = Args.freq;
                        
                    end
                end
                clear 'Fx2y' 'Fy2x' 'Fxy' 'x2yProb' 'y2xProb' 'xyProb'
            end
        else
            Session.coh(cues).Fx2y = []; Session.coh(cues).Fy2x = []; Session.coh(cues).Fxy = []; Session.coh(cues).x2yProb = []; Session.coh(cues).y2xProb = []; Session.coh(cues).xyProb = [];%coh(cues).confC = []; coh(cues).phierr = []; coh(cues).Cerr = [];
        end
    end
    
    eval(sprintf('%s.Session = Session;',elecm{measure}));
    clear coh
    
end

f = Args.freq;
if Args.save
    [pdir,cdir] = getDataDirs('lfp','relative','CDNow');
    options = varargin;
    save(matfile,'cohPP','cohPF','cohInter','f','options','ntrials');
    cd(cdir)
end









