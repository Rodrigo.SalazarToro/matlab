function mkGCdirStat(coh,ind,varargin)


Args = struct('redo',0','save',0,'firstBlockOnly',0,'notfromFiles',0,'generalized',0,'selfSur',0,'InterAreaOnly',0);
Args.flags = {'redo','save','firstBlockOnly','notfromFiles','generalized','selfSur','InterAreaOnly'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});


index = coh.data.Index;
mainDir = 'GCdirStat';

for i = vecr(ind)
    %                     coh.data.setNames{i}
    cd(coh.data.setNames{i}); % day directory
    pairg = index(i,10:11);
    if isempty(strfind(coh.data.setNames{i},'betty')); Args.ML = false; else Args.ML = true; end
    for r = 1 : 2
        %index(:,[10 11])
        cd(coh.data.setNames{i});
        clear C phi t
        if isempty(nptDir(mainDir))
            mkdir(mainDir)
        end
        
        rfile = nptDir('rules.mat');
        if ~isempty(rfile);
            rules = load('rules.mat'); s = find(rules.r == r) + 1;
        elseif Args.ML;
            s = 1;
        end
        
        cd(coh.data.setNames{i});
        cd(mainDir)
        if Args.generalized
            if Args.InterAreaOnly
                 matfile = sprintf('GCintergeneralRule%d.mat',r);
            else
            matfile = sprintf('GCgeneralRule%d.mat',r);
            end
        elseif Args.selfSur
            matfile = sprintf('GCinterg%04.0fg%04.0fRule%dSur.mat',pairg(1),pairg(2),r);
        else
            matfile = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),r);
        end
        file = nptDir(matfile);
        cd ..
        
        if ~isempty(nptDir(sprintf('session0%d',s))) && (isempty(file) || Args.redo)
            
            cd(sprintf('session0%d',s))
            NeuroInfo = NeuronalChAssign;
            if Args.generalized
                [ch,~,~] = checkChannels('cohInter');
                if Args.InterAreaOnly
                    [ch1,~,~] = checkChannels('cohPP');
                    [ch2,~,~] = checkChannels('cohPF');
                    if max(ch1) > max(ch2)
                        sch = ch2;
                    else
                        sch = ch1;
                    end
                    ch1 = [1 : length(sch)];
                end
                
            else
                for c =1  : 2; ch(c) = find(pairg(c) == NeuroInfo.groups); end
            end
            mts = mtstrial('auto');
            errortrials = false;
            
            if s ==1
                if Args.firstBlockOnly
                    trials = [];
                    block = 1;
                    while isempty(trials)
                        trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',r,'ML',modvarargin{:},'block',block);
                        block = block + 1;
                    end
                else
                    trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',r,'ML',modvarargin{:});
                end
            else
                trials = mtsgetTrials(mts,'stable','BehResp',1,modvarargin{:});
            end
            if isempty(trials)
                
                errortrials = true;
            else
                if Args.notfromFiles
                    [data,lplength] = lfpPcut(trials,ch,modvarargin{:});
                else
                    [data,lplength] = lfpPcut(trials,ch,'fromFiles',modvarargin{:});
                end
                
            end
            
            
            cd ..
            if ~errortrials
                Fdiff = zeros(100,4);
                pvalues = zeros(100,12,4);
                Fx2y = zeros(100,4);
                Fy2x = zeros(100,4);
                Fxy = zeros(100,4);
                for p = 1 : 4
                    if Args.generalized
                        if Args.InterAreaOnly
                            [~,pvalues(:,:,p),levels,f] = GCdirection_test(data{p},'onlyInter',ch1,modvarargin{:});
                        else
                            [~,pvalues(:,:,p),levels,f] = GCdirection_test(data{p},modvarargin{:});
                        end
                    elseif Args.selfSur
                        [~,pvalues(:,:,p),levels,f,~,~,~] = GCdirection_test(data{p},modvarargin{:});
                    else
                        [Fdiff(:,p),~,levels,f,Fx2y(:,p),Fy2x(:,p),Fxy(:,p)] = GCdirection_test(data{p},'nrep',1,modvarargin{:});
                    end
                end
                
                cd(coh.data.setNames{i});
                if Args.save
                    sprintf('%s %s',pwd,matfile)
                    cd(mainDir)
                    save(matfile,'Fdiff','pvalues','f','levels','Fx2y','Fy2x','Fxy')
                    cd ..
                end
                
            end
            
        end
        
    end
    cd ..
end