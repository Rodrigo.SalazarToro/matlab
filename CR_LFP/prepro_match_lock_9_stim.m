function prepro_match_lock_9_stim(monkeyname)
% run from data directory (contains monkey name folders)
cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'02', '03'}
        disp('processing session....')
        days(i).name
        sess
        cd(strcat('session',sess{1}))
        mtst = mtstrial('auto');
        trials = mtsgetTrials(mtst,'stable','BehResp',1);
        [neuroinfo] = NeuronalChAssign
        info2 = ProcessSessionMTS;
        a=0;
        Objs = unique(info2.CueObj);
        Locs = unique(info2.CueLoc);
        for j=1:3
            for k=1:3
                a=a+1;
                trials = mtsgetTrials(mtst,'stable','BehResp',1,'CueObj',Objs(j),'CueLoc',Locs(k))
                [data_sepstim_ml{a},lplength] = lfpPcut_mod(trials,1:size(neuroinfo.cortex,2));
            end
        end
        disp('Total Channels...')
        gchannels = 1:size(neuroinfo.cortex,2)
        [gchannels,comb,CHcomb] = checkChannels('cohInter');
        disp('good channels...')
        gchannels
        mkdir prepro
        cd prepro
        disp('saving...')
        save(strcat(days(i).name,sess{1},'prepro.mat'),'trials','info2','mtst','neuroinfo','gchannels','Objs','Locs')
        save(strcat(days(i).name,sess{1},'_match_locked.mat'),'data_sepstim_ml','lplength')
        cd ..
        cd ..
    end
    cd ..
end
cd ..






