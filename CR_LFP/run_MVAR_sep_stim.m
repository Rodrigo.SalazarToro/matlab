function run_MVAR_sep_stim(monkeyname)
% run from data directory (contains monkey name folders)
cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'02', '03'}
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        load(strcat(days(i).name,sess{1},'_4_epochs.mat'));
        disp('performing AR spectral analysis....')
        days(i).name
        sess
        for q=1:size(data_sepstim,1);
            for j=1:size(data_sepstim,2)
                dattmp = data_sepstim{q,j};
                dattmpp = permute(dattmp,[3 1 2]);
                dattmpprsh = reshape(dattmpp,[size(dattmpp,1) size(dattmpp,2)*size(dattmpp,3)]);
                [P(q,j,:,:),cohe,Fx2y,Fy2x,Fxy,rp]=pwcausalrp(dattmpprsh(gchannels,:),size(dattmp,2),size(dattmp,1),11,200,1:100);
                [C(q,j,:,:,:)]=yc2mat(size(gchannels,2),100,cohe);
                [RP(q,j,:,:,:)]=yc2mat(size(gchannels,2),100,rp);
                [Ginst(q,j,:,:,:)]=yc2mat(size(gchannels,2),100,Fxy);
                [G(q,j,:,:,:)]=yc2mat(size(gchannels,2),100,Fx2y,Fy2x);
            end
        end
        cd ..
        mkdir mvar
        cd mvar
        save(strcat(days(i).name,sess{1},'_9_stim_4_epoch_spect.mat'),'G','C','P','RP','Ginst');
        cd ..
        cd ..
        clear P C RP Ginst G data data_sepstim info2 gchannels lplength mtst neuroinfo trials
    end
    cd ..
end
cd ..


