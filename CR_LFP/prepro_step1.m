function prepro_step1

% creates base files and runs preprocess1 and saves
% run from work directory

cd gray
cd clark
cd Days

str = dir;
for j=1:length(str)-2
    idd{j}=str(j+2).name;
end


for i=1:size(idd,2)
    i
    cd(idd{i});
    mkdir('base');
    cd session02
    [info2,neuroinfo,data,num_channels,sampling_rater,scan_order,pointsr]=preprocess1
    str = dir
    id = str_loader(str, 'ini');
    id = id{1}(1:13);
    cd ..
    cd base
    save(strcat(id,'base'),'info2', 'neuroinfo', 'data', 'num_channels', 'sampling_rater', 'scan_order', 'pointsr', 'id')
    clear info2 neuroinfo data num_channels sampling_rater scan_order pointsr
    cd ..
    cd session03
    [info2,neuroinfo,data,num_channels,sampling_rater,scan_order,pointsr]=preprocess1
    str = dir
    id = str_loader(str, 'ini');
    id = id{1}(1:13);
    cd ..
    cd base
    save(strcat(id,'base'),'info2', 'neuroinfo', 'data', 'num_channels', 'sampling_rater', 'scan_order', 'pointsr', 'id')
    clear info2 neuroinfo data num_channels sampling_rater scan_order pointsr
    cd ..
    cd ..
end

cd ..
cd ..
cd ..

