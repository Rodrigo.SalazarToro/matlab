function [celray] = str_loader(struct, pat);

c=1;
celray={};
for i=1:length(struct)
    if length(regexp(struct(i).name,pat))~=0
        celray{c}=struct(i).name;
        c=c+1;
    end
end

        