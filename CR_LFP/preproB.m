function preproB(monkeyname)
%run from data directory (contains monkey name folders)
cd (strcat('/Applications/MATLAB74/work/Montana_Monkey_Data/',monkeyname,'/'))
 if strcmp(monkeyname,'betty')==1
     monkeynamepre='be';
     days = nptDir('*09*');
 end
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'01'}
        disp('processing session....')
        days(i).name
        sess
        cd(strcat('session',sess{1}))
        mts = mtstrial('auto', 'save','ML','RTfromML');
        trials = mtsgetTrials(mts,'stable','BehResp',1,'ML');
        [neuroinfo] = NeuronalChAssign;
        %info2 = ProcessSessionMTS;
        [data,lplength] = lfpPcut(trials,1:size(neuroinfo.cortex,2));
        a=0;
        Objs = unique(mts.data.CueObj);
        Locs = unique(mts.data.CueLoc);
        for j=1:3
            for k=1:3
                a=a+1;
                trials = mtsgetTrials(mts,'stable','BehResp',1, 'ML', 'CueObj',Objs(j),'CueLoc',Locs(k))
                if a==1;
                    [data_sepstim,lplength] = lfpPcut(trials,1:size(neuroinfo.cortex,2));
                else
                    [data_sepstim_tmp,lplength] = lfpPcut(trials,1:size(neuroinfo.cortex,2));
                    data_sepstim=cat(1,data_sepstim,data_sepstim_tmp);
                end
            end
        end
        disp('Total Channels...')
        gchannels = 1:size(neuroinfo.cortex,2)
        [gchannels,comb,CHcomb] = checkChannels('cohInter');
        disp('good channels...')
        gchannels
        mkdir prepro
        cd prepro
        disp('saving...')
        save(strcat(days(i).name,sess{1},'prepro.mat'),'trials','mts','neuroinfo','gchannels','Objs','Locs')
        save(strcat(days(i).name,sess{1},'_4_epochs.mat'),'data','data_sepstim','lplength')
        cd ..
        cd ..
    end
    cd ..
end
cd ..






