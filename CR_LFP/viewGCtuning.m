function [meandata,varargout] = viewGCtuning(varargin)
% meandata{direc,rule,p}

Args = struct('pairedAnalysis',0,'nsur',1,'plot','mean','norm',0,'zscore',0,'nbins',30,'diffFromPre',0,'viewInd',0);
Args.flags = {'pairedAnalysis','norm','zscore','diffFromPre','viewInd'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'pairedAnalysis','norm','zscore','diffFromPre'});

load switchdays.mat

wind = {'presample','sample','delay1','delay2'};
sdir = pwd;
path = uigetdir(pwd,'Select the MVAR_Results folder');
cd(path);
meanbeta = load('gc/clark_mean.mat');
surr = load('gc/clark_01_mean_perm.mat');
rules = {'IDENTITY' 'LOCATION'};
cd(sdir)
dlabel = {'k2l' 'l2k'};
%% std analysis
for d= 1 : 21;cd(days{d});for s = 2 : 3; cd(sprintf('session0%d',s)); mts = mtstrial('auto'); rule(d,s-1) = unique(mts.data.Index(:,1)); cd ..; end; cd .. ; end;
meandata = cell(2,2,4);
surdata = cell(2,2,4);
stddata = cell(2,2,4);
for d = 1 : 21
    allthepairs = [];for pp = 1 : 36; if ~isempty(surr.pairnames{d,1,1,pp}); allthepairs = [allthepairs;surr.pairnames{d,1,1,pp}]; end; end
    for pairs = 1 : size(allthepairs,1)
        if ~isempty(strfind(allthepairs(pairs,:),'P')) && ~isempty(strfind(allthepairs(pairs,:),'F'))
            for s = 1 : 2
                data1 = squeeze(meanbeta.meank2l(d,s,:,:,pairs));
                data2 = squeeze(meanbeta.meanl2k(d,s,:,:,pairs));
                if Args.norm
                    norm1 = max(max(data1));
                    norm2 = max(max(data2));
                else
                    norm1 = 1;
                    norm2 = 1;
                end
                cross = checkForSig(data1,data2,Args.nsur*squeeze(surr.gcmean(d,s,:,pairs)),modvarargin{:});
                data1 = data1/norm1;
                data2 = data2/norm2;
                for p = 1 : 4
                    if Args.zscore
                        
                        sdata1 = (data1(p,:) - mean(data1(p,:)))/ std(data1(p,:));
                        sdata2 = (data2(p,:) - mean(data2(p,:)))/ std(data2(p,:));
                    else
                        sdata1 = data1(p,:);
                        sdata2 = data2(p,:);
                    end
                    
                    if Args.pairedAnalysis && sum(cross) ~= 0
                        
                        newdata1 = sdata1;
                        newdata2 = sdata2;
                        meandata{1,rule(d,s),p} = [meandata{1,rule(d,s),p}; newdata1];
                        meandata{2,rule(d,s),p} = [meandata{2,rule(d,s),p}; newdata2];
                        stddata{2,rule(d,s),p} = [stddata{2,rule(d,s),p}; std(data2(p,:))];
                        stddata{1,rule(d,s),p} = [stddata{1,rule(d,s),p}; std(data1(p,:))];
                    end
                    if ~Args.pairedAnalysis
                        if cross(1)
                            newdata1 = sdata1;
                            meandata{1,rule(d,s),p} = [meandata{1,rule(d,s),p}; newdata1];
                            stddata{1,rule(d,s),p} = [stddata{1,rule(d,s),p}; std(data1(p,:))];
                        end
                        if cross(2)
                            newdata2 = sdata2;
                            meandata{2,rule(d,s),p} = [meandata{2,rule(d,s),p}; newdata2];
                            stddata{2,rule(d,s),p} = [stddata{2,rule(d,s),p}; std(data2(p,:))];
                        end
                    end
                end
            end
        end
    end
end


%%
if Args.viewInd
    for r = 1 : 2
        for direc = 1 : 2
            hh(r) = figure;
            thedata = squeeze(meandata(direc,r,:));
            nsig = size(thedata{1},1);
            for pair = 1 : nsig
                maxy = [];
                for ref = 1 : 4
                    [b,ix] = sort(thedata{ref}(pair,:),2);
                   
                    for p =1 : 4
                         rdata = thedata{p}(pair,ix); 
                        subplot(4,4,(ref -1)* 4 + p)
                        plot(rdata)
                        maxy = [maxy max(rdata)];
                        if ref == 1; title(wind{p}); end
                        if p == 1; ylabel(sprintf('ref. %s',wind{ref})); end
                    end
                end
                for sb = 1 : 16; subplot(4,4,sb); ylim([0 max(maxy)]); end
                xlabel('Stim #')
                ylabel('GCvalue')
                set(hh,'Name',sprintf('Rule %s; direc %s; ',rules{r},dlabel{direc}))
                pause
            end
        end
    end
end
%%
comp = [1 2; 1 3; 1 4];
maxy = [];
for r = 1 : 2
    h = figure;
    set(h,'Name',rules{r})
    for direc = 1 : 2
        
        for p = 1 : 4
            
            if strmatch(Args.plot,'mean')
                da{p} = reshape(meandata{direc,r,p},1,9*size(meandata{direc,r,p},1));
                le = 50;
            else
                da{p} = stddata{direc,r,p};
                le = 5;
            end
            if Args.diffFromPre & p ~= 1
                da{p} = (da{p} - da{1});% ./ da{1};
            end
            maxda(p) = max(da{p});
            minda(p) = min(da{p});
        end
        
        range = [min(minda) : (max(maxda) - min(minda)) / Args.nbins : max(maxda)];
        te = [max(maxda)/2 le];
        if Args.diffFromPre; da{1} = da{1} - da{1}; end
        ptiles = prctile(da{1},[95 99]);
        
        for p =  1: 4
            subplot(4,2,(p-1)*2+direc)
            
            [n,xout] = hist(da{p},range);
            plot(xout,n)
            me = mean(da{p});
            md = median(da{p});
            st = std(da{p});
            hold on
            line([me me],[0 max(n)],'Color','r')
            line([md md],[0 max(n)],'Color','r','LineStyle','--')
            line([ptiles(1) ptiles(1)],[0 max(n)],'Color','k')
            line([ptiles(2) ptiles(2)],[0 max(n)],'Color','k','LineStyle','--')
            if Args.diffFromPre && p ~= 1; maxy = [maxy max(n)];elseif  ~Args.diffFromPre; maxy = [maxy max(n)]; end
            
            text(te(1),te(2),sprintf('n=%d',size(meandata{direc,r,p},1)))
            title(sprintf('%s; %s',wind{p},dlabel{direc}))
        end
    end
    for sb = 1 : 8; subplot(4,2,sb); axis([min(range) max(range) 0 max(maxy)]); grid on;end
    if strmatch(Args.plot,'mean')
        xlabel('z-score')
        ylabel('# pairs*9')
    elseif Args.diffFromPre
        xlabel('std - std(presample)')
        ylabel('# pairs')
    else
        xlabel('std')
        ylabel('# pairs')
    end
    legend('distr','mean','median','95th ptiles from presample','99th ptiles from presample')
end


%%

function cross = checkForSig(gc1,gc2,sur,varargin)

Args = struct('wind',[1:4]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});
%format gc : 4x9; sur : 4;

mgc1 = reshape(gc1(Args.wind,:),length(Args.wind)*9,1);
mgc2 = reshape(gc2(Args.wind,:),length(Args.wind)*9,1);
msur = repmat(sur(Args.wind),9,1);


cross(1) = ~isempty(find(mgc1 > msur));
cross(2) = ~isempty(find(mgc2 > msur));


function rankOrder()


