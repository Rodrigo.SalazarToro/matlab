function [info2,neuroinfo,data,num_channels,sampling_rater,scan_order,pointsr]=preprocess1

%run from session folder

%Preprocess
% load dat
[info2,data,num_channels,sampling_rate,scan_order,points]=loader;
% resample
[data,pointsr,sampling_rater]=resampler(data,sampling_rate);
% Rem60 noise removal
data = gray_noise_remove(data,5,9);
% load Neuroinfo
[neuroinfo] = NeuronalChAssign;




