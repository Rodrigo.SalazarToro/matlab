function clust_prep_4_epochs(monkeyname)

% prepares gray data for cluster - loads proprod m-files and formats them
% for cluster - tf planes

cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end

mkdir clustdat
cd clustdat
fid=fopen('perm_com','wt');
cd ..
for i=1:size(days,1)
    cd(days(i).name);
    for sess = {'02', '03'}
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        days(i).name
        sess
        cd ..
        cd ..
        cd ..

        cd clustdat

        for j=1:size(data,2)
            dattmp = data{j};
            dattmpp = permute(dattmp,[3 1 2]);
            dattmpprsh = reshape(dattmpp(gchannels,:,:),[size(gchannels,2) size(dattmpp,2)*size(dattmpp,3)]);
            save(strcat(monkeynamepre,'_',days(i).name,'_',sess{1},'e',num2str(j),'_1.dat'),'dattmpprsh','-ascii');
            fprintf(fid,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e' num2str(j) ' 1',' %1.0f %1.0f %1.0f 11 200 100 1000 richter\n'], size(gchannels,2), size(dattmpp,3), size(dattmpp,2))
        end

        cd ..
        cd(days(i).name);
    end
    cd ..
end
fclose(fid);
cd ..




