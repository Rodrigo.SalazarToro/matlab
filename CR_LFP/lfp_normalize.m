function O = lfp_normalize(data,opt);

%LFP_NORMALIZE Estimates and removes the mean.
% usage: [...]=lfp_normalize(DATA,OPT)
% Inputs:
%          DATA  a 3D array with dimensions time x channels x
%          trials
%          OPT   is an integer valued flag that indicates the type
%          of normalization to be done.
%          OPT=1 => subtract estimated mean
%          OPT=2 => subtract estimated mean and divide with
%          standard deviation
%          OPT=3 => subtract ensamble mean using linear regression
% Outputs: 
%         [O]=lfp_normalize(...) returns the normalized data
%         

% 03 November 2003
% Anders Ledberg, Center for Complex Systems &
% Brain Sciences, FAU, Boca Raton, FL 33413 

% Check that enough inputs are given

if (nargin < 1)
  'error: too few arguments' 
  'usage: '
  help lfp_normalize
  return
end

if (nargin==1)
  opt=1;
end


if (opt==1)
  avg  = mean( data, 3);
  avg  = repmat(avg,[1 1 size(data,3)]);
  O = data - avg;
elseif (opt==2) % remove mean and divide with temporal standard deviation
  % remove mean
  avg  = mean( data, 3);
  avg  = repmat(avg,[1 1 size(data,3)]);
  O = data - avg;
  % divide with standard deviation
  % loop over channels
  for n = 1 : size(O,2)
    sd_t        = std( squeeze(O(:,n,:)) );
    sd_t        = repmat( sd_t, size(O,1), 1);
    O(:,n,:) = squeeze( O(:,n,:) ) ./ sd_t;
  end
elseif (opt==3) % remove mean using regression, allows for amplitude variability

  % set up output
  O= zeros(size(data));
  % 'design' matrix
  X=zeros(size(data,1),2);
  % loop over channels
  for c = 1: size(data,2);
    tmp=squeeze(data(:,c,:));
    me=mean(tmp');
    X(:,1)=me';
    X(:,2)=[gradient(me)]';
    beta=pinv(X'*X)*X'*tmp;
    O(:,c,:)=tmp-X*beta;
  end
end
return;
