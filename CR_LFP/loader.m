function [info,data,num_channels,sampling_rate,scan_order,points]=loader;

%loads session information and lfp time series as cell array
%run from within session folder

[info] = ProcessSessionMTS
str=dir('lfp');
strings = str_loader(str, 'lfp');
cd lfp
for i=1:size(strings,2);
    [data{i},num_channels,sampling_rate,scan_order,points(i)]=nptReadStreamerFile(strings{i});  
end
cd ..

