function [Fx2y,Fy2x,Fxy,freq]=GCepoch(data1,data2,params,porder)

% Using Geweke's method to compute the causality between any two channels
% data1 (in form samples x trials) -- required

%       data2 (in form samples x trials) -- required
%   x is a two dimentional matrix whose each row is one variable's time series
%   Nr is the number of realizations,
%   Nl is the length of every realization
%      If the time series have one ralization and are stationary long, just let Nr=1, Nl=length(x)
%   porder is the order of AR model
%   fs is sampling frequency
%   freq is a vector of frequencies of interest, usually freq=0:fs/2
%
%   Fx2y is the causality measure from x to y
%   Fy2x is causality from y to x
%   Fxy is instantaneous causality between x and y
%        the order of Fx2y/Fy2x is 1 to 2:L, 2 to 3:L,....,L-1 to L.  That is,
%        1st column: 1&2; 2nd: 1&3; ...; (L-1)th: 1&L; ...; (L(L-1))th: (L-1)&L.

%   pp is power spectrum
%   rp is relative phase

fs = params.Fs;
freq = [1 : 50];
Nwin = size(data1,1); % number of samples in window
Nr = size(data1,2);

%% initialisation of the data
datawin(1,:) = reshape(data1,1, Nwin * Nr);
datawin(2,:) = reshape(data2,1, Nwin * Nr);

[~,~,Fx2yt,Fy2xt,Fxyt,~]=pwcausalrp(datawin,Nr,Nwin,porder,fs,freq);
Fx2y = Fx2yt;
Fy2x = Fy2xt;
Fxy = Fxyt;
