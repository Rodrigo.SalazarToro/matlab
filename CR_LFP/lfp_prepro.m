function []=lfp_prepro

% Preprocesses all the data for session02 and session03 for the all dates
% in the directroy from which it is run

days = nptDir('0*');

for i=1:size(days,1)
    cd(days(i).name)
    for sess = {'session02' 'session03'};
        cd(cell2mat(sess))
        disp(days(i).name)
        disp(sess)
        [neuroinfo] = NeuronalChAssign;
        [info] = ProcessSessionMTS;
        mtst = mtstrial('auto');
        trials = mtsgetTrials(mtst,'stable','BehResp',1);
        [data,lplength] = lfpPcut(trials,1:size(neuroinfo.cortex,2))
        mkdir prepro
        cd prepro
        save(strcat(days(i).name,(sess{1}(:,8:9)),'prepro'), 'data', 'info', 'lplength',...
            'mtst', 'neuroinfo', 'trials');
        clear 'data', 'info', 'lplength',...
            'mtst', 'neuroinfo', 'trials';
        cd ..
        cd ..
    end
    cd ..
end


