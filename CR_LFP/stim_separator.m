function dat=stim_separator(trials,data,info2)

count2=0;
stim_type=info2.CueObj(trials');
stim_loc=info2.CueLoc(trials');
ust=unique(stim_type);
usl=unique(stim_loc);
for i=1:3
    Isl = find(stim_loc==usl(i));
    for j=1:3
        count2=count2+1;
        Ist = find(stim_type==ust(j));
        Iint=intersect(Isl,Ist);
        for k=1:4
            dat{count2,k}=data{k}(:,Iint,:);
        end
    end
end

