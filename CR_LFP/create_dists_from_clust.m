function create_dists_from_clust(monkeyname)
% run from data directory (contains monkey name folders)

% creates a cell array containing cell arrays from all stim/epoch
% combinations - this is to save time parsing the text files.

cd(monkeyname)
sess={'02','03'};
a=0;
b=0;
for n=1:2
    cd clust_results_perm_4_epochs_9_stim
    clustresC = nptDir('*cohist*');
    clustresGC = nptDir('*gchist*');
    for i=1:4
        for j=1:9
            a=a+1;
            if regexp(clustresC(a).name,sess{n})
                pairsCtmp=load_perm_clust(clustresC(a).name);
                pairs_all_C{j,i}=pairsCtmp;
            end
        end
    end
    for i=1:4
        for j=1:9
            b=b+1;
            if regexp(clustresGC(b).name,sess{n})
                pairsGCtmp=load_perm_clust(clustresGC(b).name);
                pairs_all_GC{j,i}=pairsGCtmp;
            end
        end
    end
    cd ..
    cd 060428
    cd(strcat('session',sess{n}));
    mkdir perms
    cd perms
    save(strcat('060428',sess{n},'_dists.mat'),'pairs_all_C','pairs_all_GC');
    clear pairsCtmp pairs_all_C pairsGCtmp pairs_all_GC
    cd ..
    cd ..
    cd ..
end



