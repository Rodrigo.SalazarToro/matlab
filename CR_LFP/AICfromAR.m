function [aic,varargout] = AICfromAR(Z2,nparams,windLength,varargin)




aic = log(det(Z2)) + (2*nparams / windLength);