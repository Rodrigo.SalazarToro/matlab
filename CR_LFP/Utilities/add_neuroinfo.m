str = dir;
for j=1:length(str)-2
    idd{j}=str(j+2).name;
end

for i=1:size(idd,2)
    i
    cd(idd{i});
    cd Base
    load(strcat('clark',idd{i},'02base'),'neuroinfo');
    cd ..
    cd preprod
    save(strcat('clark',idd{i},'02preprod_e1'),'neuroinfo','-append');
    save(strcat('clark',idd{i},'02preprod_e2'),'neuroinfo','-append');
    clear neuroinfo
    cd ..
    cd Base
    load(strcat('clark',idd{i},'03base'),'neuroinfo');
    cd ..
    cd preprod
    save(strcat('clark',idd{i},'03preprod_e1'),'neuroinfo','-append');
    save(strcat('clark',idd{i},'03preprod_e2'),'neuroinfo','-append');
    clear neuroinfo
    cd ..
    cd ..
end
    