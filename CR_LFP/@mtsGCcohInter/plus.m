function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
    % check if second input is the right kind of object
    if(~isa(q,classname))
        % both inputs are not the right kind of object so create empty
        % object and return it
        r = feval(classname);
    else
        % second input is the right kind of object so return that
        r = q;
    end
else
    if(~isa(q,classname))
        % p is the right kind of object but q is not so just return p
        r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
    else
        % both p and q are the right kind of objects so add them
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        %         r.data.spiketimes = [p.data.spiketimes; q.data.spiketimes];
        % 		r.data.spiketrials = [p.data.spiketrials; q.data.spiketrials];
        
        
        q.data.Index(:,2) = q.data.Index(:,2) + p.data.Index(end,2);
        
        
        r.data.Index = [p.data.Index; q.data.Index];
        
        
        %         r.data.Type = [p.data.Type; q.data.Type];
        r.data.betamodIDE = [p.data.betamodIDE; q.data.betamodIDE];
        r.data.betamodLOC = [p.data.betamodLOC; q.data.betamodLOC];
        
        r.data.gammamodIDE = [p.data.gammamodIDE; q.data.gammamodIDE];
        r.data.gammamodLOC = [p.data.gammamodLOC; q.data.gammamodLOC];
        
%         r.data.PtoFbetaIDE = [p.data.PtoFbetaIDE; q.data.PtoFbetaIDE];
%         r.data.FtoPbetaIDE = [p.data.FtoPbetaIDE; q.data.FtoPbetaIDE];
%         r.data.PtoFbetaIDESur = [p.data.PtoFbetaIDESur; q.data.PtoFbetaIDESur];
%         r.data.FtoPbetaIDESur = [p.data.FtoPbetaIDESur; q.data.FtoPbetaIDESur];
%        
%          r.data.PtoFbetaLOC = [p.data.PtoFbetaLOC; q.data.PtoFbetaLOC];
%         r.data.FtoPbetaLOC = [p.data.FtoPbetaLOC; q.data.FtoPbetaLOC];
%         r.data.PtoFbetaLOCSur = [p.data.PtoFbetaLOCSur; q.data.PtoFbetaLOCSur];
%         r.data.FtoPbetaLOCSur = [p.data.FtoPbetaLOCSur; q.data.FtoPbetaLOCSur];
%         
        % useful fields for most objects
        r.data.numSets = p.data.numSets + q.data.numSets;
        r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};
        
        % add nptdata objects as well
        r.nptdata = plus(p.nptdata,q.nptdata);
    end
end
