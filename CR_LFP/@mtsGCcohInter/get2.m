function [r,varargout] = get(obj,varargin)
% Index(:,1) = pair number
% Index(:,2) = day number
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% all of the above is either 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules
% Index(:,7) = beta differences between the two rules (0 for no, 1 for delay 1, 2 for delay 2 and 3 for both)
% Index(:,8) = gamma differences between the two rules (0 for no, 1 for
% fix, 2 for delay 2 and 3 for both)
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'betaIncr',[],'betaDecr',[],'gammaIncr',[],'gammaDecr',[],'betaRuleEffect',[],'gammaRuleEffect',[],'pairhist',[],'rudeHist',0,'IDEruleTuning',[],'LOCruleTuning',[],'snr',1.8,'alphaPeak',[],'DC',[],'removeGr',[],'type',[]);
Args.flags = {'Number','ObjectLevel','rudeHist'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    
    if ~isempty(Args.betaIncr)
        rtemp1 = [];
        for nf = 1 : length(Args.betaIncr)
            
            rtemp1 = [rtemp1; find(obj.data.Index(:,3) == Args.betaIncr(nf))];
        end
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.betaDecr)
        rtemp2 = [];
        for nf = 1 : length(Args.betaDecr)
            rtemp2 = [rtemp2; find(obj.data.Index(:,4) == Args.betaDecr(nf))];
        end
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaIncr)
        rtemp3 = [];
        for nf = 1 : length(Args.gammaIncr)
            rtemp3 = [rtemp3; find(obj.data.Index(:,5) == Args.gammaIncr(nf))];
        end
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaDecr)
        rtemp4 = [];
        for nf = 1 : length(Args.gammaDecr)
            rtemp4 = [rtemp4; find(obj.data.Index(:,6) == Args.gammaDecr(nf))];
        end
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.betaRuleEffect)
        rtemp5 = [];
        for nf = 1 : length(Args.betaRuleEffect)
            rtemp5 = [rtemp5; find(obj.data.Index(:,7) == Args.betaRuleEffect(nf))];
        end
    else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaRuleEffect)
        rtemp6 = [];
        for nf = 1 : length(Args.gammaRuleEffect)
            rtemp6 = [rtemp6; find(obj.data.Index(:,8) == Args.gammaRuleEffect(nf))];
        end
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.pairhist)
        if Args.rudeHist
            switch Args.pairhist
                case 'md'
                    
                    code = 1;
                case 'mv'
                    code = 2;
                case 'ld'
                    code = 3;
                case 'lv'
                    code = 4;
                case 'sd'
                    code = 5;
                case 'sv'
                    code = 6;
                case 'ms'
                    code = 7;
                case 'ls'
                    code = 8;
            end
        else
            switch Args.pairhist
                case '54'
                    code = 1;
                case '56'
                    code = 2;
                case '58'
                    code = 3;
                case 'l4'
                    code = 4;
                case 'l6'
                    code = 5;
                case 'l8'
                    code = 6;
                case 'm4'
                    code = 7;
                case 'm6'
                    code = 8;
                case 'm8'
                    code = 9;
                case '74'
                    code = 10;
                case '76'
                    code = 11;
                case '78'
                    code = 12;
            end
        end
        if ~exist('code'); code = nan; end
        rtemp7 = find(obj.data.Index(:,9) == code);
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.IDEruleTuning)
        rtemp8 = [];
        for nf = 1 : length(Args.IDEruleTuning)
            rtemp8 = [rtemp8; find(obj.data.Index(:,12) == Args.IDEruleTuning(nf))];
        end
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.LOCruleTuning)
        rtemp9 = [];
        for nf = 1 : length(Args.LOCruleTuning)
            rtemp9 = [rtemp9; find(obj.data.Index(:,13) == Args.LOCruleTuning(nf))];
        end
    else
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    ttem = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,rtemp9))))))));
    if ~isempty(Args.snr)
        indsnr = find(obj.data.Index(:,24) >= Args.snr & obj.data.Index(:,25) >= Args.snr);
    else
        indsnr = ttem;
    end
    
    bind = intersect(ttem,indsnr);
    
    %     indAlpha = [];
    %     for nf = [1 3]
    %
    %         indAlpha = [indAlpha; find(obj.data.Index(:,26) == nf)];
    %     end
    if ~isempty(Args.alphaPeak)
        indAlpha = [];
        for nf = 1 : length(Args.alphaPeak)
            
            indAlpha = [indAlpha; find(obj.data.Index(:,26) == Args.alphaPeak(nf))];
        end
        indAlpha = intersect(bind,indAlpha);
        
    else
        %           indAlpha = setdiff(bind,indAlpha);
        indAlpha = bind;
    end
    
    
    indDC = [];
    for nf = [1 3]
        
        indDC = [indDC; find(obj.data.Index(:,27) == nf)];
    end
    if ~isempty(Args.DC)
        indDC = [];
        for nf = 1 : length(Args.DC)
            
            indDC = [indDC; find(obj.data.Index(:,27) == Args.DC(nf))];
        end
        lastind = intersect(indDC,indAlpha);
    else
        lastind = setdiff(indAlpha,indDC);
    end
    if ~isempty(Args.removeGr)
        indGr = find(ismember(obj.data.Index(:,10),Args.removeGr) + ismember(obj.data.Index(:,11),Args.removeGr) ==1);
        lastind = setdiff(lastind,indGr);
    end
    
    if ~isempty(Args.type)
        news = lastind;
        for c = 1 : size(Args.type,1)
            tnews = [];
            for e =  1 : length(Args.type{c,2})
            eval(sprintf('tnews = [tnews; vecc(find(obj.data.%s == %d))];',Args.type{c,1},Args.type{c,2}(e)));
            end
            news = intersect(news,tnews);
        end
        lastind = news;
    end
    
    varargout{1} = lastind;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

