function obj = plot(obj,varargin)


Args = struct('raster',0,'mvavg',[],'siglevel',3,'stdfold',1,'cues',0,'cues9',0,'rank',0,'frange',[5 45],'betaRange',[12 25]);
Args.flags = {'raster','phase','cues','cues9','rank'};
[Args,varargin2] = getoptargs(varargin,Args,'remove',{'raster'});

[numevents,dataindices] = get(obj,'number',varargin2{:});
if ~isempty(dataindices)
    if ~isempty(Args.NumericArguments)
        
        n = Args.NumericArguments{1}; % to work oon
        ind = dataindices(n);
        
    else
        
    end
    cd(obj.data.setNames{ind})
    load GCcohInter.mat
    sur = load('GCgeneralSur.mat');
    frange = find(f >= Args.frange(1) & f <= Args.frange(2));
    wind = {'fixation' 'cue' 'delay1' 'delay2'};
    ymax = [];
    ymin = [];
    mlrules = {'identity' 'location'};
    % if switchday; if isempty(varargin) | ~isempty(findstr(type,'cs')); col = 2; else col = length(varargin{1}) - 1; end; f1 = figure(99);ss = s; else; col = 1;f1 = figure;ss = 1;end
    
    cmb = obj.data.Index(ind,1);
    cuecomb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    
    tunel = {'-' '-.'};
    lrules = {'o' 'x'};
    marking = {'r' 'b'};
    stimL = {'.' 'o' 'x'};
    for s = 1 : length(sur.Day.session); rules(s) = sur.Day.session(s).rule; end
    %%
    if Args.cues
        for ru = 1 : 2
            legs(1).cues = 'ide1-2-3';
            legs(2).cues = 'loc1-2-3';
            tuning = {'ide' 'loc'};
            
            
            for tu = 1 : 2
                [status, result] = unix(sprintf('find . -name ''%sTuningRule%dGC.mat''',tuning{tu},ru));
                tune(tu).data = load(result(~isspace(result)),'cohInter','f');
                f = tune(1).data.f;
                if ~isempty(tune(tu).data)
                    for p = 1: 4
                        for stim = 1 : 3
                            data(1,:) = squeeze(tune(ru).data.cohInter.Session.coh(stim).Fx2y(cmb,:,p));
                            data(2,:) = squeeze(tune(ru).data.cohInter.Session.coh(stim).Fy2x(cmb,:,p));
                            subplot(4,4,(ru-1)* 8 +((tu-1)*4)+p)
                            
                            if ~isempty(Args.mvavg)
                                ff = repmat(1/Args.mvavg,1,Args.mvavg);
                                data = filtfilt(ff,1,data);
                            end
                            plot(f,data);
                            ymax = [ymax max(max(data(:,Args.frange)))];
                            ymin = [ymin min(min(data(:,Args.frange)))];
                            hold on;
                            if isempty(Args.mvavg)
                                thesur(1,:) = sur.Day.session(find(rules == ru)).x2yProb(:,Args.siglevel,cmb,p);
                                thesur(2,:) = sur.Day.session(find(rules == ru)).y2xProb(:,Args.siglevel,cmb,p);
                            else
                                thesur(1,:) = filtfilt(ff,1,sur.Day.session(find(rules == ru)).x2yProb(:,Args.siglevel,cmb,p));
                                thesur(2,:) = filtfilt(ff,1,sur.Day.session(find(rules == ru)).y2xProb(:,Args.siglevel,cmb,p));
                            end
                            plot(f,thesur,'--');
                            ylabel(wind{p})
                            
                        end
                        if p == 4;subplot(4,4,(ru-1)* 8 + (tu-1)*4+p); title(mlrules{ru}); legend(sprintf('P->F:%s',legs(tu).cues),sprintf('F->P:%s',legs(tu).cues),'P->F:surro','F->P:surro');end
                    end
                    xlabel('frequency [hz]')
                end
            end
        end
        for sb = 1 : 16; subplot(4,4,sb); axis([5 max(Args.frange) min(ymin) max(ymax)]); end
        %%
    elseif Args.cues9
        
        legs(1).cues = {'ide1' 'ide2' 'ide3'};
        legs(2).cues = {'loc1' 'loc2' 'loc3'};
        for r = 1 : 2
            [status, result] = unix(sprintf('find . -name ''allCuesTuningRule%dGC.mat''',r));
            tune.data = load(result(~isspace(result)),'cohInter','f');
            sur = load('GCcohIntersur.mat');
            f = tune.data.f;
            if ~isempty(tune.data)
                for p = 1: 4
                    for stim = 1 : 9
                        data(1,:) = squeeze(tune.data.cohInter.Session.coh(stim).Fx2y(cmb,:,p));
                        data(2,:) = squeeze(tune.data.cohInter.Session.coh(stim).Fy2x(cmb,:,p));
                        
                        subplot(9,4,(stim -1) * 4+p)
                        
                        if ~isempty(Args.mvavg)
                            ff = repmat(1/Args.mvavg,1,Args.mvavg);
                            data = filtfilt(ff,1,data);
                        end
                        plot(f,data);
                        ymax = [ymax max(max(data(:,Args.frange)))];
                        ymin = [ymin min(min(data(:,Args.frange)))];
                        hold on;
                        if isempty(Args.mvavg)
                            thesur(1,:) = sur.Day.session(find(rules == r)).x2yProb(:,Args.siglevel,cmb,p);
                            thesur(2,:) = sur.Day.session(find(rules == r)).y2xProb(:,Args.siglevel,cmb,p);
                        else
                            thesur(1,:) = filtfilt(ff,1,sur.day.session(find(rules == r)).x2yProb(:,Args.siglevel,cmb,p));
                            thesur(2,:) = filtfilt(ff,1,sur.day.session(find(rules == r)).y2xProb(:,Args.siglevel,cmb,p));
                        end
                        
                        plot(f,thesur,'--');
                        if p == 1; ylabel(sprintf('%s; %s',legs(1).cues{cuecomb(2,stim)},legs(2).cues{cuecomb(1,stim)})); end
                    end
                    
                    subplot(9,4,(stim -1) * 4+p); title(wind{p});
                end
                
                
            end
            legend('P->F:IDE&LOC','F->P:IDE&LOC','surro:IDE&LOC');
            ylabel('granger')
            xlabel('frequency [hz]')
            %         if unique(mtst.data.index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'identity'); elseif length(unique(mtst.data.index(:,1)) == 1) > 1; rule = mlrules{s};else; rule = sprintf('session%d = %s',s+1,'location'); end
            
        end
        
        for sb = 1 : 36; subplot(9,4,sb); axis([5 max(Args.frange) min(ymin) max(ymax)]); end
        
        %%
    elseif Args.rank
        colorR = {'r' 'b'};
        legs(1).cues = {'ide1' 'ide2' 'ide3'};
        legs(2).cues = {'loc1' 'loc2' 'loc3'};
        maxy = [];
        for r = 1 : 2
            [status, result] = unix(sprintf('find . -name ''allCuesTuningRule%dGC.mat''',r));
            tune.data = load(result(~isspace(result)),'cohInter','f');
            
            f = tune.data.f;
            therange = find(f >= Args.betaRange(1) & f <= Args.betaRange(2));
            if ~isempty(tune.data)
                
                for stim = 1 : 9
                    data(1,:,stim) = mean(squeeze(tune.data.cohInter.Session.coh(stim).Fx2y(cmb,therange,:)),1);
                    data(2,:,stim) = mean(squeeze(tune.data.cohInter.Session.coh(stim).Fy2x(cmb,therange,:)),1);
                end
                for direc = 1 : 2
                    
                    thedata = squeeze(data(direc,:,:));
                    
                    for ref = 1 : 4
                        [b,ix] = sort(thedata(ref,:),2);
                        
                        for p = 1: 4
                            rdata = thedata(p,ix);
                            subplot(4,4,(ref -1)* 4 + p)
                            plot(rdata,sprintf('%s%s',tunel{direc},colorR{r}))
                            hold on
                            maxy = [maxy max(rdata)];
                            if ref == 1; title(wind{p}); end
                            if p == 1; ylabel(sprintf('ref. %s',wind{ref})); end
                            
                        end
                        
                    end
                end
            end
        end
        for sb = 1 : 16; subplot(4,4,sb); ylim([0 max(maxy)]); end
        xlabel('Stim #')
        ylabel('GCvalue')
        legend('IDE; P->F','IDE; F->P','LOC; P->F','LOC; F->P')
        
        %%
    else
        for s = 1 : length(Day.session)
            
            alldata{1} = Day.session(s).Fx2y;
            alldata{2} = Day.session(s).Fy2x;
            surdata{1} = sur.Day.session(s).Fx2y;
            surdata{2} = sur.Day.session(s).Fy2x;
            for dir = 1 : 2
                if ~isempty(alldata{dir})
                    
                    for p = 1: 4
                        subplot(2,4,(s-1)*4+p)
                        
                        data = alldata{dir}(:,cmb,p);
                        if ~isempty(Args.mvavg)
                            ff = repmat(1/Args.mvavg,1,Args.mvavg);
                            fdata = filtfilt(ff,1,data);
                            plot(f,fdata,marking{dir})
                            hold on
                        end
                        plot(f,data,marking{dir});
                        ymax = [ymax max(data(frange))];
                        ymin = [ymin min(data(frange))];
                        hold on;
                        if isempty(Args.mvavg)
                            thesur = surdata{dir}(:,Args.siglevel,p);
                        else
                            thesur = filtfilt(ff,1,surdata{dir}(:,Args.siglevel,cmb,p));
                        end
                         ymax = [ymax max(surdata{dir}(frange,Args.siglevel,p))];
                        ymin = [ymin min(surdata{dir}(frange,Args.siglevel,p))];
                        %                 if Args.phase; plot(f,thesur,'--'); hold on; plot(f,-thesur,'--') ;else; plot(f,thesur,'--'); end
                        plot(f,thesur,sprintf('%s--',marking{dir}));
                        hold on
                        ylabel(wind{p})
                        
                    end
                    
                    xlabel('frequency [hz]')
                    %         if unique(mtst.data.index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'identity'); elseif length(unique(mtst.data.index(:,1)) == 1) > 1; rule = mlrules{s};else; rule = sprintf('session%d = %s',s+1,'location'); end
                    subplot(2,4,(s-1)*4+p); title(mlrules(Day.session(s).rule));
                    %         set(f1,'name',sprintf('%s; %s; combination %d',day(end-5:end),,cmb))
                    varargout{1} = max(ymax);
                else
                    varargout{1} = nan;
                end
            end
        end
        for sb = 1 : 8; subplot(2,4,sb);  set(gca,'TickDir','out');xlim(Args.frange); ylim([min(ymin) max(ymax)]); end; legend('p->f','p->fsur','f->p','f-psur');
    end
    info = sprintf('%s pair # %d; groups # %d and %d',obj.data.setNames{ind},cmb,obj.data.Index(ind,10),obj.data.Index(ind,11));
    display(info)
    h = gcf;
    set(h,'name',info)
end