function obj = mtsGCcohInter(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'Tuning',0,'betaRange',[14 30],'pvalue',0.01);
Args.flags = {'Auto','noTuning'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsGCcohInter';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'GCcohInter';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

pvalues = [0.01 0.001 0.0001 0.00001];

plevel = find(Args.pvalue == pvalues);
if ~isempty(plevel)
    
    dataGCfile = nptDir('GCcohInter.mat');
    surGCfile = nptDir('GCgeneralSur.mat');
    
    datafile = nptDir('cohInter.mat');
    surfile = nptDir('generalSur.mat');
    compfile = nptDir('cohInterPcomp.mat');
    %     rulefile = nptDir('allCuesComb11.mat');
    if Args.Tuning
        idefile = nptDir('ideTuning11.mat'); % from the compareCohRule.m
        locfile = nptDir('locTuning11.mat');
        rulefile = nptDir('allCuesComb11.mat');
        
    else
        % from the compareCohRule.m
        idefile = 0; % from the compareCohRule.m
        locfile = 0;
        rulefile = 0;
    end
    
    if  ~isempty(datafile) && ~isempty(surfile) && ~isempty(compfile) && ~isempty(rulefile) && ~isempty(dataGCfile) && ~isempty(surGCfile)
        
        data = getIndexCohInter(datafile,surfile,compfile,rulefile,idefile,locfile,varargin{:});
        %% additional index values gc specific
        da = load(dataGCfile.name);
        sur = load(surGCfile.name);
        
%         for s = 1 : length(da.Day.session)
%             if da.Day.session(s).rule == 1
%                 data.PtoFbetaIDE = squeeze(mean(da.Day.session(s).Fx2y(Args.betaRange(1):Args.betaRange(2),:,:),1)); % 4 values for 4 windows
%                 data.FtoPbetaIDE = squeeze(mean(da.Day.session(s).Fy2x(Args.betaRange(1):Args.betaRange(2),:,:),1));
%                 data.PtoFbetaIDESur = squeeze(mean(sur.Day.session(s).Fx2y(Args.betaRange(1):Args.betaRange(2),plevel,:,:),1));
%                 data.FtoPbetaIDESur = squeeze(mean(sur.Day.session(s).Fy2x(Args.betaRange(1):Args.betaRange(2),plevel,:,:),1));
%             else
%                 data.PtoFbetaLOC = squeeze(mean(da.Day.session(s).Fx2y(Args.betaRange(1):Args.betaRange(2),:,:),1)); % 4 values for 4 windows
%                 data.FtoPbetaLOC = squeeze(mean(da.Day.session(s).Fy2x(Args.betaRange(1):Args.betaRange(2),:,:),1));
%                 data.PtoFbetaLOCSur = squeeze(mean(sur.Day.session(s).Fx2y(Args.betaRange(1):Args.betaRange(2),plevel,:,:),1));
%                 data.FtoPbetaLOCSur = squeeze(mean(sur.Day.session(s).Fy2x(Args.betaRange(1):Args.betaRange(2),plevel,:,:),1));
%                 
%             end
%         end
        
        %%
        
        if ~isempty(data)
            data.setNames = repmat({pwd},1,size(data.Index,1));
            
            data.numSets = size(data.Index,1); % nbr of trial
            % create nptdata so we can inherit from it
            n = nptdata(data.numSets,0,pwd);
            d.data = data;
            obj = class(d,Args.classname,n);
            if(Args.SaveLevels)
                fprintf('Saving %s object...\n',Args.classname);
                eval([Args.matvarname ' = obj;']);
                % save object
                eval(['save ' Args.matname ' ' Args.matvarname]);
            end
        else
            obj = createEmptyObject(Args);
        end
        
        
    else
        % create empty object
        fprintf('The mtstrial object is empty or the group directory does not exist \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('pvalue not existing, sorry choose another value \n');
    obj = createEmptyObject(Args);
end
function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];
% data.PtoFbetaIDE = [];
% data.FtoPbetaIDE = [];
% data.PtoFbetaIDESur = [];
% data.FtoPbetaIDESur = [];
% 
% data.PtoFbetaLOC = [];
% data.FtoPbetaLOC = [];
% data.PtoFbetaLOCSur = [];
% data.FtoPbetaLOCSur = [];

data.betamodIDE = [];
data.betamodLOC = [];
data.gammamodIDE = [];
data.gammamodLOC = [];

% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
