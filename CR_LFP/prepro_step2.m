function prepro_step2

% loads base files and runs preprocess2 and saves
% run from work directory

cd gray
cd clark
cd Days

str = dir;
for j=1:length(str)-2
    idd{j}=str(j+2).name;
end

for i=1:size(idd,2)
    i
    cd(idd{i});
    cd base
    load(strcat('clark',idd{i},'02base'));
    cd ..
    % outlier reject
    [datag,pointsrg,info2g,datrej,pointsrej,inforej,bad_trials]=outlier_reject(info2,data,pointsr);
    [epoch1dnr02,info2c,datac,pointsrc,outdat_incor,ptsout_incor,outinfo_incor]=preprocess2(datag,info2g,pointsrg,2);
    mkdir preprod
    cd preprod
    save(strcat('clark',idd{i},'02preprod_e1'),'epoch1dnr02','datag','pointsrg','info2g','pointsrc','sampling_rater','scan_order','info2c','datac','id','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','neuroinfo');
    cd ..
    clear('datac','epoch1dnr02','scan_order','info2c','id','num_channels','pointsrc','outdat_incor','ptsout_incor','outinfo_incor','data','info2','neuroinfo','pointsr','sampling_rater');
    cd base
    load(strcat('clark',idd{i},'02base'));
    cd ..
    [epoch2dnr02,info2c,datac,pointsrc,outdat_incor,ptsout_incor,outinfo_incor]=preprocess2(datag,info2g,pointsrg,3);
    cd preprod
    save(strcat('clark',idd{i},'02preprod_e2'),'epoch2dnr02','datag','pointsrg','info2g','pointsrc','sampling_rater','scan_order','info2c','datac','id','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','neuroinfo');
    cd ..
    clear('datac','epoch2dnr02','sampling_rater','scan_order','info2c','id','num_channels','pointsrc','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','data','datag','info2','info2g','neuroinfo','pointsr','pointsrg');
    cd base
    load(strcat('clark',idd{i},'03base'));
    cd ..
    [datag,pointsrg,info2g,datrej,pointsrej,inforej,bad_trials]=outlier_reject(info2,data,pointsr);
    [epoch1dnr03,info2c,datac,pointsrc,outdat_incor,ptsout_incor,outinfo_incor]=preprocess2(datag,info2g,pointsrg,2);
    cd preprod
    save(strcat('clark',idd{i},'03preprod_e1'),'epoch1dnr03','datag','pointsrg','info2g','pointsrc','sampling_rater','scan_order','info2c','datac','id','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','neuroinfo');
    cd ..
    clear('datac','epoch1dnr03','scan_order','info2c','id','num_channels','pointsrc','outdat_incor','ptsout_incor','outinfo_incor','data','info2','neuroinfo','pointsr','sampling_rater');
    cd base
    load(strcat('clark',idd{i},'03base'));
    cd ..
    [epoch2dnr03,info2c,datac,pointsrc,outdat_incor,ptsout_incor,outinfo_incor]=preprocess2(datag,info2g,pointsrg,3);
    cd preprod
    save(strcat('clark',idd{i},'03preprod_e2'),'epoch2dnr03','datag','pointsrg','info2g','pointsrc','sampling_rater','scan_order','info2c','datac','id','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','neuroinfo');
    cd ..
    cd ..
    clear('datac','epoch2dnr03','sampling_rater','scan_order','info2c','id','num_channels','pointsrc','datrej','pointsrej','inforej','bad_trials','outdat_incor','ptsout_incor','outinfo_incor','data','datag','info2','info2g','neuroinfo','pointsr','pointsrg');
end
    
clear all
cd ..
cd ..
cd ..
    