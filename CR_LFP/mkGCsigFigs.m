function mkGCsigFigs(obj,varargin)


[nn(1),allind] = get(obj,'Number','snr',99);
[nn(2),bind] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});

nbind = setdiff(allind,bind);
nn(3) = length(nbind);
plevel = 3;
select = {'allind' 'bind' 'nbind'};

lsel = {'all pairs' 'betapos34 pairs' 'no betapos34 pairs'};
figure

fsb = [1 5 9];
for type = 1 : 3
    n = nn(type);
    sind = eval(select{type});
    count = 1;
    clear GC surro
    for i = sind
        cd(obj.data.setNames{i})
        load GCcohInter.mat
        if Day.session(1).rule == 1
            s = 1;
        else
            s = 2;
        end
        cmb = obj.data.Index(i,1);
        GC(1,count,:,:) = Day.session(s).Fx2y(:,cmb,:);
        GC(2,count,:,:) = Day.session(s).Fy2x(:,cmb,:);
        GC(3,count,:,:) = Day.session(s).Fxy(:,cmb,:);
        load GCgeneralSur.mat
        surro(1,count,:,:) = Day.session(s).Fx2y(:,plevel,:);
        surro(2,count,:,:) = Day.session(s).Fy2x(:,plevel,:);
        surro(3,count,:,:) = Day.session(s).Fy2x(:,plevel,:);
        count = count + 1;
    end
    GCsig = GC >= surro;
    dirl = {'b','r'};
    
    for dir = 1 : 2; for p = 1 : 4; subplot(3,4,(type-1)*4+p); plot(100*sum(squeeze(GCsig(dir,:,:,p)),1)/size(GC,2),dirl{dir}); hold on;end; end
    
    epochs = {'presample' 'sample' 'delay 1' 'delay2'};
    xlabel('Frequency [Hz]')
    ylabel('% significant pairs')
    legend('PPC -> PFC','PFC -> PPC')
    % subplot(2,4,9); ylabel('Inst.')
    for sb = 1 : 4; subplot(3,4,sb);title(epochs{sb}); end
    for sb = 1 : 12; subplot(3,4,sb);axis([12 30 0 40]); end
    subplot(3,4,fsb(type)); ylabel(sprintf('%s; n=%d',lsel{type},n));
end