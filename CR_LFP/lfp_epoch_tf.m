function [epochmln,epochsln,epochml,epochsl] = lfp_epoch_tf(trials,channels,info2,varargin)
% to run in the session directory
% output: data{period}(samples x trials x channels)

Args = struct('plength',500,'downSR',200,'beforeSac',0,'EPnotRemoved',0);
Args.flags = {'beforeSac','EPnotRemoved'};
[Args,modvarargin] = getOptArgs(varargin,Args);


% plength = Args.plength;
%
% downsamp = 1000/Args.downSR;
% lplength = plength/downsamp;

mtst = ProcessSession(mtstrial,'auto');

[pdir,cdir] = getDataDirs('lfp','relative','CDNow');
files = nptDir('*_lfp.*');

count = 1;

for t = vecr(trials)
    fprintf(' %d',t)

    nch = 1;
    for ch = channels
        [lfp,num_channels,sampling_rate,scan_order,points(count)]=nptReadStreamerChannel(files(t).name,ch);
        [dlfp{count}(:,nch),SR] = preprocessinglfp(lfp,'downSR',Args.downSR);
        nch = nch + 1;
    end
    count = count + 1;
    clear lfp rslfp flfp
end

%epoch the data into match and sample locked

MatchOnset = info2.MatchOnset(trials)/5;
points = points/5;


% sample locked

for i=1:size(dlfp,2)
    epochsl(:,:,i)=dlfp{i}(1:(round(min(MatchOnset))),:);
end

% match locked

a = round(min(MatchOnset)-202)
b = round(min(points - MatchOnset')-1)
for i=1:size(dlfp,2)
    epochml(:,:,i)=dlfp{i}(round(MatchOnset(i)-a):round(MatchOnset(i)+b),:);
end


if ~Args.EPnotRemoved
        epochsln = lfp_normalize(epochsl,1);
        epochmln = lfp_normalize(epochml,1);
end
cd(cdir)