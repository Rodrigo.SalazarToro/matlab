function out = lfp_detrend(data,varargin)

%LFP_DETREND Removes linear trends in the data
% usage: [...]=lfp_detrend(DATA,OPT)
% Inputs:
%          DATA  a 3D array with dimensions time x channels x
%          trials
% Outputs:
%         [O]=lfp_detrend(...) returns the detrended data
%
%
% Comments: Uses matlab routine detrend

% 30 January 2004
% Anders Ledberg, Center for Complex Systems &
% Brain Sciences, FAU, Boca Raton, FL 33413

% Check that enough inputs are given
Args = struct('dorder',[],'detrendPlot',0);
Args.flags = {'detrendPlot'};
[Args,modvarargin] = getOptArgs(varargin,Args);

if (nargin < 1)
    'error: too few arguments'
    'usage: '
    help lfp_detrend
    return
end
params = struct('trialave',1,'tapers',[3 5],'Fs',200,'fpass',[0 50]);

nchans=size(data,2);
out=zeros(size(data));
for ch=1:nchans
    tmp=squeeze(data(:,ch,:));
    if ~isempty(Args.dorder)
        for t = 1 : size(tmp,2)
            x = [1:size(tmp,1)]';
            co = polyfit(x,tmp(:,t),Args.dorder);
            dtm = polyval(co,x);
            
            if Args.detrendPlot
                subplot(2,1,1)
                plot(tmp(:,t)); hold on; plot(tmp(:,t) - dtm,'r'); plot(dtm,'k')
                legend('raw trace','resulting trace','detrending traces')
                xlabel('time serie')
                subplot(2,1,2)
                [S,f] = mtspectrumc(tmp(:,t),params);
                plot(f,S)
                hold on
                [S,f] = mtspectrumc(tmp(:,t) - dtm,params);
                plot(f,S,'r')
                xlim([0 30]);
                xlabel('power')
                pause
                clf
            end
            tmp(:,t) = tmp(:,t) - dtm;
            
        end
    else
        tmp=detrend(tmp);
    end
    out(:,ch,:)=tmp;
end

