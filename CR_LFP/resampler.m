function [outdat,pts,smp_rate]=resampler(indat,insmp_rate)

for i=1:size(indat,2)
    outdat{i}=(resample(indat{i}',1,5))';
    pts(i)=size(outdat{i},2);
end
smp_rate=insmp_rate/5;

