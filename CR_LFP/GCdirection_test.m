function [Fdiff,pvalues,levels,varargout] = GCdirection_test(data,varargin)
% input in data(samples x trials x channels)



Args = struct('nrep',1000,'gProb',[0.05 0.01 0.001 0.0001 0.00001 0.000001],'FSpread',2,'freqs',[1 : 100],'norder',20,'Fs',200,'onlyInter',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

Nr = size(data,2);
Nl = size(data,1);

proba = [Args.gProb./2 (1 - Args.gProb./2)];

%% create the surrogate
area1 = Args.onlyInter;
area2 = setdiff([1 : size(data,3)],area1);
sdata = zeros(2,Nr*Nl);
if Args.nrep > 1
    sFdiff = zeros(Args.nrep,length(Args.freqs));
    for i = 1 : Args.nrep
       sdata = zeros(2,Nr*Nl);
        for t = 1 : size(data,2)
            if isempty(Args.onlyInter)
                ch(1) = randi(size(data,3));
                ch(2) = randi(size(data,3));
            else
                firstch = randi(2);
                secch = setdiff([1 2],firstch);
                ch(firstch) = area1(randi(length(area1)));
                ch(secch) = area2(randi(length(area2)));
            end
            while diff(ch) == 0
                if isempty(Args.onlyInter)
                    ch(2) = randi(size(data,3));
                else
                    ch(2) = area2(randi(length(area2)));
                end
            end
            sdata(:,(t-1)*Nl + 1: t*Nl) = shiftdim(squeeze(data(:,t,ch)),1);
            
        end
        [~,~,sFx2y,sFy2x,~,~] = pwcausalrp(sdata,size(sdata,2)/Nl,Nl,Args.norder,Args.Fs,Args.freqs);
        sFdiff(i,:) = sFx2y - sFy2x;
        
    end
    [~,~,pvalues,~] = fitSurface(sFdiff,Args.freqs,'Prob',proba,'FSpread',Args.FSpread,modvarargin{:});
    Fdiff = [];
    varargout{2} = [];
    varargout{3} = [];
    varargout{4} = [];
else
    %%
    [~,~,Fx2y,Fy2x,Fxy,~] = pwcausalrp(reshape(shiftdim(data,2),2,Nl*Nr),size(sdata,2)/Nl,Nl,Args.norder,Args.Fs,Args.freqs);
    Fdiff = Fx2y - Fy2x;
    pvalues = [];
    varargout{2} = Fx2y;
    varargout{3} = Fy2x;
    varargout{4} = Fxy;
end

varargout{1} = Args.freqs;
levels = [Args.gProb Args.gProb];
%%
