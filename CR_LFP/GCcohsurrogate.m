function [x2yProb,y2xProb,xyProb,varargout] = GCcohsurrogate(data,Nr,Nl,porder,fs,freq,varargin)
% data ch x (trials*length(trials))
% if surrogate ch is big
% the modvarargin goes into fitSurface
% By default runs the coherencyc but with the args 'SFC' or 'STA' will run
% them instead

Args = struct('nrep',1000,'Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2,'general',0);
Args.flags = {'general'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'nrep'});

ntrials = Nr;
[~,~,Fx2yt,~,~,~] = pwcausalrp(data,Nr,Nl,porder,fs,freq);

if Args.general
    sur = nan(2,size(data,2));
else
    sur = nan(size(data,1),size(data,2));
end

Fx2y = nan(Args.nrep,size(Fx2yt,2));
Fy2x = nan(Args.nrep,size(Fx2yt,2));
Fxy = nan(Args.nrep,size(Fx2yt,2));
for rep = 1 : Args.nrep
    
    if Args.general
        ordT1 = randperm(ntrials);
        ordT2 = randperm(ntrials);
        thech = [1; 1];
        while diff(thech) == 0
            thech = randi(size(data,1),[2,1]);
        end
        seq =  [(reshape(repmat(ordT1,Nl,1),1,Nl * length(ordT1)) - 1) * Nl + repmat([1 : Nl],1,ntrials); (reshape(repmat(ordT2,Nl,1),1,Nl * length(ordT2)) - 1) * Nl + repmat([1 : Nl],1,ntrials)];
        for cc = 1 : 2;sur(cc,:) = data(thech(cc),seq(cc,:));end
        
    else
        for ch = 1 : size(data,1)
            ordT = randperm(ntrials);
            
            seq =  (reshape(repmat(ordT,Nl,1),1,Nl * length(ordT)) - 1) * Nl + repmat([1 : Nl],1,ntrials);
            
            sur(ch,:) = data(ch,seq);
        end
    end
    [~,~,Fx2y(rep,:),Fy2x(rep,:),Fxy(rep,:),~] = pwcausalrp(sur,Nr,Nl,porder,fs,freq);
    
end

if unique(isnan(Fx2y)) || unique(isnan(Fy2x)) || unique(isnan(Fxy))
    x2yProb = [];
    y2xProb = [];
    xyProb = [];
else
    
    [~,~,x2yProb,~] = fitSurface(Fx2y,freq,'Prob',Args.Prob,'FSpread',Args.FSpread,modvarargin{:});
    [~,~,y2xProb,~] = fitSurface(Fy2x,freq,'Prob',Args.Prob,'FSpread',Args.FSpread,modvarargin{:});
    [~,~,xyProb,~] = fitSurface(Fxy,freq,'Prob',Args.Prob,'FSpread',Args.FSpread,modvarargin{:});
end
