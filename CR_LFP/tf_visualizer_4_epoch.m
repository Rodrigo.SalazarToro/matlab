function tf_visualizer_4_epoch(dat,freqrange,fromchan,tochan,cmax,cmin)

%TF_VISUALIZER plots t-f planes
%
% usage: [...]=tf_visualizer(dat,freqrange,fromchan,tochan,cmax,cmin,opt)
%
% Inputs:
%    dat:       time frequency array (GC, Coherence or Power)
%    freqrange: the range of frequencies to be plotted (i.e. [1 100])
%    fromchan:  for GC, this is the sending channel, for coherence
%               fromchan and tochan are interchangeable.  For Power they
%               must be the same
%    tochan:    the target channel
%    cmax:      upper value of the colorbar
%    cmin:      lower value of the colorbar
%
%    ex.        tf_visualizer(dat,[1 100],4,1,.05,0,2)
%
%    NOTE:  Coherence and power are both calculated from the same array
%    that results from processing a *.co file with tf_load_from_clust.m, GC
%    plots are constructed from arrays resulting from *.gc cluster files
%
% October 27 2008
% Craig Richter, Center for Complex Systems &
% Brain Sciences, FAU, Boca Raton, FL 33413


dat = permute(dat,[1 4 2 3]);
datrange = [1 size(dat,1)];
freq = [1 size(dat,2)];
surf(dat(datrange(1):datrange(2),freq(1):freq(2),fromchan,tochan)','FaceColor','interp','LineStyle','none');
view(2);
set(gcf,'renderer','zbuffer');
axis([datrange(1) datrange(2) freqrange(1) freqrange(2)]);
set(gca,'xtick',[1,11,22,32,42,52,62,72,82,92]);
set(gca,'xticklabel',{-450,-400,-350,-300,-250,-200,-150,-100,-50});
title('Match Onset Locked');
%sets color axis limit (max/min values):
set(gca,'CLim',[cmin cmax])
xlabel('ms')
ylabel('Hz')
colorbar;
box on
set(gca,'tickdir','out')