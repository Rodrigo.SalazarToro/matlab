function [sigvalue,sigcoh,f,varargout] = GC_summary(days,varargin)
% to run in the monkey direcory
% need MTSlfp3 to be run before and save files. Also needs days as the
% switching days
% pvalue = 2 corresponds to p < 0.00001 or could be 1 or 3 for the lower
% and higher levels


Args = struct('cohtype','cohInter','Fborder',8,'pvalue',2,'plot',0,'prctile',[25 50 75 90],'list',[]);
Args.flags = {'plot'};
[Args,modvarargin] = getOptArgs(varargin,Args);



%% plotting options

lb = {'b' 'k'}; % black for location and blue for identity
therules = {'Identity' 'Location'};
xlabel('Frequency [Hz]')
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
epochsc = {'-' '--' '-.' ':'};
faxis = [Args.Fborder 60]; % 100-Args.Fborder];
philimit = 3.2;
bins = [-philimit : 0.4: philimit];
phiaxis = philimit+0.1;
%%

for r = 1 : 2;
    for p = 1: 4;
        for direc = 1 : 2;
            sigvalue{r,p,direc} = [];
            sigcoh{r,p,direc} = [];
            
        end;
    end;
end;
ncount = zeros(1,2);

%% acquiring the data
for d = 1 : size(days,2)
    clear rules
    cd(days{d});
    betty = findstr(days{d},'betty');
    cohfile = nptDir(sprintf('GC%s.mat',Args.cohtype));
    surfile = nptDir(sprintf('GC%sSur.mat',Args.cohtype));
    if ~isempty(cohfile) & ~isempty(surfile)
        data = load(cohfile.name);
        sur = load(surfile.name);
        f = data.f;
        fband = find(f>= Args.Fborder & f <= (f(end) - Args.Fborder));
        
        ncomb = size(data.Day.comb,1);
        combs = [1 : ncomb];
        if ~isempty(Args.list)
            thelist = Args.list;
            forday = strmatch(days{d},thelist.days);
            ncomb =  length(forday);
            combs = thelist.npair(forday);
        end
        
        nses = length(data.Day.session);
        eval(sprintf('load(''%s.mat'')',Args.cohtype))
        if ~isempty(betty); rules = [1 2]; else; for s = 1 : nses; rules(s) = Day.session(s).rule; end; end
        for ses = 1 : nses
            
            if ~isempty(betty); rule = ses; else; rule = Day.session(ses).rule; end
            thes = find(rules == rule);
            if ses == thes(end)
                
                if ~isempty(data.Day.session(ses).Fx2y) & ~isempty(data.Day.session(ses).Fy2x)
                    
                    for p = 1 : 4 ; usePP{p} = [];  usePF{p} = [];end
                    
                    for comb = combs
                        
                        ncount(rule) = ncount(rule) + 1;
                        for p = 1 : 4
                            
                            cohc{1} = squeeze(data.Day.session(ses).Fx2y(:,comb,p));
                            coht{1} = squeeze(sur.Day.session(ses).x2yProb(:,Args.pvalue,comb,p));
                            
                            cohc{2} = squeeze(data.Day.session(ses).Fy2x(:,comb,p));
                            coht{2} = squeeze(sur.Day.session(ses).y2xProb(:,Args.pvalue,comb,p));
                            
                            for direc = 1 : 2
                                sigco{direc} = cohc{direc} > coht{direc};
                                
                                %                             end
                                signcoh{rule,direc}(:,ncount(rule),p) = sigco{direc};
                                
                                if ~isempty(find(sigco{direc}(fband) == 1))
                                    sigvalue{rule,p,direc} = [sigvalue{rule,p,direc} cohc{direc}];
                                    
                                    sigcoh{rule,p,direc} = [sigcoh{rule,p,direc} sigco{direc}];
                                end
                            end
                        end
                        
                    end
                    thePP = data.Day.comb(comb,1);
                    thePF = data.Day.comb(comb,2);
                    
                    
                end
            end
            
        end
    end
    
    
    cd ..
    
end %for d = 1 : size(days,1)
%%
varargout{1} = signcoh;
varargout{2} = ncount;
% d = 37 problem with the surrogate

%%
varargout{3} = totn;

%% Plotting
if Args.plot
    
    nh = 1;
    
    h(nh) = figure;
    
    set(h(nh),'Name','GC : percent # of significant pairs');
    
    
    for r =1 : 2;
        for p = 1 : 4;
            for direc = 1 : 2
                subplot(4,2,(direc-1) * 4 + p);
                value = 100 * sum(squeeze(signcoh{r,direc}(:,:,p)),2) / size(signcoh{r,direc},2);
                plot(f,value,sprintf('%s',lb{r}))
                hold on
                ylabel(sprintf('%s [perc. pairs]',epochs{p}))
                axis([faxis 0 100])
            end
        end
        xlabel('Frequency [Hz]')
    end
    legend('IDE','LOC')
    
    nh = nh + 1;
    
    h(nh) = figure;
    
    set(h(nh),'Name','GC : percent # of significant pairs');
    
    
    for r =1 : 2;
        for p = 1 : 4;
            for direc = 1 : 2
                subplot(2,2,(direc - 1) * 2 +  r);
                value = 100 * sum(squeeze(signcoh{r,direc}(:,:,p)),2) / size(signcoh{r,direc},2);
                plot(f,value,sprintf('%s',epochsc{p}))
                hold on
                ylabel('significant pairs [%]')
                title(sprintf('%s',therules{r}));
                axis([faxis 0 100])
                legend(epochs)
            end
        end
        xlabel('Frequency [Hz]')
    end
    
    for direc = 1 : 2
        nh = nh + 1;
        
        h(nh) = figure;
        if direc == 1
            set(h(nh),'Name','GC : distri. of GC PPC-> PFC values for sign. pairs');
        else
            set(h(nh),'Name','GC : distri. of GC PFC-> PPC values for sign. pairs');
        end
        themax = [];
        for r = 1 : 2;
            for p = 1 : 4;
                
                pr = Args.prctile;
                
                for prc = 1 : length(pr)
                    subplot(4,2,(p-1)*2 +1);
                    dist{prc} = prctile(sigvalue{r,p},pr(prc),2);
                    themax = [themax max(dist{prc})];
                    %                 dist{prc} = prctile([sigvalue{1,p} sigvalue{2,p}],pr(prc),2);
                    plot(f,dist{prc},sprintf('%s',sprintf('%s%s',lb{r},epochsc{prc})))
                    %                 plot(f,dist{prc})
                    %                 axis([faxis 0 0.6])
                    hold on
                    subplot(4,4,(p-1)*2+2);
                    distP{prc} = prctile(sigphi{r,p},pr(prc),2);
                    plot(f,distP{prc},sprintf('%s',lb{r}))
                    hold on
                    
                end
                subplot(4,2,(p-1)*2+1); ylabel(sprintf('%s [coh]',epochs{p}));
                
                
                subplot(4,2,(p-1)*2+2); ylabel(sprintf('%s [phase]',epochs{p}));
                line(f,zeros(1,length(f)),'LineStyle','--')
                line(f,repmat(pi,1,length(f)),'LineStyle','--')
                line(f,repmat(-pi,1,length(f)),'LineStyle','--')
                axis([faxis -pi pi])
                if p ==4; xlabel('Frequency [Hz]'); end
                
            end
            
        end
    end
end


%%