function [Fx2y,Fy2x,Fdiff,f] = GCptProc(sp1,sp2,mts,strials,varargin)

Args = struct('save',0,'redo',0,'addName',[],'iterations',1000);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

matfile = sprintf('gccorrg%s%sg%s%s%s.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);

ntrials = length(strials);

dt = 0.01;
Fs = 1/dt;

epochsLim = {[1:50] [51:100] [101 : 150] [1:50]};

if isempty(nptDir(matfile)) || Args.redo
    Fy2x = cell(4,1);
    Fx2y= cell(4,1);
    Fdiff= cell(4,1);
    f = [];
    for ep = 1 : 4
        if ep == 4
            
            bins = [-510:10:10];
            data = nan(2,ntrials,51);
        else
            
            bins = [-510:10:1310];
            data = nan(2,ntrials,181);
        end
        for tr = 1 : ntrials
            if ep == 4
                ref = mts.data.MatchOnset(strials(tr));
                
            else
                ref = mts.data.CueOnset(strials(tr));
                
            end
            [n,~] = hist(sp1.data.trial(strials(tr)).cluster.spikes-ref,bins);
            data(1,tr,:) = n(2:end-1);
            [n,~] = hist(sp2.data.trial(strials(tr)).cluster.spikes-ref,bins);
            data(2,tr,:)= n(2:end-1);
            %     data(2,tr,:)= [n(2:end-1) 0];
        end
        
        [Sp,f]= module_spectrum(data,dt,[1 2],epochsLim{ep});
        f = f(1:end-1);
        try
            [Snew,Hnew,Znew] = sfactorization_wilson1(Sp,f,Fs);
            [Fy2x{ep},Fx2y{ep},~,~]=pwcausalr_c(Snew,Hnew,Znew,f,Fs);
            
            Fdiff{ep} = zeros(Args.iterations,length(Fx2y{ep}));
            for ii = 1 : Args.iterations
                ndata =zeros(2,ntrials,size(data,3));
                for tr = 1 : ntrials
                    rch = randi(2);
                    ndata(1,tr,:) = data(rch,tr,:);
                    ndata(2,tr,:) = data(setdiff([1 2],rch),tr,:);
                    
                end
                [Sp,~]= module_spectrum(ndata,dt,[1 2],epochsLim{ep});
                
                [Snew,Hnew,Znew] = sfactorization_wilson1(Sp,f,Fs);
                [tFy2x,tFx2y,~,~]=pwcausalr_c(Snew,Hnew,Znew,f,Fs);
                Fdiff{ep}(ii,:) = tFx2y-tFy2x;
            end
        catch
            display('NaNs in the matrix');
        end
    end
    if Args.save
        
        save(matfile,'Fx2y','Fy2x','Fdiff','f')
        display([pwd '/' matfile])
    end
else
    load(matfile)
end