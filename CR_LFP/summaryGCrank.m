function [data,varargout] = summaryGCrank(days,varargin)
% use 'betaRange',[12 25],'pvalue',0.01 as args
% and 'betaAboveSur' with th folloowing options:
% data.PtoFbetaIDE = [];
% data.FtoPbetaIDE = [];
% data.PtoFbetaLOC = [];
% data.FtoPbetaLOC = [];
% it will selected the pair whose gc is above sur for any window
%
%
%
%
%
%
%

Args = struct('linkedRules',0,'linkedDirec',0,'gc',[],'betaRange',[12 25]);
Args.flags = {'linkedRules','linkedDirec'};
[Args,modvarargin] = getOptArgs(varargin,Args);

conds = {'PtoFbetaLOC' 'FtoPbetaLOC' 'PtoFbetaIDE' 'FtoPbetaIDE'};

if isempty(Args.gc); gc = ProcessDays(mtsGCcohInter,'days',days,'sessions',{'session01'},'NoSites','betaRange',Args.betaRange,modvarargin{:}); else gc = Args.gc; end

for cond = 1 : 4; [r,ind{cond}] = get(gc,'betaAboveSur',{conds{cond}},'Number',modvarargin{:});end

if Args.linkedRules
    
    
    if Args.linkedDirec
        
        selInd = repmat(union(ind{1},ind{2}),1,4);
    else
        selInd = {union(ind{1},ind{3}), union(ind{2},ind{4}),union(ind{1},ind{3}), union(ind{2},ind{4})};
    end
else
    
    if Args.linkedDirec
        selInd = {union(ind{1},ind{2}),union(ind{1},ind{2}),union(ind{3},ind{4}),union(ind{3},ind{4})};
    else
        selInd = ind;
    end
    
end

for cond = 1 : 4
    dirs = gc.data.setNames(selInd{cond});
    pair = gc.data.Index(selInd{cond},1);
    for dpair = 1 : length(pair)
        cd(dirs{dpair})
        for r = 1 : 2
            [s,w] = unix(sprintf('find . -name ''allCuesTuningRule%dGC.mat''',r));
            
            load(w(1:end-1))
            for stim = 1 : 9; data(r,dpair,stim,:,:) = squeeze(cohInter.Session.coh(stim).Fx2y(pair(dpair),Args.betaRange(1):Args.betaRange(2),:)); end% (cb,:,p)
        end
        cd ..
        
    end
end