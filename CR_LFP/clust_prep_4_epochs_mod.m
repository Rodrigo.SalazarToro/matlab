function clust_prep_4_epochs_mod(monkeyname)

% prepares gray data for cluster - loads proprod m-files and formats them
% for cluster - this is used to create permutations for use with the 9 stim
% data based on the 4 epochs.  The 9 stim data is apporxiamted by using a
% random selection based on the mean number of trials used for the 9 stim
% subensembles

cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end

mkdir clustdat_4_epochs_mod



for i=1:size(days,1)
    cd clustdat_4_epochs_mod
    mkdir(days(i).name)
    cd(days(i).name);
    fid=fopen('permcogc_com','wt');
    cd ..
    cd ..
    cd(days(i).name);
    for sess = {'02', '03'}
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'_4_epochs.mat'));
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        days(i).name
        sess
        cd ..
        cd ..
        cd ..
        cd clustdat_4_epochs_mod
        cd(days(i).name)
        for p=1:9
            a(p)=size(data_sepstim{p,1},2);
        end
        %mean number of trials per stimulus type
        a = round(mean(a));
        for j=1:size(data,2)
            dattmp = data{j};
            dattmpp = permute(dattmp,[3 1 2]);
            dattmpprsh = reshape(dattmpp(gchannels,:,:),[size(gchannels,2) size(dattmpp,2)*size(dattmpp,3)]);
            save(strcat(monkeynamepre,'_',days(i).name,'_',sess{1},'e',num2str(j),'_1.dat'),'dattmpprsh','-ascii');
            fprintf(fid,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e' num2str(j) ' ' '1',' %1.0f %1.0f %1.0f 101 11 200 100 1000 richter\n'], size(gchannels,2), size(dattmpp,3), a)
        end
        cd ..
        cd ..
        cd(days(i).name);
    end
    fclose(fid);
    cd ..
end
cd ..




