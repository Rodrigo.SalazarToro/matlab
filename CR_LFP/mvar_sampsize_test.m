%run from session directory


mtst = mtstrial('auto');
trials = mtsgetTrials(mtst,'stable','BehResp',1);
c=0;
% sample sizes to be tested
for sampsize = [25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400]
    c=c+1;
    % number of estimates i to be use dfor mean and std
    for i=1:100
        disp(sampsize)
        disp(i)
        index = ceil(rand(1,sampsize)*size(trials,2));
        dat = trials(index);
        % input channels of interest to calculate coherence
        [data,lplength] = mvar_test_lfp_prep(dat,[2 5]);
        mvardat = permute(data,[3 1 2]);
        mvardat = reshape(mvardat,[size(mvardat,1) size(mvardat,2)*size(mvardat,3)]);
        [pp(c,i,:,:),cohe,Ix2y,Iy2x,Fxy,rp]=pwcausalrp(mvardat,sampsize,size(data,1),11,200,1:100)
        [C(c,i,:,:,:)]=yc2mat(2,100,cohe);
        [GC(c,i,:,:,:)]=yc2mat(2,100,Ix2y,Iy2x);
    end
end

mn = mean(C,2);
mn = squeeze(mn);
vr = std(C,0,2);
vr = squeeze(vr);
errorbar(squeeze(mn(:,1,2,20)),squeeze(vr(:,1,2,20)))
set(gca,'xtick',0:20);
set(gca,'xticklabel',{0 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400 425})
xlabel('resample size');
ylabel('Coherence');
title('Mean +/- std of 20 Hz coherence peak')