function [epoch]=epocher(data,MatchOnset,points,opt);

% for 200 Hz downsampled data

MatchOnset = MatchOnset/5;
points = points/5

%if opt=1
%epoch spans 500ms wait

%if opt=2
%epoch spans (500ms wait + 500ms stim1 + (shortest delay time))

%if opt=3
%epoch is match locked

if opt==1
    for i=1:size(data,2)
        epoch(:,:,i)=shiftdim(data{i}(:,1:101),1);
    end
end

if opt==2
    for i=1:size(data,2)
        epoch(:,:,i)=shiftdim(data{i}(:,1:(round(min(MatchOnset)))),1);
    end
end

if opt==3
    a = round(min(MatchOnset)-202)
    b = round(min(points - MatchOnset')-1)
    for i=1:size(data,2)
        epoch(:,:,i)=data{i}(:,round(MatchOnset(i)-a):round(MatchOnset(i)+b));
    end
    epoch=permute(epoch,[2 1 3]);
end

