function [Fx2ygram,Fy2xgram,t,f] = mkAveGCgram(obj,ind,varargin)
% to be run in /Volumes/raid/data/monkey


Args = struct('save',0,'redo',0,'prctile',[],'minTbin',38,'relFreq',[12 22],'type','IdePerLoc','rule','1','delayRange',[1.0 1.8],'addNameFile',[],'Interpol',0,'chance',[]);
Args.flags = {'save','redo','noSur','MatchAlign','plot','Interpol'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

r = length(ind);
sdir = pwd;
switch Args.type
    
    case  {'LocPerIde'} %% needs to be worked out
        ntype = 3;
        name = 'LocPerIde';
        prefix = [];
    case {'IdePerLoc' 'IdePerLocIncor'}
        ntype = 3;
        name = 'IdePerLoc';
        prefix = [];
        
    case 'Rules'
        ntype = 1;
        name = 'all';
        Args.rule = 's';
        prefix = 'Stim';
        nstim = 1;
end

thefile = sprintf('GC%sRule%s%d%s.mat',Args.type,Args.rule,Args.addNameFile);

if isempty(nptDir(thefile)) || Args.redo
    Fx2ygram = zeros(r,Args.minTbin,50);
    Fy2xgram = zeros(r,Args.minTbin,50);
    selectLoc = zeros(r,1);
    selectIde = zeros(r,1);
    for ii = 1 : r
        cd(obj.data.setNames{ind(ii)})
        cd grams
        gr = obj.data.Index(ind(ii),10:11);
        meanMI = zeros(3,1);
         meanCoh = zeros(3,1);
        for l = 1 : ntype
            matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%d%s.mat',gr(1),gr(2),Args.rule,name,l,Args.addNameFile);
            load(matfile,'mutualgram','time','f')
            time = time(1:Args.minTbin);
            relFreq = find(f>Args.relFreq(1) & f <Args.relFreq(2));
            thedelay = time > Args.delayRange(1) & time < Args.delayRange(2);
            meanMI(l) = mean(mean(mutualgram(thedelay,relFreq)));
        end
        [~,selectLoc(ii)] = max(meanMI);
        
        matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%d%s.mat',gr(1),gr(2),Args.rule,name,selectLoc(ii),Args.addNameFile);
         load(matfile,'C')
        for l = 1 : ntype
            meanCoh(l) = mean(mean(C{l}(thedelay,relFreq)));
        end
         [~,selectIde(ii)] = max(meanCoh);
          gcfile = sprintf('gcgramg%04.0fg%04.0fRule%sLoc%dIde%d%s.mat',gr(1),gr(2),Args.rule,selectLoc(ii),selectIde(ii),Args.addNameFile);
         load(gcfile,'Fx2y','Fy2x','f','t')
         Fx2ygram(ii,:,:) = Fx2y(1:Args.minTbin,:);
         Fy2xgram(ii,:,:) = Fy2x(1:Args.minTbin,:);
    end
   
    
end

if Args.save
    
        save(thefile,'Fx2ygram','t','f','Fy2xgram','selectLoc','selectIde')
    
    display(sprintf('saving %s',thefile))
end

