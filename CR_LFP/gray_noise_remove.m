function newdat = gray_noise_remove(data,NW,K,Fs)

% NW=5;
% K=9;

pad = 4800;
if ~exist('Fs')
    Fs = 200;
end

if iscell(data)
    nchns=size(data{1,1},1);
    ntrls=size(data,2);


    for i=1:nchns
        for j=1:ntrls
            NGPTS=size(data{j},2);
            y = (squeeze(data{j}(i,:)))';
            % [Pyy, f] = pmtm(y, NW, pad, Fs);
            [fstat, mu, f, trueid] = FTEST(y, NW, K, pad, Fs);
            harmon = RECONSTR(fstat, mu, NGPTS, trueid, 3)';
            newdat{j}(i,:) = y - harmon;
        end
    end

elseif isvector(data)
    NGPTS=length(data);
    y = vecc(data);
    % [Pyy, f] = pmtm(y, NW, pad, Fs);
    [fstat, mu, f, trueid] = FTEST(y, NW, K, pad, Fs);
    harmon = RECONSTR(fstat, mu, NGPTS, trueid, 3)';
    newdat = y - harmon;
end