function [data,lplength,varargout] = mvar_test_lfp_prep(trials,channels,varargin)
% to run in the session directory
% output: data{period}(samples x trials x channels)

Args = struct('plength',500,'downSR',200,'beforeSac',0,'EPnotRemoved',0);
Args.flags = {'beforeSac','EPnotRemoved'};
[Args,modvarargin] = getOptArgs(varargin,Args);


plength = Args.plength;

downsamp = 1000/Args.downSR;
lplength = plength/downsamp;

mtst = ProcessSession(mtstrial,'auto');

[pdir,cdir] = getDataDirs('lfp','relative','CDNow');
files = nptDir('*_lfp.*');

count = 1;

for t = vecr(trials)
    periods(1) = round((mtst.data.CueOnset(t) - plength)/downsamp);
    nch = 1;
    for ch = channels
        [lfp,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(files(t).name,ch);
        [dlfp,SR] = preprocessinglfp(lfp,'downSR',Args.downSR,modvarargin{:});
        %         datacut = [];
        for p = 1 : length(periods)
            %             datacut = [datacut vecr(dlfp(periods(p):periods(p) + lplength))];
            data(:,count,nch) = dlfp(periods(p):periods(p) + lplength);

        end
        %         data{ch}(:,count) = single(datacut);
        nch = nch + 1;
    end
    count = count + 1;
    clear lfp rslfp flfp dlfp
end

if ~Args.EPnotRemoved
    for p = 1 : length(periods)
        pdata = permute(data,[1 3 2]);
        data = lfp_normalize(pdata,1);
        data = permute(data,[1 3 2]);

    end
end
cd(cdir)
varargout{1} = periods;