function clust_prep_4th_epoch_9_stim(monkeyname)

% prepares gray data for cluster - loads proprod m-files and formats them
% for cluster
cd(monkeyname);
if strcmp(monkeyname,'clark')==1
    monkeynamepre='cl';
    days = nptDir('*06*');
else
    monkeynamepre='be';
    days = nptDir('*07*');
end

mkdir clustdat_4th_epoch_9_stim
for i=1:size(days,1)
    cd clustdat_4th_epoch_9_stim
    mkdir(days(i).name)
    cd(days(i).name)
    fid=fopen('cogctf_com','wt');
    cd ..
    cd ..
    cd(days(i).name);
    for sess = {'02', '03'}
        cd(strcat('session',sess{1}))
        cd prepro
        load(strcat(days(i).name,sess{1},'_4_epochs.mat'));
        load(strcat(days(i).name,sess{1},'prepro.mat'));
        days(i).name
        sess
        cd ..
        cd ..
        cd ..
        cd clustdat_4th_epoch_9_stim
        cd(days(i).name)
        for k=4:4
            a=0;
            for j=1:size(data_sepstim,1)
                a=a+1;
                dattmp = data_sepstim{j,k};
                dattmpp = permute(dattmp,[3 1 2]);
                dattmpprsh = reshape(dattmpp(gchannels,:,:),[size(gchannels,2) size(dattmpp,2)*size(dattmpp,3)]);
                save(strcat(monkeynamepre,'_',days(i).name,'_',sess{1},'e',num2str(k),'_',num2str(a),'.dat'),'dattmpprsh','-ascii');
                fprintf(fid,[monkeynamepre ' ' days(i).name ' ' sess{1} 'e' num2str(k) ' ' num2str(a),' %1.0f %1.0f 101 20 11 200 100 richter\n'], size(gchannels,2), size(dattmpp,3))
            end
        end
        cd ..
        cd ..
        cd(days(i).name);
    end
    fclose(fid);
    cd ..
end






