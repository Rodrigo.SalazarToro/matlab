function [epochdnr,infocor,datacor,pointscor,outdat_incor,ptsout_incor,outinfo_incor]=preprocess2(data,info,points,opt)

% separate correct and incorrect trials
[datacor,pointscor,outdat_incor,ptsout_incor,infocor,outinfo_incor]=divide_resp(data,points,info);
% subensemble
[epoch]=epocher(datacor,infocor.MatchOnset,pointscor,opt);
% detrend
epochd = lfp_detrend(epoch);
% normalize (mean subtraction & division by std)
epochdnr = lfp_normalize(epochd,2);