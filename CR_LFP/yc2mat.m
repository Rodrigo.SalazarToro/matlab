function [out]=yc2mat(nchans,freq,varargin);

% converst the output of Yonghong Chen's pwcausal output to chanxchanxfreq
% one input array of cohe gives coherence, two (Fx2y, Fy2x) gives GC  

a=0;
out=zeros(nchans,nchans,freq);
if nargin==3
    for i=1:nchans
        for j=i+1:nchans
            a=a+1;
            out(i,j,:)=varargin{1}(a,:);
            out(j,i,:)=varargin{1}(a,:);
        end
    end
end

if nargin==4
    for i=1:nchans
        for j=i+1:nchans
            a=a+1;
            out(i,j,:)=varargin{1}(a,:);
            out(j,i,:)=varargin{2}(a,:);
        end
    end
end
        