function epoch_vis
 cd('/Applications/MATLAB74/work/Clark/060328/session02/mvar')
load('06032802_4_epoch_spect.mat');
 cd('/Applications/MATLAB74/work/Clark/060328/session02/prepro')
load('06032802prepro.mat');
%a = ls;
% %load(a(3,:))
% load(ls)
% cd ..
% % cd('/Applications/MATLAB74/work/Clark')
% cd MVAR
% %a = ls;
% %load(a(3,:))
% load(ls)
% cd ..

for i=1:size(C,2)
    for j=i+1:size(C,2)
        scrsz = get(0,'ScreenSize');
        figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2])
        for k=1:4
            subplot(2,2,k)
            hold on
            hl1 = line(1:100,squeeze(C(k,i,j,:)),'Color','k');
            ylabel('Coherence')
            ax1 = gca;
            set(ax1,'XColor','k','YColor','k','xlim',[1 100],'ylim',[0 1.10*max(max(C(:,i,j,:)))])
            ax2 = axes('Position',get(ax1,'Position'),...
                'YAxisLocation','right',...
                'Color','none',...
                'YColor','r','xlim',[1 100],'ylim',[0 1.10*max(max(max(G(:,j,i,:))),max(max(G(:,i,j,:))))]);
            hl3 = line(1:100,squeeze(G(k,i,j,:)),'Color','r');
            hl2 = line(1:100,squeeze(G(k,j,i,:)),'Color','b');
            ylabel('Granger Casuality')
            xlabel('Hz')
            title(['Epoch ' num2str(k)]);

        end
        if info2.Index(1)==1
            text(0.7,2.5,'IDENTITY','Units','normalized')
        elseif info2.Index(1)==2
            text(0.7,2.5,'LOCATION','Units','normalized')
        end
        legend_h=legend(strcat(num2str(gchannels(i)),neuroinfo.cortex(gchannels(i)),'-->',num2str(gchannels(j)),neuroinfo.cortex(gchannels(j))),strcat(num2str(gchannels(i)),neuroinfo.cortex(gchannels(i)),'<--',num2str(gchannels(j)),neuroinfo.cortex(gchannels(j))));
        text(-1.5,2.5,pwd,'Units','normalized')
        pause
        close
    end
end
clear all
