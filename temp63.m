

clear all
cd /Volumes/raid/data/monkey

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number','snr',99);

% [PREmugram,time,f,ntrials,sig,idePairs,presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','biasCorr','minusPresample','addNameFile','noSel','chance',0.05,'relFreq',Flim,'getPrefStim','redo','delayRange',[0 0.4],'locSel','max');

locselects = {'max' 'inter' 'min'};
biases = {'ExpV' 'PresampleDiv' 'PresampleZscore'};
periods = {'Delay' 'PRESAMPLE'};
prange = [0.1 0.4];

Flim = [12 22];
sdir = pwd;
betafreq = find(f>= 12 & f <= 22);
for lcs = 1 : 3
    cd(sdir)
    [idemugram,time,f,ntrials,sig,idePairs,presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('PRElocSel%s',locselects{lcs}),'getPrefStim','delayRange',[0.1 0.4],'locSel',locselects{lcs});
    cd(sdir)
    save(sprintf('PRElocSel%s.mat',locselects{lcs}))
    [locamugram,time,f,ntrials,sig,idePairs,presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('PREideSel%s',locselects{lcs}),'getPrefStim','redo','save','locSel',locselects{lcs},'delayRange',[0.1 0.4]);
    cd(sdir)
    save(sprintf('PREideSel%s.mat',locselects{lcs}))
   
    set(gcf,'Name',sprintf('ideSel%s',locselects{lcs}))
    nsessions = length(unique(obj.data.setNames(ind(idePairs))));
    npairs = length(idePairs);
    figure
    set(gcf,'Name',sprintf('locSel%s',locselects{lcs}))
    stimlab = {'r' 'b' 'k'};
    betafreq = find(f> Flim(1) & f< Flim(2));
    for stim = 1 : 3
        subplot(5,1,1)
        me = squeeze(mean(mean(prefide(stim,idePairs,:,betafreq),4),2));
        ste = squeeze(std(mean(prefide(stim,idePairs,:,betafreq),4),[],2)) / sqrt(length(idePairs));
        ste2 = squeeze(std(mean(prefide(stim,idePairs,:,betafreq),4),[],2)) / sqrt(nsessions);
        plot(time,me,stimlab{stim})
        hold on
        plot(time,me + ste,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste,sprintf('%s--',stimlab{stim}))
        plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
        set(gca,'TickDir','out')
        ylim([0.13 0.22])
        xlim([0.11 1.8])
        title(sprintf('mean coherence for the 3 ides; n=%d',npairs))
        xlabel('Time [sec.]')
        ylabel('Coherency')
        
        
        subplot(5,1,2)
        me = squeeze(circ_mean(circ_mean(prefidePhaseSTD(stim,idePairs,:,betafreq),[],4),[],2));
        ste = squeeze(circ_std(circ_mean(prefidePhaseSTD(stim,idePairs,:,betafreq),[],4),[],[],2)) / sqrt(length(idePairs));
        ste2 = squeeze(circ_std(circ_mean(prefidePhaseSTD(stim,idePairs,:,betafreq),[],4),[],[],2)) / sqrt(nsessions);
        plot(time,me,sprintf('%s',stimlab{stim}))
        hold on
        plot(time,me + ste,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste,sprintf('%s--',stimlab{stim}))
        plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
        set(gca,'TickDir','out')
        %     ylim([0.13 0.22])
        xlim([0.11 1.8])
        title('mean phase variance for the 3 ides')
        xlabel('Time [sec.]')
        ylabel('Phase variance')
        
        
        
        for area = 1 : 2
            subplot(5,1,2+area)
            me = squeeze(mean(mean(prefideS{area}(stim,idePairs,:,betafreq),4),2));
            ste = squeeze(std(mean(prefideS{area}(stim,idePairs,:,betafreq),4),[],2)) / sqrt(length(idePairs));
            ste2 = squeeze(std(mean(prefideS{area}(stim,idePairs,:,betafreq),4),[],2)) / sqrt(nsessions);
            plot(time,me,sprintf('%s',stimlab{stim}))
            hold on
            plot(time,me + ste,sprintf('%s--',stimlab{stim}))
            plot(time,me - ste,sprintf('%s--',stimlab{stim}))
            plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
            plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
        end
        set(gca,'TickDir','out')
        %     ylim([0.13 0.22])
        xlim([0.11 1.8])
        title('mean power for the 3 ides')
        xlabel('Time [sec.]')
        ylabel('power')
    end
    
    subplot(5,1,5)%      set(gcf,'Name',cellnames{ct})
    
    % plot(time,prctile(squeeze(mean(idemugram(idePairs,:,betafreq),3)),[25 50 75],1))
    me = mean(squeeze(mean(idemugram(idePairs,:,betafreq),3)),1);
    ste = std(squeeze(mean(idemugram(idePairs,:,betafreq),3)),[],1) / sqrt(length(idePairs));
    ste2 = std(squeeze(mean(idemugram(idePairs,:,betafreq),3)),[],1) / sqrt(nsessions);
    plot(time,me)
    hold on
    plot(time,me+ ste,'--')
    plot(time,me- ste,'--')
    plot(time,me+ ste2,'--')
    plot(time,me- ste2,'--')
end

%%
locs = {'max' 'inter' 'min'};
parfor lc = 1 : 3
    [idemugramLcos{lc},time,f,ntrials,sig,idePairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DelaylocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','redo','save');
end