% Index(:,1) = pair number
% Index(:,2) = day number
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% all of the above is either 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules

% Index(:,7) = beta differences between the two rules (0 for no, 1 for delay 1, 2 for delay 2 and 3 for both)
% Index(:,8) = gamma differences between the two rules (0 for no, 1 for
% fix, 2 for delay 2 and 3 for both)
% Index(:,9) = histology (1: PPCm-PFCd; 2: PPCm-PFCv; 3 PPCl-PFCd;
% 4 : PPCl-PFCv; 5: PPCs-PFCd; 6: PPCs-PFCv; 7: PPCm-PFCs; 8: PPCl-PFCs)
%
% Index(:,10:11) = group number
% Index(:,12) = IDENTITY rule, no tuning (0), tuning for the
% identity (1) or the location (2) or (3) both location and idenities tuning of the sample only for the beta
% range of course
% Index(:,13) = LOCATION rule, no tuning (0), tuning for the
% identity (1) or the location (2) of the sample only for the beta
% range of course
% Index(:,14) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 3
% Index(:,15) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 4
% Idex(:,16) ide tuning index during IDE rule for window 4 PP
% Idex(:,17) ide tuning index during LOC rule for window 4 PP
% Idex(:,18) loc tuning index during IDE rule for window 4 PP
% Idex(:,19) loc tuning index during LOC rule for window 4 PP
% Idex(:,20) ide tuning index during IDE rule for window 4 PF
% Idex(:,21) ide tuning index during LOC rule for window 4 PF
% Idex(:,22) loc tuning index during IDE rule for window 4 PF
% Idex(:,23) loc tuning index during LOC rule for window 4 PF


clear n
rulesind = {[16 18; 20 22] [17 19; 21 23]};
for r = 1 : 2
    figure
    ind = find(index(:,3) == 3); % | index(:,6) == 2 | index(:,6) == 3);
    for sb = 1 : 2
        subplot(2,1,sb)
        [n(:,1),xout] = hist(index(ind,rulesind{r}(1,sb)),[0:0.05:0.8]);
        hold on
        [n(:,2),xout] = hist(index(ind,rulesind{r}(2,sb)),[0:0.05:0.8]);
        bar(xout,n)
        axis([ 0 0.8 0 25])
    end
end



