[ind_pairs_sort pair_directory_sort] = mtsPairs_sort('PF');

for pair = 1 :  length(ind_pairs_sort)
    
    dupl = find(diff(ind_pairs_sort(pair).days) == 0);
    therep = unique(ind_pairs_sort(pair).days(dupl))';
    for nrep = 1 : ind_pairs_sort(pair).number
        if ~isempty(find(nrep == therep))
            trep = find(ind_pairs_sort(pair).days == nrep);
            thedays = pair_directory_sort(pair).days(trep);
            clear C
            for d = 1 : length(thedays)
                cd(thedays{d})
                load('cohInter.mat')
                fband = find(f > 8 & f < 50);
                tchpair(d) = find(ind_pairs_sort(pair).channels(trep(d),1) == Day.comb(:,1) & ind_pairs_sort(pair).channels(trep(d),2) == Day.comb(:,2));
                C(d) = max([max(max(Day.session(1).C(fband,tchpair(d),:))) max(max(Day.session(2).C(fband,tchpair(d),:)))]);
                cd ..
            end
            [value, thed] = max(C);
            ind = thed + trep(1) - 1;
            chpair = tchpair(thed);
        else
            ind = find(ind_pairs_sort(pair).days == nrep);
            cd(pair_directory_sort(pair).days{ind})
            load('cohInter.mat')
            chpair = find(ind_pairs_sort(pair).channels(ind,1) == Day.comb(:,1) & ind_pairs_sort(pair).channels(ind,2) == Day.comb(:,2));
            cd ..
        end
        ind_pairs_sort(pair).indDays{nrep} = pair_directory_sort(pair).days{ind};
        ind_pairs_sort(pair).indCh(nrep,:) = ind_pairs_sort(pair).channels(ind,:);
        ind_pairs_sort(pair).indPair(nrep,:) = chpair;
    end
    
end

load switchdays.mat

for d = 1 : length(days);chList(d).pairs = []; end
for pair = 1 : length(ind_pairs_sort)
    
    for d = 1 : length(days)
        ind = strmatch(days{d},ind_pairs_sort(pair).indDays);
        if ~isempty(ind)
            chList(d).days = days{d};
            chList(d).pairs = [chList(d).pairs ind_pairs_sort(pair).indPair(ind)];
            
        end
    end
end
            
 for d = 1 : length(days)
     cd(days{d})
        lastP(d) = max(chList(d).pairs);
        load cohInter.mat
        RealP(d) = size(Day.comb,1);
     indpairs = sort(chList(d).pairs);
     save('indpairs.mat','indpairs')
     cd ..
     
 end
 
  [lastP RealP]