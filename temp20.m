params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1,'err',[2 0.05]);
% matlabpool local 4
for d = 1 : 3
    clear Day Cmean Cstd phimean phistd tile99 Prob
    cd(redoDays{d})
    data = load('cohInter.mat');
    sur = load('cohInterSur.mat');
    ! mv cohInterSur.mat cohInterSurOld.mat
   Day.name = sur.Day.name;
    Day.option = sur.Day.option;
    for s = 1 : length(data.Day.session)
        
        for cmb = 1 : size(data.Day.comb,1)
            nind = find(data.Day.comb(cmb,1) == sur.Day.comb(:,1) & data.Day.comb(cmb,2) == sur.Day.comb(:,2));
            if ~isempty(nind)
                Day.comb(cmb,:) = sur.Day.comb(nind,:);
                fields = fieldnames(sur.Day.session(s));
                for field = 1 : length(fields)
                    if ~unique(strcmp(fields{field},{'Prob' 'norm' 'rule' 'trials'}))
                    eval(sprintf('Day.session(s).%s(:,cmb,:) = sur.Day.session(s).%s(:,nind,:);',fields{field},fields{field}))
                    end
                end
                Day.session(s).Prob(:,:,cmb,:) = sur.Day.session(s).Prob(:,:,nind,:);
            else
                selectTrials = data.Day.session(s).trials ;
                plength = 400;
                cd session01
                
                channels = data.Day.comb(cmb,:);
                [lfp,~,periods,norm] = lfpPcut(selectTrials,channels,'plength',plength,'bandP',[3 90]);
                for p = 1 : 4
                    [Cmean(:,p),Cstd(:,p),phimean(:,p),phistd(:,p),tile99(:,p),Prob(:,:,p),f] = cohsurrogate(lfp{p}(:,:,1),lfp{p}(:,:,2),params);%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                end
                Day.comb(cmb,:) = data.Day.comb(cmb,:);
                fields = fieldnames(sur.Day.session(s));
                for field = 1 : length(fields)
                    if ~unique(strcmp(fields{field},{'Prob' 'norm' 'rule' 'trials'}))
                    eval(sprintf('Day.session(s).%s(:,cmb,:) = %s;',fields{field},fields{field}))
                    end
                end
                Day.session(s).Prob(:,:,cmb,:) = Prob(:,:,:);
                
                cd ..
            end
            
        end
        Day.session(s).norm = data.Day.session(s).norm;
        Day.session(s).rule = data.Day.session(s).rule;
        Day.session(s).trials = data.Day.session(s).trials;
    end
    save cohInterSur.mat Day f
    fprintf('saving in %s',pwd)
    
    
    cd ..
    
end
