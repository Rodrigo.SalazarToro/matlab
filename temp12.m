% cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
% first raw location;
% second raw identities
% allspikes{cue}(tcount,p)


areas = {'P' 'F'};
rules = {'I' 'L'};
wind = {'pre-sample' 'sample' 'delay 1' 'delay2' 'saccade'};
for p = 1 : 5
    h(p) = figure;
    set(h(p),'Name',wind{p})
    mx = [];
    for rule =1 : 2
        for area = 1 : 2
            ind = intersect(strmatch(rules{rule},index(:,2)),strmatch(areas{area},index(:,3)));
            
            subplot(2,2,(area-1) * 2 + rule)
            plot(index(ind,27+p),index(ind,32+p),'.')
            mx = max([mx nanmax(index(ind,27+p)) nanmax(index(ind,32+p))]);
            hold on
            line([0 25],[0 25]);
            xlabel('ide tuning')
            ylabel('loc tuning')
            title(sprintf('area %s rule %s',areas{area},rules{rule}))
            axis([0 25 0 25])
        end
    end
    for sb = 1 : 4; subplot(2,2,sb); axis([0 max(mx) 0 max(mx)]); end
end



data = load('cohInter.mat');

fix = load('cohInterfix.mat')
f = data.f;

for pair = 1 : size(data.Day.comb,1)
    for p = 1 : 4
        subplot(4,1,p)
        plot(f,data.Day.session(1).C(:,pair,p),'r')
        hold on
        plot(f,fix.Day.session(1).C(:,pair,p),'k')
    end
    pause
    clf
end

