
clear all
cd F:\salazar\data\Electrophy
load('psthobj20151001.mat')
sound = [5 6 17 21 33 34 37 38 44];
odor = [2 3 6 13 16 18 22 26 27 37 43 ];


[r,n]=get(obj,'ChR2',1,'psthbin',100,'number');
clear mpsth
ttype = 2;
psthbin = 100;
for ncell = 1 : length(odor)
    cd(obj.data.setNames{n(odor(ncell))})
    
    load spikesdata.mat
    
    [tspth,xtick] = hist(spikes{ttype},[0:psthbin:5000]);
    mpsth(ncell,:) = (1000/psthbin)* (tspth(1:end-1)/ntrials(ttype));
    
end
figure
subplot(2,1,1)
errorbar(xtick(1:end-1),mean(mpsth,1),std(mpsth,[],1)/sqrt(length(odor)))
title('Odor')
ylabel('Firing rate (sp/s)')
xlim([-100 5000])
text(0,10,sprintf('n=%d',length(odor)))
ylim([0 12])
clear mpsth
ttype = 3;
psthbin = 100;
for ncell = 1 : length(sound)
    cd(obj.data.setNames{n(sound(ncell))})
    
    load spikesdata.mat
    
    [tspth,xtick] = hist(spikes{ttype},[0:psthbin:5000]);
    mpsth(ncell,:) = (1000/psthbin)* (tspth(1:end-1)/ntrials(ttype));
    
end
subplot(2,1,2)
%  errorbar(xtick(1:end-1),mean(mpsth,1),std(mpsth,[],1)/sqrt(length(sound)))
plot(xtick(1:end-1),mean(mpsth,1))
hold on
plot(xtick(1:end-1),mean(mpsth,1)+std(mpsth,[],1)/sqrt(length(sound)))
plot(xtick(1:end-1),mean(mpsth,1)-std(mpsth,[],1)/sqrt(length(sound)))
title('Sound')
ylabel('Firing rate (sp/s)')
xlabel('Time (ms)')

text(0,10,sprintf('n=%d',length(sound)))
ylim([0 12])
xlim([-100 5000])




%%% temp psth workng memory
c = 1;
clear psth
for gr = 1 : 5
    cd(sprintf('group000%d',gr))
    clusters = nptDir('cluster*');
    
    for cl = 1 : length(clusters)
        cd(clusters(cl).name)
        load('ispikes.mat')
        
        ctimes = [];
        ttime = [0 : 100: 8000];
        for tr = 1 : sp.data.numTrials
            ctimes = cat(2,ctimes,sp.data.trial(tr).cluster.spikes);
        end
        n =  hist(ctimes,ttime);
        
        psth(c,:) = 10*n / sp.data.numTrials;
        c = c + 1;
        cd ..
    end
    cd ..
end


figure;

plot(ttime,mean(psth))
hold on
plot(ttime,mean(psth) + std(psth)/sqrt(size(psth,1)))
plot(ttime,mean(psth) - std(psth)/sqrt(size(psth,1)))
ylabel('Firing rate (sp/s)')
xlabel('Time (ms)')