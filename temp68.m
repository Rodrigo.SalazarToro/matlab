% at the day level
clear all
monkeys = {'clark' 'betty'};
count = 1;
for m = 1 : 2
    cd(monkeys{m})
    
    if m == 1
        load idedays
        days = idedays;
        
    else
        load idedays
        load longdays
        days = [idedays lcdays lbdays];
    end
    for d = 1 : length(days)
        cd(days{d})
        cd grams
        
        files = nptDir('migramg*Rule1IdePerLoc1sameTIncor.mat');
        
        
        for fi = 1 : length(files)
            otherloc2 = regexprep(files(fi).name,'Loc1','Loc2');
            otherloc3 = regexprep(files(fi).name,'Loc1','Loc3');
            
            iloc1 = regexprep(files(fi).name,'sameTIncor','Incor');
            iloc2 = regexprep(iloc1,'Loc1','Loc2');
            iloc3 = regexprep(iloc1,'Loc2','Loc3');
            
            if ~isempty(nptDir(otherloc2)) && ~isempty(nptDir(otherloc3)) && ~isempty(nptDir(iloc1)) && ~isempty(nptDir(iloc2)) && ~isempty(nptDir(iloc3))
                cdloc1 = load(files(fi).name);
                cdloc2 = load(otherloc2);
                cdloc3 = load(otherloc3);
                
                idloc1 = load(iloc1);
                idloc2 = load(iloc2);
                idloc3 = load(iloc3);
                
                % f= 5:8; t = 21:33
                
                mcohiloc1 = median(median(idloc1.C{1}(21:33,5:8))) + median(median(idloc1.C{2}(21:33,5:8))) + median(median(idloc1.C{3}(21:33,5:8)));
                mcohiloc2 = median(median(idloc2.C{1}(21:33,5:8))) + median(median(idloc2.C{2}(21:33,5:8))) + median(median(idloc2.C{3}(21:33,5:8)));
                mcohiloc3 = median(median(idloc3.C{1}(21:33,5:8))) + median(median(idloc3.C{2}(21:33,5:8))) + median(median(idloc3.C{3}(21:33,5:8)));
                
                mcohcloc1 = median(median(cdloc1.C{1}(21:33,5:8))) + median(median(cdloc1.C{2}(21:33,5:8))) + median(median(cdloc1.C{3}(21:33,5:8)));
                mcohcloc2 = median(median(cdloc2.C{1}(21:33,5:8))) + median(median(cdloc2.C{2}(21:33,5:8))) + median(median(cdloc2.C{3}(21:33,5:8)));
                mcohcloc3 = median(median(cdloc3.C{1}(21:33,5:8))) + median(median(cdloc3.C{2}(21:33,5:8))) + median(median(cdloc3.C{3}(21:33,5:8)));
                
                mcohloc1 = mcohcloc1;% + mcohiloc1;
                mcohloc2 = mcohcloc2;% + mcohiloc2;
                mcohloc3 = mcohcloc3;% + mcohiloc3;
                
                [~,sloc] = max([mcohloc1 mcohloc2 mcohloc3]);
                %                 for sloc = 1 : 3
                thecorrect = eval(sprintf('cdloc%d',sloc));
                theincorrect = eval(sprintf('idloc%d',sloc));
                mintrials = min(cellfun(@length,thecorrect.trials));
                
                
                p(count) = signrank(reshape(thecorrect.mutualgram(21:33,5:8),1,13*4),reshape(theincorrect.mutualgram(21:33,5:8),1,13*4));
                diffM(count) = (median(median(thecorrect.mutualgram(21:33,5:8))) - median(median(theincorrect.mutualgram(21:33,5:8)))) / (median(median(thecorrect.mutualgram(21:33,5:8))) + median(median(theincorrect.mutualgram(21:33,5:8))));
                diffcoh(count) = (mcohiloc1 + mcohiloc2 + mcohiloc3) / (mcohcloc1 + mcohcloc2 + mcohcloc3);
                allmtrials(count) = mintrials;
                
                count = count + 1;
                %                 end
                
            end
        end
        cd ../..
    end
    cd ..
end


figure
sigpairs = find(p<(0.05) & allmtrials > 15);

hist(diffM(sigpairs),100)

spairs = find(allmtrials > 15 & abs(diffcoh) < 1.1 & abs(diffcoh) > 0.9);

figure
[n,xout] = hist(diffM(spairs),100);
bar(xout,n)
hold on


line([median(diffM(spairs)) median(diffM(spairs))],[0 max(n)],'Color','r')
pvalue = signrank(diffM(spairs))

