load switchdays.mat
allspect = [];
% h = figure;
leg = {'IDENTITY' 'LOCATION'};
% for d = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16]; 
%     cd(days{d})
%     set(h,'Name',days{d})
%     load('cohInter.mat')
%     for r = 1 : 2
%     data = Day.session(r).C;
%     nf = size(data,2) * size(data,3);
%     spectra = reshape(data,65,nf)';
%     allspect = [allspect; spectra];
%     %     plot(f,spectra);
%     [n,xout] = hist(spectra,[0:0.01:1]);
%     n = n/4;
%     subplot(2,1,r)
%     surf(f,xout,n)
%     title(leg{r})
%     xlabel('Freq. [Hz]')
%     ylabel('coherence')
%     zlabel('# pairs')
%     end
%     pause
%     clf
%     cd ..
% end
wind = {'PreSample','Sample','Delay1','Delay2'};
ff = figure;
for d = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16]; 
    cd(days{d})
    [ich,iCHcomb,iflat,igroups] = getFlatCh;
    load('cohInter.mat')
    thech = setdiff(ich,iflat.ch);
    thecomb = setdiff([1:size(Day.comb,1)],iflat.pairs);
%     nch =  unique(Day.comb);
    for ch =  1 : length(thech)
         
        [r,c] = find(Day.comb(thecomb,:) == thech(ch));
        maxy = [];
        for p =1 : 4
        data = Day.session(1).C(:,thecomb(r),p);
        
%         nf = size(data,2) * size(data,3);
%         spectra = reshape(data,65,nf)';
        
        %     plot(f,spectra);
        subplot(4,1,p)
        plot(f,data)
        title(wind{p})
        maxy = [maxy max(max(data))];
        set(ff,'Name',sprintf('%s; group %d',days{d},igroups(ch)));
        end
%         subplot(2,1,2)
%         load('cohInterOld.mat')
%         [r,c] = find(Day.comb == nch(ch));
%         data = Day.session(1).C(:,r,:);
%         nf = size(data,2) * size(data,3);
%         spectra = reshape(data,65,nf)';
%           plot(f,spectra)
%           themax = max([max(max(spectra)) maxy]);
          for sb = 1 : 4; subplot(4,1,sb);axis([0 100 0 max(maxy)]); end
        pause
        clf
    end
    cd ..
end

