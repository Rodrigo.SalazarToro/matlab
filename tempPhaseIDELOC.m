% 1:'6DR ' 2:'8AD ' 3:'8B  ' 4:'dPFC ' 5:'vPFC ' 6:'PS  ' 7:'AS  ' 8:'PEC ' 9:'PGM ' 10:'PE  ' 11:'PG  ' 12:'MIP ' 13:'LIP ' 14:'PEcg' 15:'IPS ' 16:'WM ' 17:'9L  '};
cd D:\workingmemory\data\monkey
clear all

LOCobj = loadObject('mtsXCobjLOC.mat');
IDEobj = loadObject('mtsXCobjIDE.mat');
[sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);


IDEobj.data.sigPhase(IDEobj.data.sigPhase == 0) = nan;
LOCobj.data.sigPhase(LOCobj.data.sigPhase == 0) = nan;

modphase = ones(LOCobj.data.numSets,1);
modphase(diff(LOCobj.data.hist,[],2) < 0) = -1;
LOCobj.data.sigPhase= LOCobj.data.sigPhase .* -(repmat(modphase,1,4,101));


modphase = ones(IDEobj.data.numSets,1);
modphase(diff(IDEobj.data.hist,[],2) < 0) = -1;
IDEobj.data.sigPhase= IDEobj.data.sigPhase .* -(repmat(modphase,1,4,101));

pairs = unique(IDEobj.data.hist(sindIDE,:),'rows');

gr1 = [4 11 13];
gr2 = [2 3 8 10];
allgr = [2 3 4 8 10 11 13];



epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};

areas{1} = [8 10 11 13];
areas{2} = [2 3 4];
areas{3} = [2 8; 2 10; 2 11; 2 13; 3 8; 3 10; 3 11; 3 13;4 8; 4 10; 4 11; 4 13];
areas{4} = [2 3 4 8 10 11 13];
areas{5} = [4 11; 4 13];
areasL = {'PFC' 'PPC' 'PFC_PPC' 'all' 'dPFC vs PG or LIP'};

bin =1;
frange = [1 : 7];
plotrange = [-40 40];
histrange = [-180 : bin : 180];
conds = {  };
for a = 1 : length(areas)
    f1 = figure;
     set(gcf,'Name',areasL{a})
    f2 = figure;
     set(gcf,'Name',areasL{a})
    srow = [];
    if a ==3
        for li = 1 : size(areas{a},1)
            srow = [srow; find(ismember(pairs,areas{a}(li,:),'rows') == 1)];
        end
    else
        srow = find(sum(ismember(pairs,areas{a}),2) == 2);
    end
    spairs = pairs(srow,:);
    [~,ind] = get(IDEobj,'Number','hist',spairs);
    mnan = ~isnan(IDEobj.data.sigPhase);
    for ep = 1 : 4
        figure(f1)
        subplot(3,4,ep)
        
        dist1 = nanmedian(IDEobj.data.sigPhase(ind,ep,frange),3);
        dist2 = nanmedian(LOCobj.data.sigPhase(ind,ep,frange),3);
        %         rose(dist1,[deg2rad(bin) : deg2rad(bin) : 2*pi]);
        hist(rad2deg(dist1),histrange);
        title(epochs{ep})
        ylabel('IDENTITY RULE')
        xlim(plotrange)
%         title(sprintf('mean = %d degree',rad2deg(circ_mean(dist1))))
        xlabel(sprintf('mean = %d degree',rad2deg(nanmedian(dist1))))
        
        subplot(3,4,ep+4)
        
        %         rose(dist2,[deg2rad(bin) : deg2rad(bin) : 2*pi]);
        hist(rad2deg(dist2),histrange);
        
        %         xlabel(sprintf('%d',pval))
%         title(sprintf('mean = %d degree',rad2deg(circ_mean(dist2))))
        xlim(plotrange)
        ylabel('LOCATION RULE')
        xlabel(sprintf('median = %d degree',rad2deg(nanmedian(dist2))))
        subplot(3,4,ep+8)
        hist(rad2deg(dist2-dist1),histrange);
        title(sprintf('median = %d degree',rad2deg(nanmedian(dist2 - dist1))))
        xlim(plotrange)
        
        ylabel('LOC - IDE')
        figure(f2)
        
        
        
        subplot(1,4,ep)
        
        plot(rad2deg(dist1),rad2deg(dist2),'.')
        xlabel('IDENTITY RULE')
        ylabel('LOCATION RULE')
        title(epochs{ep})
         xlim(plotrange)
          ylim(plotrange)
    end
   
    xlabel('Phase (deg)')
    ylabel('# pairs')
end


