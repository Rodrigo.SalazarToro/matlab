ncomb = nchoosek([1:48],2);

params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100]);
prob = [0.95 0.99 0.999 0.9999 0.99999 0.999999];

parfor cc = 1 : 600
for p = 1 : 4
c1 = ncomb(cc,1);
c2 = ncomb(cc,2);
file = sprintf('c%dc%d10000p%d750t.mat',ncomb(cc,1),ncomb(cc,2),p);
[Cmean,Cstd,phimean,phistd,tile99,Prob,f] = cohsurrogate(squeeze(data{p}(:,:,ncomb(cc,1))),squeeze(data{p}(:,:,ncomb(cc,2))),params,'saveName',file,'Prob',prob,'nrep',10000);
end
end


[r,ind] = get(bothMonkeys,'Number','snr',[]);r
ty = unique(bothMonkeys.data.hist(ind,:),'rows');

for histpair = 1 : size(ty,1)
    [r,ind] = get(bothMonkeys,'Number','snr',[],'hist',ty(histpair,:));
    c = 1;
    for i = ind
        cd(bothMonkeys.data.setNames{i})
        load cohInter.mat
        C{histpair}(c,:,:) = squeeze(Day.session(1).C(:,bothMonkeys.data.Index(i,1),:));
        phi{histpair}(c,:,:) = squeeze(Day.session(1).phi(:,bothMonkeys.data.Index(i,1),:));
        c = c + 1;
    end
end


figure
for tp = 1 : 22
    for p = 1 : 5
        subplot(22,5,(tp-1) * 5 + p)
        if p < 5
            plot(f,squeeze(mod(phi{tp}(:,:,p) + 2*pi,2*pi)),'.')
            axis([0 60 0 2*pi])
            set(gca,'xTick',[])
        else
            text(0.2,0.2,sprintf('%s %s',areas{ty(tp,1)},areas{ty(tp,2)})); 
            set(gca,'xTick',[])
        end
    end
end

figure
for tp = 1 : 22
    for p = 1 : 5
        subplot(22,5,(tp-1) * 5 + p)
        if p < 5
            plot(f,C{tp}(:,:,p))
            axis([0 60 0 0.6])
            set(gca,'xTick',[])
        else
            text(0.2,0.2,sprintf('%s %s',areas{ty(tp,1)},areas{ty(tp,2)})); 
            set(gca,'xTick',[])
        end
    end
end