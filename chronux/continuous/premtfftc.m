function [J,f] = premtfftc(data,params)


data=change_row_to_column(data);
[tapers,pad,Fs,fpass,err,trialave,params]=getparams(params);

N=size(data,1);
nfft=2^(nextpow2(N)+pad);
[f,findx]=getfgrid(Fs,nfft,fpass); 
tapers=dpsschk(tapers,N,Fs); % check tapers
J=mtfftc(data,tapers,nfft,Fs);
J=J(findx,:,:);