function [dz,vdz,Adz,f,varargout] = compareCoherence(data1,data2,params,varargin)
% data1 and data2 in format samples x trials x channels. Trial should be of
% the same length and channels should only be two. Data1 and data2
% correspond to the two conditions.
%
% Outputs:
% dz    test statistic (will be distributed as N(0,1) under H0
% vdz   Arvesen estimate of the variance of dz
% Adz   1/0 for accept/reject null hypothesis of equal population
%       coherences based dz ~ N(0,1)
% f     frequency
%
% Note: all outputs are functions of frequency

Args = struct('p',0.05,'plot',0,'diffNtrials',0,'smoothing',[]);
Args.flags = {'plot','diffNtrials'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'p'});

if ~Args.diffNtrials
    nch = size(data1,3);
   params.trialave = 0;
    
    for ch = 1 : nch
        
        [Jc1{ch},f] = premtfftc(squeeze(data1(:,:,ch)),params);
        [Jc2{ch},f] = premtfftc(squeeze(data2(:,:,ch)),params);
        
        Jc1{ch} = reshape(Jc1{ch},size(Jc1{ch},1),size(Jc1{ch},2)*size(Jc1{ch},3));
        Jc2{ch} = reshape(Jc2{ch},size(Jc2{ch},1),size(Jc2{ch},2)*size(Jc2{ch},3));
        
    end
    
    [dz,vdz,Adz] = two_group_test_coherence(Jc1{1},Jc1{2},Jc2{1},Jc2{2},Args.p);
    
    if nargout > 4 | Args.plot
        params.trialave = 1;
        params.err = [2 Args.p];
        
        [C1,phi1,S12,S1,S2,f,confC,phierr1,Cerr1]=coherencyc(squeeze(data1(:,:,1)),squeeze(data1(:,:,2)),params);
        [C2,phi2,S12,S1,S2,f,confC,phierr2,Cerr2]=coherencyc(squeeze(data2(:,:,1)),squeeze(data2(:,:,2)),params);
        varargout{1} = C1;
        varargout{2} = C2;
        varargout{3} = Cerr1;
        varargout{4} = Cerr2;
    end
    
else
    [rdiff,dz,vdz,f,C] = compareTwoGroupCoh(data1,data2,params,modvarargin{:});
    l = find(vdz == Args.p);
    if ~isempty(Args,smoothing)
       dz = filtfilt(repmat(1/Args.smoothing,1,Args.smoothing),1,dz);
       rdiff = filtfilt(repmat(1/Args.smoothing,1,Args.smoothing),1,rdiff);
    end
    Adz = rdiff > dz(:,l(1)) & rdiff < dz(:,l(2));
    varargout{1} = C(:,1);
    varargout{2} = C(:,2);
    varargout{3} = rdiff;
end


if Args.plot
    figure
    plot(f,C1,'b'); hold on; plot(f,Cerr1(1,:),'b--'); plot(f,Cerr1(2,:),'b--');
    
    sigInd = find(Adz == 0);
    
    plot(f,C2,'k'); hold on; plot(f,Cerr2(1,:),'k--'); plot(f,Cerr2(2,:),'k--');
    plot(f(sigInd),C1(sigInd),'*y'); plot(f(sigInd),C2(sigInd),'*y');
    
end
