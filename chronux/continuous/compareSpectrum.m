function [dz,vdz,Adz,f,varargout] = compareSpectrum(data1,data2,params,varargin)
% data1 and data2 in format sample x trials. the trials have to be of same
% length


Args = struct('p',0.0001,'plot',0);
Args.flags = {'plot'};
[Args,modvarargin] = getOptArgs(varargin,Args);

params.trialave = 0;

[J1,f] = premtfftc(data1,params);
[J2,f] = premtfftc(data2,params);

J1 = reshape(J1,size(J1,1),size(J1,2)*size(J1,3));
J2 = reshape(J2,size(J2,1),size(J2,2)*size(J2,3));

[dz,vdz,Adz]=two_group_test_spectrum(J1,J2,Args.p);

if nargout > 4 | Args.plot
    params.trialave = 1;
    params.err = [2 Args.p];

    [S1,f,Serr1]=mtspectrumc(data1,params);
    [S2,f,Serr2]=mtspectrumc(data2,params);
    varargout{1} = S1;
    varargout{2} = S2;
    varargout{3} = Serr1;
    varargout{4} = Serr2;
end

if Args.plot
    figure


    plot(f,S1,'b'); hold on; plot(f,Serr1(1,:),'b--'); plot(f,Serr1(2,:),'b--');

    plot(f,S2,'k'); hold on; plot(f,Serr2(1,:),'k--'); plot(f,Serr2(2,:),'k--');

    sigInd = find(Adz == 0);

    plot(f(sigInd),S1(sigInd),'*y'); plot(f(sigInd),S2(sigInd),'*y');
end
