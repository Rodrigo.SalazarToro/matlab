function [H,K]=multiple_comparison(p,q,method)
% Function to do multiple comparisons tests based on Family Wise
% Error rates using one of a set of multiple comparisons procedures
% 
% Usage: H=multiple_comparisons(p,q,method)
%       or [H,K]=multiple_comparisons(p,q,method)
% Inputs: 
% p : a collection of p values - required
% q : threshold for multiple comparisons (i.e. a p value for the tests)
% method: 'bonxxxx' - Bonferroni correction
%         'holxxxx' - Holm step-up procedure
%         'hocxxxx' - Hochberg step-down procedure
%         'fdrxxxx' - false discovery rate
% Output: 
% H : collection of 1s and 0s
%     1s - do not reject the null hypothesis
%     0s - reject the null hypothesis
%     (dimension of H is the same as the dimension of p)
% K : the FDR based method can be used when assuming independence or
% positive dependece of the tests as well as when no such assumption is
% invoked. K contains the results without any dependency assumption and H
% contains the results with an independent or positive dependence
% assumption. K is also a collection of 1s and 0s (as with H). When the
% other methods are used K is simply a collection of 1s.

k=length(p);
H=ones(1,k);
K=ones(1,k);
method=method(1:3);
if strcmp(method,'bon')
    q=q/k;
    indx=find(p<q);
    H(indx)=0;
elseif strcmp(method,'hol');
    [psort,indxsort]=sort(p);
    q=q./(k-[1:k]+1);
    n=0;
    doloop=1;
    while  doloop==1 && n < k;
        n=n+1;
        if psort(n) > q(n)
           doloop=0;
        end;
    end;
    H(indxsort(1:n-1))=0;
elseif strcmp(method,'hoc');
    [psort,indxsort]=sort(p);
    q=q./(k-[1:k]+1);
    n=k+1;
    doloop=1;
    while doloop==1 && n > 1
        n=n-1;
        if psort(n) <= q(n)
           doloop=0;
        end;
    end;
    if doloop==0; H(indxsort(1:n))=0;end
elseif strcmp(method,'fdr');
    [psort,indxsort]=sort(p);
    q=[1:k].*q/k;
    n=k+1;
    doloop=1;
    while doloop==1 && n>1;
        n=n-1;
        if psort(n)<=q(n);
           doloop=0;
        end;
    end;
    if doloop==0; H(indxsort(1:n))=0; end;
%     [pID,pN] = FDR(p,q);
%     if ~isempty(pID); 
%        indx1=find(p<pID);
%        H(indx1)=0;
%     end;
%     if ~isempty(pN);
%        indx2=find(p<pN);
%        K(indx2)=0;
%     end;
end;
    
    
    
    