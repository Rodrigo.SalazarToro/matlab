function lstatperm=two_group_permutation_test(x,y1,y2,Nperm,family,nn,h)
% Function to do a two group permutation test to determine the distribution
% of the statistic lstat based on locfit smoothing
% Usage: lstatperm=two_group_permutation_test(data1,data2,lstat,Nperm)
% Inputs:
% x: independent variable (at which the fits to y1 and y2 are computed
% using locfit) as a column vector
% y1, y2: data sets - BOTH struct arrays with length equal to number of trials
%                     or BOTH matrices where data corresponding to each trial is
%                     stored in a column - required
% lstat: the statistic to be computed - required
% Takes values 'd' for lstat=smooth(y1)-smooth(y2)
%                 'ad' for lstat=|smooth(y1)-smooth(y2)|
%                 'mad' for lstat=max(|smooth(y1)-smooth(y2)|)
%                 'com' for lstat=differences in the centre of mass of y1
%                       and y2
%      smooth denotes a locfit operation - currently supports computing
%      rates, densities, and doing a likelihood estimate
% Nperm: number of permutations - required
% family: for locfit - can take values that locfit.m allows e.g. rate, dens, 
%                      poisson, binomial etc - required
%         (currently density type estimates are only supported when data is
%         a struct array and regression type estimates are supported when
%         data is a matrix)
% nn : nearest neighbor bandwidth - required
% h : fixed bandwidth - required
%
% Outputs:
% lstatperm:cell array contains the statistics computed for each permutation
%           (exchange of samples between the two groups) for the following 4 statistics 
%                            lstat=smooth(y1)-smooth(y2) 
%                            lstat=|smooth(y1)-smooth(y2)|
%                            lstat=max(smooth(y1)-smooth(y2))
%                            lstat=differences in the centre of mass of y1
%                                  and y2
x=x(:);
if isstruct(y1) && isstruct(y2); 
   Ntr1=length(y1);
   Ntr2=length(y2);
   N=Ntr1+Ntr2;
   y=[y1 y2];
   lstatperm=cell(1,4);
   for n=1:Nperm;
       fprintf('%d\n',n);
       vv=randperm(N);
       v=vv(1:Ntr1);
       w=vv(Ntr1+1:end);
       dtmp1=y(v);
       dtmp2=y(w);
       %
       % Convert the struct arrays into 1d arrays 
       %
       data1=[];
       for n=1:Ntr1;
           data1=[data1;dtmp1(n).times];
       end;

       data2=[];
       for n=1:Ntr2;
           data2=[data2;dtmp2(n).times];
       end;
       %
       % Compute the fits
       %
       fit1=locfit(data1,'nn',nn,'h',h,'family',family);
       fali1=fit1{4}{5};
       fit1=predict(fit1,x);
       fit1=invlink(fit1,fali1);

       fit2=locfit(data2,'nn',nn,'h',h,'family',family);
       fali2=fit2{4}{5};
       fit2=predict(fit2,x);
       fit2=invlink(fit2,fali2);
              
       if strcmp(family,'rate'); fit1=fit1/Ntr1; fit2=fit2/Ntr2; end;
       %
       % Compute the statistics
       %
       lstatperm{1}=[lstatperm{1} fit1-fit2];
       lstatperm{2}=[lstatperm{2} abs(fit1-fit2)];
       lstatperm{3}=[lstatperm{3} max(abs(fit1-fit2))];
%        com1=trapz(x,x.*fit1)/trapz(x,fit1);
%        com2=trapz(x,x.*fit2)/trapz(x,fit2);
       com1=trapz(x,x.*x.*fit1);
       com2=trapz(x,x.*x.*fit2);
       lstatperm{4}=[lstatperm{4} com1-com2];
   end;
elseif ~isstruct(y1) && ~isstruct(y2);
    Ntr1=size(y1,2);
    Ntr2=size(y2,2);
    N=Ntr1+Ntr2;
    y=[y1 y2];
    x1=repmat(x,[1 Ntr1]);
    x2=repmat(x,[1 Ntr2]);
    x1=x1';
    x2=x2';
    x1=reshape(x1,[prod(size(x1)) 1]);
    x2=reshape(x2,[prod(size(x2)) 1]);
    lstatperm=cell(1,4);
    for n=1:Nperm;
        fprintf('%d\n',n);
        vv=randperm(N);
        v=vv(1:Ntr1);
        w=vv(Ntr1+1:end);
        data1=y(:,v)';
        data2=y(:,w)';
        data1=reshape(data1,[prod(size(data1)),1]);
        data2=reshape(data2,[prod(size(data2)),1]);
        %
        % Compute the fits
        %
        fit1=locfit(x1,data1,'nn',nn,'h',h,'family',family);
        fali1=fit1{4}{5};
        fit1=predict(fit1,x);
        fit1=invlink(fit1,fali1);

        fit2=locfit(x2,data2,'nn',nn,'h',h,'family',family);
        fali2=fit2{4}{5};
        fit2=predict(fit2,x);
        fit2=invlink(fit2,fali2);
        %
        % Compute the statistics
        %
       lstatperm{1}=[lstatperm{1} fit1-fit2];
       lstatperm{2}=[lstatperm{2} abs(fit1-fit2)];
       lstatperm{3}=[lstatperm{3} max(abs(fit1-fit2))];
%        com1=trapz(x,x.*fit1)/trapz(x,fit1);
%        com2=trapz(x,x.*fit2)/trapz(x,fit2);
       com1=trapz(x,x.*x.*fit1);
       com2=trapz(x,x.*x.*fit2);
       lstatperm{4}=[lstatperm{4} com1-com2];
    end;
else;
    error('BOTH data sets should be struct arrays or matrices');
end;

    
