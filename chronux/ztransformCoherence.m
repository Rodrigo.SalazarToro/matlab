function [Cz, varargout] = ztransformCoherence(C,ntrials,ntapers)


B = 1.5;
v = ntrials * ntapers;
%Jarvis & Mitra 2001

q = sqrt(-(v-2)*log(1-abs(C).^2)); 

Cz = B*(q-B);