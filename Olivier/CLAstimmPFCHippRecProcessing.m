clear all
sdir = 'F:\salazar\data\Electrophy\hippPFCCLA';
cd(sdir)
days = {'20170510' '20170512' '20170519'};


cd(sdir)


blabels = { 'pulsePFC' 'pulseHipp'}; % for events 1 and 8 on the 6/13/2017

for d = 1 : length(days)
    cd(days{d});
    sessions = nptDir('session*');
    for ss = 1 : length(sessions);
        cd(sessions(ss).name);
        % at the session level
        [Ev,iD]=OG_extract_events; % Ev will be in milliseconds
        labels = cell(length(iD),1);
        uiD = unique(iD);
        for ii = 1 : length(iD); labels{ii}=blabels{iD(ii) == uiD};end
        SaveEvt(sprintf('%s0%d.LIG.evt',days{d},ss),Ev,labels,32000,0);
        [firstpulse,seqFreq] = getPulseSeq(Ev,iD,'multiplePulses','save');
        makepwgramPolytrode('name',sprintf('%s%d',days{d},ss));
        makepwgramPolytrode('name',sprintf('%s%d',days{d},ss),'coherency');
        
        cd ..
    end
    cd ..
end
cd(sdir)
obj = ProcessDays(micepsthChR2,'days',days,'NoSites');
