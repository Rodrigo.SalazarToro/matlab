function [firstpulse,seqFreq,varargout] = getPulseSeq(varargin)
% the jitter args needs to be tested
% in the session folder
% output firstpulse is in milliseconds

Args = struct('redo',0,'save',0,'addName',[],'minITI', 5000,'maxITI',30000,'maxIPT',200,'jitter',1,'expectedFreq',[10 30 50],'multiplePulses',0);
Args.flags = {'save','redo','multiplePulses'};
[Args,modvarargin] = getOptArgs(varargin,Args);

filename = sprintf('PulseFreq%s.mat',Args.addName);

if isempty(nptDir(filename)) || Args.redo
    if Args.multiplePulses
        allEv = varargin{1};
        alliD = varargin{2};
        uiD = unique(alliD);
        for nid = 1 : length(uiD)
            Ev = allEv(alliD == alliD(nid));
            [n,xout]=hist(diff(Ev),[1:Args.jitter:Args.maxITI]);
            itis = find(n > 0 & (xout > Args.minITI & xout < Args.maxITI));
            ntrial = sum(n(itis)) + 1;
            begi = find(diff(Ev) >= min(itis));
            firstpulse(nid,:) = [Ev(1) Ev(begi+1)];
            secpulse = [Ev(2) Ev(begi+2)];
            if ntrial > length(firstpulse(nid,:)) + 1
                error([pwd ' MISMATCH # TRIALS AND FIRST PULSES; CHECK DEFAULTS PARAMETERS'])
            end
            ipts =  find(n > 0 & xout < Args.maxIPT);
           seqFreq(nid,:) = round(1000./(secpulse - firstpulse(nid,:)));
            if length(ipts) ~= length(Args.expectedFreq) || length(ipts) ~= length(Args.expectedFreq)
                error([pwd ' MISMATCH # FREQ MEASURES AND EXPECTED; CHECK DEFAULTS PARAMETERS'])
            end
            if sum(round(1000./sort(ipts)) == sort(Args.expectedFreq)) == length(Args.expectedFreq)
                warning([pwd ' MISMATCH FREQs MEASURED AND EXPECTED; CHECK DEFAULTS PARAMETERS'])
            end
        end
        varargout{1} = uiD;
        
    else
        Ev = varargin{1};
        [n,xout]=hist(diff(Ev),[1:Args.jitter:Args.maxITI]);
        
        
        %% # trials
        
        
        itis = find(n > 0 & (xout > Args.minITI & xout < Args.maxITI));
        
        ntrial = sum(n(itis)) + 1;
        
        begi = find(diff(Ev) >= min(itis));
        firstpulse = [Ev(1) Ev(begi+1)];
        secpulse = [Ev(2) Ev(begi+2)];
        if ntrial > length(firstpulse) + 1
            
            error([pwd ' MISMATCH # TRIALS AND FIRST PULSES; CHECK DEFAULTS PARAMETERS'])
            
        end
        
        ipts =  find(n > 0 & xout < Args.maxIPT);
        
        
        seqFreq = round(1000./(secpulse - firstpulse));
        if length(ipts) ~= length(Args.expectedFreq) || length(ipts) ~= length(Args.expectedFreq)
            
            error([pwd ' MISMATCH # FREQ MEASURES AND EXPECTED; CHECK DEFAULTS PARAMETERS'])
        end
        
        if sum(round(1000./sort(ipts)) == sort(Args.expectedFreq)) == length(Args.expectedFreq)
            warning([pwd ' MISMATCH FREQs MEASURED AND EXPECTED; CHECK DEFAULTS PARAMETERS'])
            
        end
    end
    if Args.save
        save(filename,'firstpulse','seqFreq')
        display(['saving ' pwd filesep filename])
        
    end
    
else
    load(filename)
end
