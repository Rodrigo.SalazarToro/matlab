% function [EvtRes, EvtClu, EvtLabels, Labels] = LoadEvt(FileName,SampleRate,Labels)
%
% Loads a Neuroscope event file.  Labels is optional, and will seek only
% events with that label.

function [EvtRes, EvtClu, EvtLabels, Labels] = LoadEvt(FileName,SampleRate,Labels,ExportSamp)

if nargin<3
    Labels=[];
    ExportSamp=1;
end

[EvtRes EvtLabels] = textread(FileName,'%f %s');

uLabels = unique(EvtLabels);
if isempty(Labels)
	Labels = uLabels;
elseif ~isempty(setdiff(Labels,uLabels))
	Lablels = intersect(Labels, uLabels);
end

myInd = find(ismember(EvtLabels,Labels));
EvtLabels = EvtLabels(myInd);
if (ExportSamp)
    %EvtRes = round(EvtRes(myInd)*SampleRate/1000)+1;	
    evtRes=EvtRes/32;
end

for i=1:length(Labels)
	cluInd = find(strcmp(EvtLabels,Labels{i}));
	EvtClu(cluInd) = i;	
end