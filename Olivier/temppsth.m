

load ispikes
timeserie = double(sp.data.trial.cluster.spikes);

bins = [-Args.binsize:Args.binSize:triallength+Args.binSize];

freqs = unique(seqFreq);

for fq = 1 : length(freqs)
    strial = find(seqFreq == freqs(fq));
    raster = zeros(length(strial),length(bins)-2);
    for t = 1 : length(strial)
        ntimes = timeserie - firstpulse(strial(t)) + Args.timebefore;
        [n,xout ] = hist(ntimes,bins);
        raster(t,:) =  n(2:end-1);
    end
    subplot(1,length(freqs),fq)
    imagesc(xout(2:end-1),[1:length(strial)],-raster)
    colormap('gray')
    hold on
    
    line([Args.timebefore Args.timebefore],[0 length(strial)],'Color','r')
    title(['Frequency ' num2str(freqs(fq)) 'Hz'])
    xlabel('Time (ms)')
    ylabel('Trial #')
end