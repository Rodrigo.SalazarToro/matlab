% v3 now read .lfp files (just different suffix than .eeg)
function [final_insf,attention_f]=MP_finsp3(d1thresh,auto,chselect,numchannel, min_dur, insp_offset)
% d1thresh now -4 because we are smoothing
% auto flag is for whether to show you the identification
% chselect is the channel with respiration info
% numchannel is total number of channels
% min_dur is the minimum duration of the inspiration; for fast breathing
% mice, want to set min_dur ~20; for slow_breathing ~40; if inspiration
% gives larger voltage deflections than respiration, you can also set
% min_dur to be smaller

if nargin < 5
    min_dur = 25;
end
if nargin < 6
    min_dur = 1000;
end

srate=2713; % what's this?

[filename, pathname] = uigetfile('*.eeg');
cd(pathname);
fbase = filename(1:(length(filename)-4)); % name of the experiment

% Read the trace
fprintf(1,'Reading trace %s\n',filename);
hreeg = readmulti(filename,numchannel,chselect);
% hreeg = hreeg;

scrsz = get(0,'ScreenSize');
browse_x=1;
attention_f=[];

win=5000;
tot=length(hreeg);
seg_size = 1e5;
num_segments=ceil(tot/seg_size); % break data into segments
first_seg = 1;
for j=first_seg:num_segments 
%     fprintf(1,'Analyzing segment %d of %d\n',j,num_segments);
    close all;
    tic
    seg_start=(j-1)*seg_size +1; % start of segment
    seg_end = min(j*seg_size+5000,tot);
    hr=hreeg(seg_start:seg_end); % pick out one segment
    hrb=smooth(hr,55,'moving');
    hrb_base=smooth(hr,2001,'moving'); % get baseline '0' value    
    
%     fprintf(1,'Differentiating and smoothing\n');

    hr1=diff(hrb);
    hr1s=smooth(hr1,5,'moving');

%     fprintf(1,'Picking strong inspirations\n');
    insp_starts=hr1s<d1thresh; % indices of strong inspiration

%     %get rid of short inspirations (less than 15 pts)
%     insp_starts2=smooth(insp_starts,30,'moving');
%     insp_starts2 = insp_starts2>0.97;

    insp_times = hrb< (hrb_base-insp_offset); % periods with inspiration (not start/stop)
    insp_starts3 = insp_starts .* insp_times(1:end-1); % because starts is a diff
    insp_starts4 = diff(insp_starts3); % only want during inspiration, not during exhale
    
    attention=find(hr1s>2500); % artifacts

    begins=find(insp_starts4>0); % get indices of beginnings and ends of breath
    ends=find(insp_starts4<0); 
    
    if isempty(begins) continue; end % if by chance do not have breath (end of exp), get out
    while(begins(1)>ends(1)) % first end must be after first begin
        ends(1)=[];
    end
    
    while(begins(1)<2001) % skip early points due to hrb_base being bad due to end effects
        begins(1)= [];
        ends(1) = [];
        if isempty(begins) break; end
    end
    
    if isempty(begins) continue; end % if by chance do not have breath (end of exp), get out
    
    while(begins(end)>(seg_size+2000)) % skip late points due to hrb_base being bad due to end effects
        begins = begins(1:end-1);
        ends = ends(1:end-1);
    end
    
    % get rid of artifacts, and connect breaths during fluctuating
    % inhalation; min_dur*2.5 = interval between "breaths" that will be
    % linked
    [begins ends] = filter_inspiration(begins, ends, min_dur*2.5, min_dur); % connect locally
    [begins ends] = filter_inspiration(begins, ends, min_dur*5, min_dur+25); % run second time with longer filter to connect more stuff
    
    num_breaths=length(begins);
    insf=zeros(num_breaths,4);
    for i=1:num_breaths
        %fprintf(1,'Begin: %d, end: %d\n',begins(i),ends(i));


        insf(i,1)=begins(i);        %beginnig time
        insf(i,2)=hrb(begins(i));    %beginning value

        %end value,end value
        insf(i,3)=ends(i);
        insf(i,4)=hrb(ends(i));    %beginning value
    end

    toc

    c=0;

    if(~auto)
        attention
        h2=figure(100);
        set(h2,'Position',[1 scrsz(4)/2-100 scrsz(3)*5/6 scrsz(4)/2]);
        plot(hr);hold on;
        set(gca,'Position',[0.02 0.03 0.97 0.92])
        plot(insf(:,1),insf(:,2),'r*','LineWidth',2);
        plot(insf(:,3),insf(:,4),'go','LineWidth',2);
        plot(-10000*(hrb<hrb_base),'c');
        

        switch(j)
            case 1
                plot([49 50],[min(hr) max(hr)],'y','LineWidth',3);
                plot([1e5+2500 1e5+2501],[min(hr) max(hr)],'y','LineWidth',3);
            case num_segments
                plot([2499 2500],[min(hr) max(hr)],'y','LineWidth',3);
            otherwise
                plot([2499 2500],[min(hr) max(hr)],'y','LineWidth',3);
                plot([1e5+2500 1e5+2501],[min(hr) max(hr)],'y','LineWidth',3);
        end
        %plot(hr2*100,'k');
        h3=figure(101);
        set(h3,'Position',[10 10 scrsz(3)/2 scrsz(4)/3]);
        plot(hr);hold on;
        plot(insf(:,1),insf(:,2),'r*','LineWidth',2);
        plot(insf(:,3),insf(:,4),'go','LineWidth',2);
        xlim([1 5000]);
        ylim('auto');

        while(isempty(c) || c~=9 )
            fprintf(1,'\n');
            str='Please make a selection\n1-Continue to next segment\n2-Pick inpiration\n';
            str=[str '3-Erase inpiration\n4-Browse\n5-Change browse window width'];
            str=[str '\n6-Page Browsing\n9-Cancel\n'];
            c=input(str,'s');
            switch(c)
                case '1'
                    if(j<num_segments)
                        fprintf(1,'Proceeding to segment %d\n',j+1);
                    else
                        fprintf(1,'Process Complete\n',j+1);
                    end
                    break
                case '2'
                    disp('Picking inspiration');
                    insf=pick_Inspiration(h2,h3,insf);
                case '3'
                    disp('Erasing inspiration');
                    insf=erase_Inspiration(h3,insf);
                case '4'
                    disp('Browsing');
                    browse_x=browse_Insp(h2,h3,insf,hr,win);
                case '5'
                    win=input('Enter window size (default = 5000 points): ');
                    figure(h3);
                    xlim([max(1,browse_x-win/2) browse_x+win/2]);
                    ylim('auto');
                    disp('Browsing');
                    browse_x=browse_Insp(h2,h3,insf,hr,win);
                case '6'
                    disp('Page Browsing');
                    browse_x=browse_Page(browse_x,h3,insf,hr,win);

                case '9'
                    disp('Aborting operation');
                    return
                otherwise
                    disp('Ivalid option');
            end
        end
    end
    attention_f=[attention_f;attention+1e5*(j-1)-1];

    % make up for segmentation in begin times
    if(j==first_seg)
        final_insf=insf;
    else
        insf(:,1)=insf(:,1)+(j-1)*seg_size-1;
        insf(:,3)=insf(:,3)+(j-1)*seg_size-1;
        final_insf=[final_insf;insf];
    end

    disp('________________________');

end

labels = cellstr(repmat('breathin',size(final_insf,1),1));
SaveEvt([fbase '.ins.evt'],final_insf(:,1),labels,srate,1);


end


function insf=pick_Inspiration(h2,h3,insf)

c='n';
figure(h3);
disp('Please click at inspiration onset')
[x1,y1]=ginput(1);
if(isempty(x1))
    return
end
figure(h3);hold on;plot(x1,y1,'m+');
disp('Please click at end of inspiration')
[x2,y2]=ginput(1);
if(isempty(x2))
    return
end
figure(h3);hold on;plot(x2,y2,'g+');
c=input('Is this information correct(y/n)\n','s');
if(c~='y' )
    disp('Points ignored');
    figure(h3);hold on;
    plot(x2,y2,'+','LineWidth',3);plot(x1,y1,'+','LineWidth',3);
    return
else
    figure(h2);hold on;
    plot(x2,y2,'go','LineWidth',2);
    plot(x1,y1,'r*','LineWidth',2);
end


insf(1,1)=x1;
insf(1,2)=y1;
insf(1,3)=x2;
insf(1,4)=y2;

[insf(:,1),ix]=sort(insf(:,1));
insf(:,2)=insf(ix,2);
insf(:,3)=insf(ix,3);
insf(:,4)=insf(ix,4);
end


function insf=erase_Inspiration(h3,insf)

c='n';

disp('Please click at inspiration onset')
figure(h3);
[x1,y1]=ginput(1);
if(isempty(x1))
    return
end
figure(h3);hold on;plot(x1,y1,'k+');

ind=1
for(i=1:length(insf))
    if( abs(insf(ind,1)-x1) > abs(insf(i,1)-x1))
        ind=i;
    end
end

figure(h3);hold on;
plot(insf(ind,1),insf(ind,2),'*','LineWidth',3);
plot(insf(ind,3),insf(ind,4),'o','LineWidth',3);

c=input('Is this information correct(y/n)\n','s');
if(c=='y' )
    insf(ind,:)=[];
else
    disp('Ignoring erasure')
end
end

function x=browse_Insp(h2,h3,insf,hr,width)
%to quit browsing just use right-click

button=1;

while(1)
    figure(h2);
    [x,y,button]=ginput(1);

    if(button~=1 || isempty(x))
        break;
    end
    figure(h3);
    %cla;
    if(x>(width/2))
        xlim([max(1,x-width/2) x+width/2]);
    else
        xlim([1 width]);
    end
    ylim('auto');

end

end


function x=browse_Page(x,h3,insf,hr,width)
%to quit browsing just use keyboard/ENTER


while(1)
    [d,b,button]=ginput(1);
    if(isempty(button))
        disp('No button informaton present');
        return;
    end

    switch(button)
        case 3
            x=max(x-width,1);
        case 1
            x=min(x+width,1e5+5000);
        otherwise
            disp('Leaving browse mode')
            return
    end
    if(x>(width/2))
        xlim([max(1,x-width/2) x+width/2]);
    else
        xlim([1 width]);
    end


    ylim('auto');

end

end

function [begins_out ends_out] = filter_inspiration(begins_in, ends_in, interval, duration)

    % get rid of begins near each other
    i=1;
    while i<length(begins_in)
        if begins_in(i+1)-ends_in(i)<interval % another pulse soon after
            begins_in = [begins_in(1:i); begins_in(i+2:end)]; % combine
            ends_in = [ends_in(1:i-1); ends_in(i+1:end)];
        elseif ends_in(i) - begins_in(i)<duration % short pulse; probably artifact
            begins_in = [begins_in(1:i-1); begins_in(i+1:end)]; 
            ends_in = [ends_in(1:i-1); ends_in(i+1:end)];
        else
            i=i+1; % no errors, move on
        end
    end %for
begins_out = begins_in;
ends_out = ends_in;
end