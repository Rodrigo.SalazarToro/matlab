function OG_makePSTH_lightStim(nbPulses,nbPerFreq,freq,plot)
close all
% INPUTS
% nbPulses
load('ispikes.mat');
cd ..
cd ..
% load('Events1.mat');
% load('Res1.mat');
load('Events.mat');


tBef=3000;
tAft=5000;
bin=100;

Ev=Ev(1:nbPulses:end-600);
%Ev=Ev([1 31 61]);
spk=double(sp.data.trial.cluster.spikes);
%spk=Res1;

trials=cumsum(nbPerFreq);
%trials=60;

H(length(Ev),:)=zeros(1,length(-tBef:bin:tAft));
 


for i=1:length(Ev)
    Spk=spk(find(spk > Ev(i)-tBef & spk < Ev(i)+tAft));
    if ~isempty(Spk)
        spike{i}=Spk-Ev(i);
        if plot
        subplot(2,1,1)
        line([spike{i}';spike{i}'],[ones(1,length(spike{i}));ones(1,length(spike{i}))+1],'Color','k','LineWidth',2); 
        xlim([-tBef tAft]);
        subplot(2,1,2)
        hist(Spk-Ev(i),[-tBef:bin:tAft]);
        
        %fprintf(['Time of stim ',num2str(N),' . ',num2str(R)])
        pause
        close all
        end
        H(i,:)=hist(Spk-Ev(i),[-tBef:bin:tAft]);
    end
    
end

%% Plot PSTH
x=0; y=0;
for j=trials
        y=y+1;
        mH=mean(H(x+1:j,:),1);
        semH=std(H(x+1:j,:))/sqrt(j);
        subplot(length(nbPerFreq),1,y);
        boundedline([-tBef:bin:tAft],mH,semH);
        xlabel('Time (ms)'); ylabel('Spike counts');
        title(['Frequ: ',num2str(freq(y)),' Hz']);
        x=j;
end

%% Plot raster
figure
x=0;
for k=1:length(Ev)
    line([spike{k}';spike{k}'],[ones(1,length(spike{k}))+x;ones(1,length(spike{k}))+x+.2],'Color','k','LineWidth',2);
    x=x+1;
end
xlabel('Time (ms)');
    
       