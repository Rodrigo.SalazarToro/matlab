function obj = micepsthChR2(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'micepsthChR2';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mpsthChR2';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) && isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [~,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                && (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

end
function obj = createObject(Args,varargin)
sdir = pwd;
% drug = ~isempty(nptDir('drug.txt'));
% cd ..

if isempty(nptDir('aneasthesia.txt'))
    mouse = 'unknown';
else
    readt = textread('aneasthesia.txt','%s');
    mouse = readt{1};
end
cd(sdir)
load PulseFreq
trialInfo{1} = firstpulse;trialInfo{2} = seqFreq;
% if ~isempty(nptDir('histo.mat'))
%     load histo.mat
%     
%     
% end
if ~isempty(nptDir('group0*'))
    
    groups = nptDir('group0*');
    if  ~isempty(groups)
        count = 1;
        data.Index = [];
        sortdir = pwd;
        
        for g = 1 : size(groups,1)
%             thegroup = str2double(groups(g).name(6:end));
%             peakfile = nptDir(sprintf('*lfp_group0%dLFPpeaksNRG1.mat',thegroup));
%             if ~isempty(peakfile)
%             peak = load(peakfile.name);
%             else
%                 
%                 peak.peaks{1} = nan;
%             end
            cd(groups(g).name)
            gdir = pwd;
            
            clusters = nptDir('cluster*');
            for c = 1 : size(clusters,1)
                cd(clusters(c).name)
        
                load ispikes.mat
                [spikes,raster,ntrials, autocor,pOnset] = spikesdata(sp,'trialInfo',trialInfo,'save');
                data.Index(count,1) = count;
                
                if clusters(c).name(end) == 'm';  data.Index(count,2) = 2; else  data.Index(count,2) = 1; end
                % 1 for SUA 2 for MUA
%                 data.Index(count,3) = ChR2;
                data.Index(count,3) = 1;
                data.onset(count,:) = pOnset';
                data.Index(count,4) = (length(spikes{1}) + length(spikes{2}) + length(spikes{3})) / (sum(ntrials) * 6);
                data.Index(count,5) = str2double(groups(g).name(end-2:end));
                
                
%                 if isempty(nptDir('stationary.mat'));
%                     stationarity('save');
%                 end
%                 load stationary.mat
%                 data.Index(count,6) = mintime;
%                 data.Index(count,7) = maxtime;
                
%                 if ~exist('histo','var')
%                     data.histo{count} = 'unknown';
%                 else
%                     data.histo{count} = histo{g};
%                 end
                data.aneasthesia{count} = mouse;
              
               
                data.setNames{count} = pwd;
                count = count + 1;
                cd(gdir)
            end%c = 1 : size(clusters,1)
            cd(sortdir)
        end%g = 1 : size(group,1)
        data.numSets = count-1; % nbr of trial
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('empty object \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('empty object \n');
    obj = createEmptyObject(Args);
end
cd(sdir)
end
function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.aneasthesia = [];

data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
end