function obj = plot(obj,varargin)


Args = struct('psthbin',100,'rasterbin',5,'plotType','standard','triallength',6000,'timebefore',2000,'smoothPSTH',0);
Args.flags = {'smoothPSTH'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
% display(obj.data.Index(ind,28:37))

%% plotting fct
load ispikes
timeserie = double(sp.data.trial.cluster.spikes);
load spikesdata
set(gcf,'Name',obj.data.setNames{ind})
switch Args.plotType
    
    case {'standard' }
        bins = [-Args.psthbin : Args.psthbin : Args.triallength + Args.psthbin];
        bins2 = [-Args.rasterbin : Args.rasterbin : Args.triallength + Args.rasterbin];
        [~,cdir] = getDataDirs('session','relative','CDNow');
        [firstpulse,seqFreq] = getPulseSeq;
        cd(obj.data.setNames{ind})
        freqs = unique(seqFreq);
        maxy = 1;
        for fq = 1 : length(freqs)
            strial = find(seqFreq == freqs(fq));
            raster = zeros(length(strial),length(bins)-2);
            rster2 = zeros(length(strial),length(bins)-2);
            for t = 1 : length(strial)
                ntimes = timeserie - firstpulse(strial(t)) + Args.timebefore;
                [n,xout ] = hist(ntimes,bins);
                raster(t,:) =  n(2:end-1);
                [n,xout2] = hist(ntimes,bins2);
                raster2(t,:) =  n(2:end-1);
                
            end
            subplot(2,length(freqs),fq)
            psth = (1000/ Args.psthbin) * (sum(raster)/size(raster,1));
            maxy = max([maxy max(psth)]);
            if Args.smoothPSTH; psth = smooth(psth,3); end
            plot(xout(3:end),psth)
            ylabel('Firing rate (sp/s)')
            xlim([0 xout(end)])
            subplot(2,length(freqs),3+fq)
            imagesc(xout2(2:end-1),[1:length(strial)],-raster2)
            colormap('gray')
            hold on
            
            line([Args.timebefore Args.timebefore],[0 length(strial)],'Color','r')
            title(['Frequency ' num2str(freqs(fq)) 'Hz' ' WSR test pvalue ' sprintf('%f',obj.data.onset(ind,fq))])
            xlabel('Time (ms)')
            ylabel('Trial #')
        end
        %         display(pvalue)
        for sb = 1 : length(freqs); subplot(2,length(freqs),sb);ylim([0 maxy]); end
    case {'rasterForAlan'}
        [~,cdir] = getDataDirs('session','relative','CDNow');
        [firstpulse,seqFreq] = getPulseSeq;
        freqs = unique(seqFreq);
        bins = [-Args.psthbin : Args.psthbin : Args.triallength + Args.psthbin];
         maxy = 1;
        for fq = 1 : length(freqs)
            strial = find(seqFreq == freqs(fq));
            raster = zeros(length(strial),length(bins)-2);
            for t = 1 : length(strial)
                ntimes = timeserie - firstpulse(strial(t)) + Args.timebefore;
                iind = find(ntimes >= 0 & ntimes <= Args.triallength);
                [n,xout ] = hist(ntimes,bins);
                raster(t,:) =  n(2:end-1);
                if ~isempty(iind)
                    sspike = ntimes(iind);
                    subplot(2,length(freqs),fq + length(freqs))
                    line([sspike'; sspike'],[(ones(1,length(sspike)) + t); (ones(1,length(sspike)) + t + 1)],'Color','k','LineWidth',2);
                    hold on
                end
            end
             ylim([0 length(strial)])
            subplot(2,length(freqs),fq)
            psth = (1000/ Args.psthbin) * (sum(raster)/size(raster,1));
            maxy = max([maxy max(psth)]);
            if Args.smoothPSTH; psth = smooth(psth,3); end
            plot(xout(3:end),psth)
            ylabel('Firing rate (sp/s)')
            xlim([0 xout(end)])
           
        end
         for sb = 1 : length(freqs); subplot(2,length(freqs),sb);ylim([0 maxy]); end
end
%%
