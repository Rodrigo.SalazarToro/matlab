function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'unit',[],'ChR2',[],'aneasthesia',[],'mintime',[],'maxtime',[],'minNumSpike',[],'maxNumSpike',[],'onset',[],'list',[]);
Args.flags = {'Number','ObjectLevel','bothRules'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.unit)
        ttemp = find(obj.data.Index(:,2) == strmatch(Args.unit,{'s' 'm'}));
    else
        ttemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.ChR2)
        runits = [];
        for el = 1 : length(Args.ChR2)
           runits =  [runits; find(obj.data.Index(:,3) == Args.ChR2(el))];
        end
        ttemp = intersect(runits,ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
    if ~isempty(Args.aneasthesia)
       mice = find(strcmp(obj.data.aneasthesia,Args.aneasthesia));
         ttemp = intersect(mice,ttemp);
        
    end
%     if ~isempty(Args.drug)
%     ttemp = intersect(ttemp, find(obj.data.Index(:,5) == Args.drug));
%     end
    
    if ~isempty(Args.minNumSpike)
       
            ttemp = intersect(ttemp, find(obj.data.Index(:,4) >= Args.minNumSpike));
        
    end
    
    if ~isempty(Args.maxNumSpike)
      
            ttemp = intersect(ttemp, find(obj.data.Index(:,4) <= Args.maxNumSpike));
      
    end
    
%     if ~isempty(Args.histo)
%        histot = find(strcmp(obj.data.histo,Args.histo));
%          ttemp = intersect(histot,ttemp);
%     end
    
    if ~isempty(Args.mintime)
        mtime = find(obj.data.Index(:,6) <= Args.mintime);
        ttemp = intersect(mtime,ttemp);
    end
    
    if ~isempty(Args.maxtime)
        mtime = find(obj.data.Index(:,7) >= Args.maxtime);
        ttemp = intersect(mtime,ttemp);
    end
    
     if ~isempty(Args.onset)
       onset = find(obj.data.onset(:,Args.onset(1)) < Args.onset(2));
        ttemp = intersect(onset,ttemp);
     end
    
     if ~isempty(Args.list)
         sel = zeros(1,length(ttemp));
         for iii = 1 : length(Args.list)
         thedirs = strfind(obj.data.setNames(ttemp),Args.list{iii});
         sel = sel + ~cellfun(@isempty,thedirs);
         end
         ttemp = ttemp(sel > 0);
     end
     
%     if ~isempty(Args.thetapeak)
%         thetat = [];
%        for pp = 1 : length(Args.thetapeak)
%          thetat = union(thetat, find(obj.data.thetapeak(:,Args.thetapeak(pp))));
%         
%        end 
%         ttemp = intersect(thetat,ttemp);
%     end
    
    varargout{1} = ttemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

