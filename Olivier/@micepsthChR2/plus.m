function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
    % check if second input is the right kind of object
    if(~isa(q,classname))
        % both inputs are not the right kind of object so create empty
        % object and return it
        r = feval(classname);
    else
        % second input is the right kind of object so return that
        r = q;
    end
else
    if(~isa(q,classname))
        % p is the right kind of object but q is not so just return p
        r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
    else
        % both p and q are the right kind of objects so add them
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        %         r.data.spiketimes = [p.data.spiketimes; q.data.spiketimes];
        % 		r.data.spiketrials = [p.data.spiketrials; q.data.spiketrials];
        
        
        
        q.data.Index(:,1) = q.data.Index(:,1) + p.data.Index(end,1);
        
        q.data.Index(:,3) = q.data.Index(:,3) + p.data.Index(end,3);
        
        r.data.Index = [p.data.Index; q.data.Index];
        
       
        %         r.data.Type = [p.data.Type; q.data.Type];
        
        
        % useful fields for most objects
        r.data.numSets = p.data.numSets + q.data.numSets;
        r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};
         r.data.aneasthesia = {p.data.aneasthesia{:} q.data.aneasthesia{:}};
           r.data.onset = [p.data.onset; q.data.onset];
        % add nptdata objects as well
        r.nptdata = plus(p.nptdata,q.nptdata);
    end
end
