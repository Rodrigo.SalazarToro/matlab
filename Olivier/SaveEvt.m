% function SaveEvt(fname,times,labels,srate,Samps);
%
% Saves data in an event file format for Neuroscope.
% Samps is boolean that says whether your input is in samples or not if
% from the OG_extract_events it is not thus =0;
% input fname = '2016122201.LIG.evt'
% times = 
% labels = cell with strings


function SaveEvt(fname,times,labels,srate,Samps)

if nargin<4, Samps=0; end;

if (length(times)~=length(labels))
    fprintf('Error, the number of samples is not equal to the number of labels\n');
    exit(0);
end;

if (Samps)
    mstimes = times/srate*1000;

else
    mstimes = times;
end

% mstimes1=mstimes(2:end);
% Diff=mstimes1-mstimes(1:end-1);
% I=find(Diff<IntRms);
% 
% mstimes=mstimes(I);
% labels=labels(I);

fm = fopen(fname,'w');

for ii=1:length(mstimes)
    fprintf(fm,'%0.5f\t%s\n',mstimes(ii),labels{ii});
end

fclose(fm);

