function [Ev,iD]=OG_extract_events
% at the session level
% Ev in milliseconds
format long

[TimeStampsData, ~,~,NumberOfValidSamples, Samples, Header] = Nlx2MatCSC('CSC1.ncs', [1 1 1 1 1],1,1,[]);
[TimestampsEv,EventID,TTL,Extras,EvStr,Header]=Nlx2MatEV('Events.nev',[1 1 1 1 1],1,1,1);

% samplePerTS=size(Samples,1);
%durTS=samplePerTS/sampRec;



firstTS=TimestampsEv(1);
tsEv=TimestampsEv(2:end);
idx=find(TTL(2:end) > 0);

tsEv1=tsEv(idx);
TTL1=TTL(2:end);

Ev=(tsEv1-firstTS)/1000;
iD=TTL1(idx);

save('Events.mat','Ev','iD');
    
    