clear all
sdir = 'E:\salazar\Electrophy\CLAstimmPFCrec';
cd(sdir)
days = {'20161128' '20161129' '20161130' '20161201' '20161205' '20161206' '20161207b' '20161208' '20161222' '20170113'  '20170404' '20170412' '20170424' '20170426' '20170502'};

% days = {'20161128' '20161129' '20161130' '20161201' '20161201' '20161206' '20161207b' '20161208' '20161222' '20170113' '20170404'};

% put a 'aneasthesia.txt' file with the 'isoflurane' or 'awake' in it. At the session level.
cd(sdir)

for d = 1 : length(days)
    cd(days{d});
    sessions = nptDir('session*');
    for ss = 1 : length(sessions);
        cd(sessions(ss).name);
        % at the session level
        [Ev,iD]=OG_extract_events; % Ev will be in milliseconds
        labels = cell(length(iD),1);
        for ii = 1 : length(iD); labels{ii}= 'pulse';end
        SaveEvt(sprintf('%s0%d.LIG.evt',days{d},ss),Ev,labels,32000,0);
        [firstpulse,seqFreq] = getPulseSeq(Ev,'save');
       
        
        makepwgramPolytrode('name',sprintf('%s%d',days{d},ss));
        makepwgramPolytrode('name',sprintf('%s%d',days{d},ss),'coherency');
        
        cd ..
    end
    cd ..
end
cd(sdir)
obj = ProcessDays(micepsthChR2,'days',days,'NoSites');


%% mean psth
pvalue = 0.05;
state = {'awake' 'isoflurane'};
bin = 75;
minFR = .2;
psth = cell(2,3);
npsth = cell(2,3);
freqs = [10 30 50];
tot = zeros(2,1);
for st = 1 : 2
    [r,ind] = get(obj,'Number','unit','s','aneasthesia',state{st},'minNumSpike',minFR);
    tot(st) = r;
    for fq = 1 : 3
        [r,ind] = get(obj,'Number','unit','s','aneasthesia',state{st},'onset',[fq pvalue/3],'minNumSpike',minFR); % pvalue divided by 3 because of Bonferonni correction
        nc = 1;
        pc = 1;
        for u = 1 : r
            cd(obj.data.setNames{ind(u)});
            load spikesdata
            [n,xout] = hist(spikes{fq},[-bin:bin:6000+bin]);
            n = smooth(n(2:end-1),3);
            xout = xout(3:end);
            baseline = length(find(spikes{fq} <2000));
            stim = length(find(spikes{fq} >=2000 & spikes{fq} <4000));
            if stim > baseline
                psth{st,fq}(pc,:) = (1000 / bin) * (n/ntrials(fq));
                pc = pc + 1;
            else
                npsth{st,fq}(nc,:) = (1000 / bin) * (n/ntrials(fq));
                nc = nc + 1;
            end
        end
    end
end
nSUA = zeros(2,3);
totSUA = zeros(2,1);
sind =cell(2,3);
allgrResSUA = cell(2,1);
for st = 1 : 2
    
    for fq = 1 : 3
        [nSUA(st,fq),sind{st,fq}] = get(obj,'Number','unit','s','aneasthesia',state{st},'onset',[fq pvalue/3],'minNumSpike',minFR);
        allgrResSUA{st} = [allgrResSUA{st}; sind{st,fq}];
    end
    allgrResSUA{st} =unique(allgrResSUA{st});
end

for st = 1 : 2
    for iii = 1 : length(allgrResSUA{st})
        anaesthesia(st).sgr{iii} = obj.data.setNames{allgrResSUA{st}(iii)}(1:end-11);
    end
end
for st = 1 : 2;
    anaesthesia(st).sgr = unique( anaesthesia(st).sgr);
    [totSUA(st),n] = get(obj,'Number','unit','s','aneasthesia',state{st},'minNumSpike',minFR,'list',anaesthesia(st).sgr);
end

figs{1}=figure;
figs{2}=figure;
selSUA{1} = 'onset increased spikes';
selSUA{2} = 'onset decreased spikes';
for fi = 1: 2
for st = 1 : 2
    for fq = 1 : 3
        figure(figs{fi})
        subplot(2,3,(st-1)*3 +fq)
        if ~isempty(psth{st,fq}) && fi ==1;
            plot(xout,psth{st,fq}')
        elseif ~isempty(npsth{st,fq}) && fi ==2;
             plot(xout,npsth{st,fq}')
        end
        xlabel('Time (ms)')
        ylabel('Firing rate (sp/s)')
    end
    title(sprintf('%s %s',state{st},selSUA{fi}))
end
end
figs{3} = figure;
figs{4} = figure;
figs{5} = figure;
figs{6} = figure;
for cre = 1 : 2
    
for st = 1 : 2
    for fq = 1 : 3
        if cre == 1
        thepsth = psth{st,fq};
        elseif cre == 2
           thepsth = npsth{st,fq}; 
        end
        if ~isempty(thepsth)
            
            data = thepsth;
            mdata = mean(thepsth);
            vdata = std(thepsth);
            zpsth = ( data - repmat(mean(data,2),1,size(data,2))) ./ (repmat(std(data,[],2),1,size(data,2)));
            figure(figs{cre+2})
            subplot(2,3,(st-1)*3 +fq)
            plot(xout,mdata' + mean(zpsth)')
            hold on
            plot(xout,(mdata + mean(zpsth) + vdata.*(std(zpsth) / sqrt(size(zpsth,1))))')
            plot(xout,(mdata + mean(zpsth) - vdata.*(std(zpsth) / sqrt(size(zpsth,1))))')
            ylim([-1 12])
            xlabel('Time (ms)')
            ylabel('Firing rate (Hz)')
            title(sprintf('Frequency %d Hz %s; n = %d / %d',freqs(fq),state{st},size(zpsth,1),totSUA(st)))
            figure(figs{cre+4})
            subplot(2,3,(st-1)*3 +fq)
            plot(xout,mdata')
            hold on
            plot(xout,(mdata + vdata/ sqrt(size(zpsth,1)))')
            plot(xout,(mdata - vdata/ sqrt(size(zpsth,1)))')
            ylim([-1 12])
            xlabel('Time (ms)')
            ylabel('Firing rate (Hz)')
            title(sprintf('Frequency %d Hz %s; n = %d / %d',freqs(fq),state{st},size(zpsth,1),totSUA(st)))
        end
    end
end

legend('mean +/- SEM')

set(figs{cre+2},'Name','z-score normalized')
set(figs{cre+4},'Name','no normalization')
end
InspectGUI(obj,'unit','s','aneasthesia','isoflurane','onset',[3 pvalue/3],'minNumSpike',minFR)
InspectGUI(obj,'unit','s','aneasthesia','awake','onset',[3 pvalue/3],'minNumSpike',minFR,'psthbin',100,'smoothPSTH')
