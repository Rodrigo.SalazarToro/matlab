% cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
% mtst = mtstrial('auto');
% allloc = vecr(unique(mtst.data.CueLoc));
% allobj = unique(mtst.data.CueObj);
% align = {'CueOnset','MatchOnset','FirstSac'};
load ispikes.mat

allymin = [];
allymax = [];
for r = 1 : 2
    for cue = 1 : 9
        for al = 1 : 3
            
            [bins{cue,al},spikefr{cue,al},A{cue,al},ymin{cue,al},ymax{cue,al},reference{cue,al},ttspikes, tttrials, fr{cue,al}] = plotMTSpsth(mtst,sp,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1,'alignement',align{al},'bin',100,'rule',r,'ML');%,varargin2{:});
            
        end
    end
    
    save(sprintf('nineStimPSTH%d.mat',r),'bins','spikefr','A','ymin','ymax','reference','fr')
end


load nineStimPSTH1.mat
c = 1; for cue = 1 : 9; labels{c} = sprintf('ide %d; loc %d',cueComb(2,c),cueComb(1,c)); c = c + 1; end
ncues = 9;
allymin = [];
allymax = [];
for cue = 1 : ncues
    
    for al = 1 : 3
        subplot(ncues,3,(cue-1) * 3 + al);
        plot(bins{cue,al}+ 1,spikefr{cue,al});
        if length(bins{cue,al}) > 1
            axis([bins{cue,al}(1) bins{cue,al}(end) ymin{cue,al} ymax{cue,al}])
        end
        
        hold on
        line([0 0],[0 max(spikefr{cue,al})],'Color','r')
        %             if ~isempty(Args.displayStats) & al == 1
        %                 load FR4epochs.mat
        %                 [cueP,delayP,matchP] = CellFRstats(allspikes);
        %                 text(1500,max(spikefr{cue,al}),sprintf('p = %.4f',eval(sprintf('%s.sample(cue).p',cell2mat(Args.displayStats)))));
        %             end
        if al == 1; text(0,max(spikefr{cue,al}),sprintf('n=%d',size(A{cue,al},1))); end
        hold off
        allymin = [allymin ymin{cue,al}];
        allymax = [allymax ymax{cue,al}];
        if al == 1; ylabel(labels{cue}); end
    end
end
for sb = 1 : ncues*3; subplot(ncues,3,sb); ylim([min(allymin) max(allymax)]); set(gca,'TickDir','out');end