cd /Volumes/raid/data/monkey
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

obj = loadObject('psthObjIDEandLong.mat');
% allcc = unique(obj.data.Index(:,3));
allcc = [0 : 4];
acount = 1;
gareas = [];
clear sfc sfcSur
for cc = 1 : length(allcc)
    
    %     [r,ind] = get(obj,'Number','cortex',allcc(cc));
    [r,locTuned] = get(obj,'Number','LocTuned',allcc(cc),'epochTuned',1,'tuneLog','==');
    [r,ideTuned] = get(obj,'Number','ideTuned',allcc(cc),'epochTuned',1,'tuneLog','==');
    
    [r,locTunedD] = get(obj,'Number','LocTuned',allcc(cc),'epochTuned',2,'tuneLog','==');
    [r,ideTunedD] = get(obj,'Number','ideTuned',allcc(cc),'epochTuned',2,'tuneLog','==');
    
   if cc == 1 
      
        ind = intersect(intersect(intersect(locTuned,ideTuned),ideTunedD),locTunedD);
   else
      ind = union(union(union(locTuned,ideTuned),ideTunedD),locTunedD);
       ind = union(intersect(locTuned,ideTuned),intersect(ideTunedD,locTunedD));
      
      
   end
    r = length(ind);
    count = 1;
    if r >1
        for ii = 1 : r
            starst = findstr(obj.data.setNames{ind(ii)},'group00');
            cd(obj.data.setNames{ind(ii)})
            files = nptDir('SFCall1g00*Rule1.mat');
            sfiles = nptDir('SFCall1g00*Rule1Sur.mat');
            if ~isempty(files) && length(files) == length(sfiles)
                sameEl = obj.data.setNames{ind(ii)}(starst:starst+8);
                grn = str2num(obj.data.setNames{ind(ii)}(starst+6:starst+8));
                allfiles = strvcat(files(:).name);
                allgr = str2num(allfiles(:,10:12));
                if isempty(findstr('betty',pwd))
                    if grn <= 8
                        delCH = find(allgr <= 8);
                    else
                        delCH = find(allgr > 8);
                    end
                else
                    if grn <= 32
                        delCH = find(allgr <= 32);
                    else
                        delCH = find(allgr > 32);
                    end
                    
                end
                
                sameFile = sprintf('SFCall1%sRule1.mat',sameEl);
                saFn = strmatch(sameFile,allfiles);
                files(delCH) = [];
                
                sfiles(delCH) = [];
                
                npairs = length(files);
                
                for ff = 1 : npairs
                    load(files(ff).name)
                    
                    load(sfiles(ff).name)
                    
                    sfc{acount}(count,:,:) = C;
                    sfcSur{acount}(count,:,:) = squeeze(Prob(:,1,:));
                    
                    count = count + 1;
                end
            end
        end
        acount = acount + 1;
        gareas = [gareas allcc(cc)];
    end
    
end



% figure
% for sb = 1 : 5
%     for p = 1 : 4
%         subplot(5,4,(sb-1)*4+p)
%         sigSFC = sfc{sb}(:,:,p) > sfcSur{sb}(:,:,p);
%         plot(f,sum(sigSFC,1)/size(sigSFC,1))
%         title(sprintf('%s %d pairs',areas{gareas(sb)},size(sigSFC,1)))
%         ylim([0 0.5])
%     end
% end
% figure
% for sb = 6 : 11
%     for p = 1 : 4
%         subplot(6,4,(sb-6)*4+p)
%         sigSFC = sfc{sb}(:,:,p) > sfcSur{sb}(:,:,p);
%         plot(f,sum(sigSFC,1)/size(sigSFC,1))
%         title(sprintf('%s %d pairs',areas{gareas(sb)},size(sigSFC,1)))
%         ylim([0 0.6])
%     end
% end
figure
for sb = 1 : length(gareas)
    for p = 1 : 4
        subplot(length(gareas),4,(sb-1)*4+p)
        sigSFC = sfc{sb}(:,:,p) > sfcSur{sb}(:,:,p);
        plot(f,sum(sigSFC,1)/size(sigSFC,1))
        title(sprintf('%d pairs',size(sigSFC,1)))
        ylim([0 0.5])
    end
end