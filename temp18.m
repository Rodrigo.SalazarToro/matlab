tdays = [nptDir('0*'); nptDir('1*')];
for d = 1 : length(tdays); days{d} = tdays(d).name; end


for d = 1 : length(days)
    clear stds
    cd(days{d})
    if isempty(nptDir('skip.txt'))
        [ch,allCHcomb,iflat,groups] = getFlatCh('cohtype','cohInter');
        sch = setdiff(ch, iflat.ch);
        if ~isempty(nptDir('session01'))
            cd session01
            if isempty(nptDir('skip.txt'))
                tobj = mtstrial('auto');
                cd lfp
                if isempty(nptDir('skip.txt'))
                    files = nptDir('*_lfp.*');
                    for t = 1 : size(files,1)
                        fixOnset = round(tobj.data.CueOnset(t)) - 500;
                        sacOnset = round(tobj.data.MatchOnset(t));
                        [lfp,num_channels,sampling_rate,scan_order,points]=nptReadStreamerChannel(files(t).name,sch);
                        stds(:,t) = std(lfp(:,fixOnset : sacOnset),[],2);
                    end
                    save stds.mat stds
                    clear stds
                end
                cd ..
            end
            cd ..
        end
    end
    cd ..
end


for d = 1  : length(days)
    cd(days{d})
    if isempty(nptDir('skip.txt'))
        sessions = nptDir('session0*');
        for s = 2 : length(sessions)
            cd(sessions(s).name)
            if isempty(nptDir('skip.txt'))
                [C,phi,f,unit] = spikeFieldCoh('save','stable','BehResp',1,'surrogate');
            end
            cd ..
        end
    end
    cd ..
end
