for d =1 : 16
    cd(days{d})
    cd session01
    load SNR_channels.mat
    SNR_channels(isnan(SNR_channels)) =0;
%      SNR_channels(SNR_channels > 12) =nan;
     for ch =1 : size(SNR_channels,2)
      [threshold,outliers,kdist] = getKurtosisThresh(SNR_channels(:,ch));
      SNR_channels(outliers,ch) = nan;
     end
    figure
    hist(reshape(SNR_channels,1,size(SNR_channels,1),size(SNR_channels,2)),300)
    allSNR{d} = SNR_channels;
     cd ../..
end
    
    