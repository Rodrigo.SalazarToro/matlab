tune = {'ide','loc'};

areas = {'PP' 'PF'};
clear tuning rules
% tuning = cell(2,3,4,2);
tuning = [];
index = [];
% rules = cell(2,3,4,2);
co = {'r' 'k' 'b'};
limitB = find(f>= 12 & f<= 25);
count = 0;
for d = 1 : 21
    cd(days{d})
    for a = 1 : 2
        for s = 1 : 2
            cd(sprintf('session0%d/lfp',s+1))
            %          [cohPP,cohPF,cohInter,f] = checktuning('save','remoteName','anomaly','locTuning');
            %          [cohPP,cohPF,cohInter,f] = checktuning('save','remoteName','anomaly','ideTuning');
            mt = mtstrial('auto');
            r = unique(mt.data.Index(:,1));
            for t = 1 : 2
                load(sprintf('%sTuningRule%d.mat',tune{t},r))
                
                for p = 1 : 4
                    
                    for c = 1 : 3
                        data = eval(sprintf('squeeze(power%s.power(c).S(:,:,:,p))',areas{a}));
                        
%                         me = squeeze(mean(data,3));
%                         st = squeeze(std(data,[],3));
                        %                         data = (data - repmat(me,[1 1 size(data,3)])) ./ repmat(st,[1 1 size(data,3)]);
                        if length(size(data)) == 3
                            tuning = [tuning; mean(data,3)];
                            %                             rules{a,c,p,t} = [rules{a,c,p,t}; repmat(r,size(data,1),1)];
                            thedata = mean(data,3);
                            npairs = size(data,1);
                        elseif length(size(data)) == 2
                            tuning = [tuning; mean(data,2)'];
                            %                             rules{a,c,p,t} = [rules{a,c,p,t}; repmat(r,size(data,1),1)];
                            thedata = mean(data,2)';
                            npairs = 1;
                        end
                        clear tindex
                        tindex(1,:) = repmat(a,1,npairs);
                        tindex(2,:) = repmat(c,1,npairs);
                        tindex(3,:) = repmat(p,1,npairs);
                        tindex(4,:) = repmat(t,1,npairs);
                        tindex(5,:) = repmat(r,1,npairs);
                        
                        tindex(6,:) = [count + 1: count + npairs];
                        tindex(7,:) = nan(1,npairs);
                        index = [index tindex];
                        tarea(:,c) = sum(thedata(:,limitB),2);
                    end
                    index(7,isnan(index(7,:))) = repmat(reshape((max(tarea,[],2) - min(tarea,[],2)) ./ min(tarea,[],2),1,size(tarea,1)),1,3);
                    clear tarea
                end
            end
            cd ../..
        end
        
        
        count = count + npairs;
    end
    cd ..
    
end

lim(1) = prctile(index(7,find(index(3,:) == 1)),99);
lim(2) = prctile(index(7,find(index(3,:) == 1)),95);
for r = 1 : 2
    figure
    for t = 1 : 2
        for p = 1 : 4
            ind = find(index(3,:) == p & index(5,:) == r & index(4,:) == t);
            subplot(4,2,(t - 1)* 4 +p)
            [n,xout] = hist(index(7,ind),[0 : 0.002 : 1]);
            bar(xout,n/3)
            
            hold on
            for l = 1 : 2; line([lim(l) lim(l)],[0 25],'Color','r'); hold on;end
            axis([ 0 0.8 0 10])
        end
    end
end



% for a = 1 : 2
%     for it = 1 : size(tuning,1)
%         clf
%         maxy = [];
%         for t =1 : 2
%             for p = 1 : 4
%                 for c = 1 : 3
%                     subplot(2,4,(t-1)*4+p)
%                     thedata = tuning{a,c,p,t}(it,:);
%                     tarea(c) = sum(thedata(limitB));
%                     plot(f,thedata,co{c})
%                     maxy = [maxy max(thedata)];
%                     title(rules{a,c,p,t}(it))
%                     xlabel(tune{t})
%                     hold on
%                 end
%                 tindex{a,p,t,rules{a,c,p,t}(it)}(it) = (max(tarea) - min(tarea)) / (max(tarea) + min(tarea));
%                 text(40, max(maxy),sprintf('%0.1g',tindex{a,p,t,rules{a,c,p,t}(it)}(it)))
%             end
%         end
%         for sb = 1 : 8; subplot(2,4,sb); axis([0 50 0 max(maxy)]); end
%         % pause
%     end
%
% end
%
