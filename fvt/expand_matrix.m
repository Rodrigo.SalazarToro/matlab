function b = expand_matrix(a,n,m)
%b =expand_matrix(a,n)
%
%this function takes a matrix a
%and expands each value of a into n rows and m columns



%close all
%pcolor(a)

%found out how many times to repeat each block
repeatrow = floor(n/size(a,1));
repeatcol = floor(m/size(a,2));

for i=1:size(a,1)
   for j=1:size(a,2)
      b((i-1)*n+1:i*n,(j-1)*m+1:(j*m))=a(i,j);
   end
end

      
      
      
 %     figure
%pcolor(b)

      