% play_revcorr_sn.m - shows results of revcorr_sn as a movie
%
% function play_revcorr_sn(R,init_info,r_thresh)
%

function play_revcorr_sn(R,init_info,r_thresh)

if ~exist('r_thresh','var')
    r_thresh=0.5;
end

% revcorr dimensions
[M N]=size(R);
R=R(1:M-1,:);   % get rid of last row, which is outside box
M=M-1;

% normalize R matrix
R=R/max(R(:));  

% get init_info variables
rows=init_info.rows;
cols=init_info.cols;
num_or=init_info.num_orientations;
angle=init_info.bar_orientation;
num_colors=init_info.num_colors;

% get i j o c params for each index
seq=index2ijoc([0:M-1]',num_colors,num_or,rows,cols);

% compute bar start and end points for each grid position
x=seq(:,2)-floor(cols/2);
y=-(seq(:,1)-floor(rows/2));
bar_start=[x'; y'-0.5]; % start points
bar_end=[x'; y'+0.5];   % end points

% rotate points according to grid orientation
for k=0:num_or-1
    theta=(90-angle(k+1))*pi/180;
    % rotation matrix for clockwise rotation by theta
    rot=[cos(theta) sin(theta); -sin(theta) cos(theta)];
    i=find(seq(:,3)==k);
    bar_start(:,i)=rot*bar_start(:,i);
    bar_end(:,i)=rot*bar_end(:,i);
end

% set bar colors: green for dark bar, red for light bar
color=[0 1 0; 1 0 0]; 

% setup separate axes for dark and light bars
clf
for c=0:1
    h(c+1)=subplot(1,2,c+1);
    axis equal
    axis(1.5*[-cols cols -rows rows]/2)
    grid
end

% loop through time, starting from spike time into past
for j=N:-1:1    
    
    % sort r values in ascending order, and find those exceeding threshold
    [r ind]=sort(R(:,j));
    ri=find(r>r_thresh);

    % loop over bar colors
    for c=0:1
        ci=ri(find(seq(ind(ri),4)==c)); % find indices for this bar color
        axes(h(c+1)), cla   % clear axes
        if ~isempty(ci)
            X=[bar_start(1,ind(ci)); bar_end(1,ind(ci))];
            Y=[bar_start(2,ind(ci)); bar_end(2,ind(ci))];
            hl=line(X,Y);
            set(hl,'LineWidth',2);
            for i=1:length(ci)
                set(hl(i),'Color',r(ci(i))*color(c+1,:)+(1-r(ci(i)))*[1 1 1]);
            end
        end
        text(-1.45*cols/2,-1.4*rows/2,sprintf('%03d ms',N-j));
    end

    drawnow
        
end
