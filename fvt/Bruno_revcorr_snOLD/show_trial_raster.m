% show_trial_raster.m - displays trial raster together with smoothed psth
%
% function show_trial_raster(raster,bin_size,frame_dur)
%
% raster:       raster to display
% bin_size:     bin size used in raster (ms) - used for labeling plots
% frame_dur:    if defined, sets width of intervals superimposed on plots (ms)

function show_trial_raster(raster,bin_size,frame_dur)

[num_trials n]=size(raster);
t=[0:n-1]*bin_size;

if exist('frame_dur','var')
    num_frames=floor((n-1)*bin_size/frame_dur);
end

G=exp(-0.5*[-2:2].^2);
G=G/sum(G);

colormap(gray)
subplot(211)
imagesc(t,1:num_trials,-raster)
axis([0 t(n) 1 161])
if exist('frame_dur','var')
    line([1 1]'*frame_dur*[0:num_frames-1],[1 num_trials]'*ones(1,num_frames))
end
xlabel('time (msec)')
ylabel('trial #')

subplot(212)
plot(t,conv2(mean(raster),G,'same'))
axis([0 t(n) 0 0.05])
if exist('frame_dur','var')
    line([1 1]'*frame_dur*[0:num_frames-1],[0 0.05]'*ones(1,num_frames))
end
xlabel('time (msec)')
ylabel('mean # of spikes')

