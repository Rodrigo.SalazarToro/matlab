% read_eyepos.m - read eye position from calibrated eye position
% files and shift if necessary
%
% function [eyepos sample_rate] = read_eyepos(filename_root,duration,hv)
%
% filename_root:    Root filename (e.g. tang06190108)
% duration:         Optional duration (sec) obtained from infer_latency.m
%                   (used to align eye position traces to correct
%                   for variable offset).
% hv:               Indices of horizontal and vertical coordinates 
%                   (default=[2 1])
%
% eyepos:           2 x N x num_trials array of eye position in screen
%                   coordinates.  First row is horizontal, second row
%                   is vertical.  N is determined by duration or
%                   length of file.
% sample_rate:      Sample rate of eye position
% 
% Assumes that you reside in same directory as raw streamer files, and that
% the eye/ subdirectory exists.

function [eyepos, sample_rate] = read_eyepos(filename_root,duration,hv)

if ~exist('hv','var')
   hv=[2 1];
end

cd('eye');

D=dir([filename_root '_eye.0*']);

for i=1:length(D)
   
   fprintf('%d ',i);
   
   [data num_chan sample_rate so n]=nptReadDataFile(D(i).name);
   
   
   if exist('duration','var')
      dur_n=floor(duration*sample_rate)+1;
      if i==1														%if first time
         eyepos=zeros(length(hv),dur_n,length(D));		%initalize matrix
      end
      if n>=dur_n
         eyepos(:,:,i)=data(hv,n-dur_n+[1:dur_n]);
      else
         eyepos(:,1:n,i)=data(hv,:);
         fprintf(' warning: length shorter than requested duration');
      end
   else
      eyepos(:,1:n,i)=data(hv,:);
   end
   
end

fprintf('\n')

cd ('..')
