% infer_latency.m - Loops over trial data and infers variable stimulus
% latency from length of streamer file.  Used to correct data
% collected with hardware bug in which the control pulse used to start
% data acquisition was not properly synchronized with the actual start
% of stimulus display.
%
% You must reside in the directory containing the data files.
%
% function [dT actual_stim_dur] =
%   infer_latency(filename_root,stim_dur,refresh_rate,refreshes_per_frame)
%
% filename_root:        root of the filename of streamer data (e.g.,
%                       tang06190108)
% stim_dur:             commanded stimulus sequence duration (sec)
% refreshes_per_frame:  number of refreshes per frame 
% refresh_rate:         actual rate of monitor (Hz)
%
% dT:                   array of inferred stimulus latencies (in sec.)
% actual_stim_dur:      actual stimulus duration (rounded up to integer 
%                       number of frames) (sec)
%
%see stimulus_latency_bug.pdf in doc folder for more information
%

function [dT, actual_stim_dur] = ...
    infer_latency(filename_root,stim_dur,refresh_rate,refreshes_per_frame)

% actual # of frames displayed
actual_frames=ceil(stim_dur*refresh_rate/refreshes_per_frame);
% actual stimulus duration is one refresh beyond actual # of frames displayed
actual_stim_dur=actual_frames*refreshes_per_frame/refresh_rate + 1/refresh_rate;

fprintf('Inferring stimulus latency using actual stimulus duration of %f sec.\n',...
        actual_stim_dur);
    
D=dir([filename_root '.*']);

for i=1:length(D)
    
    j = sscanf(D(i).name,'%*[^.].%d');
    
    if ~isempty(j)
        
        fprintf('%d ',j);

        [data num_chan sr so n]=nptReadStreamerFile(D(i).name);
        
        dT(j) = (n-1)/sr - actual_stim_dur;
        
        if dT(j)<0
            fprintf('  warning: dT=%f',dT(j));
        end
        
    end
    
end

fprintf('\n')
