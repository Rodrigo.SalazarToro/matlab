% trial_raster.m - makes a rastergram of spikes according to trials
%
% function rastergram = trial_raster(session,cluster,bin_size)
%
% session:      session data (from GenerateSessionSpikeTrains)
% cluster:      desired spike cluster within session
% bin_size:     bin size for raster (ms).  default = 1
%
% rastergram:   m x n array of spike rasters.  m = number of trials, n=trial length in bins
%

function rastergram = trial_raster(session,k,bin_size)

if nargin<3
    bin_size=1;
end

m=length(session.trial);
n=floor(1000*session.duration/bin_size);

rastergram = zeros(m,n);

for i=1:m
    
    j=floor(session.trial(i).cluster(k).spikes/bin_size)+1;
    rastergram(i,j)=1;
    
end
