% chop.m - chops a signal into pieces by a fixed interval in time
%
% function R = chop(s,sample_rate,T)
%
% s             1D signal to be chopped
% sample_rate   sample rate of signal (Hz)
% T             temporal interval to chop by (sec)
%
% R             m x n array of the chopped signal
%               m=floor(length(s)/n), n=floor(sample_rate*T)

function R = chop(s,sample_rate,T)

n=floor(T*sample_rate);
m=floor(length(s)/n);

R=zeros(m,n);

for i=0:m-1

    j=floor(i*T*sample_rate);
    R(i+1,:)=s(j+[1:n]);
    
end
