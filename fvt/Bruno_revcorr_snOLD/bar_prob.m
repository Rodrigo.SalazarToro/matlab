% bar_prob.m - calculates marginal probability of each bar configuration for
% each time relative to spike
%
% function P = bar_prob(session, init_info, noise_seq,
%                       window_size, bin_size, eyepos)
%
% session:      session structure containing all spike info for each trial
%               (Note: session.duration is assumed to be correct.)
% init_info:    init_info structure returned by read_init_info (contains frame 
%               # for each trial)
% noise_seq:    sparse noise parameters for each frame
% window_size:  reverse correlation window (ms)
% bin_size:     bin size (ms)
% eyepos:       optional eye position data to correct for eye movements
%               (assumed to be already sampled according to bin_size)
%
% P:            M x N array of bar probability. Each column contains a
%               probability distribution over bar configurations at a given
%               time relative to the occurence of a potential spike -- P(bar).
%               M = # of sparse noise configurations, N = # of time bins.  
%               (See ijoc2index for conversion from ijoc to row index of R.)

function P = bar_prob(session,k,init_info,seq,window_size,bin_size,skiptrials,eyepos)

num_trials=length(session.trial);

num_colors=init_info.num_colors;
num_or=init_info.num_orientations;
rows=init_info.rows;
cols=init_info.cols;
M=rows*cols*num_or*num_colors + 1; % +1 is for bar outside grid
N=floor(window_size/bin_size);

P=zeros(M,N);
count=zeros(1,N);

refresh_rate=85.1;  % temporary fix: init_info.refresh_rate=85, which is wrong
refreshes_per_frame=init_info.refreshes_per_frame;
bins_per_trial=ceil(1000*session.duration/bin_size);
frames_per_trial=init_info.trial_separation;
max_bins_per_trial=floor((1000*frames_per_trial*refreshes_per_frame/refresh_rate)/bin_size);
trial_frames=floor([0:max_bins_per_trial-1]*bin_size/(1000*refreshes_per_frame/refresh_rate));

for i=2:num_trials-1    % first trial is usually bad (data timing corrupted by latency bug)
   % last trial contains only a fraction of usual duration
   if isempty(skiptrials) | isempty(find(skiptrials==i))
      
      first_frame = init_info.frames(i);
      trial_seq=seq(first_frame+trial_frames+1,:); % frames for this trial in steps of bin_size
      trial_seq(find(trial_seq(:,1)==-1),:)=[];   % ignore spont. frames
      trial_seq=trial_seq(1:bins_per_trial,:);  % truncate after last potential spike
      
      if exist('eyepos','var')
         % correct for eye position
         trial_seq=eyeshift_sn(trial_seq,init_info,eyepos(:,:,i));    
      end
      
      % convert params to indices
      trial_seq=ijoc2index(trial_seq,num_colors,num_or,rows,cols);  
      
      for j=1:bins_per_trial    
         ii=trial_seq(j)+1;
         jj=max(1,j-(bins_per_trial-N)):N;
         
         P(ii,jj) = P(ii,jj) + 1;
         count(jj) = count(jj) + 1;
         
      end
   end
   
  % fprintf(' %d',i);
end

%fprintf('\n');

% normalize R
for i=1:N
   P(:,i)=P(:,i)/count(i);
end
