% shift_spikes.m - shifts spikes in spike structure by a specified amount 
% for each trial.  Also corrects duration of spike structure.
%
% function shifted_spike = shift_spikes(spike,dT,actual_stim_dur)
%
% spike:          session data containing spikes to be shifted
% dT:               array of shift amounts for each trial (sec.)
% actual_stim_dur:  actual duration of stimulus and data acquisition
%
% shifted_spike:  spike structure containing shifted spikes and
%                   correct duration

function shifted_spike = shift_spikes(spike,dT,actual_stim_dur) 

num_trials=length(spike.trial);

if length(dT)~=num_trials
    fprintf('number of trials in session and size of time shift array do not match\n');
    return
end

num_clusters=length(spike.trial(1).cluster);

shifted_spike=spike;
shifted_spike = struct(shifted_spike);
shifted_spike.duration = actual_stim_dur;

for i=1:num_trials
    
    for k=1:num_clusters
        
         spike_array=spike.trial(i).cluster(k).spikes;
         spike_array=spike_array-1000*dT(i);
         j=find(spike_array>=0);
         shifted_spike.trial(i).cluster(k).spikes=spike_array(j);
         shifted_spike.trial(i).cluster(k).spikecount=length(spike_array(j));
         
    end
     
 end
 