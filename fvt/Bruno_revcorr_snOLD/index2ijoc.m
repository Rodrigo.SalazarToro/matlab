% index2ijoc.m - converts sparse noise index to [i j o c] params
%
% function seq=index2ijoc(index,num_colors,num_or,rows,cols)
%
% index:        list of indices to be converted
% num_colors:   number of colors
% num_or:       number of orientations
% rows:         number of rows
% cols:         number of cols
%
% seq:          sequence array whose rows are [i j o c] params corresponding 
%               to each entry in index array

function seq=index2ijoc(index,num_colors,num_or,rows,cols)

M=rows*cols*num_or*num_colors + 1;

seq(:,1)=mod(index,rows);
seq(:,2)=floor(mod(index,rows*cols)/rows);
seq(:,3)=floor(mod(index,rows*cols*num_or)/(rows*cols));
seq(:,4)=floor(index/(rows*cols*num_or));

i=find(index==M-1);
seq(i,1:2)=-1;
seq(i,3:4)=0;
