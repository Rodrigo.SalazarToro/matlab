% compare_chop.m - script to compare chopped up psth's
% 
% bin_size, raster and raster_s must be defined

if ~exist('G','var')
    G=exp(-0.5*[-2:2].^2);
    G=G/sum(G);
end

R=chop(conv2(mean(raster),G,'same'),1000,3/85.1);
Rs=chop(conv2(mean(raster_s),G,'same'),1000,3/85.1);

[m n]=size(R);
t=[0:n-1]*bin_size;

subplot(221)
imagesc(t,1:m,R)
ylabel('frame #')
xlabel('time (ms)')
title('original')

subplot(223)
plot(t,mean(R),'LineWidth',2)
hold on
plot(t,mean(R)+std(R),'b--',t,mean(R)-std(R),'b--')
axis([0 t(n) 0 .02])
ylabel('mean # of spikes')
xlabel('time (ms)')
hold off

subplot(222)
imagesc(t,1:m,Rs)
ylabel('frame #')
xlabel('time (ms)')
title('shift corrected')

subplot(224)
plot(t,mean(Rs),'LineWidth',2)
hold on
plot(t,mean(Rs)+std(Rs),'b--',t,mean(Rs)-std(Rs),'b--')
axis([0 t(n) 0 .02])
ylabel('mean # of spikes')
xlabel('time (ms)')
hold off