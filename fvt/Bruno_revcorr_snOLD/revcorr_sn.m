% revcorr_sn.m - performs reverse correlation of spike train against sparse 
% noise parameters
%
% function [R Pspike] = revcorr_sn(session, cluster_num, init_info, noise_seq,
%                                  window_size, bin_size, skiptrials, eyepos)
%
% session:      session structure containing all spike info for each trial
%               (Note: session.duration is assumed to be correct.)
% cluster_num:  desired spike train cluster within session for performing 
%               reverse correlation
% init_info:    init_info structure returned by read_init_info (contains frame 
%               # for each trial)
% noise_seq:    sparse noise parameters for each frame
% window_size:  reverse correlation window (ms)
% bin_size:     bin size (ms)
% skiptrials:   skip trials is a matrix listing the trial numbers which should be
%               ignored for this analysis.  These trials are usually ignored becuase 
%               Streamer.c fouled up the data for these trials in some way.  
%               skiptrials should be an empty matrix if no trials are to be skipped.
% eyepos:       optional eye position data to correct for eye movements
%               (assumed to be already sampled according to bin_size)
%
% R:            M x N reverse correlation array. Each column contains a
%               probability distribution over bar configurations at a given
%               time relative to the occurence of a spike -- P(bar|spike).
%               M = # of sparse noise configurations, N = # of time bins.  
%               (See ijoc2index for conversion from ijoc to row index of R.)
%
% Pspike:       marginal probability of spike -- P(spike)

function [R, Pspike] = ...
   revcorr_sn(session,k,init_info,seq,window_size,bin_size,skiptrials,eyepos)


num_trials=length(session.trial);

num_colors=init_info.num_colors;
num_or=init_info.num_orientations;
rows=init_info.rows;
cols=init_info.cols;
M=rows*cols*num_or*num_colors + 1; % +1 is for bar outside grid
N=floor(window_size/bin_size);

R=zeros(M,N);
Rcount=zeros(1,N);

refresh_rate=85.1;  % temporary fix: init_info.refresh_rate=85, which is wrong
refreshes_per_frame=init_info.refreshes_per_frame;
bins_per_trial=ceil(1000*session.duration/bin_size);
frames_per_trial=init_info.trial_separation;
max_bins_per_trial=floor((1000*frames_per_trial*refreshes_per_frame/refresh_rate)/bin_size);
trial_frames=floor([0:max_bins_per_trial-1]*bin_size/(1000*refreshes_per_frame/refresh_rate));

Pspike=0;

for i=2:num_trials-1    % first trial is usually bad (data timing corrupted by latency bug)
 % last trial contains only a fraction of usual duration
   if isempty(skiptrials) | isempty(find(skiptrials==i))
      spike_bin = floor(session.trial(i).cluster(k).spikes/bin_size);   % bin spikes
      num_spikes = session.trial(i).cluster(k).spikecount;
      
      if num_spikes>0
         
         first_frame = init_info.frames(i);
         trial_seq=seq(first_frame+trial_frames+1,:); % frames for this trial in steps of bin_size
         trial_seq(find(trial_seq(:,1)==-1),:)=[];   % ignore spont. frames
         trial_seq=trial_seq(1:max(spike_bin)+1,:);  % truncate after last spike
        
         if exist('eyepos','var')
            % correct for eye position
            trial_seq=eyeshift_sn(trial_seq,init_info,eyepos(:,:,i));    
         end
         
         % convert params to indices
         trial_seq=ijoc2index(trial_seq,num_colors,num_or,rows,cols);  
         
         for j=1:num_spikes    
            
            si=spike_bin(j) + [-(N-1):0] + 1;  % spike triggered list of indices
            si(find(si<1))=[];  % chop off indices < 0
            
            ii=trial_seq(si)+1;
            jj=N+[-(length(si)-1):0]';
            ind=(jj-1)*M+ii;
            
            R(ind) = R(ind) + 1;
            Rcount(jj) = Rcount(jj) + 1;
            
         end
         
      end
      
      Pspike = Pspike + num_spikes/bins_per_trial;
      
   end%if skiptrials
end

% normalize R
for i=1:N
   R(:,i)=R(:,i)/Rcount(i);
end

Pspike = Pspike/(num_trials-2); % first and last trial are thrown out
