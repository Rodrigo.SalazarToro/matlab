function b = collapse_matrix(a,n,m)
%b = collapse_matrix(a,n,m)
%
%this function takes a matrix a
%and collapses each  n rows and m columns into a single value.

%close all
%pcolor(a)

%found out how many times to repeat each block
repeatrow = floor(size(a,1)/n);
repeatcol = floor(size(a,2)/m);

for i = 1:repeatrow
   for j = 1:repeatcol
      b(i,j)=mean(mean(a((i-1)*n+1:i*n,(j-1)*m+1:(j*m))));
   end
end
%b
%figure
%pcolor(b)

      