Free-Viewing Toolbox 
Change Log (Shih-Cheng Yen)

August 5, 2004
view/gui/displaydata.m: Modifed code to get session name to work with
	both old and new directory structure.

August 24, 2004
view/gui/displaydata.m: Replaced uigetfolder call, which is Windows only,
	with Matlab built-in function uigetdir, which is cross-platform. The
	uigetdir function also has a button to create a new directory if 
	that is necessary.
BatchProcessor/bpGUI2.m: Modified ProcessDirs call to add objects in the
	order they are generated. This makes it possible for the data in the
	objects to be compared to the directories in the nptdata object used
	with ProcessDirs. Replaced uigetfolder call with uigetdir and
	removed code that only ran on Windows machines. Added code to check
	the return value from uigetdir to make sure the user did not hit the
	Cancel button.


September 3, 2004
view/@stiminfo/private/GenerateStimInfo.m: Fixed nptDir calls to use
	correct capitalization.

October 1, 2004
view/gui/displaydata.m: Changed case of adjspikes, adjspikespsth, 
	firingrate objects to lower case so that objects created and saved
	on Windows machines can be read by non-Windows machines.

October 14, 2004
view/gui/displaydata.m: Now calls plot with robj instead of r.

November 11, 2004
Eye/fvtGenerateSessionEyeMovements.m: Now uses getOptArgs to parse
	optional input arguments. Added help comments to describe optional
	input arguments. Now uses the values in Args in creating plot
	legends instead of using hard coded strings. Added code to quit
	properly using the 'q' key. Now turns divideByZero warning back on
	before exiting function.
view/@eyemovements/eyemovements.m: Added code to remove 'Auto' from
	varargin. Added varargin to createObject function so optional input
	arguments to fvtGenerateSessionEyeMovements will be passed on
	properly. Added missing semi-colon to call to create nptdata object.
view/@eyemovements/plot.m: Removed second mandatory argument so that we
	can use this function with nptdata/plot. Added option to plot
	histogram of saccade velocities. 

November 15, 2004
view/@eyemovements/hist.m: New function that contains code that used to 
	be in plot which computes histogram of saccade velocities. Added
	options to specify size of bins.
view/@eyemovements/plot.m: Moved code that computed histogram of saccade
	velocities to hist function. Fixed typo in spelling of 
	NumericArguments.

November 19, 2004
view/@eyemovements/hist.m: Shortened optional input argument names so
	they can be used with other options like fixation and saccade
	durations. Added additional input arguments for computing histograms
	on fixation and saccade durations (code for those options not
	written yet).

November 22, 2004
Eye/fvtGenerateSessionEyeMovements.m: Modified help comments for 
	IgnoreSaccadeMaxAcceleration to correctly identify option as a flag. 

November 23, 2004
view/@eyemovements/ProcessSession.m: Corrected spelling of nptDir.

April-18-2005
view/@revcorr/revcorr.m: Removed code to change directories before 
	instantiating the stiminfo object since the stiminfo object now changes 
	to the appropriate directory before creating the object. Removed 
	multiple calls to pwd and replaced them with a variable intialized to 
	the output of pwd. 
view/@stiminfo/stiminfo.m: Now uses getDataDirs to move to the session 
	directory before creating of loading saved objects. 

October-09-2006
view/gui/displaydata.m: Added 'CaseInsensitive' argument to calls to nptDir 
	so things will work properly on non-Windows machines. 
view/@stiminfo/private/GenerateCatInfo.m: Committing Jonathan's changes. 
view/@revcorr/private/GenerateCatRevcorrMSeqFrame.m: Corrected typo in 
	spelling of kernel. 
view/@revcorr/revcorr.m: Committing Jonathan's changes. 
