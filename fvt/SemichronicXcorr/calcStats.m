function s = calcStats(C)
%s = calcStats(R)
%
%s is a structure
%C is the correlation matrix from R

%initialize variables
s.SignifMean =  NaN([size(C,1) size(C,2)]);
s.SignifStdev = NaN([size(C,1) size(C,2)]);
s.PhaseMean =   NaN([size(C,1) size(C,2)]);
s.PhaseStdev =  NaN([size(C,1) size(C,2)]);
s.Prob =        NaN([size(C,1) size(C,2)]);
for ii=1:size(C,1)
    for jj=ii+1:size(C,2)

        %calculate mean and stdev of Correlation Strengths
        p1 = squeeze(C(ii,jj,:,1));
        ind = ~isnan(p1);
        s.SignifMean(ii,jj)= mean(p1(ind));
        s.SignifStdev(ii,jj)= std(p1(ind));

        %calculate mean and stdev of all phase lags.
        p2 = squeeze(C(ii,jj,:,2));
        ind = ~isnan(p2);
        s.PhaseMean(ii,jj)= mean(p2(ind));
        s.PhaseStdev(ii,jj)= std(p2(ind));

        %calculate probability
        s.Prob(ii,jj) = (sum(~isnan(p1))) / (length(p1));
    end
end