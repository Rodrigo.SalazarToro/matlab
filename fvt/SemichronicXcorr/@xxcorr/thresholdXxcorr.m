function R=thresholdXxcorr(R)
%function R=thresholdXxcorr(R)
%
%thresholds the Xxcorr object with the following crition.  When a point
%is excluded due to this function the point is set to NaN.  The thresholds
%are variables that can be changed within this file.  After the
%Significance threshold is applied the following thresholds are applied to
%a copy of the correlation events so that the results of the thresholding
%can be compared.
%
%Significance Threshold
%Stdev of Phase
%Probabilty
%Phase
%Temporal Threshold




%%%%%%%%%%%    Variables   %%%%%%%%%%

%Significance Threshold
%uses precalculated thresholds at different 
%percentiles from the surrogate population .
percentileIndex = 3;

%Stdev of Phase threshold
%Ignore all correlations btween a channel 
%combination if the stdev of phase
%is greater than this threshold.  
%Units are msecs.
%set SPT to Inf to ignore.
StDevPT = 10;

%Event Probabilty Threshold
%Remove channel combinations 
%with probabilties lower than threshold.
%Values can range from 0 to 1.
%Set Threshold to zero to ignore.
EPT = 0.05;

%Phase Threshold
%All events with phases larger than 
%the abs of this value are ignored.
%Set threshold to any value greater than
%phaseSize (for instance, Inf) to disregard
%this threshold.
phaseThreshold = 10;

%Temporal Threshold
%Event must be correlated with atleast
%TEMP numbers on either side of it.
%Set TEMP=0 to disregard this threshold.
temp=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




fprintf('\nThresholding Data ...')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  Significance Threshold   %%%%%%%%%%%
%Set all values less than threshold to NaN.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nSignificance Threshold = %f%%',R.data.percentiles(percentileIndex))

%StrengthThreshold = R.data.mean + R.data.std * NumSTD;
SignifThreshold = R.data.CorrSurrogate(:,:,percentileIndex);
%mthresh = min(SignifThreshold(find(SignifThreshold)));
threshold = repmat(SignifThreshold,[1,1,size(R.data.Corr,3)]);
Rpeakk = R.data.Corr(:,:,:,1);
Rphase = R.data.Corr(:,:,:,2);
ind = find(Rpeakk<threshold);
Rpeakk(ind)=NaN;
Rphase(ind)=NaN;
R.data.Corr(:,:,:,1)= Rpeakk;
R.data.Corr(:,:,:,2)= Rphase;
R.data.ThreshVar.SignifThreshold = R.data.percentiles(percentileIndex);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate Stats on Correlations after significance test but before all
%other thresholds.
R.data.OrigStats.duration = calcDuration(R.data.Corr,0);
R.data.OrigStats.stats = calcStats(R.data.Corr);
R.data.ThreshCorr = R.data.Corr; %copy matrix before thresholding

%%%%%%%%%%%%%%%%%%%%%%%   StDev of Phase Thresh   %%%%%%%%%%%%%%%%%%%%%%
fprintf('\nStandard Deviation of Phase Threshold = %f',StDevPT)
StDevPTindex = find(R.data.OrigStats.stats.PhaseStdev>StDevPT);
%fprintf('\nRemoved %i Channel Combinations\n',length(StDevPTindex))
R.data.ThreshVar.PhaseStDevThreshold = StDevPT;


%%%%%%%%%%%%%%   Event Probabilty Threshold   %%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nEvent Probabilty Threshold = %f%%',EPT*100)
EPTindex = find(R.data.OrigStats.stats.Prob<EPT);
%fprintf('\nRemoved %i Channel Combinations\n',length(EDPTindex))
R.data.ThreshVar.ProbThreshold = EPT;

%Remove channel combinations that failed the above two thresholds.
remove = union(EPTindex , StDevPTindex);
[r,c] = ind2sub([size(R.data.Corr,1) size(R.data.Corr,2)] , remove);
for jj=1:length(r)
    R.data.ThreshCorr(r(jj),c(jj),:,:) = NaN;
end
fprintf('\nStdev of Phase and Prob thresholds removed %i Channel Combinations',length(r))


%%%%%%%%%%%%%%%%%%%%    Phase Threshold    %%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nPhase Threshold = %i',phaseThreshold)
if phaseThreshold<R.data.phaseSize
    Rpeakk = R.data.ThreshCorr(:,:,:,1);
    Rphase = R.data.ThreshCorr(:,:,:,2);
    ind = find(abs(Rphase)>phaseThreshold);
    Rpeakk(ind)=NaN;
    Rphase(ind)=NaN;
    R.data.ThreshCorr(:,:,:,1)= Rpeakk;
    R.data.ThreshCorr(:,:,:,2)= Rphase;
    R.data.ThreshVar.phaseThreshold = phaseThreshold;
else
    R.data.ThreshVar.phaseThreshold = R.data.phaseSize;
end

%%%%%%%%%%%%%%%%   Temporal Threshold   %%%%%%%%%%%%%%%%%%%%%
[R.data.ThreshStats.duration b] = calcDuration(R.data.ThreshCorr,temp);
R.data.ThreshVar.DurationMin = temp*2+1;
fprintf('\nMin Continous Temporal Threshold = %i',R.data.ThreshVar.DurationMin)
clear('Rpeakk','Rphase','ind','threshold')
if temp
    R.data.ThreshCorr=R.data.ThreshCorr+b; %b is zero where passing threshold and NaN when below threshold.
end

%%%%%%%%%%%%%%%%%   Calculate Statistics on thresholded data   %%%%%%%%%%%
R.data.ThreshStats.stats = calcStats(R.data.ThreshCorr);

%calc Grapg properties
R.data.GP = GraphParameters(R);


fprintf('\n\n')
R.data


