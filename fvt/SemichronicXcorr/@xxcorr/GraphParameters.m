function GP = GraphParameters(R)

C=R.data.ThreshCorr;



%[mn,md,sd] = DistHist(R,'ShowHist',0);
%loop over all time steps
for ii=1:size(C,3)

    %get only this time step
    Cb = C(:,:,ii,1);

    %create Binary and Undirected Adjacency Matrix
    indN = isnan(Cb);
    ind1 = find(Cb>0);
    Cb(indN)=0;
    Cb(ind1)=1;

    %Add bottom row so it is symettric matrix.
    Cb = [Cb ; zeros(1,size(Cb,2))];
    %mirror to bottom triangle also.
    CIJ = Cb + Cb';
    
    %connection density of entire page
    [kden,N,K] = density(CIJ);

    %reachability and distance matrix
    [Rc,D] = reachdist(CIJ);

    %components
    [strconn,mulcomp,ncomps,ind_comps,siz_comps] = components(CIJ,Rc);
    zz = find(siz_comps==0);
    if zz
        ncomps = ncomps-length(zz);
        siz_comps(zz)=[];
    end

GP(ii).kden = kden;
    GP(ii).ncomps = ncomps;
    GP(ii).ind_comps = ind_comps;
    GP(ii).size_comps = siz_comps;


    %for each component
    counter=0;

    for cc = 1:ncomps
            GP(ii).components(cc).distance=[];
        %calc Component Distance
        %find the distance in units for each connection and organize into
        %components  (may need to multiply by interelectrode spacing to
        %convert to mm)
        indv = ind_comps(counter+1:counter+siz_comps(cc));
        
        %loop over vertices
        for vv=1:length(indv)
            %look in C and find all 1st order connections
            indv_v = find(Cb(indv(vv),:));
            for jj = 1:length(indv_v)
                GP(ii).components(cc).distance = [GP(ii).components(cc).distance R.data.Distance(indv(vv),indv_v(jj))];
            end
        end
        GP(ii).components(cc).medianDist = median(GP(ii).components(cc).distance);
        counter = counter + siz_comps(cc);
    end
end




%how many networks exist?
%threshold small networks.
%what is the centroid of each network?
%members
%number of vertices?
%number of edges?
%diameter
%raduis?