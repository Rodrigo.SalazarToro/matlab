function obj = xxcorr(varargin)
%xxcorr Constructor function for the xxcorr class 
%used to calculate xcorr for the semichronic data
%
%
%   s = xxcorr('auto') instantiates an xcorr object.
%        
%   s is a structure with the following fields:
%
% s.data.peak
% s.data.mean
% s.data.r_surrogate
% s.data.directory
% s.data.sessionname
% s.data.channels
% s.data.totalTime
% s.data.LowPassLow
% s.data.LowPassHigh
% s.data.windowSize
% s.data.windowStep
% s.data.phaseSize
% s.data.NumRandomSamples
% s.data.percentiles
% 
% dependencies:  xcorrScript.m
%

if nargin==0
    obj = createEmptyObject;
elseif isa(varargin{1},'xxcorr')
    obj = varargin{1};
else
    obj = createObject();
end





function obj = createEmptyObject
% property of nptdata base class
holdAxis = 1;
s.data=[];
nd = nptdata(holdAxis,0);
obj = class(s,'xxcorr',nd);




function obj = createObject()
holdAxis = 0;
R=xxcorrScript;
s.data=R;
R
nd = nptdata(size(R.Corr,3),holdAxis);
obj = class(s,'xxcorr',nd);