function [mn,md,sd] = DistHist(R,varargin)

Args = struct('ShowHist',1);
Args = getOptArgs(varargin,Args,'flags',{});

%DistHist(R)
%Distance Histogram
%show histogram of the distances of all pair wise combinations.
ind = find(R.data.Distance);
figure;
d = R.data.Distance(ind)*1.2;
if Args.ShowHist
    hist(d,100)
end

mn = num2str(mean(d));
md = num2str(median(d));
sd = num2str(std(d));

title(['Pairwise Distances  --  Mean = ' mn ' Median = ' md ' Stdev = ' sd])
xlabel('distance (mm)')
ylabel('Number of Occurrences')