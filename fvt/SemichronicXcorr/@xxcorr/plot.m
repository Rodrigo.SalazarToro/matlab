function obj = plot(obj,ii,varargin)
%@xxcorr/plot Plots data from xxcorr object
%   OBJ = plot(OBJ,N,VARARGIN) plots the xxcorr object

Args = struct('Histograms',0,'Mirror',0,'StrengthNumBins',40,'Position',0,'Channel',0,'Lines',0);
Args = getOptArgs(varargin,Args,'flags',{'Histograms','Position','Mirror','Lines'});

if ii==1
    obj.data
end

pp = find(obj.data.percentiles==obj.data.ThreshVar.SignifThreshold);
CS = obj.data.CorrSurrogate(:,:,pp);
ind = find(CS);
minST = min(CS(ind));

%%%%%%%%%%%%%%%%%%%%  Position  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Args.Position
    for kk=1:5
        subplot(2,3,kk)
        %create blank semichronic image
        [ch,p] = SemichronicGridOrder;
        bl = NaN(size(p'));
        ind = find(p');

        switch kk
            case 1
                cmin = minST;
                cmax = max2(obj.data.ThreshStats.stats.SignifMean);
                %First add bottom row so it is symettric matrix.
                dd = [obj.data.ThreshStats.stats.SignifMean ; zeros(1,size(obj.data.ThreshStats.stats.SignifMean,2))];
                %grab non-zero data from the row and column ii.
                %Since it upper triangular, grab column first.
                d = [transpose(dd(1:ii-1,ii))  cmax  dd(ii,ii+1:end) ];
                ts = ['Mean Significant Correlation'];

            case 2
                dd = [obj.data.ThreshStats.stats.PhaseMean ; zeros(size(obj.data.ThreshStats.stats.PhaseMean,2))];
                d = [transpose(dd(1:ii-1,ii))  0  dd(ii,ii+1:end) ];
                v = max2(abs(obj.data.ThreshStats.stats.PhaseMean));
                cmin = -v ;
                cmax = v;
                ts = ['Mean Phase'];

            case 3
                dd = [obj.data.ThreshStats.stats.PhaseStdev ; zeros(size(obj.data.ThreshStats.stats.PhaseStdev,2))];
                d = [transpose(dd(1:ii-1,ii))  0  dd(ii,ii+1:end) ];
                cmin = 0;
                cmax = obj.data.ThreshVar.phaseThreshold;
                ts = ['Stdev of Phase'];

            case 4
                cmin = min2(obj.data.ThreshStats.duration.DurationMedian)*obj.data.windowStep ;
                cmax = 500;%max2(obj.data.ThreshStats.duration.DurationMedian)*obj.data.windowStep;
                dd = [obj.data.ThreshStats.duration.DurationMedian ; zeros(size(obj.data.ThreshStats.duration.DurationMedian,2))]*obj.data.windowStep;
                d = [transpose(dd(1:ii-1,ii))  cmax  dd(ii,ii+1:end) ];
                ts = ['Median Event Duration(msec)'];

            case 5
                ind2 = find(obj.data.ThreshStats.stats.Prob);
                cmin = 0;%cmin = min(obj.data.ThreshStats.stats.Prob(ind2)) ;
                cmax = 1;%cmax = max2(obj.data.ThreshStats.stats.Prob);
                dd = [obj.data.ThreshStats.stats.Prob ; zeros(size(obj.data.ThreshStats.stats.Prob,2))];
                d = [transpose(dd(1:ii-1,ii))  cmax  dd(ii,ii+1:end) ];
                %display zeros same as NaNs
                d(find(d==0))=NaN
                ts = ['Event Probability'];

        end

        %now put it in the correct spot on the electrode map
        bl(ind)=d;
        bl=bl';

        %add black to colormap
        cmap=colormap(jet(64));
        m = length(cmap);
        cmap = [cmap; 0 0 0];

        index = fix((bl-cmin)/(cmax-cmin)*m);
        t = isnan(bl);
        index(t) = length(cmap); % Point the index for data that are NaN's to black color.

        han_1 = imagesc(index);
        colormap(cmap); %set NaN to black
        set(han_1,'Cdatamapping','direct');

        set(gca,'xTick',[],'YTick',[])
        axis('square')
        maxTick = length(cmap)-1;
        TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
        colorbar('YTick',[1 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));

        hold on
        [iii,jjj] = ind2sub(size(p'),ind);
        scatter(iii,jjj,1,'r','filled')
        [iii,jjj] = ind2sub(size(p'),ind(ii));
        scatter(iii,jjj,25,'r','filled')
        grid on

        xlim(gca,[.5 8.5])
        ylim(gca,[.5 10.5])
        title(ts)

    end
    drawnow


    sstring = [obj.data.sessionname ' -  Channel ' num2str(ii) ];
    position = [.45 .98 .5 .01];


    %%%%%%%%%%%%%%%%%%%    Plot Histograms   %%%%%%%%%%%%%%%%%%
elseif Args.Histograms

    s=size(obj.data.ThreshCorr,2);
    u=triu(ones(s));
    l=tril(u);
    ind = find(transpose(u-l));
    [jj,ii]=ind2sub([s,s],ind(ii));

    subplot(1,3,1)
    b=linspace(minST,1,Args.StrengthNumBins);
    hist(obj.data.ThreshCorr(ii,jj,:,1),b)
    xlim(gca,[minST 1])
    tstring = ['Correlation Strengths  Mean=' num2str(obj.data.ThreshStats.stats.SignifMean(ii,jj)) '  Std=' num2str(obj.data.ThreshStats.stats.SignifStdev(ii,jj))];
    title(tstring)

    subplot(1,3,2)
    b=[-obj.data.ThreshVar.phaseThreshold : obj.data.ThreshVar.phaseThreshold];
    hist(obj.data.ThreshCorr(ii,jj,:,2),b)
    xlim(gca,[-obj.data.ThreshVar.phaseThreshold obj.data.ThreshVar.phaseThreshold])
    tstring = ['Correlation Phases  Mean=' num2str(obj.data.ThreshStats.stats.PhaseMean(ii,jj)) '  Std=' num2str(obj.data.ThreshStats.stats.PhaseStdev(ii,jj))];
    title(tstring)

    subplot(1,3,3)
    %maxMEV = max(max(obj.data.DurationMax))*obj.data.windowStep;
    v = obj.data.ThreshStats.duration.Duration(ii,jj).durations*obj.data.windowStep;
    b=[0:obj.data.windowStep:max(v)];
    if isempty(v)
        b=[0 10000];
    end
    hist(v,b)
    %xlabel('Time (msec)')
    
    ind=~isnan(obj.data.ThreshStats.duration.DurationMax);
    mDM = mean(obj.data.ThreshStats.duration.DurationMax(ind));
    %xlim(gca,[(obj.data.ThreshVar.DurationMin-1)*obj.data.windowStep  max2(obj.data.ThreshStats.duration.DurationMax)*obj.data.windowStep])
    xlim(gca,[(obj.data.ThreshVar.DurationMin-1)*obj.data.windowStep  mDM*obj.data.windowStep])
    
    tstring = ['Duration   Probability=' num2str(round((obj.data.ThreshStats.stats.Prob(ii,jj))*100)) '%' ...
        '  Median=' num2str(obj.data.ThreshStats.duration.DurationMedian(ii,jj)*obj.data.windowStep) ...
        '  95%=' num2str(obj.data.ThreshStats.duration.Duration95Percentile(ii,jj)*obj.data.windowStep)];  ...
        title(tstring)

    position = [.45 .98 .5 .01];
    sstring = [obj.data.sessionname ' - Channels ' num2str(ii) ' & '  num2str(jj) '   Distance-' num2str(obj.data.Distance(ii,jj))];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif Args.Channel  %%%% plot strength and phase vs. time with specified seed channel


    mm=obj.data.ThreshCorr(:,:,ii,1);
    nn=obj.data.ThreshCorr(:,:,ii,2);
    %create blank semichronic image
    [ch,p] = SemichronicGridOrder;
    ind = find(p');

    for cc=1:length(Args.Channel)

        for kk=1:2
            bl = NaN(size(p'));
            subplot(length(Args.Channel),2,(cc-1)*2+kk)
            switch kk
                case 1
                    dd=[ mm ; zeros(1,size(mm,2))];
                    d = [transpose(dd(1:Args.Channel(cc)-1,Args.Channel(cc)))  1  dd(Args.Channel(cc),Args.Channel(cc)+1:end) ];
                    cmin = minST;
                    cmax = 1;
                    ts = 'Correlation Significance';
                case 2
                    dd=[ nn ; zeros(1,size(nn,2))];
                    d = [transpose(dd(1:Args.Channel(cc)-1,Args.Channel(cc)))  0  dd(Args.Channel(cc),Args.Channel(cc)+1:end) ];
                    cmax = obj.data.ThreshVar.phaseThreshold;
                    cmin = -obj.data.ThreshVar.phaseThreshold;
                    ts = 'Correlation Phase';
            end

            %now put it in the correct spot on the electrode map
            bl(ind)=d;
            bl=bl';

            %add black to colormap
            cmap=colormap(jet(64));
            m = length(cmap);
            cmap = [cmap; 0 0 0];

            index = fix((bl-cmin)/(cmax-cmin)*m);
            t = isnan(bl);
            index(t) = length(cmap); % Point the index for data that are NaN's to black color.

            han_1 = imagesc(index);
            colormap(cmap); %set NaN to black
            set(han_1,'Cdatamapping','direct');

            set(gca,'xTick',[],'YTick',[])
            axis('square')
            maxTick = length(cmap)-1;
            TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
            colorbar('YTick',[1 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));

            hold on
            [iii,jjj] = ind2sub(size(p'),ind);
            scatter(iii,jjj,3,'r','filled')
            [iii,jjj] = ind2sub(size(p'),ind(Args.Channel(cc)));
            scatter(iii,jjj,30,'k','filled')
            grid on

            xlim(gca,[.5 8.5])
            ylim(gca,[.5 10.5])
            title(ts)

        end

    end
    drawnow

    timee = obj.data.windowSize/2 + (ii-1)*obj.data.windowStep;
    sstring = [obj.data.sessionname ' - ' num2str(timee) 'msec  -  Channel ' num2str(Args.Channel)];
    position = [.4 .98 .5 .01];

    %%%%%%%%%%%%%%%%   Plot Correlation Lines   %%%%%%%%%%%%%%%%%%%%%%%%%
elseif Args.Lines

    mm=obj.data.ThreshCorr(:,:,ii,1);
    nn=obj.data.ThreshCorr(:,:,ii,2);

    %create blank semichronic image
    [ch,p] = SemichronicGridOrder;
    ind = find(p');
    [rr,cc] = ind2sub(size(p'),ind);
    
    %find nonNaN values (same for mm and nn).
    indm = find(~isnan(mm));
    [e1,e2] = ind2sub(size(mm),indm); %electrode 1 and 2

    y = [cc(e1) cc(e2)]; %y position of electrodes
    x = [rr(e1) rr(e2)]; %x position of electrodes

    %colormap of plot
    cmap=colormap(jet(64));
    m = length(cmap);
    cmap = [cmap; 0 0 0];
    bl = (m+1) * ones(size(p'));        
    
    for kk=1:2
        switch kk
            case 1
                subplot(1,2,kk)
                imagesc(bl',[0 m+1])
                title('Correlation Strength')
                cmin = minST;
                cmax = 1;
                %determine line colors based on range of data
                index = fix((mm-cmin)/(cmax-cmin)*(m-1))+1;
            case 2
                subplot(1,2,kk)
                imagesc(bl',[0 m+1])
                title('Correlation Phase')
                cmin = -obj.data.ThreshVar.phaseThreshold;
                cmax = obj.data.ThreshVar.phaseThreshold;
                %determine line colors based on range of data
                index = fix((nn-cmin)/(cmax-cmin)*(m-1))+1;
        end

        colormap(cmap)
        hold on
        %place red dots on electrode positions
        scatter(rr,cc,40,[.5 .5 .5],'filled')
        %drawlines
        h=line(x',y');
        
        %set line color based on strength of correlation.
        c = cmap(index(indm),:);
        %c = mat2cell(c,ones(1,58),3)
        for hh = 1:length(h)
            set(h(hh),'Color',c(hh,:),'LineWidth',3)
        end

        %color each component
        v = obj.data.GP(ii).ind_comps;  %vertices in all components at this time step.
        for cp=1:obj.data.GP(ii).ncomps
            numV = obj.data.GP(ii).size_comps(cp); %num of vertices in this component
            cv = v(1:numV);  %vertices in the current component
            v(1:numV)=[];  %remove these from list.
            %now color this a different color.
            colr = nptDefaultColors(cp);
            scatter(rr(cv),cc(cv),40,colr,'filled')

        end


        %frame axis correctly
        set(gca,'xTick',[],'YTick',[])
        axis('square')
        colorbar
        maxTick = length(cmap)-1;
        TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
        colorbar('YTick',[0 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));

    end
    
    timee = obj.data.windowSize/2 + (ii-1)*obj.data.windowStep;
    sstring = [obj.data.sessionname ' - ' num2str(timee) 'msec' ];
    position = [.4 .98 .5 .01];

else %%%%%%%%%%%%%     Plot Matrix  %%%%%%%%%%%%%%%%%%%%%%%

    mm=obj.data.ThreshCorr(:,:,ii,1);
    nn=obj.data.ThreshCorr(:,:,ii,2);

    if Args.Mirror
        ind = find(isnan(mm));
        mm(ind)=-1000;%add mirror image
        mmi = mm';
        mmi=[mmi -1000*ones(size(mmi,1),1)];  %add last column
        mm=[mm ; -1000*ones(1,size(mm,2))];  %add last row
        mm=mm+mmi+1000;

        ind = find(isnan(nn));
        nn(ind)=-1000;
        %add mirror image
        nni = nn';
        nni=[nni -1000*ones(size(nni,1),1)];  %add last column
        nn=[nn ; -1000*ones(1,size(nn,2))];  %add last row
        nn=nn+nni+1000;
    end

    cmap=colormap(jet(64));
    m = length(cmap);
    cmap = [cmap; 0 0 0];

    subplot(1,2,1) %strength
    cmin = minST;
    cmax = 1;
    index = fix((mm-cmin)/(cmax-cmin)*m)+1;
    t = isnan(mm);
    index(t) = length(cmap); % Point the index for data that are NaN's to black color.
    %han_1 = imagesc(mm,[minn 1])
    han_1 = imagesc(index);
    colormap(cmap);
    set(han_1,'Cdatamapping','direct');
    set(gca,'TickDir','out')
    axis('square')
    maxTick = length(cmap)-1;
    TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
    colorbar('YTick',[1 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));
    title('Correlation Significance')
    % subplot(2,2,2)
    % imagesc(mm,[minn 1]);
    % set(gca,'TickDir','out')
    % axis('square')
    % colorbar
    % title('Correlation Strength')

    
    subplot(1,2,2) %phase

    cmin = -obj.data.ThreshVar.phaseThreshold; cmax = obj.data.ThreshVar.phaseThreshold;
    index = fix((nn-cmin)/(cmax-cmin)*m)+1;
    t = isnan(nn);
    index(t) = length(cmap); % Point the index for your data that are NaN's to black color.
    %imagesc(nn,[-obj.data.ThreshVar.phaseThreshold obj.data.ThreshVar.phaseThreshold])
    han_2 = imagesc(index);
    colormap(cmap);
    set(han_2,'Cdatamapping','direct');
    axis('square')
    set(gca,'TickDir','out')
    maxTick = length(cmap)-1;
    TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
    colorbar('YTick',[1 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));
    title('Correlation Phase')
    % subplot(2,2,4)
    % imagesc(nn,[-obj.data.ThreshVar.phaseThreshold obj.data.ThreshVar.phaseThreshold])
    % axis('square')
    % set(gca,'TickDir','out')
    % colorbar
    % title('Correlation Phase')

    timee = obj.data.windowSize/2 + (ii-1)*obj.data.windowStep;
    sstring = [obj.data.sessionname ' - ' num2str(timee) 'msec'];
    position = [.45 .90 .5 .01];

end

%print title window


h=findall(gcf,'Tag','xxcorr_title');
if isempty(h)
    h=annotation('textbox',position,'String',sstring,'Tag','xxcorr_title','FitHeightToText','on','LineStyle','none') ;
else
    set(h,'String',sstring)
end



