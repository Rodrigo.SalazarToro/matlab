function scatter(obj)

distanceMM = obj.data.Distance*1.22;

subplot(2,3,1)
    indD = find(distanceMM);
    plot(distanceMM(indD),obj.data.ThreshStats.stats.SignifMean(indD),'*')
    
    pp = find(obj.data.percentiles==obj.data.ThreshVar.SignifThreshold);
    CS = obj.data.CorrSurrogate(:,:,pp);
    ind = find(CS);
    minST = min(CS(ind));

    ylim(gca,[minST 1])
    ylabel('Mean correlation Significance') 
    xlabel('spatial separation (mm)')
        
    subplot(2,3,2)
    plot(distanceMM(indD),obj.data.ThreshStats.stats.PhaseMean(indD),'*')
    ylabel('Mean phase')
    xlabel('spatial separation (mm)')
    

    subplot(2,3,3)
    plot(distanceMM(indD),obj.data.ThreshStats.stats.PhaseStdev(indD),'*')
    ylabel('Stdev of phase')
    xlabel('spatial separation (mm)')
    
    subplot(2,3,4)
    plot(distanceMM(indD),obj.data.ThreshStats.duration.DurationMedian(indD)*obj.data.windowStep,'*')
    ylabel('Median event duration(msec)')
    xlabel('spatial separation (mm)')
    
    subplot(2,3,5)
    plot(distanceMM(indD),obj.data.ThreshStats.stats.Prob(indD),'*')
    ylabel('Event Probability')
    xlabel('spatial separation (mm)')

    subplot(2,3,6)
    %title('Correlation strength vs phase')
    s=obj.data.ThreshCorr(:,:,:,1);
    p=obj.data.ThreshCorr(:,:,:,2);
    ind = find(~isnan(s));
    plot(p(ind),s(ind),'*')
    xlabel('Phase')
    ylabel('Strength')
         
sstring = [obj.data.sessionname];
    position = [.45 .98 .5 .01];
        h=annotation('textbox',position,'String',sstring,'Tag','xxcorr_title','FitHeightToText','on','LineStyle','none') ;