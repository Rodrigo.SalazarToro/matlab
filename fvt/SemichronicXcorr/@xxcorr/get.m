function p = get(obj,prop_name,varargin)
%catcell/get Returns appropriate object properties
%   VALUE = GET(OBJ,PROP_NAME,VARARGIN) returns the object property 
%   specified by PROP_NAME. PROP_NAME can be one of the following:
%      'Number'

Args = struct('Histograms',0,'Position',0);

Args = getOptArgs(varargin,Args,'flags',{'Histograms','Position'});

if(Args.Histograms & strcmp(lower(prop_name),'number'))
	s=size(obj.data.Corr,2);
    p = (s^2-s)/2;
elseif (Args.Position & strcmp(lower(prop_name),'number'))
        p=size(obj.data.Corr,2);
else
	% call get function from parent class
	p = get(obj.nptdata,prop_name);
end
