function showGP(obj,varargin)
% showGP(obj,varargin)
% plots Graph Properties.
%
%Optional Input Paramters
%  'histograms' -default
%  'time'  
%  'scatter'
%  'Tapers',[BW T]  
%        where BW is the bandwidth coeffiect and T is number of tapers.
%        the default is [3 5].
%



Args = struct('Histograms',1,'Time',0,'Scatter',0,'Tapers',[3 5]);
Args = getOptArgs(varargin,Args,'flags',{'Histograms','Time','Scatter'});

if Args.Tapers(1)*2-1<Args.Tapers(2)
    error('Tapers must be less than or equal to 2*Bandwidth-1 ')
end

figure
Kden=[];
Ncomps=[];
Size_comps=[];
MDist = [];
maxMDist =[];
maxSize_comps=[];

for ii=1:size(obj.data.GP,2)

    Kden = [Kden obj.data.GP(ii).kden];
    Ncomps = [Ncomps obj.data.GP(ii).ncomps];
    new_size = obj.data.GP(ii).size_comps;
    Size_comps = [Size_comps new_size];
    [newMaxSize ind] = max(new_size);
    maxSize_comps = [maxSize_comps newMaxSize];
    newMDist=[];
    for jj=1:size(obj.data.GP(ii).components,2)
        newMDist = [newMDist obj.data.GP(ii).components(jj).medianDist];
    end
    maxMDist = [maxMDist newMDist(ind)];
    MDist = [MDist newMDist];
end

%change distance to mm
maxMDist = maxMDist*1.2;
MDist = MDist * 1.2;


if Args.Time
    
    tt = obj.data.windowSize/2/1000:obj.data.windowStep/1000:obj.data.windowSize/2/1000 + obj.data.windowStep*(size(obj.data.GP,2)-1)/1000;
    
    ax(1) = subplot(2,2,1);
    plot(tt,Kden)
    ylabel('Connection Density')
    xlabel('Time (sec)')
    axis('tight')


    ax(2) = subplot(2,2,2);
    plot(tt,Ncomps)
    ylabel('Number of Networks')
    xlabel('Time (sec)')
    axis('tight')
    
    ax(3) = subplot(2,2,3);
    plot(tt,maxSize_comps)
    ylabel('Max Network Size')
    xlabel('Time (sec)')
    axis('tight')
    
    ax(4) = subplot(2,2,4);
    plot(tt,maxMDist)
    ylabel('Median Distance of Largest Network (mm)')
    xlabel('Time (sec)')
    axis('tight')

    linkaxes(ax,'x');
    zoom on
    
    figure
%     subplot(1,3,1)
%     ezfft(tt,Kden,'freq')
%     title('Power Spectrum')
%     subplot(1,3,2)
%     PlotFFT(Kden, 1000/obj.data.windowStep)
%     xlabel('Frequency (Hz)')
%     title('FFT')
%     subplot(1,3,3)
    
    params.tapers = Args.Tapers;
    params.pad=0;
    params.Fs = 1000/obj.data.windowStep;
    params.fpass = [0 10];
    dmKden = Kden-mean(Kden);
    [S,f]=mtspectrumc(dmKden',params);
    plot(f,S)
    xlabel('Frequency (Hz)')
    ylabel('Magnitude')
    title('MultiTaper Spectrum of Connection Density')
    
elseif Args.Scatter
    subplot(2,3,1)
    scatter(Kden,Ncomps)
    xlabel('Connection Density')
    ylabel('Number of Networks')
    
    subplot(2,3,2)
    scatter(Kden,maxSize_comps)
    xlabel('Connection Density')
    ylabel('Maximum Network Size')
    
    subplot(2,3,3)
    scatter(Kden,maxMDist)
    xlabel('Connection Density')
    ylabel('Median Distance of Largest Network (mm)')
    
    subplot(2,3,4)
    scatter(Ncomps,maxSize_comps)
    xlabel('Number of Networks')
    ylabel('Maximum Network Size')
        
    subplot(2,3,5)
    scatter(Ncomps,maxMDist)
    xlabel('Number of Networks')
    ylabel('Median Distance of Largest Network (mm)')

    subplot(2,3,6)
    scatter(maxSize_comps,maxMDist)
    xlabel('Maximum Network Size')
    ylabel('Median Distance of Largest Network (mm)')
    
    
else

   
    subplot(2,2,1)
    hist(Kden,50)
    mm=median(Kden);
    title(['Connection Density - median=' num2str(mm)])

    subplot(2,2,2)
    hist(Ncomps,50)
    mm=median(Ncomps);
    title(['Number of Networks - median =' num2str(mm)])

    subplot(2,2,3)
    hist(Size_comps,50)
    mm=median(Size_comps);
    title(['Network Size - median =' num2str(mm)])

    subplot(2,2,4)
    hist(MDist,50)
    mm=median(MDist);
    title(['Median Distance of Networks (mm) - median =' num2str(mm)])


end

    zoom on