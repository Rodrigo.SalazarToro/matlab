%file variables
directory = 'C:\Documents and Settings\Baldwin\My Documents\Data\session03';
directory ='J:\data\annie semichronic\060504\session03';
sessionname = 'annie06050403';
channels = [24 32];  %sequentially ordered channel numbers
totalTime = 2;  %time in seconds

%Filter variables
LowPassLow = 10;
LowPassHigh = 100;

MonteCarloIterations = 0;%25


%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Program  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd(directory)
d = ReadUEIFile('FileName',[sessionname '.bin'],'Header');
d = ReadUEIFile('FileName',[sessionname '.bin'],'Channels',channels,'Samples',[1 d.samplingRate*totalTime]);
[data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
 clear d
 timevector = 1/samplingRate:1/samplingRate:size(data,2)/samplingRate;
d1=timevector';
d2=timevector';
d1(:,2) =data(1,:);
d2(:,2) =data(2,:);


seriesname={num2str(channels(1)) num2str(channels(2))};



% tlim=[min(d1(1,1),d2(1,1)) max(d1(end,1),d2(end,1))];
% subplot(2,1,1);
% wt(d1);
% title(['WT- ' seriesname{1}  '  WN=' num2str(p)]);
% set(gca,'xlim',tlim);
% freq=[256 128 64 32 16 8 4 2 1];
% set(gca,'ytick',log2(1./freq),'yticklabel',freq)
% ylabel('Frequency (Hz)')
% subplot(2,1,2)
% wt(d2)
% title(['WT- ' seriesname{2}  '  WN=' num2str(p)]);
% set(gca,'xlim',tlim)
% freq=[256 128 64 32 16 8 4 2 1];
% set(gca,'ytick',log2(1./freq),'yticklabel',freq)
% ylabel('Frequency (Hz)')

figure
plot(d1(:,1),d1(:,2))
hold on
plot(d2(:,1),d2(:,2),'r')

 for p=[4 6 8 10]
figure
subplot(2,1,1)

xwt(d1,d2,'ArrowSize',.1,'ArrowHeadSize',.1,'ArrowDensity',[10 10],'Param',p)
title(['XWT: ' seriesname{1} '-' seriesname{2} '  WN=' num2str(p) ] )
freq=[256 128 64 32 16 8 4 2 1];
set(gca,'ytick',log2(1./freq),'yticklabel',freq)
ylabel('Frequency (Hz)')
subplot(2,1,2)




wtc(d1,d2,'mcc',MonteCarloIterations,'ArrowSize',.1,'ArrowHeadSize',.1,'ArrowDensity',[10 10],'Param',p)
title(['WTC: ' seriesname{1} '-' seriesname{2} '  WN=' num2str(p) ] )
freq=[256 128 64 32 16 8 4 2 1];
set(gca,'ytick',log2(1./freq),'yticklabel',freq)
ylabel('Frequency (Hz)')


end

% 
% 
% 
% 
% step1 = [zeros(1,50) ones(1,200) zeros(1,150) ones(1,100) zeros(1,50) ones(1,200) zeros(1,250)];
% step2 = [zeros(1,500) ones(1,200) zeros(1,100) ones(1,100) zeros(1,100)];
% 
% t=(1/1000:1/1000:1)';
% X1=sin(t*2*pi*20 )+randn(size(t))*.1;
% X2=sin(t*2*pi*80 + pi/4)+randn(size(t))*.1;
% 
% Y1=sin(t*2*pi*20)+randn(size(t))*.1;
% Y2=sin(t*2*pi*80)+randn(size(t))*.1;
% 
% 
% X=X1.*step1'+X2.*step2';
% Y=Y1.*step1'+Y2.*step2';
% 
% X = [X ; .25*X];
% Y = [Y ; Y];
% X = X + .1* randn(size(X));
% Y = Y + .1* randn(size(X));
% t=(1/1000:1/1000:2)';
% 
% for p=[4 6 8 10]
% 
% for f=0:1
%         figure
%     
% subplot(2,1,1)
% 
% wtc([t X],[t Y],'mcc',MonteCarloIterations,'ArrowSize',.1,'ArrowHeadSize',.01,'Param',p,'Filter',f)
% freq=[256 128 64 32 16 8 4 2 1];
% set(gca,'ytick',log2(1./freq),'yticklabel',freq)
% ylabel('Frequency (Hz)')
% xlabel('Time (sec)')
% title(['WTC - WN=' num2str(p) '  Filter=' num2str(f)])
% set(gca,'Yscale','Linear')
% 
% subplot(2,1,2)
% plot(t,X)
% hold on
% plot(t,Y,'r')
% title('Combinations of 20Hz and 80Hz signals')
% colorbar
% end
% figure
% subplot(2,1,1)
% xwt([t X],[t Y],'Param',p,'ArrowSize',.1,'ArrowHeadSize',.01)
% %[Wxy,period,scale,coi,sig95]=xwt([t X],[t Y]);
% title(['XWT - WN=' num2str(p)] )
% freq=[256 128 64 32 16 8 4 2 1];
% set(gca,'ytick',log2(1./freq),'yticklabel',freq)
% ylabel('Frequency (Hz)')
% xlabel('Time (sec)')
% set(gca,'Yscale','Linear')
% 
% subplot(2,1,2)
% plot(t,X)
% hold on
% plot(t,Y,'r')
% title('Combinations of 20Hz and 80Hz signals')
% colorbar
% 
% end
