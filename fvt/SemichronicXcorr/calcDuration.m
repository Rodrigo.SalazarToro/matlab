function [d b] = calcDuration(C,thresh)
%[d b] = calcDuration(C,thresh)
%
%this function removes all short temporal events.
%an event must have atleast THRESH*2+1 number of contious points.
%The function returns a structure d containing stats of these passing
%durations and b which is a matrix of NaNs and Zeros.  All passing events
%are marked with a zero in b.


%intialize variables
b = NaN(size(C));
d.Duration = [];
d.DurationMax =          NaN([size(C,1) size(C,2)]);
d.DurationMedian =       NaN([size(C,1) size(C,2)]);
d.Duration95Percentile = NaN([size(C,1) size(C,2)]);



for ii=1:size(C,1)
    for jj=ii+1:size(C,2)
        to=squeeze(~isnan(C(ii,jj,:,1)));
        to=[to ;NaN];
        t=to;
        for tt=1:thresh
            t = t+(circshift(to,tt))+(circshift(to,-tt));
        end
        ind = find(t==thresh*2+1);
        if ~isempty(ind)
            indm = ind-thresh;
            indp = ind+thresh;

            indmm = find(diff(indm)>1);
            indpp = find(diff(indp)>1);

            indmmm = [indm(1);  indm(indmm+1)];
            indppp = [indp(indpp) ; indp(end)];

            d.DurationMax(ii,jj) = max(indppp-indmmm);

            for mm=1:length(indmmm)
                %add zeros where it passes threshold
                b(ii,jj,indmmm(mm):indppp(mm),:)=0;
            end

            %calculate median and 95% of all durations.
            bij=[NaN ;squeeze(b(ii,jj,:,1)); NaN];
            [di,dj,durations] = find(diff(find(isnan(bij)))'-1);
            %d.DurationProb(ii,jj) = sum(durations)*R.data.windowStep/1000/R.data.totalTime;
            d.Duration(ii,jj).durations = durations;
            d.DurationMedian(ii,jj) = median(durations);
            d.Duration95Percentile(ii,jj) = prctile(durations,95);

        end
    end
end