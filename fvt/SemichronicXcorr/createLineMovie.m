function M=createLineMovie(obj,sframe,eframe,filename,FPS,Q)
%M=createLineMovie(obj,sframe,eframe,filename,FPS,Q)
%inputs:
% obj is R matrix
% sframe is start frame
% eframe is end frame
% filename is avi filename, ie'example'
% FPS is Frames PerSecond
% Q is compression quality, ie. 100 is no compression
% 

 fig=figure;
p=[20 44 1200 677];
set(fig,'Position',p)
       set(fig,'DoubleBuffer','on');
       set(gca,'NextPlot','replace','Visible','off')

       
pp = find(obj.data.percentiles==obj.data.ThreshVar.SignifThreshold);
CS = obj.data.CorrSurrogate(:,:,pp);
ind = find(CS);
minST = min(CS(ind));
counter=1;

for ii=sframe:eframe
    mm=obj.data.ThreshCorr(:,:,ii,1);
    nn=obj.data.ThreshCorr(:,:,ii,2);

    %create blank semichronic image
    [ch,p] = SemichronicGridOrder;
    ind = find(p');
    [rr,cc] = ind2sub(size(p'),ind);
    
    %find nonNaN values (same for mm and nn).
    indm = find(~isnan(mm));
    [e1,e2] = ind2sub(size(mm),indm); %electrode 1 and 2

    y = [cc(e1) cc(e2)]; %y position of electrodes
    x = [rr(e1) rr(e2)]; %x position of electrodes

    %colormap of plot
    cmap=colormap(jet(64));
    m = length(cmap);
    cmap = [cmap; 0 0 0];
    bl = (m+1) * ones(size(p'));        
    
    for kk=1:2
        switch kk
            case 1
                subplot(1,2,kk)
                imagesc(bl',[0 m+1])
                title('Correlation Strength','FontName','times new roman','FontSize',16)
                cmin = minST;
                cmax = 1;
                %determine line colors based on range of data
                index = fix((mm-cmin)/(cmax-cmin)*(m-1))+1;
            case 2
                subplot(1,2,kk)
                imagesc(bl',[0 m+1])
                title('Correlation Phase','FontName','times new roman','FontSize',16)
                cmin = -obj.data.ThreshVar.phaseThreshold;
                cmax = obj.data.ThreshVar.phaseThreshold;
                %determine line colors based on range of data
                index = fix((nn-cmin)/(cmax-cmin)*(m-1))+1;
        end

        colormap(cmap)
        hold on
        %place red dots on electrode positions
        scatter(rr,cc,40,[.5 .5 .5],'filled')
        %drawlines
        h=line(x',y');
        
        %set line color based on strength of correlation.
        c = cmap(index(indm),:);
        %c = mat2cell(c,ones(1,58),3)
        for hh = 1:length(h)
            set(h(hh),'Color',c(hh,:),'LineWidth',3)
        end

        %color each component
        v = obj.data.GP(ii).ind_comps;  %vertices in all components at this time step.
        for cp=1:obj.data.GP(ii).ncomps
            numV = obj.data.GP(ii).size_comps(cp); %num of vertices in this component
            cv = v(1:numV);  %vertices in the current component
            v(1:numV)=[];  %remove these from list.
            %now color this a different color.
            colr = nptDefaultColors(cp);
            scatter(rr(cv),cc(cv),40,colr,'filled')

        end


        %frame axis correctly
        set(gca,'xTick',[],'YTick',[])
        axis('square')
        h=colorbar;
        set(h,'FontName','times new roman','FontSize',12.8)
        maxTick = length(cmap)-1;
        TickLabels  = (cmax-cmin)*[0; .25 ;.5 ;.75; 1] + cmin;
        h=colorbar('YTick',[0 .25*maxTick .5*maxTick .75*maxTick maxTick],'YTickLabel',num2str(TickLabels,3));
        set(h,'FontName','times new roman','FontSize',12.8)
    end
    
    timee = obj.data.windowSize/2 + (ii-1)*obj.data.windowStep;
 %   sstring = [obj.data.sessionname ' - ' num2str(timee) 'msec' ];
     sstring = [obj.data.sessionname(end-7:end) ' - ' num2str(timee) 'msec' ];
    position = [.4 .85 .5 .01];

    
    
h=findall(gcf,'Tag','xxcorr_title');
if isempty(h)
    h=annotation('textbox',position,'String',sstring,'Tag','xxcorr_title','FitHeightToText','on','LineStyle','none') ;
    set(h,'FontName','times new roman','FontSize',20)
else
    set(h,'String',sstring)
end


      
       	%set(h,'EraseMode','xor');
       	M(counter) = getframe(gcf);
counter=counter+1;
       end
       
        movie2avi(M,[filename '.avi'],'FPS',FPS,'Compression','Cinepak','Quality',Q);     