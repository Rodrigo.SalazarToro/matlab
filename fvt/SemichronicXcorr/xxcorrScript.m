function R = xcorrScript

%%%%%%%%%%%%  Variables    %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Data
directory = 'C:\Documents and Settings\Baldwin\My Documents\Data\session03';
directory ='J:\data\annie semichronic\060504\session03';
sessionname = 'annie06050403';
channels = [1:57];  %sequentially ordered channel numbers
totalTime = 'all';  %time in seconds or 'all' for the entire data set.
chunkTime=5;  %seconds

%Filter
LowPassLow = 10;
LowPassHigh = 100;

%Xcorr
windowSize = 150;
windowStep = 20;
phaseSize = 20;

%Surrogate Distribution
NumRandomSamples=10000;
percentiles = [95 99 99.9 99.99];

%Display
pairDisplay=1;
numBins=1000;  %num bins in histogram

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%  Program   %%%%%%%%%%%%%%%%%%%%%%%%%%
cd(directory);

dtype = DaqType([sessionname '.bin']);
if strcmp(dtype,'UEI')
    d = ReadUEIFile('FileName',[sessionname '.bin'],'Header');
else
    error('Not Semichronic Data.')
end

%reorder data in sequential order
ch=SemichronicGridOrder;
oldChannels = transpose(ch(channels));  %transpose just to print better.

%calculate number of chunks to process
if strcmp(totalTime,'all')
    filelist = nptDir([sessionname '.bin']);
    totalTime = (filelist(1).bytes-d.headerSize)/2/d.numChannels/d.samplingRate;
end
numchunks = ceil(totalTime/chunkTime);

previousChunk=[];
sstart=0;
for tt=1:numchunks %grab chunks of data

    fprintf('\n%0.5g seconds',tt*chunkTime)
    %load data
    d = ReadUEIFile('FileName',[sessionname '.bin'],'Channels',oldChannels,'Samples',[(tt-1)*chunkTime*d.samplingRate+1 tt*chunkTime*d.samplingRate]);

    [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
    data = [previousChunk data];

    %calculate number of bins
    steps = floor((length(data)-windowSize)/windowStep);
    start =  1:windowStep:steps*windowStep+1;
    endd = start+windowSize-1;
    centers = start+windowSize/2-1;
    previousChunk = data(:,start(end)+windowStep:end);

    %initialize variables
    peak=NaN(length(channels)-1,length(channels),length(start),2);
    if tt==1
        ss = size(peak);
        ss(3) = ss(3)*numchunks;
        R.Corr = [];
        R_surrogate=[];
    end

    startt = sstart+1;
    enndd = startt+steps;
    

    
    %find Xcorr for all pairwise combinations
    for ii=1:length(channels)
        fprintf('\n%0.5g     ',ii)
        for jj=ii+1:length(channels)
            fprintf(' %0.5g',jj)
            %cross correlation
            r = GrayXxcorr(data(ii,:),data(jj,:),windowSize,windowStep,phaseSize,start,endd);

            %find xcorr at random shifts
            r_surrogate = RandXxcorr(data(ii,:),data(jj,:),windowSize,round(NumRandomSamples/numchunks));

            %find closest peak to zero lag
            p=zeros(size(r,1),2);
            for kk=1:size(r,1)
                rr=r(kk,:);
                t=fpeak(1:length(rr),rr,1,[1,length(rr),0,inf]);
                phase=t(:,1)-phaseSize-1;
                [y,ind]=min(abs(phase));
                if isempty(t) %all negative correlations
                    p(kk,1)=0;
                    p(kk,2)=0;
                elseif y==phaseSize %max correlation is at max phase
                    p(kk,1)=0;
                    p(kk,2)=0;
                else
                    p(kk,1) = t(ind,2);
                    p(kk,2) = phase(ind);
                end
            end

            if pairDisplay

                %**********
                figure
                surf(r)
                xlabel('Phases')
                ylabel('Bins')
                zlabel('correlation coeffecients')
                title(['Correlation coeffecients Channels ' num2str(channels(ii)) ' ' num2str(channels(jj))])
                hold on
                plot3(p(:,2)+phaseSize+1,[1:size(p,1)],p(:,1),'*')

                %**********
                figure
                %bar(x,n)
                hist(r_surrogate,numBins)
                title(['Histogram of Correlation Coeffecients of Random Combinations.'])%  Mean = ' num2str(Rmean) ' Stdev = ' num2str(Rstd)])

                %************
                figure
                ax(1) = subplot(2,1,1);
                maxd = max(abs([data(ii,:)  data(jj,:)]));
                plot(data(ii,:)/maxd,'b')
                hold on
                plot(data(jj,:)/maxd,'r')

                plot(centers,p(:,1),'k','LineWidth',2)
                %            line([0 centers(end)],[Rmean+NumSTD*Rstd Rmean+NumSTD*Rstd]);
                ylabel('Correlation')
                title(['Max Correlation per Correlogram for ' sessionname ' Channels ' num2str(channels(ii)) ' ' num2str(channels(jj)) ])
                ax(2) = subplot(2,1,2);
                plot(centers,p(:,2))

                ylim([-phaseSize-1 phaseSize+1])
                ylabel('Phase Shift of Max correlation')
                xlabel('time(msec)')
                linkaxes(ax,'x')
                zoom on
                pause
            end%display


           % peak(ii,jj,:,:) = p;
            %R.mean(ii,jj)= Rmean;
            %R.std(ii,jj)= Rstd;
            RSurrogate(ii,jj,:) = r_surrogate;

%             size(p);
%             p;
            %size(p) = [91 2]
            %column 1 is strength
            %column 2 is phase

            R.Corr(ii,jj,startt:enndd,:) = p;

        end%ii
    end%jj
        R_surrogate = cat(3,R_surrogate,RSurrogate);
        RSurrogate=[];
        sstart = enndd;    
    %whos
end %tt


R.directory = directory;
R.sessionname = sessionname;
R.channels = channels;
R.totalTime = totalTime;

R.LowPassLow = LowPassLow;
R.LowPassHigh = LowPassHigh;

R.windowSize = windowSize;
R.windowStep = windowStep;
R.phaseSize = phaseSize;

%just save some stats on Rsurrogate
for pp=1:length(percentiles)
    %R.r_surrogate(:,:,pp) = prctile(R_surrogate,percentiles(pp),3);
    %above statement is too memory intensive so we must loop
    for ii=1:size(R_surrogate,1)
        for jj=1:size(R_surrogate,2)
            R.CorrSurrogate(ii,jj,pp) = prctile(squeeze(R_surrogate(ii,jj,:)),percentiles(pp));
        end
    end
end
R.NumSamples = size(R_surrogate,3);
R.percentiles = percentiles;
%whos

for ii=1:size(R.Corr,1)
    for jj=ii+1:size(R.Corr,2)
        %calculate all pairwise distances
        [ch,p] = SemichronicGridOrder;
        [i1,j1] = find(p==ch(ii));
        [i2,j2] = find(p==ch(jj));
        R.Distance(ii,jj) = sqrt((i1-i2)^2 + (j1-j2)^2);
    end
end

