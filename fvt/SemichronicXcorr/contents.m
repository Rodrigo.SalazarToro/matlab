% Xxcorr toolbox
% 
% this toolbox calculates cross correlations in windows between all channel
% combinations of the semichronic data.
% 
% xxcorr creates an object.  It reads the raw data, filters it and then
% calculates windowed correlation coeffecient distributions.  All of the 
% variables to create the xcorr object can be changed in the top of xcorrScript.m
% R=xxcorr('auto');
% 
% thresholdXxcorr applies strength and temporal thresholds to the object.  The variables 
% for this program are at the top of the thresholdXxcorr.m file.  The BCT
% toolbox must be added to the matlab path also.
% R=thresholdXxcorr(R)
%     
% InspectGUI displays this object after it has been thresholded.  The default 
% is to display the object in matrix 
% form but several different options can also be used. The variables for this
% program are passed in on the command line.  Here are some examples:
% InspectGUI(R)
% InspectGUI(R,'Histograms')
% InspectGUI(R,'Channels',[32 48 56])
% InspectGUI(R,'Position')
% InspectGUI(R,'Lines')
% 
%Also it is possible to pass the 'PlayMovie' option into 
%InspectGui.  After the first plot comes up the movie is started by hitting
%the 'Next' button.  For example:
%InspectGUI(R,'Lines','PlayMovie')
%
%some other display routines are:
% showGP(R) - shows histograph of Graph Properties.
% scatter(R) - channel combination plots
% DistHist(R) - plots a histogram of a pairwise distances.

