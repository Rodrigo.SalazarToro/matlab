function r = GrayXcorr(x,y,windowSize,windowStep,phaseSize,start,endd)



r=NaN(length(start),phaseSize*2+1);
for ii=1:length(start)
    data1 = x(start(ii):endd(ii));
    data2 = y(start(ii):endd(ii));
    %r = [r (dot(data1,data2))/sqrt(dot(data1,data1)*dot(data2,data2))];
    r(ii,:) = xcorr(data1,data2,phaseSize,'coeff');
   
end
    
    