function newR = response2field(revcorr_sn,init_info)

%get spatial response field for each color and orientation
% revcorr dimensions
R=revcorr_sn.R;
[M N]=size(R);
R=R(1:M-1,:);   % get rid of last row, which is outside box
M=M-1;

% get init_info variables
rows=init_info.rows;
cols=init_info.cols;
nrc=rows*cols;
num_or=init_info.num_orientations;
angle=init_info.bar_orientation;
num_colors=init_info.num_colors;
pixel_cols = init_info.grid_x_size;
pixel_rows = init_info.grid_y_size;
bar_length = init_info.bar_length;
bar_width = init_info.bar_width;
grid_row_space = floor(pixel_rows/rows);
grid_col_space = floor(pixel_cols/cols);



for c=0:num_colors-1		%loop over color groups
   for k=0:num_or-1		%loop over orientation groups
      
      group = c*num_or+k;
      oindex = group*nrc + [1:nrc];
      
      for t=1:N	%loop over times or columns
         r=reshape(R(oindex,t),rows,cols);		%grab all positions for this color and orientation
         
         %so now we have a grid with the probabilities of a spike occuring
         %at each time during the window.
         %We now want to deconvolve this probabilty (response map) grid with the
         %image itself.
         
         %expand probability matrix from grid size to pixel size
         r = expand_matrix(r,grid_row_space,grid_col_space);
         
         %create summmation matrix as all zeros
         sum = zeros(size(r));
         
         %grab probabilities at bar locations and add to summation matrix
         %to create new receptive field map
         for i=1:rows
            for j=1:cols
               cindex = [(j-1)*grid_row_space+floor((grid_row_space/2)-(bar_width/2))+1:(j-1)*grid_row_space+floor((grid_row_space/2)-(bar_width/2))+bar_width];
               rindex = [(i-1)*grid_col_space+floor((grid_col_space/2)-(bar_length/2))+1:(i-1)*grid_col_space+floor((grid_col_space/2)-(bar_length/2))+bar_length];
               %get rid of bar outside of grid
               if ~isempty(find(rindex<1))
                  rindex = rindex(max(find(rindex<1))+1:length(rindex));
               elseif ~isempty(find(rindex>pixel_rows))
                  rindex = rindex(1:min(find(rindex>pixel_rows))-1);
               end
               
               if ~isempty(find(cindex<1))
                  cindex = cindex(max(find(cindex<1))+1:length(cindex));
               elseif ~isempty(find(cindex>pixel_cols))
                  cindex = cindex(1:min(find(cindex>pixel_cols))-1);
               end
               bar=r(rindex,cindex);
               sum(rindex,cindex) = sum(rindex,cindex) + bar;
            end
         end
         
         %average new probabilities over pixel locations to grid locations
         grid = collapse_matrix(sum,grid_row_space,grid_col_space);
         
         %put new receptive field map back into R matrix formend
         r=reshape(grid,nrc,1);
         newR(oindex,t)=r;
      end
   end
end
%add last row of zeros to newR
newR=[newR ; zeros(1,N)];