function ecc = Eccentricity(eyepos)
%[ecc, smallest, largest] = Eccentricity(eyepos)
%ecc contains the eccentricity of each eyepos from the average
%smallest contains the vertical and horizontal smallest values per trial
%largest contains the vertical and horizontal largest values per trial
%used with revcorr

%loop over trials (pages)
%need to get an average value first.
%This average value does not have to be really precise 
%but the relative deviation from this mean (eccentricity) is critical.
avg =[];
for jj=1:length(eyepos)
    trialeye = eyepos(jj).trial;
    nan = isnan(trialeye(1,:));
    if sum(nan)>0
        trialeye(:,find(nan))=[];
    end
    avg = [avg   mean(trialeye,2)];
end

if ~isempty(avg)
    sessionMean = mean(avg,2);
end


smallest=[];
largest=[];
%now find the eccentricity to this mean and the x&y range.
ecc=[];
for jj=1:length(eyepos)
    ecc(jj).trial = eyepos(jj).trial-repmat(sessionMean,[1 length(eyepos(jj).trial)]);
    ecc(jj).smallest = min(ecc(jj).trial,[],2);
    ecc(jj).largest = max(ecc(jj).trial,[],2);
    
end

