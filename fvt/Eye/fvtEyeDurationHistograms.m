function duration_histograms = fvtEyeDurationHistograms(eye)
%function duration_histograms = fvtEyeDurationHistograms(eye)
%This function calculates histograms for fixation and saccade durations for a session.
%All values are in milliseconds.
%InspectEyeDurationHistogram can be used to view the histograms
%
%eye is an object of the eyemovements class containing the start and end times for the different eye
%movement events and is created by eyemovements
%

duration_histograms.sessionname = eye.data.sessionname;

fixation_duration=[];
saccade_duration=[];
%drift_duration=[];


%Fixations
for i=1:length(eye.data.fixation)	%loop over trials
   for j=1:length(eye.data.fixation(i).start)	%loop over events
      duration=eye.data.fixation(i).finish(j) - eye.data.fixation(i).start(j);
      fixation_duration = [fixation_duration duration];
   end
end
duration_histograms.fixation=fixation_duration;



%Saccades
for i=1:length(eye.data.saccade)
   for j=1:length(eye.data.saccade(i).start)
      duration = eye.data.saccade(i).finish(j) - eye.data.saccade(i).start(j);
      saccade_duration = [saccade_duration duration];
   end
end
duration_histograms.saccade=saccade_duration;


% %Drifts
% for i=1:length(eye.data.drift)
%    for j=1:length(eye.data.drift(i).start)
%       duration=eye.data.drift(i).finish(j) - eye.data.drift(i).start(j);
%       drift_duration = [drift_duration duration];
%    end
% end
% duration_histograms.drift=drift_duration; 
% 
% %Nonsaccadic Events
% nonsaccade_duration = [drift_duration fixation_duration];
% duration_histograms.nonsaccade=nonsaccade_duration;