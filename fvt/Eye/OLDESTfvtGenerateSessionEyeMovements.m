function [eye,status] = fvtGenerateSessionEyeMovements(display_flag)
%
%[eye,status] = fvtGenerateSessionEyeMovements(display_flag)
%This function determines which regions of an eye signal correspond to 
%fixations, saccades or slow drifts.
%This function determines the regions for all trials in a session and then 
%creates a structure containing the start and finish times in (milliseconds)
%for each of the events.  The structure is eye(trial).fixation(event#).start
%There are similiar fields for saccade, drift and start, finish.
%
%If display_flag is 1 then the results for each trial will be plotted along with the 
%velocities and frequency spectrum.  If no parameters are entered then display_flag = 0 
%and the entire session is calculated and the eye structure is returned.
%
%status is returned as a -1 if there is an error
%Assumptions:
%The pwd must be the eye folder.

warning off MATLAB:divideByZero
axisgain = 200;

%%%%%%%%%%% MAGIC NUMBERS %%%%%%%%%%%%
overshoot_period = 19;	 	%overshoots must be less than this number of points(minimum length of fixation or drift)
lazy_saccade = 210;			%ignore saccades longer than this
buffer = 15;				%cutoff buffer from questionable section to perform analysis
minimum_length = 70;		%minimum length to apply beyond buffer to otherwise keep entire section.
window_size = 40;			%a window is moved over the section and average velocities are calculated
avg_vel_thresh = 0.0015;	%velocity threshold in degrees per millisecond
num_points_thresh = 20;		%if this many points within a section exceed fixation threshold 
saccade_threshold = 4;		%velocity must be greater than x standard deviations above noise
fix_threshold = 1;		    %velocity must be lower than this to classify as a fixation

%A significant displacement is linearly proportional to the length of the event
%by the following equation:  cutoff = slope * length + y_intercept
slope = 0.0003;%.0004					%degrees per millisecond
y_intercept = 0.08;			            %degrees
r_sac = [];
r_fix = [];
r_drift = [];
amp_sac = [];
sac_vel = [];
sac_dur = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
status = 1;
if nargin == 0
    display_flag = 0;
end

if display_flag
    %close all
    h1 = figure;
    set(h1,'Position', [1 29 1280 928]);
    zoom on
    h2 = figure;
    set(h2,'Position', [1 29 1280 928]);
    zoom on
    %h3 = figure;
    %set(h3,'Position', [1 29 1280 928]);
    %zoom on
    h4 = figure;
end

dirlist = nptDir('*_eye.0*');
numTrials = size(dirlist, 1);
trial = 1;

eye.sessionname = dirlist(1).name(1:length(dirlist(1).name) - 9);
cd ..
[iniInfo, status] = ReadRevCorrIni([eye.sessionname, '.ini']);
cd eye

while trial <= numTrials
    if display_flag
        fprintf('\nTrial Number %i\n', trial);
    else
        fprintf('%i ', trial);
    end
    
    %read data
    filename = dirlist(trial).name;
    [data, num_channels, sampling_rate, datatype, points] = nptReadDataFile(filename);
    
    %change data from pixels to degrees
    [data(1,:) data(2,:)] = pixel2degree(data(1,:), data(2,:));
    
    if display_flag
        %plot unclassified signals
        figure(h2)
        hold off
        plot(data(1,:), 'k')
        hold on
        plot(data(2,:), 'b')
    end
    
    %NaN strategy
    %if this trial contains NaNs then  
    %replace NaNs with surronding values and mark NaN positions.
    %Run through ordinary algorithm
    %then replace the NaN fake fixations and surrounding saccades 
    %with zeros in the true-false(tf) matrices.  
    
    NaNflag = 0;
    if sum(isnan(data), 2) > 0			%if this trial contains NAN's
        NaNflag = 1;
        n = isnan(data);
        [i,j] = find(n);
        if size(i,1) == (size(data, 2) * 2)	%then entire trial is NANs
            NaNstart = 1;
            NaNstop = size(data, 2) - 1;
            data(size(data, 2)) = 0;
        else
            j = j(1:2:length(j))';
            %how many NaN sections are there??
            j1 = [0 j(1:length(j) - 1)];	%shift j
            NaNstart = j(find((j - j1) > 1));
            j1 = [j(2:length(j)) 0];
            NaNstop = [j(find((j1 - j) > 1)) j(length(j))];
            if length(NaNstart) < length(NaNstop)
                NaNstart = [1 NaNstart];
            end
        end
        numNaN = length(NaNstart);
        
        for i = 1:numNaN
            if NaNstart(i) == 1
                data(1, NaNstart(i):NaNstop(i)) = data(1, NaNstop(i) + 1);		%incase NaNs start trial		
                data(2, NaNstart(i):NaNstop(i)) = data(2, NaNstop(i) + 1);				
            else
                data(1, NaNstart(i):NaNstop(i)) = data(1, NaNstart(i) - 1);		%or end trial
                data(2, NaNstart(i):NaNstop(i)) = data(2, NaNstart(i) - 1);
            end
        end
    end
    
    %plot frequency spectrum
    %if display_flag
    %   figure(h3)
    %   plotFFT(data(1,:), sampling_rate)
    %end
    
    %FIR filter data 
    order = 6;
    b = ones(1, order)/order;
    filtered = filtfilt(b, 1, data');	%12th order running average(boxcar) with no delay
    
    %calculate position derivative (velocity->degrees per sec)
    vely = gradient(filtered(:,1), 1);
    velx = gradient(filtered(:,2), 1);
    vel = [abs(vely)  abs(velx)];
    
    %smooth velocity signal
    order = 15;	
    b = ones(1, order)/order;
    vel = filtfilt(b, 1, vel);
    
    %get noise parameters and saccade thresholds from velocities
    [data_mean, stddev] = nptThreshold(vel);
    saccade_thresh = data_mean + saccade_threshold * stddev;
    fix_thresh = data_mean + fix_threshold * stddev;
    
    if display_flag
        figure(h1)
        hold off
        plot(vel(:,1), 'g.-')
        hold on
        plot(vel(:,2), 'b.-')
        lh1 = line([length(data), 1], [saccade_thresh(1), saccade_thresh(1)]);
        lh2 = line([length(data), 1], [saccade_thresh(2), saccade_thresh(2)]);
        lh3 = line([length(data), 1], [fix_thresh(1), fix_thresh(1)]);
        lh4 = line([length(data), 1], [fix_thresh(2), fix_thresh(2)]);
        set(lh1, 'Color', 'g');
        set(lh2, 'Color', 'b');
        set(lh3, 'Color', 'g');
        set(lh4, 'Color', 'b');
        
        legend('vertical velocity (pixel/msec)', 'horizontal velocity (pixel/msec)', 'vertical threshold @ 4sigma+mean', 'horizontal threshold @ 4sigma+mean', 'vert threshold @ sigma+mean', 'horiz threshold @ sigma+mean') 
        clear clip_data;
    end
    
    
    %%%%%%%%%%%%%%%%% 	Recognition Rules 	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %  -  ignore all events at the start and end of a trial
    %  -  ignore lazy saccades lasting more than 210 ms (lazy_saccade value from above)
    %  -  saccades are regions with large velocities, but longer than 3 datapoints in length
    %  -  slowdrifts are defined by average velocities greater than fixations but smaller than 
    %		saccades or significant displacements.
    %  -  if the data has NANs then ignore the section and both saccades on either side
    %  -  ignore noise spikes in data
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %Define TrueFalse matrices for saccades, fixations, and slowdrifts.
    %Saccades are easily detected because of large velocities.
    saccadetf = (vel(:,1) > saccade_thresh(1))  |  (vel(:,2) > saccade_thresh(2));
    
    %... but fixations and slowdrifts are much harder to distinguish
    fixationtf = zeros(size(saccadetf));
    slowdrifttf = zeros(size(saccadetf));
    
    %create marker to define sections of saccades and non-saccades(fixation or slowdrift)
    marker = [1];
    flag = 0; %start is a fixation
    for i = 1:length(saccadetf)
        if flag == 0 & saccadetf(i) == 1
            marker = [marker i];
            flag = 1;
        elseif flag == 1 & saccadetf(i)==0 & i ~= length(saccadetf)
            marker = [marker (i - 1)];
            flag=0;
        elseif flag == 0 & i == length(saccadetf)
            marker = [marker i];
        end
    end
    if saccadetf(1) == 1	%in case start of trial is a saccade
        marker = marker(3:length(marker));
    end
    
    %if saccades have overshoot keep the overshoot as saccade
    %change = [];
    %for i = 3:2:length(marker) - 1
    %   if (marker(i + 1) - marker(i)) < overshoot_period	         
    %      saccadetf(marker(i):marker(i + 1)) = ones(1, marker(i + 1) - marker(i) + 1);
    %      change = [change i];
    %   end
    %end
    %for i = length(change):-1:1
    %   marker = [marker(1:change(i) - 1) marker(change(i) + 2:length(marker))];
    %end
    
    %ignore saccades less than 3 points
    change = [];
    for i = 2:2:length(marker) - 1
        if (marker(i + 1) - marker(i)) < 3
            saccadetf(marker(i):marker(i + 1)) = zeros(1, marker(i + 1) - marker(i) + 1);
            change = [change i];
        end
    end
    for i = length(change):-1:1
        marker = [marker(1:change(i) - 1) marker(change(i) + 2:length(marker))];
    end
    
    %redefine saccade starting and ending points based on the 
    %intersection of a linear fits to the saccade and to the surrounding events.
    buf = 6;
    for i = 2:2:length(marker) - 2
        %  if (marker(i+1)-marker(i))>(2*buf)  &  (marker(i+2)-marker(i+1))>(2*buf) &  (marker(i)-marker(i-1))>(2*buf)
        
        %find saccade amplitude
        ampv = abs(filtered(marker(i), 1) - filtered(marker(i + 1), 1));
        amph = abs(filtered(marker(i), 2) - filtered(marker(i + 1), 2));
        
        if ampv < 1 & amph < 1      %if small saccade
            %find max velocity during saccade
            [junk, sac_max_v] = max(vel(marker(i):marker(i + 1), 1));
            [junk, sac_max_h] = max(vel(marker(i):marker(i + 1), 2));
            sac_max_v = sac_max_v + marker(i) - 1;
            sac_max_h = sac_max_h + marker(i) - 1;
            %adjusting maxes if they are too close to the beginning or end of the trial
            if sac_max_v == 2
                sac_max_v = 3;
            end
            if sac_max_h == 2
                sac_max_h = 3;
            end
            if sac_max_v == marker(end) - 1
                sac_max_v = marker(end) - 2;
            end
            if sac_max_v == marker(end) - 1
                sac_max_v = marker(end) - 2;
            end
            saccade_section_v = filtered(sac_max_v-2:sac_max_v+2,1)';
            saccade_section_h = filtered(sac_max_h-2:sac_max_h+2,2)';
            saccade_v = regress(saccade_section_v',[ones(length(saccade_section_v),1) [sac_max_v-2:sac_max_v+2]']);
            saccade_h = regress(saccade_section_h',[ones(length(saccade_section_h),1) [sac_max_h-2:sac_max_h+2]']);
        else
            saccade_section_v = filtered((marker(i)+5):(marker(i+1)-5),1)';
            saccade_section_h = filtered((marker(i)+5):(marker(i+1)-5),2)';
            saccade_v = regress(saccade_section_v',[ones(length(saccade_section_v),1) [marker(i)+5:marker(i+1)-5]']);
            saccade_h = regress(saccade_section_h',[ones(length(saccade_section_h),1) [marker(i)+5:marker(i+1)-5]']);
        end
        
        %ignore buffer in each event
        %event to the left and right
        left_section_v = filtered((marker(i-1)+buf):(marker(i)-buf),1)';
        left_section_h = filtered((marker(i-1)+buf):(marker(i)-buf),2)';
        right_section_v = filtered((marker(i+1)+buf):(marker(i+2)-buf),1)';
        right_section_h = filtered((marker(i+1)+buf):(marker(i+2)-buf),2)';
        
        %find slopes and y-intercept using linear regression on both channels 
        %output of regress is [yint;slope]
        [left_v,bint,r,rint,stats] = regress(left_section_v',[ones(length(left_section_v),1) [marker(i-1)+buf:marker(i)-buf]']);
        [right_v,bint,r,rint,stats] = regress(right_section_v',[ones(length(right_section_v),1) [marker(i+1)+buf:marker(i+2)-buf]']);
        [left_h,bint,r,rint,stats] = regress(left_section_h',[ones(length(left_section_h),1) [marker(i-1)+buf:marker(i)-buf]']);
        [right_h,bint,r,rint,stats] = regress(right_section_h',[ones(length(right_section_h),1) [marker(i+1)+buf:marker(i+2)-buf]']);
        
        
        %find the intersection
        left_y_v =(left_v(2)*saccade_v(1) - saccade_v(2)*left_v(1))/(left_v(2)-saccade_v(2));
        left_x_v = round((left_y_v-saccade_v(1))/saccade_v(2));
        right_y_v =(right_v(2)*saccade_v(1) - saccade_v(2)*right_v(1))/(right_v(2)-saccade_v(2));
        right_x_v = round((right_y_v-saccade_v(1))/saccade_v(2));
        left_y_h =(left_h(2)*saccade_h(1) - saccade_h(2)*left_h(1))/(left_h(2)-saccade_h(2));
        left_x_h = round((left_y_h-saccade_h(1))/saccade_h(2));
        right_y_h =(right_h(2)*saccade_h(1) - saccade_h(2)*right_h(1))/(right_h(2)-saccade_h(2));
        right_x_h = round((right_y_h-saccade_h(1))/saccade_h(2));
        
        %how should these intersection points define the boundary?
        %lets make the closest data point the boundary
        %so we will take data points on either side 
        %and find the data point with a minimum distance to the intersection point.
        
        %only use this criteria on the channel that saccades (sometimes only one channel will saccade)
        ampv = abs(filtered(marker(i),1)-filtered(marker(i+1),1));
        amph = abs(filtered(marker(i),2)-filtered(marker(i+1),2));
        ampq=ampv/amph;
        buf2=30;
        if ampq<2 & (1/ampq)<2		%if saccaded on both channels
            %need to check if intersection point is near the ends of the trial
            if  right_x_v>buf2 & right_x_h>buf2 & (length(filtered)-left_x_v)>buf2 & (length(filtered)-left_x_h)>buf2 ...
                    & left_x_v>buf2 & left_x_h>buf2 & (length(filtered)-right_x_v)>buf2 & (length(filtered)-right_x_h)>buf2
                
                %use 10 data points on each side.
                [v,ind_l_v] = min((axisgain*(filtered(left_x_v-buf2:left_x_v+buf2,1)-left_y_v)).^2 + ([-buf2:buf2].^2)');
                [v,ind_r_v] = min((axisgain*(filtered(right_x_v-buf2:right_x_v+buf2,1)-right_y_v)).^2 + ([-buf2:buf2].^2)');
                [v,ind_l_h] = min((axisgain*(filtered(left_x_h-buf2:left_x_h+buf2,2)-left_y_h)).^2 + ([-buf2:buf2].^2)');
                [v,ind_r_h] = min((axisgain*(filtered(right_x_h-buf2:right_x_h+buf2,2)-right_y_h)).^2 + ([-buf2:buf2].^2)');
                if display_flag
                    figure(h2)
                    plot(left_x_v,left_y_v,'k*')
                    plot(right_x_v,right_y_v,'k*')
                    plot(left_x_h,left_y_h,'k*')
                    plot(right_x_h,right_y_h,'k*')
                end
                newl=	min([(left_x_v + ind_l_v-buf2-1),(left_x_h + ind_l_h-buf2-1)]);
                if newl>marker(i-1)  & newl<marker(i+1)     %make sure this is not less than previous marker point
                    marker(i) = newl;
                end
                newr=max([(right_x_v + ind_r_v-buf2-1),(right_x_h + ind_r_h-buf2-1)]);
                if (length(marker)-i)>1
                    rlimit=marker(i+2);
                else
                    rlimit=length(filtered);
                end
                if newr>marker(i) & newr<rlimit      %make sure this is not less than previous marker point
                    marker(i+1) = newr;
                end
                
            end %if not near ends of trial
            
            %else use channel with largest saccade
        elseif ampv>amph	%vert is bigger
            %need to check if saccade is near the ends of the trial
            if  right_x_v>buf2 & (length(filtered)-left_x_v)>buf2  ...
                    & left_x_v>buf2 & (length(filtered)-right_x_v)>buf2 
                
                %use 10 data points on each side.
                [v,ind_l_v] = min((axisgain*(filtered(left_x_v-buf2:left_x_v+buf2,1)-left_y_v)).^2 + ([-buf2:buf2].^2)');
                [v,ind_r_v] = min((axisgain*(filtered(right_x_v-buf2:right_x_v+buf2,1)-right_y_v)).^2 + ([-buf2:buf2].^2)');
                newl=	left_x_v + ind_l_v-buf2-1;
                if newl>marker(i-1)  & newl<marker(i+1)     %make sure this is not less than previous marker point
                    marker(i) = newl;
                end
                newr=right_x_v + ind_r_v-buf2-1;
                if (length(marker)-i)>1
                    rlimit=marker(i+2);
                else
                    rlimit=length(filtered);
                end
                if newr>marker(i) & newr<rlimit      %make sure this is not less than previous marker point
                    marker(i+1) = newr;
                end
                if display_flag
                    figure(h2)
                    plot(left_x_v,left_y_v,'k*')
                    plot(right_x_v,right_y_v,'k*')
                end
            end %if not near ends of trial
            
        else %horiz is bigger  
            %need to check if saccade is near the ends of the trial
            if right_x_h>buf2 & (length(filtered)-left_x_h)>buf2 ...
                    & left_x_h>buf2 & (length(filtered)-right_x_h)>buf2
                
                %use 10 data points on each side.
                [v,ind_l_h] = min((axisgain*(filtered(left_x_h-buf2:left_x_h+buf2,2)-left_y_h)).^2 + ([-buf2:buf2].^2)');
                [v,ind_r_h] = min((axisgain*(filtered(right_x_h-buf2:right_x_h+buf2,2)-right_y_h)).^2 + ([-buf2:buf2].^2)');
                newl =	left_x_h + ind_l_h-buf2-1;
                if newl>marker(i-1)  & newl<marker(i+1)     %make sure this is not less than previous marker point
                    marker(i) = newl;
                end
                newr = right_x_h + ind_r_h-buf2-1;
                if (length(marker)-i)>1
                    rlimit=marker(i+2);
                else
                    rlimit=length(filtered);
                end
                if newr>marker(i) & newr<rlimit      %make sure this is not less than previous marker point
                    marker(i+1) = newr;
                end
                if display_flag
                    figure(h2)
                    plot(left_x_h,left_y_h,'k*')
                    plot(right_x_h,right_y_h,'k*')
                end
            end %if not near ends of trial
            
        end	%which saccade is biggest
        %end	%if saccades are longer than buffer
    end  %for length of marker 
    
    
    %now fix saccadetf to correspond to the new markers
    saccadetf = zeros(size(fixationtf));
    for i=2:2:length(marker)-1
        saccadetf(marker(i):marker(i+1))=1;
    end
    
    %ignore saccades that last more than lazy saccade)
    for i=2:2:length(marker)-1
        if (marker(i+1)-marker(i))> lazy_saccade
            saccadetf(marker(i):marker(i+1))=zeros(1,marker(i+1)-marker(i)+1);
        end
    end
    
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%Decide whether a fixation or slow drift%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %I would like to redo this solve using a clustering algorithm
    %with measurements of:
    %	average velocity
    %  max velocity
    %	absolute displacement
    %but for now this works.
    
    for i=1:2:length(marker)-1
        
                         %first throw away all sections will velocity
                         %over saccade threshold
                         %vert_index = find(vel(marker(i):marker(i+1),1)<saccade_thresh(1));
                         %horiz_index = find(vel(marker(i):marker(i+1),2)<saccade_thresh(2));
                         %if isempty(vert_index) | isempty(horiz_index)
            avg_vel_mat = [];
            tooshort_flag = 0;
            %find ends of questionable section
            slowdrift_flag=0;
            
            vert_index = find(vel(marker(i):marker(i+1),1)<fix_thresh(1));
            horiz_index = find(vel(marker(i):marker(i+1),2)<fix_thresh(2));
            if (isempty(vert_index) | isempty(horiz_index))
                slowdrift_flag=1;
                slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                if display_flag
                    fprintf('section %i triggered on never less than fix threshold\n',round(i/2));
                end
                
            else %find part of questionable section after it has slowed down 
                start = max([vert_index(1) horiz_index(1)]);    %1st velocity less than fix threshold
                finish = min([vert_index(length(vert_index)) horiz_index(length(horiz_index))]);    %last velocity less than fix threshold
                
                if (finish-start)>minimum_length		%cutoff ends of questionable section (only keep middle)
                    start = start + buffer;
                    finish = finish - buffer;
                end
                section = filtered((marker(i)+start-1):(marker(i)+finish-1),:);
                vel_section = vel((marker(i)+start-1):(marker(i)+finish-1),:);
                if length(section)<window_size
                    tooshort_flag=1;
                end
            end
            
            if slowdrift_flag==0 & tooshort_flag==0
                %check if average velocity across a window is large enough to be a slowdrift
                velocity_record=[];
                for j=1:length(section)-window_size 
                    if slowdrift_flag == 0
                        window = section(j:j+window_size,:);
                        avg_velocity = abs((window(1,:)-window(window_size,:))/window_size);
                        if max(avg_velocity) > avg_vel_thresh
                            slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                            slowdrift_flag = 1;
                            if display_flag
                                fprintf('section %i triggered on average velocity of %d @ %i VELOCITY WINDOW\n',round(i/2), max(avg_velocity),j)
                            end
                        else
                            velocity_record = [velocity_record max(avg_velocity)];
                        end
                    end
                end
                if slowdrift_flag == 0
                    if display_flag
                        fprintf('section %i did not trigger with average velocity of %d VELOCITY WINDOW\n',round(i/2), max(velocity_record))
                    end
                end
                
                cutoff=slope * (length(section)) + y_intercept;
                max_displacement = max(max(section)-min(section));
                
                %check if section has 20 points above fixation threshold on a single channel to be a slowdrift
                if slowdrift_flag == 0  &  (max([sum(vel_section(:,1) > fix_thresh(1))  , sum(vel_section(:,2) > fix_thresh(2))]) > num_points_thresh)
                    slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                    slowdrift_flag = 1;
                    if display_flag
                        fprintf('section %i triggered on 20 points\n',round(i/2))
                    end
                    
                    %check if eye signal drifts significantly
                elseif slowdrift_flag == 0 & max_displacement > cutoff
                    slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                    slowdrift_flag = 1;
                    if display_flag
                        fprintf('section %i triggered more than %d degrees MAX DISPLACEMENT\n',round(i/2),cutoff)
                    end
                    
                elseif slowdrift_flag == 0 %if not then a fixation
                    fixationtf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                    if display_flag
                        fprintf('section %i only triggered at %d rather than the required %d MAX DISPLACEMENT\n',round(i/2),max_displacement,cutoff);
                    end
                end
            elseif slowdrift_flag==0 & tooshort_flag==1
                %then just find velocity using endpoints 
                if isempty(section)
                    slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                else
                    avg_velocity = abs(section(1,:)-section(size(section,1),:))/length(section);
                    if max(avg_velocity) > avg_vel_thresh
                        slowdrifttf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                    else
                        fixationtf((marker(i)+1):(marker(i+1)-1)) = ones(1,marker(i+1)-marker(i)-1);
                    end
                end
            end
            %end%if no sections above saccade threshold
    end
    
    
    
    %If the session is fre-viewing, take off the start from every trial...
    if isfield(iniInfo, 'win_movie')
        if iniInfo.win_movie
            if saccadetf(1)==1
                saccadetf(1:min([find(fixationtf) ; find(slowdrifttf)]))=0;
            else
                fixationtf(1:min(find(saccadetf)))=0;
                slowdrifttf(1:min(find(saccadetf)))=0;
            end
            
            %...and the finish from every trial
            if saccadetf(length(saccadetf))==1
                if isempty(find(fixationtf)) & isempty(find(slowdrifttf))
                    saccadetf=zeros(size(saccadetf));
                else
                    saccadetf(max([find(fixationtf) ; find(slowdrifttf)]):length(saccadetf))=0;
                end
            else
                fixationtf(max(find(saccadetf)):length(fixationtf))=0;
                slowdrifttf(max(find(saccadetf)):length(slowdrifttf))=0;
            end
        end
    end
    %take out NaN fixations and surrounding saccades
    %fixations
    if NaNflag
        NaNskip=[];
        for i=1:numNaN
            [dummy ind] = min(abs(NaNstart(i)-marker));
            %ind is index of start of fixation before NaN
            if ind<length(marker)	%if it is during the last fixation then it will be ignored anyway
                
                if saccadetf(marker(ind)+1)==1
                    ind = [ind-2 ind-1 ind ind+1];
                    
                else
                    ind = [ind-1 ind ind+1 ind+2];
                end
            end
            s=find(ind==0);	%if at start of trial
            if ~isempty(s)
                ind = ind(s+1:length(ind));
            end
            s=find(ind>length(marker));	%if at end of trial
            if ~isempty(s)
                ind = ind(1:s-1);
            end
            
            NaNskip = [NaNskip marker(ind(1)) marker(ind(length(ind)))];
        end
        for i=1:2:length(NaNskip)
            
            fixationtf(NaNskip(i):NaNskip(i+1))=0;
            saccadetf(NaNskip(i):NaNskip(i+1))=0;
            slowdrifttf(NaNskip(i):NaNskip(i+1))=0;
        end
    end
    
    %lazy event detection 
    %use max velocity and duration of saccade
    %if event falls above line dur=55*max_velocity+49
    %this line was determined by examining scatterplots from multiple
    %sessions
    %eliminate fixations greater than .4  degrees and drifts 
    %greater than 2 degrees of position range
    
    %loop over marker
    m2=55;
    yint2=49;
    if display_flag
        figure(h4);
        title('Lazy Saccade Detection')
        hold on
        tt=.02:.01:1;
        yy=m2*tt+yint2;
        plot(tt,yy)
        xlabel('max velocity')
        ylabel('duration')
        hold on
    end
    for i=1:length(marker)-1
        if saccadetf(marker(i)+1)
            section_v = filtered((marker(i)):(marker(i+1)),1);
            section_h = filtered((marker(i)):(marker(i+1)),2);
            vel_v = max(abs(gradient(section_v,1)));
            vel_h = max(abs(gradient(section_h,1)));
            mv=max(vel_v,vel_h);
            y=[m2*mv+yint2 length(section_v)];
            if display_flag
                figure(h4)
                plot(max(vel_v,vel_h),y(2),'r*')
                zoom on
            end
            if y(2) > y(1)  %then lazy saccade
                %remove saccade and nieghboring events
                saccadetf(marker(i):marker(i+1))=zeros(marker(i+1)-marker(i)+1,1);
                fixationtf(marker(i-1):marker(i+2))=zeros(marker(i+2)-marker(i-1)+1,1);
                slowdrifttf(marker(i-1):marker(i+2))=zeros(marker(i+2)-marker(i-1)+1,1);
                if display_flag
                    figure(h2)
                    plot(marker(i-1):marker(i+2),filtered(marker(i-1):marker(i+2),1),'k.')
                    plot(marker(i-1):marker(i+2),filtered(marker(i-1):marker(i+2),2),'k.')
                end
            end
        elseif fixationtf(marker(i)+1)
            %measure position range(same way as done on histograms)
            section = data(:,marker(i):marker(i+1));
            n2 = dist2(section', section');
            max_dist = max(max(n2));
            max_dist = sqrt(max_dist);
            if max_dist>.4 
                fixationtf(marker(i):marker(i+1))=zeros(marker(i+1)-marker(i)+1,1);
                if i>1 & i<length(marker)-1
                    saccadetf(marker(i-1):marker(i+2))=zeros(marker(i+2)-marker(i-1)+1,1);
                elseif i<length(marker)-1
                    saccadetf(marker(i):marker(i+2))=zeros(marker(i+2)-marker(i)+1,1);
                elseif i>1
                    saccadetf(marker(i-1):marker(i))=zeros(marker(i)-marker(i-1)+1,1);
                end
            end
        elseif slowdrifttf(marker(i)+1)%measure position range(same way as done on histograms)
            section = data(:,marker(i):marker(i+1));
            n2 = dist2(section', section');
            max_dist = max(max(n2));
            max_dist = sqrt(max_dist);
            if max_dist>2 
                slowdrifttf(marker(i):marker(i+1))=zeros(marker(i+1)-marker(i)+1,1);
                if i>1 & i<length(marker)-1
                    saccadetf(marker(i-1):marker(i+2))=zeros(marker(i+2)-marker(i-1)+1,1);
                elseif i<length(marker)-1
                    saccadetf(marker(i):marker(i+2))=zeros(marker(i+2)-marker(i)+1,1);
                elseif i>1
                    saccadetf(marker(i-1):marker(i))=zeros(marker(i)-marker(i-1)+1,1);
                end
            end
        end
    end
    
    
    %remove unbounded sacccades
    for i=1:length(marker)-1
        if saccadetf(marker(i))==1 & slowdrifttf(marker(i)-1)==0 & fixationtf(marker(i)-1)==0
            if marker(i+1)<length(saccadetf) 
                if slowdrifttf(marker(i+1)+1)==0 & fixationtf(marker(i+1)+1)==0
                    saccadetf(marker(i):marker(i+1))=zeros(marker(i+1)-marker(i)+1,1);
                end
                saccadetf(marker(i):marker(i+1))=zeros(marker(i+1)-marker(i)+1,1);
            end
        end
    end
        
    
    %doublecheck that all tf matices are not overlapping
    overlap = find(((fixationtf+saccadetf+slowdrifttf)~=0)&((fixationtf+saccadetf+slowdrifttf)~=1));
    if ~isempty(overlap)
        status=-1;
        fprintf('Overlap Error %d',overlap);
        break;
    end
    
    
    
    %create eye structure
    saccadecounter=0;
    saccadeflag=0;
    fixationcounter=0;
    fixationflag=0;
    driftcounter=0;
    driftflag=0;
    %create empty matrix incase no events happen
    eye.saccade(trial).start=[];
    eye.saccade(trial).finish=[];
    eye.drift(trial).start=[];
    eye.drift(trial).finish=[];
    eye.fixation(trial).start=[];
    eye.fixation(trial).finish=[];
    
    
    for i=1:length(saccadetf)
        if (saccadetf(i)==1 & saccadeflag==0)
            saccadeflag=1;
            saccadecounter=saccadecounter+1;
            eye.saccade(trial).start(saccadecounter) = i;
        elseif (saccadeflag==1 & saccadetf(i)==0)
            eye.saccade(trial).finish(saccadecounter) = i;
            saccadeflag=0;
            %find max velocity during saccade
            [val,ind] = max(vel(eye.saccade(trial).start(saccadecounter):i,:));
            [val,index] = max(val);
            ind = ind(index);
            eye.saccade(trial).max_velocity(saccadecounter) = ind + eye.saccade(trial).start(saccadecounter) - 1;
            
        elseif (saccadeflag==1 & i==length(saccadetf))
            eye.saccade(trial).finish(saccadecounter) = i;
            %find max velocity during saccade
            [val,ind] = max(vel(eye.saccade(trial).start(saccadecounter):i,:));
            [val,index] = max(val);
            ind = ind(index);
            eye.saccade(trial).max_velocity(saccadecounter) = ind + eye.saccade(trial).start(saccadecounter) - 1;
            
        end
        
        if (fixationtf(i)==1 & fixationflag==0)
            fixationflag=1;
            fixationcounter=fixationcounter+1;
            eye.fixation(trial).start(fixationcounter) = i;
        elseif (fixationflag==1 & fixationtf(i)==0)
            eye.fixation(trial).finish(fixationcounter) = i;
            fixationflag=0;
        elseif (fixationflag==1 & i==length(fixationtf))
            eye.fixation(trial).finish(fixationcounter) = i;
            
        end
        
        if (slowdrifttf(i)==1 & driftflag==0)
            driftflag=1;
            fixationcounter=fixationcounter+1;
            eye.fixation(trial).start(fixationcounter) = i;
            %driftcounter=driftcounter+1;
            %eye.drift(trial).start(driftcounter) = i;
        elseif (driftflag==1 & slowdrifttf(i)==0)
            eye.fixation(trial).finish(fixationcounter) = i;
            %eye.drift(trial).finish(driftcounter) = i;
            driftflag=0;
        elseif (driftflag==1 & i==length(slowdrifttf))
            eye.fixation(trial).finish(fixationcounter) = i;
            %eye.drift(trial).finish(driftcounter) = i;
        end
    end
    
    
    
    if display_flag
        figure(h2)
        
        %plot results of classification
        saccade = filtered .* [saccadetf , saccadetf];
        fixation = filtered .* [fixationtf , fixationtf];
        slowdrift = filtered .* [slowdrifttf , slowdrifttf];
        
        i=find(saccadetf==0);
        saccade(i,:)=100;
        i=find(fixationtf==0);
        fixation(i,:)=100;
        i=find(slowdrifttf==0);
        slowdrift(i,:)=100;
        
        plot(saccade(:,1),'r.')
        plot(fixation(:,1),'g.')
        plot(slowdrift(:,1),'b.')
        plot(saccade(:,2),'r.')
        plot(fixation(:,2),'g.')
        plot(slowdrift(:,2),'b.')
        
        a=axis;
        axis([a(1) a(2) -20 20]);
        legend('vertical raw','horizontal raw','saccade','fixation','slowdrift')
        title(filename)
        xlabel('Time (msec)');
        ylabel('Eye Position (degrees)')
        hold off
        figure(h1)
        zoom on
        %figure(h3)
        %zoom on
        figure(h2)
        zoom on
        
        % get keyboard input to see what to do next
        
        key = input('RETURN - Next Trial; p - Previous trial; N - Trial N: ','s');
        n = str2num(key);
        if strcmp(key,'p')
            trial = trial - 1;
            if trial<1
                trial = 1;
            end
        elseif ~isempty(n)
            if n>1 & n<=numTrials
                trial = n;
            end	
        else
            trial = trial + 1;
        end
        
    else 
        trial = trial + 1;
    end
end

fprintf('\n');

% 
% figure
% plot(eye.sac_vel,eye.sac_dur,'.')
% xlabel('max velocity(degree/msec)')
% ylabel('duration (msec)')
% title(eye.sessionname)
% hold on
% [b,bint,r,rint,stats] = regress(eye.sac_dur',[ones(length(eye.sac_vel),1) eye.sac_vel']);
% refline(b(2),b(1))
% std(r)
% i=find(r>std(r));
% plot(eye.sac_vel(i),eye.sac_dur(i),'cy*')
% i=find(r>2*std(r));
% plot(eye.sac_vel(i),eye.sac_dur(i),'r*')