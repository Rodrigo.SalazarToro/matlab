function [boxX,boxY] = RFEccentricity(rf,rfnum)
%queries init_info.ReceptiveField to get the 
%eccentricity of the RF box corners in screen coordinates

rfnum = rfnum+1;	%b/c first Rf is fixation point

ref = find(rf.Type==2);
refX = rf.CenterX(ref);
refY = rf.CenterY(ref);

%box corners in screen coordinates
boxX = rf.Points(1:2:8,rfnum) - refX;
boxY = rf.Points(2:2:8,rfnum) - refY;








