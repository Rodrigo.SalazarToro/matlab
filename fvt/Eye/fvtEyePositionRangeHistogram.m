function position_range = fvtEyePositionRangeHistogram(eye)
%function position_range = fvtEyePositionRangeHistogram(eye)
%
%This function inputs the eye object containing the start and finish times of 
%fixations and saccades.
%
%The program calculates Position Range Histograms for the fixations and saccades
%InspectEyePositionRangeHistogram can be used to view the histogram
%
%the program assumes you have cd to the correct eye directory
%

buffer = 15;                        %Buffer (towards the center) from the end of each fixation
min_length = (buffer * 3) + 1;		%minimum length to apply beyond buffer; otherwise keep entire section.

position_range.sessionname = eye.data.sessionname;

dirlist = nptDir('*_eye.0*');

fix_range_bin = [];
saccade_range_bin = [];

for trial = 1:size(eye.data.fixation,2)
    filename = dirlist(trial).name;
    [data, num_channels, sampling_rate, datatype, points] = nptReadDataFile(filename);
    %change data from pixels to degrees
    [data(1,:) data(2,:)] = pixel2degree(data(1,:), data(2,:));
    %filter data
    order = 6;
    b = ones(1, order)/order;
    filtered = filtfilt(b, 1, data');	%12th order running average(boxcar) with no delay
    
    for event = 1:size(eye.data.fixation(trial).start,2)
        start = eye.data.fixation(trial).start(event);
        finish = eye.data.fixation(trial).finish(event);
        if (finish - start) >= min_length
            %Remove the buffer sections at each end of the fixation
        fixation_section = filtered((eye.data.fixation(trial).start(event) + buffer):(eye.data.fixation(trial).finish(event) - buffer), :);
        else      %If the fixation isn't big enough to buffer
        fixation_section = filtered(eye.data.fixation(trial).start(event):eye.data.fixation(trial).finish(event), :);
        end
        %Find the distances between every 2 points
        dist = dist2(fixation_section, fixation_section);
        max_dist = max(max(dist));
        PR = sqrt(max_dist);
        fix_range_bin = [fix_range_bin PR];
    end
    
    for event = 1:size(eye.data.saccade(trial).start,2)
        saccade_section = filtered(eye.data.saccade(trial).start(event):eye.data.saccade(trial).finish(event), :);
        %Find the distances between every 2 points
        dist = dist2(saccade_section, saccade_section);
        max_dist = max(max(dist));
        PR = sqrt(max_dist);
        saccade_range_bin = [saccade_range_bin  PR];
    end
end

position_range.fixation = fix_range_bin;
position_range.saccade = saccade_range_bin;