function getAnimalEyeData(filename)
%getAnimaldata(filename)
%
%filename is the name of an excel file 
%which contains the session numbers to get data from
%
%we will get median durations, velocities and amplitudes
%Assume the excel file is in the animal directory

[dates,title] = xlsread(filename);

animal=lower(char(title(1,1)));

for j=1:size(dates,2)
    type=lower(char(title(2,j)));
    d_median.fixation=[];
    d_median.drift=[];
    d_median.saccade=[];
    pr_median.fixation=[];
    pr_median.drift=[];
    pr_median.saccade=[];
    v_median.fixation=[];
    v_median.drift=[];
    v_median.saccade=[];
    for i=1:size(dates,1)
        sessiondate=num2str(dates(i,j));
        if ~strcmp(sessiondate,'NaN')
            %need to check if month needs a leading zero
            if length(sessiondate)==7
                sessiondate = ['0' sessiondate];
            end 
            %parse to session and date
            date=sessiondate(1:length(sessiondate)-2);
            session=sessiondate(length(sessiondate)-1:length(sessiondate));
            cd ([date '/' session '/eye'])
            %get histogram data
            clear duration_histograms
            clear position_range
            clear velocity
            load([animal sessiondate '_duration'])
            load([animal sessiondate '_velocity'])
            load([animal sessiondate '_positionrange'])
            d_median.fixation = [d_median.fixation; median(duration_histograms.fixation)];
            d_median.drift = [d_median.drift; median(duration_histograms.drift)];
            d_median.saccade = [d_median.saccade; median(duration_histograms.saccade)];
            pr_median.fixation = [pr_median.fixation; median(position_range.fixation)];
            pr_median.drift = [pr_median.drift; median(position_range.drift)];
            pr_median.saccade = [pr_median.saccade; median(position_range.saccade)];
            v_median.fixation = [v_median.fixation; median(velocity.fixation)];
            v_median.drift = [v_median.drift; median(velocity.drift)];
            v_median.saccade = [v_median.saccade; median(velocity.saccade)];
        cd ../../..
    end
    end
    
    outfile1=[animal type 'med_dur_fix.txt'];
    outfile2=[animal type 'med_dur_drift.txt'];
    outfile3=[animal type 'med_dur_sac.txt'];
    outfile4=[animal type 'med_amp_fix.txt'];
    outfile5=[animal type 'med_amp_drift.txt'];
    outfile6=[animal type 'med_amp_sac.txt'];
    outfile7=[animal type 'med_vel_fix.txt'];
    outfile8=[animal type 'med_vel_drift.txt'];
    outfile9=[animal type 'med_vel_sac.txt'];
    
    csvwrite(outfile1,d_median.fixation);
    csvwrite(outfile2,d_median.drift);
    csvwrite(outfile3,d_median.saccade);
    csvwrite(outfile4,pr_median.fixation);
    csvwrite(outfile5,pr_median.drift);
    csvwrite(outfile6,pr_median.saccade);
    csvwrite(outfile7,v_median.fixation);
    csvwrite(outfile8,v_median.drift);
    csvwrite(outfile9,v_median.saccade);
    
end