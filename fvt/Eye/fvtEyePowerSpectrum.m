function power_spectrum = fvtEyePowerSpectrum(eye)
%function power_spectrum = fvtEyePowerSpectrum(eye)
%
%this function inputs the eye object containing the start and finish times of 
%fixations, saccades and drifts.
%The program calculates the power spectrum on fixations and drifts 
%for both the vertical and horizontal channels.
%
%Use InspectEyePowerSpectrum to view the plot
%
%the program assumes you have cd to the correct eye directory

power_spectrum.sessionname = eye.data.sessionname;

dirlist=nptDir('*_eye.0*');

fixation=[];
drift=[];

for trial=1:size(eye.data.fixation,2)
   filename=dirlist(trial).name;
   [data,num_channels,sampling_rate,datatype,points]=nptReadDataFile(filename);
   for event=1:size(eye.data.fixation(trial).start,2)
      if (eye.data.fixation(trial).finish(event) - eye.data.fixation(trial).start(event)) > 40
			%grab section with 20 point buffer on both sides
         fixation_section = data(:,eye.data.fixation(trial).start(event) + 20 : eye.data.fixation(trial).finish(event) - 20);
         %change from pixels to degrees
         [fixation_section(1,:), fixation_section(2,:)] = pixel2degree(fixation_section(1,:), fixation_section(2,:));
         %subtract mean value
         fixation_section = fixation_section - mean(fixation_section,2)*ones(1,length(fixation_section));
			%concantonate all sections for the entire session
         fixation = [fixation fixation_section];
      end
   end
   for event=1:size(eye.data.drift(trial).start,2)
      if (eye.data.drift(trial).finish(event) - eye.data.drift(trial).start(event)) > 40
         %grab section with 20 point buffer on both sides
         drift_section = data(:,eye.data.drift(trial).start(event) + 20 : eye.data.drift(trial).finish(event) - 20);
         %change from pixels to degrees
         [drift_section(1,:), drift_section(2,:)] = pixel2degree(drift_section(1,:), drift_section(2,:));
			%subtract mean value
         drift_section = drift_section - mean(drift_section,2)*ones(1,length(drift_section));
%concantonate all sections for the entire session
         drift = [drift drift_section];
      end
   end
end

%Power Spectrum
NFFT =1024;
NOVERLAP=0;
WINDOW = hanning(NFFT);
Fs=1000;

%fixation
if ~isempty(fixation)
[power_spectrum.fixation.vert,power_spectrum.frequencies] = spectrum(fixation(1,:),NFFT,NOVERLAP,WINDOW,Fs);
[power_spectrum.fixation.horiz,power_spectrum.frequencies] = spectrum(fixation(2,:),NFFT,NOVERLAP,WINDOW,Fs);
end

%drift
if ~isempty(drift)
[power_spectrum.drift.vert,power_spectrum.frequencies] = spectrum(drift(1,:),NFFT,NOVERLAP,WINDOW,Fs);
[power_spectrum.drift.horiz,power_spectrum.frequencies] = spectrum(drift(2,:),NFFT,NOVERLAP,WINDOW,Fs);
end
