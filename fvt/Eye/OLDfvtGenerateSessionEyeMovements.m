function [eye,status] = fvtGenerateSessionEyeMovements(display_flag)
%
%[eye,status] = fvtGenerateSessionEyeMovements(display_flag)
%This function determines which regions of an eye signal correspond to
%fixations and saccades.
%This function determines the regions for all trials in a session and then
%creates a structure containing the start and finish times in (milliseconds)
%for each of the events.  The structure is eye(trial).fixation(event#).start
%There are similiar fields for saccade start, finish.
%
%If display_flag is 1 then the results for each trial will be plotted along
%with the velocities, clipped data for noise calculation, and lazy saccade
%data.  If no parameters are entered then display_flag = 0 and the entire
%session is calculated and the eye structure is returned.
%
%status is returned as a -1 if there is an error
%Assumptions:
%The pwd must be the eye folder.

warning off MATLAB:divideByZero

%%%%%%%%%%% MAGIC NUMBERS %%%%%%%%%%%%
r_threshold = 0.9;  	 	%if the fit to a line is lower than this, check for overshoots
lazy_saccade = 210;			%ignore saccades longer than this
minimum_saccade = 3;		%minimum length of a saccade (in ms/pts)
initial_saccade_threshold = 4;		%threshold for removing saccades in order to find the mean and std of the noise
saccade_threshold = 6;		%velocity must be greater than x standard deviations above noise to classify as a saccade
m2 = 55;                    %slope of the line used to detect lazy saccades
yint2 = 60;                 %y-intercept of the line used to detect lazy saccades; NOTE:  Changed from 49
buf = 5;                    %buffer used from the ends of a fixation in determining the end points of saccades; both buf and 2*buf are used
buf2 = 30;                  %a section -buf2:buf2 is used in identifying points of intersections when determining the end points of saccades
axisgain = 100;             %the gain added to the visual dimension to even it out with the time dimension when finding "nearest points"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
status = 1;
if nargin == 0
    display_flag = 0;
end

if display_flag
    h1 = figure;
    set(h1,'Position', [1 29 1280 928]);
    zoom on
    h2 = figure;
    set(h2,'Position', [1 29 1280 928]);
    zoom on
    h3 = figure;
    set(h3,'Position', [1 29 1280 928]);
    zoom on
    h4 = figure;
end

dirlist = nptDir('*_eye.0*');
numTrials = size(dirlist, 1);
trial = 1;

eye.sessionname = dirlist(1).name(1:length(dirlist(1).name) - 9);
cd ..
[iniInfo, status] = ReadRevCorrIni([eye.sessionname, '.ini']);
cd eye

trialThresh = zeros(numTrials, 2);
Rstats = [];
while trial <= numTrials
    if display_flag
        fprintf('\nTrial Number %i\n', trial);
    else
        fprintf('%i ', trial);
    end
    
    %read data
    filename = dirlist(trial).name;
    [data, num_channels, samples_per_sec, datatype, points] = nptReadDataFile(filename);
    
    %change data from pixels to degrees
    [data(1,:) data(2,:)] = pixel2degree(data(1,:), data(2,:));
    
    if display_flag
        %plot unclassified signals
        figure(h2)
        hold off
        plot(data(1,:), 'k')
        hold on
        plot(data(2,:), 'b')
    end
    
    %NaN strategy
    %if this trial contains NaNs then  
    %replace NaNs with surronding values and mark NaN positions.
    %Run through ordinary algorithm
    %then replace the NaN fake fixations and surrounding saccades 
    %with zeros in the true-false(tf) matrices.  
    
    NaNflag = 0;
    if sum(isnan(data), 2) > 0			%if this trial contains NAN's
        NaNflag = 1;
        n = isnan(data);
        [i,j] = find(n);
        if size(i,1) == (size(data, 2) * 2)	%then entire trial is NANs
            NaNstart = 1;
            NaNstop = size(data, 2) - 1;
            data(size(data, 2)) = 0;
        else
            j = j(1:2:length(j))';
            %how many NaN sections are there??
            j1 = [0 j(1:length(j) - 1)];	%shift j
            NaNstart = j(find((j - j1) > 1));
            j1 = [j(2:length(j)) 0];
            NaNstop = [j(find((j1 - j) > 1)) j(length(j))];
            if length(NaNstart) < length(NaNstop)
                NaNstart = [1 NaNstart];
            end
        end
        numNaN = length(NaNstart);
        
        for i = 1:numNaN
            if NaNstart(i) == 1
                data(1, NaNstart(i):NaNstop(i)) = data(1, NaNstop(i) + 1);		%incase NaNs start trial		
                data(2, NaNstart(i):NaNstop(i)) = data(2, NaNstop(i) + 1);				
            else
                data(1, NaNstart(i):NaNstop(i)) = data(1, NaNstart(i) - 1);		%or end trial
                data(2, NaNstart(i):NaNstop(i)) = data(2, NaNstart(i) - 1);
            end
        end
    end
    
    %FIR filter raw data 
    order = 6;
    b = ones(1, order)/order;
    filtered = filtfilt(b, 1, data');	%12th order running average(boxcar) with no delay
    
    %calculate actual trajectory velocity
    delta_vert = diff(filtered(:,1));
    delta_horiz = diff(filtered(:,2));
    distance = sqrt(delta_vert.^2 + delta_horiz.^2);
    realVel = distance * samples_per_sec;
    unfilVel = realVel;
    %filter velocity signal;
    order = 15;	
    b = ones(1, order)/order;
    realVel = filtfilt(b, 1, realVel);
    
    %calculate component position derivative (velocity->degrees per sec)
    vel = abs(diff(filtered));
    %smooth velocity signal
    order = 15;	
    b = ones(1, order)/order;
    vel = filtfilt(b, 1, vel);
    
    %get noise parameters and saccade thresholds from velocities
    [data_mean, stddev] = nptThreshold(vel);
    saccade_thresh = data_mean + initial_saccade_threshold * stddev;
    
    if display_flag
        figure(h1)
        hold off
        plot(vel(:,1), 'g.-')
        hold on
        plot(vel(:,2), 'b.-')
        lh1 = line([(length(data) - 1), 1], [saccade_thresh(1), saccade_thresh(1)]);
        lh2 = line([(length(data) - 1), 1], [saccade_thresh(2), saccade_thresh(2)]);
        set(lh1, 'Color', 'g');
        set(lh2, 'Color', 'b');
        set(lh1, 'LineWidth', 3);
        set(lh2, 'LineWidth', 3);
        legend('vertical velocity (degrees/ms)', 'horizontal velocity (degrees/ms)', 'vertical threshold @ 4sigma+mean', 'horizontal threshold @ 4sigma+mean')
    end
    
    if display_flag
        figure(h3)
        hold off
        plot(realVel, 'g.-')
        hold on
        plot(unfilVel, 'b.-')
%         lh1 = line([(length(data) - 1), 1], [saccade_thresh(1), saccade_thresh(1)]);
%         lh2 = line([(length(data) - 1), 1], [saccade_thresh(2), saccade_thresh(2)]);
%         set(lh1, 'Color', 'g');
%         set(lh2, 'Color', 'b');
%         set(lh1, 'LineWidth', 3);
%         set(lh2, 'LineWidth', 3);
        legend('filtered velocity (degrees/ms)', 'unfiltered velocity (degrees/ms)')
    end
    
    Ycrossings = nptThresholdCrossings(vel(:,1), saccade_thresh(1), 'ignorefirst');
    %if the last point crossed, we need to get rid of it, because after
    %"diff," the vector will be too small.
    if ~isempty(Ycrossings)
        if Ycrossings(end) == length(vel)
            Ycrossings = Ycrossings(1:(end - 1));
        end
        Y = diff(vel(:, 1));
        Yslopes = Y(Ycrossings);
        if Yslopes(1) < 0
            Yslopes = [1; Yslopes];
            Ycrossings = [1; Ycrossings];
        end
        if Yslopes(end) > 0
            Yslopes = [Yslopes; 1];
            Ycrossings = [Ycrossings; length(Y)];
        end
        Ycutoffs = round(Ycrossings - (vel(Ycrossings, 1)./Yslopes));
        Ycutoffs(1:2:end) = Ycutoffs(1:2:end) - (2 * buf);
        Ycutoffs(2:2:end) = Ycutoffs(2:2:end) + (2 * buf);
        tooEarly = find(Ycutoffs < 1);
        if ~isempty(tooEarly)
            Ycutoffs(tooEarly) = 1;
        end
        tooLate = find(Ycutoffs > length(Y));
        if ~isempty(tooLate)
            Ycutoffs(tooLate) = length(Y);
        end
    else
        Ycutoffs = [];
    end   
    
    Xcrossings = nptThresholdCrossings(vel(:,2), saccade_thresh(2), 'ignorefirst');
    %if the last point crossed, we need to get rid of it, because after
    %"diff," the vector will be too small.
    
    if ~isempty(Xcrossings)
        if Xcrossings(end) == length(vel)
            Xcrossings = Xcrossings(1:(end - 1));
        end
        X = diff(vel(:, 2));
        Xslopes = X(Xcrossings);
        if Xslopes(1) < 0
            Xslopes = [1; Xslopes];
            Xcrossings = [1; Xcrossings];
        end
        if Xslopes(end) > 0
            Xslopes = [Xslopes; 1];
            Xcrossings = [Xcrossings; length(X)];
        end
        Xcutoffs = round(Xcrossings - (vel(Xcrossings, 2)./Xslopes));
        Xcutoffs(1:2:end) = Xcutoffs(1:2:end) - (2 * buf);
        Xcutoffs(2:2:end) = Xcutoffs(2:2:end) + (2 * buf);
        tooEarly = find(Xcutoffs < 1);
        if ~isempty(tooEarly)
            Xcutoffs(tooEarly) = 1;
        end
        tooLate = find(Xcutoffs > length(X));
        if ~isempty(tooLate)
            Xcutoffs(tooLate) = length(X);
        end
        if isempty(Ycutoffs)
            Ycutoffs = Xcutoffs;
        end            
    else
        Xcutoffs = Ycutoffs;
    end
    
    if ~isempty(Ycutoffs) | ~isempty(Xcutoffs)
        Ycompare = zeros((length(vel) - 1), 1);
        for index = 1:2:length(Ycutoffs) - 1
            Ycompare(Ycutoffs(index):Ycutoffs(index + 1)) = 1;
        end
        Xcompare = zeros((length(vel) - 1), 1);
        for index = 1:2:length(Xcutoffs) - 1
            Xcompare(Xcutoffs(index):Xcutoffs(index + 1)) = 1;
        end
        cutoffs = Ycompare + Xcompare;
        clippedVelY = vel(find(cutoffs == 0), 1);
        clippedVelX = vel(find(cutoffs == 0), 2);
        meanY = mean(clippedVelY);
        stdY = std(clippedVelY);
        meanX = mean(clippedVelX);
        stdX = std(clippedVelX);
    else
        clippedVelY = vel(:, 1);
        meanY = data_mean(1) ;
        stdY = stddev(1);
        clippedVelX = vel(:, 2);
        meanX = data_mean(2);
        stdX = stddev(2);
    end
%     
%     if display_flag
%         figure(h3)
%         hold off
%         plot(clippedVelY, 'g.-')
%         hold on
%         plot(clippedVelX, 'b.-')
%         lh1 = line([length(clippedVelY), 1], [saccade_thresh(1), saccade_thresh(1)]);
%         lh2 = line([length(clippedVelY), 1], [saccade_thresh(2), saccade_thresh(2)]);
%         set(lh1, 'Color', 'g');
%         set(lh2, 'Color', 'b');
%         legend('vertical velocity (degrees/ms)', 'horizontal velocity (degrees/ms)', 'vertical threshold @ 4sigma+mean', 'horizontal threshold @ 4sigma+mean')
%     end
    
    data_mean = [meanY, meanX];
    stddev = [stdY, stdX];
    saccade_thresh = data_mean + saccade_threshold * stddev;
    trialThresh(trial, :) = saccade_thresh;
    
    if display_flag
        figure(h1)
        lh1 = line([length(data), 1], [saccade_thresh(1), saccade_thresh(1)]);
        lh2 = line([length(data), 1], [saccade_thresh(2), saccade_thresh(2)]);
        set(lh1, 'Color', 'g');
        set(lh2, 'Color', 'b');
        axis tight
        legend('vertical velocity (degrees/ms)', 'horizontal velocity (degrees/ms)', 'original vertical threshold @ 4sigma+mean',...
            'original horizontal threshold @ 4sigma+mean', 'new vertical threshold @ 6sigma+mean', 'new horizontal threshold @ 6sigma+mean')
    end

    
    %%%%%%%%%%%%%%%%% 	Recognition Rules 	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %  -  ignore all events at the start and end of a trial
    %  -  ignore lazy saccades lasting more than 210 ms (lazy_saccade value from above)
    %  -  saccades are regions with large velocities, but longer than 3 datapoints in length
    %  -  slowdrifts are defined by average velocities greater than fixations but smaller than 
    %		saccades or significant displacements.
    %  -  if the data has NANs then ignore the section and both saccades on either side
    %  -  ignore noise spikes in data
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %Define TrueFalse matrices for saccades, fixations, and slowdrifts.
    %Saccades are easily detected because of large velocities.
    saccadetf = (vel(:,1) > saccade_thresh(1))  |  (vel(:,2) > saccade_thresh(2));

    %create marker to define sections of saccades and non-saccades(fixation or slowdrift)
    marker = [1];
    sacEnd = 0;
    sacStart = 0;
    flag = 0; %start is a fixation
    for i = 1:length(saccadetf)
        if flag == 0 & saccadetf(i) == 1
            marker = [marker i];
            flag = 1;
        elseif flag == 1 & saccadetf(i)==0 & i ~= length(saccadetf)
            marker = [marker (i - 1)];
            flag=0;
        elseif flag == 0 & i == length(saccadetf)
            marker = [marker i];
        end
    end
    if saccadetf(1) == 1	%in case start of trial is a saccade, marker will begin with two 1's
        sacStart = marker(3);
        marker = [1, marker(4:end)];
    end
    if saccadetf(end) == 1	%in case end of trial is a saccade
        sacEnd = marker(end);
    end
    
    %Get rid of the saccades that are less than 3 ms (pts) long.
    change = [];
    for i = 2:2:length(marker) - 1
        if (marker(i + 1) - marker(i)) < minimum_saccade
            saccadetf(marker(i):marker(i + 1)) = zeros(1, marker(i + 1) - marker(i) + 1);
            change = [change i];
        end
    end
    if ~isempty(change)
        for i = length(change):-1:1
            marker = [marker(1:change(i) - 1) marker(change(i) + 2:length(marker))];
        end
    end
    %redefine saccade starting and ending points based on the 
    %intersection of a linear fits to the saccade and to the surrounding events.
    Rstat = [];
    for i = 2:2:length(marker) - 2        
        %find saccade amplitude
        ampv = abs(filtered(marker(i), 1) - filtered(marker(i + 1), 1));
        amph = abs(filtered(marker(i), 2) - filtered(marker(i + 1), 2));
        ampq = ampv/amph;
        if (ampv > 0.5) & (amph > 0.5)      %if microsaccad-ish, just use velocity
            %Finding a linear fit to every saccade
            if (ampv < 1) & (amph < 1)      %if small saccade
                %find max velocity during saccade
                [junk, sac_max_v] = max(vel(marker(i):marker(i + 1), 1));
                [junk, sac_max_h] = max(vel(marker(i):marker(i + 1), 2));
                sac_max_v = sac_max_v + marker(i) - 1;
                sac_max_h = sac_max_h + marker(i) - 1;
                %adjusting maxes if they are too close to the beginning or end of the trial
                if sac_max_v == 2
                    sac_max_v = 3;
                end
                if sac_max_h == 2
                    sac_max_h = 3;
                end
                if sac_max_v == marker(end) - 1
                    sac_max_v = marker(end) - 2;
                end
                if sac_max_v == marker(end) - 1
                    sac_max_v = marker(end) - 2;
                end
                saccade_section_v = filtered((sac_max_v - 2):(sac_max_v + 2), 1)';
                saccade_section_h = filtered((sac_max_h - 2):(sac_max_h + 2), 2)';
                [saccade_v, bint, r, rint, vStats] = regress(saccade_section_v', [ones(length(saccade_section_v), 1) [(sac_max_v - 2):(sac_max_v + 2)]']);
                [saccade_h, bint, r, rint, hStats] = regress(saccade_section_h', [ones(length(saccade_section_h), 1) [(sac_max_h - 2):(sac_max_h + 2)]']);
            else            %if at least one channel saccaded > 1 degree
                saccade_section_v = filtered((marker(i) + buf):(marker(i + 1) - buf), 1)';
                saccade_section_h = filtered((marker(i) + buf):(marker(i + 1) - buf), 2)';
                [saccade_v, bint, r, rint, vStats] = regress(saccade_section_v', [ones(length(saccade_section_v), 1) [(marker(i) + buf):(marker(i + 1) - buf)]']);
                [saccade_h, bint, r, rint, hStats] = regress(saccade_section_h', [ones(length(saccade_section_h), 1) [(marker(i) + buf):(marker(i + 1) - buf)]']);
            end
            
            %The rest of the regression algorithm will be conducted only on the channel that saccades
            %(sometimes only one channel will saccade).
            %Find slopes and y-intercept using linear regression on both channels
            %The output of regress is [yint; slope].
            %The closest data point to the intersection points defines the
            %boundaries.  We will take data points on either side 
            %and find the data point with a minimum distance to the intersection point.
            
            if (ampq < 2) & ((1/ampq) < 2) 		%if saccaded on both channels
                Rstat = [Rstat vStats(1) hStats(1)];
                if vStats(1) > r_threshold
                    %If the saccade does not have overshoot, find the linear
                    %fit to the fixations before and after the saccade.
                    left_section_v = filtered(marker(i - 1):marker(i), 1)';
                    left_v = regress(left_section_v', [ones(length(left_section_v), 1) [marker(i - 1):marker(i)]']);
                    right_section_v = filtered(marker(i + 1):marker(i + 2), 1)';
                    right_v = regress(right_section_v', [ones(length(right_section_v), 1) [marker(i + 1):marker(i + 2)]']);
                else
                    %If the saccade possibly has overshoot, use linear segments
                    %at the beginning and end of the saccade to find its end points.
                    left_section_v = filtered(marker(i):(marker(i) + (2 * buf)), 1)';
                    left_v = regress(left_section_v', [ones(length(left_section_v), 1) [marker(i):(marker(i) + (2 * buf))]']);
                    right_section_v = filtered((marker(i + 1) - (2 * buf)):marker(i + 1), 1)';
                    right_v = regress(right_section_v', [ones(length(right_section_v), 1) [(marker(i + 1) - (2 * buf)):marker(i + 1)]']);
                end
                %Do the same thing on the horizontal channel.
                if hStats(1) > r_threshold
                    left_section_h = filtered(marker(i - 1):marker(i), 2)';
                    left_h = regress(left_section_h', [ones(length(left_section_h), 1) [marker(i - 1):marker(i)]']);
                    right_section_h = filtered(marker(i + 1):marker(i + 2), 2)';            
                    right_h = regress(right_section_h', [ones(length(right_section_h), 1) [marker(i + 1):marker(i + 2)]']);
                else
                    left_section_h = filtered(marker(i):(marker(i) + (2 * buf)), 2)';
                    left_h = regress(left_section_h', [ones(length(left_section_h), 1) [marker(i):(marker(i) + (2 * buf))]']);
                    right_section_h = filtered((marker(i + 1) - (2 * buf)):marker(i + 1), 2)';
                    right_h = regress(right_section_h', [ones(length(right_section_h), 1) [(marker(i + 1) - (2 * buf)):marker(i + 1)]']);
                end
                
                %Find the intersections.
                left_y_v = ((left_v(2) * saccade_v(1)) - (saccade_v(2) * left_v(1)))/(left_v(2) - saccade_v(2));
                left_x_v = round((left_y_v - saccade_v(1))/saccade_v(2));
                right_y_v = ((right_v(2) * saccade_v(1)) - (saccade_v(2) * right_v(1)))/(right_v(2) - saccade_v(2));
                right_x_v = round((right_y_v - saccade_v(1))/saccade_v(2));
                left_y_h = ((left_h(2) * saccade_h(1)) - (saccade_h(2) * left_h(1)))/(left_h(2) - saccade_h(2));
                left_x_h = round((left_y_h - saccade_h(1))/saccade_h(2));
                right_y_h = ((right_h(2) * saccade_h(1)) - (saccade_h(2) * right_h(1)))/(right_h(2) - saccade_h(2));
                right_x_h = round((right_y_h - saccade_h(1))/saccade_h(2));
                
                %need to check if intersection point is near the ends of the trial
                if  right_x_v > buf2 & right_x_h > buf2 & (length(filtered) - left_x_v) > buf2 & (length(filtered) - left_x_h) > buf2 ...
                        & left_x_v > buf2 & left_x_h > buf2 & (length(filtered) - right_x_v) > buf2 & (length(filtered) - right_x_h) > buf2
                    
                    [v, ind_l_v] = min((axisgain * (filtered(left_x_v - buf2:left_x_v + buf2, 1) - left_y_v)).^2 + ([-buf2:buf2].^2)');
                    [v, ind_r_v] = min((axisgain * (filtered(right_x_v - buf2:right_x_v+buf2, 1) - right_y_v)).^2 + ([-buf2:buf2].^2)');
                    [v, ind_l_h] = min((axisgain * (filtered(left_x_h - buf2:left_x_h+buf2, 2) - left_y_h)).^2 + ([-buf2:buf2].^2)');
                    [v, ind_r_h] = min((axisgain * (filtered(right_x_h - buf2:right_x_h+buf2, 2) - right_y_h)).^2 + ([-buf2:buf2].^2)');
                    if display_flag
                        figure(h2)
                        plot(left_x_v,left_y_v, 'k*')
                        plot(right_x_v,right_y_v, 'k*')
                        plot(left_x_h,left_y_h, 'k*')
                        plot(right_x_h,right_y_h, 'k*')
                    end
                    newl = min([(left_x_v + ind_l_v - buf2 - 1), (left_x_h + ind_l_h-buf2 - 1)]);
                    if newl > marker(i - 1) & newl < marker(i + 1)     %make sure this is not less than previous marker point
                        marker(i) = newl;
                    end
                    newr = max([(right_x_v + ind_r_v - buf2 - 1),(right_x_h + ind_r_h - buf2 - 1)]);
                    if (length(marker) - i) > 1
                        rlimit = marker(i + 2);
                    else
                        rlimit = length(filtered);
                    end
                    if newr > marker(i) & newr < rlimit      %make sure this is not less than previous marker point
                        marker(i + 1) = newr;
                    end
                end %if not near ends of trial
                
                %else use channel with largest saccade
            elseif (ampv > amph) %& (vStats(1) > r_threshold)	%vert is bigger
                Rstat = [Rstat vStats(1)];
                if vStats(1) > r_threshold
                    %If the saccade does not have overshoot, find the linear
                    %fit to the fixations before and after the saccade.
                    left_section_v = filtered(marker(i - 1):marker(i), 1)';
                    left_v = regress(left_section_v', [ones(length(left_section_v), 1) [marker(i - 1):marker(i)]']);
                    right_section_v = filtered(marker(i + 1):marker(i + 2), 1)';
                    right_v = regress(right_section_v', [ones(length(right_section_v), 1) [marker(i + 1):marker(i + 2)]']);
                else
                    %If the saccade possibly has overshoot, use linear segments
                    %at the beginning and end of the saccade to find its end points.
                    left_section_v = filtered(marker(i):(marker(i) + (2 * buf)), 1)';
                    left_v = regress(left_section_v', [ones(length(left_section_v), 1) [marker(i):(marker(i) + (2 * buf))]']);
                    right_section_v = filtered((marker(i + 1) - (2 * buf)):marker(i + 1), 1)';
                    right_v = regress(right_section_v', [ones(length(right_section_v), 1) [(marker(i + 1) - (2 * buf)):marker(i + 1)]']);
                end
                %Find the intersections.
                left_y_v = ((left_v(2) * saccade_v(1)) - (saccade_v(2) * left_v(1)))/(left_v(2) - saccade_v(2));
                left_x_v = round((left_y_v - saccade_v(1))/saccade_v(2));
                right_y_v = ((right_v(2) * saccade_v(1)) - (saccade_v(2) * right_v(1)))/(right_v(2) - saccade_v(2));
                right_x_v = round((right_y_v - saccade_v(1))/saccade_v(2));
                
                %need to check if saccade is near the ends of the trial
                if  right_x_v > buf2 & (length(filtered) - left_x_v) > buf2 & left_x_v > buf2 & (length(filtered) - right_x_v) > buf2 
                    
                    [v,ind_l_v] = min((axisgain * (filtered(left_x_v - buf2:left_x_v + buf2, 1) - left_y_v)).^2 + ([-buf2:buf2].^2)');
                    [v,ind_r_v] = min((axisgain * (filtered(right_x_v - buf2:right_x_v + buf2, 1) - right_y_v)).^2 + ([-buf2:buf2].^2)');
                    newl=	left_x_v + ind_l_v - buf2 - 1;
                    if newl > marker(i - 1) & newl < marker(i + 1)     %make sure this is not less than previous marker point
                        marker(i) = newl;
                    end
                    newr = right_x_v + ind_r_v - buf2 - 1;
                    if (length(marker) - i) > 1
                        rlimit = marker(i + 2);
                    else
                        rlimit = length(filtered);
                    end
                    if newr > marker(i) & newr < rlimit      %make sure this is not less than previous marker point
                        marker(i + 1) = newr;
                    end
                    if display_flag
                        figure(h2)
                        plot(left_x_v,left_y_v, 'k*')
                        plot(right_x_v,right_y_v, 'k*')
                    end
                end %if not near ends of trial
                
            else%if hStats(1) > r_threshold   %horiz is bigger
                Rstat = [Rstat hStats(1)];
                if hStats(1) > r_threshold
                    left_section_h = filtered(marker(i - 1):marker(i), 2)';
                    left_h = regress(left_section_h', [ones(length(left_section_h), 1) [marker(i - 1):marker(i)]']);
                    right_section_h = filtered(marker(i + 1):marker(i + 2), 2)';            
                    right_h = regress(right_section_h', [ones(length(right_section_h), 1) [marker(i + 1):marker(i + 2)]']);
                else
                    left_section_h = filtered(marker(i):(marker(i) + (2 * buf)), 2)';
                    left_h = regress(left_section_h', [ones(length(left_section_h), 1) [marker(i):(marker(i) + (2 * buf))]']);
                    right_section_h = filtered((marker(i + 1) - (2 * buf)):marker(i + 1), 2)';
                    right_h = regress(right_section_h', [ones(length(right_section_h), 1) [(marker(i + 1) - (2 * buf)):marker(i + 1)]']);
                end
                %Find the intersections.
                left_y_h = ((left_h(2) * saccade_h(1)) - (saccade_h(2) * left_h(1)))/(left_h(2) - saccade_h(2));
                left_x_h = round((left_y_h - saccade_h(1))/saccade_h(2));
                right_y_h = ((right_h(2) * saccade_h(1)) - (saccade_h(2) * right_h(1)))/(right_h(2) - saccade_h(2));
                right_x_h = round((right_y_h - saccade_h(1))/saccade_h(2));
                
                %need to check if saccade is near the ends of the trial
                if right_x_h > buf2 & (length(filtered) - left_x_h) > buf2 & left_x_h > buf2 & (length(filtered) - right_x_h) > buf2
                    
                    [v,ind_l_h] = min((axisgain * (filtered(left_x_h - buf2:left_x_h + buf2, 2) - left_y_h)).^2 + ([-buf2:buf2].^2)');
                    [v,ind_r_h] = min((axisgain * (filtered(right_x_h - buf2:right_x_h + buf2, 2) - right_y_h)).^2 + ([-buf2:buf2].^2)');
                    newl =	left_x_h + ind_l_h - buf2 - 1;
                    if newl>marker(i - 1) & newl < marker(i + 1)     %make sure this is not less than previous marker point
                        marker(i) = newl;
                    end
                    newr = right_x_h + ind_r_h - buf2 - 1;
                    if (length(marker) - i) > 1
                        rlimit = marker(i + 2);
                    else
                        rlimit = length(filtered);
                    end
                    if newr > marker(i) & newr < rlimit      %make sure this is not less than previous marker point
                        marker(i + 1) = newr;
                    end
                    if display_flag
                        figure(h2)
                        plot(left_x_h,left_y_h, 'k*')
                        plot(right_x_h,right_y_h, 'k*')
                    end
                end %if not near ends of trial
                
            end	%which saccade is biggest
        end	%if microsaccadish
    end  %for length of marker
    
%     if display_flag & ~isempty(Rstat)
%         figure(h3)
%         hold off
%         plot(Rstat, 'r*')
%         title('Pearson Correlation Factors for Saccades')
%     end

    %now fix saccadetf and fixationtf to correspond to the new markers
    saccadetf = zeros(length(vel), 1);
    for i=2:2:length(marker)-1
        saccadetf(marker(i):marker(i+1))=1;
    end
    fixationtf = 1 - saccadetf;
    if sacStart
        fixationtf(1:sacStart) = 0;
    end
    if sacEnd
        fixationtf(sacEnd:end) = 0;
    end
   
    
    if NaNflag
        NaNskip = [];
        for i = 1:numNaN
            [dummy ind] = min(abs(NaNstart(i) - marker));
            %ind is index of start of fixation before NaN
            if ind < length(marker)	%if it is during the last fixation then it will be ignored anyway
                if saccadetf(marker(ind) + 1) == 1
                    ind = [(ind - 2) (ind - 1) ind (ind + 1)];
                else
                    ind = [(ind - 1) ind (ind + 1) (ind + 2)];
                end
            end
            s = find(ind == 0);	%if at start of trial
            if ~isempty(s)
                ind = ind((s + 1):length(ind));
            end
            s = find(ind > length(marker));	%if at end of trial
            if ~isempty(s)
                ind = ind(1:(s - 1));
            end
            NaNskip = [NaNskip marker(ind(1)) marker(ind(length(ind)))];
        end
        for i=1:2:length(NaNskip)
            fixationtf(NaNskip(i):NaNskip(i + 1)) = 0;
            saccadetf(NaNskip(i):NaNskip(i + 1)) = 0;
        end
    end
    
    %lazy event detection 
    %use max velocity and duration of saccade
    %if event falls above line dur=55*max_velocity+49
    %this line was determined by examining scatterplots from multiple
    %sessions
    %eliminate fixations greater than .4  degrees and drifts 
    %greater than 2 degrees of position range
    
    %ignore saccades that last more than lazy saccade)
    for i = 2:2:length(marker) - 1
        if (marker(i + 1) - marker(i)) > lazy_saccade
            saccadetf(marker(i):marker(i + 1)) = zeros(1, marker(i + 1) - marker(i) + 1);
        end
    end
    
    %loop over marker
    if display_flag
        figure(h4);
        title('Lazy Saccade Detection')
        hold on
        tt = .02:.01:1;
        yy = (m2 * tt) + yint2;
        plot(tt, yy)
        xlabel('max velocity')
        ylabel('duration')
        hold on
    end
    
    for i = 1:length(marker) - 1
        if saccadetf(marker(i) + 1)
            section_v = filtered((marker(i)):(marker(i + 1)), 1);
            section_h = filtered((marker(i)):(marker(i + 1)), 2);
            vel_v = max(abs(diff(section_v, 1)));
            vel_h = max(abs(diff(section_h, 1)));
            mv = max(vel_v, vel_h);
            y = [((m2 * mv) + yint2) length(section_v)];
            if display_flag
                figure(h4)
                plot(max(vel_v, vel_h), y(2), 'r*')
                zoom on
            end
            if y(2) > y(1)  %then lazy saccade
                %remove saccade and neighboring events (if possible)
                saccadetf(marker(i):marker(i + 1)) = zeros(marker(i + 1) - marker(i) + 1, 1);
                if i == length(marker) - 1      %If looking at the last event, only worry about what was in front
                    fixationtf(marker(i - 1):marker(i + 1)) = zeros(marker(i + 1) - marker(i - 1) + 1, 1);
                    if display_flag
                        figure(h2)
                        plot(marker(i - 1):marker(i + 1), filtered(marker(i - 1):marker(i + 1), 1), 'k.')
                        plot(marker(i - 1):marker(i + 1), filtered(marker(i - 1):marker(i + 1), 2), 'k.')
                    end
                else
                    fixationtf(marker(i - 1):marker(i + 2)) = zeros(marker(i + 2) - marker(i - 1) + 1, 1);
                    if display_flag
                        figure(h2)
                        plot(marker(i - 1):marker(i + 2), filtered(marker(i - 1):marker(i + 2), 1), 'k.')
                        plot(marker(i - 1):marker(i + 2), filtered(marker(i - 1):marker(i + 2), 2), 'k.')
                    end
                end
            end
        elseif fixationtf(marker(i) + 1)
            %measure position range(same way as done on histograms)
            section = data(:, marker(i):marker(i + 1));
            n2 = dist2(section', section');
            max_dist = max(max(n2));
            max_dist = sqrt(max_dist);
            if max_dist > 2 
                fixationtf(marker(i):marker(i + 1)) = zeros(marker(i + 1) - marker(i) + 1, 1);
                if (i > 1) & (i < length(marker) - 1)
                    saccadetf(marker(i - 1):marker(i + 2)) = zeros(marker(i + 2) - marker(i - 1) + 1, 1);
                elseif i < length(marker) - 1
                    saccadetf(marker(i):marker(i + 2)) = zeros(marker(i + 2) - marker(i) + 1, 1);
                elseif i > 1
                    saccadetf(marker(i - 1):marker(i)) = zeros(marker(i) - marker(i - 1) + 1, 1);
                end
            end
        end
    end
    
    %If the session is fre-viewing, take off the start from every trial...
    if isfield(iniInfo, 'win_movie')
        if iniInfo.win_movie
            if saccadetf(1) == 1
                saccadetf(1:min(find(fixationtf))) = 0;
            elseif fixationtf(1) == 1
                fixationtf(1:min(find(saccadetf))) = 0;
            end
            
            %...and the finish from every trial
            if saccadetf(end) == 1
                if isempty(find(fixationtf))
                    saccadetf=zeros(size(saccadetf));
                else
                    saccadetf(max(find(fixationtf)):end) = 0;
                end
            elseif fixationtf(end) == 1
                fixationtf(max(find(saccadetf)):end) = 0;
            end
        end
    end
    
    %In order to remove all of the unbounded short saccades and fixations
    %we'll use iterations.
    %But first, check to see if the trial begins with a short fixation
    if fixationtf(marker(2) - 1) & (min(find(fixationtf)) - marker(2) <= 40)
        fixationtf(1:marker(2)) = 0;
    end
    change = 1;
    while change
        change = 0;
        %Remove lone sacccades and unbounded microsaccades
        for i = 2:(length(marker) - 1)
            %if there's no fixation on the left side...
            if (saccadetf(marker(i) + 1)) & (fixationtf(marker(i) - 1) == 0)
                ampv = abs(filtered(marker(i), 1) - filtered(marker(i + 1), 1));
                amph = abs(filtered(marker(i), 2) - filtered(marker(i + 1), 2));
                if (ampv < 0.5) & (amph < 0.5)      %if microsaccad-ish, remove
                    saccadetf(marker(i):marker(i + 1)) = 0;
                    change = 1;
                elseif fixationtf(marker(i + 1) + 1) == 0   %if the saccade is lone, remove
                    saccadetf(marker(i):marker(i + 1)) = 0;
                    change = 1;
                end
                %and/or the right side of the saccade
            elseif (saccadetf(marker(i) + 1)) & (marker(i + 1) < length(saccadetf))
                if fixationtf(marker(i + 1) + 1) == 0
                    ampv = abs(filtered(marker(i), 1) - filtered(marker(i + 1), 1));
                    amph = abs(filtered(marker(i), 2) - filtered(marker(i + 1), 2));
                    if (ampv < 0.5) & (amph < 0.5)      %if microsaccad-ish, remove
                        saccadetf(marker(i):marker(i + 1)) = 0;
                        change = 1;
                    end
                end
            end
            %Remove unbounded short fixations;  Note that although "lazy"
            %and long saccades have been removed, their markers remain.
            %If the fixation follows a saccade marker
            if fixationtf(marker(i) + 1) & (saccadetf(marker(i) - 1) == 0) & (marker(i + 1) - marker(i) <= 40)
                fixationtf(marker(i):marker(i + 1)) = 0;
                change = 1;
            elseif (fixationtf(marker(i) + 1)) & (marker(i + 1) == length(fixationtf))
                if (marker(i + 1) - marker(1) <= 40)
                    fixationtf(marker(i):marker(i + 1)) = 0;
                    change = 1;
                end
            elseif (fixationtf(marker(i) + 1)) & (saccadetf(marker(i + 1) + 1) == 0) & (marker(i + 1) - marker(i) <= 40)
                fixationtf(marker(i):marker(i + 1)) = 0;
                change = 1;
            end
        end
    end
    
    %doublecheck that all tf matices are not overlapping
    overlap = find(((fixationtf + saccadetf) ~= 0) & ((fixationtf + saccadetf) ~= 1));
    if ~isempty(overlap)
        status = -1;
        fprintf('Overlap Error %d', overlap);
        break;
    end
    
    
    
    %create eye structure
    saccadecounter = 0;
    saccadeflag = 0;
    fixationcounter = 0;
    fixationflag = 0;
    %create empty matrix incase no events happen
    eye.saccade(trial).start = [];
    eye.saccade(trial).finish = [];
    eye.fixation(trial).start = [];
    eye.fixation(trial).finish = [];
    %For now, I'll create this dummy field until downstream scripts can be
    %fixed to be "drift-free"
    eye.drift(trial).start = [];
    eye.drift(trial).finish = [];

    for i=1:length(saccadetf)
        if (saccadetf(i) == 1) & (saccadeflag == 0)
            saccadeflag = 1;
            saccadecounter = saccadecounter + 1;
            eye.saccade(trial).start(saccadecounter) = i;
        elseif (saccadeflag == 1) & (saccadetf(i) == 0)
            eye.saccade(trial).finish(saccadecounter) = i;
            saccadeflag = 0;
            %find max velocity during saccade
            [val, ind] = max(realVel(eye.saccade(trial).start(saccadecounter):i, :));
            eye.saccade(trial).max_velocity(:, saccadecounter) = [ind + eye.saccade(trial).start(saccadecounter) - 1; val];
            
        elseif (saccadeflag == 1) & (i == length(saccadetf))
            eye.saccade(trial).finish(saccadecounter) = i;
            %find max velocity during saccade
            [val, ind] = max(realVel(eye.saccade(trial).start(saccadecounter):i, :));
            eye.saccade(trial).max_velocity(saccadecounter) = [ind + eye.saccade(trial).start(saccadecounter) - 1; val];
        end
        
        if (fixationtf(i) == 1) & (fixationflag == 0)
            fixationflag = 1;
            fixationcounter = fixationcounter + 1;
            eye.fixation(trial).start(fixationcounter) = i;
        elseif (fixationflag == 1) & (fixationtf(i) == 0)
            eye.fixation(trial).finish(fixationcounter) = i;
            fixationflag = 0;
        elseif (fixationflag == 1) & (i == length(fixationtf))
            eye.fixation(trial).finish(fixationcounter) = i;
        end
    end
    Rstats = concatenate(Rstats, Rstat);
    
    if display_flag
        Rstat
        figure(h2)
        
        %plot results of classification
        saccade = filtered(1:(end - 1), :) .* [saccadetf, saccadetf];
        fixation = filtered(1:(end - 1), :) .* [fixationtf, fixationtf];
        
        i=find(saccadetf==0);
        saccade(i,:)=100;
        i=find(fixationtf==0);
        fixation(i,:)=100;
        
        plot(saccade(:,1), 'r.')
        plot(fixation(:,1), 'g.')
        plot(saccade(:,2), 'r.')
        plot(fixation(:,2), 'g.')
        
        a=axis;
        axis([a(1) a(2) -20 20]);
        legend('vertical raw','horizontal raw','saccade','fixation')
        title(filename)
        xlabel('Time (ms)');
        ylabel('Eye Position (degrees)')
        hold off
        figure(h1)
        zoom on
        %figure(h3)
        %zoom on
        figure(h2)
        zoom on
        
        % get keyboard input to see what to do next
        
        key = input('RETURN - Next Trial; p - Previous trial; N - Trial N: ','s');
        n = str2num(key);
        if strcmp(key,'p')
            trial = trial - 1;
            if trial<1
                trial = 1;
            end
        elseif ~isempty(n)
            if n>1 & n<=numTrials
                trial = n;
            end	
        else
            trial = trial + 1;
        end
        
    else 
        trial = trial + 1;
    end
end

thresh = [mean(trialThresh(:, 1)) std(trialThresh(:, 1)) mean(trialThresh(:, 2)) std(trialThresh(:, 2))];
save([eye.sessionname, '_threshold'], 'thresh')
fprintf('\n');