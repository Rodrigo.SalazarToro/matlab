function [R, Pspike] = PhotoRevCorr(session, windowSize, binSize, displayBins, varargin);
%[R, Pspike] = PhotoRevCorr(session, windowSize, binSize, displayBins, varargin);
%PhotoRevCorr -- function to reverse correlate photodiode data from a sparse noise stimulus
%   Input:  session -- session name prefix, ex:  'test043003'
%			windowSize -- reverse correlation window size (ms)
% 			binSize -- bin size (ms)
%			displayBins -- number of time bins to average for each step displayed
%           numTrials -- number of trials in vector form, ex: 1:100;
%				default = total number of trials according to the .ini file
%           photoChannel -- channel number of photodiode data; default  = 6
%           presenterTrig -- channel number of the presenter trigger; default = 2
%			photoThresh -- threshhold for both the presenter trigger, and the photodiode data
%				default = 4000

%Dependencies:   ReadRevCorrIni.m, CreatePhotoSpikes.m, ispikes.m, nptReadStreamerFile.m, 
%	nptThresholdCrossings.m, FindFramePoints.m, ReadExtraSyncsFile.m, FindTrialFrames.m, 
%	nptReadSyncsFile.m, SNRevCorr.m, show_revcorr.m, ijoc2index.m

[iniInfo, status] = ReadRevCorrIni([session '.ini']);

%Setting Defaults
numTrials = 1:(iniInfo.num_trials);
photoChannel = 7;
presenterTrig = 2;
photoThresh = 4000;
%The assumption here is that the photodiode threshold is also suitable for the presenter trigger.

%Reading in optional variables if necessary
if nargin > 4
   numTrials = varargin{1};
end
if nargin > 5
   photoChannel = varargin{2};
end
if nargin > 6
   presenterTrig = varargin{3};
end
if nargin > 7
   photoThresh = varargin{4};
end

fprintf('Thresholding Photodiode Data\n')
[photoSpikes, presentTrigs] = CreatePhotoSpikes(session, numTrials, presenterTrig, photoChannel, photoThresh);

fprintf('Finding Points of Frame Transitions\n')
framePoints = FindFramePoints(session, iniInfo, presentTrigs);

fprintf('Performing Reverse Correlation to the Stimulus Sequence\n')
seq = read_seq([session '.seq']);
[R, Pspike] = SNRevCorr(photoSpikes, 1, framePoints, iniInfo, seq, windowSize, binSize);

show_revcorr(R, iniInfo, displayBins)