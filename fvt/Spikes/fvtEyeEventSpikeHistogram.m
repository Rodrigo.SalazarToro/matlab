function eyeeventspike = fvtEyeEventSpikeHistogram(em, spike, bin_length)
%
%function eyeeventspike = fvtEyeEventSpikeHistogram(em,spike,bin_length)
%
%this function calculates the Post Stimulus Time Histograms
%for the saccades, fixations, and drifts during a session.
%
%the structure containing the eye data (em) and the session spike train (spike)
%must be passed in.
%
%Usually, the em structure is saved in the eye folder as a .MAT file and the 
%spike structure is saved in the sort folder as a .MAT File.
%
%bin_length is a 1x3 matrix containing the bin lengths for the fixations, saccades and drifts respectively.
%The default binlength is [20 5 20] in milliseconds.
%
%This function loops over all clusters.
spike = spike.data;
sessionname = spike.sessionname;
snIndex = strfind(sessionname,'_highpass');
if ~isempty(snIndex)
    sessionname=sessionname(1:(snIndex-1));
end
eyeeventspike.sessionname=sessionname;
eyeeventspike.groupname = spike.groupname;

if nargin < 3
   bin_length = [20 5 20];
end
eyeeventspike.bin_length = bin_length;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	FIXATION		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.numClusters						%loop over clusters
   cum_bin = 0;	%initialize histogram
   
   for tr = 1:size(spike.trial,2)					%loop over trials
      for ev = 1:size(em.data.fixation(tr).start,2)		%loop over events
         
         start = em.data.fixation(tr).start(ev)-100; 	%start is 100 msec before event
         
         finish = em.data.fixation(tr).finish(ev);
         %find spikes during this event
         ind = find(spike.trial(tr).cluster(cl).spikes > start & spike.trial(tr).cluster(cl).spikes <= finish);	%ie. spikes 2,3&4
         %bin these spikes
         bin = ceil((spike.trial(tr).cluster(cl).spikes(ind)-start)/bin_length(1));								%ie. which are in bins 5,5&6
         
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_length(1));
         bin(find(bin>last_bin))=[];
         
         if ~isempty(bin)
            bin = histc(bin,[1:max(bin)]);	%number of spikes per bin								%ie. 0 0 0 0 2 1
         else
            bin=0;
         end
         
         %cumulative bin 
         cum_bin = add(cum_bin,bin);
         
         %keep track of event durations (all clusters same as cluster #1)
         if cl==1
            bin_dur = [bin_dur last_bin];
         end
      end
   end
    
          [afr,num_bins] = fvtAverageFiringRate(bin_dur,cum_bin,bin_length(1));
  eyeeventspike.num_bins.fixation = num_bins;
eyeeventspike.cluster(cl).fixation.AFR = afr;

end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	SACCADE	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.numClusters						%loop over clusters
   cum_bin = 0;	%initialize histogram
   for tr = 1:size(spike.trial,2)					%loop over trials
      for ev = 1:size(em.data.saccade(tr).start,2)		%loop over events
         start = em.data.saccade(tr).max_velocity(ev)-100;
         finish = em.data.saccade(tr).max_velocity(ev)+100;
         %find spikes during this event
         ind = find(spike.trial(tr).cluster(cl).spikes > start & spike.trial(tr).cluster(cl).spikes <= finish);	%ie. spikes 2,3&4
         %bin these spikes
         bin = ceil((spike.trial(tr).cluster(cl).spikes(ind)-start)/bin_length(2));						%ie. which are in bins 5,5&6
         
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_length(2));
         bin(find(bin>last_bin))=[];
         
         if ~isempty(bin)
            bin = histc(bin,[1:max(bin)]);	%number of spikes per bin								%ie. 0 0 0 0 2 1
         else
            bin=0;
         end
         
         %cumulative bin 
         cum_bin = add(cum_bin,bin);
         
         %keep track of event durations (all clusters same as cluster #1)
         if cl==1
            bin_dur = [bin_dur last_bin];
         end
      end
   end
   
          [afr,num_bins] = fvtAverageFiringRate(bin_dur,cum_bin,bin_length(2));
  eyeeventspike.num_bins.saccade = num_bins;
eyeeventspike.cluster(cl).saccade.AFR = afr;

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	SLOW DRIFT	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.numClusters						%loop over clusters
   cum_bin = 0;	%initialize histogram
   for tr = 1:size(spike.trial,2)					%loop over trials
      for ev = 1:size(em.data.drift(tr).start,2)		%loop over events
         start = em.data.drift(tr).start(ev)-100;		%start is 100 msec before event
         finish = em.data.drift(tr).finish(ev);
         %find spikes during this event
         ind = find(spike.trial(tr).cluster(cl).spikes > start & spike.trial(tr).cluster(cl).spikes <= finish);	%ie. spikes 2,3&4
         %bin these spikes
         bin = ceil((spike.trial(tr).cluster(cl).spikes(ind)-start)/bin_length(3));									%ie. which are in bins 5,5&6
         
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_length(3));
         bin(find(bin>last_bin))=[];
         
         if ~isempty(bin)
            bin = histc(bin,[1:max(bin)]);	%number of spikes per bin								%ie. 0 0 0 0 2 1
         else
            bin=0;
         end
         
         %cumulative bin 
         cum_bin = add(cum_bin,bin);
         
         %keep track of event durations (all clusters same as cluster #1)
         if cl==1
            bin_dur = [bin_dur last_bin];
         end
      end
   end
   
          [afr,num_bins] = fvtAverageFiringRate(bin_dur,cum_bin,bin_length(3));
  eyeeventspike.num_bins.drift = num_bins;
eyeeventspike.cluster(cl).drift.AFR = afr;
end




