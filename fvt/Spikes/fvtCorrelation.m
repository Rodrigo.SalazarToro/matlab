function [c,lags] = fvtCorrelation(spktrn1,spktrn2,max_lag,resolution)
%[c,lags] = fvtCorrelation(spktrn1,spktrn2,max_lag,resolution)
%
%finds the cross corelation between spiketrian1 and spiketrain2.
%The correlation calculation is done with an input variable maximum lag.
%max_lag is inputted in milliseconds.
%Autocorrelation is done if spiketrain1 and spiketrain2 are the same.
%
%spiketrains are assumed to be row vectors of 1's and 0's at 10 KHz resolution.
%Spiketrains can be binned at a lower resolution specified by resolution.
%Resolution should be specified in Hz.
%
%The outputs are the correlations at each lag.  Lags are in milliseconds.


orig_res = 10000;	%spike sorting results in spiketrains with 10KHz resoltion.

if nargin==4
   %then rebin the data to a lower resolution
   if resolution > 10000
      error('ERROR, the resolution must be less than 10 KHz!!')
   else
     % spktrn1 = resample(spktrn1,resolution,orig_res);
     % spktrn2 = resample(spktrn2,resolution,orig_res);
   end
end

max_lag = max_lag/1000*resolution;

%[c,lags] = xcorr(spktrn1,spktrn2,max_lag);


[c,lags] = xcorr(spktrn1,spktrn2,'none');
c
max(c)



