function getAnimalSpikeData(filename)
%getAnimaldata(filename)
%
%filename is the name of an excel file 
%which contains the session numbers to get data from
%
%we will get median durations, velocities and amplitudes
%Assume the excel file is in the animal directory

[dates,title] = xlsread(filename);

animal=lower(char(title(1,1)));

for j=1
    type=lower(char(title(2,j)));
    nonsaccade=[];
    saccade=[];
    for i=1:size(dates,1)
        sessiondate=num2str(dates(i,j));
        if ~strcmp(sessiondate,'NaN')
            %need to check if month needs a leading zero
            if length(sessiondate)==7
                sessiondate = ['0' sessiondate];
            end 
            %parse to session and date
            date=sessiondate(1:length(sessiondate)-2);
            session=sessiondate(length(sessiondate)-1:length(sessiondate));
            [date '/' session '/sort']
            cd ([date '/' session '/sort'])
            %get group and cluster
            group=dates(i,j+1);
            cl=dates(i,j+2);
            %get histogram data
            clear spikeprob
            load([animal sessiondate 'G' num2strpad(group,4) '_spikeprob'])
            nsnew=[];
            snew=[];
            c = concatenate(spikeprob.cluster(cl).fixation,spikeprob.cluster(cl).drift,0);
            nonsaccade = concatenate(nonsaccade,sum(c)/sum(sum(c)),0);
            saccade = concatenate(saccade,spikeprob.cluster(cl).saccade/sum(spikeprob.cluster(cl).saccade),0);
            cd ../../..
        end
    end
   
    outfile1=[animal '_nonsac_spike.txt'];
    outfile2=[animal '_sac_spike.txt'];
    
    csvwrite(outfile1,nonsaccade);
    csvwrite(outfile2,saccade);
    
end