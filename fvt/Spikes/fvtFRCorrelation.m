function FRCor = fvtFRCorrelation(spike,eye,bins)
%function FRCor = fvtFRCorrelation(spike,eye,bins)
%
%finds the correlation coeffecient across all cells during 
%each eye event type (saccade, drift, fixation).
%
%bins is a matrix of bin sizes in milliseconds to correlate over
%for each eye event type
%default bins = [20 5 20;40 10 40; 80 20 80; 160 40 160; 320 80 320]
%
%a linear fit can be used on this cluster to get
%slope, correlation (r) and significance (p).
%
%assumes each spike is from the same session
%but not neccessarily from the same group.


if nargin==2
   bins = [20 5 20;40 10 40; 80 15 80; 160 20 160];%; 320 80 320];
end
FRCor.bins=bins;

sessionname = spike.data.sessionname;
snIndex = strfind(sessionname,'_highpass');
if ~isempty(snIndex)
    sessionname=sessionname(1:(snIndex-1));
end
FRCor.sessionname = sessionname;
FRCor.groupname = spike.data.groupname;



for i=1:size(bins,1)
   FR = fvtFiringRate(spike,eye,bins(i,:));
   %corrcoef		correlation coefficient
   %cov				covariance
   %xcorr			cross correlation
   %xcov				cross covariance
   
   %How many cells are there?
   num_cells = size(FR.cluster,2);
   %need all combinations of two cells
   c1=0;
   for j = 1:num_cells
      c1=c1+1;
      for k = 0:num_cells-j
         c2=c1+k;
         
         
         warning off
         %Spikes Per Event
         FRCor.fixation(i).SPE(c1,c2).corrcoef = corrcoef(FR.cluster(c1).fixation.SpikesPerEvent',FR.cluster(c2).fixation.SpikesPerEvent');
         %FRCor.fixation(i).SPE(c1,c2).cov = cov(FR.cluster(c1).fixation.SpikesPerEvent,FR.cluster(c2).fixation.SpikesPerEvent);
         FRCor.saccade(i).SPE(c1,c2).corrcoef = corrcoef(FR.cluster(c1).saccade.SpikesPerEvent',FR.cluster(c2).saccade.SpikesPerEvent');
         %FRCor.saccade(i).SPE(c1,c2).cov = cov(FR.cluster(c1).saccade.SpikesPerEvent,FR.cluster(c2).saccade.SpikesPerEvent);
         FRCor.drift(i).SPE(c1,c2).corrcoef = corrcoef(FR.cluster(c1).drift.SpikesPerEvent',FR.cluster(c2).drift.SpikesPerEvent');
         %FRCor.drift(i).SPE(c1,c2).cov = cov(FR.cluster(c1).drift.SpikesPerEvent,FR.cluster(c2).drift.SpikesPerEvent);
         
         %Spikes Per Bin
         FRCor.fixation(i).SPB(c1,c2).corrcoef = corrcoef(FR.cluster(c1).fixation.SpikesPerBin',FR.cluster(c2).fixation.SpikesPerBin');
         %FRCor.fixation(i).SPB(c1,c2).cov = cov(FR.cluster(c1).fixation.SpikesPerBin,FR.cluster(c2).fixation.SpikesPerBin);
         FRCor.saccade(i).SPB(c1,c2).corrcoef  = corrcoef(FR.cluster(c1).saccade.SpikesPerBin',FR.cluster(c2).saccade.SpikesPerBin');
         %FRCor.saccade(i).SPB(c1,c2).cov  = cov(FR.cluster(c1).saccade.SpikesPerBin,FR.cluster(c2).saccade.SpikesPerBin);
         FRCor.drift(i).SPB(c1,c2).corrcoef = corrcoef(FR.cluster(c1).drift.SpikesPerBin',FR.cluster(c2).drift.SpikesPerBin');
         %FRCor.drift(i).SPB(c1,c2).cov  = cov(FR.cluster(c1).drift.SpikesPerBin,FR.cluster(c2).drift.SpikesPerBin);
         warning on
         
         %Spikes Per Binned Event
         for m=1:size(FR.cluster(1).fixation.SpikesPerBinnedEvent,2)
            c1_data = FR.cluster(c1).fixation.SpikesPerBinnedEvent(:,m);
            c2_data = FR.cluster(c2).fixation.SpikesPerBinnedEvent(:,m);
            %take out NaNs
            c1_data(find(isnan(c1_data))) = [];
            c2_data(find(isnan(c2_data))) = [];
            if ~isempty(c1_data) & size(c1_data,1)>1
               warning off
               c = corrcoef(c1_data, c2_data);
               warning on
               FRCor.fixation(i).SPBE(c1,c2).corrcoef(m) = c(1,2);
               else
               FRCor.fixation(i).SPBE(c1,c2).corrcoef(m) = NaN;
            end
            
         end
         for m=1:size(FR.cluster(1).saccade.SpikesPerBinnedEvent,2)
            c1_data = FR.cluster(c1).saccade.SpikesPerBinnedEvent(:,m);
            c2_data = FR.cluster(c2).saccade.SpikesPerBinnedEvent(:,m);
            %take out NaNs
            c1_data(find(isnan(c1_data))) = [];
            c2_data(find(isnan(c2_data))) = [];
            if ~isempty(c1_data)& size(c1_data,1)>1
               warning off
               c = corrcoef(c1_data, c2_data);
               warning on
               FRCor.saccade(i).SPBE(c1,c2).corrcoef(m) = c(1,2);
               else
               FRCor.saccade(i).SPBE(c1,c2).corrcoef(m) = NaN;
            end
         end
         
         for m=1:size(FR.cluster(1).drift.SpikesPerBinnedEvent,2)
            c1_data = FR.cluster(c1).drift.SpikesPerBinnedEvent(:,m);
            c2_data = FR.cluster(c2).drift.SpikesPerBinnedEvent(:,m);
            %take out NaNs
            c1_data(find(isnan(c1_data))) = [];
            c2_data(find(isnan(c2_data))) = [];
            if ~isempty(c1_data)& size(c1_data,1)>1
               warning off
               c = corrcoef(c1_data, c2_data);
               warning on
               FRCor.drift(i).SPBE(c1,c2).corrcoef(m) = c(1,2);
            else
               FRCor.drift(i).SPBE(c1,c2).corrcoef(m) = NaN;
            end
         end
         
      end	%for j
   end	%for k
   fprintf('bins =[%d %d %d]....   ',bins(i,:))
end


