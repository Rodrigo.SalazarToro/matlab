function ISI = ISIhist(spike)
%ISI = ISIhist(spike)
%
%calculates the InterSpike Interval Histogram(ISI)
%for all spike trains within the spike object.
%
%The ISI is a histogram of the time intervals between 
%adjacent spikes within a spike train(single cell).


ISI.sessionname=spike.data.sessionname;
ISI.groupname = spike.data.groupname;

for i=1:spike.data.numClusters
   ISI.cluster(i).hist=[];
end



for tr = 1:size(spike.data.trial,2)					%loop over trials
   for cl = 1:size(spike.data.trial(tr).cluster,2)
      ISI.cluster(cl).hist = [ISI.cluster(cl).hist diff(spike.data.trial(tr).cluster(cl).spikes)];
   end
end

