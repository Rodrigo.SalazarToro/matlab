function spike = CreateRFSpikes(eye,spike,init_info)
%function spike = CreateRFSpikes(eye,spike,init_info)
%
%Spikes occuring when the RF is outside of the 
%the image boundaries need to be removed.  
%
%Assumptions:
%pwd must be the eye directory
%the RFs are listed sequentially by group number
%the fixation point is the first RF

%get Receptive Field info
%which RF do we use from init_info
rfnum = str2num(spike.groupname);	
if rfnum+1<=init_info.ReceptiveField.numRF  %+1 b/c first rf is fixation point of type 2

[boxX,boxY] = RFEccentricity(init_info.ReceptiveField,rfnum);
    
%now need to check if any of the RF boxes fall outside of the image
%how big is the image
xsize = init_info.x_size;
ysize = init_info.y_size;

%calculate image offset
xoffset = init_info.x_center - xsize/2;
yoffset = init_info.y_center - ysize/2;

dirlist = nptDir('*_eye.0*');
numTrials = size(dirlist,1);


for t = 1:numTrials	%loop over trials
   if t==39
      me=0;
   end
   
   filename = dirlist(t).name;
   [data,num_channels,sampling_rate,datatype,points]=nptReadDataFile(filename);
   
   data(2,:)=data(2,:)-xoffset;		%horizontal is channel 2
   data(1,:)=data(1,:)-yoffset;		%vertical is channel 1
      
   for e=1:size(eye.fixation(t).start,2)	%loop over fixations during this trial
      start = eye.fixation(t).start(e);
      finish = eye.fixation(t).finish(e);
      sect_data = data(:,start:finish);
      
      spike = remove_spikes(t,sect_data,boxX,boxY,xsize,ysize,spike,start,finish);
   end
   
   for e=1:size(eye.saccade(t).start,2)	%loop over saccades during this trial
      start = eye.saccade(t).start(e);
      finish = eye.saccade(t).finish(e);
      sect_data = data(:,start:finish);
      
      spike = remove_spikes(t,sect_data,boxX,boxY,xsize,ysize,spike,start,finish);
    end
    
    for e=1:size(eye.drift(t).start,2)	%loop over drifts during this trial
      start = eye.drift(t).start(e);
      finish = eye.drift(t).finish(e);
      sect_data = data(:,start:finish);
      
      spike = remove_spikes(t,sect_data,boxX,boxY,xsize,ysize,spike,start,finish);
    end
end
else
   fprintf('!!Handmapped Receptive Field not avalable!!\n')
   end