function firingrate = fvtFiringRate(spike,eye,bin_length)
%function firingrate = fvtFiringRate(spike,eye,bin_length)
%
%computes types of firing rates
%
%computes:
%1.)  FiringRate per Event (split each trial into different eye events)
%2.)  FiringRate per Bin  (split each event into time bins)
%3.)  Number of Occurences of Number of Spikes per Bin
%
%the structure containing the eye data (eye) and the session spike train (spike)
%must be passed in.
%
%Usually, the eye structure is saved in the eye folder as a .MAT file and the 
%spike structure is saved in the sort folder as a .MAT File.
%
%bin_length is a 1x3 matrix containing the bin lengths for the fixations, saccades and drifts respectively.
%The default binlength is [20 5 20] in milliseconds.
%
%This function loops over all clusters.



event={'fixation' 'saccade' 'drift'};

firingrate.sessionname = spike.data.sessionname;
firingrate.groupname = spike.data.groupname;

if nargin < 3
   bin_length = [40 10 40];
end
eyeeventspike.bin_length = bin_length;





for type = 1:length(event)
   
   %initializations
   bin_dur = [];
   for cl = 1:spike.data.numClusters						%loop over clusters
      
      %initializations
      FiringRatePerEvent = [];
      cum_SPE = [];
      cumC_SPB = [];
      cumR_SPB = [];
      sum_SPB = 0;
      cum_nnSPB = 0;
      
      for tr = 1:size(spike.data.trial,2)					%loop over trials
         eval(['num_events = size(eye.data.' char(event(type)) '(tr).start,2);'])
         for ev = 1:num_events		%loop over events
            
            eval(['start = eye.data.' char(event(type)) '(tr).start(ev);'])
            eval(['finish = eye.data.' char(event(type)) '(tr).finish(ev);'])
            
            %%%%%%%%%%%%find spikes during this event
            ind = find(spike.data.trial(tr).cluster(cl).spikes > start & spike.data.trial(tr).cluster(cl).spikes <= finish);  %ie. spikes 2,3&4
            
            %%%%%%%%%%%% Firing Rate Per Event(FRPE)
            SPE = length(ind);	%SpikesPerEvent
            cum_SPE = [cum_SPE SPE];
            %FRPE = SPE/(finish-start);
            %FiringRatePerEvent = [FiringRatePerEvent FRPE];														
            
            %%%%%%%%%%%%% put spikes during this event into bins 
            bins = ceil((spike.data.trial(tr).cluster(cl).spikes(ind)-start)/bin_length(1));					%ie. which are in timebins 5,5&6
            
            %get rid of last incomplete bin
            last_bin = floor((finish-start)/bin_length(1));
            bins(find(bins>last_bin))=[];
            %keep track of event durations (all clusters same as cluster #1)
            if cl==1
               bin_dur = [bin_dur last_bin];
            end
            
            
            %%%%%%%%%%%%% find how many Spikes Per Bin(SPB)
            if ~isempty(bins)
               SPB = histc(bins,[1:max(bins)]);				%number of spikes per bin							%ie. 0 0 0 0 2 1
            else
               SPB=0;
            end
            
            
            if length(SPB)< last_bin																			               
               SPB = [SPB zeros(1,last_bin-length(SPB))];															%    0 0 0 0 2 1 0 0
            end
            
            %cumulative Spikes Per Bin 
            sum_SPB = add(sum_SPB,SPB);
            cumR_SPB = [cumR_SPB SPB];
            cumC_SPB = concatenate(cumC_SPB, SPB);

            %%%%%%%%%%%%% how many occurences of spikes per bin(nnSPB)
            nnSPB = histc(SPB,[0:max(SPB)]);		%number of number of spikes per bin					%ie. 6 1 1
            cum_nnSPB = add(cum_nnSPB,nnSPB);
            
         end	%event
      end	%trial
      eval(['firingrate.cluster(cl).' char(event(type)) '.SpikesPerEvent = cum_SPE;'])		%concantonate each event as one long row
      eval(['firingrate.cluster(cl).' char(event(type)) '.SpikesPerBin = cumR_SPB;'])		%concantonate each bin of an event as one long row
		eval(['firingrate.cluster(cl).' char(event(type)) '.SpikesPerBinnedEvent = cumC_SPB;'])	%concantonate each event as a different row
      eval(['firingrate.cluster(cl).' char(event(type)) '.FiringRatePerBin = fvtAverageFiringRate(bin_dur,sum_SPB,bin_length(1));'])	%EyeEventSpike
      eval(['firingrate.cluster(cl).' char(event(type)) '.NumNumSPB = cum_nnSPB;'])   %Firing Rate Probability for each bin (number of occurences of number of spikes per bin of an event)
      
   end	%cluster
   
end
