function CorrelateWrapper(group,clus1,clus2)
%must be in the correct sort directory

%read the spike matfile
dirlist = nptdir('*_spike.mat');
if ~isempty(dirlist)
   spike = load(dirlist(group).name);
   spike=spike.spike;
   %loop through trials
   spktrn1=[];
   spktrn2=[];
   for i=1:size(spike.data.trial,2)
      new = [spike.data.trial(i).cluster(clus1).spikes/1000];
      if size(new,2)>size(spktrn1,2)
         spktrn1 = [spktrn1 zeros(size(spktrn1,1),size(new,2)-size(spktrn1,2))];
      else
         new  = [new zeros(1,size(spktrn1,2)-size(new,2))];
      end
      spktrn1 = [spktrn1 ; new];
      
      
      new = [spike.data.trial(i).cluster(clus2).spikes/1000];
      if size(new,2)>size(spktrn2,2)
         spktrn2 = [spktrn2 zeros(size(spktrn2,1),size(new,2)-size(spktrn2,2))];
      else
         new  = [new zeros(1,size(spktrn2,2)-size(new,2))];
      end
      spktrn2 = [spktrn2 ; new];

   end
end

ccorr(spktrn1,spktrn2,[],'r',[0 .5],1)
