function status = ProcessSessionSpike(varargin)

spiketrainsValue=0;
isiValue=0;
eyeeventspikehistogramsValue=0;
firingratecorrelationsValue=0;
firingrateValue=0;
redoValue=0;

extract_varargin


%should we skip this session
marker = nptDir('skip.txt');
% check for processedsession.txt unless redo is 1
if redoValue==0
    marker=[marker nptDir('*processspiketrains.txt')];
end
if isempty(marker)

    
dirlist = nptdir('eye');
if ~isempty(dirlist)
    cd eye
    eyelist = nptdir('*eyemovement.mat');
    if ~isempty(eyelist)
        load(eyelist(1).name)
    end
    cd ..
end

stimInfo = stiminfo('Auto','save',1);  %do not need
%dirlist = nptDir('*.0001');
%[p,sessionname,e] = fileparts(dirlist(1).name);
sessionname = stimInfo.data.sessionname;

if ~isempty(nptdir('sort'))
    cd sort
    
    hdrdirlist = nptDir('0*.hdr');    %how many groups are there?
    for ii=1:size(hdrdirlist,1)		%loop over groups (electrode channels or tetrodes)
        [p,groupname,e] = fileparts(hdrdirlist(ii).name);
        
        %is there an ispikes file already?
        ispikedirlist = nptdir(['*' groupname '_ispike.mat']);
        ispikedirlist = [ispikedirlist nptdir(['*' groupname '_spike.mat'])];
        
        if spiketrainsValue | isempty(ispikedirlist)    %create
            cutdirlist = nptDir(['*g' groupname 'waveforms.cut']);	%new fileformat after MClust
            gdfdirlist  = nptDir([groupname '1.gdf']);%old file format
            if ~isempty(cutdirlist)
                fprintf('Generating Session Spike Trains for Group # %s ...\n',groupname)
                sp = ispikes(groupname,1);
            elseif ~gdfdirlist
                fprintf('Generating Session Spike Trains for Group # %s ...\n',groupname)
                sp = ispikes(groupname,1,0);
            else
                fprintf('\n!!!Warning!!!!\nGroup # %s has not been sorted yet.\n',groupname)
                continue
            end
            filename = [sessionname 'G' groupname '_ispike' ];
            eval(['save ' filename ' sp'])
            
        else %load
            fprintf('Loading Session Spike Trains for Group # %s ...\n',groupname)
            spike = load(ispikedirlist(1).name);
            f = fieldnames(spike);
            eval(['sp=spike.' char(f) ';'])
        end
        
        if isiValue
            fprintf('Generating ISIs for Group # %s ...\n',groupname)
            ISI = ISIhist(sp);
            filename = [sessionname 'G' groupname '_ISI'];
            eval(['save ' filename ' ISI'])
        end
        
        if exist('em','var')
            %                 fprintf('Receptive Field Spike Train ...\n')
            %                 RF=CreateRFspikes(em,spike,stimInfo.iniInfo);
            %                 RF = ispikes(RF);
            %                 filename = [sessionname 'G' groupname '_spikeRF' ];
            %                 eval(['save ' filename ' RF'])
            
            if eyeeventspikehistogramsValue
                fprintf('Eye Event Spike Histograms ...\n')
                bin_length = [20 5 20];
                eyeeventspike = fvtEyeEventSpikeHistogram(em,sp,bin_length);
                %eyeeventspike contains data for all clusters
                filename = [eyeeventspike.sessionname 'G' eyeeventspike.groupname '_eyeeventspike' ];
                eval(['save ' filename ' eyeeventspike'])
            end
            
            if firingrateValue
                %should be able to combine firing rate stuff more effeciently....
                fprintf('Spike Probability ...\n')
                spikeprob = fvtFiringRate_old(sp,em);
                filename = [spikeprob.sessionname 'G' spikeprob.groupname '_spikeprob' ];
                eval(['save ' filename ' spikeprob'])
            end
            
            if firingratecorrelationsValue
                fprintf('Firing Rate Correlations ...\n')
                FRCor = fvtFRCorrelation(sp,em);
                filename = [FRCor.sessionname 'G' FRCor.groupname '_FRcor' ];
                eval(['save ' filename ' FRCor'])
            end
        end %if eye exists
    end	%loop over groups
end	%if sort folder exists
cd ..
end
status=1;