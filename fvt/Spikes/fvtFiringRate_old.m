function fr = fvtFiringRate(spike,eye,bin_size)
%function fr = fvtFiringRate(spike,eye,bin_size)
%
%  Seperates each eye event into bins and then creates
%  a histogram of the number of spikes per bin.  
%  default bin size is 40 ms

if nargin < 4
   display_flag = 0;
end
if nargin < 3
   bin_size = 40;
end
fr.bin_size = bin_size;

sessionname = spike.data.sessionname;
snIndex = strfind(sessionname,'_highpass');
if ~isempty(snIndex)
    sessionname=sessionname(1:(snIndex-1));
end
fr.sessionname = sessionname;
fr.groupname = spike.data.groupname;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% All Eye Events %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.data.numClusters
   his = 0;	%initialize histogram
   for tr = 1:size(spike.data.trial,2)
      bin = ceil((spike.data.trial(tr).cluster(cl).spikes)/bin_size);
      last_bin = ceil(spike.data.min_duration*1000/bin_size);						%not good b/c we throwaway data at the end of trials
      
      
      if ~isempty(bin)
         n = histc(bin,[1:max(bin)]);	%number of spikes per bin
         if length(n)< last_bin																			               
            n = [n zeros(1,last_bin-length(n))];													
         end
         n = histc(n,[0:max(n)]);		%number of number of spikes per bin
      else
         n = last_bin;
      end
      his = add(his,n);
      
   end
   fr.cluster(cl).all = his;
   end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Fixations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.data.numClusters						%loop over clusters
   his = 0;	%initialize histogram
   for tr = 1:size(spike.data.trial,2)					%loop over trials
      for ev = 1:size(eye.data.fixation(tr).start,2)		%loop over events
         start = eye.data.fixation(tr).start(ev);
         finish = eye.data.fixation(tr).finish(ev);
         %find spikes during this event
         ind = find(spike.data.trial(tr).cluster(cl).spikes > start & spike.data.trial(tr).cluster(cl).spikes <= finish);  %ie. spikes 2,3&4
         %bin these spikes
         bin = ceil((spike.data.trial(tr).cluster(cl).spikes(ind)-start)/bin_size);							%ie. which are in bins 5,5&6
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_size);																%ie. lastbin=8
         
         bin(find(bin>last_bin))=[];
         
         %keep track of event durations
         if cl==1
            bin_dur = [bin_dur last_bin];
         end
         %histogram calculations
         if ~isempty(bin)
            n = histc(bin,[1:max(bin)]);	%number of spikes per bin								%ie. 0 0 0 0 2 1
            if length(n)< last_bin																			               
               n = [n zeros(1,last_bin-length(n))];													%    0 0 0 0 2 1 0 0
            end
            n = histc(n,[0:max(n)]);		%number of number of spikes per bin					%ie. 6 1 1
         else
            n = last_bin;
         end
         his = add(his,n);
      end
   end
   fr.cluster(cl).fixation = his;
   
  end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Drifts %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.data.numClusters						%loop over clusters
   his = 0;	%initialize histogram
   for tr = 1:size(spike.data.trial,2)					%loop over trials
      for ev = 1:size(eye.data.drift(tr).start,2)		%loop over events
         start = eye.data.drift(tr).start(ev);
         finish = eye.data.drift(tr).finish(ev);
         %find spikes during this event
         ind = find(spike.data.trial(tr).cluster(cl).spikes > start & spike.data.trial(tr).cluster(cl).spikes <= finish);
         %bin these spikes
         bin = ceil((spike.data.trial(tr).cluster(cl).spikes(ind)-start)/bin_size);
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_size);
         bin(find(bin>last_bin))=[];
         if ~isempty(bin)
            n = histc(bin,[1:max(bin)]);	%number of spikes per bin
            if length(n)< last_bin																			               
               n = [n zeros(1,last_bin-length(n))];													
            end
            n = histc(n,[0:max(n)]);		%number of number of spikes per bin
         else
            n = last_bin;
         end
         his = add(his,n);
      end
   end
   fr.cluster(cl).drift = his;
   end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Saccades %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_dur = [];
for cl = 1:spike.data.numClusters						%loop over clusters
   his = 0;	%initialize histogram
   for tr = 1:size(spike.data.trial,2)	        %loop over trials
      for ev = 1:size(eye.data.saccade(tr).start,2)		%loop over events
         start = eye.data.saccade(tr).start(ev);
         finish = eye.data.saccade(tr).finish(ev);
         %find spikes during this event
         ind = find(spike.data.trial(tr).cluster(cl).spikes > start & spike.data.trial(tr).cluster(cl).spikes <= finish);
         %bin these spikes
         bin = ceil((spike.data.trial(tr).cluster(cl).spikes(ind)-start)/bin_size);
         %only works for saccades greater than bin size
         %need to do something different here!!!!!!!!!!!
         %get rid of last incomplete bin
         last_bin = floor((finish-start)/bin_size);
         bin(find(bin>last_bin))=[];
         if ~isempty(bin)
            n = histc(bin,[1:max(bin)]);	%number of spikes per bin
            if length(n)< last_bin																			               
               n = [n zeros(1,last_bin-length(n))];													
            end
            n = histc(n,[0:max(n)]);		%number of number of spikes per bin
         else 
            n = last_bin;
         end
         his = add(his,n);
      end
   end
   
   fr.cluster(cl).saccade = his;
   end

