function InspectISIHistogram(ISI,histogram)
%function InspectISIHistogram(ISI,bin_length,time_start,time_stop)
%
%shows the ISI Histograms for all clusters within a group
%the default bin_length is .25 msec





if nargin==1 | isempty(histogram.bin_length)
    bin_length = .25;
else
    bin_length = histogram.bin_length;
end


numClusters = size(ISI.cluster,2);

num_axis = 0;

for i=1:numClusters
    num_axis = num_axis+1;
    if numClusters<10
        subplot('position',[(2+(i-1)*3.5)/((numClusters-1)+4+numClusters*2.5)  .3 2.5/((numClusters-1)+4+numClusters*2.5) .5])
    else
        subplot(2,numClusters/2,i)
    end
    
    %show histogram of ranges
    zoom on
    bins=[0:bin_length:max(ISI.cluster(i).hist)];
    if ~isempty(bins)
        %  hist(log(ISI.cluster(i).hist),bins)
        n=histc(ISI.cluster(i).hist,bins);
        bar(bins,n)

        a=axis;
        if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
            axis([0 1.05*max(bins) a(3) 1.1*max(n)])
        else
            axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
        end
        
        title(['Cluster ' num2str(i)])
        xlabel('time (msecs)')
    else
        plot([],[])
    end
end

h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
title = ['Session:  ' ISI.sessionname 'Group:  ' ISI.groupname];
set(edithandle,'String',title)
set(h,'Name','ISI Histograms')
s.functionname = mfilename;
s.data = ISI;
set(h,'UserData',s);
edithandle = findobj(h,'Tag','bin_length_editbox');
if ~isempty(edithandle)
    set(edithandle,'String',num2str(bin_length))
end
edithandle = findobj(h,'Tag','AxisListbox');
if ~isempty(edithandle)
    num_axis = [1:num_axis]';
    set(edithandle,'String',num2str(num_axis))
end
