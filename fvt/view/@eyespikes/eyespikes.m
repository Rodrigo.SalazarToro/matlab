function es = eyespikes(varargin) %function es = eyespikes(eyemovements,ispikes)

switch nargin
   case 0
      eysp.eyemovements=[];
      eysp.ispikes=[];
      %em = eyemovements(eysp.eyemovements);
		em = eyemovements;
		is = ispikes;
      es = class(eysp,'eyespikes',em,is);
	case 1
		if (isa(varargin{1},'eyespikes'))
			es = varargin{1};
		else
			error('Wrong argument type')
		end
	case 2
      
      eysp.em = varargin{1};
      eysp.is = varargin{2};
      em = eyemovements(eysp.em);
      is = ispikes(eysp.is);
		es = class(eysp,'eyespikes',em,is);
	otherwise
		error('Wrong number of input arguments')
	end
 