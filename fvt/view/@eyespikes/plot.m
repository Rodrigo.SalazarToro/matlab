function vi = plot(vi,n)
%ISPIKES/PLOT Plots interleaved spikes over raw traces
%   OBJ = PLOT(OBJ,N) plots a spike train for each cluster above the 
%   eyemovements data for trial N. The eye data is assumed to be in the current 
%   directory. 
%
%	Dependencies: nptReadStreamerFile.

% get color order list. gcf will use current figure if there is one 
% or create one if there are no figures currently
hfig = gcf;
clist = get(gca,'ColorOrder');
clistsize = size(clist,1);

trial = n;
channel = vi.ispikes.signal;
numTrials = vi.ispikes.numTrials;
numClusters = vi.ispikes.numClusters;
sessionname = vi.ispikes.sessionname;
groupname = vi.ispikes.groupname;
duration = vi.ispikes.duration;


plot(vi.eyemovements,trial);	
hold on
ax = axis;
cplotheight = (ax(4) - ax(3)) * 0.1;
for cluster=1:numClusters
	% draw a line
	lineY = ax(4)+(cluster-1)*cplotheight;
	line(ax(1:2),[lineY lineY],'Color',clist(mod(cluster-1,clistsize)+1,:));
	lineY2 = lineY + cplotheight;
	% draw the spikes
	spikecount = vi.ispikes.trial(trial).cluster(cluster).spikecount;
	for spikenum=1:spikecount
		spiketime = vi.ispikes.trial(trial).cluster(cluster).spikes(spikenum);
		line([spiketime spiketime],[lineY lineY2],'Color',clist(mod(cluster-1,clistsize)+1,:),'LineWidth',2);
	end
end
% shift axis a little bit so we can see the start of the data
% ax = axis;
axis([-25 ax(2) ax(3) lineY2]);
hold off
axish=gca;
titleh = get(axish,'Title');
streamertitle = get(titleh,'String');
title(['Trial: ' streamertitle '     Group: ' groupname]);
% set zoom to on
zoom on
