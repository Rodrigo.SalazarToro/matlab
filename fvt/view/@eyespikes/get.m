function n = get(s,prop_name,n,n2,n3)
%ISPIKES/GET Returns object properties
%   VALUE = GET(OBJ,PROP_NAME,N1,N2,N3) returns an object 
%   property. PROP_NAME can be one of the following:
%
%	gets values from eyemovements class which is a parent
%
%   Dependencies: None.


    n = get(s.eyemovements,prop_name);

