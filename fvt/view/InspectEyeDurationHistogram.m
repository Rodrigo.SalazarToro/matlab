function InspectEyeDurationHistogram(duration_histograms, histogram)
%function InspectEyeDurationHistogram(duration_histograms, histogram)
%
%Plots the histograms of the different eye event durations for a session
%
%The input arguments to the function are the results from fvtEyeDurationHistogram 
%The default bin lengths are 10 and 5 ms for fixations and saccades, respectively.

if nargin==1 | isempty(histogram.bin_length)
    bin_length = [10 5];
else
    bin_length = histogram.bin_length;
end
num_axis=0;


%fixation
if ~isempty(duration_histograms.fixation)
    num_bins=ceil((max(duration_histograms.fixation))/bin_length(1));
    bin_edges = [0:bin_length(1):bin_length(1)*num_bins];
    n=histc(duration_histograms.fixation,bin_edges);
    
    subplot('position',[.05 .3 .4 .5])
    num_axis = num_axis+1;
    bar(bin_edges,n)
    title('fixation');
    ylabel('Number of Occurences')
    zoom on
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(duration_histograms.fixation),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(duration_histograms.fixation),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(duration_histograms.fixation),3)])
end

%saccade
if ~isempty(duration_histograms.saccade)
    num_bins=ceil((max(duration_histograms.saccade))/bin_length(2));
    bin_edges = [0:bin_length(2):bin_length(2)*num_bins];
    n=histc(duration_histograms.saccade,bin_edges);
    
    subplot('position',[.55 .3 .4 .5])
    num_axis = num_axis+1;
    bar(bin_edges,n)
    title('saccade')
    zoom on
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.05*a(2),.95*a(4),['Mean = ' num2str(mean(duration_histograms.saccade),3)])
    text(.05*a(2),.87*a(4),['Median = ' num2str(median(duration_histograms.saccade),3)])
    text(.05*a(2),.8*a(4),['Std = ' num2str(std(duration_histograms.saccade),3)])
end

h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
set(edithandle,'String',duration_histograms.sessionname)
set(h,'Name','Eye Event Duration Histograms')
s.functionname=mfilename;
s.data=duration_histograms;
set(h,'UserData',s);
edithandle = findobj(h,'Tag','bin_length_editbox');
if ~isempty(edithandle)
    set(edithandle,'String',num2str(bin_length))
end
edithandle = findobj(h,'Tag','AxisListbox');
if ~isempty(edithandle)
    num_axis = [1:num_axis]';
    set(edithandle,'String',num2str(num_axis))
end