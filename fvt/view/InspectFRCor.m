function InspectFRCor(FRCor)


num_cells = size(FRCor.fixation(1).SPE,2);




h=gcf;
if isempty(h)
   h=figure
end

counter=0;
c1=0;
for j = 1:num_cells
   c1=c1+1;
   for k = 0:num_cells-j
      c2=c1+k;
      if c1~=c2
         counter = counter+1;
         if num_cells<6
               subplot(2,ceil((num_cells*(num_cells-1))/4),counter)
            else
               subplot(3,ceil((num_cells*(num_cells-1))/6),counter)
            end

         title(['Cells ' num2str(c1) ' and ' num2str(c2)])
         ylabel('Correlation Coefficient')
         xlabel('Bin Length (msec)')
         y=[];
         x=[];
         hold on
         for m=1:size(FRCor.bins,1)
            y=[y FRCor.fixation(m).SPB(c1,c2).corrcoef(1,2)];
            x=[x FRCor.bins(m,1)];
         end
         plot(x,y,'g*-')
         axis tight
			ax = axis;
         axis([ax(1) ax(2) -.1 1])
         y=[];
         x=[];
         for m=1:size(FRCor.bins,1)
            y=[y FRCor.saccade(m).SPB(c1,c2).corrcoef(1,2)];
            x=[x FRCor.bins(m,1)];
         end
         plot(x,y,'r*-')

         y=[];
         x=[];
         for m=1:size(FRCor.bins,1)
            y=[y FRCor.drift(m).SPB(c1,c2).corrcoef(1,2)];
            x=[x FRCor.bins(m,1)];
         end
         plot(x,y,'b*-')
			line([ax(1) ax(2)],[FRCor.fixation(1).SPE(c1,c2).corrcoef(1,2) FRCor.fixation(1).SPE(c1,c2).corrcoef(1,2)],'Color',[0 1 0]);
         			         line([ax(1) ax(2)],[FRCor.saccade(1).SPE(c1,c2).corrcoef(1,2) FRCor.saccade(1).SPE(c1,c2).corrcoef(1,2)],'Color',[1 0 0]);
line([ax(1) ax(2)],[FRCor.drift(1).SPE(c1,c2).corrcoef(1,2) FRCor.drift(1).SPE(c1,c2).corrcoef(1,2)],'Color',[0 0 1]);

                  legend('fixation','saccade','drift')
      end
      
   end
end

h=gcf;
handle=findobj(h,'Tag','Titlebox');
titletext = ['session: ' FRCor.sessionname ' group: ' FRCor.groupname];
set(handle,'String',titletext)
set(h,'Name','Spikes Per Bin Correlations')
