function n = get(em,prop_name)
%REVCORR/GET Returns object properties
%   VALUE = GET(OBJ,PROP_NAME) returns an object property. PROP_NAME 
%   can be one of the following:
%     'Sessionname' - name of session.
%		'Group'
%		'Clusters'
%		'R'
%		'Pspike'
%		'BinSize'
%		'WindowSize'
%		'Cols'
%		'Rows'
%		'NumOrientations'
%		'BarOrientation'
%		'NumColors'
%      
%   Dependencies: None.

switch prop_name
case 'Sessionname'
   n = em.sessionname;
   case 'Groupname'
   n = em.groupname;
case 'Clusters'
   n = em.clusters;
   case 'BinSize'
   n = em.bin_size;
   case 'WindowSize'
   n = em.window_size;
case 'R'
   n = em.R;
case 'Pspike'
   n = em.Pspike;
   case 'Rows'
   n = em.clusters;
case 'Cols'
   n = em.clusters;
case 'NumOrientations'
   n = em.clusters;
case 'BarOrientation'
   n = em.clusters;
case 'NumColors'
   n = em.clusters;

otherwise
	n = get(em.eyes,prop_name);

end
