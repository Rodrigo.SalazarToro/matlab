function rc = revcorr(varargin)
%REVCORR Constructor function for the REVCORR class
%   RC = REVCORR(SPIKE, INIT_INFO) instantiates an REVCORR
%   object that contains the results of the reverse corrleation 
%	 of the spike train with the stimulus.
%
%   SPIKE is the ispikes object containing the spike 
%   trains for a particular group.
%   INIT_INFO is the init_info structure for this session.
%	 Assumes we are in the session folder
%    
%   RC is a structure with the following fields:
%   	RC.sessionname
%	 	RC.groupname
%   	RC.clusters
%   	RC.R
%   	RC.Pspike
%		RC.bin_size
%		RC.window_size
%   	
%   Dependencies: revcorr_sn.

switch nargin
case 0
   r.bin_size=0;
   r.window_size=0;
   r.R=[];
   r.Pspike=[];
   r.P=[];
   r.sessionname = '';
   r.groupname='';
   r.clusters=0;
   r.rows=0;
	r.cols=0;
	r.num_orientations=0;
	r.bar_orientation=[];
	r.num_colors=0;
   rc = class(r,'revcorr');
   
case 1
   if (isa(varargin{1},'revcorr'))
      rc = varargin{1};
   end
   
case 2
   r = fvtGenerateRevcorr_SN(varargin{:});
   rc = class(r,'revcorr');
   
otherwise
   error('Wrong number of input arguments')
end

   
