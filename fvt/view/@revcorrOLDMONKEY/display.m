function display(em)
%revcorr/DISPLAY Command window display of a reverse correlation object
%
%   Dependencies: None.

fprintf('\n%s =\n',inputname(1));
fprintf('revcorr object from %s with fields:\n',em.sessionname); 
fprintf('\tbin_size\n');
fprintf('\twindow_size\n');
fprintf('\tR\n');
fprintf('\tP\n');
fprintf('\tPspike\n');
fprintf('\tsessionname\n');
fprintf('\tgroupname\n');
fprintf('\tclusters\n');
fprintf('\trows\n');
fprintf('\tcols\n');
fprintf('\tnum_orientations\n');
fprintf('\tbar_orientation\n');
fprintf('\tnum_colors\n');

