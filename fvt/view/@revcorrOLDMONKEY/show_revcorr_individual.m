% show_revcorr_sn.m -  static display of revcorr_sn as individual frames
%
% function show_revcorr_sn(rc,cluster,color,orientation,step_number,bins_per_step,display_mode,interp_mode)
%

function show_revcorr_individual(rc,cluster,color,orientation,step_number,bins_per_step,display_mode,interp_mode)

if ~exist('interp_mode','var')
   interp_mode='flat';
end
if ~exist('display_mode','var')
   display_mode = 'R';
end

switch display_mode
case 'R'
   R=rc.R(:,:,cluster);
case 'P'
   R=rc.P(:,:,cluster);	%P is result from bar_prob.m
case 'R/P'
   [i,j] = find(rc.P(:,:,cluster)==0);
   index = sub2ind(size(rc.P(:,:,cluster)),i,j);
   % warning off
   R = rc.R(:,:,cluster)./rc.P(:,:,cluster);
   %warning backtrace
   %R(index) = 0;
end


% revcorr dimensions
[M N]=size(R);
R=R(1:M-1,:);   % get rid of last row, which is outside box
M=M-1;

num_steps=floor(N/bins_per_step);

% normalize R matrix
R = R/max(R(:));  

% get init_info variables
rows = rc.rows;
cols = rc.cols;
[row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);
nrc = rows*cols;
num_or = rc.num_orientations;
angle = rc.bar_orientation;
num_colors = rc.num_colors;

% revcorr map
map=zeros(rows+1,cols+1);

% center pos
cx=floor(cols/2);
cy=rows-1-floor(rows/2);

% axis size
sz=1.2*max([rows cols]);

color=color-1; %because index starts at zero
if color==0
   colortext='dark';
else
   colortext='light';
end


orientation = orientation -1; %because index starts at zero
group=color*num_or + orientation;
i=group*nrc + [1:nrc];

j=(num_steps-step_number)*bins_per_step+[1:bins_per_step];
r=reshape(mean(R(i,j),2),rows,cols);


map(1:rows,1:cols)=r(rows:-1:1,:);
if strcmp(interp_mode,'flat')
   h=pcolor([0:cols]-0.5,[0:rows]-0.5,map);
else
   h=pcolor([0:cols],[0:rows],map);
end

axis image off
axis([cx-sz/2 cx+(sz)/2 cy-(sz)/2 cy+sz/2])
caxis([0 1])
shading(interp_mode)
%set(h,'LineStyle','none')
rotate(h,[0 90],angle(orientation+1)-90,[cx cy 1])
%title([rc.sessionname '  Group: ' rc.groupname '  Cluster: ' num2str(cluster) ...
%      ' Luminance: ' colortext '  Orientation: ' num2str(rc.bar_orientation(orientation+1)) ' degrees'...
%      ' TimeRange: ' num2str((step_number-1)*bins_per_step) '-' num2str(step_number*bins_per_step) ' msec']);

title = ([rc.sessionname '  Group: ' rc.groupname '  Cluster: ' num2str(cluster) ...
      ' Luminance: ' colortext '  Orientation: ' num2str(rc.bar_orientation(orientation+1)) ' degrees'...
      ' TimeRange: ' num2str((step_number-1)*bins_per_step) '-' num2str(step_number*bins_per_step) ' msec']);
h=gcf;
eh=findobj(h,'Tag','Title_box');
set(eh,'String',title)
eh=findobj(h,'Tag','DegreeTextbox');
text= {['Rows span ' num2str(row_degrees,2) ' degrees'],['Columns span ' num2str(col_degrees,2) ' degrees']};
set(eh,'String',text)
%hcb=findobj('Tag','Colorbar');
colorbar%(hcb)
