function b = subsref(obj, index)

switch index(1).type
case '.'
   switch index(1).subs
      
   case 'sessionname'
      b = obj.sessionname;
   case 'groupname'
      b = obj.groupname;
   case 'clusters'
      b = obj.clusters;
   case 'bin_size'
      b = obj.bin_size;
	case 'window_size'
      b = obj.window_size;
   case 'R'
      b = obj.R;
      case 'P'
      b = obj.P;
   case 'Pspike'
      b = obj.Pspike;
    case 'rows'
      b = obj.rows;
	case 'cols'
      b = obj.cols;
	case 'num_orientations'
      b = obj.num_orientations;
   case 'bar_orientation'
      switch length(index)
         case 1
            b = obj.bar_orientation;
         case 2
            b = obj.bar_orientation(index(2).subs{1});
         end
  	case 'num_colors'
      b = obj.num_colors;
   otherwise 
      error('Invalid field name')
   end
end


