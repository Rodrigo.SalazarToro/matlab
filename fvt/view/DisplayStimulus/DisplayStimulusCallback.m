function DisplayStimulusCallback(action)

%function DisplayStimulusCallback(action)
%
%  callback function for Display Stimulus Gui
% 	


h=gcf;
s = get(h,'UserData');
%FrameAxes = findobj(h,'Tag','FrameAxes');
%RFAxes = findobj(h,'Tag','RFAxes');
%SpikeAxes = findobj(h,'Tag','SpikeAxes');

switch(action)
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
case('GetData')
   %callback function for the GetData button
   %on the Display Stimulus Gui
   %prompts user for location of data
   %and stores info in GUI UserData
   
   if size(dir(s.spikedir),1)~=0
      cd (s.spikedir) 
      f=1
   end
   
   %get spike train
   [s.spike_filename,s.spikedir] = uigetfile('*spike.mat','Select Spike Train File');
      
   %get eye directory
   cd(s.spikedir)
   cd ('..')
   if ispresent('eye','dir')
       cd ('eye')
       s.eyedir = nptPWD;
       dirlist = nptDir('*_eye.0*');
       s.numTrials = size(dirlist,1);
       cd ..
   else
       s.eyedir=[];
       s.numTrials =1; %cat data
   end
   s.sessionname = s.spike_filename(1:length(s.spike_filename)-16) ;
   s.groupname = s.spike_filename(length(s.spike_filename)-13:length(s.spike_filename)-10);
   edithandle = findobj(h,'Tag','Titlebox');
   title =['Session: ' s.sessionname '  Group: ' s.groupname];
   set(edithandle,'String',title)
   s.extrasyncs_file = [s.sessionname , '_extra_syncs.txt'];
   
   s.init_file = [s.sessionname '.ini'];
   
   s.init_info = ReadIniFileWrapper(s.init_file);
   str=[];
   for i=1:s.init_info.ReceptiveField.numRF-1
      eval(['str=[str {''' num2str(i) '''}];'])
   end
   edithandle = findobj(h,'Tag','RFListbox');
   set(edithandle,'String',str)
   %and initialize to 1
   s.rf = 1;	
   set(edithandle,'Value',s.rf)
   
   edithandle = findobj(h,'Tag','WindowSize');
   s.WindowSize = str2num(get(edithandle,'String'));
   
   
   %Has the Stimulus Path changed since last time?
   question={['Are the Stimulus Files located at ' s.stimdir]};
   buttonname = questdlg(question,'?Get Stimulus Directory?',{'Yes','No'});
   %get the Stimulus Path
   if strcmp(buttonname,'No')   
      s.stimdir = uigetfolder('Browse for Stimulus Directory');
   end
   cd (s.stimdir);
   [s.stimheader] = ReadStimulusHeader(s.init_info);
   
   %initializations
   s.trial = 0;
   s.frame = 0;
   
   
   edithandle = findobj(h,'Tag','FrameNum');
   set(edithandle,'String',s.frame)
   set(h,'UserData',s)
   
   %increment trial number
   DisplayStimulusCallback('NextTrial')
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case('NextTrial')
   if s.trial<s.numTrials
      s.trial = s.trial +1;
      s.frame = 0;
      
      s = update_trial(s);
      
      edithandle = findobj(h,'Tag','Trialnum');
      set(edithandle,'String',s.trial)
      set(h,'UserData',s)
      
      DisplayStimulusCallback('NextFrame')
   end
   
   
case('PreviousTrial')
   if s.trial<s.numTrials
      s.trial = s.trial -1;
      s.frame = 0;
      
      s = update_trial(s);
      
      edithandle = findobj(h,'Tag','Trialnum');
      set(edithandle,'String',s.trial)
      set(h,'UserData',s)
      
      DisplayStimulusCallback('NextFrame')
   end
   
case('Trialnum')
   edithandle = findobj(h,'Tag','Trialnum');
   trial = str2num(get(edithandle,'String'));
   if trial>1 & trial<s.numTrials
      s.trial = trial;
      s.frame = 0;
      s = update_trial(s);
      set(h,'UserData',s)
      
      DisplayStimulusCallback('NextFrame')
   end
   edithandle = findobj(h,'Tag','Trialnum');
   set(edithandle,'String',s.trial)
   
   
case('NextFrame')
   if s.frame<size(s.points,2) & s.frame>=0
      s.frame = s.frame +1;
      edithandle = findobj(h,'Tag','Framenum');
      set(edithandle,'String',s.frame)
      
      [Frame,RFFrame] = CreateFrame(s);
      axes(s.FrameAxes)
      image(Frame)
      axes(s.RFAxes)
      image(RFFrame)
      ShowEyeSpike(s)
      
   end
   set(h,'UserData',s)
   
   
case('PreviousFrame')
   
   if s.frame<=size(s.points,2) & s.frame>1
      s.frame = s.frame - 1;
      
      [Frame,RFFrame] = CreateFrame(s);
      axes(s.FrameAxes)
      image(Frame)
      axes(s.RFAxes)
      image(RFFrame)
      ShowEyeSpike(s)

      
      
      edithandle = findobj(h,'Tag','Framenum');
      set(edithandle,'String',s.frame)
   end
   set(h,'UserData',s)
   
case('Framenum')
   edithandle = findobj(h,'Tag','Framenum');
   frame = str2num(get(edithandle,'String'))
   if s.frame<=size(s.points,2) & s.frame>=1
      s.frame = frame;
      [Frame,RFFrame] = CreateFrame(s);
      axes(s.FrameAxes)
      image(Frame)
      axes(s.RFAxes)
      image(RFFrame)
      ShowEyeSpike(s)
      
      
   end
   edithandle = findobj(h,'Tag','Framenum');
   set(edithandle,'String',s.frame)
   set(h,'UserData',s)
   
   
case('PlayMovie')
   for i=1:size(s.points,2)
      s.frame=0;
      DisplayStimulusCallback('NextFrame')
      drawnow
   end
   
   
case('WindowSize')
   edithandle = gcbo;
   s.WindowSize = str2num(get(edithandle,'String'));
   set(h,'UserData',s)
   ShowSpike(s)
   
case('RFnum')
   edithandle = gcbo;
   str = get(edithandle,'String');
   val = get(edithandle,'Value');
   s.rf = str2num(char(str(val)));
   
   [Frame,RFFrame] = CreateFrame(s);
   axes(s.FrameAxes)
   image(Frame)
   axes(s.RFAxes)
   image(RFFrame)
   ShowEyeSpike(s)
   set(h,'UserData',s)
   
case 'Load'
   cd (s.spikedir)
   [filename,path] = uigetfile('*.*','Select data file');
   if(filename~=0)
      cd(path);
      obj = CreateDataObject(filename);
   end
   
case('Quit')
   closereq
end

