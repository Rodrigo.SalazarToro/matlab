function fig = DisplayStimulus()
% This is the machine-generated representation of a Handle Graphics object
% and its children.  Note that handle values may change when these objects
% are re-created. This may cause problems with any callbacks written to
% depend on the value of the handle at the time the object was saved.
% This problem is solved by saving the output as a FIG-file.
%
% To reopen this object, just type the name of the M-file at the MATLAB
% prompt. The M-file and its associated MAT-file must be on your path.
% 
% NOTE: certain newer features in MATLAB may not have been saved in this
% M-file due to limitations of this format, which has been superseded by
% FIG-files.  Figures which have been annotated using the plot editor tools
% are incompatible with the M-file/MAT-file format, and should be saved as
% FIG-files.

load DisplayStimuluss %changed name b/c matlab gets confused w/ same name as directory

h0 = figure('Units','normalized', ...
	'Color',[0.8 0.8 0.8], ...
	'Colormap',mat0, ...
	'DoubleBuffer','on', ...
	'FileName','C:\MATLABR11\work\fvt\view\DisplayStimulus\DisplayStimulus.m', ...
	'Name','Display Stimulus', ...
	'PaperPosition',[18 180 576 432], ...
	'PaperUnits','points', ...
	'Position',[0 0.017578125 1 0.90625], ...
	'Renderer','zbuffer', ...
	'RendererMode','manual', ...
	'ResizeFcn','doresize(gcbf)', ...
	'Tag','Fig1', ...
	'ToolBar','figure', ...
	'UserData',mat1, ...
	'DefaultaxesCreateFcn','plotedit(gcbf,''promoteoverlay''); ');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeHGBinObject', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeFigObjStorage', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeHGBinObject', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeHGBinObject', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeFigObjStorage', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeHGBinObject', ...
	'Visible','off');
h1 = uimenu('Parent',h0, ...
	'HandleVisibility','off', ...
	'Tag','ScribeFigObjStorage', ...
	'Visible','off');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[1 1 1], ...
	'Callback','DisplayStimulusCallback RFnum', ...
	'FontSize',0.0927536231884058, ...
	'Position',[0.9164062500000001 0.1649269311064718 0.05703125000000001 0.1242171189979123], ...
	'String',mat2, ...
	'Style','listbox', ...
	'Tag','RFListbox', ...
	'Value',1);
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback NextFrame', ...
	'FontSize',0.4444444444444445, ...
	'ListboxTop',0, ...
	'Position',[0.8078124999999999 0.2473903966597077 0.09140625000000001 0.03966597077244258], ...
	'String','Next Frame', ...
	'Tag','Pushbutton1');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback PreviousFrame', ...
	'FontSize',0.380952380952381, ...
	'ListboxTop',0, ...
	'Position',[0.65625 0.2400835073068893 0.08828125000000001 0.04384133611691023], ...
	'String','Previous Frame', ...
	'Tag','Pushbutton2');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback NextTrial', ...
	'FontSize',0.3902439024390244, ...
	'ListboxTop',0, ...
	'Position',[0.8132812500000001 0.1847599164926931 0.09218750000000001 0.04384133611691023], ...
	'String','Next Trial', ...
	'Tag','Pushbutton3');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback PreviousTrial', ...
	'FontSize',0.4444444444444445, ...
	'ListboxTop',0, ...
	'Position',[0.6570312500000001 0.1816283924843424 0.08671875000000001 0.03966597077244258], ...
	'String','Previous Trial', ...
	'Tag','Pushbutton4');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.8 0.8 0.8], ...
	'FontSize',0.6666666666666666, ...
	'ListboxTop',0, ...
	'Position',[0.9070312500000001 0.2898706896551724 0.07578125000000001 0.01724137931034483], ...
	'String','Receptive Field #', ...
	'Style','text', ...
	'Tag','StaticText1');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback Quit', ...
	'FontSize',0.5, ...
	'ListboxTop',0, ...
	'Position',[0.93359375 0.009698275862068966 0.05390625 0.03448275862068966], ...
	'String','Quit', ...
	'Tag','Pushbutton5');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback Load', ...
	'FontSize',0.4705882352941177, ...
	'ListboxTop',0, ...
	'Position',[0.8734375000000001 0.01077586206896552 0.0546875 0.03663793103448276], ...
	'String','Load', ...
	'Tag','Pushbutton6');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback GetData', ...
	'FontSize',0.32, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.75 0.1142241379310345 0.06796875000000002 0.04310344827586207], ...
	'String','Get Data', ...
	'Tag','Pushbutton7');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[1 1 1], ...
	'Callback','DisplayStimulusCallback Framenum', ...
	'FontSize',0.3137254901960785, ...
	'ListboxTop',0, ...
	'Position',[0.7570312500000002 0.2564655172413793 0.0359375 0.02586206896551724], ...
	'String','1', ...
	'Style','edit', ...
	'Tag','Framenum');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[1 1 1], ...
	'Callback','DisplayStimulusCallback Trialnum', ...
	'FontSize',0.2882882882882883, ...
	'ListboxTop',0, ...
	'Position',[0.7601562500000001 0.197198275862069 0.04062500000000001 0.0290948275862069], ...
	'String','1', ...
	'Style','edit', ...
	'Tag','Trialnum');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.8 0.8 0.8], ...
	'FontSize',0.5, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.7531250000000002 0.2877155172413793 0.04453125 0.02370689655172414], ...
	'String','Trial', ...
	'Style','text', ...
	'Tag','StaticText2');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[1 1 1], ...
	'Callback','DisplayStimulusCallback WindowSize', ...
	'FontSize',0.64, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.8851562500000001 0.7165948275862069 0.03671875 0.02693965517241379], ...
	'String','300', ...
	'Style','edit', ...
	'Tag','WindowSize');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.8 0.8 0.8], ...
	'FontSize',0.6666666666666666, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.8 0.7144396551724138 0.08437500000000001 0.02586206896551724], ...
	'String','Window Size:', ...
	'Style','text', ...
	'Tag','StaticText3');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.8 0.8 0.8], ...
	'FontSize',0.7272727272727273, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.925 0.7155172413793104 0.0421875 0.02370689655172414], ...
	'String','msecs', ...
	'Style','text', ...
	'Tag','StaticText4');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'BackgroundColor',[0.8 0.8 0.8], ...
	'FontSize',12, ...
	'FontWeight','bold', ...
	'ForegroundColor',[1 0 0], ...
	'ListboxTop',0, ...
	'Position',[0.53515625 0.7295258620689655 0.08515625 0.01831896551724138], ...
	'String','Current Time', ...
	'Style','text', ...
	'Tag','StaticText5');
h1 = uicontextmenu('Parent',h0, ...
	'Callback','domymenu update', ...
	'HandleVisibility','off', ...
	'Tag','ScribeEditlineObjContextMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu cut', ...
	'HandleVisibility','off', ...
	'Label','Cu&t', ...
	'Tag','ScribeEditlineObjCutMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu copy', ...
	'HandleVisibility','off', ...
	'Label','&Copy', ...
	'Tag','ScribeEditlineObjCopyMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu paste', ...
	'HandleVisibility','off', ...
	'Label','&Paste', ...
	'Tag','ScribeEditlineObjPasteMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu clear', ...
	'HandleVisibility','off', ...
	'Label','Clea&r', ...
	'Tag','ScribeEditlineObjClearMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu size', ...
	'HandleVisibility','off', ...
	'Label','Line &Width', ...
	'Separator','on', ...
	'Tag','ScribeEditlineObjSizeMenu');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 0.5', ...
	'HandleVisibility','off', ...
	'Label','0.5', ...
	'Tag','ScribeEditlineObjSizeMenu0.5');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 1', ...
	'HandleVisibility','off', ...
	'Label','1', ...
	'Tag','ScribeEditlineObjSizeMenu1');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 2', ...
	'HandleVisibility','off', ...
	'Label','2', ...
	'Tag','ScribeEditlineObjSizeMenu2');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 3', ...
	'HandleVisibility','off', ...
	'Label','3', ...
	'Tag','ScribeEditlineObjSizeMenu3');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 4', ...
	'HandleVisibility','off', ...
	'Label','4', ...
	'Tag','ScribeEditlineObjSizeMenu4');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 5', ...
	'HandleVisibility','off', ...
	'Label','5', ...
	'Tag','ScribeEditlineObjSizeMenu5');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 6', ...
	'HandleVisibility','off', ...
	'Label','6', ...
	'Tag','ScribeEditlineObjSizeMenu6');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 7', ...
	'HandleVisibility','off', ...
	'Label','7', ...
	'Tag','ScribeEditlineObjSizeMenu7');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 8', ...
	'HandleVisibility','off', ...
	'Label','8', ...
	'Tag','ScribeEditlineObjSizeMenu8');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 9', ...
	'HandleVisibility','off', ...
	'Label','9', ...
	'Tag','ScribeEditlineObjSizeMenu9');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu size 10', ...
	'HandleVisibility','off', ...
	'Label','10', ...
	'Tag','ScribeEditlineObjSizeMenu10');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu more', ...
	'HandleVisibility','off', ...
	'Label','more...', ...
	'Tag','ScribeEditlineObjSizeMenuMore');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu style', ...
	'HandleVisibility','off', ...
	'Label','Line &Style', ...
	'Tag','ScribeEditlineObjStyleMenu');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu style solid', ...
	'HandleVisibility','off', ...
	'Label','solid', ...
	'Tag','ScribeEditlineObjStyleMenuSolid');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu style dash', ...
	'HandleVisibility','off', ...
	'Label','dash', ...
	'Tag','ScribeEditlineObjStyleMenuDash');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu style dot', ...
	'HandleVisibility','off', ...
	'Label','dot', ...
	'Tag','ScribeEditlineObjStyleMenuDot');
h3 = uimenu('Parent',h2, ...
	'Callback','domymenu style dashdot', ...
	'HandleVisibility','off', ...
	'Label','dash-dot', ...
	'Tag','ScribeEditlineObjStyleMenuDashDot');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu color', ...
	'HandleVisibility','off', ...
	'Label','Color...', ...
	'Tag','ScribeEditlineObjColorMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu more', ...
	'HandleVisibility','off', ...
	'Label','Properties...', ...
	'Separator','on', ...
	'Tag','ScribeEditlineObjMoreMenu');
h1 = uicontextmenu('Parent',h0, ...
	'Callback','domymenu update', ...
	'HandleVisibility','off', ...
	'Tag','ScribeAxisObjContextMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu cut', ...
	'HandleVisibility','off', ...
	'Label','Cu&t', ...
	'Tag','ScribeAxisObjCutMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu copy', ...
	'HandleVisibility','off', ...
	'Label','&Copy', ...
	'Tag','ScribeAxisObjCopyMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu paste', ...
	'HandleVisibility','off', ...
	'Label','&Paste', ...
	'Tag','ScribeAxisObjPasteMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu clear', ...
	'HandleVisibility','off', ...
	'Label','Clea&r', ...
	'Tag','ScribeAxisObjClearMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu showlegend', ...
	'HandleVisibility','off', ...
	'Label','Show Legend', ...
	'Separator','on', ...
	'Tag','ScribeAxisObjShowLegendMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu moveresize', ...
	'HandleVisibility','off', ...
	'Label','Lock Position', ...
	'Separator','on', ...
	'Tag','ScribeAxisObjMoveResizeMenu');
h2 = uimenu('Parent',h1, ...
	'Callback','domymenu more', ...
	'HandleVisibility','off', ...
	'Label','Properties...', ...
	'Separator','on', ...
	'Tag','ScribeAxischildObjMoreMenu');
h1 = uicontrol('Parent',h0, ...
	'Units','normalized', ...
	'FontUnits','normalized', ...
	'BackgroundColor',[0.831372549019608 0.815686274509804 0.784313725490196], ...
	'Callback','DisplayStimulusCallback PlayMovie', ...
	'FontSize',0.4444444444444445, ...
	'FontWeight','bold', ...
	'ListboxTop',0, ...
	'Position',[0.7484375000000001 0.05818965517241379 0.0734375 0.03879310344827586], ...
	'String','Play Movie', ...
	'Tag','Pushbutton8');
h1 = axes('Parent',h0, ...
	'Box','on', ...
	'CameraUpVector',[0 1 0], ...
	'CameraUpVectorMode','manual', ...
	'Color',[1 1 1], ...
	'ColorOrder',mat3, ...
	'CreateFcn','plotedit(gcbf,''promoteoverlay''); ', ...
	'Position',[0.03125 0.03663793103448276 0.6039062500000001 0.6842672413793104], ...
	'Tag','FrameAxes', ...
	'XColor',[0 0 0], ...
	'YColor',[0 0 0], ...
	'ZColor',[0 0 0]);
h2 = line('Parent',h1, ...
	'Color',[0 0 1], ...
	'Tag','Line1', ...
	'XData',0, ...
	'YData',0);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[-0.002590673575129432 -1.075709779179811 17.32050807568877], ...
	'Tag','Text4', ...
	'VerticalAlignment','cap');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[-1.085492227979275 -0.003154574132492316 17.32050807568877], ...
	'Rotation',90, ...
	'Tag','Text3', ...
	'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','right', ...
	'Position',[-1.106217616580311 1.813880126182965 17.32050807568877], ...
	'Tag','Text2', ...
	'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat4, ...
	'Tag','Text1', ...
	'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);
h1 = axes('Parent',h0, ...
	'Box','on', ...
	'CameraUpVector',[0 1 0], ...
	'CameraUpVectorMode','manual', ...
	'Color',[1 1 1], ...
	'ColorOrder',mat5, ...
	'CreateFcn','plotedit(gcbf,''promoteoverlay''); ', ...
	'FontSize',9.999999999999998, ...
	'Position',[0.7234375000000001 0.3351293103448276 0.24921875 0.3760775862068965], ...
	'Tag','RFAxes', ...
	'XColor',[0 0 0], ...
	'YColor',[0 0 0], ...
	'ZColor',[0 0 0]);
h2 = line('Parent',h1, ...
	'Color',[0 0 1], ...
	'Tag','Axes2Line1', ...
	'XData',0, ...
	'YData',0);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat6, ...
	'Tag','Axes2Text4', ...
	'VerticalAlignment','cap');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat7, ...
	'Rotation',90, ...
	'Tag','Axes2Text3', ...
	'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','right', ...
	'Position',[-6.830188679245283 2.53448275862069 17.32050807568877], ...
	'Tag','Axes2Text2', ...
	'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat8, ...
	'Tag','Axes2Text1', ...
	'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);
h1 = axes('Parent',h0, ...
	'Box','on', ...
	'CameraUpVector',[0 1 0], ...
	'CameraUpVectorMode','manual', ...
	'Color',[1 1 1], ...
	'ColorOrder',mat9, ...
	'CreateFcn','plotedit(gcbf,''promoteoverlay''); ', ...
	'FontSize',9.999999999999998, ...
	'Position',[0.03359375 0.7607758620689655 0.9390625 0.2058189655172414], ...
	'Tag','SpikeAxes', ...
	'XColor',[0 0 0], ...
	'YColor',[0 0 0], ...
	'ZColor',[0 0 0]);
h2 = line('Parent',h1, ...
	'Color',[0 0 1], ...
	'Tag','Line1', ...
	'XData',0, ...
	'YData',0);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[-0.0008326394671106518 -1.252631578947368 17.32050807568877], ...
	'Tag','Text4', ...
	'VerticalAlignment','cap');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat10, ...
	'Rotation',90, ...
	'Tag','Text3', ...
	'VerticalAlignment','baseline');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','right', ...
	'Position',mat11, ...
	'Tag','Text2', ...
	'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',mat12, ...
	'Tag','Text1', ...
	'VerticalAlignment','bottom');
set(get(h2,'Parent'),'Title',h2);
h1 = axes('Parent',h0, ...
	'CameraUpVector',[0 1 0], ...
	'CameraUpVectorMode','manual', ...
	'Color','none', ...
	'ColorOrder',mat13, ...
	'CreateFcn','', ...
	'HandleVisibility','off', ...
	'HitTest','off', ...
	'Position',[-0.00078125 -0.06788793103448276 1 1], ...
	'Tag','ScribeOverlayAxesActive', ...
	'Visible','off', ...
	'XColor',[0.8 0.8 0.8], ...
	'XLimMode','manual', ...
	'XTickMode','manual', ...
	'YColor',[0.8 0.8 0.8], ...
	'YLimMode','manual', ...
	'YTickMode','manual', ...
	'ZColor',[0 0 0]);
h2 = line('Parent',h1, ...
	'ButtonDownFcn','scriberestoresavefcns', ...
	'Color',[0 0 0], ...
	'LineStyle','none', ...
	'Tag','ScribeArrowlineObject', ...
	'UserData','1.090972e+002', ...
	'XData',[0.5343749999999999 0.5343749999999999], ...
	'YData',[0.7579615398459441 0.8173277661795413]);
h2 = line('Parent',h1, ...
	'Color',[0 0 0], ...
	'Tag','ScribeArrowlineBody', ...
	'UserData','1.090972e+002', ...
	'XData',[0.5343749999999999 0.5343749999999999], ...
	'YData',[0.7579615398459441 0.8087070765243689]);
h2 = patch('Parent',h1, ...
	'FaceColor',[0 0 0], ...
	'Faces',[1 2 3 4 5], ...
	'NormalMode','manual', ...
	'Tag','ScribeArrowlineHead', ...
	'UserData','1.090972e+002', ...
	'VertexNormals',mat14, ...
	'Vertices',mat15);
h2 = text('Parent',h1, ...
	'Color',[0.8 0.8 0.8], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[0.4996090695856138 -0.008629989212513456 17.32050807568877], ...
	'VerticalAlignment','cap', ...
	'Visible','off');
set(get(h2,'Parent'),'XLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0.8 0.8 0.8], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[-0.004691164972634871 0.4983818770226537 17.32050807568877], ...
	'Rotation',90, ...
	'VerticalAlignment','baseline', ...
	'Visible','off');
set(get(h2,'Parent'),'YLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','right', ...
	'Position',mat16, ...
	'Visible','off');
set(get(h2,'Parent'),'ZLabel',h2);
h2 = text('Parent',h1, ...
	'Color',[0 0 0], ...
	'HandleVisibility','off', ...
	'HorizontalAlignment','center', ...
	'Position',[0.4996090695856138 1.007551240560949 17.32050807568877], ...
	'VerticalAlignment','bottom', ...
	'Visible','off');
set(get(h2,'Parent'),'Title',h2);
if nargout > 0, fig = h0; end


h=gcf;
s = get(h,'UserData');
s.FrameAxes = findobj(h,'Tag','FrameAxes');
s.RFAxes = findobj(h,'Tag','RFAxes');
s.SpikeAxes = findobj(h,'Tag','SpikeAxes');
set(h,'UserData',s)



