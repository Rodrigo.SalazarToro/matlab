function [mov,sound] = fvtDisplayStillImage2(spike,flag)
%
%function fvtDisplayStillImage
%
%this function superimposes the eye traces on top of the stillimage.
%this function assumes the following:
%       the pwd is the eye directory
%       the init file is contained in the dir above 
%The program prompts the user for the stimulus file
%If the flag is not specified or is 'm' then this program creates a movie 
%(an associated sound file is also created which needs to be added to the
%movie as the audio tract using another software such as Premiere.)
%otherwise just a single jpg image.
%
%A seperate movie or image is created for each trial number.  This program
%automatically loops through all trials.


if nargin<2 | strcmp(flag,'m')
    movieflag=1;
else 
    movieflag=0;
end


for trial=1:spike.numTrials
    if movieflag
        mov = avifile(['movie' num2str(trial) '.avi'],'FPS',85.1/3,'compression','indeo5')%'indeo5'
    end
    
    %get directories
    eye_dir=nptPwd;
    stim_path = uigetfolder('Browse for Stimulus Directory');
    %stim_path = 'c:\stimulus'
    
    cd (eye_dir)
    dirlist = nptDir('*_eye.*');
    numTrials = size(dirlist,1);
    session_name = dirlist(1).name(1:length(dirlist(1).name)-9);
    
    cd ..
    filename = [session_name '.ini'];
    init_info = read_init_info(filename);
    if ~strcmp(init_info.movie_type,'still_image')
        errordlg('ERROR Not a Still Image Session','!! ERROR  !!');
    end
    
    if nargin>=1
        spikeflag=1;
        %find receptive field eccentricity(assume firstRF is fixation point)
        eccx = init_info.ReceptiveField.CenterX(1)-init_info.ReceptiveField.CenterX(str2num(spike.groupname)+1);
        eccy = init_info.ReceptiveField.CenterY(1)-init_info.ReceptiveField.CenterY(str2num(spike.groupname)+1);
    end
    
    cd(stim_path);
    stimheader = ReadStimulusHeader(init_info);
    
    
    %get the correct stimulus
    frame_num=init_info.frames(trial);
    cd (stim_path);
    frame = ReadStimulusFrame(frame_num,init_info,stimheader);
    frame=frame';
    %calculate image offset
    xoffset=init_info.x_center - init_info.x_size/2;
    yoffset=init_info.y_center - init_info.y_size/2;
    %now move image by offsets with gray background
    frame=[200*ones(stimheader.h,xoffset),frame,200*ones(stimheader.h,init_info.ScreenWidth-xoffset-stimheader.w)];
    frame=[200*ones(yoffset,init_info.ScreenWidth);frame;200*ones(init_info.ScreenHeight-yoffset-stimheader.h,init_info.ScreenWidth)];
    
    
    %get the eye data in pixels
    cd (eye_dir)
    filename=dirlist(trial).name;
    [data,num_channels,samplingRate,datatype,points]=nptReadDataFile(filename);
    
    %plot image on the screen
    
    figure('units','normalized','outerposition',[0 0 1 1]); 
    
    colormap(gray)
    imagesc(frame)
    axis([0 init_info.ScreenWidth 0 init_info.ScreenHeight]);
    set(gca,'XTick',[],'YTick',[])
    hold on
    set(gcf,'DoubleBuffer','on') % To turn it on 
    p = plot(data(2,1),data(1,1),'.:','MarkerSize',20,'EraseMode','none','Color',[1 .2 0]);%,
    clist = [1 1 0;0 1 1;.5 1 .5;1 .75 0];
    clistsize = size(clist,1);
    for cl=1:spike.numClusters-1
        eval(['p' num2str(cl) '= plot(1,1,''.:'',''MarkerSize'',20,''EraseMode'',''none'',''Color'',clist(mod(cl-1,clistsize)+1,:));']) %
    end
    rf=(3/85.1)*1000;
    %plot coords on top of image
    size(data)
    for k=1:rf:points-rf    %so last incomplete frame is not included
        a=floor(k);
        b=floor(k+rf);
        set(p,'XData',data(2,a:b),'YData',data(1,a:b));
        if spikeflag
            for cl=1:spike.numClusters-1 %don't include mua
                s = find(spike.trial(trial).cluster(cl).spikes>a);
                f = find(spike.trial(trial).cluster(cl).spikes<b);
                times = round(spike.trial(trial).cluster(cl).spikes(min(s):max(f)));
                %now find where receptive field was at these times
                locy = data(1,times)-eccy+2*(cl-1);
                locx = data(2,times)-eccx+2*(cl-1);
                %then put a dot 
                eval(['set(p' num2str(cl) ',''XData'',locx,''YData'',locy);'])   
            end
        end
        drawnow
        if movieflag
            F = getframe(gca);
            mov = addframe(mov,F);
        end
    end
    if movieflag
        %add a couple of blank frames to end
        figure('units','normalized','outerposition',[0 0 1 1]); 
        colormap(gray)
        blank=50*ones(2,2);
        image(blank)
        set(gca,'XTick',[],'YTick',[])
        numblanks=25;
        for i=1:numblanks
            B=getframe(gca);
            mov = addframe(mov,B);
        end
        
        
        
        %how long is the movie
        s_len = mov.TotalFrames/mov.Fps;  %in seconds
        
        mov = close(mov);
        
        cd(eye_dir)
        
        
        if spikeflag
            sound = nptSpike2Sound(spike,trial,s_len);
        end
        
        
        close all
    else
        cd(eye_dir)
        %create bitmap
        j=getframe;
        jpeg_quality=80;
        imwrite(j.cdata,[num2str(trial) '.jpg'],'jpg','Quality',jpeg_quality)
        close all
       
    end
end
