function fvtDisplayMovie(trial)
%
%function fvtDisplayMovie(trial)
%
%this function superimposes the eye traces on top of the movie clip shown
%this function assumes the following:
%the pwd is the eye directory
%the init file is contained in the dir above 
%program prompts the user for the stimulus file
%the input parameter (trial) is optional and determines the 
%starting trial number.  If trial is not specified then the 
%default starting trial number is one.


%intializations
matlab_movie = 0;
repeat_num = 2;
framerate = 30;
bitmap = 0;
jpeg = 0;
mpeg = 0;
figure

%get directories
eye_dir=nptPwd;
stim_path = uigetfolder('Browse for Stimulus Directory');
%[stim_file, stim_path] = uigetfile('*.*', 'Browse for Stimulus File');	%Only needs the correct directory not the actual file.

cd (eye_dir)
dirlist = nptDir('*_eye.*');
numTrials = size(dirlist,1);
session_name = dirlist(1).name(1:length(dirlist(1).name)-9);

cd ..
filename = [session_name '.ini'];
init_info = read_init_info(filename);
if ~strcmp(init_info.movie_type,'movie')
   errordlg('ERROR Not a WINDOW MOVIE Session','!! ERROR  !!');
end

cd (stim_path);
[stimheader] = ReadStimulusHeader(init_info);

extrasyncs_file = [session_name , '_extra_syncs.txt'];

if nargin==0
   trial = 1;
end

while trial<=numTrials
    
   %initializations
   startpoint = 0;

   cd (eye_dir)
   cd ..
   mov = avifile(['movie' num2str(trial) '.avi'])
   extrasyncs = read_extrasyncs(extrasyncs_file,trial);  %for this trial only
   
   %get the eye data in pixels
   cd (eye_dir)
   filename = dirlist(trial).name;
   [data,num_channels,samplingRate,datatype,points] = nptReadDataFile(filename);
   
   %points will be a 3 row array containing the framenumber, number of syncs and eye datapoints per frame for this trial.
   [points,dp_per_refresh,start_frame] = create_points(init_info,samplingRate,extrasyncs,trial,points);
   
   %calculate image offset
   xoffset = init_info.x_center - init_info.x_size/2;
   yoffset = init_info.y_center - init_info.y_size/2;
   
   set(gcf,'DoubleBuffer','on') % To turn doublebuffer off

   %%%START LOOP TO CREATE IMAGE FRAMES%%%%%
   for frame_num = 1:size(points,2)		
      
      %get the correct stimulus
      cd (stim_path);
      frame = ReadStimulusFrame(points(1,frame_num), init_info, stimheader);

      %change frame from a gray scale intensity image to a rgb true image
      frame = frame';			
      RGBframe = (cat(3,frame,frame,frame));		
      
      
      %create points for that frame
      a=startpoint+1;
      b=startpoint+points(3,frame_num);
      startpoint=b;     
      
      %%%% following two lines used for testing	%%%%
      %c=round((b-a)/dp_per_refresh);
      %fprintf('%s show frame #%d with points %d to %d which is %d refreshes\n',filename,points(1,frame_num),a,b,c);
      
      %points per frame 
      r = data(:,a:b);
      %instead of moving stimulus image ontop of coords we are moving coords ontop of the image.
      %This will create a frame of the image only and not of the entire computer screen.
      r(2,:)=round(r(2,:)-xoffset);		%horizontal is channel 2
      r(1,:)=round(r(1,:)-yoffset);		%vertical is channel 1
      
            
      %superimpose a red dot specified by r (eye position in screen coordinates) on the image
      dot=3;	%dot size
      for j=1:size(r,2)
         if (~isnan(r(1,j)) |   ~isnan(r(1,j)))
            RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,1)=255;	%set points to red
            RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,2)=0; 
            RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,3)=0;  
         end	
      end
      
      %superimpose receptive fields on image
      
      image(RGBframe,'EraseMode','none');
      title(['Trial Number' num2str(trial)]);
      drawnow
      
      %draw receptive field rectangle
      %plotRF(init_info.ReceptiveField,mean(r(2,:)),mean(r(1,:)),'LineWidth',5)

      %write bitmap file
      if bitmap
         %create uint8 bitmap frame
         destination_dir=[display_dir '\movie' ctrial_num];
         cd(destination_dir);
         filename=['trial' ctrial_num 'frame' num2strpad(frame_num,4) '.bmp']
         imwrite(RGBframe,filename,'bmp');
      end  
      
      %write jpeg file
      if jpeg
         destination_dir=[display_dir '\movie' ctrial_num];
         cd(destination_dir);
         filename=['trial' ctrial_num 'frame' num2strpad(frame_num,4) '.jpg']
         imwrite(RGBframe, filename, 'jpg', 'Quality', jpeg_quality); 
         image(RGBframe,'EraseMode','none');
         drawnow
      end
      
      %display frame or first frame on figure
      %if jpeg | bitmap | points(1,frame_num)==start_frame
      %   image(RGBframe,'EraseMode','none');
      %   drawnow
      %end
      
      %create matlab movie frames
      if matlab_movie
         m(frame_num)=im2frame(RGBframe);
      end
      
      F = getframe(gca);
       	mov = addframe(mov,F);
      
   end	%end loop over frames
   set(gcf,'DoubleBuffer','off') % To turn doublebuffer off
    mov = close(mov);
   
   %display matlab movie
   if matlab_movie
      fprintf('displaying movie\n')
      if (init_info.x_size==660 & init_info.y_size==352)
         movie(gcf,m,repeat_num,framerate);%,[370,276,0,0]);	%[x,y, , ]
      else
         movie(gcf,m,repeat_num,framerate);%,[270,180,0,0]);	%[x,y, , ]
      end
   end
   
   
   
   
   %create mpeg movie from matlab movie
   if mpeg
      destination_dir=[eye_dir '\movie' num2str(trial)];
      cd(eye_dir);
      filename=['trial' num2str(trial)];
      mpgwrite(m,hsv,filename);
   end
   
   
   
   
   % get keyboard input to see what to do next
   key = input('RETURN - Next Trial; p - Previous trial; N - Trial N: ','s');
   n = str2num(key);
   if strcmp(key,'p')
      trial = trial - 1;
      if trial<1
         trial = 1;
      end
   elseif ~isempty(n)
      if n>1 & n<=numTrials
         trial = n;
      end	
   else
      trial = trial + 1;
   end
end	%end while loop




