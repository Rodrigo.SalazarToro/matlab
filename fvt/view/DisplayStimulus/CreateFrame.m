function [Frame,RFFrame] = CreateFrame(s)

%update frame data
%and display on graphs
% s is the structure contained in 'UserData'

%get the correct stimulus
      cd (s.stimdir);
      frame = ReadStimulusFrame(s.points(1,s.frame), s.init_info, s.stimheader);

      %change frame from a gray scale intensity image to a rgb true image
      frame = frame';			
      Frame = (cat(3,frame,frame,frame));		
      
      
      %create points for that frame
      %a=startpoint+1;
      if s.frame==1
         a=1;
      else
         a = sum(s.points(3,1:s.frame-1));
      end
      %b=startpoint+points(3,frame_num);
      b = a + s.points(3,s.frame)-1;
            
      %%%% following two lines used for testing	%%%%
      %c=round((b-a)/dp_per_refresh);
      %fprintf('%s show frame #%d with points %d to %d which is %d refreshes\n',filename,points(1,frame_num),a,b,c);
      
      %points per frame 
      if ~isempty(s.eyedir)
      r = s.eyedata(:,a:b);
  else
      r= zeros(2,b-a);
  end
      %instead of moving stimulus image ontop of coords we are moving coords ontop of the image.
      %This will create a frame of the image only and not of the entire computer screen.
      r(2,:)=round(r(2,:)-s.xoffset);		%horizontal is channel 2
      r(1,:)=round(r(1,:)-s.yoffset);		%vertical is channel 1
      
            
      %superimpose a red dot specified by r (eye position in screen coordinates) on the image
      dot=1;	%dot size
      for j=1:size(r,2)
         if (~isnan(r(1,j)) |   ~isnan(r(1,j)))
            Frame(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,1)=255;	%set points to red
            Frame(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,2)=0; 
            Frame(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,3)=0;  
         end	
      end
      
      [boxX,boxY] = RFEccentricity(s.init_info.ReceptiveField,s.rf);
      boxX = round(boxX + mean(r(2,:)));
      boxY = round(boxY + mean(r(1,:)));

      if isempty(find(boxX>size(Frame,2)-1)) & isempty(find(boxX<1)) & isempty(find(boxY>size(Frame,1)-1)) & isempty(find(boxY<1))
        % boxX = [boxX-1;boxX;boxX+1];
        % boxY = [boxY-1;boxY;boxY+1];
         %set box corners to red 
         Frame(sub2ind(size(Frame),boxY,boxX,ones(size(boxX))))=255;	
         Frame(sub2ind(size(Frame),boxY,boxX,2*ones(size(boxX))))=0; 
         Frame(sub2ind(size(Frame),boxY,boxX,3*ones(size(boxX))))=0;  
         
         
         %get smaller RFframe
         minX = min(boxX);
         maxX = max(boxX);
         minY = min(boxY);
         maxY = max(boxY);
         
         RFFrame = Frame(minY:maxY,minX:maxX,:);
         
      else
         RFFrame=[];
         
      end
      
   
