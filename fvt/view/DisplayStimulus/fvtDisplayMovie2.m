function [mov,sound] = fvtDisplayMovie2(spike)
%
%function fvtDisplayMovie(trial)
%
%this function superimposes the eye traces on top of the movie clip shown
%this function assumes the following:
%       the pwd is the eye directory
%       the init file is contained in the dir above 
%program prompts the user for the stimulus file
%the input parameter (trial) is optional and determines the 
%starting trial number.  If trial is not specified then the 
%default starting trial number is one.
%
%if spike is passed in then a sound file is generated with 
%the spike trains represented as beeps of different frequencies.
%Also a colored dot is displayed in the correct receptive field (from
%init_file estimation not calculated) when a spike fires.
%the receptivefield spike train should be passed in rather than the normal
%spike train.



%get directories
eye_dir=nptPwd;
%stim_path = uigetfolder('Browse for Stimulus Directory');
stim_path='c:/stimulus';

cd (eye_dir)
dirlist = nptDir('*_eye.*');
numTrials = size(dirlist,1);
session_name = dirlist(1).name(1:length(dirlist(1).name)-9);

cd ..
filename = [session_name '.ini'];
init_info = read_init_info(filename);
if ~strcmp(init_info.movie_type,'movie')
    error('ERROR Not a WINDOW MOVIE Session','!! ERROR  !!');
end
if nargin==1
    spikeflag=1;
    %find receptive field eccentricity(assume firstRF is fixation point)
    eccx = init_info.ReceptiveField.CenterX(1)-init_info.ReceptiveField.CenterX(str2num(spike.groupname)+1);
    eccy = init_info.ReceptiveField.CenterY(1)-init_info.ReceptiveField.CenterY(str2num(spike.groupname)+1);
    colors=[0 255 255;255 255 0;128 255 128;255 128 0;0 128 255;0 0 255];
end

cd (stim_path);
[stimheader] = ReadStimulusHeader(init_info);

extrasyncs_file = [session_name , '_extra_syncs.txt'];
%for trial=1:spike.numTrials
for trial=2
       
    %initializations
    startpoint = 0;
    
    cd (eye_dir)
    cd ..
    mov = avifile(['movie' num2str(trial) '.avi'],'FPS',85.1/3,'compression','indeo5')
    extrasyncs = read_extrasyncs(extrasyncs_file,trial);  %for this trial only
    
    %get the eye data in pixels
    cd (eye_dir)
    filename = dirlist(trial).name;
    [data,num_channels,samplingRate,datatype,points] = nptReadDataFile(filename);
    
    %points will be a 3 row array containing the framenumber, number of syncs and eye datapoints per frame for this trial.
    [points,dp_per_refresh,start_frame] = create_points(init_info,samplingRate,extrasyncs,trial,points);
    
    %calculate image offset
    xoffset = init_info.x_center - init_info.x_size/2;
    yoffset = init_info.y_center - init_info.y_size/2;
    
    set(gcf,'DoubleBuffer','on') % To turn doublebuffer off
     figure('units','normalized','outerposition',[0 0 1 1]); 
        set(gca,'XTick',[],'YTick',[])
    
    %%%START LOOP TO CREATE IMAGE FRAMES%%%%%
    for frame_num = 1:size(points,2)		
        
        %get the correct stimulus
        cd (stim_path);
        frame = ReadStimulusFrame(points(1,frame_num), init_info, stimheader);
        
        %change frame from a gray scale intensity image to a rgb true image
        frame = frame';			
        RGBframe = (cat(3,frame,frame,frame));		
        
        
        %create points for that frame
        a=startpoint+1;
        b=startpoint+points(3,frame_num);
        startpoint=b;     
        
        %%%% following two lines used for testing	%%%%
        %c=round((b-a)/dp_per_refresh);
        %fprintf('%s show frame #%d with points %d to %d which is %d refreshes\n',filename,points(1,frame_num),a,b,c);
        
        %points per frame 
        r = data(:,a:b);
        %instead of moving stimulus image ontop of coords we are moving coords ontop of the image.
        %This will create a frame of the image only and not of the entire computer screen.
        r(2,:)=round(r(2,:)-xoffset);		%horizontal is channel 2
        r(1,:)=round(r(1,:)-yoffset);		%vertical is channel 1
        
        %superimpose a red dot specified by r (eye position in screen coordinates) on the image
        dot=3;	%dot size
        for j=1:size(r,2)
            if (~isnan(r(1,j)) |   ~isnan(r(2,j)))
                if min(r(:,j)-dot)>1 & min((r(:,j)+dot)'< [init_info.frame_rows init_info.frame_cols])
                    RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,1)=255;	%set points to red
                    RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,2)=0; 
                    RGBframe(r(1,j)-dot:r(1,j)+dot,r(2,j)-dot:r(2,j)+dot,3)=0;  
                end
            end	
        end
   
        %put spikedots from on movie
        if spikeflag
            for cl=1:spike.numClusters-1 %don't include mua
                s = find(spike.trial(trial).cluster(cl).spikes>a);
                f = find(spike.trial(trial).cluster(cl).spikes<b);
                times = round(spike.trial(trial).cluster(cl).spikes(min(s):max(f)) - a+1);
                %now find where receptive field was at these times
                locy = r(1,times)-eccy+dot*(cl-1);
                locx = r(2,times)-eccx+dot*(cl-1);
                %then put a dot 
                for j=1:size(locy,2)
                    if (~isnan(locy(j)) | ~isnan(locx(j)))
                        RGBframe(locy(j)-dot:locy(j)+dot,locx(j)-dot:locx(j)+dot,1)=colors(cl-floor((cl-1)/size(colors,1))*size(colors,1),1);	%red
                        RGBframe(locy(j)-dot:locy(j)+dot,locx(j)-dot:locx(j)+dot,2)=colors(cl-floor((cl-1)/size(colors,1))*size(colors,1),2); %set points to green
                        RGBframe(locy(j)-dot:locy(j)+dot,locx(j)-dot:locx(j)+dot,3)=colors(cl-floor((cl-1)/size(colors,1))*size(colors,1),3);  %blue
                    end	
                end    
            end
        end
        
        %now add a gray border to image
        RGBframe=[200*ones(stimheader.h,xoffset,3),RGBframe,200*ones(stimheader.h,init_info.ScreenWidth-xoffset-stimheader.w,3)];
        RGBframe=[200*ones(yoffset,init_info.ScreenWidth,3);RGBframe;200*ones(init_info.ScreenHeight-yoffset-stimheader.h,init_info.ScreenWidth,3)];
        
        image(RGBframe,'EraseMode','none');
        set(gca,'XTick',[],'YTick',[])
        title(['Trial Number' num2str(trial)]);
        drawnow
        
        %superimpose receptive fields on image
        %draw receptive field rectangle
        %plotRF(init_info.ReceptiveField,mean(r(2,:)),mean(r(1,:)),'LineWidth',5)
        
        
        F = getframe(gca);
       % for rf=1:points(2,frame_num)
            mov = addframe(mov,F);
        %end
    end	%end loop over frames
    %add a couple of blank frames to end
    close(gcf)
    figure('units','normalized','outerposition',[0 0 1 1]); 
    axes
    colormap(gray)
    blank=50*ones(2,2);
    image(blank)
    set(gca,'XTick',[],'YTick',[])
    numblanks=15;
    for i=1:numblanks
        B=getframe(gca);
        mov = addframe(mov,B);
    end
    
    %how long is the movie
    s_len = mov.TotalFrames/mov.Fps;  %in seconds
    
    mov = close(mov);
    
    cd(eye_dir)
    cd ..
    pause
    if spikeflag
        sound = nptSpike2Sound(spike,trial,s_len);
    end
     
    
   
  close all  
end