%create_points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This program creates a array specifying which screencoordinate data points go with which movie frames.



function [points,dp_per_refresh,start_frame]=create_points(init_info,sampling_rate,extrasyncs,trial_num,count)

syncs=[];

%use extrasyncs and refreshes per frame to create an array of data points per frame
%11.8 msec/refresh x 1 datapoint/msec = 11.8 dp/refresh
dp_per_refresh=1/init_info.refresh_rate*sampling_rate;
%array of number of syncs(or refreshes) per frame

%find the first frame of the trial
start_frame=init_info.frames(trial_num);

%initializations
frame=start_frame;		%presenter numbering system is one more than movie number 
								%but presenter starts at zero and matlab starts at one so it works out.
frame_count=0;
remainder=0;
j=0;

while j<count
   if frame>init_info.end_frame	
      frame=0;				%reset frame number to beginning of movie 
   end
   
   frame_count=frame_count+1;
   syncs(1,frame_count)=frame;
   syncs(2,frame_count)=init_info.refreshes_per_frame;
   %see if this frame number has any extrasyncs
   for k=1:size(extrasyncs,2)
      if extrasyncs(2,k)==frame
         syncs(2,frame_count)=syncs(2,frame_count)+extrasyncs(1,k);
      end
   end
   %syncs will now be a 3 row array showing the framenumber, number of syncs and datapoints per frame 
   syncs(3,frame_count)=floor(dp_per_refresh*syncs(2,frame_count)+remainder);
   remainder=(dp_per_refresh*syncs(2,frame_count)+remainder)-floor(dp_per_refresh*syncs(2,frame_count)+remainder);
   j=sum(syncs(3,:));
   if strcmp(init_info.movie_type,'movie')		%only increment for movies not still images
      frame=frame+1;
   end
   
end
%but now last frame could have too many points
length(syncs);
size(syncs);
syncs(3,length(syncs))=count-sum(syncs(3,1:length(syncs)-1));
points=syncs;