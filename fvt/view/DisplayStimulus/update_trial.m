function s = update_trial(s)
%function update_trial(trial,s)
%
%this function used in DisplayStimulus callback
%calculate properties for each trial



cd (s.spikedir)
cd ..
extrasyncs = ReadExtraSyncsFile(s.extrasyncs_file);%,s.trial);  %for this trial only

%get the eye data in pixels
if ~isempty(s.eyedir)
cd (s.eyedir)
eyefile = [s.sessionname '_eye.' num2strpad(s.trial,4)];
[s.eyedata,num_channels,samplingRate,datatype,points] = nptReadDataFile(eyefile);
name = [s.sessionname '_eye'];
eval(['obj=load(''' name ''');'])
eye = obj.eye;
else
    samplingRate = 30000;   %assume 30KHz
end

%points will be a 3 row array containing the framenumber, number of syncs and eye datapoints per frame for this trial.
[s.points,s.dp_per_refresh,s.start_frame] = create_points(s.init_info,samplingRate,extrasyncs,s.trial,points);
%calculate image offset
s.xoffset = s.init_info.x_center - s.init_info.x_size/2;
s.yoffset = s.init_info.y_center - s.init_info.y_size/2;

cd (s.spikedir)
name = s.spike_filename(1:length(s.spike_filename)-4);
eval(['obj=load(''' name ''');'])
spike = obj.spike;
if ~isempty(s.eyedir)
s.eyespikes = eyespikes(eye,spike);  %create eyespikes object
else
    s.eyespikes=spike;
end