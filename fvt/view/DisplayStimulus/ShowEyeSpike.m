function ShowEyeSpike(s)


axes(s.SpikeAxes);
if ~isempty(s.eyedir)

cd (s.eyedir)
%plot(0,0)
b = sum(s.points(3,1:s.frame));
end



%axis([0 (s.eyespikes.ispikes.duration*1000+s.WindowSize) 0 1]);
plot(s.eyespikes,s.trial)
ax = axis;

% shift axis a little bit so we can see the start of the data
%axis([(b-s.WindowSize/2) (b+s.WindowSize/2) ax(3) ax(4)]);
xlabel('Time from start of trial (msec)                                                       ')
  