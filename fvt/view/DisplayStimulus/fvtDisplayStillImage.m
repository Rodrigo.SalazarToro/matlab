function [mov,sound] = fvtDisplayStillImage(trial,spike)
%
%function fvtDisplayStillImage
%
%this function superimposes the eye traces on top of the stillimage shown
%this function assumes the following:
%the pwd is the eye directory
%the init file is contained in the dir above 
%program prompts the user for the stimulus file

%get directories
eye_dir=nptPwd;
stim_path = uigetfolder('Browse for Stimulus Directory');
%[stim_file, stim_path] = uigetfile('*.*', 'Browse for Stimulus File');	%Only needs the correct directory not the actual file.


cd (eye_dir)
dirlist = nptDir('*_eye.*');
numTrials = size(dirlist,1);
session_name = dirlist(1).name(1:length(dirlist(1).name)-9);

cd ..
filename = [session_name '.ini'];
init_info = read_init_info(filename);
if ~strcmp(init_info.movie_type,'still_image')
   errordlg('ERROR Not a Still Image Session','!! ERROR  !!');
end

cd(stim_path);
stimheader = ReadStimulusHeader(init_info);

trial = 1;
while trial<=numTrials
   
   %get the correct stimulus
   frame_num=init_info.frames(trial);
   cd (stim_path);
   frame = ReadStimulusFrame(frame_num,init_info,stimheader);
   frame=frame';
	%calculate image offset
	xoffset=init_info.x_center - init_info.x_size/2;
	yoffset=init_info.y_center - init_info.y_size/2;
	%now move image by offsets
	frame=[255*ones(stimheader.h,xoffset),frame];
	frame=[255*ones(yoffset,stimheader.w+xoffset);frame];


   %get the eye data in pixels
   cd (eye_dir)
   filename=dirlist(trial).name;
	[data,num_channels,samplingRate,datatype,points]=nptReadDataFile(filename);

   %plot image on the screen
	colormap(gray)
	imagesc(frame)
	axis([0 init_info.ScreenWidth 0 init_info.ScreenHeight]);
   hold on
   
   %plot coords on top of image
   p = plot(data(2,1),data(1,1),'r.:', ...
      'EraseMode','none','MarkerSize',10); % Set EraseMode to none
   for k=2:points	
      set(p,'XData',data(2,k),'YData',data(1,k));
      drawnow
	end
   hold off
   
   % get keyboard input to see what to do next
	key = input('RETURN - Next Trial; p - Previous trial; N - Trial N: ','s');
	n = str2num(key);
	if strcmp(key,'p')
		trial = trial - 1;
		if trial<1
			trial = 1;
		end
	elseif ~isempty(n)
		if n>1 & n<=numTrials
			trial = n;
		end	
	else
		trial = trial + 1;
	end
end



   
