% show_seq.m - display sparse noise stimulus sequence
%
% function show_seq(noise_seq,init_info)
%
% noise_seq is the array which is read in from the .seq file using read_seq.m
% init_info is the structure read in from the .INI file using read_init_info.m

function show_seq(noise_seq,init_info)

n=size(noise_seq,1);

M=init_info.rows;
N=init_info.cols;

x=noise_seq(:,2)-(N-1)/2;
y=-(noise_seq(:,1)-(M-1)/2);

bar_start=[x'; y'-0.5];
bar_end=[x'; y'+0.5];

k=noise_seq(:,3);
angle=init_info.bar_orientation;

%lum=(1-(noise_seq(:,4)-0.5)*contrast)*init_info.background_luminance;
lum=noise_seq(:,4);
c=['g' 'r'];

for i=1:n
    
    if noise_seq(i,1)~=-1
        
        theta=(90-angle(k(i)+1))*pi/180;
        
        % rotation matrix for clockwise rotation by theta
        rot=[cos(theta) sin(theta); -sin(theta) cos(theta)];
        
        P=rot*[bar_start(:,i) bar_end(:,i)];
        
        plot(P(1,:),P(2,:),c(lum(i)+1),'LineWidth',2)
        axis(1.5*[-N N -M M]/2)
        
        drawnow
        pause(0.05)
        
    end
    
end
