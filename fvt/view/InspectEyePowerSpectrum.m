function InspectEyePowerSpectrum(power_spectrum)
%function InspectEyePowerSpectrum(power_spectrum)
%
%Shows Power Spectrum plot for both vertical and
%horizontal channels of the fixation and Drifts.
%
%power_spectrum is obtained from the output of fvtEyePowerSpectrum
%

if isfield(power_spectrum,'fixation')      
    subplot(1,2,1)
    zoom on
    plot(power_spectrum.frequencies,power_spectrum.fixation.vert(:,1),'k')
    title('Power Spectrum of Raw Fixations')
    xlabel('Frequency (Hz)')
    hold on
    plot(power_spectrum.frequencies,power_spectrum.fixation.horiz(:,1))
    legend('vertical','horizontal')
end

if isfield(power_spectrum,'drift')      
    subplot(1,2,2)
    zoom on
    plot(power_spectrum.frequencies,power_spectrum.drift.vert(:,1),'k')
    title('Power Spectrum of Raw Drift')
    xlabel('Frequency (Hz)')
    hold on
    plot(power_spectrum.frequencies,power_spectrum.drift.horiz(:,1))
    legend('vertical','horizontal')
end


h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
set(edithandle,'String',power_spectrum.sessionname)
set(h,'Name','Eye Power Spectrum Histograms')

