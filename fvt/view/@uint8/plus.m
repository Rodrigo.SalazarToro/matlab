function r = plus(p,q)
%r = plus(p,q)
%p & q must be the same size

for kk=1:size(p,3)
    a=double(p(:,:,kk));
    b=double(q(:,:,kk));
    r(:,:,kk)=uint8(a+b);
end