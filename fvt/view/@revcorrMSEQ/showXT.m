function showXT(obj,RF,clusters)


R=obj.R;


% xh = figure;
% set(xh,'Name','X')
xyh = figure;
set(xyh,'Name','X-Y')
xth = figure;
set(xth,'Name','X-T')
ah= figure;
set(ah,'Name','XY Area')


%loop over clusters
for ii=1:length(clusters)
    Rc = R(:,:,:,clusters(ii));
    RF.cluster(ii)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   XY    
    figure(xyh)
    nptsubplot(length(clusters),ii)
    
    %calculate contrast for entire kernel
    m1=min(Rc(:));
    m2=max(Rc(:));
    m3=mean(Rc(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    
    %show x-y at time w/ max contrast
    imagesc(Rc(:,:,RF.cluster(ii).center(3)),contrast)
    hold on
    
    %plot orthogonal orientation line
    plot(RF.cluster(ii).xi,RF.cluster(ii).yi,'k')
    
    %Show center
    plot(RF.cluster(ii).center(1),RF.cluster(ii).center(2),'*r')
    xlabel('X (pixels)')
    ylabel('Y (pixels)')
    axis equal
    axis tight
    title(['Cluster ' num2str(clusters(ii)) ' - frame ' num2str(RF.cluster(ii).center(3))])
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   AREA
    figure(ah)
    nptsubplot(length(clusters),ii)
    imagesc(RF.cluster(ii).areaM);
    hold on
    imcontour(RF.cluster(ii).areaM,[RF.cluster(ii).areaThreshold RF.cluster(ii).areaThreshold],'k');
    %Show center
    plot(RF.cluster(ii).center(1)*RF.cluster(ii).Algo.oversample,RF.cluster(ii).center(2)*RF.cluster(ii).Algo.oversample,'*r')
    title(['Cluster #' num2str(clusters(ii)) ' - Area =' num2str(RF.cluster(ii).area) 'pixels^2'])
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   XT
    figure(xth)
    nptsubplot(length(clusters),ii)
    
    
    %calculate contrast for entire kernel
    xt = RF.cluster(ii).xt;
    m1=min(xt(:));
    m2=max(xt(:));
    m3=mean(xt(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    
    %show xt plot
    xlim = sqrt((RF.cluster(ii).xi(1)-RF.cluster(ii).xi(end))^2 + (RF.cluster(ii).yi(1)-RF.cluster(ii).yi(end))^2);
    x=linspace(0,xlim,length(RF.cluster(ii).xi));
    y=1:size(xt,1);
    imagesc(x,y,xt,contrast)
    colorbar
    
    latency = RF.cluster(ii).latency;
    hold on
    line([0 size(xt,2)] , [latency(1) latency(1)]);
    line([0 size(xt,2)] , [latency(2) latency(2)]);
    xts = RF.cluster(ii).XTsum;
    scale=1;
    plot(scale*xts,1:length(xts))
    line([scale*RF.cluster(ii).XTthreshold scale*RF.cluster(ii).XTthreshold],[1 length(xts)])
    
    
    
    title(['Cluster ' num2str(clusters(ii)) ' : XT'])
    ylabel('Time (msec)')
    xlabel('Space (pixels)')
    
end

