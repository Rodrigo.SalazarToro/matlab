function R = MSeqRevCorr(spikes, ecc, clusterNum, stimInfo, stim, window_size);
% SNRevCorr - performs reverse correlation from an ispikes object for a sparse noise stimulus
%
% [R, Pspike] = SNRevCorr(spikes, clusterNum, stimInfo, stim, window_size, bin_size);
%
% spikes:  		ispikes structure containing all spike info for each trial
%					all times must be in points
% ecc:       optional eye eccentricities to correct for eye movements
%               (assumed to be already sampled according to bin_size)
%               If eye positions are not used then ecc=[];
% clusterNum:	the cluster number of the data to be inspected
% framePoints	a matrix of numTrials x (numFrames + 2).  Trials with fewer 
%				frames than others are padded with zeroes.  The last nonzero
%				number in each row is the point at which the last frame
%				shown for that trial ended.  All rows end in a zero.
% totalFrames	the total number of stimulus frames shown in the session.
% iniInfo:      iniInfo structure returned by ReadRevCorrIni
% stim:          stimulus surface 
% windowSize:   reverse correlation window size (ms)
% binSize:      NO LONGER an input always set to 1 ms (time resolution of
%               eye file
%
% R:            M x N reverse correlation array. Each column contains a
%               probability distribution over bar configurations at a given
%               time relative to the occurence of a spike -- P(bar|spike).
%               M = # of sparse noise configurations, N = # of time bins.  
%               (See ijoc2index for conversion from ijoc to row index of R.)
%
% Pspike:       marginal probability of spike -- P(spike)
%
%Dependencies:  ijoc2index.m

padarray([],1); %acquire image processing toolbox so we don't crash later.

bin_size=1;     %1ms
%Obtain necessary information from the stimInfo
iniInfo = stimInfo.iniInfo;
numTrials = iniInfo.num_trials;
if strcmp(iniInfo.m_seq_size,'16x16 (order=16)')
   numRows = 16;
   numCols = 16;
elseif strcmp(iniInfo.m_seq_size,'64x64 (order=16)')
   numRows = 64;
   numCols = 64;
else
    s= stimInfo.iniInfo.m_seq_size;
    numRows=s(1); numCols=s(2); 
end;
if isfield(iniInfo,'oldcat')
    lastframe = iniInfo.frames_displayed-1;
else
    lastframe = iniInfo.last_frames(end);
end

order = 16; %order is 16 for both 16x16 and 64x64
framePoints = stimInfo.framePoints;


%Create a stimulus matrix page for every latency bin.
%For each spike add up all the pages and then divide by the total number of
%spikes at the end.
%
N = floor(window_size/bin_size);                        %Number of bins per rev corr window
if ~isempty(ecc)
    R = zeros(iniInfo.grid_y_size,iniInfo.grid_x_size,N);   %allocate memory
else
    R = zeros(numRows,numCols,N);
end
binSize = bin_size * 30;					            %Change to datapoints.
maxBins = floor(spikes.duration*30000/binSize);	    %Maximum number of bins in one trial (time)
%maxBins = floor(spikes.duration/binSize);	        %Maximum number of%bins in one trial (DP)

spikenum=0;

%how many different stimulus frames can be shown with one surface?
%first frame is zero.
%Last frame is 65535 = 2^16-1
%So the stimulus repeats after 2^16th frames
stimSeq = (0:lastframe)+1;

if isfield(iniInfo,'oldcat')
    stimSeq = [ 0 stimSeq];
    addframe=1;
else 
    addframe=0;
end

loop = length(stimSeq);
Pspike = 0;
for i = 1:numTrials
    fprintf('%i\n',i)
    spikeBin = ceil(spikes.trial(i).cluster(clusterNum).spikes/bin_size);   % bin spikes(time)
    %spikeBin = ceil(spikes.trial(i).cluster(clusterNum).spikes/binSize);   % bin spikes(DP)
    numSpikes = spikes.trial(i).cluster(clusterNum).spikecount;
    if numSpikes > 0
        trialSeq = zeros(1, maxBins);
        counter = 0;
        datapoint = 0;
        stimStep = iniInfo.first_frames(i) + 1;    %Add 1 to make up for zero basing
        step = 1;
        while counter < maxBins				%Loops through the columns of framePoints
            counter = counter + 1;
            %if the loop has advanced to the next frame, advance the stimulus sequence
            if datapoint > framePoints(i, step)
                trialSeq(counter) = stimSeq(stimStep);
                step = step + 1;
                stimStep = stimStep + 1;
                %otherwise, show that the previous frame is still being shown
            elseif (step > 1+addframe) & (stimStep > (1+addframe))
                trialSeq(counter) = stimSeq(stimStep - 1);
            elseif (step > 1+addframe) & (stimStep == (1+addframe))
                trialSeq(counter) = stimSeq(loop);
            end
            datapoint = datapoint + binSize;	%Advance through the current trial
            if stimStep > loop
                stimStep = 1+addframe;
            end
            %Every row in framePoints end in zero, thus, when zero is reached...
            if framePoints(i, step) == 0
                %remove the stimulus change accounted to the last frame
                %shown, and...
                trialSeq(counter) = 0;
                break					%escape the while loop.
            end
        end  
       
        if isfield(iniInfo,'oldcat')
            if numCols==16 
                ind = find(trialSeq>65790);
            elseif numCols==64 
                ind = find(trialSeq>65550);
            end
            trialSeq(ind)=0;
        end

        
        for j=1:numSpikes 
            si = spikeBin(j) + [-(N-1):0];  % spike triggered list of indices
            si(find(si < 1)) = [];  % chop off indices < 0
            ii = trialSeq(si);      %ii is list of frame numbers for each bin
            ii(find(ii < 1)) = [];  %Some of these indices may be between the control and
            %presenter triggers, so we're removing those.
            ii=fliplr(ii);
            if ~isempty(ii)
                frames = MSeqframe(stim,numRows,numCols,order,ii-1);
                frames = frames/255;  %normalize to 0's and 1's.
                if ~isempty(ecc)
                    frames = imresize(frames,[iniInfo.grid_y_size iniInfo.grid_x_size],'nearest');  %resize to pixel resolution
                    %only need to pad by smallest and lagest values!!!
                    frames = padarray(frames,[ceil(abs(ecc(i).smallest(1)))  ceil(abs(ecc(i).smallest(2)))],0,'pre');               %pad with zeros
                    f = padarray(frames,[ceil(abs(ecc(i).largest(1)))  ceil(abs(ecc(i).largest(2)))],0,'post');               %pad with zeros
                    clear frames;
                    eccent = ecc(i).trial(:,si); %eccentricity
                    %correct for eye position
                    for kk=1:size(f,3)
                        Ystart = ceil(abs(ecc(i).smallest(1)))+1+round(eccent(1,kk));
                        Ystop = Ystart+iniInfo.grid_y_size-1;
                        Xstart = ceil(abs(ecc(i).smallest(2)))+1+round(eccent(2,kk));
                        Xstop = Xstart+iniInfo.grid_x_size-1;
                        frames(:,:,kk) = f(Ystart:Ystop,Xstart:Xstop,kk);
                    end
                end
                S = size(frames,3);
                if S<N
                    frames = padarray(frames,[0 0 N-S],'post');
                end
                R = R + frames;
                spikenum=spikenum+1;
            end
        end
    end
end

% normalize R
R = R/spikenum;


