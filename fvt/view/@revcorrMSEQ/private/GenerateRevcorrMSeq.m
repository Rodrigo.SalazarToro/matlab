function r = GenerateRevcorrSN(groupNum,stimInfo,window_size)
%r = GenerateRevcorrSN(groupNum,StimInfo,window_size,pixel)
%
%assumes we are in the session folder 
%and a 'sort' folder exists within this.

%ignore old Tang files just give warning
%if eye directory 
%       get eyepos  (What do we do with this???)
%get spike train
%get stimulus
%call SNRevCorr.m to get kernel
%  Thats it!!

sessionname = stimInfo.data.sessionname;

%get eye signals
dirlist = nptDir('eye','CaseInsensitive');
if ~isempty(dirlist)
    cd('eye')
    D=dir([sessionname '_eye.0*']);
    for ii=1:length(D)
        [data num_chan sample_rate so n]=nptReadDataFile(D(ii).name);
        eyepos(ii).trial=data;
    end
    %find eccentricities based on average eye position
    cd ..
else
    eyepos=[];
end
ecc = Eccentricity(eyepos);

%get spikes
cd('sort')
dirlist = nptDir([sessionname 'g' num2strpad(groupNum,4) '_ispike.mat'],'CaseInsensitive');
if isempty(dirlist)
    warningdlg('ispikes does not exist.  Must create this first!')
   warning('ispikes does not exist.  Must create this first!')
r=[];
    return;
else
    spike = load(dirlist(1).name);
    f = fieldnames(spike);
    eval(['spike=spike.' char(f(1)) ';'])
end
cd ..



if strcmp(stimInfo.data.iniInfo.m_seq_size,'16x16 (order=16)')
   numRows = 16;
   numCols = 16;
elseif strcmp(stimInfo.data.iniInfo.m_seq_size,'64x64 (order=16)')
   numRows = 64;
   numCols = 64;
else
    s= stimInfo.data.iniInfo.m_seq_size;
    numRows=s(1); numCols=s(2); 
end;
order = 16;		%both m-sequences are order 16
seqStep = (2^order)/(numRows*numCols);
surfaceRows = numRows * seqStep + (numRows - 1);
surfaceCols = 2 * numCols - 1;
%get stimulus
p=which('revcorrMSeq');
[p,n,e]=fileparts(p);
if numRows == 16
    seqFile = [p filesep 'mat16.bin'];
elseif numRows == 64
    seqFile = [p filesep 'mat64.bin'];
end
fid = fopen(seqFile);
F = fread(fid, [surfaceCols, surfaceRows], 'uchar');
fclose(fid);
F=F';




%loop over clusters
for clusNum = 1:spike.data.numClusters		%loop over clusters
    fprintf('calculating revcorr for cluster number %i\n',clusNum)
        eval(['R(:,:,:,' num2str(clusNum) ') = MSeqRevCorr(spike,ecc,clusNum,stimInfo,F,window_size);'])
%eval(['P(:,:,' num2str(clusNum) ') = bar_prob(spike,eyepos,clusNum,stimInfo,noise_seq,window_size,bin_size);'])
end

r.sessionname = stimInfo.data.sessionname;
r.groupname = groupNum;	
r.clusters = clusNum;
r.R = R;
%r.Pspike = Pspike;
r.window_size = window_size;
r.bin_size = 1; %default





























% if init_info.Date(3)<=1 & init_info.Date(1)<9	%if data is prior to 9/01/01 
%    %before 9/01/01 there was a bug with the stimulus trigger
%    %also the Timing Information was not recorded in the init file
%    %also before 12/17/01 the refresh rate was written as 85 rather than the more accurate 85.1 Hz.
%    init_info.StimulusDuration = 1.998;		%not implemented
%    init_info.refresh_rate = 85.1;
%    
%    [dT, actual_stim_dur] = infer_latency(spike.sessionname, init_info.StimulusDuration, init_info.refresh_rate, init_info.refreshes_per_frame);
%    %shifted spike train
%    spike = shift_spikes(spike, dT, actual_stim_dur);
%    fprintf('reading eye position\n');
%    [eyepos, sample_rate] = read_eyepos(spike.sessionname, actual_stim_dur);
%    
% else
%    [eyepos, sample_rate] = read_eyepos(spike.sessionname);
%    
% end	%if before 9/01/01
% 
% noise_seq = read_seq([spike.sessionname '.seq']);
% %does a fvtskiptrials file exist
% dirlist=nptdir('fvtskiptrials.txt');
% if ~isempty(dirlist)
%    fprintf('fvtskiptrials detected!\n');
%    skiptrials = fvtReadSkipTrials(dirlist(1).name);
% else
%    skiptrials=[];
% end
% 
% 
% cd ('sort')	%move back to sort folder
% 
% 
% window_size = 150;	%150 ms
% bin_size = 1;			%1 ms
% revcorr_sn.bin_size = bin_size;
% revcorr_sn.window_size = window_size;
% for cluster_num = 1:size(spike.trial(1).cluster,2)		%loop over clusters
%    fprintf('Sparse Noise Reverse Correlation - Cluster #%d\n',cluster_num);
%    warning off MATLAB:mir_warning_variable_used_as_function
%    eval(['[revcorr_sn.R(:,:,' num2str(cluster_num) '), revcorr_sn.Pspike(:,:,' num2str(cluster_num) ')] = revcorr_sn(spike,cluster_num,init_info,noise_seq,window_size,bin_size,skiptrials,eyepos);'])
%   	 eval(['revcorr_sn.P(:,:,' num2str(cluster_num) ') = bar_prob(spike,cluster_num,init_info,noise_seq,window_size,bin_size,skiptrials,eyepos);'])
% 
% end	%loop over clusters
% 
% 
% revcorr_sn.sessionname = spike.sessionname;
% revcorr_sn.groupname = spike.groupname;	
% revcorr_sn.clusters = cluster_num;
% revcorr_sn.rows = init_info.rows;
% revcorr_sn.cols = init_info.cols;
% revcorr_sn.num_orientations = init_info.num_orientations;
% revcorr_sn.bar_orientation = init_info.bar_orientation;
% revcorr_sn.num_colors = init_info.num_colors;
% 
% 
