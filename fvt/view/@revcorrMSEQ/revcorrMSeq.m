function obj = recvcorrMSeq(groupNum , varargin)
%revcorrMSeq Constructor function for revcorrMSeq object
%   
%   OBJ = revcorrMSeq(groupNum) instantiates a revcorr object for
%   groupNum in the current session directory
%
%   Optional arguments that can also be passed:
%      redo             ie. 'redo',1
%      saveobj          ie. 'saveobj',1
%
%   The following optional arguments for calculating
%   the Revcorr map can also be passed in:
%      window_size      ie. 'window_size',200     
%      pixel            ie. 'pixel',1
%   but the defaults are 150 milliseconds and not pixel resolution
%
%   The object contains the following fields:
%       RC.sessionname
%	 	RC.groupname
%       RC.sessionnname
%   	RC.clusters
%   	RC.R
%       RC.P
%       RC.Pspike
%		RC.bin_size
%		RC.window_size

%
%   Inherits from: @nptdata and stiminfo.
%
%	

redo=0;
saveobj=0;
pixel=0;
window_size = 200;  %150 milliseconds
extract_varargin

if nargin==0
    % create empty object
     %r.P=[];
    r.sessionname = '';
    r.groupname=0;
    r.clusters=0;
    r.R=[];
    %r.Pspike=[];
    r.window_size=0;
    r.bin_size=0;
    
    stimInfo=stiminfo;
    n = nptdata;
    obj = class(r,'revcorrMSeq',n,stimInfo);
    
elseif ((nargin==1) & (isa(groupNum,'revcorrMSeq')))
    obj = groupNum;
    
else
    %try to create one
    n=nptdata;    
    stimInfo = stiminfo('auto',1,'saveobj',1);
    r = GenerateRevcorrMSeq(groupNum,stimInfo,window_size);
    obj = class(r,'revcorrMSeq',n,stimInfo);
    if saveobj
        fprintf('Saving revcorrMSeq object...\n');
        rc = obj;
        filename = [rc.sessionname 'g' num2strpad(rc.groupname,4) '_revcorrMSeq']; 
        save(filename,'rc')
    end
end
