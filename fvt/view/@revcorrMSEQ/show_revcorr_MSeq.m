% show_revcorr_MSeq.m -  static display of revcorr_MSeq as individual frames
%
% function show_revcorr_MSeq(rc,binsPerStep,cluster,display_mode,interp_mode)
%

function show_revcorr_MSeq(rc,binsPerStep,cluster,display_mode)


if ~exist('display_mode','var')
    display_mode = 'R';
end

switch display_mode
    case 'R'
        R=rc.R;
        %         %   R=rc.R(:,:,:,cluster);
        %     case 'P'
        %          R=rc.P(:,:,:,cluster);	%P is result from bar_prob.m
        %      case 'R/P'
        %          
        %          %still need to fix 1/28/04
        %          [i,j] = find(rc.P(:,:,:,cluster)==0);
        %          index = sub2ind(size(rc.P(:,:,:,cluster)),i,j);
        %          % warning off
        %          R = rc.R(:,:,cluster)./rc.P(:,:,cluster);
        %          %warning backtrace
        %         %R(index) = 0;
end

numClusters = size(R,4);
if strcmp(cluster,'all')
    cluster = 1:numClusters;
end

% get init_info variables
rows = rc.stiminfo.data.iniInfo.grid_x_size;
cols = rc.stiminfo.data.iniInfo.grid_y_size;
[row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);

num_steps=floor(size(R,3)/binsPerStep);
for cl=cluster
    %normalize each cluster independently
    Rcl = R(:,:,:,cl);
    m1=min(Rcl(:));
    m2=max(Rcl(:));
    m3=mean(Rcl(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    %fprintf('before normilization, cluster %i has min=%0.2d, max=%0.2d, mean=%0.2d.\n',cl,m1,m2,m3)
    
    
    for stepNumber=1:num_steps
        %average over time bin
        Rind = Rcl(:,:,(stepNumber-1)*binsPerStep+1:stepNumber*binsPerStep);
        Rind=mean(Rind,3);
        
        %display
        if size(cluster,2)>1
            subplot(numClusters,num_steps,(cl-1)*num_steps+stepNumber)
        else
            nptsubplot(num_steps,stepNumber)
        end
        imagesc(Rind,contrast)
        
        axis equal
        axis tight
       
        if cl==1 | size(cluster,2)==1
            title(['TimeRange: ' num2str((stepNumber-1)*binsPerStep) '-' num2str(stepNumber*binsPerStep) ' msec']);
        end
    end
end

title = ([rc.sessionname '  Group: ' num2str(rc.groupname) '      Clusters: '  num2str(cluster)]);

h=gcf;
eh=findobj(h,'Tag','Title_box');
set(eh,'String',title)
eh=findobj(h,'Tag','DegreeTextbox');
if ~isempty(eh)
text= {['Stimulus Rows span ' num2str(row_degrees,2) ' degrees'],['Stimulus Columns span ' num2str(col_degrees,2) ' degrees']};
set(eh,'String',text)
end
%hcb=findobj('Tag','Colorbar');
%colorbar%(hcb)
% linkedzoom(gcf,'off')
% linkedzoom(gcf,'onxy')