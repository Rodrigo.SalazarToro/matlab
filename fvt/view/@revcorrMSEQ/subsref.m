function [b,res] = subsref(obj, index)
%SESINFO/SUBSREF Index method for SESINFO object

myerror = 0;
res = 1;
il = length(index);

if il>0 & strcmp(index(1).type,'.')
    switch index(1).subs        
        case 'R'
            b = obj.R;
        case 'groupname'
            b = obj.groupname;
        case 'clusters'
            b = obj.clusters;
        case 'nptdata'
            b = obj.framePoints;
        case 'stiminfo'
            if il==2 & strcmp(index(2).type,'.')
                switch index(2).subs
                    case 'iniInfo'
                        b=obj.stiminfo.iniInfo;
                    case 'extraSyncs'
                        b=obj.stiminfo.extraSyncs;
                    case 'framePoints'
                        b=obj.stiminfo.framePoints;  
                    case 'scanLines'
                        b=obj.stiminfo.scanLines; 
                end
            elseif il==3 & strcmp(index(2).subs,'iniInfo')
                eval(['b=obj.stiminfo.iniInfo.' index(3).subs ';'] )
            else
                b = obj.stiminfo;
            end
        case 'sessionname'
            b = obj.sessionname;
            case 'window_size'
            b = obj.window_size;
            case 'bin_size'
            b = obj.bin_size;
    end
end
