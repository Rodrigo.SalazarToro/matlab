% show_revcorr_sn.m -  static display of revcorr_sn as individual frames
%
% function show_revcorr_sn(rc,cluster,color,orientation,step_number,bins_per_step,display_mode,interp_mode)
%

function show_revcorr_individual(rc,cluster,stepNumber,binsPerStep,display_mode)


if ~exist('display_mode','var')
    display_mode = 'R';
end

switch display_mode
    case 'R'
        R=rc.R(:,:,:,cluster);
        % case 'P'
        %    R=rc.P(:,:,:,cluster);	%P is result from bar_prob.m
        % case 'R/P'
        %     
        %     %still need to fix 1/28/04
        %    [i,j] = find(rc.P(:,:,:,cluster)==0);
        %    index = sub2ind(size(rc.P(:,:,:,cluster)),i,j);
        %    % warning off
        %    R = rc.R(:,:,cluster)./rc.P(:,:,cluster);
        %    %warning backtrace
        %    %R(index) = 0;
end


m1=min(min(min(R)));
m2=max(max(max(R)));
m3=mean(R(:));
contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];

% get init_info variables
rows = rc.stiminfo.data.iniInfo.grid_x_size;
cols = rc.stiminfo.data.iniInfo.grid_y_size;
[row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);


%average over time bin
R = R(:,:,(stepNumber-1)*binsPerStep+1:stepNumber*binsPerStep);
R=mean(R,3);

%display
imagesc(R,contrast);
axis equal
axis tight


title = ([rc.sessionname '  Group: ' num2str(rc.groupname) '  Cluster: ' num2str(cluster) ...
        ' TimeRange: ' num2str((stepNumber-1)*binsPerStep) '-' num2str(stepNumber*binsPerStep) ' msec']);
h=gcf;
eh=findobj(h,'Tag','Title_box');
set(eh,'String',title)
eh=findobj(h,'Tag','DegreeTextbox');
if ~isempty(eh)
text= {['Rows span ' num2str(row_degrees,2) ' degrees'],['Columns span ' num2str(col_degrees,2) ' degrees']};
set(eh,'String',text)
end
%hcb=findobj('Tag','Colorbar');
colorbar%(hcb)
