function showMSeqRevCorr(rc,cluster)

R=rc.R(:,:,:,cluster);

for ii=1:size(R,3)
    
    title(['cluster: ' num2str(cluster) '    bin: ' num2str(ii)])
    %colormap('gray');
    imagesc(R(:,:,ii));set(gca,'Ydir','reverse');drawnow
end