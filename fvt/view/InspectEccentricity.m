function InspectEccentricity(eccentricity)

ButtonName=questdlg('Which units?', ...
   'Eccentricity Units', ...
   'Pixels','Degrees','Pixels');


if strcmp(ButtonName,'Degrees')   
      %eccentricity.avg = eccentricity.avg-transpose(ones(length(eccentricity.avg'),1)*eccentricity.fixation);
   [eccentricity.avg(1,:) , eccentricity.avg(2,:)] = pixel2degree(eccentricity.avg(1,:), eccentricity.avg(2,:))
   eccentricity.stddev = eccentricity.stddev+transpose(ones(length(eccentricity.stddev'),1)*eccentricity.fixation)
   [eccentricity.stddev(1,:) , eccentricity.stddev(2,:)] = pixel2degree(eccentricity.stddev(1,:), eccentricity.stddev(2,:))
   eccentricity.fixation=[0 0];

end 
plot(eccentricity.fixation(2),eccentricity.fixation(1),'k*');
hold on
plot(eccentricity.avg(2,:),eccentricity.avg(1,:),'.')
vert_avg = mean(eccentricity.avg(1,:));
horiz_avg = mean(eccentricity.avg(2,:));
plot(horiz_avg,vert_avg,'r*')
vert_stddev = mean(eccentricity.stddev(1,:));
horiz_stddev = mean(eccentricity.stddev(2,:));
y = vert_avg-vert_stddev:.001:vert_avg+vert_stddev;
x = horiz_avg + sqrt(horiz_stddev^2*(1-((y-vert_avg).^2)/vert_stddev^2));
plot(x,y,'.g')
x = horiz_avg - sqrt(horiz_stddev^2*(1-((y-vert_avg).^2)/vert_stddev^2));
plot(x,y,'.g')


axis([eccentricity.fixation(2)-3*max(max(eccentricity.stddev)) eccentricity.fixation(2)+3*max(max(eccentricity.stddev)) eccentricity.fixation(1)-3*max(max(eccentricity.stddev)) eccentricity.fixation(1)+3*max(max(eccentricity.stddev))])




legend('fixation position','mean eye position per trial','mean eye position per session','mean standard deviation per session')
zoom on

h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
set(edithandle,'String',eccentricity.sessionname)
set(h,'Name','Eye Eccentricity')

