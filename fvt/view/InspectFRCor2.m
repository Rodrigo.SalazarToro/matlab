function InspectFRCor2(FRCor)


clist = get(gca,'ColorOrder');
clistsize = size(clist,1);




num_cells = size(FRCor.fixation(1).SPE,2);

event={'fixation' 'saccade' 'drift'};
for type = 1:length(event)
   
   h=basicInspectGui;
   set(h,'Name',['Firing Rate Correlations for Spikes Per Binned ' char(event(type))])
	handle=findobj(h,'Tag','Titlebox');
	titletext = ['event: 'char(event(type)) '        session: ' FRCor.sessionname '        group: ' FRCor.groupname];
	set(handle,'String',titletext)

   counter=0;
   c1=0;
   
   for j = 1:num_cells
      c1=c1+1;
      for k = 0:num_cells-j
         legend_text=[];
         c2=c1+k;
         if c1~=c2
            counter = counter+1;
            if num_cells<6
               subplot(2,ceil((num_cells*(num_cells-1))/4),counter)
            else
               subplot(3,ceil((num_cells*(num_cells-1))/6),counter)
            end
            

            title(['Cells ' num2str(c1) ' and ' num2str(c2)])
            ylabel('Correlation Coefficient')
            xlabel('Time (msec)')
            hold on
            zoom on
            for m=1:size(FRCor.bins,1)
               eval(['data = FRCor.' char(event(type)) '(m).SPBE(c1,c2).corrcoef;'])
               %get rid of NaNs
               ind = max(find(~isnan(data)));
               if isempty(ind)
                  ind=1;
               end
               
					tt = FRCor.bins(m,type):FRCor.bins(m,type):FRCor.bins(m,type)*ind;
               plot(tt,data(1:ind),'-*','Color',clist(m,:))
               legend_text = strvcat(legend_text, [num2str(FRCor.bins(m,type)) ' ms']);
            end
            axis tight
            ax = axis;
            axis([ax(1) ax(2) -1 1])
            legend(gca,legend_text)
         end
      end
   end
end

