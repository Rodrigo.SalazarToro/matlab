function InspectFiringRate(fr)
%FIRINGRATE/PLOT Plots the firing rate object

%how many subplots do we need
num_clusters = size(fr.cluster,2);

%set all axis' to maximum firing rate
for i=1:num_clusters
   maxa = length(fr.cluster(i).all);
   maxf = length(fr.cluster(i).fixation);
   maxd = length(fr.cluster(i).drift);
   maxs = length(fr.cluster(i).saccade);
   m(i) = max([maxa maxf maxd maxs]);
end
m=max(m);

for i=1:num_clusters	
   subplot(4,num_clusters,i)
   his = fr.cluster(i).all;
   prob=his/sum(his);
   bar([0:length(his)-1],prob,1)
   a=axis;
   axis([-.5 m a(3) 1]);
   t=['# bins = ' num2str(sum(his))];
   text(2,.8,t)
   title(['All Cluster #' num2str(i)])
   zoom on

   
   subplot(4,num_clusters,i+num_clusters)
   his = fr.cluster(i).fixation;
   prob=his/sum(his);
	bar([0:length(his)-1],prob,1)
   a=axis;
   axis([-.5 m a(3) 1]);
   t=['# bins = ' num2str(sum(his))];
   text(2,.8,t)
   title(['Fixation Cluster #' num2str(i)])
   zoom on

   subplot(4,num_clusters,i+2*num_clusters)
   his = fr.cluster(i).drift;
   prob=his/sum(his);
	bar([0:length(his)-1],prob,1)
   a=axis;
   axis([-.5 m a(3) 1]);
   t=['# bins = ' num2str(sum(his))];
   text(2,.8,t)
   title(['Drift Cluster #' num2str(i)])
   zoom on

   subplot(4,num_clusters,i+3*num_clusters)
   his = fr.cluster(i).saccade;
   prob=his/sum(his);
	bar([0:length(his)-1],prob,1)
   a=axis;
   axis([-.5 m a(3) 1]);
   t=['# bins = ' num2str(sum(his))];
   text(2,.8,t)
   title(['Saccade Cluster #' num2str(i)])
   xlabel(['Spikes in ' num2str(fr.bin_size) ' msec bin'])
	zoom on

end
h=gcf;
edithandle=findobj(h,'Tag','Titlebox');
      title = ['session: ' fr.sessionname ' group: ' fr.groupname];

set(edithandle,'String',title)
set(h,'Name','Firing Rate Probability')








