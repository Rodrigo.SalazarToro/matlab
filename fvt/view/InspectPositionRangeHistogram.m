function InspectPositionRangeHistogram(position_range,histogram)
%function InspectPositionRangeHistogram(position_range,bin_length,time_start,time_stop)
%
%shows the Position Range Histograms for drifts and fixations
%position_range is the output from fvtPositionRangeHistogram
%the default bin_length = [.01 .01 1]


if nargin==1 | isempty(histogram.bin_length)
    bin_length = [.01 1];
else
    bin_length = histogram.bin_length;
end
num_axis = 0;

%show histogram of ranges
if ~isempty(position_range.fixation)
    num_bins=ceil((max(position_range.fixation))/bin_length(1));
    bin_edges = [0:bin_length(1):bin_length(1)*num_bins];
    n=histc(position_range.fixation,bin_edges);
    
    subplot('position',[.05 .3 .4 .5])
    num_axis = num_axis+1;
    zoom on
    bar(bin_edges,n)
    title('Fixation Position Ranges')
    xlabel('displacement (degrees)')
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(position_range.fixation),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(position_range.fixation),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(position_range.fixation),3)])
end

if ~isempty(position_range.saccade)
    num_bins=ceil((max(position_range.saccade))/bin_length(2));
    bin_edges = [0:bin_length(2):bin_length(2)*num_bins];
    n=histc(position_range.saccade,bin_edges);
    subplot('position',[.55 .3 .4 .5])
    num_axis = num_axis+1;
    zoom on
    bar(bin_edges,n)
    title('Saccade Position Ranges')
    xlabel('displacement (degrees)')
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(position_range.saccade),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(position_range.saccade),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(position_range.saccade),3)])
end



h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
set(edithandle,'String',position_range.sessionname)
set(h,'Name','Eye Event Position Range Histograms')
s.functionname=mfilename;
s.data=position_range;
set(h,'UserData',s);
edithandle = findobj(h,'Tag','bin_length_editbox');
if ~isempty(edithandle)
    set(edithandle,'String',num2str(bin_length))
end

edithandle = findobj(h,'Tag','AxisListbox');
if ~isempty(edithandle)
    num_axis = [1:num_axis]';
    set(edithandle,'String',num2str(num_axis))
end