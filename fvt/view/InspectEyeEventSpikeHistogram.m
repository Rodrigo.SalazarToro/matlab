function InspectEyeEventSpikeHistogram(eyeeventspike)

bin_length = eyeeventspike.bin_length;
%plot the PSTHs

num_clusters = size(eyeeventspike.cluster,2);

for cluster_num = 1:num_clusters
   
   %FIXATIONS 
   
   AFR = eyeeventspike.cluster(cluster_num).fixation.AFR;
   tt = -100+bin_length(1):bin_length(1):-100+length(AFR)*bin_length(1);
   %get AFR through 400 msecs 
   if max(tt)>400
      tt = -100+bin_length(1):bin_length(1):400;
      AFR = AFR(1:length(tt));
   end
   if num_clusters < 10
      subplot('position',[(1+(cluster_num-1)*3.5)/((num_clusters-1)+2+num_clusters*2.5)  .4 3.5/((num_clusters-1)+4+num_clusters*2.5) .42])
   else
      subplot(2,ceil(num_clusters/2),cluster_num)
   end
   
   %subplot(1,num_clusters,cluster_num) 
   l=plot(tt,AFR,'k');
   set(l,'LineWidth',2);
   text=['Cluster ' num2str(cluster_num)];
   title(text);
   
   if cluster_num==1
      ylabel('Average Firing Rate (Hz)')
   end
   zoom on
   hold on
   
   %DRIFTS
   AFR = eyeeventspike.cluster(cluster_num).drift.AFR;
   tt = -100+bin_length(3):bin_length(3):-100+length(AFR)*bin_length(3);
 %get AFR through 400 msecs 
   if max(tt)>400
      tt = -100+bin_length(3):bin_length(1):400;
      AFR = AFR(1:length(tt));
   end
l=plot(tt,AFR,'b');
   set(l,'LineWidth',2);
   
   %SACCADES
   %first get rid of data after 80 msec
   %num=ceil(60/eyeeventspike.bin_length(2));
   %AFR = eyeeventspike.cluster(cluster_num).saccade.AFR(1,1:num);
   AFR = eyeeventspike.cluster(cluster_num).saccade.AFR;
   tt = -100+bin_length(2):bin_length(2):-100+length(AFR)*bin_length(2);
   l=plot(tt,AFR,'r');
   set(l,'LineWidth',2);
   
   a(cluster_num,:)=axis;
   %axis([a(cluster_num,1) 400 a(cluster_num,3) a(cluster_num,4)])
   if cluster_num==1
      legend(['Fixation ' num2str(bin_length(1)) 'ms'],['Drift ' num2str(bin_length(3)) 'ms'],['Saccade ' num2str(bin_length(2)) 'ms']);
   end
   
   hold off
end

for cluster_num = 1:num_clusters
   if num_clusters < 10
      subplot('position',[(1+(cluster_num-1)*3.5)/((num_clusters-1)+2+num_clusters*2.5)  .4 3.5/((num_clusters-1)+4+num_clusters*2.5) .42])
   else
      subplot(2,ceil(num_clusters/2),cluster_num)
   end
   
   axis([-100 400  0 max(a(:,4))])
end


%now plot number of occurences
if num_clusters < 10
   subplot('position',[.2 .15 .5 .2])
   tt = -100+bin_length(1):bin_length(1):-100+length(eyeeventspike.num_bins.fixation)*bin_length(1);
   plot(tt,eyeeventspike.num_bins.fixation,'k')
   hold on
   tt = -100+bin_length(3):bin_length(3):-100+length(eyeeventspike.num_bins.drift)*bin_length(3);
   plot(tt,eyeeventspike.num_bins.drift,'b')
   tt = -100+bin_length(2):bin_length(2):-100+length(eyeeventspike.num_bins.saccade)*bin_length(2);
   plot(tt,eyeeventspike.num_bins.saccade,'r')
   a=axis;
   axis([-150 800  a(3) a(4)])
   xlabel('time from start of event (msec)')
   ylabel('number of eye event occurrences')
end







h=gcf;
handle=findobj(h,'Tag','Titlebox');
string = ['session: ' eyeeventspike.sessionname ' group: ' eyeeventspike.groupname];
set(handle,'String',string)
set(h,'Name','Eye Event Spike Histogram')
