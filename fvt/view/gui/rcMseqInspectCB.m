function InspectCB(action)

pos = [ 0.2 0.18 .7 0.758620689655172 ];

%find the Display Mode
h=gcbf;
%edithandle = findobj(h,'Tag','DisplayModeListBox');
%display_value = get(edithandle,'Value');
display_value=1;
display_mode = {'R';'P';'R/P'};		

switch(action)
    case 'CLPrevious'
        %change cluster
        s = get(gcbf,'UserData');
        [cluster,s.clev] = Decrement(s.clev);
        edithandle = findobj(h,'Tag','CLEditText');
        set(edithandle,'String',num2str(cluster));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
    case 'CLNext'
        
        %change cluster
        s = get(gcbf,'UserData');
        [cluster,s.clev] = Increment(s.clev);
        edithandle = findobj(h,'Tag','CLEditText');
        set(edithandle,'String',num2str(cluster));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
        
    case 'CLNumber'
        %change cluster
        s = get(gcbf,'UserData');
        edithandle = findobj(h,'Tag','CLEditText');
        cluster = str2num(get(edithandle,'String'));
        s.clev = SetEventNumber(s.clev,cluster);
        set(edithandle,'String',num2str(cluster));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
        
    case 'TPrevious'
        %change time
        s = get(gcbf,'UserData');
        [t,s.tev] = Decrement(s.tev);
        edithandle = findobj(h,'Tag','TEditText');
        set(edithandle,'String',num2str(t));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
    case 'TNext'
        %change time
        s = get(gcbf,'UserData');
        [t,s.tev] = Increment(s.tev);
        edithandle = findobj(h,'Tag','TEditText');
        set(edithandle,'String',num2str(t));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
    case 'TNumber'
        %change time
        s = get(gcbf,'UserData');
        edithandle = findobj(h,'Tag','TEditText');
        t = str2num(get(edithandle,'String'));
        s.tev = SetEventNumber(s.tev,t);
        set(edithandle,'String',num2str(t));
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
    case 'bins_per_stepCB'
        
        s = get(gcbf,'UserData');
        edithandle = findobj(h,'Tag','bins_per_stepEditText');
        bins_per_step = str2num(get(edithandle,'String'));
        num_steps=floor(s.obj.window_size/bins_per_step);
        s.tev = event(1,num_steps);
        time = GetEventNumber(s.tev);
        edithandle = findobj(h,'Tag','TEditText');
        set(edithandle,'String',time);
        set(gcbf,'UserData',s);
        rcMseqInspectCB('All')
        
        
    case 'All'
        AllTimesHandle = findobj(h,'Tag','AllTimes');
        AllClustersHandle = findobj(h,'Tag','AllClusters');
        s = get(gcbf,'UserData');
        edithandle = findobj(h,'Tag','bins_per_stepEditText');
        bins_per_step = str2num(get(edithandle,'String'));
        AllTimes = get(AllTimesHandle,'Value');
        AllClusters = get(AllClustersHandle,'Value');
        if AllTimes 
            timeBinNumber = 'all';
        else
            edithandle = findobj(h,'Tag','TEditText');
            timeBinNumber = GetEventNumber(s.tev);
        end
        if AllClusters
            cluster='all';
        else
            cluster = GetEventNumber(s.clev);
        end
        subplot(1,1,1)
        if AllClusters | AllTimes
            show_revcorr_MSeq(s.obj,cluster,timeBinNumber, bins_per_step);
        else
            show_revcorr_individualMSeq(s.obj,cluster,timeBinNumber,bins_per_step);
        end
        %keep zoom and interpolate&contour
        edithandle = findobj(h,'Tag','LinkedZoom');
        if get(edithandle,'Value')
            rcMseqInspectCB('LinkedZoom')
        end
        
        edithandle = findobj(h,'Tag','Interpolate');
        if get(edithandle,'Value')
            rcMseqInspectCB('Interpolate')
        end
        
        edithandle = findobj(h,'Tag','Contour');
        if get(edithandle,'Value')
            rcMseqInspectCB('Contour')
        end
        
        drawnow            
        
        
    case 'Interpolate'
        %grabs the displayed data
        %oversample
        %gaussain filter
        %replot with same axis values      
        s = get(gcbf,'UserData'); 
        edithandle = findobj(h,'Tag','Interpolate');
        interp_value = get(edithandle,'Value');
        
        axesh = findobj(h,'Type','axes','Tag','');
        
        
        if interp_value >= 1
            edithandle = findobj(h,'Tag','Oversample');
            oversample = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','GaussSize');
            siz = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','GaussSigma');
            sigma = str2num(get(edithandle,'String'));
            
            for ii=1:length(axesh)
                axes(axesh(ii))
                a=round(axis);
                %keep title
                titleText = get(get(axesh(ii),'Title'),'String');
                %get data
                hsurf = findobj(gcf,'Parent',axesh(ii));
                data = get(hsurf,'CData');
                if a(2)>size(data,1) | a(4)>size(data,2) | a(1)<1 | a(3)<1
                    data=data;
                else
                    data = data(a(3):a(4),a(1):a(2));
                end
                
                contrast = get(axesh(ii),'CLim');
                
                data = imresize(data,oversample); 
                origS = size(data); 
                f=TwoDimGaussFilter(siz,sigma);
                data = filter2(f,data,'valid');
                newS = size(data);
                
                imagesc(data,contrast)
                title(titleText)
                
                drawnow
                axis equal
                axis tight
                
            end
            %readjust ticks and then change to pixels
            reduc = newS./origS; %filtering reduces the size of the image slightly
            %original image went from axis
            xticks = str2num(get(axesh(ii),'XTickLabel'));
            yticks = str2num(get(axesh(ii),'YTickLabel'));
            xnewrange = (a(2)-a(1))*reduc(2);
            xticks = a(1) + xticks/(size(data,2))*xnewrange;
            ynewrange = (a(4)-a(3))*reduc(1);
            yticks = a(3) + yticks/size(data,1)*ynewrange;
            [xticks, yticks] = MSeqGrid2degree(s.obj,xticks,yticks);
            x=sprintf('%6.1f|',xticks);
            y=sprintf('%6.1f|',yticks);
            set(axesh,'XTickLabel',x)
            set(axesh,'YTickLabel',y)
            
        else    
            rcMseqInspectCB('All')
        end
        
    case 'CalculateXT'  
        
        if get(gcbo,'Value')
            %get parameters
            edithandle = findobj(h,'Tag','XTOversample');
            XTOversample = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','XTGaussianSize');
            XTGaussianSize = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','XTSigma');
            XTSigma = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','XTPercentile');
            XTPercentile = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','XTOverlap');
            XTOverlap = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','XTDurationSigma');
            XTDurationSigma = str2num(get(edithandle,'String'));
            
            s=get(gcf,'UserData');
            %all clusters? 
            AllClustersHandle = findobj(h,'Tag','AllClusters');
            if get(AllClustersHandle,'Value')
                clusters = 1:size(s.obj.data.R,4);
            else
                edithandle = findobj(h,'Tag','CLEditText'); 
                clusters = str2num(get(edithandle,'String'));
            end
            
            RF = RFproperties(s.obj,clusters,XTOversample,XTGaussianSize,XTSigma,XTPercentile,XTOverlap,XTDurationSigma);
            fprintf('\nChanging from pixels to Degrees...\n')
            RF = MSeq2degree(s.obj,RF);  
            s.RFproperties=RF;
            set(gcbf,'UserData',s);
            
        end
        
    case 'DisplayXT' 
        if get(gcbo,'Value')
            
            s = get(gcbf,'UserData'); 
            %all clusters? 
            AllClustersHandle = findobj(h,'Tag','AllClusters');
            if get(AllClustersHandle,'Value')
                clusters = 1:size(s.obj.data.R,4);
            else
                edithandle = findobj(h,'Tag','CLEditText'); 
                clusters = str2num(get(edithandle,'String'));
            end
            showXT(s.obj,s.RFproperties,clusters)
        end
        
        
    case 'LinkedZoom'
        edithandle = findobj(h,'Tag','LinkedZoom');
        zoomValue = get(edithandle,'Value');
        
        if zoomValue >= 1
            s=get(gcf,'UserData');
            axesh = findobj(h,'Type','axes','Tag','');
            
            edithandle = findobj(h,'Tag','XRange');
            xrange = str2num(get(edithandle,'String'));
            edithandle = findobj(h,'Tag','YRange');
            yrange = str2num(get(edithandle,'String'));
            [xrange, yrange] = MSeqGrid2degree(s.obj,xrange,yrange,'grid');
            
            axis equal
            axis(axesh,[xrange yrange])
            
            y = get(axesh(1),'YTick');
            x = get(axesh(1),'XTick');
            [xdeg, ydeg] = MSeqGrid2degree(s.obj,x,y,'deg');
            x=sprintf('%6.1f|',xdeg);
            y=sprintf('%6.1f|',ydeg);
            set(axesh,'XTickLabel',x);
            set(axesh,'YTickLabel',y);
            
            if strcmp(s.obj.stiminfo.data.iniInfo.m_seq_size,'16x16 (order=16)')
                numRows = 16;
                numCols = 16;
            elseif strcmp(s.obj.stiminfo.data.iniInfo.m_seq_size,'64x64 (order=16)')
                numRows = 64;
                numCols = 64;
            else
                siz= s.obj.stiminfo.data.iniInfo.m_seq_size;
                numRows=siz(1); numCols=siz(2); 
            end
            %mseqSize = s.obj.stiminfo.iniInfo.m_seq_size;
            rows = (yrange(2)-yrange(1))*s.obj.stiminfo.data.iniInfo.grid_x_size/numRows;
            cols = (xrange(2)-xrange(1))*s.obj.stiminfo.data.iniInfo.grid_y_size/numCols;
            [row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);
            eh=findobj(h,'Tag','DegreeTextbox');
            text = get(eh,'String');
            text = [text(1:2);{[];['Vertical - ' num2str(row_degrees,2) ' degrees'];['Horiz - ' num2str(col_degrees,2) ' degrees']}];
            set(eh,'String',text)
            
        else
            rcMseqInspectCB('All')
        end
        
    case 'Contour' 
        l = findobj(h,'Type','line');
        delete(l)
        
        edithandle = findobj(h,'Tag','Contour');
        contourValue = get(edithandle,'Value');
        if contourValue >= 1
            
            
            axesh = findobj(h,'Type','axes','Tag','');
            edithandle = findobj(h,'Tag','NumContours');
            numContour = str2num(get(edithandle,'String'));
            
            for ii=1:length(axesh)
                axes(axesh(ii))
                %get data
                hsurf = findobj(gcf,'Parent',axesh(ii));
                data = get(hsurf,'CData');
                contrast = caxis;
                v = [contrast(1):(contrast(2)-contrast(1))/numContour:contrast(2)];
                hold on
                imcontour(data,v,'-k')
                hold off
                
                %find max2(abs(data)
                %then find the two nearest 1st derivative zeros.
                %draw a curve through these three points
                %plot the values along the curve
            end
        end
end


%update textboxes
s = get(gcbf,'UserData');

%time
time = GetEventNumber(s.tev);
edithandle = findobj(h,'Tag','bins_per_stepEditText');
bins_per_step = str2num(get(edithandle,'String'));
num_steps=floor(s.obj.window_size/bins_per_step);
edithandle=findobj(h,'Tag','TimeText');
timetext = [num2str((time-1)*bins_per_step) '-' num2str(time*bins_per_step) ' msec'];
%set(edithandle,'String',timetext);







