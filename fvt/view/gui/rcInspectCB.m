function InspectCB(action)

pos = [ 0.2 0.18 .7 0.758620689655172 ];

%find the Display Mode
h=gcbf;
edithandle = findobj(h,'Tag','DisplayModeListBox');
display_value = get(edithandle,'Value');
display_mode = {'R';'P';'R/P'};		

switch(action)
case 'Load'
   [filename,path] = uigetfile('*.*','Select data file');
   if(filename~=0)
      cd(path);
      obj = CreateDataObject(filename);
   end

case 'CLPrevious'
   %change cluster
   s = get(gcbf,'UserData');
   [cluster,s.clev] = Decrement(s.clev);
   edithandle = findobj(h,'Tag','CLEditText');
   set(edithandle,'String',cluster);
   
   %set everything else to 1
   %color   
   s.coev = event(1,s.obj.stiminfo.data.iniInfo.num_colors);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',1);
   %orientation
   s.oev = event(1,s.obj.stiminfo.data.iniInfo.num_orientations);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',1);
   %time
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',1);
   
   
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   set(gcbf,'UserData',s);
   
   %plot
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      %call show_revcorr_sn
      show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
   else
      SNshow_revcorr_individual(s.obj,cluster,1,1,1,bins_per_step, char(display_mode(display_value)), interp_mode);
   end
   
case 'CLNext'
   %change cluster
   s = get(gcbf,'UserData');
   [cluster,s.clev] = Increment(s.clev);
   edithandle = findobj(h,'Tag','CLEditText');
   set(edithandle,'String',cluster);
   
   
   %set everything else to 1
   %color   
   s.coev = event(1,s.obj.stiminfo.data.iniInfo.num_colors);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',1);
   %orientation
   s.oev = event(1,s.obj.stiminfo.data.iniInfo.num_orientations);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',1);
   %time
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',1);
   
   
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   set(gcbf,'UserData',s);
   
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
   else
      SNshow_revcorr_individual(s.obj,cluster,1,1,1,bins_per_step, char(display_mode(display_value)), interp_mode);
   end
   
case 'CLNumber'
   %change cluster
   cluster = eval(get(gcbo,'String'));
   s = get(gcbf,'UserData');
   s.clev = SetEventNumber(s.clev,cluster);
   edithandle = findobj(h,'Tag','CLEditText');
   set(edithandle,'String',cluster);
   
   %set everything else to 1
   %color   
   s.coev = event(1,s.obj.stiminfo.data.iniInfo.num_colors);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',1);
   %orientation
   s.oev = event(1,s.obj.stiminfo.data.iniInfo.num_orientations);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',1);
   %time
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',1);
   
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   set(gcbf,'UserData',s);
   
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
   else
      SNshow_revcorr_individual(s.obj,cluster,1,1,1,bins_per_step, char(display_mode(display_value)), interp_mode);
   end
   
case 'COPrevious'
   %change Color
   s = get(gcbf,'UserData');
   [co,s.coev] = Decrement(s.coev);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',co);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
case 'CONext'
   %change Color
   s = get(gcbf,'UserData');
   [co,s.coev] = Increment(s.coev);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',co);
   
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
case 'CONumber'
   %change Color
   co = eval(get(gcbo,'String'));
   s = get(gcbf,'UserData');
   s.coev = SetEventNumber(s.coev,co);
   edithandle = findobj(h,'Tag','COEditText');
   set(edithandle,'String',co);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
   
case 'OPrevious'
   %change Orientation
   s = get(gcbf,'UserData');
   [o,s.oev] = Decrement(s.oev);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',o);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
   
case 'ONext'
   %change Orientation
   s = get(gcbf,'UserData');
   [o,s.oev] = Increment(s.oev);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',o);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
   
   
case 'ONumber'
   %change orientaion
   o = eval(get(gcbo,'String'));
   s = get(gcbf,'UserData');
   s.oev = SetEventNumber(s.oev,o);
   edithandle = findobj(h,'Tag','OEditText');
   set(edithandle,'String',o);
   
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
case 'TPrevious'
   %change time
   s = get(gcbf,'UserData');
   
   [t,s.tev] = Decrement(s.tev);
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',t);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
   
   
case 'TNext'
   %change time
   s = get(gcbf,'UserData');
   
   [t,s.tev] = Increment(s.tev);
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',t);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
case 'TNumber'
   t = eval(get(gcbo,'String'));
   s = get(gcbf,'UserData');
   
   s.tev = SetEventNumber(s.tev,t);
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',t);
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      set(edithandle,'Value',0)
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
   end
   
   %keep everything below the same
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   set(gcbf,'UserData',s);
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else 
      interp_mode = 'flat';
   end
SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   
   
case 'bins_per_stepCB'
   s = get(gcbf,'UserData');
   
   edithandle = findobj(h,'Tag','bins_per_stepEditText');
   bins_per_step = str2num(get(edithandle,'String'));
   
   num_steps=floor(s.obj.window_size/bins_per_step);
   s.tev = event(1,num_steps);
   cluster = GetEventNumber(s.clev);
   color = GetEventNumber(s.coev);
   orientation = GetEventNumber(s.oev);
   time = GetEventNumber(s.tev);
   edithandle = findobj(h,'Tag','TEditText');
   set(edithandle,'String',time);
   
   %plot
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
if interp_value >= 1
         interp_mode = 'interp';
      else 
         interp_mode = 'flat';
      end
   if Entire_value >= 1
      %call show_revcorr_sn
      show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
      
   else
      set(gcbf,'UserData',s);
      
   SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
      
   end
   
case 'Entire'
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   if interp_value >= 1
      interp_mode = 'interp';
   else
      interp_mode= 'flat';
   end
   
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   if Entire_value >= 1
      s = get(gcbf,'UserData');
      cluster = GetEventNumber(s.clev);
      edithandle = findobj(h,'Tag','bins_per_stepEditText');
      bins_per_step = str2num(get(edithandle,'String'));
      %call show_revcorr_sn
      subplot(1,1,1)
      show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
   else
      subplot(1,1,1)
      edithandle = gca;
      set(edithandle,'Position',pos);
      
      s = get(gcbf,'UserData');
      edithandle = findobj(h,'Tag','bins_per_stepEditText');
      bins_per_step = str2num(get(edithandle,'String'));
      num_steps=floor(s.obj.window_size/bins_per_step);
      s.tev = event(1,num_steps);
      cluster = GetEventNumber(s.clev);
      color = GetEventNumber(s.coev);
      orientation = GetEventNumber(s.oev);
      time = GetEventNumber(s.tev);
   SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
   end
   set(gcbf,'UserData',s);
   
case 'Interpolate'
   edithandle = findobj(h,'Tag','InterpolateCheckbox');
   interp_value = get(edithandle,'Value');
   edithandle = findobj(h,'Tag','EntireCheckbox');
   Entire_value = get(edithandle,'Value');
   
   if Entire_value >= 1 
      if interp_value >= 1
         interp_mode = 'interp';
         s = get(gcbf,'UserData');
         cluster = GetEventNumber(s.clev);
         edithandle = findobj(h,'Tag','bins_per_stepEditText');
         bins_per_step = str2num(get(edithandle,'String'));
         %call show_revcorr_sn
         subplot(1,1,1)
         show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
      else 
         interp_mode = 'flat';
         s = get(gcbf,'UserData');
         cluster = GetEventNumber(s.clev);
         edithandle = findobj(h,'Tag','bins_per_stepEditText');
         bins_per_step = str2num(get(edithandle,'String'));
         %call show_revcorr_sn
         subplot(1,1,1)
         show_revcorr_sn(s.obj, bins_per_step, cluster, char(display_mode(display_value)), interp_mode);
      end
   else
      if interp_value >= 1
         interp_mode = 'interp';
         subplot(1,1,1)
         edithandle = gca;
         set(edithandle,'Position',pos);
         
         s = get(gcbf,'UserData');
         edithandle = findobj(h,'Tag','bins_per_stepEditText');
         bins_per_step = str2num(get(edithandle,'String'));
         num_steps=floor(s.obj.window_size/bins_per_step);
         %s.tev = event(1,num_steps);
         cluster = GetEventNumber(s.clev);
         color = GetEventNumber(s.coev);
         orientation = GetEventNumber(s.oev);
         time = GetEventNumber(s.tev);
      SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
         
      else
         interp_mode = 'flat';
         subplot(1,1,1)
         edithandle = gca;
         set(edithandle,'Position',pos);
         
         s = get(gcbf,'UserData');
         edithandle = findobj(h,'Tag','bins_per_stepEditText');
         bins_per_step = str2num(get(edithandle,'String'));
         num_steps=floor(s.obj.window_size/bins_per_step);
         %s.tev = event(1,num_steps);
         cluster = GetEventNumber(s.clev);
         color = GetEventNumber(s.coev);
         orientation = GetEventNumber(s.oev);
         time = GetEventNumber(s.tev);
      SNshow_revcorr_individual(s.obj,cluster,color,orientation,time,bins_per_step, char(display_mode(display_value)), interp_mode);
         
      end
   end
   set(gcbf,'UserData',s);
   
case 'Quit'
   close(gcbf)
end


%update textboxes
s = get(gcbf,'UserData');
%color
color = GetEventNumber(s.coev);
if color==0
   colortext='dark luminance';
else
   colortext='light luminance';
end
edithandle=findobj(h,'Tag','ColorText');
set(edithandle,'String',colortext);
%orientation
orientation = GetEventNumber(s.oev);
edithandle = findobj(h,'Tag','OrientationText');
f=fieldnames(s.obj);
if sum(strcmp(f,'stiminfo'))==1
    angles = s.obj.stiminfo.data.iniInfo.bar_orientation;
else
    angles = s.obj.bar_orientation;
end
set(edithandle,'String',[num2str(angles(orientation)) ' degrees']);
%time
time = GetEventNumber(s.tev);
edithandle = findobj(h,'Tag','bins_per_stepEditText');
bins_per_step = str2num(get(edithandle,'String'));
num_steps=floor(s.obj.window_size/bins_per_step);
edithandle=findobj(h,'Tag','TimeText');
timetext = [num2str((time-1)*bins_per_step) '-' num2str(time*bins_per_step) ' msec'];
set(edithandle,'String',timetext);







