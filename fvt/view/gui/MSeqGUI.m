function h = MSeqGUI(obj,varargin)
% This function was created by saveas(...,'mfig'), or print -dmfile.
% It loads an HG object saved in binary format in the FIG-file of the
% same name.  NOTE: if you want to see the old-style M code
% representation of a saved object, previously created by print -dmfile,
% you can obtain this by using saveas(...,'mmat'). But be advised that the
% M-file/MAT-file format does not preserve some important information due
% to limitations in that format, including ApplicationData stored using
% setappdata.  Also, references to handles stored in UserData or Application-
% Data will no longer be valid if saved in the M-file/MAT-file format.
% ApplicationData and stored handles (excluding integer figure handles)
% are both correctly saved in FIG-files.
%
%load the saved object

[path, name] = fileparts(which(mfilename));
figname = fullfile(path, [name '.fig']);
if (exist(figname,'file')), open(figname), else open([name '.fig']), end
if nargout > 0, h = gcf; end

s.obj = obj;
s.clev = event(1,size(obj.data.R,4));

h = gcf;
handle = findobj(h,'Tag','bins_per_stepEditText');
bins_per_step = str2num(get(handle,'String'));
if bins_per_step > size(s.obj.data.R,3)
    bins_per_step = size(s.obj.data.R,3);
    set(handle,'String',num2str(bins_per_step))
end

num_steps = floor(s.obj.data.windowSize/bins_per_step);
s.tev = event(1,num_steps);
show_revcorr_individualMSeq(s.obj,1,1,bins_per_step,varargin{:});
set(gcf,'UserData',s);
colormap('default')




