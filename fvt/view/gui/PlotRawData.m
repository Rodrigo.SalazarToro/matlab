function varargout = PlotRawData(varargin)
% PLOTRAWDATA M-file for PlotRawData.fig
%      PLOTRAWDATA, by itself, creates a new PLOTRAWDATA or raises the existing
%      singleton*.
%
%      H = PLOTRAWDATA returns the handle to a new PLOTRAWDATA or the handle to
%      the existing singleton*.
%
%      PLOTRAWData('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTRAWDATA.M with the given input arguments.
%
%      PLOTRAWDATA('Property','Value',...) creates a new PLOTRAWDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotRawData_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotRawData_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotRawData

% Last Modified by GUIDE v2.5 13-Apr-2011 17:11:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PlotRawData_OpeningFcn, ...
    'gui_OutputFcn',  @PlotRawData_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT






%************************************************************
% --- Executes just before PlotRawData is made visible.
function PlotRawData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotRawData (see VARARGIN)

% Choose default command line output for PlotRawData
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlotRawData wait for user response (see UIRESUME)
% uiwait(handles.figure1);
kids = findallchildren(hObject);
% set(kids,'Enable','off')
set(handles.SetPath,'Enable','on')
%****************************************************************



% --- Outputs from this function are returned to the command line.
function varargout = PlotRawData_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%***************************************************************
% --- Executes on button press in SetPath.
function SetPath_Callback(hObject, eventdata, handles)
% hObject    handle to SetPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.lfp,'Value',0)
set(handles.highpass,'Value',0)
set(handles.spiketrain,'Value',0)
cwd=pwd;
path = uigetdir(pwd,'Select Session Folder')
if(path~=0)
    kids = findallchildren(gcbf);
    %     set(kids,'Enable','on')
    cd(path)
    [p n e] = fileparts(path);
    % get the last two characters so it will work with both the old and new
    % directory structures
    % get length of n
    nl = length(n);
    sessionname = n((nl-1):nl);
    
    UD = get(gcbf,'UserData');
    %     UD.path = path;
    set(gcbf,'UserData',UD);
    set(handles.SessionPath,'String',path);
    handles.path=path;
    
    if (isempty(nptDir(['*' sessionname '.bin'])) &  ...
            isempty(nptDir(['*' sessionname '.0*']))  ...
            & isempty(nptDir('highpass','CaseInsensitive')))
        set(handles.highpass,'Enable','off')
    end
    
    lfplist = nptDir('lfp','CaseInsensitive');  % cats don't have this directory
    if isempty(lfplist)
        set(handles.lfp,'Enable','off')
    end
    
    eyelist = nptDir('eye','CaseInsensitive');
    if isempty(eyelist)
        set(handles.eyemovements,'Enable','off')
    end
    
    sortlist = nptDir('sort','CaseInsensitive');
    if isempty(sortlist)
        set(handles.spiketrain,'Enable','off')
    end
    
    %if ~cat
    set([handles.PlotDuration handles.plotDurationText],'Enable','on')
    %end
    
    %if ~cat | isempty(sortlist)
    set(handles.raster,'Enable','off')
    %end
    
    if isempty(nptDir(['*' sessionname '.0*']))
        trialbased=0;
        h=[handles.ChunkSizeAcquired  ; ...
            handles.ChunkSizeEditBox  ;  ...
            handles.plotDurationText  ;  ...
            handles.PlotDuration  ];
        set(h,'Enable','on')
        set(handles.ChunkedAcquiredCheckbox,'Value',1)
        set(handles.ChunkPreprocessedCheckbox,'Value',1)
    else
        trialbased=1;
        %chunked is turned off by default
        h=[handles.ChunkSizeAcquired  ; ...
            handles.ChunkSizeEditBox  ;  ...
            handles.plotDurationText  ;  ...
            handles.PlotDuration  ];
        set(h,'Enable','off')
        set(handles.ChunkedAcquiredCheckbox,'Value',0)
        set(handles.ChunkPreprocessedCheckbox,'Value',0)
    end
    
    filelist = nptDir('*reorder.txt');
    if isempty(filelist)
        set(handles.ReorderCheckbox,'Enable','off','Value',0)
    end
    
end



function kids = findallchildren(h)
kids = [];
c = get(h,'Children');
for ii=1:length(c)
    if strcmp(get(c(ii),'Type'),'uipanel')
        kids = [kids; findallchildren(c(ii))];
    else
        kids = [kids;c(ii)];
    end
end



function rawdata_Callback(~, ~, handles)

UD = get(gcbf,'UserData');
% cd(UD.path)
[pdir,cdir] = getDataDirs('session','relative','CDNow')
dirlist = [nptDir('*.0001') nptDir('*.bin')];
[~,name] = nptFileParts(dirlist(1).name);


descriptor = nptDir('*_descriptor.txt');
if ~isempty(descriptor)
    t=getChannelNames;
    %prompt user for groups
    [s,OK] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
        'ListString',t');%,'ListSize',[160 (length(g))*1.25]);
else
    warning('No descriptor.txt file!  File index numbering used.')
    dtype = DaqType(dirlist(1).name);
    if strcmp(dtype,'Streamer')
        [numChannels,~,scan_order,headersize]=nptReadStreamerFileHeader(dirlist(1).name);
        chunkSize=500000;
    elseif strcmp(dtype,'UEI')
        data = ReadUEIFile('FileName',dirlist(1).name,'Header','Units','MilliVolts');
        sampling_rate = data.samplingRate;
        numChannels = data.numChannels;
        headersize = 90;
        chunkSize=500;
    elseif  strcmp(dtype,'mat')
        
        load(dirlist(1).name,'-mat');
        headersize = 90;
        chunkSize=500;
    else
        error('unknown file type')
    end
    % create vector of channels
    c = 1:numChannels;
    % prompt user for channel number
    [s,OK] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
        'ListString',cellstr(num2str(c')));%,'ListSize',[160 (numChannels)*18]);
end
if OK==1
    
    gainValue =     str2double(get(handles.GainEditBox,'String'));
    stackedValue =  get(handles.stacked,'Value');
    reorderValue =  get(handles.ReorderCheckbox,'Value');
    if get(handles.ChunkedAcquiredCheckbox,'Value')
        chunksizeValue =     str2double(get(handles.ChunkSizeEditBox,'String'));
    else
        chunksizeValue=1000;  % 16 minutes of data
    end
    
    
    
    obj = streamer(name,s,chunksizeValue);
    InspectGUI(obj,'Stacked',stackedValue,'ViewerGain',gainValue,'ReOrder',reorderValue)
    
end



function highpassdata_Callback(hObject, eventdata, handles)
UD = get(gcbo,'UserData');
% cd(UD.path)
[pdir,cdir] = getDataDirs('session','relative','CDNow')
if exist('highpass','dir')
    try
        cd([handles.SessionPath.String '/highpass'])
    catch
        cd highpass
    end
    dirlist = nptDir('*.0001');
    if isempty(dirlist)
        dirlist = nptDir('*.bin');
    end
    [~,name] = nptFileParts(dirlist(1).name);
    [t,s]=getChannelNames;
    
    %prompt user for groups
    [s,OK] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
        'ListString',t');%,'ListSize',[160 (length(g))*1.25]);
    
    %             numChannels = nptReadStreamerFileHeader(dirlist(1).name);
    %         % create vector of channels
    %         c = 1:numChannels;
    %         % prompt user for channel number
    %         [s,OK] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
    %             'ListString',cellstr(num2str(c')));%,'ListSize',[160 (numChannels)*18]);
    
    if OK %sel_group_index
        stackedValue =  get(handles.stacked,'Value');
        reorderValue =  0 %only reordr rawrawdata
        gainValue =     str2double(get(handles.GainEditBox,'String'));
        if get(handles.ChunkedAcquiredCheckbox,'Value')
            chunksizeValue =     str2double(get(handles.ChunkSizeEditBox,'String'));
        else
            chunksizeValue=1000;  % 16 minutes of data
        end
        obj = streamer(name,s,chunksizeValue);
        InspectGUI(obj,'Stacked',stackedValue,'ViewerGain',gainValue,'ReOrder',reorderValue)
    end
else
    fprintf(1,'No Highpass directory \n')
    return
end


function lfpdata_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
% cd(UD.path)
if exist('lfp','dir')
    try
        cd('lfp')
    end
    dirlist = nptDir('*.0001');
    if isempty(dirlist)
        dirlist = nptDir('*.bin');
    end
    [path,name] = nptFileParts(dirlist(1).name);
    [t,s]=getChannelNames;
    
    %prompt user for groups
    [s,OK] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
        'ListString',t');%,'ListSize',[160 (length(g))*1.25]);
    
    %             numChannels = nptReadStreamerFileHeader(dirlist(1).name);
    %         % create vector of channels
    %         c = 1:numChannels;
    %         % prompt user for channel number
    %         [s,OK] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
    %             'ListString',cellstr(num2str(c')));%,'ListSize',[160 (numChannels)*18]);
    
    
    if OK %sel_group_index
        stackedValue =  get(handles.stacked,'Value');
        reorderValue =  0 %only reordr rawrawdata
        gainValue =     str2double(get(handles.GainEditBox,'String'));
        if get(handles.ChunkedAcquiredCheckbox,'Value')
            chunksizeValue =     str2double(get(handles.ChunkSizeEditBox,'String'));
        else
            chunksizeValue=1000;  % 16 minutes of data
        end
        obj = streamer(name,s,chunksizeValue);
        InspectGUI(obj,'Stacked',stackedValue,'ViewerGain',gainValue,'ReOrder',reorderValue)
    end
else
    fprintf(1,'No LFP directory \n')
    return
end


function [sel_group,sel_group_index,OK] = getGroups
%must be in the session folder
sel_group=[];
%need to read the descriptor file to find group info
descriptor = nptDir('*_descriptor.txt');

if size(descriptor,1) > 1
    p = pwd;
    i = strfind(p,filesep);
    des = [p((i(end-2)+1):(i(end-1)-1)) p((i(end-1)+1):(i(end)-1)) p((end-1) : end) '_descriptor.txt'];
    descriptor = nptDir(des);
end

if ~isempty(descriptor)
    descriptor_info = ReadDescriptor(descriptor.name);
    neurongroup_info=GroupSignals(descriptor_info);
    groups = 1:size(neurongroup_info,2);
    g=[];
    for ii=groups
        g = [g ; neurongroup_info(ii).group];
    end
    
    %these are the recorded channels minus eye and trigger
    t = cellstr(num2str(g));
    plot_dir = pwd;
    %get reconstruction or histology information
    try
        if isempty(nptDir('*.bhv*'));
            N = NeuronalHist;
        else
            N = NeuronalHist('ml');
        end
        for iii=1:size(t,1)
            %loc = N.level1{iii};
            t{iii}=[ t{iii}];% ' - ' loc(1,(1:end-3))];
        end
    catch
        %if there is no reconstruction information then do nothing
    end
    cd(plot_dir) %if catch a cd .. occurs
    
    plot_dir = pwd;
    %get SNRs
    try
        cd('highpass')
        load SNR_channels
        %find snrs that correspond to groups
        ni = find(descriptor_info.group~=0);
        snr = round(channel_snr_list(:,2)*100)/100;
        snr_groups = channel_snr_list(:,1);
        a_groups = transpose(descriptor_info.group);
        all_groups = a_groups(find(a_groups));
        for q = 1:size(ni,2)
            ggg = all_groups(q);
            s = snr(find(snr_groups == ggg)); %match the group to the correct snr
            area = N.level1{snr_groups == ggg}; %get area information fron NeuronalHist
            t{q}=[ t{q} ' - ' area ' - ' num2str(s)];
        end
        
    catch
        %if there is no reconstruction information then do nothing
    end
    cd(plot_dir) %if catch a cd .. occurs
    
    %prompt user for groups
    [sel_group_index,OK] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
        'ListString',t);%,'ListSize',[160 (length(g))*1.25]);
    sel_group = g(sel_group_index);
    
    
    % % % % %
    % % % % %     %prompt user for groups
    % % % % %     [sel_group_index,OK] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
    % % % % %         'ListString',cellstr(num2str(g)));%,'ListSize',[160 (length(g))*1.25]);
    % % % % %     sel_group = g(sel_group_index);
end





function waveforms_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist = nptDir('*waveforms.bin');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*10+10]);
    if v==1
        createDataObject(dirlist(s).name)
    end
else
    errordlg('Result not found')
end

% --- Executes on button press in plot.
function plot_Callback(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

objCounter = 0; %set counter

%get path
data = get(gcbf,'UserData');
% if ~isfield(data,'path') || isempty(data.path)
%     errordlg('Must set the Start Directory.','Error')
%     return
% else
%     cd(data.path)   %assume session folder
% end

numGroupArgs = get(handles.lfp,'Value') + get(handles.highpass,'Value') + ...
    get(handles.spiketrain,'Value') + get(handles.raster,'Value');
if numGroupArgs
    sel_group = getGroups;
else
    sel_group=[];
end


if strcmp('on',get(handles.PlotDuration,'Enable'))
    chunkSize = str2num(get(handles.PlotDuration,'String'));
else
    chunkSize=[];
end


%loop over groups
multObjs={};
optCounter=1;

numCellArgs = get(handles.spiketrain,'Value') + get(handles.raster,'Value');
cd sort
for gr=1:length(sel_group)
    cd(['group' num2strpad(sel_group(gr),4)  ])
    
    if numCellArgs
        clusdirlist = getCells;
        eval(['ng' num2str(gr) ' = nptgroup(''auto'',''CellList'',clusdirlist);'  ])
    end
    
    
    
    
    %for each group we want to create one nptgroup object.
    %then for each object we use the same nptgroup object for instance...
    %     inspectGUI(nptdata,'multObjs',{ng1;ng1;sp1;ng2;ng2;sp2}, ...
    %     'optArgs',{  ...
    %         {'Object',{'ispikes',{'chunksizeacquired',1}}} ...
    %         {'Object',{'adjspikespsth',{'chunksizeacquired',1,'showChunk'}}}  ...
    %         {'showStreamer','showISpikes',0,'chunksizeacquired',1} ...    %rawdata
    %         {'Object',{'ispikes',{'chunksizeacquired',1}}} ...
    %         {'Object',{'adjspikespsth',{'chunksizeacquired',1,'showChunk'}}}  ...
    %         {'showStreamer','showISpikes',0,'chunksizeacquired',1} ...    %rawdata
    %         })
    %
    %
    
    %     So we want to plot multiple objects at the same time.
    %     This is done with a nptdata object.  The nptdata object
    %     has an inspectgui function.  If the nptdata object is
    %     plotted with a �multipleObjects� argument then it will
    %     then plot each of those objects on the same figure.  To
    %     do this, each object is instantiated during the plot
    %     call in nptdata�s inspectgui.  Pretty simple so far,
    %     but it gets more complicated �
    %
    % The ispikes object is different than most objects.  Since
    % the data in this object is derived from a streamer object
    % (either a rawfile or a highpas file), additional
    % functionality has been given to the ispikes plot function
    % to either plot ispikes, ispikes and signals, or just the
    % signals.  Ispikes knows whether the ispikes data was derived
    % from a raw file in the session dir or a highpass file.  The
    % streamer constructor then cds to the correct directory to
    % get the correct data.  LFP data also needs to be displayed
    % via the ispikes function since ispikes knows which file and
    % channel each groups data is found within.
    %
    %     In DisplayData.m, if highpass and ispikes are selected the
    %     ispikes object is specified once for the highpass and once
    %     for each cluster as well.  Why doesn�t it just specify it
    %     for each cluster since the ispikes can plot both at
    %     the same time?  Good question.  This is because each cluster
    %     is a separate ispikes file (and directory) and if we did it
    %     this way then the raw data would be plotted redundantly.
    %
    %     Another complication, spikes are plotted with an nptgroup object that
    %     arranges all of the graphs next to each other and then calls the
    %     ispikes plot function within it.
    
    
    
    
    if get(handles.spiketrain,'Value')
        optArgs{optCounter}={'Object',{'ispikes',{'chunkSize',chunkSize,'linkedZoom','TickDir','out'}}};
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.highpass,'Value')
        
        optArgs{optCounter}={'showStreamer','showISpikes',0,'chunkSize',chunkSize,'linkedZoom','TickDir','out'};
        dirlist = nptDir('cluster*');
        cd(dirlist(1).name);
        eval(['sp' num2str(gr) ' = ispikes(''auto'');'  ])
        cd ..
        multObjs = [multObjs {eval(['sp' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.raster,'Value')
        if isempty(chunkSize)
            optArgs{optCounter}={'Object',{'adjspikes'}};
        else
            optArgs{optCounter}={'Object',{'adjspikes',{'chunkSize',chunkSize,'showChunk'}}};
        end
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    
    if get(handles.lfp,'Value')
        optArgs{optCounter}={'showStreamer','showLfp','showISpikes',0,'chunkSize',chunkSize,'linkedZoom','TickDir','out'};
        dirlist = nptDir('cluster*');
        cd(dirlist(1).name);
        eval(['sp' num2str(gr) ' = ispikes(''auto'');'  ])
        cd ..
        multObjs = [multObjs {eval(['sp' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
%     cd(data.path)   %assume session folder
    cd ..
end %loop over groups


if get(handles.eyemovements,'Value')
    cd('eye')
    em = eyemovements('auto','save')
    multObjs = [multObjs {em}];
    optArgs{optCounter}={'linkedZoom','TickDir','out'};
    optCounter = optCounter+1;
    cd ..
end

linkedzoomVar = get(handles.LinkedZoom,'Value');

    InspectGUI(nptdata,'multObjs',multObjs,'optArgs',optArgs,'Subplot',[size(multObjs,2) 1],'LinkedZoom',linkedzoomVar,'RemoveXLabels')

%**************************************************************************



%***************************************************
function [clusdirlist,clusterlistabs] = getCells
%must be in the group folder
%first which cells do you want to show.
[p f e] = fileparts(pwd);
clusdirlist = nptDir('cluster*');
cdl=struct2cell(clusdirlist);
ind = strmatch('cluster01m', cdl{1,1});
if ind
    fn=fieldnames(clusdirlist)
    clusdirlist=cell2struct(reshape({cdl{:,2:end} cdl{:,1}},size(cdl)),fn,1)
end
clusterlist={};
clusterlistabs={};
for ii = 1:size(clusdirlist,1)
    clusterlist = {clusterlist{:} ['''' clusdirlist(ii).name ''''] };
    clusterlistabs = {clusterlistabs{:} [ pwd filesep clusdirlist(ii).name ] };
end

clusStr = ['Select clusters in ' f ': '];

[sel_cluster,v] = listdlg('PromptString',clusStr,'SelectionMode','multiple',...
    'ListString',clusterlist,'ListSize',[160 length(clusterlist)*30],'InitialValue',[1:length(clusterlist)]);

clusterlist(setdiff([1:ii],sel_cluster))=[];
clusterlistabs(setdiff([1:ii],sel_cluster))=[];
clusdirlist(setdiff([1:ii],sel_cluster))=[];



% --- Executes on selection change in ObjectListbox.
function ObjectListbox_Callback(hObject, eventdata, handles)
% hObject    handle to ObjectListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ObjectListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        ObjectListbox
UD = get(gcbf,'UserData');
ind = get(hObject,'Value')
list = get(hObject,'String');
p = [char(UD.path) char(list(ind))]
obj = load(p);
f = fieldnames(obj);
eval(['obj = obj.' char(f(1)) ';']);
cd(fileparts(p))
inspectgui(obj)

% --- Executes during object creation, after setting all properties.
function ObjectListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ObjectListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in FindObjects.
function FindObjects_Callback(hObject, eventdata, handles)
% hObject    handle to FindObjects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UD = get(gcbf,'UserData');
cd(UD.path)
fns = FindFiles('*.mat','StartingDirectry',UD.path,'CheckSubdirs',1);
notobjects=[];
for ii=1:length(fns)
    obj = load(fns{ii});
    f = fieldnames(obj);
    eval(['obj = obj.' char(f(1)) ';']);
    if ~isobject(obj)
        notobjects = [notobjects ii];
    end
end
fns(notobjects)=[];
fnss=strrep(fns,UD.path,'');
set(handles.ObjectListbox,'String',fnss);






% --- Executes on button press in RawGroupCheckbox.
function RawGroupCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to RawGroupCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RawGroupCheckbox




% --- Executes on button press in ReorderCheckbox.
function ReorderCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to ReorderCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ReorderCheckbox
if get(hObject,'Value')
    set(handles.StatusTextbox,'String', ...
        ['When displaying RawData, only choose spike channels since '  ...
        'reorder flag is checked.  reorder.txt file must be present '  ...
        'in session directory'])
else
    set(handles.StatusTextbox,'String','')
end

% --- Executes on button press in ChunkSizeAcquired.
function ChunkSizeCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to ChunkSizeAcquired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ChunkSizeAcquired



function ChunkSizeEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to ChunkSizeEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ChunkSizeEditBox as text
%        str2double(get(hObject,'String')) returns contents of ChunkSizeEditBox as a double


% --- Executes during object creation, after setting all properties.
function ChunkSizeEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChunkSizeEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ChunkedAcquiredCheckbox.
function ChunkedAcquiredCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to ChunkedAcquiredCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ChunkedAcquiredCheckbox

h=[handles.ChunkSizeAcquired;handles.ChunkSizeEditBox];
v=get(hObject,'Value');
if v
    set(h,'Enable','on')
else
    set(h,'Enable','off')
end




% --- Executes on button press in ChunkPreprocessedCheckbox.
function ChunkPreprocessedCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to ChunkPreprocessedCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ChunkPreprocessedCheckbox
h=[handles.plotDurationText;handles.PlotDuration];
v=get(hObject,'Value');
if v
    set(h,'Enable','on')
else
    set(h,'Enable','off')
end


% --- Executes on button press in stacked.
function stacked_Callback(hObject, eventdata, handles)
% hObject    handle to stacked (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stacked

if get(hObject,'Value')
    set(handles.StatusTextbox,'String','Y axis labels will be channel numbers from descriptor file.')
else
    set(handles.StatusTextbox,'String','')
end


