function varargout = displaydata(varargin)
% DISPLAYDATA M-file for displaydata.fig
%      DISPLAYDATA, by itself, creates a new DISPLAYDATA or raises the existing
%      singleton*.
%
%      H = DISPLAYDATA returns the handle to a new DISPLAYDATA or the handle to
%      the existing singleton*.
%
%      DISPLAYDATA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYDATA.M with the given input arguments.
%
%      DISPLAYDATA('Property','Value',...) creates a new DISPLAYDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before displaydata_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to displaydata_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help displaydata

% Last Modified by GUIDE v2.5 28-Nov-2006 13:34:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @displaydata_OpeningFcn, ...
    'gui_OutputFcn',  @displaydata_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before displaydata is made visible.
function displaydata_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to displaydata (see VARARGIN)

% Choose default command line output for displaydata
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes displaydata wait for user response (see UIRESUME)
% uiwait(handles.figure1);
hchildren = get(hObject,'Children');
set(hchildren,'Enable','off')
set([handles.SetPath handles.load handles.or handles.selectFile],'Enable','on')



% --- Outputs from this function are returned to the command line.
function varargout = displaydata_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in SetPath.
function SetPath_Callback(hObject, eventdata, handles)
% hObject    handle to SetPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.lfp,'Value',0)
set(handles.highpass,'Value',0)
set(handles.spiketrain,'Value',0)
cwd=pwd
path = uigetdir(pwd,'Select Session Folder');
if(path~=0)
    hchildren = get(gcbf,'Children');
    set(hchildren,'Enable','on')
    cd(path)
    [p n e] = fileparts(path);
    % get the last two characters so it will work with both the old and new 
    % directory structures
    % get length of n
    nl = length(n);
    sessionname = n((nl-1):nl);
    
    UD = get(gcbf,'UserData');
    UD.path = path;
    set(gcbf,'UserData',UD);
    set(handles.path,'String',path);
    
    if isempty(nptDir(['*' sessionname '.0*']))
        cat=1;
    else
        cat=0;
    end
    
    if (isempty(nptDir(['*' sessionname '.bin'])) &  ...
            isempty(nptDir(['*' sessionname '.0*']))  ...
            & isempty(nptDir('highpass','CaseInsensitive')))
        set(handles.highpass,'Enable','off')
    end
    
    lfplist = nptDir('lfp','CaseInsensitive');  % cats don't have this directory
    if isempty(lfplist) 
        set(handles.lfp,'Enable','off')
    end
    
    eyelist = nptDir('eye','CaseInsensitive');
    if isempty(eyelist) 
        set(handles.eyes,'Enable','off')
        set(handles.eyemovements,'Enable','off')
    end
    
    sortlist = nptDir('sort','CaseInsensitive');
    if isempty(sortlist) 
        set(handles.spiketrain,'Enable','off')
    end
    
    if ~cat 
        set(handles.chunkSize,'Enable','off')
    end
    
    if ~cat | isempty(sortlist)
        set(handles.raster,'Enable','off')
        set(handles.psth,'Enable','off')
    end
    
end


% --- Executes on button press in plot.
function plot_Callback(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

objCounter = 0; %set counter

%get path 
data = get(gcbf,'UserData');
if ~isfield(data,'path') | isempty(data.path)
    errordlg('Must set the Start Directory.','Error')
    return
else 
    cd(data.path)   %assume session folder
end

numGroupArgs = get(handles.lfp,'Value') + get(handles.highpass,'Value') + ...
    get(handles.spiketrain,'Value') + get(handles.raster,'Value') + get(handles.psth,'Value') + get(handles.events,'Value');
if numGroupArgs
    sel_group = getGroups;
else
    sel_group=[];
end


if strcmp('on',get(handles.chunkSize,'Enable'))
    chunkSize = str2num(get(handles.chunkSize,'String'));
else
    chunkSize=[];
end


%loop over groups
multObjs={};
optCounter=1;

numCellArgs = get(handles.spiketrain,'Value') + get(handles.raster,'Value') + get(handles.psth,'Value') + get(handles.events,'Value');

for gr=1:length(sel_group)
    cd(['group' num2strpad(sel_group(gr),4)  ])
    
    if numCellArgs
        clusdirlist = getCells;
        eval(['ng' num2str(gr) ' = nptgroup(''auto'',''CellList'',clusdirlist);'  ])
    end
    
    
    
    
    %for each group we want to create one nptgroup object.
    %then for each object we use the same nptgroup object for instance...
    %     inspectGUI(nptdata,'multObjs',{ng1;ng1;sp1;ng2;ng2;sp2}, ...
    %     'optArgs',{  ...
    %         {'Object',{'ispikes',{'chunkSize',1}}} ...
    %         {'Object',{'adjspikespsth',{'chunkSize',1,'showChunk'}}}  ...
    %         {'showStreamer','showISpikes',0,'chunkSize',1} ...    %rawdata
    %         {'Object',{'ispikes',{'chunkSize',1}}} ...
    %         {'Object',{'adjspikespsth',{'chunkSize',1,'showChunk'}}}  ...
    %         {'showStreamer','showISpikes',0,'chunkSize',1} ...    %rawdata
    %         })
    % 
    %     
    
%     So we want to plot multiple objects at the same time.
%     This is done with a nptdata object.  The nptdata object
%     has an inspectgui function.  If the nptdata object is 
%     plotted with a �multipleObjects� argument then it will
%     then plot each of those objects on the same figure.  To
%     do this, each object is instantiated during the plot 
%     call in nptdata�s inspectgui.  Pretty simple so far, 
%     but it gets more complicated �
% 
% The ispikes object is different than most objects.  Since
% the data in this object is derived from a streamer object
% (either a rawfile or a highpas file), additional
% functionality has been given to the ispikes plot function
% to either plot ispikes, ispikes and signals, or just the 
% signals.  Ispikes knows whether the ispikes data was derived
% from a raw file in the session dir or a highpass file.  The 
% streamer constructor then cds to the correct directory to 
% get the correct data.  LFP data also needs to be displayed 
% via the ispikes function since ispikes knows which file and 
% channel each groups data is found within.
% 
%     In DisplayData.m, if highpass and ispikes are selected the
%     ispikes object is specified once for the highpass and once
%     for each cluster as well.  Why doesn�t it just specify it 
%     for each cluster since the ispikes can plot both at 
%     the same time?  Good question.  This is because each cluster
%     is a separate ispikes file (and directory) and if we did it
%     this way then the raw data would be plotted redundantly.    
% 
%     Another complication, spikes are plotted with an nptgroup object that
%     arranges all of the graphs next to each other and then calls the
%     ispikes plot function within it.
 
    
    
    
    if get(handles.spiketrain,'Value')
        optArgs{optCounter}={'Object',{'ispikes',{'chunkSize',chunkSize,'linkedZoom','TickDir','out'}}};
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.highpass,'Value') 

        optArgs{optCounter}={'showStreamer','showISpikes',0,'chunkSize',chunkSize,'linkedZoom','TickDir','out'};
        dirlist = nptDir('cluster*');
        cd(dirlist(1).name);
        eval(['sp' num2str(gr) ' = ispikes(''auto'');'  ])
        cd ..
        multObjs = [multObjs {eval(['sp' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.raster,'Value')
        if isempty(chunkSize)
            optArgs{optCounter}={'Object',{'adjspikes'}};
        else
            optArgs{optCounter}={'Object',{'adjspikes',{'chunkSize',chunkSize,'showChunk'}}};
        end
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.psth,'Value') & ~get(handles.events,'Value')
        if isempty(chunkSize)
            optArgs{optCounter}={'Object',{'adjspikespsth'}};
        else
            optArgs{optCounter}={'Object',{'adjspikespsth',{'chunkSize',chunkSize,'showChunk'}}};
        end
        
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.events,'Value')
        showThreshold = get(handles.eventThresholds,'Value');
        
        %Events
        if isempty(chunkSize)
            optArgs{optCounter}={'Object',{'events',{'showEvents'}}};
        else
            optArgs{optCounter}={'Object',{'events',{'chunkSize',chunkSize,'showChunk','showEvents'}}};
        end
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
        %EventsPSTH
        if isempty(chunkSize)
            optArgs{optCounter}={'Object',{'events',{'showPSTH','showThreshold',showThreshold}}};
        else
            optArgs{optCounter}={'Object',{'events',{'chunkSize',chunkSize,'showChunk','showPSTH','showThreshold',showThreshold}}};
        end
        multObjs = [multObjs {eval(['ng' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    if get(handles.lfp,'Value')
        optArgs{optCounter}={'showStreamer','showLfp','showISpikes',0,'chunkSize',chunkSize,'linkedZoom','TickDir','out'};
        dirlist = nptDir('cluster*');
        cd(dirlist(1).name);
        eval(['sp' num2str(gr) ' = ispikes(''auto'');'  ])
        cd ..
        multObjs = [multObjs {eval(['sp' num2str(gr)])}];
        optCounter = optCounter+1;
    end
    
    cd(data.path)   %assume session folder
    
end %loop over groups

if get(handles.eyes,'Value')
    ey = eyes('auto')
    multObjs = [multObjs {ey}];
    optArgs{optCounter}={'linkedZoom','TickDir','out'};
    optCounter = optCounter+1;
end

if get(handles.eyemovements,'Value')
    cd('eye')
    em = eyemovements('auto','save')
    multObjs = [multObjs {em}];
    optArgs{optCounter}={'linkedZoom','TickDir','out'};
    optCounter = optCounter+1;
    cd ..
end

linkedzoomVar = get(handles.LinkedZoom,'Value');
 
InspectGUI(nptdata,'multObjs',multObjs,'optArgs',optArgs,'Subplot',[size(multObjs,2) 1],'LinkedZoom',linkedzoomVar,'RemoveXLabels')


function [sel_group,sel_group_index] = getGroups
%must be in the session folder
sel_group=[];
%need to read the descriptor file to find group info
descriptor = nptDir('*_descriptor.txt');
if ~isempty(descriptor)
    descriptor_info = ReadDescriptor(descriptor.name);
    neurongroup_info=GroupSignals(descriptor_info);
    groups = 1:size(neurongroup_info,2);
    g=[];
    for ii=groups
        g = [g ; neurongroup_info(ii).group];
    end
    %prompt user for groups
    [sel_group_index,v] = listdlg('PromptString','Select groups to display:','SelectionMode','multiple',...
        'ListString',cellstr(num2str(g)))%,'ListSize',[160 (length(g))*1.25]);
    sel_group = g(sel_group_index);
end


function [clusdirlist,clusterlistabs] = getCells
%must be in the group folder
%first which cells do you want to show.
[p f e] = fileparts(pwd);
clusdirlist = nptDir('cluster*');
clusterlist={};
clusterlistabs={};
for ii = 1:size(clusdirlist,1)
    clusterlist = {clusterlist{:} ['''' clusdirlist(ii).name ''''] };
    clusterlistabs = {clusterlistabs{:} [ pwd filesep clusdirlist(ii).name ] };
end
clusStr = ['Select clusters in ' f ': '];

[sel_cluster,v] = listdlg('PromptString',clusStr,'SelectionMode','multiple',...
    'ListString',clusterlist,'ListSize',[160 length(clusterlist)*30],'InitialValue',[1:length(clusterlist)]);

clusterlist(setdiff([1:ii],sel_cluster))=[];
clusterlistabs(setdiff([1:ii],sel_cluster))=[];
clusdirlist(setdiff([1:ii],sel_cluster))=[];
    
% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename,path] = uigetfile('*.*','Select data file');
if(filename~=0)
    cd(path);
    obj = CreateDataObject(filename);
end


function rawdata_Callback(hObject, eventdata, handles)

UD = get(gcbf,'UserData');
cd(UD.path)
dirlist = nptDir('*.0001');
if isempty(dirlist)
    dirlist = nptDir('*.bin');
end
[path,name] = nptFileParts(dirlist(1).name);
dtype = DaqType(dirlist(1).name);
if strcmp(dtype,'Streamer')
    [numChannels,sampling_rate,scan_order]=nptReadStreamerFileHeader(dirlist(1).name);
    headersize = 73;
    chunkSize=500000;
elseif strcmp(dtype,'UEI')
    data = ReadUEIFile('FileName',dirlist(1).name,'Header');
    sampling_rate = data.samplingRate;
    numChannels = data.numChannels;
    headersize = 90;
    chunkSize=500;
else
    error('unknown file type')
end

% create vector of channels
c = 1:numChannels;
% prompt user for channel number
[s,v] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
    'ListString',cellstr(num2str(c')))%,'ListSize',[160 (numChannels)*18]);
if v==1
    obj = streamer(name,s);
    if get(handles.GainCheckBox,'Value')
        InspectGUI(obj,'Stacked','ViewerGain',str2num(get(handles.GainEditBox,'String')));
    elseif get(handles.stacked,'Value')
        InspectGUI(obj,'Stacked');
    else
        InspectGUI(obj);
    end
end



function highpassdata_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if exist('lfp','dir')
    [sel_group,sel_group_index] = getGroups;
    cd('highpass')
    dirlist = nptDir('*.0001');
    if isempty(dirlist)
        dirlist = nptDir('*.bin');
    end
    [path,name] = nptFileParts(dirlist(1).name);
    %     numChannels = nptReadStreamerFileHeader(dirlist(1).name);
    %     % create vector of channels
    %     c = 1:numChannels;
    %     % prompt user for channel number
    %     [s,v] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
    %         'ListString',cellstr(num2str(c')),'ListSize',[160 (numChannels)*18]);
    if sel_group_index
        obj = streamer(name,sel_group_index);
        if get(handles.GainCheckBox,'Value')
            InspectGUI(obj,'Stacked','ViewerGain',str2num(get(handles.GainEditBox,'String')));
        elseif get(handles.stacked,'Value')
            InspectGUI(obj,'Stacked');
        else
            InspectGUI(obj);
        end
    end
else
    error('No Highpass directory')
end






function lfpdata_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if exist('lfp','dir')
    [sel_group,sel_group_index] = getGroups;
    cd('lfp')
    dirlist = nptDir('*.0001');
    if isempty(dirlist)
        dirlist = nptDir('*.bin');
    end
    [path,name] = nptFileParts(dirlist(1).name);
%     numChannels = nptReadStreamerFileHeader(dirlist(1).name);
%     % create vector of channels
%     c = 1:numChannels;
%     % prompt user for channel number
%     [s,v] = listdlg('PromptString','Select a channel:','SelectionMode','multiple',...
%         'ListString',cellstr(num2str(c')),'ListSize',[160 (numChannels)*18]);
    if sel_group_index
        obj = streamer(name,sel_group_index);
        if get(handles.GainCheckBox,'Value')
            InspectGUI(obj,'Stacked','ViewerGain',str2num(get(handles.GainEditBox,'String')));
        elseif get(handles.stacked,'Value')
            InspectGUI(obj,'Stacked');
        else
            InspectGUI(obj);
        end
    end
else
    error('No LFP directory')
end







function waveforms_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist = nptDir('*waveforms.bin');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*10]);
    if v==1			
        createDataObject(dirlist(s).name)
    end
else
    errordlg('Result not found')
end

function eyeduration_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*duration.mat');
if ~isempty(dirlist)
    obj = load(dirlist(1).name);
    histInspectGUI(obj)
else
    errordlg('Result not found')
end

function eyevelocity_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*velocity.mat');
if ~isempty(dirlist)
    obj = load(dirlist(1).name);
    histInspectGUI(obj)
else
    errordlg('Result not found')
end

function eyepower_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*powerspectrum.mat');
if ~isempty(dirlist)
    obj = load(dirlist(1).name);
    InspectGUI(obj)
else
    errordlg('Result not found')
end

function positionrange_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*positionrange.mat');
if ~isempty(dirlist)
    obj = load(dirlist(1).name);
    histInspectGUI(obj)
else
    errordlg('Result not found')
end

function eccentricity_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*eccentricity.mat');
if ~isempty(dirlist)
    obj = load(dirlist(1).name);
    InspectGUI(obj)
else
    errordlg('Result not found')
end


function eyemovements_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
end
dirlist= nptdir('*eyemovement.mat');
obj=eyemovements('auto')
InspectGUI(obj)

function eye_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('eye')
    cd('eye')
    dirlist = nptdir('*eye.0*');
    [path,name] = nptFileParts(dirlist(1).name);
    name = name(1:end-4);    %takeoff _eye
    channels=[1 2];
    obj = eyes(name,channels);
    InspectGUI(obj)
else
    errordlg('Result not found')
end




function eyeeventspike_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist= nptdir('*eyeeventspike.mat');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*30]);
    if v==1			
        obj = load(dirlist(s).name);
        InspectGUI(obj)
    end
else
    errordlg('Result not found')
end

function isi_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist= nptdir('*ISI.mat');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*30]);
    if v==1			
        obj = load(dirlist(s).name);
        histInspectGUI(obj)
    end
else
    errordlg('Result not found')
end


function firingrate_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist= nptdir('*spikeprob.mat');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*30]);
    if v==1			
        obj = load(dirlist(s).name);
        figure
        InspectFiringRate(obj.spikeprob)
    end
else
    errordlg('Result not found')
end


function FRCor_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
if isdir('sort')
    cd('sort')
end
dirlist= nptdir('*FRcor.mat');
if ~isempty(dirlist)
    for ii=1:length(dirlist)
        st{ii} = dirlist(ii).name;
    end
    % prompt user for channel number
    [s,v] = listdlg('PromptString','Select a group:','SelectionMode','single',...
        'ListString',st,'ListSize',[250 length(dirlist)*30]);
    if v==1			
        obj = load(dirlist(s).name);
        figure
        InspectFRCor(obj.FRCor)
    end
else
    errordlg('Result not found')
end


function xymaps_Callback(hObject, eventdata, handles)
UD = get(gcbf,'UserData');
cd(UD.path)
sel_group = getGroups;

%loop over groups
optCounter=1;
cellList={};
for gr=1:length(sel_group)
    cd(['group' num2strpad(sel_group(gr),4)  ])
   
    [clusterdirlist,clusterlistabs] = getCells;
    cellList = [cellList clusterlistabs];
    cd(UD.path)
end

obj = processsession(revcorr,'Cells',cellList)
        inspectGUI(obj)



% --- Executes on button press in eyes.
function Responsiveness_Callback(hObject, eventdata, handles)
% hObject    handle to eyes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

optArgs = getCatResultParams(handles);

if ~get(handles.selectFile,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'path')
        cd(UD.path)
    end
    nd = ProcessSession(nptdata,'nptCellCmd','robj = nptdata(''Eval'',''ispresent(''''responsiveness.mat'''',''''file'''')'');');
     InspectGUI(nd,'Objects',{{'responsiveness',{optArgs{:}}}});
else
    [fname,pname,findex] = uigetfile(['*' get(hObject,'String') '*.mat'],'Select .mat File');
    if findex==1
        a = load(fullfile(pname,fname));
        obj = eval(['a.' char(fieldnames(a))])
        figure;
        plot(obj,optArgs{:});
        zoom on
    end
end

% --- Executes on button press in eyes.
function Sparsity_Callback(hObject, eventdata, handles)
% hObject    handle to eyes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

optArgs = getCatResultParams(handles);

if ~get(handles.selectFile,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'path')
        cd(UD.path)
    end
    nd = ProcessSession(nptdata,'nptCellCmd','robj = nptdata(''Eval'',''ispresent(''''sparsity.mat'''',''''file'''')'');');
     InspectGUI(nd,'Objects',{{'sparsity',{optArgs{:}}}});
else
    [fname,pname,findex] = uigetfile(['*' get(hObject,'String') '*.mat'],'Select .mat File');
    if findex==1
        a = load(fullfile(pname,fname));
        obj = eval(['a.' char(fieldnames(a))])
        figure;
        plot(obj,optArgs{:});
        zoom on
    end
end


% --- Executes on button press in eyes.
function firingRate_Callback(hObject, eventdata, handles)
% hObject    handle to eyes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

optArgs = getCatResultParams(handles);

if ~get(handles.selectFile,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'path')
        cd(UD.path)
    end
    nd = ProcessSession(nptdata,'nptCellCmd','robj = nptdata(''Eval'',''ispresent(''''firingrate.mat'''',''''file'''')'');');
    InspectGUI(nd,'Objects',{{'firingrate',{optArgs{:}}}});
else
    [fname,pname,findex] = uigetfile(['*' get(hObject,'Tag') '*.mat'],'Select .mat File');
    if findex==1
        a = load(fullfile(pname,fname));
        obj = eval(['a.' char(fieldnames(a))])
        figure;
        plot(obj,optArgs{:});
        zoom on
    end
end




% --- Executes on button press in events.
function events_Callback(hObject, eventdata, handles)
% hObject    handle to events (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of events
if get(handles.events,'Value')
    set(handles.psth,'Value',1)
end



% --- Executes on button press in loglog.
function loglog_Callback(hObject, eventdata, handles)
% hObject    handle to loglog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of loglog
if get(hObject,'Value')
    set(handles.semilogy,'Value',0)
end


% --- Executes on button press in selectFile.
function selectFile_Callback(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
UD = get(gcbf,'UserData');
UD.Path = '';

%we want to turn everything off except for stuff in this box and selectpath
%and selctfile.
badh = get(gcbf,'Children');
Tags = {'SetPath','load','selectFile','Responsiveness',  ...
        'Sparsity','FiringRate','EventDurations',  ...
        'EventProbability','NumBins','NormalizeXaxis',  ...
        'NormalizeYaxis','semilogy','loglog','text73','or','text8', ...
        'JEDurations','JEProbability','JESimilarityIndex', ...
        'JESpikeCountCorrelations','JESpikeCountCorrelationsDurations', ...
        'JEpsthCorrelation','text84','Significance', ...
        'JEShiftedSpikeCountCorrelations','heterogeneity','JointPSTHScatterPlot',...
        'ShowJointEvents'};

goodh=[];
for ii=1:length(Tags)
    goodh = [goodh ; findobj(gcbf,'Tag',Tags{ii})];
end
badh = setdiff(badh,goodh);
set(badh,'Enable','off')
set(goodh,'Enable','on')

function semilogy_Callback(hObject, eventdata, handles)
if get(hObject,'Value')
    set(handles.loglog,'Value',0)
end

function optArgs = getCatResultParams(handles)
optArgs = {'numBins',str2num(get(handles.NumBins,'String')),  ...
        'NormalizeXaxis',get(handles.NormalizeXaxis,'Value'),  ...
        'NormalizeYaxis',get(handles.NormalizeYaxis,'Value'),  ...
        'loglog',get(handles.loglog,'Value'), ...
        'semilogy',get(handles.semilogy,'Value'), ...
        'Significance',str2num(get(handles.Significance,'String')), ...
        'HeterogeneityValue',get(handles.heterogeneity,'Value')    };


function EventDurations_Callback(hObject, eventdata, handles)
% hObject    handle to eyes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

optArgs = getCatResultParams(handles);
if strcmp(get(hObject,'Tag'),'EventDurations')
    optArgs = {optArgs{:} 'showDurations' 1};
elseif  strcmp(get(hObject,'Tag'),'EventProbability')
    optArgs = {optArgs{:} 'showProbability' 1};
end

if ~get(handles.selectFile,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'path')
        cd(UD.path)
    end
    nd = ProcessSession(nptdata,'nptCellCmd','robj = nptdata(''Eval'',''ispresent(''''events.mat'''',''''file'''')'');');
    InspectGUI(nd,'Objects',{{'events',{optArgs{:}}}});
else
    [fname,pname,findex] = uigetfile('*events*.mat','Select .mat File');
    if findex==1
        a = load(fullfile(pname,fname));
        obj = eval(['a.' char(fieldnames(a))])
        figure;
        plot(obj,optArgs{:});
        zoom on
    end
end



function JointEvents_Callback(hObject, eventdata, handles)
% hObject    handle to eyes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
inspect=0;
optArgs = getCatResultParams(handles);
switch get(hObject,'Tag')
    case{'JEDurations'}    
        optArgs = {optArgs{:} 'showDurations' 1};
    case{'JEProbability'}    
        optArgs = {optArgs{:} 'showProbability' 1};
    case{'JESimilarityIndex'}    
        optArgs = {optArgs{:} 'showSimilarityIndex' 1};
    case{'JESpikeCountCorrelations'}    
        optArgs = {optArgs{:} 'showSpikeCountCorrelations' 1};
    case{'JESpikeCountCorrelationsDurations'}    
        optArgs = {optArgs{:} 'showSpikeCountCorrelationsDurations' 1};
    case{'JEpsthCorrelation'}    
        optArgs = {optArgs{:} 'showJointEventsPSTHCorrelations' 1};
    case{'JEJointEventsRaster'}    
        optArgs = {optArgs{:} 'showJointEventsRaster' 1};
    case{'JEShiftedSpikeCountCorrelations'}    
        optArgs = {optArgs{:} 'showShiftedSpikeCountCorrelations' 1};    
    case{'JointPSTHScatterPlot'}
        optArgs = {optArgs{:} 'showJointPSTHScatterPlot' 1};
        inspect=1;
    case{'ShowJointEvents'}
        optArgs = {optArgs{:} 'showJointEvents' 1}; 
        inspect=1;
end
        
if ~get(handles.selectFile,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'path')
        cd(UD.path)
    end
    nd = ProcessSession(nptdata,'nptCellCmd','robj = nptdata(''Eval'',''ispresent(''''jointevents.mat'''',''''file'''')'');');
    InspectGUI(nd,'Objects',{{'jointevents',{optArgs{:}}}});
else
    [fname,pname,findex] = uigetfile('*jointevents*.mat','Select .mat File');
    if findex==1
        a = load(fullfile(pname,fname));
        obj = eval(['a.' char(fieldnames(a))])
        if inspect
            inspectGUI(obj,optArgs{:});
        else
            figure;
            plot(obj,optArgs{:});
            zoom on
        end
    end
end


% --- Executes during object creation, after setting all properties.
function GainEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GainEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


