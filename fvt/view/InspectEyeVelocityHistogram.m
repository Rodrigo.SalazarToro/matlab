function InspectEyeVelocityHistogram(velocity,histogram)
%function InspectEyeVelocityHistogram(velocity,bin_length,time_start,time_stop)
%
%shows the Velocity Histograms for drifts and fixations for a session
%
%velocity is a structure created by fvtEyeVelocityHistograms
%the default bin_length = [.25 .25 50]

if nargin==1 | isempty(histogram.bin_length)
    bin_length = [0.25 0.25 50];
else
    bin_length = histogram.bin_length;
end

num_axis=0;

if ~isempty(velocity.fixation)
    num_bins = ceil((max(velocity.fixation(2, :)))/bin_length(1));
    bin_edges = [0:bin_length(1):bin_length(1)*num_bins];
    n=histc(velocity.fixation(2, :),bin_edges);
    
    subplot('position',[.05 .3 .275 .4])
    num_axis = num_axis+1;
    zoom on
    bar(bin_edges,n)
    
    title('Maximum Fixation Velocity');
    xlabel('Velocity (degrees/sec)')
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(velocity.fixation(2, :)),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(velocity.fixation(2, :)),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(velocity.fixation(2, :)),3)])
end

if ~isempty(velocity.fixation)
    num_bins=ceil((max(velocity.fixation(1, :)))/bin_length(1));
    bin_edges = [0:bin_length(2):bin_length(2)*num_bins];
    n=histc(velocity.fixation(1, :),bin_edges);
    
    subplot('position',[.3625 .3 .275 .4])
    num_axis = num_axis+1;
    zoom on
    bar(bin_edges,n)
    
    title('Mean Fixation Velocity')
    xlabel('Velocity (degrees/sec)')
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(velocity.fixation(1, :)),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(velocity.fixation(1, :)),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(velocity.fixation(1, :)),3)])
end


if ~isempty(velocity.saccade)
    num_bins=ceil((max(velocity.saccade))/bin_length(3));
    bin_edges = [0:bin_length(3):bin_length(3)*num_bins];
    n=histc(velocity.saccade,bin_edges);
    subplot('position',[.675 .3 .275 .4])
    num_axis = num_axis+1;
    zoom on
    %[n,x] = hist(velocity.saccade,bin_length(2));
    %bar(x,n)
    bar(bin_edges,n)
    title('Saccade Velocity');
    xlabel('Velocity (degrees/sec)')
    a=axis;
    if nargin==1 |  ~(histogram.axis_num==num_axis) | isempty(histogram.time_stop) | isempty(histogram.time_start) 
        axis([0 1.05*max(bin_edges) a(3) 1.1*max(n)])
    else
        axis([histogram.time_start histogram.time_stop a(3) 1.1*max(n)])
    end
    a=axis;
    text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(velocity.saccade),3)])
    text(.55*a(2),.87*a(4),['Median = ' num2str(median(velocity.saccade),3)])
    text(.55*a(2),.8*a(4),['Std = ' num2str(std(velocity.saccade),3)])
end


h=gcf;
edithandle = findobj(h,'Tag','Titlebox');
set(edithandle,'String',velocity.sessionname)
set(h,'Name','Eye Event Velocity Histograms')
s.functionname=mfilename;
s.data=velocity;
set(h,'UserData',s);
edithandle = findobj(h,'Tag','bin_length_editbox');
if ~isempty(edithandle)
    set(edithandle,'String',num2str(bin_length))
end
edithandle = findobj(h,'Tag','AxisListbox');
if ~isempty(edithandle)
    num_axis = [1:num_axis]';
    set(edithandle,'String',num2str(num_axis))
end

