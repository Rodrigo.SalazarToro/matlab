% show_revcorr_sn.m - static display of revcorr_sn as sequence of frames
%
% function show_revcorr_sn(R,init_info,bins_per_step,display_mode,display_mode,interp_mode)
%
%   R:              revcorr array (from revcorr_sn)
%   init_info:      init_info structure
%   bins_per_step:  number of time bins to average for each step displayed
%	 display_mode:		optional display mode: 'R'(default) 'P' or 'R/P'
%	 interp_mode:           optional shading mode: 'flat' (default) or 'interp'
%

function show_revcorr_sn(rc,bins_per_step,cluster, display_mode, interp_mode)

if ~exist('display_mode','var')
   display_mode = 'R';
end

if ~exist('interp_mode','var')
   interp_mode = 'flat';
end
R=rc.data.R;
%P=rc.P;
switch display_mode
case 'R'
   R=R(:,:,cluster);
case 'P'
   R=P(:,:,cluster);	%P is result from bar_prob.m
case 'R/P'
   [i,j] = find(P(:,:,cluster)==0);
   index = sub2ind(size(P(:,:,cluster)),i,j);
   warning off
   R = R(:,:,cluster)./P(:,:,cluster);
   warning backtrace
   %R(index) = 0;
end



R=fliplr(R);			%flipped so time goes grom left to right



% revcorr dimensions
[M N]=size(R);
%R=R(1:M-1,:);   % get rid of last row, which is outside box
%M=M-1;

num_steps=floor(N/bins_per_step);

% normalize R matrix
R=R/max(R(:));  

% get init_info variables
rows = rc.stiminfo.data.iniInfo.rows;
cols = rc.stiminfo.data.iniInfo.cols;
%[row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);
nrc = rows*cols;
num_or = rc.stiminfo.data.iniInfo.num_orientations;
angles = rc.stiminfo.data.iniInfo.bar_orientation;
num_colors = rc.stiminfo.data.iniInfo.num_colors;

 type = rc.stiminfo.data.iniInfo.type;
 
% 
% rc.stiminfo.data.iniInfo.obj_length
% rc.stiminfo.data.iniInfo.obj_width
% rc.stiminfo.data.iniInfo.obj_diameter





% revcorr map
map=zeros(rows+1,cols+1);

% center pos
cx=floor(cols/2);
cy=rows-1-floor(rows/2);

% axis size
sz=1.2*max([rows cols]);

% loop over subplots
for c=0:num_colors-1		%loop over color groups
   for k=0:num_or-1		%loop over orientation groups
      
      group=c*num_or+k;
      i=group*nrc + [1:nrc];
      
      for t=0:num_steps
         warning on
         if t==0 %draw colored, oriented bar
            subplot(num_colors*num_or, num_steps+1, group*(num_steps+1)+t+1) %+1 for bar patch
            if c==0
               h=patch([cx-cols/10 cx+cols/10 cx+cols/10 cx-cols/10],[0 0 rows rows],'k');
            else
               h=patch([cx-cols/10 cx+cols/10 cx+cols/10 cx-cols/10],[0 0 rows rows],'w');
            end
         else
            j=(t-1)*bins_per_step+[1:bins_per_step];
            r=reshape(mean(R(i,j),2),rows,cols);
            subplot(num_colors*num_or, num_steps+1, group*(num_steps+1)+t+1) %+1 for bar patch
            map(1:rows,1:cols)=r(rows:-1:1,:);	%flip y dimension
            
            if strcmp(interp_mode,'flat')
               h=pcolor([0:cols]-0.5,[0:rows]-0.5,map);
            else
               h=pcolor([0:cols],[0:rows],map);
            end 
            if group==0
               title([num2str(bins_per_step*(t-1)) '-' num2str(bins_per_step*t) ' ms'])
            end
            
         end
         axis image off
         axis([cx-sz/2 cx+sz/2 cy-sz/2 cy+sz/2])
         
         if t~=0
            caxis([0 1])
            shading(interp_mode)
         end
         rotate(h,[0 90],angles(k+1)-90,[cx cy 1])
         warning off
      end 
   end
end

title = ([char(rc.data.sessionname(cluster)) '  Group: ' char(rc.data.groupname(cluster)) '  Cluster: ' char(rc.data.cellname(cluster))]);
h=gcf;
h=findobj(h,'Tag','Title_box');
set(h,'String',title)

