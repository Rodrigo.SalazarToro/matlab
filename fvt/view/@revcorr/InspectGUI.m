function [obj,vargout] = inspectGUI(obj,varargin)

%what is the stimulus type?
switch(obj.stiminfo.data.iniInfo.type)
    case{'M-Sequence','m_sequence'}
        MSeqGui(obj,varargin{:});
    case{'SparseNoise','Sparse Noise','sparse_noise'}
        SNGui(obj,varargin{:});
    case('Movie')
    otherwise
        warning('Unknown Stimulus Type')
        
end








  