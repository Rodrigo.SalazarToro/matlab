% show_revcorr_MSeq.m -  static display of revcorr_MSeq as individual frames
%
% function show_revcorr_MSeq(rc,binsPerStep,cluster,TimeBinNumber,display_mode,interp_mode)
%

function show_revcorr_MSeq(rc,cluster,timeBinNumber,binsPerStep,display_mode)


if ~exist('display_mode','var')
    display_mode = 'R';
end

switch display_mode
    case 'R'
        R=rc.data.R;
        %         %   R=rc.R(:,:,:,cluster);
        %     case 'P'
        %          R=rc.P(:,:,:,cluster);	%P is result from bar_prob.m
        %      case 'R/P'
        %          
        %          %still need to fix 1/28/04
        %          [i,j] = find(rc.P(:,:,:,cluster)==0);
        %          index = sub2ind(size(rc.P(:,:,:,cluster)),i,j);
        %          % warning off
        %          R = rc.R(:,:,cluster)./rc.P(:,:,cluster);
        %          %warning backtrace
        %         %R(index) = 0;
end

numClusters = size(R,4);
 if strcmp(cluster,'all')
     cluster = 1:numClusters;
     clusterTitle=1;
 else
     clusterTitle=0;
 end

% get init_info variables
rows = rc.stiminfo.data.iniInfo.grid_x_size;
cols = rc.stiminfo.data.iniInfo.grid_y_size;
[row_degrees , col_degrees] = pixel2degree(rows+300, cols+400);

num_steps=floor(size(R,3)/binsPerStep);
 if strcmp(timeBinNumber,'all')
     timeBinNumber = 1:num_steps;
 end
for cl=cluster
    %normalize each cluster independently
    Rcl = R(:,:,:,cl);
    m1=min(Rcl(:));
    m2=max(Rcl(:));
    m3=mean(Rcl(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    %fprintf('before normilization, cluster %i has min=%0.2d, max=%0.2d, mean=%0.2d.\n',cl,m1,m2,m3)
    
    
    for sn = timeBinNumber
        %average over time bin
        Rind = Rcl(:,:,(sn-1)*binsPerStep+1:sn*binsPerStep);
        Rind=mean(Rind,3);
        
        %display
        if length(cluster)>1 & length(timeBinNumber)>1
            subplot(length(cluster),length(timeBinNumber),(cl-1)*length(timeBinNumber)+sn);
        elseif length(timeBinNumber)>1
            nptsubplot(length(timeBinNumber),sn);
        elseif length(cluster)>1
            nptsubplot(length(cluster),cl);
        end
        
        imagesc(Rind,contrast)
        
        axis equal
        axis tight
        
        y = get(gca,'YTick');
        x = get(gca,'XTick');
        [xdeg, ydeg] = MSeqGrid2degree(rc,x,y);
        set(gca,'XTickLabel',round(xdeg))
        set(gca,'YTickLabel',round(ydeg))
        
%         if cl==1 | size(cluster,2)==1
            tstr = ['TimeRange: ' num2str((sn-1)*binsPerStep) '-' num2str(sn*binsPerStep) ' msec'];
            if clusterTitle
                tstr = ['Cluster ' num2str(cl) '  ' tstr];
            end
            title(tstr)
%         end
    end
end

if isfield(rc.data,'setNames')
    titleStr = rc.data.setNames{cluster(1)};    
elseif isfield(rc.data,'sessionname')
    titleStr = ([char(rc.data.sessionname(1))          ]);%     '  Group: ' char(rc.data.groupname) ]);
else
    titleStr = 'a';
end
if clusterTitle
    titleStr = fileparts(titleStr);
end

h=gcf;
eh=findobj(h,'Tag','Title_box');
set(eh,'String',titleStr)
eh=findobj(h,'Tag','DegreeTextbox');
if ~isempty(eh)
    text= {['Stimulus Rows span ' num2str(row_degrees,2) ' degrees'],['Stimulus Columns span ' num2str(col_degrees,2) ' degrees']};
    set(eh,'String',text)
end

%hcb=findobj('Tag','Colorbar');
%colorbar%(hcb)
% linkedzoom(gcf,'off')
% linkedzoom(gcf,'onxy')