function data = GenerateCatRevcorrMSeqFrame(stimInfo,adjspikes,Args);
%This function will calculate the reverse correlation kernel from the
%m-sequence. and save the output into a obj.
%%% Get the mseq grid %%%%
if strcmp(num2str(Args.mseq_size(1)),'16')
    numRows = 16;
    numCols = 16;
    order = 16;
    load mseq16grid
elseif strcmp(num2str(Args.mseq_size(1)),'64')
    numRows = 64;
    numCols = 64;
    order = 16;
    load mseq64grid % Load the mseq grid file.
end
%%%%%%%%%% Get the Spikes %%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(stimInfo.data.iniInfo,'last_frames')
    mseq_frame_numbers = stimInfo.data.iniInfo.first_frames:stimInfo.data.iniInfo.last_frames;
else
    mseq_frame_numbers = stimInfo.data.iniInfo.first_frames:stimInfo.data.iniInfo.frames_displayed;
    mseq_frame_numbers = mseq_frame_numbers(1:stimInfo.data.iniInfo.frames_displayed);
end
spikes = adjspikes.data.adjSpiketrain;
frames_vector = adjspikes.data.adjFramePoints;
if frames_vector(1) < 1 %% This is due to the conversion from 1 datapoint to ms
    frames_vector(1) = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Args.Burst | Args.Tonic
    [frame_numbers,spike_counts] = BurstTonicActivityFinder(spikes,frames_vector,Args);
    if Args.SpikeCounts
        sci = find(spike_counts == Args.NumSpikes);
        spike_counts = spike_counts(sci);
        frame_numbers = frame_numbers(sci);
    end
else    
    spike_counts = histcie(spikes,frames_vector,'DropLast')';
    frame_numbers = find(spike_counts > 0);
    spike_counts = spike_counts(find(spike_counts > 0));
    if Args.SpikeCounts
        sci = find(spike_counts == Args.NumSpikes);
        spike_counts = spike_counts(sci);
        frame_numbers = frame_numbers(sci);
    end
end
%%%%% Create space in memory
R = zeros(numRows,numCols,Args.NumFramesBack);
%%%%% Create Average Stimulus
for m = 1:Args.NumFramesBack
    fprintf(['Frames Back ',num2str(m) '\n'])
    Rbin = zeros(numRows,numCols);
    count=0;
    for s = 1:length(frame_numbers)
        if Args.Complex
            if strcmp(Args.ONorOFF,'ON')
                if frame_numbers(s) > 0
                    if frame_numbers(s) == 1 % Due to spikes in the first frames of the mseq
                        frame1 = zeros(numRows,numCols);
                        frame2 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)))/255;
                    else % Get the frame prior to the frame with the spike and the frame with the spike
                        frame1 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)-1))/255; 
                        frame2 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)))/255;
                    end
                    frame = frame1-frame2; % Take the difference to find the pixels that changed from black to white.
                    frame(find(frame==0)) = .5; % Change the null changed pixels
                    frame(find(frame==1)) = .5; % Change the OFF responses to 0's
                    frame(find(frame==-1)) = 1; % Changes the ON responsses to 1's to make the map light enhanced
                    %frame(find(frame==.5)) = 0; % Change the null changed pixels to enhance the ON responses
                    for ff = 1:length(spike_counts(s));
                        Rbin = Rbin + frame;
                        count = count + 1;
                    end
                end
            elseif strcmp(Args.ONorOFF,'OFF')
                if frame_numbers(s) > 0
                    if frame_numbers(s) == 1 % Due to spikes in the first frames of the mseq
                        frame1 = zeros(numRows,numCols);
                        frame2 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)))/255;
                    else % Get the frame prior to the frame with the spike and the frame with the spike
                        frame1 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)-1))/255; 
                        frame2 = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)))/255;
                    end
                    frame = frame1-frame2; % Take the difference to find the pixels that changed from black to white.
                    frame(find(frame==0)) = .5; % Change the null changed pixels
                    frame(find(frame==1)) = 0; % Change the OFF responses to 0's to make the map dark enhanced
                    frame(find(frame==-1)) = .5; % Changes the ON responsses to 1's   
                    %frame(find(frame==.5)) = 1; % Change the null changed pixels to enhance the OFF responses
                    for ff = 1:length(spike_counts(s));
                        Rbin = Rbin + frame;
                        count = count + 1;
                    end
                end
            end
        else
            if frame_numbers(s) > 0
                frame = MSeqframe(mseqgrid,numRows,numCols,order,mseq_frame_numbers(frame_numbers(s)))/255; 
                for ff = 1:length(spike_counts(s));
                    Rbin = Rbin + frame;
                    count = count + 1;
                end
            end
        end            
    end
    frame_numbers = frame_numbers-1; % For the number of frames before the spikeframe
    R(:,:,m) = Rbin/count; % Average RF based on the spike counts or number of frames. the average should be .5
end   

data.R = R;
data.windowSize = Args.NumFramesBack;
data.binSize = 'frame';
data.numSpikesUsed = sum(spike_counts);
data.numFramesUsed = length(spike_counts);
data.Complex = Args.Complex;
data.ON_OFF = Args.ONorOFF;
data.Burst = Args.Burst;
data.Tonic = Args.Tonic;
data.SpikeCounts = Args.SpikeCounts;
data.NumSpikes = Args.NumSpikes;