function [R, Pspike] = SNRevCorr(spikes, eyepos,  stimInfo, seq, window_size);
% SNRevCorr - performs reverse correlation from an ispikes object for a sparse noise stimulus
%
% [R, Pspike] = SNRevCorr(spikes, clusterNum, stimInfo, seq, window_size, bin_size);
%
% spikes:  		ispikes structure containing all spike info for each trial
%					all times must be in points
% eyepos:       optional eye position data to correct for eye movements
%               (assumed to be already sampled according to bin_size)
%               If eye positions are not used then eyepos=[];
% clusterNum:	the cluster number of the data to be inspected
% framePoints	a matrix of numTrials x (numFrames + 2).  Trials with fewer 
%				frames than others are padded with zeroes.  The last nonzero
%				number in each row is the point at which the last frame
%				shown for that trial ended.  All rows end in a zero.
% totalFrames	the total number of stimulus frames shown in the session.
% iniInfo:      iniInfo structure returned by ReadRevCorrIni
% seq:          sparse noise parameters for each frame
% windowSize:   reverse correlation window size (ms)
% binSize:      bin size (ms)
%
% R:            M x N reverse correlation array. Each column contains a
%               probability distribution over bar configurations at a given
%               time relative to the occurence of a spike -- P(bar|spike).
%               M = # of sparse noise configurations, N = # of time bins.  
%               (See ijoc2index for conversion from ijoc to row index of R.)
%
% Pspike:       marginal probability of spike -- P(spike)
%
%Dependencies:  ijoc2index.m


bin_size = 1;
%Obtain necessary information from the stimInfo
iniInfo = stimInfo.data.iniInfo;
numTrials = iniInfo.num_trials;
numColors = iniInfo.num_colors;
numOris = iniInfo.num_orientations;
%rows = iniInfo.grid_rows;
%cols = iniInfo.grid_cols;
rows = iniInfo.rows;
cols = iniInfo.cols;
numBlocks = iniInfo.num_blocks;
%repeatBlocks = iniInfo.repeat_blocks;      %not neccessary
framePoints = stimInfo.data.framePoints;


M = rows * cols * numOris * numColors;		%Total number of original stimuli
N = floor(window_size/bin_size);			%Number of bins per rev corr window

R = zeros(M, N);
Rcount = zeros(1, N);
binSize = bin_size * 30;					        %Change to datapoints.
maxBins = floor(spikes.data.duration*30000/binSize);	    %Maximum number of bins in one trial(time)
%maxBins = floor(spikes.duration/binSize);	    %Maximum number of bins in one trial(datapoints)

%Transfer stimulus sequence matrix into vector of original stimuli
stimSeq = ijoc2index(seq, numColors, numOris, rows, cols) + 1;
if isfield(iniInfo,'oldcat')
    stimSeq = [ 0 ; stimSeq];
    addframe=1;
else 
    addframe=0;
end
loop = length(stimSeq);

Pspike = 0;


for i = 1:numTrials
    %fprintf('%i\n',i)
	spikeBin = ceil(spikes.data.trial(i).cluster.spikes/bin_size);  %bin spikes(time)
    %spikeBin = ceil(spikes.trial(i).cluster(clusterNum).spikes/binSize);   % bin spikes
 	numSpikes = spikes.data.trial(i).cluster.spikecount;
		
    	if numSpikes > 0
            trialSeq = zeros(1, maxBins);
			counter = 0;
	        datapoint = 0;
            stimStep = iniInfo.first_frames(i) + 1;    %Add 1 to make up for zero basing
	        step = 1;
	        while counter < maxBins				%Loops through the columns of framePoints
    	        counter = counter + 1;
		        %if the loop has advanced to the next frame, advance the stimulus sequence
    	        if datapoint > framePoints(i, step)
			        trialSeq(counter) = stimSeq(stimStep);
     	     	    step = step + 1;
			        stimStep = stimStep + 1;
		            %otherwise, show that the previous frame is still being shown
		        elseif (step > 1+addframe) & (stimStep > (1+addframe))
			        trialSeq(counter) = stimSeq(stimStep - 1);
                elseif (step > 1+addframe) & (stimStep == (1+addframe))
			        trialSeq(counter) = stimSeq(loop);
		        end
    	        datapoint = datapoint + binSize;	%Advance through the current trial
		        if stimStep > loop
                        stimStep = 1+addframe;
                end
                %Every row in framePoints end in zero, thus, when zero is reached...
                if framePoints(i, step) == 0
                    %remove the stimulus change accounted to the last frame
                    %shown, and...
                    trialSeq(counter) = 0;
			        break					%escape the while loop.
		        end
	        end  
         
            if ~isempty(eyepos)
            % correct for eye position
            trial_seq=eyeshift_sn(trial_seq,init_info,eyepos(:,:,i));    
         end
            
            
            
			for j=1:numSpikes    
            	si = spikeBin(j) + [-(N-1):0];  % spike triggered list of indices
            	si(find(si < 1)) = [];  % chop off indices < 0
            
            	ii = trialSeq(si);
                %Some of these indices may be between the control and
                %presenter triggers, so we're removing those.
                ii(find(ii < 1)) = [];
            	jj = N + [-(length(ii) - 1):0];
              	ind = (jj - 1) * M + ii;
            
            	R(ind) = R(ind) + 1;
            	Rcount(jj) = Rcount(jj) + 1;
			end
		end
     	Pspike = Pspike + numSpikes/maxBins;
end

% normalize R
for i = 1:N
   R(:, i) = R(:, i)/Rcount(i);
end

Pspike = Pspike/(numTrials);    