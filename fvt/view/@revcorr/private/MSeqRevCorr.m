function R = MSeqRevCorr(spikes, ecc, stimInfo, stim, window_size);
% SNRevCorr - performs reverse correlation from an ispikes object for a sparse noise stimulus
%
% [R, Pspike] = SNRevCorr(spikes, clusterNum, stimInfo, stim, window_size, bin_size);
%
% spikes:  		ispikes structure containing all spike info for each trial
%					all times must be in points
% ecc:       optional eye eccentricities to correct for eye movements
%               (assumed to be already sampled according to bin_size)
%               If eye positions are not used then ecc=[];
% clusterNum:	the cluster number of the data to be inspected
% framePoints	a matrix of numTrials x (numFrames + 2).  Trials with fewer 
%				frames than others are padded with zeroes.  The last nonzero
%				number in each row is the point at which the last frame
%				shown for that trial ended.  All rows end in a zero.
% totalFrames	the total number of stimulus frames shown in the session.
% iniInfo:      iniInfo structure returned by ReadRevCorrIni
% stim:          stimulus surface 
% windowSize:   reverse correlation window size (ms)
% binSize:      NO LONGER an input always set to 1 ms (time resolution of
%               eye file
%
% R:            M x N reverse correlation array. Each column contains a
%               probability distribution over bar configurations at a given
%               time relative to the occurence of a spike -- P(bar|spike).
%               M = # of sparse noise configurations, N = # of time bins.  
%               (See ijoc2index for conversion from ijoc to row index of R.)
%
% Pspike:       marginal probability of spike -- P(spike)
%
%Dependencies:  ijoc2index.m

padarray([],1); %acquire image processing toolbox so we don't crash later.

bin_size=1;     %1ms
%Obtain necessary information from the stimInfo
iniInfo = stimInfo.data.iniInfo;
numTrials = iniInfo.num_trials;
if (~isempty(strfind(stimInfo.data.iniInfo.stimulus_path,'16')) | strcmp(iniInfo.m_seq_size,'16x16 (order=16)'))
    numRows = 16;
    numCols = 16;
elseif (~isempty(strfind(stimInfo.data.iniInfo.stimulus_path,'64')) | strcmp(iniInfo.m_seq_size,'64x64 (order=16)'))
    numRows = 64;
    numCols = 64;
elseif stimInfo.data.iniInfo.m_seq_size(1) == 16
    numRows = 16;
    numCols = 16;
elseif stimInfo.data.iniInfo.m_seq_size(1) == 64
    numRows = 64;
    numCols = 64;
else
    s= stimInfo.data.iniInfo.m_seq_size;
    numRows=s(1); numCols=s(2); 
end
if isfield(iniInfo,'oldcat')
    lastframe = iniInfo.frames_displayed-1;
else
    lastframe = iniInfo.last_frames(end);
end

order = 16; %order is 16 for both 16x16 and 64x64
framePoints = stimInfo.data.framePoints;


%Create a stimulus matrix page for every latency bin.
%For each spike add to all pages and then divide by the total number of
%spikes at the end.
%
N = floor(window_size/bin_size);                        %Number of bins per rev corr window

binSize = bin_size * 30;					            %Change to datapoints.
maxBins = floor(spikes.data.duration*30000/binSize);	    %Maximum number of bins in one trial (time)
%maxBins = floor(spikes.data.duration/binSize);	        %Maximum number of%bins in one trial (DP)
spikenum=0;

%how many different stimulus frames can be shown with one surface?
%first frame is zero.
%Last frame is 65535 = 2^16-1
%So the stimulus repeats after 2^16th frames
stimSeq = (0:lastframe)+1;

if isfield(iniInfo,'oldcat')
    stimSeq = [ 0 stimSeq];
    addframe=1;
else 
    addframe=0;
end

loop = length(stimSeq);
Pspike = 0;
if ~isempty(ecc)
    R=zeros(iniInfo.grid_y_size, iniInfo.grid_x_size, N);
else
    R=zeros(numRows,numCols,N);
end

%Create average stimulus for each trial
for i = 1:numTrials
    fprintf('%i\n',i)
    spikeBin = ceil(spikes.data.trial(i).cluster.spikes/bin_size);   % bin spikes(time)
    %spikeBin = ceil(spikes.trial(i).cluster(clusterNum).spikes/binSize);   % bin spikes(DP)
    numSpikes = spikes.data.trial(i).cluster.spikecount;
    if numSpikes > 0
        
        %trialSeq - list of which Stimulus frame is shown for each bin
        trialSeq = StimulusBin(iniInfo,i,maxBins,framePoints,stimSeq,addframe,binSize,loop);
        
        
        %case only for older cat experiments
        if isfield(iniInfo,'oldcat')
            if numCols==16 
                ind = find(trialSeq>65790);
            elseif numCols==64 
                ind = find(trialSeq>65550);
            end
            trialSeq(ind)=0;
        end
        
        %framebins - list of stimulus frames shown for each spike.
        frameBins = zeros(numSpikes,N);
        eccYbin = zeros(numSpikes,N); 
        eccXbin = zeros(numSpikes,N);  
        for j=1:numSpikes 
            si = spikeBin(j) + [0:-1:-(N-1)];  %spike triggered list of indices
            si(find(si < 1)) = [];  %chop off indices < 0
            frameBins(j,1:length(si)) = trialSeq(si);      %frameBins is list of frame numbers for each bin
            %ii(find(ii < 1)) = 0; 
            if ~isempty(ecc)    %eye eccentricities
                eccYbin(j,1:length(si)) = ecc(i).trial(1,si); %eccentricity
                eccXbin(j,1:length(si)) = ecc(i).trial(2,si); %eccentricity
            end
        end
        
        %Some of these spikes may not have a stimulus so remove them
        frameBins(find(sum(frameBins,2)==0),:)=[];
        
        
        %Create Average Stimulus (slow part)
        for kk = 1:N
            fprintf('%i',kk)
            Rbin = R(:,:,kk);
            for jj=1:size(frameBins,1)
                if frameBins(jj,kk)>=1
                    frame = MSeqframe(stim,numRows,numCols,order,frameBins(jj,kk)-1)/255;   %trialSeq is 1 based & MSeqFrame is 0 based.
                    if ~isempty(ecc)
                        frame = imresize(frame,[iniInfo.grid_y_size iniInfo.grid_x_size],'nearest');  %resize to pixel resolution
                        %only need to pad by smallest and lagest values!!!
                        frame = padarray(frame,[ceil(abs(ecc(i).smallest(1)))  ceil(abs(ecc(i).smallest(2)))],0,'pre');               %pad with zeros
                        frame = padarray(frame,[ceil(abs(ecc(i).largest(1)))  ceil(abs(ecc(i).largest(2)))],0,'post');               %pad with zeros
                        %correct for eye position
                        Ystart = ceil(abs(ecc(i).smallest(1)))+1+round(eccYbin(jj,kk));
                        Ystop = Ystart+iniInfo.grid_y_size-1;
                        Xstart = ceil(abs(ecc(i).smallest(2)))+1+round(eccXbin(jj,kk));
                        Xstop = Xstart+iniInfo.grid_x_size-1;
                        frame = frame(Ystart:Ystop,Xstart:Xstop);
                    end
                    Rbin = Rbin + frame;
                end
            end
            R(:,:,kk) = Rbin;
        end
        
        spikenum = spikenum + jj;  
    end
end
R = R/spikenum;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function trialSeq = StimulusBin(iniInfo,trial,maxBins,framePoints,stimSeq,addframe,binSize,loop)
%trialSeq tells which stimulus occurred during any bin.
%
%iniInfo - stimulus info
%maxBins - number of bins needed for entire spiketrain
%framepoints - datapoints onwhich stimulus frame changed
%stimSeq - simply stimulus frame number list ie.(1:lastframe)

trialSeq = zeros(1, maxBins);
counter = 0;
datapoint = 1;%=0;
stimStep = iniInfo.first_frames(trial) + 1;    %Add 1 to make up for zero basing
step = 1;

while counter < maxBins				%Loops through the columns of framePoints
    counter = counter + 1;
    %if the loop has advanced to the next frame, advance the stimulus sequence
    if datapoint >= framePoints(trial, step)    %>
        trialSeq(counter) = stimSeq(stimStep);
        step = step + 1;
        stimStep = stimStep + 1;
        %otherwise, show that the previous frame is still being shown
    elseif (step > 1+addframe) & (stimStep > (1+addframe))
        trialSeq(counter) = stimSeq(stimStep - 1);
    elseif (step > 1+addframe) & (stimStep == (1+addframe))
        trialSeq(counter) = stimSeq(loop);
    end
    datapoint = datapoint + binSize;	%Advance through the current trial
    if stimStep > loop
        stimStep = 1+addframe;
    end
    %Every row in framePoints end in zero, thus, when zero is reached...
    if framePoints(trial, step) == 0
        %remove the stimulus change accounted to the last frame
        %shown, and...
        trialSeq(counter) = 0;
        break					%escape the while loop.
    end
end  