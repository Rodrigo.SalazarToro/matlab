function data = GenerateCatRevcorrMSeq(stimInfo,adjspikes,num_ms_back,mseq_size);
%This function will calculate the reverse correlation kernal from the
%m-sequence. and save the output into a obj.
%%% Get the mseq grid %%%%
if strcmp(num2str(mseq_size),'16')
    numRows = 16;
    numCols = 16;
    order = 16;
    load mseq16grid
elseif strcmp(num2str(mseq_size),'64')
    numRows = 64;
    numCols = 64;
    order = 16;
    load mseq64grid % Load the mseq grid file.
end
%%%%%%%%%% Get the Spikes %%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(stimInfo.data.iniInfo,'last_frames')
    f_numbers = stimInfo.data.iniInfo.first_frames:stimInfo.data.iniInfo.last_frames;
else
    f_numbers = stimInfo.data.iniInfo.first_frames:stimInfo.data.iniInfo.frames_displayed;
    f_numbers = f_numbers(1:stimInfo.data.iniInfo.frames_displayed);
end
frame_duration = ceil(stimInfo.data.catInfo.frame_duration);
spikes = adjspikes.data.adjSpiketrain;
frames_vector = adjspikes.data.adjFramePoints;
if frames_vector(1) < 1 %% This is due to the conversion from 1 datapoint to ms
    frames_vector(1) = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frame_numbers = zeros(length(spikes),num_ms_back);
for s = 1:length(spikes)    
    spike_time = round(spikes(s));    
    index = find(frames_vector <= spike_time);    
    if isempty(index)
        first_frame_diff = 0;
        spike_vector = [];
        index=0;
    elseif length(index) == length(frames_vector)-1
        first_frame_diff = 0;
        spike_vector = [];
        index=0;
    else
        first_frame_diff = round(spike_time-frames_vector(index(end)));
        spike_vector(1:first_frame_diff) = f_numbers(index(end)+1);
        if index(end)==1
            index(end)=0;
        else
            index(end)=[];
        end    
    end
    counter = first_frame_diff;    
    num_full_frames = floor((num_ms_back-first_frame_diff)/frame_duration);    
    last_frame_diff = round(mod((num_ms_back-first_frame_diff),frame_duration));        
    if (first_frame_diff+(num_full_frames*frame_duration)+last_frame_diff) ~= num_ms_back
        error('Error in the number of frames and ms back')
    end           
    for f = 1:num_full_frames
        spike_vector(counter+1:counter+frame_duration) = f_numbers(index(end)+1);
        counter = counter+frame_duration;        
        if index(end) <= 1; % Since the first spike could come before num_ms_back into the stimulus
            index(end) = 0;
        else
            index(end) = [];
        end        
    end    
    if last_frame_diff ~= 0;
        spike_vector(num_ms_back-last_frame_diff:num_ms_back) = f_numbers(index(end)+1);
    end    
    frame_numbers(s,:) = spike_vector;    
    spike_vector=[];  
end

%%%%% Create space in memory
R = zeros(numRows,numCols,num_ms_back);
%%%%% Create Average Stimulus
for m = 1:num_ms_back
    fprintf(['Millisecond Bin ',num2str(m) '\n'])
    Rbin = zeros(numRows,numCols);
    num_frames = 0;
    for s = 1:size(frame_numbers,1)
        if frame_numbers(s,m) > 0
            frame = MSeqframe(mseqgrid,numRows,numCols,order,f_numbers(frame_numbers(s,m)))/255; % Values range from 0 - 255.
            Rbin = Rbin+frame;
            num_frames = num_frames+1;
        end
    end
    R(:,:,m) = Rbin/num_frames;
end   

data.R = R;
data.windowSize = num_ms_back;
data.binSize = 1;