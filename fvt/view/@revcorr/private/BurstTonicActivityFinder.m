function [frame_numbers,spike_counts] = BurstTonicActivityFinder(spiketrain,frames_vector,Args);
% This private revcorr function will calculate the bursting activity within the
% spiketrain based on the intra and inter burst intervals.

isis = diff(spiketrain);
BurstSpikes = (find(isis>Args.IBI))+1; % Find the intervals greater than the intra burst interval
if ~isempty(BurstSpikes)
    if BurstSpikes(1)~=1
        BurstSpikes = [1 BurstSpikes];
    end
    if BurstSpikes(end) == length(spiketrain)
        BurstSpikes(end) = [];
    end
    new_spikes = [];
    for ii = 1:length(BurstSpikes)
        n_isi = diff(spiketrain(BurstSpikes(ii):BurstSpikes(ii)+1));
        if n_isi > Args.IBI
            BurstSpikes(ii) = NaN;
        end
        count = 1;
        while n_isi < Args.IBI
            new_spikes = [new_spikes BurstSpikes(ii)+count];
            count = count + 1;
            if (BurstSpikes(ii)+count) > length(spiketrain)
                n_isi = Args.IBI+1;
            else
                n_isi = diff(spiketrain(BurstSpikes(ii)+(count-1):BurstSpikes(ii)+count)); 
            end
        end
    end    
    BurstSpikes = sort([BurstSpikes new_spikes]); % Add the new spikes
    BurstSpikes = BurstSpikes(find(BurstSpikes>0)); % Take out the NaNs
    BurstSpikes = unique(BurstSpikes); % Possibly doubled the counts.
    if Args.Burst
        Spikes = spiketrain(BurstSpikes);
    elseif Args.Tonic
        TonicSpikes = 1:length(spiketrain);
        TonicSpikes(BurstSpikes)=[];
        Spikes = spiketrain(TonicSpikes);
    end        
end
    
spike_counts = histcie(Spikes,frames_vector,'DropLast')';
frame_numbers = find(spike_counts > 0);
spike_counts = spike_counts(find(spike_counts > 0));