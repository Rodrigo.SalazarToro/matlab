function [xout, yout] = MSeqGrid2degree(R,x,y,flag)
%RF = MSeq2degree(R,RF)
%
%R - revcorr object with a stiminfo field
%RF receptive field kernel  property structure
%
%converts and adds the new info from fields with units of pixels to
%degrees.
if nargin<4
    flag='';
end
if nargin==2
    average=1;
else
    average=0;
end


ScreenWidth = R.stiminfo.data.iniInfo.ScreenWidth;   %units of pixels
ScreenHeight = R.stiminfo.data.iniInfo.ScreenHeight; %units of pixels
%xCenter = R.stiminfo.data.iniInfo.grid_center(1);     %units of pixels
%yCenter = R.stiminfo.data.iniInfo.grid_center(2);     %units of pixels
xCenter = R.stiminfo.data.iniInfo.grid_x_center;     %units of pixels
yCenter = R.stiminfo.data.iniInfo.grid_y_center;     %units of pixels
pixelXSize = R.stiminfo.data.iniInfo.grid_x_size;    %units of pixels
pixelYSize = R.stiminfo.data.iniInfo.grid_y_size;    %units of pixels

if ~isempty(strfind(R.stiminfo.data.iniInfo.stimulus_path,'16')) 
    gridXSize = 16;           %units of Grid
    gridYSize = 16;           %units of Grid
elseif ~isempty(strfind(R.stiminfo.data.iniInfo.stimulus_path,'64'))
    gridXSize = 64;           %units of Grid
    gridYSize = 64;           %units of Grid
elseif    strcmp(R.stiminfo.data.iniInfo.m_seq_size,'16x16 (order=16)')
     gridXSize = 16;           %units of Grid
    gridYSize = 16;           %units of Grid
elseif strcmp(R.stiminfo.data.iniInfo.m_seq_size,'64x64 (order=16)')
     gridXSize = 64;           %units of Grid
    gridYSize = 64;           %units of Grid
else
    s= R.stiminfo.data.iniInfo.m_seq_size;
    gridXSize=s(1); gridYSize=s(2); 
end;

xPixelsPerGrid = pixelXSize/gridXSize;
yPixelsPerGrid = pixelYSize/gridYSize;

yDegreesPerPixel  = (atan((2.54*11.9375/2)/57)*180/pi)/ (ScreenHeight/2);
xDegreesPerPixel  = (atan((2.54*16/2)/57)*180/pi)/ (ScreenWidth/2);


if strcmp(flag,'grid')
    xout = x * (1/xPixelsPerGrid) * (1/xDegreesPerPixel);
    yout = y * (1/yPixelsPerGrid) * (1/yDegreesPerPixel);
elseif average
    xout = x * mean(xPixelsPerGrid,yPixelsPerGrid) * mean(xDegreesPerPixel,yPixelsPerGrid);
    yout = [];
else
    xout = x * xPixelsPerGrid * xDegreesPerPixel;
    yout = y * yPixelsPerGrid * yDegreesPerPixel;
end
