function showXT(obj,RF,clusters)


R=obj.data.R;


% xh = figure;
% set(xh,'Name','X')
xyh = figure;
set(xyh,'Name','X-Y')
xth = figure;
set(xth,'Name','X-T')
ah= figure;
set(ah,'Name','XY Area')



%loop over clusters
hs=[];
for ii=1:length(clusters)
    Rc = R(:,:,:,clusters(ii));
    RF.cluster(ii)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%   XY    
    figure(xyh)
    nptsubplot(length(clusters),ii);
    
    %calculate contrast for entire kernel
    m1=min(Rc(:));
    m2=max(Rc(:));
    m3=mean(Rc(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    
    %show x-y at time w/ max contrast
    imagesc(Rc(:,:,RF.cluster(ii).center(3)),contrast)
    hold on
    
    %plot orthogonal orientation line
    plot(RF.cluster(ii).xi,RF.cluster(ii).yi,'k')
    
    %Show center
    plot(RF.cluster(ii).center(1),RF.cluster(ii).center(2),'*r')
    
    axis equal
    axis tight    
    title(['Cluster ' num2str(clusters(ii)) ' - frame ' num2str(RF.cluster(ii).center(3))])

    y = get(gca,'YTick');
    x = get(gca,'XTick');
    [xdeg, ydeg] = MSeqGrid2degree(obj,x,y);
    x=sprintf('%6.1f|',xdeg);
    set(gca,'XTickLabel',x)
    y=sprintf('%6.1f|',ydeg);
    set(gca,'YTickLabel',y)
    xlabel('X (degrees)')
    ylabel('Y (degrees)')
    set(gca,'xlimmode','manual','ylimmode','manual','zlimmode','manual','xtickmode','manual','ytickmode','manual','ztickmode','manual')

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   AREA
    figure(ah)
    nptsubplot(length(clusters),ii);
    imagesc(RF.cluster(ii).areaM);
    hold on
    imcontour(RF.cluster(ii).areaM,[RF.cluster(ii).areaThreshold RF.cluster(ii).areaThreshold],'k');
    %Show center
    plot(RF.cluster(ii).center(1)*RF.cluster(ii).Algo.oversample,RF.cluster(ii).center(2)*RF.cluster(ii).Algo.oversample,'*r')
    title(['Cluster #' num2str(clusters(ii)) ' - Area =' num2str(RF.cluster(ii).areaDEG) 'degrees^2'])
    
    y = get(gca,'YTick');
    x = get(gca,'XTick');
    [xdeg, ydeg] = MSeqGrid2degree(obj,x/RF.cluster(ii).Algo.oversample,y/RF.cluster(ii).Algo.oversample);
    x=sprintf('%6.1f|',xdeg);
    set(gca,'XTickLabel',x)
    y=sprintf('%6.1f|',ydeg);
    set(gca,'YTickLabel',y)
    xlabel('X (degrees)')
    ylabel('Y (degrees)')
    axis equal
    axis tight    
    set(gca,'xlimmode','manual','ylimmode','manual','zlimmode','manual','xtickmode','manual','ytickmode','manual','ztickmode','manual')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   XT
    figure(xth)
    nptsubplot(length(clusters),ii);
    
    
    %calculate contrast for entire kernel
    xt = RF.cluster(ii).xt;
    m1=min(xt(:));
    m2=max(xt(:));
    m3=nanmean(xt(:));
    contrast=[(m3-max([(m3-m1) (m2-m3)])) (m3+max([(m3-m1) (m2-m3)]))];
    
    %show xt plot
    xlim = sqrt((RF.cluster(ii).xi(1)-RF.cluster(ii).xi(end))^2 + (RF.cluster(ii).yi(1)-RF.cluster(ii).yi(end))^2);
    x=linspace(0,xlim,length(RF.cluster(ii).xi));
    y=1:size(xt,1);
    imagesc(x,y,xt,contrast)
    hs = [hs gca];
    
    
    latency = RF.cluster(ii).latency;
    hold on
    line([0 xlim] , [latency(1) latency(1)]);
    line([0 xlim] , [latency(2) latency(2)]);
    xts = RF.cluster(ii).XTsum;
    scale=1;
    plot(scale*xts,1:length(xts))
    line([scale*RF.cluster(ii).XTthreshold scale*RF.cluster(ii).XTthreshold],[1 length(xts)])
    axis square
    
    x = get(gca,'XTick');
    xdeg = MSeqGrid2degree(obj,x);
    x=sprintf('%6.1f|',xdeg);
    set(gca,'XTickLabel',x)
    xlabel('Space (degrees)')
    set(gca,'xlimmode','manual','ylimmode','manual','zlimmode','manual','xtickmode','manual','ytickmode','manual','ztickmode','manual')

    title(['Cluster ' num2str(clusters(ii)) ' : XT'])
    ylabel('Time (msec)')
    colorbar;
    

end
% linkedzoom(hs,'onxy')
% linkedzoom(ah,'onxy')
% linkedzoom(xyh,'onxy')