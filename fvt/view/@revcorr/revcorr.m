function obj = revcorr(varargin)
% constructor for revcorr class
%   OBJ = revcorr(varargin) 
%   Calculates the average stimulus at 1msec resolution 
%   during at latency window using reverse correlation.
%   example rc = revcorr('save','redo')
% 
%dependencies:  ispikes, stimInfo, and adjspikes for the frame level calc.

Args = struct('RedoLevels',0,'SaveLevels',0, ...
    'Auto',0,'WindowSize',300,'FrameLevel',0,'NumFramesBack',7,...
    'Complex',0,'ONorOFF','ON','Burst',0,'Tonic',0,'IBI',5,...
    'SpikeCounts',0,'NumSpikes',1,'stimInfoDir','../../');

[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'flags',{'Auto','FrameLevel','Complex','Burst','Tonic'},...
    'shortcuts',{'redo',{'RedoLevels',1};'save',{'SaveLevels',1}}, ...
    'subtract',{'RedoLevels','SaveLevels'});

Args.classname = 'revcorr';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'rev';

if nargin==0
    % create empty object
    obj = createEmptyObject(Args);
elseif( (nargin==1) & (isa(varargin{1},'revcorr'))  )
    obj = varargin{1};
elseif Args.RedoLevels==0
    % check for saved object
    dirlist = nptDir('revcorr.mat');
    if ~isempty(dirlist)
        fprintf('Loading saved revcorr object...\n');
        % load saved object and exit
        try
            l = load(dirlist(1).name);
            obj = l.obj;
        catch
            fprintf('older format... Recalculating...')
            obj = createObject(Args,modvarargin{:});
        end
    else
        % no saved object so try to create one
        obj = createObject(Args,modvarargin{:});
    end
elseif Args.RedoLevels
    obj = createObject(Args,modvarargin{:});
end

function obj = createEmptyObject(Args)
data.numSets = 0;
data.setNames = '';
data.dlist = [];
data.setIndex = [];
d.data = data;
n=nptdata(0,0);
obj = class(d,Args.classname,n,stiminfo);

function obj = createObject(Args,varargin)
%get ispikes
sp = ispikes('auto');
if isempty(sp)
    obj=createEmptyObject;
    return
end
pdir = pwd;

%get stiminfo
stimInfo = stiminfo('auto',varargin{:});
switch(stimInfo.data.iniInfo.type)
    case{'M-Sequence','m_sequence'}
        if isfield(stimInfo.data,'catInfo') %%%% This is a current addition 
            if Args.FrameLevel %% This is the frame level calculation using Jonathan's function
                sp = adjspikes('auto',varargin{:}); % Load the adjspikes for the frame level calculation
                Args.mseq_size = stimInfo.data.iniInfo.m_seq_size(1); % Set so you can load the mseq_grid
                dat = GenerateCatRevcorrMSeqFrame(stimInfo,sp,Args);
            else
                dat = GenerateRevcorrMSeq(stimInfo,sp,Args.WindowSize,Args.stimInfoDir);
            end
        end
    case{'SparseNoise','Sparse Noise','sparse_noise'}
        dat = GenerateRevcorrSN(stimInfo,sp,Args.WindowSize,Args.stimInfoDir);
    case('Movie')
        obj = createEmptyObject;
        return
    otherwise
        warning('Unknown Stimulus Type')
        obj = createEmptyObject;
        return
end

data = dat;
data.numSets = 1;
data.setNames{1} = pdir;
data.setIndex = 1;
data.dlist = nptDir;
d.data=data;
number = 1;
holdaxis = get(sp,'HoldAxis');
nd = nptdata(number,holdaxis,pdir);
obj = class(d,Args.classname,nd,stimInfo);
if(Args.SaveLevels>0)
    fprintf('Saving revcorr object...\n');
    filename = [Args.classname '.mat'];
    save(filename,'obj')
end
