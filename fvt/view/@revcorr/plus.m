function r = plus(p,q,varargin)
% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
    % check if second input is the right kind of object
    if(~isa(q,classname))
        % both inputs are not the right kind of object so create empty
        % object and return it
        r = feval(classname);
    else
        % second input is the right kind of object so return that
        r = q;
    end
else
    if(~isa(q,classname))
        % p is the right kind of object but q is not so just return p
        r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
    else
        % both p and q are the right kind of objects so add them 
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        % useful fields for most objects
        r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};        
        % object specific fields
        r.data.dlist = [p.data.dlist; q.data.dlist];
        r.data.setIndex = [p.data.setIndex; (p.data.setIndex(end) ...
                + q.data.setIndex(2:end))];        
        % add nptdata objects as well
        switch(p.stiminfo.data.iniInfo.type)
            case{'M-Sequence','m_sequence'}
                r.data.R = cat(4,p.data.R,q.data.R);
            case{'SparseNoise','Sparse Noise','sparse_noise'}
                r.data.R = cat(3,p.data.R,q.data.R);
            case('Movie')
                r.data.R = [];
            otherwise
                warning('Unknown Stimulus Type')
                r.data.R = [];
        end        
        r.data.windowSize = [p.data.windowSize; q.data.windowSize];
        r.data.binSize = [r.data.binSize; q.data.binSize];
        r.nptdata = plus(p.nptdata,q.nptdata);        
    end
end
