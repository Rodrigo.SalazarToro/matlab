function RF = MSeq2degree(R,RF)
%RF = MSeq2degree(R,RF)
%
%R - revcorr object with a stiminfo field
%RF receptive field kernel  property structure
%
%converts and adds the new info from fields with units of pixels to
%degrees.


ScreenWidth = R.stiminfo.data.iniInfo.ScreenWidth;   %units of pixels
ScreenHeight = R.stiminfo.data.iniInfo.ScreenHeight; %units of pixels
%xCenter = R.stiminfo.data.iniInfo.grid_center(1);     %units of pixels
%yCenter = R.stiminfo.data.iniInfo.grid_center(2);     %units of pixels
xCenter = R.stiminfo.data.iniInfo.grid_x_center;     %units of pixels
yCenter = R.stiminfo.data.iniInfo.grid_y_center;     %units of pixels
pixelXSize = R.stiminfo.data.iniInfo.grid_x_size;    %units of pixels
pixelYSize = R.stiminfo.data.iniInfo.grid_y_size;    %units of pixels

if ~isempty(strfind(R.stiminfo.data.iniInfo.stimulus_path,'16')) 
    gridXSize = 16;           %units of Grid
    gridYSize = 16;           %units of Grid
elseif ~isempty(strfind(R.stiminfo.data.iniInfo.stimulus_path,'64'))
    gridXSize = 64;           %units of Grid
    gridYSize = 64;           %units of Grid
elseif    strcmp(R.stiminfo.data.iniInfo.m_seq_size,'16x16 (order=16)')
     gridXSize = 16;           %units of Grid
    gridYSize = 16;           %units of Grid
elseif strcmp(R.stiminfo.data.iniInfo.m_seq_size,'64x64 (order=16)')
     gridXSize = 64;           %units of Grid
    gridYSize = 64;           %units of Grid
else
    s= R.stiminfo.data.iniInfo.m_seq_size;
    gridXSize=s(1); gridYSize=s(2); 
end;

xPixelsPerGrid = pixelXSize/gridXSize;
yPixelsPerGrid = pixelYSize/gridYSize;

yDegreesPerPixel  = (atan((2.54*11.9375/2)/57)*180/pi)/ (ScreenHeight/2);
xDegreesPerPixel  = (atan((2.54*16/2)/57)*180/pi)/ (ScreenWidth/2);

for ii=1:length(RF.cluster)
    
    %area
    RF.cluster(ii).areaDEG = RF.cluster(ii).area * xPixelsPerGrid * yPixelsPerGrid * yDegreesPerPixel * xDegreesPerPixel;
    
    %center 
    %horiz_pixel = xCenter - gridXSize/2 + RF.cluster(ii).center(1)*xPixelsPerGrid;
    %vert_pixel = yCenter - gridYSize/2 + RF.cluster(ii).center(2)*yPixelsPerGrid;
    %[RF.cluster(ii).centerDEG(2) , RF.cluster(ii).centerDEG(1)] = pixel2degree(vert_pixel, horiz_pixel);
    RF.cluster(ii).centerDEG(1) = RF.cluster(ii).center(1) * xPixelsPerGrid * xDegreesPerPixel;
    RF.cluster(ii).centerDEG(2) = RF.cluster(ii).center(2) * yPixelsPerGrid * yDegreesPerPixel;
    
    %xfreq
    RF.cluster(ii).xfreqDEG = RF.cluster(ii).xfreq  * (1/xPixelsPerGrid) * (1/xDegreesPerPixel);
    
    %yfreq
    RF.cluster(ii).yfreqDEG = RF.cluster(ii).yfreq * (1/yPixelsPerGrid) * (1/yDegreesPerPixel);
    
    %spatial freq
    RF.cluster(ii).sfreqDEG = sqrt(RF.cluster(ii).xfreqDEG^2 + RF.cluster(ii).yfreqDEG^2);
    
    %velocity
    RF.cluster(ii).velDEG = RF.cluster(ii).tfreq/RF.cluster(ii).sfreqDEG;

end

