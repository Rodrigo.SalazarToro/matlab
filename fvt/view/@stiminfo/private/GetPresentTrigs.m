function presentTrigs = GetPresentTrigs(sessionname, channel)


dirlist = nptDir([sessionname '.0*']);
numTrials = length(dirlist);
presentTrigs = zeros(numTrials, 2);
threshold = 1500;

for index = 1:numTrials			%For each trial...
    trialNum = num2strpad(index,4);
    [data, nc, sr, so, points] = nptReadStreamerFile(dirlist(index).name);
    data = data(channel,:);
    %threshold = max(data)/2;  Doesn't work, must use constant.
    crossings = nptThresholdCrossings(data, threshold, 'rising');
    if length(crossings) > 2
        error('More than 2 presenter trigs on trial %i\n', index)
    elseif length(crossings) == 1
        fprintf('Warning:  Only one presenter trig on trial %i\n',index)
        if crossings < (points/2)
            presentTrigs(index, :) = [crossings, points];
        else
            presentTrigs(index, :) = [0, crossings];
        end
    elseif isempty(crossings)
        presentTrigs(index, :) = [0, points];
    else
	    presentTrigs(index, :) = crossings;
    end
end