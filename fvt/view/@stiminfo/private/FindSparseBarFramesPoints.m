function [framePoints,catInfo] = FindSparseBarFramesPoints(sessionname, iniInfo, catInfo)

%  [framePoints, totalFrames] = FindGratingFramePoints(sessionname, iniInfo);
%  FindFramePoints -- function to find the millisecond value of stimulus frame changes
%  very similar to "findFramePoints" for single trial data
%
%  Input:   session -- session name prefix, ex:  'test043003'
%			iniInfo -- output of ReadRevCorrIni.m  (list of session parameters)
%           
%  Output:  framePoints -- a matrix of numTrials x (numFrames + 2).  Trials with fewer frames
%				than others are padded with zeroes.  The last nonzero number in each row is
%				the point at which the last frame shown for that trial ended.  All rows end
%				in a zero as this is used by SNRevCorr.m to advance to the next trial.
%
% Dependencies: ReadRevCorrIni.m,ReadExtraSyncsFile.m, nptReadSyncsFile.m

%%%%% Get grating values to create the frame points vector %%%%%
total_number_frames = iniInfo.total_grating_frames; %%%%%%% Total number of Frames Displayed %%%%%
num_orientations = iniInfo.num_orientations; %%%%%%% Number of Orientation %%%%%%
dir_per_orientation = iniInfo.dir_per_orientation; %%%%%% Directions Per Orientation %%%%%
num_repetitions = iniInfo.num_blocks; %%%%%% Number of Repeats %%%%%%%
num_columns =  iniInfo.grid_cols; %%% Number Columns %%%%
num_rows =  iniInfo.grid_rows; %%% Number Rows %%%%
num_colors = iniInfo.num_colors; %%% Number of Colors %%%%%%

if total_number_frames ~= num_orientations*dir_per_orientation*num_repetitions*num_columns*num_rows*num_colors
    error('Error in the Sparse Bars Numbers')
end

%%%%%% Place some info into the catInfo structure %%%%%%
catInfo.num_repetitions = num_repetitions;
catInfo.num_frames = 1;
catInfo.num_orientations = num_orientations;
catInfo.dir_per_orientation = dir_per_orientation;
catInfo.num_columns = num_columns;
catInfo.num_rows = num_rows;
catInfo.num_colors = num_colors;

%Read the extrasyncs file
[es] = ReadExtraSyncsFile([sessionname '_extra_syncs.txt']);

if ~isempty(es.trials)
    extra_syncs = [es.frames' es.numExtraS'];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Will have to change this once the first frame presented is blank.
    if extra_syncs(1) == 0
        extra_syncs(1,:)=[];
        fprintf('extra_syncs on first frame')
    end
    if extra_syncs(end) == total_number_frames+1
        extra_syncs(end,:)=[];
    end
    iniInfo.total_extra_syncs=sum(extra_syncs(:,2));
    extra_syncs(1) = 100000000000000000;    
else
    extra_syncs(1) = 100000000000000000; % Since some files have no extra-syncs I do this so the loop does not crash.
    iniInfo.total_extra_syncs = 0;
end

%%%%%%% Read the syncs file %%%%%%
[syncs,dataFilename,records,meanF,stdF] = nptReadSyncsFile([sessionname '.snc']);

%%% Check for an error in the creation
if length(syncs) ~= (total_number_frames*iniInfo.refreshes_per_frame)+2 % add 2 due to start and end trigger syncs
    fprintf('Missmatch in sync file and frames vector')
    length_of_syncs_file = length(syncs)
    expected_number_of_syncs = (total_number_frames*iniInfo.refreshes_per_frame)+2 
    if length(syncs) < (total_number_frames*iniInfo.refreshes_per_frame)+2 
        num_syncs_diff = (total_number_frames*iniInfo.refreshes_per_frame)+2 - length(syncs)
        for s = 1:num_syncs_diff
            syncs = [syncs;syncs(end)+mean(diff(syncs))];
        end
    end
end

%since the last sync is the end trigger sync and the start is the start trigger.
syncs(end) = []; % Take out the last sync corresponding to the end trigger
syncs(1) = []; % Take out the first sync corresponding to the start trigger

%%%%%%%% Now add to the total number of frames the isi times %%%%%%%%
% FROM THE SYNC-DATAPOINTS AND THE EXTRA-SYNC VECTOR get the frame vector
frame_numbers = 1:catInfo.num_frames;
for r = 1:total_number_frames
    for f = frame_numbers(1):frame_numbers(end)
        if extra_syncs(1,1)~=f
            frames_vector(f) = iniInfo.refreshes_per_frame;
        else
            frames_vector(f) = iniInfo.refreshes_per_frame+extra_syncs(1,2);
            extra_syncs(1,:)=[];
            if isempty(extra_syncs)
                extra_syncs(1) = 1000000000000000;
            end
        end        
    end
    %%%%%% Increment the frame numbers along with the isi frame %%%%
    frame_numbers = frame_numbers(end)+1:frame_numbers(end)+catInfo.num_frames;
end

frames_vector = cumsum(frames_vector);
% Frames_vector was an index that allows us to grab the syncs corresponding
% the appropriate frame ending point. and convert to milliseconds
frames_vector = (syncs(frames_vector)');%/(catInfo.samplingrate/1000);

%%%%% Presenter was changed to have no blank frame at the beginning of the
%%%%% stimulus
if iniInfo.Date(3) >= 3
    framePoints = [0 frames_vector];
else
    framePoints = [frames_vector frames_vector(end)+catInfo.frame_duration];
end