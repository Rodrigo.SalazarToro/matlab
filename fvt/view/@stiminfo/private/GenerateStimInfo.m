function d = GenerateStimInfo(sessionname)
%d = GenerateStimInfo(sessionname)
%
% fname is the sessionname
% pwd is the session directory
%
%   1.  reads the ini file
%   2.  reads the extrasyncs file
%   3.  reads the scanlines file
%   4.  creates a FramePoints vector which tells atwhich datapoints a new
%   stimulus frame was shown.  There are three different experimental types
%   and each type's FramePoints are created in a different way.
%
%       4a.  New Presenter (post 6/2003)
%               reads the descriptor file to get the presentertrigger channel
%               reads the raw data files to get the presenter triggers for each trial
%               reads the snc monitor file
%               Calculates the FramePoints (datapoints of frame changes)
%
%       4b.  Old Monkey Multiple Trial Files
%
%       4c.  Old Cat Single Trial Files
%               same as 4a. New Presenter except there is no presenter trigger

%initialize
newRevCorr = 0;
oldMonkey = 0;
oldCat = 0;
newCat = 0;

%1
filename = nptDir(['*.ini'],'CaseInsensitive');
iniInfo = ReadIniFileWrapper(filename.name);

if (iniInfo.Date(1) >= 5 & iniInfo.Date(3) == 3) | iniInfo.Date(3) > 3
    newRevCorr = 1;
    if strcmp(iniInfo.type,'m_sequence')
        iniInfo.m_seq_size = [str2num(iniInfo.m_seq_size(1:2)) str2num(iniInfo.m_seq_size(1:2))];
    elseif strcmp(iniInfo.type,'sparse_noise')
        newCat = 1;
        iniInfo.still_images = 0;
    end
    if isfield(iniInfo,'refreshes_per_frame')
        if iniInfo.refreshes_per_frame == 6 %%%%%% This determines if the session was for the new cat data
            newCat = 1;
            iniInfo.still_images = 0;
        elseif isfield(iniInfo,'DAQ')
            if strcmp(iniInfo.DAQ,'UEI')
                newCat = 1;
                iniInfo.still_images = 0;
            end
        end
    end
elseif isfield(iniInfo,'oldMonkey')     %Free viewing monkey data before 2003
    oldMonkey = 1;
else
    oldCat = 1;
    iniInfo.num_trials = 1;
    iniInfo.first_frames(1) = 0;
    if strcmp(iniInfo.type,'m_sequence')
        iniInfo.m_seq_size = [str2num(iniInfo.stimulus_path(34:35)) str2num(iniInfo.stimulus_path(34:35))];
    end
    iniInfo.oldcat = 1;
    iniInfo.still_images = 0;
end

%2
if (~isempty(nptDir('*_extra_syncs.txt'))) & (~strcmp(iniInfo.type,'Calibration'))
    extrasyncs = ReadExtraSyncsFile([sessionname '_extra_syncs.txt']);
end

%3
if ~isempty(nptDir('*_scanlines.txt'))
    %    scanlines = ReadScanlinesFile([sessionname '_scanlines.txt']);
end

%4
if exist('extrasyncs','var')
    if newRevCorr
        descriptor_info = ReadDescriptor([sessionname '_descriptor.txt']);
        for ii = 1:descriptor_info.number_of_channels
            if ((strcmp(descriptor_info.description{ii},'pres_trig')))% & (strcmp(descriptor_info.state{ii},'Active')))
                prestrig_channel = ii;
                break;
            end
        end
        if ~exist('prestrig_channel','var')
            % Since the cat data is not trial based, a single presenter
            % trigger is created
            presentTrigs = [0,inf];
            [syncs, filename, records, meanF, stdF] = nptReadSyncsFile([sessionname '.snc']);
            syncs=syncs';
            trialFrames = iniInfo.frames_displayed;
        else
            presentTrigs = GetPresentTrigs(sessionname, prestrig_channel);
            trialFrames = iniInfo.trial_frames;
            %Reading sync monitor files to obtain the point values of the syncs
            syncs = [];
            for ii=1:iniInfo.num_trials
                trialNum = num2strpad(ii,4);
                [syncsii, filename, records, meanF, stdF] = nptReadSyncsFile([sessionname '.snc' trialNum]);
                syncs = concatenate(syncs, syncsii', 'DiscardEmptyA');
            end
        end
    end

    if oldCat
        presentTrigs = [0,inf];
        trialFrames = iniInfo.frames_displayed;
        [syncs, filename, records, meanF, stdF] = nptReadSyncsFile([sessionname '.snc']);
        syncs=syncs';
    end

    if oldMonkey
        descriptor_info = ReadDescriptor([sessionname '_descriptor.txt']);
        for ii = 1:descriptor_info.number_of_channels
            if ((strcmp(descriptor_info.description{ii},'pres_trig')))% & (strcmp(descriptor_info.state{ii},'Active')))
                prestrig_channel = ii;
                break;
            end
        end
        %If the presenter trigger was acquired, use the usual script;
        %otherwise, if the session was free-viewing, it was triggered by
        %presenter, so [0, inf] will be used for each trial.
        %If there's no presenter trigger, and the session was
        %control-triggered, there's no point in guessing.
        if exist('prestrig_channel','var')
            presentTrigs = GetPresentTrigs(sessionname, prestrig_channel);
        elseif iniInfo.win_movie
            presenterTrigs = zeros(iniInfo.num_trials, 2);
            presenterTrigs(:, 2) = inf;
        else
            presenterTrigs = [];
        end
        if isfield(iniInfo, 'trial_frames')
            trialFrames = iniInfo.trial_frames;
        else
            trialFrames = FindTrialFrames(iniInfo, extrasyncs, sessionname);
        end
        %Reading sync monitor files (if they exist) to obtain the point values of the syncs
        syncs = [];
        if exist([sessionname '.snc0001'], 'file')
            for ii=1:iniInfo.num_trials
                trialNum = num2strpad(ii,4);
                [syncsii, filename, records, meanF, stdF] = nptReadSyncsFile([sessionname '.snc' trialNum]);
                syncs = concatenate(syncs, syncsii', 'DiscardEmptyA');
            end
        end
        %If the session was still images, we can produce a synthetic
        %framePoints of sort by noting the beginning and end of the
        %presenter-triggered trials.  (Extra syncs on the triggers were not
        %caught at this point [pre-2003] so that is not a concern.)
        if iniInfo.still_images & isempty(syncs)
            framePoints = zeros(iniInfo.num_trials, 3);
            framePoints(:, 1) = 1;
            for ii=1:iniInfo.num_trials
                trialNum = num2strpad(ii,4);
                [data, nc, sr, so, points] = nptReadStreamerFile([sessionname '.' trialNum]);
                framePoints(ii, 2) = points;
            end
        end
    end

    if ~isempty(syncs)  %(Monkey data prior to 2003 -- no point in guessing here)
        framePoints = FindFramePoints(iniInfo, trialFrames, extrasyncs, presentTrigs, syncs);
    end
    if newCat | oldCat
        [catInfo,iniInfo,framePoints] = GenerateCatInfo(sessionname, iniInfo, meanF, framePoints);
    end

end %extrasyncs

d.iniInfo = iniInfo;

if ~exist('extrasyncs','var')
    extrasyncs=[];
end
d.extrasyncs = extrasyncs;

if ~exist('scanlines','var')
    scanlines=[];
end
d.scanlines = scanlines;

if ~exist('framePoints','var')
    framePoints=[];
end
d.framePoints = framePoints;

if ~exist('syncs','var')
    syncs=[];
end
d.syncs = syncs;

if ~exist('catInfo','var')
    catInfo = [];
end
d.catInfo = catInfo;