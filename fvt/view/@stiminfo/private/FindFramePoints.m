function framePoints = FindFramePoints(iniInfo, trialFrames, extrasyncs, presentTrigs, syncs);
%  [framePoints, totalFrames] = FindFramePoints(sessionname, iniInfo, presentTrigs, extrasyncs);
%  FindFramePoints -- function to find the datapoint value of stimulus frame changes
%	very similar to "vecframes" for single trial data
%
%  Input:   session -- session name prefix, ex:  'test043003'
%			iniInfo -- output of ReadRevCorrIni.m  (list of session parameters)
%           trialFrames -- number of frames for each trial from the init
%               info file
%           syncs -- datapoint that each sync occurred at from the sync
%               monitor file           
%			presentTrigs -- an output of getPresentTrigs.m or getStimInfo.m
%				This is a matrix of numTrials x 2, with each row containing the start and 
%				stop trigger values (in datapoints) for that trial.
%           extrasyncs -- output structure of ReadExtraSyncsFile.m
%
%  Output:  framePoints -- a matrix of numTrials x (numFrames + 2).  Trials with fewer frames
%				than others are padded with zeroes.  The last nonzero number in each row is
%				the point at which the last frame shown for that trial ended.  All rows end
%				in a zero as this is used by SNRevCorr.m to advance to the next trial.
%
%Dependencies:  ReadExtraSyncsFile.m, FindTrialFrames.m, nptReadSyncsFile.m

%Find out if the data is "oldcat" or "oldmonkey"
if isfield(iniInfo,'oldMonkey')
    oldmonkey = iniInfo.oldMonkey;
else
    oldmonkey = 0;
end
if isfield(iniInfo,'oldcat')
    oldcat = iniInfo.oldcat;
else
    oldcat = 0;
end
%Find the number of frames shown per trial
numTrials = length(trialFrames);
%The plus 2 here is for the endpoint of the last frame, and a zero
framePoints = zeros(numTrials, max(trialFrames) + 2);
crop = 0;               %A later guard against "over-initialization"
%fprintf('  Calculating FramePoints ...   ');
for index = 1:numTrials
	if oldcat   %In this data, the first frame is blank; extra syncs here are attributed to the 0 frame.
        trialSyncs = [ones(1, (trialFrames(index) + 1)) * iniInfo.refreshes_per_frame];
    else        %The "0" here refers to the presenter trigger sync.  An extra sync can occur here.
        trialSyncs = [0, ones(1, trialFrames(index)) * iniInfo.refreshes_per_frame];
    end
    %Adjust for extrasyncs
    trigExtraS = 0;
    if ~isempty(extrasyncs)
        if ismember(index, extrasyncs.trials)
            tr = find(extrasyncs.trials == index);
            %If an extra sync occurs on the blank frame before the end trigger,
            %note the occurence (with trigExtraS), but do nothing else.
            if extrasyncs.frames(tr(end)) > (length(trialSyncs) - 1)
                trigExtraS = extrasyncs.numExtraS(tr(end));
                tr = tr(1:end - 1);
            end
            %Add a sync at each location according to the extra syncs file.  The "+ 1" here is to account
            %for the possibility of an extra syncs on the zero frame (trigger, or opening blank frame).
            trialSyncs(extrasyncs.frames(tr) + 1) = trialSyncs(extrasyncs.frames(tr) + 1) + extrasyncs.numExtraS(tr);
        end
    end
    %Find the sync immediately prior to the presenter start trigger
    %For cat and free-viewing monkey data, "startSync" should always be 1,
    %meaning that the trial begins on the first member of the sync monitor
    startSync = max(find(syncs(index,:) < presentTrigs(index, 1)));
    if isempty(startSync)
        startSync = 1;
    end
    %Advance the other syncs by number of syncs prior to the trigger
    %"frameSyncs" is the vector of indices of frame beginnings into the syncs matrix/vector
    frameSyncs = cumsum(trialSyncs) + startSync;
    
    %Check to make sure that the data accounts for what we empirically
    %expect (between the presenter triggers, at least).
    if oldmonkey | oldcat
        trigSyncs = 0;
    else
        trigSyncs = 1;  %In post 2003 data, there is one blank sync before the end trigger.
    end
    %Make sure that we accounted for every sync that was present in the data.
    %The " + 1" is for the zero sync that all trials begin on.
    %"trigExtraS" is the number of extra syncs on the end trigger, which
    %are not accounted for in "trialSyncs".
    expectedSyncs = sum(trialSyncs) + 1 + trigSyncs + trigExtraS;
    endSync = max(find(syncs(index,:) < presentTrigs(index, 2)));
    actualSyncs = endSync - startSync + 1;
    %In the case of still images, the data will always seem longer than necessary.
    if (expectedSyncs < actualSyncs) & (iniInfo.still_images == 0)
        fprintf('Warning:  More syncs present in data than expected on trial %i\n', index)
    elseif expectedSyncs > actualSyncs
        fprintf('Warning:  Fewer syncs present in data than expected on trial %i\n', index)
        crop = 1;
        newEnd = max(find(frameSyncs < endSync));          %If there are fewer syncs than necessary,
        frameSyncs = [frameSyncs(1:newEnd), endSync];      %adjust frameSyncs to end at the trigger.
    end
    
    if iniInfo.still_images == 0 %Since the "refreshes per frame" of a still images session is
        %meaningless, the full frameSyncs vector is not useful.
        %The " + 1" is to account for syncs being zero-based.
        trialFramePoints = syncs(index,frameSyncs) + 1;
    else        %if the stimulus was still images we only need the first member of frameSyncs and the sync which
        endInd = max(find(syncs(index,:) <= presentTrigs(index, 2)));   %coincides with the end presenter trigger        
        trialFramePoints = [syncs(index, frameSyncs(1)), syncs(index, endInd)] + 1;
    end
    %pad with zeros if trialFramePoints isn't large enough
    if length(trialFramePoints) < size(framePoints, 2)
        trialFramePoints = [trialFramePoints zeros(1, size(framePoints, 2) - length(trialFramePoints))]; 
    end
    framePoints(index, :) =  trialFramePoints;
end
if crop                                                     %If one (or more) of the trials had fewer frames
    smashedFP = sum(framePoints, 1);                        %than expected, framePoints may have been "over initialized."
    correctLength = min(find(smashedFP == 0));               %Crop it so that the trial with the most frames has
    framePoints = framePoints(:, [1:correctLength]);        %only a single zero at it's row's end.
end