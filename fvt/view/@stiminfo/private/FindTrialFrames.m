function trialFrames = FindTrialFrames(iniInfo, extrasyncs, sessionname);

%trialFrames = FindTrialFrames(iniInfo, extrasyncs, sessionname);
%   Function which uses the data from the ini file of a session
%   to determine how many frames are shown in each trial.  This is
%   necessary if the data was produced prior to 2003 when the [NUMBER OF
%   FRAMES SHOWN PER TRIAL] field was included in the .ini file, and the
%   so-called "trial_separation" was unreliable.
%   trialFrames is a vector of frame numbers (index num = trial num)

dirlist = nptDir([sessionname '.0*']);
stimType = iniInfo.type;
numTrials = length(dirlist);

%If the session is fixating
if (strcmp(stimType, 'M-Sequence')) | (strcmp(stimType, 'Sparse Noise')) | (strcmp(stimType, 'Movie') & ~iniInfo.win_movie)
    %Because these sessions are triggered by control and neither the sync signal, 
    %nor the presenter trigger were acquired there is a good deal of guessing
    %(and trusting the .ini file) involved here.
    %Here are my assumptions:  A trial never contained more frames than it
    %was supposed to according to the .ini file, AND, if a trial was too
    %short to show this "correct" number of frames, the largest possible number
    %was shown, given the timing parameters, AND, there was at least 1 sync
    %between the control and presenter start triggers.
    for index = 1:numTrials
        [data, nc, sr, so, points] = nptReadStreamerFile(dirlist(index).name);
        syncTime = round(1000/iniInfo.refresh_rate);
        if ~isempty(extrasyncs)
            if ismember(index, extrasyncs.trials)
                tr = find(extrasyncs.trials == index);
                %The frames don't matter, only the number of extra syncs in the trial
                ESvec =  extrasyncs.numExtraS(tr);
                positive = find(ESvec > 0);
                %We don't want to include negative extra syncs, though.
                numES = sum(ESvec(positive));
            else
                numES = 0;
            end
        else
            numES = 0;
        end
        if isfield(iniInfo, 'StimulusLatency')
            if iniInfo.StimulusLatency < syncTime
                latency = syncTime;
            else
                latency = iniInfo.StimulusLatency;
            end
            timing = latency + iniInfo.PostStimulusDuration;
        else
            timing = syncTime;
        end
        adjPoints = points - (sr * ((numES * syncTime) + timing));
        maxFrames = floor(adjPoints/(sr * syncTime * iniInfo.refreshes_per_frame));
        if maxFrames > iniInfo.trial_separation
            trialFrames(index) = iniInfo.trial_separation;
        else
            trialFrames(index) = maxFrames;
        end
    end
        
elseif ~iniInfo.still_images        %If the session is a free viewing movie
    %Because these sessions are triggered by presenter, the entire acquired trial
    %has stimulus in it.
    for index = 1:numTrials
        [data, nc, sr, so, points] = nptReadStreamerFile(dirlist(index).name);
        syncPoints = round(sr/iniInfo.refresh_rate);
        if ~isempty(extrasyncs)
            if ismember(index, extrasyncs.trials)
                tr = find(extrasyncs.trials == index);
                %The frames don't matter, only the number of extra syncs in the trial
                ESvec =  extrasyncs.numExtraS(tr);
                positive = find(ESvec > 0);
                %We don't want to include negative extra syncs, though.
                numES = sum(ESvec(positive));
            else
                numES = 0;
            end
        else
            numES = 0;
        end
        adjPoints = points - (numES * syncPoints);
        trialFrames(index) = floor(adjPoints/(syncPoints * iniInfo.refreshes_per_frame));
    end
        
else                %If the session is still images
    trialFrames = ones(numTrials, 1);
end