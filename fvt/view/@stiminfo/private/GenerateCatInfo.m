function [catInfo,iniInfo,framePoints] = GenerateCatInfo(sessionname, iniInfo, meanF, framePoints)
% This is a subfunction used under the stimInfo class, it will calculate
% and generate extra information needed for the cat experimental data.
% It will also check the framePoints vector to look for errors in the
% number of values within the vector, monkey data has a stop trigger.

% Flag the new field name, DAQ if it exists, only for new UEI DAQ files
if isfield(iniInfo,'DAQ')

    if strcmp(iniInfo.DAQ,'UEI')

        catInfo.samplingrate = 29990; % Samplingrate in datapoints/second, usually 30000.
        catInfo.video_refresh = meanF; % Video Monitor refresh rate in Hz.
        catInfo.sync_duration = (1/meanF)*1000; % Monitor Sync Duration in Milliseconds
        catInfo.frame_duration = (1/meanF)*1000*iniInfo.refreshes_per_frame; % Stimulus Frame Duration in Milliseconds
        % FindFramePoints adds a 1 to account for the 0 based values, since
        % we later convert it to time, in milliseconds, we want it zero
        % based. % Convert this to 1 based for LFP data
        framePoints = framePoints-1;

        if strcmpi(iniInfo.type,'movie')

            %%%%%% Cat info for the movie stimulus %%%%%%%%%%%
            catInfo.num_frames = iniInfo.end_frame-iniInfo.start_frame+1; % Calculate the number of frames displayed for each repetition
            catInfo.repetition_duration = catInfo.num_frames*catInfo.frame_duration; %Average Repetition Duration in milliseconds
            catInfo.num_repetitions = iniInfo.number_movie_repetitions;
            % Since presenter is ZERO based and matlab files are ONE based
            % I change the values, by adding 1. Other programs reading the
            % movie files should not correct for this error, since the
            % readPGLFrames program corrects for this by subtracting 1
            % from each frame number passed to it. The scene changes
            % numbers are zero based, so no correction is needed.
            iniInfo.end_frame = iniInfo.end_frame+1;
            iniInfo.start_frame = iniInfo.start_frame+1;
            framePoints = framePoints(1:(iniInfo.total_number_frames_displayed+1));
            cwd = pwd;
            cd ../../..
            load scenechanges.mat
            if strcmpi('cats',iniInfo.stimulus_root(end-3:end));
                scene_changes = scenechanges.cats;
            elseif strcmpi('pinkcats',iniInfo.stimulus_root);
                scene_changes = scenechanges.pinkcats;
            elseif strcmpi('Photo',iniInfo.stimulus_root);
                scene_changes = [];
            elseif strcmpi('CSD',iniInfo.stimulus_root(end-2:end));
                scene_changes = 125; % zero indexed, actually 126th frame
            elseif strcmpi('gwn10',iniInfo.stimulus_root);
                scene_changes = [];
            elseif strcmpi('gwn15',iniInfo.stimulus_root);
                scene_changes = [];
            elseif strcmpi('lsrc',iniInfo.stimulus_root);
                scene_changes = [];
            end
            % Correct for the zero based numbering of the scene changes
            % The first frame displayed can be considered a scene change
            % Even if the movie was only repeated one time. But not for CSD
            % movies, which have not scene transitions
            if strcmpi('CSD',iniInfo.stimulus_root(end-2:end));
                catInfo.scene_changes=scene_changes+1;
            else
                catInfo.scene_changes = [iniInfo.start_frame intersect(scene_changes+1,iniInfo.start_frame:iniInfo.end_frame)];
            end
            % New data from P1 onward includes spontaneous gray frames at
            % the end, thus the end frame a scene change occurs. 
            if isfield(iniInfo,'spontaneous_activity_duration_in_frames')
                if iniInfo.spontaneous_activity_duration_in_frames>0
                    catInfo.scene_changes = [catInfo.scene_changes iniInfo.end_frame+1];
                end
            end
%             catInfo.contrast = MovieContrast(iniInfo);
%             if isfield(iniInfo,'spontaneous_activity_duration_in_frames')
%                 catInfo.contrast = [catInfo.contrast zeros(1,iniInfo.spontaneous_activity_duration_in_frames)];
%             end
            cd(cwd)

        elseif strcmpi(iniInfo.type,'sparse_noise')

            %%%%%% Read the SEQ file
            catInfo.seq_file = read_seq([sessionname '.seq']);
            framePoints = framePoints(1:iniInfo.total_number_frames_displayed+1);
            %%%%%% Cat info for the Grating stimulus %%%%%%%%%%%
            % The -1 added to the grid points is to make the values 0 based
            if strcmpi(iniInfo.obj_type ,'square grating')

                num_orientations = iniInfo.num_orientations;
                directions_per_orientation = iniInfo.directions_per_orientation;
                num_spatial_frequencies = length(iniInfo.spatial_frequencies);
                num_temporal_frequencies = length(iniInfo.temporal_frequencies);
                num_repetitions = iniInfo.num_blocks;

                if strcmpi(iniInfo.repeated_blocks,'on')
                    rep_count=1;
                else
                    rep_count=num_repetitions;
                end

                catInfo.num_orientations = num_orientations;
                catInfo.directions_per_orientation = directions_per_orientation;
                catInfo.num_spatial_frequencies = num_spatial_frequencies;
                catInfo.num_temporal_frequencies = num_temporal_frequencies;
                catInfo.num_repetitions = num_repetitions;
                catInfo.num_grating_stimuli = num_orientations*directions_per_orientation*num_spatial_frequencies*num_temporal_frequencies;

                % Check the SEQ File for the correct number of Stimuli
                num_sn_frames = catInfo.num_grating_stimuli*rep_count;
                num_sp_frames = iniInfo.spontaneous_activity_duration_in_frames;
                num_seq_frames = size(catInfo.seq_file,1);
                if num_sn_frames+num_sp_frames~=num_seq_frames;
                    fprintf('NUMBER FRAMES IN SEQ FILE DIFFER')
                end

                % Since the SEQ file does not write down the TF and SF for
                % when only one TF and SF are used, we add them as zeros.
                numSTFs = catInfo.num_spatial_frequencies+catInfo.num_temporal_frequencies;
                if numSTFs==2;
                    ind=find(catInfo.seq_file(:,1)==-1);
                    catInfo.seq_file = [catInfo.seq_file zeros(size(catInfo.seq_file,1),2)];
                    if ~isempty(ind)
                        catInfo.seq_file(ind,5:6)=-1;
                    end
                end

            elseif strcmpi(iniInfo.obj_type,'bar')

                num_orientations = iniInfo.num_orientations;
                num_colors = iniInfo.num_colors;
                num_locations = iniInfo.grid_cols*iniInfo.grid_rows;
                num_repetitions = iniInfo.num_blocks;

                if strcmpi(iniInfo.repeated_blocks,'on')
                    rep_count=1;
                else
                    rep_count=num_repetitions;
                end

                catInfo.num_orientations = num_orientations;
                catInfo.num_colors = num_colors;
                catInfo.num_locations = num_locations;
                catInfo.num_repetitions = num_repetitions;
                catInfo.num_sparse_bar_stimuli = num_orientations*num_colors*num_locations;

                % Add the grid points, x and y for each object, only
                % written in the new ini files, post 2006. Convert to zero
                % based indexing by subtracting 1
                num_sn_frames = catInfo.num_sparse_bar_stimuli*rep_count;
                num_sp_frames = iniInfo.spontaneous_activity_duration_in_frames*rep_count;
                num_seq_frames = size(catInfo.seq_file,1);
                if num_sn_frames+num_sp_frames==num_seq_frames;
                    catInfo.seq_file = [catInfo.seq_file (iniInfo.Grid_Points.x_grid_points-1)' (iniInfo.Grid_Points.y_grid_points-1)'];
                else
                    fprintf('NUMBER FRAMES IN SEQ FILE DIFFER')
                end

            elseif strcmpi(iniInfo.obj_type,'square')

                num_orientations = iniInfo.num_orientations;
                num_colors = iniInfo.num_colors;
                num_locations = iniInfo.grid_cols*iniInfo.grid_rows;
                num_repetitions = iniInfo.num_blocks;

                if strcmpi(iniInfo.repeated_blocks,'on')
                    rep_count=1;
                else
                    rep_count=num_repetitions;
                end

                catInfo.num_orientations = num_orientations;
                catInfo.num_colors = num_colors;
                catInfo.num_locations = num_locations;
                catInfo.num_repetitions = num_repetitions;
                catInfo.num_sparse_square_stimuli = num_orientations*num_colors*num_locations;

                % Add the grid points, x and y for each object, only
                % written in the new ini files, post 2006. Convert to zero
                % based indexing by subtracting 1
                num_sn_frames = catInfo.num_sparse_square_stimuli*rep_count;
                num_sp_frames = iniInfo.spontaneous_activity_duration_in_frames;
                num_seq_frames = size(catInfo.seq_file,1);
                if num_sn_frames+num_sp_frames==num_seq_frames;
                    catInfo.seq_file = [catInfo.seq_file (iniInfo.Grid_Points.x_grid_points-1)' (iniInfo.Grid_Points.y_grid_points-1)'];
                else
                    fprintf('NUMBER FRAMES IN SEQ FILE DIFFER')
                end

            end

        end

    elseif strcmp(iniInfo.DAQ,'NI')

        catInfo.samplingrate = 30000; % Samplingrate in datapoints/second, usually 30000.
        catInfo.video_refresh = meanF; % Video Monitor refresh rate in Hz.
        catInfo.sync_duration = (1/meanF)*1000; % Monitor Sync Duration in Milliseconds
        catInfo.frame_duration = (1/meanF)*1000*iniInfo.refreshes_per_frame; % Stimulus Frame Duration in Milliseconds

        if strcmpi(iniInfo.type,'movie')

            %%%%%% Cat info for the movie stimulus %%%%%%%%%%%
            catInfo.num_frames = iniInfo.end_frame-iniInfo.start_frame+1; % Calculate the number of frames displayed for each repetition
            catInfo.repetition_duration = catInfo.num_frames*catInfo.frame_duration; %Average Repetition Duration in milliseconds
            catInfo.num_repetitions = iniInfo.frames_displayed/catInfo.num_frames; % Calculate the number of movie repetitions
            % Since presenter if ZERO based and the .pgl files are ONE based I change the values of the frame numbers displayed.
            iniInfo.end_frame = iniInfo.end_frame+1;
            iniInfo.start_frame = iniInfo.start_frame+1;
            total_num_frames = iniInfo.frames_displayed;
            framePoints = framePoints(1:total_num_frames+1);
            diff_syncs = mean(diff(framePoints));
            if diff(framePoints(end-1:end)) < diff_syncs-100 %%%%% This takes care of a missing end sync.
                framePoints(end) = framePoints(end-1)+diff_syncs;
            end

        elseif strcmpi(iniInfo.type,'m_sequence')

            total_num_frames = iniInfo.frames_displayed;
            framePoints = framePoints(1:total_num_frames+1); % Takes out the 0 for the monkey data at the end
            catInfo.num_frames = total_num_frames; % Calculate the number of frames displayed for each repetition
            catInfo.repetition_duration = catInfo.num_frames*catInfo.frame_duration; %Repetition Duration in milliseconds
            catInfo.num_repetitions = 1; % Calculate the number of movie repetitions

        elseif strcmpi(iniInfo.type,'sparse_noise')

            %% Read the SEQ file and remove the -1 values which indicate a new block %%%%%%
            seq = read_seq([sessionname '.seq']);
            ind = find(seq(:,1)==-1);
            seq(ind,:)=[];
            catInfo.seq_file = seq;
            %%%%%% Cat info for the Grating stimulus %%%%%%%%%%%
            if strcmpi(iniInfo.obj_type ,'grating')
                [framePoints,catInfo] = FindGratingFramesPoints(sessionname, iniInfo, catInfo);
                %%%%%%% If a Grating Jittering File was Used %%%%%
                if iniInfo.use_jittering_file == 1
                    if strcmp(iniInfo.jittering_file_name(end-16:end),'OneOverFBeta1.txt')
                        filename = 'OneOverFBeta1.txt';
                    elseif strcmp(iniInfo.jittering_file_name(end-18:end),'BuracasA3B2P033.txt')
                        filename = 'BuracasA3B2P033.txt';
                    else
                        filename = [];
                    end
                    cwd = pwd;
                    if ~isempty(filename)
                        cd c:\data
                        temporal_frequency = readGratingSequence(filename)';
                        catInfo.temporal_frequency = temporal_frequency;
                    end
                    cd(cwd);
                else
                    catInfo.temporal_frequency =[];
                end
                %%%%%% Cat info for the Sparse Bars stimulus %%%%%%%%%%%
            elseif strcmpi(iniInfo.obj_type,'bar')
                iniInfo.obj_type='Bar';
                %%%%% Correct for later versions with different fields %%%%%%
                if isfield(iniInfo,'bar_orientation')
                    iniInfo.orientation_angles = iniInfo.bar_orientation;
                    iniInfo = rmfield(iniInfo,'bar_orientation');
                end
                if isfield(iniInfo,'rows')
                    iniInfo.grid_rows = iniInfo.rows;
                    iniInfo = rmfield(iniInfo,'rows');
                    iniInfo.grid_cols = iniInfo.cols;
                    iniInfo = rmfield(iniInfo,'cols');
                end
                if isfield(iniInfo,'spontaneous_trials')
                    iniInfo.spontaneous_activity_duration_in_frames = iniInfo.spontaneous_trials;
                    iniInfo = rmfield(iniInfo,'spontaneous_trials');
                end
                if isfield(iniInfo,'bar_length')
                    iniInfo.object_length = iniInfo.bar_length/20; % Convert to Degrees
                    iniInfo = rmfield(iniInfo,'bar_length');
                    iniInfo.object_width = iniInfo.bar_width/20; % Convert to Degrees
                    iniInfo = rmfield(iniInfo,'bar_width');
                end
                if isfield(iniInfo,'random_type')
                    if strcmpi(iniInfo.random_type,'PSEUDO_RANDOM')
                        iniInfo.stimulus_order_type = 'Pseudo-Random';
                        iniInfo = rmfield(iniInfo,'random_type');
                    end
                end
                if isfield(iniInfo,'repeat_blocks')
                    if strcmpi(iniInfo.repeat_blocks,'REPEAT')
                        iniInfo.repeated_blocks = 'on';
                        iniInfo = rmfield(iniInfo,'repeat_blocks');
                    elseif strcmpi(iniInfo.repeat_blocks,'NO_REPEAT')
                        iniInfo.repeated_blocks = 'off';
                        iniInfo = rmfield(iniInfo,'repeat_blocks');
                    end
                end
                if isfield(iniInfo,'num_stimulus_repetitions')
                    iniInfo.num_sparse_noise_repetitions = iniInfo.num_stimulus_repetitions;
                end
                if isfield(iniInfo,'frames_displayed')
                    iniInfo.total_number_frames_displayed = iniInfo.frames_displayed;
                end
                if isfield(iniInfo,'extra_syncs');
                    iniInfo.number_extra_syncs = iniInfo.extra_syncs;
                    iniInfo = rmfield(iniInfo,'extra_syncs');
                end
                iniInfo.total_number_of_frames_per_block = iniInfo.grid_rows*iniInfo.grid_cols*iniInfo.num_colors*iniInfo.num_orientations;
                iniInfo.on_luminance = 255;
                iniInfo.off_luminance = 0;
                framePoints(end) = []; % Remove the zero added for the monkey.
            end

        end

    else % Older Cat StimInfo Files

        catInfo.samplingrate = 30000; % Samplingrate in datapoints/second, usually 30000.
        catInfo.video_refresh = meanF; % Video Monitor refresh rate in Hz.
        catInfo.sync_duration = (1/meanF)*1000; % Monitor Sync Duration in Milliseconds
        catInfo.frame_duration = (1/meanF)*1000*iniInfo.refreshes_per_frame; % Stimulus Frame Duration in Milliseconds

        if strcmpi(iniInfo.type,'movie')

            %%%%%% Cat info for the movie stimulus %%%%%%%%%%%
            catInfo.num_frames = iniInfo.end_frame-iniInfo.start_frame+1; % Calculate the number of frames displayed for each repetition
            catInfo.repetition_duration = catInfo.num_frames*catInfo.frame_duration; %Average Repetition Duration in milliseconds
            catInfo.num_repetitions = iniInfo.frames_displayed/catInfo.num_frames; % Calculate the number of movie repetitions
            % Since presenter if ZERO based and the .pgl files are ONE based I change the values of the frame numbers displayed.
            iniInfo.end_frame = iniInfo.end_frame+1;
            iniInfo.start_frame = iniInfo.start_frame+1;
            total_num_frames = iniInfo.frames_displayed;
            framePoints = framePoints(1:total_num_frames+1);
            diff_syncs = mean(diff(framePoints));
            if diff(framePoints(end-1:end)) < diff_syncs-100 %%%%% This takes care of a missing end sync.
                framePoints(end) = framePoints(end-1)+diff_syncs;
            end

        elseif strcmpi(iniInfo.type,'m_sequence')

            total_num_frames = iniInfo.frames_displayed;
            framePoints = framePoints(1:total_num_frames+1); % Takes out the 0 for the monkey data at the end
            catInfo.num_frames = total_num_frames; % Calculate the number of frames displayed for each repetition
            catInfo.repetition_duration = catInfo.num_frames*catInfo.frame_duration; %Repetition Duration in milliseconds
            catInfo.num_repetitions = 1; % Calculate the number of movie repetitions

        elseif strcmpi(iniInfo.type,'sparse_noise')

            %%%%%% Read the SEQ file
            catInfo.seq_file = read_seq([sessionname '.seq']);
            %%%%%% Cat info for the Grating stimulus %%%%%%%%%%%
            if strcmpi(iniInfo.obj_type ,'grating')
                [framePoints,catInfo] = FindGratingFramesPoints(sessionname, iniInfo, catInfo);
                %%%%%%% If a Grating Jittering File was Used %%%%%
                if iniInfo.use_jittering_file == 1
                    if strcmp(iniInfo.jittering_file_name(end-16:end),'OneOverFBeta1.txt')
                        filename = 'OneOverFBeta1.txt';
                    elseif strcmp(iniInfo.jittering_file_name(end-18:end),'BuracasA3B2P033.txt')
                        filename = 'BuracasA3B2P033.txt';
                    else
                        filename = [];
                    end
                    cwd = pwd;
                    if ~isempty(filename)
                        cd c:\data
                        temporal_frequency = readGratingSequence(filename)';
                        catInfo.temporal_frequency = temporal_frequency;
                    end
                    cd(cwd);
                else
                    catInfo.temporal_frequency =[];
                end
                %%%%%% Cat info for the Sparse Bars stimulus %%%%%%%%%%%
            elseif strcmpi(iniInfo.obj_type,'bar')
                framePoints(1) = 0; %Change first datapoint to time 0.
                framePoints(end) = []; % Remove the zero added for the monkey.
            end
        end
    end
end
