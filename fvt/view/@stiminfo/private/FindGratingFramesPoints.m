function [framePoints,catInfo] = FindGratingFramesPoints(sessionname, iniInfo, catInfo)

%  [framePoints, totalFrames] = FindGratingFramePoints(sessionname, iniInfo);
%  FindFramePoints -- function to find the millisecond value of stimulus frame changes
%  very similar to "findFramePoints" for single trial data
%
%  Input:   session -- session name prefix, ex:  'test043003'
%			iniInfo -- output of ReadRevCorrIni.m  (list of session parameters)
%           
%  Output:  framePoints -- a matrix of numTrials x (numFrames + 2).  Trials with fewer frames
%				than others are padded with zeroes.  The last nonzero number in each row is
%				the point at which the last frame shown for that trial ended.  All rows end
%				in a zero as this is used by SNRevCorr.m to advance to the next trial.
%
% Dependencies: ReadRevCorrIni.m,ReadExtraSyncsFile.m, nptReadSyncsFile.m

%%%%% Get grating values to create the frame points vector %%%%%
total_number_frames = iniInfo.total_frames; %%%%%%% Total number of Frames Displayed %%%%%
num_orientations = iniInfo.num_orientations; %%%%%%% Number of Orientation %%%%%%
dir_per_orientation = iniInfo.dir_per_orientation; %%%%%% Directions Per Orientation %%%%%
num_repetitions = iniInfo.num_blocks; %%%%%% Number of Repeats Per Orientation %%%%%%%
num_frames = total_number_frames/(num_repetitions*num_orientations*dir_per_orientation); %%%% Number of Frames per Repetition %%%%

if strmatch(sessionname(1:2),strvcat('a3','a4')) %%%%%%%%% Due to the fact that the last repetition does not have a blank.
    num_blank_repetitions = (num_repetitions*num_orientations*dir_per_orientation)-1;
    num_stimulus_repetitions = num_repetitions*num_orientations*dir_per_orientation;
    isi = iniInfo.inter_stimulus_interval; %%%%%%% Inter Stimulus Interval in syncs. 
    if iniInfo.use_jittering_file == 0
        drifting_order = [];
        num_cycles = iniInfo.temporal_freq*25; %%% The total number of cycles displayed
        num_grating_stimuli = num_repetitions*dir_per_orientation*num_orientations; % Total Number of grating stimuli
        d_order = 1:num_cycles;
        for g = 1:num_grating_stimuli
            if g == 1
                drift_order = d_order(2:end);
                num_diff = (num_frames-length(drift_order))-1;
                drift_order = [drift_order d_order(1:num_diff)];
            else
                if drift_order(end) == num_cycles
                    drift_order = d_order(1:end);
                    num_diff = num_frames-length(drift_order);
                    drift_order = [drift_order d_order(1:num_diff)];                    
                else
                    drift_order = drift_order(end)+1:num_cycles;
                    num_diff = num_frames-length(drift_order);
                    drift_order = [drift_order d_order(1:num_diff)];   
                end
            end
            drifting_order = [drifting_order drift_order];
        end  
        catInfo.drifting_order = drifting_order;
    end
else
    num_blank_repetitions = (num_repetitions*num_orientations*dir_per_orientation);
    isi = iniInfo.isi;
end

%%%%%% Place some info into the catInfo structure %%%%%%
catInfo.num_repetitions = num_repetitions;
catInfo.repetition_duration = num_frames*catInfo.frame_duration;
catInfo.num_frames = num_frames;
catInfo.num_stimulus_repetitions = num_repetitions*dir_per_orientation*num_orientations;
catInfo.num_blank_repetitions = num_blank_repetitions; 
catInfo.num_stimulus_frames = num_frames;
catInfo.num_orientations = num_orientations;
catInfo.num_directions = dir_per_orientation; 
catInfo.repetition_duration_with_isi = (catInfo.num_stimulus_frames*catInfo.frame_duration) + (iniInfo.inter_stimulus_interval*catInfo.sync_duration); % Repetition Durations in Milliseconds
catInfo.repetition_duration_without_isi = catInfo.num_stimulus_frames*catInfo.frame_duration; % Repetition Durations in Milliseconds

%Read the extrasyncs file
[es] = ReadExtraSyncsFile([sessionname '_extra_syncs.txt']);

if ~isempty(es.trials)
    extra_syncs = [es.frames' es.numExtraS'];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Will have to change this once the first frame presented is blank.
    if extra_syncs(1) == 0
        extra_syncs(1,:)=[];
        fprintf('extra_syncs on first frame')
    end
    if extra_syncs(end) == total_number_frames+1
        extra_syncs(end,:)=[];
    end
    if strmatch(sessionname(1:2),strvcat('a3','a4')) %%%%% Since the sync monitor was fucked up on experiments a3 and a4
        extra_syncs(1,:) = [];        
    end
    iniInfo.total_extra_syncs=sum(extra_syncs(:,2));
    extra_syncs(1) = 100000000000000000;    
else
    extra_syncs(1) = 100000000000000000; % Since some files have no extra-syncs I do this so the loop does not crash.
    iniInfo.total_extra_syncs = 0;
end

%%%%%%% Read the syncs file %%%%%%
[syncs,dataFilename,records,meanF,stdF] = nptReadSyncsFile([sessionname '.snc']);
if strmatch(sessionname(1:2),strvcat('a3','a4')) %%%%% Since the first frame was fucked up on experiments a3 and a4 and missing
    count=-6;       
else
    count=0;
end

%%% Check for an error in the creation
if length(syncs) ~= ((total_number_frames*iniInfo.refreshes_per_frame)+(isi*num_blank_repetitions))+2+count % add 2 due to start and end trigger syncs
    fprintf('Missmatch in sync file and frames vector')
    length_of_syncs_file = length(syncs)
    expected_number_of_syncs = (total_number_frames*iniInfo.refreshes_per_frame)+(isi*num_blank_repetitions)+2 
    if length(syncs) < ((total_number_frames*iniInfo.refreshes_per_frame)+(isi*num_blank_repetitions))+2 
        num_syncs_diff = ((total_number_frames*iniInfo.refreshes_per_frame)+(isi*num_blank_repetitions))+2 - length(syncs)
        for s = 1:num_syncs_diff
            syncs = [syncs;syncs(end)+mean(diff(syncs))];
        end
    end
end

%since the last sync is the end trigger sync and the start is the start trigger.
syncs(end) = []; % Take out the last sync corresponding to the end trigger
syncs(1) = []; % Take out the first sync corresponding to the start trigger

%%%%%%%% Now add to the total number of frames the isi times %%%%%%%%
% FROM THE SYNC-DATAPOINTS AND THE EXTRA-SYNC VECTOR get the frame vector
frame_numbers = 1:num_frames;
for r = 1:(num_repetitions*num_orientations*dir_per_orientation)
    %%%%%%% Go through each grating stimulus and account for extra syncs
    if strmatch(sessionname(1:2),strvcat('a3','a4'))
        %%%%%% Due to another fuckup in the grating first frame and first
        %%%%%% presentation we change the frame vector to account for it.
        if r == 1
            frame_numbers=1:num_frames-1;
        end    
    end        
    for f = frame_numbers(1):frame_numbers(end)
        if extra_syncs(1,1)~=f
            frames_vector(f) = iniInfo.refreshes_per_frame;
        else
            frames_vector(f) = iniInfo.refreshes_per_frame+extra_syncs(1,2);
            extra_syncs(1,:)=[];
            if isempty(extra_syncs)
                extra_syncs(1) = 1000000000000000;
            end
        end        
    end
    %%%%%% Increment the frame numbers along with the isi frame %%%%
    frame_numbers = frame_numbers(end)+2:frame_numbers(end)+1+num_frames;
    %%%%%% Add to the frames_vector the isi in syncs.
    if strmatch(sessionname(1:2),strvcat('a3','a4')) %% To take care of the problems in the stimulus presentation
        if r ~= (num_repetitions*num_orientations*dir_per_orientation) % Check since the last grating stimulus did not have a blank ISI.
            frames_vector(f+1) = isi;
        end
    else
        frames_vector(f+1) = isi;
    end
end

frames_vector = cumsum(frames_vector);
% Frames_vector was an index that allows us to grab the syncs corresponding
% the appropriate frame ending point. and convert to milliseconds
frames_vector = (syncs(frames_vector)')+1; %add 1 due to the datapoints starting at 0

%%%%% Presenter was changed to have no blank frame at the beginning of the
%%%%% stimulus
if iniInfo.Date(3) >= 3
    framePoints = [0 frames_vector];
else
    framePoints = [frames_vector(1)-catInfo.frame_duration frames_vector];
end