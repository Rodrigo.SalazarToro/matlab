function stimInfo = stimInfo(varargin)
%STIMINFO Constructor function for STIMINFO object
%	obj = STIMINFO(varargin) instantiates a stimInfo object by reading
%   the ini, extrasync, scanlines, descriptor, raw data, and syncmonitor
%   files.  Which are assumed to be in the current directory. 
%
%   stimInfo = StimInfo creates an empty sesinfo object.
%
%   Optional arguments are defined by the file getOptArgs and 
%   are ('Auto','redo',1,'save',1)
%
%   The object contains the following fields:
%      S.iniInfo
%      S.extrasyncs
%      S.scanlines
%      S.framePoints
%
%
%   Dependencies: GenerateStimInfo,getOptArgs.


Args = struct('RedoLevels',0,'SaveLevels',0, ...
    'Auto',0);

[Args,varargin] = getOptArgs(varargin,Args, ...
    'flags',{'Auto'}, ...
    'shortcuts',{'redo',{'RedoLevels',1};'save',{'SaveLevels',1}}, ...
    'subtract',{'RedoLevels','SaveLevels'});



if nargin==0
    % create empty object
    stimInfo = createEmptyObject;
    
elseif ((nargin==1) & (isa(varargin{1},'stiminfo')))
    stimInfo = varargin{1};
    
else
    % move to appropriate directory
    [p,cwd] = getDataDirs('Session','Relative','CDNow');
    % check if stiminfo.mat exists
    dirlist = nptDir('*stiminfo.mat','CaseInsensitive');
    if (~isempty(dirlist) & (Args.RedoLevels==0))
        try
            lastwarn('')
            fprintf('Loading saved stiminfo object...\n');
            l = load(dirlist(1).name);
            stimInfo = l.stimInfo;
             [lastmsg, lastid] = lastwarn; 
            if ~isempty(lastmsg)
                fprintf('Older format saved so recalculating stimInfo ....\n')
                error
            end
        catch
            Args.SaveLevels=1;
            stimInfo = createObject(Args);
        end
    else
        stimInfo = createObject(Args);
    end
    if(~isempty(cwd))
        cd(cwd);
    end
end





function stimInfo = createEmptyObject
d.data.iniInfo = [];
d.data.extrasyncs = [];
d.data.scanlines = [];
d.data.framePoints = [];
d.data.syncs = [];
d.data.catinfo = [];
d.data.sessionname = '';
stimInfo = class(d,'stiminfo');


function stimInfo = createObject(Args)

dirlist = nptDir('*.ini','CaseInsensitive');
if length(dirlist)>1
    fprintf('more than 1 ini file present!!  Using %s.\n',dirlist(1).name);
end
[p,fname,ext] = nptFileParts(dirlist(1).name);
d.data = GenerateStimInfo(fname);
d.data.sessionname = fname;
fprintf('\nCreating New StimInfo object for %s...\n',fname)
stimInfo = class(d,'stiminfo');
if Args.SaveLevels
    fprintf('\nSaving stiminfo object...\n');
    str = [fname '_stiminfo']; 
    save(str,'stimInfo')
end