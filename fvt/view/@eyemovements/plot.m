function [vem, varargout] = plot(vem,varargin)
%EYEMOVEMENTS/PLOT Plots raw data from Eye folder with 
%eye movements superimposed.
%   OBJ = PLOT(OBJ,N) plots the raw data from the trial specified N. 
%   The eye files are assumed to be in the current directory.

Args = struct('showTitle',0,'showPixels',0,'showXaxis',0,'title','', ...
    'linkedZoom',0,'Hist',0,'TickDir','out');
Args.flags = {'showTitle','showPixels','showXaxis','linkedZoom','Hist'};

[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'Hist'});
varargout{1} = {'Args',Args,'handle',gca};

if(~isempty(Args.NumericArguments))
    trial = Args.NumericArguments{1};
else
    trial = 1;
end
  
% open new figure if none exists, otherwise just use the current figure
gcf;
clist = get(gca,'ColorOrder');
clistsize = size(clist,1);

% channel = vem.eyes.channel;
sessionname = vem.data.sessionname;
%plot eye data

hold off

if(Args.Hist)
	[n,binedges,ni,a] = hist(vem,varargin2{:});
	bar(binedges,n,'histc');
    varargout{1} = {a};
else
	%instantiate an eyes object
	ey = eyes('auto');
	[ey outputs]= plot(ey,trial);
	%vertical is channel 1 
	%horizontal is channel 2
	linehandles = struct('h1',[],'h2',[]);
	linehandles = getOptArgs(outputs,linehandles);
	
	data(1,:) = get(linehandles.h1,'YData');
	data(2,:) = get(linehandles.h2,'YData');
	
	
	%superimpose eyemovements on top of eye data.
	%superimpose fixations
	fixationtf = zeros(1,length(data));
	for event=1:size(vem.data.fixation(trial).start,2)
      fixationtf(1,vem.data.fixation(trial).start(event):vem.data.fixation(trial).finish(event))=1;
	end
	fixation = data .* [fixationtf ; fixationtf];
	i=find(fixationtf==0);
	fixation(:,i)=NaN;
	hold on
	plot(fixation(1,:),'g.')
	plot(fixation(2,:),'g.')
	
	%superimpose saccades
	saccadetf = zeros(1,length(data));
	for event=1:size(vem.data.saccade(trial).start,2)
      saccadetf(1,vem.data.saccade(trial).start(event):vem.data.saccade(trial).finish(event))=1;
	end
	saccade = data .* [saccadetf ; saccadetf];
	i=find(saccadetf==0);
	saccade(:,i)=NaN;
	plot(saccade(1,:),'r.')
	plot(saccade(2,:),'r.')
	
	%superimpose drifts
	drifttf = zeros(1,length(data));
	for event=1:size(vem.data.drift(trial).start,2)
      drifttf(1,vem.data.drift(trial).start(event):vem.data.drift(trial).finish(event))=1;
	end
	drift = data .* [drifttf ; drifttf];
	i=find(drifttf==0);
	drift(:,i)=NaN;
	plot(drift(1,:),'b.')
	plot(drift(2,:),'b.')
	
	a=axis;
	%axis([-25 a(2) -20 20])
	ylabel('degrees')
    set(gca,'TickDir',Args.TickDir)
end
%xlabel('Time (msec)')
hold off
% set zoom to on
zoom on
