function em = ProcessSession(varargin)

positionrangesflag=0;
velocitiesflag=0;
EMDurationsflag=0;
spectraflag=0;
eccentricitiesflag=0;
redo=0;

num_args = nargin;
for i=1:num_args
    if ~isobject(varargin{i}) & ~isempty(varargin{i})
        switch varargin{i}
            case('positionrangesValue')
                positionrangesflag = varargin{i+1};    
            case('velocitiesValue')
                velocitiesflag = varargin{i+1};
            case('EMdurationsValue')
                EMDurationsflag = varargin{i+1};
            case('spectraValue')
                spectraflag = varargin{i+1};
            case('eccentricitiesValue')
                eccentricitiesflag = varargin{i+1};
            case('redo')% if passed redo argument, ignore the marker file 
                % processedeyemovements.txt
                redo=1;
            case('redoValue')
                redo = varargin{i+1};
        end
    end
end
em = eyemovements;
if (~checkMarkers(varargin{1},redo,'session'))
    dirlist = nptDir('eye');
    inilist = nptDir('*.ini');
    stimInfo = stiminfo('auto',1,'save');
    
    sessionname = inilist(1).name(1:end-4);
    if ~isempty(dirlist)
        cd eye
        fprintf('Generating Session Eye Movements ...\n')
        em = eyemovements('auto','save','RedoLevels',redo);
        
        if EMDurationsflag
            bin_length = [10 5 10];
            fprintf('Eye Duration Histograms ...\n')
            duration_histograms = fvtEyeDurationHistograms(em);
            filename = [sessionname , '_duration'];
            eval(['save ' filename ' duration_histograms'])
        end
        
        if positionrangesflag
            fprintf('Eye Position Ranges ...\n')
            position_range = fvtEyePositionRangeHistogram(em);
            filename = [sessionname , '_positionrange'];
            eval(['save ' filename ' position_range'])
        end
        
        if spectraflag
            fprintf('Eye Power Spectrum ...\n')
            power_spectrum = fvtEyePowerSpectrum(em);
            filename = [sessionname , '_powerspectrum'];
            eval(['save ' filename ' power_spectrum'])
        end
        
        if velocitiesflag
            fprintf('Eye Velocity Histograms ...\n')
            velocity = fvtEyeVelocityHistogram(em);
            filename = [sessionname , '_velocity'];
            eval(['save ' filename ' velocity']);
        end
        
        %if fixationg trial (sparse noise)
        if eccentricitiesflag
            if isfield(stimInfo.data.iniInfo, 'win_movie')
                if ~(stimInfo.data.iniInfo.win_movie | strcmp(stimInfo.data.iniInfo.type,'Calibration'))
                    fprintf('Eye Eccenticities ...\n')
                    eccentricity = fvtEccentricity(stimInfo);
                    filename = [sessionname , '_eccentricity'];
                    eval(['save ' filename ' eccentricity']);
                end
            elseif ~strcmp(stimInfo.data.iniInfo.type,'Calibration')
                fprintf('Eye Eccenticities ...\n')
                eccentricity = fvtEccentricity(stimInfo);
                filename = [sessionname , '_eccentricity'];
                eval(['save ' filename ' eccentricity']);
            end
        end
        cd ..
    end
end
status=1;