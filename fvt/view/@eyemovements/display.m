function display(em)
%eyemovements/DISPLAY Command window display of a eyemovements object
%
%   Dependencies: None.

	fprintf('\n%s =\n',inputname(1));
   fprintf('eyemovements object from %s with fields:\n',em.data.sessionname); 
   fprintf('\tdata.sessionname\n');
   fprintf('\tdata.fixation.start\n');
   fprintf('\tdata.fixation.finish\n');
   fprintf('\tdata.saccade.start\n');
   fprintf('\tdata.saccade.finish\n');
   fprintf('\tdata.saccade.max_velocity\n');
   fprintf('\tdata.drift.start\n');
   fprintf('\tdata.drift.finish\n');
   fprintf('\tnptdata\n');
   