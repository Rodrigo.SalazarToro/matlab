function [n,binedges,ni,a] = hist(obj,varargin)
%eyemovements/hist Plots histogram of data in eyemovement object
%   [N,BINEDGES,NI,A] = hist(OBJ) computes the histogram of saccade 
%   velocities computed using 10 bins. N contains the raw counts, 
%   BINEDGES are the bins used to compute the histogram and NI 
%   are the corresponding bin indices. The matrix A contains the
%   following data in rows: trial number, saccade number within the
%   trial, saccade time and saccade velocity.
%
%   hist(OBJ,'NumBins',NBINS) uses NBINS instead of 10.
%
%   hist(OBJ,'BinSize',BINSIZE) specifies the bin size
%   instead of the number of bins.

Args = struct('NumBins',10,'BinSize',0,'Fixation',0,'Durations',0);
Args.flags = {'Fixation','Durations'};
Args = getOptArgs(varargin,Args);

if(Args.Fixation)
    % compute histogram on fixation durations
elseif(Args.Durations)
    % compute histograms on saccade durations
else
	% get saccade data
	sac = obj.data.saccade;
	a = [];
	for ind = 1:length(sac)
		% get saccade velocities
		v = sac(ind).max_velocity;
		% get number of saccades in this trial
		vs = size(sac(ind).max_velocity,2);
		% save trial number and saccade number along with saccade time
		% and saccade velocity
		a = [a [repmat(ind,1,vs); 1:vs; v]];
	end
	
	% find the maximum saccade velocity
	vmax = max(a(4,:));
	
	if(Args.BinSize)
		binedges = 0:Args.BinSize: ...
			(ceil(vmax/Args.BinSize)*Args.BinSize);
	else
		% get size of bins
		binsize = vmax/Args.NumBins;
		binedges = 0:binsize:vmax;
	end
	[n,ni] = histcie(a(4,:),binedges);
end
