function obj = eyemovements(varargin)
%EYEMOVEMENTS Constructor function for the EYEMOVEMENTS class
%   EM = EYEMOVEMENTS('auto',varargin) instantiates an EYEMOVEMENTS
%   object that tells which regions of an eye signal correspond to 
%	fixations, saccades or slow drifts.
%   
%   The eye folder must be the current directory.
% 
%    
%   EM is a structure with the following fields:
%   	EM.data.sessionname
%	 	EM.data.fixation.start
%   	EM.data.fixation.finish
%   	EM.data.saccade.start
%   	EM.data.saccade.finish
%   	EM.data.drift.start
%   	EM.data.drift.finish
%
%   Dependencies: fvtGenerateSessionEyeMovements.
Args = struct('RedoLevels',0,'SaveLevels',0, ...
    'Auto',0);

[Args,varargin2] = getOptArgs(varargin,Args, ...
    'flags',{'Auto'}, ...
    'shortcuts',{'redo',{'RedoLevels',1};'save',{'SaveLevels',1}}, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'remove',{'Auto'});

if nargin==0
    % create empty object
    obj = createEmptyObject;
elseif( (nargin==1) & (isa(varargin{1},'eyemovements')) )
    obj = varargin{1};
elseif(Args.RedoLevels==0)
    % check for saved object
    dirlist = nptDir('*eyemovement.mat');
    if ~isempty(dirlist)
        fprintf('Loading %s\n',dirlist(1).name);
        try
            lastwarn('');
            % load saved object and exit
            l = load(dirlist(1).name);
            obj = l.em;
%             fprintf('Loading %s eyemovements object.\n',dirlist(1).name)
            [lastmsg, lastid] = lastwarn; 
            if ~isempty(lastmsg)
                fprintf('Older format saved so recalculating eyemovements ....\n')
                error
            end
        catch
            fprintf('Error loading object.  Now recreating new object.\n') 
            Args.SaveLevels=1;
            obj = createObject(Args,varargin2{:});
        end
    else
        obj = createObject(Args,varargin2{:});
    end
    
elseif Args.RedoLevels
    obj = createObject(Args,varargin2{:});
end





function obj = createEmptyObject
n=nptdata(0,0);
d.data.sessionname = '';
d.data.fixation = [];
d.data.saccade = [];
d.data.drift = [];
obj = class(d,'eyemovements',n);



function obj = createObject(Args,varargin)
holdAxis=1;
[d.data,status] = fvtGenerateSessionEyeMovements(varargin{:});
if status==-1
    fprintf('Error in GenerateSessionEyeMovements')
    d.data=[];
end
n = nptdata( size(d.data.saccade,2),holdAxis);
obj = class(d,'eyemovements',n);
if(Args.SaveLevels>0)
    fprintf('Saving eyemovement object...\n');
    em = obj;
    filename = [d.data.sessionname '_eyemovement.mat'];
    save(filename, 'em')
end



