% ijoc2index.m - converts from sparse noise params (i j o c) to index
%
% function indices = ijoc2index(seq,num_colors,num_or,rows,cols)
%
% seq:          matrix of sparse noise params (each row is a 4-tuple: i j o c)
% num_colors:   number of colors
% num_or:       number of orientations
% rows:         number of rows
% cols:         number of columns
%
% indices:      array of sparse noise indices in same order as seq
%
% Conversion from i j o c to index is given by
%
%   index = ((c*num_or + o)*cols + j)*rows + i
%

function indices = ijoc2index(seq,num_colors,num_or,rows,cols)

M=rows*cols*num_or*num_colors + 1;

i=seq(:,1);
j=seq(:,2);
if size(seq, 2) == 3
    k = zeros(size(seq, 1), 1);
    l = seq(:, 3);
else
    k = seq(:, 3);
    l = seq(:, 4);
end

indices = ((l*num_or + k)*cols + j)*rows + i;

ind=find(i<0 | i>=rows | j<0 | j>=cols);
indices(ind)=M-1;