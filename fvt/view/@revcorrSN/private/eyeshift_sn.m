% eyeshift_sn.m - shifts sparse noise position params according to eye position
%
% function seq_shifted = eyeshift_sn(seq,iniInfo,eyepos)
%
% seq:          array of sparse noise params
% iniInfo:    init info structure
% eyepos:       2 X N array of eye positions
%
% length of seq array must be less than or equal to length of eyepos array

function seq_shifted = eyeshift_sn(seq,iniInfo,eyepos)

n_eye=size(eyepos,2);
if n_eye<size(seq,1)
    fprintf('eyeshift_sn: length of eyepos array is less than seq array\n')
    return
end

fix=[StimInfo.iniInfo.fixation_location_x   StimInfo.iniInfo.fixation_location_y]';
eyepos=eyepos-fix*ones(1,n_eye);    % eye position centered about fixation
eyepos(2,:)=-eyepos(2,:);   % flip y direction (screen coordinates go down)

rows=iniInfo.rows;
cols=iniInfo.cols;
num_or=iniInfo.num_orientations;
angle=iniInfo.bar_orientation;

grid_center_i=floor(rows/2);
grid_center_j=floor(cols/2);

grid_x_size=iniInfo.grid_x_size;
grid_y_size=iniInfo.grid_y_size;

grid_x_step=grid_x_size/cols;
grid_y_step=grid_y_size/cols;

% grid points scaled to screen coordinates, relative to center of grid, with y going up
points=[grid_x_step*(seq(:,2)-grid_center_j) -grid_y_step*(seq(:,1)-grid_center_i)]';

for k=0:num_or-1
    theta=(90-angle(k+1))*pi/180;
    % rotation matrix for clockwise rotation by theta
    rot=[cos(theta) sin(theta); -sin(theta) cos(theta)];
    i=find(seq(:,3)==k);
    points(:,i)=rot*points(:,i);    % rotate points according to orientation of grid
    points(:,i)=points(:,i)-eyepos(:,i);    % shift according to eye position
    points(:,i)=rot'*points(:,i);   % rotate back
end

% requantize into rows and columns of grid
seq_shifted(:,1)=floor(-points(2,:)'/grid_y_step + grid_center_i);
seq_shifted(:,2)=floor(points(1,:)'/grid_x_step + grid_center_j);

seq_shifted(:,3:4)=seq(:,3:4);
