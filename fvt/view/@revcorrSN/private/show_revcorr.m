% show_revcorr_sn.m - static display of revcorr_sn as sequence of frames
%
% function show_revcorr_sn(R,init_info,bins_per_step,mode)
%
%   R:              revcorr array (from revcorr_sn)
%   init_info:      init_info structure
%   bins_per_step:  number of time bins to average for each step displayed
%   mode:           optional shading mode: 'flat' (default) or 'interp'
%

function show_revcorr_sn(R,iniInfo,bins_per_step,mode)

if ~exist('mode','var')
    mode='flat';
end

%R=rc.R(:,:,cluster);

% revcorr dimensions
[M N]=size(R);
%R=R(1:M-1,:);   % get rid of last row, which is outside box
%M=M-1;

num_steps=floor(N/bins_per_step);

% normalize R matrix
R=R/max(R(:));  

% get init_info variables
rows=iniInfo.grid_rows;
cols=iniInfo.grid_cols;
nrc=rows*cols;
num_or=iniInfo.num_orientations;
angle=iniInfo.bar_orientation;
num_colors=iniInfo.obj_colors;

% revcorr map
map=zeros(rows+1,cols+1);

% center pos
cx=floor(cols/2);
cy=rows-1-floor(rows/2);

% axis size
sz=1.2*max([rows cols]);

% loop over subplots
for c=0:num_colors-1		%loop over color groups
    
 
    for k=0:num_or-1		%loop over orientation groups
        group=c*num_or+k;
        i=group*nrc + [1:nrc];
        
        for t=1:num_steps

            j=(t-1)*bins_per_step+[1:bins_per_step];
            r=reshape(mean(R(i,j),2),rows,cols);
            
            subplot(num_colors*num_or, num_steps, group*num_steps+t) 

            map(1:rows,1:cols)=r(rows:-1:1,:);	%flip y dimension
            if strcmp(mode,'flat')
                h=pcolor([0:cols]-0.5,[0:rows]-0.5,map);
            else
                h=pcolor([0:cols],[0:rows],map);
            end
            %axis image off
            axis([cx-sz/2 cx+(sz)/2 cy-(sz)/2 cy+sz/2])
            caxis([0 1])
            shading(mode)
            rotate(h,[0 90],angle(k+1)-90,[cx cy 1])
           
        end
        
    end 
end
%title = ([init_info.sessionname '  Group: ' init_info.groupname '  Cluster: ' num2str(cluster)]);
%h=gcf;
%h=findobj(h,'Tag','Title_box');
%set(h,'String',title)

