function obj = revcorrSN(groupNum , varargin)
%revcorrSN Constructor function for revcorrSN object
%   
%   OBJ = revcorrSN(groupNum) instantiates a revcorr object for
%   groupNum in the current session directory
%
%   Optional arguments that can also be passed:
%      redo             ie. 'redo',1
%      saveobj          ie. 'saveobj',1
%
%   The following optional arguments for calculating
%   the Revcorr map can also be passed in:
%      window_size      ie. 'window_size',200     
%      bin_size         ie. 'bin_size',2     
%   but the defaults are 150 milliseconds and 1 millisecond
%
%   The object contains the following fields:
%       RC.sessionname
%	 	RC.groupname
%       RC.sessionnname
%   	RC.clusters
%   	RC.R
%       RC.P
%       RC.Pspike
%       RC.rows
%       RC.cols
%       RC.orientation_angles
%       RC.num_colors
%		RC.bin_size
%		RC.window_size

%
%   Inherited from: @eyestarget.
%
%	Dependencies: eyestarget, removeargs.

redo=0;
saveobj=0;
window_size = 120;  %150 milliseconds
extract_varargin

if nargin==0
    % create empty object
    %r.P=[];
    
    r.data.sessionname = '';
    r.data.groupname=0;
    r.data.clusters=0;
    r.data.R=[];
    r.data.Pspike=[];
    r.data.window_size=0;
    r.data.bin_size=0;
    %r.nptdata=[];
    %r.stiminfo=[];
    
    n = nptdata; 
    stimInfo=stiminfo;
    obj = class(r,'revcorrSN',n,stimInfo);
    
elseif ((nargin==1) & (isa(groupNum,'revcorrSN')))
    obj = groupNum;
    
else
    %try to create one
    n=nptdata;    
    stimInfo = stiminfo('auto',1,'saveobj',1);
    r = GenerateRevcorrSN(groupNum,stimInfo,window_size);
    obj = class(r,'revcorrSN',n,stimInfo);
    if saveobj
        fprintf('Saving revcorrSN object...\n');
        rc = obj;
        filename = [rc.sessionname 'g' num2strpad(rc.groupname,4) '_revcorrSN']; 
        save(filename,'rc')
    end
end
