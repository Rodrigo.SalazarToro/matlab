%fvtAddPath
%script to setup the correct paths for this toolbox
%you must first add the fvt path 
%run this script
%then save the path through matlab's pulldown menu
%

basedir = 'c:\matlabr11\work\fvt';

addpath (basedir, ...
				[basedir filesep 'BatchProcessor'], ...
            [basedir filesep 'Eye'], ...
            [basedir filesep 'LFP'], ...
            [basedir filesep 'revcorr_sn'], ...
            [basedir filesep 'Spikes'], ...
            [basedir filesep 'view']);
