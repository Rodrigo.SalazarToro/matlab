% FVT- Free Viewing Toolbox
% 
% FVT is a Matlab programming toolbox originally 
% created to perform post preprocessing on monkey 
% data but has been enlarged to process both monkey and cat data.  
% 
% The toolbox has two main goals: 
% 1.  Process Data
% 2.  View Data
% 
% A third 
% A main gui function callled fvtmenu can be used
% to either view or processdata.
% 
% The toolbox architecture trys to use classes for
% the different data types. All of the class 
% directories are located in the view directory. 
% 
% There are two main data types which have their own classes:
% 1.  EyeMovements
% 2.  RevCorr 
% 
% A third class that is stiminfo which contains:
% 1.  iniInfo
% 2.  framePoints
% 3.  extrasyncs
% 
% Ideally each class should be written to take
% advantage of nptdata's graphing and batchprocessing
% cababilities.  So they should inherit from the nptdata
% class. They should contain a constructor, subsref and 
% plot function.  Additional files could be display, loadobj
% or processsession files.
% 


  

