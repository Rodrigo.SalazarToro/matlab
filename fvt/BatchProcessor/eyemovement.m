function varargout = eyemovement(varargin)
% EYEMOVEMENT M-file for eyemovement.fig
%      EYEMOVEMENT, by itself, creates a new EYEMOVEMENT or raises the existing
%      singleton*.
%
%      H = EYEMOVEMENT returns the handle to a new EYEMOVEMENT or the handle to
%      the existing singleton*.
%
%      EYEMOVEMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EYEMOVEMENT.M with the given input arguments.
%
%      EYEMOVEMENT('Property','Value',...) creates a new EYEMOVEMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before eyemovement_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to eyemovement_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help eyemovement

% Last Modified by GUIDE v2.5 22-Sep-2003 15:00:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @eyemovement_OpeningFcn, ...
                   'gui_OutputFcn',  @eyemovement_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before eyemovement is made visible.
function eyemovement_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to eyemovement (see VARARGIN)

% Choose default command line output for eyemovement
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes eyemovement wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = eyemovement_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox15.
function checkbox15_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox15


% --- Executes on button press in EMRedo.
function EMRedo_Callback(hObject, eventdata, handles)
% hObject    handle to EMRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMRedo


% --- Executes on button press in EMDurations.
function EMDurations_Callback(hObject, eventdata, handles)
% hObject    handle to EMDurations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMDurations


% --- Executes on button press in EMVelocities.
function EMVelocities_Callback(hObject, eventdata, handles)
% hObject    handle to EMVelocities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMVelocities


% --- Executes on button press in EMPositionRange.
function EMPositionRange_Callback(hObject, eventdata, handles)
% hObject    handle to EMPositionRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMPositionRange


% --- Executes on button press in EMSpectra.
function EMSpectra_Callback(hObject, eventdata, handles)
% hObject    handle to EMSpectra (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMSpectra


% --- Executes on button press in EMEccentricities.
function EMEccentricities_Callback(hObject, eventdata, handles)
% hObject    handle to EMEccentricities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMEccentricities


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%need to pass parameters to main waindow (batchProcessor)
EMstring = EMparameters(handles);
BPhandle = findobj('Name','bpGUI2');
UD = get(BPhandle,'UserData');
UD.EM.EMstring = EMstring;
set(BPhandle,'UserData',UD)
%we should also save this info so it can be displayed if the window is
%opened again
p=which('eyemovement.fig');
[p,n,e]=fileparts(p);
saveas(gcbf,[p filesep 'eyemovement.fig'])
closereq

function EMstring = EMparameters(handles)   
    EMredo = get(handles.EMRedo,'Value');
    EMpositionranges = get(handles.EMPositionRange,'Value');
    EMvelocities = get(handles.EMVelocities,'Value');
    EMdurations = get(handles.EMDurations,'Value');
    EMspectra = get(handles.EMSpectra,'Value');
    EMeccentricities = get(handles.EMEccentricities,'Value');
    EMstring = ['''redoValue'',' num2str(EMredo) ',''positionrangesValue'',' num2str(EMpositionranges) ...
            ',''velocitiesValue'',' num2str(EMvelocities) ',''EMdurationsValue'',' num2str(EMdurations) ...
            ',''spectraValue'',' num2str(EMspectra) ',''eccentricitiesValue'',' num2str(EMeccentricities) ];
