function varargout = preprocessing(varargin)
% PREPROCESSING M-file for preprocessing.fig
%      PREPROCESSING, by itself, creates a new PREPROCESSING or raises the existing
%      singleton*.
%
%      H = PREPROCESSING returns the handle to a new PREPROCESSING or the handle to
%      the existing singleton*.
%
%      PREPROCESSING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PREPROCESSING.M with the given input arguments.
%
%      PREPROCESSING('Property','Value',...) creates a new PREPROCESSING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before preprocessing_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to preprocessing_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help preprocessing

% Last Modified by GUIDE v2.5 07-Dec-2006 06:07:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @preprocessing_OpeningFcn, ...
                   'gui_OutputFcn',  @preprocessing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before preprocessing is made visible.
function preprocessing_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to preprocessing (see VARARGIN)

% Choose default command line output for preprocessing
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes preprocessing wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = preprocessing_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%need to pass parameters to main waindow (batchProcessor)
PPstring = PPparameters(handles);
BPhandle = findobj('Name','bpGUI2');
UD = get(BPhandle,'UserData');
UD.PP.PPstring = PPstring;
set(BPhandle,'UserData',UD)
%we should also save this info so it can be displayed if the window is
%opened again
p=which('preprocessing.fig');
[p,n,e]=fileparts(p);
saveas(gcbf,[p filesep 'preprocessing.fig'])
closereq



function PPstring = PPparameters(handles)
PPredo = get(handles.PPRedo,'Value');
PPchunksize = get(handles.PPChunkSize,'String');
PPextraction = get(handles.PPExtraction,'Value');
PPeye = get(handles.PPEye,'Value');
PPhighpass = get(handles.PPHighpass,'Value');
PPhighlow = get(handles.highlow,'String');
PPhighhigh = get(handles.highhigh,'String');
PPlowpass = get(handles.PPLowpass,'Value');
PPlowlow = get(handles.lowlow,'String');
PPlowhigh = get(handles.lowhigh,'String');
PPtiming = get(handles.PPTiming,'Value');
PPextractthreshold = get(handles.PPExtractThreshold,'String');
PPsnrextractcutoff = get(handles.PPSNRExtractCutoff,'String');
PPsnrsortcutoff = get(handles.PPSNRSortCutoff,'String');
if isempty(PPchunksize)
    PPchunksize = ''' ''';
end
if isempty(PPextractthreshold)
    PPextractthreshold = ''' ''';
end
if isempty(PPsnrextractcutoff)
    PPsnrextractcutoff = ''' ''';
end
if isempty(PPsnrsortcutoff)
    PPsnrsortcutoff = ''' ''';
end
PPgroups = get(handles.PPGroups,'String');
if isempty(PPgroups)
    PPgroups=''' ''';
end
PPnomove = get(handles.PPNoMove,'Value');
PPkk = get(handles.PPKK,'Value');
PPbb = get(handles.PPBB,'Value');
if PPkk
    sort_algo = '''KK''';
elseif PPbb
    sort_algo = '''BB''';
else 
    sort_algo = '''none''';
end

PPstring = ['''redoValue'','  num2str(PPredo) ',''extractionValue'',' num2str(PPextraction) ...
        ',''eyeValue'',' num2str(PPeye) ',''highpassValue'',' num2str(PPhighpass) ...
        ',''highpasslow'',' num2str(PPhighlow)   ',''highpasshigh'',' num2str(PPhighhigh) ...
        ',''lowpassValue'',' num2str(PPlowpass) ',''lowpasslow'',' num2str(PPlowlow) ...
        ',''lowpasshigh'',' num2str(PPlowhigh) ',''timingValue'',' num2str(PPtiming) ...
        ',''threshold'',' PPextractthreshold ',''SNRExtractionCutoff'',' PPsnrextractcutoff ...
        ',''SNRSortCutoff'',' PPsnrsortcutoff   ',''groups'',' PPgroups ...
        ',''chunkSize'',' PPchunksize ...
        ',''nomoveValue'',' num2str(PPnomove) ',''sort_algo'',' sort_algo];














% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in PPEye.
function PPEye_Callback(hObject, eventdata, handles)
% hObject    handle to PPEye (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPEye


% --- Executes on button press in PPRedo.
function PPRedo_Callback(hObject, eventdata, handles)
% hObject    handle to PPRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPRedo


% --- Executes on button press in PPExtraction.
function PPExtraction_Callback(hObject, eventdata, handles)
% hObject    handle to PPExtraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPExtraction


% --- Executes on button press in PPLowpass.
function PPLowpass_Callback(hObject, eventdata, handles)
% hObject    handle to PPLowpass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPLowpass


% --- Executes on button press in PPTiming.
function PPTiming_Callback(hObject, eventdata, handles)
% hObject    handle to PPTiming (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPTiming


% --- Executes on button press in PPHighpass.
function PPHighpass_Callback(hObject, eventdata, handles)
% hObject    handle to PPHighpass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPHighpass


% --- Executes during object creation, after setting all properties.
function PPExtractThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPExtractThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PPExtractThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to PPExtractThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPExtractThreshold as text
%        str2double(get(hObject,'String')) returns contents of PPExtractThreshold as a double


% --- Executes on button press in PPKK.
function PPKK_Callback(hObject, eventdata, handles)
% hObject    handle to PPKK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPKK


% --- Executes on button press in PPBB.
function PPBB_Callback(hObject, eventdata, handles)
% hObject    handle to PPBB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPBB


% --- Executes during object creation, after setting all properties.
function PPGroups_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPGroups (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PPGroups_Callback(hObject, eventdata, handles)
% hObject    handle to PPGroups (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPGroups as text
%        str2double(get(hObject,'String')) returns contents of PPGroups as a double


% --- Executes on button press in PPNoMove.
function PPNoMove_Callback(hObject, eventdata, handles)
% hObject    handle to PPNoMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPNoMove


% --- Executes during object creation, after setting all properties.
function lowlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function lowlow_Callback(hObject, eventdata, handles)
% hObject    handle to lowlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowlow as text
%        str2double(get(hObject,'String')) returns contents of lowlow as a double


% --- Executes during object creation, after setting all properties.
function highlow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to highlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function highlow_Callback(hObject, eventdata, handles)
% hObject    handle to highlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of highlow as text
%        str2double(get(hObject,'String')) returns contents of highlow as a double


% --- Executes during object creation, after setting all properties.
function lowhigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function lowhigh_Callback(hObject, eventdata, handles)
% hObject    handle to lowhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowhigh as text
%        str2double(get(hObject,'String')) returns contents of lowhigh as a double


% --- Executes during object creation, after setting all properties.
function highhigh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to highhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function highhigh_Callback(hObject, eventdata, handles)
% hObject    handle to highhigh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of highhigh as text
%        str2double(get(hObject,'String')) returns contents of highhigh as a double




function PPSNRExtractionThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to PPSNRExtractionThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPSNRExtractionThreshold as text
%        str2double(get(hObject,'String')) returns contents of PPSNRExtractionThreshold as a double


% --- Executes during object creation, after setting all properties.
function PPSNRExtractionThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPSNRExtractionThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PPChunkSize_Callback(hObject, eventdata, handles)
% hObject    handle to PPChunkSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPChunkSize as text
%        str2double(get(hObject,'String')) returns contents of PPChunkSize as a double


% --- Executes during object creation, after setting all properties.
function PPChunkSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPChunkSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


