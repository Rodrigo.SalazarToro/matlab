function Execute_Callback
%calls the batch processor script with the revelant 
%algorithms.


%get directory
dir_path = get(findobj('Tag','DirText'),'String');
%what type of directory - name, date, session
%data must have the correct directory structure 
%ie.	etc..->data->name->date->session->etc...
[p,n] = fileparts(dir_path);
[p,n] = fileparts(p);
if strcmp(lower(n),'data')
   dirtype = 'name';
else
   [p,n] = fileparts(p);
   if strcmp(lower(n),'data')
      dirtype = 'date';
   else
      [p,n] = fileparts(p);
      if strcmp(lower(n),'data')
         dirtype = 'session';
      end
   end
end

%get algorithms from gui
algorithms = get(findobj('Tag','AlgoListbox'),'String');
values = get(findobj('Tag','AlgoListbox'),'Value');
arguments = algorithms(values);

%call batch processor scripts
cd (dir_path)

switch dirtype
case 'name'
   status = fvtBatchProcessor(arguments{:});
   msgbox(status,'FVT Batch Processor Log')
case  'date'
   status = fvtProcessDay(arguments{:});
   msgbox(status,'FVT Batch Processor Log')
case 'session'
   fvtProcessSession(arguments{:});
end


