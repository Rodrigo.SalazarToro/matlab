function load_DirListbox(dir_path)
%function load_DirListbox(dir_path)
%
%this function loads a directory listbox
%it can be reused but the listbox must be named DirListbox
%and the edittextbox must be named DirEditText.

cd (dir_path) % Change to the specified directory
dir_struct = dir(dir_path); % List contents of directory
[sorted_names,sorted_index] = sortrows({dir_struct.name}'); % Sort names
userdata.file_names = sorted_names; % Save the sorted names
userdata.is_dir = [dir_struct.isdir]; % Save names of directories
userdata.sorted_index = [sorted_index]; % Save sorted index values
set(gcf,'UserData',userdata) % Save the handles structure
set(findobj('Tag','DirListbox'),'String',userdata.file_names,'Value',1) % Load listbox
set(findobj('Tag','DirText'),'String',pwd) % Display current directory
AlgoListbox_Load





