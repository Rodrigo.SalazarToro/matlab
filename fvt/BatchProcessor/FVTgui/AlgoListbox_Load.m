function AlgoListbox_Load
%loads the Algorithm Listbox with the revelant 
%algorithms.

%get directory
dir_path = get(findobj('Tag','DirText'),'String');
%what type of directory - name, date, session
%data must have the correct directory structure 
%ie.	etc..->data->name->date->session->etc...
[p,n] = fileparts(dir_path);
[p,n] = fileparts(p);

dirtype=[];
if strcmp(lower(n),'data')
   dirtype = 'name';
else
   [p,n] = fileparts(p);
   if strcmp(lower(n),'data')
      dirtype = 'date';
   else
      [p,n] = fileparts(p);
      if strcmp(lower(n),'data')
         dirtype = 'session';
      end
   end
end

if strcmp(dirtype,'name') | strcmp(dirtype,'date') | strcmp(dirtype,'session')
   handle = findobj('Tag','AlgoListbox');
   algorithms = {'eye','spike','lfp','redo','npt'};
   set(handle,'String',algorithms)
else
   handle = findobj('Tag','AlgoListbox');
   set(handle,'String',{})
end
