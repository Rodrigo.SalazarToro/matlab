%function varargout = listbox1_Callback(h,eventdata,handles,varargin)
function varargout = DirListbox_Callback(varargin)

userdata =get(gcbf,'UserData'); 

if strcmp(get(gcbf,'SelectionType'),'open') % If double click
    index_selected = get(findobj('Tag','DirListbox'),'Value');
    file_list = get(findobj('Tag','DirListbox'),'String');
    filename = file_list{index_selected}; % Item selected in list box
    if  userdata.is_dir(userdata.sorted_index(index_selected)) % If directory
        cd (filename)
        load_DirListbox(pwd) % Load list box with new directory
    else
        [path,name,ext,ver] = fileparts(filename);
        switch ext
        case '.fig' 
            guide (filename) % Open FIG-file with guide command
        otherwise 
           try
              %!!!!!!!!!!!!!!need to make an open command!!!!!!!!!!
                 %open(filename) % Use open for other file types
            catch
                 errordlg(lasterr,'File Type Error','modal')
            end
        end
    end
end

