function varargout = spiketrain(varargin)
% SPIKETRAIN M-file for spiketrain.fig
%      SPIKETRAIN, by itself, creates a new SPIKETRAIN or raises the existing
%      singleton*.
%
%      H = SPIKETRAIN returns the handle to a new SPIKETRAIN or the handle to
%      the existing singleton*.
%
%      SPIKETRAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPIKETRAIN.M with the given input arguments.
%
%      SPIKETRAIN('Property','Value',...) creates a new SPIKETRAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spiketrain_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spiketrain_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spiketrain

% Last Modified by GUIDE v2.5 04-Jan-2005 10:29:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @spiketrain_OpeningFcn, ...
                   'gui_OutputFcn',  @spiketrain_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before spiketrain is made visible.
function spiketrain_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to spiketrain (see VARARGIN)

% Choose default command line output for spiketrain
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes spiketrain wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = spiketrain_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in STRedo.
function STRedo_Callback(hObject, eventdata, handles)
% hObject    handle to STRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STRedo


% --- Executes on button press in STEyeEventSpikeHistograms.
function STEyeEventSpikeHistograms_Callback(hObject, eventdata, handles)
% hObject    handle to STEyeEventSpikeHistograms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STEyeEventSpikeHistograms


% --- Executes on button press in STFiringRate.
function STFiringRate_Callback(hObject, eventdata, handles)
% hObject    handle to STFiringRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STFiringRate


% --- Executes on button press in STFiringRateCorrelations.
function STFiringRateCorrelations_Callback(hObject, eventdata, handles)
% hObject    handle to STFiringRateCorrelations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STFiringRateCorrelations


% --- Executes on button press in STISI.
function STISI_Callback(hObject, eventdata, handles)
% hObject    handle to STISI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STISI

% --- Executes on button press in STSpikeTrains.
function STSpikeTrains_Callback(hObject, eventdata, handles)
% hObject    handle to STSpikeTrains (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STSpikeTrains


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%need to pass parameters to main waindow (batchProcessor)
ST = STparameters(handles);
% STstring = STparameters(handles);
BPhandle = findobj('Name','bpGUI2');
UD = get(BPhandle,'UserData');
UD.ST = ST;
set(BPhandle,'UserData',UD)
%we should also save this info so it can be displayed if the window is
%opened again
p=which('spiketrain.fig');
[p,n,e]=fileparts(p);
saveas(gcbf,[p filesep 'spiketrain.fig'])
closereq

function ST = STparameters(handles)   
%function STstring = STparameters(handles)   
 
    ST.EyeEventSpikeHistograms.value = get(handles.STEyeEventSpikeHistograms,'Value');
    ST.ISI.value = get(handles.STISI,'Value');
    ST.FiringRateCorrelations.value = get(handles.STFiringRateCorrelations,'Value');
    ST.EyeFiringRate.value = get(handles.STEyeFiringRate,'Value');
    ST.adjSpikes.value = get(handles.STAdjSpikeTrain,'Value');
    ST.adjSpikesPSTH.value = get(handles.STpsth,'Value');
    ST.adjSpikesPSTH.Binsize = get(handles.STpsthBinsize,'String');
    ST.adjSpikesPSTH.Overlap = get(handles.STpsthOverlap,'String');
    ST.FiringRate.value = get(handles.STFiringRate,'Value');
    ST.FiringRate.Binsize = get(handles.STfiringrateBinsize,'String');
    ST.jointevents.value = get(handles.STjointEvents,'Value');
    ST.events.value = get(handles.STevents,'Value');
    ST.events.Binsize = get(handles.STeventsBinsize,'String');
    if get(handles.percentile,'Value')
        ST.events.ThresholdType = '''percentile''';
    else
        ST.events.ThresholdType = '''randomize''';
    end
    ST.events.Upper = get(handles.STEventsUpper,'String');
    ST.events.Lower = get(handles.STEventsLower,'String');
    ST.sparsity.value = get(handles.STsparsity,'Value');
    ST.responsiveness.value = get(handles.STresponsiveness,'Value');
    ST.unitaryEvents.value = get(handles.STUnitaryEvent,'Value');

    redoValue = num2str(get(handles.STRedo,'Value'));
    ST.EyeEventSpikeHistograms.RedoLevels = redoValue;
    ST.ISI.RedoLevels = redoValue;
    ST.FiringRateCorrelations.RedoLevels = redoValue;
    ST.adjSpikes.RedoLevels =redoValue;
    ST.FiringRate.RedoLevels = redoValue;
    ST.sparsity.RedoLevels = redoValue;
    ST.responsiveness.RedoLevels = redoValue;
    ST.events.RedoLevels = redoValue;
    ST.jointevents.RedoLevels = redoValue;
    ST.adjSpikesPSTH.RedoLevels = redoValue;
    ST.unitaryEvents.RedoLevels = redoValue;
    
    
    % --- Executes on button press in STpsth.
function STpsth_Callback(hObject, eventdata, handles)
% hObject    handle to STpsth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STpsth


% --- Executes on button press in STAdjSpikeTrain.
function STAdjSpikeTrain_Callback(hObject, eventdata, handles)
% hObject    handle to STAdjSpikeTrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STAdjSpikeTrain


% --- Executes during object creation, after setting all properties.
function STpsthBinsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STpsthBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function STpsthBinsize_Callback(hObject, eventdata, handles)
% hObject    handle to STpsthBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STpsthBinsize as text
%        str2double(get(hObject,'String')) returns contents of STpsthBinsize as a double


% --- Executes during object creation, after setting all properties.
function STpsthOverlap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STpsthOverlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function STpsthOverlap_Callback(hObject, eventdata, handles)
% hObject    handle to STpsthOverlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STpsthOverlap as text
%        str2double(get(hObject,'String')) returns contents of STpsthOverlap as a double




% --- Executes on button press in STsparsity.
function STsparsity_Callback(hObject, eventdata, handles)
% hObject    handle to STsparsity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STsparsity


% --- Executes on button press in STresponsiveness.
function STresponsiveness_Callback(hObject, eventdata, handles)
% hObject    handle to STresponsiveness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STresponsiveness


% --- Executes on button press in checkbox15.
function checkbox15_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox15


% --- Executes on button press in STUnitaryEvent.
function STUnitaryEvent_Callback(hObject, eventdata, handles)
% hObject    handle to STUnitaryEvent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STUnitaryEvent


% --- Executes on button press in STfiringrateprob.
function STfiringrateprob_Callback(hObject, eventdata, handles)
% hObject    handle to STfiringrateprob (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STfiringrateprob


% --- Executes on button press in STEyeFiringRate.
function STEyeFiringRate_Callback(hObject, eventdata, handles)
% hObject    handle to STEyeFiringRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STEyeFiringRate


% --- Executes during object creation, after setting all properties.
function STfiringrateBinsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STfiringrateBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function STfiringrateBinsize_Callback(hObject, eventdata, handles)
% hObject    handle to STfiringrateBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STfiringrateBinsize as text
%        str2double(get(hObject,'String')) returns contents of STfiringrateBinsize as a double


% --- Executes on button press in STevents.
function STevents_Callback(hObject, eventdata, handles)
% hObject    handle to STevents (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STevents


% --- Executes during object creation, after setting all properties.
function STeventsBinsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STeventsBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function STeventsBinsize_Callback(hObject, eventdata, handles)
% hObject    handle to STeventsBinsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STeventsBinsize as text
%        str2double(get(hObject,'String')) returns contents of STeventsBinsize as a double


% --- Executes during object creation, after setting all properties.
function STEventsUpper_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STEventsUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function STEventsUpper_Callback(hObject, eventdata, handles)
% hObject    handle to STEventsUpper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STEventsUpper as text
%        str2double(get(hObject,'String')) returns contents of STEventsUpper as a double


% --- Executes on button press in STJointEvents.
function STJointEvents_Callback(hObject, eventdata, handles)
% hObject    handle to STJointEvents (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STJointEvents


% --- Executes on button press in STjointEvents.
function STjointEvents_Callback(hObject, eventdata, handles)
% hObject    handle to STjointEvents (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STjointEvents




% --- Executes on button press in randomized.
function randomized_Callback(hObject, eventdata, handles)
% hObject    handle to randomized (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')==1
    set(handles.percentile,'Value',0)
    set(handles.STEventsUpper,'Enable','off')
    set(handles.STEventsLower,'Enable','off')
end

% --- Executes on button press in percentile.
function percentile_Callback(hObject, eventdata, handles)
% hObject    handle to percentile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')==1
    set(handles.randomized,'Value',0)
    set(handles.STEventsUpper,'Enable','on')
    set(handles.STEventsLower,'Enable','on')
end



function STEventsLower_Callback(hObject, eventdata, handles)
% hObject    handle to STEventsLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of STEventsLower as text
%        str2double(get(hObject,'String')) returns contents of STEventsLower as a double


% --- Executes during object creation, after setting all properties.
function STEventsLower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to STEventsLower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


