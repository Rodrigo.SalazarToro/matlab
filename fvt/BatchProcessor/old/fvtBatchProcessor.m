function status = fvtBatchProcessor (varargin)

%
%function fvtBatchProcessor
%This function starts the FVT Batch Processor.  
%
%NPT "preprocessing" must be completed before 
%FVT processing can take place.
%
%The pwd must be the animal name directory.
%
%"fvtskip.txt" is used to skip the processing on 
%a day or a session.  
%
%arguments can be used to tell what types of algorithms to
%run on the data.
%'redo' argument 


delete('diary.txt')
diary ('diary.txt')
diary on



status={'FVT BatchProcessor Log'};

dirlist=nptDir;
for i=1:size(dirlist,1)		%loop over days
   if dirlist(i).isdir
      fprintf(['Processing Day ' dirlist(i).name '\n']);
      cd (dirlist(i).name)
      daystatus = fvtProcessDay(varargin{:});
      status = cellstr(strvcat(char(status),['Day ' dirlist(i).name]));
      status = cellstr(strvcat(char(status),char(daystatus)));
      cd ..
   end
end


diary off




 