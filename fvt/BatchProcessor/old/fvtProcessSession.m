function fvtProcessSession(varargin)
%
%function fvtProcessSession
%This function starts all the FVT processing for a session.
%
%NPT "preprocessing" must be completed before 
%FVT processing can take place.
%
%The pwd must be the correct session folder.
%
%"fvtskip.txt" is used to skip the processing on 
%a day or a session.  


if ~isempty(varargin) 
   num_args = nargin;
   redo=0;
   npt=0;
   eyeflag=0;
   spikeflag=0;
   lfpflag=0;
   for i=1:num_args
      switch varargin{i}
         % if passed redo argument, ignore the marker file 
         % fvtprocessedsession.txt
      case('redo')
         redo=1;
      case('npt')
         npt=1;
      case('eye')
         eyeflag=1;
      case('spike')
         spikeflag=1;
      case('lfp')
         lfpflag=1;
      end
   end  
   % check if redo was the only argument, in which case
   % we need to redo everything
   if (redo==1 & num_args==1) | (npt==1 & num_args==1) | (redo==1 & npt==1 & num_args==2)
      eyeflag=1;
      spikeflag=1;
      lfpflag=1;
   end
else 
   redo=0;
   npt=0;
end 

% should we skip this session
marker = nptDir('*skip.txt');

%has this day been preprocessed?
nptmarker = [marker nptDir('processedsession.txt')];
if isempty(nptmarker) & npt
   fprintf('FVT Eye analysis will not be performed because the entire day must be preprocessed to include the calibration sessions.\n')
   ProcessSession
end

% check for fvtprocessedsession.txt unless redo is 1
if redo==0
   marker=[marker nptDir('fvtprocessedsession.txt')];
end

if isempty(marker)
   
   %determine what type of stimulus was shown for this session from the init file
   %and create/read a stimInfo object in the current directory
   dirlist = nptDir('*stiminfo.mat');
   if ~isempty(dirlist)
       load(dirlist.name)
   else
       stimInfo = stiminfo('auto',1,'saveobj',1)  
   end
   
   switch(stimInfo.iniInfo.type)
   case('Movie')	%movie or still image
      
      dirlist = nptDir;
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if eyeflag
            %do eye processing first
            if strcmp(dirlist(i).name,'eye') & dirlist(i).isdir
               cd (dirlist(i).name)
               eye_dir = nptPWD;
               
               fprintf('Generating Session Eye Movements ...\n')
               eye = eyemovements(1);	%argument means nothing just to differentiate btween null constructor
               filename = [sessionname , '_eye'];
               eval(['save ' filename ' eye'])
               
               bin_length = [10 5 10];
               fprintf('Eye Duration Histograms ...\n')
               duration_histograms = fvtEyeDurationHistograms(eye);
               filename = [sessionname , '_duration'];
               eval(['save ' filename ' duration_histograms'])
               
               fprintf('Eye Position Ranges ...\n')
               position_range = fvtEyePositionRangeHistogram(eye);
               filename = [sessionname , '_positionrange'];
               eval(['save ' filename ' position_range'])
               
               fprintf('Eye Power Spectrum ...\n')
               power_spectrum = fvtEyePowerSpectrum(eye);
               filename = [sessionname , '_powerspectrum'];
               eval(['save ' filename ' power_spectrum'])
               
               fprintf('Eye Velocity Histograms ...\n')
               velocity = fvtEyeVelocityHistogram(eye);
               filename = [sessionname , '_velocity'];
               eval(['save ' filename ' velocity']);
               
               cd ..
            end %if eye folder exists
         end   %if eyeflag
      end	%loop over folders
      
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if spikeflag
            if strcmp(dirlist(i).name,'sort') & dirlist(i).isdir
               cd (dirlist(i).name)
               sort_dir = nptPWD;
               
               cutdirlist = nptDir('*.cut');		
               for group=1:size(cutdirlist,1)				%loop over groups (electrode channels or tetrodes)
                  
                  groupname = cutdirlist(group).name(1:length(cutdirlist(group).name)-5);
                  
                  fprintf('Generating Session Spike Trains for Group # %s ...\n',groupname)
                  spike = ispikes(groupname,1);
                  filename = [sessionname 'G' groupname '_spike' ];
                  eval(['save ' filename ' spike'])
                  
                  fprintf('Generating ISIs for Group # %s ...\n',groupname)
                  ISI = ISIhist(spike);
                  filename = [sessionname 'G' groupname '_ISI'];
                  eval(['save ' filename ' ISI'])
                  
                  
                  if eyeflag
                     fprintf('Receptive Field Spike Train ...\n')
                     cd (eye_dir)
                     RF=CreateRFspikes(eye,spike,stimInfo.iniInfo);
                     RF = ispikes(RF);
                     cd (sort_dir)
                     filename = [sessionname 'G' groupname '_spikeRF' ];
                     eval(['save ' filename ' RF'])
                     
                     fprintf('Eye Event Spike Histograms ...\n')
                     bin_length = [20 5 20];
                     eyeeventspike = fvtEyeEventSpikeHistogram(eye,RF,bin_length);
                     %eyeeventspike contains data for all clusters
                     filename = [eyeeventspike.sessionname 'G' eyeeventspike.groupname '_eyeeventspike' ];
                     eval(['save ' filename ' eyeeventspike'])
                     
                     %should be able to combine firing rate stuff more effeciently....
                     fprintf('Spike Probability ...\n')
                     spikeprob = fvtFiringRate_old(RF,eye);
                     filename = [spikeprob.sessionname 'G' spikeprob.groupname '_spikeprob' ];
                     eval(['save ' filename ' spikeprob'])
                     
                     fprintf('Firing Rate Correlations ...\n')
                     FRCor = fvtFRCorrelation(RF,eye);
                     filename = [FRCor.sessionname 'G' FRCor.groupname '_FRcor' ];
                     eval(['save ' filename ' FRCor'])
                     
                  end	% if eyeflag==1
               end	%loop over groups
               cd ..
            end	%if sort folder exists
         end	%if spikeflag
      end	%loop over folders
      
   case('sparse_noise')
      
      dirlist = nptDir;
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if eyeflag
            %do eye processing first
            if strcmp(dirlist(i).name,'eye') & dirlist(i).isdir
               cd (dirlist(i).name)
               eye_dir = nptPWD;
               
               fprintf('Eye Eccentricity ...\n')
               eccentricity = fvtEccentricity(init_info);
               filename = [sessionname , '_eccentricity'];
               eval(['save ' filename ' eccentricity'])

               
               fprintf('Generating Session Eye Movements ...\n')
               eye = eyemovements(1);	%argument means nothing just to differentiate btween null constructor
               filename = [sessionname , '_eye'];
               eval(['save ' filename ' eye'])
               
               bin_length = [10 5 10];
               fprintf('Eye Duration Histograms ...\n')
               duration_histograms = fvtEyeDurationHistograms(eye);
               filename = [sessionname , '_duration'];
               eval(['save ' filename ' duration_histograms'])
               
               fprintf('Eye Position Ranges ...\n')
               position_range = fvtEyePositionRangeHistogram(eye);
               filename = [sessionname , '_positionrange'];
               eval(['save ' filename ' position_range'])
               
               fprintf('Eye Power Spectrum ...\n')
               power_spectrum = fvtEyePowerSpectrum(eye);
               filename = [sessionname , '_powerspectrum'];
               eval(['save ' filename ' power_spectrum'])
               
               fprintf('Eye Velocity Histograms ...\n')
               velocity = fvtEyeVelocityHistogram(eye);
               filename = [sessionname , '_velocity'];
               eval(['save ' filename ' velocity']);
               
               cd ..
            end %if eye folder exists
         end   %if eyeflag
      end	%loop over folders
      
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if spikeflag
            if strcmp(dirlist(i).name,'sort') & dirlist(i).isdir
               cd (dirlist(i).name)
               
               GDFdirlist = nptDir('*.gdf');		
               for group=1:size(GDFdirlist,1)				%loop over groups (electrode channels)
                  groupname = GDFdirlist(group).name(1:length(GDFdirlist(group).name)-5);
                  
                  fprintf('Generating Session Spike Trains for Group # %s ...\n',groupname)
                  %spike = GenerateSessionSpikeTrains(groupname,1,0);
                  filename = [sessionname 'G' groupname '_spike' ];
                  spikedirlist = nptdir([filename '.mat'])
                  spike = ispikes(groupname,1);
                  eval(['save ' filename ' spike'])
                  cd ..		%move up to raw files
                  
                  rc = revcorr(spike, init_info);	%constructor for revcorr class
                  
                  %save object in sort folder
                  filename = [spike.sessionname 'G' spike.groupname '_revcorr' ];
                  eval(['save ' filename ' rc'])
                  
                  fprintf('Generating ISIs for Group # %s ...\n',groupname)
                  ISI = ISIhist(spike);
                  filename = [sessionname 'G' groupname '_ISI' ];
                  eval(['save ' filename ' ISI'])
                  
                  
               end	%loop over groups
               cd .. 		%back to session directory

            end	%if sort exists
         end	%if spikeflag
      end	%loop over folders
      
   case('Calibration')
      dirlist = nptDir;
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if eyeflag
            %do eye processing first
            if strcmp(dirlist(i).name,'eye') & dirlist(i).isdir
               cd (dirlist(i).name)
               eye_dir = nptPWD;
               
               fprintf('Generating Session Eye Movements ...\n')
               eye = eyemovements(1);	%argument means nothing just to differentiate btween null constructor
               filename = [sessionname , '_eye'];
               eval(['save ' filename ' eye'])
               
               bin_length = [10 5 10];
               fprintf('Eye Duration Histograms ...\n')
               duration_histograms = fvtEyeDurationHistograms(eye);
               filename = [sessionname , '_duration'];
               eval(['save ' filename ' duration_histograms'])
               
               fprintf('Eye Position Ranges ...\n')
               position_range = fvtEyePositionRangeHistogram(eye);
               filename = [sessionname , '_positionrange'];
               eval(['save ' filename ' position_range'])
               
               fprintf('Eye Power Spectrum ...\n')
               power_spectrum = fvtEyePowerSpectrum(eye);
               filename = [sessionname , '_powerspectrum'];
               eval(['save ' filename ' power_spectrum'])
               
               fprintf('Eye Velocity Histograms ...\n')
               velocity = fvtEyeVelocityHistogram(eye);
               filename = [sessionname , '_velocity'];
               eval(['save ' filename ' velocity']);
               
               cd ..
            end %if eye folder exists
         end   %if eyeflag
      end	%loop over folders
      
      for i=1:size(dirlist,1)		%loop over folders (eye, sort, etc...)
         if spikeflag
            if strcmp(dirlist(i).name,'sort') & dirlist(i).isdir
               cd (dirlist(i).name)
               
               GDFdirlist = nptDir('*.gdf');		
               for group=1:size(GDFdirlist,1)				%loop over groups (electrode channels)
                  
                  groupname = GDFdirlist(group).name(1:length(GDFdirlist(group).name)-5);
                  
                  fprintf('Generating Session Spike Trains for Group # %s ...\n',groupname)
                  %spike = GenerateSessionSpikeTrains(groupname,1,0);
                  spike = ispikes(groupname,1,0);
                  filename = [sessionname 'G' groupname '_spike' ];
                  eval(['save ' filename ' spike'])
                  
                  fprintf('Generating ISIs for Group # %s ...\n',groupname)
                  ISI = ISIhist(spike);
                  filename = [sessionname 'G' groupname '_ISI' ];
                  eval(['save ' filename ' ISI'])
                  
               end	%loop over groups
               cd ..
            end	%if sort exists
         end 	%if spikeflag
      end	%loop over folders
      
   end	%switch session type
   
   %create marker file to show this session has been processed
   fid=fopen('fvtprocessedsession.txt','wt');
   fclose(fid);
   
end %if no marker file is present

fprintf('\n!!! Session Done!!!\n');
