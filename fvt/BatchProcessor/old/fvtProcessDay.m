function status = fvtProcessDay(varargin)
%
%function fvtProcessDay
%This function starts all the FVT processing for all sessions in a day.
%
%NPT "preprocessing" must be completed before 
%FVT processing can take place.
%
%The pwd must be the correct date folder.
%
%"fvtskip.txt" is used to skip the processing on 
%a day or a session. 
%fvtprocessedday.txt is written as a marker file in the date folder
%
%'redo'			Performs the relevant calculations regardless of
%						of whether the calculations have already been 
%						performed.
%
%'npt' 		calls the preprocessing scripts if they have not 
%				been run,  ie. no npt marker file and no skip.txt file.


status={'ProcessDay Status Log'};

redo = 0;
npt = 0;
num_args = nargin;
for i=1:num_args
   switch varargin{i}
   case('redo')
      redo = 1;
   case('npt')
      npt = 1;
   end
end


% is there a skip file for this day?
marker = nptDir('*skip.txt');

%has this day been preprocessed?
nptmarker = [marker nptDir('processedday.txt')];
if isempty(nptmarker) & npt
   ProcessDay
   status = cellstr(strvcat(char(status),'Preprocessing Day first !!!!!!! '));
end

% check for fvtprocessedday.txt unless redo is 1
if redo==0
	marker = [marker nptDir('fvtprocessedday.txt')];
end


if isempty(marker)
   sessions=[];
   currentDir=nptPWD;
   
   % if we are on a platform that uses filenames that are case
   % sensitive, run command to fix the filenames
   platform = computer;
   if ~(strcmp(platform,'PCWIN') | strcmp(platform,'MAC2'))
      !tolower.tcsh
   end
   
   sessions = nptDir;
   sesSize = size(sessions,1);
   for i=1:sesSize
      % check to make sure it is of the right format
      if sessions(i).isdir
         session_num = sessions(i).name;
         fprintf(['\n\tProcessing Session ' session_num '\n']);
         cd ([currentDir filesep session_num])
         fvtProcessSession(varargin{:})
         status = cellstr(strvcat(char(status),['Session ' session_num]));
         cd ..
      end
   end	%loop over sessions
   %create marker file to show this day has been processed
         fid=fopen('fvtprocessedday.txt','wt');
         fclose(fid);
end	%if marker file exists