function skiptrials = fvtReadSkipTrials(filename)
%function skiptrials = fvtReadSkipTrials(filename)
%
%fvtReadSkipTrials reads the fvtskiptrials file if one is present.  
%fvtskiptrials contains the trial numbers of trials that have been 
%corrupted in some way during data acquisition and should be ignored
%data analysis.

[skiptrials] = textread(filename, '%d', 'delimiter', ' \n');

