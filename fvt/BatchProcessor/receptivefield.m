function varargout = receptivefield(varargin)
% RECEPTIVEFIELD M-file for receptivefield.fig
%      RECEPTIVEFIELD, by itself, creates a new RECEPTIVEFIELD or raises the existing
%      singleton*.
%
%      H = RECEPTIVEFIELD returns the handle to a new RECEPTIVEFIELD or the handle to
%      the existing singleton*.
%
%      RECEPTIVEFIELD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RECEPTIVEFIELD.M with the given input arguments.
%
%      RECEPTIVEFIELD('Property','Value',...) creates a new RECEPTIVEFIELD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before receptivefield_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to receptivefield_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help receptivefield

% Last Modified by GUIDE v2.5 20-Jan-2005 13:47:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @receptivefield_OpeningFcn, ...
                   'gui_OutputFcn',  @receptivefield_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before receptivefield is made visible.
function receptivefield_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to receptivefield (see VARARGIN)

% Choose default command line output for receptivefield
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes receptivefield wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = receptivefield_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%need to pass parameters to main waindow (batchProcessor)
RF = RFparameters(handles);
BPhandle = findobj('Name','bpGUI2');
UD = get(BPhandle,'UserData');
UD.RF = RF;
set(BPhandle,'UserData',UD)
%we should also save this info so it can be displayed if the window is
%opened again
p=which('receptivefield.fig');
[p,n,e]=fileparts(p);
saveas(gcbf,[p filesep 'receptivefield.fig'])
closereq

function RF = RFparameters(handles)   
    RF.revcorr.value = get(handles.CalcRevCorr,'Value');
    redoValue = num2str(get(handles.RFRedo,'Value'));
    RF.revcorr.RedoLevels = redoValue;
    RF.revcorr.WindowSize = get(handles.RFWindowSize,'String');
    RF.revcorr.FrameLevel = num2str(get(handles.RFFrameLevel,'Value'));
    RF.revcorr.NumFramesBack = get(handles.RFNumFramesBack,'String');

    %RFstring = ['''''redoValue'''',' num2str(RFredo) ',''''RFSNxymapsValue'''',' num2str(RFSNxymaps)  ',''''RFSNwindowSizeValue'''',' RFSNwindowSize ',''''MSeqxymapsValue'''',' num2str(RFMSeqxymaps)   ',''''RFMSeqwindowSizeValue'''',' RFMSeqwindowSize ];













% --- Executes on button press in RFFrameLevel.
function RFFrameLevel_Callback(hObject, eventdata, handles)
% hObject    handle to RFFrameLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RFFrameLevel



function RFNumFramesBack_Callback(hObject, eventdata, handles)
% hObject    handle to RFNumFramesBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RFNumFramesBack as text
%        str2double(get(hObject,'String')) returns contents of RFNumFramesBack as a double


% --- Executes during object creation, after setting all properties.
function RFNumFramesBack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RFNumFramesBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


