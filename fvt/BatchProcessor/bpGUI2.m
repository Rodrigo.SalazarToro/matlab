function varargout = bpGUI2(varargin)
% BPGUI2 M-file for bpGUI2.fig
%      BPGUI2, by itself, creates a new BPGUI2 or raises the existing
%      singleton*.
%
%      H = BPGUI2 returns the handle to a new BPGUI2 or the handle to
%      the existing singleton*.
%
%      BPGUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BPGUI2.M with the given input arguments.
%
%      BPGUI2('Property','Value',...) creates a new BPGUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bpGUI2_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bpGUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bpGUI2

% Last Modified by GUIDE v2.5 02-Sep-2004 22:15:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @bpGUI2_OpeningFcn, ...
    'gui_OutputFcn',  @bpGUI2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bpGUI2 is made visible.
function bpGUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bpGUI2 (see VARARGIN)

% Choose default command line output for bpGUI2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes bpGUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bpGUI2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Start.
function Start_Callback(hObject, eventdata, handles)
% hObject    handle to Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(gcbf,'UserData');
if ~isfield(data,'path') | isempty(data.path)
    warndlg('Must set the Start Directory and Directory Type.','Warning')
    return
else
    cd(char(data.path))
end

type = get(handles.DirType,'String');
type = char(type(get(handles.DirType,'Value'),:));

%%%%%%%%%%%%%%%%%%%%%%Query Algorithms%%%%%%%%%%%%%%%%%%%%%
if get(handles.Diary,'Value')
    diary('diary.txt')
end


%create nptdata object of path(s)
%nd = nptdata('SessionDirs',data.path);

switch(type)
    case('days')
        processString = 'ProcessDays';
    case('day')
        processString = 'ProcessDay';
    case('site')
        processString = 'ProcessSite';
    case('session')
        processString = 'ProcessSession';
    case('group')
        processString = 'ProcessGroup';
    case('cell')
        processString = 'ProcessCell';
end

unitType = get(handles.cellPattern,'String');
if isfield(data,'celllist')
    celllist = data.celllist;
else
    celllist = {};
end
if get(handles.intergroup,'Value')
    interintra = '''InterGroup''';
elseif get(handles.intragroup,'Value')
    interintra = '''IntraGroup''';
else
    interintra ='''none''';
end



%Preprocessing
if get(handles.PP,'Value')
    %get PPstring from figure UserData
    UD = get(gcbf,'UserData');
    if isfield(UD,'PP')
        PPstring = UD.PP.PPstring;
        %eval([processString  '('     ','   PPstring ')']);
        eval([processString  '('    PPstring ')']);
    else
        warndlg('Must set the Preprocessing Parameters by rightclicking on the Checkbox.','Warning')
        return
    end
end

%EyeMovements
if get(handles.EM,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'EM')
        EMstring = UD.EM.EMstring;
        str = [processString '(eyemovements' ','  EMstring ');']
        eval(str)
    else
        warndlg('Must set the EyeMovement Parameters by rightclicking on the Checkbox.','Warning')
        return
    end
end

%SpikeTrain
if get(handles.ST,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'ST')
        objectnames = fieldnames(UD.ST);
        for ii = 1:length(objectnames)
            if eval(['UD.ST.' objectnames{ii} '.value'])
                optArgs = fieldnames(eval(['UD.ST.' objectnames{ii}]));
                optString = ['''save'',''UseSites'',''SkipGroups'',''sort'',''UnitType'',' unitType  ',' interintra];
                if ~isempty(optArgs)
                    for jj=2:length(optArgs)
                        optString = [optString  ',''' optArgs{jj} ''',' eval(['UD.ST.' objectnames{ii} '.' optArgs{jj}])  ];
                    end
                end 
                eval(['robj = ' processString '('  objectnames{ii} ',' optString ',''Cells'','      'celllist'                 ')'])
                %DirCmd = ['[nd,robj] =ProcessDirs(nd,''datainit'','   objectnames{ii}        ',''nptDirCmd'',''' PProcessStr 'data = data+obj;'')']
                
                %eval(PProcessStr)
                if ~isempty(UD.ListPath)
                    [p,listname,e] = fileparts(UD.ListPath);
                    filename = [p filesep listname '_' objectnames{ii}];
                    if ~isempty(eval(unitType))
                        filename = [filename  '_' eval(unitType) ]
                    elseif ~strcmp('''none''',interintra)    
                        filename = [filename  '_' eval([interintra(1:6) '''']) ]
                    end
                    save(filename,'robj')
                end
            end
        end
    else
        warndlg('Must set the SpikeTrain Parameters by rightclicking on the Checkbox.','Warning')
        return
    end
end


%ReceptiveField
if get(handles.RF,'Value')
    UD = get(gcbf,'UserData');
    if isfield(UD,'RF')
        objectnames = fieldnames(UD.RF);
        for ii = 1:length(objectnames)
            if eval(['UD.RF.' objectnames{ii} '.value'])
                optArgs = fieldnames(eval(['UD.RF.' objectnames{ii}]));
                optString = ['''save'',''UseSites'',''SkipGroups'',''sort'',''UnitType'',' unitType  ',' interintra];
                if ~isempty(optArgs)
                    for jj=2:length(optArgs)
                        optString = [optString  ',''' optArgs{jj} ''',' eval(['UD.RF.' objectnames{ii} '.' optArgs{jj}])  ];
                    end
                end 
                eval(['robj = ' processString '('  objectnames{ii} ',' optString ',''Cells'','      'celllist'                 ')'])
                %DirCmd = ['[nd,robj] =ProcessDirs(nd,''datainit'','   objectnames{ii}        ',''nptDirCmd'',''' PProcessStr 'data = data+obj;'')']
                
                %eval(PProcessStr)
                if ~isempty(UD.ListPath)
                    [p,listname,e] = fileparts(UD.ListPath);
                    filename = [p filesep listname '_' objectnames{ii}];
                    if ~isempty(eval(unitType))
                        filename = [filename  '_' eval(unitType) ]
                    elseif ~strcmp('''none''',interintra)    
                        filename = [filename  '_' eval([interintra(1:6) '''']) ]
                    end
                    save(filename,'robj')
                end
            end
        end
    else
        warndlg('Must set the Receptive Field Parameters by rightclicking on the Checkbox.','Warning')
        return
    end
end


if get(handles.Diary,'Value')
    diary off
end




% --- Executes on button press in UseList.
function UseList_Callback(hObject, eventdata, handles)
% hObject    handle to UseList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fn,pn,status]=uigetfile('*.txt','Select a Cell List file');

if ~status
    celllist={};
    listpath='';
    fn='';
    set(handles.text31,'Enable','on')
    set(handles.cellPattern,'Enable','on')
else
    celllist =  textread(fullfile(pn,fn),'%s');
    listpath = fullfile(pn,fn);
    set(handles.text31,'Enable','off')
    set(handles.cellPattern,'Enable','off')
    set(handles.cellPattern,'String','''''')
end
UD = get(gcbf,'UserData');
UD.celllist = celllist;
UD.ListPath = listpath;
set(gcbf,'UserData',UD);
set(handles.listname,'String',fn);



% --- Executes on button press in SetPath.
function SetPath_Callback(hObject, eventdata, handles)
% hObject    handle to SetPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
path = uigetdir;
if(path~=0)
	cd(path);
	data = get(gcbf,'UserData');
	data.path = {path};
	data.ListPath='';
	set(gcbf,'UserData',data);
	handle=findobj(gcbf,'Tag','PathTag');
	set(handle,'String',path);
end

% --- Executes on button press in SetPath.
function PPKK_Callback(hObject, eventdata, handles)
% hObject    handle to PPKK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value')
    set(handles.PPBB,'Value',0)
end


% --- Executes on button press in PP.
function PP_Callback(hObject, eventdata, handles)
% hObject    handle to PP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PP


% --- Executes on button press in PPRedo.
function PPRedo_Callback(hObject, eventdata, handles)
% hObject    handle to PPRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPRedo


% --- Executes on button press in PPExtraction.
function PPExtraction_Callback(hObject, eventdata, handles)
% hObject    handle to PPExtraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPExtraction


% --- Executes on button press in PPEye.
function PPEye_Callback(hObject, eventdata, handles)
% hObject    handle to PPEye (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPEye


% --- Executes on button press in PPHighpass.
function PPHighpass_Callback(hObject, eventdata, handles)
% hObject    handle to PPHighpass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPHighpass


% --- Executes on button press in PPLowpass.
function PPLowpass_Callback(hObject, eventdata, handles)
% hObject    handle to PPLowpass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPLowpass


% --- Executes on button press in PPTiming.
function PPTiming_Callback(hObject, eventdata, handles)
% hObject    handle to PPTiming (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPTiming


% --- Executes during object creation, after setting all properties.
function PPThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PPThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to PPThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPThreshold as text
%        str2double(get(hObject,'String')) returns contents of PPThreshold as a double


% --- Executes during object creation, after setting all properties.
function PPGroups_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PPGroups (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function PPGroups_Callback(hObject, eventdata, handles)
% hObject    handle to PPGroups (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PPGroups as text
%        str2double(get(hObject,'String')) returns contents of PPGroups as a double


% --- Executes during object creation, after setting all properties.
function Sessions_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sessions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Sessions_Callback(hObject, eventdata, handles)
% hObject    handle to Sessions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sessions as text
%        str2double(get(hObject,'String')) returns contents of Sessions as a double


% --- Executes on button press in PPBB.
function PPBB_Callback(hObject, eventdata, handles)
% hObject    handle to PPBB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPBB
if get(hObject,'Value')
    set(handles.PPKK,'Value',0)
end

% --- Executes on button press in PPNoMove.
function PPNoMove_Callback(hObject, eventdata, handles)
% hObject    handle to PPNoMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PPNoMove


% --- Executes on button press in EyeMovement.
function EyeMovement_Callback(hObject, eventdata, handles)
% hObject    handle to EyeMovement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EyeMovement


% --- Executes on button press in EMRedo.
function EMRedo_Callback(hObject, eventdata, handles)
% hObject    handle to EMRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMRedo


% --- Executes on button press in EMPositionRange.
function EMPositionRange_Callback(hObject, eventdata, handles)
% hObject    handle to EMPositionRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMPositionRange


% --- Executes on button press in EMVelocities.
function EMVelocities_Callback(hObject, eventdata, handles)
% hObject    handle to EMVelocities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMVelocities


% --- Executes on button press in EMDurations.
function EMDurations_Callback(hObject, eventdata, handles)
% hObject    handle to EMDurations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMDurations


% --- Executes on button press in EMSpectra.
function EMSpectra_Callback(hObject, eventdata, handles)
% hObject    handle to EMSpectra (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMSpectra


% --- Executes during object creation, after setting all properties.
function DirType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DirType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in DirType.
function DirType_Callback(hObject, eventdata, handles)
% hObject    handle to DirType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns DirType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DirType


% --- Executes on button press in Diary.
function Diary_Callback(hObject, eventdata, handles)
% hObject    handle to Diary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Diary


% --- Executes on button press in STEyeEventSpikeHistograms.
function STEyeEventSpikeHistograms_Callback(hObject, eventdata, handles)
% hObject    handle to STEyeEventSpikeHistograms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STEyeEventSpikeHistograms


% --- Executes on button press in STFiringRate.
function STFiringRate_Callback(hObject, eventdata, handles)
% hObject    handle to STFiringRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STFiringRate


% --- Executes on button press in STFiringRateCorrelations.
function STFiringRateCorrelations_Callback(hObject, eventdata, handles)
% hObject    handle to STFiringRateCorrelations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STFiringRateCorrelations


% --- Executes on button press in STISI.
function STISI_Callback(hObject, eventdata, handles)
% hObject    handle to STISI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STISI


% --- Executes on button press in EMEccentricities.
function EMEccentricities_Callback(hObject, eventdata, handles)
% hObject    handle to EMEccentricities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EMEccentricities


% --- Executes on button press in SpikeTrain.
function SpikeTrain_Callback(hObject, eventdata, handles)
% hObject    handle to SpikeTrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SpikeTrain


% --- Executes on button press in STRedo.
function STRedo_Callback(hObject, eventdata, handles)
% hObject    handle to STRedo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of STRedo



% --- Executes on button press in spiketrainParams.
function spiketrainParams_Callback(hObject, eventdata, handles)
% hObject    handle to spiketrainParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
spiketrain

% --- Executes on button press in spiketrainParams.
function eyemovementParams_Callback(hObject, eventdata, handles)
% hObject    handle to spiketrainParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eyemovement

% --- Executes on button press in spiketrainParams.
function preprocessingParams_Callback(hObject, eventdata, handles)
% hObject    handle to spiketrainParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preprocessing

% --- Executes on button press in spiketrainParams.
function receptivefieldParams_Callback(hObject, eventdata, handles)
% hObject    handle to spiketrainParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
receptivefield





% --- Executes on button press in EM.
function EM_Callback(hObject, eventdata, handles)
% hObject    handle to EM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of EM


% --- Executes on button press in checkbox46.
function checkbox46_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox46


% --- Executes on button press in checkbox47.
function checkbox47_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox47


% --- Executes on button press in ST.
function ST_Callback(hObject, eventdata, handles)
% hObject    handle to ST (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ST


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over ST.
function ST_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ST (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in RF.
function RF_Callback(hObject, eventdata, handles)
% hObject    handle to RF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RF




% --- Executes during object creation, after setting all properties.
function cellPattern_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellPattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function cellPattern_Callback(hObject, eventdata, handles)
% hObject    handle to cellPattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellPattern as text
%        str2double(get(hObject,'String')) returns contents of cellPattern as a double




% --- Executes on button press in intergroup.
function interintra_Callback(hObject, eventdata, handles)
% hObject    handle to intergroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp('intragroup',get(hObject,'Tag'))
    if get(handles.intragroup,'Value')
        set(handles.intergroup,'Value',0)
    elseif get(handles.intergroup,'Value')
        set(handles.intragroup,'Value',0)
    end
else
    if get(handles.intergroup,'Value')
        set(handles.intragroup,'Value',0)
    elseif get(handles.intragroup,'Value')
        set(handles.intergroup,'Value',0)
    end
end