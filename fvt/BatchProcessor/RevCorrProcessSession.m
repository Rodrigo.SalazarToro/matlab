function RevCorrProcessSession(varargin)


redo=0;
RFSNxymaps=1;
RFMSeqxymaps=1;
MSeqwindowSize=200;
SNwindowSize=200;

if ~isempty(varargin) 
    num_args = nargin;
    
    for i=1:num_args
        switch varargin{i}
            case('redoValue')
                redo = varargin{i+1};
            case('RFSNxymapsValue')
                RFSNxymaps = varargin{i+1};
            case('RFSNwindowSizeValue')
                SNwindowSize = varargin{i+1};
            case('RFMSeqxymapsValue')
                RFMSeqxymaps = varargin{i+1};
            case('RFMSeqwindowSizeValue')
                MSeqwindowSize = varargin{i+1};
        end
    end  
end


%check to see if this is a sparse noise session
stimInfo = stiminfo('Auto','save',1);
if strcmp(stimInfo.data.iniInfo.type,'sparse_noise') & RFSNxymaps
    cd sort
    dirlist = nptDir('*spike.mat');
    cd ..
    for ii=1:length(dirlist)
        ind = findstr('g00',dirlist(ii).name);
        group = dirlist(ii).name(ind+1:ind+4);
        RCdirlist = nptDir(['*g' group '_revcorrSN.mat']);
        if isempty(RCdirlist) | redo==1
            revcorrSN(str2num(group),'saveobj',1,'window_size',SNwindowSize,'redo',redo)
        end
    end
    
elseif strcmp(stimInfo.data.iniInfo.type,'m_sequence') & RFMSeqxymaps
    cd sort
    dirlist = nptDir('*spike.mat');
    cd ..
    for ii=1:length(dirlist)
        ind = findstr('g00',dirlist(ii).name);
        if isempty(ind)
            ind = findstr('G00',dirlist(ii).name);
        end
        group = dirlist(ii).name(ind+1:ind+4);
        RCdirlist = nptDir(['*g' group '_revcorrMSeq.mat']);
        if isempty(RCdirlist) | redo==1
            revcorrMSeq(str2num(group),'saveobj',1,'window_size',MSeqwindowSize,'redo',redo)
        end
    end
end