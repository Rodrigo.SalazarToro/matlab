NeuroInfo = NeuronalChAssign;
pf = NeuroInfo.cortex == 'F';
pf = [1 : 25];
comb = nchoosek([1:48],2);
sgr = nptDir('group0*');
for c = 1 : 23; gch(c) = str2num(sgr(c).name(end-3:end)); end
gch = find(ismember(NeuroInfo.groups,gch) == 1);
acomb = ismember(comb,pf);
gcomb = ~ismember(comb,gch);
tcomb = sum(acomb,2);
% 1 == PP; 2 == inter; 3 == PF
pvalues10000 = cell(4,3);

nrep = 10000;
cd lfp/
for c = 1 : 1128
    if sum(gcomb(c,:),2) == 0
        for p = 1 : 4
            file = sprintf('c%dc%d%dp%d.mat',comb(c,1),comb(c,2),nrep,p);
            
            if ~isempty(nptDir(file))
            load(file)
            
            pvalues10000{p,tcomb(c)+1} = cat(3,pvalues10000{p,tcomb(c)+1},thresh);
            end
        end
    end
end

wn = {'pre-sample' 'sample' 'delay1' 'delay2'};

ylims = [0.075 0.095;0.095 0.125; 0.12 0.16; 0.135 0.2; 0.15 0.23; 0.16 0.25]; 

% for p = 1 : 4
%    figure
%    subplot(6,3,sb)
%     [Cmean,Cstd,phimean,phistd,tile99,Prob2{p},f] = cohsurrogate(data{1},data{1},params,'Prob',prob,'nrep',10000,'generalized');
%      [Cmean,Cstd,phimean,phistd,tile99,Prob{p},f] = cohsurrogate(data{1},data{1},params,'Prob',prob,'nrep',1000,'generalized');
% end

for p = 1 : 4
    f1 = figure;
    set(f1,'Name',wn{p})
    for pl = 1 : 6
        for t = 1 : 3
            subplot(6,3,(pl-1) * 3 + t)
            plot(f,prctile(squeeze(pvalues10000{p,t}(:,pl,:)),[10 25 50 75 90 99],2))
            hold on
            plot(f,Prob{p}(:,pl),'.')
             plot(f,Prob2{p}(:,pl),'r.')
            ylim(ylims(pl,:))
            xlim([5 60]);
        end
    end
    type = {'PP; n = 105' 'Inter; n = 120' 'PF; n = 28'};
    
    for sb = 1 : 3
        subplot(6,3,sb)
        title(type{sb})
    end
    
    
    
    pv = {'p=0.05' 'p=0.01' 'p=0.001' 'p=0.0001' 'p=0.00001' 'p=0.000001'};
    cc = 1;
    for sb = [1 : 3 : 16]
        subplot(6,3,sb)
        ylabel(pv{cc})
        cc = cc + 1;
    end
    
    subplot(6,3,18); xlabel('Frequency'); ylabel('Coherence')
end


