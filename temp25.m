for i = ind(105:end)
    day = cohC_B.data.setNames{i};
    npair = cohC_B.data.Index(i,1);
    groups = cohC_B.data.Index(i,10:11);
    if ~isempty(strfind(day,'clark'))
    [session,f,vdz] = compCorInCortrials(day,npair,groups,'redo');
    else
        [session,f,vdz] = compCorInCortrials(day,npair,groups,'redo','ML'); 
    end
end
