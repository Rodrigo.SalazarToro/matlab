function processSessionSpikesMice(varargin)

Args = struct('xcorrlag',300,'minlength',5000,'onlyxcorr',0);
Args.flags = {'onlyxcorr'};
[Args,modvarargin]  = getOptArgs(varargin,Args);


sdir = pwd;
% Detect if drugs use and separate trials if needed
if exist('Dreadd.txt','file') || exist('SalvB.txt','file')
    % get trials without drugs
    [light,odors,sound,boths,odor,both] = getTrialType(1,modvarargin{:});
    
    trialTypes{1} = light;
    trialTypes{2} = odors;
    trialTypes{3} = sound;
    trialTypes{4} = boths;
    trialTypes{5} = (odors + sound + boths) > 0;
    for cc = 1 : size(odor,1);
        trialTypes{5+cc} = odor(cc,:);
        trialTypes{5+size(odor,1)+cc} = both(cc,:);
    end
    
    % get trials with drugs
    [light,odors,sound,boths,odor,both] = getTrialType(2,modvarargin{:});
    trialTypes{6+cc} = light + drug;
    trialTypes{7+cc} = odors + drug;
    trialTypes{8+cc} = odors + drug;
    trialTypes{9+cc} = odors + drug;
    trialTypes{10+cc} = odors + drug;
    for ccc = 1 : size(odor,1);
        trialTypes{10+ccc} = odor(ccc,:);
        trialTypes{10+size(odor,1)+ccc} = both(ccc,:);
    end
    
else
    % condition without drugs
    [light,odors,sound,boths,odor,both] = getTrialType(modvarargin{:});
    trialTypes{1} = light;
    trialTypes{2} = odors;
    trialTypes{3} = sound;
    trialTypes{4} = boths;
    trialTypes{5} = (odors + sound + boths) > 0;
    for cc = 1 : size(odor,1);
        trialTypes{5+cc} = odor(cc,:);
        trialTypes{5+size(odor,1)+cc} = both(cc,:);
    end
end


count = 1;

if ~isempty(nptDir('lfp'))
    cd lfp
    lfpfiles = nptDir('*_lfp.0*');
    tdata = load(lfpfiles(1).name,'-mat');
    lfps = zeros(size(tdata.lfp,1),Args.minlength,length(lfpfiles));
    for ff = 1 : length(lfpfiles)
        load(lfpfiles(ff).name,'-mat');
        lfps(:,1:Args.minlength,ff) =  lfp(:,1:Args.minlength);
        
    end
    cd ..
end
if ~isempty(nptDir('sort'))
    cd sort
    groups = nptDir('group0*');
    sortdir = pwd;
    for g = 1 : length(groups)
        cd(groups(g).name)
        gdir = pwd;
        clusters = nptDir('cluster0*');
        for cl = 1 : length(clusters)
            cd(clusters(cl).name)
            
            load ispikes
            allsp{count} = sp;
            unitname{count} = pwd;
            if ~Args.onlyxcorr
                %                 spikesdata(sp,'save','trialType',trialTypes,'lfp',lfps,modvarargin{:});
                compareOdors('save',modvarargin{:});
                compareSound('save',modvarargin{:});
            end
            cd(gdir)
            count = count + 1;
        end
        cd(sortdir)
        
    end
end
%% xcorr

if exist('allsp','var') && length(allsp) > 1
    bins = [-Args.xcorrlag - 1 : Args.xcorrlag + 1];
    
    pcomb = nchoosek([1:length(allsp)],2);
    cumcoi = zeros(length(trialTypes),size(pcomb,1),length(bins));
    for ncomb = 1 : size(pcomb,1)
        
        for ttype = 1 : length(trialTypes)
            for tr = vecr(find(trialTypes{ttype}))
                if  tr <= allsp{pcomb(ncomb,1)}.data.numTrials
                    for nspike = 1 : length(allsp{pcomb(ncomb,1)}.data.trial(tr).cluster.spikes)
                        if ~isempty(allsp{pcomb(ncomb,2)}.data.trial(tr).cluster.spikes)
                            nh = hist(allsp{pcomb(ncomb,2)}.data.trial(tr).cluster.spikes - allsp{pcomb(ncomb,1)}.data.trial(tr).cluster.spikes(nspike),bins);
                            cumcoi(ttype,ncomb,:) = squeeze(cumcoi(ttype,ncomb,:)) + nh';
                        end
                    end
                end
            end
        end
    end
    
    cumcoi = cumcoi(:,:,2:end-1);
    bins = bins(2:end-1);
    
    save('xcorrSpikes.mat','cumcoi','pcomb','bins','unitname')
    display(['saving ' pwd '/' 'xcorrSpikes.mat'])
end
cd(sdir)