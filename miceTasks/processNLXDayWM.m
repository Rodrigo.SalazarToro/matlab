function processNLXDayWM(varargin)
% test 2werwergfsdgfdgsdfgfsd
% timeBeforeStim in sec

Args = struct('startSession',1,'timeBeforeStim',1.0,'timeAfterOlf',6,'withNl2dat',0);
Args.flags = {'withNl2dat'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});
try
    matlabpool 12
end
cygwinpath = 'c:\cygwin64\bin\';
startdir = pwd;
[thepath,basename] = fileparts(pwd);
pc =  ispc;
diary([pwd filesep 'diary.txt']);
sessions = nptDir('20*');
if isempty(nptDir('movedfiles.txt'))
    
    
    if ~isempty(sessions)
        tdir = cell(length(sessions),1);
        parfor ss = 1 : length(sessions)
            sdir = [thepath filesep basename filesep sessions(ss).name filesep '*'];
            tdir{ss} = [thepath filesep basename filesep sprintf('session%0.2d',ss + Args.startSession - 1)];
            mkdir(tdir{ss})
            movefile(sdir,tdir{ss})
            display(sprintf('Moving %s to %s',sdir,tdir{ss}))
            
        end
        
    end
    
    if pc
        doscommand = [cygwinpath 'touch movedfiles.txt'];
        dos(doscommand);
        cd(startdir)
        doscommand = [cygwinpath ' rmdir *'];
        dos(doscommand);
        doscommand = [cygwinpath 'touch movedfiles.txt'];
        dos(doscommand);
    else
        ! touch movedfiles.txt
        cd(startdir)
        ! rmdir *
    end
end
cd(startdir)
sessions = nptDir('session*');
if Args.withNl2dat
    for ss = 1 : length(sessions);
        cd([startdir filesep sessions(ss).name])
        processNLXSession(modvarargin{:})
        cd(startdir)
    end
end
% timing
for ss = 1 : length(sessions)
    cd(sessions(ss).name)
    desFile = nptDir('*_descriptor.txt');
    descriptor_info = ReadDescriptor(desFile.name);
    channelStart = find(strcmp(descriptor_info.description,'starttrial') == 1);
    [~,stOlf,SR] = detectTstart(channelStart,channelStart,channelStart,sprintf('%s%0.2d',basename,ss),'save','olfaction',modvarargin{:},'threshold',100);
    cd ..
end
for ss = 1 : length(sessions)
    cd([startdir filesep sessions(ss).name])
    desFile = nptDir('*_descriptor.txt');
    descriptor_info = ReadDescriptor(desFile.name);
    channelStart = find(strcmp(descriptor_info.description,'starttrial') == 1);
    [~,stOlf,SR] = detectTstart(channelStart,channelStart,channelStart,sprintf('%s%0.2d',basename,ss),'olfaction');
    % cutCSCfilesTrials(sprintf('%s%0.2d',basename,ss) ,[1 : descriptor_info.number_of_channels],stAudio - SR * Args.timeBeforeStim,'triallength',1000 * (Args.timeBeforeStim + Args.timeAfterAudio))
    % cutCSCfilesTrials(sprintf('%s%0.2d',basename,ss),[1 : descriptor_info.number_of_channels],stOlf - SR * Args.timeBeforeStim,'triallength',1000 * (Args.timeBeforeStim + Args.timeAfterOlf),'startnumering',length(stAudio) + 1)
    cutCSCfilesTrials(sprintf('%s%0.2d',basename,ss),[1 : descriptor_info.number_of_channels],stOlf - SR * Args.timeBeforeStim,'triallength',1000 * (Args.timeBeforeStim + Args.timeAfterOlf))
    
    cd ..
end



keyboard

for ss = 1 : length(sessions)
    cd(sessions(ss).name)
    ProcessSession('highpass','highpasshigh',8000);
    cd(startdir)
     status1 = MoveProcessedFiles(sessions,ss);
      cd(sessions(ss).name)
    PlotRawData
    diplay('do the rejectrials.mat and delete the lfp and highpass folder')
    keyboard
    cd highpass
    delete('*highpass*')
    
    cd ..
    rearrangeTrials
    adaptStimSeq('save','addName',sprintf('%s0%d',basename,ss))
    realignTrial('autosave')
    delete('processedsession.txt')
    ProcessSession('lowpass','highpass','highpasslow',500,'highpasshigh',8000,'extraction','threshold',4,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','no'});
    cd ..
end
mycomp = computer;

if strcmp(mycomp,'PCWIN64')
    status1 = MoveProcessedFiles(sessions,length(sessions));
else
    mname = which('moveprocessedfiles.sh');
    [status1,w] = system(mname);
end

for ss = 1 : length(sessions)
    cd(sessions(ss).name)
    cd sort
    hdrfiles = nptDir('WaveformsHeader_g*');
    for ff = 1 : length(hdrfiles)
        copyfile(hdrfiles(ff).name,[startdir filesep sessions(ss).name filesep 'sort' filesep 'FD' filesep hdrfiles(ff).name])
    end
    cd(startdir)
end

diary off;