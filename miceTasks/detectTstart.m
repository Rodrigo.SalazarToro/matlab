function [stAudio,stOlf,varargout] = detectTstart(channelAudio,channelOlf,channelResp,basename,varargin)

Args = struct('chunksize',60*32556,'movingAve',500,'plotRaw',0,'minlength',200,'resample',1000,'lowpassed',200,'save',0,'stimthresh',-15000,'audio',0,'olfaction',0,'redoDetectTstart',0);
Args.flags = {'plot','save','audio','olfaction','redoDetectTstart'};
[Args] = getOptArgs(varargin,Args,'remove',{});

filename = sprintf('%strialStart.mat',basename);
if isempty(nptDir(filename)) || Args.redoDetectTstart
    [~,~,nrSamples,sampleFreq,~,headerInfo] = getRawCSCTimestamps(sprintf('CSC%02g.Ncs',channelAudio));
    varargout{1} = sampleFreq;
    stAudio = [];
    stOlf = [];
    
    if Args.audio
        % nchunk = ceil(nrSamples / Args.chunksize);
        nchunk = 1;
        chunkstart = [0 : Args.chunksize : nrSamples];
        
        nc = 1;
        chunk = cell(nchunk,1);
        display('Processing Audio')
        while nc <= nchunk
            if nc < nchunk
                [~,data] = getRawCSCData(sprintf('CSC%02g.Ncs',channelAudio),chunkstart(nc),chunkstart(nc + 1) );
            elseif nc == nchunk
                [~,data] = getRawCSCData(sprintf('CSC%02g.Ncs',channelAudio),chunkstart(nc),nrSamples);
            end
            ldata = nptLowPassFilter(data,sampleFreq,Args.resample,0.1,Args.lowpassed);
            
            figure
            plot(ldata);
            start = input('Start time?');
            stop = input('End time?');
            if ~isempty(start) && ~isempty(stop)
                newdata = ldata(start:stop);
                
                clf
                newdata = diff(newdata);
                plot(newdata)
                threshold = input('Upper threshold?');
                aboveT = find(newdata > threshold );
                
                tempS = find(diff(aboveT) > Args.minlength);
                startT = [aboveT(1); aboveT(tempS + 1)];
                
                chunk{nc} = (nc - 1) * Args.chunksize + (startT + start - 1) * (sampleFreq / Args.resample);
                
                if Args.plotRaw
                    plot(data)
                    hold on
                    line([chunk{nc}'; chunk{nc}'],[repmat(min(data),1,length(chunk{nc})); repmat(max(data),1,length(chunk{nc}))],'lineStyle','--','color','k')
                else
                    plot(ldata)
                    hold on
                    liness = chunk{nc} * (Args.resample / sampleFreq);
                    line([liness'; liness'],[repmat(min(data),1,length(chunk{nc})); repmat(max(data),1,length(chunk{nc}))],'lineStyle','--','color','k')
                end
            end
            keepon = input('OK? manual cleaning ==2; yes == 1; no = 0? ');
            while keepon == 2
                display(liness')
                for ii = 1 : length(liness); text(liness(ii),threshold,num2str(ii)); end
                cols = input('Onsets to remove?');
                chunk{nc}(cols) = [];
                clf
                plot(ldata)
                hold on
                liness = chunk{nc} * (Args.resample / sampleFreq);
                line([liness'; liness'],[repmat(min(data),1,length(chunk{nc})); repmat(max(data),1,length(chunk{nc}))],'lineStyle','--','color','k')
                keepon = input('OK? manual cleaning ==2; yes == 1; no = 0? ');
            end
            
            if keepon
                nc = nc + 1;
                
            end
        end
        stAudio = chunk{1};
    end
    if Args.olfaction
        clear start stop
        display('Processing Odors')
        [~,data] = getRawCSCData(sprintf('CSC%02g.Ncs',channelOlf),0,nrSamples);
        [~,resp] = getRawCSCData(sprintf('CSC%02g.Ncs',channelResp),0,nrSamples);
        ldata = nptLowPassFilter(data,sampleFreq,Args.resample,1,Args.lowpassed);
        
        lresp = nptLowPassFilter(resp,sampleFreq,Args.resample,1,Args.lowpassed);
        keepon = false;
        
        while ~keepon
            subplot(2,1,1)
            plot(ldata)
            subplot(2,1,2)
            plot(lresp)
            
            % linkedzoom(gcf,'onxy')
            start = input('Start time?');
            stop = input('End time?');
            threshold = input('Lower threshold?');
            
            if ~isempty(start) && ~isempty(stop)
                if threshold > 0
                     onsets = find(ldata > threshold);
                else
                onsets = find(ldata < threshold);
                end
                onsets(onsets < start | onsets > stop) = [];
                tonsets = onsets;
                b1 = find(diff(tonsets) == 1);
                
                tonsets(b1) = [];
                while ~isempty(b1)
                    b1 = find(diff(tonsets) == 1);
                    tonsets(b1) = [];
                end
                subplot(2,1,1)
                hold on
                
                plot(tonsets, repmat(threshold,1,length(tonsets)),'r*')
                
                
                keepon = input('OK? manual cleaning ==2; yes == 1; no = 0? ');
                while keepon == 2
                    display(tonsets)
                    for ii = 1 : length(tonsets); text(tonsets(ii),threshold,num2str(ii)); end
                    cols = input('Onsets to remove?');
                    tonsets(cols) = [];
                    clf
                    plot(ldata)
                    hold on
                   
                    line([tonsets'; tonsets'],[repmat(min(data),1,length(tonsets)); repmat(max(data),1,length(tonsets))],'lineStyle','--','color','k')
                    keepon = input('OK? manual cleaning ==2; yes == 1; no = 0? ');
                end
                stOlf = tonsets * (sampleFreq / Args.resample);
            end
            clf
        end
        
        
    end
    
    if Args.save
        ss = input('Save file?');
        if ss
            display([pwd '/' filename])
            save(filename,'stOlf','stAudio','headerInfo','channelAudio','channelOlf','sampleFreq')
        end
    end
else
    display([pwd 'folder with timing already detected already processed'])
    load(filename)
     varargout{1} = sampleFreq;
end
