function obj = plot(obj,varargin)


Args = struct('psthbin',50,'maxtime',5000,'plotType','standard','maxC',0.5,'fmax',100,'newbinraster',[],'rasterforAlan',0);
Args.flags = {'rasterforAlan'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
% display(obj.data.Index(ind,28:37))

%% plotting fct
load ispikes
load spikesdata

cats = {'light' 'odor' 'sound' 'odor + sound' 'all' 'odor1' 'odor2' 'odor3'  'odor1+sound' 'odor2+sound' 'odor3+sound' };
set(gcf,'Name',obj.data.setNames{ind})
switch Args.plotType
    
    case {'standard' 'specificOdors'}
        
        bins = [10 200 200 200];
        switch Args.plotType
            case 'standard'
                thetypes = [1:5];
            case 'specificOdors'
                thetypes =  6 : length(spikes);
        end
        bins = [10 200 200 200];
        for ttype = thetypes
            subplot(length(thetypes),3,(ttype-thetypes(1)) * 3 + 1)
            if ttype == 1
                [n,xtick] = hist(spikes{ttype},[0:2:Args.maxtime]);
                plot(xtick(1:end-1),(1000/2)* (n(1:end-1)/ntrials(ttype)))
            else
                try
                    [n,xtick] = hist(spikes{ttype},[0:Args.psthbin:Args.maxtime]);
                    plot(xtick(1:end-1),(1000/Args.psthbin)* (n(1:end-1)/ntrials(ttype)))
                end
            end
            ylabel(cats{ttype})
            xlim([0 Args.maxtime])
            %             ylim([0 15])
            if ttype-thetypes(1)+ 1 == length(thetypes)
                ylabel('FR (sp/s)')
                xlabel('time (ms)')
            end
            subplot(length(thetypes),3,(ttype-thetypes(1))  * 3 + 2)
            %try
            
            if isempty(Args.newbinraster)
                if Args.rasterforAlan
                    tottrials = size(raster{ttype},1);
                    
                    bins = [1 : Args.maxtime/binraster];
                    for t = 1 : tottrials
                        ntimes = find(raster{ttype}(t,:));
                        
                        [n,xout ] = hist(ntimes,bins);
                        
                        if ~isempty(ntimes)
                            sspike = ntimes;
                            
                            line([sspike; sspike],[(ones(1,length(sspike)) + t); (ones(1,length(sspike)) + t + 1)],'Color','k','LineWidth',2);
                            hold on
                        end
                    end
                    xlim([0,size(raster{ttype},2)]);
                     ylim([0,size(raster{ttype},1)]);
                else
                    imagesc([1 : binraster : Args.maxtime],[1:size(raster{ttype},1)],~raster{ttype})
                    
                end
            else
                newraster = zeros(size(raster{ttype},1),length([0 : Args.newbinraster : Args.maxtime]));
                sttrial = [1 find(diff(spikes{ttype}) < 0) length(spikes{ttype})];
                c = 1;
                for ntr = 1 : size(raster{ttype},1)
                    if sum(raster{ttype}(ntr,:)) ~= 0 && (c < length(sttrial))
                        newraster(ntr,:) = hist(spikes{ttype}(sttrial(c) : sttrial(c + 1)),[0 : Args.newbinraster : Args.maxtime]);
                        
                        c = c + 1;
                    end
                    
                end
                imagesc([1 : Args.newbinraster : Args.maxtime],[1:size(newraster,1)],~newraster)
            end
            colormap('gray')
            
            subplot(length(thetypes),3,(ttype-thetypes(1))  * 3 + 3)
            try
                plot(slags,autocor{ttype})
            end
            if ttype == length(thetypes)
                ylabel('cum. spikes')
                xlabel('lags (ms)')
            end
        end
        
    case 'spfieldCoh'
        
        for ttype = 1 : length(spikes)
            subplot(length(spikes),2,(ttype - 1) * 2 + 1)
            plot(f,squeeze(C(ttype,:,:)))
            ylabel(cats{ttype})
            ylim([0 Args.maxC])
            xlim([0 Args.fmax])
            subplot(length(spikes),2,(ttype - 1) * 2 + 2)
            plot(f,squeeze(phi(ttype,:,:)))
            xlim([0 Args.fmax])
        end
        xlabel('Frequency (Hz)')
        ylabel('Angle (rad)')
        
end
%%
