function obj = micepsth(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedLFP',[]);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'micepsth';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mpsth';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) && isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [~,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                && (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

end
function obj = createObject(Args,varargin)
sdir = pwd;
try
    [light,odor,sound,both] = getTrialType;
    success = true;
catch
    success = false;
end
if ~isempty(nptDir('lightFreq.txt'))
    try
    readt = textread('lightFreq.txt','%d');
    freq = readt;
    catch
        freq=nan;
    end
    
else
    freq=nan;
    
end
if ~isempty(nptDir('sort')) && success
    cd sort
    groups = nptDir('group0*');
    if  ~isempty(groups)
        count = 1;
        data.Index = [];
        sortdir = pwd;
        for g = 1 : size(groups,1)
            cd(groups(g).name)
            gdir = pwd;
            
            clusters = nptDir('cluster*');
            for c = 1 : size(clusters,1)
                cd(clusters(c).name)
                
                if ~isempty(nptDir('ChR2.txt'))
                    ChR2 = 1;
                elseif ~isempty(nptDir('NB.txt'))
                    ChR2 = 2;
                else
                    ChR2 = 0;
                end
                load ispikes.mat
                data.Index(count,1) = count;
                
                if clusters(c).name(end) == 'm';  data.Index(count,2) = 2; else  data.Index(count,2) = 1; end
                % 1 for SUA 2 for MUA
                data.Index(count,3) = ChR2;
                % odor and sound onset & discrimination
                load compareOdors.mat
                data.stimOnset(count,1) = min(pOnset);
                data.stimOnset(count,2) = mean(direc{1});
                load compareSound.mat
                data.stimOnset(count,3) = pOnset(1);
                data.stimOnset(count,4) = mean(direc{1});
                data.stimOnset(count,5) = pOnset(2);
                data.stimOnset(count,6) = mean(direc{2});
                load compareOdors2.mat
                [mini,xi] = min(pOnset);
                data.stimOnset(count,7) = mini;
                data.stimOnset(count,8) = mean(direc{xi});
                
                nspikes = 0;
                for tr = 1 : sp.data.numTrials
                    nspikes = nspikes + sp.data.trial(tr).cluster.spikecount;
                    
                end
                fr = nspikes/(5*(sp.data.numTrials));
                data.Index(count,4) = fr;
                
                data.Index(count,5) = freq;
                data.odorsDiscr(count,1) = pdiscr;
                data.setNames{count} = pwd;
                count = count + 1;
                cd(gdir)
            end%c = 1 : size(clusters,1)
            cd(sortdir)
        end%g = 1 : size(group,1)
        data.numSets = count-1; % nbr of trial
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('empty object \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('empty object \n');
    obj = createEmptyObject(Args);
end
cd(sdir)
end
function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.odorsDiscr = [];
data.stimOnset = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
end