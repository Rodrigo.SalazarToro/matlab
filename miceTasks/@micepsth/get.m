function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'unit',[],'ChR2',[],'stimOnset',[],'modality','odor','odorsDiscr',[],'stimEffect',[],'minFR',[],'lightFreq',[]);
Args.flags = {'Number','ObjectLevel','bothRules'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.unit)
        ttemp = find(obj.data.Index(:,2) == strmatch(Args.unit,{'s' 'm'}));
    else
        ttemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.ChR2)
        runits = [];
        for el = 1 : length(Args.ChR2)
           runits =  [runits; find(obj.data.Index(:,3) == Args.ChR2(el))];
        end
        ttemp = intersect(runits,ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
    if ~isempty(Args.stimOnset)
        switch Args.modality
            
            case 'odor'
                ttemp = intersect(find(obj.data.stimOnset(:,1) < Args.stimOnset),ttemp);
                if ~isempty(Args.stimEffect)
                    eval(sprintf('ttemp = intersect(find(obj.data.stimOnset(:,2) %s 0),ttemp);',Args.stimEffect))
                end
            case 'sound'
                ttemp = intersect(find(obj.data.stimOnset(:,3) < Args.stimOnset),ttemp);
                if ~isempty(Args.stimEffect)
                    eval(sprintf('ttemp = intersect(find(obj.data.stimOnset(:,4) %s 0),ttemp);',Args.stimEffect))
                end
            case 'sound&odor'
                ttemp = intersect(find(obj.data.stimOnset(:,5) < Args.stimOnset),ttemp);
                if ~isempty(Args.stimEffect)
                    eval(sprintf('ttemp = intersect(find(obj.data.stimOnset(:,6) %s 0),ttemp);',Args.stimEffect))
                end
            case 'odorspecific'
                 ttemp = intersect(find(obj.data.stimOnset(:,7) < Args.stimOnset),ttemp);
                if ~isempty(Args.stimEffect)
                    eval(sprintf('ttemp = intersect(find(obj.data.stimOnset(:,8) %s 0),ttemp);',Args.stimEffect))
                end
                
        end
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
    if ~isempty(Args.odorsDiscr)
        ttemp = intersect(find(obj.data.odorsDiscr(:,1) < Args.odorsDiscr),ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
    if ~isempty(Args.minFR)
        ttemp = intersect(find(obj.data.Index(:,4) >= Args.minFR),ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    
     if ~isempty(Args.minFR)
        ttemp = intersect(find(obj.data.Index(:,4) >= Args.minFR),ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
     end
    
      if ~isempty(Args.lightFreq)
        ttemp = intersect(find(obj.data.Index(:,5) == Args.lightFreq),ttemp);
    else
        ttemp = intersect(ttemp,[1 : size(obj.data.Index,1)]);
    end
    varargout{1} = ttemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

