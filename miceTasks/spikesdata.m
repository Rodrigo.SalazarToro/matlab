function [spikes,raster,ntrials, autocor,varargout] = spikesdata(sp,varargin)
% calculates the psth raster and autocor for each unit
% if use trialInfo then cell{1} with timing info and cell{2} with trial
% type
% trialcut is the time before the time of the trial onset and the time used
% for testing onsets
Args = struct('psthbin',10,'ocorbin',1,'lags',300,'trialType',[],'trialInfo',[],'trialcut',2000,'maxtime',6000,'save',0,'redo',0,'lfp',[],'tapers',[10 19],'Fs',1000,'fpass',[0 150],'rangeOnsetTest',2000);
Args.flags = {'save','redo'};
[Args,modvarargin]  = getOptArgs(varargin,Args);

filename = 'spikesdata.mat';
params =struct('tapers',Args.tapers,'Fs',Args.Fs,'trialave',1,'fpass',Args.fpass);
if isempty(nptDir(filename)) || Args.redo
    
    if ~isempty(Args.trialInfo)
        
        utype = unique(Args.trialInfo{2});
        spikes = cell(length(utype),1);
        raster = cell(length(utype),1);
        ntrials = zeros(length(utype),1);
        autocor = cell(length(utype),1);
        pOnset = ones(length(utype),1);
        for ttype = 1 : length(utype)
            st = find(Args.trialInfo{2} == utype(ttype));
            clear ospikes
            baseline = zeros(length(st),1);
            trial = baseline;
            for tr = 1 : length(st)
                allspikes = double(sp.data.trial.cluster.spikes);
                sspike =  find((allspikes >= (Args.trialInfo{1}(st(tr)) - Args.trialcut(1))) & (allspikes <= (Args.trialInfo{1}(st(tr)) + Args.maxtime)));
                newspikes = allspikes(sspike) -  Args.trialInfo{1}(st(tr)) + Args.trialcut(1);
                spikes{ttype} = cat(2,spikes{ttype},newspikes');
                raster{ttype} = [raster{ttype}; hist(newspikes,[0 : Args.psthbin : Args.maxtime + Args.trialcut(1)])];
                ntrials(ttype) = ntrials(ttype) + 1;
                [allcumspikes,slags] = autocorr(newspikes,Args.lags,Args.ocorbin);
                
                baseline(tr) = length(find((newspikes >= (Args.trialcut - Args.rangeOnsetTest)) & (newspikes <= Args.trialcut))) ;
                trial(tr) = length(find((newspikes >= Args.trialcut) & (newspikes <= (Args.trialcut + Args.rangeOnsetTest))) );
                
                if isempty(autocor{ttype})
                    autocor{ttype} =   allcumspikes;
                else
                    autocor{ttype} = autocor{ttype} +  allcumspikes;
                end
                ospikes(tr).times = newspikes' / 1000;
                
            end
            if ~isempty(st) && ~isempty(Args.lfp)
                for ch = 1 : size(Args.lfp,1);
                    [C(ttype,ch,:),phi(ttype,ch,:),~,~,~,f]=coherencycpt(squeeze(Args.lfp(ch,:,st)),ospikes,params);
                end
            else
                for ch = 1 : size(Args.lfp,1);
                    C(ttype,ch,:) = [];
                    phi(ttype,ch,:)=[];
                end
            end
%             pOnset(ttype) = ranksum(baseline,trial);% WRS (W rank sum)
           pOnset(ttype) = signrank(baseline,trial);% paired WSR (W signed rank)
        end
        
    elseif ~isempty(Args.trialType)
        spikes = cell(length(Args.trialType),1);
        raster = cell(length(Args.trialType),1);
        ntrials = zeros(length(Args.trialType),1);
        autocor = cell(length(Args.trialType),1);
        for tr = 1 : sp.data.numTrials
            for ttype = 1 : length(Args.trialType)
                
                if ismember(tr,find(Args.trialType{ttype}))
                    spikes{ttype} = cat(2,spikes{ttype},sp.data.trial(tr).cluster.spikes);
                    raster{ttype} = [raster{ttype}; hist(sp.data.trial(tr).cluster.spikes,[0 : Args.psthbin : Args.maxtime])];
                    ntrials(ttype) = ntrials(ttype) + 1;
                    [allcumspikes,slags] = autocorr(sp.data.trial(tr).cluster.spikes,Args.lags,Args.ocorbin);
                    
                    if isempty(autocor{ttype})
                        autocor{ttype} =   allcumspikes;
                    else
                        autocor{ttype} = autocor{ttype} +  allcumspikes;
                    end
                end
                
            end
        end
        %% spike field coherence
        ospikes(tr).times = sp.data.trial(tr).cluster.spikes(sp.data.trial(tr).cluster.spikes < Args.maxtime)' /1000;
        %%
        %% spike field coherence
        for ttype = 1 : length(Args.trialType)
            strials = find(Args.trialType{ttype});
            strials = strials(strials < length(ospikes));
            if ~isempty(strials) && ~isempty(Args.lfp)
                for ch = 1 : size(Args.lfp,1);
                    [C(ttype,ch,:),phi(ttype,ch,:),~,~,~,f]=coherencycpt(squeeze(Args.lfp(ch,:,strials)),ospikes(strials),params);
                end
            else
                for ch = 1 : size(Args.lfp,1);
                    C(ttype,ch,:) = [];
                    phi(ttype,ch,:)=[];
                end
            end
        end
        %%
    end
    
else
    load(filename)
end
 varargout{1} = pOnset;
binraster = Args.psthbin;

if Args.save
    if ~isempty(Args.lfp)
    save(filename,'spikes','raster','ntrials','autocor','slags','C','phi','f','binraster','pOnset');
    else
         save(filename,'spikes','raster','ntrials','autocor','slags','binraster','pOnset'); 
    end
    display(['saving ' pwd '/' filename])
end


