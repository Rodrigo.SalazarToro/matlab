function [light,odor,sound,both] = getseqTrial(nseq,conds,varargin)

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

light =  ismember(nseq,find(ismember(conds,{'Air + Light'})));

odor = ismember(nseq,find(ismember(conds,{'3-Hexanone' 'Amyl acetate' 'Ethyl butyrate'})));
sound = ismember(nseq,find(ismember(conds,{'Sound'})));
both = ismember(nseq,find(ismember(conds,{'Sound+3-Hexanone' 'Sound+Amyl acetate' 'Sound+Ethyl butyrate'})));