function makeShankdat(basename,shanks,varargin)
% basename is the base name of the .dat files
% shanks is a cell array with each element as the list of the channels of
% the shank. The order of the channels must follow the order of neighboring
% channels as defined in the .prb file (very important for the sorting to
% work).


Args = struct('shankSplit',0);
Args.flags = {'shankSplit'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


nshank = length(shanks);
if nshank > 1 && Args.shankSplit
    parfor ns = 1 : nshank
        
        sname = sprintf('%s_shank%d',basename,ns)
        
        ncs2dat(shanks{ns},sname,modvarargin{:})
        
    end
    ncs2dat(cat(2,shanks{:}),sprintf('%s_all',basename),modvarargin{:})
elseif length(shanks) == 1
     ncs2dat(shanks{1},basename,modvarargin{:})
else
     ncs2dat(cell2mat(shanks),basename,modvarargin{:})
end