function [varargout] = putSpikeWVintoCluster(file,group,varargin)
% in the session folder

sdir = pwd;
data = load(file);
clusters = data.clusters(3:end);


thegroup = sprintf('group%0.4d',group);
cd(thegroup)
gdir = pwd;
rclusters = nptDir('cluster*s');

if length(rclusters) == length(clusters)
    for cl = 1 : length(clusters)
        thecluster = sprintf('cluster%0.2ds',clusters(cl));
        if ~isempty(nptDir(thecluster))
            cd(thecluster)
            if isempty(nptDir('ispikes.mat'))
                error(['No ispikes for this cluster ' pwd])
            else
                load ispikes.mat
                if length(sp.data.trial.cluster.spikes) == size(data.wv{1,cl},1);
                    for el = 1 : size(data.wv,1)
                        sp.data.trial.cluster.waveform(el,:,:) = data.wv{el,cl};
                    end
                    save('ispikes.mat','sp')
                    display(['Saving ispikes with waveforms ' pwd])
                else
                    error(['Different # spikes and # waveform' pwd]);
                end
            end
            cd(gdir)
        else
            error(['the cluster does not exist' pwd])
        end
    end
else
    display('error different # of SUAs as folders and in spikeWaveformclu')
end
cd(sdir)