function audiStim(varargin)

Args = struct('ntrials',100,'pre_stim',500,'duration',500,'iti',[500 700],'FSsound',8192,'dBW',10);
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args,'remove',{});


vl = (Args.FSsound / 1000) * Args.duration; 
y = wgn(vl,1,Args.dBW);
y = y / max(abs(y));

display('!!!!!!!!!!!!!!!!!!!!START OF SESSION!!!!!!!!!!!!!!!!!!!!!!!!!!')
for tr = 1 : Args.ntrials
    display(sprintf('%d',tr))
     pause(Args.pre_stim/1000)
    sound(y)
    pause((Args.iti(1) + randi(Args.iti(2) - Args.iti(1),1))/1000)
end


display('!!!!!!!!!!!!!!!!!!!!END OF SESSION!!!!!!!!!!!!!!!!!!!!!!!!!!!')