function [wv,varargout] = getspikewaveforms(kwikfile,baseclufile,shank,CSCchannels,varargin)
% in the session folder; output only the SUA
% needs to get the spike times in timestamps from the res files. It is not precise enough because precision is in ms
% shank in reference to kwikfile thus start at 0
Args = struct('save',0,'redo',0,'addName',[],'sampling_rate',32000,'lowpass',500,'highpass',15000,'nptBefore',16,'nptAfter',16);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

filename = sprintf('spikeWaveform%s.mat',Args.addName);

if isempty(nptDir(filename)) || Args.redo
    
    sksamples = double(hdf5read(kwikfile,sprintf('/channel_groups/%d/spikes/time_samples',shank)));
    clufile = sprintf('%s.%d',baseclufile,shank);
    if ~isempty(nptDir(clufile))
        clusterlist = load(clufile);
        if length(clusterlist) == length(sksamples) + 1
           clusterlist = clusterlist(2:end);
        else
            error('different lengths between clu and kwik files')
        end
        clusters = unique(clusterlist);
        if length(clusters) >= 2
        % clusters = double(hdf5read(kwikfile,sprintf('/channel_groups/%d/spikes/clusters/main',shank)));% problem cluster numbers OK but no info on MUA, noise or SUA.
        wv = cell(length(CSCchannels),length(clusters) - 2);
        for ch = 1 : length(CSCchannels)
            [~,data] = getRawCSCData(sprintf('CSC%d.ncs',CSCchannels(ch)),1,1,1);
            hp = nptHighPassFilter(data,Args.sampling_rate,Args.lowpass,Args.highpass);
            clear data
            
            for cl = 3 : length(clusters)
                thecluster = clusters(cl);
                cpt = sksamples(clusterlist == thecluster);
                timeserie = cpt - Args.nptBefore; % in samples
                for npt = -Args.nptBefore+1 : Args.nptAfter
                    timeserie = cat(2,timeserie, cpt + npt);
                end
                
                for is = 1 : size(timeserie,1)
                    wv{ch,cl-2}(is,:) = single(hp(timeserie(is,:)));
                end
            end
        end
        if Args.save
            save(filename,'wv','clusters','kwikfile','baseclufile','shank','CSCchannels')
            display([pwd filesep filename]);
        end
       else
            display([pwd filesep clufile 'no SUA']);
            wv = [];
        end
    else
        display(['Error ' pwd filesep 'no clu file'])
        wv = [];
    end
else
    load(filename)
    
end

varargout{1} = clusters;