function [allcumspikes,range] = autocorr(sptimes,lags,bin,varargin)
% all in msec

Args = struct('redo',0,'infolder',0,'save',0,'addName',[],'multitaper',0,'tapers',[5 9],'Fs',1000,'fpass',[.5 100],'spikeLFP',0,'movingWind',[2 .1],'iterations',200,'spikeLFPSur',0,'onlyFitSurrogate',0,'pvalues',[10^-3 10^-4 10^-5 10^-6 10^-7],'HistBinNumber',20,'timelimit',[]);
Args.flags = {'infolder','redo','save','multitaper','spikeLFP','spikeLFPSur','onlyFitSurrogate'};
[Args,modvarargin]  = getOptArgs(varargin,Args);

if Args.infolder
    load ispikes.mat
    if isempty(Args.timelimit)
    sptimes = double(sp.data.trial.cluster.spikes);
    else
        
       sptimes = double(sp.data.trial.cluster.spikes); 
       indtimes = find(sptimes >= Args.timelimit(1) & sptimes <= Args.timelimit(2));
       sptimes = sptimes(indtimes);
    end
    if Args.spikeLFP || Args.spikeLFPSur
        thepath = pwd;
        thegr =  str2double(thepath(strfind(pwd,'group') + 5 : strfind(pwd,'group') + 8));
        cd ../..
        lfpfiles = nptDir('*lfp_group*.mat');
        for ff = 1 : length(lfpfiles)
            if ~isempty(strfind(lfpfiles(ff).name,['group0' mat2str(thegr)]))
                load(lfpfiles(ff).name)
                mlfp = double(mean(lfp))';
                
            end
        end
        cd(thepath)
    end
end

if Args.multitaper
    sptimes = sptimes/1000;
    params = struct('tapers',Args.tapers,'Fs',Args.Fs,'fpass',Args.fpass,'trialave',1,'err',[2 .05]);
    
    if Args.spikeLFP
        savename = ['mtsspikefield' Args.addName '.mat'];
        if Args.redo || isempty(nptDir(savename))
            
            [C,phi,~,~,~,t,f,zerosp,~,~,Cerr]=cohgramcpt(mlfp,sptimes,Args.movingWind,params);
            if Args.save
                save(savename,'C','phi','zerosp','f','t','Cerr')
                display([pwd filesep savename])
            end
        end
    elseif Args.spikeLFPSur
        savename = ['mtsspikefieldSur' Args.addName '.mat'];
        if Args.onlyFitSurrogate && ~isempty(nptDir(savename))
            load(savename)
            try
            [datafit,xx,thresh,rsquare] = fitSurface(C,f,'Distribution','Norm','HistBinNumber',20,'Prob',1-Args.pvalues,'twoSided');
            save(savename,'datafit','xx','thresh','rsquare','-append')
            end
        else
            if Args.redo || isempty(nptDir(savename))
                [~,~,~,~,~,t,f,~] = cohgramcpt(mlfp,sptimes,Args.movingWind,params);
                C = zeros(Args.iterations,length(f));
                %             phi = zeros(Args.iterations,length(f));
                %             tic
                for ii = 1 : Args.iterations
                    tic
                    shift = randi(round(sptimes(end-10)));
                    rtimes = mod(sptimes + shift,sptimes(end));
                    [Ct,~,~,~,~,t,f,zerosp]=cohgramcpt(mlfp,rtimes,Args.movingWind,params);
                    Ct(zerosp==1,:) = nan(sum(zerosp),size(Ct,2));
                    %                phit(zerosp==1,:) = nan(sum(zerosp),size(Ct,2));
                    C(ii,:) = nanmedian(Ct);
                    %                 phi(ii,:) = nanmedian(phit);
                    toc
                end
                %             toc
                if Args.save
                    save(savename,'C','f','t')
                    display([pwd filesep savename])
                end
            end
        end
    else
        savename = ['mtsautocorr' Args.addName '.mat'];
        if Args.redo || isempty(nptDir(savename))
            %     [mtspwd,f] = mtspectrumpt(sptimes,params);
            [mtspwd,t,f] = mtspecgrampt(sptimes,[2 .1],params);
            if Args.save
                save(savename,'mtspwd','f','t')
                display([pwd filesep savename])
            end
        end
    end
    allcumspikes = nan;
    range =nan;
else
    
    savename = ['autocorr' Args.addName '.mat'];
    if isempty(nptDir(savename)) || Args.redo
        range = [-lags: bin : lags];
        
        allcumspikes = zeros(1,length(range));
        for sp = 1 : length(sptimes)
            
            ntimes = setdiff(sptimes,sptimes(sp)) - sptimes(sp);
            cumspikes = histc(ntimes,range);
            allcumspikes = allcumspikes + vecr(cumspikes(1:end));
            
        end
        
        if Args.save
            
            save(savename,'allcumspikes','range')
            display([pwd filesep savename])
        end
    else
        load(savename)
    end
end