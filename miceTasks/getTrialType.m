function [light,odors,sound,boths,varargout] = getTrialType(varargin)


Args = struct('save',0,'redo',0);
Args.odors = {'3-Hexanone', 'Amyl acetate', 'Ethyl butyrate'};
Args.flags = {'save','redo'};
Args = getOptArgs(varargin,Args);


filename = 'seqID';

if isempty(nptDir(filename)) || Args.redo
    seqfile = nptDir('*newSeq.mat');
    load(seqfile.name,'-mat');
    load('rejectedTrials.mat');
%     
%     if cond == 1
%         % select trials before drug
%         idx=find(rejectedTrials <= length(conds)*30);
%         nseq=nseq(idx);
%     elseif cond == 2
%         % select trials after drug
%         idx=find(rejectedTrials > length(conds)*30);
%         nseq=nseq(idx);
%     end
    light =  ismember(nseq,find(ismember(conds,{'Air + Light'})));
    
    odors = ismember(nseq,find(ismember(conds,Args.odors)));
    
    odor = zeros(length(Args.odors),length(odors));
    for otype = 1 : length(Args.odors)
       odor(otype,:) = ismember(nseq,find(ismember(conds,Args.odors{otype}))); 
        
    end
    sound = ismember(nseq,find(ismember(conds,{'Sound'})));
    
    spluso = cell(1,length(Args.odors));
     both = zeros(length(Args.odors),length(odors));
    for cc = 1 : length(Args.odors)
        spluso{1,cc} = ['Sound+' Args.odors{cc}];
       
       both(cc,:) = ismember(nseq,find(ismember(conds,spluso{cc}))); 
    end
    
    boths = ismember(nseq,find(ismember(conds,spluso)));
    
else
    load(filename)
end

varargout{1} = odor;
varargout{2} = both;

if Args.save
   save(filename,'light','odors','sound','boths','odor','both') 
   display(['saving ' pwd '/' filename])
end