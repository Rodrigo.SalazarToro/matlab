function kwik2clu_res(kwikfile,varargin)

Args = struct('manytrials',0,'redo',0);
Args.flags = {'manytrials','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

sdir = pwd;
clufile = regexprep(kwikfile,'.kwik','.clu.0');
resfile = regexprep(clufile,'.clu.','.res.');

if isempty(nptDir(resfile)) || Args.redo || isempty(nptDir(clufile))
    
    gr = 0;
    nogr = false;
    clusters = [];
    spiketimes = [];
    callclusters = [];
    while ~nogr
        try
            theclusters = gr * 1000 + hdf5read(kwikfile,sprintf('/channel_groups/%d/spikes/clusters/main',gr));
            
            uclusters = unique(theclusters);
            
           
            for cg = 1 : length(uclusters)
                clustergr = hdf5read(kwikfile,sprintf('/channel_groups/%d/clusters/main/%d/cluster_group',gr,uclusters(cg)));
                if clustergr == 0 || clustergr == 1
                ind = find(theclusters == uclusters(cg));
                theclusters(ind) = int32(clustergr);
                end
            end
            
            clusters = cat (1,clusters,theclusters);
            spiketimes =cat(1,spiketimes,hdf5read(kwikfile,sprintf('/channel_groups/%d/spikes/time_samples',gr)));
        catch
            nogr = true;
        end
        gr = gr + 1;
    end
    
    [spiketimes,ix] = sort(spiketimes);
   
    clusters = [length(unique(clusters)); clusters(ix)];
    
    fileid= fopen(resfile,'w');
    
    fprintf(fileid,'%u \n',spiketimes);
    
    fclose(fileid);
    
    fileid= fopen(clufile,'w');
    
    fprintf(fileid,'%u \n',clusters);
    
    fclose(fileid);
    
    display(sprintf('saving %s%s%s',pwd,filesep,resfile))
  
    
end
