function kwik2res(kwikfile,clufile,varargin)

% Organization of the .clu file:
%
% cluster 0 : the noise spikes from klustaviewa
% cluster 1 : the mua from klustaviewa
% watch out bug with cluster with only one spike... the cluster cannot be
% deleted thus some SUA may have only opn cluster
%
% Organiaztion of the kwik file
% the spiketimes are given in sample #
% output in milliseconds spiketimes
% dayname = '20170829';
% sessions = nptDir('session*');
% for ss = 1 : length(sessions)
%     cd(sessions(ss).name)
%     for sh = 0 : 7
%         if ~isempty(nptDir(sprintf('%s0%d.clu.%d',dayname,ss,sh)))
%             kwik2res(sprintf('%s0%d.kwik',dayname,ss),sprintf('%s0%d.clu.%d',dayname,ss,sh),'redo','channel_groups',sh);
%         end
%     end
%     cd ..
% end
Args = struct('manytrials',0,'redo',0,'manyshanks',0,'channel_groups',0);
Args.flags = {'manytrials','redo','manyshanks'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});
sdir = pwd;
resfile = regexprep(clufile,'.clu.','.res.');
if isempty(nptDir(resfile)) || Args.redo
    
    spiketimes =hdf5read(kwikfile,sprintf('/channel_groups/%d/spikes/time_samples',Args.channel_groups));
    
    fileid= fopen(resfile,'w');
    
    fprintf(fileid,'%u \n',spiketimes);
    
    fclose(fileid);
    display(sprintf('saving %s%s%s',pwd,filesep,resfile))
    cluster = load(clufile);
    sampling_rate = hdf5read(kwikfile,'/recordings/0/sample_rate');
    if length(cluster) == length(spiketimes) + 1
        
        
        cluster = cluster(2:end);
        
    else
        error('different lengths between clu and kwik files')
    end
    
    if ~isempty(findstr(resfile,'shank'))
        shankn = str2double(resfile(findstr(resfile,'shank') + 5));
        if isnan(shankn)
            shankn = 1;
        end
    else
        shankn = Args.channel_groups +1;
    end
    gdir = sprintf('group%04.0f',shankn);
    mkdir(gdir)
    display(sprintf('creating %s%s%s folder',pwd,filesep,gdir))
    cd(gdir)
    dirs = nptDir('*');
    for dd = 1 : length(dirs);rmdir(dirs(dd).name,'s'); end
    gdir = pwd;
    cldir = sprintf('cluster%02.0fm',1);
    mkdir(cldir)
    display(sprintf('creating %s%s%s folder',pwd,filesep,cldir))
    cd(cldir)
    
    
    if Args.manytrials
        
    else
        sp.data.trial(1).cluster.spikes = 1000 * spiketimes(cluster == 1) / sampling_rate; %
        display(sprintf('saving %s%sispikes.mat',pwd,filesep))
    end
    save('ispikes.mat','sp')
    
    cd(gdir)
    clustername = unique(cluster);
    for cl = 1 : length(unique(cluster)) - 2
        cldir = sprintf('cluster%02.0fs',clustername(2+cl));
        mkdir(cldir)
        display(sprintf('creating %s%s%s folder',pwd,filesep,cldir))
        cd(cldir)
        if Args.manytrials
            
        else
            sp.data.trial(1).cluster.spikes = double(1000 * spiketimes(cluster == clustername(2+cl)) / sampling_rate); %
            display(sprintf('saving %s%sispikes.mat',pwd,filesep))
        end
        save('ispikes.mat','sp')
        cd(gdir)
    end
    cd(sdir)
end

