function [pOnset,direc,pdiscr] = compareOdors(varargin)

Args = struct('basePeriod',[1 2000],'stimPeriod',[2001 4000],'save',0,'redo',0,'loadFile','spikesdata.mat','odorCat',2,'sname','compareOdors.mat');
Args.flags = {'save','redo'};
Args = getOptArgs(varargin,Args);

sname = Args.sname;
% cats = {'light' 'odor' 'sound' 'odor + sound' 'all' 'odor1' 'odor2' 'odor3'  'odor1+sound' 'odor2+sound' 'odor3+sound' };
if (Args.redo || isempty(nptDir(sname))) && ~isempty(nptDir(Args.loadFile))
    load(Args.loadFile)
    
    %% response to odors
    
    direc = cell(length(Args.odorCat),1);
    pOnset = ones(length(Args.odorCat),1);
    for cat = 1 : length(Args.odorCat)
        baseline = sum(raster{Args.odorCat(cat)}(:,ceil(Args.basePeriod(1) / binraster) : ceil(Args.basePeriod(2) / binraster)),2);
        stim =  sum(raster{Args.odorCat(cat)}(:,ceil(Args.stimPeriod(1) / binraster) : ceil(Args.stimPeriod(2) / binraster)),2);
%         pOnset(cat) = signrank(baseline,stim);
         pOnset(cat) = ranksum(baseline,stim);
        direc{cat} = stim - baseline;
    end
    
    
    stims = [];
    groups = [];
    for s = 1 : 3
        el = ceil(Args.stimPeriod(1) / binraster) : ceil(Args.stimPeriod(2) / binraster);
        nel = sum(raster{5+s}(:,el),2);
        stims = [stims; nel];
        groups = [groups; ones(length(nel),1) * s];
    end
    
    pdiscr = kruskalwallis(stims,groups,'off');
    
elseif isempty(nptDir(Args.loadFile))
    dispaly(['missing file ' pwd filesep Args.loadFile])
else
    load(sname)
    
end

if Args.save
    save(sname,'pdiscr','pOnset','direc');
    display(['saving ' pwd sname])
end