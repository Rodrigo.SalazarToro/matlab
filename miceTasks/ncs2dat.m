function ncs2dat(channels,targetfile,varargin)
% based on python file ncs2dat.py

Args = struct('basename','CSC','zeropad',0,'pyFileLoc','python C:\Users\salazarr\mypythonlib\ncs2dat.py','fileExt','.ncs');
Args.flags = {'zeropad'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});


files = [];
for ch = channels
    if Args.zeropad
        files  = cat(2,files,sprintf('%s%.02d%s ',Args.basename,ch,Args.fileExt));
        
    else
        files = cat(2,files,sprintf('%s%d%s ',Args.basename,ch,Args.fileExt));
    end
   
end

cmd = [Args.pyFileLoc ' ' files targetfile '.dat'];

[status,cmdout] = dos(cmd,'-echo');

if status == 0 
    display(['saved ' pwd filesep targetfile])
else
    display('ERROR ERROR ERROR ERROR')
    save('ncs2datError.mat','cmdout')
    
end