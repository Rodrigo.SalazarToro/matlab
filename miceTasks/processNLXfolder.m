function processNLXfolder(varargin)
% to run in the folder where the CSC files are

Args = struct('nchannel',36,'dir',[],'filename',[]','redo',0);
Args.flags = {'redo'};
[Args] = getOptArgs(varargin,Args,'remove',{});

sdir = pwd;
if ~isempty(Args.dir)
    
    sdir = Args.dir;
end

if isempty(Args.filename)
    [~,filename,~] = fileparts(sdir);
else
    filename = Args.filename;
end

if isempty(nptDir(filename)) || Args.redo
    display(['Processing audio ' sdir '/' filename]);
    unix(sprintf('nl2dat CSC `count 1 %d` %s/%s.dat',Args.nchannel,sdir,filename))
else
    display([pwd '/' filename ' already processed'])
end
