% fezgfezdfezdf
% sessions = nptDir('session*');
%
% for ss = 1 : length(sessions)
%     cd(sessions(ss).name)
%     if isempty(nptDir('skip.txt'))
%         processSessionSpikesMice
%     end
%     cd ..
% end
%
%
%
%
clear all
cd E:\salazar\Electrophy\CLArec
alldays = {'20150604' '20150623' '20150703' '20150707' '20150710' '20150713' '20150803' '20150804' '20150922' '20151001'};
obj1 = ProcessDays(micepsth,'days',alldays,'NoSites');
% obj2 = ProcessDays(micexcorr,'days',alldays,'NoSites');
% InspectGUI(obj)
% InspectGUI(obj,'plotType','spfieldCoh')

psthbin = 50;
cells = [1 2]; % 2 for neighboring cell and 1 for ChR2 responsive
pthres = 0.05;
minFR = .2;
%% SUA firing rate
cats = {'light' 'odor' 'sound' 'odor + sound' 'all' 'odor1' 'odor2' 'odor3'  'odor1+sound' 'odor2+sound' 'odor3+sound' };

[nunit,ind]=get(obj1,'Number','minFR',minFR);
[nsua,ind]=get(obj1,'unit','s','Number','minFR',minFR);
[nmua,ind]=get(obj1,'unit','m','Number','minFR',minFR);
[nChR2,ind]=get(obj1,'ChR2',cells,'Number','minFR',minFR);
sprintf('%d mice %d unit %d SUA %d MUA %d unit ChR2 responsive',length(alldays),nunit,nsua,nmua,nChR2)
InspectGUI(obj1,'ChR2',cells,'unit','s')

[n,ind]=get(obj1,'ChR2',cells,'unit','s','Number','minFR',minFR);

sprintf('%d SUA ChR2 responsive',n)
% baseline FR

for ii = 1 : n
    
    cd(obj1.data.setNames{ind(ii)})
    load spikesdata
    
    baselineFR(ii) = length(find(spikes{5} < 2000)) / (2*ntrials(5));
end
figure
hist(baselineFR,[0:max(baselineFR)])
xlabel('Baseline Firing Rate (sp/s)')
ylabel('# SUA')
title(sprintf('n = %d',n))

sum(raster{7}(:,201:400),2)

%% mean psth to ChR2
stimFreq = 50;
[nChR2,ind]=get(obj1,'ChR2',1,'Number','lightFreq',stimFreq);
[nMUA,~]=get(obj1,'ChR2',1,'Number','unit','m','minFR',minFR,'lightFreq',stimFreq);
[nSUA,~]=get(obj1,'ChR2',1,'Number','unit','s','minFR',minFR,'lightFreq',stimFreq);
ttype = 1;
psthbin2 = 5;

thebins = [-psthbin2 : psthbin2 : 5000 + psthbin2];
FR = zeros(length(ind),length(thebins)-2);
nFR = FR;
for ii =  1 : length(ind)
    cd(obj1.data.setNames{ind(ii)})
    load spikesdata
    
    [n,xtemp] = hist(spikes{ttype},thebins);
    tpsth = (1000/psthbin2)* (n(2:end-1)/ntrials(ttype));
    zpths = (tpsth - mean(tpsth)) / std(tpsth);
    FR(ii,:) = tpsth;
    nFR(ii,:) = smooth(zpths,3);
%         nFR(ii,:) = zpths;
    %          nFR{ef}(ii,:) = FR{ef}(ii,:) /max(FR{ef}(ii,:));
    xtick = xtemp(2:end-1);
end

figure

meanmax = mean(FR);
stdmax = std(FR);

plot(xtick,meanmax + mean(nFR))
hold on
plot(xtick,meanmax + mean(nFR) + stdmax .*std(nFR)/sqrt(size(nFR,1)))
plot(xtick,meanmax + mean(nFR) - stdmax .*std(nFR)/sqrt(size(nFR,1)))
xlim([1000 4000])

xlabel('Time (ms)')
ylabel('Firing rate (sp/s)')

title(sprintf('n = %d MUA and %d SUA  ChR2 responsive',nMUA,nSUA,nChR2))
set(gcf,'Name','light')
%% stim effect
%% odors
stim = cell(2,1);
FR = cell(2,1);
nFR= cell(2,1);


[nChR2,ind]=get(obj1,'ChR2',cells,'Number','minFR',minFR);
[n,stim{1}] = get(obj1,'ChR2',cells,'stimOnset',pthres,'stimEffect','<','Number','minFR',minFR);
[n,stim{2}] = get(obj1,'ChR2',cells,'stimOnset',pthres,'stimEffect','>','Number','minFR',minFR);
[nMUA,~] = get(obj1,'unit','m','ChR2',cells,'stimOnset',pthres,'stimEffect','>','Number','minFR',minFR);
[nSUA,~] = get(obj1,'unit','s','ChR2',cells,'stimOnset',pthres,'stimEffect','>','Number','minFR',minFR);


ttype = 2;
for ef = 1 : 2
    for ii =  1 : length(stim{ef})
        cd(obj1.data.setNames{stim{ef}(ii)})
        load spikesdata
        
        [n,xtemp] = hist(spikes{ttype},[-psthbin : psthbin : 5000+psthbin]);
        tpsth = (1000/psthbin)* (n(2:end-1)/ntrials(ttype));
        zpths = (tpsth - mean(tpsth)) / std(tpsth);
        FR{ef}(ii,:) = tpsth;
        nFR{ef}(ii,:) = smooth(zpths,3);
        %          nFR{ef}(ii,:) = FR{ef}(ii,:) /max(FR{ef}(ii,:));
        xtick = xtemp(2:end-1);
    end
end
lb ={'r' 'k'};
figure
for ef = 1 : 2%2
    meanmax = mean(FR{ef});
    stdmax = std(FR{ef});
    subplot(2,1,ef)
    plot(xtick,meanmax + mean(nFR{ef}),lb{ef})
    hold on
    plot(xtick,meanmax + mean(nFR{ef}) + stdmax .*std(nFR{ef})/sqrt(size(nFR{ef},1)),lb{ef})
    plot(xtick,meanmax + mean(nFR{ef}) - stdmax .*std(nFR{ef})/sqrt(size(nFR{ef},1)),lb{ef})
    xlim([0 5000])
    
    xlabel('Time (ms)')
    ylabel('Firing rate (sp/s)')
    
end
title(sprintf('n = %d MUA and %d SUA over %d unit ChR2 responsive or neigbours',nMUA,nSUA,nChR2))
set(gcf,'Name','odors')

%% Sound and sound plus odor
modalities = {'sound' 'sound&odor'};

stim = cell(2,1);
FR = cell(2,1);
nFR = cell(2,1);
for mod = 1 : 2
    stim = cell(2,1);
    FR = cell(2,1);
    [n,stim{1}] = get(obj1,'ChR2',cells,'stimOnset',pthres,'modality',modalities{mod},'stimEffect','<','Number','minFR',minFR);
    [n,stim{2}] = get(obj1,'ChR2',cells,'stimOnset',pthres,'modality',modalities{mod},'stimEffect','>','Number','minFR',minFR);
    [nMUA,~] = get(obj1,'unit','m','ChR2',cells,'stimOnset',pthres,'modality',modalities{mod},'stimEffect','>','Number','minFR',minFR);
    [nSUA,~] = get(obj1,'unit','s','ChR2',cells,'stimOnset',pthres,'modality',modalities{mod},'stimEffect','>','Number','minFR',minFR);
    
   
    ttype = 2+mod;
    for ef = 1 : 2
        for ii =  1 : length(stim{ef})
            cd(obj1.data.setNames{stim{ef}(ii)})
            load spikesdata
            
            [n,xtemp] = hist(spikes{ttype},[-psthbin: psthbin :5000+psthbin]);
            tpsth = (1000/psthbin)* (n(2:end-1)/ntrials(ttype));
            zpths = (tpsth - mean(tpsth)) / std(tpsth);
            FR{ef}(ii,:) = tpsth;
            nFR{ef}(ii,:) = smooth(zpths,3);
            nFR{ef}(ii,:) = zpths;
            xtick = xtemp(2:end-1);
        end
    end
    lb ={'r' 'k'};
    figure
    for ef = 1 : 2%2
        meanmax = mean(FR{ef});
        stdmax = std(FR{ef});
        subplot(2,1,ef)
        plot(xtick,meanmax + mean(nFR{ef}),lb{ef})
        hold on
        plot(xtick,meanmax + mean(nFR{ef}) + stdmax .*std(nFR{ef})/sqrt(size(nFR{ef},1)),lb{ef})
        plot(xtick,meanmax + mean(nFR{ef}) - stdmax .*std(nFR{ef})/sqrt(size(nFR{ef},1)),lb{ef})
        xlim([0 5000])
        
        xlabel('Time (ms)')
        ylabel('Firing rate (sp/s)')
        
    end
    title(sprintf('n = %d MUA and %d SUA over %d unit ChR2 responsive or neighbors',nMUA,nSUA,nChR2))
    set(gcf,'Name',modalities{mod})
    figure
     for ef = 1 : 2%2
      if ~isempty(FR{ef})
        subplot(2,1,ef)
        plot(xtick,FR{ef})
       
        xlim([0 5000])
        
        xlabel('Time (ms)')
        ylabel('Firing rate (sp/s)')
      end
    end
    title(sprintf('n = %d MUA and %d SUA over %d unit ChR2 responsive or neighbors',nMUA,nSUA,nChR2))
    set(gcf,'Name',modalities{mod})
    
    
end


%% LFP analysis
keyboard
[nChR2,ind]=get(obj1,'ChR2',cells,'Number');

for ii = 1 : length(ind)
    
    cd(obj1.data.setNames{ind(ii)})
    load spikesdata
    gr = str2double(obj1.data.setNames{ii}(53:55));
    allC(ii,:) = mean(C(2,gr*4-3:gr*4,:),2);
    
end

figure

plot(f,prctile(allC,[25 50 75],1))
xlim([0 80])

xlabel('Frequency (Hz)')
ylabel('Spike Field Coherence')
title(sprintf('n = %d',nChR2))
legend('25th prctile','50th prctile','75th prctile')