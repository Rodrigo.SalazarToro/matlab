function realignTrial(varargin)

Args = struct('BeforeStim',2,'AfterStim',3,'threshold',300,'inspect',0,'autosave',0,'startTrial',1);
Args.flags = {'inspect','autosave'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

files = nptDir('*.0*');

seqfile = nptDir('*newSeq.mat');

desfile = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(desfile.name);

audioCH = strcmp(descriptor_info.description,'audio');
olfCH = strcmp(descriptor_info.description,'olfactometer');



load(seqfile.name)
[light,odor,sound,both] = getseqTrial(nseq,conds);


for tr = Args.startTrial : length(files)
    load(files(tr).name,'-mat')
    time1= [1:size(raw,2)] / samplingRate;
    ptbefore = Args.BeforeStim *samplingRate;
    ptafter = Args.AfterStim *samplingRate;
    if ismember(tr,find(odor))  ||  ismember(tr,find(light))
        [~,ind] = max(raw(olfCH,:));
        ntrial = raw(:,ind - ptbefore : ind + ptafter);
        
    elseif  ismember(tr,find(sound)) ||  ismember(tr,find(both))
        ind = find(raw(audioCH,:) > Args.threshold);
        ntrial = raw(:,ind(1) - ptbefore : ind(1) + ptafter);
        
    end
    
    if exist('ntrial','var')
        time2= [1:size(ntrial,2)] / samplingRate;
        if Args.inspect
            subplot(2,1,1)
            
            plot(time1,raw(find(olfCH + audioCH),:)')
            title(files(tr).name)
            subplot(2,1,2)
            plot(time2,ntrial(find(olfCH + audioCH),:)')
            xlabel('time (s)')
        end
        
        if Args.autosave
            raw = ntrial;
            save(files(tr).name,'raw','samplingRate','numChannels')
            display(['realigning' files(tr).name])
        else
            a = input('save 1/0 (yes/no)?');
            if a == 1
                raw = ntrial;
                save(files(tr).name,'raw','samplingRate','numChannels')
                  display(['realigning ' files(tr).name])
            end
        end
        clear ntrial
        
    end
end
