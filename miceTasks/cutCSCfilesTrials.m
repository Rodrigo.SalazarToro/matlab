function cutCSCfilesTrials(basename,channels,timing,varargin)
% timing in samples
% 'triallength in ms
Args = struct('triallength',1501,'startnumering',1,'extension','ncs','zeropad',0,'fromEnd',0,'changePolarity',0);
Args.flags = {'zeropad','fromEnd','changePolarity'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});
if Args.zeropad
    [timestamps,nrBlocks,nrSamples,samplingRate,isContinous,header] = getRawCSCTimestamps(sprintf('CSC%02g.%s',channels(1),Args.extension));
else
    [timestamps,nrBlocks,nrSamples,samplingRate,isContinous,header] = getRawCSCTimestamps(sprintf('CSC%d.%s',channels(1),Args.extension));
    
end

if Args.fromEnd
    firstS = floor(timing(1)/512) - floor(((Args.triallength/1000) * samplingRate) / 512);
    lastS = floor(timing(1)/512);
else
    firstS = floor(timing(1)/512);
    lastS = floor(timing(1)/512) + floor(((Args.triallength/1000) * samplingRate) / 512);
end
if Args.zeropad
    [~,traw] = getRawCSCData(sprintf('CSC%02g.%s',channels(1),Args.extension),firstS,lastS);
else
    [~,traw] = getRawCSCData(sprintf('CSC%d.%s',channels(1),Args.extension),firstS,lastS);
    
end
numChannels = length(channels);
for tr = 1 : length(timing)
    filename = sprintf('%s.%0.4d',basename,Args.startnumering + tr -1);
    
    raw = nan(length(channels),length(traw));
    for ch = 1 : numChannels
        csc = channels(ch);
        if Args.fromEnd
            firstS = floor(timing(tr)/512) - floor(((Args.triallength/1000) * samplingRate) / 512);
            lastS = floor(timing(tr)/512);
        else
            firstS = floor(timing(tr)/512);
            lastS = floor(timing(tr)/512) + floor(((Args.triallength/1000) * samplingRate) / 512);
        end
        if Args.zeropad
            [~,traw] = getRawCSCData(sprintf('CSC%02g.%s',csc,Args.extension),firstS,lastS);
        else
            [~,traw] = getRawCSCData(sprintf('CSC%d.%s',csc,Args.extension),firstS,lastS);
            
        end
        if Args.changePolarity; raw(ch,:) = -traw;else; raw(ch,:) = traw; end
    end
    display(['Saving ' pwd '/' filename])
    save(filename,'raw','samplingRate','numChannels')
end