function cutCSCfilesTrialsFromTimeStamp(basename,channels,timing,varargin)

Args = struct('triallength',1501,'startnumering',1,'extension','ncs','zeropad',0,'fromEnd',0);
Args.flags = {'zeropad','fromEnd'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});
if Args.zeropad
    [timestamps,nrBlocks,nrSamples,samplingRate,isContinous,header] = getRawCSCTimestamps(sprintf('CSC%02g.%s',channels(1),Args.extension));
else
    [timestamps,nrBlocks,nrSamples,samplingRate,isContinous,header] = getRawCSCTimestamps(sprintf('CSC%d.%s',channels(1),Args.extension));
    
end
stampsperS = (timestamps(end) - timestamps(1)) / nrSamples;
if Args.fromEnd
    firstS = round(samplingRate * ((timing(1) - Args.triallength) / 1000) * stampsperS)  + timestamps(1) ;
    lastS = round(samplingRate * ((timing(1)) / 1000) * stampsperS) + timestamps(1);
else
    firstS = round(samplingRate * ((timing(1)) / 1000) * stampsperS) + timestamps(1);
    lastS = round(samplingRate * ((timing(1) - Args.triallength) / 1000) * stampsperS)  + timestamps(1) ;
end
if Args.zeropad
    [~,traw] = getRawCSCData(sprintf('CSC%02g.%s',channels(1),Args.extension),firstS,lastS);
else
    [~,traw] = getRawCSCData(sprintf('CSC%d.%s',channels(1),Args.extension),firstS,lastS);
    
end
numChannels = length(channels);
for tr = 1 : length(timing)
    filename = sprintf('%s.%0.4d',basename,Args.startnumering + tr -1);
    
    raw = nan(length(channels),length(traw));
    for ch = 1 : numChannels
        csc = channels(ch);
        if Args.fromEnd
            firstS = round(samplingRate * ((timing(tr) - Args.triallength) / 1000) * stampsperS)  + timestamps(1) ;
            lastS = round(samplingRate * ((timing(tr)) / 1000) * stampsperS) + timestamps(1);
        else
            firstS = round(samplingRate * ((timing(tr)) / 1000) * stampsperS) + timestamps(1);
            lastS = round(samplingRate * ((timing(tr) - Args.triallength) / 1000) * stampsperS)  + timestamps(1) ;
        end
        if Args.zeropad
            [~,traw] = getRawCSCData(sprintf('CSC%02g.%s',csc,Args.extension),firstS,lastS);
        else
            [~,traw] = getRawCSCData(sprintf('CSC%d.%s',csc,Args.extension),firstS,lastS);
            
        end
        raw(ch,:) = -traw;
    end
    display(['Saving ' pwd '/' filename])
    save(filename,'raw','samplingRate','numChannels')
end