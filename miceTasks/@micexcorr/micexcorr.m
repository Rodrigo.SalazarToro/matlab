function obj = micexcorr(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedLFP',[]);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'micexcorr';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mxcor';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) && isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [~,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                && (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

end
function obj = createObject(Args,varargin)
sdir = pwd;
[light,odor,sound,both] = getTrialType;
if ~isempty(nptDir('sort'))
    cd sort
    
    if  ~isempty(nptDir('xcorrSpikes.mat'))
        load xcorrSpikes.mat
        ncounts = size(pcomb,1);
        data.Index(1:ncounts,1) = 1 : ncounts;
        data.Index(1:ncounts,2) = 1 : ncounts;
      
        gline = findstr(unitname{pcomb(1,2)},'group');
        cline  = findstr(unitname{pcomb(1,2)},'cluster');
        
        for pair = 1 : ncounts
            gr1 = str2double(unitname{pcomb(pair,1)}(gline+5:60));
            gr2 = str2double(unitname{pcomb(pair,2)}(gline+5:60));
            cl1 = str2double(unitname{pcomb(pair,1)}(cline+7:end-1));
            cl2 = str2double(unitname{pcomb(pair,2)}(cline+7:end-1));
            
            ua1 = unitname{pcomb(pair,1)}(end);
            ua2 = unitname{pcomb(pair,2)}(end);
            data.Index(pair,3) = gr1;
            data.Index(pair,4) = gr2;
            data.Index(pair,5) = cl1;
            data.Index(pair,6) = cl2;
            data.Index(pair,7) = ua1;
            data.Index(pair,8) = ua2;
              data.setNames{pair} = pwd;
        end
        data.numSets = ncounts; % nbr of trial
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('empty object \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('empty object \n');
    obj = createEmptyObject(Args);
end
cd(sdir)
end
function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
end