function obj = plot(obj,varargin)


Args = struct('psthbin',50,'maxtime',5000);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});


if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
% display(obj.data.Index(ind,28:37))

%% plotting fct

load xcorrSpikes.mat

cats = {'light' 'odor' 'sound' 'odor + sound'};
for ttype = 1 : length(cats)
    subplot(length(cats)+1,1,ttype)
    plot(bins,squeeze(cumcoi(ttype,obj.data.Index(n,2),:)))
    title(cats{ttype})
end
subplot(length(cats)+1,1,ttype+1)
  plot(bins,squeeze(sum(cumcoi(2:length(cats),obj.data.Index(n,2),:),1)))

  xlabel('time lag (ms)')
    ylabel('# of spikes')
% set(gcf,'Name',[obj.data.setNames{ind} 'group ' num2str(obj.data.Index(n,3)) ' cluster ' num2str(obj.data.Index(n,[5 7]))  ' & group '  num2str(obj.data.Index(n,4)) ' cluster ' num2str(obj.data.Index(n,[6 8]))]);
 gline = findstr(unitname{pcomb(obj.data.Index(n,2),1)},'group');
set(gcf,'Name',[obj.data.setNames{ind} ' ' unitname{pcomb(obj.data.Index(n,2),1)}(gline:end) ' ' unitname{pcomb(obj.data.Index(n,2),2)}(gline:end)])
%%
