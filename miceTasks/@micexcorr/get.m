function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'unit',[]);
Args.flags = {'Number','ObjectLevel','bothRules'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
   
    if ~isempty(Args.unit)
         ttemp = find(obj.data.Index(:,2) == strmatch(Args.unit,{'s' 'm'}));
    else
         ttemp = [1 : size(obj.data.Index,1)];
    end
       
    varargout{1} = ttemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

