function [conds,nseq,condLight,condSound,varargout] = adaptStimSeq(varargin)

Args = struct('save',0,'redo',0,'addName',[]);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

thefile = nptDir('*.stim.mat');

sdir = pwd;

savename = sprintf('%snewSeq.mat',Args.addName);

if isempty(nptDir(savename)) || Args.redo
    load(thefile.name);
    cd rejectedTrials\
    
    rfiles = nptDir('*.0*');
    rtrials = nan(length(rfiles),1);
    for ff = 1 : length(rfiles)
        rtrials(ff) = str2double(rfiles(ff).name(12:end));
    end
    
    c = 1;
    seq = cell(length(S) - length(rtrials),1);
    for tr = 1 : length(S)
        if isempty(find(tr == rtrials))
            seq{c} = S(tr).Odor{1};
            c = c + 1;
        end
    end
    conds = unique(seq');
    
    condLight = ~cellfun(@isempty,strfind(conds,'Light'));
    condSound = ~cellfun(@isempty,strfind(conds,'Sound'));
    
    nseq = zeros(length(seq),1);
    for c = 1 : length(conds)
        nseq = nseq + c * strcmpi(conds{c},seq);
    end
    cd(sdir)
    if Args.save
        
        save(savename,'conds','nseq','condLight','condSound') ;
        display(['saving ' sdir filesep savename])
    end
else
    load(savename)
end


