function processNLXSession(varargin)
% to run in the folder where the CSC files are

Args = struct('nchannel',37,'dir',[],'filename',[]','redo',0);
Args.flags = {'redo'};
[Args] = getOptArgs(varargin,Args,'remove',{});
cygwinpath = 'c:\cygwin64\bin\';
pc = ispc;
sdir = pwd;
if ~isempty(Args.dir)
    
    sdir = Args.dir;
end

if isempty(Args.filename)
    [~,filename,~] = fileparts(sdir);
else
    filename = Args.filename;
end

if isempty(nptDir('*.dat')) || Args.redo
    display(['Processing CSC file for dat ' sdir filesep filename]);
    command = sprintf('nl2dat CSC `count 1 %d` %s%s%s.dat',Args.nchannel,sdir,filesep,filename);
    % nl2dat CSC `count 1 35` ~/Desktop/test/test.dat
    if pc
        system([cygwinpath command])
        dos([cygwinpath 'touch movedfiles.txt']);
    else
        unix(command)
        unix('touch movedfiles.txt')
    end
else
    display([pwd '/' filename ' already processed'])
end
