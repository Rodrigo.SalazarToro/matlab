function preprocessingPolytrode(dayname,varargin)
%
Args = struct('polytrode','buzsaki64_A64','redo',0,'resampling',200,'startsession',1,'addNameLFP',[]);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

%% moving files
sdir = pwd;
foldersd = nptDir('201*-*');

if ~isempty(foldersd)
    for fd = 1 : length(foldersd)
        newfold = sprintf('session%0.02d',fd);
        mkdir(newfold)
        
        movefile(sprintf('%s%s%s%s*',sdir,filesep,foldersd(fd).name,filesep),sprintf('%s%s%s%s',sdir,filesep,newfold,filesep))
        rmdir(foldersd(fd).name)
    end
    
end

%% create the prob file and the shank .dat
sessions = nptDir('session*');
sdir = pwd;
for s = Args.startsession : length(sessions)
    
    cd(sessions(s).name)
    if isempty(nptDir('preprocessed.txt')) || Args.redo
        basename = sprintf('%s%0.02d',dayname,s);
        switch Args.polytrode
            case {'buzsaki64-A64old'}
                prbfile = 'Buzsaki64-A64oneshank.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki64-A64oneshank.prb',prbfile)
                prbfile = 'Buzsaki64-A64oneshank.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki64-A64oneshank.prb',prbfile)
                shanks = {[1 8 2 7 3 6 4 5], [9 16 10 15 11 14 12], [17 24 18 23 19 22 20 21], [25 32 26 31 27 30 28 29], [33 40 34 39 35 38 36 37], [41 48 42 47 43 46 44 45], [49 56 50 55 51 54 52 53], [57 64 58 63 59 62 60 61]};
%                 shanks = {[1 : 8], [9 : 16], [17 : 24], [25 : 32], [33 : 40], [41 : 48], [49 : 56], [57 : 64]};
                for sh = 1 : 8
                    destprmfile = sprintf('%s%s%s%s%sshank%d.prm',sdir,filesep,sessions(s).name,filesep,basename,sh);
                    copyfile(sprintf('F:%ssalazar%selectrophyTech%sshank%d.prm',filesep,filesep,filesep,sh),destprmfile)
                    fid = fopen(destprmfile,'r+');
                    fseek(fid,0,'bof');
                    fprintf(fid,'experiment_name = ''%s''',sprintf('%sshank%d',basename,sh));
                    fclose(fid);
                end
            case {'buzsaki64-A64'}
                prbfile = 'Buzsaki64-A64.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki64-A64.prb',prbfile)
                prbfile = 'Buzsaki64-A64.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki64-A64.prb',prbfile)
                shanks = {[1 : 8], [9 : 16], [17 : 24], [25 : 32], [33 : 40], [41 : 48], [49 : 56], [57 : 64]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%sBuzsaki64-A64.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'1x32polyold'}
                prbfile = '1x32poly.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly.prb',prbfile)
                shanks = {[1 2 3 4 5 6 7 8 9 10 13 12 23 14 25 18 21 16 27 22 19 20 29 26 17 24 31 30 15 28 11]};
                %                                  shanks = {[1 2 3 4 5 6 7 8 9 10 13 12 23 14 25 18 21 16 27 22 19 20 29 26 17 24 31 30 15 28 11 32]};
                %                 shanks = {[1:31]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%s1x32poly.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'1x32poly'}
                prbfile = '1x32poly.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly.prb',prbfile)
                shanks = {[1 :32]};
                %                                  shanks = {[1 2 3 4 5 6 7 8 9 10 13 12 23 14 25 18 21 16 27 22 19 20 29 26 17 24 31 30 15 28 11 32]};
                %                 shanks = {[1:31]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%s1x32poly.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'1x32poly2_Buzsaki64L-A64old'}
                prbfile = '1x32poly2_Buzsaki64L-A64.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly2_Buzsaki64L-A64.prb',prbfile)
                shanks = {[1:32], [1 8 2 7 3 6 4 5]+32, [9 16 10 15 11 14 12 13]+32, [17 24 18 23 19 22 20 21]+32, [25 32 26 31 27 30 28 29]+32, [33 40 34 39 35 38 36 37]+32, [41 48 42 47 43 46 44 45]+32, [49 56 50 55 51 54 52 53]+32, [57 64 58 63 59 62 60 61]+32};
                
                for sh = 1 : length(shanks)
                    destprmfile = sprintf('%s%s%s%s%sshank%d.prm',sdir,filesep,sessions(s).name,filesep,basename,sh);
                    copyfile(sprintf('F:%ssalazar%selectrophyTech%sshank%d.prm',filesep,filesep,filesep,sh),destprmfile)
                    fid = fopen(destprmfile,'r+');
                    fseek(fid,0,'bof');
                    fprintf(fid,'experiment_name = ''%s''',sprintf('%sshank%d',basename,sh));
                    fclose(fid);
                end
                
            case {'1x32poly2_Buzsaki64L-A64'}
                prbfile = '1x32poly2_Buzsaki64L-A64.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly2_Buzsaki64L-A64.prb',prbfile)
                shanks = {[1:32], [1 : 8]+32, [9 : 16]+32, [17 : 24]+32, [25 : 32]+32, [33 : 40]+32, [41 : 48]+32, [49 : 56]+32, [57 : 64]+32};
                
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%s1x32poly2_Buzsaki64L-A64.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'buzsaki32-A32'}
                prbfile = 'Buzsaki32-A32.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki32-A32.prb',prbfile)
                prbfile = 'Buzsaki32-A32.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\Buzsaki32-A32.prb',prbfile)
                shanks = {[1 : 8], [9 : 16], [17 : 24], [25 : 32]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%sBuzsaki32-A32.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'chronic16'}
                %prbfile = 'chronic16.prb';
                %copyfile('F:\salazar\electrophyTech\probes-master\chronic16.prb',prbfile)
                prbfile = 'chronic16.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\chronic16.prb',prbfile)
                shanks = {[1 : 16]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%schronic16.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'1x32poly3old'}
                prbfile = '1x32poly3.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly3.prb',prbfile)
                %                 shanks = {[1 :32]};
                % shanks = {[1 17 2 9 23 5 28 11 8 10 29 15 13 14 12 21 7 6
                % 16 20 18 22 19 24 25 26 3 27 31 30 4 32]};% first version
                %                 shanks = {[11 9 7 5 3 1 2 4 6 8 10 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]};
                shanks = {[2 17 3 30 20 9 7 14 6 10 23 17 13 11 12 16 26 21 19 8 18 22 29 27 25 5 24 28 1 4 31 32]};% this order was based on the A/D neuronexus probe mapping excel spreadsheet (Neuralynx_NeuronexusChannels.xlsx)
                %However a visual inspection on klustaviewa revealed that
                %the middle column was inverted.
                
                %thus
                shanks = {[32 17 3 31 20 9 28 14 6 25 23 17 22 11 12 19 26 21 16 8 18 13 29 27 10 5 24 7 1 4 30 2]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%s1x32poly3.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
            case {'1x32poly3'}
                prbfile = '1x32poly3.prb';
                copyfile('F:\salazar\electrophyTech\probes-master\neuronexus\1x32poly3.prb',prbfile)
                %                 shanks = {[1 :32]};
                % shanks = {[1 17 2 9 23 5 28 11 8 10 29 15 13 14 12 21 7 6
                % 16 20 18 22 19 24 25 26 3 27 31 30 4 32]};% first version
                %                 shanks = {[11 9 7 5 3 1 2 4 6 8 10 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32]};
                shanks = {[1:32]};
                destprmfile = sprintf('%s%s%s%s%s0%d.prm',sdir,filesep,sessions(s).name,filesep,dayname,s);
                copyfile(sprintf('F:%ssalazar%selectrophyTech%s1x32poly3.prm',filesep,filesep,filesep),destprmfile)
                fid = fopen(destprmfile,'r+');
                fseek(fid,0,'bof');
                fprintf(fid,'experiment_name = ''%s''',sprintf('%s0%d',dayname,s));
                fclose(fid);
        end
        switch Args.polytrode
            case {'buzsaki64-A64old'}
                makeShankdat(basename,shanks,modvarargin{:},'shankSplit')
            otherwise
                makeShankdat(basename,shanks,modvarargin{:})
        end
        save('preprocessed.txt')
    end
    cd(sdir)
end


%% makes the lfp files

sessions = nptDir('session*');
sdir = pwd;
for s = 1 : length(sessions)
    
    cd(sessions(s).name)
    if isempty(nptDir('lfpprocessed.txt')) || Args.redo
        
        switch Args.polytrode
            case {'buzsaki64-A64old'}
                
                shanks = {[1 8 2 7 3 6 4 5], [9 16 10 15 11 14 12], [17 24 18 23 19 22 20 21], [25 32 26 31 27 30 28 29], [33 40 34 39 35 38 36 37], [41 48 42 47 43 46 44 45], [49 56 50 55 51 54 52 53], [57 64 58 63 59 62 60 61]};
            case {'buzsaki64-A64'}
                
                shanks = {[1 : 8], [9 : 16], [17 : 24], [25 : 32], [33 : 40], [41 : 48], [49 : 56], [57 : 64]};
            case {'buzsaki32-A32'}
                shanks = {[1 : 8], [9 : 16], [17 : 24], [25 : 32]};
                
            case  {'1x32buzsaki' '1x32poly'}
                
                shanks = {[1:32]};
            case {'1x32poly3'}
                shanks = {[1 17 2 9 23 5 28 11 8 10 29 15 13 14 12 21 7 6 16 20 18 22 19 24 25 26 3 27 31 30 4 32]};
                
                
            case {'1x32poly2_Buzsaki64L-A64old'}
                shanks = {[1:32], [1 8 2 7 3 6 4 5]+32, [9 16 10 15 11 14 12 13]+32, [17 24 18 23 19 22 20 21]+32, [25 32 26 31 27 30 28 29]+32, [33 40 34 39 35 38 36 37]+32, [41 48 42 47 43 46 44 45]+32, [49 56 50 55 51 54 52 53]+32, [57 64 58 63 59 62 60 61]+32};
            case {'1x32poly2_Buzsaki64L-A64'}
                shanks = {[1:32], [1 : 8]+32, [9 : 16]+32, [17 : 24]+32, [25 : 32]+32, [33 : 40]+32, [41 : 48]+32, [49 : 56]+32, [57 : 64]+32};
            case {'chronic16'}
                shanks = {[1 : 16]};
                
        end
        [~,~,nrSamples,samplingRate,isContinous,~] = getRawCSCTimestamps('CSC1.ncs');
        nrSamples = ceil((nrSamples / samplingRate)* Args.resampling);
        for sh = 1 : length(shanks)
            lfp = zeros(length(shanks{sh}),nrSamples);
            for el = 1 : length(shanks{sh})
                [~,lfptemp] = getRawCSCData(sprintf('CSC%d.ncs',shanks{sh}(el)),1,1,1);
                [lfptemp2,resampling] = preprocessinglfp(lfptemp,'SR',samplingRate,'downSR',Args.resampling,'No60Hz','detrend','Zscore');
                if length(lfptemp2) == size(lfp,2)
                    lfp(el,:) = lfptemp2;
                elseif length(lfptemp2) == (size(lfp,2) - 1)
                    lfp(el,1:end-1) = lfptemp2;
                end
            end
            lfp = single(lfp);
            sname = sprintf('%s%0.02dlfp%d_group%0.02d.mat',dayname,s,Args.addNameLFP,sh);
            save(sname,'lfp','resampling')
            display(['saving ' pwd '\' sname])
        end
        save('lfpprocessed.txt')
    end
    cd(sdir)
end




