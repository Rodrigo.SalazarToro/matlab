function rearrangeTrials(varargin)


files = nptDir('*.0*');

load rejectedTrials.mat
sdir = pwd;
mkdir('rejectedTrials')


for ff = 1 : length(files)
    if ~isempty(find(ff == rejectedTrials))
        movefile(files(ff).name,[sdir filesep 'rejectedTrials' filesep files(ff).name])
        display(['moving' files(ff).name ' to rejected folder'])
    end
end
nfiles = nptDir('*.0*');
basename = files(ff).name(1:11);
count = 1;
for ff = 1 : length(nfiles)
    nname = sprintf('%s%s%s%04d',sdir, filesep,basename,count);
    if ~strcmp([sdir filesep nfiles(ff).name],nname)
        movefile(nfiles(ff).name,nname)
        display(['moving' nfiles(ff).name ' to ' nname])
       
    end
     count = count + 1;
end
