function readAMfile(finame, varargin)

Args = struct('EPM',0, 'OF',0, 'NORT',0, 'FC',0);
Args.flags = {'EPM', 'OF', 'NORT', 'FC'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});




allmeandist = []; % used to create matrix containing the means of distance time mob and time immob for each treatment over several days (1 line = 1 day)
alldaysem = [];

%%
for d = 1:length(finame) % in case several days are processed (must be same type of test)
    filename = strcat(finame{d},'.xlsx');
    tab1= readtable(filename); %convert to matlab-readable table
    
    
    treat = tab1.Treatment;
    treatarg = unique(treat);
    means = [];
    allmeans = [];
    sems = [];
    sems2 = [];
    
    %convert treatment names to numbers, not useful since find() works
    
    % for ii = 1:length(treat);
    %     for iii = 1:length(treatarg);
    %         if isequal(treat{ii}, treatarg{iii});
    %         treat{ii} = iii;
    %         end
    %     end
    % end
    %
    % treat = cell2mat(treat)
    
    
    
    
    dist = tab1.Distance;
    mob = tab1.TimeMobile;
    imm = tab1.TimeImmobile;
    vars = [dist mob imm];
    varnames = {'Distance Totale' 'Time Mobile' 'Time Immobile'};
    
    
    %for loop that creates matrix containing means of distance for each day
%     if length(finame) > 1
%         meandistance = [];
%         st2 = [];
%   
%         for xx = 1:length(vars)
%             curvr = vars(:, xx);
%         for g = 1:length(treatarg)          
%             curst = curvr(find(strcmp(treatarg{g},treat)==1));
%             meand = mean(curst)
%             meandistance = horzcat(meandistance, meand)
%             st = std(curst)/sqrt(length(curst))
%             st2 = cat(1, st2, st)
%         end
%         allmeandist = vertcat(allmeandist, meandistance)
%         alldaysem = vertcat(alldaysem, st2)
%         end
%     end
    
    
    for im = 1:length(vars(1,:)) % this for loop creates a matrix containing the means for each parameteres (distance, entries etc) and each treatment (param x treatment format)
        curm = vars(:,im);
        for v = 1:length(treatarg);
            means(:,v) = mean(curm(find(strcmp(treatarg{v},treat)==1)));
        end
        allmeans = vertcat(allmeans, means)
    end
    
    if length(finame) > 1
        allmeandist = vertcat(allmeandist, allmeans)
    end
    
    for iv = 1:length(allmeans(:,1)) % this for loop gives a matrix containing the sems for each parameters(distance, entries etc) for each ttreatment (treat x param format)
        var1 = vars(:,iv);
        for vv = 1:length(treatarg)
            var2 = var1(find(strcmp(treatarg{vv},treat)==1));
            sem1 = std(var2)/sqrt(length(var2));
            sems = cat(1,sems, sem1);
        end
        sems2 = horzcat(sems2, sems);
        sems = [];
    end
    
    if length(finame) > 1
        alldaysem = vertcat(alldaysem, sems2)
    end
    
    figure1 = figure;
    for iv = 1:length(allmeans(:,1)) % this for loop creates the figures.
        %     maxval1 = max(max(sems2));
        %     minval1 = min(min(sems2));
        %     maxval = max(max(allmeans(iv,:)))+maxval1;
        %     minval = min(min(allmeans(iv,:)))+minval1;
        subplot(1,3,iv);
        bar(allmeans(iv,:),0.6,'EdgeColor',[0 0 0], 'FaceColor', [0.7 0.7 0.7])
        hold on
        errorbar(allmeans(iv,:), sems2(:,iv), '.', 'color' , [0 0 0])
        set(gca,'XTick',[1 : length(treatarg)])
        set(gca,'XTickLabel',treatarg)
        title(varnames{iv})
        %ylim([minval maxval])
        hold on
    end
    savename = strcat(finame{d}, 'FIG');
    saveas(figure1, savename)
    
%% Detailed parameters of the OF can be obtained by calling the 'OF' flag 
    
    if Args.OF
        OFmeans = [];
        OFallmeans = [];
        OFsems = [];
        OFsems2 = [];
        distout = tab1.Outer_distance;
        timeout = tab1.Outer_time;
        outent = tab1.Outer_entries;
        outmob = tab1.Outer_timeMobile;
        outspeed = tab1.Outer_averageSpeed;
        inent = tab1.Inner_entries;
        intime = tab1.Inner_time;
        indis = tab1.Inner_distance;
        inlat = tab1.Inner_latencyToFirstEntry;
        inspeed = tab1.Inner_averageSpeed;
        inmob = tab1.Inner_timeMobile;
        
        OFvars = [outent timeout distout outmob inent intime indis inmob ];
        OFvarnames = {'Outer : Entries' 'Outer : Time' 'Outer : Distance' 'Outer : Time Mobile'  'Inner : Entries' 'Inner : Time' 'Inner : Distance' 'Inner : Time Mobile'};
        
        for im = 1:length(OFvars(1,:)) % this for loop creates a matrix containing the means for each parameteres (distance, entries etc) and each treatment (param x treatment format)
            curm = OFvars(:,im);
            for v = 1:length(treatarg);
                OFmeans(:,v) = mean(curm(find(strcmp(treatarg{v},treat)==1)));
            end
            OFallmeans = vertcat(OFallmeans, OFmeans);
        end
        
        for iv = 1:length(OFallmeans(:,1)) % this for loop gives a matrix containing the sems for each parameters(distance, entries etc) for each ttreatment (treat x param format)
            OFvar1 = OFvars(:,iv);
            for vv = 1:length(treatarg)
                OFvar2 = OFvar1(find(strcmp(treatarg{vv},treat)==1));
                OFsem1 = std(OFvar2)/sqrt(length(OFvar2));
                OFsems = cat(1,OFsems, OFsem1);
            end
            OFsems2 = horzcat(OFsems2, OFsems);
            OFsems = [];
        end
        
        figure2 = figure;
        for iv = 1:length(OFallmeans(:,1)) % this for loop creates the figures.
            subplot(2,4,iv);
            %OFmaxval = max(max(OFallmeans(iv,:)))*1.2; %does not work!!
            %OFminval = min(min(OFallmeans(iv,:)))*1.2;
            bar(OFallmeans(iv,:),0.6,'EdgeColor',[0 0 0], 'FaceColor', [0.7 0.7 0.7])
            hold on
            errorbar(OFallmeans(iv,:), OFsems2(:,iv), '.', 'color' , [0 0 0])
            set(gca,'XTick',[1 : length(treatarg)])
            set(gca,'XTickLabel',treatarg)
            title(OFvarnames{iv})
            %ylim([OFminval OFmaxval])
        end
        OFsavename = strcat(finame{d}, 'OF_FIG');
        saveas(figure2, OFsavename)
    end
    
%% Detailed parameters of the EPM can be obtained by calling the 'EPM' flag 
    
    if Args.EPM
        Emeans = [];
        Eallmeans = [];
        Esems = [];
        Esems2 = [];
        distcl = tab1.Closed_distance;
        timecl = tab1.Closed_time;
        clmob = tab1.Closed_timeMobile;
        openent = tab1.Open_entries;
        openmob = tab1.Open_timeMobile;
        opentime = tab1.Open_time;
        opendis = tab1.Open_distance;
        openlat = tab1.Open_latencyToFirstEntry;
        
        
        Evars = [timecl distcl clmob openent opentime opendis openmob openlat];
        Evarnames = {'Closed : Time' 'Closed : Distance' 'Closed : Time Mobile' 'Open : Entries' 'Open : Time' 'Open : Distance' 'Open : Time Mobile' 'Open : Latency'};
        
        for im = 1:length(Evars(1,:)) % this for loop creates a matrix containing the means for each parameteres (distance, entries etc) and each treatment (param x treatment format)
            curm = Evars(:,im);
            for v = 1:length(treatarg);
                Emeans(:,v) = mean(curm(find(strcmp(treatarg{v},treat)==1)));
            end
            Eallmeans = vertcat(Eallmeans, Emeans);
        end
        
        for iv = 1:length(Eallmeans(:,1)) % this for loop gives a matrix containing the sems for each parameters(distance, entries etc) for each ttreatment (treat x param format)
            Evar1 = Evars(:,iv);
            for vv = 1:length(treatarg)
                Evar2 = Evar1(find(strcmp(treatarg{vv},treat)==1));
                Esem1 = std(Evar2)/sqrt(length(Evar2));
                Esems = cat(1,Esems, Esem1);
            end
            Esems2 = horzcat(Esems2, Esems);
            Esems = [];
        end
        
        figure3 = figure;
        for iv = 1:length(Eallmeans(:,1)) % this for loop creates the figures.
            subplot(2,4,iv);
            %OFmaxval = max(max(OFallmeans(iv,:)))*1.2;
            %OFminval = min(min(OFallmeans(iv,:)))*1.2;
            bar(Eallmeans(iv,:),0.6,'EdgeColor',[0 0 0], 'FaceColor', [0.7 0.7 0.7])
            hold on
            errorbar(Eallmeans(iv,:), Esems2(:,iv), '.', 'color' , [0 0 0])
            set(gca,'XTick',[1 : length(treatarg)])
            set(gca,'XTickLabel',treatarg)
            title(Evarnames{iv})
            %ylim([OFminval OFmaxval])
            
        end
        Esavename = strcat(finame{d}, 'EPM_FIG');
        saveas(figure3, Esavename)
    end
    
    
%% Detailed parameters of the NORT can be obtained by calling the 'NORT' flag 
    
    % if Args.NORT
    % Nmeans = [];
    % Nallmeans = [];
    % Nsems = [];
    % Nsems2 = [];
    % leftnum = tab1.LeftObject_number;
    % leftpress = tab1.LeftObject_timePressed;
    % rightnum = tab1.RightObject_number;
    % rightpress = tab1.RightObject_timePressed;
    % nov1 = [];
    % nov = [];
    % old1 = [];
    % old = [];
    %
    %
    % %% used to find the variables for the novel and old object (left or right)
    % tests = find(strcmp(tab1.Stage,'Test stage')==1);
    % for nn = 1:length(tests)
    %     if strcmp(tab1.NOposition(tests(nn),:), 'L')==1
    %         non = leftnum(tests(nn),:);
    %         nop = leftpress(tests(nn),:);
    %         oldn = rightnum(tests(nn),:);
    %         oldp = rightpress(tests(nn),:);
    %     end
    %
    %     if strcmp(tab1.NOposition(tests(nn),:), 'R')==1
    %         non = rightnum(tests(nn),:);
    %         nop = rightpress(tests(nn),:);
    %         oldn = leftnum(tests(nn),:);
    %         oldp = leftpress(tests(nn),:);
    %     end
    %     nov1 = [non nop]
    %     nov = vertcat(nov, nov1)
    %     old1 = [oldn oldp]
    %     old = vertcat(old, old1)
    % end
    %
    % Nvars = [leftnum leftpress rightnum rightpress];
    % Nobj = [nov old]
    % Nvarnames = {'Left : presses' 'Left : time pressed' 'Right : presses' 'Right : time pressed'};
    % 'Novel object : Entries' 'Novel object : time' 'Old object : Entries' 'Old object : time'
    %
    %
    % for im = 1:length(Nvars(1,:)) % this for loop creates a matrix containing the means for each parameteres (distance, entries etc) and each treatment (param x treatment format)
    %     curm = Nvars(:,im);
    %         for v = 1:length(treatarg);
    %         Nmeans(:,v) = mean(curm(find(strcmp(treatarg{v},treat)==1)));
    %         end
    %     Nallmeans = vertcat(Nallmeans, Nmeans);
    % end
    %
    % for iv = 1:length(Nallmeans(:,1)) % this for loop gives a matrix containing the sems for each parameters(distance, entries etc) for each ttreatment (treat x param format)
    %     Evar1 = Nvars(:,iv);
    %     for vv = 1:length(treatarg)
    %         Evar2 = Evar1(find(strcmp(treatarg{vv},treat)==1));
    %         Esem1 = std(Evar2)/sqrt(length(Evar2));
    %         Nsems = cat(1,Nsems, Esem1);
    %     end
    %     Nsems2 = horzcat(Nsems2, Nsems);
    %     Nsems = [];
    % end
    %
    % figure3 = figure;
    % for iv = 1:length(Nallmeans(:,1)) % this for loop creates the figures.
    %     subplot(2,4,iv);
    %     %OFmaxval = max(max(OFallmeans(iv,:)))*1.2;
    %     %OFminval = min(min(OFallmeans(iv,:)))*1.2;
    %     bar(Nallmeans(iv,:),0.6,'EdgeColor',[0 0 0], 'FaceColor', [0.7 0.7 0.7])
    %     hold on
    %     errorbar(Nallmeans(iv,:), Nsems2(:,iv), '.', 'color' , [0 0 0])
    %     set(gca,'XTick',[1 : length(treatarg)])
    %     set(gca,'XTickLabel',treatarg)
    %     title(Nvarnames{iv})
    %     %ylim([OFminval OFmaxval])
    %
    % end
    % Esavename = strcat(name, 'EPM_FIG');
    % saveas(figure3, Esavename)
    % end
end

%% 
if length(finame) > 1
    days1 = [];
    figurex = figure;%plot the means of distance for each treatment and each days in one plot. useful for eg. habituation
    am = allmeandist'
    am = am(:)
    dy = length(am)/length(treatarg)
    for yy = 1:dy
        days = yy
        days1 = cat(1, days1, days)
    end
    
%     for uu = 1:length(days1)
%     namesy = strcat('Day ' , num2str(days{uu}))
%     namess = cat(1, namess, namesy)
%     end
%     
    for y = 1:length(am(:,1))
    bar(am)
    hold on
    errorbar(am, alldaysem, '.', 'color' , [0 0 0])
    %set(gca,'XTick',[1 : length(am)])
    hold on
    set(gca,'XTickLabel',treatarg)
    end
    savenx = strcat(finame{d}, 'alldays')
    saveas(figurex, savenx)
    
end
end


