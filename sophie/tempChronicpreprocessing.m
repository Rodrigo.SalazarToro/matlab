cd E:\salazar\Electrophy\CLArec\chronic
days = {  '20170817' '20170822'};

for d = 1 : 3
    cd(days{d})
    cd session01
    [Ev,iD]=OG_extract_events;unique(iD);% Ev will be in milliseconds
    
    
    labels = cell(length(iD),1);
    for ii = 1 : length(iD); labels{ii}= 'pulse';end
    SaveEvt(sprintf('%s01.LIG.evt',days{d}),Ev,labels,32000,0);
    [firstpulse,seqFreq] = getPulseSeq(Ev,'save');
    
    mindur = floor(min(diff(Ev)));
    
    endtrials = Ev(2:2:end);
    cutCSCfilesTrials(sprintf('%s01',days{d}),[1:16],32000*(endtrials/1000),'triallength',mindur,'fromEnd');
    % make the descriptor file
    ProcessSession('lowpass','highpass','highpasslow',500,'highpasshigh',15000,'extraction','threshold',3.0,'extraction','NegativePositive','sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','no'},'redo');
    cd ..;
    sessions=nptDir('session*');
    
    status = MoveProcessedFiles(sessions,1);
    % move the header files into the FD folder
    cd E:\salazar\Electrophy\CLArec\chronic
end
