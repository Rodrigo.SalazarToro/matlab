% script to find and concatenate the values of startle over the 200ms
% period of recording. the values for each trial with 0 dB prepulse, 120 dB
% pulse, for block 2, each chamber and each sessions are concatenated

days = {'20160128' '20160129' '20160201' '20160202'};
startleAll = [];
meanstartleAll = [];
miceID = [];
miceID1 = [];
doses = [];
sdir=pwd;

% Rentre dans les dossiers des jours
for d=1:length(days)
    cd([sdir filesep days{d}])
    % Rentre dans les sessions pour chaque jour
    sessions=nptDir('session*');
    for ss=1:length(sessions)
        cd(sessions(ss).name);
        pwd
        % finds wether there is a 'nomice' flag in the LabID txt file,
        % meaning one of the boxes is empty. Stores that info in 'eliminate'.
        fnameID=nptDir('*LabID.txt');
        fileID=fopen(fnameID.name,'r');
        a=textscan(fileID,'%s');
        miceID=cat(1,miceID, a{1}(1,1),a{1}(3,1)); % cr�e un vecteur qui prend les identit�s des souris
        miceID1=cat(1,miceID1, a{1}(1,1),a{1}(3,1)); % cr�e un vecteur qui prend les identit�s des souris
        doses=cat(1,doses,str2double(a{1}(2,:)),str2double(a{1}(4,:))); % cr�e un vecteur qui prend les doses inject�es
        eliminate = find(strcmp('nomice',miceID) == 1);
        eliminate1 = find(strcmp('nomice',miceID1) == 1);
        
        %finds the trial informations
        table=nptDir ('*TableTest.TAB*'); % cherche le fichier ayant l'extension .TAB
        data=nptDir('*rawdata*'); % cherche les donn�es brutes
        [infoTrial,defaultParams] = ReadInfoPPItxtFile(table.name);
        [startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(data.name);
        raw = find(strcmp('Prepulse Level',defaultParams) ==1);
        raw2 = find(strcmp('Startle Level',defaultParams) ==1);
        
        
        %finds all the trials with 0 dB prepulse and 120 dB pulse for box
        %1, sotores it in startle1
        index1= intersect(intersect(intersect(find(infoTrial(raw,:) == 0), find(infoTrial(raw2,:) == 120)), find(chamberid == 1)),find(blockid == 2));
        startle1 = startle.Startle(index1,:);
        meanstartle1 = mean(startle1);
        
        %finds all the trials with 0 dB prepulse and 120 dB pulse for box 2
        %IF eliminate is empty (as empty box is always box 2), stores it in startle2. I know, I
        %should have made it work in case any box is empty...
        empt = isempty(eliminate);
        if empt == 1
            index2= intersect(intersect(intersect(find(infoTrial(raw,:) == 0), find(infoTrial(raw2,:) == 120)), find(chamberid == 2)),find(blockid == 2));
            startle2 = startle.Startle(index2,:);
            meanstartle2 = mean(startle2);
            
            %concatenates all desired trials (12 per mice, 200 columns as there
            %is 1 sampling per ms and recording lasts 200ms
            startleAll = cat(1, startleAll, startle1, startle2);
            meanstartleAll = cat(1, meanstartleAll, meanstartle1, meanstartle2);
        else
            startleAll = cat(1, startleAll, startle1);
            meanstartleAll = cat(1, meanstartleAll, meanstartle1);
        end
        
        
        cd ..;
        eliminate = [];
        empt = [];
        miceID = [];
        
        %create matrix containing mice ID, minus the 'nomice'`
        miceID1(eliminate1) = [];
        doses(eliminate1) = [];
    end
    cd(sdir);
end
 