[r,all] = get(bothMonkeys,'Number','snr',99,'sigSur',{'beta' [3 4] [1 3] });
[r,ev] = get(bothMonkeys,'Number','snr',99);
[r,ind] = get(bothMonkeys,'Number','snr',99,'type',{'betapos34' [1 3]});

pairt = unique(bothMonkeys.data.hist(ev,:),'rows');
clear tfbeta tfgamma 
for pp = 1 : size(pairt,1)
    
    tfbeta(pp,1) = sum(ismember(bothMonkeys.data.hist(ind,:),pairt(pp,:),'rows') == 1);
    tfbeta(pp,2) = sum(ismember(bothMonkeys.data.hist(all,:), pairt(pp,:),'rows') == 1);
%      tfbeta(pp,3) = sum(ismember(bothMonkeys.data.hist(ev,:), pairt(pp,:),'rows') == 1);
end
areas = {'6DR'; '8AD' ;'8B'; 'dPFC' ;'vPFC'; 'PS'; 'AS'; 'PEC'; 'PGM'; 'PE'; 'PG'; 'MIP'; 'LIP'; 'PEcg'; 'IPS' ;'WM'; '9L'};

for pp = 1 : size(pairt,1); if pairt(pp,1) ~= 99 & pairt(pp,2) ~= 99; pareas{pp} = sprintf('%s %s',areas{pairt(pp,1)},areas{pairt(pp,2)}); end;end
[r,all] = get(bothMonkeys,'Number','snr',99,'sigSur',{'gamma' [3 4] [1 3] });
[r,ind] = get(bothMonkeys,'Number','snr',99,'type',{'gammaneg14' [1 3]});

pairt = unique(bothMonkeys.data.hist(all,:),'rows');

for pp = 1 : size(pairt,1)
    
    tfgamma(pp,1) = sum(ismember(bothMonkeys.data.hist(ind,:),pairt(pp,:),'rows') == 1);
    tfgamma(pp,2) = sum(ismember(bothMonkeys.data.hist(all,:), pairt(pp,:),'rows') == 1);
end

subplot(2,1,1)
bar(tfbeta)

subplot(2,1,2)
bar(tfgamma)

set(gca,'XTick',[1:length(pareas)]);
set(gca,'XTickLabel',pareas)

title('Gamma decrease')
ylabel('Number of pairs')
xlim([0 length(pareas)+1])
subplot(2,1,1);xlim([0 length(pareas)+1])

%%
areas = {'6DR'; '8AD' ;'8B'; 'dPFC' ;'vPFC'; 'PS'; 'AS'; 'PEC'; 'PGM'; 'PE'; 'PG'; 'MIP'; 'LIP'; 'PEcg'; 'IPS' ;'WM'; '9L'};
[r,ev] = get(bothMonkeys,'Number','snr',99);
pairt = unique(bothMonkeys.data.hist(ev,:),'rows');
for pp = 1 : size(pairt,1); if pairt(pp,1) ~= 99 && pairt(pp,2) ~= 99;pareas{pp} = sprintf('%s %s',areas{pairt(pp,1)},areas{pairt(pp,2)}); end;end
[r,all] = get(bothMonkeys,'Number','snr',99,'sigSur',{'gamma' [3 4] [1 3] });
[r,ind] = get(bothMonkeys,'Number','snr',99,'type',{'gammaneg14' [1 3]});
frange = [30:42];
clear allFx2y allFy2x nc
for pp = 1 : 21
gp = ismember(bothMonkeys.data.hist(ind,:),pairt(pp,:),'rows');
if length(find(gp==1)) > 10
[allFx2y{pp},allFy2x{pp}] =summarizeGC(bothMonkeys,ind(gp),'figTit',pareas{pp});
 nc{pp} = mean(squeeze(allFx2y{pp}(frange,:,1)),1) - mean(squeeze(allFy2x{pp}(frange,:,1)),1);
end
end


clear mv sv ; 
for pp =1 : length(nc); 
    [h,ptest] = ttest(nc{pp}); 
    if ~isempty(allFx2y{pp}) && ptest <= 0.05; 
       
        mv(pp) = mean(nc{pp});
        sv(pp) = std(nc{pp}) / length(nc{pp});
    end; 
end

figure; bar(mv); hold on;errorbar(mv,sv,'x');set(gca,'XTick',[1:length(pareas)]);set(gca,'XTickLabels',pareas)

%% 
[r,all] = get(bothMonkeys,'Number','snr',99,'sigSur',{'beta' [3 4] [1 3] });
[r,ind] = get(bothMonkeys,'Number','snr',99,'type',{'betapos34' [1 3]});
frange = [14:30];
clear allFx2y allFy2x nc
for pp = 1 : 21
gp = ismember(bothMonkeys.data.hist(ind,:),pairt(pp,:),'rows');
if length(find(gp==1)) > 10
[allFx2y{pp},allFy2x{pp}] =summarizeGC(bothMonkeys,ind(gp),'figTit',pareas{pp});
 nc{pp} = mean(squeeze(allFx2y{pp}(frange,:,4)),1) - mean(squeeze(allFy2x{pp}(frange,:,4)),1);
end
end


clear mv sv ; 
for pp =1 : length(nc); 
    [h,ptest] = ttest(nc{pp}); 
    if ~isempty(allFx2y{pp}) && ptest <= 0.05; 
       
        mv(pp) = mean(nc{pp});
        sv(pp) = std(nc{pp}) / length(nc{pp});
    end; 
end

figure; bar(mv); hold on;errorbar(mv,sv,'x');set(gca,'XTick',[1:length(pareas)]);set(gca,'XTickLabels',pareas)