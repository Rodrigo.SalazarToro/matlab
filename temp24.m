[r,ind] = get(cohC_B,'type',{'betapos34' [1 3]},'Number','sigSur',{'alpha' [1 : 4] [ 0 2]; 'gamma' [1 : 4] [ 0 2]},'sigSurPIntersect','pairhist','ld','rudeHist');
figure;[pair,nind] = mkAveCohgram(cohC_B,ind,'clim',0.2,'frange',[8 35],'varPhi','InCor');
newind = nind{1};
clear cumDiff
sdir = pwd;

c = 1;
for i = newind
    day = cohC_B.data.setNames{i};
    npair = cohC_B.data.Index(i,1);
    groups = cohC_B.data.Index(i,10:11);
    
    [session,f,vdz] = compCorInCortrials(day,npair,groups);
    
    clark = findstr(day,'clark');
    
    if isempty(clark)
        ns = 1;
    else
        cd(day)
        load rules.mat
        ns = find(r == 1);
        
    end
    cumDiff(:,:,c) = session(ns).rdiff;
    sigDiff(:,:,c) = ~session(ns).Adz;
    c = c + 1;
end