% ddir = pwd;
% parfor d = 4:16
%     
%     cd(sprintf('%s/%s',ddir,days{d}))
%     sessions = nptDir('session*');
%     for s = 1 : length(sessions)
%         cd(sprintf('%s/%s/%s',ddir,days{d},sessions(s).name))
%         sdir = pwd;
%         lfpdir = nptDir('lfp');
%         if ~isempty(lfpdir)
%             cd lfp
%             art = nptDir('rejectedTrials.mat');
%             cd(sdir)
%             if isempty(art)
%                 
%                 [stdTrials,PWTrials,rejectCH] = ArtRemTrialsLFP('save','distr','sigma','Kthresh',9);
%             end
%         end
%         cd(ddir)
%     end
% end



for d = 1 : 21
    cd(days{d})
    session = nptDir('session0*');
    
    for s = 1 : 2
        cd(session(s+1).name)
        mt = mtstrial('auto');
        allloc = unique(mt.data.CueLoc);
        allobj = unique(mt.data.CueObj);
        for dim = 1 : 2
            for item = 1 : 3
                if dim == 1
                    minTrials(d,s,dim,item) = length(mtsgetTrials(mt,'CueLoc',allloc,'CueObj',allobj(item),'BehResp',1,'stable',1));
                else
                    minTrials(d,s,dim,item) = length(mtsgetTrials(mt,'CueLoc',allloc(item),'CueObj',allobj,'BehResp',1,'stable',1));
                end
            end
            
        end
        cd ..
    end
    cd ..
    
end
dif = [];
for d = 1 : 21
   
    for s = 1 : 2
       
        for dim = 1 : 2
            
                dif = [dif max(minTrials(d,s,dim,:)) - min(minTrials(d,s,dim,:))];
            
        end
      
    end
  
    
end