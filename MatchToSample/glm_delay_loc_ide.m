function glm_output = glm_delay_loc_ide(varargin)


%prepares model for R

Args = struct('cross_loc',[],'cross_ide',[],'stim_number_loc',[],'cross_obj',[],'stim_number_obj',[],'epoch',[],'model',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);


if Args.epoch == 0
    delay = [18:34];
elseif Args.epoch == 1
    delay = [1:8];
end

points = size(delay,2);
time = [1:points];


if Args.model == 2
    all_cross = Args.cross_loc(:,delay);
    stim_numbers = Args.stim_number_loc;
elseif Args.model == 3
    all_cross = Args.cross_obj(:,delay);
    stim_numbers = Args.stim_number_obj;
end

 [s1 s2] = size(delay);

all_times = [];
for yy = 1 : 3
    asm = size(all_times,1) + 1;
    all_times = [all_times; time'];
    all_stim_numbers((asm:((asm-1)+s2)),1) = stim_numbers(yy);
    stims((asm:((asm-1)+s2)),1) = yy;
    cross((asm:((asm-1)+s2)),1)  = all_cross(yy,:);
end

glm_stimuli(:,1) = all_times;
glm_stimuli(:,2) = stims;
glm_stimuli(:,3) = cross;
glm_stimuli(:,4) = all_stim_numbers;


if Args.model == 2
    csvwrite(['glm_location.csv'],glm_stimuli,1,0)
    %run R batch
    !R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/matlab_glm_code_location.R
    fid = fopen('matlab_glm_code_location.Rout');
elseif Args.model == 3
    csvwrite(['glm_identity.csv'],glm_stimuli,1,0)
    %run R batch
    !R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/matlab_glm_code_identity.R
    %get R output and place in a structure
    fid = fopen('matlab_glm_code_identity.Rout');
end



%type: location or identity

%p-vals : DnD test for interaction model(respons~TIME+type+TIME*type) VS model(response~TIME+type)
%       : DnD test for STIM model(response~TIME+STIM) VS model(response~TIME)
%       : walds test for time model(response~TIME)

%coefficients : all three models

e=0;
while e == 0
    interaction = fgetl(fid);
    if ~isnan(str2double(interaction(5:end))) %make sure any messages that appear at top of output are not reported as p-values
        e = 1;
    end
end

glm_output.interaction = interaction(5:end)

fgetl(fid); %discard

%interaction coefficients
for ic = 1:6
    interaction_coefs{ic,1} = fgetl(fid);
    glm_output.interaction_coefs{ic,1} = interaction_coefs{ic,1}(7:end);  
end

fgetl(fid); %discard

%interaction coefficients, pval
for ic = 1:6
    interaction_coefs_pval{ic,1} = fgetl(fid);
    glm_output.interaction_coefs_pval{ic,1} = interaction_coefs_pval{ic,1}(7:end);  
end




fclose(fid);

















