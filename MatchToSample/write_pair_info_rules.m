function write_pair_info_rules


load idedays

monkeydir = pwd;

for x = 1:size(idedays,2)
    
    
    cd(idedays{x})
    daydir = pwd;
    cd session01
    sesdir = pwd;
    N = NeuronalChAssign;
    all_channels = N.groups;
    chnumb=size(N.groups,2);
    
    rules = [{'location' 'identity'}];
    for y = 1:2
        
        cd(['lfp/lfp2/' rules{y}])
        
        
        %THESE ARE THE CHANNELS USED!
        all_pairs = [];
        ap = 0;
        for ap1 = 1: chnumb
            for ap2 = (ap1+1) : chnumb
                ap = ap + 1;
                all_pairs(ap,:) = [all_channels(ap1) all_channels(ap2)]; 
            end
        end
        
        save all_channels all_channels
        save all_pairs all_pairs
        cd ..
        
        %also save in lfp2 directory
        save all_channels all_channels
        save all_pairs all_pairs
        cd(sesdir)
        
    end
    cd(monkeydir)
end