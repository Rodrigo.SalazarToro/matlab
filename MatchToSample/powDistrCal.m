function power = powDistrCal(varargin)
% to be run in any session directory



Args = struct('save',0,'redo',0);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

[pdir,cdir] = getDataDirs('session','relative','CDNow');

cd ..
cd session01/lfp

pfile = 'powerD.mat';

isthere = nptDir(pfile);

if Args.redo | isempty(isthere)


    rejectTrials = ArtRemTrialsLFP(modvarargin{:});

    trialsname = nptDir('*_lfp.*');
    ntrials = length(trialsname);

    trials = setdiff([1:ntrials],rejectTrials);

    for t = 1 : length(trials)
        [data,num_channels,sampling_rate,scan_order,points] = nptReadStreamerFile(trialsname(trials(t)).name);
        for ch = 1 : size(data,1)
            [prelfp(ch,:),SR] = preprocessinglfp(data(ch,:),modvarargin{:});
        end
        if exist('lfp') == 1
            lfp(:,:,t) = prelfp(:,1:size(lfp,2));
        else
            lfp(:,:,t) = prelfp;
        end
        clear prelfp
    end


    [power] = powerDistr(lfp,'Fs',SR,modvarargin{:});

else
    load(pfile);
end


if Args.save

    save(pfile,'power')
end

cd(cdir)
