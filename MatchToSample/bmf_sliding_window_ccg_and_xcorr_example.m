function bmf_sliding_window_ccg_and_xcorr_example

cd('/Volumes/bmf_raid/data/monkey/ethyl/110714/session01')

sesdir = pwd;

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');

incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','Nlynx');

corrtrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','Nlynx');




cd([sesdir '/lfp/lfp2'])

chpairs = nptDir('channelpair*')
npairs = size(chpairs,1);

figure

load('channelpair201248.mat')
load('channelpair018201.mat')
% % % channelpair201209.mat		
% % % channelpair201233.mat		
% % % channelpair201235.mat		
% % % channelpair201246.mat		
% % % channelpair201248.mat		
% % % channelpair201255.mat


ntrials = size(corrtrials,2);


allccgs = zeros(71,101);
for t = corrtrials %corrtrials%incorrtrials
    ccgs = correlograms{t}(1:71,:);
    
%     ccgs = correlograms{t}(end-70:end,:);
    allccgs = allccgs + ccgs;
end
allccgs = allccgs ./ ntrials; %make mean
%     imagesc(allccgs',[-.3 .3])
imagesc(allccgs')
colorbar

set(gca,'YTick',[1,25,50,75,100])
set(gca,'YTicklabel',[-50 -25 0 25 50]);

set(gca,'XTick',[1:7:65])
set(gca,'XTicklabel',[150:350:3300])
hold on
%zero phase
%im{stim}(50,:) = max_val;
plot([1 71],[50 50],'color','k')
hold on
%sample
plot([8 8],[1 101],'color','k')
hold on
%delay
plot([18 18],[1 101],'color','k')
hold on
%first match
plot([34 34],[1 101],'color','k')
hold on
%last match
plot([42 42],[1 101],'color','k')
hold on

%first iti start
%     plot([44 44],[1 101],'color','b')
plot([52 52],[1 101],'color','b')
hold on

%last iti end
%     plot([59 59],[1 101],'color','g')
plot([67 67],[1 101],'color','g')
hold on






%% 
cd(sesdir)
[~,~,gg,~,sortedpairs] = bmf_groups;
cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'identity'])

load all_gabors

% [i ii] = intersect(gg,[172 248],'rows'); %this is the example from above
[i ii] = intersect(gg,[201 248],'rows'); %this is the example from above

a1 = all_gabors{4,sortedpairs(ii)};
a2 = all_gabors{9,sortedpairs(ii)};

figure
subplot(1,2,1)
plot([-50: 50],a1.epoch_correlogram,'b')
hold on
plot([-50: 50],a1.cf,'r')
axis([-50 50 -.15 .15])
subplot(1,2,2)
plot([-50: 50],a2.epoch_correlogram,'b')
hold on
plot([-50: 50],a2.cf,'r')
axis([-50 50 -.15 .15])

cd(sesdir)