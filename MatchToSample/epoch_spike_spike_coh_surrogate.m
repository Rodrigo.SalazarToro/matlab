function epoch_spike_spike_coh_surrogate(varargin)


%run at session level
Args = struct('ml',0,'ide_only',1,'bmf',0,'rules',[],'bin_width',1,'perms',100,'redo',0);
Args.flags = {'ml','bmf','rules','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);


params = struct('tapers',[2 3],'fpass',[0 100],'Fs',1000,'trialave',1);
sesdir = pwd;
cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
    iti_reject = rejecTrials;
else
    iti_reject = [];
end

if Args.ml
    
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx'); %incorrect/identity
        
        N = NeuronalHist('bmf');
        sortedgroups = bmf_groups('ml'); %already kicks out noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames','save','redo');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(find(mt.data.CueObj == 55),get_reject_trials);
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        
        %get rid of incorrect trials with bad itis if the artifact
        %detection has been run on it
        if ~isempty(iti_reject)
            [~,baditi] = intersect(incorrtrials,iti_reject);
            incorrtrials(baditi) = [];
        end
        
        N = NeuronalHist('ml');
        noisy = noisy_groups;
        sortedgroups = sorted_groups('ml');
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    location = mtsgetTrials(mt,'BehResp',1,'stable','rule',2);
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    noisy = noisy_groups;
    [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
    [~,ii] = intersect(sortedgroups,noisy);
    sortedgroups(ii) = []; %get rid of noisy groups
    
    [~,chgroups] = intersect(N.gridPos,sortedgroups);
end
%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1 4];
        else
            rules = [2 3 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        elseif rule == 3
            trials = fixtrials;
        elseif rule == 4
            trials = incorrtrials;
        end
    else
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        end
    end
    
    rulenames = {'identity','location','fixation','incorrect'};
    
    if ~isempty(trials)
        counter = 0;
        for g = sortedgroups
            counter = counter + 1;
            fprintf('\n%0.5g     ',g)
            for gg = sortedgroups(counter + 1: end)
                fprintf(' %0.5g',gg)
                %determine if pair has already been written
                cd([sesdir filesep 'lfp' filesep 'lfp2' filesep rulenames{rule}])
                spair = ['surrogate_spikecoh_g' num2strpad(g,4) 'g' num2strpad(gg,4) '.mat'];
                if ~exist(spair,'file') && ~Args.redo
                    surrogate_spikecoh = [];
                    
                    cd(sesdir)
                    
                    cd([sesdir filesep 'group' num2strpad(g,4)])
                    gdir1 = pwd;
                    %find clusters
                    clusters1 = nptDir('cluster*');
                    nclusters1 = size(clusters1,1);
                    
                    cd([sesdir filesep 'group' num2strpad(gg,4)])
                    gdir2 = pwd;
                    %find clusters
                    clusters2 = nptDir('cluster*');
                    nclusters2 = size(clusters2,1);
                    
                    ccounter = 0;
                    for c = 1 : nclusters1
                        cd([gdir1 filesep clusters1(c).name]);
                        %get spikes
                        load ispikes.mat
                        sp1 = sp;
                        for cc = 1 : nclusters2
                            ccounter = ccounter + 1;
                            cd([gdir2 filesep clusters2(cc).name])
                            load ispikes.mat
                            sp2 = sp;
                            
                            nbins = size([-200:Args.bin_width:200],2);
                            
                            for p = 1 : Args.perms
                                tcounter = 0;
                                
                                %make two permutations of the trials
                                ntrials = size(trials,2);
                                trialperm1 = trials(randperm(ntrials));
                                trialperm2 = trials(randperm(ntrials));
                                
                                totalspikes1 = 0;
                                totalspikes2 = 0;
                                for ttrial = 1 : ntrials
                                    tcounter = tcounter + 1;
                                    s_on1 = sample_on(trialperm1(ttrial));
                                    s_off1 = sample_off(trialperm1(ttrial));
                                    m1 = match(trialperm1(ttrial));
                                    
                                    s_on2 = sample_on(trialperm2(ttrial));
                                    s_off2 = sample_off(trialperm2(ttrial));
                                    m2 = match(trialperm2(ttrial));
                                    %                             data1(ccounter).times = vecc(ceil(sp1.data.trial(ttrial).cluster.spikes)) ./ 1000; %ceil to avoid spikes a t = 0
                                    %                             data2(ccounter).times = vecc(ceil(sp2.data.trial(ttrial).cluster.spikes)) ./ 1000; %ceil to avoid spikes a t = 0
                                    
                                    spikes1 = ceil(sp1.data.trial(trialperm1(ttrial)).cluster.spikes);
                                    spikes2 = ceil(sp2.data.trial(trialperm2(ttrial)).cluster.spikes);
                                    totalspikes1 = totalspikes1 + size(spikes1,2);
                                    totalspikes2 = totalspikes2 + size(spikes2,2);
                                    
                                    %fix
                                    fix1(tcounter).times = vecc(spikes1(find(spikes1 > (s_on1 - 400) & spikes1 <= s_on1))) ./ 1000;
                                    fix2(tcounter).times = vecc(spikes2(find(spikes2 > (s_on2 - 400) & spikes2 <= s_on2))) ./ 1000;
                                    
                                    %sample
                                    sample1(tcounter).times = vecc(spikes1(find(spikes1 > (s_off1 - 400) & spikes1 <= s_off1))) ./ 1000;
                                    sample2(tcounter).times = vecc(spikes2(find(spikes2 > (s_off2 - 400) & spikes2 <= s_off2))) ./ 1000;
                                    
                                    %delay 400
                                    delay4001(tcounter).times = vecc(spikes1(find(spikes1 > s_off1 & spikes1 <= (s_off1 + 400)))) ./ 1000;
                                    delay4002(tcounter).times = vecc(spikes2(find(spikes2 > s_off2 & spikes2 <= (s_off2 + 400)))) ./ 1000;
                                    
                                    %sample locked delay (delay800)
                                    delay8001(tcounter).times = vecc(spikes1(find(spikes1 > (s_off1 + 400) & spikes1 <= (s_off1 + 800)))) ./ 1000;
                                    delay8002(tcounter).times = vecc(spikes2(find(spikes2 > (s_off2 + 400) & spikes2 <= (s_off2 + 800)))) ./ 1000;
                                    
                                    
                                    %delay
                                    delay1(tcounter).times = vecc(spikes1(find(spikes1 > (s_off1 + 200) & spikes1 <= (s_off1 + 800)))) ./ 1000;
                                    delay2(tcounter).times = vecc(spikes2(find(spikes2 > (s_off2 + 200) & spikes2 <= (s_off2 + 800)))) ./ 1000;
                                    
                                    %match locked delay
                                    delaymatch1(tcounter).times = vecc(spikes1(find(spikes1 > (m1 - 400) & spikes1 <= m1))) ./ 1000;
                                    delaymatch2(tcounter).times = vecc(spikes2(find(spikes2 > (m2 - 400) & spikes2 <= m2))) ./ 1000;
                                    
                                    %full trial
                                    fulltrial1(tcounter).times = vecc(spikes1(find(spikes1 > (s_on1 - 500) & spikes1 <= m1))) ./ 1000;
                                    fulltrial2(tcounter).times = vecc(spikes2(find(spikes2 > (s_on2 - 500) & spikes2 <= m2))) ./ 1000;
                                end
                                if (totalspikes1 / ntrials) > 5 && (totalspikes2 / ntrials) > 5
                                    [surrogate_spikecoh.fix_c{ccounter,p},surrogate_spikecoh.fix_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.fix_f{ccounter},zerosp] = coherencypt(fix1,fix2,params);
                                    [surrogate_spikecoh.sample_c{ccounter,p},surrogate_spikecoh.sample_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.sample_f{ccounter},zerosp] = coherencypt(sample1,sample2,params);
                                    [surrogate_spikecoh.delay400_c{ccounter,p},surrogate_spikecoh.delay400_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.delay400_f{ccounter},zerosp] = coherencypt(delay4001,delay4002,params);
                                    [surrogate_spikecoh.delay800_c{ccounter,p},surrogate_spikecoh.delay800_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.delay800_f{ccounter},zerosp] = coherencypt(delay8001,delay8002,params);
                                    [surrogate_spikecoh.delay_c{ccounter,p},surrogate_spikecoh.delay_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.delay_f{ccounter},zerosp] = coherencypt(delay1,delay2,params);
                                    [surrogate_spikecoh.delaymatch_c{ccounter,p},surrogate_spikecoh.delaymatch_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.delaymatch_f{ccounter},zerosp] = coherencypt(delaymatch1,delaymatch2,params);
                                    [surrogate_spikecoh.fulltrial_c{ccounter,p},surrogate_spikecoh.fulltrial_phi{ccounter,p},S12,S1,S2,surrogate_spikecoh.fulltrial_f{ccounter},zerosp] = coherencypt(fulltrial1,fulltrial2,params);
                                end
                            end
                            surrogate_spikecoh.clusters{ccounter} = single([clusters1(c).name; clusters2(cc).name]);
                            if strmatch(clusters1(c).name(end),'s')
                                surrogate_spikecoh.unittype1(ccounter) = 1; %single unit
                            else
                                surrogate_spikecoh.unittype1(ccounter) = 2; %multi unit
                            end
                            if strmatch(clusters2(cc).name(end),'s')
                                surrogate_spikecoh.unittype2(ccounter) = 1; %single unit
                            else
                                surrogate_spikecoh.unittype2(ccounter) = 2; %multi unit
                            end
                            surrogate_spikecoh.groups(ccounter,:) = [g gg];
                        end
                    end
                    
                    cd([sesdir filesep 'lfp' filesep 'lfp2'])
                    
                    write_info = writeinfo(dbstack);
                    
                    if rule == 1;
                        cd('identity')
                        save(spair,'surrogate_spikecoh','write_info')
                    elseif rule == 2;
                        cd('location')
                        save(spair,'surrogate_spikecoh','write_info')
                    elseif rule == 3
                        cd('fixation')
                        save(spair,'surrogate_spikecoh','write_info')
                    elseif rule == 4
                        cd('incorrect')
                        save(spair,'surrogate_spikecoh','write_info')
                    end
                end
            end
        end
    end
    
end


cd(sesdir)