function [estSac,measSac,unctrials,varargout] = checkFirstSac(varargin)
% run in the session folder
% positive mean probably wrong saccade 

Args = struct('anticipatorySac', 100,'doubleSac',-100,'minTime',350,'meanSacTime',30);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);


mts = mtstrial('auto');


cd lfp
files = nptDir('*_lfp.*');

for fi = 1 : length(files);
    [~,~,~,~,points] = nptReadStreamerChannel(files(fi).name,1);
    triall(fi) = points;
end

measSac = mts.data.MatchOnset + mts.data.FirstSac;

estSac = triall - Args.minTime - Args.meanSacTime;

ptrials = find((vecc(estSac) - vecc(mts.data.MatchOnset)) < 0);


extrials = find((vecc(estSac) - vecc(measSac)) > Args.anticipatorySac | (vecc(estSac) - vecc(measSac)) < Args.doubleSac);


unctrials = union(extrials, ptrials);

varargout{1} = ptrials;
varargout{2} = extrials;

cd ..

