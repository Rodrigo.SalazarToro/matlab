function bayes_stimulus_classifier(varargin)

Args = struct('calc_dist',0,'new_rand',0,'stims',0);
Args.flags = {'calc_dist','new_rand','stims'};
Args = getOptArgs(varargin,Args);



cd('/media/raid/data/monkey/clark/060428/session02/lfp/lfp2')


load glm_fits_train trial_corrcoefs trial_phases
load stimulus







%determine stimulus locations
locs = zeros(1,size(stimulus,2));
locs([find(stimulus==1) find(stimulus==4) find(stimulus==7)]) = 1;
locs([find(stimulus==2) find(stimulus==5) find(stimulus==8)]) = 2;
locs([find(stimulus==3) find(stimulus==6) find(stimulus==9)]) = 3;


num_pairs = size(trial_corrcoefs,2);
[num_trials,num_points] = size(trial_corrcoefs{1});




if Args.new_rand
    Args.calc_dist = 1;
    %select trials to withold
    r = randperm(num_trials);
    r = r(1:128); %hard coded
    
    trials = 1:num_trials;
    trials(r) = [];
    
    save train_test trials r
else
    load train_test
end


if Args.calc_dist
    
    pair_distributions = cell(1,num_pairs);
    pair_probdist = cell(1,num_pairs);
    pair_mus = cell(1,num_pairs);
    pair_sigmas = cell(1,num_pairs);
    pair_cumdist = cell(1,num_pairs);
    pair_original_cumulative = cell(1,num_pairs);
    
    %make distributions of conditional probabilities P(r|s)
    for p = 1 : num_pairs
        cum_dist = cell(1,9);
        loc_dist = cell(3,9);
        prob_dist =cell(3,9);
        stim_dist = cell(9,9);
        
        phase_cum_dist = cell(1,9);
        phase_loc_dist = cell(3,9);
        phase_stim_dist = cell(9,9);
        
        for t = trials
            for tp = 1 : num_points
                point = trial_corrcoefs{p}(t,tp);
                
                loc_dist{locs(t),tp} = [loc_dist{locs(t),tp} point];
                cum_dist{1,tp} = [cum_dist{1,tp} point];
                
                stim_dist{stimulus(t),tp} = [stim_dist{stimulus(t),tp} point];
                
                phase_point = trial_phases{p}(t,tp);
                
                phase_loc_dist{locs(t),tp} = [phase_loc_dist{locs(t),tp} phase_point];
                phase_cum_dist{1,tp} = [phase_cum_dist{1,tp} phase_point];
                
                phase_stim_dist{stimulus(t),tp} = [phase_stim_dist{stimulus(t),tp} phase_point];
            end
        end
        
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         mus= zeros(3,9);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         sigmas = zeros(3,9);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         for np = 1 : num_points
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             for nl = 1 : 3 %3 locations
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %calculate mean and std of each distribution, including the cumulative
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 m = mean(loc_dist{nl,np});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 mus(nl,np) = m;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 s = std(loc_dist{nl,np});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 sigmas(nl,np) = s;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %calculate normalized distributions for finding probabilities
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 if m > 0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     range = [0:.01:1];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 else
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     range = [-1:.01:0];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 y = normpdf(range,m,s);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 norm_y = y ./ sum(y);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 prob_dist{nl,np} = norm_y;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 if nl == 1
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     %calculate p(r), so go across locations
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     m = mean(cum_dist{1,np});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cum_mu(1,np) = m;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     s = std(loc_dist{1,np});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cum_sigma(1,np) = s;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     if m > 0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                         range = [0:.01:1];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     else
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                         range = [-1:.01:0];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     y = normpdf(range,m,s);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     norm_y = y ./ sum(y);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     c_dist{1,np} = norm_y;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         pair_mus{p} = mus;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         pair_sigmas{p} = sigmas;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         pair_probdist{p} = prob_dist; %fitted distribution for each location
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         pair_cumdist{p} = c_dist; %cumuluative fitted distributions
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         
        
        pair_distributions{p} = loc_dist; %original distributions for locations
        pair_original_cumulative{p} = cum_dist; %orignial cumulative distribution
        pair_allstim_distributions{p} = stim_dist;
    end
    
    save bayes_distributions pair_distributions pair_original_cumulative pair_allstim_distributions
else
    load bayes_distributions
end

gp = 0;
good_p = []
pair_predictions = [];
all_loc_probs = zeros(128,num_pairs,3);
all_probabilities = cell(1,9);
all_loc_distances = zeros(128,num_pairs,3);

phase_all_loc_probs = zeros(128,num_pairs,3);
phase_all_probabilities = cell(1,9);
phase_all_loc_distances = zeros(128,num_pairs,3);

for nump = 1 : num_pairs
    match = 0;
    counter = 0;
    predict = [];
    for numt = r%1 : num_trials
        counter = counter + 1;
        loc_probabilities = zeros(3,9);
        stim_probabilities = zeros(9,9);
        for ntime = 1 : num_points
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             corr_coef = trial_corrcoefs{nump}(numt,ntime);
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             cc = (round(corr_coef*100))/100; %round to hundredths place
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             if corr_coef > 0
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 range = [0:.01:1];
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [~,ii]=intersect(range,cc);
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             else
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 range = [-1:.01:0];
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [~,ii]=intersect(range,cc);
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             an = [pair_probdist{nump}{1,ntime}',pair_probdist{nump}{2,ntime}',pair_probdist{nump}{3,ntime}'];
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             p = anova1(an,[],'off')
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             %use fitted distributions
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             for l = 1 : 3
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cond_distribution = pair_probdist{nump}{l,ntime};
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cumulative_dist = pair_cumdist{nump}{1,ntime};
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cond_prob = cond_distribution(ii);
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cum_prob = cumulative_dist(ii);
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %probability of the sample is 1/3
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 prob_sample = 1/3;
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 loc_probabilities(l,ntime) = (cond_prob * prob_sample) / cum_prob;
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
            
            phase = trial_phases{nump}(numt,ntime);
            ph = (round(phase*10))/10;
            
            corr_coef = abs(trial_corrcoefs{nump}(numt,ntime));
            cc = (round(corr_coef*10))/10; %round to tenths place
            
            %use original distributions
            for l = 1 : 3
                cond_prob = 0;
                cum_prob = 0;
                cond_distribution = abs(pair_distributions{nump}{l,ntime});
                [pairn,pair_xout]=hist(cond_distribution,[0:.1:1]);

                %interpolate
                pair_xout = [0:.1:1];
                pairn = interp1([0:.1:1],pairn,pair_xout);
 
                %normalize pair distribution
                pairn = pairn ./ sum(pairn);
                
                [~,ii] = intersect(pair_xout,cc);
                
                if isempty(pairn(ii)) || pairn(ii) == 0
                    cond_prob = .01;
                else
                    cond_prob = pairn(ii);
                end
                cumulative_dist = abs(pair_original_cumulative{nump}{1,ntime});
                
                [cumn,cum_xout]=hist(cumulative_dist,[0:.1:1]);
                %interpolate
                cum_xout = [0:.1:1];
                cumn = interp1([0:.1:1],cumn,cum_xout);
                %normalize cumuluative distribution
                cumn = cumn ./ sum(cumn);
                
                [~,ii] = intersect(cum_xout,cc);
                
                if isempty(cumn(ii)) || cumn(ii) == 0
                    cum_prob = .01;
                else
                    cum_prob = cumn(ii);
                end
                
                %probability of the sample is 1/3
                prob_sample = 1/3;
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                
                loc_probabilities(l,ntime) = (cond_prob * prob_sample) / cum_prob;
            end
            
            
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                         %use original distributions
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             for s = 1 : 9
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cond_prob = 0;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cum_prob = 0;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cond_distribution = abs(pair_allstim_distributions{nump}{s,ntime});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [pairn,pair_xout]=hist(cond_distribution,[0:.1:1]);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %interpolate
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 pair_xout = [0:.01:1];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 pairn = interp1([0:.1:1],pairn,pair_xout);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %normalize pair distribution
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 pairn = pairn ./ sum(pairn);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [~,ii] = intersect(pair_xout,cc);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 if isempty(pairn(ii)) || pairn(ii) == 0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cond_prob = .01;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 else
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cond_prob = pairn(ii);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cumulative_dist = abs(pair_original_cumulative{nump}{1,ntime});
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [cumn,cum_xout]=hist(cumulative_dist,[0:.1:1]);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %interpolate
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cum_xout = [0:.01:1];
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cumn = interp1([0:.1:1],cumn,cum_xout);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %normalize cumuluative distribution
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 cumn = cumn ./ sum(cumn);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 [~,ii] = intersect(cum_xout,cc);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 if isempty(cumn(ii)) || cumn(ii) == 0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cum_prob = .01;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 else
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     cum_prob = cumn(ii);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %probability of the sample is 1/9 for 9 stimuli
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 prob_sample = 1/9;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 stim_probabilities(s,ntime) = (cond_prob * prob_sample) / cum_prob;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
            

            if nump == 1 && counter == 1
                all_probabilities{ntime} = zeros(128,36,3);
            end
            
            for ll = 1 : 3
                all_probabilities{ntime}(counter,nump,ll) = loc_probabilities(ll,ntime);
            end
            
        end
        
        
        % %             c={'r','g','k'};for x = 1:3;scatter([1:9],loc_probabilities(x,:),[c{x} 'o']);hold on;end;lsline
        % %             c={'r','g','k'};for x = 1:3;plot(loc_probabilities(x,:),c{x});hold on;end;
        
        all_loc_probs(counter,nump,:) = sum(loc_probabilities');

        
        %determine the best location
        %         [~,best] = max(loc_probabilities(:,1));
        %         [~,best] = max(max(loc_probabilities'));
        
        if Args.stims
            for nn = 1:9
                norms(nn) = norm(stim_probabilities(nn,:));
            end
            [~,best]=max(norms);
            predict(numt) = best;
            if best == stimulus(numt)
                match = match + 1;
            end
        else
            for nn = 1:3
                norms(nn) = norm(loc_probabilities(nn,:));
            end
            for nn = 1:3
                all_loc_distances(counter,nump,nn) = norms(nn);
            end
            [~,best]=max(norms);
            predict(numt) = best;
            if best == locs(numt)
                match = match + 1;
            end
        end
        
        
        
    end
    
    nump
    perf = (match / 128)*100
    
    
    if perf >=39
        gp = gp + 1;
        good_p(gp) = nump
    end
    
    pair_predictions(:,nump) = predict';
    
end


alln = zeros(3,9);
predict = zeros(9,128);
for p = 1:9
    l=locs(r);
    m=0;
    for t=1:128
        alln(1,p) = norm(all_probabilities{p}(t,[1 9 11 30],1));
        alln(2,p) = norm(all_probabilities{p}(t,[1 9 11 30],2));
        alln(3,p) = norm(all_probabilities{p}(t,[1 9 11 30],3));
        [~,b]=max([norm(all_probabilities{p}(t,:,1)), norm(all_probabilities{p}(t,:,2)), norm(all_probabilities{p}(t,:,3))]);
        predict(p,t) = b;
        if b ==l(t);
            m = m+1;
        end
    end
    pr=(m/128)*100
    pp(p) = pr
end



m = 0;
for t = 1 : 128
    [~,b] = max([size(find(predict((6:9),t) == 1),1), size(find(predict((6:9),t) == 2),1), size(find(predict((6:9),t) == 3),1)]);
    if b == locs(r(t))
        m = m + 1;
    end
end

(m/128)*100










pair_predictions;
m=0;for x = r,for xx = 1:3;c(xx)=size(find(pair_predictions(x,good_p) == xx),2),end;[~,best] = max(c),if best == locs(x);m = m+1;end;end;
total_perf = (m/128) * 100

total_perf


for ps = 1 : 128
    for dd = 1:3
        norms(dd) = norm(all_loc_distances(ps,:,dd));
    end
    [~,best]=max(norms);
    p(ps) = best;
end
p

m = 0;
c = 0;
for x =r
    c = c+1;
    if p(c) == locs(x)
        m = m+1;
    end
end
tp = (m/128) *100
















