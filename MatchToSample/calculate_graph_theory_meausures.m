function calculate_graph_theory_meausures

cd('/media/bmf_raid/data/monkey/ethyl/')
% load alldays
% allbmf = ProcessDays(bmfCohgram,'days',alldays,'ml','NoSites','Sites','session01');
% save allbmf allbmf
load allbmf allbmf

% cd('/media/bmf_raid/data/monkey/ethyl/110810/session01')
% % % bmfcoh = bmfCohgram('auto')
% % % save bmfcoh bmfcoh
% load bmfcoh
% allbmf = bmfcoh;
[totalpairs allpairs] = get(allbmf,'Number');

unique_groups = sort(unique([allbmf.data.Index(allpairs,1);allbmf.data.Index(allpairs,2)]))';
numb_groups = size(unique_groups,2);

for fbands =2 %1:3 %go through all frequency bands
    for e = 1 : 4
        [~,pairs{e}] = get(allbmf,'Number','epoch_sig_coh',e,'epoch_sig_percent',100,'freq_band',fbands);
    end
    for allepochs = 1 : 4
        connectivity_mat{allepochs} = zeros(numb_groups,numb_groups);
        connectivity_mat_weighted{allepochs} = zeros(numb_groups,numb_groups);
        for x = pairs{allepochs}
            g1 = allbmf.data.Index(x,1);
            [~,cmatnumber1] = intersect(unique_groups,g1);
            
            g2 = allbmf.data.Index(x,2);
            [~,cmatnumber2] = intersect(unique_groups,g2);
            
            %             get the specific anatomical area
            a1 = allbmf.data.Index(x,40);
            a2 = allbmf.data.Index(x,41);
            
            cat1 = allbmf.data.Index(x,42);
            cat2 = allbmf.data.Index(x,43);
            %             if cat1 ~= cat2
            m = allbmf.data.Index(x,17+(allepochs*4));
            if ~isnan(m)
                connectivity_mat{allepochs}(cmatnumber1,cmatnumber2) = 1;
                connectivity_mat{allepochs}(cmatnumber2,cmatnumber1) = 1;
                
                connectivity_mat_weighted{allepochs}(cmatnumber1,cmatnumber2) = m;
                connectivity_mat_weighted{allepochs}(cmatnumber2,cmatnumber1) = m;
            end
            %             end
        end
                dense(allepochs) = density_und(connectivity_mat{allepochs});
        
        db = distance_bin(connectivity_mat{allepochs});
        db(find(db == inf)) = nan;
        d(allepochs) = nanmean(nanmean(db));
        
        %run perm
        dperm = [];
        for permg = 1 :100
            R = randmio_und(connectivity_mat{allepochs},1);
            
            db = distance_bin(R);
            db(find(db == inf)) = nan;
            dperm(permg) = nanmean(nanmean(db));
        end
        mdperms(allepochs) = prctile(dperm,99);
        
        
        dcon(allepochs,:) = betweenness_bin(connectivity_mat{allepochs});
        
    end
end
figure;
plot(d)
hold on;
plot(mdperms,'r')
title('distance')


figure
plot(dense,'k')
title('density')

figure
for x = 1:4
    subplot(2,4,x);imagesc(connectivity_mat{x})
    hold on
    subplot(2,4,x+4);imagesc(connectivity_mat_weighted{x})
    hold on
end

figure
%take out common connections
commonc = zeros(numb_groups,numb_groups);
for x = 1:4
    commonc(find(connectivity_mat{x})) = commonc(find(connectivity_mat{x})) + 1;
end

for x = 1:4
    connectivity_mat{x}(find(commonc > 3)) = 0;
    subplot(1,4,x);imagesc(connectivity_mat{x})
    hold on
end



% figure
% for x = 1:4
%     subplot(1,4,x)
%     hist(dcon(x,:));
%     hold on
% end



