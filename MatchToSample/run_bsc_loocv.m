function run_bsc_loocv(varargin)

%run at monkey level
%runs bayes_stimulus_classifier_LOOCV

Args = struct('days',[],'loc',0,'ide',0,'all',0);
Args.flags = {'loc','ide','all'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;

num_days = size(Args.days,2);

for d = 1 : num_days
    cd([monkeydir filesep Args.days{d} filesep 'session01'])
    
    if Args.loc
        bayes_stimulus_classifier_LOOCV_fast('ml','loc')
    end
    
    if Args.ide
        bayes_stimulus_classifier_LOOCV_fast('ml','ide')
    end
    
    if Args.all
        bayes_stimulus_classifier_LOOCV_fast('ml')
    end
end