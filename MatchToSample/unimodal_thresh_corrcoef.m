
function thresh = unimodal_thresh_corrcoef(varargin)

%method from rosin_2001 "Unimodal Thresholding"

Args = struct('plot',0);
Args.flags = {'plot'};
Args = getOptArgs(varargin,Args);


cd('/media/raid/data/monkey/betty')
load cpp_b
cd('/media/raid/data/monkey/clark')
load cpp_c


[~,ppb] = get(cpp_b,'Number','ml');
[~,ppc] = get(cpp_c,'Number');

[h i] = hist([real(cpp_b.data.Index(ppb,68)); real(cpp_c.data.Index(ppc,68))],2000);

x1 = max(find(h == 0)); %first non-zero bin
y1 = h(x1);

x2 = size(h,2); %last bin
y2 = h(x2);

if Args.plot
    bar(h);
%     xlim([800 1000])
    hold on
    plot([x1 x2],[y1 y2])
    hold on
end

%calculate length from each bin to the line
for p = x1+1 : x2
    
    %get xy for bin (point)
    x3 = p;
    y3 = h(p);
    
    Q1 = [x1 y1];
    Q2 = [x2 y2];
    P = [x3 y3];
    
    lengths(p) = abs(det([Q2-Q1;P-Q1]))/norm(Q2-Q1);
    R = (dot(P-Q2,Q1-Q2)*Q1+dot(P-Q1,Q2-Q1)*Q2)/dot(Q2-Q1,Q2-Q1);
    allR(p,:) = R;
    allpoint(p,:) = [x3 y3];
    
    x4 = R(1);
    y4 = R(2);
    
    if Args.plot
        hold on
        plot([x3 x4],[y3 y4])
    end
end

[~,th] = max(lengths);
if Args.plot
    hold on
    plot([allpoint(th,1) allR(th,1)],[allpoint(th,2) allR(th,2)],'r')
end

thresh = i(th);



