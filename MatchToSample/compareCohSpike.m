function compareCohSpike(varargin)
% in the xcorr directory
Args = struct('save',0,'redo',0,'pvalue',0.001,'prctileSur',99.9);
Args.flags = {'save','redo'};
Args = getOptArgs(varargin,Args);



sdir = pwd;
files = cell(2,1);
for r = 1 : 2
    [~,tfiles] = unix(sprintf('find %s -name ''xcorrg*Rule%d.mat''',sdir,r));
    nf = length(findstr(tfiles,'.mat'));
    if ~strcmp(tfiles(1),'/')
        tfiles = tfiles(2:end);
    end
    files{r} = (reshape(tfiles,length(tfiles)/nf,nf))';
end

for ff = 1 : nf
    dz = cell(4,1);
    vdz = cell(4,1);
    Adz = cell(4,1);
    rfreq = cell(4,1);
    c1 = load(files{1}(ff,1:end-1));
    sur1 = load([files{1}(ff,1:end-5) 'Surtrial.mat']);
    sigf1 = zeros(4,101);
    sigf2 = zeros(4,101);
    ff2 = find(ismember(files{2}(:,1:end-6),files{1}(1,1:end-6),'rows') == 1);
    [~, name,~] = fileparts(files{1}(ff,1:end-1));
    matfile = sprintf('%sComp.mat',name(1:end-1));
    if ~isempty(ff2) && ~isempty(c1.J1)
        if isempty(nptDir(matfile)) || Args.redo
            c2 = load(files{2}(ff2,1:end-1));
            sur2 = load([files{2}(ff,1:end-5) 'Surtrial.mat']);
            if ~isempty(c2.rfreq)
                for ep = 1 : 4
                    if length(c1.rfreq{ep}) == length(c2.rfreq{ep}) && iscell(sur1.S) && iscell(sur2.S) && ~isempty(sur1.S{ep}) && ~isempty(sur2.S{ep})
                        f = c1.rfreq{ep};
                        J1c1 = reshape(c1.J1{ep},size(c1.J1{ep},1),size(c1.J1{ep},2)*size(c1.J1{ep},3));
                        J2c1 = reshape(c1.J2{ep},size(c1.J2{ep},1),size(c1.J2{ep},2)*size(c1.J2{ep},3));
                        J1c2 = reshape(c2.J1{ep},size(c2.J1{ep},1),size(c2.J1{ep},2)*size(c2.J1{ep},3));
                        J2c2 = reshape(c2.J2{ep},size(c2.J2{ep},1),size(c2.J2{ep},2)*size(c2.J2{ep},3));
                        if ~isempty(J1c1) && ~isempty(J2c1) && ~isempty(J2c2) && ~isempty(J1c2) && size(J1c1,1) == size(J1c2,1)
                            sigf1(ep,:) = c1.S{ep} > prctile(sur1.S{ep},Args.prctileSur,1);
                            sigf2(ep,:) = c2.S{ep} > prctile(sur2.S{ep},Args.prctileSur,1);
                            [dz{ep},vdz{ep},Adz{ep}]=two_group_test_coherence(J1c1,J2c1,J1c2,J2c2,Args.pvalue,'n',f);
                            rfreq{ep} = f;
                        end
                    end
                end
                if Args.save
                    save(matfile,'dz','vdz','Adz','rfreq','sigf1','sigf2')
                    display(['saving ' sdir ' ' sprintf('%sComp.mat',name(1:end-1))])
                end
            end
        end
    end
end