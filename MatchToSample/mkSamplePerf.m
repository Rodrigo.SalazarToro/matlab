function [obj,loc,varargout] = mkSamplePerf(varargin)
% at the Day level

Args = struct('save',0,'redo',0,'cues9',0,'ML',0);
Args.flags = {'save','redo','9cues','ML'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

if Args.ML
    sessions(1).name = 'session01';
    ss = 1;
else
    sessions = nptDir('session*');
    ss = 2;
end
dday = pwd;
for s = ss : length(sessions)
    cd(sessions(s).name)
    mt = mtstrial('auto');
    for r = vecr(unique(mt.data.Index(:,1)));
         
        fname = sprintf('samplePerf%d.mat',r);
        for item = 1 : 3
            
            tottObj = length(mtsgetTrials(mt,'iCueObj',item,'stable','rule',r,varargin2{:}));
            tottLoc = length(mtsgetTrials(mt,'iCueLoc',item,'stable','rule',r,varargin2{:}));
            corObj = length(mtsgetTrials(mt,'iCueObj',item,'stable','BehResp',1,'rule',r,varargin2{:}));
            corLoc = length(mtsgetTrials(mt,'iCueLoc',item,'stable','BehResp',1,'rule',r,varargin2{:}));
            
            obj(item) = corObj / tottObj;
            loc(item) = corLoc / tottLoc;
        end
         if Args.save; cd(dday);save(fname,'obj','loc'); display(sprintf('saving %s %s',pwd,fname));cd(sessions(s).name);end
    end
   cd ..
end
%% clark
% for d = 1 : 29;
% cd(idedays{d})
% sessions = nptDir('session*');
% for s = 2 : length(sessions)
% cd(sessions(s).name)
% [obj,loc] = mkSamplePerf('save');
% cd ..
% end
% cd ..
% end
%% betty
% for d = 1 : 27;
% cd(idedays{d})
%
% cd session01
% for r = 1 : 2
% [obj,loc] = mkSamplePerf('save','rule',r);
%
% end
% cd ../..
% end