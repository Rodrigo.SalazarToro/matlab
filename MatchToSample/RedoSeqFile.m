function RedoSeqFile
% assumes that the number of files-trials and markers matches
% needs to be run into the session folder

startDir = pwd;
st = strfind(startDir,'session');
sessionNb = str2num(startDir(st+7:st+8));

cd ..
daydir = pwd;



namedir = 'originalSessionFormat';
cd(namedir);
d = nptDir('session*','CaseInsensitive');
backup = sprintf('%s%s%s',daydir,filesep,namedir);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get the session rule
for ses = 1 : size(d,1) % assumes that session 01 is the eye calibration
    cd(d(ses).name)
    seqfile = nptDir('*.seq','CaseInsensitive');
    if isempty(seqfile)
        type(ses) = -1;
    else
        [SEQUENCE,SessionType] = ReadSequenceFile(seqfile.name);

        if strcmp(SessionType,'IDENTITY')

            type(ses) = 1;

        elseif strcmp(SessionType,'LOCATION')

            type(ses) = 2;
        end
    end %isempty(seq)

    cd(backup)

end %ses = 1 : size(d,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

trans = [0 find((diff(type)) ~= 0) size(type,2)]; % find the transition trials between rules

% ns = trans(sessionNb+1) - trans(sessionNb);

NewA = [];
for n = [trans(sessionNb)+1 : trans(sessionNb+1)] % runs through all the session that were combined to produce the new session
    cd(sprintf('%s%s%s',backup,filesep,d(n).name)) % in the
    fprintf('going to %s%s%s \n',backup,filesep,d(n).name)
    sfiles = nptDir('*.0*','CaseInsensitive');
    ntrial = size(sfiles,1);

    seqfile = nptDir('*.seq','CaseInsensitive');
   
    fid = fopen(seqfile.name,'r');
    A = textread(seqfile.name,'%s','whitespace','','bufsize',1000000);
    fclose(fid);
    
    if n == trans(sessionNb)+1
        NewA = [NewA A{1}(1:(60+ntrial*14))]; % includes the headers if it is teh first session 
    else
        NewA = [NewA A{1}(61:(60+ntrial*14))];% appends the sequence of trials to the last trials-session
    end
    cd(backup)
   
end

cd(startDir)
seqfile = nptDir('*.seq','CaseInsensitive');
fid = fopen(seqfile.name,'w');
fprintf(fid,'%s',NewA);% the 60th firt characters are the info at the beginning of the seq file and then each trial occupies 14 characters
fclose(fid);

