function write_fixation_trial_times

%run at session level
bhv = nptDir('MTS-*');
b = bhv_read(bhv.name);


%get fixation trials
fix_int = find(b.ConditionNumber == 47)'; %interleaved
fix_block = find(b.ConditionNumber == 6)'; %block
blank_trials = find(b.ConditionNumber == 48)'; %these are the blank trials

af = sort([fix_int fix_block]);
%find correct trials (in the case of fixation this means completed)
correct = find(b.TrialError == 0)';

[all_fix,fixind] = intersect(correct,af);

[~,bind] = intersect(correct, blank_trials);
%determine the values that qualify as fixation using the correct trials
mtstrials = correct;
mtstrials([fixind bind]) = []; %get rid of the fixation trials and blank trials

c = 0;
for mt = mtstrials
    c = c + 1;
    f = b.CodeTimes{mt}(5) - 500;
    e(c,1) = (b.AnalogData{mt}.EyeSignal(f,1));
    e(c,2) = (b.AnalogData{mt}.EyeSignal(f,2));
end
% figure
% subplot(1,2,1)
% scatter(e(:,1),e(:,2));

fixval = 3.05; %this is the value used in the timing file and the mts trials correspond

c = 0;
fixtimes = [];
for nf = all_fix
    c = c + 1;
    %determine when eyetraces are in the fixation window 
    eyefix = intersect(find(abs(b.AnalogData{nf}.EyeSignal(:,1)) < fixval), find(abs(b.AnalogData{nf}.EyeSignal(:,2)) < fixval));
    
    %fixation must occur within 500 ms, so start there and look back
    deye500  = diff(eyefix(find(eyefix <= 500))); %this takes the diff of for the time values where fixation is occuring, when fixation breaks the value will be greater than 1
    
    realfix = eyefix(max(find(deye500 ~= 1))+1); %find the value closest to 500;
    
    if isempty(realfix);
        realfix = min(eyefix); %take earliest fixation
    end

    tend = size(b.AnalogData{nf}.EyeSignal(:,1),1); %use end of trial
    tlength(c) = max(eyefix) - realfix;
    fixtimes(c) = realfix;% - b.CodeTimes{nf}(1);
    
%         plot(b.AnalogData{nf}.EyeSignal(:,1));hold on
%         plot(b.AnalogData{nf}.EyeSignal(:,2));hold on
%         plot([realfix realfix], [-10 10],'r');
end

% subplot(1,2,2)
% hist(tlength,50)


%for the timing to be comparable to the parsed streamer files the start
%trigger time needs to be subtracted from the fix time.
save fixation_trial_times fixtimes

