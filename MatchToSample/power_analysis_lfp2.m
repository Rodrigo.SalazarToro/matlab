function [all_freq_peaks, fp, m, frequencies] = power_analysis_lfp2(varargin)

%run at session level
%computes mean power for each channel for 7 different time windows using detrended lfp sampled at 1KHz.

Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

if Args.ml
    N = NeuronalHist;
    g = sorted_groups;
    [~,ch] = intersect(N.gridPos,g);
else
    N = NeuronalChAssign;
    g = sorted_groups;
    [~,ch] = intersect(N.groups,g);

end
chnumb = size(ch,2);
cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'power'])

powertrials = nptDir('*_power.*');
ntrials = size(powertrials,1);
%make data matrix
trial_power = cell(1,chnumb);
for nt = identity
    load(powertrials(nt).name)
    
    for c = 1 : chnumb
        if nt == identity(1)
            trial_power{c} = power.S_delay(ch(c),:);
        else
            trial_power{c} = [trial_power{c}; power.S_delay(ch(c),:)];
        end
    end
end

%get frequencies
frequencies = power.f_delay';

 m = cellfun(@mean,trial_power,'UniformOutput',0);
% hold on
% for x = 1:size(m,2);plot(power.f_delay',smooth(m{x},3));hold on;end
all_freq_peaks = [];
for x = 1 : size(m,2)
    f = findpeaks(smooth(m{x},3));
    frqp = power.f_delay(f.loc)';
    
    frqp(frqp <= 4) = []; %cut out everything below 2
    frqp(frqp > 50) = []; %cut out everything above 50
    
    all_freq_peaks = [all_freq_peaks frqp];
    fp{x} = frqp;
end

cd(sesdir)

















