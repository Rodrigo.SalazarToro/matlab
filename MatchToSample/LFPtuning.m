function [nind,rind,varind,varargout] = LFPtuning(varargin)


Args = struct('cohtype',{'cohInter'},'plot',0,'tuningType','allCuesTuning');
Args.flags = {'plot'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'cohtype','plot'});


switch Args.tuningType
    case 'allCuesTuning'
        cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3]; % cueComb(1,:) = location and cueComb(2,:) = identity
        dime = Args.tuningType;
    case {'locTuning','ideTuning'}
        cueComb = [1 2 3];
        dime = Args.tuningType;
end

wind = {'presample' 'sample' 'delay1' 'delay2'};
rules = {'IDENTITY' 'LOCATION'};
cohtype = {'cohInter' 'cohPP' 'cohPF'};
cohcolor = {'r' 'b' 'y'};
periodm = {':' '-.' '--' '-'};


matfile = sprintf('%sFreqAve.mat',Args.tuningType);

prematfile = nptDir(matfile);
if isempty(prematfile)

    [index,data,factors,surro,ntrials] = tuningFreqAve('tuningType',Args.tuningType,modvarargin{:});
else
    load(prematfile.name)
end
coh = strmatch(Args.cohtype,cohtype);
if size(cueComb,1) ==2
    indpairs = find(index(6,:) == coh);
else
    indpairs = find(index(5,:) == coh);
end
npairs = unique(index(1,indpairs));

for ff = 1 : length(factors); if ~isempty(strfind(factors{ff},'p<.01')); ro = ff; end; end


for pp = 1 : 2; for r = 1 : 2; for p = 1 : 4; nind{pp,r,p} = [];rind{pp,r,p} = []; varind{pp,r,p} = [];end; end; end

for pair = npairs


    for r = 1 : 2

        %         if size(cueComb,1) ==2
        %
        %             ind1 = find(index(1,:) == pair & index(5,:) == r & (index(4,:) == 3 | index(4,:) == 4) & index(6,:) == coh);
        %         else
        %             ind1 = find(index(1,:) == pair & index(4,:) == r & (index(3,:) == 3 | index(3,:) == 4) & index(5,:) == coh);
        %         end

        %         if sum(unique(index(ro,ind1))) == 1
        for pp = 3 : 4
            if size(cueComb,1) ==2

                ind = find(index(1,:) == pair & index(5,:) == r & index(4,:) == pp & index(6,:) == coh);
                preind = find(index(1,:) == pair & index(5,:) == r & index(4,:) == 1 & index(6,:) == coh);
                allind = find(index(1,:) == pair & index(5,:) == r & (index(4,:) == 1 | index(4,:) == 2 | index(4,:) == 3 | index(4,:) == 4) & index(6,:) == coh);
               
            else
                ind = find(index(1,:) == pair & index(4,:) == r & index(3,:) == pp & index(5,:) == coh);
                preind = find(index(1,:) == pair & index(4,:) == r & index(3,:) == 1 & index(5,:) == coh);
                
                allind = find(index(1,:) == pair & index(4,:) == r & (index(3,:) == 1 | index(3,:) == 2 | index(3,:) == 3 | index(3,:) == 4) & index(5,:) == coh);
                
            end
            if sum(unique(index(ro,ind))) == 1
                %             if size(cueComb,1) ==2
                %
                %                 ind = find(index(1,:) == pair & index(4,:) == p & index(5,:) == r & index(6,:) == coh);
                %             else
                %                 ind = find(index(1,:) == pair & index(3,:) == p & index(4,:) == r & index(5,:) == coh);
                %             end
                %             if sum(unique(index(ro,ind))) == 1
               
                
%                 mx = max(data(ind));
%                 %                 mn = mean(surro{2});%            mn = min(data(ind));
%                 if p == 1; mn = mean(data(ind)); end
%                 tuningInd = (mx-mn) / (mx + mn);
%                 tind{r,p} = [tind{r,p} tuningInd];
%                 %             end
                
                maxn = max(data(allind));
                ndata = data(ind)/ maxn;
                [rdata,ix] = sort(ndata);
                for p = 1 : 4
                    if size(cueComb,1) == 2
                        ind = find(index(1,:) == pair & index(5,:) == r & index(4,:) == p & index(6,:) == coh);
                    else
                        ind = find(index(1,:) == pair & index(4,:) == r & index(3,:) == p & index(5,:) == coh);
                    end
                    ndata = data(ind)/ maxn;
                    rdata = ndata(ix);
                    vardata = std(ndata);
                    nind{pp-2,r,p} = [nind{pp-2,r,p} ndata'];
                    rind{pp-2,r,p} = [rind{pp-2,r,p} rdata'];
                    varind{pp-2,r,p} = [varind{pp-2,r,p} vardata];
                end
                
%                 
%                     
%                 ndata = data(ind)/ maxn;
%                 prendata = data(preind) / maxn;
%                 var = std(ndata);
%                 
%                 
%                 rdata= sort(ndata)';
%                 rdatapres = sort(prendata)';
%                 pre = std(data(preind));
% %                 tind{r,p} = [tind{r,p} [var;pre]];
%                   tind{r,p} = [tind{r,p} rdata];
% %                   tind{r,p-2} = [tind{r,p-2} rdatapres];
                  
            end
            
%                 for p = 1 : 4; tind{r,p} = [tind{r,p} nan];end
        end
    end
    
end






if Args.plot
    nfig = ceil(length(npairs)/5);

    for fig = 1 : nfig
        fi{fig} = figure;
        if length(npairs) > fig*5
            endp = fig*5;
        else
            endp = length(npairs);
        end
        spairs = npairs([(fig-1)*5+1 : endp]);

        cp = 1;
        for pair = spairs

            for r = 1 : 2
                if size(cueComb,1) ==2

                    ind = find(index(1,:) == pair & index(5,:) == r & (index(4,:) == 3 | index(4,:) == 4) & index(6,:) == coh);
                else
                    ind = find(index(1,:) == pair & index(4,:) == r & (index(3,:) == 3 | index(3,:) == 4) & index(5,:) == coh);
                end

                if sum(unique(index(ro,ind))) == 1
                    for p = 1 : 4
                        if size(cueComb,1) ==2
                            ind = find(index(1,:) == pair & index(4,:) == p & index(5,:) == r & index(6,:) == coh);
                        else
                            ind = find(index(1,:) == pair & index(3,:) == p & index(4,:) == r & index(5,:) == coh);
                        end
                        %                         if sum(unique(index(ro,ind))) == 1
                        %                             datap(p,:) = data(ind);
                        %                         else
                        %                             datap(p,:) = nan(1,length(ind));
                        %                         end
                        datap(p,:) = data(ind);
                    end

                    datap = datap/max(max(datap));
                    alldata(pair,r,:,:) = datap;
                    subplot(2,5,(r-1)*5+cp); plot(datap');
                    if r ==1 ;title(sprintf('pair # %d',pair)); end

                end

            end
            cp = cp + 1;
        end
        legend('presample','sample','delay1','delay2')
        subplot(2,5,1); ylabel('IDENTITY; normalized beta coherence'); subplot(2,5,6); ylabel('LOCATION; normalized beta coherence');
        for sb = 6 : (5+length(spairs)); subplot(2,5,sb); xlabel(sprintf('%s stimuli',dime(1:3))); end

    end
    varargout{1} = alldata;
end


