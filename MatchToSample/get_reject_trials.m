function rejecTrials = get_reject_trials


%run at session level
sesdir = pwd;

cd([sesdir filesep 'lfp'])

load rejectedTrials.mat

cd(sesdir)
