function binary_pattern_finder

cd('/media/raid/data/monkey/clark/060428/session02/lfp/lfp2')


load glm_fits_train trial_fits_binary
load stimulus

locs = zeros(1,size(stimulus,2));
locs([find(stimulus==1) find(stimulus==4) find(stimulus==7)]) = 1;
locs([find(stimulus==2) find(stimulus==5) find(stimulus==8)]) = 2;
locs([find(stimulus==3) find(stimulus==6) find(stimulus==9)]) = 3;


pnumb = 36;
num_trials = size(stimulus,2);

tfb = zeros(9,pnumb,427);

for nt = 1 : num_trials
    for x = 1:pnumb
        tfb(:,x,nt) = trial_fits_binary{x}(nt,:);
    end
end

dub_stim = cell(1,9);

q = zeros(36,36,3);
q9 = zeros(36,36,9);

% qp = zeros(9,630,9);
qp = zeros(81,630,3);
qp_trials = cell(1,427);


r = randperm(427);
r = r(1:128);
time=0;
for timep = 1:9
    for timepp = 1:9%timep
        time = time + 1;
        q = zeros(36,36,3);
        for all_trials = 1 : num_trials
            if time == 1
                %                 qp_trial{all_trials} = zeros(81,630);
                qp_trial{all_trials} = zeros(81,630);
            end
            
            c = 0;
            trial = tfb(:,:,all_trials);
            pair_pair = 0;
            for p = 1 : pnumb
                for pp = (p + 1) : pnumb
                    pair_pair = pair_pair + 1;
                    if trial(timep,p) && trial(timepp,pp)
                        c = c + 1;
                        dub{all_trials}(c,:) = [p pp];
                        dub_pair{all_trials}(c) = pair_pair;
                        
                        qp_trial{all_trials}(time,pair_pair) = 1; 
                        
                        if ~sum(all_trials == r)
                            q(p,pp,locs(all_trials)) = q(p,pp,locs(all_trials)) + 1;
                            
                            qp(time,pair_pair,locs(all_trials)) = qp(time,pair_pair,locs(all_trials)) + 1; 
                            
                            q9(p,pp,stimulus(all_trials)) = q9(p,pp,stimulus(all_trials)) + 1;
                        end
                        
                        dub_stim{stimulus(all_trials)} = [dub_stim{stimulus(all_trials)} pair_pair];
                    end
                end
            end
        end
        
        maxq =  max(max(max(q)));
        
        q = q ./ maxq;
        
        q(find(q <= .5)) = 0;
        q(find(q>.5)) = 1;
        
%         for x = 1:3,subplot(4,1,x);imagesc(q(:,:,x));colorbar;end
        
        
        
        % for xx = 1 : nchoosek(36,2);
        %     for stims = 1 : 9
        %         s_bin(stims)
        %     end
        % end
        
        match = 0;
        predict = [];
        for at = r
            trials = zeros(36,36);
            ints = dub{at};
            for d = 1 : size(ints,1);
                trials(ints(d,1),ints(d,2)) = 1;
            end
            cors = [];
            for c = 1:3
                cors(c) = corr2(trials,q(:,:,c));
            end
            [i ii] = max(cors);
            predict(at) = ii;
         
            ii;
            locs(at);
%             subplot(4,1,4);imagesc(trials)
            
            if ii == locs(at)
                match = match + 1;
            end
            
% % % % % %                         cors = [];
% % % % % %             for c = 1:9
% % % % % %                 cors(c) = corr2(trial,q9(:,:,c));
% % % % % %             end
% % % % % %             [i ii] = max(cors);
% % % % % %             predict(at) = ii;
% % % % % %             if ii == stimulus(at)
% % % % % %                 match = match + 1;
% % % % % %             end
% % % % % %             
            
            
        end
        (match / 128)*100;
    end
end
originalqp =qp;

for st = 1:3
    qp(:,:,st) = qp(:,:,st) ./ size(find(locs==st),2);
    qqq = qp(:,:,st);
    qqq(find(qqq<=(max(max(qqq))/2))) = 0;
    qqq(find(qqq>(max(max(qqq))/2))) = 1;
    qp(:,:,st) = qqq;
end

%qp = qp ./ max(max(max(qp)));
% 
% qp(find(qp<=.15)) = 0;
% qp(find(qp>.5)) = 1;


        match = 0;
        predict = [];
        for at = r
            trials = qp_trial{at};
            cors = [];
            sums = [];
            for c = 1:3
                cors(c) = corr2(trials,qp(:,:,c));
%                 sums(c) = size(find((trials+qp(:,:,c)) == 2),1);
                sums(c) = size(find((trials(:,(1:100))+qp(:,(1:100),c)) == 2),1);
            end
            [i ii] = max(sums);
            predict(at) = ii;
            
            if locs(at) == 2;
                if ii == locs(at)
                    match = match + 1;
                end
            end
        end

 (match / size(find(locs(r)==2),2))*100

        match = 0;
        predict = [];
        for at = 1:num_trials
            trials = qp_trial{at};
            cors = [];
            sums = [];
            for c = 1:3
                %cors(c) = corr2(trials,qp(:,:,c));
                sums(c) = size(find((trials+qp(:,:,c)) == 2),1);
            end
            [i ii] = max(sums);
            predict(at) = ii;

            if ii == locs(at)
                match = match + 1;
            end
        end

 (match / 427)*100

















