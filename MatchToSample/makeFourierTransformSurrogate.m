function makeFourierTransformSurrogate(varargin)
% in the sessions folder

Args = struct('redo',0,'Fs',200,'tapers',[2 3],'fpass',[0 100],'plength',400,'ML',0);
Args.flags = {'redo','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args);

NeuroInfo = NeuronalChAssign;

params = struct('Fs',Args.Fs,'tapers',Args.tapers,'fpass',Args.fpass);

sdir = pwd;
if Args.ML; mts = mtstrial('auto');end

if isempty(nptDir('skip.txt')) && length(NeuroInfo.groups) > 1
    gcomb = nchoosek(NeuroInfo.groups,2);
    ccomb = nchoosek([1 : length(NeuroInfo.groups)],2);
    cd lfp/
    ldir = pwd;
    ndir = 'fourierTransSur';
    
    if isempty(nptDir(ndir))
        unix(sprintf('mkdir %s',ndir));
    end
    
    cd(ndir)
    surdir = pwd;
    [st,result] = unix('find . -name ''*_fft_*.mat''');
    cd(ldir)
    ndir = sprintf('%s/%s',pwd,ndir);
    files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
    
    [~,num_channels,~,~,~]=nptReadStreamerFile(files(1).name);
    [data,~,~,~] = lfpPcut([1:length(files)],[1 : num_channels],'plength',Args.plength,modvarargin{:},'EPnotRemoved','fromFiles');
    %     [a,num_channels,b,c,d]=nptReadStreamerFile(files(1).name);
    %     clear a b c d
    if Args.ML
        %         nblock = length(find(diff(mts.data.Index(:,1)) ~= 0)) + 1;
        twoTrials = [];
        for ru = 1 : 2
            ind = mtsgetTrials(mts,'rule',1);
            twoTrials = [twoTrials; single(nchoosek(ind,2))];
        end
        
    else
        twoTrials = single(nchoosek([1:length(files)],2));
    end
    
    if Args.redo; startfile = 1; else startfile = length(strfind(result,'.mat')) + 1; end;
    clear result
    
    for nf = startfile : size(twoTrials,1)
        
        fname = regexprep(files(1).name, 'lfp.*',sprintf('fft_%d_%d',twoTrials(nf,1),twoTrials(nf,2)));
        fname = sprintf('%s.mat',fname);
        
        
        %         cd(sdir)
        %         [data,~,~,~] = lfpPcut(twoTrials(nf,:),[1 : num_channels],'plength',Args.plength,modvarargin{:},'EPnotRemoved','fromFiles');
        %
        %              [data,a,b] = lfpPcut(twoTrials(nf,:),[1 : num_channels],'plength',Args.plength,modvarargin{:},'EPnotRemoved');
        %              clear a b
        for p = 1 : 4
            % sample x trials x channels
            for dir = 1 : 2
                
                for nc = 1 : size(ccomb,1)
                    data1 = squeeze(data{p}(:,twoTrials(nf,dir),ccomb(nc,1)));
                    data2 = squeeze(data{p}(:,twoTrials(nf,setdiff([1 2],dir)),ccomb(nc,2)));
                    [~,~,S12(:,nc,dir,p),~,~,f] = coherencyc(data1,data2,params);
                    %                          [a,b,S12{p}(:,nc,dir),c,d,f] = coherencyc(data1,data2,params);
                    %                          clear a b c d
                end
            end
        end
        cd(surdir)
        if nf ==1 ; save('basicInfo.mat','f','ccomb','gcomb'); end
        %         save(fname,'f','S12','ccomb','gcomb')
        fid = fopen(fname,'w'); fwrite(fid,S12); fclose(fid);
        sprintf('saving %s/%s \n',pwd,fname)
        
        
    end
end
cd(sdir)