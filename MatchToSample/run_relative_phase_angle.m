function run_relative_phase_angle(varargin)

%run at session level
%computes relative phase angle tass_1998 and lavenquen
Args = struct('ml',0,'redo',0,'bmf',0);
Args.flags = {'ml','redo','bmf'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
        %         c = mtscpp2('auto','ml');
        N = NeuronalHist('ml','bmf');
        g = bmf_groups; %these are the non-noisy groups
        s = sorted_groups; %these are the sorted groups
        ggroups = intersect(g,s);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        c = mtscpp2('auto','ml');
        N = NeuronalHist('ml');
    end
else
    c = mtscpp2('auto');
    N = NeuronalHist;
    mt = mtstrial('auto','redosetNames');
end

sample_on = floor(mt.data.CueOnset); %lock to sample on

pair_list = [];
for g1 = 1 : size(ggroups,2);
    for g2 = g1+1 : size(ggroups,2)
        pair_list = [pair_list; ggroups(g1) ggroups(g2)];
    end
end

npairs = size(pair_list,1);

%run through all the frequencies for each pair
all_freq = [5 : 5 : 85];
nfreq = size(all_freq,2);

cd([sesdir filesep 'lfp'])

%get index of all lfp trials  lfpdata2.name
lfpdata = nptDir('*_lfp.*');
ntrials = size(lfpdata,1);
for p = 1 : npairs
%     phaseangledata = [ 'relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2) '.mat'];
    if Args.ml
        [~,ch(1)] = intersect(N.gridPos,pair_list(p,1));
        [~,ch(2)] = intersect(N.gridPos,pair_list(p,2));
    else
        ch(1) = pair_list(p,1);
        ch(2) = pair_list(p,2);
    end
    
    %     if ~exist(phaseangledata) || Args.redo
    relative_phases = [];
    unwrapped_diffs = [];
    for t = 1 : ntrials
        fprintf(1,['Pair ' num2str(p) ' of ' num2str(npairs) ', Trials to go = ' num2str(ntrials-t) '\n']);
        trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
        
        if Args.ml
            if Args.bmf
                [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
            else
                %betty = UEI
                orig_data = ReadUEIFile('FileName',trial);
            end
        else
            %clark = Streamer
            [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
        end
        
        %make trial name
        for nf = 1 : nfreq

            %lowpass
            hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,all_freq(nf)-2,all_freq(nf)+2);
            %get data ranges
            datarange = [sample_on(t) - 499 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
            data = hdata(:,datarange);

            data = data'; %need to do this before and after using hilbert, don't include during the transform
            %compute hilbert transform
            data = hilbert(data);
            
            %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
            %COMPENT IS FLIPPED
            %find instantaneous phase angles
            data = angle(data);
            data = data';
            
            
            d1 = unwrap(data(ch(1),:)) ./ (2*pi);
            d2 = unwrap(data(ch(2),:)) ./ (2*pi);
            
            %cyclic relative phase
            d = mod(d1 - d2,1);

            relative_phases{t}(nf,:) = single(d);
        end
    end
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    channel_pairs = [ 'relative_phase_pair' num2strpad(pair_list(p,1),3) num2strpad(pair_list(p,2),3)];
    save(channel_pairs,'relative_phases','all_freq')
    cd(sesdir)
    %     end
end
cd(sesdir)


