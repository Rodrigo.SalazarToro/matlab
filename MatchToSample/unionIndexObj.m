function [uind1,uind2] = unionIndexObj(obj1,ind1,obj2,ind2)





for ii = 1:length(ind1)
    ind = ind1(ii);
    if ~isempty(strfind(obj1.data.setNames{ind},'acute'))
        ndir = regexprep(obj1.data.setNames{ind},'acute/','');
    else
        ndir = obj1.data.setNames{ind};
    end
    name1a(ii,:) = sprintf('%s%04g%02g%s', ndir(1:end-15),obj1.data.Index(ind,3),obj1.data.Index(ind,9),obj1.data.Index(ind,7));
    name1b(ii,:) = sprintf('%s%04g%02g%s', ndir(1:end-15),obj1.data.Index(ind,4),obj1.data.Index(ind,10),obj1.data.Index(ind,8));
end



for ii = 1:length(ind2)
    ind = ind2(ii);
    if ~isempty(strfind(obj2.data.setNames{ind},'acute'))
        ndir = regexprep(obj2.data.setNames{ind},'acute/','');
    else
        ndir = obj2.data.setNames{ind};
    end
    name2a(ii,:) = sprintf('%s%04g%02g%s', ndir(1:end-15),obj2.data.Index(ind,3),obj2.data.Index(ind,9),obj2.data.Index(ind,7));
    name2b(ii,:) = sprintf('%s%04g%02g%s',ndir(1:end-15),obj2.data.Index(ind,4),obj2.data.Index(ind,10),obj2.data.Index(ind,8));
end
name1 = [name1a name1b];
name1f = [name1b name1a];
name2 = [name2a name2b];
name2f = [name2b name2a];
lia1 = ismember(name1,name2,'rows') | ismember(name1,name2f,'rows');
lia2 = ismember(name2,name1,'rows') | ismember(name2,name1f,'rows');

uind1 = ind1(lia1);
uind2 = ind2(lia2);