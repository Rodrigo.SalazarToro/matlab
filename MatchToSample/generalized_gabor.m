function [sse] = generalized_gabor(varargin)

%A,decay,frequency,phase_shift,offset,exponent, B, sig2,c

v = varargin{1}; %intial arguments
c = varargin{2}; %correlogram
% s = varargin{3}; %std
% s = s .^2; %square std to get variance

A = v(1);
decay = v(2);
frequency = v(3);
phase_shift = v(4);
offset = v(5);
exponent = v(6);
B = v(7);
sig2 = v(8);

t = floor(size(c,2) / 2);

time = [(-1*t):t];
for iter = 1 : size(time,2)
    x = time(iter);
    %generalized gabor function see konig_94
    cf(iter) = A * exp( -1*(abs(x-phase_shift)/decay) ^ exponent) * cos(2*pi*frequency*(x-phase_shift)) + offset + B * exp(-x/sig2)^2;
end

error= c - cf;
sse = sum(error.^2);












