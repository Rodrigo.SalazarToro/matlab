function locations = ethyl_lookup(varargin)

%indexes look up table for ethyl
%example ethyl_lookup('table',lut,'day',{'110810'},'groups',[31 238])
Args = struct('table',[],'day',[],'groups',[]);
Args.flags = {''};
Args = getOptArgs(varargin,Args);

day = find([Args.table{:,1}] == str2num(Args.day));
locations = [Args.table{day,Args.groups + 1}]; %must add one to groups because first column is the days
