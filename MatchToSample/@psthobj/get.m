function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'bmfgroups',1,'group',[],'days',[],'min_rate',[],'multi',0,'single',0);
Args.flags = {'Number','ObjectLevel','multi','single'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rt = [1 : size(obj.data.Index,1)]';
    
    if ~isempty(Args.days)
    %temporary, used to find the days
    daytemp = [];
    for allrt = rt'
        if ~isempty(strfind(obj.data.setNames{allrt},Args.days{1}))
            daytemp = [daytemp; allrt];
        end
    end
    else
        daytemp = rt;
    end
    
    
    %get only groups that pass the SNR, rejectCH, and POWER criteria
    if Args.bmfgroups
        rtemp1 = find(obj.data.Index(:,3) == 1)';
    else
        rtemp1 = rt;
    end
    
    
    if ~isempty(Args.group)
        %get multiple groups
        rtemp2 = [];
        for rr = 1 : size(Args.group,2)
            rtemp2 = [rtemp2 find(obj.data.Index(:,1) == Args.group(rr))'];
        end
        rtemp2 = sort(rtemp2);
    else
        rtemp2 = rt;
    end
    
    if ~isempty(Args.min_rate)
        rtemp3 = find(obj.data.Index(:,5) > Args.min_rate)';
    else
        rtemp3 = rt;
    end
    
    if Args.multi %only get multi unit
        rtemp4 = find(obj.data.Index(:,4) == 1)';
    else
        rtemp4 = rt;
    end
    
    if Args.single %only get multi unit
        rtemp5 = find(obj.data.Index(:,4) > 1)';
    else
        rtemp5 = rt;
    end
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,daytemp)))));
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end

