function obj = plot(obj,varargin)

%Arguments
%phase: plots the phase of the coherence
%ide:   plots all 5 stimuli

Args = struct('correct',0,'ide',0,'incorrect',0,'fixation',0,'days',[],'line',0,'smooth',[]);
Args.flags = {'correct','ide','incorrect','fixation','line'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{},'days');

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end

if isempty(Args.days)
    
    %load cohgram info
    load(obj.data.setNames{ind})
    sday = strfind(obj.data.setNames{ind},'/');
    ns = size(sday,2);
    day = obj.data.setNames{ind}(sday(ns-4)+1:sday(ns-3)-1);
    
    binx = [-500 + (binsize/2):binsize:1500 - (binsize/2)];
    nbins = size(binx,2);
    
    %determine which cluster to use
    cluster = obj.data.Index(ind,4);
    
    if Args.ide
        for x = 1:5
            %determine the largest bin height
            mh = 0;
            for xx = 1:5
                hn =  hist(psth{cluster,xx},binx);
                h = hn ./ (trial_count{cluster,xx} * ((2000/nbins)/1000));
                if max(h) > mh
                    mh = max(h);
                end
            end
            
            subplot(5,1,x)
            hn =  hist(psth{cluster,x},binx);
            h = hn ./ (trial_count{cluster,x} * ((2000/nbins)/1000));
            plot(h);hold on
            
            nticks = size([1:1:nbins],2);
            set(gca,'XTick',[1:1:nbins])
            bintime = 2000 / nbins;
            set(gca,'XTickLabel',[-500+bintime:2000/nticks:1500])
            
            plot([(500/bintime) + .5, (500/bintime) + .5],[0 mh],'color','r'); hold on %sample on
            plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 mh],'color','r'); hold on %sample off
            plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 mh],'color','r'); hold on %earliest match
            
            title([day ' group: ' num2str(obj.data.Index(ind,1)) ' cluster: ' num2str(cluster) '   ntrials: ' num2str(trial_count{cluster,x}) '   nspikes: ' num2str(sum(hn))])
            axis([0 nbins 0 mh])
        end
    else
        np = zeros(1,3);
        if Args.correct
            np(1) = 1;
        end
        if Args.incorrect
            np(2) = 1;
        end
        if Args.fixation
            np(3) = 1;
        end
        allcond = [6,7,8];
        numplots = sum(np);
        conds = find(np);
        colors = {'r' 'g' 'b'};
        maxh = 0;
        
        for allp = 1:numplots
            if ~Args.line
                subplot(numplots,1,allp)
            end
            ac = allcond(conds(allp));
            
            hn =  hist(psth{cluster,ac},binx);
            numbtrials = num2str(trial_count{cluster,ac});
            h = hn ./ (trial_count{cluster,ac} * ((2000/nbins)/1000));  %spikes per second. Calculate the total time in each bin, ntrials*bin width in seconds
            
            if Args.line
                if ~isempty(Args.smooth)
                    plot(smooth(h,Args.smooth),colors{ac-5});hold on
                else
                    plot(h,colors{ac-5});hold on
                end
            else
                bar(h,'hist');hold on
            end
            
            nticks = size([1:1:nbins],2);
            set(gca,'XTick',[1:1:nbins])
            bintime = 2000 / nbins;
            set(gca,'XTickLabel',[-500+(bintime/2):2000/nticks:1500])
            if conds(allp) == 1
                title([day ' group: ' num2str(obj.data.Index(ind,1)) ' cluster: ' num2str(cluster) '   ntrials: ' numbtrials '   nspikes: ' num2str(sum(hn))]);
            end
            if max(h) > maxh
                maxh = max(h);
            end
            for allp = 1:numplots
                if ~Args.line
                    subplot(numplots,1,allp)
                end
                axis([0 nbins 0 maxh])
                plot([(500/bintime) + .5, (500/bintime) + .5],[0 maxh],'color','r'); hold on %sample on
                plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 maxh],'color','r'); hold on %sample off
                plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 maxh],'color','r'); hold on %earliest match
            end
        end
    end
else
    pcounter = 0;
    for ind = dataindices
        pcounter = pcounter + 1;
        
        if size(dataindices,2) <= 10;
            subplot(2,5,pcounter)
        elseif size(dataindices,2) <= 15;
            subplot(3,5,pcounter)
        elseif size(dataindices,2) <= 20;
            subplot(4,5,pcounter)
        elseif size(dataindices,2) <= 25;
            subplot(5,5,pcounter)
        elseif size(dataindices,2) <= 30;
            subplot(6,5,pcounter)
        elseif size(dataindices,2) <= 35;
            subplot(7,5,pcounter)
        elseif size(dataindices,2) <= 40;
            subplot(8,5,pcounter)
        elseif size(dataindices,2) <= 45;
            subplot(9,5,pcounter)
        elseif size(dataindices,2) <= 50;
            subplot(10,5,pcounter)
        end
        
        %load cohgram info
        load(obj.data.setNames{ind})
        sday = strfind(obj.data.setNames{ind},'/');
        day = obj.data.setNames{ind}(sday(6)+1:sday(7)-1);
        
        binx = [-500 + (binsize/2):binsize:1500 - (binsize/2)];
        nbins = size(binx,2);
        
        cluster = obj.data.Index(ind,4);
        
        hn =  hist(psth{cluster,6},binx);
        numbtrials = num2str(trial_count{cluster,6});
        h = hn ./ (trial_count{cluster,6} * ((2000/nbins)/1000));  %spikes per second. Calculate the total time in each bin, ntrials*bin width in seconds
        
        bar(h,'hist');hold on
        
        nticks = size([1:1:nbins],2);
        set(gca,'XTick',[1:1:nbins])
        bintime = 2000 / nbins;
        set(gca,'XTickLabel',[-500+(bintime/2):2000/nticks:1500])
        
        plot([(500/bintime) + .5, (500/bintime) + .5],[0 max(h)],'color','r'); hold on %sample on
        plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 max(h)],'color','r'); hold on %sample off
        plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 max(h)],'color','r'); hold on %earliest match
        title([day ' group: ' num2str(obj.data.Index(ind,1)) ' cluster: ' num2str(cluster) '   ntrials: ' numbtrials '   nspikes: ' num2str(sum(hn))])
        axis([0 nbins 0 max(h)])
        
    end
end





