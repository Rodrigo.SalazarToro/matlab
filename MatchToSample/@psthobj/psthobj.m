function obj = psthobj(varargin)

%makes bmfCohgram object

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'psthobj';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'psthobj';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

daydir = pwd;
cd([daydir filesep 'session01'])
sesdir = pwd;
gg = bmf_groups; %get list of good groups
N = NeuronalHist('bmf');
cd([sesdir filesep 'highpass'])
highpassdir = pwd;

if exist('psth','dir')
    cd([highpassdir filesep 'psth'])
    psthdir = pwd;
    
    files = nptDir('psth*');
    nfiles = size(files,1);
    if ~isempty(files)
        spikecounter = 0;
        for f = 1 : nfiles
            load(files(f).name,'numnc','average_rate','binsize')
            unit_type = 0;
            for allspikes = 1 : numnc %get multi and single units, first one is a multi
                unit_type = unit_type + 1;
                spikecounter = spikecounter + 1;
                %get group (this corresponds to the electrode position)
                g = str2double(files(f).name(end-7:end-4));
                data.Index(spikecounter,1) = g;
                
                %get channel number (this corresponds to the order of recorded channels
                [~,cch] = intersect(N.gridPos,g);
                data.Index(spikecounter,2) = cch;
                
                %determine if groups is good
                if ~isempty(intersect(gg,g))
                    data.Index(spikecounter,3) = 1;
                end
                
                data.Index(spikecounter,4) = unit_type; % 1 = multi, 2+ = single unit
                
                data.Index(spikecounter,5) = average_rate{allspikes,6}; %this is the average rate for the correct trials
                
                data.Index(spikecounter,6) = binsize; %this is the binsize that the statistics were made with
                
                %load baseline information
                data.setNames{spikecounter,1} = [psthdir filesep files(f).name];
            end
        end
        
        data.numSets = spikecounter;
        
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        obj = createEmptyObject(Args);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
