function run_evoked_potential_calculation(varargin)

Args = struct('days',[],'sessions',[],'evp_sample_locked',0,'evp_saccade_locked',0);
Args.flags = {'evp_sample_locked','evp_saccade_locked'};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl')

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            rejected_trials = get_reject_trials;
            N = NeuronalHist('bmf');
            %this gets the groups that pass reject_filter.m
            [g,~,~,ch] = bmf_groups;
            
            nch = size(ch,2);
            
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
            for ides = 1 : 5
                trials{ides} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'iCueObj',ides,'notfromMTStrial','Nlynx');
            end
            
            trials{6} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
            trials{7} = mtsgetTrials(mt,'BehResp',0,'notfromMTStrial','Nlynx'); %incorrect
            
            %get interleaved fixation trials
            fixtrials = find(mt.data.CueObj == 56)'; %see ProcessSessionMTS
            [~,rej] = intersect(fixtrials,rejected_trials);
            fixtrials(rej) = [];
            trials{8} = fixtrials;
            
            %get the fixation block (if it exists)
            fixblocktrials = find(mt.data.CueObj == 57)';
            [~,rej] = intersect(fixblocktrials,rejected_trials);
            fixblocktrials(rej) = [];
            trials{9} = fixblocktrials;
            fixblock = 1;
            if isempty(trials{9})
                fixblock = 0;
            end
            
            %get the blank trials
            blanktrials = find(mt.data.CueObj == 55)';
            [~,rej] = intersect(blanktrials,rejected_trials);
            blanktrials(rej) = [];
            trials{10} = blanktrials;
            blankblock = 1;
            if isempty(trials{10})
                blankblock = 0;
            end
            
            trials{11} = mtsgetTrials(mt,'ML','notfromMTStrial','Nlynx');
            
            
            sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
            match = floor(mt.data.MatchOnset);    %match
            sac = mt.data.MatchOnset + mt.data.FirstSac;
            rawtrials = nptDir(['ethyl' Args.days{d} num2strpad(Args.sessions,2) '.*']);
            
            cd([sesdir filesep 'lfp']);
            lfpdir = pwd;
            lfptrials = nptDir('ethyl*');
            
            if Args.evp_sample_locked
                %run for 5 ides, all trials, incorrect, interleaved fixation,
                %fixation block and blank trials
                for all_ides = 6:7% 1 : 11
                    if all_ides == 11
                        cd(sesdir)
                    end
                    idetrials = trials{all_ides};
                    ntrials = size(idetrials,2);
                    
                    evokedmatrix = zeros(nch,2600);
                    counter = 0;
                    for t = idetrials
                        alldata = [];
                        counter = counter + 1;
                        fix = sample_on(t) - 500; %this is approx fixation
                        
                        if all_ides == 8 | all_ides == 9 | all_ides == 10%fixation
                            sac = match(t);
                        else
                            sac = match(t) + 200; %match plus saccade
                        end
                        
                        if all_ides ~= 11
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            alldata = data.rawdata;
                        else
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(rawtrials(t).name);
                            alldata = nptLowPassFilter(data.rawdata,data.samplingRate,1,300); %lfp is 1:150, use 1:300
                        end
                        
                        alldata = alldata(ch,(fix:sac)); %take out only the desired trials and time
                        %blank trials can be longer
                        if size(alldata,2) > size(evokedmatrix,2);
                            alldata = alldata(:,(1:size(evokedmatrix,2)));
                        else
                            padn = size(evokedmatrix,2) - size(alldata,2);
                            alldata = padarray(alldata',padn,'post')'; %pad array so that the matrices can be added togethe
                        end
                        
                        evokedmatrix = evokedmatrix + alldata; %put all the data in one matrix
                        
                        %                     if rem(counter,20) == 0
                        %                         hold on;
                        %                         plot(evokedmatrix(8,:)./10);
                        %                         pause
                        %                         evokedmatrix = zeros(nch,2600);
                        %                     end
                    end
                    allevokedpotentials{all_ides} = evokedmatrix ./ ntrials;
                    trial_count{all_ides} = ntrials;
                end
                
                cd(lfpdir)
                if ~exist('evokedpotentials','dir')
                    mkdir('evokedpotentials')
                end
                cd([lfpdir filesep 'evokedpotentials']);
                for p = 1 : nch
                    evp_pairs = [ 'evokedpotential' num2strpad(g(p),4)];
                    
                    %seperate out the information for the individual channels
                    evokedpotential = [];
                    for all_ides = 1 : 11
                        evokedpotential{all_ides} = allevokedpotentials{all_ides}(p,:);
                    end
                    
                    write_info = writeinfo(dbstack);
                    save(evp_pairs,'evokedpotential','trial_count','write_info');
                end
            end
            
            
            
            if Args.evp_saccade_locked
                %run for saccade locked
                %run for 5 ides, all trials, incorrect, interleaved fixation,
                %fixation block and blank trials
                for all_ides = 6:7% 1 : 11
                    if all_ides == 11
                        cd(sesdir)
                    end
                    idetrials = trials{all_ides};
                    ntrials = size(idetrials,2);
                    
                    evokedmatrix = zeros(nch,1500);
                    counter = 0;
                    for t = idetrials
                        alldata = [];
                        counter = counter + 1;

                        
                        fsac = sac(t);
      
                        
%                         
%                         if all_ides == 8 | all_ides == 9 | all_ides == 10%fixation
%                             sac = match(t);
%                         else
%                             sac = match(t) + 200; %match plus saccade
%                         end
                        
                        if all_ides ~= 11
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            alldata = data.rawdata;
                            
                            
                             [alldata, resample_rate] = nptLowPassFilter(data.rawdata,1000,8,25);
                        else
%                             [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(rawtrials(t).name);
%                             alldata = nptLowPassFilter(data.rawdata,data.samplingRate,1,300); %lfp is 1:150, use 1:300
                        end
                        
                        alldata = alldata(ch,(fsac-499:fsac+1000)); %take out only the desired trials and time
                        %blank trials can be longer
% %                         if size(alldata,2) > size(evokedmatrix,2);
% %                             alldata = alldata(:,(1:size(evokedmatrix,2)));
% %                         else
% %                             padn = size(evokedmatrix,2) - size(alldata,2);
% %                             alldata = padarray(alldata',padn,'post')'; %pad array so that the matrices can be added togethe
% %                         end
                        
                        evokedmatrix = evokedmatrix + alldata; %put all the data in one matrix
                        
                        %                     if rem(counter,20) == 0
                        %                         hold on;
                        %                         plot(evokedmatrix(8,:)./10);
                        %                         pause
                        %                         evokedmatrix = zeros(nch,2600);
                        %                     end
                    end
                    allevokedpotentials{all_ides} = evokedmatrix ./ ntrials;
                    trial_count{all_ides} = ntrials;
                end
                
                cd(lfpdir)
                if ~exist('evokedpotentials','dir')
                    mkdir('evokedpotentials')
                end
                cd([lfpdir filesep 'evokedpotentials']);
                for p = 1 : nch
                    evp_pairs = [ 'evokedpotential_saccade_locked' num2strpad(g(p),4)];
                    
                    %seperate out the information for the individual channels
                    evokedpotential = [];
                    for all_ides =6:7% 1 : 11
                        evokedpotential{all_ides} = allevokedpotentials{all_ides}(p,:);
                    end
                    
                    write_info = writeinfo(dbstack);
                    save(evp_pairs,'evokedpotential','trial_count','write_info');
                end
                
            end
        end
    end
end

cd(monkeydir)












