function [sigvalue,sigcoh,sigphi,f,varargout] = coherence_summary(days,varargin)
% to run in the monkey direcory
% need MTSlfp3 to be run before and save files. Also needs days as the
% switching days
% pvalue = 2 corresponds to p < 0.00001 or could be 1 or 3 for the lower
% and higher levels
% list is the 3rd output of selectPairs

Args = struct('cohtype','cohInter','Fborder',0,'pvalue',1,'beta',[12 25],'gamma',[26 40],'plot',0,'prctile',[25 50 75 90],'coherence',0,'power',0,'list',[],'direct','x2y','allPairs',0,'Pnorm',0,'allPower',0);
Args.flags = {'plot','coherence','power','allPairs','Pnorm','allPower'};
[Args,modvarargin] = getOptArgs(varargin,Args);







%% plotting options

lb = {'b' 'k'}; % black for location and blue for identity
therules = {'Identity' 'Location'};
xlabel('Frequency [Hz]')
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
epochsc = {'-' '--' '-.' ':'};
faxis = [Args.Fborder 60]; % 100-Args.Fborder];

philimit = 3.2;
bins = [-philimit : 0.4: philimit];
phiaxis = philimit+0.1;
%%

for r = 1 : 2;for p = 1: 4; sigvalue{r,p} = []; sigphi{r,p} = [];sigcoh{r,p} = []; if Args.power; powerPP{r,p} = []; powerPF{r,p} = []; end; end; end;ncount = zeros(1,2);

%% acquiring the data
for d = 1 : size(days,2)
    clear rules
    cd(days{d});
    betty = findstr(days{d},'betty');
    cohfile = nptDir(sprintf('%s.mat',Args.cohtype));
    surfile = nptDir('generalSur.mat');
    if ~isempty(cohfile) & ~isempty(surfile)
        data = load(cohfile.name);
        sur = load(surfile.name);
        
        f = data.f;
        ffind = find(f >= faxis(1) & f <= faxis(2));
        fband = find(f>= Args.Fborder & f <= (f(end) - Args.Fborder));
        [ch,CHcomb,iflat,groups] = getFlatCh;
        if isempty(findstr(Args.cohtype,'SFC'))
            if Args.allPairs
                combs = setdiff([1:size(data.Day.comb,1)],iflat.pairs);
                
                
                
                %             combs = [1 : ncomb];
            elseif ~isempty(Args.list)
                thelist = Args.list;
                forday = strmatch(days{d},thelist.days);
                ncomb =  length(forday);
                combs = thelist.npair(forday);
            else
                %                 indpairs = load('indpairs.mat');
                %                 combs = indpairs.indpairs;
                [r,combs] = get(obj,'Number');
            end
            ncomb = length(combs);
        end
        nses = length(data.Day.session);
        eval(sprintf('load(''%s.mat'')',Args.cohtype))
        if ~isempty(betty); rules = [1 2]; else; for s = 1 : nses; rules(s) = Day.session(s).rule; end; end
        for ses = 1 : nses
            
            if ~isempty(betty); rule = ses; else; rule = Day.session(ses).rule; end
            thes = find(rules == rule);
            if ses == thes(end)
                if ~isempty(findstr(Args.cohtype,'SFC'))
                    nch = size(data.Day.session(ses).C{1},3);
                    nunit = size(data.Day.session(ses).C{1},2);
                    ncomb = nch * nunit;
                    combs = [1 : ncomb];
                    cn = 1; for ch = 1 : nch; for unit = 1 : nunit; combtype(cn,:) = [unit ch]; cn = cn + 1; end; end;
                end
                if isempty(findstr(Args.cohtype,'GC')); thedata = data.Day.session(ses).C; else; thedata = data.Day.session(ses).Fx2y; end
                if ~isempty(thedata)
                    
                    for p = 1 : 4 ; usePP{p} = [];  usePF{p} = [];end
                    
                    for comb = vecr(combs)
                        
                        ncount(rule) = ncount(rule) + 1;
                        for p = 1 : 4
                            
                            %                             coht = squeeze(sur.Day.session(ses).Cmean(:,comb,p)) + stdfold(th) * squeeze(sur.Day.session(ses).Cstd(:,comb,p));
                            
                            if ~isempty(findstr(Args.cohtype,'GC'))
                                directs = {'x2y' 'y2x'};
                                odir = setdiff(directs,Args.direct);
                                cohc = eval(sprintf('data.Day.session(ses).F%s(:,comb,p)',Args.direct));
                                phi = eval(sprintf('data.Day.session(ses).F%s(:,comb,p)',odir{1}));
                                coht = eval(sprintf('squeeze(sur.Day.session(ses).%sProb(:,Args.pvalue,comb,p));',Args.direct));
                                
                            elseif ~isempty(findstr(Args.cohtype,'SFC'))
                                S12 = []; for ns = 1 : length(thes); S12 = [S12 squeeze(data.Day.session(ses).S12{p}(:,combtype(comb,1),combtype(comb,2)))]; end
                                S1 = []; for ns = 1 : length(thes); S1 = [S1 squeeze(data.Day.session(ses).S1{p}(:,combtype(comb,1),combtype(comb,2)))]; end
                                S2 = []; for ns = 1 : length(thes); S2 = [S2 squeeze(data.Day.session(ses).S2{p}(:,combtype(comb,1),combtype(comb,2)))]; end
                                surt = []; for ns = 1 : length(thes); surt = [surt squeeze(sur.Day.session(ses).sur{p}(:,Args.pvalue,combtype(comb,1),combtype(comb,2)))]; end
                                S12 = mean(S12,2); S1 = mean(S1,2); S2 = mean(S2,2); surt = mean(surt,2);
                                C12=S12./sqrt(S1.*S2);
                                
                                
                                cohc = abs(C12); % cohc = squeeze(data.Day.session(ses).C{p}(:,combtype(comb,1),combtype(comb,2)));
                                phi = angle(C12); % phi = squeeze(data.Day.session(ses).phi{p}(:,combtype(comb,1),combtype(comb,2)));
                                coht = surt; % coht = squeeze(sur.Day.session(ses).sur{p}(:,Args.pvalue,combtype(comb,1),combtype(comb,2)));
                            else
                                S12 = []; for ns = 1 : length(thes); S12 = [S12 squeeze(data.Day.session(ses).S12(:,comb,p))]; end
                                S1 = []; for ns = 1 : length(thes); S1 = [S1 squeeze(data.Day.session(ses).S1(:,comb,p))]; end
                                S2 = []; for ns = 1 : length(thes); S2 = [S2 squeeze(data.Day.session(ses).S2(:,comb,p))]; end
                                surt = []; for ns = 1 : length(thes); surt = [surt squeeze(sur.Day.session(ses).Prob(:,Args.pvalue,1,p))]; end
                                S12 = mean(S12,2); S1 = mean(S1,2); S2 = mean(S2,2); surt = mean(surt,2);
                                C12=S12./sqrt(S1.*S2);
                                cohc = abs(C12); % cohc = squeeze(data.Day.session(ses).C(:,comb,p));
                                phi = angle(C12); % phi = squeeze(data.Day.session(ses).phi(:,comb,p));
                                coht = surt; % coht = squeeze(sur.Day.session(ses).Prob(:,Args.pvalue,comb,p));
                                %                                 coht = repmat(0.14,65,1);
                            end
                            
                            %                                 plot(f,cohc,'r'); hold on; plot(f,coht); pause; clf
                            %                             if Args.ML
                            %                                 thePP = data.Day.comb(comb,2);
                            %                                 thePF = data.Day.comb(comb,1);
                            %                                 % this need to be changed if the cohInter.mat
                            %                                 % are rerun with the new version of NeuroAssign
                            %                             else
                            thePP = data.Day.comb(comb,1);
                            thePF = data.Day.comb(comb,2);
                            %                             end
                            
                            sigco = cohc > coht;
                            signcoh{rule}(:,ncount(rule),p) = sigco;
                            
                            
                            if Args.power;
                                %                                 if Args.ML
                                %                                     if isempty(find(thePP == usePP{p})); powerPP{rule,p} = [powerPP{rule,p} S2]; usePP{p} = [usePP{p} thePP]; end;
                                %                                     if isempty(find(thePF == usePF{p})); powerPF{rule,p} = [powerPF{rule,p} S1]; usePF{p} = [usePF{p} thePF]; end
                                %                                 else
                                if isempty(find(thePP == usePP{p})); powerPP{rule,p} = [powerPP{rule,p} S1]; usePP{p} = [usePP{p} thePP]; end;
                                if isempty(find(thePF == usePF{p})); powerPF{rule,p} = [powerPF{rule,p} S2]; usePF{p} = [usePF{p} thePF]; end
                                %                                 end
                            end
                            if ~isempty(find(sigco(fband) == 1))
                                sigvalue{rule,p} = [sigvalue{rule,p} cohc];
                                sigphi{rule,p} = [sigphi{rule,p} phi];
                                sigcoh{rule,p} = [sigcoh{rule,p} sigco];
                            end
                        end
                        
                    end
                    
                    
                end
            end
            
        end
    end
    
    
    cd ..
    
end %for d = 1 : size(days,1)
%%
varargout{1} = signcoh;
varargout{2} = ncount;
% d = 37 problem with the surrogate
%% phase distribution at specific bands
band{1} = find(f >= Args.beta(1) & f <=Args.beta(2));
band{2} = find(f >= Args.gamma(1) & f <=Args.gamma(2));
for r =1 : 2
    for p = 1 : 4
        for b = 1 : size(band,2)
            range = band{b};
            %                 totn{r,p}(:,b) = zeros(1,length(bins));
            sigpair = sum(sigcoh{r,p}(range,:),1) ~= 0;
            n = sum(sigpair);
            [maxcoh,ind] = max(sigvalue{r,p}(range,sigpair),[],1);
            tphi = sigphi{r,p}(range,sigpair);
            phase = []; for i = 1 : length(ind); phase = [phase tphi(ind(i),i)];end;
            totn{r,p}(:,b) = hist(phase,bins')/n;
            
        end
        
    end
    
end
%%
varargout{3} = totn;
if Args.power;
    varargout{4} = powerPP;
    varargout{5} = powerPF;
end
%% Plotting
if Args.plot
    
    nh = 1;
    
    h(nh) = figure;
    
    set(h(nh),'Name',sprintf('%s : percent # of significant pairs',Args.cohtype));
    
    maxv = [];
    for r =1 : 2;
        for p = 1 : 4;
            subplot(4,1,p);
            value = 100 * sum(squeeze(signcoh{r}(:,:,p)),2) / size(signcoh{r},2);
            plot(f,value,sprintf('%s',lb{r}))
            hold on
            ylabel(sprintf('%s [perc. pairs]',epochs{p}))
            maxv = [maxv max(value)];
            
        end
        xlabel('Frequency [Hz]')
    end
    for sb = 1 : 4; subplot(4,1,sb); axis([faxis 0 1.1 * max(maxv)]); grid on; end
    legend('IDE','LOC')
    
    nh = nh + 1;
    
    h(nh) = figure;
    
    set(h(nh),'Name',sprintf('%s : percent # of significant pairs',Args.cohtype));
    
    
    for r =1 : 2;
        for p = 1 : 4;
            subplot(1,2,r);
            value = 100 * sum(squeeze(signcoh{r}(:,:,p)),2) / size(signcoh{r},2);
            plot(f,value,sprintf('%s',epochsc{p}))
            hold on
            ylabel('significant pairs [%]')
            title(sprintf('%s',therules{r}));
            
            legend(epochs)
        end
        xlabel('Frequency [Hz]')
    end
    for sb = 1 : 2; subplot(1,2,sb); axis([faxis 0 1.1 * max(maxv)]); grid on;end
    
    nh = nh + 1;
    
    h(nh) = figure;
    if isempty(findstr(Args.cohtype,'GC'))
        set(h(nh),'Name',sprintf('%s : distri. of coh and phi  values for sign. pairs',Args.cohtype));
    else
        set(h(nh),'Name',sprintf('%s : distri. of GC  values for sign. pairs',Args.cohtype));
    end
    themax = [];
    for r = 1 : 2;
        for p = 1 : 4;
            if Args.coherence; sigvalue{r,p} = sigvalue{r,p}.^2; end
            pr = Args.prctile;
            for prc = 1 : length(pr)
                subplot(4,2,(p-1)*2+1);
                dist{prc} = prctile(sigvalue{r,p},pr(prc),2);
                themax = [themax max(dist{prc}(ffind))];
                %                 dist{prc} = prctile([sigvalue{1,p} sigvalue{2,p}],pr(prc),2);
                plot(f,dist{prc},sprintf('%s',sprintf('%s%s',lb{r},epochsc{prc})))
                %                 plot(f,dist{prc})
                %                 axis([faxis 0 0.6])
                hold on
                grid on
                subplot(4,2,(p-1)*2+2);
                distP{prc} = prctile(sigphi{r,p},pr(prc),2);
                plot(f,distP{prc},sprintf('%s',lb{r}))
                hold on
                grid on
            end
            subplot(4,2,(p-1)*2+1); ylabel(sprintf('%s [coh]',epochs{p}));
            
            if isempty(findstr(Args.cohtype,'GC'))
                subplot(4,2,(p-1)*2+2); ylabel(sprintf('%s [phase]',epochs{p}));
            else
                subplot(4,2,(p-1)*2+2); ylabel(sprintf('%s [coh]',epochs{p}));
            end
            if isempty(findstr(Args.cohtype,'GC'))
                line(f,zeros(1,length(f)),'LineStyle','--')
                line(f,repmat(pi,1,length(f)),'LineStyle','--')
                line(f,repmat(-pi,1,length(f)),'LineStyle','--')
                axis([faxis -pi pi])
            end
            if p ==4; xlabel('Frequency [Hz]'); end
            if ~isempty(findstr(Args.cohtype,'GC')); subplot(4,2,1); title(Args.direct);subplot(4,2,2); title(odir); end
        end
        
    end
    for sb = [1 3 5 7]; subplot(4,2,sb); axis([faxis 0 max(themax)]); end
    if ~isempty(findstr(Args.cohtype,'GC'))
        for sb = [2 4 6 8]; subplot(4,2,sb); axis([faxis 0 max(themax)]); end
    end
    h(nh) = figure;
    set(h(nh),'Name',sprintf('%s : distr. of phase for gamma and beta',Args.cohtype));
    nh = nh + 1;
    for r =1 : 2
        for p = 1 : 4
            
            subplot(4,2,(p-1)*2+r)
            
            bar(bins,100*totn{r,p});
            hold on
            %                     plot(bins,100*totn{r,p}(2,:)/length(range)/size(sigphi{r,p},2),'b')
            if p ==1; if r ==1; title('Identity'); elseif r ==2; title('Location');end; end;
            if p ==4; xlabel('Phase [radians]');end;
            axis([-phiaxis phiaxis 0 100])
            %         axis([-3.6 3.6 0 1500])
        end
        %     if r ==1; for sb = 1 : 4;subplot(4,2,(sb-1)*2+1);ylabel(sprintf('%s [sum]',epochs{sb}));end;end
        if r ==1; for sb = 1 : 4;subplot(4,2,(sb-1)*2+1);ylabel(sprintf('%s [perc. pairs]',epochs{sb}));end;end
        legend('beta','gamma')
    end
    
    if Args.power
        nh = nh + 1;
        themax = [];
        themaxP = [];
        h(nh) = figure;
        set(h(nh),'Name',sprintf('%s : distri. of PP and PF power',Args.cohtype));
        if Args.Pnorm
            Pf = f;
        else
            
            Pf = ones(1,length(f));
        end
        for r = 1 : 2;
            for p = 1 : 4;
                
                if ~Args.allPower
                    pr = Args.prctile;
                    for prc = 1 : length(pr)
                        subplot(4,2,(p-1)*2+1);
                        dist{prc} = prctile(powerPP{r,p},pr(prc),2)  .* Pf';
                        %                 dist{prc} = prctile([sigvalue{1,p} sigvalue{2,p}],pr(prc),2);
                        plot(f,dist{prc},sprintf('%s%s',lb{r},epochsc{prc}))
                        %                 plot(f,dist{prc})
                        %                 axis([faxis 0 0.6])
                        
                        hold on
                        subplot(4,2,(p-1)*2+2);
                        distP{prc} = prctile(powerPF{r,p},pr(prc),2) .* Pf';
                        
                        if ~strcmp(epochsc{prc},'-');
                            hand = plot(f,distP{prc},sprintf('%s',lb{r},epochsc{prc})); set(get(get(hand,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                        else
                            plot(f,distP{prc},sprintf('%s',lb{r},epochsc{prc}));
                        end
                        hold on
                        themax = [themax max(dist{prc})];
                        themaxP = [themaxP max(distP{prc})];
                    end
                else
                    subplot(4,2,(p-1)*2+1);
                    
                    plot(f,powerPP{r,p}.* repmat(Pf',1,size(powerPP{r,p},2)))
                    
                    hold on
                    subplot(4,2,(p-1)*2+2);
                    plot(f,powerPF{r,p} .* repmat(Pf',1,size(powerPF{r,p},2)));
                    themax = [themax max(max(powerPP{r,p}))];
                    themaxP = [themaxP max(max(powerPF{r,p}))];
                    hold on
                end
                subplot(4,2,(p-1)*2+1); ylabel(sprintf('%s [power]',epochs{p}));
                
                axis([faxis 0 max(themax)])
                subplot(4,2,(p-1)*2+2); ylabel(sprintf('%s [power]',epochs{p}));
                %                 line(f,zeros(1,length(f)),'LineStyle','--')
                %                 line(f,repmat(pi,1,length(f)),'LineStyle','--')
                %                 line(f,repmat(-pi,1,length(f)),'LineStyle','--')
                axis([faxis 0 max(themaxP)])
                if p ==4; xlabel('Frequency [Hz]'); end
                
            end
            
        end
        subplot(4,2,1); title('Power PP'); text(50,3*max(themax)/4,sprintf('n = %d',size(powerPP{1},2)));
        subplot(4,2,2); title('Power PF'); text(50,3*max(themaxP)/4,sprintf('n = %d',size(powerPF{1},2)));
        if ~Args.allPower;
            subplot(4,2,7); for prc = 1 : length(pr); le{prc} = sprintf('%dth prctile',pr(prc)); end; legend(le);
            subplot(4,2,8); legend(therules);
        end
    end
    
    
end


%%