function make_bmf_channel_density_grid(varargin)

%loads power information written from "write_power_trials" and plot
%information in a grid


Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;
load alldays

allrecordings = [];
for x = 1 : size(alldays,2);
    cd([monkeydir filesep alldays{x} filesep 'session01'])
    i = bmf_groups;
    allrecordings = [allrecordings i];
end
n = size(allrecordings,2);
uniquen = size(unique(allrecordings),2);
%bmf grid
grid = [ ...
    
0, 0,  0,  0,  0,  0,  50, 61, 72, 83, 94,  105, 116, 127, 138, 149, 160, 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  0,  30, 40, 51, 62, 73, 84, 95,  106, 117, 128, 139, 150, 161, 171, 181, 191, 0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  21, 31, 41, 52, 63, 74, 85, 96,  107, 118, 129, 140, 151, 162, 172, 182, 192, 201, 210, 0,   0,   0,   0,   0,   0;
0, 0,  13, 22, 32, 42, 53, 64, 75, 86, 97,  108, 119, 130, 141, 152, 163, 173, 183, 193, 202, 211, 219, 227, 0,   0,   0,   0;
0, 6,  14, 23, 33, 43, 54, 65, 76, 87, 98,  109, 120, 131, 142, 153, 164, 174, 184, 194, 203, 212, 220, 228, 235, 0,   0,   0;
0, 7,  15, 24, 34, 44, 55, 66, 77, 88, 99,  110, 121, 132, 143, 154, 165, 175, 185, 195, 204, 213, 221, 229, 236, 242, 0,   0;
1, 8,  16, 25, 35, 45, 56, 67, 78, 89, 100, 111, 122, 133, 144, 155, 166, 176, 186, 196, 205, 214, 222, 230, 237, 243, 248, 0;
2, 9,  17, 26, 36, 46, 57, 68, 79, 90, 101, 112, 123, 134, 145, 156, 167, 177, 187, 197, 206, 215, 223, 231, 238, 244, 249, 253;
3, 10, 18, 27, 37, 47, 58, 69, 80, 91, 102, 113, 124, 135, 146, 157, 168, 178, 188, 198, 207, 216, 224, 232, 239, 245, 250, 254;
4, 11, 19, 28, 38, 48, 59, 70, 81, 92, 103, 114, 125, 136, 147, 158, 169, 179, 189, 199, 208, 217, 225, 233, 240, 246, 251, 255;
5, 12, 20, 29, 39, 49, 60, 71, 82, 93, 104, 115, 126, 137, 148, 159, 170, 180, 190, 200, 209, 218, 226, 234, 241, 247, 252, 256];

grid = flipud(grid');

% Get XY coordinates for all channels
xy = [];
for row = 1:28
    for column = 1:11
        if grid(row,column) ~= 0
            xy(grid(row,column),1) = row;
            xy(grid(row,column),2) = column;
        end
    end
end

figure('Position',[150 200 600 950])

mx = 0;
for channels = 1 : 256
    nx = [];
    nx = size(find(allrecordings == channels),2);
                 
    if nx > mx
        mx = nx;
    end
    scatter(xy(channels,2),(11-xy(channels,1)),50,[0 0 0]);hold on
    if ~isempty(nx)
        
        if nx > 20
            scatter(xy(channels,2),(11-xy(channels,1)),400,[0 0 0],'filled');hold on
        elseif nx > 10
            scatter(xy(channels,2),(11-xy(channels,1)),200,[0 0 0],'filled');hold on
        elseif nx > 5
            scatter(xy(channels,2),(11-xy(channels,1)),100,[0 0 0],'filled');hold on
        elseif nx > 0
            scatter(xy(channels,2),(11-xy(channels,1)),50,[0 0 0],'filled');hold on
        end
        hold on
%         text((xy(channels,2) - .25),(11-xy(channels,1)),num2str(channels),'FontSize',9,'FontWeight','bold','color','r')
    end
end

axis([-1 13 -20 14])
hold on
scatter(0,13,400,[0 0 0],'filled');hold on
text(1,13,'20+');hold on
scatter(0,12,200,[0 0 0],'filled');hold on
text(1,12,'10+');hold on
scatter(0,11,100,[0 0 0],'filled');hold on
text(1,11,'5+');hold on
scatter(0,10,50,[0 0 0],'filled');hold on
text(1,10,'1+');hold on

text(0,9,['n = ' num2str(n)]);hold on
text(0,8,['unique n = ' num2str(uniquen)]);

mx
