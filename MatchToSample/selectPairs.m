function [spairs,ntot,varargout] = selectPairs(varargin)
% to be run in the monkey directory
% Args.data = list obtained from mkListPairs


Args = struct('type','windowSpectra','rule',[1 2],'windows',[3 4],'fband',[],'gui',0,'year',[],'data',[],'ruleComb',{'union'},'sigSur',0,'nptAbove',1);
Args.comparison = {'pos'}; Args.range = {'beta'};
Args.flags = {'gui','sigSur'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'gui'});

dir = pwd;

clark = findstr(dir,'clark');
betty = findstr(dir,'betty');

if ~isempty(clark)
    if isempty(Args.year)
        Args.year = 6;
    end
    if isempty(Args.fband)
        Args.fband = [14 30;30 42; 7 13; 1 7];
    end
elseif ~isempty(betty)
    if isempty(Args.year)
        Args.year = 9;
    end
    if isempty(Args.fband)
        Args.fband = [14 30;30 42; 7 13; 1 7];
    end
end
if isempty(Args.data)
    load list.mat
else
    var = fieldnames(Args.data);
    for dd = 1 : length(var)
        eval(sprintf('%s = Args.data.%s;',var{dd},var{dd}));
    end
end
ntot = size(Index{1},1);
compareChart = [1 2;1 3;1 4;2 3;2 4;3 4];

for c = 1 : size(Args.windows,1)
    if ~Args.sigSur
        
        [val, ia, allcmp(c)] = intersect(Args.windows(c,:),compareChart,'rows');
    end
    switch Args.range{c}
        case 'beta'
            allband(c) = 1;
        case 'gamma'
            allband(c) = 2;
        case 'alpha'
            allband(c) = 3;
        case 'theta'
            allband(c) = 4;
    end
    %     if ~isempty(strmatch(Args.range{c},'beta')); allband(c) = 1; else allband(c) = 2; end
end

beta = Args.fband(1,:);
gamma = Args.fband(2,:);
alpha = Args.fband(3,:);
theta = Args.fband(4,:);
limitB = find(f>= beta(1) & f<= beta(2));
limitG = find(f>= gamma(1) & f<= gamma(2));
limitA = find(f>= alpha(1) & f<= alpha(2));
limitT = find(f>= theta(1) & f<= theta(2));
Flimit = [limitB(1) limitB(end); limitG(1) limitG(end); limitA(1) limitA(end); limitT(1) limitT(end)];

for r = Args.rule
    if Args.sigSur
        if ~isempty(Smod{r})
            [row,cc] = find(Smod{r}(Flimit(allband(c),1):Flimit(allband(c),2),:,Args.windows) > 0);
            cu = unique(cc);
            multipt = [];for allcu = 1 : length(cu); if length(find(cc == cu(allcu))) >= Args.nptAbove; multipt = [multipt cu(allcu)]; end; end; % find at least 2 pts above surrogate
            selpairs{c} = Index{Args.rule(c)}(multipt,:);
            if c >1; tspairs{r} = intersect(spairs,selpairs{c},'rows'); else tspairs{r} = selpairs{c}; end
            
        else
            selpairs{c} = [];
            tspairs{r} = [];
        end
    else
        for c = 1 : length(allcmp)
            %         if allband(c) == 3
            %             if isempty(coher{r})
            %                 selpairs{c} = [];
            %                 tspairs{r} = [];
            %             else
            %                 cc = [];
            %                 for p = 1 : 4;[rowt,cct] = find(squeeze(coher{r}(Flimit(allband(c),1):Flimit(allband(c),2),:,p)) ~= 0); cc = [cc; cct];end
            %                 cu = unique(cc);
            %                 multipt = [];for allcu = 1 : length(cu); if length(find(cc == cu(allcu))) > 2; multipt = [multipt cu(allcu)]; end; end; % find at least 2 pts above surrogate
            %                 selpairs{c} = Index{Args.rule(c)}(multipt,:);
            %                 if c >1; tspairs{r} = intersect(spairs,selpairs{c},'rows'); else tspairs{r} = selpairs{c}; end
            %             end
            %         else
            cmp = allcmp(c);
            
            if eval(sprintf('~isempty(%s{r,cmp})',Args.comparison{c}))
                eval(sprintf('[row,cc] = find(%s{r,cmp}(Flimit(allband(c),1):Flimit(allband(c),2),:) == 1);',Args.comparison{c}));
                cu = unique(cc);
                multipt = [];for allcu = 1 : length(cu); if length(find(cc == cu(allcu))) >= Args.nptAbove; multipt = [multipt cu(allcu)]; end; end; % find at least 2 pts above surrogate
                selpairs{c} = Index{Args.rule(c)}(multipt,:);
                if c >1; tspairs{r} = intersect(spairs,selpairs{c},'rows'); else tspairs{r} = selpairs{c}; end
                
            else
                selpairs{c} = [];
                tspairs{r} = [];
            end
        end
        %         end
    end
end
if length(Args.rule) > 1
    spairs = eval(sprintf('%s(tspairs{1},tspairs{2},''rows'');',Args.ruleComb));
else
    spairs = tspairs{Args.rule};
end

for d = 1 : size(spairs,1)
    index.days{d} = sprintf('%s/0%d%04.0f',dir,Args.year,spairs(d,1));
    index.npair(d) = spairs(d,4);
    index.ch(d,:) = spairs(d,2:3);
end
index.number = d;
if isempty(d);
    index.days = [];
    index.npair = [];
    index.ch = [];
end
varargout{1} = index;

if Args.gui; figGUI(index,Args.type); end


