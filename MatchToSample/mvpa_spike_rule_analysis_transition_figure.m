function mvpa_spike_rule_analysis_transition_figure


cd('/Volumes/raid/data/monkey/betty')
load switchdays;
mdir=pwd;




for ep = 1 : 6
    pfcloccounter = 0;
    ppcloccounter = 0;
    pfcidecounter = 0;
    ppcidecounter = 0;
    behperf_loc_pfc = [];
    behlocpfc = [];
    behperf_loc_ppc = [];
    behlocppc = [];
    behperf_ide_pfc = [];
    behidepfc = [];
    behperf_ide_ppc = [];
    behideppc = [];
    for x = 1:24
        
        
        cd([mdir filesep days{x} filesep 'session01']);
        
        
        load mvpa_perf_transition
        
        
        if ~isempty(strmatch(firstrule,'location'))
            
            if mean(perf_pfc(ep,1)) > 60
                pfcloccounter = pfcloccounter + 1;
                behperf_loc_pfc(pfcloccounter,:) = perf_pfc(ep,:);
                behlocpfc(pfcloccounter,:) = behperf;
            end
            
            if mean(perf_ppc(ep,1)) > 60
                ppcloccounter = ppcloccounter + 1;
                behperf_loc_ppc(ppcloccounter,:) = perf_ppc(ep,:);
                behlocppc(ppcloccounter,:) = behperf;
            end
            
            
            
        else %ide to location
            
            
            if mean(perf_pfc(ep,1)) > 60 
                pfcidecounter = pfcidecounter + 1;
                behperf_ide_pfc(pfcidecounter,:) = perf_pfc(ep,:);
                behidepfc(pfcidecounter,:) = behperf;
            end
            
            if mean(perf_ppc(ep,1)) > 60
                ppcidecounter = ppcidecounter + 1;
                behperf_ide_ppc(ppcidecounter,:) = perf_ppc(ep,:);
                behideppc(ppcidecounter,:) = behperf;
            end
            
            
            
        end
    end
    
   smb = 10;
    subplot(2,6,ep)
    plot(smooth(median(behperf_ide_pfc),smb),'k')
    hold on
    plot(smooth(median(behidepfc),smb),'k--')
    hold on
    plot(smooth(median(behperf_ide_ppc),smb),'r')
    hold on
    plot(smooth(median(behideppc),smb),'r--')
    
    subplot(2,6,ep+6)
    plot(smooth(median(behperf_loc_pfc),smb),'k')
    hold on
    plot(smooth(median(behlocpfc),smb),'k--')
    hold on
    plot(smooth(median(behperf_loc_ppc),smb),'r')
    hold on
    plot(smooth(median(behlocppc),smb),'r--')

    
end
