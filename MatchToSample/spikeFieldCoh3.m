function [C,phi,f,varargout] = spikeFieldCoh3(varargin)
% To be run in the session directory
%
% cautious modvarargin will pass into cohsurrogate only in local machines
% not in wintermute
% C{p}(:,s,ch)

Args = struct('save',0,'redo',0,'downSR',200,'surrogate',0,'trialJitter',20,'types',1,'gram',0,'mwind',[0.2 0.05],'iterations',1000,'selectedCells',[],'minSpikeTrials',2,'minTrials',10,'rule',1,'onlyDelay',0);
Args.flags = {'save','redo','surrogate','gram','onlyDelay'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'type','plotting','surrogate','NocheckForStability','STA'});

params = struct('tapers',[5 9],'Fs',Args.downSR,'fpass',[0 50],'trialave',1);
mtst = mtstrial('auto','redosetNames');


NeuroInfo = NeuronalChAssign;
ch = [];
for c = 1 : length(NeuroInfo.groups)
    tgroup = sprintf('group%04.f',NeuroInfo.groups(c));
    if ~isempty(nptDir(tgroup))
        
        ch = [ch c];
        
    end
end
group = nptDir('group*');
for g = 1 : length(group); groups{g} = sprintf('%s/%s',pwd,group(g).name); end
ncues = [1 3 3];
arg = {'all' 'iCueLoc' 'iCueObj'};

if Args.gram; iniName = 'SFCgram';plength = 1800;elseif Args.onlyDelay iniName = 'SFConlyDelay';plength = 800;  else iniName = 'SFC';plength = 400; end
for type = Args.types
    if ~isempty(ch)
        for r = 1 : Args.rule
            
            for cues = 1 : ncues(type)
                
                if type > 1
                    Tr = mtsgetTrials(mtst,arg{type},cues,'rule',r,'stable','BehResp',1,modvarargin{:});
                else
                    Tr = mtsgetTrials(mtst,'rule',r,'stable','BehResp',1,modvarargin{:});
                end
                if ~isempty(Tr)
                    if Args.gram || Args.onlyDelay
                        cd lfp
                        files = nptDir('*_lfp.*');
                        count = 1;
                        clear lfp
                        for t = vecr(Tr);
                            [tlfp,~,~,~,~] = nptReadStreamerFile(files(t).name);
                            for thech = 1 : length(NeuroInfo.groups)
                                if Args.gram
                                    addtime = 500;
                                else
                                    addtime = -500;
                                end
                                [lfp(:,count,thech),~] = preprocessinglfp(tlfp(thech,mtst.data.CueOnset(t) - addtime : mtst.data.CueOnset(t) - addtime + plength),modvarargin{:},'detrend');
                            end
                            count = count +1;
                        end
                        cd ..
                    else
                        [lfp,~] = lfpPcut(Tr,[1:length(NeuroInfo.groups)],'fromFiles',modvarargin{:});
                    end
                    for g = 1:length(ch)
                        
                        [modtrials,cluster] = checkForStability(groups{g});
                        unitgr = str2num(group(g).name(7:end));
                        cd(groups{g})
                        for cl = 1 : size(cluster,1)
                            
                            cd(cluster(cl).name)
                            if isempty(Args.selectedCells) || (~isempty(Args.selectedCells) && ~isempty(nptDir(Args.selectedCells)))
                                stableTrials = intersect(modtrials{cl},Tr);
                                
                                if ~isempty(stableTrials)  && length(find(ismember(stableTrials,Tr) == 1)) >= (length(Tr)-Args.trialJitter)
                                    
                                    
                                    
                                    for thech = 1 : length(ch)
                                        if Args.surrogate
                                            filename = sprintf('%s%s%dg%04.fRule%dSur.mat',iniName,arg{type},cues,NeuroInfo.groups(ch(thech)),r);
                                        else
                                            filename = sprintf('%s%s%dg%04.fRule%d.mat',iniName,arg{type},cues,NeuroInfo.groups(ch(thech)),r);
                                        end
                                        if Args.redo || isempty(nptDir(filename))
                                            clear period
                                            load ispikes.mat
                                            tcount = 1;
                                            removetrials = [];
                                            for t = vecr(Tr);
                                                if Args.gram
                                                    periods(1) = round((mtst.data.CueOnset(t) - 500));
                                                    
                                                elseif Args.onlyDelay
                                                    periods(1) = round((mtst.data.CueOffset(t)));
                                                else
                                                    periods(1) = round((mtst.data.CueOnset(t) - plength));
                                                    periods(2) = round(mtst.data.CueOnset(t));
                                                    periods(3) = round(mtst.data.CueOffset(t));
                                                    periods(4) = round((mtst.data.MatchOnset(t) - plength));
                                                end
                                                for p = 1 : length(periods)
                                                    d = vecc(sp.data.trial(t).cluster.spikes);
                                                    ind = (d>=periods(p) & d<=periods(p)+plength);
                                                    if length(ind) <= Args.minSpikeTrials
                                                        spiketimes = [];
                                                        removetrials = [removetrials tcount];
                                                    else
                                                        spiketimes = d(ind)-periods(p);
                                                    end
                                                    period(p).trial(tcount).times = spiketimes/1000;
                                                    
                                                end
                                                tcount = tcount + 1;
                                            end
                                            if Args.onlyDelay
                                                C = zeros(65,4);
                                                phi = zeros(65,4);
                                                Prob = nan(65,6,4);
                                            else
                                                C = zeros(33,4);
                                                phi = zeros(33,4);
                                                Prob = nan(33,6,4);
                                            end
                                            
                                            removetrials = unique(removetrials);
                                            keeptrials = setdiff([1:length(Tr)],removetrials);
                                            if length(keeptrials) >= Args.minTrials
                                                for p = 1 : length(periods); period(p).trial = period(p).trial(keeptrials);  mlfp{p} = lfp{p}(:,keeptrials,:);end
                                               
                                                
                                                
                                                for p =1 : length(periods)
                                                    if Args.surrogate
                                                        if Args.gram
                                                            newSeq = randperm(size(mlfp{p},2));
                                                            [Probt,~,~,~,~,~,~,zerospt] = cohgramcpt(squeeze(mlfp{p}(:,newSeq,ch(thech))),period(p).trial,Args.mwind,params);
                                                            Prob = zeros(size(Probt,1),size(Probt,2),Args.iterations);
                                                            zerosp = zeros(size(zerospt,1),size(zerospt,2),Args.iterations);
                                                            for ii = 1 : Args.iterations
                                                                newSeq = randperm(size(mlfp{p},2));
                                                                
                                                                [Prob(:,:,ii),~,~,~,~,time,f,zerosp(:,:,t)] = cohgramcpt(squeeze(mlfp{p}(:,newSeq,ch(thech))),period(p).trial,Args.mwind,params);
                                                                
                                                            end
                                                            Prob = single(Prob);
                                                            zerosp = single(zerosp);
                                                        elseif Args.onlyDelay
                                                            if (NeuroInfo.groups(ch(thech)) > 32 && unitgr <= 32) || (NeuroInfo.groups(ch(thech)) <= 32 && unitgr > 32)
                                                            [~,~,~,~,~,Prob,f] = cohsurrogate(squeeze(mlfp{p}(:,:,ch(thech))),period(p).trial,params,'SFC','saveName',sprintf('%srogates.mat',filename(1:end-4)));
                                                            else
                                                                clear f
                                                            end
                                                        else
                                                            try
                                                                [~,~,~,~,~,Prob(:,:,p),f] = cohsurrogate(squeeze(mlfp{p}(:,:,ch(thech))),period(p).trial,params,'SFC','saveName',sprintf('%srogates.mat',filename(1:end-4)),modvarargin{:});
                                                                
                                                            end
                                                        end
                                                        
                                                    else
                                                        if Args.gram
                                                            %[C,phi,S12,S1,S2,t,f,zerosp,confC,phistd,Cerr]=cohgramcpt(data1,data2,movingwin,params,fscorr)
                                                            [C,phi,~,~,~,time,f,zerosp] = cohgramcpt(squeeze(mlfp{p}(:,:,ch(thech))),period(p).trial,Args.mwind,params);
                                                        elseif Args.onlyDelay
                                                            [C,phi,~,~,~,f] = coherencycpt(squeeze(mlfp{p}(:,:,ch(thech))),period(p).trial,params);
%                                                             subplot(3,1,1)
%                                                             plot(f,C); hold on
%                                                             sptimesb = zeros(161,size(mlfp,2));
%                                                             for t = 1: size(mlfp,2)
%                                                                 sptimesb(:,t) = hist(period(p).trial(t).times,[0:.005:0.8]);
%                                                             end
%                                                             [C,phi,~,~,~,f] = coherencycpb(squeeze(mlfp(:,:,ch(thech))),sptimesb,params);
%                                                             plot(f,C,'r'); xlim([0 50])
%                                                             subplot(3,1,2)
%                                                              [C,phi,~,~,~,time,f,zerosp] = cohgramcpt(squeeze(mlfp(:,:,ch(thech))),period(p).trial,Args.mwind,params);
%                                                              imagesc(time,f,C')
%                                                              subplot(3,1,3)
%                                                              [C,phi,~,~,~,time,f,zerosp] = cohgramcpb(squeeze(mlfp(:,:,ch(thech))),sptimesb,Args.mwind,params);
%                                                              imagesc(time,f,C')
%                                                              
%                                                             pause
%                                                             clf
                                                        else
                                                            [C(:,p),phi(:,p),~,~,~,f] = coherencycpt(squeeze(mlfp{p}(:,:,ch(thech))),period(p).trial,params);
                                                        end
                                                    end
                                                end
                                            else
                                                Prob =[];
                                                f=[];
                                                stableTrials=[];
                                                time=[];
                                                zerosp=[];
                                                C=[];
                                                phi=[];
                                            end
                                            if Args.save
                                                if exist('f')
                                                    if Args.surrogate
                                                        if Args.gram
                                                            save(filename,'Prob','f','stableTrials','Args','modvarargin','time','zerosp');
                                                        else
                                                            save(filename,'Prob','f','stableTrials','Args','modvarargin');
                                                        end
                                                    elseif Args.gram
                                                        save(filename,'C','phi','f','stableTrials','Args','modvarargin','time','zerosp');
                                                    else
                                                        save(filename,'C','phi','f','stableTrials','Args','modvarargin');
                                                    end
                                                    fprintf('saving %s \n',filename)
                                                end
                                            end
                                            
                                        end
                                        
                                    end
                                end
                            end
                            cd ..
                        end
                        cd ..
                    end
                end
            end
        end
        
        
    else
        fprintf('!!!!!!Problem: No trials or no channel selected ');
        C = [];
        phi = [];
        f = [];
        unit = [];
        varargout{1} = [];
    end
end

if ~exist('C')
    C = [];
    phi = [];
    f = [];
    unit = [];
    varargout{1} = [];
end
