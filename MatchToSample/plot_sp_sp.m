function plot_sp_sp

cd /media/raid/data/monkey/betty/
mdir = pwd;
load alldays
figure
for d = 26%1 : 46
    cd([mdir filesep alldays{d} filesep 'session01'])
    N = NeuronalHist('ml');
    
    cd([mdir filesep alldays{d} filesep 'session01' filesep 'lfp' filesep 'lfp2' filesep 'identity'])
    
    
    sp = nptDir('spikexcorr*');
    nsp = size(sp,1);
    
    
    counter = 0;
    for n = 1 : nsp
        counter = counter + 1;
        load(sp(n).name)
        alldays{d}
        
        a1 = N.level1(find(N.gridPos == spikexcorrs.groups(1,1)));
        a2 = N.level1(find(N.gridPos == spikexcorrs.groups(1,2)));
        
        sp_sp = spikexcorrs.delay(1,:);
        if counter > 20
            figure
            counter = 1;
        end
        
        subplot(4,5,counter)
        plot(smooth(sp_sp,3))
        title([a1{:} ' ' num2str(spikexcorrs.groups(1,1)) '   ' a2{:} ' ' num2str(spikexcorrs.groups(1,2))])
    end
    
%     pause
%     close all
    
end