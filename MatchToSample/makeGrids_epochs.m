function makeGrids_epochs(varargin)


%run at day level
%
%Args
%epoch: fix(1),sample(2),delay1(3),delay2(4)
%rule: indicates rule

Args = struct('parietal',0,'frontal',0,'inter',0,'rule','identity','epoch',4,'phase',0,'depths',0,'snr',1.8);
Args.flags = {'parietal','frontal','inter','phase','depths'};
Args = getOptArgs(varargin,Args);

daydir = pwd;
goodgroups = good_groups;
cd('session01')
sesdir = pwd;
cd('highpass')
load SNR_channels
cd(daydir)
cd ..
monkeydir = pwd;
load channel_offsets
cd(sesdir)

%get epochs xcorrs
cd(['lfp' filesep 'lfp2' filesep Args.rule]);
load avg_correlograms_epochs 
load all_channels
load all_pairs %these are used to index the xcorr information

numb_pairs = size(all_pairs,1);

%classify pairs as p,f,i
if Args.frontal || Args.parietal || Args.inter
    type = zeros(1,numb_pairs);
    for x = 1:numb_pairs
        if ((all_pairs(x,1) <= 32) && (all_pairs(x,2) <= 32))
            if Args.frontal
                type(x) = 1;
            end
        elseif ((all_pairs(x,1) > 32) && (all_pairs(x,2) > 32))
            if Args.parietal
                type(x) = 1;
            end
        else
            if Args.inter
                type(x) = 1;
            end
            
        end
    end
else
    type = ones(1,numb_pairs);
end

cd(sesdir)

descriptor = nptdir('*descriptor.txt');
descriptor_info = ReadDescriptor(descriptor(1).name);

channels = descriptor_info.channel;
channel_depths = descriptor_info.recdepth ./ 1000;
zero_channels = find(channel_depths == 0);
c_o = channel_offsets(channels);
c_o(zero_channels) = 0;
channel_depths = channel_depths - c_o; % subtract offsets
num_channels = size(channels,2);

%get histology information
N = NeuronalHist('ml');
locations = N.locs;
for cc = 1:num_channels
    day_depths(channels(cc)) = channel_depths(cc);
    
    [i ii iii]= intersect(channels(cc),N.gridPos);
    if ii
        loc_depths{channels(cc)} = N.locs{iii};
        location_depths{channels(cc)} = N.location{iii};
    end
end


%get rid of flat and noisy channel information
    for all_n = 1:64
        if isempty(intersect(all_n,goodgroups))
            day_depths(all_n) = 0;
            loc_depths{all_n} = {};
            location_depths{all_n} = {};
        end
    end

%PF grid
pf_order = [ ...
    
0; 1; 2; 3; 4; 5;
0; 6; 7; 8; 9;10;
11;12;13;14;15;16;
17;18;19;20;21;22;
0;23;24;25;26;27;
0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    
0;33;34;35;36;37;
0;38;39;40;41;42;
43;44;45;46;47;48;
49;50;51;52;53;54;
0;55;56;57;58;59;
0;60;61;62;63;64];



%get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
        end
    end
end

%plot groups with colors indicating location
for n = 1 : 64
    g = xy(n,:);
    if day_depths(n) ~= 0
        if Args.depths
            dd =[(day_depths(1:32)/max(day_depths(1:32))) (day_depths(33:64)/max(day_depths(33:64)))];
            
            %this is the xy coordinates for the channel on the grid
            scatter3(g(1),g(2),(day_depths(n)*(-1)),(500-(dd(n)*400)),[0 0 0],'filled')
            hold on
        else
            %this is the xy coordinates for the channel on the grid
            scatter3(g(1),g(2),(day_depths(n)*(-1)),(500),[0 0 0],'filled')
            hold on
        end
    end
    scatter(g(1),g(2),(500),[0 0 0])
    hold on
end


%plot all corrcoefs and phases
for n = 1 : numb_pairs
    if type(n)
        
        if (~isempty(intersect(all_pairs(n,1),goodgroups)) && ~isempty(intersect(all_pairs(n,2),goodgroups)))
            %all_ccg = avg_correlograms_epochs{1,n};
            %ccg = all_ccg(Args.epoch,:);
            %lags = floor(size(ccg,2)/2);
            %[c p] = find_correlogram_peak('correlogram',ccg);
            
            
            c = avg_corrcoef_epochs{4,n}; %EPOCH 4 (delay 2)
            p = avg_phase_epochs{4,n};
            
            c1 = all_pairs(n,1);
            g1 = xy(c1,:);
            
            c2 = all_pairs(n,2);
            g2 = xy(c2,:);
            
            z1 = day_depths(c1) * (-1);
            z2 = day_depths(c2) * (-1);
            
            if c >= 0 %in phase RED<-PURPLE->BLUE  ([1 0 0]<-[.5 0 .5]->[0 0 1])
                if Args.phase
                    if p > 0
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 0 0],'LineWidth',c*10)
                        hold on
                    elseif p < 0
                        p = abs(p) / 5;
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 0 1],'LineWidth',c*10)
                        hold on
                    else
                        c = abs(c);
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[.5 0 .5],'LineWidth',c*10)
                        hold on
                    end
                else
                    plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 0 0],'LineWidth',c*10)
                    hold on
                end
            else %out of phase GREEN<-ORANGE->YELLOW
                if Args.phase
                    if p > 0
                        c = abs(c);
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 1 0],'LineWidth',c*10)
                        hold on
                    elseif p < 0
                        c = abs(c);
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 1 0],'LineWidth',c*10)
                        hold on
                    else
                        c = abs(c);
                        plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 .5 0],'LineWidth',c*10)
                        hold on
                    end
                else
                    c = abs(c);
                    plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 1 0],'LineWidth',c*10)
                    hold on
                end
            end
            l1 = location_depths{c1};
            l2 = location_depths{c2};
            text([g1(1)+.3],[g1(2)],[z1],l1(1:(end-3)),'FontSize',10,'FontWeight','bold')
            text([g2(1)+.3],[g2(2)],[z2],l2(1:(end-3)),'FontSize',10,'FontWeight','bold')
        end
    end
end

axis([-.5 14.5 0 7.5])
cd(daydir)