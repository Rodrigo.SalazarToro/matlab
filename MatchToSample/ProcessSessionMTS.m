function [varargout] = ProcessSessionMTS(varargin)
% Function that combines differents files to produce a matrix that contains
% all the necessary information for each trial of the matching-to-sample
% task. By default this function should be run in the session folder
% One of the output is a flat struture with 13 fileds:
%
% Session.CueObj - cue object
% Session.CueLoc - cue location
% Session.MatchObj1 - matched object1
% Session.MatchLoc1 - matched position1
% Session.MatchObj2 - matched object2
% Session.MatchLoc2 - matched position 2
% Session.CueOnset - Cue onset
% Session.CueOffset - Cue offset
% Session.MatchOnset - Match onset
% Session.BehResp - Behavioral response (2 timed out 1 for correct and 0 for incorrect)
% Session.FirstSac - First saccadic response onset
% Session.LastSac - last saccadic response offset
% Session.NumSac - Number of saccadic responses
% Session.Index - has all the information about each trial; column wise:
%      1. Rule (identity = 1; location = 2)
%      2. Cummulative session
%      3. Cummulative trial
%      4. Real trial number according to the recordings
%      5. Trial type (Go = 1; NoGo = 0)
%
% The followings arguments can be used:
%
%   - NumOfSac: Creates the output with the specified number of saccades.
%               Dep. ProcessSessionMTS.
%
%   - RTLimit: Creates the output with reaction times above or equal to the specified threshold.
%              Dep. ProcessSessionMTS.
%   - ObjectLibrary : gives the code used for tracking the objects.
%
%
% Dependencies: nptDir, ReadSequenceFile, ReadIniCognitive, ReadMarkerFile, getDataDirs, @eyemvt.



Args = struct('ObjectLibrary',0,'NumOfSac',[],'RTLimit',[],'ML',0,'RTfromML',0,'Nlynx',0);
Args.flags = {'ObjectLibrary','ML','RTfromML','Nlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

%%%%%%%%%Library of objects which can be modified
if Args.ML
    
    if Args.Nlynx
        ObjIdentity = {'';'';'';'';'';'';'';'balloons';'bananas';'bear';'berries';'bluefish';'bowlingpins';'broccoli';'canteloupe';'cartoon';'cat';'chicken';'clown';'diamond';'dinosaur';'earth';'grapefruit';'hat';'headphones';'honey';'jellybeans';'leaves';'lion';'lobster';'metalnut';'nuts';'orangejuice';'pears';'pinkflower';'popcorn';'pumpkin';'rooster';'rubixcube';'sandwich';'shoe';'tent';'tiger';'tree';'vulture';'watermelon';'yellowflower';'lum0';};
        ObjIdentity{55} = 'blank'; % to match old ML files and to recognize teh fixation trials
        ObjIdentity{56} = 'fix'; %this is used for the interleaved fixation trials
        ObjIdentity{57} = 'fixblock'; %this is for the fixation blocks
    else
        ObjIdentity = {'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'A';'B';'C';'D';'E';'F';'G';'H';'I';'J';'K';'L';'M';'N';'O';'A75';'B75';'C75';'D75';'E75';'F75';'G75';'H75';'I75';'J75';'K75';'L75';'M75';'N75';'O75';'Black'}; % the first three are skipped in order to avoid problem with the original coding 0 to 2
        
    end
else
    ObjIdentity = {'';'';'';'A-Object';'B-Object';'banana';'C-Object';'CircleWtOnBlck';'club';'diamond';'donkey';'elephant';'filix';...
        'goat';'heart';'icecreamcone';'lobster';'monkey';'skiing';'smblckfilix';'spade';'SquareWtOnBlck';'TriangleWtOnBlck';'turtle';'CrossWhiteonBlack';'flower';'gear';'HorizontalStripes';'NewCheck';'rectangle';'starfish';'sun';'triangle';'VerticleStripes';'Wheel';'SquareTarget';'Target';'MonkeyFace';'spaceship'}; % the first three are skipped in order to avoid problem with the original coding 0 to 2
end

for i = 4 : size(ObjIdentity,1)
    ObjLibrary{i-3} = ObjIdentity{i};
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~Args.ObjectLibrary
    
    if Args.ML
        basedir = pwd;
        datafile = nptDir('*.bhv');
        BHV = bhv_read(sprintf('%s%s%s',basedir,filesep,datafile.name));
        if Args.Nlynx
            trials = find(BHV.TrialError == 0 | BHV.TrialError == 6);
            
            fixT = find(BHV.ConditionNumber == 47 & BHV.TrialError == 0); % temporary solution
            fixBlock = find(BHV.ConditionNumber == 6 & BHV.TrialError == 0);
            blankT = find(BHV.ConditionNumber == 48); % temporary solution
            mtsT = setdiff(trials,[fixT;blankT;fixBlock]);
            sampleOn = unique(cell2mat(cellfun(@(x) find(x == 25),BHV.CodeNumbers(mtsT),'UniformOutput', false)));
            sampleOff = unique(cell2mat(cellfun(@(x) find(x == 26),BHV.CodeNumbers(mtsT),'UniformOutput', false)));
            matchOn = unique(cell2mat(cellfun(@(x) find(x == 27),BHV.CodeNumbers(mtsT),'UniformOutput', false)));
        else
            load(sprintf('.%sdelFiles%sdeletedfiles.mat',filesep,filesep));
            sampleOn = unique(cell2mat(cellfun(@(x) find(x == 25),BHV.CodeNumbers(trials),'UniformOutput', false)));
            sampleOff = unique(cell2mat(cellfun(@(x) find(x == 26),BHV.CodeNumbers(trials),'UniformOutput', false)));
            matchOn = unique(cell2mat(cellfun(@(x) find(x == 27),BHV.CodeNumbers(trials),'UniformOutput', false)));
        end
        
        
        presTrigOnsetsMS(:,1) = cell2mat(cellfun(@(x) (x(sampleOn) - x(1)),BHV.CodeTimes(trials),'UniformOutput', false))';
        presTrigOnsetsMS(:,2) = cell2mat(cellfun(@(x) (x(sampleOff) - x(1)),BHV.CodeTimes(trials),'UniformOutput', false))';
        presTrigOnsetsMS(:,3) = cell2mat(cellfun(@(x) (x(matchOn) - x(1)),BHV.CodeTimes(trials),'UniformOutput', false))';
        
        [tseq,INI] = SeqFromML(BHV,modvarargin{:}); %CO CP MO1 MP1 MO2 MP2 between 0 and 2
        if Args.Nlynx
            %associate each trial type to an image
            BlankN = [];
            if ~isempty(blankT)
                nfield = length(fieldnames(INI));
                INI = setfield(INI,sprintf('Object%dFileName',nfield),'blank');
                BlankN = find(strcmp(struct2cell(INI),'blank') ==1) -1; %This got changed because the other line didn't make sense. -Nick
                tseq(blankT,[1 3 5]) = BlankN;
                tseq(blankT,[2 4 6]) = 0;
            end
            
            fixN = [];
            if ~isempty(fixT)
                nfield = length(fieldnames(INI));
                INI = setfield(INI,sprintf('Object%dFileName',nfield),'fix');
                fixN = find(strcmp(struct2cell(INI),'fix') ==1) -1; %This got changed because the other line didn't make sense. -Nick
                tseq(fixT,[1 3 5]) = fixN;
                tseq(fixT,[2 4 6]) = 0;
            end
            
            fixblockN = [];
            if ~isempty(fixBlock)
                nfield = length(fieldnames(INI));
                INI = setfield(INI,sprintf('Object%dFileName',nfield),'fixblock');
                fixblockN = find(strcmp(struct2cell(INI),'fixblock') ==1) -1; %This got changed because the other line didn't make sense. -Nick
                tseq(fixBlock,[1 3 5]) = fixblockN;
                tseq(fixBlock,[2 4 6]) = 0;
            end
        end
        
        SEQUENCE = tseq(trials,:);
        if Args.Nlynx
            %blank trials are simply three second periods, there is no
            %fixation or task so they are always correct.
            % nonmts = find(SEQUENCE(:,1) == BlankN | SEQUENCE(:,1) == fixcodeN);
            if ~isempty(BlankN)
                nonmts = find(SEQUENCE(:,1) == BlankN);
                presTrigOnsetsMS(nonmts,1) = 550; %setting their "sample on" to 550, this can be changed
            end
            
            %fix trials: there was no "sample on" trigger. Subsequently the
            %start of fixation had to be esitmated. This is done by finding
            %when the eye traces are both in the fixation window (radis = 3
            %deg). write_fixation_trial_times.m does this.
%             if exist('fixation_trial_times.mat')
%                 load fixation_trial_times fixtimes
%             else
                write_fixation_trial_times
                load fixation_trial_times fixtimes
%             end
            nonmtsfix = [];
            if ~isempty(fixN)
                nonmtsfix = find(SEQUENCE(:,1) == fixN);
            end
            
            nonmtsfixblock = [];
            if ~isempty(fixblockN)
                nonmtsfixblock = find(SEQUENCE(:,1) == fixblockN);
            end
            
            allfixtrials = sort([nonmtsfix' nonmtsfixblock']);
            presTrigOnsetsMS(allfixtrials,1) = fixtimes + 500; %add 500 to get the estimated "sample on" time
            presTrigOnsetsMS(allfixtrials,2) = fixtimes + 500 + 500; %add another 500 to get the estimated "sample off" time
        end
        INI.MatchTimeLimit = 850;
        responses = BHV.TrialError(trials) == 0;
        
    else
        timingfile = nptDir('*timing.mat','CaseInsensitive');
        load(timingfile.name,'presTrigOnsets','presTrigOnsetsMS');
        
        seqfile = nptDir('*.seq','CaseInsensitive');
        [SEQUENCE,SessionType] = ReadSequenceFile(seqfile.name); % 6 first columns
        
        inifile = nptDir('*.ini','CaseInsensitive');
        [INI,status] = ReadIniCognitive(inifile.name);
        
        markerfile = nptDir('*.mrk','CaseInsensitive');
        if isempty(markerfile)
            markerfile = nptDir('*_MRK.mat','CaseInsensitive');
            load(markerfile.name)
        else
            [MARKERS,records] = ReadMarkerFile(markerfile.name);
            
            tmarkers = MarkersToTrials(MARKERS);
        end
        for i = 1 : size(tmarkers,2)
            responses(i) = tmarkers(i).response;
        end
        responses = responses';
    end
    
    if ~Args.RTfromML
        
        [pdir,cdir] = getDataDirs('eye','relative','CDNow');
        eyemvtobj = eyemvt('auto','save',varargin{:},'SacThresh',60);
        
        if isempty(eyemvtobj)
            fprintf('Empty eyemvt object!!!!\n')
            
        end
        if(~isempty(cdir))
            cd(cdir)
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%% library of objects
    %%%%%%%%%%%%%%%%%%%%%% identity with each corresponding to a number
    if Args.Nlynx
        nobj = size(strmatch('Object',fieldnames(INI)),1);
        for allobj = 1 : nobj
            realname = eval(['INI.Object' num2str(allobj) 'FileName']);
            objnum(allobj) = find(strcmp(ObjIdentity,realname)); %get the object number
            
            r{allobj} = find(SEQUENCE(:,1) == allobj); %determine what number to give each object
        end
        for allobj = 1 : nobj
            SEQUENCE(r{allobj},[1 3 5]) = objnum(allobj); %replace current number with the one specified above
        end
    else
        if Args.ML
            nobj = length(strmatch('Object',fieldnames(INI)));
        else
            nobj = 3;
        end
        for Obj = 1 : nobj
            ObjName = sprintf('INI.Object%dFileName',Obj);
            FullName = eval(ObjName);
            if Args.ML
                eval(sprintf('realName{Obj} = INI.Object%dFileName;',Obj));
            else
                k1 = strfind(FullName,'\');
                k2 = strfind(FullName,'.bmp');
                realName{Obj} = FullName(k1(end)+1:k2(end)-1);
            end
            CorrectObj = strcmp(ObjIdentity,realName{Obj});
            code(Obj) = find(CorrectObj == 1);
            
            
            for Col = [1 3 5]
                clear r
                r = find(SEQUENCE(:,Col) == Obj-1);
                SEQUENCE(r,Col) = code(Obj);
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%Correction for the posisions being 90 or 270 degrees
    
    if INI.Position == 270
        for pos = 0 : 2
            for Col = [2 4 6]
                clear r
                r = find(SEQUENCE(:,Col) == pos);
                SEQUENCE(r,Col) = pos+3;
            end
        end
        
    end
    %%%
    sizes = [length(presTrigOnsetsMS),length(SEQUENCE),length(responses)];
    shortest = min(sizes); % solution to handle cases where SEQUENCE or presTrigOnsetsMS or responses have different number of trials
    TrialsInfo = [SEQUENCE(1:shortest,:) presTrigOnsetsMS(1:shortest,:) responses(1:shortest,:)];
    
    minRange = presTrigOnsetsMS(:,3);
    maxRange = minRange+INI.MatchTimeLimit;
    
    if Args.RTfromML
        TrialsInfo(:,11) =  BHV.ReactionTime(trials);% tming leaving the fixation window
        TrialsInfo(:,12) = nan(length(responses),1); % no last saccade from ML
        TrialsInfo(:,13) = ones(length(responses),1); % always one saccade
    else
        
        sacResp = [];
        
        for sac = 1: length(eyemvtobj.data.sacStart)
            
            timing1 = eyemvtobj.data.sacStart(sac) - minRange(eyemvtobj.data.sacSetIndex(sac,3));
            timing2 = eyemvtobj.data.sacEnd(sac) - maxRange(eyemvtobj.data.sacSetIndex(sac,3));
            if timing1 > 0  && timing2 < 0
                sacResp = [sacResp sac];
            end
        end
        
        for sac = 1 : length(sacResp)
            switch sac
                case 1
                    firstsac = 1;
                    lastsac = 0;
                case length(sacResp)
                    firstsac = 0;
                    lastsac = 1;
                otherwise
                    firstsac = 0;
                    lastsac = 0;
            end
            
            if firstsac || eyemvtobj.data.sacSetIndex(sacResp(sac-1),3) ~= eyemvtobj.data.sacSetIndex(sacResp(sac),3)
                TrialsInfo(eyemvtobj.data.sacSetIndex(sacResp(sac),3),11) = (eyemvtobj.data.sacStart(sacResp(sac)) - minRange(eyemvtobj.data.sacSetIndex(sacResp(sac),3))); % start of the first saccadic response
                n = 1;
            end
            if lastsac || eyemvtobj.data.sacSetIndex(sacResp(sac+1),3) ~= eyemvtobj.data.sacSetIndex(sacResp(sac),3)
                TrialsInfo(eyemvtobj.data.sacSetIndex(sacResp(sac),3),12) = (eyemvtobj.data.sacEnd(sacResp(sac)) - minRange(eyemvtobj.data.sacSetIndex(sacResp(sac),3))); % end of the last saccadic response
                TrialsInfo(eyemvtobj.data.sacSetIndex(sacResp(sac),3),13) = n; % number of saccadic responses
                
            else
                n = n + 1;
            end
            
        end
    end
    [r,c] = find(TrialsInfo(:,11:12) == 0);
    TrialsInfo(r,10+c) = NaN;
    
    if Args.ML
        Index = [BHV.BlockNumber(trials) ones(size(TrialsInfo,1),1) (1:size(TrialsInfo,1))' (1:size(TrialsInfo,1))' ones(size(TrialsInfo,1),1)];
    else
        
        if ~Args.ML && strcmp(SessionType,'IDENTITY')
            
            ruleI = 1;
            
        elseif ~Args.ML && strcmp(SessionType,'LOCATION')
            
            ruleI = 2;
        end
        if ruleI == 1
            for tr = 1 : size(TrialsInfo,1)
                if TrialsInfo(tr,1) == TrialsInfo(tr,3) || TrialsInfo(tr,1) == TrialsInfo(tr,5)
                    goNogo(tr) = 1; % go trials
                else
                    goNogo(tr) = 0; % nogo trials
                end
            end
        elseif ruleI == 2
            for tr = 1 : size(TrialsInfo,1)
                if TrialsInfo(tr,2) == TrialsInfo(tr,4) || TrialsInfo(tr,2) == TrialsInfo(tr,6)
                    goNogo(tr) = 1; % go trials
                else
                    goNogo(tr) = 0; % nogo trials
                end
            end
        end
        
        Index = [repmat(ruleI,size(TrialsInfo,1),1) ones(size(TrialsInfo,1),1) (1:size(TrialsInfo,1))' (1:size(TrialsInfo,1))' goNogo'];
    end
    
    if ~isempty(Args.NumOfSac)
        
        I1 = find(TrialsInfo(:,13) == Args.NumOfSac);
    else
        I1 = 1 : size(TrialsInfo,1);
    end
    if ~isempty(Args.RTLimit)
        I2 = find(TrialsInfo(:,11) >= Args.RTLimit);
    else
        I2 = 1 : size(TrialsInfo,1);
        
    end
    
    I1ANDI2 =  intersect(I1,I2);
    
    SelectedTrials = TrialsInfo(I1ANDI2,:);
    SelectedIndex = Index(I1ANDI2,:);
    SelectedIndex(:,3) = (1 : size(SelectedIndex,1))';
    
    Session.CueObj = SelectedTrials(:,1);
    Session.CueLoc = SelectedTrials(:,2);
    Session.MatchObj1 = SelectedTrials(:,3);
    Session.MatchPos1 = SelectedTrials(:,4);
    Session.MatchObj2 = SelectedTrials(:,5);
    Session.MatchPos2 = SelectedTrials(:,6);
    Session.CueOnset = SelectedTrials(:,7);
    Session.CueOffset = SelectedTrials(:,8);
    Session.MatchOnset = SelectedTrials(:,9);
    Session.BehResp = SelectedTrials(:,10);
    Session.FirstSac = SelectedTrials(:,11);
    Session.LastSac = SelectedTrials(:,12);
    Session.NumSac = SelectedTrials(:,13);
    Session.Index = SelectedIndex;
    
    varargout{1} = Session;
    varargout{2} = ObjIdentity;
    
elseif Args.ObjectLibrary
    varargout{1} = ObjIdentity;
end
