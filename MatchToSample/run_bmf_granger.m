function run_bmf_granger(varargin)

Args = struct('days',[],'sessions',[],'granger',0,'redo_granger',0,'match_trials',0,'surrogates',0);
Args.flags = {'granger','redo_granger','match_trials','surrogates'};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;

movingwin = [.2 .05]; %this is in seconds
params.Fs = 200;

num_days = size(Args.days,2);
for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            rejected_trials = get_reject_trials;
            %this gets the groups that pass reject_filter.m
            [ch chpairs gpairs] = bmf_groups;
            nch = size(ch,2);
            
            total_pairs = size(chpairs,1);
            
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
            
            for ides = 1 : 5
                trials{ides} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'iCueObj',ides,'notfromMTStrial','Nlynx');
            end
            trials{6} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
            trials{7} = mtsgetTrials(mt,'BehResp',0,'Nlynx');
            
            %get interleaved fixation trials
            fixtrials = find(mt.data.CueObj == 56)'; %see ProcessSessionMTS
            [~,rej] = intersect(fixtrials,rejected_trials);
            fixtrials(rej) = [];
            trials{8} = fixtrials;
            
            
            sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
            match = floor(mt.data.MatchOnset);    %match
            
            cd([sesdir filesep 'lfp']);
            lfpdir = pwd;
            lfptrials = nptDir('ethyl*');
            
            if Args.granger
                for p = 1 : total_pairs
                    clc
                    total_pairs - p
                    cd(lfpdir)
                    
                    %run for 5 ides + all trials
                    if Args.match_trials
                        ide_run = 1:5;
                    else
                        ide_run = 1:8;
                    end
                    
                    %run on correct stable trials only, until tested
                    for all_ides = 6 %ide_run
                        idetrials = trials{all_ides};
                        ntrials = size(idetrials,2);
                        
                        if Args.match_trials
                            nt = cellfun(@length,trials);
                            min_nt = min(nt(1:5));
                            
                            %match the trial counts to the mininmum number of trials
                            permtrials = idetrials(randperm(ntrials));
                            idetrials = sort(permtrials(1:min_nt));
                            ntrials = min_nt;
                        end
                        
                        p1trials = zeros(ntrials,650); %data is downsampled
                        p2trials = zeros(ntrials,650);
                        counter = 0;
                        for t = idetrials
                            counter = counter + 1;
                            fix = sample_on(t) - 500; %this is approx fixation
                            if all_ides == 8 %fixation
                                sac = match(t);
                            else
                                sac = match(t) + 200; %match plus saccade
                            end
                            
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            p1t = data.rawdata(chpairs(p,1),:);
                            p2t = data.rawdata(chpairs(p,2),:);
                            
                            %cut, get rid of 60! and detrend
                            p1t = preprocessinglfp(p1t(fix:sac),'detrend')';
                            p2t = preprocessinglfp(p2t(fix:sac),'detrend')';
                            
                            p1trials(counter,(1:size(p1t,2))) = p1t;
                            p2trials(counter,(1:size(p2t,2))) = p2t;
                        end
                        data1 = p1trials(:,(1:400))';  %keep only presample to first match + 200, which is 2000 ms or 400 samples
                        data2 = p2trials(:,(1:400))';
                        
                        if all_ides == 6
                            
                            [fx2y,fy2x,fxy,time,freq]=GCgram(data1,data2,params,movingwin,20);
                            
                            grangergram.Fx2y{1} = single(squeeze(fx2y));
                            grangergram.Fy2x{1} = single(squeeze(fy2x));
                            grangergram.Fxy{1} = single(squeeze(fxy));
                            grangergram.t{1} = single(time);
                            grangergram.f{1} = single(freq);
                            grangergram.trials{1} = single(idetrials);
                            grangergram.groups{1} = single(gpairs(p,:));
                        else
                            %                            idegrangergram = []; %nothing to save yet
                        end
                    end
                    if ~exist('grangergrams','dir')
                        mkdir('grangergrams')
                    end
                    cd([lfpdir filesep 'grangergrams']);
                    
                    write_info = writeinfo(dbstack);
                    if ~Args.match_trials
                        granger_pairs = ['grangergram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(granger_pairs,'grangergram','write_info');
                    else
                        
                    end
                end
            end
            
            
            
            
            
            
            
            
            if Args.surrogates
                cd([lfpdir filesep 'grangergrams']);
                
                for p = 1 : total_pairs
                    if ~exist([ 'suridegrangergram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3) '.mat'],'file') || ~exist([ 'surgrangergram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3) '.mat'],'file')
                        cd(lfpdir)
                        
                        idetrials = trials{6};
                        ntrials = size(idetrials,2);
                        
                        p1trials = zeros(ntrials,650); %data is downsampled
                        p2trials = zeros(ntrials,650);
                        counter = 0;
                        for t = idetrials
                            counter = counter + 1;
                            fix = sample_on(t) - 500; %this is approx fixation
                            sac = match(t) + 200; %match plus saccade
                            
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            p1t = data.rawdata(chpairs(p,1),:);
                            p2t = data.rawdata(chpairs(p,2),:);
                            
                            %cut, get rid of 60! and detrend
                            p1t = preprocessinglfp(p1t(fix:sac),'detrend')';
                            p2t = preprocessinglfp(p2t(fix:sac),'detrend')';
                            
                            %get all trials for each channel
                            p1trials(counter,(1:size(p1t,2))) = p1t;
                            p2trials(counter,(1:size(p2t,2))) = p2t;
                        end
                        data1 = p1trials(:,(1:400))';  %keep only presample to first match + 200, which is 2000 ms or 400 samples
                        data2 = p2trials(:,(1:400))';
                        perms = [];
                        for all_perms = 1 : 100
                            clc
                            1000 - all_perms
                            total_pairs - p
                            perms = randperm(ntrials);
                            perms1 = randperm(ntrials);
                            perms2 = randperm(ntrials);
                            
                            permdata1 = data1(:,perms1);
                            permdata2 = data2(:,perms2);
                            
                            [fx2y,fy2x,fxy,time,freq]=GCgram(permdata1,permdata2,params,movingwin,20);
                            
                            surgrangergram.Fx2y{1,all_perms} = single(squeeze(fx2y));
                            surgrangergram.Fy2x{1,all_perms} = single(squeeze(fy2x));
                            surgrangergram.Fxy{1,all_perms} = single(squeeze(fxy));
                            surgrangergram.t{1,all_perms} = single(time);
                            surgrangergram.f{1,all_perms} = single(freq);
                            surgrangergram.trials{1,all_perms} = single(idetrials);
                            surgrangergram.groups{1,all_perms} = single(gpairs(p,:));
                            
                            
% %                             
% %                             rdata1 = [];
% %                             rdata2 = [];
% %                             for ide = 1 : 5
% %                                 %randomize the trials, with the correct trial count
% %                                 idet =  size(trials{ide},2);
% %                                 ptrials = perms(1:idet);
% %                                 perms(1:idet) = [];
% %                                 rdata1 = data1(:,ptrials);
% %                                 rdata2 = data2(:,ptrials);
% %                                 
% %                                 [suridecohgram.C{all_perms,ide},suridecohgram.phi{all_perms,ide},~,~,~,suridecohgram.t{all_perms,ide},suridecohgram.f{all_perms,ide},suridecohgram.confC{all_perms,ide},suridecohgram.phistd{all_perms,ide}] = cohgramc(rdata1,rdata2,movingwin,params);
% %                                 
% %                                 suridecohgram.trials{all_perms,ide} = ptrials;
% %                                 suridecohgram.groups{all_perms,ide} = gpairs(p,:);
% %                             end
                        end
                        
                        cd([lfpdir filesep 'grangergrams']);
                        
                        write_info = writeinfo(dbstack);
                        file = [ 'surgrangergram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(file,'surgrangergram','write_info');
                        
%                         file = [ 'suridecohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
%                         save(file,'suridecohgram','write_info');
                    end
                end
            end
            
            % %             %fit surrogates
            % %             if Args.fit_surrogates
            % %                 cd([lfpdir filesep 'cohgrams']);
            % %                 fit_bmfcohgram_thresh('pairwise');
            % %             end
            % %
            % %             if Args.threshold_coh
            % %                 cd([lfpdir filesep 'cohgrams']);
            % %                 threshold_coherence('pairwise');
            % %             end
            
        end
    end
end

cd(monkeydir)












