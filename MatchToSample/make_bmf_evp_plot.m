function make_bmf_evp_plot

cd('/media/bmf_raid/data/monkey/ethyl/')
% allevoked = ProcessDays(evokedobj,'days',alldays,'ml','NoSites','Sites','session01');
% save allevoked allevoked
load allevoked allevoked

j = jet; %use colormap jet
hh = jet;
close gcf
h(:,:,1) = hh(:,1);h(:,:,2) = hh(:,2);h(:,:,3) = hh(:,3);
lc = linspace(-1,1,64);


grid = [ ...
    
0, 0,  0,  0,  0,  0,  50, 61, 72, 83, 94,  105, 116, 127, 138, 149, 160, 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  0,  30, 40, 51, 62, 73, 84, 94,  106, 117, 128, 139, 150, 161, 171, 181, 191, 0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  21, 31, 41, 52, 63, 74, 85, 96,  107, 118, 129, 140, 151, 162, 172, 182, 192, 201, 210, 0,   0,   0,   0,   0,   0;
0, 0,  13, 22, 32, 42, 53, 64, 75, 86, 97,  108, 119, 130, 141, 152, 163, 173, 183, 193, 202, 211, 219, 227, 0,   0,   0,   0;
0, 6,  14, 23, 33, 43, 54, 65, 76, 87, 98,  109, 120, 131, 142, 153, 164, 174, 184, 194, 203, 212, 220, 228, 235, 0,   0,   0;
0, 7,  15, 24, 34, 44, 55, 66, 77, 88, 99,  110, 121, 132, 143, 154, 165, 175, 185, 195, 204, 213, 221, 229, 236, 242, 0,   0;
1, 8,  16, 25, 35, 45, 56, 67, 78, 89, 100, 111, 122, 133, 144, 155, 166, 176, 186, 196, 205, 214, 222, 230, 237, 243, 248, 0;
2, 9,  17, 26, 36, 46, 57, 68, 79, 90, 101, 112, 123, 134, 145, 156, 167, 177, 187, 197, 206, 215, 223, 231, 238, 244, 249, 253;
3, 10, 18, 27, 37, 47, 58, 69, 80, 91, 102, 113, 124, 135, 146, 157, 168, 178, 188, 198, 207, 216, 224, 232, 239, 245, 250, 254;
4, 11, 19, 28, 38, 48, 59, 70, 81, 92, 103, 114, 125, 136, 147, 158, 169, 179, 189, 199, 208, 217, 225, 233, 240, 246, 251, 255;
5, 12, 20, 29, 39, 49, 60, 71, 82, 93, 104, 115, 126, 137, 148, 159, 170, 180, 190, 200, 209, 218, 226, 234, 241, 247, 252, 256];


xy = [];
cch = 0;
for column = 1:28
    for row = 1:11
        if grid(row,column) ~= 0
            cch = cch +1;
            xy(cch,1) = row;
            xy(cch,2) = column;
        end
    end
end


%make a movie
figure('Position',[150 200 1400 700])
counter = 0;
for t = 500:800
    counter = counter + 1;
    for allch = 1 : 256
        ii = find(allevoked.data.Index(:,1) == allch);
        if ~isempty(ii)
            ind = ii(1); %use first recording
            
            
            load(allevoked.data.setNames{ind})
            
            evp = diff(evokedpotential{11}) ./ max(abs(diff(evokedpotential{11})));
            
            [~,cc] = min(abs(lc - evp(t)));
            scatter(xy(allch,2),(11-xy(allch,1)),550,hh(cc,:),'filled');hold on %plot voltage
        end
        
        
        
        
        
        
    end
    axis([-1 30 -1 12])
    colorbar
    
    title(['Time: ' num2str(t)]);
    
    F(counter)= getframe(gcf);
    
    
    
end

movie2avi(F, ['test_mov'],'compression','none','fps',2)
implay('test_mov.avi')

F;




