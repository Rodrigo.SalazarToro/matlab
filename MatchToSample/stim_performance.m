function stim_performance

%run at session level, gets performance information for each stimulus

mt = mtstrial('auto','ML','RTfromML');
locs = unique(mt.data.CueLoc);
objs = unique(mt.data.CueObj);

all = zeros(1,400);
all_inc = zeros(1,400);

counter = 0;
for object = 1:3
    for location = 1:3
        counter = counter + 1;
        correct = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
        num_correct = size(correct,2);
       
        incorrect = mtsgetTrials(mt,'ML','BehResp',0,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
        num_incorrect = size(incorrect,2);
        all(correct) = counter;
        all_inc(incorrect) = counter;
        performance(counter) = (num_correct / (num_correct + num_incorrect)) * 100;
    end
end

performance
