function phase_angle_table_info(varargin)


%make summary figure for ips phase relationship
%currently parietal only

Args = struct('acpval',.00001,'xc',.99); %xcthresh = unimodal_thresh_corrcoef
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/raid/data/monkey/betty/')

% cd(['/media/raid/data/monkey/betty/091001/session01'])
% 
% cpp_b = mtscpp2('auto','ml');

%
% load alldays
% cpp_b = processDays(mtscpp2,'days',alldays,'ml','NoSites')
% save cpp_b cpp_b
load cpp_b


minpairs = 10;
rtestp = .05;
disttestp = .05;
% 
% epoch1 = 54; %identity delay
% % epoch2 = 153; %incorrect iti
% epoch2 = 205; %location delay



epoch1 = 54;
epoch2 = 153;




figure
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10

[i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'cross_corr',Args.xc,'hsfpp_criteria','corr_mag_criteria');

        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp


                counter = counter + 1;

                rtests(counter) = circ_rtest(pangles.*(pi/180));
                rtests2(counter) = circ_rtest(pangles2.*(pi/180));

                mangles(counter) = circ_mean(pangles.*(pi/180)) * (180/pi);
                mangles2(counter) = circ_mean(pangles2.*(pi/180)) * (180/pi);

                sangles(counter) = circ_std(pangles.*(pi/180)) * (180/pi);
                sangles2(counter) = circ_std(pangles2.*(pi/180)) * (180/pi);

                combon{counter} = [allmlhist(mlh1) allmlhist(mlh2)];
                comboarea{counter} = reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)]);
                reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)])


                errorbar(counter,mangles(counter),sangles(counter),'xb')
                hold on
                errorbar(counter,mangles2(counter),sangles2(counter),'xr')
                hold on
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    text(counter,200,comboarea{counter},'Color',[1 0 0])
                else
                    text(counter,200,comboarea{counter},'Color',[0 0 0])
                end
                axis([0 counter + 1 -250 250])
            end
        end
    end
end
i;
%% incorrect delay and iti
figure
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
        [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'incorr_iti');

        pangles = cpp_b.data.Index(ii,150);
        pangles2 = cpp_b.data.Index(ii,epoch2);
        if i >= minpairs 1
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp


                counter = counter + 1;

                rtests(counter) = circ_rtest(pangles.*(pi/180));
                rtests2(counter) = circ_rtest(pangles2.*(pi/180));

                mangles(counter) = circ_mean(pangles.*(pi/180)) * (180/pi);
                mangles2(counter) = circ_mean(pangles2.*(pi/180)) * (180/pi);

                sangles(counter) = circ_std(pangles.*(pi/180)) * (180/pi);
                sangles2(counter) = circ_std(pangles2.*(pi/180)) * (180/pi);

                combon{counter} = [allmlhist(mlh1) allmlhist(mlh2)];
                comboarea{counter} = reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)]);
                reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)])


                errorbar(counter,mangles(counter),sangles(counter),'xb')
                hold on
                errorbar(counter,mangles2(counter),sangles2(counter),'xr')
                hold on
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    text(counter,200,comboarea{counter},'Color',[1 0 0])
                else
                    text(counter,200,comboarea{counter},'Color',[0 0 0])
                end
                axis([0 counter+1 -250 250])
            end

        end
    end
end

















%% scatter plot correct delay, incorrect iti PP
figure
cm = colormap;
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

ncounter = 0;
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
        [i ii] = get(cpp_b,'Number','ml','pp','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    ncounter = ncounter + 1;
                end
            end
        end
    end
end
colorinc = floor(64/ncounter);

for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
%         [i ii] = get(cpp_b,'Number','ml','pp','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
        [i ii] = get(cpp_b,'Number','ml','pp','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp

                    if cmean1 < 20 && cmean2 < 20
                        subplot(1,2,1)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    else
                        subplot(1,2,2)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    end
                    cmcounter = cmcounter+colorinc;
                    title('pp')
                    axis([0 180 0 180])
                end
            end
        end
    end
end


%%
%% scatter plot correct delay, incorrect iti PF
figure
cm = colormap;
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

ncounter = 0;
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
        [i ii] = get(cpp_b,'Number','ml','pf','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    ncounter = ncounter + 1;
                end
            end
        end
    end
end
colorinc = floor(64/ncounter);

for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
%         [i ii] = get(cpp_b,'Number','ml','PF','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
  [i ii] = get(cpp_b,'Number','ml','PF','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp

                    if cmean1 < 20 && cmean2 < 20
                        subplot(1,2,1)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    else
                        subplot(1,2,2)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    end
                    cmcounter = cmcounter+colorinc;
                    title('pf')
                    axis([0 180 0 180])
                end
            end
        end
    end
end
%%
%
%% scatter plot correct delay, incorrect iti FF
figure
cm = colormap;
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

ncounter = 0;
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
        [i ii] = get(cpp_b,'Number','ml','ff','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    ncounter = ncounter + 1;
                end
            end
        end
    end
end
colorinc = floor(64/ncounter);

for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
%         [i ii] = get(cpp_b,'Number','ml','ff','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
[i ii] = get(cpp_b,'Number','ml','ff','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);

        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp

                    if cmean1 < 20 && cmean2 < 20
                        subplot(1,2,1)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    else
                        subplot(1,2,2)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    end
                    cmcounter = cmcounter+2;
                    title('ff')
                    axis([0 180 0 180])
                end
            end
        end
    end
end
%%


%% scatter plot correct delay, incorrect iti
figure
cm = colormap;
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

ncounter = 0;
for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
        [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    ncounter = ncounter + 1;
                end
            end
        end
    end
end
colorinc = floor(64/ncounter);

for mlh1 = 1 : 10
    for mlh2 = mlh1+1 : 10
%         [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
[i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp

                    if cmean1 < 20 && cmean2 < 20
                        subplot(1,2,1)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    else
                        subplot(1,2,2)
                        scatter(abs(pangles),abs(pangles2),50,[cm(cmcounter,:)],'filled')
                        hold on
                        scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
                        hold on
                        scatter(cmean1,cmean2,100,[0 0 0],'filled');
                    end
                    cmcounter = cmcounter + 2;

                    if cmcounter > 64
                        cmcounter = 1;
                    end

                    title('all')
                    axis([0 180 0 180])
                end
            end
        end
    end
end
%%



%% scatter plot correct delay, incorrect iti
figure
cm = colormap;
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

ncounter = 0;
for mlh1 = 1 : 10
    for mlh2 = 1 : 10
        [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    ncounter = ncounter + 1;
                end
            end
        end
    end
end
plotcounter = 0;
for mlh1 = 1 : 10
    for mlh2 = 1 : 10
%         [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
[i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);

        cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
        cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
%             if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
%                 if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    plotcounter = plotcounter + 1;
                    subplot(7,10,plotcounter);
%                     if cmean1 < 20 && cmean2 < 20
%                         subplot(1,2,1)
                        scatter(abs(pangles),abs(pangles2),20,[0 0 0],'filled')
                        hold on
%                         scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
%                         hold on
%                         scatter(cmean1,cmean2,100,[0 0 0],'filled');
%                     else
%                         subplot(1,2,2)
%                         scatter(abs(pangles),abs(pangles2),1,[cm(cmcounter,:)],'filled')
%                         hold on
%                         scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
%                         hold on
%                         scatter(cmean1,cmean2,100,[0 0 0],'filled');
%                     end
                    cmcounter = cmcounter + 1;

                    if cmcounter > 64
                        cmcounter = 1;
                    end

                   h1 = reverse_categorizeNeuronalHist(allmlhist(mlh1));
                    h2 = reverse_categorizeNeuronalHist(allmlhist(mlh2));
                    title([h1{1} ' ' h2{1}])
                    axis([0 180 0 180])
%                 end
%             end
        end
    end
end



%% scatter plot correct delay, incorrect iti
figure
cm = colormap;3
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
cmcounter = 1;

% ncounter = 0;
% for mlh1 = 1 : 10
%     for mlh2 = 1 : 10
%         [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
%         pangles = cpp_b.data.Index(ii,61);
%         pangles2 = cpp_b.data.Index(ii,161);
%
%         cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
%         cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
%         if i >= minpairs
%             if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
%                 if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
%                     ncounter = ncounter + 1;
%                 end
%             end
%         end
%     end
% end
plotcounter = 0;
ncounter = 71
for mlh1 = 1 : 10
    for mlh2 = 1 : 10
%         [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
[i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = abs(cpp_b.data.Index(ii,61));
        pangles2 = abs(cpp_b.data.Index(ii,153));

%         cmean1 = abs(circ_mean(pangles.*(pi/180)) * (180/pi));
%         cmean2 = abs(circ_mean(pangles2.*(pi/180)) * (180/pi));
        if i >= minpairs
%             if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
%                 if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    plotcounter = plotcounter + 1;
                    if plotcounter <=70
                    subplot(7,10,plotcounter);
%                     if cmean1 < 20 && cmean2 < 20
%                         subplot(1,2,1)
                        scatter(pangles,pangles2,20,[0 0 0],'filled')
                        hold on
%                         scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
%                         hold on
%                         scatter(cmean1,cmean2,100,[0 0 0],'filled');
%                     else
%                         subplot(1,2,2)
%                         scatter(abs(pangles),abs(pangles2),1,[cm(cmcounter,:)],'filled')
%                         hold on
%                         scatter(cmean1,cmean2,300,[cm(cmcounter,:)],'filled');
%                         hold on
%                         scatter(cmean1,cmean2,100,[0 0 0],'filled');
%                     end
                    cmcounter = cmcounter + 1;

                    if cmcounter > 64
                        cmcounter = 1;
                    end

                   h1 = reverse_categorizeNeuronalHist(allmlhist(mlh1));
                    h2 = reverse_categorizeNeuronalHist(allmlhist(mlh2));
                    title([h1{1} ' ' h2{1}])
                    axis([0 1 0 1])
                    hold on
                    plot([0 1],[0 1],'r')
                    end
%                 end
%             end
        end
    end
end









figure
cd('/media/raid/data/monkey/betty/')
load alldays
daydir = pwd;
cmcounter = 0;
for d = 1 : 46
    cmcounter = cmcounter + 1;
    cd([daydir filesep alldays{d} filesep 'session01'])
    cpp_b = mtscpp2('auto','ml')

%% scatter plot correct delay, incorrect iti

        %         [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc,'correct_delay_incorr_iti');
        [i ii] = get(cpp_b,'Number','ml','mlhist',[4 11],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        pangles = abs(cpp_b.data.Index(ii,54));
        pangles2 = abs(cpp_b.data.Index(ii,153));
        
        scatter(pangles,pangles2,20,[cm(cmcounter,:)],'filled')


        h1 = reverse_categorizeNeuronalHist(4);
        h2 = reverse_categorizeNeuronalHist(11);
        title([h1{1} ' ' h2{1}])
        axis([0 180 0 180])
        hold on

end








