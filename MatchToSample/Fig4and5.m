
%% fixation
cd /Volumes/raid/data/monkey/betty
obj = loadObject('cohInterLong.mat');
[r,ind] = get(obj,'Number','snr',99);r
cd ..
[mugram,time,f,ntrials,sig,idePairs,presample] = mkAveMugram(obj,ind,'type','IdePerLoc','delayRange',[1.3 1.8],'addNameFile','onlyLongDays');
[mugram,time,f,ntrials] = mkAveMugram(obj,ind,'type','IdePerLoc','noSur','addNameFile','IncorCorrected','save','IncorCorrected');
[fixgram,time,f,ntrialsF] = mkAveMugram(obj,ind,'type','Fix','noSur','minTbin',37);

figure; 
subplot(3,2,1)
imagesc(time,f,squeeze(prctile(mugram(idePairs,:,:),50,1))',[0.15 0.35])
colorbar
ylim([8 30])
title('MTS trials')
subplot(3,2,3)
imagesc(time,f,squeeze(prctile(fixgram(idePairs,:,:),50,1))',[0.15 0.35])
ylim([8 30])
title('Fixation trials')
colorbar


subplot(3,2,2)
hist(ntrials(idePairs),[10:140])
subplot(3,2,4)
hist(ntrialsF(idePairs),[10:140])
xlabel('Number of trials')
ylabel('Number of pairs')

diffgram = mugram(:,1:37,:)-fixgram;
subplot(3,2,5)
imagesc(time,f,squeeze(mean(diffgram,1))')
colorbar
ylim([8 30])
title('Difference')
xlabel('Time [sec]')
ylabel('Frequency [Hz]')

for row = 1 : 37; for col = 1 : size(mugram,3); [h,p(row,col)] = ttest(diffgram(idePairs,row,col)); end; end
subplot(3,2,6)
imagesc(time,f,(p<10^-5)')
xlabel('Time [sec]')
ylabel('Frequency [Hz]')
title('Significant bins')
ylim([8 30])

%% incorrect

cd monkey/

obj = loadObject('cohInterIDEL.mat');
[r,ind] = get(obj,'Number','snr',99);r
[mugram,time,f,ntrials,sig,idePairs,presample] = mkAveMugram(obj,ind,'type','IdePerLoc','delayRange',[1.3 1.8]);
[mugram,time,f,ntrials] = mkAveMugram(obj,ind,'type','IdePerLoc','noSur','addNameFile','IncorCorrected','IncorCorrected');
[Incorgram,time,f,ntrialsF] = mkAveMugram(obj,ind,'type','IdePerLocIncor','noSur');

limit = 22;
enoughT = find(ntrials > limit & ntrialsF > limit);

goodpairs = intersect(idePairs,enoughT);

figure; 
subplot(4,1,1)
imagesc(time,f,squeeze(prctile(mugram(goodpairs,:,:),50,1))')
% imagesc(time,f,squeeze(mean(mugram(goodpairs,:,:),1))')
ylim([8 30])
title('MTS trials')
colorbar
subplot(4,1,2)
imagesc(time,f,squeeze(prctile(Incorgram(goodpairs,:,:),50,1))')
% imagesc(time,f,squeeze(mean(Incorgram(goodpairs,:,:),1))')
ylim([8 30])
title('Incorrect trials')
colorbar

diffgram = mugram -Incorgram;
subplot(4,1,3)
imagesc(time,f,squeeze(mean(diffgram(goodpairs,:,:),1))')
% imagesc(time,f,squeeze(mean(Incorgram(goodpairs,:,:),1))')
ylim([8 30])
title('difference')
colorbar


xlabel('Time [sec]')
ylabel('Frequency [Hz]')
for sb = 1 : 2; subplot(4,1,sb); caxis([0.15 0.4]); end

% for row = 1 : size(mugram,2); for col = 1 : size(mugram,3); [h,p(row,col)] = ttest2(mugram(goodpairs,row,col),Incorgram(goodpairs,row,col)); end; end
for row = 1 : size(mugram,2); for col = 1 : size(mugram,3); [h,p(row,col)] = ttest(diffgram(goodpairs,row,col)); end; end

subplot(4,1,4)
imagesc(time,f,(p<10^-5)')
colorbar
ylim([8 30])


figure; 
subplot(2,1,1)
hist(ntrials(goodpairs),[0:50])
subplot(2,1,2)
hist(ntrialsF(goodpairs),[0:50])

