function [dat,time,varargout] = mkPopPSTHMI(obj,ind,varargin)

Args = struct('save',0,'redo',0,'sigLevel',99,'rule',1,'binSize',100,'addNameFile',[],'type','IdePerLoc','MatchAlign',0,'periodSelection','both','surNorm',0,'biasCorr',0);
Args.flags = {'save','redo','plot','pSFC','MatchAlign','surNorm','biasCorr'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

sdir = pwd;
if Args.MatchAlign
    matfile = sprintf('mkPop%sMatchAlign%s.mat',Args.type,Args.addNameFile);
else
    matfile = sprintf('mkPop%s%s.mat',Args.type,Args.addNameFile);
end
if Args.redo || isempty(nptDir(matfile))
    switch Args.type
        
        case 'IdePerLoc'
            files =  {'PSTHIdePerLoc1' 'PSTHIdePerLoc2' 'PSTHIdePerLoc3'};
            mu = true;
        case 'LocPerIde'
            mu = true;
            files =  {'PSTHLocPerIde1' 'PSTHLocPerIde2' 'PSTHLocPerIde3'};
            
        case 'PSTH'
            mu = false;
            files =  {'nineStimPSTH'};
            
        case 'Ide'
          mu = true;
            files =  {'PSTHIde'};  
        case 'Loc'
             mu = true;
            files =  {'PSTHLoc'};  
    end
    
    for ff = 1 : length(files);
        if Args.MatchAlign
            if mu
                files{ff} = sprintf('%sMatchAlignrule%d.mat',files{ff},Args.rule);
            else
                files{ff} = sprintf('%s%d.mat',files{ff},Args.rule);
                align = 2;
            end
        else
            if mu
                files{ff} = sprintf('%srule%d.mat',files{ff},Args.rule);
            else
                files{ff} = sprintf('%s%d.mat',files{ff},Args.rule);
                align = 1;
            end
        end
    end
    c = 1;
    
    for ii = ind
        
        cd(obj.data.setNames{ii})
        
        if mu == false
            
            load(files{ff},'A','reference')
            time = [reference{1,align} : Args.binSize : size(A{1,align},2)+reference{1,align}] + Args.binSize/2;
            allt = cell2mat(A(:,1));
            
            for bi = 1 : length(time)-1
                dat(c,bi) = sum(sum(allt(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2),1)*1000/(Args.binSize*size(allt,1));
                
            end % rebin the 1mse matraix
            sig = [];
        else
            for ff = 1 : length(files)
                meanMI = zeros(1,length(files));
                load(files{ff},'mi','time')
                switch Args.periodSelection
                    case 'sample'
                        stime = find(time > 0 & time <= 500);
                    case 'delay'
                        stime = find(time > 500 & time <= 1300);
                    case 'both'
                        stime = find(time > 0 & time <= 1300);
                end
                meanMI(ff) = mean(mi(10001,stime));
            end
            [~,sff] = max(meanMI);
            load(files{sff},'mi','time')
            sur = prctile(mi(1:10000,:),Args.sigLevel,1);
            if Args.biasCorr
                %                 bias = prctile(mi(1:10000,:),50,1);
                %                 bias = mean(mi(1:10000,:),1);
                [distr,values] = hist(squeeze(mi(1:10000,:)), 500) ;
                distr = distr ./ repmat(sum(distr,1),500,1);
                bias = sum(distr .* repmat(values,1,size(distr,2)),1);
            end
            if Args.surNorm
                dat(c,:) = mi(10001,:)- sur;
            else
                if Args.biasCorr
                    dat(c,:) = mi(10001,:) - bias;
                else
                    dat(c,:) = mi(10001,:) ;
                end
            end
            sig(c,:) = mi(10001,:) > sur;
            
        end
        c = c+ 1;
    end
    if Args.save
        cd(sdir)
        save(matfile)
        display(sprintf('saving %s \n',matfile))
    end
    
else
    load(matfile)
end
varargout{1} = sig;