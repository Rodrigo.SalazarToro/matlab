function obj = plot(obj,varargin)


Args = struct('raster',0,'displayStats',[],'tune',[],'rule',1,'plotType','nineStimPSTH','mutualInfo',[],'binSize',100,'MItile',99,'bmf',0,'ACtile',[96 1 98],'pThres',0.01);
Args.flags = {'raster','bmf'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'raster'});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3]; % first row == loc
IDECues = [1 2 3; 4 5 6; 7 8 9];
LOCCues = [1 4 7; 2 5 8; 3 6 9];
epochs = {'presample' 'sample' 'delay1' 'delay2'};
alignement = {'sample onset' 'match onset' 'saccade onset'};
if Args.bmf
    areasN = [];
else
    areasN = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
end
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
% display(obj.data.Index(ind,28:37))
load ispikes
switch Args.plotType
    %%
    case 'nineStimPSTH'
        load(sprintf('%s%d.mat',Args.plotType,Args.rule))
        
        if ~isempty(Args.tune)
            scues = eval(sprintf('%sCues',Args.tune));
            
            cA = cell(3,3);
            
            
            for al = 1: 3
                for cue = 1 : 3
                    tspike = 0;
                    nt = 0;
                    for sbc = 1 : 3
                        cA{cue,al} = [cA{cue,al}; A{scues(cue,sbc),al}];
                        tspike = tspike + spikefr{scues(cue,sbc),al} * size(A{scues(cue,sbc),al},1);
                        nt = nt + size(A{scues(cue,sbc),al},1);
                    end
                    cspikefr{cue,al} = tspike / nt;
                    labels{cue} = sprintf('%s %d',Args.tune,cue);
                end
            end
            
            A = cA;
            ncues = 3;
            spikefr = cspikefr;
        elseif strcmp(Args.plotType,'threeSacPSTH')
            for cue = 1 : 3; labels{cue} = sprintf('Saccade to location %d',cue); end
            ncues = 3;
            
        elseif Args.bmf
            
            ncues = size(A,1);
            c = 1; for cue = 1 : ncues; labels{c} = sprintf('ide %d',cue); c = c + 1; end
        else
            c = 1; for cue = 1 : 9; labels{c} = sprintf('ide %d; loc %d',cueComb(2,c),cueComb(1,c)); c = c + 1; end
            ncues = 9;
        end
        
        
        if Args.raster
            
            for cue = 1 : ncues
                
                for al = 1 : 3
                    subplot(ncues,3,(cue-1) * 3 + al);
                    if size(A{cue,al},1) <= 10
                        A{cue,al} = (filter(ones(1,6)/6,1,A{cue,al}'))';
                    elseif size(A{cue,al},1) > 10 & size(A{cue,al},1) <= 20
                        A{cue,al} = (filter(ones(1,7)/7,1,A{cue,al}'))';
                        
                    elseif size(A{cue,al},1) > 20
                        A{cue,al} = (filter(ones(1,12)/12,1,A{cue,al}'))';
                    end
                    imagesc(A{cue,al} == 0)
                    colormap(gray)
                    
                    hax = gca;
                    set(hax,'Xtick',[1 : abs(reference{cue,al}) : size(A{cue,al},2)]);
                    set(hax,'XTickLabel',[reference{cue,al} : abs(reference{cue,al}) : size(A{cue,al},2)+reference{cue,al}]);
                    if al == 1; ylabel(labels{cue}); end
                end
            end
        else
            
            allymin = [];
            allymax = [];
            
            for cue = 1 : ncues
                
                for al = 1 : 3
                    
                    subplot(ncues,3,(cue-1) * 3 + al);
                    Mbins = [reference{cue,al} : Args.binSize : size(A{cue,al},2)+reference{cue,al}] + Args.binSize;
                    Mspikefr = zeros(length(Mbins),1);
                    for bi = 1 : length(Mbins)-1
                        Mspikefr(bi) = sum(sum(A{cue,al}(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2),1)*1000/(Args.binSize*size(A{cue,al},1));
                        
                    end % rebin the 1mse matraix
                    if length(Mspikefr) > 1
                        step = Mbins(2) - Mbins(1);
                        plot([Mbins(1:end-1) Mbins(end)+step],Mspikefr);
                        if length(Mbins) > 1
                            axis([Mbins(1) Mbins(end) ymin{cue,al} ymax{cue,al}])
                        end
                        
                        hold on
                        line([0 0],[0 max(Mspikefr)],'Color','r')
                        if ~isempty(Args.displayStats) & al == 1
                            load FR4epochs.mat
                            [cueP,delayP,matchP] = CellFRstats(allspikes);
                            text(1500,max(Mspikefr),sprintf('p = %.4f',eval(sprintf('%s.sample(cue).p',cell2mat(Args.displayStats)))));
                        end
                        if al == 1; text(0,max(Mspikefr),sprintf('n=%d',size(A{cue,al},1))); end
                        hold off
                        allymin = [allymin ymin{cue,al}];
                        allymax = [allymax max(Mspikefr(1:end-5))];
                    end
                    if al == 1; ylabel(labels{cue}); end
                end
            end
            if max(allymax) ~= 0; for sb = 1 : ncues*3; subplot(ncues,3,sb); ylim([min(allymin) max(allymax)]); set(gca,'TickDir','out');end; end
        end
        for sb = 1 : 3; subplot(ncues,3,sb); title(alignement{sb}); end
        %%
    case 'MI'
        file = sprintf('%srule%d.mat',Args.mutualInfo,Args.rule);
        load(file,'time','mi','ntrial')
        time = time +50;
        ncues = sum(ntrial);
        sur = prctile(mi(1:end-1,:),Args.MItile,1);
        
        plot(time,mi(end,:),'r')
        hold on
        plot(time,sur,'b--')
        xlabel('time [sec]')
        ylabel('mutual info [bits]')
        title(sprintf('%d trials',ncues))
        set(gca,'TickDir','out');
        %%
    case 'autoCorr'
        
        matfile = sprintf('xcorrg%s%sg%s%sRule%d.mat',sp.data.groupname,sp.data.cellname,sp.data.groupname,sp.data.cellname,Args.rule);
        mdir = pwd;
        cd ../..
        cd xcorr
        load(matfile)
        
        if max(max(max(crosscorr))) ~= 0
            %             surfile = sprintf('xcorrg%s%sg%s%sRule%dSurspikeTime.mat',sp.data.groupname,sp.data.cellname,sp.data.groupname,sp.data.cellname,Args.rule);
            %             if ~isempty(nptDir(surfile))
            
            try
                sur = load(surfile);
            end
            cd(mdir)
            xc = sum(crosscorr,3);
            for ep = 1 : 4
                subplot(2,4,ep)
                plot(timelag,xc(ep,:))
                if exist('sur','var')
                    hold on
                    plot(timelag,prctile(squeeze(sur.crosscorr(ep,:,:)),Args.ACtile(1),2),'--')
                    plot(timelag,prctile(squeeze(sur.crosscorr(ep,:,:)),Args.ACtile(2),2),'--')
                end
                title(epochs{ep})
                ylim([min(min(xc)) 1.2*max(max(xc))])
                xlim([min(timelag) max(timelag)])
            end
            
            xlabel('Time lags (msec)')
            ylabel('auto-correlation (# coincidences)')
            maxy = [];
            miny = [];
            for ep = 1 : 4
                subplot(2,4,4+ep)
                try
                    thedata = nanmedian(S1{ep} - repmat(nanmean(S1{ep},1),101,1),2);
                    %                         thedata = nanmean(S1{ep} - repmat(getExpValue(sur.S1{ep},sur.f)',1,size(S1{ep},2)),2);
                    plot(f,thedata);
                    maxy = [maxy; max(thedata)];
                    miny = [miny; min(thedata)];
                end
                
                title(epochs{ep})
                
                xlim([0 30])
            end
            try for sb = 5 : 8;  subplot(2,4,sb); ylim([min(miny) max(maxy)]); end; end
            %             end
            xlabel('Frequency (Hz)')
            ylabel('Power')
        end
        cd(mdir)
        %%
    case 'autoCorrStim'
        
        maxl = [];
        minl = [];
        le = {'b' 'g' 'r' 'c'};
        for objs = 1 : 3
            for loc = 1 : 3
                matfile = sprintf('xcorrg%s%sg%s%sRule%dIde%dLoc%d.mat',sp.data.groupname,sp.data.groupname,sp.data.cellname,sp.data.cellname,Args.rule,objs,loc);
                if ~isempty(nptDir(matfile))
                    load(matfile)
                    
                    if max(max(crosscorr)) ~= 0
                        %                     surfile = sprintf('xcorrg%sg%sRule%dSurspikeTime.mat',sp.data.groupname,sp.data.groupname,Args.rule);
                        %                     sur = load(surfile);
                        
                        subplot(3,3,(objs-1)*3+loc)
                        if isempty(Args.tune)
                            maxl = [maxl max(max(crosscorr))];
                            minl = [minl min(min(crosscorr))];
                            plot(timelag,crosscorr)
                        else
                            for ep = 1 : 4
                                pdata = S{ep}/nanmean(S{ep});
                                maxl = [maxl max(max(pdata(3:end)))];
                                minl = [minl min(min(pdata))];
                                try plot(f,pdata,le{ep}); end
                                hold on
                            end
                        end
                        title(sprintf('Ide%dLoc%d',objs,loc))
                    end
                end
            end
        end
        xlabel('Time lags (msec)')
        ylabel('auto-correlation (# spikes)')
        legend(epochs)
        if ~isempty(minl)
            if isempty(Args.tune)
                for sb = 1 : 9; subplot(3,3,sb); axis([timelag(1) timelag(end) 0.9*min(minl) 1.1*max(maxl)]); end;
            else
                for sb = 1 : 9; subplot(3,3,sb); axis([f(1) f(end) 0.9*min(minl) 1.1*max(maxl)]); end;
            end
        end
        
    case 'rulePSTH'
        %         epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
        epochs = {'allTrialSample' 'allTrialMatch'};
        %         timings = {'from sample onset' 'from sample onset' 'from sample onset' 'from match onset' 'from match onset'};
        timings = {'from sample onset' 'from Match onset'};
        rules = {'IDE' 'stats' 'LOC'};
        rlb = {'r' 'b' 'k--' 'g--' 'k-.' 'g-.' 'k:' 'g:'};
        maxFR = zeros(length(epochs),2);
        minFR = zeros(length(epochs),2);
        sdir = pwd;
        
        bfiles = nptDir('nineStimPSTHb*');
        ns = 2 + length(bfiles);
        
        
            for r =1 : ns
                
                if r <= 2
                    load(sprintf('nineStimPSTH%d.mat',r),'A','reference')
                else
                    load(sprintf('nineStimPSTHb%d.mat',r-2),'A','reference')
                end
                
                for ep = 1 : length(epochs)
                    statfile = nptDir(sprintf('compPSTHRules%s*.mat',epochs{ep}));
                    if ~isempty(statfile)
                        psthstat = load(statfile(1).name);
                        switch epochs{ep}
                            case 'presample'
                                timing = [-400 0];
                                ref = 1;
                            case 'sample'
                                timing = [100 500];
                                ref = 1;
                            case 'delay1'
                                timing = [500 900];
                                ref = 1;
                            case 'delay2'
                                timing = [-400 0];
                                ref = 2;
                            case 'saccade'
                                timing = [-100 200];
                                ref = 2;
                            case 'allTrialSample'
                                timing = [-499 1300];
                                ref = 1;
                            case 'allTrialMatch'
                                timing = [-799 200];
                                ref = 2;
                        end
                        
                        
                        alltrials = cat(1,A{:,ref});
                        if size(alltrials,1) > 10 && sum(sum(alltrials)) ~= 0
                            FR = 1000* sum(alltrials,1) ;
                            
                            FR = FR(abs(reference{1,ref}) + timing(1) : abs(reference{1,ref}) + timing(2));
                            binSize = abs(psthstat.Mbins(1) - psthstat.Mbins(2));
                            Mbins = [timing(1) : binSize : timing(2)];
                            Mspikefr = zeros(length(Mbins),1);
                            
                            for bi = 1 : length(Mbins)-1
                                Mspikefr(bi) = sum(sum(FR(:,(bi-1)*binSize +1 : bi*binSize ),2),1)/(binSize * size(alltrials,1));
                                
                            end
                            
                            ml = min([length(psthstat.pval) length(psthstat.Mbins)]);
                            subplot(1,length(epochs),ep)
                            plot(Mbins(2:end),filtfilt(repmat(1/2,1,2),1,Mspikefr(1:end-1)),rlb{r})
                            
                            hold on
                            if r < 3
                                plot(Mbins(1:ml),(psthstat.pval(1:ml) < (Args.pThres/length(Mbins))).*filtfilt(repmat(1/2,1,2),1,Mspikefr(1:ml)),'r*')
                            end
                            grid on
                            title(sprintf('%s',epochs{ep}))
                            maxFR(ep,r) = max(Mspikefr(1:end-1));
                            minFR(ep,r) = min(Mspikefr(1:end-1));
                            if ep == 1
                            xlim([-450 125])
                            else
                                xlim([-750 150])
                            end
                            xlabel(timings{ep})
                        end
                    end
                    
                end
            end
            
            legend(rules)
            
            ylabel('Firing rate (sp/s)')
            if exist('statfile','var') && ~isempty(statfile); for sb = 1 : length(epochs); subplot(1,length(epochs),sb);ylim([min(min(minFR)) max(max(maxFR))]); end; end
        
    case 'ISI'
        rules = {'IDE' 'LOC'};
        maxv = zeros(2,1);
        for r = 1 : 2
            load(sprintf('nineStimPSTH%d.mat',r),'A','reference')
            isi = [];
            for tr = 1 : size(A{1,2},1)
                isi = [isi diff(find(A{1,2}(tr,1400:1800) == 1)) zeros(1,length(find(A{1,2}(tr,1400:1800) == 2))) zeros(1,3*length(find(A{1,2}(tr,1400:1800) == 3))) zeros(1,4*length(find(A{1,2}(tr,1400:1800) == 4))) zeros(1,5*length(find(A{1,2}(tr,1400:1800) == 5)))];
                
            end
            subplot(2,1,r)
            [n,x]=hist(isi,[0:400]);
            bar(x,100*n/sum(n))
            maxv(r) = max(100*n/sum(n));
            xlim([-1 200])
            title(rules{r})
        end
        xlabel('ISI (ms)')
        ylabel('% isi')
        try ;for sb = 1 : 2;subplot(2,1,sb); ylim([0 max(maxv)]); end ; end
        
    case 'psthRuleMI'
        
        load('PSTHrulesMI.mat')
        
        for al = 1 : 3
            subplot(1,3,al)
            plot(time{al},mi{al}(end,:))
            hold on
            plot(time{al},prctile(mi{al}(1:end-1,:),99.95,1),'--')
            if al == 1
                xlabel(['Time from sample onset (ms)']);
                xlim([-500 1300])
            elseif al == 2
                xlabel(['Time from match onset (ms)']);
                xlim([-800 200])
            elseif al ==3
                xlabel(['Time from match onset (ms)']);
                xlim([-800 100])
                
            end
            ylabel('MI')
        end
        
end
if obj.data.Index(ind,3) ~= 99
    try
        info = sprintf('%s area %s \n',obj.data.setNames{ind},areasN{obj.data.Index(ind,3)});
    catch
        info = sprintf('%s \n',obj.data.setNames{ind});
    end
else
    info = sprintf('%s area ?? \n',obj.data.setNames{ind});
end
fprintf('%s \n',info)
h = gcf;
set(h,'Name',info)
