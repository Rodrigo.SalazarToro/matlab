function bayes_beta_pair_LOOCV_all(varargin)

%runs for single pairs
%calculates performance at each location for the 3 identities

%computes LOOCV
Args = struct('fix',0,'sample',0,'mi_cutoff',0,'cor_range',[],'test',0,'perm',0);
Args.flags = {'test','perm','fix','sample'};
Args = getOptArgs(varargin,Args);



nb = 1;


%code from Rodrigo to identify the beta increase pairs with significant
%mutual information during the delay (300 - 800ms).
cd /media/raid/data/monkey

monkeydir = pwd;

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number','type',{'betapos34' [1 3]});

[mugram,time,f,ntrials,sig,idePairs,presample] = mkAveMugram(obj,ind,'type','IdePerLoc','delayRange',[1.3 1.8],'addNameFile','betapos34');

dirs = obj.data.setNames(ind(idePairs))';

pairs = obj.data.Index(ind(idePairs),10:11)';

number_beta_pairs = size(pairs,2);
all_performance = [];
all_trials = [];
all_pvals = [];
for nbp = 1 : number_beta_pairs
    
    ml = 0;
    if ~isempty(strfind(dirs{nbp},'betty'))
        ml = 1;
    end
    
    pair_info = pairs(:,nbp)';
    
    if ml
        cd([dirs{nbp} filesep 'session01'])
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        ses = 1;
    else
        cd([dirs{nbp} filesep 'session02'])
        mt = mtstrial('auto','redosetNames');
        if mt.data.Index(1,1) == 1;
            ses = 2;
        else
            ses = 3;
        end
    end
    
    performance = zeros(3,1);
    ntrials = zeros(3,1);
    pvals = zeros(3,1);
    all_mutual_info = cell(1,3);
    for all_locations = 1 : 3
        
        cd([dirs{nbp} filesep 'session0' num2str(ses)])
        if ml
            [~,~,group_pair_list,~] = sorted_groups('ml');
            stimulus = stimulus_list('ml','locbyide',all_locations);
            if Args.fix
                out = corr_phase_list('ml','fix','locations',all_locations);
            elseif Args.sample
                out = corr_phase_list('ml','sample','locations',all_locations);
            else
                out = corr_phase_list('ml','locations',all_locations);
            end
            [~,pair_list] = intersect(group_pair_list,sort(pair_info),'rows');
            num_pairs = 1;
        else
            [~,~,group_pair_list,~] = sorted_groups;
            stimulus = stimulus_list('locbyide',all_locations);
            if Args.fix
                out = corr_phase_list('fix','locations',all_locations);
            elseif Args.sample
                out = corr_phase_list('sample','locations',all_locations);
            else
                out = corr_phase_list('locations',all_locations);
            end
            [~,pair_list] = intersect(group_pair_list,pair_info,'rows');
            num_pairs = 1;
        end
        
        if Args.perm
            stimulus=stimulus(randperm(size(stimulus,2)));
        end
        
        cd([pwd filesep 'lfp' filesep 'lfp2']);
        
        trial_correlograms = out.trial_correlograms;
        trial_corrcoefs = out.trial_corrcoefs;
        trial_phases = out.trial_phases;
        if ml
            num_trials = size(trial_correlograms{pair_list},2);
        else
            num_trials = size(trial_correlograms{pair_list},2);
        end
        
        if nb
            m = 0;
            for leave = 1 : num_trials
                
                thcorrcoefs = trial_corrcoefs{pair_list};
                corrcoefs = trial_corrcoefs{pair_list};
                phases = trial_phases{pair_list};

                thcorrcoefs(thcorrcoefs<=.6) = 0;
                thcorrcoefs(find(thcorrcoefs)) = 1;
                
                thphases(phases<=.6) = 0;
                thphases(find(phases)) = 1;
                
                thcorrcoefs = sum(thcorrcoefs')';
                
                thleave_corrcoefs = thcorrcoefs(leave,:);
                leave_corrcoefs  = corrcoefs(leave,:);
                leave_phases = phases(leave,:);

                thcorrcoefs(leave,:) = [];
                corrcoefs(leave,:) = [];
                phases(leave,:) = [];
                
                
                st = stimulus;
                st(leave) = [];
                
                %     NB = NaiveBayes.fit([corrcoefs phases],st,'Distribution','kernel');
                NB = NaiveBayes.fit([thcorrcoefs],st,'Distribution','mn','Prior','uniform');
                
                %     pre = predict(NB,[leave_corrcoefs leave_phases]);
                pre = predict(NB,[thleave_corrcoefs]);

                clear NB
                
                if pre == stimulus(leave)
                    m = m + 1;
                end
                (m/leave) * 100;
            end
            
            if m ~= 0
                performance(all_locations) = (m/leave) * 100
            else
                performance(all_locations) = 0
            end
            
            
            
            
            
            
        else
            
            
            
            
            if isempty(Args.cor_range)
                cor_range = [-1:.1:1];
            else
                cor_range = Args.cor_range;
            end
            cor_range = single(cor_range);
            c_range = size(cor_range,2);
            
            all_joint_cond = cell(1,num_pairs);
            num_points = 101 * 9;
            m = 0;
            for leave = 1 : num_trials
                %make distributions of conditional probabilities P(r|s)
                mutual_info = zeros(num_pairs,num_points);
                predictions = zeros(num_pairs,num_points);
                probs = zeros(num_pairs,num_points);
                
                pair_counter = 0;
                for p = pair_list
                    pair_counter = pair_counter + 1;
                    
                    joint_cond_dist = cell(3,num_points);
                    joint_cum_dist = cell(1,num_points);
                    
                    if isempty(all_joint_cond{pair_counter})
                        %calculate conditional distributions for all trials
                        for t = 1 : num_trials
                            for tp = 1 : num_points
                                
                                point = single((trial_correlograms{p}{t}(tp)));
                                po = [];
                                if point >= min(cor_range) && point <= max(cor_range)
                                    [~,po]=min(abs(cor_range - point));
                                end
                                
                                if isempty(joint_cond_dist{stimulus(t),tp})
                                    joint_cond_dist{stimulus(t),tp} = zeros(1,c_range);
                                end
                                
                                if ~isempty(po)
                                    joint_cond_dist{stimulus(t),tp}(po) = joint_cond_dist{stimulus(t),tp}(po) + 1;
                                end
                            end
                        end
                        all_joint_cond{pair_counter} = joint_cond_dist;
                    end
                    
                    %gets joint conditional distribution with all trials
                    joint_cond_dist = all_joint_cond{pair_counter};
                    
                    %remove information from the trial to be left out
                    for tp = 1 : num_points
                        point = single((trial_correlograms{p}{leave}(tp)));
                        po = [];
                        if point >= min(cor_range) && point <= max(cor_range)
                            [~,po]=min(abs(cor_range - point));
                        end
                        joint_cond_dist{stimulus(leave),tp}(po) = joint_cond_dist{stimulus(leave),tp}(po) - 1; %subtract 1 from the conditional distribution
                    end
                    
                    
                    for stims = 1 : max(stimulus) % 3 or 9
                        for tp = 1 : num_points
                            if isempty(joint_cum_dist{1,tp})
                                joint_cum_dist{1,tp} = zeros(1,c_range);
                            end
                            
                            joint_cond_dist{stims,tp{pair_list}} = joint_cond_dist{stims,tp} ./ nansum(joint_cond_dist{stims,tp});
                            
                            %probability of the sample (not counting the withheld trial
                            stm = stimulus;stm(leave) = [];
                            prob_sample = size(find(stm == stims),2) / (size(stm,2));
                            prob_sample = 1/3;
                            
                            joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
                        end
                    end
                    
                    stim_probabilities = zeros(3,num_points);
                    
                    for ntime = 1 : num_points
                        
                        point = single((trial_correlograms{p}{leave}(ntime)));
                        po = [];
                        if point >= min(cor_range) && point <= max(cor_range)
                            [~,po]=min(abs(cor_range - point));
                        end
                        
                        cum_prob = joint_cum_dist{1,ntime}(po);
                        for stims = 1 : max(stimulus)
                            cond_prob = joint_cond_dist{stims,ntime}(po);
                            
                            %probability of the sample (not counting the withheld trial
                            stm = stimulus;stm(leave) = [];
                            prob_sample = size(find(stm == stims),2) / (size(stm,2));
                            prob_sample = 1/3;
                            
                            %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                            if ~isempty(cum_prob) && ~isempty(cond_prob)
                                stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                            end
                        end
                        %get rid of nans
                        stim_probabilities(isnan(stim_probabilities)) = 0;
                        
                        if ~isempty(cum_prob)
                            if cum_prob ~= 0
                                post_probs = stim_probabilities(:,ntime)';
                                mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                                mutual_info(pair_counter,ntime) = mi;
                                
                                if mi >= Args.mi_cutoff
                                    [time_prob,pred] = max(post_probs);
                                    predictions(pair_counter,ntime) = pred;
                                    probs(pair_counter,ntime) = time_prob;
                                end
                            end
                        end
                    end
                end
                
                b = zeros(1,max(stimulus));
                for xs = 1 : max(stimulus);
                    b(xs) = sum(mutual_info(predictions == xs));
                end
                [~,pre] = max(b);
                
                leave_predict(leave) = pre;
                
                if pre == stimulus(leave)
                    m = m + 1;
                    if isempty(all_mutual_info{stimulus(leave)})
                        all_mutual_info{stimulus(leave)} = zeros(101,9);
                    else
                        all_mutual_info{stimulus(leave)} = all_mutual_info{stimulus(leave)} + reshape(mutual_info,101,9);
                    end
                end
                
                leave
                (m/leave) * 100
            end
            m
            if m ~= 0
                performance(all_locations) = (m/leave) * 100;
            else
                performance(all_locations) = 0
            end
            mutual_info_loc{all_locations} = all_mutual_info;
        end
        ntrials(all_locations) = num_trials;
        
        %calculate p-value (see Quiroga_2009, pg 176 Box 2)
        n = num_trials;
        K = 3;
        p = 1 / K;
        c_trials = round((n * (performance(all_locations) / 100))); %number of correct trials
        pvalue = 0;
        for k = c_trials : n
            P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
            pvalue = pvalue + P;
        end
        
        pvals(all_locations) = pvalue;
        
        
        
    end
    all_performance = [all_performance performance];
    all_trials = [all_trials ntrials];
    all_pvals = [all_pvals pvals];
    
    cd(monkeydir)
    if Args.fix
        save all_performance_fix all_performance all_trials all_pvals
    elseif Args.sample
        save all_performance_sample all_performance all_trials all_pvals
    else
        save all_performance all_performance all_trials all_pvals
    end
end

