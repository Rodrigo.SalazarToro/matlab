function csc = write_nlynx2streamer_files(csc_channel,start_time,stop_time)

fid = fopen(csc_channel);

first_record = 16384; %16kb, or 16 * 1024
recordsize = 1044;

fseek(fid,first_record,'bof'); %skip 16kb header
firstts = fread(fid,1,'uint64');

start_record = floor((start_time - firstts) / 16000) ; %subtract 1000 records from the beginning
stop_record = ceil((stop_time - firstts) / 16000) + 100; %add 100 records to end


% fseek(fid,first_record + (recordsize * (start_record)),'bof'); %records are 1044 bytes

%find start, make sure time stamp is before the requested start (jump back 100 records)
ts = firstts;
counter = 0;
while ts < start_time
    counter = counter + 1;
    fseek(fid,first_record + (recordsize*(counter*100)),'bof'); %records are 1044 bytes
    tstamp = fread(fid,1,'uint64');
    if ~isempty(tstamp) && tstamp ~= 0 
        ts = tstamp;
    else
        ts = inf;
    end
end


fseek(fid,first_record + (recordsize*((counter-1)*100)),'bof'); %skip to start

%get records up to stop
counter = 0;
currentts = 0;
csc.Samples = [];
while stop_time > currentts
    counter = counter + 1;
    currenttstamp = fread(fid,1,'uint64');
    if ~isempty(currenttstamp) 
        currentts = currenttstamp;
    else
        currentts = 0;
    end
    csc.TimeStamps(counter) = currentts;
    csc.ChannelNumbers(counter) = fread(fid,1,'uint32');
    csc.SampleFrequencies(counter) = fread(fid,1,'uint32'); %samples per second (512 samples per record)
    csc.NumberValidSamples(counter) = fread(fid,1,'uint32');
    %skip to record
    %     fseek(fid,12,'cof');
    
    csc.Samples = [csc.Samples fread(fid,512,'int16')'];
end

fclose(fid);






