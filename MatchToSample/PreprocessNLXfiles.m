function PreprocessNLXfiles()

%Used to convert .ncs files to streamer files
%run at session level

display=0;

%delete copy.ncs files
delete *_copy.ncs


%find CSC files.
dirlist = nptDir('*.ncs');
%sort them into numerical order
CSC =struct2cell(dirlist);
%[~,ind]=sortn(CSC(1,:)); %sort into numerical order
[a,ind]=sortn(CSC(1,:)); %sort into numerical order
CSC = CSC(:,ind);
channels = find(cell2mat(CSC(3,:))>17000);

%     descriptor_file = nptDir('*_descriptor.txt');
%     sessionname = descriptor_file.name(1:end-15);
%     dinfo = ReadDescriptor(descriptor_file.name);
%     if ~isempty(find(channels-dinfo.channel))
%         error('Descriptor File is Not Correct!')
%     end


sessionname = 'test';


%read events file
FieldSelectionArray=[1 1 1 1 1 ]; %get everything
ExtractHeaderValue = 1;%get header also
ExtractionMode = 1;%all times
ExtractionModeArray = []; %not needed since we are getting all times
[events.TimeStamp, events.EventIDs, events.Nttls, events.Extras, events.EventStrings, events.NlxHeader] ...
    = Nlx2MatEV( 'events.nev', FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );

%save the events as a matlab structure
save events events

%MonkeyLogic uses 9 and 18 as triggers.
startTriggerNum=9;
stopTriggerNum=18;
%search for trial triggers
t = GetNLXTrialTimes(events,startTriggerNum,stopTriggerNum);

for tt = 1 : size(t.start,2)
    fprintf('\nTrial #%i\nChannels ',tt)
    
    save timing t channels CSC
    filename = [sessionname '01.' num2strpad(tt,4)];
    %if trial already exists then skip it
    if isempty(nptDir(filename))
        for cc=1:length(channels)
            fprintf('%i ',cc)
            
            %[csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt)-512/.032556 t.stop(tt)]);
            
            [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt) t.stop(tt)]);
            
            if cc==1
                data=NaN(length(channels),numel(csc.Samples));
                
            end
            csc.TimeStamps;
            data(cc,:) = reshape(csc.Samples,1,numel(csc.Samples));
        end
        tic
        if find(isnan(data))
            warning('preallocation is incorrect')
        end
        toc
        
        %Now we need to remove data points that are in the first record
        %before
        %start trigger.
        data(:,1:(round((t.start(tt)-csc.TimeStamps(1))*.032556)))=[];

        %And then also remove data that were in the last record after
        %the trigger.
        data(:,end - (csc.NumberValidSamples(end) - (round((t.stop(tt)-csc.TimeStamps(end))*(csc.SampleFrequencies(end)/1000000))+2)):end)=[];
        status=nptWriteStreamerFile(filename,32556,data,channels');

        if display
            close all
            plot(data(1,:))
            [dataRead,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(filename);
            if ~isempty(find(data-dataRead))
                error('data written is not the same as data read')
            end
            hold on
            plot(dataRead(1,:),'r')
            dataRead=[];
        end
        data=[];
    end
end





