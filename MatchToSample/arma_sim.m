function arma_sim(varargin)  

%simulates an arma(4,2) model
%ar and ma can be matrices
Args = struct('length',2000,'burn',1000,'ar',[],'ma',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

total_length = Args.burn + Args.length + 1; %add 1 to account for difference

ar = Args.ar;
num_ar = size(Args.ar,2);

ma = Args.ma;
num_ma = size(Args.ma,2)

y = zeros(2,total_length);
wt = zeros(2,total_length);

prob_vector = [];
prob_vector(1:1000) = 1; %keep seperate during the burn in period
prob_vector(1001:1300) = .8;
prob_vector(1301:1600) = .6;
prob_vector(1601:1800) = .4;
prob_vector(1801:2500) = .1;
prob_vector(2501:3001) = .1;

%prob_vector(1:3001) = 1;



%burn
for x = 10 : total_length
    
    for e = 1 : size(wt,1);
        r = rand;
        if r < prob_vector(x) || e == 1
            new_error = randn; %normally distributed random number
            wt(e,x) = new_error;
        else
            wt(e,x) = new_error; %use error for first channel
            %wt(e,x) =  wt(1,(x-10));
        end
    end
    
    
    
    %ar process
    for ar_p = 1 : num_ar
        y(:,x) = y(:,x) + ar(:,ar_p) .* y(:,x-ar_p);
    end
    
    %ma process
    for ma_p = 1 : num_ma
        y(:,x) = y(:,x) + ma(:,ma_p) .* wt(:,x-ma_p);
    end
    
    y(:,x) = y(:,x) + wt(:,x);
    
    
end

%delete burn in
y(:,(1:Args.burn)) = [];

%undifference
lag_series = nan(size(y));
lag_series(:,(2:end)) = y(:,(1:(end-1)));
y = y + lag_series;
y(:,1) = [];

subplot(2,1,1)
plot(y(1,:));hold on;plot(y(2,:),'r')


%run cross correlation
steps = floor((size(y,2)-50)/50);
start = 1:50:steps*50+1;
endd = start+50-1;


r = GrayXxcorr(y(1,:),y(2,:),200,50,0,start,endd);
subplot(2,1,2)
plot(r)



% r = GrayXxcorr(y(1,:),y(2,:),200,50,50,start,endd);
% 
% r = r';
% [m mm] = max(abs(r));
% 
% subplot(2,1,2)
% plot(r(mm))
















