function [groups chpairs gpairs channels good_pair_list] = bmf_groups(varargin)

%run at session level
%outputs a list of pairs that aren't rejected using reject_filter

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;
cd([sesdir filesep 'lfp'])

load rejectedTrials.mat
cd(sesdir)

N = NeuronalHist('bmf');
groups = N.gridPos;
channels = 1 : N.chnumb;
noisy = rejectCH;
groups(noisy) = []; %get bad groups
channels(noisy) = [];

ngroups = size(groups,2);
p = 0;
for g1 = 1 : ngroups
    for g2 = (g1+1) : ngroups
        p = p + 1;
        gpairs(p,:) = [groups(g1) groups(g2)];
        chpairs(p,:) = [channels(g1) channels(g2)];
    end
end


%determine which pairs are good
good_pair_list = [];
counter = 0;
for c = 1 : size(N.gridPos,2)
    for cc = c + 1 : size(N.gridPos,2)
        counter = counter + 1;
        if ~isempty(intersect(channels,c)) && ~isempty(intersect(channels,cc))
            good_pair_list = [good_pair_list counter];
        end
    end
end

cd(sesdir)

