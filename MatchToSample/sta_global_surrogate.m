function sta_global_surrogate(varargin)

%run at session level

Args = struct('ml',0,'rule',1,'iterations',1000);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
sesdir = pwd;

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',Args.rule);
else
    mt = mtstrial('auto','redosetNames');
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',Args.rule);
end
ntrials = size(total_trials,2);

sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

N = NeuronalHist;
[NeuroInfo] = NeuronalChAssign();
if Args.ml
    sg = sorted_groups;
    [~,good_channels] = intersect(N.gridPos,sg);
    nchannels = size(good_channels,2);
else
    sg = sorted_groups;
    [~,good_channels] = intersect(NeuroInfo.groups,sg);
    nchannels = size(good_channels,2);
end

cd(['lfp' filesep 'lfp2'])

%get list of trials
lfp2_trials = nptdir('*lfp2*');
%make a giant cell of trials
all_trials = cell(1,ntrials);
for allt = 1 : ntrials
    load(lfp2_trials(total_trials(allt)).name);
    tstart = sample_on(total_trials(allt)) - 500;
    tstop = match(total_trials(allt));
    all_trials{allt} = data(:,(tstart:tstop));
end

increments = [10 25 50 100 150 250 500 1000 2000 4000 8000 16000 32000];
num_increments = size(increments,2);
global_sta = zeros(num_increments,Args.iterations);
pwd

for ni = 1 : num_increments
    fprintf(1,['increment ' num2str(ni) '\n'])
    for x = 1 : Args.iterations
        
        nperms = ceil(increments(ni) / 10);
        pt = [];
        for npp = 1 : nperms
            %select random list of trials for each iteration
            perm_trials = randperm(ntrials); %randomize trials
            pt = [pt perm_trials(1 : 10)];
        end
        
        d_inc = zeros(1,increments(ni));
        for inc = 1 : increments(ni)
            %go through each trial
            trial = pt(inc);

            %randomly select a channel from the trial
            perm_ch = randperm(nchannels);
            pc = good_channels(perm_ch(1));
            
            trial_data = all_trials{trial};
            
            %randomly slect a data point from the random trial/ch
            perm_data = randperm(size(trial_data,2));
            perm_data_point = trial_data(perm_data(1));
            d_inc(inc) = perm_data_point;
        end
       global_sta(ni,x) = mean(d_inc);
    end
end

write_info = writeinfo(dbstack);
save global_sta global_sta increments write_info

cd(sesdir)