function groups = check_groups(varargin)

%used to determine what groups have been sorted
%run at session level

Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

groups = [];
g = nptDir('group*');

num_groups = size(g,1);

for x = 1: num_groups
    groups(x) = str2double(g(x).name(6:end));
end

if ~Args.ml
    Nch = NeuronalChAssign;
    [i ii] = intersect(Nch.groups,groups);
    
    groups = ii;
end
