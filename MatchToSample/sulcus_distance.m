function dist = sulcus_distance(varargin)

%run at session level
%outputs the distance of each electrode from it's respective sulcus in mm.
%currently only works for clark

Args = struct('plot',0);
Args.flags = {'plot'};
Args = getOptArgs(varargin,Args);

if Args.plot
    figure
end

% IPS coordinates for clark with respect to orig_grid
ips_sulcus = [0 7;1 7;2 7;3 7;4 7.2;5 6.9;6 7.2;7 7;8 7.1;9 7.3;10 7.3;11 7.5;12 6.8;13 6.4;14 6;15 5.8;15.6 5.7];

% PS coordinates for clark with respect to orig_grid
ps_sulcus = [0 8.5;1 8;2 6.65;3 5.6;4 4.9;6 3.4;7 3;9 2.45;13.1 1.9];

%this is the grid that all other grids are aligned to.
orig_grid = [1:8;9:16;17:24;25:32;33:40;41:48;49:56;57:64];

%PP GRID 1
pp_grid{1} = rot90(orig_grid,3);
pp_originxy{1} = [6.5,3]; %this is on a 16X16 grid
pp_originch{1} = 64;
pp_refpointxy{1} = [13,6.75]; %8
pp_angle{1} = atan((pp_originxy{1}(2)-pp_refpointxy{1}(2)) / (pp_originxy{1}(1)-pp_refpointxy{1}(1)));
% pp_angle{1} = 25*pi/180; %visualy adjusted angle

%PP GRID 2
pp_grid{2} = rot90(orig_grid,3);
pp_originxy{2} = [3,6]; %this is on a 16X16 grid
pp_originch{2} = 64;
pp_refpointxy{2} = [9.75,3]; %8
pp_angle{2} = atan((pp_originxy{2}(2)-pp_refpointxy{2}(2)) / (pp_originxy{2}(1)-pp_refpointxy{2}(1)));

%PP GRID 3
pp_grid{3} = rot90(orig_grid,2);
pp_originxy{3} = [3,5.6]; %this is on a 16X16 grid
pp_originch{3} = 8;
pp_refpointxy{3} = [10,3]; %1
pp_angle{3} = atan((pp_originxy{3}(2)-pp_refpointxy{3}(2)) / (pp_originxy{3}(1)-pp_refpointxy{3}(1)));

%PP GRID 4
pp_grid{4} = orig_grid;
pp_originxy{4} = [7.1,2.3]; %this is on a 16X16 grid
pp_originch{4} = 57;
pp_refpointxy{4} = [12.75,5.5]; %64
pp_angle{4} = atan((pp_originxy{4}(2)-pp_refpointxy{4}(2)) / (pp_originxy{4}(1)-pp_refpointxy{4}(1)));

%PP GRID 5
pp_grid{5} = rot90(orig_grid,2);
pp_originxy{5} = [6.7,2.25]; %this is on a 16X16 grid
pp_originch{5} = 8;
pp_refpointxy{5} = [13,6.4]; %1
pp_angle{5} = atan((pp_originxy{5}(2)-pp_refpointxy{5}(2)) / (pp_originxy{5}(1)-pp_refpointxy{5}(1)));

%PP GRID 6
pp_grid{6} = orig_grid;
pp_originxy{6} = [3.1,4.9]; %this is on a 16X16 grid
pp_originch{6} = 57;
pp_refpointxy{6} = [10,2.5]; %64
pp_angle{6} = atan((pp_originxy{6}(2)-pp_refpointxy{6}(2)) / (pp_originxy{6}(1)-pp_refpointxy{6}(1)));

%PF GRID 1
pf_grid{1} = rot90(orig_grid,1);
pf_originxy{1} = [3,5];
pf_originch{1} = 1;
pf_refpointxy{1} = [10,3]; %57
pf_angle{1} = atan((pf_originxy{1}(2)-pf_refpointxy{1}(2)) / (pf_originxy{1}(1)-pf_refpointxy{1}(1)));

%PF GRID 2
pf_grid{2} = rot90(orig_grid,2);
pf_originxy{2} = [3,4];
pf_originch{2} = 8;
pf_refpointxy{2} = [11,3]; %1
pf_angle{2} = atan((pf_originxy{2}(2)-pf_refpointxy{2}(2)) / (pf_originxy{2}(1)-pf_refpointxy{2}(1)));

%PF GRID 3
pf_grid{3} = rot90(orig_grid,3);
pf_originxy{3} = [3,5];
pf_originch{3} = 64;
pf_refpointxy{3} = [11,3]; %8
pf_angle{3} = atan((pf_originxy{3}(2)-pf_refpointxy{3}(2)) / (pf_originxy{3}(1)-pf_refpointxy{3}(1)));

%PF GRID 4
pf_grid{4} = rot90(orig_grid,3);
pf_originxy{4} = [7.1,2.3];
pf_originch{4} = 64;
pf_refpointxy{4} = [12.75,5.5]; %8
pf_angle{4} = atan((pf_originxy{4}(2)-pf_refpointxy{4}(2)) / (pf_originxy{4}(1)-pf_refpointxy{4}(1)));


%make XY coordinates for original grid
xy = [];
ch = 0;
for x = 1 : 8
    for y = [8 7 6 5 4 3 2 1]
        ch = ch + 1;
        xy(ch,:) = [x y];
    end
end


%Get grid postions. Note: grid postions are dependent on the grid used.
N = NeuronalHist;
numbch = size(N.gridPos,2);
%get information about which grid was used. See above for the grid information.
[ppgrid pfgrid] = get_grid_info;

%determine which channels are parietal and which channels are frontal
ppch = zeros(1,numbch);
ppch(strfind(N.cortex,'P')) = 1;

%make ips sulcus for PPC
pp_sulcus(:,1) = [0:.1:15.6]';
pp_sulcus(:,2) = interp1q(ips_sulcus(:,1),ips_sulcus(:,2),pp_sulcus(:,1));

%make ps sulcus for PFC
pf_sulcus(:,1) = [0:.1:13.1]';
pf_sulcus(:,2) = interp1q(ps_sulcus(:,1),ps_sulcus(:,2),pf_sulcus(:,1));

%loop through all the grid postions
for ch = 1 : numbch
    
    if ppch(ch)
        %determine which grid to use and flip it up/down so that the origin is now row1,column1
        grid = pp_grid{ppgrid};
        angle_correct = pp_angle{ppgrid}; %correct for angle
        orig = pp_originxy{ppgrid}; %add origin offset, channel 1 is 8,1
        sulcus = pp_sulcus;
        pl = 1;
    else
        grid = pf_grid{pfgrid};
        angle_correct = pf_angle{pfgrid};
        orig = pf_originxy{pfgrid};
        sulcus = pf_sulcus;
        pl = 2;
    end
    xy1 = xy(grid(N.gridPos(ch)),:); %get xy coordinates
    
    [x1 y1] = grid_transform(angle_correct,xy1(1),xy1(2));
    
    if isnan(x1)
        x1 = orig(1);
        y1 = orig(2);
    else
        x1 = x1 + orig(1);
        y1 = y1 + orig(2);
    end
    
    %calculate the distance to each sulcus xy point
    [dist(ch) i] = min( sqrt( (sulcus(:,1) - x1).^2 + (sulcus(:,2) - y1).^2 ));
    
    if Args.plot
        if pl == 1
            subplot(2,1,1)
            rectangle('curvature',[1,1],'Position',[0,0,16,16],'LineWidth',5)
            hold on
            plot(sulcus(:,1),sulcus(:,2))
            hold on
            scatter(sulcus(:,1),sulcus(:,2))
            hold on
            scatter(x1,y1)
            hold on
            plot([x1 sulcus(i,1)],[y1 sulcus(i,2)],'r')
        else
            subplot(2,1,2)
            rectangle('curvature',[1,1],'Position',[0,0,16,16],'LineWidth',5)
            hold on
            plot(sulcus(:,1),sulcus(:,2))
            hold on
            scatter(sulcus(:,1),sulcus(:,2))
            hold on
            scatter(x1,y1)
            hold on
            plot([x1 sulcus(i,1)],[y1 sulcus(i,2)],'r')
        end
    end
end


dist = dist * .86; %multiply by the spacing of the grid, result is mm


