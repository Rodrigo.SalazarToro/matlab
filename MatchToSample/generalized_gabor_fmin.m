function [cf, estimates, sse, p_angle_deg, peak, xc, peak_lag, chi2] = generalized_gabor_fmin(varargin)

Args = struct('correlogram',[],'peak',[],'lag',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);


%frequency
correlogram = Args.correlogram;
NFFT = 2^nextpow2(size(correlogram,2));
f = fft(correlogram,NFFT);

power = abs(f(1:NFFT/2+1)).^2;
freq = 1000/2 * linspace(0,1,NFFT/2+1);


[f ff] = max(power);
start_freq = freq(ff) / 1000;

%amplitude
%[a i] = max(Args.correlogram);
[a i] = max(abs(correlogram));

%if peak is not specified then use maximum
if isempty(Args.peak)
    max_lag = correlogram(i);
else
    max_lag = Args.peak;
end


%if max lag is negative then flip correlogram
if max_lag < 0
    correlogram =correlogram * -1;
end

%phase shift
if isempty(Args.lag)
    ps = i - (size(correlogram,2) / 2);
else
    ps = Args.lag;
end


%initial values
arg_in(1) = a; %amplitude
arg_in(2) = 30; %decay
arg_in(3) = start_freq; %frequency
arg_in(4) = ps; %phase shift
arg_in(5) = 0; %offset
arg_in(6) = 2; %this is the value for a traditional gabor (exponent)
arg_in(7) = 0; %this is the value for a traditional gabor (B)
arg_in(8) = 1; %sig2

options=optimset('MaxFunEvals',100000); %default is 200 * number_of_variables (200*8)


[estimates,sse] = fminsearch(@generalized_gabor,arg_in, options, correlogram); 



%plot output
A = estimates(1);
decay = estimates(2);
frequency = estimates(3);
phase_shift = estimates(4);
offset = estimates(5);
exponent = estimates(6);
B = estimates(7);
sig2 = estimates(8);

t = floor(size(Args.correlogram,2) / 2);
time = [(-1*t):t];
for iter = 1 : size(time,2)
    x = time(iter);
    %generalized gabor function see konig_94
    cf(iter) = A * exp( -1*(abs(x-phase_shift)/decay) ^ exponent) * cos(2*pi*frequency*(x-phase_shift)) + offset + B * exp(-x/sig2)^2;
end

if max_lag < 0
    correlogram =correlogram * -1;
    cf = cf * - 1;
end

delt_t = estimates(4) * (10^-3); %delt_t is in seconds (ms * 10^-3)
% %         if delt_t < 0
% %             delt_t = abs(delt_t + 1/frequency);
% %         end


%this is the phase angle calculated as if the correlogram is positive.
%since negative correlograms must be flipped this needs to be accounted for
p_angle_deg = 360 * (frequency * 1000) * delt_t;

%determin if correlogram is negative
if max_lag < 0
    if delt_t > 0 %phase angle is greater than 180
        p_angle_deg = (360 * (frequency * 1000) * delt_t) - 180;
    else %phase angle is greater than 90 but less than 180
        p_angle_deg = 180 - abs((360 * (frequency * 1000) * delt_t));
    end 
end

%get peak information
[~,peak_lag] = min(abs([-50:50] - estimates(4)));
peak = cf(peak_lag);
xc = xcorr(cf,correlogram,0,'coef');

c = correlogram;
chi2 = sum(((abs(c*100)-abs(cf*100)).^2 ./ (sqrt(abs(c*100)))));

        