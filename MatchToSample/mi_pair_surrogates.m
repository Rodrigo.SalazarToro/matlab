function mi_pair_surrogates


load mi_single_pair_permutations

nperms = size(all_perms,2);
npairs = size(all_perms{1},2);

mi_prcts = cell(1,npairs); %one for each location

for p = 1 : npairs
    perm_mi = zeros(nperms,33);
    for np = 1 : nperms
        perm_mi(np,:) = all_perms{np}{p};
    end
    mi_prcts{p} = prctile(perm_mi,95);
end

save mi_single_pair_prctiles mi_prcts