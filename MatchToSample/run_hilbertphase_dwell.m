function run_hilbertphase_dwell(varargin)

%run at session level
%determines the start time, the length and phase of stable phase locking periods

Args = struct('ml',0,'redo',0,'plot',0);
Args.flags = {'ml','redo','plot'};
Args = getOptArgs(varargin,Args);

figure

sesdir = pwd;
if Args.ml
    c = mtscpp2('auto','ml');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    [alltr trialorder] = get_trial_list('ml');
else
    c = mtscpp2('auto');
    [~,pairs] = get(c,'Number'); %use all pairs
    mt = mtstrial('auto','redosetNames');
    
    [alltr trialorder] = get_trial_list;
end

pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);
cd([sesdir filesep 'lfp' filesep 'lfp2'])

for p = 11 %: npairs
    entfile = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2) '.mat'];
    if ~exist(entfile,'file') || Args.redo
        load(['relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)])
        for alltypes = 17% : 19
            tr = alltr{alltypes};
            ntrials = size(tr,2);
        end
        all_start_times = cell(1,nfreq);
        all_durations = cell(1,nfreq);
        all_phases = cell(1,nfreq);
        all_dwells = cell(1,nfreq);
        counter = 0;
        for nf = 6%1 : nfreq
            all_ds = [];
            all_dd = [];
            all_d = [];
            all_p = [];
            all_ddiff = [];
            for t = tr
                
                            
%                                                                                                  load(['clark06042802_bandpass_18_22.' num2strpad(t,4) '.mat'])
                
                counter = counter + 1;
                fprintf(1,['Pair ' num2str(p) ' of ' num2str(npairs) ', Trials to go = ' num2str(ntrials-counter) '\n']);
               
                und = unwrapped_diffs(nf,1:1800,t);
                d = (relative_phases(nf,1:1800,t) .* (2*pi))  .* (180/pi);
                
                timecounter = 0;
                dur = 0;
                ndwells = 0;
                dwell_durs = [];
                dwell_starts = [];
                dd = nan(1,size(und,2));
                dwells = [];
                dwell_phases = [];
                dwell_diffs = [];
                
                for tp = 2 : size(und,2)
                    if dur == 0
                        timecounter = timecounter + 1;
                    end
                    if abs(und(tp) - und(timecounter)) <= .01 %abs((und(tp) - und(timecounter)) / (tp - timecounter)) <=.001
                        if dur ~= 0
                            dur = dur + 1;
                            if dur == 50
                                ndwells = ndwells + 1;
                                dwell_starts(ndwells) = timecounter;
                            end
                        else
                            dur = dur + 1;
                        end
                    else
                        if dur ~= 0
                            if dur >= 50
                                dd(dwell_starts(ndwells) : tp) = und(dwell_starts(ndwells) : tp);
                                dwells = [dwells dwell_starts(ndwells) : tp];
                                dwell_durs(ndwells) = dur;
                                dwell_phases = circ_mean(((d(dwell_starts(ndwells) : tp)) .*(pi/180))'); %* (2*pi))');
                                dwell_diffs = mean(und(dwell_starts(ndwells) : tp));
                            end
                            dur = 0;
                            timecounter = tp - 1;
                        end
                    end
                end
%                 subplot(1,2,1)
%                                 plot(und);
                                hold on
                                plot(dd,'r')
                                hold on
%                                 plot(data(1,:)./500,'g')
%                                 hold on
%                                 plot(data(2,:)./500,'k')
% axis([1 1800 -3 3])
% % % % % % % % % % % % % % % % % % % % % % % %                                 hold on
% % % % % % % % % % % % % % % % % % % % % % % %                                 subplot(1,2,2)
% % % % % % % % % % % % % % % % % % % % % % % %                                 di = diff(und);
% % % % % % % % % % % % % % % % % % % % % % % %                                 ddi = diff(diff(und));
% % % % % % % % % % % % % % % % % % % % % % % %                                 scatter3(und(1400:end-2),di(1400:end-1),ddi(1400:end))
% % % % % % % % % % % % % % % % % % % % % % % %                                 hold on
%                                 
%                 pause
%            cla
                               
                                
                all_ds = [all_ds dwell_starts];
                all_dd = [all_dd dwell_durs];
                all_d = [all_d dwells];
                all_p = [all_p dwell_phases];
                all_ddiff = [all_ddiff dwell_diffs];
            end
            
            
            all_start_times{nf} = all_ds;
            all_durations{nf} = all_dd;
            all_dwells{nf} = all_d;
            all_phases{nf} = all_p;
            
            
            figure
            subplot(4,1,1)
            hist(all_ds,[1:50:1800])
            
            subplot(4,1,2)
            hist(all_d,[1:50:1800])
            
            subplot(4,1,3)
            hist(all_p,[-pi:pi/10:pi])
            
            subplot(4,1,4)
            hist(all_dd,100)
            
            figure;
            hist(all_ddiff,100)
            
        end
        %             hilbertdwell{alltypes} = hentropy;
        %             mean_phases{alltypes} = mphase;
        %
    end
    %         groups = pair_list(p,:);
    %         channel_pairs = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    %         save(channel_pairs,'hilbertentropy','mean_phases','all_freq','steps','window','trialorder','groups')
end

cd(sesdir)















