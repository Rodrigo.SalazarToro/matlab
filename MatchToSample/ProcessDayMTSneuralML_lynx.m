%run in day directory
diary([pwd filesep 'diary.txt']);

sessions = nptDir('session*'); 

for s = 1 : length(sessions)
    
    cd(sessions(s).name)
    status(s) = removeUEImissTrials('eyeML');
    cd ..
end
 

if ~isempty(find(status == 1))
    ProcessDay('lowpass','highpass','highpasshigh',7000,'UEI')
    %ProcessDay('lowpass','highpass','UEI')
    delete processedday.txt
    for s = 1 : length(sessions)
        
        cd(sessions(s).name)
        delete processedsession.txt
        cd ..
    end
    mycomp = computer;
    
    if strcmp(mycomp,'PCWIN')
        status1 = MoveProcessedFiles(sessions,length(sessions));
    else
        mname = which('moveprocessedfiles.sh');
        [status1,w] = system(mname);
    end
    ProcessDay('extraction','threshold',4,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','no'});

    if strcmp(mycomp,'PCWIN')
        status1 = MoveProcessedFiles(sessions,length(sessions));
    end
    % display w so we can see what happened
    % fprintf('%s\n',w);
    
    % sessions = nptDir('session*'); sesSize = length(sessions);MoveProcessedFiles(sessions,sesSize)
    ProcessDay(nptdata,'nptSessionCmd','eyemvt(''auto'',''SacThresh'',60,''save'',''NoSites'');'); % mtstrial(''auto'',''save'');');
    mts =ProcessDay(mtstrial,'NoSites','auto','save','ML','RTfromML');
    % InspectGUI(mts,'ObjPos','percentCR','Hist')
%     for s = 1 : length(sessions)
%         
%         cd(sessions(s).name)
%         compareEYE
%         [crossings,times,dif] = compareBHVcodetimeVStriggers('save','plot');
%         cd ..
%     end
%     compareBHV2MTS
else
    fprintf('Warning: mismatch of trials and files')
end
diary off;

% mtsday = ProcessDay(mtstrial,'auto');
% InspectGUI(mtsday,'ObjPos','percentCR')