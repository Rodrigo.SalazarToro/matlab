function sim_epochCorrelograms(varargin)

%computes 4 average correlograms for each channel pair across all trials at
%each epoch
%computes the surrogate thresholds for the average correlograms

Args = struct('ml',0,'chnumb',[],'monkey',[],'correlograms',0,'surrogate',0);
Args.flags = {};
Args = getOptArgs(varargin,Args);


if ~Args.ml
    %create trial object
    mt=mtstrial('auto');
    all_trials = mtsgetTrials(mt,'BehResp',1,'stable');
    num_ide = size(all_trials,2); %only need the number of trials for the session
else
    mt = mtstrial('auto','ML','RTfromML');
    type = mt.data.Index(:,1);
    %get only the specified trial indices (correct and stable)
    all_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML');
    type = mt.data.Index(:,1);
    
    ide = intersect(all_trials,find(type == 1));
    num_ide = size(ide,2);
    loc = intersect(all_trials,find(type == 2));
    num_loc = size(loc,2);
end
cue_on = floor(mt.data.CueOnset);
cue_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);
number_pairs = nchoosek(Args.chnumb,2);
cd('lfp2')
%get index of trials
lfpdata = nptDir(['sim_' Args.monkey '*']);

if Args.ml
    ide_loc = [1 2];
    rule = [{'identity' 'location'}];
else
    ide_loc = [1];
    rule = [{}];
end

if Args.correlograms
    all_correlograms = cell(2,number_pairs);
    if ~isempty(all_trials)
        for x = all_trials %all correct stable trials
            load ([lfpdata(x).name],'sim_trials');
            ccc = 0;
            for c1 = 1:Args.chnumb
                epochs = zeros(4,101);
                for c2 = (c1 + 1) : Args.chnumb
                    ccc = ccc + 1;
                    
                    [epochs(1,:) lags] = xcorr(sim_trials(c1,((cue_on(x)-400):cue_on(x))),sim_trials(c2,((cue_on(x)-400):cue_on(x))),50,'coef');
                    [epochs(2,:) lags] = xcorr(sim_trials(c1,(cue_off(x)-400:cue_off(x))),sim_trials(c2,(cue_off(x)-400:cue_off(x))),50,'coef');
                    [epochs(3,:) lags] = xcorr(sim_trials(c1,(cue_off(x):(cue_off(x)+400))),sim_trials(c2,(cue_off(x):(cue_off(x)+400))),50,'coef');
                    [epochs(4,:) lags] = xcorr(sim_trials(c1,((match(x)-400):(match(x)))),sim_trials(c2,((match(x)-400):(match(x)))),50,'coef');
                    
                    if  Args.ml
                        if type(x) == 1
                            if isempty(all_correlograms{1,ccc})
                                all_correlograms{1,ccc} =  epochs;
                            else
                                all_correlograms{1,ccc} = all_correlograms{1,ccc} + epochs;
                            end
                        elseif type(x) == 2
                            if isempty(all_correlograms{2,ccc})
                                all_correlograms{2,ccc} =  epochs;
                            else
                                all_correlograms{2,ccc} = all_correlograms{2,ccc} + epochs;
                            end
                        end
                    else
                        if isempty(all_correlograms{1,ccc})
                            all_correlograms{1,ccc} =  epochs;
                        else
                            all_correlograms{1,ccc} = all_correlograms{1,ccc} + epochs;
                        end
                    end
                end
            end
        end
        
        for ttt = ide_loc
            for www = 1 : size(all_correlograms,2)
                %Average correlograms by the number of trials
                if ttt == 1;
                    e = all_correlograms{ttt,www} / num_ide;
                    all_correlograms{ttt,www} = e;
                else
                    e = all_correlograms{ttt,www} / num_loc;
                    all_correlograms{ttt,www} = e;
                end
                
                for ep = 1:size(e,1)
                    [cor ph] = find_correlogram_peak('correlogram',e(ep,:));
                    avg_phase_epochs{ep,www} = ph;
                    avg_corrcoef_epochs{ep,www} = cor;
                end
            end
            if Args.ml
                cd(rule{ttt});
                avg_correlograms_epochs = all_correlograms(ttt,:);
                save sim_avg_correlograms_epochs avg_correlograms_epochs avg_phase_epochs avg_corrcoef_epochs
                cd ..
            else
                avg_correlograms_epochs = all_correlograms(ttt,:);
                save sim_avg_correlograms_epochs avg_correlograms_epochs avg_phase_epochs avg_corrcoef_epochs
            end
        end
    end
    fprintf(1,'Epoch Correlgrams Done \n')
end

%make surrogates for epoch correlograms
if Args.surrogate

    iterations = 1000;

    if Args.ml
        ide_loc = [1 2];
        rules = [{'identity' 'location'}];
    else
        ide_loc = [1];
        rules = [{}];
    end
    
    for rule = ide_loc
        if Args.ml
            if rule == 1
                trials = ide;
            elseif rule == 2
                trials = loc;
            end
        else
            trials = all_trials;
        end
        
        if Args.ml
            cd(rules{rule})
        end
        
        if isempty(nptdir('epoch_surrogates.mat'))
            epoch1 = zeros(number_pairs,iterations);
            epoch2 = zeros(number_pairs,iterations);
            epoch3 = zeros(number_pairs,iterations);
            epoch4 = zeros(number_pairs,iterations);
        else
            load epoch_surrogates
            
            if ~exist('epoch1')
                !rm epoch_surrogates.mat
                epoch1 = zeros(number_pairs,iterations);
                epoch2 = zeros(number_pairs,iterations);
                epoch3 = zeros(number_pairs,iterations);
                epoch4 = zeros(number_pairs,iterations);
            end
            
        end
        
        if Args.ml
            cd ..
        end
        
        num_trials = size(trials,2);
        if num_trials > 0 %if there are no trials then do not run
            
            p = 0;
            %make pair combinations
            for cc1 = 1 : Args.chnumb
                for cc2 = (cc1 + 1) : Args.chnumb
                    p = p + 1;
                    pair_comb(p,:) = [cc1 cc2];
                end
                
            end
            
            all_iterations1 = zeros(iterations,num_trials);
            all_iterations2 = zeros(iterations,num_trials);
            
            %calculate iterations
            for it = 1 : iterations
                t = 0;
                while(t == 0)
                    t1 = randperm(num_trials);
                    t2 = randperm(num_trials);
                    for check = 1: num_trials
                        if t1(check) == t2(check)
                            t = 0;
                            break
                        else
                            t = 1;
                        end
                    end
                end
                all_iterations1(it,:) = t1;
                all_iterations2(it,:) = t2;
            end
            
            e_surrogates1 = zeros(num_trials,1);
            e_surrogates2 = zeros(num_trials,1);
            e_surrogates3 = zeros(num_trials,1);
            e_surrogates4 = zeros(num_trials,1);
            
            for pair = 1 : number_pairs
                cc1 = pair_comb(pair,1);
                cc2 = pair_comb(pair,2);
                if Args.ml
                    fprintf(1,[pwd '  ' rules{rule} '  pair  ' num2str(pair) '\n'])
                else
                    fprintf(1,[pwd '  pair  ' num2str(pair) '\n'])
                    
                end
                if isempty(find(epoch1(pair,:)))
                    for iter = 1 : iterations
                        if rem(iter,25) ==0;fprintf(1,'\n');end
                        fprintf(1,[num2str(iter) ' '])
                        t1 = all_iterations1(iter,:);
                        t2 = all_iterations2(iter,:);
                        
                        for permut = 1 : num_trials
                            tt1 = trials(1,t1(1,permut));
                            tt2 = trials(1,t2(1,permut));
                            
                            nd1 = load ([lfpdata(tt1).name],'sim_trials');
                            normdata1 = nd1.sim_trials;
                            
                            nd2 = load ([lfpdata(tt2).name],'sim_trials');
                            normdata2 = nd2.sim_trials;
                            
                            e_surrogates1(permut,1) = xcorr(normdata1(cc1,((cue_on(tt1)-400):cue_on(tt1))),normdata2(cc2,((cue_on(tt2)-400):cue_on(tt2))),0,'coef');
                            e_surrogates2(permut,1) = xcorr(normdata1(cc1,(cue_off(tt1)-400:cue_off(tt1))),normdata2(cc2,(cue_off(tt2)-400:cue_off(tt2))),0,'coef');
                            e_surrogates3(permut,1) = xcorr(normdata1(cc1,(cue_off(tt1):(cue_off(tt1)+400))),normdata2(cc2,(cue_off(tt2):(cue_off(tt2)+400))),0,'coef');
                            e_surrogates4(permut,1) = xcorr(normdata1(cc1,((match(tt1)-400):(match(tt1)))),normdata2(cc2,((match(tt2)-400):(match(tt2)))),0,'coef');
                        end
                        
                        %average for for each iteration at 4 epochs
                        epoch1(pair,iter) = mean(e_surrogates1(:,1));
                        epoch2(pair,iter) = mean(e_surrogates2(:,1));
                        epoch3(pair,iter) = mean(e_surrogates3(:,1));
                        epoch4(pair,iter) = mean(e_surrogates4(:,1));
                        % % %                     end
                    end
                    
                else
                    fprintf(1,['pair ' num2str(pair) ' complete\n'])
                end
                if Args.ml
                    if rule == 1;
                        cd('identity')
                        save sim_epoch_surrogates epoch1 epoch2 epoch3 epoch4
                        cd ..
                    elseif rule == 2
                        cd('location')
                        save sim_epoch_surrogates epoch1 epoch2 epoch3 epoch4
                        cd ..
                    end
                else
                    save sim_epoch_surrogates epoch1 epoch2 epoch3 epoch4
                end
            end
        end
        
    end
    
    fprintf(1,'\n')
    fprintf(1,'Epoch Surrogates Done.\n')
end

cd ..







