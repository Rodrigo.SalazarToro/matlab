function bayes_stimulus_classifier_LOOCV_kde(varargin)

%run at session level
%computes mutual information for each pair at the three locations across the 3 identities
Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%find sorted pairs
[~,pair_list] = sorted_groups;
num_pairs = size(pair_list,2);

%load threshold information calculated over all trials
mi_pair_surrogates
load mi_single_pair_prctiles mi_prcts

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
else
    mt = mtstrial('auto','redosetNames');
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

stimulus_lists = cell(1,3);
out_list = cell(1,3);
%get stimulus list and xcorr information
for locations = 1 : 3
    if Args.ml
        out_list{locations} = corr_phase_list('ml','all','locations',locations);
        stimulus_lists{locations} = stimulus_list('ml','locbyide',locations);
        all_locations = stimulus_list('ml','loc');
    else
        out_list{locations} = corr_phase_list('all','locations',locations);
        stimulus_lists{locations} = stimulus_list('locbyide',locations);
        all_locations = stimulus_list('loc');
    end
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);

counter = 0;
m = 0;
ntrials = size(total_trials,2);
pair_prediction = zeros(ntrials,33);
all_predictions = zeros(1,ntrials);
all_stims = zeros(1,ntrials);
performance = zeros(1,ntrials);
no_prediction = zeros(1,ntrials);
for leave = total_trials
    pre = zeros(1,3);
    counter = counter + 1;
    pair_counter = 0;
    
    
    locations = all_locations(counter);
    
    for p = pair_list
        pair_counter = pair_counter + 1;
        
        trial_corrcoefs = out_list{locations}.trial_corrcoefs{pair_counter};
        trial_phases = out_list{locations}.trial_phases{pair_counter};
        trials = out_list{locations}.trials;
        [num_trials, num_points] = size(trial_corrcoefs);
        
        stimulus = stimulus_lists{locations};
        
        joint_cond_dist = cell(3,num_points);
        %calculate conditional distributions for all trials
        for t = 1 : num_trials
            for tp = 1 : num_points
                if leave ~= trials(t);
                    point = single(abs(trial_corrcoefs(t,tp)));
                    phase_point = single(trial_phases(t,tp));
                    
                    if isempty(joint_cond_dist{stimulus(t),tp})
                        joint_cond_dist{stimulus(t),tp} = [point; phase_point];
                    else
                        joint_cond_dist{stimulus(t),tp} = [joint_cond_dist{stimulus(t),tp} [point; phase_point]];
                    end
                else
                    leave_ide = stimulus(t);
                    cor_range = linspace(0,1,32);
                    ph_range = linspace(-50,50,32);
                    
                    point = single(abs(trial_corrcoefs(t,tp)));
                    [~,leave_cor(tp)]=min(abs(cor_range - point));
                    
                    phase_point = single(trial_phases(t,tp));
                    [~,leave_phase(tp)]=min(abs(ph_range - phase_point));
                end
            end
        end
        
        %normalize conditional distributions and make cumulative distribution
        joint_cum_dist = cell(1,num_points);
        for stims = 1 : 3
            for tp = 1 : num_points
                
                data = joint_cond_dist{stims,tp}';
                
                MIN_XY=[0,-50];
                MAX_XY=[1,50];
                
                ngrid = 32;
                
                [~,density]=kde2d(data,ngrid,MIN_XY,MAX_XY);
                
                density = density ./ sum(nansum(density));
                
                %                 surf(X,Y,density+1,'LineStyle','none'), view([0,60])
                %                 colormap hot, hold on, alpha(.8)
                %                 plot(data(:,1),data(:,2),'w.','MarkerSize',5)
                
                density(density < 0) = 0;
                density(density < .0001) = 0;
                joint_cond_dist{stims,tp} = density;
                
                prob_sample = 1/3;
                
                if isempty(joint_cum_dist{1,tp})
                    joint_cum_dist{1,tp} = zeros(ngrid,ngrid);
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
            end
        end
        
        leave_probs = zeros(3,33);
        posterior_distributions = cell(3,num_points);
        for stims = 1 : 3
            for tp = 1 : num_points
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                posterior_distributions{stims,tp} = (joint_cond_dist{stims,tp} * prob_sample) ./ joint_cum_dist{1,tp};
                posterior_distributions{stims,tp}(isnan(posterior_distributions{stims,tp})) = 0;
                
                leave_probs(stims,tp) = posterior_distributions{stims,tp}(leave_phase(tp),leave_cor(tp));
                
            end
        end
        
        mi = mutual_info_pairs('posterior_distributions',posterior_distributions,'prob_response',joint_cum_dist);
        
        %threshold
        th = mi_prcts{pair_counter};
        mi(mi <= th) = 0;
        leave_probs(:,mi<th) = 0;
        
%         if size(find(mi(25:33)),2) >= 2
            pre = pre + sum(leave_probs(:,(25:33))')
%         end
        
        pi = 0;
        if sum(pre) ~= 0
            [~,pi] = max(pre);
        end
        
        pair_prediction(counter,pair_counter) = pi;
    end
    [~,ii]= max(pre);
    
    if sum(pre) == 0
        no_prediction(counter) = 1;
    end
    
    if ii == leave_ide
        m = m + 1;
        performance(counter) = 1;
    end
    (m/counter) * 100
    
    all_predictions(counter) = ii;
    all_stims(counter) = leave_ide;
end

save bayes_classifer_kde pair_prediction performance all_predictions all_stims no_prediction

cd(sesdir)

