function [session,f,vdz,varargout] = compCorInCortrials(day,npair,groups,varargin)
% to be run at the monkey level



Args = struct('gram',0,'ML',0,'save',0,'plevel',0.01,'minTrials',140,'redo',0,'plot',0);
Args.flags = {'gram','ML','save','redo','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1);

fname = sprintf('cohDiffCorIncorg%04.0fg%04.0f.mat',groups(1),groups(2));
cd(day)

files = {'cohInter.mat' 'cohInterInCor.mat'};
if isempty(nptDir('CorIncorStats')); mkdir CorIncorStats; end
cd CorIncorStats
thefile = nptDir(fname);
cd ..


if isempty(thefile) || Args.redo
    load(files{1});
    ch = Day.comb(npair,:);
    
    nses = size(Day.session,2);
    for fi =1 : 2
        load(files{fi});
        
        for s = 1 : nses
            trials{fi,s} = Day.session(s).trials;
        end
    end
    clear Day
    
    for s = 1 : nses
        if length(trials{1,s}) > Args.minTrials && length(trials{2,s}) > Args.minTrials
            if Args.ML; cd session01/lfp ; modvarargin = [modvarargin 'fromFiles'];else cd(sprintf('session0%d/lfp',s+1)); end
            
            for f = 1 : 2
                [data,lplength] = lfpPcut(trials{f,s},ch,modvarargin{:});
                eval(sprintf('data%d = data;',f))
                clear data
            end
            for p = 1 : 4
                [session(s).dz(:,:,p),vdz,session(s).Adz(:,p),f,~,~,session(s).rdiff(:,p)] = compareCoherence(data1{p},data2{p},params,'diffNtrials','p',Args.plevel,modvarargin{:});
            end
            cd ../..
        else
            session(s).dz = [];
            session(s).Adz = [];
            session(s).rdiff = [];
            f = [];
            vdz = [];
            
        end
    end
    
    if Args.save && exist('session')
        cd CorIncorStats
        save(fname,'session','f','trials','vdz')
        fprintf('saving %s \n',fname)
        cd ..
    end
else
     cd CorIncorStats
    load(fname)
    cd ..
end
cd ..