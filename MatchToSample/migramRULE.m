function varargout = migramRULE(obj,varargin)


Args = struct('save',0,'redo',0,'type','all','surrogate',0,'iterations',1000,'ML',0,'BehResp',1,'theday',[],'matchAlign',0,'minTrials',5,'IncorCorrected',0);
Args.flags = {'save','redo','surrogate','ML','matchAlign','IncorCorrected'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'surrogate'}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );


switch Args.type
    case 'all'
        nstim = 1;
        stims1 = '[1:3]';
        stims2 = '[1:3]';
        
    case 'Nine'
        nstim = 9;
        cueComb = [1 2 3 1 2 3 1 2 3; 1 1 1 2 2 2 3 3 3];% first row == loc
        stims1 = 'cueComb(2,stim)';
        stims2 = 'cueComb(1,stim)';
        
end

sdir = pwd;

[r,ind] = get(obj,'Number','snr',99);
if ~isempty(Args.theday);
    inddays = find(cellfun(@isempty,strfind(obj.data.setNames,Args.theday)) == 0);
    ind = intersect(ind,inddays);
    if Args.surrogate
        se = 1;
    else
        se = length(ind);
    end
else
    
    se = r;
end
count = 1;

for pair = 1 : se
    if isempty(strfind(obj.data.setNames{ind(pair)},'betty'))
        ses = [2 3];
    else
        ses = [1 1];
        Args.ML = true;
        modvarargin = [modvarargin {'ML'}];
    end
    cd(obj.data.setNames{ind(pair)})
    
    ddir = pwd;
    for s = 1 : length(ses); sessions{s} = sprintf('session0%d',ses(s)); existses(s) = ~isempty(nptDir(sessions{s})); end
    
    if sum(existses == 0) ==0
        cd(sessions{1})
        neuroinfo = NeuronalChAssign;
        gr = obj.data.Index(ind(pair),10:11);
        for c = 1 : 2;ch(c) = find(neuroinfo.groups == gr(c)); end
        cd(obj.data.setNames{ind(pair)})
        if Args.matchAlign; align = 'MatchAlign'; else align = []; end
        if Args.surrogate
            matfile = sprintf('migramSurGenInterStimRules%s%s.mat',Args.type,align);
        else
            
            if Args.BehResp == 1
                if Args.IncorCorrected
                    matfile = sprintf('migramg%04.0fg%04.0fStimRules%s%ssameTIncor.mat',gr(1),gr(2),Args.type,align);
                    
                else
                    matfile = sprintf('migramg%04.0fg%04.0fStimRules%s%s.mat',gr(1),gr(2),Args.type,align);
                end
            else
                matfile = sprintf('migramg%04.0fg%04.0fStimRules%sIncor%s.mat',gr(1),gr(2),Args.type,align);
            end
        end
        
        cd grams
        if isempty(nptDir(matfile)) || Args.redo
            trials = cell(2,1);
            mutualgram = cell(nstim,1);
            
            for stim = 1 : nstim;
                for s = 1 : 2
                    if Args.ML
                        modvarargin = cat(2,modvarargin,{'Rule'},{s});
                        
                    end
                    cd(obj.data.setNames{ind(pair)});
                    %                     if Args.ML; s = 1; rules.r = [1 1];else rules = load('rules.mat'); s = find(r == rules.r); end
                    %
                    cd(sessions{s})
                    osessions{s} = pwd;
                    mts{s} = mtstrial('auto','redoSetNames');
                    
                    if Args.BehResp == 1
                        if Args.IncorCorrected
                            ntrialsIncor = length(mtsgetTrials(mts{s},'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'BehResp',0,modvarargin{:}));
                            Tcor = mtsgetTrials(mts{s},'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'stable','BehResp',Args.BehResp,modvarargin{:});
                            Tcor = Tcor(randperm(length(Tcor)));
                            minT = min([ntrialsIncor length(Tcor)]);
                            trials{s,1} = Tcor(1:minT);
                        else
                            trials{s,1} =  mtsgetTrials(mts{s},'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'stable','BehResp',Args.BehResp,modvarargin{:});
                        end
                    else
                        trials{s,1} =  mtsgetTrials(mts{s},'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'BehResp',0,modvarargin{:});
                    end
                    
                end
                
                if length(trials{1}) > Args.minTrials && length(trials{2}) > Args.minTrials
                    %% surrogate
                    if Args.surrogate
                        
                        if isempty(Args.theday);
                            theday = obj.data.Index(ind(pair),2);
                        else
                            theday = find(cellfun(@isempty,strfind(obj.data.setNames,Args.theday)) ==0);
                            theday = unique(obj.data.Index(theday,2));
                            d = 1;
                            while iscell(theday) && d <= length(theday)
                                if ~isempty(theday{d});
                                    clear theday; theday = d;
                                end;
                                d = d +1;
                            end;
                            %                             if d > length(theday)
                            %                                 break
                            %                             end
                        end
                        [~,goodpairs] = get(obj,'Number','snr',99);
                        daypairs = find(obj.data.Index(:,2) == theday);
                        pairss = intersect(daypairs,goodpairs);
                        grPP = unique(obj.data.Index(pairss,10),'rows');
                        grPF = unique(obj.data.Index(pairss,11),'rows');
                        neuroinfo = NeuronalChAssign;
                        chPP = find(ismember(neuroinfo.groups,grPP) == 1);
                        chPF = find(ismember(neuroinfo.groups,grPF) == 1);
                        
                        [rtrials,rses] = randTrials(trials,2);
                        cd(obj.data.setNames{ind(pair)})
                        if isempty(Args.theday);
                            [mt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:},'Rules','fromCell','sessions',osessions,'sesOrder',rses,'surrogate');
                        else
                            
                            [mt,f,time,~,~,~,~] = migram(mts,rtrials,[chPP(1) chPF(1)],modvarargin{:},'Rules','fromCell','sessions',osessions,'sesOrder',rses,'surrogate');
                        end
                        mutualgramt =zeros(Args.iterations,size(mt,1)-2,size(mt,2));
                        mutualgramt(1,:,:) = mt(1:size(mt,1)-2,:); % -2 is to avoid random selection of short trials that decrease the max time in mugram
                        
                        for ii = 2 : Args.iterations
                            
                            clear ch
                            ch(1) = chPP(randi(length(grPP)));
                            ch(2) = chPF(randi(length(grPF)));
                            [rtrials,rses] = randTrials(trials,2);
                            [mutualgramtt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:},'Rules','fromCell','sessions',osessions,'sesOrder',rses,'surrogate');
                            mutualgramt(ii,:,:) = single(mutualgramtt(1:size(mutualgramt,2),:));
                        end
                        time = time(1:size(mt,1)-1);
                        gr = [vecr(grPP) vecr(grPF)];
                        mutualgram{stim} = mutualgramt;
                        
                    else
                        %% data
                        if Args.matchAlign
                            [mutualgram,f,time,C,phi,phistd,Cerr,before] = migram(mts,trials,ch,modvarargin{:},'Rules','fromCell','sessions',osessions);
                            time = time -before/1000;
                        else
                            
                            [mutualgramt,f,time,Ct,phit,phistdt,Cerrt] = migram(mts,trials,ch,modvarargin{:},'Rules','fromCell','sessions',osessions);
                            
                            mutualgram{stim} = mutualgramt;
                            C{stim,:} = Ct;
                            phi{stim,:} = phit;
                            phistd{stim,:} = phistdt;
                            Cerr{stim,:} = Cerrt;
                        end
                    end
                end
            end
            if Args.save
                cd(obj.data.setNames{ind(pair)})
                cd grams
                if Args.surrogate
                    save(matfile,'mutualgram','f','time','trials','grPP','grPF','Args')
                else
                    save(matfile,'mutualgram','f','time','trials','gr','C','phi','phistd','Cerr','Args')
                end
                display(['saving' ' ' matfile])
            end
            
        else
            cd ..
            cd grams
            try
                load(matfile,'mutualgram','f','time')
            catch
                unix(sprintf('rm %s',matfile))
                display('removing corrupted file')
            end
        end
        
    end
    if nargout > 0
        allmi{count} = mutualgram;
        count = count + 1;
    end
end

if nargout > 0
    varargout{1} = allmi;
    varargout{2} = f;
    varargout{3} = time;
end
cd(sdir)
%%
function [rtrials,varargout] = randTrials(trials,nstim,varargin)

stimN = zeros(nstim,1);
rtrials = cell(nstim,1);
alltrials = cat(2,trials{:});

indrand = randperm(length(alltrials));
randtrial= alltrials(indrand);
if nargout > 1
    sesn = [ones(1,length(trials{1})) ones(1,length(trials{2})) + 1];
    randses = sesn(indrand);
    
end
rses = cell(nstim,1);
for stim = 1: nstim;
    stimN(stim) = length(trials{stim});
    if stim == 1
        rtrials{stim} = randtrial(1:stimN(stim));
        if nargout > 1
            rses{stim} = randses(1:stimN(stim));
        end
    else
        rtrials{stim} = randtrial(stimN(stim-1)+ 1 : stimN(stim-1)+stimN(stim));
        if nargout > 1
            rses{stim} = randses(stimN(stim-1) + 1 : stimN(stim-1)+stimN(stim));
        end
    end
end
if nargout > 1; varargout{1} = rses; end
