pervalue = 98;


cd /Volumes/raid/data/monkey

obj = loadObject('cohInterIDEL.mat');
[r,ind] = get(obj,'Number','snr',99);
% [r,ind] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});

clear sur mugram delay 
tlim = 39; % result from teh mugram
for ii = 1 : r
    cd(obj.data.setNames{ind(ii)})
    cd grams
    gr = obj.data.Index(ind(ii),10:11);
    matfile = sprintf('migramg%04.0fg%04.0fRule%d%s.mat',gr(1),gr(2),1,'Loc');
    
    
    load('migramSurGenInterRule1Loc.mat','f','time','mutualgram')
    mutualgram = mutualgram(:,1:tlim,:);
    sur(ii,:,:) = prctile(mutualgram,pervalue,1);
    
    load(matfile,'mutualgram')
    mugram(ii,:,:) = mutualgram(1:tlim,:);
end

delay(1,:,:) = repmat((time(1:tlim) >= 1 & time(1:39) <= 1.8)',1,length(relFreq));

% sig99 = (mugram > sur99);
sig97 = (mugram > sur);
% sig95 = (mugram > sur95);

figure; 
% subplot(3,1,1)
% imagesc(time(1:39),f,(squeeze(sum(sig95,1))./3414)',[0 0.15]); colorbar
% title('p<0.05')
% subplot(3,1,2)
imagesc(time(1:tlim),f,(squeeze(sum(sig97,1))./3414)',[0 0.1]); colorbar
title('p<0.02')
ylim([8 42])
% subplot(3,1,3)
% imagesc(time(1:39),f,(squeeze(sum(sig99,1))./3414)',[0 0.1]); colorbar
% title('p<0.01')
% xlabel('Time [sec]'); ylabel('Frequency [Hz]')
relFreq = find(f>7 & f <42);
sigpair = sum(sum(sig97(:,:,relFreq) .* repmat(delay,[size(sur,1) 1 1]),3),2);

ndbin = sum(time(1:tlim) >= 1 & time(1:tlim) <= 1.8);
sigg = squeeze(sum(sig97,1))./r;
presample = find(time < 0.5);
nsiglim = ndbin*length(relFreq)*mean(mean(sigg(presample,relFreq),2),1); % get the number of sig. bins by chance
thepairs = find(sigpair > ceil(nsig)); length(thepairs)/r
figure
% subplot(1,2,1)
imagesc(time(1:tlim),f,squeeze(prctile(mugram(thepairs,:,:),50,1))',[0.1 0.5]); colorbar
ylim([8 42])
%%


% cd /Volumes/raid/data/monkey
% 
% obj = loadObject('cohInterIDEL.mat');
% [r,ind] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});
% clear sur mugram mugramMatchAlign
% 
% 
% for ii = 1 : r
%     cd(obj.data.setNames{ind(ii)})
%     cd grams
%     gr = obj.data.Index(ind(ii),10:11);
%     matfile = sprintf('migramg%04.0fg%04.0fRule%d%s.mat',gr(1),gr(2),1,'Loc');
%     
%     
%     load('migramSurGenInterRule1Loc.mat','f','time','mutualgram')
%     mutualgram = mutualgram(:,1:tlim,:);
%     sur(ii,:,:) = prctile(mutualgram,pervalue,1);
%     
%     load(matfile,'mutualgram')
%     mugram(ii,:,:) = mutualgram(1:tlim,:);
%     
%     matfile = sprintf('migramg%04.0fg%04.0fRule%d%sMatchAlign.mat',gr(1),gr(2),1,'Loc');
%     match = load(matfile,'mutualgram','time');
%     mugramMatchAlign(ii,:,:) = match.mutualgram(1:40,:);
%     
% end
% 
% delay(1,:,:) = repmat((time(1:tlim) > 1 & time(1:tlim) < 1.8)',1,length(relFreq));
% ndbin = sum(time(1:tlim) >= 1 & time(1:tlim) <= 1.8);
% % sig99 = (mugram > sur99);
% sig97 = (mugram > sur);
% % sig95 = (mugram > sur95);
% %
% % figure;
% % % subplot(3,1,1)
% % % imagesc(time(1:39),f,(squeeze(sum(sig95,1))./3414)',[0 0.15]); colorbar
% % % title('p<0.05')
% % % subplot(3,1,2)
% % imagesc(time(1:39),f,(squeeze(sum(sig97,1))./size(mugram,1))',[0 0.1]); colorbar
% % title('p<0.03')
% % % subplot(3,1,3)
% % imagesc(time(1:39),f,(squeeze(sum(sig99,1))./3414)',[0 0.1]); colorbar
% % title('p<0.01')
% % xlabel('Time [sec]'); ylabel('Frequency [Hz]')
% 
% sigpair = sum(sum(sig97(:,:,relFreq) .* repmat(delay,[size(sur,1) 1 1]),3),2); % get the sig bins duringhte delay
% 
% sigg = squeeze(sum(sig97,1))./r;
% 
% nsiglim = ndbin*length(relFreq)*mean(mean(sigg(presample,relFreq),2),1); % get the number of sig. bins by chance
% thepairs = find(sigpair > ceil(nsiglim)); length(thepairs)/r
% 
% % subplot(1,2,2)
% % imagesc(match.time(1:40),f,squeeze(prctile(mugramMatchAlign(thepairs,:,:),50,1))',[0.1 0.5]); colorbar
% % 
% % figure
% % imagesc(match.time(1:40),f,squeeze(prctile(mugramMatchAlign,50,1))',[0.1
% % 0.5]); colorbar