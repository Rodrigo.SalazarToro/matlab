function bayes_stimulus_classifier3(varargin)

%run at session level
Args = struct('ml',0,'loc',0,'ide',0,'calc_joint',0,'cp_list',0,'new_rand',0);
Args.flags = {'ml','comb','loc','ide','calc_joint','new_rand'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%get stimulus list
if Args.ide
    stimulus = stimulus_list('ml','ide');
elseif Args.loc
    stimulus = stimulus_list('ml','loc');
else
    stimulus = stimulus_list('ml');
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);
cpd = nptDir('corr_phase_delay.mat');
if isempty(cpd)
    cd(sesdir)
    if Args.ml
        corr_phase_list('ml')
    else
        corr_phase_list
    end
    cd([sesdir filesep 'lfp' filesep 'lfp2']);
else
    %load correlation coefficients and phases
    load corr_phase_delay %compute with corr_phase_list.m
end

if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    [num_trials,num_points] = size(trial_corrcoefs{pair_list(1)});
else
    num_pairs = size(trial_corrcoefs,2);
    [num_trials,num_points] = size(trial_corrcoefs{1});
    pair_list = [1 : num_pairs];
end

%percent of trials to withold
per = round(num_trials * .30);
if Args.new_rand
    Args.calc_joint = 1; %must recalculate
    %select trials to withold
    r = randperm(num_trials);
    r = r(1:per); %trials to withold
    trials = 1:num_trials;
    trials(r) = [];
    r = sort(r);
    save train_test trials r
else
    load train_test
end


cor_range = [.7:.1:1];
cor_range = single(cor_range);
c_range = size(cor_range,2);
ph_range = [-50:1:50];
ph_range = single(ph_range);
s_phase = size(ph_range,2);

if Args.calc_joint
    %make distributions of conditional probabilities P(r|s)
    for p = pair_list
        if Args.loc || Args.ide
            joint_cond_dist = cell(3,9);
            joint_cum_dist = cell(1,9);
        else
            joint_cond_dist = cell(9,9);
            joint_cum_dist = cell(1,9);
        end
        t_counter = 0;
        for t = trials %1 : num_trials
            for tp = 1 : num_points
                point = single(abs(trial_corrcoefs{p}(t,tp)));
                
                point = round(point*10)/10;
                
                [~,po] = intersect(cor_range ,point);
                
                phase_point = single(trial_phases{p}(t,tp));
                [~,ph]=min(abs(ph_range - phase_point));
                
                if isempty(joint_cond_dist{stimulus(t),tp})
                    joint_cond_dist{stimulus(t),tp} = zeros(s_phase,c_range);
                end
                if isempty(joint_cum_dist{1,tp})
                    joint_cum_dist{1,tp} = zeros(s_phase,c_range);
                end
                if ~isempty(ph) && ~isempty(po)
                    joint_cond_dist{stimulus(t),tp}(ph,po) = joint_cond_dist{stimulus(t),tp}(ph,po) + 1;
                end
            end
        end
        
        for stims = 1 : max(stimulus) % 3 or 9
            for tp = 1 : num_points
                ajc = joint_cond_dist{stims,tp};
                xs=[];
                ys=[];
                zs=[];
                count = 0;
                for xx = 1 : c_range
                    for yy = 1 : s_phase
                        if ajc(yy,xx) > 0
                            count = count + 1;
                            xs(count,:) = xx;
                            ys(count,:) = yy;
                            zs(count,:) = ajc(yy,xx);
                        end
                    end
                end
                
                if ~isempty(zs);
                    %                     F = TriScatteredInterp(xs,ys,zs,'natural');
                    %
                    %                     ti = 1:1:c_range;
                    %                     tti = 1:1:s_phase;
                    %                     [qx,qy] = meshgrid(ti,tti);
                    %                     qz = F(qx,qy);
                    %
                    %                     jcd = qz ./ (sum(nansum(qz)));
                    %                     jcd(isnan(jcd)) = 0;%get rid of nan's
                    %
                    jcd = [];
                    
                    if isempty(jcd)
                        joint_cond_dist{stims,tp} = ajc ./ sum(nansum(ajc));
                    else
                        joint_cond_dist{stims,tp} = jcd;
                    end
                end
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
            end
        end
        
        all_joint_cond{p} = joint_cond_dist;
        all_joint_cum{p} = joint_cum_dist;
    end
    if Args.ide
        save ide_joint_distributions_classify all_joint_cond all_joint_cum
    elseif Args.loc
        save loc_joint_distributions_classify all_joint_cond all_joint_cum
    else
        save pair_joint_distributions_classify all_joint_cond all_joint_cum
    end
else
    if Args.ide
        load ide_joint_distributions_classify
    elseif Args.loc
        load loc_joint_distributions_classify
    else
        load pair_joint_distributions_classify
    end
end

m = 0;
for numt = r%1 : num_trials
    pair_counter = 0;
    mutual_info = zeros(size(pair_list,2),9);
    predictions = zeros(size(pair_list,2),9);
    for nump = pair_list
        pair_counter = pair_counter + 1;
        if Args.loc || Args.ide
            stim_probabilities = zeros(3,9);
        else
            stim_probabilities = zeros(9,9);
        end
        
        for ntime = 1 : num_points
            point = single(abs(trial_corrcoefs{nump}(numt,ntime)));
            point = round(point*10)/10;
            [~,po] = intersect(cor_range,point);
            
            phase_point = single(trial_phases{nump}(numt,ntime));
            [~,ph]=min(abs(ph_range - phase_point));
            
            cum_prob = all_joint_cum{nump}{1,ntime}(ph,po);
            for stims = 1 : max(stimulus)
                cond_prob = all_joint_cond{nump}{stims,ntime}(ph,po);
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                end
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                if ~isempty(cum_prob) && ~isempty(cond_prob)
                    stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                end
            end
            %get rid of nans
            stim_probabilities(isnan(stim_probabilities)) = 0;
            
            if ~isempty(cum_prob)
                post_probs = stim_probabilities(:,ntime)';
                mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                mutual_info(pair_counter,ntime) = mi;
                
                if mi > .02
                    [~,predict] = max(post_probs);
                    predictions(pair_counter,ntime) = predict;
                end
            end
        end
    end
    
    %determine best match
    b = zeros(1,max(stimulus));
    for xs = 1 : max(stimulus);
         b(xs) = size(find(predictions  == xs),1);
    end
    [~,p] = max(b);
    
    if p == stimulus(numt)
        m = m + 1;
    end
end

m






