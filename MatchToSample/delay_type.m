function dtype = delay_type(varargin)

%Gets delay period information
%run at day level
%
%Args.plot: plots histogram of delays for all trials
%
%Delay Types:
%   -standard (800 : 1200)
%   -long     (1000 : 3000)
%   -long_bin (1000 , 2000 , 3000)
%   -variable

Args = struct('plot',0);
Args.flags = {'plot'};
[Args,modvarargin] = getOptArgs(varargin,Args);

daydir = pwd;



cd('session01')
load mtstrial

delay = mtst.data.MatchOnset-mtst.data.CueOffset;

maximum = max(delay);
minimum = min(delay);

if (minimum > 775) && (minimum < 850) && (maximum > 1150) && (maximum < 1250)
    dtype.type = 'standard';
    dtype.max = maximum;
    dtype.min = minimum;
    dtype.delays = delay;
elseif (minimum > 975) && (minimum < 1050) && (maximum > 2975) && (maximum < 3050)
    if isempty(intersect(delay,[1050:1900,2050:2900]))
        dtype.type = 'long_bin';
        dtype.max = maximum;
        dtype.min = minimum;
        dtype.delays = delay;
    else
        dtype.type = 'long';
        dtype.max = maximum;
        dtype.min = minimum;
        dtype.delays = delay;
    end
else
    dtype.type = 'variable';
    dtype.max = maximum;
    dtype.min = minimum;
    dtype.delays = delay;
end

cd(daydir)


if Args.plot
    hist(delay)
end




