function make_snr_spreadsheet

%run at betty level
load all_depths_snr_reconstruction
load betty_reconstruction
snr_depths = cell(size(betty_reconstruction,1),size(betty_reconstruction,2));
betty_reconstruction_snr = betty_reconstruction;
betty_snr = cell(size(betty_reconstruction,1),size(betty_reconstruction,2));
depths = all_depths_snr_reconstruction.depths;
snr = all_depths_snr_reconstruction.snr;

for channels = 1 : 64
    for days = 1 : size(depths,1)
        
        dep = depths(days,channels);
        if dep ~= 0
            
            decdep = dep - floor(dep);
           
            if decdep <=.25
                dep = floor(dep); 
            elseif (decdep > .25) && (decdep <=.5) 
                dep = floor(dep) + .5;
            elseif (decdep > .5) && (decdep <=.75)
                dep = floor(dep) + .5;
            else
                dep = ceil(dep);
            end
            
            d = dep / .5; %channel offset not taken into account
            if d == 0;
                d = 1;
            end
            
            if isempty(snr_depths{channels,d})
                snr_depths{channels,d} = snr(days,channels);
                betty_reconstruction_snr{channels,d} = [betty_reconstruction{channels,d} '  snr:' num2str(snr(days,channels))];
                betty_snr{channels,d} = num2str(snr(days,channels));
            else
                if snr(days,channels) > snr_depths{channels,d}
                    betty_reconstruction_snr{channels,d} = [betty_reconstruction{channels,d} '  snr:' num2str(snr(days,channels))];
                    betty_snr{channels,d} = num2str(snr(days,channels));
                end
            end 
        end
    end
end


save betty_reconstruction_snr betty_reconstruction_snr 
save betty_snr betty_snr









