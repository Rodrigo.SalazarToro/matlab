function [phase,periods,varargout] = mtsStatPSTH(sp,mtst,varargin)



Args = struct('OnsetCut',50,'Nlynx',0);
Args.flags = {'Nlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

% cue ID effect
cues = unique(mtst.data.CueObj);
locs = unique(mtst.data.CueLoc);


periodLabel = {'Fixation' 'Cue' 'Delay' 'Match'};
varargout{1} = periodLabel;

for cueid = 1 : length(cues)
    for cueloc = 1 : length(locs)
        if Args.Nlynx
            indexes{cueid,1} = mtsgetTrials(mtst,'CueObj',cues(cueid),'BehResp',1,'Nocharlie',1,modvarargin{:});
        else
            indexes{cueid,cueloc} = mtsgetTrials(mtst,'CueObj',cues(cueid),'CueLoc',locs(cueloc),'BehResp',1,modvarargin{:});
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%task modulation

ind = mtsgetTrials(mtst,'BehResp',1,modvarargin{:});

[perf,ti,crossperf] = getperformance('getThreshCross',modvarargin{:});



seltrials = {[],[]};
for p = 1 : size(crossperf,2)
    seltrials{1} = [seltrials{1} ind(ind >= crossperf(1,p) & ind <= crossperf(2,p))]; % stable phase
    
end

seltrials{2} = setdiff(ind,seltrials{1}); % phase of transition





for l = 1 : 2 %loop for the two phases
    trials = intersect(ind,seltrials{l});
    if ~isempty(trials)
        for t = 1 : length(trials)
            CueOnset = mtst.data.CueOnset(trials(t));
            CueOffset = mtst.data.CueOffset(trials(t));
            MatchOnset = mtst.data.MatchOnset(trials(t));
            Sac = mtst.data.MatchOnset(trials(t)) + mtst.data.FirstSac(trials(t));
            period = [Args.OnsetCut CueOnset; CueOnset+1 CueOffset; CueOffset+1 MatchOnset; MatchOnset+1 Sac];
            
            for p = 1 : size(period,1)
                nspike = find(sp.data.trial(trials(t)).cluster.spikes >= period(p,1) &  sp.data.trial(trials(t)).cluster.spikes < period(p,2));
                FR{l}(t,p) = length(nspike)/(diff(period(p,:))/1000);
            end
            cueID{l}(t) = find(cues == mtst.data.CueObj(trials(t)));
            cueLoc{l}(t) = find(locs == mtst.data.CueLoc(trials(t)));
            
        end
        
        [ptask,table,stat] = anova1(FR{l},[],'off');% task effect
        
        phase(l).p = ptask;
        
        %if ptask < 0.05
        
        stats = multcompare(stat,'display','off');% differences between phases
        
        hPhase = all(logical(stats(:,3:5) < 0),2) | all(logical(stats(:,3:5) > 0),2); % determines whether the borders of the 95% interval include zero or not
        phase(l).h = hPhase';
        
    else
        phase(l).p = NaN;
        phase(l).h = NaN;
    end
    %end
end

comp = [periodLabel(stats(:,1)); periodLabel(stats(:,2))];
varargout{2} = comp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Cue  and learning effect
if ~isnan(phase(1).p) & ~isnan(phase(2).p)
    indVar{1} = [cueID{1}'; cueID{2}'];
    indVar{2} = [cueLoc{1}'; cueLoc{2}'];
    indVar{3} = [ones(size(FR{1},1),1); ones(size(FR{2},1),1)+1];
    
    for c = 1 : size(periodLabel,2)
        rate = [FR{1}(:,c); FR{2}(:,c)];
        cc = anovan(rate,indVar,'full',3,'','off');
        periods(c).p = cc';
    end
else
    for c = 1 : size(periodLabel,2)
        periods(c).p = NaN;
    end
end
factors = {'Cue identity' 'Cue location' 'Phase' 'Cue identity vs location' 'Cue identity vs phase' 'Cue location vs phase'};

varargout{3} = factors;
% for c = 1 : 6
%     if hPhase(c) == 1
%         pIDloc(:,c) = anovan(FR(:,c),{cueID' cueLoc' learning},'full',3,'','off');
%     else
%         pIDloc(:,c) = nan(3,1);
%     end
%
% end

% lb = {'r','k','b'};
% for c = 1 : 3
%     icue{c} = cueID == cues(c);
%     data = FR(icue{c},1);
%     plot(data,lb{c})
%     hold on
%
% end
%
% lb = {'r--','k--','b--'};
% for c = 1 : 3
%     iloc{c} = cueLoc == locs(c);
%     data = FR(iloc{c},1);
%     plot(data,lb{c})
%     hold on
%
% end


