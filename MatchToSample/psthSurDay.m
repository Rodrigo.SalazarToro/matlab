function psthSurDay(varargin)
% At the days level



Args = struct('rule',1,'session99',0);
Args.flags = {'session99'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sdir = pwd;
baseFile = sprintf('nineStimPSTH%d.mat',Args.rule);
if Args.session99
   [status,res] = unix(sprintf('find . -name ''%s'' -ipath ''*session99*''',baseFile)); 
else
[status,res] = unix(sprintf('find . -name ''%s'' ! -ipath ''*session99*''',baseFile));
end
% endfolder = strfind(res,'cluster0') + 10;
endfolder = strfind(res,'.mat')+4;
bfolder = strfind(res,'./');
nfiles = length(endfolder);

for ff = 1 : nfiles
    cd(res(bfolder(ff):endfolder(ff)-18))
    psthSurrogate('save',modvarargin{:});
    cd(sdir)
end