function [allNegsig,allPossig,allR,varargout] = getPerfXcorrSpike(obj1,ind,varargin)

Args = struct('prctile',[0.05 99.95],'pvalueCoh',[],'plot',0,'inspect',0,'minCoef',0.4,'addName',[],'basedOnRuleSeq',0);
Args.flags = {'plot','inspect','basedOnRuleSeq'};
[Args,~] = getOptArgs(varargin,Args);

n=sum(obj1.data.Index(ind(:),3) ~= obj1.data.Index(ind(:),4));

allNegsig = nan(2,n,4);
allPossig = nan(2,n,4);
allR = nan(2,n,4);
tcomp = [1 1;1 2; 1 3; 2 2];
c=1;

for ii = 1 : length(ind)
    name1 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7));
    name2 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8));
    if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
        cd(obj1.data.setNames{ind(ii)})
        if Args.basedOnRuleSeq
            cd ..
            mts = mtstrial('auto');
            firstRule = mts.data.Index(1,1);
            cd(obj1.data.setNames{ind(ii)})
        end
        for rule =1 : 2
            data = load(sprintf('xccorrg%sg%sRule%d50Tr%sTrialBased.mat',name1,name2,rule,Args.addName));
            conf = nan(2,4);
            for tc = 1 : length(tcomp)
                if isempty(Args.pvalueCoh)
                    conf(:,tc) = prctile(data.allR(tc,1:end-1),[Args.prctile(1) Args.prctile(2)]);
                else
                    try
                    [~,~,conf(1,tc),~] = gevfitSur(data.allR(tc,1:end-1),Args.pvalueCoh/2);
                    [~,~,conf(2,tc),~] = gevfitSur(data.allR(tc,1:end-1),1-Args.pvalueCoh/2);
                    end
                end
                if Args.basedOnRuleSeq
                    if firstRule == rule
                        
                        allNegsig(1,c,tc) = (data.allR(tc,end) < conf(1,tc)) *  (data.allR(tc,end) < -Args.minCoef);
                        allPossig(1,c,tc) = (data.allR(tc,end) > conf(2,tc)) *  (data.allR(tc,end) > Args.minCoef);
                        allR(1,c,tc) = data.allR(tc,end);
                    else
                        allNegsig(2,c,tc) = (data.allR(tc,end) < conf(1,tc)) *  (data.allR(tc,end) < -Args.minCoef);
                        allPossig(2,c,tc) = (data.allR(tc,end) > conf(2,tc)) *  (data.allR(tc,end) > Args.minCoef);
                        allR(2,c,tc) = data.allR(tc,end);
                        
                    end
                else
                    allNegsig(rule,c,tc) = (data.allR(tc,end) < conf(1,tc)) *  (data.allR(tc,end) < -Args.minCoef);
                    allPossig(rule,c,tc) = (data.allR(tc,end) > conf(2,tc)) *  (data.allR(tc,end) > Args.minCoef);
                    allR(rule,c,tc) = data.allR(tc,end);
                end
                
            end
            if Args.inspect
                
                plot(conf','--')
                hold on
                plot(data.allR(:,end))
                plot(squeeze(allNegsig(rule,c,:))+squeeze(allPossig(rule,c,:)),'*')
              
                pause
                clf
            end
        end
        varargout{1}(c) = ind(ii);
        c=c+1;
    end
end
if Args.plot
    figure
    rules = {'IDE' 'LOC'};
    
    signR = cell(2,4);
    epochs = {'presample' 'sample' 'delay1' 'delay2'};
    for sb = 1 : 4;
        for r = 1 : 2
            
            ind2 = find(allNegsig(r,:,sb) + allPossig(r,:,sb) ~=0);
            subplot(4,2,(sb-1)*2+r);hist(allR(r,ind2,sb),[-1:0.05:1]);
            xlim([-1 1])
            ylim([0 20])
            if sb == 1; title(rules{r}); end
%             text(0,1,['n=' num2str(length(ind2)) '; FD=' num2str((Args.prctile(1)*2/100)*size(allPossig,2))])
        end
        title(epochs{sb})
    end
    xlabel('corr coef for LOC')
    subplot(4,2,7)
    xlabel('corr coef for IDE')
    ylabel('# pairs')
    
    
end