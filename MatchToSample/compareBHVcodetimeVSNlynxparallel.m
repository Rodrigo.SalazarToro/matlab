function compareBHVcodetimeVSNlynxparallel(varargin)

%run at day level
Args = struct('sessions',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);
daydir = pwd;
sessions = nptdir('session0*');
for x = Args.sessions
    cd([daydir filesep sessions(x).name]);
    skip = nptDir('skip.txt');
    if isempty(skip) 
        basedir = pwd;
        datafile = nptDir('*.bhv');
        bhv = bhv_read(sprintf('%s%s%s',basedir,filesep,datafile.name));
        
        ntrials = size(bhv.TrialNumber,1);
                
        nlynxdir = nptdir('20*');
        cd(nlynxdir.name)
        
        load timing
        cd ..
        
        for nt = 1 : ntrials
            c = bhv.CodeTimes{nt};
            
            bdiff = c(end) - c(1);
            ndiff = round((t.stop(nt) - t.start(nt)) / 1000);

            alldiffs(nt) = abs(bdiff-ndiff);
            if alldiffs(nt) > 1
               fprintf(1,['problem with bhv code time and nlynx parrallel port times   ' pwd])
               fprintf(1,'\n')
            end
        end
    end
end

cd(daydir)
