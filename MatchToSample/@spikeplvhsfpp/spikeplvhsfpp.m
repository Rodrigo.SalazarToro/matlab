function obj = spikeplvhsfpp(varargin)

%makes object for spike hsfpp conditioned on plv values

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'spikeplvhsfpp';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'sph';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

sesdir = pwd;
%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end

if ~isempty(mtst)
    
    if Args.ml
        N = NeuronalHist('ml');
        %determine which groups have been sorted and which groups are noisy
        [goodgroups,~,pair_list] = sorted_groups('ml');
        noisy = noisy_groups('ml');
    else
        N = NeuronalHist;
        %determine which groups have been sorted and which groups are noisy
        [goodgroups,~,~,pair_list] = sorted_groups;
        goodgroups = 1:size(goodgroups,2);
        noisy = [];
    end
    
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
    hpairs = nptdir('threshspikehilber*');
    npairs = size(hpairs,1);
    % data.Index = zeros(npairs,12);
    if npairs > 0
        nfiles = 0;
        for f = 1 : npairs
            %         if isempty(intersect(pair_list(f,:),noisy)) & ~isempty(intersect(pair_list(f,1),goodgroups)) & ~isempty(intersect(pair_list(f,2),goodgroups))
            nfiles = nfiles + 1;
            %             load(['hilbertentropy' num2strpad(pair_list(f,1),2) num2strpad(pair_list(f,2),2)])
            load(hpairs(f).name)
            
            data.setNames{nfiles,1} = ([lfp2dir filesep hpairs(f).name]);
            
            if Args.ml
                
% %                 g1 = pair_list(nfiles,1);
% %                 g2 = pair_list(nfiles,2);
% %                 %find wich channel number the group corresponds
% %                 %get histology information for both groups
% %                 [~,gp1] = intersect(N.gridPos,g1);
% %                 [~,gp2] = intersect(N.gridPos,g2);
% %                 data.Index(nfiles,3) = N.number(gp1); %this is the number for the specific area
% %                 data.Index(nfiles,4) = N.number(gp2);
            else
                g1 = groups(1);
                g2 = groups(2);
                data.Index(nfiles,3) = N.number(groups(1)); %this is the number for the specific area, SPIKE
                data.Index(nfiles,4) = N.number(groups(2)); %FIELD
            end
            data.Index(nfiles,1) = g1;
            data.Index(nfiles,2) = g2;
            
            %entropy values for each quartile
            data.Index(nfiles,5) = biascorrected_entropy(1); % <25%
            data.Index(nfiles,6) = biascorrected_entropy(2);
            data.Index(nfiles,7) = biascorrected_entropy(3);
            data.Index(nfiles,8) = biascorrected_entropy(4); % >25%
            
            
            %get the 95 prctile
            data.Index(nfiles,9) = pvals{1}(1);
            data.Index(nfiles,10) = pvals{2}(1);
            data.Index(nfiles,11) = pvals{3}(1);
            data.Index(nfiles,12) = pvals{4}(1);
            
            data.Index(nfiles,13) = spike_counts{1};
            data.Index(nfiles,14) = spike_counts{2};
            data.Index(nfiles,15) = spike_counts{3};
            data.Index(nfiles,16) = spike_counts{4};
            
            data.Index(nfiles,17) = otest_allspikes_pval; %determine if all the spikes pass the omnibus test for uniformity
            
            %determine if channels are frontal(0) or parietal(1)
            if strmatch(N.cortex(groups(1)),'P')
                data.Index(nfiles,18) = 1;
            elseif strmatch(N.cortex(groups(1)),'F')
                data.Index(nfiles,18) = 0;
            else
                data.Index(nfiles,18) = -1;
            end
            
            if strmatch(N.cortex(groups(2)),'P')
                data.Index(nfiles,19) = 1;
            elseif strmatch(N.cortex(groups(2)),'F')
                data.Index(nfiles,19) = 0;
            else
                data.Index(nfiles,19) = -1;
            end
            
            
            data.numSets = nfiles;
            n = nptdata(data.numSets,0,pwd);
            d.data = data;
            obj = class(d,Args.classname,n);
            if(Args.SaveLevels)
                fprintf('Saving %s object...\n',Args.classname);
                eval([Args.matvarname ' = obj;']);
                % save object
                eval(['save ' Args.matname ' ' Args.matvarname]);
            end
            %         end
        end
    else
        fprintf('hilbert entropy files missing\n');
        obj = createEmptyObject(Args);
    end
else
    obj = createEmptyObject(Args);
end


function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
