function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'sig',0,'plvquartile',4,'minspikes',0,'minspikenumb',500,'otest',0,'pp',0,'ff',0,'pf',0);
Args.flags = {'Number','ObjectLevel','sig','minspikes','otest','pp','ff','pf'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rt = [1 : size(obj.data.Index,1)]';

    %get only groups that pass the SNR, rejectCH, and POWER criteria
    if Args.sig
        rtemp1 = find(obj.data.Index(:,4+Args.plvquartile) > obj.data.Index(:,8+Args.plvquartile))'; %default is the delay
    else
        rtemp1 = rt;
    end
    
    if Args.minspikes
        rtemp2 = find(obj.data.Index(:,12+Args.plvquartile) >= Args.minspikenumb)';
    else
        rtemp2 = rt;
    end
    
    if Args.otest
        rtemp3 = find(obj.data.Index(:,17) <= .01)';
    else
        rtemp3 = rt;
    end

    if Args.pp
        rtemp4 = find(obj.data.Index(:,18) == 1 & obj.data.Index(:,19) == 1);
    else
        rtemp4 = rt;
    end
    
    if Args.ff
        rtemp5 = find(obj.data.Index(:,18) == 0 & obj.data.Index(:,19) == 0);
    else
        rtemp5 = rt;
    end
    
    if Args.pf
        rtemp6 = [find(obj.data.Index(:,18) == 0 & obj.data.Index(:,19) == 1);find(obj.data.Index(:,18) == 1 & obj.data.Index(:,19) == 0)];
        rtemp6 = sort(rtemp6);
    else
        rtemp6 = rt;
    end
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,rtemp6)))));
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end


