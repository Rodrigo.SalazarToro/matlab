function obj = plot(obj,varargin)

%Arguments
Args = struct('ml',0);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end


%load cohgram info
load(obj.data.setNames{ind})
sday = strfind(obj.data.setNames{ind},'/');
ns = size(sday,2);
day = obj.data.setNames{ind}(sday(ns-4)+1:sday(ns-3)-1);

%load globablsurrogate
load([obj.data.setNames{ind}])

subplot(1,5,1)
rose(cell2mat(allspikes))
title([num2str(groups) ' otest : ' num2str(otest_allspikes_pval) '  nspikes : ' num2str(size(cell2mat(allspikes),2)) '  cmean : ' num2str(round((circ_mean(cell2mat(allspikes)')*(180/pi))))])

for x = 1:4
    subplot(1,5,x+1)
    
    rose(allspikes{x})
    title(['entropy : ' num2str(round(biascorrected_entropy(x)*10000))]);% '    cmean : ' num2str(round(circ_mean(allspikes{x}')*(180/pi)))])
end



