function write_hilbert_trials(varargin)


%run at session level

Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp2.*');
%make lfp2 directory
mkdir('hilbert')

for x = 1 : size(lfpdata,1)
    cd(lfp2dir)
    load(lfpdata(x).name)
    %hilbert transform
    d = [];
    for hx = 1 : size(data,1)
        d(hx,:) = hilbert(data(hx,:)); %%%%%MAKE SURE THE HILBERT TRANSFORM IS WORKING PROPERLY, IT MIGHT BE INVERTING THINGS 
    end
    data = d;
    
    %names the file the same as the previous file with the addition of lfp2
    trial2 = [lfpdata(x).name(1:end-9) '_hilbert.' num2strpad(x,4) '.mat'];
    
    cd([lfp2dir filesep 'hilbert'])
    save(trial2,'data')
end
fprintf(1,'hilbert directory made.\n')
cd(sesdir)
































