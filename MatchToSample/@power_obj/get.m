function [r,varargout] = get(obj,varargin)

%get function for sta
Args = struct('Number',0,'ObjectLevel',0,'ch',[],'frontal',0,'parietal',0);
Args.flags = {'Number','ObjectLevel','frontal','parietal'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;

if Args.Number

    all = [1 : length(obj.data.Index(:,1))];
    
    if ~isempty(Args.ch)
        ind1 = [];
        for nch = Args.ch
            ind1 = [ind1 find(obj.data.Index(:,1) == nch)'];
        end
    else
        ind1 = all;
    end
    
    %INDEX 2: parietal, frontal, or all
    if Args.parietal
        ind2 = find(obj.data.Index(:,2) == 1)';
    elseif Args.frontal
        ind2 = find(obj.data.Index(:,2) == 2)';
    else
        ind2 = all;
    end
    
    varargout{1} = intersect(ind1,ind2);
    r = length(varargout{1});

    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end