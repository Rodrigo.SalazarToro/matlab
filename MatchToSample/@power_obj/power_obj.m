function obj = power_obj(varargin)

%creates power object
%run at session level

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'ide_only',1,'nlynx',0,'bettynlynx',0);
Args.flags = {'Auto','ml','nlynx','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'power_obj';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'power_obj';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end



function obj = createObject(Args,varargin)
sdir = pwd;

%make mts trial object
if Args.ml || Args.nlynx
    if Args.nlynx
        mtst = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
    else
        mtst = mtstrial('auto','ML','RTfromML','redosetNames');
    end
    
else
    mtst = mtstrial('auto','redosetNames');
end


if ~isempty(mtst)
    if Args.ml || Args.nlynx %could have a session without any correct/stable trials (clark 060406 session03)
        if Args.nlynx
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
        else
            if Args.ide_only
                all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML','rule',1);
            else
                all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML');
            end
        end
    else
        if Args.ide_only
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','rule',1);
        else
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
        end
    end
end

%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end

sets = 0;
if ~isempty(mtst) && match2sample && ~isempty(all_trials)
    
    if Args.ml || Args.nlynx || Args.bettynlynx
        daydir = pwd;
        if Args.nlynx
            N = NeuronalHist('bmf');
            sg = bmf_groups;
            
        elseif Args.bettynlynx
            N = NeuronalHist('ml','bettynlynx');
            noisy = noisy_groups;
            sg = sorted_groups('bettynlynx');
            [~,ii] = intersect(sg,noisy);
            sg(ii) = []; %get rid of noisy groups
            
            cd(daydir)
        else
          
            N = NeuronalHist;
            noisy = noisy_groups;
            sg = sorted_groups;
            [~,ii] = intersect(sg,noisy);
            sg(ii) = []; %get rid of noisy groups
            
            cd(daydir)
        end
        
        [~,ii] = intersect(N.gridPos,sg);%get intersection
    else
        Nch = NeuronalChAssign;
        noisy = noisy_groups;
        sg = sorted_groups;
        [~,ii] = intersect(sg,Nch.groups(noisy));
        sg(ii) = []; %get rid of noisy groups
        [~,ii] = intersect(Nch.groups,sg);%get intersection
        N = NeuronalHist;
    end
    
    %get peaks and the mean power for unfiltered signal
    if Args.ml || Args.nlynx
        if Args.nlynx
            [~,p,m,f,am,bm,gm] = power_analysis('ml','nlynx');
        elseif Args.bettynlynx
            [~,p,m,f,am,bm,gm] = power_analysis('ml','bettynlynx');
        else
            [~,p,m,f,am,bm,gm] = power_analysis('ml');
        end
    else
        [~,p,m,f,am,bm,gm] = power_analysis;
    end
    
    numb_ch = size(sg,2);
    
    %run through all pairs
    for x = 1 : numb_ch
        
        %channel number
        if Args.ml || Args.nlynx
            data.Index(x,1) = sg(x);
        else
            data.Index(x,1) = ii(x);
        end
        if ~Args.nlynx
            %PARIETAL == 1
            %FRONTAL == 2
            if strncmpi(N.cortex(ii(x)),'P',1)
                data.Index(x,2) = 1;
            else
                data.Index(x,2) = 2;
            end
        end
        
        %write the histology number
        data.Index(x,3) = N.number(ii(x));
        
        %group number
        data.Index(x,4) = N.gridPos(ii(x));
        
        
        %determine if area is used
        if ~isempty(intersect([1 2 3 4 5 8 9 10 11 12 13 17],N.number(ii(x))))
            data.Index(x,5) = 1;
        else
            data.Index(x,5) = 0;
        end
        
        %mean power
        data.mean_alpha_power(x,:) = am(x,1:6);
        data.mean_beta_power(x,:) = bm(x,1:6);
        data.mean_gamma_power(x,:) = gm(x,1:6);
        
        %frequencies
        data.frequencies(x,:) = f(x,1:6);
        
        %peaks
        data.peaks(x,:) = p(x,1:6);
        
        %set name 1: sta
        data.setNames{x,1} = [pwd];
    end
    data.numSets = x;
    data.index_info = {'channel number', 'frontal(2), parietal(1)', 'histology number', 'group number'};
    %fix
    %sample
    %delay400
    %delay800
    %delay
    %delaymatch
    data.cell_info = {'fix(sample on locked)','sample(sample off locked)','delay400(sample off locked)','delay800(first match locked)','delay(first match locked)','delaymatch(match locked)'};
        
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;       
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end


function obj = createEmptyObject(Args)
data.Index = [];
data.mean_power = {};
data.frequencies = {};
data.peaks = {};
data.setNames = {};

data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
