function obj = plot(obj,varargin)

%used to plot sta objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct'

Args = struct('ml',0,'plotall',0,'increments',[]);
Args.flags = {'ml','plotall'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end

%r = parietal (1)
%b = frontal (2)
c = {'r','b'};
q = [1:Args.increments:650];
if Args.plotall || ~isempty(Args.increments)
    if ~isempty(Args.increments)
        dd = dataindices(q(n) : q(n) + Args.increments);
    else
        dd = dataindices;
    end
    for d = dd
        
        co = obj.data.Index(d,2);
        
        subplot(1,2,1)
        plot(obj.data.frequencies{d},obj.data.mean_power{d},c{co})
        xlim([0 50])
        hold on
        subplot(1,2,2)
        plot(obj.data.frequencies_filtered{d},obj.data.mean_power_filtered{d},c{co})
        xlim([0 50])
        hold on
    end
else
    co = obj.data.Index(ind,2);
    subplot(1,2,1)
    plot(obj.data.frequencies{ind},obj.data.mean_power{ind},c{co})
    xlim([0 50])
    subplot(1,2,2)
    plot(obj.data.frequencies_filtered{ind},obj.data.mean_power_filtered{ind},c{co})
    xlim([0 50])
end
