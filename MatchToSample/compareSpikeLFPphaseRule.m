function compareSpikeLFPphaseRule(grUnit,cluster,varargin)
% to be run in the day folder



Args = struct('ML',0,'redo',0,'chosenFreq',1,'epoch','saccade');
Args.flags = {'ML','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

allphases = cell(2,1);
sdir = pwd;

pathTosave = cell(2,1);
matfile = sprintf('compSpikeLFPphaseRules%sgr%04.0fcluster%s.mat',Args.epoch,grUnit,cluster);
if Args.ML
     for r = 1 : 2
        cd(sdir)
        cd('session01')
        mts = mtstrial('auto','redosetNames');
          trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',r,'ML');
        
        
        cd(sprintf('group%04.0f',grUnit))
        cd(sprintf('cluster%s',cluster))
        cd HBphase
        files = nptDir(sprintf('*_%sHBphase.*',Args.epoch));
        
        pathTosave{r} = pwd;
        for tr = 1 : length(trials)
            load(files(tr).name,'-mat')
            % size of phases iteration x channels x Nspikes x freq
            if ~isnan(phases)
                if isvector(squeeze(phases(1001,:,:,Args.chosenFreq)))
                    allphases{r} = [allphases{r} squeeze(phases(1001,:,:,Args.chosenFreq))'];
                else
                    allphases{r} = [allphases{r} squeeze(phases(1001,:,:,Args.chosenFreq))];
                end
                
            end
            
        end
        
        
    end
    
    
else
    
    for s = 2 : 3
        cd(sdir)
        cd(['session0' num2str(s)])
        mts = mtstrial('auto','redosetNames');
          trials = mtsgetTrials(mts,'stable','BehResp',1);
         [~,~,unctrials] = checkFirstSac;
         trials = setdiff(trials,unctrials);
        
        cd(sprintf('group%04.0f',grUnit))
        cd(sprintf('cluster%s',cluster))
        cd HBphase
        files = nptDir(sprintf('*_%sHBphase.*',Args.epoch));
        
        pathTosave{s-1} = pwd;
        for tr = 1 : length(trials)
            load(files(tr).name,'-mat')
            % size of phases iteration x channels x Nspikes x freq
            if ~isnan(phases)
                if isvector(squeeze(phases(1001,:,:,Args.chosenFreq)))
                    allphases{s-1} = [allphases{s-1} squeeze(phases(1001,:,:,Args.chosenFreq))'];
                else
                    allphases{s-1} = [allphases{s-1} squeeze(phases(1001,:,:,Args.chosenFreq))];
                end
                
            end
            
        end
        
        
    end
end


for ch = 1 : size(allphases{1},1)
    [pval(ch), k, K] = circ_kuipertest(allphases{1}(ch,:), allphases{2}(ch,:));
end

for ss = 1 : 2; nspikes(ss) = length(allphases{ss});end

for ss = 1 : 2
    cd(pathTosave{ss})
    save(matfile,'pval','k','K','nspikes')
    display([pwd '/' matfile])
end


cd(sdir)
