function pair = which_pair(ch1,ch2,all_pairs)

%find which pair number based on channels

[i pair] = intersect(all_pairs,[ch1 ch2],'rows');