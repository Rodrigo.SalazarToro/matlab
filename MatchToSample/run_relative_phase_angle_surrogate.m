function run_relative_phase_angle_surrogate(varargin)

%run at session level
%computes relative phase angle tass_1998 and lavenquen, surrogates
Args = struct('ml',0,'redo',0,'nperms',100);
Args.flags = {'ml','redo'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.ml
    c = mtscpp2('auto','ml');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
    
    %get only the specified trial indices (correct and stable)
    alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY
    alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2); %LOCATION
    alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
    [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55)); %get rid of bad fix trials
    alltr{3}(rej) = [];
    alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
    
    %get 9 stims
    counter = 4;
    for objs = 1:3
        for locs = 1:3
            counter = counter + 1;
            alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs,'iCueLoc',locs);
        end
    end
    
    %3 ides
    for objs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs);
    end
    
    %3 locs
    for locs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',locs);
    end
    
else
    c = mtscpp2('auto');
    mt = mtstrial('auto','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

[~,pairs] = get(c,'ml','Number'); %use all pairs

cd([sesdir filesep 'lfp' filesep 'lfp2'])
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp2.*');

pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);

for types = 1:19
    tr = alltr{types};
    ntrials = size(tr,2);
    for p = 1 : npairs
        relative_phases = single(zeros(nfreq,5000,ntrials));
        relative_phases_sacc = single(zeros(nfreq,1000,ntrials));
        for perm = 1 : Args.nperms
            r1 = randperm(ntrials);
            r2 = randperm(ntrials);
            
            for t = 1 : ntrials
                t1 = tr(r1(t)); %trial 1
                t2 = tr(r2(t)); %trial 2
                
                for nf = 1 : nfreq
                    
                    bpdata1 = [lfpdata(t1).name(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata(t1).name((end-8):end)];
                    load(bpdata1,'data','datasacc')
                    data1 = data;
                    datasacc1 = datasacc;
                    
                    bpdata2 = [lfpdata(t2).name(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata(t2).name((end-8):end)];
                    load(bpdata2,'data','datasacc')
                    data2 = data;
                    datasacc2 = datasacc;
                    
                    if Args.ml
                        [~,ch(1)] = intersect(N.gridPos,pair_list(p,1));
                        [~,ch(2)] = intersect(N.gridPos,pair_list(p,2));
                    else
                        ch(1) = pair_list(p,1);
                        ch(2) = pair_list(p,2);
                    end
                    
                    d1 = unwrap(data1(ch(1),:)) ./ (2*pi);
                    d2 = unwrap(data2(ch(2),:)) ./ (2*pi);
                    
                    d1sacc = unwrap(datasacc1(ch(1),:)) ./ (2*pi);
                    d2sacc = unwrap(datasacc2(ch(2),:)) ./ (2*pi);
                    
                    %cyclic relative phase
                    d = mod(d1 - d2,1);
                    dsacc = mod(d1sacc - d2sacc,1);
                    
                    
                    
                    
                    
                    
                     for nf = 1 : nfreq
                d = relative_phases(nf,:,t);
                dsacc = relative_phases_sacc(nf,:,t);
                
                %run sliding window for sample locked
                c = 0;
                max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                for w = steps
                    c = c+1;
                    
                    h = hist(d(w-((window/2)-1):w+(window/2)),bins) ./ 100;%calculate response distribution
                    mp(c) = circ_mean((d(w-((window/2)-1):w+(window/2)) * (2*pi))');
                    
                    re = -1 * nansum(h.*log2(h)); %response entropy (aka shannon entropy) see panzeri_2007
                    resp_ent(c) = (max_ent - re) / max_ent; %normalize so that response is between 0 and 1 with 1 = perfect synchrony (tass_1998)
                    
                    if w <= 900
                        hsacc = hist(dsacc(w-((window/2)-1):w+(window/2)),bins) ./ 100;
                        mpsacc(c) = circ_mean((dsacc(w-((window/2)-1):w+(window/2)) * (2*pi))');
                        
                        resacc = -1 * nansum(hsacc.*log2(hsacc));
                        resp_entsacc(c) = (max_ent - resacc) / max_ent;
                    end
                end
                hentropy(nf,:,counter) = single(resp_ent);
                mphase(nf,:,counter) = single(mp);
                
                hentropysacc(nf,:,counter) = single(resp_entsacc);
                mphasesacc(nf,:,counter) = single(mpsacc);
            end
        end
        hilbertentropy{alltypes} = hentropy;
        mean_phases{alltypes} = mphase;
                    
                    
                    
                    
                    
                    if size(d,2) >= 5000
                        relative_phases(nf,:,t) = single(d(1:5000));
                    else
                        relative_phases(nf,:,t) = single(padarray(d,[0 5000-size(d,2)],'post'));
                    end
                    relative_phases_sacc(nf,:,t) = single(dsacc); %MAYBE CALCULATE THE ENTROPY AT THE SAME TIME
                end
            end
            channel_pairs = [ 'relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
            save(channel_pairs,'relative_phases','relative_phases_sacc','all_freq')
        end
    end
    cd(sesdir)
end

