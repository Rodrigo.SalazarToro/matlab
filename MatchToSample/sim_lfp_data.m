function sim_lfp_data(varargin)

cd('/media/MONTY/data/clark/060428/session02')

%run at session level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
N = NeuronalHist;
chnumb = N.chnumb;

cd(['lfp' filesep 'lfp2'])
lfp2dir = pwd;

%concatenate first n trials
trials = nptDir('*_lfp2*');
ntrials = size(trials,1);
for n = 1 : ntrials
    ntrials-n
    load(trials(n).name)
    sim_trials = [];
    for ch = 1 : chnumb
        all_trials =[0 normdata(ch,:)]'; %add a zero because first entry in csv file gets cut off
        
        csvwrite('all_lfp_trials.csv',all_trials,1,0)
        %run R batch
        !R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/lfp_sim_all_trials.R
        
        i = importdata('sim.csv')
        %standardize data
        data = (i.data - mean(i.data)) / std(i.data);
        sim_trials(ch,:) = i.data;
        
    end
    sim_t = ['sim_' trials(n).name];
    save(sim_t,'sim_trials')
end





