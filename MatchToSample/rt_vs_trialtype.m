function rt_vs_trialtype


cd('/Volumes/raid/data/monkey/betty')
load switchdays

b1rtcorrect = [];
b1rtincorrect = [];
b1rtincorrectnon = [];

b2rtcorrect = [];
b2rtincorrect = [];
b2rtincorrectnon = [];
mdir = pwd;
for x = 1:20
    
    cd([mdir filesep days{x} filesep 'session01']);
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    trials = mtsgetTrials(mt,'stable','ML');
    
    bhvfile = nptDir('*.bhv');
    b = bhv_read(bhvfile.name);
    ntrials = size(b.TrialNumber,1);
    
    tcounter = 0;
    for t = 1 : ntrials
        
        correctloc = 0;
        correctide = 0;
        if (b.TrialError(t) == 0) || (b.TrialError(t) == 6)
            tcounter = tcounter + 1;
            if ~isempty(intersect(trials,tcounter))
                bnumber = b.BlockNumber(t);
                
                sfcom = strfind(b.TaskObject{b.ConditionNumber(t),2},',');
                sfpar1 = strfind(b.TaskObject{b.ConditionNumber(t),2},'(');
                sfpar2 = strfind(b.TaskObject{b.ConditionNumber(t),2},')');
                
                sampide = b.TaskObject{b.ConditionNumber(t),2}(sfpar1+1:sfcom(1)-1);
                samploc1 = str2double(b.TaskObject{b.ConditionNumber(t),2}(sfcom(1)+1:sfcom(2)-1));
                samploc2 = str2double(b.TaskObject{b.ConditionNumber(t),2}(sfcom(2)+1:sfpar2-1));
                
                
                
                dfcom = strfind(b.TaskObject{b.ConditionNumber(t),4},',');
                dfpar1 = strfind(b.TaskObject{b.ConditionNumber(t),4},'(');
                dfpar2 = strfind(b.TaskObject{b.ConditionNumber(t),4},')');
                
                dampide = b.TaskObject{b.ConditionNumber(t),4}(dfpar1+1:dfcom(1)-1);
                damploc1 = str2double(b.TaskObject{b.ConditionNumber(t),4}(dfcom(1)+1:dfcom(2)-1));
                damploc2 = str2double(b.TaskObject{b.ConditionNumber(t),4}(dfcom(2)+1:dfpar2-1));
                
                if bnumber == 1
                    if b.TrialError(t) == 0
                        b1rtcorrect = [b1rtcorrect b.ReactionTime(t)];
                    else
                        %determine if it is correct for location
                        if  (samploc1 == damploc1) && (samploc2 == damploc2)
                            b1rtincorrect = [b1rtincorrect b.ReactionTime(t)];
                        else
                            b1rtincorrectnon = [b1rtincorrectnon b.ReactionTime(t)];
                        end
                    end
                    
                else
                    if b.TrialError(t) == 0
                        b2rtcorrect = [b2rtcorrect b.ReactionTime(t)];
                    else
                        %determine if it is correct for ide
                        if sum(sampide == dampide) == size(sampide,2)
                            b2rtincorrect = [b2rtincorrect b.ReactionTime(t)];
                        else
                            b2rtincorrectnon = [b2rtincorrectnon b.ReactionTime(t)];
                        end
                    end
                end
            end
        end
    end
end

figure;plot(50:10:300,hist(b1rtcorrect,50:10:300)./ sum(hist(b1rtcorrect,50:10:300)),'b');hold on;plot(50:10:300,hist(b2rtcorrect,50:10:300)./ sum(hist(b2rtcorrect,50:10:300)),'r')
hold on ;plot(50:10:300,hist(b1rtincorrect,50:10:300)./ sum(hist(b1rtincorrect,50:10:300)),'b--');hold on;plot(50:10:300,hist(b2rtincorrect,50:10:300)./ sum(hist(b2rtincorrect,50:10:300)),'r--')



figure;plot(50:10:300,hist(b1rtcorrect,50:10:300)./ sum(hist(b1rtcorrect,50:10:300)),'b');hold on;plot(50:10:300,hist(b2rtcorrect,50:10:300)./ sum(hist(b2rtcorrect,50:10:300)),'r')
hold on;plot(50:10:300,hist(b1rtincorrectnon,50:10:300)./ sum(hist(b1rtincorrectnon,50:10:300)),'b--');hold on;plot(50:10:300,hist(b2rtincorrectnon,50:10:300)./ sum(hist(b2rtincorrectnon,50:10:300)),'r--')

b2rtcorrect;

%plot(mt.data.Index(:,1)+160,'r');hold on;plot(smooth(mt.data.FirstSac,50),'k');pause;cla;

