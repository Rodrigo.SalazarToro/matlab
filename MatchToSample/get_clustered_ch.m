function get_clustered_ch

try
%run at day level  
%  monkeydir = pwd;for x = 1:16;days{x},cd(monkeydir);cd(days{x});get_clustered_ch;end
%finds channels that were not run
daydir = pwd;

%get ch and group info
[ich,iCHcomb,iflat,igroups] = getFlatCh;


%find groups with snr > 1.5
cd(['session01' filesep 'highpass'])
load SNR_channels channel_snr_list

g=0;
for x = 1 : size(channel_snr_list,1)
    if channel_snr_list(x,2) > 1.5
        g=g+1;
        good_snr(g) = channel_snr_list(x,1);
    end
end

cd(daydir)

%find processed groups
cd(['session99' filesep 'sort' filesep 'FD'])

clu = nptDir('*.clu.*');

for y = 1:size(clu,1)
    processed_groups(y) = str2double(clu(y).name(17:18));
end

[i ii] = intersect(good_snr,processed_groups);

good_snr(ii) = []; %get rid of clustered groups

unclustered_groups = good_snr; 

%get channels that correspond to groups
[i ii] = intersect(igroups,good_snr);

unclustered_ch = ich(ii);

catch
    unclustered_groups = [];
    unclustered_ch = [];
end
cd(daydir)

if ~isempty(unclustered_groups)
    pwd
end
  

save unclustered_groups_ch unclustered_groups unclustered_ch



