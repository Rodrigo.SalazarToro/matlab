function [varargout] = mkTuningStat(coh,ind,varargin)
% load switchdays.mat
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% coh = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');


Args = struct('redo',0','save',0);
Args.flags = {'redo','save'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});

index = coh.data.Index;
count = 1;
params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1);
comparisons = [1 2; 1 3; 2 3];
tuneArgs = {'iCueLoc' 'iCueObj'};
for i = vecr(ind)
    %                     coh.data.setNames{i}
    cd(coh.data.setNames{i}); % day directory
    pairg = index(i,10:11);
    if isempty(strfind(coh.data.setNames{i},'betty')); Args.ML = false; else Args.ML = true; end
    for r = 1 : 2
        %index(:,[10 11])
        cd(coh.data.setNames{i});
        clear C phi t
        if isempty(nptDir('tuning'))
            mkdir('tuning')
        end
        
        rfile = nptDir('rules.mat');
        if ~isempty(rfile);
            rules = load('rules.mat'); s = find(rules.r == r) + 1;
        elseif Args.ML;
            s = 1;
        end
        for tune = 1 :2
             cd(coh.data.setNames{i});
            cd tuning
            matfile = sprintf('%sg%04.0fg%04.0fRule%d.mat',tuneArgs{tune},pairg(1),pairg(2),r);
            file = nptDir(matfile);
            cd ..
            
            if ~isempty(nptDir(sprintf('session0%d',s))) && (isempty(file) || Args.redo)
                
                cd(sprintf('session0%d',s))
                NeuroInfo = NeuronalChAssign;
                for c =1  : 2; ch(c) = find(pairg(c) == NeuroInfo.groups); end
                mts = mtstrial('auto');
                errortrials = false;
                for stim = 1 : 3
                    if s ==1
                        trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',r,'ML',tuneArgs{tune},stim,modvarargin{:});
                    else
                        trials = mtsgetTrials(mts,'stable','BehResp',1,tuneArgs{tune},stim,modvarargin{:});
                    end
                    if isempty(trials)
                        for p = 1 : 4;period(p).cdata{stim} = [];end
                        errortrials = true;
                    else
                        [data,lplength] = lfpPcut(trials,ch,'fromFiles');
                        for p = 1 : 4; period(p).cdata{stim} = data{p}; end
                    end
                end
                clear data
                cd ..
                if ~errortrials
                    for p = 1 : 4
                        [groupN(p).rdiff,groupN(p).pvalues,~,f,groupN(p).C] = compareNGroupCoh(period(p).cdata,params);
                        for comp = 1 : 3
                            [group2(p).comp(comp).rdiff,group2(p).comp(comp).pvalues,levels,f] = compareTwoGroupCoh(period(p).cdata{comparisons(comp,1)},period(p).cdata{comparisons(comp,2)},params,'gProb',[0.05 0.017 0.01 0.003]);
                        end
                    end
                     cd(coh.data.setNames{i});
                    if Args.save
                        sprintf('%s %s',pwd,matfile)
                        cd tuning
                        save(matfile,'groupN','group2','f','levels')
                        cd ..
                    end
                    
                end
                
            end
        end
    end
    cd ..
end