function varargout = figGUI(varargin)
% FIGGUI M-file for figGUI.fig
%      FIGGUI, by itself, creates a new FIGGUI or raises the existing
%      singleton*.
%
%      H = FIGGUI returns the handle to a new FIGGUI or the handle to
%      the existing singleton*.
%
%      FIGGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FIGGUI.M with the given input arguments.
%
%      FIGGUI('Property','Value',...) creates a new FIGGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before figGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to figGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help figGUI

% Last Modified by GUIDE v2.5 30-May-2008 10:58:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @figGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @figGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before figGUI is made visible.
function figGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to figGUI (see VARARGIN)

% Choose default command line output for figGUI
handles.output = hObject;


handles.index = varargin{1};
handles.type = varargin{2};
handles.n = 1;

cd(handles.index.days{handles.n})
handles.h = open(sprintf('%sComb%d.fig',handles.type,handles.index.npair(handles.n)));
scrsz = get(0,'ScreenSize');
set(handles.h,'Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
cd ..

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes figGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = figGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Previous.
function Previous_Callback(hObject, eventdata, handles)
% hObject    handle to Previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.n > 1; handles.n = handles.n - 1;else handles.n = handles.n; end
close(handles.h)
cd(handles.index.days{handles.n})
handles.h = open(sprintf('%sComb%d.fig',handles.type,handles.index.npair(handles.n)));
scrsz = get(0,'ScreenSize');
set(handles.h,'Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
cd ..
guidata(hObject,handles)

% --- Executes on button press in Next.
function Next_Callback(hObject, eventdata, handles)
% hObject    handle to Next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.n < handles.index.number; handles.n = handles.n + 1;else handles.n = handles.n; end
close(handles.h)
cd(handles.index.days{handles.n})
handles.h = open(sprintf('%sComb%d.fig',handles.type,handles.index.npair(handles.n)));
scrsz = get(0,'ScreenSize');
set(handles.h,'Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
cd ..
guidata(hObject,handles)

function Number_Callback(hObject, eventdata, handles)
% hObject    handle to Number (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Number as text
%        str2double(get(hObject,'String')) returns contents of Number as a double
handles.n = str2double(get(hObject,'string'));
if handles.n > handles.index.number; handles.n = handles.index.number; end
close(handles.h)
cd(handles.index.days{handles.n})
handles.h = open(sprintf('%sComb%d.fig',handles.type,handles.index.npair(handles.n)));
scrsz = get(0,'ScreenSize');
set(handles.h,'Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
cd ..

guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function Number_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Number (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


