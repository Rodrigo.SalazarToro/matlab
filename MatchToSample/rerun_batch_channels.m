function rerun_batch_channels

%run at session level

sesdir = pwd;
cd ..
get_clustered_ch
load unclustered_groups_ch
ch = unclustered_ch
cd(sesdir)

if ~isempty(ch)
    %try to move files
    try
        movefile(['sort' filesep '*']); %empty sort directory into session
    catch
        %if files are already moved, do nothing.
    end
    
    mbatch = which('Batch_KKwikEE.txt');
    RunClustBatch(mbatch,'Do_AutoClust','yes','remotekk','no','channels',ch,'redoFeatures',1)
    
    cd ..
    mname = which('moveprocessedfiles.sh');
    system(mname);
    cd(sesdir)
else
    fprintf(1,['No unclustered channels on ' num2str(pwd) '\n'])
end

save previously_unclustered_channels ch