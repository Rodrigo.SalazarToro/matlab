function test_gabor_fit



figure('Position',[200 300 1000 500])
for deg = 0:10:360 %0 to 360 degrees
    deg
    %frequency
    f = 12;
    %time
    t = [0:.001:.5];
    
    %make sine wave 1
    s1 = 1 * cos(2*pi*f*t);
    r = deg*(pi/180);

    %make sine wave 2
    s2 = 1 * cos(2*pi*f*t + r);
    
    xc = xcorr(s1,s2,50,'coef');
    
    [ppeaks llag] = find_correlogram_peak('correlogram',xc)
    
    subplot(2,1,1)
    plot(s1)
    hold on
    plot(s2,'r')
    axis tight
    title('Reference channel is BLUE.')
    
    lags = [-50:50];
    subplot(2,1,2)
    plot(lags,xc)
    hold on
    %calculate phase angle
    
    
    [cf, estimates, sse, p_angle_deg, peak, xc] = generalized_gabor_fmin('correlogram',xc,'peak',ppeaks,'lag',llag);
    
    subplot(2,1,2)
    plot(lags,cf,'r')
    axis([-50 50 -1 1])
    title(['Phase angle degrees  ' num2str(p_angle_deg)]);
    
    pause
    clf
    
end