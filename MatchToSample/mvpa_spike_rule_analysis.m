function mvpa_spike_rule_analysis(varargin)


%run at session level

% computes mvpa analysis on prefrontal, parietal, and both spiking activity

% 1: presample    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

Args = struct('ml',0,'bin_spikes',0,'run_mvpa',0);
Args.flags = {'ml','bin_spikes','run_mvpa'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames');

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    r1 = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    r2 = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
    N = NeuronalHist('ml');
else
    mt = mtstrial('auto','redosetNames');
    r1 = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    r2 = mtsgetTrials(mt,'BehResp',1,'stable','rule',2);
    N = NeuronalHist;
end

alltrials = sort([r1 r2]);
ntrials = size(alltrials,2);
rulelist = zeros(1,ntrials);
[~,r1list] = intersect(alltrials,r1);
rulelist(r1list) = 1;

%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%get group information
NeuroInfo = NeuronalChAssign(); %get the "group" number that corresponds to each channel


%find groups with spikes
groups = nptDir('group*');
ngroups = [1 : size(groups,1)];

if Args.bin_spikes
    cluster_counter = 0;
    for g = ngroups %spike channel
        fprintf('\n%0.5g     ',g)
        
        %determine which channels the group number corresponds to
        g2ch = str2double(groups(g).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,g2ch);
        
        cd([sesdir filesep groups(g).name])
        %find clusters
        clusters = nptDir('cluster*');
        nclusters = size(clusters,1);
        
        for c = 1 : nclusters
            cluster_counter = cluster_counter + 1;
            cd([sesdir filesep groups(g).name])
            cd(clusters(c).name);
            
            %get spikes
            load ispikes.mat
            tcounter = 0;
            for ttrial = alltrials
                tcounter = tcounter + 1;
                
                spike_data = sp.data.trial(ttrial).cluster.spikes;
                s = ceil(spike_data); %ceil because a zero spike occured when using "round"
                
                
                %determine what epoch each spike is in
                s_on = sample_on(ttrial);
                s_off = sample_off(ttrial);
                m = match(ttrial);
                
                % 1: presample    [(sample_on - 399) : (sample_on)]
                % 2: sample      [(sample_off - 399) : (sample_off)]
                % 3: early delay [(sample_off) : (sample_off + 399)]
                % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
                % 5: delay       [(sample_off + 201) : (sample_off + 800)]
                % 6: delay match [(match - 399) : (match)]
                
                bins{1}(cluster_counter,tcounter) = sum(s >= (s_on - 399) & s <= s_on);
                bins{2}(cluster_counter,tcounter) = sum(s >= (s_off - 399) & s <= s_off);
                bins{3}(cluster_counter,tcounter) = sum(s >= s_off & s < (s_off + 399));
                bins{4}(cluster_counter,tcounter) = sum(s > (s_off+400) & s <= (s_off + 800));
                bins{5}(cluster_counter,tcounter) = sum(s > (s_off+200) & s <= (s_off + 800));
                bins{6}(cluster_counter,tcounter) = sum(s >= (m - 399) & s <= m);
            end
            
            %get anatomical information for each cluster
            [~,ii] = intersect(N.gridPos,str2double(groups(g).name(end-3:end)));
            NN.recordedDepth(cluster_counter) = N.recordedDepth(ii);
            NN.gridPos(cluster_counter) = N.gridPos(ii);
            NN.cortex(cluster_counter) = N.cortex(ii);
            NN.location{cluster_counter} = N.location{ii};
            NN.level1{cluster_counter} = N.level1{ii};
            NN.level2{cluster_counter} = N.level2{ii};
            NN.level3{cluster_counter} = N.level3{ii};
            
            
        end
    end
    cd(sesdir)
    save spike_bins_mvpa bins NN
end



if Args.run_mvpa
    load spike_bins_mvpa
    
    %determine the PFC and PPC channels
    pfc_areas = strfind(NN.cortex,'F');
    ppc_areas = strfind(NN.cortex,'P');
    
    for nt = 1 : ntrials
        
        rlist = rulelist;
        test_rule = rlist(nt);
        rlist(nt) = [];
        
        % % % % % %         rp = randperm(ntrials-1);
        % % % % % %         rlist = rlist(rp);
        
        
        b1 = bins{1};
        b2 = bins{2};
        b3 = bins{3};
        b4 = bins{4};
        b5 = bins{5};
        b6 = bins{6};
        
        %kick out test trial
        testb1 = b1(:,nt);
        b1(:,nt) = [];
        
        testb2 = b2(:,nt);
        b2(:,nt) = [];
        
        testb3 = b3(:,nt);
        b3(:,nt) = [];
        
        testb4 = b4(:,nt);
        b4(:,nt) = [];
        
        testb5 = b5(:,nt);
        b5(:,nt) = [];
        
        testb6 = b6(:,nt);
        b6(:,nt) = [];
        
        
        for a = 1:2
            
            if a == 1;
                alist = pfc_areas;
            else
                alist = ppc_areas;
            end
            SVMstructe1 = svmtrain(b1(alist,:),rlist,'Kernel_Function','mlp');
            SVMstructe2 = svmtrain(b2(alist,:),rlist,'Kernel_Function','mlp');
            SVMstructe3 = svmtrain(b3(alist,:),rlist,'Kernel_Function','mlp');
            SVMstructe4 = svmtrain(b4(alist,:),rlist,'Kernel_Function','mlp');
            SVMstructe5 = svmtrain(b5(alist,:),rlist,'Kernel_Function','mlp');
            SVMstructe6 = svmtrain(b6(alist,:),rlist,'Kernel_Function','mlp');
            
            
            newClassese1(a,nt) = svmclassify(SVMstructe1,testb1(alist)');
            newClassese2(a,nt) = svmclassify(SVMstructe2,testb2(alist)');
            newClassese3(a,nt) = svmclassify(SVMstructe3,testb3(alist)');
            newClassese4(a,nt) = svmclassify(SVMstructe4,testb4(alist)');
            newClassese5(a,nt) = svmclassify(SVMstructe5,testb5(alist)');
            newClassese6(a,nt) = svmclassify(SVMstructe6,testb6(alist)');
        end
        
    end
    
    
    perf_pfc(1) = (sum(newClassese1(1,:) == rulelist) ./ ntrials) * 100;
    perf_pfc(2) = (sum(newClassese2(1,:) == rulelist) ./ ntrials) * 100;
    perf_pfc(3) = (sum(newClassese3(1,:) == rulelist) ./ ntrials) * 100;
    perf_pfc(4) = (sum(newClassese4(1,:) == rulelist) ./ ntrials) * 100;
    perf_pfc(5) = (sum(newClassese5(1,:) == rulelist) ./ ntrials) * 100;
    perf_pfc(6) = (sum(newClassese6(1,:) == rulelist) ./ ntrials) * 100;
    
    
    perf_ppc(1) = (sum(newClassese1(2,:) == rulelist) ./ ntrials) * 100;
    perf_ppc(2) = (sum(newClassese2(2,:) == rulelist) ./ ntrials) * 100;
    perf_ppc(3) = (sum(newClassese3(2,:) == rulelist) ./ ntrials) * 100;
    perf_ppc(4) = (sum(newClassese4(2,:) == rulelist) ./ ntrials) * 100;
    perf_ppc(5) = (sum(newClassese5(2,:) == rulelist) ./ ntrials) * 100;
    perf_ppc(6) = (sum(newClassese6(2,:) == rulelist) ./ ntrials) * 100;
    
    cd(sesdir)
    save mvpa_perf perf_ppc perf_pfc pfc_areas ppc_areas
end
