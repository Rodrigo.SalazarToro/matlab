function [response_entropy,noise_entropy,mutual_information] = hilbertphase_mi_surrogate(varargin)

Args = struct('ml',0,'ent',[],'plot',0,'nperms',100);
Args.flags = {'ml','plot'};
Args = getOptArgs(varargin,Args);

ents = Args.ent;
bins = [0:.05:1];

nfreq = size(ents,1);
ntime = size(ents,2);

sesdir = pwd;
N = NeuronalHist('ml');

if Args.ml
    c = mtscpp2('auto','ml');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %these are the identity trials
    
    %three locs
    for ll = 1:3
        locs{ll} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',ll); %these are the identity trials
        nlocs(ll) = size(locs{ll},2); %used for surrogates
    end
    
else
    c = mtscpp2('auto');
    mt=mtstrial('auto','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    %three locs
    for ll = 1:3
        locs{ll} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueLoc',ll); %these are the identity trials
        nlocs(ll) = size(locs{ll},2); %used for surrogates
    end
end


for p = 1 : Args.nperms
    
    perm_trials = tr(randperm(size(tr,2)));
    for x = 1 : 3
        plocs{x} = sort(perm_trials(1:nlocs(x)));
        perm_trials(1:nlocs(x)) = [];
    end
    
    ntrials = size(tr,2);
    response_entropy = zeros(nfreq,ntime);
    %calculate response entropy H(R)
    for f = 1 : nfreq
        for t = 1 : ntime
            h = hist(squeeze(ents(f,t,tr)),bins) ./ ntrials; %normalize it by the number of responses (in this case trials), this is response distribution P(r)
            response_entropy(f,t,p) = -1 * nansum( (h .* log2(h)) ); %see panzeri_2007
        end
    end
    
    %calculate the noise entropy
    for f = 1 : nfreq
        for t = 1 : ntime
            h = hist(squeeze(ents(f,t,tr)),bins) ./ ntrials;
            %calculate conditional distributions
            for ll = 1 : 3
                cond_distributions{ll} = hist(squeeze(ents(f,t,plocs{ll})),bins) ./ size(plocs{ll},2); %normalize it by the number of responses (in this case trials)
            end
            
            %sum over stimuli
            for ll = 1 : 3
                ps = size(plocs{ll},2)/ntrials;
                nr(ll,:) =   ps .* cond_distributions{ll} .* log2(cond_distributions{ll});
            end
            
            %sum over responses
            noise_entropy(f,t,p) = -1 * nansum(nansum(nr)); %this is the noise entropy H(R|S)
            
            for ll = 1:3
                ps = size(plocs{ll},2)/ntrials;
                mi(ll,:) = ps .* cond_distributions{ll} .* log2((cond_distributions{ll} ./ h));
            end
            mutual_information(f,t,p) = nansum(nansum(mi));
            
        end
    end
end




