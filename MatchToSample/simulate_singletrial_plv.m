function simulate_singletrial_plv


load('/media/raid/data/monkey/clark/060428/session02/lfp/lfp2/hilbertentropy0203.mat')

hentropy = hilbertentropy{1}(:,1:36,:);

ntrials = size(hentropy,3);

alltrials = 1:ntrials;

ms = [];
stds = [];
for x = 1:16
    for t = 1:36
        ms(x,t) = mean(squeeze(hentropy(x,t,alltrials)));
        stds(x,t) = std(squeeze(hentropy(x,t,alltrials)));
    end
end


%simulate ntrials
sim_hentropy = zeros(size(hentropy,1),size(hentropy,2),ntrials);
for nt = 1 : ntrials 
    for x = 1:16
        for t = 1:36
            sim_hentropy(x,t,nt) = normrnd(ms(x,t),stds(x,t));
        end
    end
end


figure
%get mean and std of sim entropy
sim_ms = [];
sim_stds = [];
for x = 1:16
    for t = 1:36
        sim_ms(x,t) = mean(squeeze(sim_hentropy(x,t,:)));
        sim_stds(x,t) = std(squeeze(sim_hentropy(x,t,:)));
    end
end

subplot(2,2,1);
imagesc(ms)
subplot(2,2,2)
imagesc(stds)
subplot(2,2,3)
imagesc(sim_ms)
subplot(2,2,4)
imagesc(sim_stds)




