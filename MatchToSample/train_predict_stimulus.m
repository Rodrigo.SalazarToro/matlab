function predict = train_predict_stimulus(varargin)

%run at session level
Args = struct('ml',0,'fits',0,'stim_prediction',0,'stims',0,'p_cutoff',1,'logits',0,'rule',1,'snr',1.8,'threshold',1,...
    'pair_list',[],'obj1',[],'obj2',[],'hist_info',[],'block',[],'plotlle',0,'sorted',0,'fix',0,'no_offset',0,'no_interaction',0,'pp',0,...
    'pf',0,'ff',0,'save_figs',0,'sig_interaction',0,'pair',[]);

Args.flags = {'ml','fits','stim_prediction','stims','logits','plotlle','sorted','fix','no_offset','no_interaction','pp','pf','ff','save_figs','sig_interaction'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
%rule = 1(identity), rule = 0(location)
if Args.rule
    rule = 'identity';
else
    rule = 'location';
end

if isempty(Args.hist_info)
    N = NeuronalHist;
else
    N = Args.hist_info;
end

if isempty(Args.obj2)
    if Args.ml
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    else
        mt = mtstrial('auto','redosetNames');
    end
else
    mt = Args.obj2;
end

if isempty(Args.obj1)
    if Args.ml
        c = mtscpp('auto','ml','train','ide_only','glm_data');
    else
        c = mtscpp('auto','train','ide_only','glm_data');
    end
else
    c = Args.obj1;
end

if isempty(Args.block)
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    end
else
    %determine number of blocks
    bdata = find(diff(mt.data.Index(:,1)))';
    nblocks = size(bdata,2) + 1;
    %determine block order
    bdata(:) = bdata(:) + 1;
    b_order = mt.data.Index([1,bdata],1);
    
    a_block = find(b_order == Args.rule);
    block_num = a_block(Args.block);
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1,'block',block_num);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'block',block_num);
    end
end

cd(['lfp' filesep 'lfp2']);
lfp2dir = pwd;
if Args.ml
    cd(rule);
end

combs = c.data.Index(:,[23:24]);

if Args.fix
    load glm_fix_train
    glm_train = glm_fix_train;
else
    load glm_delay_train
    glm_train = glm_delay_train;
end

if Args.sorted
    [~, total_pairs] = get(c,'Number','sorted'); %get list of sorted pairs
else
    [~, total_pairs] = get(c,'Number');
end

if Args.fits
    for pair_num = total_pairs
        total_pairs - pair_num
        cd(lfp2dir)
        chpair = (['channelpair' num2strpad(combs(pair_num,1),2) num2strpad(combs(pair_num,2),2)]);
        if Args.ml
            cd(rule)
        end
        load threshold threshold
        cd(lfp2dir)
        t = threshold{pair_num}(Args.threshold);
        
        load(chpair)
        ntrials = size(corrcoefs,2);
        bins = size(corrcoefs,1);
        binary_corrcoefs = zeros(bins,ntrials);
        original_corrcoefs = zeros(9,ntrials);
        original_phases = zeros(9,ntrials);
        
        for ccc = 1: ntrials
            % % % % % % % % % % % % % % % % % binary_corrcoefs(find(abs(corrcoefs(:,ccc) >= t)),ccc) = 1; WRONG
            binary_corrcoefs(find(abs(corrcoefs(:,ccc)) >= t),ccc) = 1;
            original_corrcoefs(:,ccc) = corrcoefs((25:33),ccc);
            original_phases(:,ccc) = phases((25:33),ccc);
        end
        
        cd ../..
        if Args.fix
            time(:,1) = (1:7)';
        else
            time(:,1) = (1:9)';
        end
        
        counter = 0;
        
        for q = total_trials
            counter = counter + 1;
            x = binary_corrcoefs(:,q);
            
            if Args.fix
                response(:,1) = x(1:7);
            else
                response(:,1) = x(25:33);
            end
            response(:,2) = ones(1,max(time));
            
            B = glmfit(time,response,'binomial','link','logit');
            if Args.no_offset
                input = time * (B(2));
            else
                input = (B(1) + time * (B(2))); %works for no interaction as well
            end
            %convert to probability
            if ~Args.logits
                input = 1 ./ (1 + exp(-input));
            end
            trialFits(counter,:) = input;
            
            trialFits_binary(counter,:) = response(:,1)';
            
            t_corrcoefs(counter,:) = original_corrcoefs(:,q)';
            t_phases(counter,:) = original_phases(:,q)';
            
        end
        trial_fits{pair_num} = trialFits;
        
        
        
        trial_fits_binary{pair_num} = trialFits_binary;
        
        trial_corrcoefs{pair_num} = t_corrcoefs;
        
        trial_phases{pair_num} = t_phases;
        
        
        cell_coefs = glm_train{pair_num}.interaction_coefs;
        coefs = cellfun(@str2double,cell_coefs);
        
        if Args.no_offset
            res(:,1) = time*coefs(2);
            res(:,2) = time*coefs(2) + coefs(3) + time* coefs(11);
            res(:,3) = time*coefs(2) + coefs(4) + time *coefs(12);
            res(:,4) = time*coefs(2) + coefs(5) + time *coefs(13);
            res(:,5) = time*coefs(2) + coefs(6) + time * coefs(14);
            res(:,6) = time*coefs(2) + coefs(7) + time * coefs(15);
            res(:,7) = time*coefs(2) + coefs(8) + time * coefs(16);
            res(:,8) = time*coefs(2) + coefs(9) + time *coefs(17);
            res(:,9) = time*coefs(2) + coefs(10) + time* coefs(18);
        elseif Args.no_interaction
            res(:,1) = coefs(1) + time*coefs(2);
            res(:,2) = coefs(1) + time*coefs(2);
            res(:,3) = coefs(1) + time*coefs(2);
            res(:,4) = coefs(1) + time*coefs(2);
            res(:,5) = coefs(1) + time*coefs(2);
            res(:,6) = coefs(1) + time*coefs(2);
            res(:,7) = coefs(1) + time*coefs(2);
            res(:,8) = coefs(1) + time*coefs(2);
            res(:,9) = coefs(1) + time*coefs(2);
        else
            res(:,1) = coefs(1) + time*coefs(2);
            res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
            res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
            res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
            res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
            res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
            res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
            res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
            res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
        end
        
        if ~Args.logits
            res = 1 ./ (1 + exp(-res));
        end
        pair_fits{pair_num} = res';
    end
    
    cd(lfp2dir)
    if Args.fix
        %saved as probabilities
        trial_fits_train_fix = trial_fits;
        pair_fits_train_fix = pair_fits;
        save glm_fits_train_fix pair_fits_train_fix trial_fits_train_fix
    else
        %saved as probabilities
        trial_fits_train = trial_fits;
        pair_fits_train = pair_fits;
        save glm_fits_train pair_fits_train trial_fits_train trial_fits_binary trial_corrcoefs trial_phases
    end
else
    cd(lfp2dir)
    if Args.fix
        % mt = mtstrial('auto','ML','RTfromML');
        load glm_fits_train_fix
        pair_fits = pair_fits_train_fix;
        trial_fits = trial_fits_train_fix;
    else
        % mt = mtstrial('auto','ML','RTfromML');
        load glm_fits_train
        pair_fits = pair_fits_train;
        trial_fits = trial_fits_train;
        
        
        
        
        
        
        
        

        
%  trial_fits = trial_fits_binary;
        
        
        
        
        
        
        
        
    end
end

if Args.stims
    cd(sesdir)
    %get stimulus info
    locs = unique(mt.data.CueLoc);
    objs = unique(mt.data.CueObj);
    stim = 0;
    for object = 1:3
        %Run for all locations
        for location = 1:3
            stim = stim+1;
            if isempty(Args.block)
                if Args.ml
                    ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
                else
                    ind = mtsgetTrials(mt,'BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
                end
            else
                %determine number of blocks
                bdata = find(diff(mt.data.Index(:,1)))';
                nblocks = size(bdata,2) + 1;
                %determine block order
                bdata(:) = bdata(:) + 1;
                b_order = mt.data.Index([1,bdata],1);
                
                a_block = find(b_order == Args.rule);
                block_num = a_block(Args.block);
                ind = mtsgetTrials(mt,'BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'ml','rule',1,'block',block_num);
            end
            stimulus(ind) = stim;                %locind = [2,5,8];
                %[i ii] = max(xc2(1,locind));
        end
    end
    [~, ~, iii] = find(stimulus);
    stimulus = iii;
    cd(lfp2dir)
    save stimulus stimulus
else
    load stimulus stimulus
end

numb_trials = max(cellfun(@length,trial_fits));

if Args.stim_prediction
    
    best = ones(2,9);
    object = 0;
    location = 0;
    
    int_pvals = c.data.Index(:,28);
    % %     spairs = int_pvals < Args.p_cutoff;
    
    if Args.fix
        tp = [1 : 7];
    else
        tp = [1 : 9];
    end
    if Args.ml
        if Args.sig_interaction
            if Args.pf
                [~, spairs] = get(c,'Number','ide_only','sorted','pf','glm_delay',.01);
            elseif Args.pp
                [~, spairs] = get(c,'Number','ide_only','sorted','pp','glm_delay',.01);
            elseif Args.ff
                [~, spairs] = get(c,'Number','ide_only','sorted','ff','glm_delay',.01);
            else
                [~, spairs] = get(c,'Number','ide_only','sorted','glm_delay',.01);
            end
        else
            if Args.pf
                [~, spairs] = get(c,'Number','ide_only','sorted','pf');
            elseif Args.pp
                [~, spairs] = get(c,'Number','ide_only','sorted','pp');
            elseif Args.ff
                [~, spairs] = get(c,'Number','ide_only','sorted','ff');
            else
                [~, spairs] = get(c,'Number','ide_only','sorted');
            end
        end
    else
        
        
        [~, spairs] = get(c,'Number');
        
    end
    
    if ~isempty(Args.pair)
        spairs = Args.pair;
    end
    
    if spairs > 0
        for t = 1:numb_trials 
            for timepoints = tp
                pp = 0;
                for p = spairs
                    pp = pp + 1;
                    pair_timepoints(pp) = trial_fits{p}(t,timepoints); %get each trial time point for all pairs
                    pfits_timepoints(:,pp) = pair_fits{p}(:,timepoints); %get 9 stims for each time point for all pairs
                end
                
                all_t_timepoints(timepoints,:) = pair_timepoints;
                
                for xx = 1 : 9
                    all_pfits_timepoints(timepoints,:,xx) = pfits_timepoints(xx,:);
                end
                
                %             for allstims = 1:9
                %                 stim_euclid(allstims,timepoints) = sqrt(sum(((pair_timepoints - pfits_timepoints(allstims,:)).^2))); %get euclidean distance of each trial from all pairs for 9 stims
                %                 xc(allstims,timepoints) = xcorr(pair_timepoints,pfits_timepoints(allstims,:),0,'coef');
                %             end
            end
            
            % %             figure
            % %             for x = 1:9;subplot(3,3,x);surface(all_pfits_timepoints(:,:,x));end
            % %
            % %             if stimulus(t) == 3
            % %                 ss = ss + all_t_timepoints;
            % %                 surface(all_t_timepoints)
            % %             end
            
            
            
            % %
            % % for i=1:9
            % %    subplot('position', [0.05 i/10 .9 1/11])
            % %    contourf(all_pfits_timepoints(:,(1:20),i))
            % % %    imagesc(squeeze(meanspec(i,20:end,:)),[0 max(meanspec(:))]), axis xy, axis off
            % % %    for k= 2*16.5:2*16:2*192, line([k k],[0 90]), end
            % %
            % % hold on
            % % end
            
            
            
            
            if Args.plotlle
                
                % %                 for k = 1:12
                % %                     subplot(3,4,k)
                % %                     colors = {'r','b','g','y','k','m','r','k','g'};
                % %                     for xx = 1 : 3
                % %                         pf = all_pfits_timepoints(:,:,xx)';
                % %
                % %                         y = lle(pf,k,3);
                % %                         line(y(1,:),y(2,:),y(3,:),'Color',colors{xx}) %'Color','r'
                % %                         hold on
                % %                     end
                % %                 end
                % %
                
                %
                
                %
                %     trial_lle = lle(tf,7,1);
                
                
                %         y = lle(pf,7,1);
                %         lle_xc(xx) = xcorr(trial_lle,y,0,'coef');
            end
            
            
            
            
            %find index of pairs for each stimulus that are increasing and
            %only use those pairs
            
% % % % % % % % % % % %             for ppp = 1:9
% % % % % % % % % % % %                pair_inc = all_pfits_timepoints(:,:,ppp);
% % % % % % % % % % % %                inc = [find((pair_inc(9,:)-pair_inc(1,:)) > .1)];
% % % % % % % % % % % %                increasing_pairs{ppp} = inc;
% % % % % % % % % % % %             end
% % % % % % % % % % % %             
            
            
            
            
            
            
            
            
% % % % %             tf = all_t_timepoints;
% % % % %             all_pfits =all_pfits_timepoints;
% % % % %             
% % % % %             del = [];
% % % % %             for ttf = 1:78
% % % % %                 if sum((tf(:,ttf) <.001)) == 9
% % % % %                     del(ttf) = 1;
% % % % %                 end
% % % % %             end
% % % % %             
% % % % %             tf(:,find(del)) = [];
% % % % %             all_pfits(:,find(del),:) = [];
% % % % %             
% % % % %             size(tf,2)
% % % % %             
% % % % %             for xx = 1 : 9
% % % % %                 pf = all_pfits(:,:,xx);
% % % % %                 xc2(xx) = corr2(pf,tf);
% % % % %             end
% % % % %             [i ii] = max(xc2);
% % % % %             stim_prediction(t) = ii;
% % % % %             stim_corr(t) = i;
% % % % %            
% % % % %             
% % % % %             if ii == stimulus(t)
% % % % %                 fprintf(1,'match\n')
% % % % %             end

%             for plots = 1:9
%             subplot(2,9,plots)
%             imagesc(all_pfits(:,:,plots)) 
%             hold on
%             end
%             subplot(2,9,(9+ii))
%             imagesc(tf)
%             pause
%             close all
            
            
            
            tf = all_t_timepoints;
            for xx = 1 : 9
                pf = all_pfits_timepoints(:,:,xx);
                xc2(xx) = corr2(pf,tf);
            end
            [i ii] = max(xc2);
            stim_prediction(t) = ii;
            stim_corr(t) = i;
            
            
            if ii == 1
                loc = 1; obj = 1;
            elseif ii == 2
                loc = 2; obj = 1;
            elseif ii == 3
                loc = 3; obj = 1;
            elseif ii == 4
                loc = 1; obj = 2;
            elseif ii == 5
                loc = 2; obj = 2;
            elseif ii == 6
                loc = 3; obj = 2;
            elseif ii == 7
                loc = 1; obj = 3;
            elseif ii == 8
                loc = 2; obj = 3;
            elseif ii == 9
                loc = 3; obj = 3;
            end
            
            
            %determine if location or identity is correct
            if stimulus(t) == 1 || stimulus(t) == 4 || stimulus(t) == 7
                sloc = 1;
            elseif stimulus(t) == 2 ||stimulus(t) == 5 ||stimulus(t) == 8
                sloc = 2;
            elseif stimulus(t) == 3 ||stimulus(t) == 6 ||stimulus(t) == 9
                sloc = 3;
            end
            
            if stimulus(t) == 1 || stimulus(t) == 2 || stimulus(t) == 3
                sobj = 1;
            elseif stimulus(t) == 4 || stimulus(t) == 5 || stimulus(t) == 6
                sobj = 2;
            elseif stimulus(t) == 7 || stimulus(t) == 8 || stimulus(t) == 9
                sobj = 3;
            end
            
            
            if loc == sloc && obj ~=sobj
                location = location+1;
            end
            
            if obj == sobj || loc == sloc
                object = object + 1;
            end
            

            
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             tf = all_t_timepoints;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             for xx = 1 : 9
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 pf = all_pfits_timepoints(:,:,xx);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 xc2(xx) = corr2(pf(:,increasing_pairs{xx}),tf(:,increasing_pairs{xx}));
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             [i ii] = max(xc2);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             stim_prediction(t) = ii;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             stim_corr(t) = i;
            
            
            
% % % %             
% % % %             xc2 = [];
% % % %             all_t_timepoints = [];
% % % %             for xx = 1 : 9
% % % %                 load(['stim' num2str(xx)], 'all_t_timepoints');
% % % %                 pf = all_t_timepoints;
% % % %                 xc2(xx) = corr2(pf,tf);
% % % %             end
% % % %             [i ii] = max(xc2);
% % % %             stim_prediction(t) = ii;
% % % %             
            
            
            
            
% % % % 
% % % %             if ii ~= stimulus(t)
% % % %                 xc2(ii) = 0;
% % % %                 [i ii] = max(xc2);
% % % %                 stim_prediction(t) = ii;
% % % %             end
% % %


% % % % % % % % % % % % % % % % % % % % % if stimulus(t) == ii
% % % % % % % % % % % % % % % % % % % % %     if i < best(1,ii)
% % % % % % % % % % % % % % % % % % % % %         best(1,ii) = i;
% % % % % % % % % % % % % % % % % % % % %         best(2,ii) = t;
% % % % % % % % % % % % % % % % % % % % %     end
% % % % % % % % % % % % % % % % % % % % % end


            
% %             
% %           
% %                 ac = [];
% %                 h = 0;
% %                 high = [];
% %                 for x = 1:size(pf,2)
% %                     ac(x) = xcorr(pf(:,x),tf(:,x),0,'coef');
% %                     if ac(x) > .9
% %                         h = h+1;
% %                         high(h) =x;
% %                     end
% %                 end
% %                 
% %                 hist(ac)
% %                 pause
% %                 close all
                
     
      
           
            
            
% % % % % % % % % 
% % % % % % % % %             %only select between the 3 stims at each location
% % % % % % % % %             if stimulus(t) == 1 || stimulus(t) == 4 || stimulus(t) == 7
% % % % % % % % %                 locind = [1,4,7];
% % % % % % % % %                 [i ii] = max(xc2(1,locind));
% % % % % % % % %                 stim_prediction(t) = locind(ii);
% % % % % % % % %             elseif stimulus(t) == 2 ||stimulus(t) == 5 ||stimulus(t) == 8
% % % % % % % % %                 locind = [2,5,8];
% % % % % % % % %                 [i ii] = max(xc2(1,locind));
% % % % % % % % %                 stim_prediction(t) = locind(ii);
% % % % % % % % %             elseif stimulus(t) == 3 ||stimulus(t) == 6 ||stimulus(t) == 9
% % % % % % % % %                 locind = [3,6,9];
% % % % % % % % %                 [i ii] = max(xc2(1,locind));
% % % % % % % % %                 stim_prediction(t) = locind(ii);
% % % % % % % % %             end
% % % % % % % % %             
            
            
            
            
            
            
            
            
            
            
% % %             
% % %             if stimulus(t) == locind(ii)
% % %                match2 =[]
% % %             end
% % %             
            
            
            
            
            
            
            
            
            %     s= sum(xc,2);
            %     [i ii] = max(s);
            %     stim_prediction(t) = ii;
% %             %     stim_corr(t) = i;
% %             
% %             if ii == stimulus(t)
% %                 max_stims(t) = i;
% %                 
% %             end
% %             
            
            
            
            
            
            % %
            % %             load stimulus
            % %             scrsz = get(0,'ScreenSize');
            % %             for iii = 1 : 9
            % %             figure('Position',[1 scrsz(4) scrsz(3) scrsz(4)])
            % %                 subplot('position', [0.0 i 1 1/1])
            % %
            % %                 contourf(all_pfits_timepoints(:,:,iii))
            % %                 title(num2str(iii))
            % %                 pause
            % %             end
            % %
            % %
            % %
            % %             subplot(2,5,10)
            % %             contourf(all_t_timepoints(:,(10:40)))
            % %             if stimulus(t) == iielse
            % %                 title(num2str(stimulus(t)),'Color','k')
            % %             else
            % %                 title(num2str(stimulus(t)),'Color','r')
            % %             end
            % %             pause
            % %             close all
            
            
            %
            %                 all_xc_match(:,:,ii) = [all_xc_match(:,:,ii) + xc];
            %             end
            %             all_xc(:,:,stimulus(t)) = [all_xc(:,:,stimulus(t)) + xc];
            
            
            % %             figure
            % %             for x = 1:9
            % %                 if x == ii
            % %                     plot(xc(x,:),'r')
            % %                 elseif x == stimulus(t)
            % %                     plot(xc(x,:),'g')
            % %                 else
            % %                     plot(xc(x,:))
            % %                 end
            % %                 hold on
            % %             end
            % %             stimulus(t) == ii
            % %             pause
            % %             close all
            % %             clc
            
            
        end
    else
        stim_prediction = [];
    end
    cd(lfp2dir)
    if Args.fix
        stim_prediction_train_fix = stim_prediction;
        save stim_prediction_train_fix stim_prediction_train_fix
    else
        stim_prediction_train = stim_prediction;
        save stim_prediction_train stim_prediction_train
    end
    
    save best best
    
else
    if Args.fix
        load stim_prediction_train_fix
        stim_prediction_train = stim_prediction_train_fix;
    else
        load stim_prediction_train
    end
end

total_matches = zeros(1,9);
ch_matches = 0;
total_stims = zeros(1,9);
matches = 0;
m = zeros(1,numb_trials);

%predict_trials = (numb_trials - round(numb_trials*.7)) + 1;
cd(lfp2dir)
if Args.fix
    load hold_trials_fix hold_trials_fix
    hold_trials = hold_trials_fix;
else
    load hold_trials hold_trials
end

if ~isempty(stim_prediction_train)
    counter = 0;
    for x = 1 : numb_trials
        counter = counter+1;
        
        stx = stimulus(x);
        if stimulus(x) == stim_prediction_train(x)
            matches = matches + 1;
            total_matches(stx) = total_matches(stx) + 1;
            m(x) = 1;
        end
        total_stims(stx) = total_stims(stx) + 1;
        
        c_stims = randperm(9);
        if stimulus(x) == c_stims(1)
            ch_matches = ch_matches + 1;
        end
    end
    
    correct_withheld = size(find(m(hold_trials)),2);
    numb_withheld = size(hold_trials,2);
    predict = (correct_withheld / numb_withheld) * 100
    
    %determine how many correct trials were from the withheld trials
    non_withheld = matches - correct_withheld;
    numb = numb_trials - numb_withheld;
    total = (non_withheld / numb) * 100
    
    %chance = (ch_matches / counter) * 100;
    
else
    predict = 0;
    total = 0;
    
end
% % % write_info = writeinfo(dbstack);
% % % if Args.fix
% % %     predict_fix = predict;
% % %     save glm_score_fix predict_fix total write_info
% % % else
% % %     save glm_score predict total write_info
% % % end

cd(sesdir)



