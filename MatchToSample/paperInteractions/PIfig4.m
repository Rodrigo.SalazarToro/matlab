cd /Volumes/raid/data/monkey/

obj = loadObject('psthIDEandLong99bin100msIdeBasedwithAC');


[r,ind] = get(obj,'Number');r


allAC = zeros(4,41,r);
allACsurh = zeros(4,41,r);
allACsurl = zeros(4,41,r);
AllS = nan(4,101,r);
AllSur = nan(4,101,r);
figure
for ii = 1 : r
    cd(obj.data.setNames{ind(ii)})
    load ispikes
    matfile = sprintf('xcorrg%sg%sRule1.mat',sp.data.groupname,sp.data.groupname);
    surfile = sprintf('xcorrg%sg%sRule1SurspikeTime.mat',sp.data.groupname,sp.data.groupname);
    load(matfile)
    allAC(:,:,ii) = crosscorr;
    
       try for ep = 1 : 4;AllS(ep,:,ii) = S{ep}; end; end
  
    load(surfile);
    allACsurh(:,:,ii) = squeeze(prctile(crosscorr,97,3));
    allACsurl(:,:,ii) = squeeze(prctile(crosscorr,2,3));
    try for ep = 1 : 4;AllSur(ep,:,ii) = prctile(S{ep},98,1); end; end
%     pause
%     clf
end


%
figure
for ep = 1 : 4
    
    subplot(4,1,ep)
    plot(f,nansum(AllS(ep,:,:) > AllSur(ep,:,:),3))
    xlim([0 100])
end
%

nCoi = squeeze(sum(sum(allAC,1),2));
ncoiperbin = 10;
eCoi = find(nCoi > 4*41*ncoiperbin);
% hist(nCoi,[0:100: 40000])

prcsigh = 100*squeeze(sum(allAC(:,:,eCoi) > allACsurh(:,:,eCoi),3)) / length(eCoi);
prcsigl = 100*squeeze(sum(allAC(:,:,eCoi) < allACsurl(:,:,eCoi),3)) / length(eCoi);
figure
plot(timelag,prcsigh)
hold on
plot(timelag,-prcsigl)

legend('presample','sample','delay1','delay2')
xlabel('Time lag (ms)')
ylabel('% pairs')
title(sprintf('n = %d over %d total',length(eCoi),r))

%% oscillatory cells
% trangeh = (timelag < 90 & timelag > 45) | (timelag > -90 & timelag < -45);
% trangel = (timelag <= 45 & timelag > 6) | (timelag > -45 & timelag < -6);
trangeh = (timelag > 12) | (timelag < -12);
trangel = (timelag > 12) | (timelag < -12);

sigsh = squeeze(sum(allAC(:,trangeh,eCoi) > allACsurh(:,trangeh,eCoi),2));
sigsl = squeeze(sum(allAC(:,trangel,eCoi) < allACsurl(:,trangel,eCoi),2));

modpow = squeeze(nanmean((AllS(:,5:90,eCoi) - AllSur(:,5:90,eCoi)) ./ AllSur(:,5:90,eCoi),2));

a = AllS(:,5:90,eCoi) > AllSur(:,5:90,eCoi); % gets significant bin power

b = diff(a,4,2); % get at least 4 points in a row

c = a(:,1:size(b,2),:) .* (b == 0);

sc = squeeze(nansum(c == 1,2));
powsig = squeeze(nansum(a,2));


% [~,osciCell] = find(sigsh >=3 & sigsl >=3);
[sepoch,osciCell] = find(sigsh >=4 & sigsl >=4 & powsig >4 & sc > 0);
osciCellt = unique(osciCell);

% for ii = 1 : length(osciCellt)
%     plot(f,AllS(:,:,eCoi(osciCellt)))
%     
%     
% end
shist = cell(4,1);
for ep = 1 : 4
    sep = find(sepoch == ep);
    shist{ep} = obj.data.Index(ind(eCoi(osciCell(sep))),3);
end
100*length(osciCellt) / length(eCoi)
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

figure
epochs={'presample' 'sample' 'delay1' 'delay2'};

[ntot,~] = hist(obj.data.Index(unique(ind(eCoi)),3),[0:19]);
for ep = 1 : 4
    subplot(2,4,ep)
    sep = find(sepoch == ep);
    [n(ep,:),xout] = hist(obj.data.Index(unique(ind(eCoi(osciCell(sep)))),3),[0:19]);
    bar(xout,100*n(ep,:)./ntot)
    set(gca,'XTick',[1:18])
    set(gca,'XTickLabel',areas)
    axis([0 18 0 40])
    set(gca,'TickDir','out')
    title(epochs{ep})
end
xlabel('Cortical areas')
ylabel('% units')


cn = 0;
nAllS = AllS   ./ repmat(nanmean(AllS,2),[1 size(AllS,2) 1]);
for ep = 1 : 4
    sep = find(sepoch == ep);
    
    me = nanmean(squeeze(nAllS(ep,:,eCoi(osciCellt))),2);
    st = nanstd(squeeze(nAllS(ep,:,eCoi(osciCellt))),[],2);
    
%     sig = squeeze(AllS(ep,:,eCoi(osciCell(sep))) > AllSur(ep,:,eCoi(osciCell(sep))));
%     sig = squeeze(AllS(ep,:,eCoi(osciCellt)) > AllSur(ep,:,eCoi(osciCellt)));
    
    %     allspectra = squeeze((AllS(ep,:,eCoi(osciCell(sep)))- AllSur(ep,:,eCoi(osciCell(sep)))) ./AllSur(ep,:,eCoi(osciCell(sep))));
    subplot(2,4,4+ep)
        plot(f,me) ;
        hold on
    
        plot(f,me+st,'--') ;
        plot(f,me-st,'--') ;
    
    
%     ppc = find(shist{ep}> 5 );
%     plot(f,sum(sig(:,ppc),2),'r')
%     hold on
%     pfc = find(shist{ep}<= 5 );
%     plot(f,sum(sig(:,pfc),2),'b')
%     plot(f,100*sum(sig,2)/ length(osciCellt))
    axis([3 50 0.5 2.1])
     set(gca,'TickDir','out')
    % plot(f,allspectra)
end

xlabel('Frequency (Hz)')
ylabel('Norm. Power (arb. unit)')










figure
params = struct('Fs',floor(1000/6),'fpass',[0 50],'tapers',[2 3]);
for ii = 1 : length(osciCell)
    set(gcf,'Name',sprintf('%s area %s',obj.data.setNames{ind(eCoi(osciCell(ii)))},areas{obj.data.Index(unique(ind(eCoi(osciCell(ii)))),3)}))
    cd(obj.data.setNames{ind(eCoi(osciCell(ii)))})
    files = nptDir('xcorrg0*Rule1.mat');
    load(files.name)
    for ep = 1 : 4
        
        subplot(3,4,ep)
        title(epochs{ep})
        [pe,fr] = max(S{ep}(8:end));
        
        text(30,0.9*pe,['peak=' num2str(round(f{ep}(fr))) 'Hz'])
        hold on
        plot(f{ep},S{ep})
        xlim([0 50])
         xlabel('Frequency (Hz)')
         ylabel('Power')
        
        subplot(2,4,4+ep)
        plot(timelag,crosscorr(ep,:))
        xlim([-120 120])
        xlabel('Time lag (ms)')
        
    end
    pause
    clf
end

%% bursting cells

trange = timelag ==0;
sigs = sum(allAC(:,trange,eCoi) > allACsur(:,trange,eCoi),2);

figure

for ep = 1 : 4
    subplot(4,1,ep)
    hist(sigs(ep,:),100)
    
end

for ep = 1 : 4
sigACu(ep) = length(find(sigs(ep,:) ==1));
end

100*sigACu/567



%% example

 InspectGUI(obj,'plotType','autoCorr')
 
 % n = 1093 for mua and 177 for sua
 
 % n =
 % /Volumes/raid/data/monkey/betty/091002/session01/group0063/cluster01s
 % area PE for a gamma single unit oscillating

