cd /Volumes/raid/data/monkey/

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number');
allpeaks = [obj.data.alphaPeak(ind,1:4); obj.data.betaPeak(ind,1:4); obj.data.gammaPeak(ind,1:4)];

cd clark/060328/
load cohInter.mat

epochs = {'presample' 'sample' 'delay1' 'delay2'};

for sb = 1 : 4; 
  
    [n(sb,:),xout]= hist(allpeaks(:,sb),f); 
   
end
figure
subplot(4,1,1);

plot(xout,n)
axis([0.8 42.18 0 350]);
%% for each monkey separately


cd /Volumes/raid/data/monkey/betty

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number');
allpeaks = [obj.data.alphaPeak(ind,1:4); obj.data.betaPeak(ind,1:4); obj.data.gammaPeak(ind,1:4)];

cd clark/060328/
load cohInter.mat

epochs = {'presample' 'sample' 'delay1' 'delay2'};

for sb = 1 : 4;
    
    [n(sb,:),xout]= hist(allpeaks(:,sb),f);
    
end
figure
subplot(3,1,2);

plot(xout,100*n/r)
axis([8 42.18 0 20]);


cd /Volumes/raid/data/monkey/clark

obj = loadObject('cohInterIDE.mat');

[r,ind] = get(obj,'Number');
allpeaks = [obj.data.alphaPeak(ind,1:4); obj.data.betaPeak(ind,1:4); obj.data.gammaPeak(ind,1:4)];

cd clark/060328/
load cohInter.mat

epochs = {'presample' 'sample' 'delay1' 'delay2'};

for sb = 1 : 4;
    
    [n(sb,:),xout]= hist(allpeaks(:,sb),f);
    
end
set(gca,'TickDir','out')
subplot(3,1,3);

plot(xout,100*n/r)
axis([8 42.18 0 20]);



cd /Volumes/raid/data/monkey/

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number');
allpeaks = [obj.data.alphaPeak(ind,1:4); obj.data.betaPeak(ind,1:4); obj.data.gammaPeak(ind,1:4)];

cd clark/060328/
load cohInter.mat

epochs = {'presample' 'sample' 'delay1' 'delay2'};

for sb = 1 : 4;
    
    [n(sb,:),xout]= hist(allpeaks(:,sb),f);
    
end
set(gca,'TickDir','out')
subplot(3,1,1);

plot(xout,100*n/r)
axis([8 42.18 0 20]);
xlabel('Frequency (Hz)')
ylabel('# counts');title('Both monkeys')
subplot(3,1,2); title('Betty')
subplot(3,1,3); title('Clark')
set(gca,'TickDir','out')

    %%
   
locs = cell(1,4);
for ii = 1 : r
    cd(obj.data.setNames{ind(ii)})
    load cohInter.mat
    
    cd session01
    neuroInfo = NeuronalChAssign;
    cd ..
    for cc = 1 : 2; chs(cc) = find(neuroInfo.groups == obj.data.Index(ind(ii),9+cc)); end
    ncomb = find(ismember(Day.comb, chs,'rows') ==1);
    
    if Day.session(1).rule == 1
        cohspectrum = squeeze(Day.session(1).C(:,ncomb,:));
        try
            sur = load('cohInterSur.mat');
            surr = squeeze(sur.Day.session(1).Prob(:,3,ncomb,:));
        catch
            sur = load('cohInterSurDelay1.mat');
            surr = squeeze(sur.Day.session(1).Prob(:,3,ncomb,:));
        end
        
    else
        cohspectrum = squeeze(Day.session(2).C(:,ncomb,:));
        try
            sur = load('cohInterSur.mat');
            surr = squeeze(sur.Day.session(2).Prob(:,3,ncomb,:));
        catch
            sur = load('cohInterSurDelay1.mat');
            surr = squeeze(sur.Day.session(2).Prob(:,3,ncomb,:));
        end
    end
    cohspectrum = filtfilt(repmat(1/3,1,3),1,cohspectrum);
    sigcoh = cohspectrum > surr;
    cohspectrum = cohspectrum .*sigcoh;
    
    fcts = which('findpeaks','-all');
    
    thefct = find(cellfun(@isempty,strfind(fcts,sprintf('toolbox%ssignal%ssignal%sfindpeaks.m',filesep,filesep,filesep))) ==0);
    cd(fcts{thefct}(1:end-11));
    for ep = 1 : 4
    [lmax,tlocs] = findpeaks(cohspectrum(:,ep)); 
    locs{ep} = cat(2,locs{ep},tlocs);
    end
end


figure
n = zeros(65,4);
for ep = 1 : 4
[n(:,ep),~] = hist(locs{ep},[1:65]);
end

plot(f,n)
xlabel('Frequency (Hz)')
ylabel('counts')
xlim([8 50])
 set(gca,'TickDir','out');
xlim([8 50]);
hold on
line([14 14],[0 350],'Color','k')
line([31 31],[0 350],'Color','k')


legend(epochs)
xlabel('Frequency (Hz)')
ylabel('Counts')
legend(epochs)
% subplot(1,2,2)
% 
% hist(reshape(allpeaks,size(allpeaks,1)*size(allpeaks,2),1),f);
% axis([0.8 42.18 0 1200]);
% set(gca,'TickDir','out')
% title('All epochs combined')
% 
% xlabel('Frequency (Hz)')
% ylabel('Counts')
% xlim([8 50]);

%% histo distr

ccomb = unique(obj.data.hist(ind,:),'rows');
[ncomb,xout] = hist(obj.data.hist(ind,:),[0 :18]);
ncomb = sum(ncomb,2);
bands = {'theta' 'alpha' 'beta' 'gamma'};
epochs = {'presample' 'sample' 'delay1' 'delay2'};
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

clear n
for ep = 1 : 4
    for bb = 2 : 4
        [r,ind] = get(obj,'Number','sigSur',{bands{bb} ep [1 3]});
        
       
        
        [nt,xout]=hist(obj.data.hist(ind,:),[0 :18]);
        n(bb-1,ep,:) = sum(nt,2);
    end
end


for bb = 1 : 3
    subplot(4,1,bb+1)
    bar(xout,100*squeeze(n(bb,:,:))'./repmat(ncomb,1,4),'grouped');
    xlim([0 18])
    ylim([0 55])
    set(gca,'TickDir','out');
    
    title(bands{bb+1});
    set(gca,'XTick',[1:17])
    set(gca,'XTickLabel',areas)
end
ylabel('%')
legend(epochs)

%% depth info

cd /Volumes/raid/data/monkey/clark
bands = {'theta' 'alpha' 'beta' 'gamma'};
obj = loadObject('cohInterIDE.mat');
mindist = 1.5;
indBand = cell(4,4);
for bb = 1 : 4
    for ep = 1 : 4
        [r,indt] = get(obj,'Number','sigSur',{bands{bb} ep [1 3]});
        %         indBand{bb,ep} = union(indBand{bb},indt);
        indBand{bb} = union(indBand{bb},indt);
    end
end

clear depth 
for bb = 1 : 4
    %      for ep = 1 : 4
    ci = 1;

    for ii = 1 : length(indBand{bb})
        cd(obj.data.setNames{indBand{bb}(ii)})
        cd session02
        dist = sulcus_distance;
        NeuroInfo = NeuronalChAssign;
        
        thegr = obj.data.Index(indBand{bb}(ii),10:11);
        
        chs = find(ismember(NeuroInfo.groups,thegr) ==1);
        if dist(chs(1)) > mindist && dist(chs(2)) > mindist
            %     depth{bb,ep}(ii,:) = NeuroInfo.recordedDepth(ismember(NeuroInfo.groups,thegr));
            depth{bb}(ci,:) = NeuroInfo.recordedDepth(chs);
            
            ci = ci+1;
        end
    end
    %      end
end
[r,ind] = get(obj,'Number');
clear alldepth
ci = 1;
for ii = 1 : r
    cd(obj.data.setNames{ind(ii)})
    cd session02
    dist = sulcus_distance;
    NeuroInfo = NeuronalChAssign;
    
    thegr = obj.data.Index(ind(ii),10:11);
    chs = find(ismember(NeuroInfo.groups,thegr) ==1);
    if dist(chs(1)) > mindist && dist(chs(2)) > mindist
        alldepth(ci,:) = NeuroInfo.recordedDepth(chs);
        if ~isempty(find(NeuroInfo.recordedDepth(chs) == 0))
            obj.data.setNames{ind(ii)}
        end
        ci = ci+1;
    end
end
 
figure
for bb = 2 : 4
    %     for ep = 1 : 4
    %     subplot(4,3,(ep-1)*3+bb-1)
    subplot(1,3,bb-1)
    
    plot(alldepth(:,1),alldepth(:,2),'.')
    hold on
    %    plot(depth{bb,ep}(:,1),depth{bb,ep}(:,2),'r.')
    plot(depth{bb}(:,1),depth{bb}(:,2),'r.')
    %    if ep ==1
    title(bands{bb})
    %    end
    axis([0 2500 0 2500])
    set(gca,'TickDir','out');
    %     end
end
xlabel('PPC depth (um)')
ylabel('PFC depth (um)')