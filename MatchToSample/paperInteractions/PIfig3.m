cd /Volumes/FITZGERALD/monkey

sfc = loadObject('SFCInterIDE.mat');

[r,ind1] = get(sfc,'Number','unitcortex',{'F'},'lfpcortex',{'P'},'rule',1);
[r,ind2] = get(sfc,'Number','unitcortex',{'P'},'lfpcortex',{'F'},'rule',1);
ind = union(ind1,ind2);
figure

multipeaks = [];
alphapeaks = cell(2,1);
betapeaks = cell(2,1);
gammapeaks = cell(2,1);

alphacell = cell(2,1);
betacell = cell(2,1);
gammacell = cell(2,1);

alphaSFC = cell(2,1);
betaSFC = cell(2,1);
gammaSFC = cell(2,1);
allspikes=cell(2,1);

allpeaks = nan(2,length(ind1),4,33);

bands = [7 13;14 30; 31 42];
for dir = 1 : 2
    ind = eval(sprintf('ind%d',dir));
    alphaSFC{dir} = nan(length(ind),33,4);
    betaSFC{dir} = nan(length(ind),33,4);
    gammaSFC{dir} = nan(length(ind),33,4);
    
    allspikes{dir} = zeros(length(ind),4);
    for ii = 1 : length(ind)
        load(sfc.data.setNames{ind(ii)})
        sur = load(sprintf('%sSur.mat',sfc.data.setNames{ind(ii)}(1:end-4)));
        
        cd(sfc.data.setNames{ind(ii)}(1:end-22))
        nfiles = nptDir('Nspikes*Rule1Epoch.mat');
        nspikes = load(nfiles.name);
        allspikes{dir}(ii,:) = nspikes.Nspikes;
        
        if ~isempty(C) && sum(nspikes.Nspikes > 50) == 4
            % bias correction from Grasse & Moxon 2010
            SFC = (repmat(nspikes.Nspikes',size(C,1),1) .* C - 1) ./ (repmat(nspikes.Nspikes',size(C,1),1) - 1);
        for p =1 : 4
            
            sdata = filtfilt(repmat(1/3,1,3),1,C(:,p));
            if sum(~isnan(sdata)) > 1 
                sdir = pwd;
                cd /Applications/MATLAB_R2009b.app/toolbox/signal/signal/
                [pks,locs] = findpeaks(sdata);
                %             plot(f,sdata)
                %             hold on
                %             plot(f(locs),pks,'r.')
                %             plot(f,sur.Prob(:,2,p),'--')
                %             pause
                %             clf
                if ~isempty(pks)
                    locs((pks'-sur.Prob(locs,3,p)) <= 0) = [];
                end
                if length(locs) >= 1
                    for iii = 1 : length(locs)
                        if locs(iii) >= 5 && locs(iii) <= 9
                            alphapeaks{dir} = [alphapeaks{dir} [ind(ii);p; max(pks)]];
                            
                            alphacell{dir} = [alphacell{dir};sfc.data.setNames{ind(ii)}];
                            
                            alphaSFC{dir}(ii,:,:) = SFC(1:33,:);
                        elseif locs(iii) >= 10 && locs(iii) <= 20
                            betapeaks{dir} = [betapeaks{dir} [ind(ii);p; max(pks)]];
                            betacell{dir} = [betacell{dir};sfc.data.setNames{ind(ii)}];
                            betaSFC{dir}(ii,:,:) = SFC(1:33,:);
                        elseif locs(iii) >= 21 && locs(iii) <= 28
                            gammapeaks{dir} = [gammapeaks{dir} [ind(ii);p; max(pks)]];
                            gammacell{dir} = [gammacell{dir};sfc.data.setNames{ind(ii)}];
                            gammaSFC{dir}(ii,:,:) = SFC(1:33,:);
                        
                        end
                    end
                
                    %             multipeaks = [multipeaks [ind(ii);p; max(pks)]];
                    %             plot(f,sdata)
                    %             hold on
                    %
                    %             plot(f,sur.Prob(:,2,p),'--')
                    %             title(sfc.data.setNames{ind(ii)})
                    %             pause
                    %             clf
                end
                allpeaks(dir,ii,p,:) = zeros(1,33);
                allpeaks(dir,ii,p,locs) = ones(1,length(locs));
            end
        end
        
        end
        cd(sdir)
    end
end
dirs = {'PFC unit to PPC lfp' 'PPC unit to PFC lfp'};
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
bnames = {'alpha' 'beta' 'gamma'};
cdir = {'b' 'r'};

% get the indices
alphaInd = cell(2,1);
betaInd = cell(2,1);
gammaInd = cell(2,1);
alphabeta = cell(2,1);
gammabeta = cell(2,1);
alphabetagamma = cell(2,1);
onlybeta = cell(2,1);
anypeak = cell(2,1);
for dir = 1 : 2
    alphaInd{dir} = find(~isnan(alphaSFC{dir}(:,1,1)) ==1);
    betaInd{dir} = find(~isnan(betaSFC{dir}(:,1,1)) ==1);
    gammaInd{dir} = find(~isnan(gammaSFC{dir}(:,1,1)) ==1);
    
    
    alphabeta{dir} = intersect(alphaInd{dir},betaInd{dir});
    gammabeta{dir} = intersect(gammaInd{dir},betaInd{dir});
    alphabetagamma{dir} = intersect(alphabeta{dir},gammabeta{dir});
    onlybeta{dir} = setdiff(setdiff(betaInd{dir},alphabeta{dir}),gammabeta{dir});
    anypeak{dir} = union(union(alphaInd{dir}, betaInd{dir}),gammaInd{dir});
end
figure
for dir = 1 : 2
    for p =1 :4;
        subplot(2,4,p)
        plot(f,100*nansum(squeeze(allpeaks(dir,:,p,1:33)),1)/sum(~isnan(allpeaks(dir,:,1,p))),cdir{dir})
        axis([8 50 0 2.5])
        hold on
        
        if dir == 1; title(epochs{p}); end
        set(gca,'TickDir','out')
        if p ==4; xlabel('Frequency (Hz)')
            ylabel('% sign.')
            legend('PFC unit to PPC lfp','PPC unit to PFC lfp')
        end
        subplot(2,4,p+4)
        plot(f,nanmean(betaSFC{dir}(anypeak{dir},:,p),1),cdir{dir})
        hold on
        plot(f,nanstd(betaSFC{dir}(anypeak{dir},:,p),1) +nanmean(betaSFC{dir}(anypeak{dir},:,p),1),[cdir{dir} '--'])
        plot(f,-nanstd(betaSFC{dir}(anypeak{dir},:,p),1) +nanmean(betaSFC{dir}(anypeak{dir},:,p),1),[cdir{dir} '--'])
        
        axis([8 50 0 0.07])
        set(gca,'TickDir','out')
        
        
        if p ==4; xlabel('Frequency (Hz)')
            ylabel('SFC')
            title(sprintf('PFCsua PPClfp n=%d; ; PPCsua PFClfp n=%d',length(anypeak{1}), length(anypeak{2})));
        end
    end
end




% bnames2 = {'alphaInd' 'onlybeta' 'gammaInd' 'anypeak'};
% bnames = {'alpha' 'beta' 'gamma' 'beta'};
% figure;
% for bb = 1 : 3
%     
%     set(gcf,'Name',bnames{bb})
%     thedata = eval(sprintf('%sSFC',bnames{bb}));
%     inds = eval(bnames2{bb});
%     for dir = 1 : 2
%         for p =1 :4;
%             %             subplot(2,4,(dir-1)*4+p);
%             %             plot(f,prctile(thedata{dir}(:,:,p),[10 25 50 75 90],1));
%             subplot(4,4,(bb-1)*4+p)
% %             me = nanmean(thedata{dir}(inds{dir},:,p),1);
% %             ste = nanstd(thedata{dir}(inds{dir},:,p),[],1)/ sqrt(sum(sum(isnan(thedata{dir}(inds{dir},:,p)),2) ~=33));
% %             plot(f,me,cdir{dir})
% %             hold on
% %              plot(f,me+ste,[cdir{dir} '--'])
% %             plot(f,me-ste,[cdir{dir} '--'])
%             plot(f,prctile(thedata{dir}(inds{dir},:,p),[25 50 75],1),cdir{dir})
%            hold on
%             axis([8 50 0 0.07])
%             if p == 1;  ylabel(bnames{bb}); end
%             if bb == 1; title(epochs{p}); end
%         end
%     end
%     
%      title(sprintf('PFC unit to PPC lfp; n=%d; PPC unit to PFC lfp; n=%d',sum(sum(isnan(thedata{1}(:,:,p)),2) ~=33), sum(sum(isnan(thedata{2}(:,:,p)),2) ~=33)));
%   
% end
% 
% xlabel('Frequency (Hz)')
% ylabel('SFC')
% 
% 
% 
% mlpeaks = unique(multipeaks(1,:));





%% alpha and beta and gamma
alphabetaSFC = cell(2,1);
gammabetaSFC = cell(2,1);
ialphabeta = cell(2,1);
igammabeta = cell(2,1);
for dir = 1 : 2
[alphabetaSFC{dir},~,ialphabeta{dir}] = intersect(alphapeaks{dir}(1,:),betapeaks{dir}(1,:));


[gammabetaSFC{dir},~,igammabeta{dir}] = intersect(gammapeaks{dir}(1,:),betapeaks{dir}(1,:));
length(intersect(intersect(alphapeaks{dir}(1,:),betapeaks{dir}(1,:)),gammapeaks{dir}(1,:)))% number of pairs for all abnds
end

for dir = 1 : 2
alphabetaUcells{dir} = unique(betacell{dir}(ialphabeta{dir},1:end-21),'rows');
gammabetaUcells{dir} = unique(betacell{dir}(igammabeta{dir},1:end-21),'rows');
alphabetaSUA(dir) = length(strfind(alphabetaUcells{dir}(:,end-1)','s'));
gammabetaSUA(dir) = length(strfind(gammabetaUcells{dir}(:,end-1)','s'));

allbands{dir} = unique(betacell{dir}(intersect(ialphabeta{dir},igammabeta{dir}),1:end-21),'rows');
allbandsSUA(dir) = length(strfind(allbands{dir}(:,end-1)','s'));
end



CAB = cell(4,2);
CGB = cell(4,2);
for dir = 1 : 2
    for ii = 1 : length(alphabetaSFC{dir})
        load(sfc.data.setNames{alphabetaSFC{dir}(ii)})
        sur = load(sprintf('%sSur.mat',sfc.data.setNames{alphabetaSFC{dir}(ii)}(1:end-4)));
        cd /Applications/MATLAB_R2009b.app/toolbox/signal/signal/
        
        for p = 1 : 4
            sdata = filtfilt(repmat(1/3,1,3),1,C(:,p));
            [pks,locs] = findpeaks(sdata);
            if ~isempty(locs)
                locs((pks'-sur.Prob(locs,2,p)) <= 0) = [];
                CAB{p,dir} = [CAB{p,dir} locs];
            end
        end
    end
    
    for ii = 1 : length(gammabetaSFC{dir})
        load(sfc.data.setNames{gammabetaSFC{dir}(ii)})
        sur = load(sprintf('%sSur.mat',sfc.data.setNames{gammabetaSFC{dir}(ii)}(1:end-4)));
        cd /Applications/MATLAB_R2009b.app/toolbox/signal/signal/
        for p = 1 : 4
            sdata = filtfilt(repmat(1/3,1,3),1,C(:,p));
            [pks,locs] = findpeaks(sdata);
            if ~isempty(locs)
                locs((pks'-sur.Prob(locs,2,p)) <= 0) = [];
                CGB{p,dir} = [CGB{p,dir} locs];
            end
        end
    end
end
figure
dirLab = {'-' '-.'};
for dir = 1 : 2
    for p = 1 : 4
        subplot(2,4,p)
        %     plot(f,mean(CAB(:,:,p),1))
        %     hold on
        %     plot(f,mean(CAB(:,:,p),1)+ std(CAB(:,:,p),1)/sqrt(size(CAB,1)),'--')
        %     plot(f,mean(CAB(:,:,p),1)- std(CAB(:,:,p),1)/sqrt(size(CAB,1)),'--')
        
        [n,xout]=hist(CAB{p,dir},[1:65]);
        plot(f,n,dirLab{dir})
        hold on
        axis([8 45 0 45])
        title(epochs{p})
        if p == 1; ylabel('alpha and beta peaks'); end
        set(gca,'TickDir','out')
        subplot(2,4,4+p)
        %     plot(f,mean(CGB(:,:,p),1))
        %     hold on
        %     plot(f,mean(CGB(:,:,p),1)+ std(CGB(:,:,p),1)/sqrt(size(CGB,1)),'--')
        %     plot(f,mean(CGB(:,:,p),1)- std(CGB(:,:,p),1)/sqrt(size(CGB,1)),'--')
        
        [n,xout]=hist(CGB{p,dir},[1:65]);
        plot(f,n,dirLab{dir})
        hold on
        axis([8 50 0 15])
        if p == 1; ylabel('gamma and beta peaks'); end
        set(gca,'TickDir','out')
    end
end
xlabel('Frequency (Hz)')
ylabel('# spectral peaks')
legend('PFCunit PPC lfp','PPCunit PFC lfp')

%% viewing
% figure
% for ii = 1 : length(gammabetaSFC)
%     load(sfc.data.setNames{gammabetaSFC(ii)})
%     sur = load(sprintf('%sSur.mat',sfc.data.setNames{gammabetaSFC(ii)}(1:end-4)));
%     for p =1 : 4
%         subplot(1,4,p)
%         plot(f,C(:,4))
%         hold on
%         
%         plot(f,squeeze(sur.Prob(:,2,p)),'--')
%         
%         
%     end
%     pause
%     clf
% end

%% ex betty/090918/session01/group0048/cluster01s/SFCallg0012Rule1
