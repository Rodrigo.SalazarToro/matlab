function PIfig3_4

cd /Volumes/raid/data/monkey/

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,indbeta] = get(obj,'Number','type',{'betapos34' [1 3]});r


bands = {'theta' 'alpha' 'beta' 'gamma'};
bepoch = {[1:4] [1:4] [3 4] [1:4]};
bandPeak = zeros(1,4);
ind = cell(4,1);
for bb = [ 2 4]
%     figure
    set(gcf,'Name',bands{bb})
    
    [bandPeak(bb),ind{bb}] = get(obj,'Number','type',{'betapos34' [1 3]},'sigSur',{bands{bb} bepoch{bb} [1 3]});
%     [pair,sind,alldata,tv,f] = mkAveCohgram(obj,ind{bb},'frange',[8 50],'clim',0.2);
%     eval(sprintf('%s{1} = alldata{1,1}(:,1:161,:);',bands{bb}));
%     eval(sprintf('%s{2} = alldata{1,2}(:,81:end,:);',bands{bb}));
%     time{1} = tv{1,1}(1:161);
%     time{2} = tv{1,1}(81:end);
end

purebeta = setdiff(indbeta,[ind{2} ind{4}]);
% [pair,sind,alldata,tv,f] = mkAveCohgram(obj,purebeta,'frange',[2 42],'clim',0.2);
beta{1} = alldata{1,1}(:,1:161,:);
beta{2} = alldata{1,2}(:,81:end,:);

[Sall,Sallnorm] = mkAvepower(obj,purebeta);

figure;
clims = {[0.05 0.19] [0.05 0.19] [0.05 0.19]};
for bb =2  : 4
    for al = 1 : 2
        subplot(3,2,(bb-2)*2+al)
       
        eval(sprintf('thedata = squeeze(median(%s{al}));',bands{bb}))
        imagesc(time{al},f,thedata')
        colorbar
         title(sprintf('%s; n=%d',bands{bb},eval(sprintf('size(%s{1},1)',bands{bb}))))
         caxis(clims{bb-1})
         ylim([8 50])
         if al == 2; xlim([1 1.8]); end
    end
end
xlabel('Time (ms)')
ylabel('Frequency (Hz)')

%% both alpha beta and gamma

[rr,indbg] = get(obj,'Number','type',{'betapos34' [1 3]},'sigSur',{'alpha' [1:4] [1 3]; 'gamma' [1:4] [1 3]});  
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,indbg,'frange',[8 50],'clim',0.2);
for sb = 1 : 8; subplot(4,2,sb);ylim([8 50]);caxis([0.05 0.18]);set(gca,'TickDir','out');end
%%
thetaind = setdiff(ind{1},union(ind{2},ind{4}));
alphaind = setdiff(ind{2},union(ind{1},ind{4}));
gammaind = setdiff(ind{4},union(ind{1},ind{2}));


figure
 set(gcf,'Name','theta')
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,thetaind,'frange',[8 50],'clim',0.2,'mean');
clf
cd /Applications/MATLAB_R2009b.app/toolbox/signal/signal/
[pks,locs] = findpeaks(alldata);

figure
 set(gcf,'Name','alpha')
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,alphaind,'frange',[8 50],'clim',0.2,'mean');
figure
 set(gcf,'Name','gamma')
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,gammaind,'frange',[8 50],'clim',0.2,'mean');


%227   304   695    90
%%  beta pairs

[fband,table,tableN,band] = summary_table(obj,'frange',[8 50],'redo','type',1,'bands',3,'combineAll');

% stat on phase
a = load('cohIntercohInter2betaAllSorted.mat');
mphase =cell(4,1);
pval = zeros(4,1);
meanpop = zeros(4,1);
for p = 1 : 4
    phase1 = a.alldata{p,1}(10:20,:);
    sigind = find(sum(phase1,1) ~= 0);
    sigphase1 = phase1(:,sigind);
    
    for ii = 1 : size(sigphase1,2)
        notz = find(sigphase1(:,ii) ~= 0);
        mphase{p}(ii) = circ_mean(sigphase1(notz,ii));
    end
    
    meanpop(p) = circ_mean(mphase{p},[],2);
    pval(p) = circ_mtest(mphase{p},meanpop(p));
end

[pval, fstat] = circ_ktest(mphase{1},mphase{2});
[pval, k, K] = circ_kuipertest(mphase{3},mphase{4});
% stat on spectral peaks

b=load('cohInter2betaAllSortedPeaks.mat');


for bf = 1 : 2;
betapeaks{bf} = find(b.allpeaks{bf+2} >= 14 & b.allpeaks{bf+2} <= 30);

end

[h,p] = kstest2(betapeaks{1},betapeaks{2},0.05,'smaller');



%% example of alpha and betpos34

% 10/0108 gr 37 and 5 pair64 and n=380 in obj or gr 36 5 or gr 37 4 or gr
% 41 5 or gr 45 21 or gr 46 5
% 10014 gr 36 and 5
% 100115 gr 46 2

cd /Volumes/FITZGERALD/monkey/betty/100114/grams/
figure

load('spectrogramgroup0036Rule1.mat') 
for al = 1 : 2
subplot(2,2,al)
% imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(5^2))
imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(5^2))
ylim([8 50])
caxis([0.06 0.135])
title('spectrogramgroup0036Rule1')
colorbar
set(gca,'TickDir','out')
end

load('spectrogramgroup0005Rule1.mat') 
for al = 1 : 2
subplot(2,2,al+2)
% imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(5^2))
imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(5^2))
ylim([8 50])
caxis([0.06 0.11])
title('spectrogramgroup0005Rule1')
colorbar
set(gca,'TickDir','out')
end


xlabel('Time (s)')
ylabel('Frequency (Hz)')

%% alpha and beta
InspectGUI(obj,'type',{'betapos34' [1 3]},'sigSur',{'alpha' [1:4] [1 3]},'plotType','TimeFreq','gram','coh')
[r,ind] = get(obj,'Number','type',{'betapos34' [1 3]},'sigSur',{'alpha' [1:4] [1 3]});
alphaind = [15 21:23 27:29 36 38:40 49 56:59 63 64 67:68 70:72 78 81:83 88 91:95 98 100 104 107:109 111 118 124:126 130:133 137:139 142 149 151 154 156:160 162 166 168 169 209 243 255:257 267 274 279 282 293 295 298:304];
figure
 set(gcf,'Name','alpha')
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,ind(alphaind),'frange',[8 50],'clim',0.2,'mean');

[Sall,Sallnorm,t,f,fnorm] = mkAvepower(obj,ind(alphaind),'plot');



%% gamma and beta

InspectGUI(obj,'type',{'betapos34' [1 3]},'sigSur',{'gamma' [1:4] [1 3]},'plotType','TimeFreq','gram','coh')
[r,ind] = get(obj,'Number','type',{'betapos34' [1 3]},'sigSur',{'gamma' [1:4] [1 3]});
% gammaind = [1:3  5:19 24:25 31:34 36:37 40 45 48 49 52:58 61:65 71:79 81:82 88];
gammaind = [2  5:15 17:19 24:25 45 54:55 71 77:79 81:82];
gammaindClark = [2  5:15 17:19];% 45 54:55 71 77:79 81:82];
goodPair = [5:13];
figure
 set(gcf,'Name','gamma and beta')
[pair,sind,alldata,tv,f] = mkAveCohgram(obj,ind(gammaind),'frange',[8 50],'clim',0.2,'mean');

[Sall,Sallnorm,f,t,fnorm] = mkAvepower(obj,ind(goodPair),'plot');


% % 060503 g 2 and 15
% cd /Volumes/FITZGERALD/monkey/clark/060503/grams/
% figure
% 
% load('spectrogramgroup0002Rule1.mat') 
% for al = 1 : 2
% subplot(2,2,al)
% % imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(4^2))
% imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(4^2))
% ylim([8 50])
% caxis([0.1 0.19])
% title('spectrogramgroup0002Rule1')
% colorbar
% set(gca,'TickDir','out')
% end
% 
% load('spectrogramgroup0015Rule1.mat') 
% for al = 1 : 2
% subplot(2,2,al+2)
% % imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(4^2))
% imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(4^2))
% ylim([8 50])
% caxis([0.1 0.12])
% title('spectrogramgroup0015Rule1')
% colorbar
% set(gca,'TickDir','out')
% end
% 
% 
% xlabel('Time (s)')
% ylabel('Frequency (Hz)')


%% exmaple of betapos34 and gamma
% 060602 gr 8 and 12

cd /Volumes/FITZGERALD/monkey/clark/060602/grams/
figure

load('spectrogramgroup0008Rule1.mat') 
for al = 1 : 2
subplot(2,2,al)
% imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(4^2))
imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(4^2))
ylim([8 50])
caxis([0.1 0.18])
title('spectrogramgroup0008Rule1')
colorbar
set(gca,'TickDir','out')
end

load('spectrogramgroup0012Rule1.mat') 
for al = 1 : 2
subplot(2,2,al+2)
% imagesc(t{al},f,(repmat(f',1,length(t{al})).*(squeeze(nanmean(S{al},3))'))/(4^2))
imagesc(t{al},f,log10(squeeze(nanmean(S{al},3))')/(4^2))
ylim([8 50])
caxis([0.1 0.133])
title('spectrogramgroup0012Rule1')
colorbar
set(gca,'TickDir','out')
end


xlabel('Time (s)')
ylabel('Frequency (Hz)')

gammaind = [];


function [Sall,Sallnorm] = mkAvepower(obj,inds)

Sall = cell(2,2);
Sall1f = cell(2,2);
Sallnorm = cell(2,2);
% SallnormSTD = cell(2,2);
indCH = [];
c= [1 1];

for ii = 1 : length(inds)
    cd(sprintf('%s/grams/',obj.data.setNames{inds(ii)}))
    for ch = 1 : 2
        if ismember(obj.data.Index(inds(ii),[2 9+ch]),indCH,'rows') ==0
            indCH = [indCH; obj.data.Index(inds(ii),[2 9+ch])];
            
            
            load(sprintf('spectrogramgroup%04.0fRule1.mat',obj.data.Index(inds(ii),9+ch)))
            for al = 1 : 2
                Sall{al,ch}(c(ch),:,:) = squeeze(median(S{al},3))'/(5^2);
                Sall1f{al,ch}(c(ch),:,:) = (repmat(f,[size(S{al},1) 1]).*squeeze(nanmean(S{al},3)))';
                Sallnorm{al,ch}(c(ch),:,:) = squeeze(prctile(S{al},95,3))'./ median(median(median(S{al})));
%                 SallnormSTD{al,ch}(c(ch),:,:) = squeeze(std(S{al},[],3))'./ median(median(median(S{al})));
                %             Sall{al}(ch,ii,:,:) = squeeze(nanmean(S{al},3))' ./ mean(mean(mean(S{al})));
                %             me=nanmean(S{al},3);
                %             maxme = repmat(max(me,[],1),[size(S{al},1) 1]);
                %             Sall{al}(ch,ii,:,:) = me ./ maxme;
            end
            c(ch) = c(ch)+ 1;
        end
    end
end


% figure
% 
% for ii = 1: size(Sall{1,1},2); 
%     subplot(2,1,1)
%     imagesc(t{al},f,squeeze(Sallnorm{1,1}(ii,:,:)))
%     colorbar
%     ylim([0 50]);
%     caxis([0 50])
%     subplot(2,1,2)
%     imagesc(t{al},f,squeeze(Sallnorm{1,2}(ii,:,:)))
%     colorbar
%     ylim([0 50]);
%     caxis([0 50])
% %     pause
% 
% end
 figure
 
 chlab = {'PPC' 'PFC'};
 for ch = 1 : 2
     for al = 1 : 2
         meanAmp = median(median(median(Sall{al,ch}(:,:,:))));
         
         subplot(4,2,(ch-1)*2+ al)
         imagesc(t{al},f,squeeze(median(Sall{al,ch}(:,:,:),1)))
         ylim([8 50])
         if ch ==1; caxis([1 45]); else caxis([1 10]);end
         colorbar
         set(gca,'TickDir','out')
       
         fnorm = squeeze(median(Sallnorm{al,ch}(:,:,:),1)) .* repmat(vecc(f), 1,size(Sallnorm{al,ch},3)); 
         
         title(sprintf('median %s; n=%d',chlab{ch},size(Sallnorm{al,ch},1)))
         if al == 1; ylabel('Absolute power (uV^2)'); end
         subplot(4,2,(ch-1)*2+ al+4)
         imagesc(t{al},f,fnorm)
         ylim([8 50])
         colorbar
         if ch ==1; caxis([100 1000]); else caxis([100 240]);end
         set(gca,'TickDir','out')
         title(sprintf('median %s; n=%d',chlab{ch},size(Sallnorm{al,ch},1)))
         if al == 1; ylabel('Norm. power'); end
     end
 end
xlabel('Time (s)')
ylabel('Frequency (Hz)')
