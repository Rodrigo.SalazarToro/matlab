%% stimulus type AC

cd /media/raid/data/monkey/betty/
% cd /Volumes/raid/data/monkey/clark
sdir = pwd;
% load idedays
% load longdays
% days = [idedays lcdays lbdays];
% ldays = load('locdays');
% days = [days ldays.days];
% days = idedays;
% betty
% rule = 1;
load switchdays
parfor d = 1 : length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    if ~isempty(nptDir('session01'))
        cd session01
        mts = mtstrial('auto','redosetNames');
        nbl = length(find(diff(mts.data.Index(:,1)) ~= 0)) + 1;
        [st,allfiles] = unix('find . -name ''ispikes.mat''');
        stfiles = findstr(allfiles,'ispikes.mat');
        for bl = 1 : nbl
            alltrials = mtsgetTrials(mts,'stable','BehResp',1,'block',bl);
            for fi = 1 : length(stfiles)
                cd(sprintf('%s/%s/session01/%s',sdir,days{d},allfiles(stfiles(fi)-21:stfiles(fi) -2)))
                sp = load(sprintf('%s/%s/session01/%s',sdir,days{d},allfiles(stfiles(fi)-21:stfiles(fi) +10)));
                %             for obj = 1 : 3
                %                 for loc = 1 : 3
                %                     trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',rule,'iCueObj',obj,'iCueLoc',loc);
                %                     if ~isempty(trials)
                %                         ntoadd = sprintf('Rule%dIde%dLoc%d',rule,obj,loc);
                %                         [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                %                         %                     [crosscorr,timelag] = mkxcorrMTS(sp.sp,trials,mts,ntoadd,'Rule1','save','epochBased','surrogate');
                %                     end
                %                 end
                %             end
                %                 ntoadd = sprintf('Rule%d',rule);
                ntoadd = sprintf('Block%d',bl);
                cd ../..
                cd xcorr
                
                [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,alltrials,mts,'addName',ntoadd,'save','epochBased','redo');
                %                 [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,alltrials,mts,'addName',ntoadd,'save','epochBased','surrogate','surType','spikeTime');
            end
        end
    end
end

cd /media/raid/data/monkey/clark
sdir = pwd;
load idedays
ldays = load('locdays');
days = [idedays ldays.days];

for d = 1 : length(days)
    
    cd(sprintf('%s%s%s',sdir,filesep,days{d}))
    nses = length(nptDir('session99'));
    for rule = 1 : 2
        cd('session99')
        mtst = mtstrial('auto','redoSetNames');
        alltrials = mtsgetTrials(mtst,'stable','BehResp',1,'rule',rule,'noRejNoisyT');
        if ~isempty(alltrials)
            fid = fopen(['C:\cygwin64\home\salazarr\tempCommand2.sh'],'w');
                fprintf(fid,'cd /cygdrive/D/workingmemory/data/monkey/betty/%s/session99; find . -ipath ''*cluster*'' -name ''ispikes.mat'' -print',days{d});
                fclose(fid);
                [st,allfiles] = system('C:\cygwin64\bin\bash --login -c "/home/salazarr/tempCommand2.sh"');
%             [st,allfiles] = unix('find . -name ''ispikes.mat''');
            stfiles = findstr(allfiles,'ispikes.mat');
            for fi = 1 : length(stfiles)
                cd(sprintf('%s%s%s%ssession99%s%s',sdir,filesep,days{d},filesep,filesep,allfiles(stfiles(fi)-21:stfiles(fi) -2)))
                sp = load(sprintf('%s%s%s%ssession99%s%s',sdir,filesep,days{d},filesep,filesep,allfiles(stfiles(fi)-21:stfiles(fi) +10)));
%                 for obj = 1 : 3
%                     for loc = 1 : 3
%                         trials = mtsgetTrials(mtst,'stable','BehResp',1,'rule',rule,'iCueObj',obj,'iCueLoc',loc,'noRejNoisyT');
%                         
%                         ntoadd = sprintf('Rule%dIde%dLoc%d',rule,obj,loc);
%                         cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
%                         [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,trials,mtst,'addName',ntoadd,'save','epochBased');
%                     end
%                 end
                ntoadd = sprintf('Rule%d',rule);
                cd(sprintf('%s%s%s%ssession99%s%s',sdir,filesep,days{d},filesep,filesep,allfiles(stfiles(fi)-21:stfiles(fi) -2)))
%                 [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,alltrials,mtst,'addName',ntoadd,'save','epochBased');
%                 [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,alltrials,mtst,'addName',ntoadd,'save','epochBased','surrogate','surType','spikeTime');
                [crosscorr,timelag] = mkxcorrMTS(sp.sp,sp.sp,alltrials,mtst,'addName',ntoadd,'save','epochBased','surrogate','surType','trial');
                
            end
        end
        
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end

%% stimulus type for crosscorr


% cd /media/raid/data/monkey/betty/
cd /media/raid/data/monkey/betty
sdir = pwd;
load idedays
load longdays
days = [idedays lcdays lbdays];
days = days([1:37 39:48]);

days = idedays;
% betty

load switchdays

cor = {'Incor' ''};
rule = 2;
BehResp = 1; % watch out if = 1 have to add 'stable' args
for d = 1 :length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    if ~isempty(nptDir('session01'))
        cd session01
        mts = mtstrial('auto','redosetNames');
        trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule);
        
        %         [st,allfiles] = unix('find . -name ''ispikes.mat''');
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        stfiles = findstr(allfiles,'ispikes.mat');
        
        iscomb = nchoosek([1:length(stfiles)],2);
        auto = [(1:length(stfiles))' (1:length(stfiles))'];
        iscomb = cat(1,iscomb,auto);
        ! mkdir xcorr
        parfor cb = 1 : size(iscomb,1)
            fi1 = iscomb(cb,1);
            fi2 = iscomb(cb,2);
            cd(sprintf('%s/%s/session01/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
            sp1 = load(sprintf('%s/%s/session01/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
            sp2 = load(sprintf('%s/%s/session01/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
            for obj = 1 : 3
                for loc = 1 : 3
                    
                    %                     if ~isempty(trials)
                    
                    ntoadd = sprintf('Rule%dIde%dLoc%d%s',rule,obj,loc,cor{BehResp+1});
                    cd(sprintf('%s/%s/session01',sdir,days{d}))
                    trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule,'iCueObj',obj,'iCueLoc',loc);
                    
                    
                    cd(sprintf('%s/%s/session01/xcorr',sdir,days{d}))
                    [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                    %                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
                    try
                        [crosscorr,timelag,S,S1,S2,phi] = mkXCspikesSurMIevoked(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                    catch
                        [crosscorr,timelag,S,S1,S2,phi] = mkXCspikesSurMIevoked(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','redo');
                    end
                    %                     end
                end
            end
        end
    end
end

cd /media/raid/data/monkey/clark
% cd /Volumes/raid/data/monkey/clark

% load locdays
% load histdays
% days = intersect(days,histdays);
% days = idedays;
% load oldswitchdays
load switchdays
sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
% clark
parfor d = 1: length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    
    
    cd('session99' )
    mts = mtstrial('auto','redosetNames');
    for rule = 1 : 2
        trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule,'noRejNoisyT');
        if ~isempty(trials)
            [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
            if ~isempty(allfiles)
                stfiles = findstr(allfiles,'ispikes.mat');
                iscomb = nchoosek([1:length(stfiles)],2);
                auto = [(1:length(stfiles))' (1:length(stfiles))'];
                iscomb = cat(1,iscomb,auto);
                ! mkdir xcorr
                for cb = 1: size(iscomb,1)
                    fi1 = iscomb(cb,1);
                    fi2 = iscomb(cb,2);
                    cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                    sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                    sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                    for obj = 1 : 3
                        for loc = 1 : 3
                            cd(sprintf('%s/%s/session99',sdir,days{d}))
                            trials = mtsgetTrials(mts,'BehResp',BehResp,'rule',rule,'iCueObj',obj,'iCueLoc',loc,'noRejNoisyT');
                            
                            ntoadd = sprintf('Rule%dIde%dLoc%d%s',rule,obj,loc,cor{BehResp+1});
                            
                            cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                            [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                                                    [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
                            try
                                [crosscorr,timelag,S,S1,S2,phi] = mkXCspikesSurMIevoked(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                            catch
                                [crosscorr,timelag,S,S1,S2,phi] = mkXCspikesSurMIevoked(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','redo');
                            end
                        end
                    end
                end
            end
        end
    end
    cd(sprintf('%s/%s',sdir,days{d}))
    
end


%% MI on XC spikes
cd /media/raid/data/monkey/clark
sdir = pwd;
load idedays
days = idedays;
load switchdays

% clark
parfor d = 1 : length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    cd session99
    for rule =  1:2
        
        if ~isempty(nptDir('xcorr'))
            cd xcorr
            files = nptDir(sprintf('xcorr*Rule%d*.mat',rule));
            basefile = zeros(length(files),21);
            for fi = 1 : length(files)
                basefile(fi,:) = files(fi).name(1:21);
                
            end
            
            upairs = unique(basefile,'rows');
            
            units1 = upairs(:,7:13);
            units2 = upairs(:,15:end);
            
            for np = 1 : size(upairs,1)
                
                [mi,timelag] = mkXCspikesMI(units1(np,:),units2(np,:),'save','rule',rule);
                
                
                [mi,timelag] = mkXCspikesMI(units1(np,:),units2(np,:),'save','SurMIevoked','rule',rule);
                
            end
            cd ..
            
        end
        
        cd ..
    end
    cd ..
end



cd /media/raid/data/monkey/betty/
% cd /Volumes/raid/data/monkey/betty
sdir = pwd;
load idedays
load longdays
days = [idedays lcdays lbdays];
days = days([1:37 39:48]);
sdays = [28 31];
days = idedays;
rule=2;
parfor d = 1 : length(days)
    %     d = sdays(dd);
    cd(sprintf('%s/%s',sdir,days{d}))
    if ~isempty(nptDir('session01'))
        cd session01
        cd xcorr
        files = nptDir(sprintf('xcorr*Rule%d*.mat',rule));
        basefile = zeros(length(files),21);
        for fi = 1 : length(files)
            basefile(fi,:) = files(fi).name(1:21);
            
        end
        
        upairs = unique(basefile,'rows');
        
        units1 = upairs(:,7:13);
        units2 = upairs(:,15:end);
        
        for np = 1 : size(upairs,1)
            
            [mi,timelag] = mkXCspikesMI(units1(np,:),units2(np,:),'save','rule',rule);
            [mi,timelag] = mkXCspikesMI(units1(np,:),units2(np,:),'save','SurMIevoked','rule',rule);
        end
        cd ../..
        
    end
    
    cd ..
end


%% XC on all trials
cd /media/raid/data/monkey/clark
% cd /Volumes/raid/data/monkey/clark

% load idedays
% days = idedays;
load switchdays

sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
% clark
for d =1: length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    
    %     for s = 2 : nses
    %         cd(['session0' num2str(s)])
    for rule = 1 : 2
        cd session99
        mts = mtstrial('auto','redosetNames');
        mkdir xcorr
        trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule,'noRejNoisyT');
%         trials = mtsgetTrials(mts,'rule',rule,'noRejNoisyT');
        if ~isempty(trials)
            [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
            if ~isempty(allfiles)
                stfiles = findstr(allfiles,'ispikes.mat');
                cross = nchoosek([1:length(stfiles)],2);
                auto = [(1:length(stfiles))' (1:length(stfiles))'];
                iscomb = cat(1,cross,auto);
                for cb = 1 : size(iscomb,1)
                    fi1 = iscomb(cb,1);
                    fi2 = iscomb(cb,2);
                    cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                    sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                    sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                    ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
%                     ntoadd = sprintf('Rule%dAlltrials',rule);
                    cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                    if cb <= size(cross,1)
                        %                         mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
                        %                         mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
                        %
                        %                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'redo','save');
                        %                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save','surrogate');
                        %                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save','xcorr');
                        %                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save','xcorr','surrogate');
%                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',[ntoadd 'epoch'],'save','xcorr','binSize',500,'xcorrBin',500);
%                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',[ntoadd 'epoch'],'save','xcorr','surrogate','binSize',500,'xcorrBin',500);
                        
                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'redo','xcorr','binSize',500,'xcorrBin',500,'plot');
                        %                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
                    else
                        %                         mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','AC');
                        %                         mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate','AC');
                    end
                end
            end
        end
        
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end


cd /media/raid/data/monkey/betty

% load idedays
% % load longdays
% % days = [idedays lcdays lbdays];
% % tdays = days([1:37 39:48]);
% load locdays
%
% days = [idedays(1:27) locdays];
% days = unique( days);
% days = idedays;
% betty

% load switchdays
% sdir = pwd;
% cor = {'Incor' ''};
% rule = 1;
% BehResp = 1; % watch out if = 1 have to add 'stable' args
% for d = 1 : length(days)
%     
%     cd(sprintf('%s/%s',sdir,days{d}))
%     if ~isempty(nptDir('session99'))
%         cd session99
%         mts = mtstrial('auto','redosetNames');
%         nbl = length(find(diff(mts.data.Index(:,1)) ~= 0)) + 1;
%         [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
%         stfiles = findstr(allfiles,'ispikes.mat');
%         
%         cross = nchoosek([1:length(stfiles)],2);
%         auto = [(1:length(stfiles))' (1:length(stfiles))'];
%         iscomb = cat(1,cross,auto);
%         
%         nb = length(find(diff(mts.data.Index(:,1)) ~=0));
%         for rule = 1 : 2
%             %           for bl = 1 : nb+1
%             trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',rule,'noRejNoisyT');
%             %                         trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'block',bl);
%             [st,allfiles] = unix('find . -name ''ispikes.mat''');
%             
%             ! mkdir xcorr
%             for cb = 1 : size(iscomb,1)
%                 fi1 = iscomb(cb,1);
%                 fi2 = iscomb(cb,2);
%                 cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
%                 sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
%                 sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
%                 
%                 if ~isempty(trials)
%                     ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
%                     %                                         ntoadd = sprintf('Block%d%s',bl,cor{BehResp+1});
%                     cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
%                     if cb <= size(cross,1)
%                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased');
%                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
%                         spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save');
%                          spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save','signalCor');
%                         %                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','surrogate');
%                     else
%                         [crosscorr,timelag] = mkxcorrMTS(sp1.sp,sp2.sp,trials,mts,'addName',ntoadd,'save','epochBased','AC');
%                         
%                     end
%                 end
%             end
%         end
%     end
% end

%% phase locking
dpath = {'/Volumes/raid/data/monkey/clark' '/Volumes/raid/data/monkey/betty/acute' '/Volumes/raid/data/monkey/betty'};
for dd = 1 : 3
cd(dpath{dd})
load switchdays

sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
% clark
for d = 1 : length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    
    for rule = 1 : 2
        cd session99
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        if ~isempty(allfiles)
            stfiles = findstr(allfiles,'ispikes.mat');
            iscomb = nchoosek([1:length(stfiles)],2);
            
            parfor cb = 1 : size(iscomb,1)
                fi1 = iscomb(cb,1);
                fi2 = iscomb(cb,2);
                
                sp1 = load(sprintf('%s/%s/session99/%snineStimPSTH%d.mat',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1)-1),rule));
                sp2 = load(sprintf('%s/%s/session99/%snineStimPSTH%d.mat',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2)-1),rule));
                
                cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
              
                ntoadd = sprintf('Rule%d%sSampleLock',rule,cor{BehResp+1});
                raster1 = cat(1,cell2mat(sp1.A(:,1)));
                raster2 = cat(1,cell2mat(sp2.A(:,1)));
                name1 = allfiles([stfiles(fi1)-21 stfiles(fi1)-16:stfiles(fi1)-13 stfiles(fi1)-4:stfiles(fi1)-2]);
                name2 = allfiles([stfiles(fi2)-21 stfiles(fi2)-16:stfiles(fi2)-13 stfiles(fi2)-4:stfiles(fi2)-2]);
                MTSphaseDiff([name1 name2],raster1,raster2,'addName',ntoadd,'save','redo');
               
                  
                ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
               raster1 = cat(1,cell2mat(sp1.A(:,2)));
                raster2 = cat(1,cell2mat(sp2.A(:,2)));
                MTSphaseDiff([name1 name2],raster1,raster2,'addName',ntoadd,'save','redo');
                
            end
        end
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end
end
cd /Volumes/raid/data/monkey/betty

load switchdays

sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
% clark
parfor d = 1: length(days)
    
    cd(sprintf('%s/%s',sdir,days{d}))
    
    for rule = 1 : 2
        cd session99
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        if ~isempty(allfiles)
            stfiles = findstr(allfiles,'ispikes.mat');
            iscomb = nchoosek([1:length(stfiles)],2);
            
            for cb = 1 : size(iscomb,1)
                fi1 = iscomb(cb,1);
                fi2 = iscomb(cb,2);
                
                sp1 = load(sprintf('%s/%s/session99/%snineStimPSTH%d.mat',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1)-1),rule));
                sp2 = load(sprintf('%s/%s/session99/%snineStimPSTH%d.mat',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2)-1),rule));
                cd(sprintf('%s/%s/session01',sdir,days{d}))
                
                ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
               
                cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                name1 = allfiles([stfiles(fi1)-21 stfiles(fi1)-16:stfiles(fi1)-13 stfiles(fi1)-4:stfiles(fi1)-2]);
                name2 = allfiles([stfiles(fi2)-21 stfiles(fi2)-16:stfiles(fi2)-13 stfiles(fi2)-4:stfiles(fi2)-2]);
                
                raster1 = cat(1,cell2mat(sp1.A(:,2)));
                raster2 = cat(1,cell2mat(sp2.A(:,2)));
                MTSphaseDiff([name1 name2],raster1,raster2,'addName',ntoadd,'save');
                
                
                ntoadd = sprintf('Rule%d%sSampleLock',rule,cor{BehResp+1});
                raster1 = cat(1,cell2mat(sp1.A(:,1)));
                raster2 = cat(1,cell2mat(sp2.A(:,1)));
                MTSphaseDiff([name1 name2],raster1,raster2,'addName',ntoadd,'save');
                
            end
        end
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end


%% compare spike count correlations

load switchdays

sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
nses = '99';
% clark
for d = 24: length(days)
    cd(sprintf('%s/%s',sdir,days{d}))
    cd(sprintf('session%s',nses))
    mts = mtstrial('auto','redosetNames');
    trials1 = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',1,'noRejNoisyT');
    trials2 = mtsgetTrials(mts,'stable','BehResp',BehResp,'rule',2,'noRejNoisyT');
    if ~isempty(trials1) || ~isempty(trials2)
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        if ~isempty(allfiles)
            stfiles = findstr(allfiles,'ispikes.mat');
            cross = nchoosek([1:length(stfiles)],2);
            iscomb = cross;
            parfor cb = 1 : size(iscomb,1)
                fi1 = iscomb(cb,1);
                fi2 = iscomb(cb,2);
                cd(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                sp1 = load(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                sp2 = load(sprintf('%s/%s/session%s/%s',sdir,days{d},nses,allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                cd(sprintf('%s/%s/session%s/xcorr',sdir,days{d},nses))
%                [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save');
%                 [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save','xcorr');
                [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1.sp,sp2.sp,mts,trials1,trials2,'save','xcorr','binSize',500,'addName','500ms');
            end
        end
    end
end

%% compute spike corr for performance

load switchdays
sdir = pwd;
BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
for d =17:length(days)
    cd(sprintf('%s/%s',sdir,days{d}))
    for rule = 1 : 2
        cd session99
        mts = mtstrial('auto','redosetNames');
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        stfiles = findstr(allfiles,'ispikes.mat');
        cross = nchoosek([1:length(stfiles)],2);
        iscomb = cross;
        alltrials = find(mts.data.Index(:,1) == rule);
        
        parfor cb = 1 : size(iscomb,1)
            fi1 = iscomb(cb,1);
            fi2 = iscomb(cb,2);
            cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
            sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
            sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
            addName = ['Rule' num2str(rule) '50Tr'];
            cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
%             xcorrSpikeTrialBased(alltrials,sp1,sp2,mts,'addName',addName,'save','nTrials',50)
             xcorrSpikeTrialBased(alltrials,sp1,sp2,mts,'addName',[addName 'epoch'],'save','nTrials',50,'xcorrBin',500)
        end
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end

%% GC on spikes

load switchdays

sdir = pwd;

BehResp = 1;% watch out if = 1 have to add 'stable' args
cor = {'Incor' ''};
% clark
for d =1: length(days)
   cd(sprintf('%s/%s',sdir,days{d}))
   for rule = 1 : 2
        cd session99
        mts = mtstrial('auto','redosetNames');
        trials = mtsgetTrials(mts,'rule',rule,'noRejNoisyT','stable','BehResp',BehResp);
        if ~isempty(trials)
            [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
            if ~isempty(allfiles)
                stfiles = findstr(allfiles,'ispikes.mat');
                cross = nchoosek([1:length(stfiles)],2);
                iscomb = cross;
                parfor cb = 1 : size(iscomb,1)
                    fi1 = iscomb(cb,1);
                    fi2 = iscomb(cb,2);
                    cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
                    sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
                    sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
                    ntoadd = sprintf('Rule%d%s',rule,cor{BehResp+1});
                    cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                   
                    GCptProc(sp1.sp,sp2.sp,mts,trials,'addName',ntoadd,'save','redo');
                     
                end
            end
        end
        
        cd(sprintf('%s/%s',sdir,days{d}))
    end
end

%% stimulus type for spike count corr


% cd /media/raid/data/monkey/betty/
dirs = {'/Volumes/raid/data/monkey/betty' '/Volumes/raid/data/monkey/clark' '/Volumes/raid/data/monkey/betty/acute'};
dirs = {'/media/raid/data/monkey/betty' '/media/raid/data/monkey/clark' '/media/raid/data/monkey/betty/acute'};


BehResp = 1; % watch out if = 1 have to add 'stable' args
for dd = 1
    cd(dirs{dd})
    sdir = pwd;
    load switchdays
    for d = 23 :length(days)
        cd(sprintf('%s/%s',sdir,days{d}))
        cd session99
        mts = mtstrial('auto','redosetNames');
        [st,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
        stfiles = findstr(allfiles,'ispikes.mat');
        iscomb = nchoosek([1:length(stfiles)],2);
        parfor cb = 1 : size(iscomb,1)
            fi1 = iscomb(cb,1);
            fi2 = iscomb(cb,2);
            cd(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) -2)))
            sp1 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
            sp2 = load(sprintf('%s/%s/session99/%s',sdir,days{d},allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
            for rule =1 : 2
                for obj = 1 : 3
                    ntoadd = sprintf('Rule%dIde%d',rule,obj);
                    cd(sprintf('%s/%s/session99',sdir,days{d}))
                    trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'noRejNoisyT','rule',rule,'iCueObj',obj);
                    cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                    spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'xcorr','binSize',500,'xcorrBin',500,'addName',ntoadd,'save');
                    spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'xcorr','binSize',500,'xcorrBin',500,'addName',ntoadd,'save','surrogate');
                end
                for loc = 1 : 3
                    ntoadd = sprintf('Rule%dLoc%d',rule,loc);
                    cd(sprintf('%s/%s/session99',sdir,days{d}))
                    trials = mtsgetTrials(mts,'stable','BehResp',BehResp,'noRejNoisyT','rule',rule,'iCueLoc',loc);
                    cd(sprintf('%s/%s/session99/xcorr',sdir,days{d}))
                    spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'xcorr','binSize',500,'xcorrBin',500,'addName',ntoadd,'save');
                    spikeCountCorr(sp1.sp,sp2.sp,mts,trials,'xcorr','binSize',500,'xcorrBin',500,'addName',ntoadd,'save','surrogate');
                end
            end
        end
    end
end
