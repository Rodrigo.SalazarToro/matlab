function run_hilbertphase_entropy_surrogate(varargin)

%run at session level
%computes relative phase angle tass_1998 and levanquyen, surrogates
Args = struct('ml',0,'redo',0,'nperms',100);
Args.flags = {'ml','redo'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.ml
    N = NeuronalHist('ml');
else
    N = NeuronalHist;
end
window = 300;
stepsize = 50;
steps = [window/2:stepsize:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
stepsacc = [window/2:stepsize:1000-(window/2)];
nw = size(steps,2);
nwsacc = size(stepsacc,2);

nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
bins = linspace(0,1,nbins);

if Args.ml
    c = mtscpp2('auto','ml');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
    
    %get only the specified trial indices (correct and stable)
    alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY
    alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2); %LOCATION
    alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
    [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55)); %get rid of bad fix trials
    alltr{3}(rej) = [];
    alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
    
    %get 9 stims
    counter = 4;
    for objs = 1:3
        for locs = 1:3
            counter = counter + 1;
            alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs,'iCueLoc',locs);
        end
    end
    
    %3 ides
    for objs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs);
    end
    
    %3 locs
    for locs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',locs);
    end
    
else
    c = mtscpp2('auto');
    [~,pairs] = get(c,'Number'); %use all pairs
    mt = mtstrial('auto','redosetNames');
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
    
    %get only the specified trial indices (correct and stable)
    alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1); %IDENTITY
    alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','rule',2); %LOCATION
    alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
    [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55)); %get rid of bad fix trials
    alltr{3}(rej) = [];
    alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','rule',1); %incorrect/identity
    
    %get 9 stims
    counter = 4;
    for objs = 1:3
        for locs = 1:3
            counter = counter + 1;
            alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueObj',objs,'iCueLoc',locs);
        end
    end
    
    %3 ides
    for objs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueObj',objs);
    end
    
    %3 locs
    for locs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueLoc',locs);
    end
end

pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

cd([sesdir filesep 'lfp' filesep 'lfp2'])
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp2.*');

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);
% nw_sacc = find(steps == 1000 - window/2);

for p = 1 : npairs
    groups = pair_list(p,:);
    surfile = [ 'surrogate_hilbertentropy' num2strpad(groups(1),2) num2strpad(groups(2),2) '.mat'];
    if ~exist(surfile,'file') || Args.redo
        for types = 1%1:counter
            tr = alltr{types};
            ntrials = size(tr,2);
            surrogate_hilbertentropy = cell(counter,Args.nperms);
            mean_phases_surrogate = cell(counter,Args.nperms);
            
            %             hilbertentropy_sacc_surrogate = cell(counter,Args.nperms);
            %             mean_phases_sacc_surrogate = cell(counter,Args.nperms);
            for perm = 1 : Args.nperms
                tic
                fprintf(1,['pairs to go: ' num2str(npairs - p) ',  types to go: ' num2str(counter-types) ',  permutations to go: ' num2str(Args.nperms - perm) '\n'])
                hentropy = single(zeros(nfreq,nw,ntrials));
                mphase = single(zeros(nfreq,nw,ntrials));
                
                % %                 hentropysacc = single(zeros(nfreq,nw_sacc,ntrials));
                % %                 mphasesacc = single(zeros(nfreq,nw_sacc,ntrials));
                r1 = randperm(ntrials);
                r2 = randperm(ntrials);
                trial_counter = 0;
                for t = 1 : ntrials
                    trial_counter = trial_counter + 1;
                    t1 = tr(r1(t)); %trial 1
                    t2 = tr(r2(t)); %trial 2
                    for nf = 1 : nfreq
                        name1 = lfpdata(t1).name;
                        bpdata1 = [name1(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) name1((end-8):end)];
                        load(bpdata1,'data','datasacc')
                        data = data'; %need to do this before and after using hilbert, don't include during the transform
                        %compute hilbert transform
                        data = hilbert(data);
                        data = data';
                        %find instantaneous phase angles
                        data1 = angle(data);
                        % %
                        % %                         datasacc = datasacc'; %need to do this before and after using hilbert, don't include during the transform
                        % %                         %compute hilbert transform
                        % %                         datasacc = hilbert(datasacc);
                        % %                         datasacc = datasacc';
                        % %                         %find instantaneous phase angles
                        % %                         datasacc1 = angle(datasacc);
                        
                        
                        
                        name2 = lfpdata(t2).name;
                        bpdata2 = [name2(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) name2((end-8):end)];
                        load(bpdata2,'data','datasacc')
                        data = data'; %need to do this before and after using hilbert, don't include during the transform
                        %compute hilbert transform
                        data = hilbert(data);
                        data = data';
                        %find instantaneous phase angles
                        data2 = angle(data);
                        % %
                        % %                         datasacc = datasacc'; %need to do this before and after using hilbert, don't include during the transform
                        % %                         %compute hilbert transform
                        % %                         datasacc = hilbert(datasacc);
                        % %                         datasacc = datasacc';
                        % %                         %find instantaneous phase angles
                        % %                         datasacc2 = angle(datasacc);
                        % %
                        if Args.ml
                            [~,ch(1)] = intersect(N.gridPos,pair_list(p,1));
                            [~,ch(2)] = intersect(N.gridPos,pair_list(p,2));
                        else
                            ch(1) = pair_list(p,1);
                            ch(2) = pair_list(p,2);
                        end
                        
                        d1 = unwrap(data1(ch(1),:)) ./ (2*pi);
                        d2 = unwrap(data2(ch(2),:)) ./ (2*pi);
                        
                        % %                         d1sacc = unwrap(datasacc1(ch(1),:)) ./ (2*pi);
                        % %                         d2sacc = unwrap(datasacc2(ch(2),:)) ./ (2*pi);
                        
                        %make data the same length
                        if size(d1,2) > size(d2,2)
                            d1 = d1(1:size(d2,2));
                        else
                            d2 = d2(1:size(d1,2));
                        end
                        
                        %cyclic relative phase
                        d = mod(d1 - d2,1);
                        % %                         dsacc = mod(d1sacc - d2sacc,1);
                        
                        if size(d,2) >= 5000
                            d = single(d(1:5000));
                        else
                            d = single(padarray(d,[0 5000-size(d,2)],'post'));
                        end
                        
                        max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                        %get data ready for hist (hist is aparently the most
                        %time consuming function, found using profile
                        cc = 0;
                        allh = zeros(window,nw);
                        % %                         allhsacc = zeros(window,nwsacc);
                        for ww = steps
                            cc = cc + 1;
                            allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
                            % %                             if ww <= (1000 - (window/2))
                            % %                                 allhsacc(:,cc) = dsacc(ww-((window/2)-1):ww+(window/2));
                            % %                             end
                        end
                        
                        allhist = hist(allh,bins) ./ window; %calculate response distribution
                        re  = -1 * nansum(allhist.*log2(allhist));
                        resp_ent = (max_ent - re) ./ max_ent;
                        mp = circ_mean((allh * (2*pi)));
                        
                        % %                         allhistsacc = hist(allhsacc,bins) ./ window;
                        % %                         resacc = -1 * nansum(allhistsacc.*log2(allhistsacc));
                        % %                         resp_entsacc = (max_ent - resacc) / max_ent;
                        % %                         mpsacc = circ_mean((allhsacc * (2*pi)));
                        
                        hentropy(nf,:,trial_counter) = single(resp_ent);
                        mphase(nf,:,trial_counter) = single(mp);
                        
                        % %                         hentropysacc(nf,:,trial_counter) = single(resp_entsacc);
                        % %                         mphasesacc(nf,:,trial_counter) = single(mpsacc);
                    end
                end
                surrogate_hilbertentropy{types,perm} = hentropy;
                mean_phases_surrogate{types,perm} = mphase;
                
                % %                 hilbertentropy_sacc_surrogate{types,perm} = hentropysacc;
                % %                 mean_phases_sacc_surrogate{types,perm} = mphasesacc;
                toc
            end
        end
        channel_pairs = [ 'surrogate_hilbertentropy' num2strpad(groups(1),2) num2strpad(groups(2),2)];
        %         save(channel_pairs,'surrogate_hilbertentropy','mean_phases_surrogate','hilbertentropy_sacc_surrogate','mean_phases_sacc_surrogate','all_freq','steps','window','trialorder','groups')
        save(channel_pairs,'surrogate_hilbertentropy','mean_phases_surrogate','all_freq','steps','window','trialorder','groups')
    end
end


cd(sesdir)










