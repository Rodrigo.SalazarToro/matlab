function nlyx_lost_records

%run at session level
%determines if any trials should be rejected because of lost records

sesdir = pwd;

lost_trials = [];
nlynxfile = nptdir('2*');
if exist('lost_records.mat') ~=0 %#ok<*EXIST>
    load lost_records.mat
    nlost = size(lost_records,1);
    cd(nlynxfile.name)
    load timing
    good_trials = find(t.trial_record);
    ntrials = size(good_trials,2);
    for nl = 1 : nlost
        lrecords = lost_records(nl,1) : lost_records(nl,2);
        for nt = 1 : ntrials
            trial = t.start(good_trials(nt)) : t.stop(good_trials(nt));
            if ~isempty(intersect(trial,lrecords))
                fprintf(1,['trial ' num2str(nt) ' lost'])
                lost_trials = [lost_trials nt];
            end
        end
    end
end

cd(sesdir)

save lost_trials lost_trials
