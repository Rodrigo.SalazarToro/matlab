function [newx newy] = grid_transform(angle,x,y)

%angle is in rad

%calculate new angle
y = y -1; %origin is (0,0)
x = x -1; 
new_angle = atan(y/x) + angle;

h = sqrt(x^2 + y^2);

newx = cos(new_angle) * h;
newy = sin(new_angle) * h;




