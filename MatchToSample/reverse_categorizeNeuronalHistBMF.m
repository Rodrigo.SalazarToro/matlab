function [histareas] = reverse_categorizeNeuronalHistBMF(HistNumbers)

%input HistNumber to get back location
%IF THERE ARE ANY CHANGES TO CATEGORIZENEURONALHISTBMF MAKE SURE THE SAME
%CHANGES ARE MADE HERE
allnumbers = 1:53; %currently there are 53 areas
occipital = {'DPT','OPT','PO','V1','V2','V3','V4','MT'};
occipitaln = [1:8];
allnumbers(occipitaln) = 1;

temporal = {'TEO','TEOM','TPO','TPOC','TPT'};
temporaln = [9:13];
allnumbers(temporaln) = 2;

postparietal = {'LIPE','MIP','PE','PEC','PECG','PFCX','POAE','PPT'};
postparietaln = [14:21];
allnumbers(postparietaln) = 3;

prefrontal = {'a44','a45B','a46D','a46V','a6/32','a6DC','a6DR','a8/32','a8AD','a8AV','a8B','a9/32','a9/46D','a9/46V','a9L','a9M'};
prefrontaln = [22:37];
allnumbers(prefrontaln) = 4;

motor = {'24D','a24C','a4','a6M','a6VC','a6VR'}; 
motorn = [38:43];
allnumbers(motorn) = 5;

somatosensory = {'PAAL','PFOP','PGOP','a1','a2','a2VE','a2e','a3a','a3b','aS2E'};
somatosensoryn = [44:53];
allnumbers(somatosensoryn) = 6;

areas = [occipital,temporal,postparietal,prefrontal,motor,somatosensory];


num_hists = size(HistNumbers,2);
for x = 1 : num_hists
   histareas{x} = areas{HistNumbers(x)};
end




