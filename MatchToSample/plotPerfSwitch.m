function [CR1,CR2] = plotPerfSwitch(days,varargin)




Args = struct('ML',0,'win',100,'transitionTrials',299,'AddAfterSwitch',0,'Monkey',[]);
Args.flags = {'ML'};
[Args,modvarargin] = getOptArgs(varargin,Args);



rlabels = {'r' 'b'};

figure
if Args.ML
    
    switch1 = cell(2,1);
    switch2 = cell(2,1);
    inter1 = cell(2,1);
    for d = 1 : length(days)
        cd(days{d})
        cd session01
        
        for b = 1 : 5
            
            mts = mtstrial('auto');
            [~,ind] = get(mts,'Number','block',b,'ML');
            if length(ind) > Args.transitionTrials;
                resp = mts.data.BehResp(ind(:,2));
                
                if mts.data.Index(1,1) == 1
                    r = 1;
                else
                    r = 2;
                end
                if b == 1
                    switch1{r} = [switch1{r}; resp(end-Args.transitionTrials+1:end)'];
                else
                    
                    inter1{r} = [inter1{r}; resp(1:Args.transitionTrials + Args.AddAfterSwitch)'];
                    switch2{r} = [switch2{r}; resp(end-Args.transitionTrials+1:end)'];
                    [nr,~] = get(mts,'Number','block',b+1,'ML');
                    if nr > Args.transitionTrials
                        switch1{r} = [switch1{r}; resp(end-Args.transitionTrials+1:end)'];
                    end
                end
            end
        end
        
        
        cd ../..
    end
    CR1 = cell(2,1);
    CR2 = cell(2,1);
    
    for r = 1 : 2
        
        for tw = 1 : Args.transitionTrials-Args.win
            CR1{r}(:,tw) = 100*sum(switch1{r}(:,tw:tw+Args.win),2)/Args.win ;
        end
        for tw = 1 : Args.transitionTrials+Args.AddAfterSwitch-Args.win
            CR1{r} = [CR1{r} 100*sum(inter1{r}(:,tw:tw+Args.win),2)/Args.win] ;
        end
        for tw = 1 : Args.transitionTrials-Args.win
            CR2{r}(:,tw) = 100*sum(switch2{r}(:,tw:tw+Args.win),2)/Args.win ;
        end
        subplot(1,2,1)
        me = mean(CR1{r},1);
        se = std(CR1{r},1);%/sqrt(size(CR1{r},1));
        plot(me,rlabels{r});
        hold on
        plot(me + se,rlabels{r})
        plot(me - se,rlabels{r})
        ylim([50 100])
        
        set(gca,'TickDir','out')
        
        subplot(1,2,2)
        me = mean(CR2{r},1);
        se = std(CR2{r},1);%/sqrt(size(CR2{r},1));
        plot(me,rlabels{r});
        hold on
        plot(me + se,rlabels{r})
        plot(me - se,rlabels{r})
        ylim([50 100])
        
        set(gca,'TickDir','out')
    end
   
    
else
    
    
    switch1 = cell(2,1);
    switch2 = cell(2,1);
    inter1 = cell(2,1);
    for d = 1 : length(days)
        cd(days{d})
        load rules
        
        for s = 1 : 2
            cd(['session0' num2str(s+1)])
            mts = mtstrial('auto');
            resp = mts.data.BehResp;
            
            if s == 1
                switch1{r(1)} = [switch1{r(1)}; resp(end-Args.transitionTrials+1:end)'];
            else
                inter1{r(1)} = [inter1{r(1)}; resp(1:Args.transitionTrials + Args.AddAfterSwitch)'];
                switch2{r(1)} = [switch2{r(1)}; resp(end-Args.transitionTrials+1:end)'];
            end
            cd ..
        end
        
        
        cd ..
    end
    CR1 = cell(2,1);
    CR2 = cell(2,1);
    
    for r = 1 : 2
        
        for tw = 1 : Args.transitionTrials-Args.win
            CR1{r}(:,tw) = 100*sum(switch1{r}(:,tw:tw+Args.win),2)/Args.win ;
        end
        for tw = 1 : Args.transitionTrials + Args.AddAfterSwitch-Args.win
            CR1{r} = [CR1{r} 100*sum(inter1{r}(:,tw:tw+Args.win),2)/Args.win] ;
        end
        for tw = 1 : Args.transitionTrials-Args.win
            CR2{r}(:,tw) = 100*sum(switch2{r}(:,tw:tw+Args.win),2)/Args.win ;
        end
        subplot(1,2,1)
        me = mean(CR1{r},1);
        se = std(CR1{r},1);%/sqrt(size(CR1{r},1));
        plot(me,rlabels{r});
        hold on
        plot(me + se,rlabels{r})
        plot(me - se,rlabels{r})
        ylim([50 100])
        
        set(gca,'TickDir','out')
        
        subplot(1,2,2)
        me = mean(CR2{r},1);
        se = std(CR2{r},1);%/sqrt(size(CR2{r},1));
        plot(me,rlabels{r});
        hold on
        plot(me + se,rlabels{r})
        plot(me - se,rlabels{r})
        ylim([50 110])
       
        set(gca,'TickDir','out')
    end
  
    title('Clark')
    
end
  xlabel('trial # from the end of the 2nd rule block')
    ylabel('CR')
legend('IDE -> LOC; MEAN+STD','IDE -> LOC; MEAN','IDE -> LOC; MEAN-STD','LOC -> IDE; MEAN+STD','LOC -> IDE; MEAN','LOC -> IDE; MEAN-STD')
subplot(1,2,1); xlabel('trial # from the switching to the 2nd rule block')

if ~isempty(Args.Monkey)
    title(Args.Monkey)
end