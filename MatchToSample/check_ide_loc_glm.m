function totals = check_ide_loc_glm

cd('/media/MONTY/data/clark/')

monkeydir = pwd;
days = nptdir('06*');
numdays = length(days)
totals = 0;
for x = 1:numdays
    
    cd(monkeydir)
    cd(days(x).name)
    daydir = pwd;
    
    for y = 2:3
        
        cd(daydir)
        cd(['session0' num2str(y)])
        
        H = NeuronalHist;
        
        c = mtscpp('auto');
        
        pp = find(c.data.Index(:,1)');
        pf = find(c.data.Index(:,2)');
        ff = find(c.data.Index(:,3)');
        
        hist_pairs(pp) = 1;
        hist_pairs(pf) = 2;
        hist_pairs(ff) = 3;
        
        if y == 2
            [i ii]=get(c,'Number','correct','stable','threshold',1,'glm_delay',.001);
        else
            [l ll]=get(c,'Number','correct','stable','threshold',1,'glm_delay',.001);
        end
        
        
    end
    int = length(intersect(ll,ii));
    
    inters = intersect(ll,ii);
    if ~isempty(inters)
        hist_pairs(inters)
    end
    
    new = (length(ll) + length(ii));
    
    totals = totals+int;
    
    
    
end



