function epoch_power(varargin)

%computes granger for 7 different time periods
%calulates power for both channels, a little redundant but works for the
%object
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]
% 8: ititrial [(match + 201) : (match + 800)], for incorrect, betty trials only

Args = struct('ml',0,'bmf',0,'rules',[]);
Args.flags = {'ml','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args);

params = struct('tapers',[2 3],'Fs',200,'fpass',[1 150],'trialave',1);

sesdir = pwd;
if Args.ml
    if Args.bmf
        N = NeuronalHist('bmf');
        [~,chpairlist,~,~,sortedpairs] = bmf_groups('ml');
    else
        N = NeuronalHist('ml');
        [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
    end
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end

cd([sesdir filesep 'lfp']);
lfpdir = pwd;

if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
end

lfptrials = nptDir('*_lfp.*');
numpairs = nchoosek(N.chnumb,2);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        [~,baditi] = intersect(incorrtrials,rejecTrials);
        incorrtrials(baditi) = [];
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1, 4];
        else
            rules = [1 2 3 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    cd(lfpdir)
    if Args.ml
        if rule == 1
            trials = identity;
            power_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 2
            trials = location;
            power_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 3
            trials = fixtrials;
            power_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 4
            trials = incorrtrials;
            power_epochs = cell(8,numpairs); %eight combinations
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        ntrials = size(trials,2);
        counter = 0;
        for sp = sortedpairs
            if Args.bmf
                counter = counter + 1;
                c1 = chpairlist(counter,1);
                c2 = chpairlist(counter,2);
            else
                c1 = chpairlist(sp,1);
                c2 = chpairlist(sp,2);
            end
            
            tcounter = 0;
            fix1 = []; sample1 = []; delay4001 = []; delay8001 = []; delay1 = []; iti1 = [];
            fix2 = []; sample2 = []; delay4002 = []; delay8002 = []; delay2 = []; iti2 = [];
            for x = trials
                tcounter = tcounter + 1;
                s_on = sample_on(x);
                s_off = sample_off(x);
                m = match(x);
                
                [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(x).name);
                p1t = data.rawdata(c1,:);
                p2t = data.rawdata(c2,:);
                
                %get rid of 60! and detrend
                data1 = preprocessinglfp(p1t,'detrend')';
                data2 = preprocessinglfp(p2t,'detrend')';
                
                fixd1 = data1((floor(s_on/5) - 80) : ceil(s_on/5));
                sampled1 = data1((floor(s_off/5) - 80) : ceil(s_off/5));
                delay400d1 = data1(floor((s_off/5) : (ceil(s_off/5) + 80)));
                delay800d1 = data1((floor(s_off/5) + 80) : (ceil(s_off/5) + 160));
                delayd1 = data1((floor(s_off/5) + 40) : (ceil(s_off/5) + 160));
                
                fix1(1:80,tcounter) = fixd1(1:80);
                sample1(1:80,tcounter) = sampled1(1:80);
                delay4001(1:80,tcounter) = delay400d1(1:80);
                delay8001(1:80,tcounter) = delay800d1(1:80);
                delay1(1:120,tcounter) = delayd1(1:120);
                
                
                fixd2 = data2((floor(s_on/5) - 80) : ceil(s_on/5));
                sampled2 = data2((floor(s_off/5) - 80) : ceil(s_off/5));
                delay400d2 = data2(floor((s_off/5) : (ceil(s_off/5) + 80)));
                delay800d2 = data2((floor(s_off/5) + 80) : (ceil(s_off/5) + 160));
                delayd2 = data2((floor(s_off/5) + 40) : (ceil(s_off/5) + 160));
                
                fix2(1:80,tcounter) = fixd2(1:80);
                sample2(1:80,tcounter) = sampled2(1:80);
                delay4002(1:80,tcounter) = delay400d2(1:80);
                delay8002(1:80,tcounter) = delay800d2(1:80);
                delay2(1:120,tcounter) = delayd2(1:120);
                
                if rule == 4
                    itid1 = data1((floor(m/5) + 140) : (ceil(m/5) + 260)); %match + 700 : match + 1300
                    iti1(1:120,tcounter) = itid1(1:120); %match + 700 : match + 1300
                    
                    itid2 = data2((floor(m/5) + 140) : (ceil(m/5) + 260)); %match + 700 : match + 1300
                    iti2(1:120,tcounter) = itid2(1:120); %match + 700 : match + 1300
                end
            end
            
            [f.Sfix1,f.ffix1] = mtspectrumc(fix1,params);
            [s.Ssample1,s.fsample1] = mtspectrumc(sample1,params);
            [d400.Sdelay4001,d400.fdelay4001] = mtspectrumc(delay4001,params);
            [d800.Sdelay8001,d800.fdelay8001] = mtspectrumc(delay8001,params);
            [d.Sdelay1,d.fdelay1] = mtspectrumc(delay1,params);
            if rule == 4
                [iti.Siti1,iti.fiti1] = mtspectrumc(iti1,params);
            end
            
            [f.Sfix2,f.ffix2] = mtspectrumc(fix2,params);
            [s.Ssample2,s.fsample2] = mtspectrumc(sample2,params);
            [d400.Sdelay4002,d400.fdelay4002] = mtspectrumc(delay4002,params);
            [d800.Sdelay8002,d800.fdelay8002] = mtspectrumc(delay8002,params);
            [d.Sdelay2,d.fdelay2] = mtspectrumc(delay2,params);
            if rule == 4
                [iti.Siti2,iti.fiti2] = mtspectrumc(iti2,params);
            end
            
            power_epochs{1,sp} = f;
            power_epochs{2,sp} =  s;
            power_epochs{3,sp} =  d400;
            power_epochs{4,sp} =  d800;
            power_epochs{5,sp} =  d;
            power_epochs{6,sp} =  [];
            power_epochs{7,sp} =  [];
            
            if rule ==4
                power_epochs{8,sp} =  iti;
            end
        end
    end
    
    cd([lfpdir filesep 'lfp2'])
    write_info = writeinfo(dbstack);
    if Args.ml
        if rule == 1;
            mkdir('identity')
            cd('identity')
            save power_epochs power_epochs write_info
        elseif rule == 2;
            mkdir('location')
            cd('location')
            save power_epochs power_epochs write_info
        elseif rule == 3
            mkdir('fixation')
            cd('fixation')
            save power_epochs power_epochs write_info
        elseif rule == 4
            mkdir('incorrect')
            cd('incorrect')
            save power_epochs power_epochs write_info
        end
    else
        save power_epochs power_epochs write_info
    end
end

cd(sesdir)

fprintf(1,'\n')
fprintf(1,'Epoch Power Done.\n')