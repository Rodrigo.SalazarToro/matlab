function plot_ch_power

cd('/media/raid/data/monkey/betty/')
monkeydir = pwd;
% load alldays
load nlynxdays
alldays = nlynxdays

allmfsl = [];
allmml = [];
figure
counter = 0;
for x = 1:size(alldays,2)
    cd([monkeydir filesep alldays{x} filesep 'session01/lfp/lfp2/incorrect'])
    
    %get list of powergrams
    pgrams = nptDir('powergram*')
    npgrams = size(pgrams,1);
    
    for npg = 1 : npgrams
        counter = counter + 1;
        load(pgrams(npg).name)
        
        %     subplot(4,6,npg)
        %     sol = mean(sampleoff_locked.S(:,3:8)');
        %     plot(sol,'b')
        %     hold on
        
%         ml = mean(match_locked.S(:,3:8)');
%         plot(match_locked.t,zscore(ml),'k');
%         hold on
%         
%         axis([1 3 -3 4])
%         
%         [i ii] = max(ml(20:end));
%         allmml(counter) = match_locked.t(ii + 19);
        
        fsl = mean(firstsacc_locked.S(:,3:8)');
        plot(firstsacc_locked.t,(fsl - mean(fsl))./max(abs(fsl)),'r');
        hold on
        
        allincorrect(counter,:) = (fsl - mean(fsl))./max(abs(fsl));
        
        
        [i ii] = max(fsl(20:end));
        allmfsl(counter) = firstsacc_locked.t(ii + 19);
%         axis([1 3 -3 4])
    end
end
% figure
% subplot(1,2,1)
% hist(allmml,[1:.1:3])
% % axis([1 2 0 10])
%
% subplot(1,2,2)
% hist(allmfsl,[1:.1:3])
% axis([1 2 0 10])



allmfsl = [];
allmml = [];
counter = 0;
for x = 1:size(alldays,2)
    cd([monkeydir filesep alldays{x} filesep 'session01/lfp/lfp2/identity'])
    
    %get list of powergrams
    pgrams = nptDir('powergram*')
    npgrams = size(pgrams,1);
    
    for npg = 1 : npgrams
        counter = counter + 1;
        load(pgrams(npg).name)
        
        %     subplot(4,6,npg)
        %     sol = mean(sampleoff_locked.S(:,3:8)');
        %     plot(sol,'b')
        %     hold on
        
%         ml = mean(match_locked.S(:,3:8)');
%         plot(match_locked.t,zscore(ml),'k');
%         hold on
%         
%         axis([1 3 -3 4])
%         
%         [i ii] = max(ml(20:end));
%         allmml(counter) = match_locked.t(ii + 19);
        
        fsl = mean(firstsacc_locked.S(:,3:8)');
        plot(firstsacc_locked.t,(fsl - mean(fsl))./max(abs(fsl)),'g');
        hold on
        
        allidentity(counter,:) = (fsl - mean(fsl))./max(abs(fsl));
        
        [i ii] = max(fsl(20:end));
        allmfsl(counter) = firstsacc_locked.t(ii + 19);
%         axis([1 3 -3 4])
    end
end
figure
plot(firstsacc_locked.t,median(abs((allincorrect(:,1:52) - allidentity))))


figure
shadeplot('bins',abs(allincorrect(:,1:52) - allidentity))










