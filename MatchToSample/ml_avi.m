function ml_avi

clip = mmreader('clip.avi');
frames = read(clip);

%make new avi file
aviobj = avifile('clip786.avi','fps',12);
numframes = size(frames,4);

for nf = 1 : numframes
    
    f = frames((116:900),(116:900),:,nf);
    imagesc(f)
    x = [-392:392];
    y = [-392:392];
    
    %based on image size and radius determine crop
    c = 0;
    crop = zeros(801,801);
    r = 392.5;
    for xs = 1:785
        for ys = 1:785
            if (x(xs)^2 + y(ys)^2) > r^2
                c = c + 1;
                f(xs,ys,:) = 0;
            end
        end
    end
    
    f(1,:,:) = 0;
    f(:,786,:) = 0;
    aviobj = addframe(aviobj,f);

    close all
end

aviobj = close(aviobj);


