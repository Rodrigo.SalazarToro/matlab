function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'bmf_groups',1,'epoch_sig_coh',[],'freq_band',2,'epoch_sig_percent',100,'no_common',0);
Args.flags = {'Number','ObjectLevel','no_common'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rt = [1 : size(obj.data.Index,1)]';
    
    %get only groups that pass the SNR, rejectCH, and POWER criteria
    if Args.bmf_groups
        rtemp1 = find(obj.data.Index(:,3) == 1)';
    else
        rtemp1 = rt;
    end
    rtemp2 = rt;
    
    
    if ~isempty(Args.epoch_sig_coh)
        if size(Args.epoch_sig_coh,2) == 1
            ind = (Args.epoch_sig_coh * 4) + (Args.freq_band-1);
            rtemp3 = find(obj.data.Index(:,ind) >= (obj.data.Index(1,Args.freq_band+35)*(Args.epoch_sig_percent/100)))';
        else
            rtemp3 = [];
            for xx = 1 : size(Args.epoch_sig_coh,2)
                ind = (Args.epoch_sig_coh(xx) * 4) + (Args.freq_band-1);
                rtemp3 = [rtemp3 find(obj.data.Index(:,ind) >= (obj.data.Index(1,Args.freq_band+35)*(Args.epoch_sig_percent/100)))'];
            end
            rtemp3 = sort(unique(rtemp3));
        end
    else
        rtemp3 = rt;
    end
    
    
    if Args.no_common %take out pairs that have coherence at all epochs
        nc = zeros(1,size(rt,1));
        commonpairs = [];
        for xx = 1 : 4
            ind = (xx * 4) + (Args.freq_band-1);
            nc(find(obj.data.Index(:,ind) >= (obj.data.Index(1,Args.freq_band+35)*(Args.epoch_sig_percent/100)))') = nc(find(obj.data.Index(:,ind) >= (obj.data.Index(1,Args.freq_band+35)*(Args.epoch_sig_percent/100)))') + 1;
        end
        rtemp4 = rt;
        rtemp4(find(nc > 1)) = [];
        
    else
        rtemp4 = rt;
    end
    
    
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,rtemp4)));
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end


