function obj = spike_spike_coh(varargin)

%spike spike coherence object

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'bmf',0);
Args.flags = {'Auto','ml','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'spike_spike_coh';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'spike_spike_coh';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

sesdir = pwd;
%make mts trial object
if Args.ml
    if Args.bmf
        mtst = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        if ~isempty(mtst)
            identity = mtsgetTrials(mtst,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        end
        N = NeuronalHist('bmf');
        sortedgroups = bmf_groups;
    else
        mtst = mtstrial('auto','ML','RTfromML','redosetNames','redo');
        if ~isempty(mtst)
            identity = mtsgetTrials(mtst,'BehResp',1,'stable','ML','rule',1); %correct/identity
        end
        N = NeuronalHist('ml');
        noisy = noisy_groups;
        sortedgroups = sorted_groups('ml');
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
    end
else
    mtst = mtstrial('auto','redosetNames');
    if ~isempty(mtst)
        identity = mtsgetTrials(mtst,'BehResp',1,'stable','rule',1); %correct/identity
    end
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    noisy = noisy_groups;
    sortedgroups = sorted_groups;
    [~,ii] = intersect(sortedgroups,noisy);
    sortedgroups(ii) = []; %get rid of noisy groups
end

cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'identity']) %go to the identity dir
idedir = pwd;

lfpdir = pwd;
if ~isempty(identity)
    pairs = nptDir('spikecoh_g*')
    
    nfiles = size(pairs,1);
    allc = 0;
    for f = 1 : nfiles
        load(pairs(f).name)
        nclusters = size(spikecoh.clusters,2);
        for c = 1 : nclusters
            if spikecoh.totalspikes1(c) / spikecoh.ntrials(c) > 5 && spikecoh.totalspikes2(c) / spikecoh.ntrials(c) > 5
                allc = allc + 1;
                %get groups
                g1 = spikecoh.groups(c,1);
                g2 = spikecoh.groups(c,2);
                
                data.Index(allc,1) = g1;
                data.Index(allc,2) = g2;
                data.Index(allc,3) = c; %indicates the cluster number
                
                %determine if both groups are good (sorted AND not noisy)
                if ~isempty(intersect(sortedgroups,g1)) && ~isempty(intersect(sortedgroups,g2))
                    data.Index(allc,4) = 1;
                end
                data.setNames{allc,1} = [idedir filesep pairs(f).name];
                
                
                %find wich channel number the group corresponds
                %get histology information for both groups
                if Args.ml
                    [~,gp1] = intersect(N.gridPos,g1);
                    [~,gp2] = intersect(N.gridPos,g2);
                else
                    [~,gp1] = intersect(Nch.groups,g1);
                    [~,gp2] = intersect(Nch.groups,g2);
                end
                data.Index(allc,5) = N.number(gp1); %this is the number for the specific area
                data.Index(allc,6) = N.number(gp2);
                
                data.Index(allc,7) = spikecoh.unittype1(c); %this is the number for the specific area
                data.Index(allc,8) = spikecoh.unittype2(c);
                
                
                %get surrogate information
                load(['surrogate_' pairs(f).name])
                
                %calculate the 99.5 percentile (highest that can be
                %calculated using prctile.m with 100 surrogates)
                
                fixs = [];
                samples = [];
                delay400s = [];
                delay800s = [];
                delays = [];
                delaymatchs = [];
                fulltrials = [];
                for s = 1:100
                    fixs(s,:) = surrogate_spikecoh.fix_c{c,s};
                    samples(s,:) = surrogate_spikecoh.sample_c{c,s};
                    delay400s(s,:) = surrogate_spikecoh.delay400_c{c,s};
                    delay800s(s,:) = surrogate_spikecoh.delay800_c{c,s};
                    delays(s,:) = surrogate_spikecoh.delay_c{c,s};
                    delaymatchs(s,:) =surrogate_spikecoh.delaymatch_c{c,s};
                    fulltrials(s,:) = surrogate_spikecoh.fulltrial_c{c,s};
                end
                
                fixp = prctile(fixs,99.5);
                samplep = prctile(samples,99.5);
                delay400p = prctile(delay400s,99.5);
                delay800p = prctile(delay800s,99.5);
                delayp = prctile(delays,99.5);
                delaymatchp = prctile(delaymatchs,99.5);
                fulltrialp = prctile(fulltrials,99.5);
                
                data.sigf{1,allc} = spikecoh.fix_f{c}(spikecoh.fix_c{c}' >= fixp);
                data.sigphi{1,allc} = spikecoh.fix_phi{c}(spikecoh.fix_c{c}' >= fixp)';
                
                data.sigf{2,allc} = spikecoh.sample_f{c}(spikecoh.sample_c{c}' >= samplep);
                data.sigphi{2,allc} = spikecoh.sample_phi{c}(spikecoh.sample_c{c}' >= samplep)';
                
                data.sigf{3,allc} = spikecoh.delay400_f{c}(spikecoh.delay400_c{c}' >= delay400p);
                data.sigphi{3,allc} = spikecoh.delay400_phi{c}(spikecoh.delay400_c{c}' >= delay400p)';
                
                data.sigf{4,allc} = spikecoh.delay800_f{c}(spikecoh.delay800_c{c}' >= delay800p);
                data.sigphi{4,allc} = spikecoh.delay800_phi{c}(spikecoh.delay800_c{c}' >= delay800p)';
                
                data.sigf{5,allc} = spikecoh.delay_f{c}(spikecoh.delay_c{c}' >= delayp);
                data.sigphi{5,allc} = spikecoh.delay_phi{c}(spikecoh.delay_c{c}' >= delayp)';
                
                data.sigf{6,allc} = spikecoh.delaymatch_f{c}(spikecoh.delaymatch_c{c}' >= delaymatchp);
                data.sigphi{6,allc} = spikecoh.delaymatch_phi{c}(spikecoh.delaymatch_c{c}' >= delaymatchp)';
                
                data.sigf{7,allc} = spikecoh.fulltrial_f{c}(spikecoh.fulltrial_c{c}' >= fulltrialp);
                data.sigphi{7,allc} = spikecoh.fulltrial_phi{c}(spikecoh.fulltrial_c{c}' >= fulltrialp)';
            end
        end
    end
    data.numSets = nfiles;

    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
else
    fprintf('EpochCoh directory missing \n');
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
