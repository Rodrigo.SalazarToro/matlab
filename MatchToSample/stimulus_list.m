function st = stimulus_list(varargin)

%run at sessoin level
Args = struct('ml',0,'loc',0,'ide',0,'locbyide',[]);
Args.flags = {'ml','loc','ide'};
Args = getOptArgs(varargin,Args);

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mt = mtstrial('auto','redosetNames');
end

%get stimulus info
locs = unique(mt.data.CueLoc);
objs = unique(mt.data.CueObj);
stim = 0;
for object = 1:3
    %Run for all locations
    for location = 1:3
        stim = stim+1;
        if Args.ml
            ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
        else
            ind = mtsgetTrials(mt,'BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
        end
        stimulus(ind) = stim;
    end
end
[~, ~, iii] = find(stimulus);
st = iii;

if ~isempty(Args.locbyide)
    %determine stimulus locations
    locs = zeros(1,size(st,2));
    locs([find(st==1) find(st==4) find(st==7)]) = 1;
    locs([find(st==2) find(st==5) find(st==8)]) = 2;
    locs([find(st==3) find(st==6) find(st==9)]) = 3;
    
    %determine stimulus identities
    ide = zeros(1,size(st,2));
    ide([find(st==1) find(st==2) find(st==3)]) = 1;
    ide([find(st==4) find(st==5) find(st==6)]) = 2;
    ide([find(st==7) find(st==8) find(st==9)]) = 3;
    
    %determine the 3 identities at a specific location
    st = ide(locs == Args.locbyide);
else
    if Args.loc
        %determine stimulus locations
        locs = zeros(1,size(st,2));
        locs([find(st==1) find(st==4) find(st==7)]) = 1;
        locs([find(st==2) find(st==5) find(st==8)]) = 2;
        locs([find(st==3) find(st==6) find(st==9)]) = 3;
        
        %USE LOCATIONS INSTEAD OF INDIVIDUAL STIMULUS COMBINATIONS
        st = locs;
    elseif Args.ide
        %determine stimulus identities
        ide = zeros(1,size(st,2));
        ide([find(st==1) find(st==2) find(st==3)]) = 1;
        ide([find(st==4) find(st==5) find(st==6)]) = 2;
        ide([find(st==7) find(st==8) find(st==9)]) = 3;
        
        %USE IDENTITIES INSTEAD OF INDIVIDUAL STIMULUS COMBINATIONS
        st = ide;
    end
end


