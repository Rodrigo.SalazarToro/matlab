function predict = predict_glm_loocv(varargin)

%run at session level
Args = struct('ml',0,'fits',[],'trial_stim',0,'trial_cross',[],'logits',0,'model',[],'probs',[]);
Args.flags = {'ml','logits'};
Args = getOptArgs(varargin,Args);

counter = 0;
num_pairs = size(Args.fits,1);
for pair_num = 1 : num_pairs
    counter = counter + 1;
    
    response(:,1) = Args.trial_cross(pair_num,:);
    response(:,2) = ones(size(response,1),1);
    
    time = 1:size(response,1);
    
    B = glmfit(time,response,'binomial','link','logit');
    
    input = (B(1) + time * (B(2))); %works for no interaction as well
    
    %convert to probability
    if Args.logits
        %keep in logit
    else
        input = 1 ./ (1 + exp(-input));
    end
    
    trial_fits(:,counter) = single(input);
    trial_raw(:,counter) = response(:,1)';

    cell_coefs = Args.fits{pair_num}.interaction_coefs;
    coefs = cellfun(@str2double,cell_coefs);
    
    if Args.model == 1
        %columns are the modeled response for each stimulus
        res(:,1) = coefs(1) + time*coefs(2);
        res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
        res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
        res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
        res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
        res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
        res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
        res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
        res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
    elseif Args.model == 2 || Args.model == 3
        %columns are the modeled response for each stimulus
        res(:,1) = coefs(1) + time*coefs(2);
        res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(5);
        res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(6);
    end
    
    if Args.logits
        %keep in logit
    else
        res = 1 ./ (1 + exp(-res));
    end
    
    pair_fits{pair_num} = single(res);
end

tf = trial_fits;

raw_tf = trial_raw;

for all_stims = 1 : size(res,2);
    for p = 1 : num_pairs
        pfits_timepoints(:,p) = pair_fits{p}(:,all_stims); %get 9 stims for each time point for all pairs
    end
    all_pfits_timepoints(:,:,all_stims) = pfits_timepoints;
end




% % %xcorrs
% % for allc = 1 : size(all_pfits_timepoints,2)
% %     for st = 1 : size(res,2)
% %         pf = all_pfits_timepoints(:,allc,st);
% %         xcs(st,allc) = xcorr(tf(:,allc),pf,0,'coef');
% %     end
% % end

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% % use original probabilities
% % for all_stims = 1 : size(res,2);
% %     for p = 1 : num_pairs
% %         pfits_timepoints(:,p) = Args.probs{p}(all_stims,:); %get 9 stims for each time point for all pairs
% %     end
% %     all_pfits_timepoints(:,:,all_stims) = pfits_timepoints;
% % end

% numpairs = size(all_pfits_timepoints,2);
% 
% list = zeros(1,numpairs);
% for allpairs = 1 : numpairs
%     pf = all_pfits_timepoints(:,allpairs,:);
%     
%     %time by stims
%     pf = reshape(pf,9,size(res,2));
%     
%     diffs = pf(9,:) - pf(1,:);
%     
%     %determine best stimulus
%     [~,mdiff] = max((diffs));
%     
%     list(allpairs) = mdiff;
% end



xcc = [];
for allxc = 1 : size(res,2)
   xcc(allxc) =  corr2(tf, all_pfits_timepoints(:,:,allxc));
end

% % xcc = [];
% % for allxc = 1 : size(res,2)
% %    xcc(allxc) =  corr2(raw_tf(:,allxc), all_pfits_timepoints(:,allxc));
% % end


[~,iii] = max(xcc);
if Args.trial_stim == iii
    predict = 1;
else
    predict = 0;
end











