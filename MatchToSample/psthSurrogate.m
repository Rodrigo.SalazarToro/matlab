function [data,mi,time,ntrial,cstim] = psthSurrogate(varargin)
% to run in the cluster folder


Args = struct('save',0,'redo',0,'iterations',10000,'binSize',100,'rule',1,'alignment',1,'bmf',0);
Args.flags = {'save','redo','bmf'};
Args = getOptArgs(varargin,Args);

references = [1:Args.binSize:3000];

switch Args.alignment
    case 1
        if Args.bmf
            load(sprintf('nineStimPSTH%d.mat',Args.rule))
            cueComb = [1:5]';% first row == loc
            NineCues = [1:5]';
            IdeCues = [1:5]';
            
            
            surTypes = {'Ide'};
            nstim = [5];
        else
            load(sprintf('nineStimPSTH%d.mat',Args.rule))
            cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];% first row == loc
            NineCues = [1:9]';
            IdeCues = [1 2 3; 4 5 6; 7 8 9]';
            LocCues = [1 4 7; 2 5 8; 3 6 9]';
            
            surTypes = {'Ide' 'Loc' 'Nine'};
            nstim = [3 3 9];
        end
        al = Args.alignment;
        
    case 2
        load(sprintf('nineStimPSTH%d.mat',Args.rule))
        cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];% first row == loc
        NineCues = [1:9]';
        IdeCues = [1 2 3; 4 5 6; 7 8 9]';
        LocCues = [1 4 7; 2 5 8; 3 6 9]';
        MatchCues = nan;
        
        surTypes = {'Ide' 'Loc' 'Nine'}; % need to work on the match stimulus
        nstim = [3 3 9 18];
        al = Args.alignment;
        
    case 3
        load(sprintf('threeSacPSTH%d.mat',Args.rule))
        surTypes = {'SacLoc'};
        nstim = 3;
        SacLocCues = [1 :3]';
        al = Args.alignment;
        
    case 4
        load(sprintf('MatchObjPSTH%d.mat',Args.rule))
        surTypes = {'MatchObj'};
        nstim = 3;
        MatchObjCues = [1 :3]';
        al = 2;
        
    case 5
        load(sprintf('MatchLocPSTH%d.mat',Args.rule))
        surTypes = {'MatchLoc'};
        nstim = 3;
        MatchLocCues = [1 :3]';
        al = 2;
    case 6
        load(sprintf('Match18PSTH%d.mat',Args.rule))
        surTypes = {'Match18'};
        nstim = 18;
        Match18Cues = [1 : 18]';
        al = 2;
    case {7,8}
        load(sprintf('nineStimPSTH%d.mat',Args.rule))
        
        LocPerIde1Cues = [1 2 3]';
        LocPerIde2Cues = [4 5 6]';
        LocPerIde3Cues = [7 8 9]';
        
        IdePerLoc1Cues = [1 4 7]';
        IdePerLoc2Cues = [2 5 8]';
        IdePerLoc3Cues = [3 6 9]';
        
        surTypes = {'IdePerLoc1' 'IdePerLoc2' 'IdePerLoc3' 'LocPerIde1' 'LocPerIde2' 'LocPerIde3'};
        nstim = [3 3 3 3 3 3];
        if Args.alignment == 7
            al = 1;
        else
            al = 2;
        end
end

for type = 1 : length(surTypes)
    if Args.alignment == 2 || Args.alignment == 8
        matfile = sprintf('PSTH%sMatchAlignrule%d.mat',surTypes{type},Args.rule);
    else
        matfile = sprintf('PSTH%srule%d.mat',surTypes{type},Args.rule);
    end
    if isempty(nptDir(matfile)) || Args.redo
        if Args.bmf
            switch size(A,1)
                
                case 6
                    A(6,:) = [];
                case 7
                    A(1,:) = [];
                    A(6,:) = [];
            end
        else
            
            
            if size(A,1) == 4 && A{4,1} ~= 0
                
                error('there are 4 obj and last obj is not empty');
                
            end
        end
        %         A = A(1:3,:);
        zerostim = [];for ss = 1 : size(A,1); if size(A{ss,1},1) ~= 1; zerostim = [zerostim ss]; end; end
        
        cuesss = sprintf('%sCues',surTypes{type});
        if ismember(Args.alignment,[1 2 7 8]) ; zerostim = eval(cuesss); zerostim = reshape(zerostim,1,size(zerostim,1) * size(zerostim,2)); end
        
        data = cell2mat(A(zerostim,al));
        bdata = zeros(size(data,1),length(references));
        
        for bin = 1 : length(references)-1;
            bdata(:,bin) = sum(data(:,references(bin): references(bin+1) -1),2);
            
        end % rebin the 1mse matraix
        time = [bins{1,al}(1)+Args.binSize/2  : Args.binSize : bins{1,al}(end)];
        data = bdata;
        
        mi = zeros(Args.iterations,size(data,2));
        ntrial = zeros(nstim(type),1);
        cstim = zeros(size(data,1),1);
        for stim = 1 : nstim(type)
            thecues = eval(sprintf('%sCues',surTypes{type}));
            if sum(thecues(stim) == zerostim) == 1
                ntrial(stim) = 0;
                
                
                for cueType = 1 : size(thecues,2);
                    tempT = size(A{thecues(stim,cueType),1},1);
                    ntrial(stim) = ntrial(stim) + tempT;
                    
                end % get the number of trials per stimuli
                
                % code the trials with the stim number
                if stim == 1
                    cstim(1:ntrial(stim)) = repmat(stim,[ntrial(stim) 1]);
                else
                    cstim(sum(ntrial(1:stim-1))+ 1 : sum(ntrial(1:stim-1)) + ntrial(stim)) = repmat(stim,[ntrial(stim) 1]);
                end
            end
        end
        for it = 1 : Args.iterations + 1
            
            if it < Args.iterations + 1
                randseq = randperm(size(data,1));
                data = bdata(randseq,:);
            else
                data = bdata;
            end
            
            
            for ti = 1 : size(data,2); mi(it,ti) = mutualinfo(cstim,data(:,ti)); end
            
        end
        if Args.save
            
            save(matfile,'data','time','cstim','ntrial','Args','mi')
            display([pwd '/' matfile])
        end
    end
    
end
% stim = 2;figure;for al = 1 : 3; subplot(3,1,al);
% plot(prctile(squeeze(allFr(al,:,:))',[0.05 99.05],2));hold on;[row,stimes] = find(A{stim,al} == 1); fr = histcie(stimes,references); fr = (fr/ntrial) * (1000/Args.binSize);plot(fr,'k'); end