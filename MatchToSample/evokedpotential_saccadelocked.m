function evokedpotential_saccadelocked(varargin)

Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);


cd('/media/raid/data/monkey/betty/091001/session01')
sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames');
N = NeuronalHist('ml');
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY

trials = mtsgetTrials(mt,'BehResp',0,'stable','rule',1); %INCORRECT/IDENTITY

ntrials = size(trials,2);

alldatamatch = [];
alldatasacc = [];
for t = 1 : ntrials
    
    match = mt.data.MatchOnset(trials(t));
    sac = mt.data.MatchOnset(trials(t)) + mt.data.FirstSac(trials(t));
    data = ReadUEIFile('FileName',['betty09100101.' num2strpad(trials(t),4)]);
    hdata = nptLowPassFilter(data.rawdata(neuronalCh,:),data.samplingRate,8,25);
    
        if isempty(alldatamatch)
            alldatamatch = hdata(:,match-700:match+299);
            alldatasacc = hdata(:,sac-850:sac+149);
        else
            alldatamatch = alldatamatch + hdata(:,match-700:match+299);
            alldatasacc = alldatasacc + hdata(:,sac-850:sac+149);
        end
    
    
    
%     if isempty(alldatamatch)
%         alldatamatch = hdata(:,match-500:match+999);
%         alldatasacc = hdata(:,sac-500:sac+999);
%     else
%         alldatamatch = alldatamatch + hdata(:,match-500:match+999);
%         alldatasacc = alldatasacc + hdata(:,sac-500:sac+999);
%     end
%     
    
end



figure
for x = 1 : size(alldatamatch,1);
    plot(alldatamatch(x,:))%+x*30000000);
    hold on;
end
axis tight


figure
for x = 1 : size(alldatasacc,1);
    plot(alldatasacc(x,:))%+x*30000000);
    hold on;
end
axis tight
