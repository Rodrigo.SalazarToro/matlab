function plot_sp_sp_coh

cd /media/raid/data/monkey/betty/
mdir = pwd;
load alldays
figure
for d = 26%1 : 46
    try
        cd([mdir filesep alldays{d} filesep 'session01'])
        N = NeuronalHist('ml');
        
        cd([mdir filesep alldays{d} filesep 'session01' filesep 'lfp' filesep 'lfp2' filesep 'identity'])
        
        
        sp = nptDir('spikecoh*');
        nsp = size(sp,1);
        
        spsur = nptDir('surrogate_spikecoh*');
        
        surcounter = 0;
        counter = 0;
        for n = 1 : nsp
            counter = counter + 1;
            load(sp(n).name)
            
            alldays{d};
            
            a1 = N.level1(find(N.gridPos == spikecoh.groups(1,1)));
            a2 = N.level1(find(N.gridPos == spikecoh.groups(1,2)));
            
            if isfield(spikecoh,'delay_f')
                f = spikecoh.delay_f{1};
                phi = spikecoh.delay_phi{1};
                phi = ((abs(phi) / pi) ./10);
                
                interval = find(f > 3 & f < 100);
                sp_sp = spikecoh.delay_c{1};
                if counter > 20
                    figure
                    counter = 1;
                end
                
                subplot(4,5,counter)
                plot(f(interval),sp_sp(interval))
                hold on
                plot(phi(interval),'g')
                title([a1{:} ' ' num2str(spikecoh.groups(1,1)) '   ' a2{:} ' ' num2str(spikecoh.groups(1,2)) '  ' num2str(n)])
                axis([3 100 0 .4])
                
                
                
                %plot surrogate
                load(['surrogate_' sp(n).name])
                delays = [];
                for s = 1:100
                    delays(s,:) = surrogate_spikecoh.delay_c{1,s};
                end
                delayp = prctile(delays,99.5);
                
                hold on
                plot(f(interval),delayp(interval),'r')
                
            end
        end
    catch
    end
    
    pause
    close all
    
end