function [allmatches,matches] = getMatches(mts)

 allmatches = unique([mts.data.MatchObj1 mts.data.MatchObj2 mts.data.MatchPos1 mts.data.MatchPos2],'rows');

if size(allmatches,1) ~= 18
    objs = unique([mts.data.MatchObj1; mts.data.MatchObj2]);
    if ~isempty(sum(objs == 55)); objs(find(objs ==55)) = []; end 
    objpair = nchoosek(objs,2);
    locs = unique([mts.data.MatchPos1; mts.data.MatchPos2]);
    locpair = nchoosek(locs,2);
    
    c = 1;
   for op = 1 : 3
       for ol = 1 : 3
      ind1 = find(allmatches(:,1) == objpair(op,1) &  allmatches(:,2) == objpair(op,2) &  allmatches(:,3) == locpair(ol,1) &  allmatches(:,4) == locpair(ol,2));
       ind2 = find(allmatches(:,2) == objpair(op,1) &  allmatches(:,1) == objpair(op,2) &  allmatches(:,4) == locpair(ol,1) &  allmatches(:,3) == locpair(ol,2));
       matches(c,:) = union(ind1,ind2);
       c = c + 1;
       ind1 = find(allmatches(:,2) == objpair(op,1) &  allmatches(:,1) == objpair(op,2) &  allmatches(:,3) == locpair(ol,1) &  allmatches(:,4) == locpair(ol,2));
       ind2 = find(allmatches(:,1) == objpair(op,1) &  allmatches(:,2) == objpair(op,2) &  allmatches(:,4) == locpair(ol,1) &  allmatches(:,3) == locpair(ol,2));
       matches(c,:) = union(ind1,ind2);
       c = c + 1;
       end
   end
    
else
    
    for m = 1 : 18; matches(m) = m;end
    
end
