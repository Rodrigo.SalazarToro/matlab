function [r,varargout] = get(obj,varargin)
%eyemvt/get Get function for EYEMVT objects
%   [numevents,dataindices] = get(obj,'Number') returns
%   all the trial numbers and the indices of those trials corresponding
%   to the SetIndex field of the EYEMVT object. ObjectLevel by defautl is
%   SESSIONOBJECT
%
%   [numevents,dataindices,Mark] = get(obj,'Number','SessionBySession')
%   returns the indices for the saccades belonging to each session.
%
%   [numevents,dataindices] = get(obj,'Number','Fix') process the fixations
%   instead of the saccades (default).
%
%   [numevents,dataindices,mark] = get(obj,'Number','Threshold',T,'Parameter',...
%   'P','Condition','C') enables to select a set of saccades/fixations according
%   to the threshold T applied to the parameter P and with a certain condition C.
%   If a certain threshold criteria has been used to select specific saccades
%   and/or fixations, MARK will be set to 1 in order to point them out during the
%   plotting.
%
%   P has to be one of the following parameter:
%
%            MaxVel: maximum velocity of saccades/fixations [degree/ms]
%            MeanVel: mean velocity of fixations [degree/ms]
%            Start:  start of saccades/fixations [ms]
%            End: end of saccades/fixations [ms]
%            MaxVelTime: occurence of the maximum velocity of the saccades/fixations [ms]
%            Ampl:  Amplitude of the saccades/fixations [degree]
%
%
%   Ex.:
%   [numevents,dataindices,mark] = get(obj,'Number','Threshold',100,'Parameter',
%   'MaxVel','Condition','<')  is going to return the indices of the
%   saccades whose maximum velocities are below 100 [degree/s].
%
%
%
%
%   Dependencies: EYEMVT
%
Args = struct('Number',0,'ObjectLevel',0,'Threshold',[],'Parameter',[],'Condition','>');
Args.flags = {'Number','Fix','ObjectLevel'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;


if Args.Number
    
    if isempty(Args.Threshold)

        rind = SetIndex(:,1:3);
        % rind = repmat(unique(rind),1,2);
        r = length(rind);
        varargout(1) = {rind};
        % if we don't recognize and of the options, pass the call to parent
        % in case it is to get number of events, which has to go all the way
        % nptdata/get
    elseif ~isempty(Args.Threshold)
        measure = eval(Args.Parameter);

        [rawindices,colindices] = eval(sprintf('find(measure %s %d)',Args.Condition,Args.Threshold));
        rind = SetIndex(rawindices,[1 2]);

        varargout(1) = {rind};
        r = length(rind);
        varargout(2) = {1};
    end

elseif(Args.ObjectLevel)
    r = 'monkey';
else
    r = get(obj.nptdata,varargin{:});

end
