function obj = mtsrule(varargin)
%   @mtsrule Constructor function for mtsrule object. The mtsrule object
%   has data binned in the same way than the mtsbinned obj but around the
%   rule switching bin.
%
%   OBJ = mtsrule('auto') uses an mtsbinned object or creates it.
%   The  object contains all the following fields (IDE2LOC: identity to
%   location; LOC2IDE : location to identity; mn : mean; std : standard
%   deviation; md : median; ltile : 25th percentile; htile : 75th
%   percentile; Perf : % correct responses; RTCR : reaction time correct
%   responses; RTIR : reaction time incorrect responses):
%
%      data.IDE2LOCmnPerf 
%      data.IDE2LOCstdPerf 
%      data.IDE2LOCmdPerf 
%      data.IDE2LOCltilePerf 
%      data.IDE2LOChtilePerf 
%      data.IDE2LOCmnRTCR 
%      data.IDE2LOCstdRTCR 
%      data.IDE2LOCmdRTCR 
%      data.IDE2LOCltileRTCR 
%      data.IDE2LOChtileRTCR 
%      data.IDE2LOCmnRTIR
%      data.IDE2LOCstdRTIR 
%      data.IDE2LOCmdRTIR 
%      data.IDE2LOCltileRTIR 
%      data.IDE2LOChtileRTIR 
%      data.LOC2IDEmnPerf 
%      data.LOC2IDEstdPerf 
%      data.LOC2IDEmdPerf 
%      data.LOC2IDEltilePerf 
%      data.LOC2IDEhtilePerf 
%      data.LOC2IDEmnRTCR 
%      data.LOC2IDEstdRTCR 
%      data.LOC2IDEmdRTCR 
%      data.LOC2IDEltileRTCR 
%      data.LOC2IDEhtileRTCR 
%      data.LOC2IDEmnRTIR 
%      data.LOC2IDEstdRTIR 
%      data.LOC2IDEmdRTIR 
%      data.LOC2IDEltileRTIR 
%      data.LOC2IDEhtileRTIR 
%
%
%   The data are raw vectors of the binned trials with the central bin being the switching bin for a given session. 
%    
%   data.index corresponds to the time of the bins relative to the
%   switching 
%
%   Dependencies: mtstrial, ProcessSessionMTS, nptdata.

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'days',[]);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto','days'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsrule';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsr';


numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('day','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)


% for i = 1 : size(varargin,2) % removal of the DayObject argument in the varargin
%     if isstr(varargin{i}) & strmatch('DayObject',varargin{i});
%         n = i;
%     end
% end

% varargin{n} = [];

if isempty(Args.days)
    alldir = nptDir;
    c = 1;
    for i = 1 : size(alldir,1)
        directory = str2num(alldir(i).name);
        if ~isempty(directory) && length(alldir(i).name) == 6
            Args.days{c} = alldir(i).name;
            c = c +1;
        end
    end
end

binobj = ProcessDays(mtsbinned,'auto',varargin{:},'days',Args.days);
if ~isempty(binobj)

    der = diff(binobj.data.Index(:,1));
    I = find(der ~= 0);
    
    ntrials = der(I); % with 1 equals from IDENTITY  to LOCATION and inverse for -1
    II = [1;I;length(binobj.data.Index(:,1))];
    diffshift = diff(II);
    
    minbins = min(diffshift);
    
    taskind{1} = find(ntrials == 1);
    taskind{2} = find(ntrials == -1);
    
    rule = {'IDE2LOC','LOC2IDE'}; 
    for task = 1 : 2
        ind = I(taskind{task});
        for nshift = 1 : length(ind)
            Perf(:,nshift) = binobj.data.perf((ind(nshift) - minbins):(ind(nshift) + minbins));
            RTCR(:,nshift) = binobj.data.RTCR((ind(nshift) - minbins):(ind(nshift) + minbins));
            RTIR(:,nshift) = binobj.data.RTIR((ind(nshift) - minbins):(ind(nshift) + minbins));
        end

        var = {'Perf','RTCR','RTIR'};
        for v = var
            eval(sprintf('data.%smn%s = nanmean(%s,2);',rule{task},v{1},v{1}))
            eval(sprintf('data.%sstd%s = std(%s,0,2);',rule{task},v{1},v{1}))
            eval(sprintf('data.%smd%s = nanmedian(%s,2);',rule{task},v{1},v{1}))
            eval(sprintf('data.%sltile%s = prctile(%s,25,2);',rule{task},v{1},v{1}))
            eval(sprintf('data.%shtile%s = prctile(%s,75,2);',rule{task},v{1},v{1}))
        end
    end
    
    data.numSets = size(data.IDE2LOCmnPerf,1);
    data.Index = binobj.data.binned *[-minbins:minbins]';
    data.setNames{1} = pwd;

    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);

        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end


else
    % create empty object
    obj = createEmptyObject(Args);
    fprintf('The mtsbinned object is empty!!!!!!\n');
end

function obj = createEmptyObject(Args)

% useful fields for most objects
rule = {'IDE2LOC','LOC2IDE'};
var = {'Perf','RTCR','RTIR'};
for task = 1 : 2
    
    for v = var
        eval(sprintf('data.%smn%s = [];',rule{task},v{1},v{1}))
        eval(sprintf('data.%sstd%s = [];',rule{task},v{1},v{1}))
        eval(sprintf('data.%smd%s = [];',rule{task},v{1},v{1}))
        eval(sprintf('data.%sltile%s = [];',rule{task},v{1},v{1}))
        eval(sprintf('data.%shtile%s = [];',rule{task},v{1},v{1}))
    end
end

data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
