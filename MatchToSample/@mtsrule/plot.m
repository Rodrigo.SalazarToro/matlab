function obj = plot(obj,varargin)
% @mtsrule/plot Plots data from the mtsrule object
% OBJ = plot(OBJ,N) plots mean percent correct responses 
% of the bins before and after a rule switch (identity to location) 
%   The following optional input arguments are valid:
%
%   - percentCR : plots the mean and 
%
%   OBJ = plot(OBJ,N) plots the eye traces of trial N.
%
%

Args = struct('RT',0,'LOC2IDE',0,'median',0);
Args.flags = {'RT','IDE2LOC','LOC2IDE','mean','median'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'RT','IDE2LOC','LOC2IDE','mean','median'});

% [numevents,dataindices,Mark] = get(obj,'Number',varargin2{:});

% if (~isempty(Args.NumericArguments))
% 
%     n = Args.NumericArguments{1};
%     ind = find(dataindices(:,1) == n);
%     limit = dataindices(ind,3);
% else
%     limit = dataindices(:,2);
% end

if Args.LOC2IDE
    shift = 'LOC2IDE';
else
    shift = 'IDE2LOC';
end

if Args.median
    var = {'md','ltile','htile'};
    titles = 'Median and 25-75 percentiles';
else 
   
     var = {'mn','std'};
    titles = 'Mean and standard deviation';
end


if Args.RT
    measure = {'RTCR','RTIR'};
    l = {'b','g'};
    leg = {'Correct Responses','Incorrect responses'};
    labely = 'Reaction time';
else 
    measure = {'Perf'};
    labely = '% Correct responses';
    leg = [];
    l = {'b'};
    
end


for m = 1 : size(measure,2)
    for v = 1 : size(var,2)
    value{m,v} = sprintf('obj.data.%s%s%s',shift,var{v},measure{m});
    end
end

if Args.median
    
    for m = 1 : size(measure,2)
        for v = 1 : size(var,2)
            plot(obj.data.Index,eval(value{m,v}),l{m})
            hold on
     
        end
    end
    
    hold off
    
else
    
    for m = 1 : size(measure,2)

        errorbar(obj.data.Index,eval(value{m,1}),eval(value{m,2}),l{m});
        hold on
        
    end
    hold off
end
title(titles);
ylabel(labely);
xlabel('Number of trials to the switch')
if ~isempty(leg)
    legend(leg)
end

