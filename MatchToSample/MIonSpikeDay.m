function MIonSpikeDay(theday,varargin)

Args = struct('alignement',[1:7]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'alignement'});


cd(theday)

for al = Args.alignement
    psthSurDay('alignment',al,modvarargin{:});
end
cd ..