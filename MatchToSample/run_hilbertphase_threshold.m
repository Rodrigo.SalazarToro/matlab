function all_out = run_hilbertphase_threshold(varargin)

%run at session level
%computes MI based on normalized Shannon Entropy tass_1998
Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    c = mtscpp2('auto','ml');
else
    c = mtscpp2('auto');
end

[~,pairs] = get(c,'ml','Number'); %use all pairs

pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

all_out = 0;

for p = 1 : npairs
    
    mi_pairs = [ 'hilbertmi' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    load(mi_pairs,'mi')
    mi_pairs = [ 'hilbertmisurrogate' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    load(mi_pairs,'mi_surr')
    
    out = hilbertphase_threshold('mi',mi,'surr',mi_surr);
    all_out = all_out + out;
end



















