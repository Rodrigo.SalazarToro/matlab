function makeGrids(varargin)

%run at monkey level

%use redo to remake the plotting information 
%use remove_flat with redo to kick out flat channels
%seed: indicate seed channel
%plot_days: indicates days to use for ploting
%rule: indicates rule
%glm_delay: only plots pairs that have a significant interaction during the delay

Args = struct('seed',[],'plot_days',[],'rule',[],'remove_flat',0,'redo',0,'glmDelay',0);
Args.flags = {'remove_flat','redo','glmDelay'};
Args = getOptArgs(varargin,Args);

load channel_offsets

color = 0; %used for plotting phase information
parietal_frontal = 0; %if parietal_frontal == 1 only plot pp, if parietal_frontal == -1 only plot pf

%run at monkey level
%only needs lfp2 folder and descriptor file.
%monkey/day/lfp2
daydir = pwd;
load switchdays

num_days = size(days,2);
if isempty(nptDir('grids_day_depths.mat')) || Args.redo
    
    %make daysXchannel(64) matrix containing the depth of each channel recorded. If
    %channel is not recorded the entry is a zero.
    day_depths = zeros(num_days,64);
    loc_depths = cell(num_days,64);
    for d = 1 : num_days
        
        cd([daydir filesep days{d}])
        [ch,CHcomb,iflat,group_numbers] = getFlatCh;
        
        if isempty(nptdir('skip.txt'))
            cd([daydir filesep days{d} filesep 'session01'])
            
            descriptor = nptdir('*descriptor.txt');
            descriptor_info = ReadDescriptor(descriptor(1).name);
            
            channels = descriptor_info.channel;
            channel_depths = descriptor_info.recdepth ./ 1000;
            zero_channels = find(channel_depths == 0);
            c_o = channel_offsets(channels);
            c_o(zero_channels) = 0;
            channel_depths = channel_depths - c_o; % subtract offsets
            num_channels = size(channels,2);


            %get histology information
            N = NeuronalHist('ml');
            locations = N.locs;
            for c = 1:num_channels
                day_depths(d,channels(c)) = channel_depths(c);
                
                [i ii iii]= intersect(channels(c),N.gridPos);
                if ii
                    loc_depths{d,channels(c)} = N.locs{iii};
                    location_depths{d,channels(c)} = N.location{iii};
                end
            end
            
            
            if Args.remove_flat
                %remove flat channels from channel list
                [i ind] = intersect(ch,iflat.ch); %take out iflat channels
                %make depths for FLAT channels = 0
                day_depths(d,group_numbers(ind)) = 0;
                if ~isempty(ind)
                    for loc_ind = 1:size(ind,2)
                        loc_depths{d,group_numbers(ind(loc_ind))} = {};
                        location_depths{d,group_numbers(ind(loc_ind))} = {};
                    end
                end
            end
            
            
        end
        
    end
    
    cd(daydir)
    
    save grids_day_depths day_depths
    save grids_loc_depths loc_depths
    save grids_location_depths location_depths
else
    load grids_day_depths day_depths
    load grids_loc_depths loc_depths
    load grids_location_depths location_depths
    
end

%%
%PF grid
pf_order = [ ...
    
0; 1; 2; 3; 4; 5;
0; 6; 7; 8; 9;10;
11;12;13;14;15;16;
17;18;19;20;21;22;
0;23;24;25;26;27;
0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    
0;33;34;35;36;37;
0;38;39;40;41;42;
43;44;45;46;47;48;
49;50;51;52;53;54;
0;55;56;57;58;59;
0;60;61;62;63;64];
%%

%make figure
figure('Name',['seed channel:  ' num2str(Args.seed)],'position', [100 400 1600 900]);

for all_days = Args.plot_days;
    
    active_ch = find(day_depths(all_days,:));
    
    chnumb = size(active_ch,2);
    
    pairs = [];
    
    p = 0;
    for t = 1 : (chnumb - 1)
        p1 = active_ch(t);
        for tt = (t+1) : chnumb
            p2 = active_ch(tt);
            p=p+1;
            pairs(p,:) = [p1 p2];
        end
    end
    
    cd([daydir filesep days{all_days} filesep 'session01' filesep 'lfp' filesep 'lfp2' filesep Args.rule])
    
    load avg_correlograms avg_corrcoef avg_phase
    load all_pairs all_pairs
    if Args.glmDelay
        load glm_delay glm_delay
        delays = glm_delay;
    end
    
    % iii provides the indices for the pairs
    [i ii iii]=intersect(pairs,all_pairs,'rows');
    
    %get corr coefs and phases
    for x = 1: size(iii,1)
        
        %try catch statment used because bad channels do not have a correlation
        %coefficient. If the corrcoef is set to zero then no dot will appear.
% % %         try
% % %             pairs(x,3) = avg_corrcoef{x};
% % %             pairs(x,4) = avg_phase{x};
% % %         catch
% % %             pairs(x,3) = 0;
% % %             pairs(x,4) = 0;
% % %         end
if Args.glmDelay
    p_delay = delays(x).interaction;
    if p_delay <= .001 %p-val for glm_delay
        pairs(x,3) = avg_corrcoef{iii(x)};
        pairs(x,4) = avg_phase{iii(x)};
    else
        pairs(x,3) = 0;
        pairs(x,4) = 0;
    end
else
    pairs(x,3) = avg_corrcoef{iii(x)};
    pairs(x,4) = avg_phase{iii(x)};
end

    end
    
    
    numbPairs = p;
    
    %% Get XY coordinates for all channels
    xy = [];
    cch = 0;
    for grids = 1:2
        ch = 0;
        
        for x = [6 5 4 3 2 1]
            for xx = 1:6
                
                ch = ch + 1;
                if grids == 1
                    if pf_order(ch) ~= 0
                        cch = cch +1;
                        xy(cch,1) = xx;
                        xy(cch,2) = x;
                    end
                else
                    if pp_order(ch) ~= 0
                        cch = cch +1;
                        xy(cch,1) = xx+7;
                        xy(cch,2) = x;
                    end
                end
            end
        end
    end
    
    %%
    xy_corr = [];
    qqq = 0;
    for allPairs = 1 : numbPairs
        %check to see if the pair includes the seed channel
        if size(unique([pairs(allPairs,(1:2)) Args.seed]),2) == 2
            qqq = qqq+1;
            
            %get channel that seed is paired with
            [i ii] = intersect(pairs(allPairs,(1:2)),Args.seed);
            if ii == 1
                xy_corr(qqq,1) = pairs(allPairs,2);
            else
                xy_corr(qqq,1) = pairs(allPairs,1);
            end
            
            %save corr coef associated with the channel
            xy_corr(qqq,2) = pairs(allPairs,3);
            
            %save phase
            xy_corr(qqq,3) = pairs(allPairs,4);
            
        end
        
    end
    
    %% Plot corr coefs
    numGridPoints = size(xy_corr,1);
    
    for n = 1 : numGridPoints
        
        %this is the xy coordinates for the channel on the grid
        g = xy(xy_corr(n,1),:);
        %this is the corresponding corr. coef.
        c = xy_corr(n,2);
        %get depths
        d = (-1*day_depths(all_days,xy_corr(n,1)));
        locations = loc_depths{all_days,xy_corr(n,1)};
        seed_locations = loc_depths{all_days,Args.seed};
        seed_text = location_depths{all_days,Args.seed};
        if (~strncmpi(seed_locations,'w',1)) && (~strncmpi(locations,'w',1))
            %     %get phase
            pp = xy_corr(n,3);
            %     p_color = 1/abs(p);
            %     if p_color == Inf
            %         p_color =1;
            %     end
            %                                                                             p_color = 1;%%%%
            if c > 0
                if color == 1
                    scatter3(g(1),g(2),d,(c*500),[0 0 1],'filled')
                else
                    % % %                     scatter3(g(1),g(2),d,(c*500),(1/pp),'filled')
                    if (pp >= -2) && (pp <= 2)
                       scatter3(g(1),g(2),d,(c*500),[0 0 1],'filled')
                    else
                    end
                end
                hold on
            elseif c < 0
                if color == 1
                    scatter3(g(1),g(2),d,(abs(c)*500),[1 0 0],'filled')
                else
                    % % % %                     scatter3(g(1),g(2),d,(abs(c)*500),(1/pp),'filled')
                    if (pp >= -2) && (pp <= 2) 
                        scatter3(g(1),g(2),d,(abs(c)*500),[1 0 0],'filled')
                    else
                    end
                end
                hold on
            else
                %if corrcoef = 0 then do not plot anything
            end
            
            %indicate locations
            if strncmpi(locations,'d',1)
                scatter3(g(1),g(2),d,500,[0 0 0],'h')
            elseif strncmpi(locations,'v',1)
                scatter3(g(1),g(2),d,500,[0 0 0],'o')
            elseif strncmpi(locations,'m',1)
                scatter3(g(1),g(2),d,500,[0 0 0],'h')
            elseif strncmpi(locations,'l',1)
                scatter3(g(1),g(2),d,500,[0 0 0],'o')
            else
                scatter3(g(1),g(2),d,500,[0 0 0],'*')
            end
            
            
            
        end
        
        
        %%
        %label grids
        for grids = 1:2
            ch = 0;
            for x = [6 5 4 3 2 1]
                for xx = 1:6
                    
                    ch = ch + 1;
                    
                    if grids == 1
                        channel = pf_order(ch);
                        if channel ~= 0
                            
                            %scatter3(xx,x,0,500,[0 0 0])
                            %text((xx-.1),(x+.5),num2str(channel),'FontSize',10,'FontWeight','bold')
                        end
                    else
                        channel = pp_order(ch);
                        if channel ~= 0
                            
                            %scatter3((xx+7),x,0,500,[0 0 0])
                            %text((xx+6.9),(x+.5),num2str(channel),'FontSize',10,'FontWeight','bold')
                            
                        end
                    end
                    
                    hold on
                    
                end
            end
            if (~strncmpi(seed_locations,'w',1))
                seed_depth =-1* day_depths(all_days,Args.seed);
                scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,200,[0 1 0],'fill')
                text(xy(Args.seed,1)+.5,xy(Args.seed,2),seed_depth,seed_text(1:end-3),'FontSize',10,'FontWeight','bold')
                
                
                hold on
                %indicate locations of seed channel
                if strncmpi(seed_locations,'d',1)
                    scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,500,[0 0 0],'h')
                elseif strncmpi(seed_locations,'v',1)
                    scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,500,[0 0 0],'o')
                elseif strncmpi(seed_locations,'m',1)
                    scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,500,[0 0 0],'h')
                elseif strncmpi(seed_locations,'l',1)
                    scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,500,[0 0 0],'o')
                else
                    scatter3(xy(Args.seed,1),xy(Args.seed,2),seed_depth,500,[0 0 0],'*')
                end
                hold on
                
                
                %     text((xy(seed,1)-.1),xy(seed,2),num2str(seed),'FontSize',12,'FontWeight','bold')
            end
        end

        axis([-.5 14.5 0 7.5])
        
        % % % % % hold on;plot([10.5, 10.5],[4.7 6.45],'color','k')
        % % % % % hold on;plot([10.5, 11.5],[4.7 4.7],'color','k')
        % % % % % hold on;plot([11.5, 11.5],[2.7 4.7],'color','k')
        % % % % % hold on;plot([11.5, 13.25],[2.7 2.7],'color','k')
        % % % % %
        % % % % % %Create text box
        % % % % % pfc = uicontrol(f,'Style','text','String','PFC','units','normalized',...
        % % % % %     'Position',[.35 .87 .1 .05],'fontsize',16,'fontweight','b');
        % % % % %
        % % % % % %Create text box
        % % % % % ppc = uicontrol(f,'Style','text','String','PPC','units','normalized',...
        % % % % %     'Position',[.65 .87 .1 .05],'fontsize',16,'fontweight','b');
        % % % % %
        % % % % % %Create text box
        % % % % % inphase = uicontrol(f,'Style','text','String','in-phase','units','normalized',...
        % % % % %     'Position',[.16 .305 .055 .04],'fontsize',10,'fontweight','b');
        % % % % % scatter(-.25,2,500,[0 0 1],'fill')
        % % % % %
        % % % % % %Create text box
        % % % % % antiphase = uicontrol(f,'Style','text','String','anti-phase','units','normalized',...
        % % % % %     'Position',[.16 .22 .055 .04],'fontsize',10,'fontweight','b');
        % % % % % scatter(-.25,1.2,500,[1 0 0],'fill')
        % % % % %
        % % % % % %Create text box
        % % % % % seed = uicontrol(f,'Style','text','String','seed','units','normalized',...
        % % % % %     'Position',[.16 .145 .055 .04],'fontsize',10,'fontweight','b');
        % % % % % scatter(-.25,.5,500,[0 1 0],'fill')
        
        set(gca,'XTick',[])
        set(gca,'XTickLabel',[])
        set(gca,'YTick',[])
        set(gca,'YTickLabel',[])
        
%         if color == 0
%             colorbar
%             caxis([-1 1]);
%         end
        
        hold on
        
        
        % % % %                 %IPS
        % % % %                 y1 = 6.5; y2 =.5;
        % % % %                 x1 = 11.5; x2 = 12.75;
        % % % %                 plot3([x1 x2],[y1 y2],[0 0],'color','k','LineWidth',10)
        % % % %                 hold on
        % % % %
        % % % %                 %PS
        % % % %                 y1 = 3.5; y2 =1;
        % % % %                 x1 = .5; x2 = 6;
        % % % %                 plot3([x1 x2],[y1 y2],[0 0],'color','k','LineWidth',10)
        % % % %                 hold on
        % % % %
        % % % %                 %Arcuate
        % % % %                 y1 = 6.5; y2 =4.5;
        % % % %                 x1 = 1.5; x2 = 6.25;
        % % % %                 plot3([x1 x2],[y1 y2],[0 0],'color','k','LineWidth',10)
        % % % %                 hold on
        
        
        % a = gca;
        % set(a,'View',[100,50])
    end
end
if parietal_frontal == 1;
    axis([7 14.5 0 7.5])
elseif parietal_frontal == -1
    axis([-5 7 0 7.5])
end


cd(daydir)

