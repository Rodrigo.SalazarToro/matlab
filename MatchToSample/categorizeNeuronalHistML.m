function [HistNumber,lev1,lev2,lev3,varargout]=categorizeNeuronalHistML(HistInfo)
%
%
% areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
%
%   Parietal: (letter/area)
% medial superficial
% -PE/ms/m
% -PEC/ms/m
%
% medial deep
% -MIP/md/m
%
% lateral superficial
% -PG/ls/l
%
% lateral deep
% -LIP/ld/l
%
% other
% -PGM/i/m
% -PEcg/i/m
%
%   Prefrontal:
% dorsal
% -dPFC/d/d
%
% ventral
% -vPFC/v/v
%
% FEF
% -8AD/i/d
%
% other
% -6DR/i/d
%
% other
% -8B/i/d
%

s = size(HistInfo,2);

for x = 1 : s
    site = HistInfo{:,x};
    %seperate parietal locations
    fslash = strfind(site,';');
    locations{1} = site(1:(fslash(1)-1)); %get first location
    
    
    % % % % % % % % Prefrontal
    if strncmpi(locations(1),'6DR',3)
        HistNumber(:,x) = 1;
        lev1{x} = '6DR';
        lev2{x} = 'f';
        lev3{x} = 'f';

    elseif strncmpi(locations(1),'8AD',3)
        HistNumber(:,x) = 2;
        lev1{x} = '8AD';
        lev2{x} = 'f';
        lev3{x} = 'f';
        
    elseif strncmpi(locations(1),'8B',2)
        HistNumber(:,x) = 3;
        lev1{x} = '8B';
        lev2{x} = 'i';
        lev3{x} = 'd';
        
    elseif strncmpi(locations(1),'dPFC',4)
        HistNumber(:,x) = 4;
        lev1{x} = 'dPFC';
        lev2{x} = 'd';
        lev3{x} = 'd';
        
    elseif strncmpi(locations(1),'vPFC',4)
        HistNumber(:,x) = 5;
        lev1{x} = 'vPFC';
        lev2{x} = 'v';
        lev3{x} = 'v';
        
    elseif strncmpi(locations(1),'PS',2)
        HistNumber(:,x) = 6;
        lev1{x} = 'PS';
        lev2{x} = 'i';
        lev3{x} = 'i';
        
    elseif strncmpi(locations(1),'AS',2)
        HistNumber(:,x) = 7;
        lev1{x} = 'AS';
        lev2{x} = 'i';
        lev3{x} = 'i';
        
        
        % % % % % % % % %Parietal
    elseif strncmpi(locations(1),'PEC',3)
        HistNumber(:,x) = 8;
        lev1{x} = 'PEC';
        lev2{x} = 'ms';
        lev3{x} = 'm';
        
    elseif strncmpi(locations(1),'PGM',3)
        HistNumber(:,x) = 9;
        lev1{x} = 'PGM';
        lev2{x} = 'i';
        lev3{x} = 'm';
        
        % NOTE: Must place below PEC
    elseif strncmpi(locations(1),'PE',2)
        HistNumber(:,x) = 10;
        lev1{x} = 'PE';
        lev2{x} = 'ms';
        lev3{x} = 'm';
        
    elseif strncmpi(locations(1),'PG',2)
        HistNumber(:,x) = 11;
        lev1{x} = 'PG';
        lev2{x} = 'ls';
        lev3{x} = 'l';
        
    elseif strncmpi(locations(1),'MIP',3)
        HistNumber(:,x) = 12;
        lev1{x} = 'MIP';
        lev2{x} = 'md';
        lev3{x} = 'm';
        
    elseif strncmpi(locations(1),'LIP',3)
        HistNumber(:,x) = 13;
        lev1{x} = 'LIP';
        lev2{x} = 'ld';
        lev3{x} = 'l';
        
    elseif strncmpi(locations(1),'PEcg',4)
        HistNumber(:,x) = 14;
        lev1{x} = 'PEcg';
        lev2{x} = 'i';
        lev3{x} = 'm';
        
    elseif strncmpi(locations(1),'IPS',3)
        HistNumber(:,x) = 15;
         lev1{x} = 'IPS';
        lev2{x} = 'i';
        lev3{x} = 'i';
        
        %White Matter
    elseif strncmpi(locations(1),'WM',2)
        HistNumber(:,x) = 16;
        lev1{x} = 'WM';
        lev2{x} = 'i';
        lev3{x} = 'i';
        
    elseif strncmpi(locations(1),'??',2)
        HistNumber(:,x) = 99;
        lev1{x} = 'unknown';
        lev2{x} = 'i';
        lev3{x} = 'i';
    else 
        HistNumber(:,x) = 89;
        lev1{x} = 'i';
        lev2{x} = 'i';
        lev3{x} = 'i';
    end
    
end
varargout{1} = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};