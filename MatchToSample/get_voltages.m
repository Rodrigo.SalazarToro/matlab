function get_voltages(varargin)

%run at monkey level
%calculates the std for each trial and the global std of each channel 
%global std is calulated by concatenating all trials
Args = struct('ml',0,'days',[]);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

monkeydir = pwd;

numdays = size(Args.days,2);
mean_for_lfp2 = [];
std_for_lfp2 = [];
for x = 1 : numdays
    cd([monkeydir filesep Args.days{x}])
    daydir = pwd;

    if Args.ml
        sessions = {'session01'};
        ses = 1;
    else
        sessions = {'session02','session03'};
        ses = [1:2];
    end

    %run through both sessions
    for s = ses
        mean_for_lfp2 = [];
        std_for_lfp2 = [];
        try
        cd([daydir filesep sessions{s} filesep 'lfp'])
        catch
            fprintf(1,['No ' sessions{s} ' on ' num2str(Args.days{x}) '\n'])
            break
        end
        %get index of all lfp trials  lfpdata.name
        lfpdata = nptDir('*_lfp.****');
        num_trials = size(lfpdata,1)
        %run through all trials
        for t = 1 : num_trials
            trial = lfpdata(t).name;
            [datas,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(trial);
            
            mean_for_lfp2(:,t) = mean(datas')';
            std_for_lfp2(:,t) = std(datas')';
            if t == 1
                num_channels = size(datas,1);
            end
        end
        
        %run through all channels and all trials
        for c = 1 : num_channels
            ch_data = [];
            for t = 1 : num_trials
                trial = lfpdata(t).name;
                [datas,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(trial);
                ch_data = [ch_data datas(c,:)];
            end
            ch_std(c) = std(ch_data);
        end
        clear ch_data
        
        cd('lfp2')
        
        i = dbstack;
        write_info.function = i.file;
        write_info.date = date;
        save mean_std_info mean_for_lfp2 std_for_lfp2 ch_std write_info
    end
end