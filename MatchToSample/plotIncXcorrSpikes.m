function [sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins,varargout] = plotIncXcorrSpikes(obj1,ind,varargin)
% mCor(rule x count x align x epochs)
% sigPos(rule x count x align x epochs)
Args = struct('prctileCoh',[1 99],'pvalueCoh',[],'prctileRule',[2 98],'plot',0,'timeRsigCor',[],'falseRate',[3 2],'alltrials',0,'movingAve',2,'append',[],'filter',0,'biasCorrectedCor',0);
Args.flags = {'plot','alltrials','filter','biasCorrectedCor'};
[Args,~] = getOptArgs(varargin,Args);


n=sum(obj1.data.Index(ind(:),3) ~= obj1.data.Index(ind(:),4));


if Args.alltrials
    add = 'Alltrials';
    
else
    if ~isempty(Args.append)
        add = Args.append;
        if strmatch(Args.append,'epoch');
            add2 = '500ms';
            sigPos = nan(2,n,2,4);
            sigNeg = nan(2,n,2,4);
            mCor = nan(2,n,2,4);
            sigPosDiff = nan(n,2,4);
            sigNegDiff = nan(n,2,4);
            lastb = 3;
            SndLocked = 3;
        else
            add2 = [];
            sigPos = nan(2,n,2,39);
            sigNeg = nan(2,n,2,39);
            mCor = nan(2,n,2,39);
            sigPosDiff = nan(n,2,39);
            sigNegDiff = nan(n,2,39);
            lastb = 36;
            SndLocked = 17:33;
        end
    else
        add= [];
    end
end

c=1;
for ii = 1 : length(ind)
    name1 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7));
    name2 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8));
    if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
        cd(obj1.data.setNames{ind(ii)})
        for rule =1 : 2
           if ~isempty(nptDir(sprintf('xccorrg%sg%sRule%d%s.mat',name1,name2,rule,add)));
            data = load(sprintf('xccorrg%sg%sRule%d%s.mat',name1,name2,rule,add));
            sur = load(sprintf('xccorrg%sg%sSurRule%d%s.mat',name1,name2,rule,add));
           elseif ~isempty(nptDir(sprintf('xccorrg%sg%sRule%d%s.mat',name2,name1,rule,add)));
               data = load(sprintf('xccorrg%sg%sRule%d%s.mat',name2,name1,rule,add));
            sur = load(sprintf('xccorrg%sg%sSurRule%d%s.mat',name2,name1,rule,add)); 
           else
               display(['file missing ' pwd])
            end
            if isempty(Args.pvalueCoh)
                sigPos(rule,c,:,1:size(data.rc,2)) = data.rc > squeeze(prctile(sur.rc(:,:,1:size(data.rc,2)),Args.prctileCoh(2),1));
                sigNeg(rule,c,:,1:size(data.rc,2)) = data.rc < squeeze(prctile(sur.rc(:,:,1:size(data.rc,2)),Args.prctileCoh(1),1));
            else
                for rw = 1 : size(sur.rc,2)
                    for cl = 1 : size(sur.rc,3)
                        try
                        [~,~,negthresh,~] = gevfitSur(sur.rc(:,rw,cl),Args.pvalueCoh/2);
                        [~,~,posthresh,~] = gevfitSur(sur.rc(:,rw,cl),1-Args.pvalueCoh/2);
                        sigPos(rule,c,rw,cl) = data.rc(rw,cl) > posthresh;
                        sigNeg(rule,c,rw,cl) = data.rc(rw,cl) < negthresh;
                        end
                    end
                end
            end
            if Args.biasCorrectedCor
                mCor(rule,c,:,1:size(data.rc,2)) = data.rc - squeeze(nanmean(sur.rc,1)) ;
            else
                mCor(rule,c,:,1:size(data.rc,2)) = data.rc;
            end
            
            compFile = load(sprintf('xccorrg%sg%sRuleComp%s.mat',name1,name2,add2));
        end
        sigPosDiff(c,:,:) = compFile.diffrc > squeeze(prctile(compFile.diffrcSur,Args.prctileRule(2),1));
        sigNegDiff(c,:,:) = compFile.diffrc < squeeze(prctile(compFile.diffrcSur,Args.prctileRule(1),1));
        varargout{1}(c) = ind(ii);
        c= c+1;
        
    end
end

sigCor = cell(2,2);

for r = 1 : 2
    sigCor{r,1} = find(squeeze(sum(sum(sigPos(r,:,1,1:lastb),1),4)) > Args.falseRate(1));
    sigCor{r,1} = [sigCor{r,1} find(squeeze(sum(sum(sigPos(r,:,2,SndLocked),1),4)) > (Args.falseRate(2)))];
    
    sigCor{r,2} = [find(squeeze(sum(sum(sigNeg(r,:,1,1:lastb),1),4)) > Args.falseRate(1))];
    sigCor{r,2} = [sigCor{r,2} find(squeeze(sum(sum(sigNeg(r,:,2,SndLocked),1),4)) > Args.falseRate(2))];
    
end
sigRule{1} = find(squeeze(sum(sigPosDiff(:,1,1:lastb),3)) > (Args.falseRate(1)));
sigRule{1} = [sigRule{1}; find(squeeze(sum(sigPosDiff(:,2,SndLocked),3)) > (Args.falseRate(2)))];

sigRule{2} = [find(squeeze(sum(sigNegDiff(:,1,1:lastb),3)) > (Args.falseRate(1)))];
sigRule{2} = [sigRule{2}; find(squeeze(sum(sigNegDiff(:,2,SndLocked),3)) > (Args.falseRate(2)))];

allsigCor = unique(cat(2,sigCor{1,1},sigCor{2,1}));
for tt = 1 : 2; sigRule{tt} = unique(intersect(sigRule{tt},allsigCor));
    for r = 1 : 2; sigCor{r,tt} = unique(sigCor{r,tt});end
end



if Args.plot
    lb = {'r' 'b'};
    figure
    for r = 1 : 2
        for tt = 1 : 2
            subplot(4,2,tt)
            
            d1 = 100*squeeze(sum(sigPos(r,:,tt,1:length(data.bins{tt})),2))/size(sigPos,2);
            if Args.filter
                d1 = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d1(1:end-1));
            end
            plot(data.bins{tt}(2:end),d1(1:length(data.bins{tt}(2:end))),lb{r})
            hold on
            ylabel('% sig. pos. cor')
            line([-800 1200],[Args.prctileCoh(1) Args.prctileCoh(1)],'LineStyle','--')
            ylim([0 20])
            title(['n=' num2str(size(sigPos,2))])
            legend('IDE','chance','LOC')
            subplot(4,2,tt+2)
            d1 = 100*squeeze(sum(sigNeg(r,:,tt,1:length(data.bins{tt})),2))/size(sigPos,2);
            if Args.filter
                d1 = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d1(1:end-1));
            end
            plot(data.bins{tt}(2:end),d1(1:length(data.bins{tt}(2:end))),lb{r})
            hold on
            ylabel('% sig. neg. cor')
            ylim([0 20])
            line([-800 1200],[Args.prctileCoh(1) Args.prctileCoh(1)],'LineStyle','--')
            title(['n=' num2str(size(sigPos,2))])
            subplot(4,2,tt+4)
            me = squeeze(nanmean(mCor(r,allsigCor,tt,1:length(data.bins{tt})),2));
            if Args.filter
                me = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,me);
            end
            st = squeeze(nanstd(mCor(r,allsigCor,tt,1:length(data.bins{tt})),[],2)) ./ sqrt(size(sigPos,2));
            if Args.filter
                st = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,st);
            end
            plot(data.bins{tt}(2:end),me(1:end-1),lb{r})
            hold on
            plot(data.bins{tt}(2:end),me(1:end-1)+st(1:end-1),[lb{r} '--'])
            plot(data.bins{tt}(2:end),me(1:end-1)-st(1:end-1),[lb{r} '--'])
            
            %             me = -squeeze(nanmean(mCor(r,sigCor{2},tt,1:length(data.bins{tt})),2));
            %             st = squeeze(nanstd(mCor(r,sigCor{2},tt,1:length(data.bins{tt})),[],2)) ./ sqrt(size(sigPos,2));
            %             plot(data.bins{tt},me,lb{r})
            %             hold on
            %             plot(data.bins{tt},me+st,[lb{r} '--'])
            %             plot(data.bins{tt},me-st,[lb{r} '--'])
            %
            ylim([.1 0.25])
            ylabel('mean+/- sem cor')
            try
                title(['pos n=' num2str(length(allsigCor)) 'neg n=' num2str(length(cat(2,sigCor{1,2},sigCor{2,2})))])
            end
            subplot(4,2,tt+6)
            %             d1 = 100*squeeze(sum(sigNegDiff(allsigCor,tt,1:length(data.bins{tt})),1))/length(allsigCor);
            d1 = squeeze(sum(sigNegDiff(allsigCor,tt,1:length(data.bins{tt})),1));
            d2 = squeeze(sum(sigPosDiff(allsigCor,tt,1:length(data.bins{tt})),1));
            d = 100*(d1 + d2)/length(allsigCor);
            %             d1 = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d1(1:end-1));
            if Args.filter
                d = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d(1:end-1));
            end
            %             d2 = 100*squeeze(sum(sigPosDiff(allsigCor,tt,1:length(data.bins{tt})),1))/length(allsigCor);
            %             d2 = filtfilt(repmat(1/Args.movingAve,1,Args.movingAve),1,d2(1:end-1));
            plot(data.bins{tt}(2:end),d(1:3),lb{1})
            hold on
            %             plot(data.bins{tt}(2:end),d2,lb{2})
            line([-800 1200],[2*Args.prctileRule(1) 2*Args.prctileRule(1)],'LineStyle','--')
            ylabel('% rule diff.')
            if ~isempty(sigRule{2})
                title(['n=' num2str(length(cat(1,sigRule{1},sigRule{2})))])
            end
            ylim([0 10])
            legend('IDE > LOC','LOC > IDE','chance')
        end
    end
end
bins = data.bins;
for sb = [1 3 5 7]; subplot(4,2,sb);xlim([-450 1250]);end; xlabel('Time from sample onset');
for sb = [2 4 6 8]; subplot(4,2,sb);xlim([-750 150]);end; xlabel('Time from match onset');