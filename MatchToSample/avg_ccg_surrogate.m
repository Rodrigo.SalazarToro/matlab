function avg_ccg_surrogate(varargin)

%used to make surrogates for avg ccg plots
Args = struct('ml',0,'monkey','','windowSize',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

%session directory
ses = pwd;
N = NeuronalHist;
chnumb = N.chnumb;
pairs = nchoosek(chnumb,2);
cd([ses filesep ('lfp')]);
mt = mtstrial('auto');
objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);

stim = 0;
for objects = 1 : 3
    for locations = 1 : 3
        stim = stim + 1;
        cd(ses)
        %check for bhv file
        if Args.ml
            ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations),'ML');
        else
            ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations));
        end
        trials(stim) = size(ind,2);
    end
end

if Args.ml
    lfp2_dirs = ['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
    num_lfp2 = [1 2];
    if Args.ide_only
        lfp2_dirs = ['identity']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
        num_lfp2 = [1];
    end
else
    lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
    num_lfp2 = [1];
end

for ide_loc = num_lfp2
    type = mt.data.Index(:,1);
    trials_identity = (find(type == 1));
    trials_location = (find(type == 2));
    trials_all = [{trials_identity'} {trials_location'}];

    cd([ses filesep 'lfp' filesep 'lfp2'])
    
    %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
    % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX

    
    lfpdata = nptDir([Args.monkey '*']);
    load ([cd filesep lfpdata(1).name],'normdata');
    numchannels = size(normdata,1);
    ddata = cell(numchannels,1);
    if isempty(trials_all{ide_loc}) %if there are no trials then do not run threshold calculation
        if ide_loc == 1
            fprintf(1,'No identity trials\n')
        else
            fprintf(1,'No location trials\n')
        end
    else
        for x = trials_all{ide_loc}
            load ([cd filesep lfpdata(x).name],'normdata');
            for y = 1: numchannels
                %Concatenates the data from all lfp files to run surrogate analysis
                if y == 1
                    ddata{y} = normdata(y,:);
                end
                ddata{y} = [ddata{y} normdata(y,:)];
            end
        end
        %Calculate Moving Surrogate
        rr_surrogate=cell(1,pairs);
        pair = 0;
        for ii=1:chnumb
            fprintf('\n%0.5g     ',ii)
            for jj=ii+1:chnumb
                fprintf(' %0.5g',jj)
                pair = pair + 1;

                for stim_trials = 1 : 9
                    for iters = 1 : 1000
                        %find xcorr at random shifts
                        r_surrogate = RandXxcorr(ddata{ii},ddata{jj},Args.windowSize,round(trials(stim_trials)));
                        stim_sur(iters) = mean(r_surrogate);
                    end
                    pair_sur(stim_trials,:) = stim_sur;
                end
                rr_surrogate{pair} = pair_sur;
            end
        end
        ccg_surrogates = cell(1,pairs);
        for p = 1:pairs
            pp = rr_surrogate{p};
            ccg_surrogates{p} = prctile(pp',[95 99 99.9 99.99]);
        end
        if Args.ml
            cd(lfp2_dirs(ide_loc,:))
        end
        save ccg_surrogates ccg_surrogates
    end
    cd([ses filesep 'lfp' filesep 'lfp2'])
end

















