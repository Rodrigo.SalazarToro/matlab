function run_hsfpp_waveforms(varargin)

%run at session level
%previously known as run_hilbert_phase.m

%computes HSFPP for all clusters with all channels for 7 different time windows
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

% 8: ITI (incorrect, Betty only)
Args = struct('ml',0,'filter_low',8,'filter_high',25,'redo',0,'incorrect',0); %10 - 20?
Args.flags = {'ml','redo','incorrect'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;


if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    r = 1; %only identity need to save in identity dir
    
    if Args.incorrect
        total_trials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',r);
        
        cd([sesdir filesep 'lfp']);
        if exist('iti_rejectedTrials.mat','file') %this is currently only run on incorrect trials for betty days
            load('iti_rejectedTrials.mat','rejecTrials')
            iti_reject = rejecTrials;
            if ~isempty(iti_reject)
                [~,baditi] = intersect(total_trials,iti_reject);
                total_trials(baditi) = [];
            end
        end
        cd(sesdir)
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
    end
else
    mt = mtstrial('auto','redosetNames');
    %determine rule
    r = mt.data.Index(1,1);
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
end


%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%get group information
NeuroInfo = NeuronalChAssign(); %get the "group" number that corresponds to each channel

%find groups with spikes
groups = nptDir('group*');
ngroups = [1 : size(groups,1)];
pwd
for g = ngroups %spike channel
    fprintf('\n%0.5g     ',g)
%     for gg = ngroups %lfp channels

gg = g;
        fprintf(' %0.5g',gg)
        
        cluster_info = {};
        
        %determine which channels the group number corresponds to
        g2ch = str2double(groups(g).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,g2ch);
        channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
        
        
        
        %get lfp groups information
        lfpg = str2double(groups(gg).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,lfpg);
        channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
        
        %determine if pair has already been written
        if Args.ml
            if Args.incorrect
                h_pairs = [ 'incorrect_hilbertpair' num2strpad(g2ch,2) num2strpad(lfpg,2)];
            else
                h_pairs = [ 'hilbertpair' num2strpad(g2ch,2) num2strpad(lfpg,2)];
            end
        else
            h_pairs = [ 'hilbertpair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
        end
        cd([sesdir filesep 'lfp' filesep 'lfp2'])
        lfp2dir = pwd;
        spair = nptDir([h_pairs '.mat']);
        cd(sesdir)
        
        
        cd([sesdir filesep groups(g).name])
        %find clusters
        clusters = nptDir('cluster*');
        nclusters = size(clusters,1);
        
        for c = 1 %: nclusters
            cd([sesdir filesep groups(g).name])
            cd(clusters(c).name);
            cluster_info{1,c} = clusters(c).name;
            
            %get spikes
            load ispikes.mat
            %get lfp group number
            lfp_group = channels(2);
            cd([sesdir filesep 'lfp' filesep 'lfp2'])
            %get trials
            lfp = nptDir('*_lfp2*');
            
            if Args.incorrect
                s_angle = cell(1,8);
            else
                s_angle = cell(1,7);
            end
            wvcounter = 0;
            allsha = [];
            allwaveforms = [];
            for ttrial = total_trials
                
                
                cd(lfp2dir)
                cd ../..
                cd('highpass')
                [hdata,~,sampling_rate] = nptReadStreamerFile([lfp(ttrial).name(end-26:end-13) 'highpass' lfp(ttrial).name(end-8:end-4)]);
                cd(lfp2dir)
                
                load(lfp(ttrial).name);
                
                LFP = data(lfp_group,:);
                
                spike_data = sp.data.trial(ttrial).cluster.spikes;
                s = ceil(spike_data); %ceil because a zero spike occured when using "round"
                
                
                %get wave forms
                %sampling rate is 29990 for betty, so multiply by 30
                wind = 50;
                for alls = s
                    wvcounter = wvcounter + 1;
                    if (alls*30)+wind < size(hdata,2) && (alls*30)-wind > 0
                        
                        [we wee] = min(hdata(channels(1),(alls*30)-wind:(alls*30)+wind));
                        if wee < wind
                            wee = wind - wee;
                        else
                            wee = wee - wind;
                        end
                        allwaveforms(wvcounter,1:32) = zscore(hdata(lfp_group,(alls*30)-wee-16:(alls*30)-wee+15));
                        
                    else
                        allwaveforms(wvcounter,1:32) = nan;
                    end
                end
                
                %use hilbert transform to determine instantaneous phase angle for each spike
                h = hilbert(LFP); %don't need to transpose if only one channel
                
                %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
                %COMPENT IS FLIPPED
                ha = angle(h);
                sha = ha(s);
                allsha = [allsha sha];
                
                
            end
            
            c_mean = circ_mean(allsha');
            c_rtest = circ_rtest(allsha);
            nspikes = size(allsha,2);
            
            
            pref_waves = [];
            pw_counter = 0;
            for nsp = 1 : nspikes
                if c_mean < 0
                    if allsha(nsp) > c_mean-.05 && allsha(nsp) < c_mean+.05
                        pw_counter = pw_counter + 1;
                        pref_waves(pw_counter,:) = allwaveforms(nsp,:);
                    end
                end
            end
%             plot(nanmean(pref_waves),'b')
            
            allrandwaves = [];
            for nperms = 1 : 1000
                rp = randperm(nspikes);
                
                allrandwaves(nperms,:) = nanmean(allwaveforms(rp(1:pw_counter),:));
            end
%             hold on
%             plot(nanmean(allrandwaves),'k')
%             hold on
%             plot(nanmean(allrandwaves)+std(allrandwaves),'r')
%             hold on
%             plot(nanmean(allrandwaves)-std(allrandwaves),'r')
%             
            
        end
        h_pairs = [ 'hsfpp_waveforms' num2strpad(g2ch,2) num2strpad(lfpg,2)];
        save(h_pairs,'allrandwaves','pref_waves','nspikes','pw_counter','allsha')
%     end
end






cd(sesdir)