function predict_stimulus_batch(varargin)

%run at monkey level
%loops through days to calculate performance


Args = struct('days',[],'glm_data',0,'fix',0,'fits',0,'pp',0,'pf',0,'ff',0,'sig_interaction',0);
Args.flags = {'glm_data','fix','fits','pp','pf','ff','sig_interaction'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    
    cd([monkeydir filesep Args.days{d}])
    
    if Args.glm_data
        if Args.fix
            mts_glm_data('ml','train','block',1,'epoch',1,'sorted')
        else
            mts_glm_data('ml','train','block',1,'sorted')
        end
    end
    
    cd([monkeydir filesep Args.days{d} filesep 'session01'])
    

    
    if Args.fix
        if Args.fits
            train_predict_stimulus('fix','fits','block',1,'sorted')
        else
            train_predict_stimulus('fix','stim_prediction','block',1,'sorted')
        end
    else
        if Args.fits
            train_predict_stimulus('fits','block',1,'sorted')
        else
            if Args.sig_interaction
                if Args.pp
                    train_predict_stimulus('stim_prediction','block',1,'sorted','pp','sig_interaction')
                elseif Args.pf
                    train_predict_stimulus('stim_prediction','block',1,'sorted','pf','sig_interaction')
                elseif Args.ff
                    train_predict_stimulus('stim_prediction','block',1,'sorted','ff','sig_interaction')
                else
                    train_predict_stimulus('stim_prediction','block',1,'sorted','sig_interaction')
                end
            else
                if Args.pp
                    train_predict_stimulus('stim_prediction','block',1,'sorted','pp')
                elseif Args.pf
                    train_predict_stimulus('stim_prediction','block',1,'sorted','pf')
                elseif Args.ff
                    train_predict_stimulus('stim_prediction','block',1,'sorted','ff')
                else
                    train_predict_stimulus('stim_prediction','block',1,'sorted')
                end
            end
        end
    end
end

get_glm_totals
cd(monkeydir)