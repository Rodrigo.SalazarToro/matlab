function obj = mtsXCspikes(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'rule',1,'Surprctile',99.5,'surType',3,'Incor',0,'SurprctileCoh',99);
Args.flags = {'Auto','Incor'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsXCspikes';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsXC';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)


groups = nptDir('group*');

neuroinfo = NeuronalChAssign;
sdir = pwd;
count = 1;

mts = mtstrial('auto','redoSetnames');
histology = NeuronalHist;


if  ~isempty(groups) && ((length(unique(mts.data.Index(:,1))) == 1 && unique(mts.data.Index(:,1)) == Args.rule) || length(unique(mts.data.Index(:,1))) == 2)
    if strcmp(computer,'GLNXA64')
        [~,allfiles] = unix('find . -maxdepth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
    else
        [~,allfiles] = unix('find . -depth 3 -ipath ''*cluster*'' -name ''ispikes.mat''');
    end
    stfiles = findstr(allfiles,'ispikes.mat');
    scomb = nchoosek([1:length(stfiles)],2);
    auto = [(1:length(stfiles))' (1:length(stfiles))'];
    scomb = cat(1,scomb,auto);
    Index = zeros(size(scomb,1),12);
    cd xcorr
    for ff = 1 : size(scomb,1)
        fi1 = scomb(ff,1);
        fi2 = scomb(ff,2);
        sp1 = load(sprintf('%s/%s',sdir,allfiles(stfiles(fi1)-21:stfiles(fi1) +10)));
        sp2 = load(sprintf('%s/%s',sdir,allfiles(stfiles(fi2)-21:stfiles(fi2) +10)));
        if Args.Incor
            append1 = 'IncorMI.mat';
            append2 = 'IncorSurMIevoked.mat';
        else
            append1 = 'MI.mat';
            append2 = 'SurMIevoked.mat';
        end
        
        thefile = ['xcorrg' sp1.sp.data.groupname sp1.sp.data.cellname 'g' sp2.sp.data.groupname sp2.sp.data.cellname 'Rule' num2str(Args.rule) append1] ;
        EVfile = ['xcorrg' sp1.sp.data.groupname sp1.sp.data.cellname 'g' sp2.sp.data.groupname sp2.sp.data.cellname 'Rule' num2str(Args.rule) append2] ;
       
        if isempty(nptDir(thefile))
            thefile = ['xcorrg' sp2.sp.data.groupname sp2.sp.data.cellname 'g' sp1.sp.data.groupname sp1.sp.data.cellname 'Rule' num2str(Args.rule) append1] ;
            EVfile = ['xcorrg' sp2.sp.data.groupname sp2.sp.data.cellname 'g' sp1.sp.data.groupname sp1.sp.data.cellname 'Rule' num2str(Args.rule) append2] ;
        end
         
        tuning = {'Loc' 'Ide'};
        if ~isempty(nptDir(thefile)) && ~isempty(nptDir(EVfile))
            mis = load(thefile);
            bias = load(EVfile);
            if ~isempty(mis.timelag)
                for tune = 1 : 2
                    for item = 1 : 3
                        switch Args.surType
                            case 2
                                sur = squeeze(prctile(bias.mi{tune}(:,item,:,:),Args.Surprctile,1));
                                
                            case 1
                                sur = squeeze(prctile(mis.mi{tune}(1:end-1,item,:,:),Args.Surprctile,1));
                            case 3
                                
                                sur1 = squeeze(prctile(mis.mi{tune}(1:end-1,item,:,:),Args.Surprctile,1));
                                sur2 = squeeze(prctile(bias.mi{tune}(:,item,:,:),Args.Surprctile,1));
                                sur = zeros(4,41);
                                for ep = 1 : 4
                                    sur(ep,:) = max([sur1(ep,:);sur2(ep,:)]);
                                end
                        end
                        sig = squeeze(mis.mi{tune}(end,item,:,:)) > sur;
                        eval(['data.' tuning{tune} 'Item' num2str(item)  '(count,:) = single(sum(sig,2))'';']);
                        for ep = 1 : 4
                            eval(['data.' tuning{tune} 'Item' num2str(item)  'stlag{count,ep} = single(find(sig(ep,:) == 1));']);
                        end
                        for item2 = 1 : 3
                            eval(['data.' tuning{tune} 'Ncoi(count,item,item2) = single(mis.ncoi(tune,item,item2));']);
                        end
                    end
                end
            end
        else
            for tune = 1 : 2
                for item = 1 : 3
                    eval(['data.' tuning{tune} 'Item' num2str(item)  '(count,:) = single(nan(1,4))'';']);
                    for ep = 1 : 4
                        eval(['data.' tuning{tune} 'Item' num2str(item)  'stlag{count,ep} = single(nan(1,1));']);
                        
                    end
                    for item2 = 1 : 3
                        eval(['data.' tuning{tune} 'Ncoi(count,item,item2) = single(nan(1,1));']);
                    end
                end
            end
        end
        Index(count,1) = count;
        Index(count,2) = 1;
        
        Index(count,3) = str2double(sp1.sp.data.groupname);
        Index(count,4) = str2double(sp2.sp.data.groupname);
        
        Index(count,5) = find(str2double(sp1.sp.data.groupname) == neuroinfo.groups);
        Index(count,6) = find(str2double(sp2.sp.data.groupname) == neuroinfo.groups);
        
        Index(count,7) = sp1.sp.data.cellname(end);
        Index(count,8) = sp2.sp.data.cellname(end);
        
        Index(count,9) = str2double(sp1.sp.data.cellname(end-2:end-1));
        Index(count,10) = str2double(sp2.sp.data.cellname(end-2:end-1));
       
        mainfile = regexprep(thefile,'MI','');
        surmfile = regexprep(thefile,'MI','Surtrial');
        coh = load(mainfile,'S','phi');
        surcoh = load(surmfile,'S');
        
        mcoh = nan(4,1);
        mscoh = nan(4,1);
        for ep =1 : 4;
            try
                mcoh(ep) = nanmean(coh.S{ep},2);
                mscoh(ep) = nanmean(prctile(surcoh.S{ep},Args.SurprctileCoh,1),2);
            end
            try
                data.sigCoh(count,ep,:) = coh.S{ep} > prctile(surcoh.S{ep},Args.SurprctileCoh,1);
                data.sigPhase(count,ep,:) = single(squeeze(data.sigCoh(count,ep,:))' .* coh.phi{ep});
            catch
                data.sigCoh(count,ep,:) = false(1,101);
                data.sigPhase(count,ep,:) =  false(1,101);
            end
        end
        
        Index(count,11) = sum(mcoh > mscoh);
        if strcmp(sp1.sp.data.cellname(end),'m')
            st1 = load(regexprep(sprintf('%s/%s',sdir,allfiles(stfiles(fi1)-21:stfiles(fi1) +10)),'ispikes','stability'));
        else
            st1.stability = true;
        end
        
        if strcmp(sp2.sp.data.cellname(end),'m')
            st2 = load(regexprep(sprintf('%s/%s',sdir,allfiles(stfiles(fi2)-21:stfiles(fi2) +10)),'ispikes','stability'));
        else
            st2.stability = true;
        end
         Index(count,12) = st1.stability & st2.stability;
         
         if strcmp([sp1.sp.data.groupname sp1.sp.data.cellname],[sp2.sp.data.groupname sp2.sp.data.cellname])
             Index(count,13) = Inf;
         else
             try
                 load(['sccorrg' sp1.sp.data.groupname sp1.sp.data.cellname 'g' sp2.sp.data.groupname sp2.sp.data.cellname 'Rule' num2str(Args.rule) '.mat'])
             catch
                 load(['sccorrg' sp2.sp.data.groupname sp2.sp.data.cellname 'g' sp1.sp.data.groupname sp1.sp.data.cellname 'Rule' num2str(Args.rule) '.mat'])
             end
             
             Index(count,13) = sum(sum(isnan(rc)));
             Index(count,14) = mean(mincoi(1,:));
             Index(count,15) = mean(mincoi(2,17:33));
         end
         
      data.setNames{count} = [sdir '/xcorr'];
      %% firing rate analysis
        load([sdir '/group' sp1.sp.data.groupname '/cluster' sp1.sp.data.cellname sprintf('/nineStimPSTH%d.mat',Args.rule)]);
        raster = cat(1,A{:,1});
        nspikes = sum(sum(raster(:,1:1800)));
        
        fr1 = nspikes / (1.8*size(raster,1));
        
        load([sdir '/group' sp2.sp.data.groupname '/cluster' sp2.sp.data.cellname sprintf('/nineStimPSTH%d.mat',Args.rule)]);
        raster = cat(1,A{:,1});
        nspikes = sum(sum(raster(:,1:1800)));
        
        fr2 = nspikes / (1.8*size(raster,1));
        load([sdir '/group' sp1.sp.data.groupname '/cluster' sp1.sp.data.cellname sprintf('/nineStimPSTH%d.mat',Args.rule)]);
        raster = cat(1,A{:,2});
        nspikes = sum(sum(raster(:,1000:1800)));
        
        fr3 = nspikes / (0.8*size(raster,1));
        
        load([sdir '/group' sp2.sp.data.groupname '/cluster' sp2.sp.data.cellname sprintf('/nineStimPSTH%d.mat',Args.rule)]);
        raster = cat(1,A{:,2});
        nspikes = sum(sum(raster(:,1000:1800)));
        
        fr4 = nspikes / (0.8*size(raster,1));
        
        
        data.fr(count,:) = [fr1 fr2 fr3 fr4];
        %%
        data.hist(count,:) = [histology.number(Index(count,5)) histology.number(Index(count,6))];
        count = count + 1;
        
    end
    cd ..
    
    if exist('Index')
        
        data.Index = Index;
        data.numSets = count-1; % nbr of trial
        % create nptdata so we can inherit from it
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        fprintf('No SFC files \n');
        obj = createEmptyObject(Args);
    end
else
    % create empty object
    fprintf('No sorted group \n');
    obj = createEmptyObject(Args);
end




function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
data.hist = [];
data.fr = [];
tuning = {'Loc' 'Ide'};

for tune = 1 : 2
    for item = 1 : 3
        
        
        
        eval(['data.' tuning{tune} 'Item' num2str(item)  ' = [];']);
        
        eval(['data.' tuning{tune} 'Item' num2str(item)  'stlag = cell(1,4);']);
        
        
        eval(['data.' tuning{tune} 'Ncoi = [];']);
    end
end
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
