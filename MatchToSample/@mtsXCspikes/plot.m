function obj = plot(obj,varargin)



Args = struct('pLevel',2,'freqLim',[0 50],'plotType','standard','rule',1,'prctile',98,'prctileCoh',98,'tuningType','Ide','standardyaxis','coi','alltrials',0,'blockBased',0,'sepoch',[1 4],'prctileMI',95,'xcorr',1,'noSmoothing',0);
Args.flags = {'alltrials','blockBased','noiseCor','noSmoothing','xcorr'};

[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[~,dataindices] = get(obj,'Number',varargin2{:});


if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work soon
    ind = dataindices(n);
else
    
end

epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
epochs2 = {'pre-sample' 'pre-sample' 'sample' 'sample' 'delay1' 'delay1' 'delay2' 'delay2'};
tuning = {'location' 'identity'};
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
lbs = {'b' 'g' 'r' 'c'};
blocklb = {'b' 'r' 'b' 'r' 'b' 'r' 'b' 'r' 'b' 'r'};
maxn = [];
minn = [];
cd(obj.data.setNames{ind})
warning off
name1 = sprintf('%04g%02g%s',obj.data.Index(ind,3),obj.data.Index(ind,9),obj.data.Index(ind,7));
name2 = sprintf('%04g%02g%s',obj.data.Index(ind,4),obj.data.Index(ind,10),obj.data.Index(ind,8));



switch Args.plotType
    
    case {'coherence';'standard'; 'phase'}
        c = 1;
        for objs = 1 : 3;
            for locs = 1 : 3;
                if Args.alltrials
                    if Args.blockBased
                        if ~isempty(nptDir(sprintf('xcorrg%sg%sBlock%d.mat',name1,name2,c)))
                            thefile = sprintf('xcorrg%sg%sBlock%d',name1,name2,c);
                        else
                            thefile = sprintf('xcorrg%sg%sBlock%d',name2,name1,c);
                        end
                    else
                        if ~isempty(nptDir(sprintf('xcorrg%sg%sRule%d.mat',name1,name2,Args.rule)))
                            thefile = sprintf('xcorrg%sg%sRule%d',name1,name2,Args.rule);
                        else
                            thefile = sprintf('xcorrg%sg%sRule%d',name2,name1,Args.rule);
                        end
                        if strcmp(Args.plotType,'phase')
                            sur = load([thefile 'Surtrial'],'phi');
                        elseif strcmp(Args.plotType,'coherence')
                            sur = load([thefile 'Surtrial'],'S');
                        end
                    end
                else
                    if ~isempty(nptDir(sprintf('xcorrg%sg%sRule%dIde%dLoc%d.mat',name1,name2,Args.rule,objs,locs)))
                        thefile = sprintf('xcorrg%sg%sRule%dIde%dLoc%d',name1,name2,Args.rule,objs,locs);
                    else
                        thefile = sprintf('xcorrg%sg%sRule%dIde%dLoc%d',name2,name1,Args.rule,objs,locs);
                    end
                    
                end
                if ~isempty(nptDir([thefile '.mat']))
                    load(thefile)
                    
                    switch Args.plotType
                        
                        case {'coherence'; 'phase'}
                            if ~Args.blockBased
                                for ep = Args.sepoch(1) : Args.sepoch(2)
                                    if strcmp(Args.plotType,'coherence') || strcmp(Args.plotType,'phase')
                                        
                                        if Args.alltrials
                                            if strcmp(Args.plotType,'phase')
                                                thedata = phi{ep};
                                                thesur(1,:) = circ_mean(sur.phi{ep});
                                                stdt = circ_std(sur.phi{ep});
                                                thesur(2,:) = thesur(1,:) + 3*stdt;
                                                thesur(3,:) = thesur(1,:) - 3*stdt;
                                            elseif strcmp(Args.plotType,'coherence')
                                                thedata = S{ep};
                                                thesur = prctile(sur.S{ep},Args.prctileCoh,1);
                                            end
                                        end
                                    else
                                        thedata = phi{ep};
                                        thesur = [];
                                    end
                                    if  ~isempty(thedata)
                                        if ~Args.alltrials
                                            subplot(3,3,(objs-1)*3+locs)
                                        elseif strcmp(Args.plotType,'phase')
                                            
                                            subplot(4,1,ep)
                                        end
                                        plot(f, thedata,lbs{ep});
                                        hold on
                                        if Args.alltrials
                                            plot(f,thesur,[lbs{ep} '--'])
                                            
                                        end
                                        maxn = [maxn max(max(thedata))];
                                        minn = [minn min(min(thedata))];
                                    end
                                end
                                xlabel('Frequency (Hz)')
                                
                                ylabel(Args.plotType)
                                
                                xmin = Args.freqLim(1);
                                xmax = Args.freqLim(2);
                            elseif Args.blockBased
                                
                                for ep = Args.sepoch(1) : Args.sepoch(2)
                                    if strcmp(Args.plotType,'coherence')
                                        thedata = S{ep};
                                        
                                    else
                                        thedata = phi{ep};
                                        
                                    end
                                    thesur = [];
                                    if  ~isempty(thedata)
                                        subplot(4,1,ep)
                                        plot(f, thedata,blocklb{c});
                                        hold on
                                        
                                        maxn = [maxn max(max(thedata))];
                                        minn = [minn min(min(thedata))];
                                        xlim([Args.freqLim]);
                                    end
                                end
                                xlabel('Frequency (Hz)')
                                
                                ylabel(Args.plotType)
                                title(epochs{ep})
                                xmin = Args.freqLim(1);
                                xmax = Args.freqLim(2);
                                
                            end
                        case 'standard'
                            thedata = squeeze(nansum(crosscorr,3));
                            if strcmp(Args.standardyaxis,'Prob')
                                thedata = thedata ./repmat(nansum(thedata,2),1,41);
                            end
                            if ~Args.alltrials
                                subplot(3,3,(objs-1)*3+locs)
                            end
                            plot(timelag, thedata);
                            
                            maxn = [maxn max(max(thedata))];
                            minn = [minn min(min(thedata))];
                            xlabel('Time lags (ms)')
                            if strcmp(Args.standardyaxis,'Prob')
                                ylabel('Prob')
                            else
                                ylabel('# coincidences')
                            end
                            xmin = timelag(1);
                            xmax = timelag(end);
                    end
                    if ~Args.alltrials
                        title(sprintf('Ide %d; Loc %d',objs,locs))
                    end
                    c =c +1;
                end
            end
        end
        
        if Args.alltrials && ~Args.blockBased; legend(epochs); elseif Args.blockBased; else legend(epochs);end
        
        if ~isempty(minn) && ~isempty(maxn) && max(maxn) ~= min(minn)
            if ~Args.alltrials
                for sb = 1 : 9; subplot(3,3,sb); axis([xmin xmax 0.9*min(minn) 1.1*max(maxn)]);end
            end
        end
        
    case 'spikeCountCorr'

       if Args.xcorr
            prefix = 'xccorr';
        else
            prefix = 'sccorr';
        end
        if ~isempty(nptDir(sprintf('%sg%sg%sRule%depoch.mat',prefix,name1,name2,Args.rule)))
            thefile = sprintf('%sg%sg%sRule%depoch',prefix,name1,name2,Args.rule);
            sfile = sprintf('%sg%sg%sSurRule%depoch',prefix,name1,name2,Args.rule);
        else
            thefile = sprintf('%sg%sg%sRule%depoch',prefix,name2,name1,Args.rule);
            sfile = sprintf('%sg%sg%sSurRule%depoch',prefix,name2,name1,Args.rule);
            
        end
        load(thefile)
        try
            
            sur = load(sfile);
            xlabs = {'Time (ms) from sample onset' 'Time (ms) from match onset'};
            for sb = 1 : 2;
                subplot(1,2,sb)
                if Args.noSmoothing
                    plot(bins{sb}(2:end),rc(sb,1:end-1),'x')
                    hold on
                    confs=squeeze(prctile(sur.rc(:,sb,1:end-1),[Args.prctile 100-Args.prctile],1));
                    plot(bins{sb}(2:end),confs(1,:),'--')
                    plot(bins{sb}(2:end),confs(2,:),'--')
                else
                    plot(bins{sb}(2:end),filtfilt(repmat(1/2,1,2),1,rc(sb,1:end-2)))
                    hold on
                    confs=squeeze(prctile(sur.rc(:,sb,1:end-2),[Args.prctile 100-Args.prctile],1));
                    plot(bins{sb}(2:end),filtfilt(repmat(1/2,1,2),1,confs(1,:)),'--')
                    plot(bins{sb}(2:end),filtfilt(repmat(1/2,1,2),1,confs(2,:)),'--')
                end
%                 if sb ==1
%                     axis([bins{sb}(2) bins{sb}(end-1) -.3 .3])
%                 else
%                     axis([-750 150 -.3 .3])
%                 end
                xlabel(xlabs{sb})
                
            end
            ylabel('Spike count correlations')
        end
        
        
    case 'xccorrPerf'
        if ~isempty(nptDir(sprintf('xccorrg%sg%sRule%d50TrepochTrialBased.mat',name1,name2,Args.rule)))
            thefile = sprintf('xccorrg%sg%sRule%d50TrepochTrialBased',name1,name2,Args.rule);
        else
            thefile = sprintf('xccorrg%sg%sRule%d50TrepochTrialBased',name2,name1,Args.rule);
        end
       load(thefile)
        xlabs = {'Time (ms) from sample onset' 'Time (ms) from match onset'};
%         
        load(thefile)
        sdir = pwd;
        cd ..
        mts = mtstrial('auto','redosetNames');
        alltrials = find(mts.data.Index(:,1) == Args.rule);
        perf = nan(length(alltrials) - 49,1);
        for tbin = 1 : length(alltrials) - 49
            strials = alltrials(tbin : 49 + tbin);
            perf(tbin) = 100*sum(mts.data.BehResp(strials)) /50;
        end
        cd(sdir)
        xx = [1:length(perf)];
        
        cdata = cat(2,squeeze(rc(:,1,1:3)),squeeze(rc(:,2,2)));
        
        [AX,H1,H2] = plotyy(xx(1:size(cdata,1)),perf(1:size(cdata,1)),xx(1:size(cdata,1)),cdata);
        set(H1,'LineStyle','--','LineWidth',2)
        set(get(AX(1),'Ylabel'),'String','Perforance (%)')
        set(get(AX(2),'Ylabel'),'String','xcorr of spikes')
        legend('performance','pre-sample','sample','delay1','delay2')
        xlabel('Trial #')
        title(['PRE: ' num2str(allR(1,end)) ' SA: ' num2str(allR(2,end)) ' D1: ' num2str(allR(3,end)) ' D2: ' num2str(allR(4,end))])
        
    case 'spikeCountCorrRules'
        ruleslb = {'r-.' 'b-.'};

        if Args.xcorr
            prefix = 'xccorr';
        else
            prefix = 'sccorr';
        end
        for rule = 1 : 2
            if ~isempty(nptDir(sprintf('%sg%sg%sRuleComp.mat',prefix,name1,name2)))
                thefile = sprintf('%sg%sg%sRuleComp',prefix,name1,name2);
                
                rfile = sprintf('%sg%sg%sRule%d',prefix,name1,name2,rule);
            else
                thefile = sprintf('%sg%sg%sRuleComp',prefix,name2,name1);
                
                rfile = sprintf('%sg%sg%sRule%d',prefix,name2,name1,rule);
            end
            load(thefile)
            load(rfile)
            
            xlabs = {'Time (ms) from sample onset' 'Time (ms) from match onset'};
            for sb = 1 : 2;
                subplot(1,2,sb)
                plot(bins{sb},rc(sb,1:end-1),ruleslb{rule})
                hold on
                plot(bins{sb},diffrc(sb,1:end-1),'k.-')
                plot(bins{sb},squeeze(prctile(diffrcSur(:,sb,1:end-1),[Args.prctileCoh 100-Args.prctileCoh],1)),'k--')
                
                axis([bins{sb}(1) bins{sb}(end) -.2 .2])
                xlabel(xlabs{sb})
                if sb == 2; xlim([-800 200]); end
            end
            
            ylabel('Spike count correlations')
        end
        legend('IDE cor','diff Rules cor','upper conf. interval','lower conf. interval','LOC cor')
        
    case 'MI'
        try
            load(sprintf('xcorrg%sg%sRule%dMI',name1,name2,Args.rule));
            EVfile = load(sprintf('xcorrg%sg%sRule%dSurMIevoked',name1,name2,Args.rule));
        catch
            load(sprintf('xcorrg%sg%sRule%dMI',name2,name1,Args.rule));
            EVfile = load(sprintf('xcorrg%sg%sRule%dSurMIevoked',name2,name1,Args.rule));
        end
        maxinfo = [];
        mininfo = [];
        switch Args.tuningType
            
            case 'Ide'
                tune = 2;
                itemL = 'location';
            case 'Loc'
                tune = 1;
                itemL = 'object';
        end
        
        for item = 1 : 3
            for ep = 1 : 4
                subplot(3,4,(item-1)*4+ep)
                data = squeeze(mi{tune}(end,item,ep,:));
                %                 if isempty(EVfile.mi{tune})
                %                     ExpV = zeros(size(data,1),1);
                %                     warning('no expected valuer calculated')
                %                 else
                MIevoked1 = squeeze(EVfile.mi{tune}(:,item,ep,:));
                MIevoked2 = squeeze(mi{tune}(1:end-1,item,ep,:));
                ExpV1 = zeros(size(MIevoked1,2),1);
                ExpV2 = zeros(size(MIevoked2,2),1);
                for tl = 1 : size(MIevoked1,2);
                    [n,xout] = hist(MIevoked1(:,tl),size(MIevoked1,1));
                    ExpV1(tl) = sum((n/size(MIevoked1,1)).*xout);
                    [n,xout] = hist(MIevoked2(:,tl),size(MIevoked2,1));
                    ExpV2(tl) = sum((n/size(MIevoked2,1)).*xout);
                end
                %                 end
                ExpV = mean([ExpV1 ExpV2],2);
                plot(timelag,data);
                hold on
                plot(timelag,ExpV,'r')
                plot(timelag,prctile(MIevoked1,Args.prctile),'k--')
                maxinfo = [maxinfo nanmax(nanmax(nanmax(nanmax(data))))];
                mininfo = [mininfo nanmin(nanmin(nanmin(nanmin(data))))];
                sur = squeeze(prctile(MIevoked1(:,:),Args.prctile,1))';
                sur2 = squeeze(prctile(mi{tune}(1:end-1,item,ep,:),Args.prctile,1));
                
                plot(timelag,sur2,'--');
                plot(timelag,sur,'k--');
                sig = data > sur & data > sur2;
                plot(timelag(sig),data(sig),'r*')
                if item == 1; title(epochs{ep}); end
                if ep == 1; ylabel(sprintf('%s # %d',itemL,item));end
            end
        end
        
        
        xlabel('Time lag (ms)')
        ylabel('Info (bits)')
        legend({'raw MI' 'Bias' [num2str(Args.prctile) 'surrogate'] [num2str(Args.prctile) 'surrogate locked']})
        if max(maxinfo) > 0
            for sb = 1 :12; subplot(3,4,sb);ylim([1.1*min(mininfo) 1.1*max(maxinfo)]); xlim([timelag(1) timelag(end)]); end
        end
        
    case 'phaseDiffEntr'
        try
            load(sprintf('phiDiffg%sg%sRule%d.mat',name1,name2,Args.rule));
        catch
            load(sprintf('phiDiffg%sg%sRule%d.mat',name2,name1,Args.rule));
        end
        h= zeros(2000,1);
        for ti = 1 : 2001
            h(ti) = entropy(phiDiff(:,ti));
        end
        xi = [-1800:200];
        plot(xi,filtfilt(repmat(1/50,1,50),1,h))
        ylabel('Raw entropy')
        xlabel('Time from Match onset (ms)')
        xlim([-800 200])
    case 'MIphiRule'
        
        addName = {'SampleLockLock' 'Lock'};
        legs = {'Time from Sample offset (ms)' 'Time from Match onset (ms)'};
        xlims = {[-1000 800] [ -800 200]};
        for sb =1 : 2
            
            try
                load(sprintf('MIphiRuleg%sg%s%s.mat',name1,name2,addName{sb}));
            catch
                load(sprintf('MIphiRuleg%sg%s%s.mat',name2,name1,addName{sb}));
            end
            if sb ==1
                time = [-1000: 1000];
            end
            subplot(1,2,sb)
            plot(time,filtfilt(repmat(1/50,1,50),1,mu))
            
            hold on
            
            ExpV = getExpValue(su,[1:2001]);
            
            rmu = mu - ExpV;
            
            plot(time,filtfilt(repmat(1/50,1,50),1,ExpV),'k')
            
            plot(time,filtfilt(repmat(1/50,1,50),1,rmu),'r')
            
            plot(time,filtfilt(repmat(1/50,1,50),1,prctile(su,Args.prctileMI,1)),'--')
            
            legend('raw MI','ExpV','realMI',[num2str(Args.prctileMI) ' conf. int.'])
            
            
            xlim(xlims{sb})
            xlabel(legs{sb})
        end
        ylabel('mutual information')
    case 'RuleSelectivity'
        tuning = {'Ide' 'Loc'};
        rules = {'IDE' 'LOC'};
        for rule = 1 : 2
            for tune = 1 : 2
                for item = 1 : 3
                    try
                        load(sprintf('xccorrg%sg%sRule%d%s%d.mat',name1,name2,rule,tuning{tune},item));
                        surfile = load(sprintf('xccorrg%sg%sSurRule%d%s%d.mat',name1,name2,rule,tuning{tune},item));
                    catch
                        load(sprintf('xccorrg%sg%sRule%d%s%d.mat',name2,name1,rule,tuning{tune},item));
                        surfile = load(sprintf('xccorrg%sg%sSurRule%d%s%d.mat',name2,name1,rule,tuning{tune},item));
                    end
                    
                    data = [rc(1,1:3) rc(2,3)];
                    me = [squeeze(mean(surfile.rc(:,1,1:3)))' squeeze(mean(surfile.rc(:,2,3)))];
                    st = [squeeze(std(surfile.rc(:,1,1:3)))' squeeze(std(surfile.rc(:,2,3)))];
                    zscore = (data - me) ./ st;
                    
                    subplot(2,2,(rule-1)*2+tune)
                    plot(zscore)
                    hold on
                    line([1 4],[ 3 3],'LineStyle','--');
                    line([1 4],[ -3 -3],'LineStyle','--');
                    if tune ==1; ylabel([rules{rule} ' zscore']); end
                    title(tuning{tune})
                    xlabel('Epochs')
                    ylim([-8 8])
                    set(gca,'TickDir','out')
                end
            end
        end
        
        
end

try
    
    set(gcf,'Name',[obj.data.setNames{ind} 'g' name1 areas{obj.data.hist(ind,1)} '-' 'g' name2 areas{obj.data.hist(ind,2)}])
catch
    set(gcf,'Name',[obj.data.setNames{ind} 'g' name1 '?' '-' 'g' name2 '?'])
    
end
