function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'unit1Type',[],'unit2Type',[],'unit1Group',[],'unit2Group',[],'hist',[],'tuning',[],'Item',[1:3],'minSigLag',2,'minNcoi',[4*41*0 inf] ,'epochs',[3:4],'AC','excluded','diffCongrUncongr',[],'cohAboveSur',0,'withUnstablePairs',0,'numNaN',1,'minfr',2,'mincoi',0);%'minNcoi',[4*41*0.1 inf]
Args.flags = {'Number','ObjectLevel','sameElec','includeAC','onlyAC','cohAboveSur','withUnstablePairs'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.day)
        rtemp = find(obj.data.Index(:,2) == Args.day);
    else
        rtemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.diffCongrUncongr)
        try
           load /Volumes/raid/data/monkey/CongrUncongrTrialRT.mat 
        catch
            load /media/raid/data/monkey/CongrUncongrTrialRT.mat 
        end
        switch  Args.diffCongrUncongr{1}
            case 'noDiff'
               ndays = find(pvalue(Args.diffCongrUncongr{2},:) >= 0.05); 
            case 'Diff'
                 ndays = find(pvalue(Args.diffCongrUncongr{2},:) < 0.05); 
        end
        rtempd = [];
        for d = 1 : length(ndays)
            rtempd = [rtempd find(~cellfun(@isempty,strfind(obj.data.setNames,days{ndays(d)})) ==1)];
        end
        rtemp = intersect(rtemp,rtempd);
    end
    
    
    if ~isempty(Args.unit1Group)
        rtemp = intersect(find(obj.data.Index(:,3) == cell2mat(Args.unitcortex)),rtemp);
        
    end
    
    if ~isempty(Args.unit2Group)
        rtemp = intersect(find(obj.data.Index(:,4) == cell2mat(Args.unitcortex)),rtemp);
        
    end
    
    if ~isempty(Args.unit1Type)
        if ~strcmp(Args.unit1Type,'s')
            rtemp = intersect(find(obj.data.Index(:,7) == 109),rtemp);
        else
            rtemp = intersect(find(obj.data.Index(:,7) == 115),rtemp);
        end
    end
    
    if ~isempty(Args.unit2Type)
        if ~strcmp(Args.unit2Type,'s')
            rtemp = intersect(find(obj.data.Index(:,8) == 109),rtemp);
        else
            rtemp = intersect(find(obj.data.Index(:,8) == 115),rtemp);
        end
    end
    if ~isempty(Args.hist)
        [sind1,~] = find(ismember(obj.data.hist,Args.hist,'rows') ==1);
        [sind2,~] = find(ismember(obj.data.hist,fliplr(Args.hist),'rows') ==1);
        sind = union(sind1,sind2);
        rtemp = intersect(rtemp,sind);
    end
    
    if ~isempty(Args.tuning)
        allrt = [];
        for ii = 1 : length(Args.Item)
            item = Args.Item(ii);
            thedata = eval(['obj.data.' Args.tuning 'Item' num2str(item)]);
%             timelag = eval(['obj.data.' Args.tuning 'Item' num2str(item) 'stlag']);
            rtemp2 = find(sum(thedata(:,Args.epochs) >= Args.minSigLag,2) >= 1);
%             adj = zeros(size(timelag,1),length(Args.epochs));
%             for ii = 1 : size(timelag,1); 
%                 for ep = 1 : length(Args.epochs);
%                     if ~isempty(diff(timelag{ii,Args.epochs}))
%                     adj(ii,ep)= sum(diff(timelag{ii,Args.epochs}) == 1);
%                     end
%                 end;
%             end
%             rtemp3 = find(sum(adj ==1,2));
            
            allrt = [allrt;rtemp2];
        end
        crt = unique(allrt);
        
        rtemp = intersect(rtemp,crt);
    end
    
    
%     if ~isempty(Args.minNcoi)
%         thedata1 = find(nansum(nansum(obj.data.IdeNcoi > Args.minNcoi(1),3),2) == 9);
%         thedata2 = find(nansum(nansum(obj.data.LocNcoi > Args.minNcoi(1),3),2) == 9);
%         
%         stdNcoi1 = find(sum(sqrt(nanstd(obj.data.IdeNcoi,[],3)) ./ nanmean(obj.data.IdeNcoi,3) < Args.minNcoi(2),2) == 3);
%         stdNcoi2 = find(sum(sqrt(nanstd(obj.data.LocNcoi,[],3)) ./ nanmean(obj.data.LocNcoi,3) < Args.minNcoi(2),2) == 3);
%         rtt = intersect(stdNcoi1,stdNcoi2);
%         
%         rtemp = intersect(rtemp,intersect(thedata1, thedata2));
%         rtemp = intersect(rtemp,rtt);
%     end
    
    rtempi = diff(obj.data.Index(:,3:10),1,2);
    rtempr = find(ismember(rtempi(:,[1 3 5]),[0 0 0],'rows') ==1);
    switch Args.AC
        case 'included'
            rtemp = union(rtemp,rtempr);
        case 'excluded'
            rtemp = setdiff(rtemp,rtempr);
        case 'only'
            rtemp = intersect(rtemp,rtempr);
            
    end
    if Args.cohAboveSur
        art = find(obj.data.Index(:,11) >= 1);
    else
        art = find(obj.data.Index(:,11) == 0);
        
    end
     rtemp = intersect(rtemp,art);
    if ~Args.withUnstablePairs
    stable = find(obj.data.Index(:,12) == 1);
    
    rtemp = intersect(rtemp,stable);
    
    end
    
    enan = find(obj.data.Index(:,13) < Args.numNaN);
    rtemp = intersect(rtemp,enan); 
    
    mincoi = find(obj.data.Index(:,14) > Args.mincoi & obj.data.Index(:,15) > Args.mincoi);
    
    rtemp = intersect(rtemp,mincoi); 
    
    minfr = find(sum(obj.data.fr > Args.minfr,2) == 4);
    rtemp = intersect(rtemp,minfr);
    
   
    varargout{1} = rtemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

