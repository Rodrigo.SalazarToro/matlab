function summarize_spike_hilbert_analysis


cd('/media/raid/data/monkey/clark/')


% load idedays
% 
% allsph = processDays(spikeplvhsfpp,'days',idedays,'NoSites');
% save allsph allsph
load allsph allsph
[icross1 iicross1] = get(allsph,'Number','sigcross1','minspikes',3);
[iminspcross1 iiminspcross1] = get(allsph,'Number','minspikes',3);


cross1percent = icross1/iminspcross1 * 100

[icross2 iicross2] = get(allsph,'Number','sigcross2','minspikes',4);
[iminspcross2 iiminspcross2] = get(allsph,'Number','minspikes',4);


cross2percent = icross2/iminspcross2 * 100



crossspikes = [allsph.data.Index(iicross1,3);allsph.data.Index(iicross2,4)]';

crossfields = [allsph.data.Index(iicross1,4);allsph.data.Index(iicross2,3)]'; 

subplot(1,2,1)
hist(crossspikes,[1:13])
% subplot(1,2,2)
% hist(crossfields,[1:12])

sp2f = [allsph.data.Index(iicross1,3:4); fliplr(allsph.data.Index(iicross2,3:4))];
sp2fminsp = [allsph.data.Index(iiminspcross1,3:4); fliplr(allsph.data.Index(iiminspcross2,3:4))];


un = unique(sp2f,'rows');
usp = unique(crossspikes);
c = 0;
for nn = usp
    c = c + 1;
    
    spikeareas(1,c) = nn;
    spikeareas(2,c) = size(find(sp2fminsp(:,1) == nn),1);
end


h = hist(crossspikes,[1:max(usp)]);
for sa = 1 : size(spikeareas,2)
    h(spikeareas(1,sa)) = (h(spikeareas(1,sa)) / spikeareas(2,sa)) * 100; 
end
subplot(1,2,2)
bar(h)



u = unique([crossspikes crossfields]);
reverse_categorizeNeuronalHist([u])

numun = size(un,1);
un
counts = zeros(1,numun);
for nm = 1 : numun
    for nmm = 1: size(sp2f,1);
        if intersect(sp2f(nmm,:),un(nm,:),'rows')
            counts(nm) = counts(nm) + 1;
        end
    end
end
counts











