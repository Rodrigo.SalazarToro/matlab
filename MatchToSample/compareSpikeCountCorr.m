function [diffrc,bins,diffrcSur] = compareSpikeCountCorr(sp1,sp2,mts,trials1,trials2,varargin)


Args = struct('save',0,'redo',0,'addName',[],'iterations',500,'xcorr',0);
Args.flags = {'save','redo','surrogate','xcorr'};
[Args,modvarargin]  = getOptArgs(varargin,Args,'remove',{'save'});

if Args.xcorr
    matfile = sprintf('xccorrg%s%sg%s%sRuleComp%s.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);
    
else
    matfile = sprintf('sccorrg%s%sg%s%sRuleComp%s.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);
end
if isempty(nptDir(matfile)) || Args.redo
    
    [rc1,bins] = spikeCountCorr(sp1,sp2,mts,trials1,modvarargin{:});
    
    [rc2,bins] = spikeCountCorr(sp1,sp2,mts,trials2,modvarargin{:});
    
    diffrc = rc1 - rc2;
    
    alltrials = cat(1,trials1,trials2);
    diffrcSur = nan(Args.iterations,size(rc1,1),size(rc1,2));
    for ii = 1 : Args.iterations
        randtrials = alltrials(randperm(length(alltrials)));
        [rc1,bins] = spikeCountCorr(sp1,sp2,mts,randtrials(1:length(trials1)),modvarargin{:});
        
        [rc2,bins] = spikeCountCorr(sp1,sp2,mts,randtrials(length(trials1)+1:end),modvarargin{:});
        
        diffrcSur(ii,:,:) = rc1 - rc2;
        
    end
    
    if Args.save
       save(matfile,'diffrc','bins','diffrcSur') 
        display(['Saving ' pwd '/' matfile]);
        
    end
else
    load(matfile)
end

