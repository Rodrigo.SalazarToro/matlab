function obj = natimagetrial(varargin)
% @dstrial Constructor function for DSTRIAL class
%   OBJ = dstrial('auto') attempts to create a dstrial object by
%   using the files *.seq *.mrk *.ini and *timing.mat (which are assumed to be
%   in the current directory). The  object contains all the
%   following fields for each trial:
%
%   The data are raw vectors of all the trials for a given session.
%
%   Dependencies: ProcessSessionMTS,nptdata.


Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'natimagetrial';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'natimagetrial';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end


%%
function obj = createObject(Args,varargin)

%try to make object if there is an error then create and empty object
%check to see if the session is a natrual image session
if  exist('natimages.txt','file')
    
    %get BHV data
    bhv = nptDir('*.bhv');
    b = bhv_read([cd filesep bhv.name]);
    
    %get index of usable trials. includes all trials that a fixation was
    %made and an image was shown
    trials = find(b.NumCodes > 8)'; %see the natural images timing file (natural_images.m)
    
    ntrials = size(trials,2);
    data.image_on = zeros(ntrials,1);
    t = 0;
    for x = trials
        t = t + 1;
        
        code_times = b.CodeTimes{x} - b.CodeTimes{x}(1);
        data.image_on(t) = code_times(5);
    end
    
    data.setNames{1} = pwd;
    data.numSets = ntrials;
    
    
    %%
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
else
    obj = createEmptyObject(Args);
end

%%
function obj = createEmptyObject(Args)


data.image_on = [];
data.setNames = {};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
