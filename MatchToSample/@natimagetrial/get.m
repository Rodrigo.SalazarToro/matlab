function [r,varargout] = get(obj,varargin)
%   dstrial/get Get function for dstrial objects
%   The following arguments can be used:
%
%   - ObjPos: gets index of a specific object location
%
%   - rtime: gets only trials w/ reaction times > rtime(ms)

Args = struct('Number',0,'ObjPos',[],'rtime',[],'ObjectLevel',0);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);

global pos

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    %get index of specific object location
    if ~isempty(Args.ObjPos)
        pos = find(obj.data.CueLoc == Args.ObjPos);
    else
        pos = [1 : length(obj.data.CueLoc)]; %if unspecified
    end
    
    if ~isempty(Args.rtime)
        react = find(obj.data.FirstSac >= Args.rtime);
    else
        react = [1 : length(obj.data.FirstSac)];
    end
      
varargout{1} = intersect(pos,react);
r = length(varargout{1});

                                    elseif(Args.ObjectLevel)
                                        r = 'Session';
else
r = get(obj.nptdata,varargin{:});
end



