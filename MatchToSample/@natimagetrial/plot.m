function obj = plot(obj,varargin)
%   @dstrial/plot plots histograms of reaction, histograms of cue locations
%   or plots the eye traces on XY from dstrial object.
%
%   The following optional input arguments are valid:
%
%     - Space : plots the eye traces on XY axes.
%
%     - ObjPos : used to select a specfic object location
%       [] = all locations, 1:8 = specific locations 
%
%     - Hist : plots a histogram of the ocurrence of each object location.
%       
%     - Reaction : plots a histogram of reaction times for all object
%     locations or for a specific location if Args.ObjPos.

%     - Save : saves the eye traces on plot  
%
%     - Note: 'Space','Hist','Reaction' must be used separately


Args = struct('Space',0,'ObjPos',[],'Hist',0,'Reaction',0,'save',0);
Args.flags = {'Space','Hist','Reaction','save'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'Space','Hist'});

global pos

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    %check to see if their are trials
    if isempty(dataindices)
        fprintf(1,'No trials match criteria')
        fprintf(1,'\n');
        return
    else
    ind = dataindices(n);
    end
end

%if Args.ObjPos then the index is for a specific location
%else, all locations
trials = ind;

if Args.Hist % staying here 
    hist(obj.data.CueLoc)
end

if Args.Reaction
    if Args.ObjPos
        hist(obj.data.FirstSac(pos))
    else
        for x = 1 : 8
            index = find(obj.data.CueLoc == x);
            subplot(2,4,x);
            hist(obj.data.FirstSac(index));
            axis([0 400 0 10])
        end
    end
end

if Args.Space
    
    %plots only data from FIX OFF to MATCH (correct)
    s = cell2mat(obj.data.AnalogData(trials));
    scatter(s(:,1),s(:,2));
    hold on
    scatter(0,0,'r','.','SizeData',500)
    hold on

    %Cue Locations (CLOCKWISE starting at top 0,5) numbers 1 : 8
    locs = ([0 5; 3.536 3.536; 5 0; 3.536 -3.53; 0 -5; -3.53 -3.53; -5 0; -3.53 3.536]);

    objloc = locs(obj.data.CueLoc(trials),:);
    scatter(objloc(1),objloc(2),'g','.','SizeData',500)
    hold on
    axis([-6 6 -6 6]); 

    if (Args.save) && (n == 1)
 
        figure
        for p = pos
            s = cell2mat(obj.data.AnalogData(p));
            scatter(s(:,1),s(:,2),'b','.','SizeData',10);
            hold on

            %Cue Locations (CLOCKWISE starting at top 0,5) numbers 1 : 8
            locs = ([0 5; 3.536 3.536; 5 0; 3.536 -3.53; 0 -5; -3.53 -3.53; -5 0; -3.53 3.536]);

            objloc = locs(obj.data.CueLoc(p),:);
            scatter(objloc(1),objloc(2),'g','.','SizeData',50)
            axis([-6 6 -6 6]); 
        end 
            scatter(0,0,'r','.','SizeData',50)
            hold on   
    end
end 



