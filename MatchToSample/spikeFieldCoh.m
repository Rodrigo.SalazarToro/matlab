function [C,phi,f,unit,varargout] = spikeFieldCoh(varargin)
% To be run in the session directory
% type can be 'PP', 'PF' or 'Inter'
% cautious modvarargin will pass into cohsurrogate only in local machines
% not in wintermute
% C{p}(:,s,ch)

Args = struct('save',0,'plength','all','downSR',200,'type','Inter','plotting',0,'surrogate',0,'remoteName',[],'NoCheckStability',0,'STA',0,'locTuning',0,'ideTuning',0,'allCuesTuning',0,'stable',0,'BehResp',1,'ML',0);
Args.flags = {'save','plotting','surrogate','NocheckForStability','STA','locTuning','ideTuning','allCuesTuning','stable','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'type','plotting','surrogate','NocheckForStability','STA'});


mtst = mtstrial('auto');
allloc = vecr(unique(mtst.data.CueLoc));
allobj = unique(mtst.data.CueObj);

% if Args.nineStim
%     cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
%     allloc = vecr(unique(mtst.data.CueLoc));
%     allobj = unique(mtst.data.CueObj);
%     for cue = 1 : 9; trials{cue} = mtsgetTrials(mtst,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),modvarargin{:}); end
%     trials{10} = mtsgetTrials(mtst,modvarargin{:});
% else
%     trials{1} = mtsgetTrials(mtst,modvarargin{:});
% end
if Args.locTuning
    ncues = 3;
    arg{1} = 'allloc(cues)';
    arg{2} = 'allobj';
    matfile = 'SFClocTuning';
elseif Args.ideTuning
    ncues = 3;
    arg{1} = 'allloc';
    arg{2} = 'allobj(cues)';
    matfile = 'SFCideTuning';
elseif Args.allCuesTuning
    ncues = 9;
    cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    arg{1} = 'allloc(cueComb(1,cues))';
    arg{2} = 'allobj(cueComb(2,cues))';
    matfile = 'SFCallCuesTuning';
else
    ncues = 1;
    arg{1} = 'allloc';
    arg{2} = 'allobj';
    matfile = 'SFCallCuesComb';
end
if isnumeric(Args.plength)
    nper = 4;
    
else
    nper = 1;
    matfile = 'SFCallCuesCombWholeTrial';
end
[NeuroInfo,~] = NeuronalChAssign(sprintf('comb%s',Args.type));
[channels,~,~] = checkChannels(sprintf('coh%s',Args.type));

creal = 1;
nchannels = [];
for c = 1 : length(channels)
    tgroup = sprintf('group%04.f',NeuroInfo.groups(channels(c)));
    if ~isempty(nptDir(tgroup))
        group(creal).name = tgroup;
        nchannels = [nchannels channels(c)];
        creal = creal + 1;
    end
end

if Args.ML; nrules = 2; else nrules = 1 ;end

if ~isempty(nchannels)
    plength=Args.plength;
    
    if ~isempty(Args.remoteName)
        out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
        params{1} = struct('tapers',[5 9],'Fs',Args.downSR,'fpass',[0 100],'trialave',1);
    else
        params = struct('tapers',[5 9],'Fs',Args.downSR,'fpass',[0 100],'trialave',1);
    end
    %     if Args.ML; eval('for r = 1 : 2'); end  need to work on tmeporary
    %     make and unmake the next line nad l 266
    for r = 1 : nrules
        clear unit datacue trii
        for cues = 1 : ncues
            if Args.ML
                Tr = mtsgetTrials(mtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),'rule',r,modvarargin{:});
            else
                Tr = mtsgetTrials(mtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),modvarargin{:});
            end
            if ~isempty(Tr)
                trials{cues} = Tr;
                [timeserie,~] = lfpPcut(Tr,nchannels,'plength',Args.plength,modvarargin{:});
                for p = 1 : nper; datacue(cues).data{p} = single(timeserie{p});end ;clear timeserie;
                sua = 1;
                for g = 1:size(group,2)
                    if ~Args.NoCheckStability
                        [modtrials,cluster] = checkForStability(group(g).name);
                    else
                        modtrials = trials;
                        cluster = nptDir('cluster*');
                    end
                    cd(group(g).name)
                    for cl = 1 : size(cluster,1)
                        
                        [stableTrials,~,trii{sua,cues}] = intersect(modtrials{cl},trials{cues});
                        
                        if ~isempty(stableTrials) && ~isempty(trii{sua,cues}) && unique(stableTrials == trials{cues}) == 1
                            clname = cluster(cl).name;
                            cd(clname)
                            load ispikes.mat
                            tcount = 1;
                            for t = vecr(stableTrials);
                                if isnumeric(Args.plength)
                                    periods(1) = round((mtst.data.CueOnset(t) - plength));
                                    periods(2) = round(mtst.data.CueOnset(t));
                                    periods(3) = round(mtst.data.CueOffset(t));
                                    periods(4) = round((mtst.data.MatchOnset(t) - plength));
                                    
                                    for p = 1 : length(periods)
                                        d = vecc(sp.data.trial(t).cluster.spikes);
                                        ind = (d>=periods(p) & d<=periods(p)+plength);
                                        if isempty(ind);
                                            spiketimes = [];
                                            
                                        else
                                            spiketimes = d(ind)-periods(p);
                                        end
                                        unit(sua).cue(cues).period(p).trial(tcount).times = single(spiketimes/1000);
                                        
                                    end
                                else
                                    unit(sua).cue(cues).period(1).trial(tcount).times = single(vecc(sp.data.trial(t).cluster.spikes)/1000);
                                end
                                tcount = tcount +1;
                            end
                            %         end
                            unit(sua).name = sprintf('g%s%s',sp.data.groupname,sp.data.cellname);
                            unit(sua).path = pwd;
                            cd ..
                        else
                            clname = cluster(cl).name;
                            cd(clname)
                            load ispikes.mat
                            for p = 1 : nper; unit(sua).cue(cues).period(p).trial(1).times = []; end
                            unit(sua).name = sprintf('g%s%s',sp.data.groupname,sp.data.cellname);
                            unit(sua).path = pwd;
                            cd ..
                        end
                        sua = sua + 1;
                        
                    end
                    cd ..
                end
            end
        end
        
        count = 1;
        list = [];
        
        %     if ~isempty(Args.remoteName);for p = 1 : length(periods); C{p} = nan(65, size(group,2) , length(trials)); phi{p} = nan(65, size(group,2) , length(trials)); SurProb{p} = nan(65,3, size(group,2) , length(trials));;end; end
        
        
        if exist('sua')
            for s = 1 : (sua-1)
                if Args.ML; therule = r; else; therule = unique(mtst.data.Index(:,1)); end
                thepath = unit(s).path(length(pwd)+1:end);
                C = cell(4,1); phi = cell(4,1); SurProb = cell(4,1); f = [];
                [pfile,results] = unix(sprintf('find . -name ''%s%d%d%d.mat'' -path ''.%s*''',matfile,Args.stable,Args.BehResp,therule,thepath));
                if  isempty(Args.remoteName) && isempty(results) || ~isempty(Args.remoteName)
                    for cc = 1 : length(trials)
                        for ch = 1 : size(group,2)
                            for p = 1 : nper
                                lfp = squeeze(datacue(cc).data{p}(:,trii{s,cc},ch));
                                if ~isempty(trii{s,cc})
                                    if ~isempty(Args.remoteName)
                                        if ~isempty(squeeze(datacue(cc).data{p}(:,trii{s,cc},ch))) & ~isempty(unit(s).cue(cc).period(p).trial)
                                            lfpt{count} = squeeze(datacue(cc).data{p}(:,trii{s,cc},ch));
                                            unitt{count} = unit(s).cue(cc).period(p).trial;
                                            [form,~,~,~,~,f,] = coherencycpt(lfp,unit(s).cue(cc).period(p).trial,params{1},1);
                                            list = [list 1];
                                            count = count + 1;
                                        else
                                            list = [list 0];
                                        end
                                    else
                                        if isempty(lfp) | isempty(unit(s).cue(cc).period(p).trial)
                                            C{p}(:,ch,cc) = nan(65,1);
                                            phi{p}(:,ch,cc) = nan(65,1);
                                        else
                                            [C{p}(:,ch,cc),phi{p}(:,ch,cc),~,~,~,f,] = coherencycpt(lfp,unit(s).cue(cc).period(p).trial,params,1);
                                        end
                                        if Args.STA
                                            [~,t,~,stac{p}(:,s,ch)] = spikeTrigAve(lfp,unit(s).cue(cc).period(p).trial,Args.downSR,'average');
                                        end
                                        if Args.surrogate
                                            [~,~,~,~,~,SurProb{p}(:,:,ch,cc),f] = cohsurrogate(lfp,unit(s).cue(cc).period(p).trial,params,'SFC',modvarargin{:});
                                            if Args.STA
                                                [~,~,~,~,~,SurSTAProb{p}(:,:,s,ch),t] = cohsurrogate(lfp,unit(s).cue(cc).period(p).trial,params,'STA',modvarargin{:});
                                            end
                                        end
                                        %                         count = count + 1;
                                    end
                                end
                            end
                        end
                    end
                    
                    if Args.ML; therule = r; else; therule = unique(mtst.data.Index(:,1)); end
                    if Args.save & isempty(Args.remoteName)
                        filename = sprintf('%s/%s%d%d%d.mat',unit(s).path,matfile,Args.stable,Args.BehResp,therule);
                        if Args.surrogate
                            
                            save(filename,'C','phi','f','SurProb','trials')
                            
                        else
                            save(filename,'C','phi','f','trials')
                        end
                        fprintf('Saving %s \n',filename)
                    end
                end
            end
        else
            C = [];phi = []; f = []; unit = [];
        end
        
        if ~isempty(Args.remoteName) && exist('sua')
            fprintf('\n Data sent to %s \n',Args.remoteName)
            [Ct,phit,~,~,~,ftemp,~] = dfeval(@coherencycpt,lfpt,unitt,repmat(params,1,length(lfpt)),repmat({1},1,length(lfpt)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
            if Args.STA
                [~,ttemp,~,statt] = dfeval(@spikeTrigAve,lfpt,unitt,repmat({Args.downSR},1,length(lfpt)),repmat({'average'},1,length(lfpt)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
            end
            fprintf('Data received from %s \n',Args.remoteName)
            if Args.surrogate
                fprintf('Surrogate sent to %s \n',Args.remoteName)
                [A{1},A{2},A{3},A{4},A{5},SurProbt,ftemp] = dfeval(@cohsurrogate,lfpt,unitt,repmat(params,1,length(lfpt)),repmat({'SFC'},1,length(lfpt)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                clear A
                if Args.STA
                    [~,~,~,~,~,SurSTAProbt,ttemp] = dfeval(@cohsurrogate,lfpt,unitt,repmat(params,1,length(lfpt)),repmat({'STA'},1,length(lfpt)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                end
                fprintf('Surrogate received from %s \n',Args.remoteName)
            end
            
            lcount = 1;
            
            count = 1;
            for s = 1 : (sua-1)
                for cc = 1 : length(trials)
                    for ch = 1 : size(group,2)
                        for p = 1 : nper
                            if  list(lcount) == 1
                                C{p}(:,ch,cc) = Ct{count};
                                phi{p}(:,ch,cc) = phit{count};
                                if Args.surrogate;if isempty(SurProbt{count}); SurProb{p}(:,:,ch,cc) = nan(length(form),3) ; else SurProb{p}(:,:,ch,cc) = SurProbt{count};end; end
                                if Args.STA
                                    stac{p}(:,s,ch) = statt{count};
                                    if Args.surrogate;SurSTAProb{p}(:,:,s,ch) = SurSTAProbt{count};end
                                end
                                lcount = lcount + 1;
                            else
                                C{p}(:,ch,cc) = nan(257,1);
                                phi{p}(:,ch,cc) = nan(257,1);
                                SurProb{p}(:,1,ch,cc) = nan(257,1);
                            end
                            count = count + 1;
                        end
                    end
                end
                if Args.ML; therule = r; else; therule = unique(mtst.data.Index(:,1)); end
                if Args.save
                    filename = sprintf('%s/%s%d%d%d.mat',unit(s).path,matfile,Args.stable,Args.BehResp,therule);
                    if Args.surrogate
                        
                        save(filename,'C','phi','f','SurProb','trials')
                    else
                        save(filename,'C','phi','f','trials')
                    end
                    fprintf('Saving %s \n',filename)
                end
            end
            f = ftemp{1};
            if Args.STA; t = ttemp{1}; end
        end
        
        if Args.surrogate; if exist('SurProb'); varargout{1} = SurProb; else varargout{1} = []; end
        else varargout{1} = [];end
        if Args.STA
            varargout{2} = stac;
            if Args.surrogate; varargout{3} = SurSTAProb;end;
        end
        
        %     if Args.ML; eval('end'); end
    end
    if Args.plotting
        
        for ch = 1 : size(group,2);
            for p = 1: length(periods);
                subplot(size(group,2),4,p+(ch-1)*4);
                plot(f,C{p}(:,:,ch));
                if Args.surrogate
                    hold on
                    plot(f,squeeze(SurProb{p}(:,2,:,ch)),'--');
                end
                axis([0 100 0 0.5]);
            end;
        end;
        legend(unit(:).name);
        
        for ch = 1 : size(group,2);
            subplot(size(group,2),4,(ch-1)*4+1);
            ylabel(group(ch).name);
        end
        per = {'pre-sample' 'sample' 'delay1' 'delay2'};
        for p = 1 : 4;
            subplot(size(group,2),4,p);
            title(per{p});
        end
        for p = 1: 4;
            subplot(size(group,2),4,p+(size(group,2)-1)*4);
            xlabel('Frequency [Hz]');
        end
        
        
        if Args.STA
            figure
            for ch = 1 : size(group,2);
                for p = 1: length(periods);
                    subplot(size(group,2),4,p+(ch-1)*4);
                    plot(t,stac{p}(:,:,ch));
                    if Args.surrogate
                        hold on
                        plot(t,squeeze(SurSTAProb{p}(:,2,:,ch)),'--');
                        plot(t,-squeeze(SurSTAProb{p}(:,2,:,ch)),'--');
                    end
                    %                     axis([min(t) max(t) 0 0.5]);
                end;
            end;
            legend(unit(:).name);
            
            for ch = 1 : size(group,2);
                subplot(size(group,2),4,(ch-1)*4+1);
                ylabel(group(ch).name);
            end
            per = {'pre-sample' 'sample' 'delay1' 'delay2'};
            for p = 1 : 4;
                subplot(size(group,2),4,p);
                title(per{p});
            end
            for p = 1: 4;
                subplot(size(group,2),4,p+(size(group,2)-1)*4);
                xlabel('Relative time from spike [sec]');
            end
        end
    end
    
else
    fprintf('!!!!!!Problem: No trials or no channel selected ');
    C = [];
    phi = [];
    f = [];
    unit = [];
    varargout{1} = [];
end