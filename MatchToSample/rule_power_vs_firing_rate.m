function rule_power_vs_firing_rate(varargin)


%run at session level

Args = struct('ml',0);
Args.flags = {'ml',};
Args = getOptArgs(varargin,Args);

% cd('/Volumes/raid/data/monkey/betty/091001/session01')

sesdir = pwd;

mt = mtstrial('auto','ML','RTfromML','redosetNames');
sample_on = floor(mt.data.CueOnset); %sample on
sample_off = floor(mt.data.CueOffset);

N = NeuronalHist('ml');
groups = 1 : N.chnumb;
[g,~,~,~] = sorted_groups('ml');
NN = NeuronalChAssign;
[i ii] =intersect(NN.groups,g);
sortgroups = zeros(1,size(groups,2));
sortgroups(ii) = 1;

noisy = noisy_groups;
[~,ii] =intersect(groups,noisy);
rejectgroups = ones(1,size(groups,2));
rejectgroups(ii) = 0; %these are the REJECTED groups

goodgroups = intersect(find(sortgroups),find(rejectgroups));
ngroups = size(goodgroups,2);

cd('lfp/power/')
powerdir = pwd;
p = nptDir('betty*');


for r = 1:2
    trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
    tcounter = 0;
    real_p = [];
    norm_p = [];
    spike_counts = [];
    for t = trials
        s_on = sample_on(t);
        s_off = sample_off(t);
        
        tcounter = tcounter + 1;
        
        cd(powerdir)
        load(p(t).name)
        
        g_counter = 0;
        for g = goodgroups
            g_counter = g_counter + 1;
            
            for pp = 1:40
                real_p{pp}(g_counter,tcounter) = power.S{5}(g,pp);
                norm_p{pp}(g_counter,tcounter) = power.S{5}(g,pp) / sum(power.S{5}(g,1:40));
            end
            
            cd([sesdir filesep 'group' num2strpad(NN.groups(g),4)])
            gdir = pwd;
            clusters = nptDir('cluster*');
            nclust = size(clusters,1);
            spt = [];
            for nc = 1%1 : nclust
                cd([gdir filesep clusters(nc).name])
                load ispikes
                spt = round(sp.data.trial(t).cluster.spikes);
                sum((spt > s_off+200) & (spt < s_off +800));
                spike_counts(g_counter,tcounter) =  sum((spt > s_off+200) & (spt < s_off +800));
            end
        end
    end
    
    power_rules.spike_counts{r} = spike_counts;
    power_rules.real_p{r} = real_p;
    power_rules.norm_p{r} = norm_p;
    
end


 cd(powerdir)
 
 save power_rules power_rules 


% % cc = cool;
% % for x = 1:23
% %     r1 = rules.real_p{1};
% %     r2 = rules.real_p{2};
% %    
% %    for xx = 1:30
% %        scatter(mean(r1{xx}(x,:)),mean(r2{xx}(x,:)),100,cc(xx+2,:),'fill','s')
% %        hold on
% %    end
% % %    axis([0 .15 0 .15])
% % %    axis square
% % %    plot([0 .15],[0 .15])
% %    pause
% %    cla
% % end
% % 
% % 
% % rsp1 = rules.spike_counts{1};
% % rsp2 = rules.spike_counts{2};
% % 
% % 
% % hist(mean(rsp1')-mean(rsp2'))
% % hold on
% %    axis([0 35 0 35])
% %    axis square
% %    plot([0 35],[0 35])


