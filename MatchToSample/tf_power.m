function tf_power(varargin)

%computes time frequency power, locked to sample, match and first saccade
%saves a file for each channel in the appropriate folder

Args = struct('ml',0,'bmf',0,'rules',[],'bettynlynx',0);
Args.flags = {'ml','bmf','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args);

params = struct('tapers',[2 3],'Fs',200,'fpass',[1 50],'trialave',1);
movingwin = [.200 .05];

sesdir = pwd;
if Args.ml
    if Args.bettynlynx
        N = NeuronalHist('ml','bettynlynx');
        noisy = noisy_groups;
        [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml','bettynlynx');
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
        
    elseif Args.bmf
        N = NeuronalHist('bmf');
        [sortedgroups,chpairlist,~,~,sortedpairs] = bmf_groups('ml'); %already kicks out noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    else
        N = NeuronalHist('ml');
        noisy = noisy_groups;
        [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml');
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    end
else
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    noisy = noisy_groups;
    [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
    [~,ii] = intersect(sortedgroups,noisy);
    sortedgroups(ii) = []; %get rid of noisy groups
    
    [~,chgroups] = intersect(Nch.groups,sortedgroups);
end

cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials') %this looks at the entire trial and is necessary to get rid of iti artifacts
else
    load('rejectedTrials.mat','rejecTrials') %use the non-incorrect specific rejected trials
end

lfpdir = pwd;
lfptrials = nptDir('*_lfp.*');


if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        
        [~,baditi] = intersect(incorrtrials,rejecTrials);
        incorrtrials(baditi) = [];
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

trialorder = {'identity','location','fixation','incorrect'};

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_off = floor((mt.data.CueOffset) ./ 5); %sample off
match = floor(floor(mt.data.MatchOnset) ./5);    %match
sacc = floor((mt.data.MatchOnset + mt.data.FirstSac) ./ 5);

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1, 4];
        else
            rules = [1 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    cd(lfpdir)
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        elseif rule == 3
            trials = fixtrials;
        elseif rule == 4
            trials = incorrtrials;
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        ntrials = size(trials,2);
        for sg = 14% : size(chgroups,2);
            cd(lfpdir)
            %get correct channel
            c1 = chgroups(sg);
            soff_locked = [];
            matchlocked = [];
            firstsacclocked = [];
            tcounter = 0;
            for x = trials
                
                tcounter = tcounter + 1;
                s_off = sample_off(x);
                m = match(x);
                s = sacc(x);

                [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(x).name);
                p1t = data.rawdata(c1,:);
                
                %get rid of 60! and detrend
                data1 = preprocessinglfp(p1t,'detrend')';

                soffl = data1((s_off - 200) : end); %sampleoff - 1000ms : end
                soff_locked(1:size(soffl,2),tcounter) = soffl;

                if rule == 4 || Args.bettynlynx
                    mlocked =  data1((m - 200) : end);
                    matchlocked((1:size(mlocked,2)),tcounter) = mlocked;%match - 1000ms : end
                    
                    flocked = data1((s - 200) : end);
                    firstsacclocked((1:size(flocked,2)),tcounter) =  flocked;%first sacc - 1000ms : end
                else
                    mlocked =  data1((m - 200) : (m+50));
                    matchlocked((1:size(mlocked,2)),tcounter) = mlocked;%match - 1000ms : match + 200
                    if ~Args.ml
                        flocked = data1((s - 200) : (s + 4));
                        firstsacclocked((1:size(flocked,2)),tcounter) =  flocked;%first sacc - 1000ms : sacc + 25
                    else
                        flocked = data1((s - 200) : (s + 49));
                        firstsacclocked((1:size(flocked,2)),tcounter) =  flocked;%first sacc - 1000ms : sacc + 250
                    end
                end
                
            end
            
            %calculate specgram
            [sampleoff_locked.S,sampleoff_locked.t,sampleoff_locked.f] = mtspecgramc(soff_locked,movingwin,params);
            [match_locked.S,match_locked.t,match_locked.f] = mtspecgramc(matchlocked,movingwin,params);
            [firstsacc_locked.S,firstsacc_locked.t,firstsacc_locked.f] = mtspecgramc(firstsacclocked,movingwin,params);
            
            cd([lfpdir filesep 'lfp2'])
            write_info = writeinfo(dbstack);
            
            if ~exist(trialorder{rule},'dir') && ~Args.ml
                mkdir(trialorder{rule})
            end
            cd(trialorder{rule})
            
            %%need to make folders for clark
            %since correct and incorrect are both being processed
            
            group = sortedgroups(sg);
            pg = ['powergram' num2strpad(group,2)];
            save(pg,'sampleoff_locked','match_locked','firstsacc_locked','ntrials','group','write_info')
        end
    end
end

cd(sesdir)

fprintf(1,'\n')
fprintf(1,'TF Power Done.\n')