function mtsGCcut

%preprocesses mts trials for GC analysis

%Get info about # of trials.
mt=mtstrial('auto');

[channels, comb, CHcomb] = checkChannels('cohInter');

trials = mtsgetTrials(mt,'BehResp',1,'stable',0);



%Data is stored in cell in 4 epochs. Format [observations, trials, channel]
[data] = lfpPcut(trials,channels);

save cut data


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Lowpass Filter%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   
    
       
 for epochs = 1:4 
     
    epoch = data{epochs};
    
    %Split channels to run lowpass filter
    lfpchannels = []; 
    for allchannels = 1 : length(channels)
    
    ch = epoch(:,:,allchannels);


    [lfp,rs] = nptLowPassFilter(ch,200,200,8,32);
    
    %save lfp data in same format [observations, trials, channel];
    lfpchannels = cat(3,lfpchannels,lfp);

    end
    
     

 
 
 


%%%%%%%%%%%%%%%%%%%%%%%%%%CorrCoefs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%Calculate corrcoefs for each interarial pair for all trials

correlationcoefs = [];
pairs = length(CHcomb);

    for allpairs = 1 : pairs
        
        allpairs
    

    xx = lfpchannels(:,:,CHcomb(allpairs,1));
    
    yy = lfpchannels(:,:,CHcomb(allpairs,2));
    
  
    %creates a matrix off all trail combinations
    cc = corr(xx,yy);
    
    %take only the diagonal which is the trial pairs
    ccc = diag(cc)';
    
    
    %create a matrix with all ccs. Rows = pairs Columns = trials
    correlationcoefs = cat(1, correlationcoefs, ccc);  

    
    
    end
    
    
 e = [ 'epoch' num2str(epochs)]
 

 
 save(e,'lfpchannels','correlationcoefs') 
 end





























