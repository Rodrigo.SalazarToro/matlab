function EpochPowers



%Dependent on mtsGCcut,mtsCorrPower,mtsCorrCausal


[NeuroInfo] = NeuronalChAssign;

chnumb=length(NeuroInfo.channels);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%GUI SETUP%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Global handle definitions
global  h_fig  h_back h_next h_c1  h_channels h_ch h_high h_low h_gc h_epoch


%Create figure
h_fig = figure('name','Power');
    
%Create text box
h_channels = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.0 .0 .15 .05],'fontsize',10,'fontweight','b');

%Create text box
h_ch = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.95 .0 .05 .08],'fontsize',10,'fontweight','b');
  
%Create edit text box
h_c1 = uicontrol(h_fig,'Style','edit','String','0','units','normalized',...
    'Position',[.45 .0 .05 .05],'Callback',@c1_callback,'fontsize',10,'fontweight','b');

%Create edit text box
h_high = uicontrol(h_fig,'Style','edit','String','75','units','normalized',...
    'Position',[.95 .8 .05 .05],'Callback',@high_callback,'fontsize',10,'fontweight','b');

%Create edit text box
h_low = uicontrol(h_fig,'Style','edit','String','25','units','normalized',...
    'Position',[.95 .7 .05 .05],'Callback',@low_callback,'fontsize',10,'fontweight','b');

%Create edit text bo
h_epoch = uicontrol(h_fig,'Style','edit','String','1','units','normalized',...
    'Position',[.95 .3 .05 .05],'Callback',@epoch_callback,'fontsize',10,'fontweight','b');

%Create pushbutton 'GC'
h_gc = uicontrol(h_fig,'Style','pushbutton','String','GC','units','normalized',...
    'Position',[.95 .5 .05 .05],'Callback',@gc_callback,'Fontsize',10,'fontweight','b');

%Create pushbutton 'back'
 h_back = uicontrol(h_fig,'Style','pushbutton','String','Back','units','normalized',...
    'Position',[.26 .0 .15 .05],'Callback',@back_callback,'fontsize',10,'fontweight','b');

%Create pushbutton 'next'
 h_next = uicontrol(h_fig,'Style','pushbutton','String','Next','units','normalized',...
    'Position',[.54 .0 .15 .05],'Callback',@next_callback,'fontsize',10,'fontweight','b');


%Display the number of channels
 set(h_channels,'String',[num2str(chnumb),' Channels ', NeuroInfo.cortex]); 


 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CALLBACKS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
    function next_callback(hObject,eventdata)
    
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String')) + 1;
        
        epoch = str2double(get(h_epoch,'String'));
        
        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');

        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_ch,'String',cpair);
        
        set(h_epoch,'String',num2str(epoch));
        
    end

 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function back_callback(hObject,eventdata)
       
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String')) - 1;
        
        epoch = str2double(get(h_epoch,'String'));

        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');
   
        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_ch,'String',cpair);
        
        set(h_epoch,'String',num2str(epoch));
        
    end   


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    function c1_callback(hObject,eventdata)
        
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String'));
        
        epoch = str2double(get(h_epoch,'String'));

        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');
    
        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_ch,'String',cpair);
        
        set(h_epoch,'String',num2str(epoch));
        
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function high_callback(hObject,eventdata)
        
        
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String'));
        
        epoch = str2double(get(h_epoch,'String'));

        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');
    
        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_high,'String',num2str(high));
        
        set(h_low,'String',num2str(low));
        
        set(h_ch,'String',cpair);
    
        set(h_epoch,'String',num2str(epoch));
        
    end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function low_callback(hObject,eventdata)
        
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String'));
        
        epoch = str2double(get(h_epoch,'String'));
       
        
        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');
    
        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_high,'String',num2str(high));
        
        set(h_low,'String',num2str(low));
        
        set(h_ch,'String',cpair);
        
        set(h_epoch,'String',num2str(epoch));
        
        
    end    
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function gc_callback(hObject,eventdata)
        
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String'));
        
        epoch = str2double(get(h_epoch,'String'));
        
        %Run mtsGCcors(low,high)and mtsGCcausal(channelpair)
        
        mtsCorrCausal(low,high,c1,epoch)

    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function epoch_callback(hObject,eventdata)
        
        high = str2double(get(h_high,'String')); 
        
        low = str2double(get(h_low,'String'));
        
        c1 = str2double(get(h_c1,'String'));
        
        epoch = str2double(get(h_epoch,'String'));
        
        %Run mtsGCcors(low,high)and mtsEpochPower(channelpair)
        mtsCorrPower(low,high,c1,epoch)
        
        [channels, comb, CHcomb] = checkChannels('cohInter');
    
        cpair = CHcomb(c1,:);
              
        set(h_c1,'String',num2str(c1));
        
        set(h_high,'String',num2str(high));
        
        set(h_low,'String',num2str(low));
        
        set(h_ch,'String',cpair);
        
        set(h_epoch,'String',num2str(epoch));
        
    end      
        
end