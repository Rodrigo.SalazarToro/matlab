function mtsCorrCausal(low,high,channelpair,epoch)

%combines mtsGCcut, mtsGCcors and mtsGCcausal

%function mtsGCcors(low, high)

%Separate trials into low/high% for GC analysis

e = ['epoch' num2str(epoch)];

load(e,'correlationcoefs', 'lfpchannels')
load cut data



    pctiles = prctile(correlationcoefs,[low;high],2);
     
        %creates a matrix of 1,0,-1 with same dimensions as correlationcoefs
        ppairs = [];
        
        %all rows, wich are the channel pairs
        for cpairs = 1 : size(correlationcoefs,1);
            
            hl = [];
            
            %all trials wich are the columns
            for trials = 1 : size(correlationcoefs,2);
                
                if correlationcoefs(cpairs, trials) >= pctiles(cpairs,2);
                    
                   h = 1;
                   hl = cat(2,hl,h);
                   
                elseif correlationcoefs(cpairs, trials) <= pctiles(cpairs,1);
                    
                   l = -1; 
                   hl = cat(2,hl,l); 
                    
                else
                    na=0;
                    hl = cat(2,hl,na);
           
                end
             
            end
            
            ppairs = cat(1,ppairs,hl); 
        
        end
        
     
%function mtsGCcausal(channelpair)

%Run GC analysis.The trials for each channel are separated into the 25%, 75%


mt=mtstrial('auto');

[channels, comb, CHcomb] = checkChannels('cohInter');

cpair = CHcomb(channelpair,:);

%Get all trials used for corrcoefs
trials = mtsgetTrials(mt,'BehResp',1,'stable',0);


gc = [];
for highlow = [1 0 -1]
    x = [];
    
    %Get trials for both channels in pair
    for cp = 1 : length(cpair)
        
        
    prcttrials = [];
    
        for cc = 1 : length(ppairs)
        
        
            if ppairs(channelpair,cc) == highlow;
            
                
            %get lfp data for trial and channel    
            lfp = lfpchannels(:,cc,cpair(cp))';    
                
            prcttrials = cat(2,prcttrials,lfp);
                
      
                
            else
        
        
            %do nothing
        
            end
            
            
        end
        
        x = cat(1,x,prcttrials);
        
    end


%Where x is the data set, Nr is the number of realizations (in this case
%150), Nl is the length of each realizations (100 in this case), porder is
%the model order (we've been using 11), fs is the sampling rate (200 Hz),
%freq is a vector of the frequencies that you want to be dispayed in the
%output (use 1:100).

Nr = length(x)/101;
fs = 200;
Nl = 101;
porder = 11;
freq = (1:100);

[pp,cohe,Fx2y,Fy2x,Fxy,rp]= pwcausalrp(x,Nr,Nl,porder,fs,freq);


%For coherence, just put in the cohe output variable from pwcausalrp.m as
%the varargin, nchans is the number of channels, and freq is the number of
%frequencies (100 in this case).

%For GC, put Fx2y, followed by Fy2x as the varargin, this will create a GC
%array.  Make sure you get them in the right order, or everything will be
%reversed.


[out]=yc2mat(2,100,Fx2y,Fy2x);


gc = cat(1,gc,Fx2y,Fy2x,Fxy);



end

figure
for xx = 1:9;
    
    yy = gc(xx,:);

subplot(3,3,xx); plot(yy); ylim([-.05 .5]); 


end
zoom on

title(subplot(3,3,1),'Parietal 2 Frontal');
ylabel('High')
ylabel(subplot(3,3,4),'Middle');

ylabel(subplot(3,3,7),'Low');

title(subplot(3,3,2),'Frontal 2 Parietal');

title(subplot(3,3,3),'Instantaneous');






     