function mtsCorrPower(low,high,channelpair,epoch)

%combines mtsGCcors with mtsEpochPower



%mtsGCcors(low,high)
%Separate trials into low/high% for GC analysis

e = ['epoch' num2str(epoch)];

load (e, 'correlationcoefs', 'lfpchannels') 
load cut data


    pctiles = prctile(correlationcoefs,[low;high],2);
     
        %creates a matrix of 1,0,-1 with same dimensions as correlationcoefs
        ppairs = [];
        
        %all rows, wich are the channel pairs
        for cpairs = 1 : size(correlationcoefs,1);
            
            hl = [];
            
            %all trials wich are the columns
            for trials = 1 : size(correlationcoefs,2);
                
                if correlationcoefs(cpairs, trials) >= pctiles(cpairs,2);
                    
                   h = 1;
                   hl = cat(2,hl,h);
                   
                elseif correlationcoefs(cpairs, trials) <= pctiles(cpairs,1);
                    
                   l = -1; 
                   hl = cat(2,hl,l); 
                    
                else
                    na=0;
                    hl = cat(2,hl,na);
           
                end
             
            end
            
            ppairs = cat(1,ppairs,hl); 
        
        end
     
     

        
        
        
        
 %function mtsEpochPower(channelpair)

%Gets power for channels at low 33% and high 33%

%depedent on: mtsGCcut

             %mtsGCcors(low, high)

[channels, comb, CHcomb] = checkChannels('cohInter');

cpair = CHcomb(channelpair,:);




%Uses preprocessed but unfiltered data

%separate out first epoch
    epochs = data{epoch};

x = 0;



for bothchannels = cpair;


maxes =[];
for highlow = [1 0 -1]
    
    x = x + 1;
    
    
    qqq = find(ppairs(channelpair,:) == highlow);
    
    timeseries = squeeze(epochs(:,qqq,bothchannels));
    
    
    
    power = powerDistr(timeseries,'KeepSpectra','Fs',200);
    
    
    
    
  %perc25 = []; perc50 = []; perc75 = []; pstd = [];  
  %for z = 1:length(power.perc25)
      
  %    perc25 = [perc25 (power.perc25(:,z)*power.f(:,z))];
  %    perc50 = [perc50 (power.perc50(:,z)*power.f(:,z))];    
  %    perc75 = [perc75 (power.perc75(:,z)*power.f(:,z))];
  %    pstd = [pstd (power.std(z,:)*power.f(:,z))];
     
  %end
   
  maxes = [maxes max(power.perc75)];

 
   subplot(2,3,x);cla; plot(power.f,power.perc25,'g');hold on; plot(power.f,power.perc75,'k'); hold on; plot(power.f,power.perc50,'r');
                   plot(power.f,power.std,'--');
                   
                    
   
        if x == 3
            axis([subplot(2,3,1),subplot(2,3,2),subplot(2,3,3)],[0 100 0 max(maxes)]);
       
        elseif x==6
            axis([subplot(2,3,4),subplot(2,3,5),subplot(2,3,6)],[0 100 0 max(maxes)]);
       
        end
   
zoom on

   

   
    
end
   
   
end

title(subplot(2,3,1),'High')
xlabel('Frequency')


title(subplot(2,3,2),'Middle')
xlabel('Frequency')


title(subplot(2,3,3),'Low')
xlabel('Frequency')



ylabel(subplot(2,3,1),'Parietal Power')
ylabel(subplot(2,3,4),'Frontal Power')



     
     
end   
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     