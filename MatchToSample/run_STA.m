function run_STA(varargin)

%run at monkey level

%computes sta for all clusters with all channels for 7 different time periods
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]
Args = struct('ml',0,'days',[],'surrogate',0,'perms',1000,'sta',0,'ide_only',1,'redo_sta',0,'redo_surrogate',0,'standardized',0);
Args.flags = {'ml','surrogate','sta','ide_only','redo_sta','redo_surrogate','standardized'};
[Args,modvarargin] = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));

monkeydir = pwd;
num_days = size(Args.days,2);
for d = 1 : num_days
    if Args.ml
        ses = 1;
        sessions = {'session01'};
    else
        ses = [1:2];
        sessions = {'session02','session03'};
    end
    
    for sesnumb = ses
        try
            cd([monkeydir filesep Args.days{d} filesep sessions{sesnumb}])
        catch
            fprintf(1,['No ' sessions{sesnumb} ' on ' Args.days{d} '\n'])
            break
        end
        sesdir = pwd;
        
        if Args.ml
            mt = mtstrial('auto','ML','RTfromML','redosetNames');
            r = 1; %only identity need to save in identity dir
            total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
        else
            mt = mtstrial('auto','redosetNames');
            %determine rule
            r = mt.data.Index(1,1);
            total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
        end
        
        %if ide only then skip session if it is location(2)
        if ((Args.ide_only && r == 1) || (~Args.ide_only)) && ~isempty(total_trials)
            %get trial timing information
            sample_on = floor(mt.data.CueOnset);
            sample_off = floor(mt.data.CueOffset);
            match = floor(mt.data.MatchOnset);
            
            num_trials = size(total_trials,2);
            
            %get group information
            N = NeuronalHist;
            [NeuroInfo] = NeuronalChAssign(); %get the "group" number that corresponds to each channel
            
            %find groups with spikes
            groups = nptDir('group*');
            ngroups = [1 : size(groups,1)];
            
            if Args.sta
                pwd
                if r == 1
                    fprintf(1,'lfp sta identity \n')
                else
                    fprintf(1,'lfp sta location \n')
                end
                for g = ngroups %spike channel
                    fprintf('\n%0.5g     ',g)
                    for gg = ngroups %lfp channels
                        fprintf(' %0.5g',gg)
                        sta_groups_mean = [];
                        sta_groups_se = [];
                        sta_groups_cluster_info = [];
                        sta_groups_peak = [];
                        sta_groups_lags = [];
                        %determine which channels the group number corresponds to
                        g2ch = str2double(groups(g).name(6:end));
                        [i ii] = intersect(NeuroInfo.groups,g2ch);
                        channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                        
                        %get lfp groups information
                        lfpg = str2double(groups(gg).name(6:end));
                        [i ii] = intersect(NeuroInfo.groups,lfpg);
                        channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                        
                        %determine if pair has already been written
                        sta_pairs = [ 'lfpstapair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
                        cd([sesdir filesep 'lfp' filesep 'lfp2'])
                        spair = nptdir([sta_pairs '.mat']);
                        cd(sesdir)
                        
                        if isempty(spair) || Args.redo_sta
                            cd([sesdir filesep groups(g).name])
                            %find clusters
                            clusters = nptDir('cluster*');
                            nclusters = size(clusters,1);
                            c_counter = 0;
                            for c = 1 : nclusters
                                cd([sesdir filesep groups(g).name])
                                c_counter = c_counter + 1;
                                cd(clusters(c).name);
                                
                                %get spikes
                                load ispikes.mat
                                
                                %get lfp group number
                                lfp_group = channels(2);
                                
                                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                                %get mean and std info for each trial (this is for data that has
                                %been lowpassed and standardized
                                
                                lfp = nptDir('*_lfp2*');
                                stas = cell(1,7);
                                s_angle = cell(1,7);
                                all_sta_cluster_mean = [];
                                all_sta_cluster_se = [];
                                sta_groups_spike_counts = [];
                                all_mean_angles = [];
                                for ttrial = total_trials
                                    load(lfp(ttrial).name);
                                    
                                    lfp_data = data(lfp_group,:);
                                    
                                    spike_data = sp.data.trial(ttrial).cluster.spikes;
                                    s = round(spike_data);
                                    s = s(s > 50 & s < (size(lfp_data,2) - 50)); %make sure the appropriate number of data points can be taken from either side of spike
                                    
                                    %use hilbert transform to determine instantaneous phase angle for each spike
                                    h = hilbert(lfp_data);
                                    ha = angle(h);
                                    sha = ha(s);
                                    
                                    w = [];
                                    w = spike_triggered_average('lfp',lfp_data,'spikes',s,'samplingrate',1000);
                                    if ~isnan(w)
                                        %determine what epoch each spike is in
                                        s_on = sample_on(ttrial);
                                        s_off = sample_off(ttrial);
                                        m = match(ttrial);
                                        
                                        % 1: fixation    [(sample_on - 399) : (sample_on)]
                                        % 2: sample      [(sample_off - 399) : (sample_off)]
                                        % 3: early delay [(sample_off) : (sample_off + 399)]
                                        % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
                                        % 5: delay       [(sample_off + 201) : (sample_off + 800)]
                                        % 6: delay match [(match - 399) : (match)]
                                        % 7: full trial  [(sample_on - 500) : (match)]
                                        
                                        stas{1} = [stas{1}; w((find(s >= (s_on - 399) & s <= s_on)),:)];
                                        stas{2} = [stas{2}; w((find(s >= (s_off - 399) & s <= s_off)),:)];
                                        stas{3} = [stas{3}; w((find(s >= s_off & s < (s_off + 399))),:)];
                                        stas{4} = [stas{4}; w((find(s > (s_off+400) & s <= (s_off + 800))),:)];
                                        stas{5} = [stas{5}; w((find(s > (s_off+200) & s <= (s_off + 800))),:)];
                                        stas{6} = [stas{6}; w((find(s >= (m - 399) & s <= m)),:)];
                                        stas{7} = [stas{7}; w((find(s >= (s_on - 500) & s <= m)),:)];
                                        
                                        s_angle{1} = [s_angle{1}, sha(find(s >= (s_on - 399) & s <= s_on))];
                                        s_angle{2} = [s_angle{2}, sha(find(s >= (s_off - 399) & s <= s_off))];
                                        s_angle{3} = [s_angle{3}, sha(find(s >= s_off & s < (s_off + 399)))];
                                        s_angle{4} = [s_angle{4}, sha(find(s > (s_off+400) & s <= (s_off + 800)))];
                                        s_angle{5} = [s_angle{5}, sha(find(s > (s_off+200) & s <= (s_off + 800)))];
                                        s_angle{6} = [s_angle{6}, sha(find(s >= (m - 399) & s <= m))];
                                        s_angle{7} = [s_angle{7}, sha(find(s >= (s_on - 500) & s <= m))];
                                    end
                                end
                                
                                %determine number of spikes in each sta
                                [all_spikes,~] = cellfun(@size,stas);
                                
                                all_sta_cluster_mean = zeros(7,101);
                                all_sta_cluster_se = zeros(7,101);
                                all_sta_cluster_zerolag = zeros(1,7);
                                
                                %c_counter is the cluster number (first cluster is multiunit)
                                for w = 1 : 7
                                    if ~isempty(stas{w}) && size(stas{w},1) > 1
                                        msta = mean(stas{w});
                                        all_sta_cluster_mean(w,:) = msta;
                                        all_sta_cluster_se(w,:) = std(stas{w}) / sqrt(size(stas{w},1));
                                        all_sta_cluster_zerolag(w) = msta(51);
                                        all_mean_angles(w) = atan2(mean(sin(s_angle{w})),mean(cos(s_angle{w}))); %atan2(mean(sin(s1)),mean(cos(s1)));
                                    else
                                        all_sta_cluster_zerolag(w) = nan;
                                        all_sta_cluster_mean(w,:) = nan;
                                        all_sta_cluster_se(w,:) = nan;
                                        all_sta_cluster_zerolag(w) = nan;
                                        all_mean_angles(w) = nan;
                                    end
                                end
                                
                                sta_groups_mean{1,c} = single(all_sta_cluster_mean);
                                sta_groups_zerolag{1,c} = single(all_sta_cluster_zerolag);
                                sta_groups_se{1,c} = single(all_sta_cluster_se);
                                sta_groups_cluster_info{1,c} = clusters(c).name;
                                sta_groups_mean_angles{1,c} = single(all_mean_angles);
                                
                                time = [-50:50];
                                %run through each sta and find peak closest to zero and the time
                                for x = 1 : size(all_sta_cluster_mean,1)
                                    
                                    %find peak (positive or negative) closest to zeros
                                    signal = all_sta_cluster_mean(x,:);
                                    if sum(signal) > 0
                                        [ppeaks llag] = find_correlogram_peak('correlogram',signal);
                                        
                                        lags(x) = llag;
                                        peaks(x) = ppeaks;
                                        
                                        %find negative peak closest to zeros
                                        cc = signal*-1;
                                        max_lag = size(cc,2);
                                        np = findpeaks(cc);
                                        if ~isempty(np.loc)
                                            for nnn = 1 : size(np.loc,1)
                                                if (cc(np.loc(nnn)) < 0)
                                                    %set to max phase index
                                                    np.loc(nnn) = max_lag;
                                                end
                                            end
                                            [~,neg_index] = (min(abs(time(np.loc))));
                                            neg_lags(x) = time(np.loc(neg_index));
                                            neg_peaks(x) = cc(np.loc(neg_index)) * -1;
                                        else
                                            neg_lags = nan;
                                            neg_peaks = nan;
                                        end
                                        
                                        %find largest value
                                        [~,ll]=max(abs(signal));
                                        max_peaks(x) = signal(ll);
                                        max_lags(x) = time(ll);
                                        
                                        %calculate max power for each sta
                                        NFFT = 2^nextpow2(size(signal,2));
                                        f = fft(signal,NFFT);
                                        
                                        power = abs(f(1:NFFT/2+1)).^2;
                                        freq = 1000/2 * linspace(0,1,NFFT/2+1);
                                        
                                        [~,ff] = max(power);
                                        max_power_freq(x) = freq(ff);
                                        
                                    else
                                        lags(x) = nan;
                                        peaks(x) = nan;
                                        neg_lags(x) = nan;
                                        neg_peaks(x) = nan;
                                        max_peaks(x) = nan;
                                        max_lags(x) = nan;
                                        max_power_freq(x) = nan;
                                    end
                                    
                                end
                                sta_groups_peak{1,c} = single(peaks);
                                sta_groups_lags{1,c} = single(lags);
                                sta_groups_spike_counts{1,c} = all_spikes;
                                
                                sta_groups_neg_peaks{1,c} = single(neg_peaks);
                                sta_groups_neg_lags{1,c} = single(neg_lags);
                                
                                sta_groups_max_peaks{1,c} = single(max_peaks);
                                sta_groups_max_lags{1,c} = single(max_lags);
                                
                                sta_groups_max_power_freq{1,c} = single(max_power_freq);
                            end
                            sta_groups.mean = sta_groups_mean;
                            sta_groups.zero_lag = sta_groups_zerolag;
                            sta_groups.se = sta_groups_se;
                            sta_groups.peak = sta_groups_peak;
                            sta_groups.lags = sta_groups_lags;
                            
                            sta_groups.neg_peak = sta_groups_neg_peaks;
                            sta_groups.neg_lags = sta_groups_neg_lags;
                            
                            sta_groups.max_peak = sta_groups_max_peaks;
                            sta_groups.max_lags = sta_groups_max_lags;
                            
                            sta_groups.max_power_freq = sta_groups_max_power_freq;
                            
                            sta_groups.mean_inst_angle = sta_groups_mean_angles;
                            
                            sta_groups.cluster_info = sta_groups_cluster_info;
                            sta_groups.spike_counts = sta_groups_spike_counts;
                            sta_groups.channels = channels;
                            
                            
                            write_info = writeinfo(dbstack);
                            sta_pairs = [ 'lfpstapair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
                            
                            save(sta_pairs,'sta_groups','write_info')
                        end
                    end
                end
            end
            
            if Args.surrogate
                if Args.ml
                    sta_global_surrogate('ml')
                else
                    sta_global_surrogate
                end
            end
            
            %RUN GLOBAL SURROGATE INSTEAD (sta_global_surrogate.m)
            % % % %             if Args.surrogate
            % % % %                 pwd
            % % % %                 if r == 1
            % % % %                     fprintf(1,'lfp sta surrogate identity \n')
            % % % %                 else
            % % % %                     fprintf(1,'lfp sta surrogate location \n')
            % % % %                 end
            % % % %                 for g = ngroups %spike channel
            % % % %                     fprintf('\n%0.5g     ',g)
            % % % %                     for gg = ngroups %lfp channels
            % % % %                         fprintf(' %0.5g',gg)
            % % % %
            % % % %                         sta_groups_mean = [];
            % % % %                         sta_groups_se = [];
            % % % %                         sta_groups_peak = [];
            % % % %                         sta_groups_time= [];
            % % % %                         %determine which channels the group number corresponds to
            % % % %                         g2ch = str2double(groups(g).name(6:end));
            % % % %                         [i ii] = intersect(NeuroInfo.groups,g2ch);
            % % % %                         channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
            % % % %
            % % % %                         %get lfp groups information
            % % % %                         lfpg = str2double(groups(gg).name(6:end));
            % % % %                         [i ii] = intersect(NeuroInfo.groups,lfpg);
            % % % %                         channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
            % % % %
            % % % %                         %determine if pair has already been written
            % % % %                         sta_pairs = [ 'surrogatelfpstapair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
            % % % %                         cd([sesdir filesep 'lfp' filesep 'lfp2'])
            % % % %                         spair = nptdir([sta_pairs '.mat']);
            % % % %                         cd(sesdir)
            % % % %
            % % % %                         if isempty(spair) || Args.redo_surrogate
            % % % %
            % % % %                             cd([sesdir filesep groups(g).name])
            % % % %                             %find clusters
            % % % %                             clusters = nptDir('cluster*');
            % % % %                             nclusters = size(clusters,1);
            % % % %                             c_counter = 0;
            % % % %                             for c = 1 : nclusters
            % % % %                                 cd([sesdir filesep groups(g).name])
            % % % %                                 c_counter = c_counter + 1;
            % % % %                                 cd(clusters(c).name);
            % % % %
            % % % %                                 %get spikes
            % % % %                                 load ispikes.mat
            % % % %
            % % % %                                 %get lfp group number
            % % % %                                 lfp_group = channels(2);
            % % % %
            % % % %                                 cd([sesdir filesep 'lfp' filesep 'lfp2'])
            % % % %                                 %get mean and std info for each trial (this is for data that has
            % % % %                                 %been lowpassed and standardized
            % % % %
            % % % %                                 load mean_std_info %get_voltages
            % % % %
            % % % %                                 lfp = nptDir('*_lfp2*');
            % % % %                                 for p = 1 : Args.perms %run specified number of permutations
            % % % %                                     perm1 = randperm(num_trials);
            % % % %                                     perm2 = randperm(num_trials);
            % % % %
            % % % %                                     stas = cell(1,7);
            % % % %                                     for ttrial = 1 : num_trials
            % % % %                                         t1 = perm1(ttrial);
            % % % %                                         t2 = perm2(ttrial);
            % % % %                                         lfp_trial = total_trials(t1);
            % % % %                                         spike_trial = total_trials(t2);
            % % % %                                         %means = mean_for_lfp2(lfp_group,lfp_trial);
            % % % %                                         stds = std_for_lfp2(lfp_group,lfp_trial);
            % % % %                                         load(lfp(lfp_trial).name);
            % % % %
            % % % %
            % % % %                                         if ~Args.standardized
            % % % %                                             %unstandardize
            % % % %                                             lfp_data = (normdata(lfp_group,:) * stds); %+ means;
            % % % %                                         else
            % % % %                                             lfp_data = normdata(lfp_group,:);
            % % % %                                         end
            % % % %                                         spike_data = sp.data.trial(spike_trial).cluster.spikes;
            % % % %                                         sdata = round(spike_data);
            % % % %                                         sdata = sdata(sdata > 100 & sdata < (size(lfp_data,2) - 100)); %make sure the appropriate number of data points can be taken from either side of spike
            % % % %                                         %run self
            % % % %                                         w = spike_triggered_average('lfp',lfp_data,'spikes',sdata,'samplingrate',1000,'surrogate');
            % % % %                                         if ~isnan(w)
            % % % %                                             %s_t_a = [s_t_a;w];
            % % % %
            % % % %                                             s_on1 = sample_on(t1);
            % % % %                                             s_off1 = sample_off(t1);
            % % % %                                             m1 = match(t1);
            % % % %                                             s_on2 = sample_on(t2);
            % % % %                                             s_off2 = sample_off(t2);
            % % % %                                             m2 = match(t2);
            % % % %
            % % % %                                             %make sure that spikes are in the same epochs for both trials. If there is no overlap then the spike is ignored
            % % % %                                             sta1 = [sta1;w((find(sdata > (s_on1 - 400) & sdata < s_on1 & sdata > (s_on2 - 400) & sdata < s_on2)),:)];
            % % % %                                             sta2 = [sta2;w((find(sdata > (s_off1 - 400) & sdata < s_off1 & sdata > (s_off2 - 400) & sdata < s_off2)),:)];
            % % % %                                             sta3 = [sta3;w((find(sdata > s_off1 & sdata < (s_off1 + 400) & sdata > s_off2 & sdata < (s_off2 + 400))),:)];
            % % % %                                             sta4 = [sta4;w((find(sdata > (m1 - 400) & sdata < m1 & sdata > (m2 - 400) & sdata < m2)),:)];
            % % % %                                         end
            % % % %                                     end
            % % % %                                     all_perm_sta(1,p)  = mean(sta1);
            % % % %                                     all_perm_sta(2,p)  = mean(sta2);
            % % % %                                     all_perm_sta(3,p)  = mean(sta3);
            % % % %                                     all_perm_sta(4,p)  = mean(sta4);
            % % % %                                     all_perm_sta(5,p) = mean([sta1; sta2; sta3; sta4]);
            % % % %                                 end
            % % % %                                 surrogate_sta_groups{1,c} = single(all_perm_sta);
            % % % %                             end
            % % % %                             write_info = writeinfo(dbstack);
            % % % %                             if Args.standardized
            % % % %                                 sta_pairs = [ 'surrogatelfpstapair_stand' num2strpad(channels(1),2) num2strpad(channels(2),2)];
            % % % %                             else
            % % % %                                 sta_pairs = [ 'surrogatelfpstapair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
            % % % %                             end
            % % % %                             save(sta_pairs,'surrogate_sta_groups','channels','write_info')
            % % % %                         end
            % % % %                     end
            % % % %                 end
            % % % %             end
        end
    end
end
%for x = 1:9;subplot(3,3,x);plot(sta_groups{x});p = prctile(surrogate_sta_groups{x},[.05 99.95]);hold on;plot([0 201],[p(1) p(1)],'r');hold on;plot([0 201],[p(2) p(2)],'r');hold on;plot([0 201],[0 0],'k');axis([1 201 -40 40]);end

cd(monkeydir)