function compare_power_with_sample_recency

cd /media/bmf_raid/data/monkey/ethyl/110704/session01

sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
[~,~,~,ch] = bmf_groups
nch = size(ch,2);

cobj = mt.data.CueObj;
u = unique(cobj);

recencies = cell(1,5);
for ui = 1:5
    recencies{ui} = diff(find(cobj == u(ui)));
end

cd ([sesdir filesep 'lfp' filesep 'power'])
ptrials = nptDir('*_power.*');
ntrials = size(ptrials,1);

%calculate the cumulative power for each trial
powerch_fix = zeros(ntrials,nch);
powerch_sample = zeros(ntrials,nch);
powerch_delay2  = zeros(ntrials,nch);
for nt = 1 : ntrials
    load(ptrials(nt).name)
    
    for nc = 1 : nch
        powerch_fix(nt,nc) = sum(power.S{1}(ch(nc),7:14));
        powerch_sample(nt,nc) = sum(power.S{2}(ch(nc),7:14));
        powerch_delay2(nt,nc) = sum(power.S{4}(ch(nc),7:14));
    end
end


for x = 1 : nch
    figure
    for ui = 1:5
        uitrials = find(cobj == u(ui));
        subplot(1,5,ui)
        scatter(diff(uitrials),powerch_fix(uitrials(2:end),x),'fill','k','s')
        hold on
        scatter(diff(uitrials),powerch_sample(uitrials(2:end),x),'fill','r','s')
        hold on
        scatter(diff(uitrials),powerch_delay2(uitrials(2:end),x),'fill','g','s')
    end
end






