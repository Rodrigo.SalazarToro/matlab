function lowfreq_power(varargin)

%run at monkey level
%computes POWER_channels.mat

Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

low5 = [];
channel_power_list = [];
N = NeuronalHist('bmf');
channel_power_list(:,1) = N.gridPos';

[~,~,spect,freq] = power_analysis('ml','nlynx','allchannels');
for x = 1: size(spect,1)
    low5(x) = sum(spect{x}(1:max(find(freq{1}<=5)))); %0 to 5 Hz
end
cd([sesdir filesep 'lfp'])
channel_power_list(:,2) = low5';

save POWER_channels channel_power_list spect freq


cd(sesdir)