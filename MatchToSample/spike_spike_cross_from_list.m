function spike_spike_cross_from_list


 list = {
    
    '/Volumes/raid/data/monkey/clark/060511/session02/group0002/cluster01s'
    '/Volumes/raid/data/monkey/clark/060511/session02/group0005/cluster01s'
  
    '/Volumes/raid/data/monkey/clark/060602/session03/group0008/cluster01m'
    '/Volumes/raid/data/monkey/clark/060602/session03/group0012/cluster01m'
  
    '/Volumes/raid/data/monkey/betty/100112/session01/group0039/cluster01m'
    '/Volumes/raid/data/monkey/betty/100112/session01/group0063/cluster01m'
  
    '/Volumes/raid/data/monkey/betty/100209/session01/group0048/cluster01s'
    '/Volumes/raid/data/monkey/betty/100209/session01/group0063/cluster01m'
   
    '/Volumes/raid/data/monkey/betty/100217/session01/group0058/cluster01m'
    '/Volumes/raid/data/monkey/betty/100217/session01/group0063/cluster01m'
    
    '/Volumes/raid/data/monkey/betty/100218/session01/group0058/cluster01m'
    '/Volumes/raid/data/monkey/betty/100218/session01/group0063/cluster01m'}

subplot(2,2,1)
load('/media/raid/data/monkey/betty/100112/session01/lfp/lfp2/identity/spikecoh_g0039g0063.mat')                
plot(interp1(spikecoh.fix_f{1},spikecoh.fix_c{1},[0:1:99]),'b');hold on;plot(interp1(spikecoh.sample_f{1},spikecoh.sample_c{1},[0:1:99]),'k');hold on;plot(interp1(spikecoh.delay400_f{1},spikecoh.delay400_c{1},[0:1:99]),'r');hold on;plot(interp1(spikecoh.delaymatch_f{1},spikecoh.delaymatch_c{1},[0:1:99]),'g')
cd('/media/raid/data/monkey/betty/100112/session01/')
N = NeuronalHist('ml');
a1 = N.level1(find(N.gridPos == spikecoh.groups(1,1)));
a2 = N.level1(find(N.gridPos == spikecoh.groups(1,2)));
title([a1{:} ' ' a2{:}])


subplot(2,2,2) %??
load('/media/raid/data/monkey/betty/100209/session01/lfp/lfp2/identity/spikecoh_g0048g0063.mat')
plot(interp1(spikecoh.fix_f{2},spikecoh.fix_c{2},[0:1:99]),'b');hold on;plot(interp1(spikecoh.sample_f{2},spikecoh.sample_c{2},[0:1:99]),'k');hold on;plot(interp1(spikecoh.delay400_f{2},spikecoh.delay400_c{2},[0:1:99]),'r');hold on;plot(interp1(spikecoh.delaymatch_f{2},spikecoh.delaymatch_c{2},[0:1:99]),'g')
cd('/media/raid/data/monkey/betty/100209/session01/')
N = NeuronalHist('ml');
a1 = N.level1(find(N.gridPos == spikecoh.groups(1,1)));
a2 = N.level1(find(N.gridPos == spikecoh.groups(1,2)));
title([a1{:} ' ' a2{:}])


subplot(2,2,3)
load('/media/raid/data/monkey/betty/100217/session01/lfp/lfp2/identity/spikecoh_g0058g0063.mat')
plot(interp1(spikecoh.fix_f{1},spikecoh.fix_c{1},[0:1:99]),'b');hold on;plot(interp1(spikecoh.sample_f{1},spikecoh.sample_c{1},[0:1:99]),'k');hold on;plot(interp1(spikecoh.delay400_f{1},spikecoh.delay400_c{1},[0:1:99]),'r');hold on;plot(interp1(spikecoh.delaymatch_f{1},spikecoh.delaymatch_c{1},[0:1:99]),'g')
cd('/media/raid/data/monkey/betty/100217/session01/')
N = NeuronalHist('ml');
a1 = N.level1(find(N.gridPos == spikecoh.groups(1,1)));
a2 = N.level1(find(N.gridPos == spikecoh.groups(1,2)));
title([a1{:} ' ' a2{:}])



subplot(2,2,4)
load('/media/raid/data/monkey/betty/100218/session01/lfp/lfp2/identity/spikecoh_g0058g0063.mat')
plot(interp1(spikecoh.fix_f{1},spikecoh.fix_c{1},[0:1:99]),'b');hold on;plot(interp1(spikecoh.sample_f{1},spikecoh.sample_c{1},[0:1:99]),'k');hold on;plot(interp1(spikecoh.delay400_f{1},spikecoh.delay400_c{1},[0:1:99]),'r');hold on;plot(interp1(spikecoh.delaymatch_f{1},spikecoh.delaymatch_c{1},[0:1:99]),'g')
cd('/media/raid/data/monkey/betty/100218/session01/')
N = NeuronalHist('ml');
a1 = N.level1(find(N.gridPos == spikecoh.groups(1,1)));
a2 = N.level1(find(N.gridPos == spikecoh.groups(1,2)));
title([a1{:} ' ' a2{:}])



%need to run spikecoh for clark
% figure
% subplot(1,2,1)
% load('/media/raid/data/monkey/clark/060511/session02/lfp/lfp2/identity/spikecoh_g0002g0005.mat')
% plot(spikecoh.fix_c{1},'b');hold on;plot(spikecoh.sample_c{1},'k');hold on;plot(spikecoh.delay400_c{1},'r');hold on;plot(spikecoh.delaymatch_c{1},'g')
% 
% subplot(1,2,2)
% load('/media/raid/data/monkey/clark/060602/session03/lfp/lfp2/identity/spikecoh_g0008g0012.mat')
% plot(spikecoh.fix_c{1},'b');hold on;plot(spikecoh.sample_c{1},'k');hold on;plot(spikecoh.delay400_c{1},'r');hold on;plot(spikecoh.delaymatch_c{1},'g')

