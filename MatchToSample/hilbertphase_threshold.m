function out = hilbertphase_threshold(varargin)

%takes the mi and mi surrogate as input along with a threshold
%output is a binary matrix, 1 for sig 

Args = struct('mi',[],'surr',[],'alpha',99);
Args.flags = {};
Args = getOptArgs(varargin,Args);

out = zeros(size(Args.mi,1),size(Args.mi,2));

p = prctile(Args.surr,99.9,3);

out(find(single(Args.mi) >= single(p))) = 1;
