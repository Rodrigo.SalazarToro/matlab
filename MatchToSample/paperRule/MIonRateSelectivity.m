cd /Volumes/raid/data/monkey/
clear all
LOCobj = loadObject('psthLOCswitchObj.mat');
IDEobj = loadObject('psthIDEswitchObj.mat');

% [~,LOCind] = get(LOCobj,'number','hist',[4 11 13]);
% [~,IDEind] = get(IDEobj,'number','hist',[4 11 13]);
[~,LOCind] = get(LOCobj,'number','hist',[2 3 8 10]);
[~,IDEind] = get(IDEobj,'number','hist',[2 3 8 10]);

ttind = find(ismember(LOCobj.data.setNames(LOCind),IDEobj.data.setNames(IDEind),'rows') == 1);
tttind = find(ismember(IDEobj.data.setNames(IDEind),LOCobj.data.setNames(LOCind),'rows') == 1);
LOCind = LOCind(ttind);
IDEind = IDEind(tttind);
miss = 0;
MIide = nan(2,length(LOCind), 30);
MIloc = nan(2,length(LOCind), 30);
MIideSig = nan(2,length(LOCind), 30);
MIlocSig = nan(2,length(LOCind), 30);


xlabels = {'Time from Sample onset (ms)' 'Time from Match onset (ms)'};
matches = {'' 'MatchAlign'};
timess = { [-450:100:2450] [-1750:100:1150]};
xlimits = {[-450 1250] [-750 150]};
for mat = 1 : 2;
    match = matches{mat};
    time = timess{mat};
    for ii = 1 : length(LOCind)
        cd(LOCobj.data.setNames{LOCind(ii)})
        for r = 1 : 2
            ide = load(sprintf('PSTHIde%srule%d.mat',match,r));
            loc = load(sprintf('PSTHLoc%srule%d.mat',match,r));
            MIide(r,ii,:) = ide.mi(end,:) - getExpValue(ide.mi(1:end-1,:),[1:size(ide.mi,2)]);
            MIloc(r,ii,:) = loc.mi(end,:) - getExpValue(loc.mi(1:end-1,:),[1:size(loc.mi,2)]);
            MIideSig(r,ii,:) = ide.mi(end,:) > prctile(ide.mi(1:end-1,:),99.9);
            MIlocSig(r,ii,:) = loc.mi(end,:)  > prctile(loc.mi(1:end-1,:),99.9);
            
        end
    end
    %% distribution of MIs
    figure
    rules = {'IDE' 'LOC'};
    
    for r = 1 : 2;
        subplot(2,2,r);
        boxplot(squeeze(MIide(r,:,:)))
        set(gca,'XTick',[1:2:30])
        set(gca,'XTickLabel',time([1:2:30]))
        set(gca,'TickDir','out');
        ylabel('ide MI')
        title(rules{r})
        ylim([-0.05 0.20])
         
        subplot(2,2,2+r);
        boxplot(squeeze(MIloc(r,:,:)))
        set(gca,'XTick',[1:2:30])
        set(gca,'XTickLabel',time([1:2:30]))
        set(gca,'TickDir','out');
        ylim([-0.05 0.45])
        ylabel('loc MI')
        title(rules{r})
         xlabel(xlabels{mat})
         
    end
    
    %% MI preference between the two rules
%     figure
%     subplot(2,1,1);
%     boxplot(squeeze(MIide(1,:,:)) - squeeze(MIide(2,:,:)))
%     for tbin = 1 : 30; p1(tbin) = signtest(squeeze(MIide(1,:,tbin)) - squeeze(MIide(2,:,tbin))); end
%     %   title('ide MI')
%     title('ide MI')
%     ylim([-0.3 0.30])
%     ylabel('IDE - LOC')
%     subplot(2,1,2);
%     boxplot(squeeze(MIloc(1,:,:)) - squeeze(MIloc(2,:,:)))
%     for tbin = 1 : 30; p2(tbin) = signtest(squeeze(MIloc(1,:,tbin)) - squeeze(MIloc(2,:,tbin))); end
%     ylim([-0.3 0.30])
%     title('loc MI')
%     ylabel('IDE - LOC')
    
    %% MI as a function of time
    figure
    subplot(2,1,1);
    plot(time,prctile(squeeze(MIide(1,:,:)),[10 25 50 75 90]),'b')
    hold on
    plot(time,prctile(squeeze(MIide(2,:,:)),[10 25 50 75 90]),'r')
     for ti = 1 : 30; pIde = signrank(squeeze(MIide(1,:,ti)),squeeze(MIide(2,:,ti))); text(time(ti),0,num2str(pIde));end
    
    %   title('ide MI')
    title('ide MI')
     set(gca,'TickDir','out');
     xlim(xlimits{mat})
     ylim([-.01 0.03])
    subplot(2,1,2);
    plot(time,prctile(squeeze(MIloc(1,:,:)),[10 25 50 75 90]),'b')
    hold on
    plot(time,prctile(squeeze(MIloc(2,:,:)),[10 25 50 75 90]),'r')
     set(gca,'TickDir','out');
      for ti = 1 : 30; pLoc = signrank(squeeze(MIloc(1,:,ti)),squeeze(MIloc(2,:,ti))); text(time(ti),0,num2str(pLoc));end
     
    title('loc MI')
    legend('IDE; 90th','IDE; 75th','IDE; 50th','IDE; 25th','IDE; 10th','LOC;90th','LOC; 75th','LOC; 50th','LOC; 25th','LOC; 10th')
     xlabel(xlabels{mat})
    ylabel('MI')
    xlim(xlimits{mat})
    ylim([-.01 0.03])
    %% incidence of significant MI
    figure
    rules = {'IDE' 'LOC'};
    la = {'b' 'r'};
   
    for r = 1 : 2;
        subplot(2,1,1);
        plot(time,100*nansum(squeeze(MIideSig(r,:,:)))/size(MIideSig,2),la{r})
       
        hold on
        title('ide MI')
        ylim([0 20])
        set(gca,'TickDir','out');
        xlim(xlimits{mat})
        subplot(2,1,2);
        plot(time,100*nansum(squeeze(MIlocSig(r,:,:)))/size(MIlocSig,2),la{r})
        
        hold on
        title('loc MI')
        ylim([0 20])
        set(gca,'TickDir','out');
        xlim(xlimits{mat})
        xlabel(xlabels{mat})
    end
    legend('IDE','LOC')
end
%% compute teh MIs

% dirs = {'/Volumes/raid/data/monkey/clark' '/Volumes/raid/data/monkey/betty' '/Volumes/raid/data/monkey/betty/acute'};
%
% for dir = 1 : 3
%     cd(dirs{dir})
%     dd = load('switchdays.mat');
%     fname = fieldnames(dd);
%     if strcmp('days',fname{1})
%         days = dd.days;
%     else
%         days = dd.switchdays;
%     end
%     parfor d = 1 : length(days)
%         cd([dirs{dir} '/' days{d}])
%
%         psthSurDay('save','rule',1,'session99')
%         psthSurDay('save','rule',2,'session99')
%
%     end
% end





