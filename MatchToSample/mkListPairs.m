function [data,varargout] = mkListPairs(varargin)

Args = struct('mainFile','cohInter','fband',[],'compareChart',[1 2;1 3;1 4;2 3;2 4;3 4],'plot',0,'pLevel',4,'save',0,'mvAvg',3,'modThresh',20,'modSurThresh',10,'thedays',[],'peakyness',[],'alphaMod',20,'noLocalMax',0,'surFile','general');
Args.flags = {'plot','save','noLocalMax'};
[Args,modvarargin] = getOptArgs(varargin,Args);

pdir = pwd;

mainFile = sprintf('%s.mat',Args.mainFile);
compFile = sprintf('%sPcomp.mat',Args.mainFile);
if ~isempty(Args.surFile)
    surfile = sprintf('%sSur.mat',Args.surFile);
else
    surfile = sprintf('%sSur.mat',Args.mainFile);
    
end

clark = findstr(pdir,'clark');
betty = findstr(pdir,'betty');

if isempty(Args.fband)
    if ~isempty(clark)
        Args.fband = [13 30;30 42;7 13; 1 7];
    elseif ~isempty(betty)
        Args.fband = [13 30;30 42;7 13; 1 7];
    end
end
if isempty(Args.thedays)
    if ~isempty(clark)
        days = switchDay;
    elseif ~isempty(betty)
        days = switchDay('ML','combNtrials',140);
    end
else
    days = Args.thedays;
end



% days = switchDay('ML','combNtrials',150);

compareChart = Args.compareChart;
beta = Args.fband(1,:);
gamma = Args.fband(2,:);
alpha = Args.fband(3,:);
theta = Args.fband(4,:);
allsurmod = [];

clear sigdiff
for r = 1 : 2; sigdiff{r} = []; msigdiff{r} = []; coher{r} = []; rdata{r} = [];mrdata{r} = []; mcoher{r} = []; Index{r} = []; mIndex{r} = []; msurro{r} = [];surro{r} = []; Smod{r} = [];mSmod{r} = [];end
for d = 1 : size(days,2)
    cd(days{d})
    filecomp = nptDir(compFile);
    if ~isempty(filecomp)
        load(filecomp.name)
        coh = load(mainFile);
        if ~isempty(Args.modSurThresh);
            sur = load(surfile);
            if size(sur.Day.session(1).Prob,3) ~= size(coh.Day.session(1).C,2)
                for s = 1 : length(sur.Day.session); sur.Day.session(s).Prob = repmat(sur.Day.session(s).Prob,[1 1 size(coh.Day.session(1).C,2) 1]); end
            end
        end
        limitB = find(f>= beta(1) & f<= beta(2));
        limitG = find(f>= gamma(1) & f<= gamma(2));
        limitA = find(f>= alpha(1) & f<= alpha(2));
        limitT = find(f>= theta(1) & f<= theta(2));
        
        for s = 1 : size(Day.session,2)
            if ~isempty(coh.Day.session(s).C)
                if size(coh.Day.session(s).C,2) ~= size(Day.session(s).Adz,2); keyboard; end
                r = Day.session(s).rule;
                sigdiff{r} = [sigdiff{r} ~Day.session(s).Adz]; % significance between windows
                if ~isempty(Args.modSurThresh);
                    if Args.pLevel > size(sur.Day.session(s).Prob,2); Args.pLevel = 1; end
                    abovesur = squeeze(coh.Day.session(s).C) > squeeze(sur.Day.session(s).Prob(:,Args.pLevel,:,:)); % data point above the surrogate
                    
                    surmod = 100 * (squeeze(coh.Day.session(s).C) - squeeze(sur.Day.session(s).Prob(:,Args.pLevel,:,:))) ./ squeeze(sur.Day.session(s).Prob(:,Args.pLevel,:,:));
                    
                    if size(coh.Day.session(s).C,2) == 1 
                        
                        surmod = reshape(surmod,[size(surmod,1) 1 size(surmod,2)]);
                    end
                else
                    abovesur = squeeze(coh.Day.session(s).C) > 0.18;
                    surmod = 100 * (squeeze(coh.Day.session(s).C) - 0.18) ./ 0.18;
                end
                
                for d1 = 1 : size(surmod,2); for d2 = 1 : size(surmod,3); surmod(:,d1,d2) = (surmod(:,d1,d2) >= 0) .* surmod(:,d1,d2); end; end;
                adata = coh.Day.session(s).C;
                
                %                 ff = repmat(1/5,1,5);
                
                if ~isempty(Args.mvAvg)
                    for dim1 = 1 : size(adata,2); for dim2 = 1 : size(adata,3); adata(:,dim1,dim2) = filtfilt(repmat(1/Args.mvAvg,1,Args.mvAvg),1,squeeze(adata(:,dim1,dim2)));end; end
                end
                sdata = squeeze(adata) .* abovesur ;% (surmod > Args.modSurThresh);% abovesur;
                for dim1 = 1 : size(adata,2); for dim2 = 1 : size(adata,3);
                        fcts = which('findpeaks','-all');
                        
                        thefct = strfind(fcts,sprintf('toolbox%ssignal%ssignal%sfindpeaks.m',filesep,filesep,filesep));
                        for nf =  1 : length(thefct); if ~isempty(thefct{nf}); tfn = nf; end; end
                        pdir = pwd;
                        cd(fcts{tfn}(1:end-11));
                        %                         tsf = filtfilt(ff,1,squeeze(adata(:,dim1,dim2)));
                        if isempty(Args.peakyness)
                            [lmax,locs] = findpeaks(adata(:,dim1,dim2)); fpeaks = f(locs);
                            %                             [lmax,locs] = findpeaks(tsf); fpeaksA = f(locs);
                        else
                            [lmax,locs] = findpeaks(adata(:,dim1,dim2),'threshold',Args.peakyness); fpeaks = f(locs);
                        end
                        cd(pdir)
                        %                         else
                        %                             lmax = findpeaks(adata(:,dim1,dim2)); fpeaks = f(lmax.loc);
                        %                         end
                        if length(size(surmod)) < 3
                            fpeaks = fpeaks(surmod(locs,dim2) > 0);
                        else
                            fpeaks = fpeaks(surmod(locs,dim1,dim2) > 0); % only select for significant peaks
                        end
                        %                         if dim1 == 267 | dim1 == 435
                        %                             display('stop')
                        %                         end
                        if isempty(find(fpeaks >= beta(1) & fpeaks <  beta(2))) || (~isempty(Args.modSurThresh) && mean(surmod(limitB,dim1,dim2)) < Args.modSurThresh)
                            %                         if (~isempty(Args.modSurThresh) && mean(surmod(limitB,dim1,dim2)) <= Args.modSurThresh)
                            sdata(limitB,dim1,dim2) = 0; % local maxima has to be in the beta range
                            surmod(limitB,dim1,dim2) = 0;
                        end
                        
                        if isempty(find(fpeaks >= gamma(1) & fpeaks < gamma(2))) || (~isempty(Args.modSurThresh) && mean(surmod(limitG,dim1,dim2)) < Args.modSurThresh)
                            %                         if (~isempty(Args.modSurThresh) && mean(surmod(limitG,dim1,dim2)) <= Args.modSurThresh)
                            sdata(limitG,dim1,dim2) = 0;% local maxima has to be in the gamma range
                            surmod(limitG,dim1,dim2) = 0;
                        end
                        
                        %                         if isempty(find(fpeaks >= alpha(1) & fpeaks <= alpha(2))) || (~isempty(Args.alphaMod) && mean(surmod(limitA,dim1,dim2)) < Args.alphaMod)
                        if isempty(find(fpeaks >= alpha(1) & fpeaks < alpha(2))) || (~isempty(Args.modSurThresh) && mean(surmod(limitA,dim1,dim2)) < Args.modSurThresh)
                            %                         if (~isempty(Args.modSurThresh) && mean(surmod(limitA,dim1,dim2)) <= Args.modSurThresh)
                            sdata(limitA,dim1,dim2) = 0;% local maxima has to be in the alpha range
                            surmod(limitA,dim1,dim2) = 0;
                        end
                        if isempty(find(fpeaks >= theta(1) & fpeaks < theta(2))) || (~isempty(Args.modSurThresh) && mean(surmod(limitT,dim1,dim2)) < Args.modSurThresh)
                            %                         if (~isempty(Args.modSurThresh) && mean(surmod(limitA,dim1,dim2)) <= Args.modSurThresh)
                            sdata(limitT,dim1,dim2) = 0;% local maxima has to be in the alpha range
                            surmod(limitT,dim1,dim2) = 0;
                        end
                        
                        allsurmod = [allsurmod  mean(surmod(limitB,dim1,dim2))];
                    end; end
                
                coher{r} = [coher{r} sdata]; % select the coherence values significantly different from the surrogate
                rdata{r} = [rdata{r} adata];
                Smod{r} = [Smod{r} surmod];
                if ~isempty(Args.modSurThresh); surro{r} = [surro{r} squeeze(sur.Day.session(s).Prob(:,Args.pLevel,:,:)) .* abovesur];else surro{r} = [surro{r} repmat(0.18,size(coh.Day.session(s).C)) .* abovesur]; end
                k = findstr('0', Day.name);
                
                Index{r} = [Index{r}; [repmat(str2double(Day.name(k+2:end)),size(Day.comb,1),1) Day.comb [1:size(Day.comb,1)]']];
            end
        end
        clear Day coh
    end
    cd ..
end
varargout{1} = allsurmod;
for r = 1 : 2; [mindex{r},m,n] = unique(Index{r},'rows');
    for row = 1 : size(mindex{r},1); indn = find(n == row);
        if length(indn) == 1;
            
            msigdiff{r} = [msigdiff{r}  sigdiff{r}(:,indn,:)];
            mcoher{r} = [mcoher{r} coher{r}(:,indn,:)];
            msurro{r} = [msurro{r} surro{r}(:,indn,:)];
            mrdata{r} = [mrdata{r} rdata{r}(:,indn,:)];
            mSmod{r} = [mSmod{r} Smod{r}(:,indn,:)];
        else
            msigdiff{r} = [msigdiff{r}  (sum(sigdiff{r}(:,indn,:),2) ~= 0)];
            mcoher{r} = [mcoher{r} mean(coher{r}(:,indn,:),2)];
            msurro{r} = [msurro{r} mean(surro{r}(:,indn,:),2)];
            mrdata{r} = [mrdata{r} mean(rdata{r}(:,indn,:),2)];
            mSmod{r} = [mSmod{r} mean(Smod{r}(:,indn,:),2)];
        end
    end
end

oldsigdiff = sigdiff;
oldcoher = coher;
oldsurro = surro;
oldSmod = Smod;
clear sigdiff coher surro rdata Smod

sigdiff = msigdiff;
coher = mcoher;
surro = msurro;
rdata = mrdata;
Smod = mSmod;
for r = 1 : 2; n(r) = size(sigdiff{r},2); end
%%
if Args.plot
    figure; for r = 1 : 2; for cmp = 1 : 6;subplot(2,6,(r-1)*6+cmp); plot(f,sum(sigdiff{r}(:,:,cmp),2)/n(r)); end; end
    for sb = 1 : 6; subplot(2,6,sb); title(compareChart(sb,:)); end
    for sb = 1 : 12; subplot(2,6,sb); axis([10 50 0 1]); grid on; end
    subplot(2,6,1); ylabel('IDENTITY')
    subplot(2,6,7); ylabel('LOCATION')
end
Flimit = [limitB(1) limitB(end); limitG(1) limitG(end); limitA(1) limitA(end); limitT(1) limitT(end)];
for r = 1 : 2; for cmp = 1 : 6; for fband = 1 : size(Flimit,1); count{r,cmp,fband} = 1; end; end; end

for r = 1 : 2
    if ~isempty(coher{r})
        for pair = 1 : n(r)
            for cmp = 1 : 6
                for fband = 1 : size(Flimit,1)
                    ind = find(sigdiff{r}(Flimit(fband,1):Flimit(fband,2),pair,cmp) == 1);
                    if ~isempty(ind)
                        for w = 1 : 2
                            co{r,cmp,fband}(:,count{r,cmp,fband},w) = coher{r}(:,pair,compareChart(cmp,w));
                            
                        end
                        count{r,cmp,fband} = count{r,cmp,fband} + 1;
                    end
                end
            end
        end
    end
end
%%
band = {'beta' 'gamma'};


clear rule posneg
for r = 1 : 2
    if isempty(coher{r})
        cmp = 1;
        rule(r).cmp = [];
        modu{r,cmp} = [];
        posneg{r} =[];
    else
        for cmp = 1 : 6
            
            rule(r).cmp(cmp).diff = sign(coher{r}(:,:,compareChart(cmp,2)) - coher{r}(:,:,compareChart(cmp,1))); % get the difference between windows
            modu{r,cmp} = 100 * (rdata{r}(:,:,compareChart(cmp,2)) - rdata{r}(:,:,compareChart(cmp,1))) ./ rdata{r}(:,:,compareChart(cmp,1)); % get the difference between windows
            
            %         posneg{r}(:,:,cmp) = squeeze(sigdiff{r}(:,:,cmp)) .*rule(r).cmp(cmp).diff; % get the significant difference where one the two values is above the surrogate
            
            posneg{r}(:,:,cmp) = squeeze(sigdiff{r}(:,:,cmp)) .* sign(modu{r,cmp}) .* ((squeeze(Smod{r}(:,:,compareChart(cmp,2))) + squeeze(Smod{r}(:,:,compareChart(cmp,1)))) > 0);
        end
    end
end
%% modulation threshold and check that the direction of teh comparison
%% matches the modulation sign
dir = {'<' '>'};
alleffects = [-1 1];

for r = 1 : 2
    if isempty(posneg{r})
        posneg{r} = [];
    else
        for b = 1 : size(Flimit,1)
            for cmp = 1 : 6
                
                for si = 1 : 2
                    eval(sprintf('[row,col1] = find(posneg{r}(Flimit(b,1):Flimit(b,2),:,cmp) %s 0);',dir{si}));
                    col1 = unique(col1);
                    for c = 1 : length(col1)
                        
                        der = unique(sign(diff(coher{r}(Flimit(b,1):Flimit(b,2),col1(c),compareChart(cmp,si)))));
                        % derivative to check for local maxima, then take the
                        % sign and then unique it
                        %                     area1 = sum(coher{r}(Flimit(b,1):Flimit(b,2),col1(c),compareChart(cmp,setdiff([1 2],si)))) - sum(surro{r}(Flimit(b,1):Flimit(b,2),col1(c),compareChart(cmp,setdiff([1 2],si))));
                        %
                        %                     area2 = sum(coher{r}(Flimit(b,1):Flimit(b,2),col1(c),compareChart(cmp,si))) - sum(surro{r}(Flimit(b,1):Flimit(b,2),col1(c),compareChart(cmp,si)));
                        %                     effect = sign(area2-area1);
                        mmod = mean(modu{r,cmp}(Flimit(b,1):Flimit(b,2),col1(c)));
                        effect = sign(mmod);
                        %                         if Args.noLocalMax
                        if effect ~= alleffects(si) || abs(mmod) < Args.modThresh
                            %                         posneg{r}(Flimit(b,1) + row(c)-1,col1(c),cmp) = 0;
                            eval(sprintf('[row2,col2] = find(posneg{r}(Flimit(b,1):Flimit(b,2),col1(c),cmp) %s 0);',dir{si}));
                            posneg{r}(Flimit(b,1) + row2 -1,col1(c),cmp) = zeros(length(row2),1);
                            %                                 posneg{r}(Flimit(b,1) : Flimit(b,2),col1(c),cmp) = zeros(size([Flimit(b,1) : Flimit(b,2)]),1);
                            % display('yeah')
                        end
                        %                         else
                        %                             if isempty(find(der == 1)) | isempty(find(der == -1)) | effect ~= alleffects(si) | abs(mmod) < Args.modThresh
                        %                                 %                         posneg{r}(Flimit(b,1) + row(c)-1,col1(c),cmp) = 0;
                        %                                 posneg{r}(Flimit(b,1) : Flimit(b,2),col1(c),cmp) = zeros(size([Flimit(b,1) : Flimit(b,2)]),1);
                        %                                 % display('yeah')
                        %                             end
                        %                         end
                    end
                    
                end
                
                
            end
        end
    end
end
%%
% allcomp = [2 3 6];
allcomp = [1:6];
for r = 1 : 2;
    for cmp = 1:length(allcomp);
        if isempty(posneg{r})
            pos{r,allcomp(cmp)} = [];
            neg{r,allcomp(cmp)} = [];
            for band = 1 : size(Flimit,1)
                modulation{r,cmp,band,1} = [];
                modulation{r,cmp,band,2} = [];
            end
        else
            
            pos{r,allcomp(cmp)} = posneg{r}(:,:,allcomp(cmp)) >0;
            neg{r,allcomp(cmp)} = posneg{r}(:,:,allcomp(cmp)) <0;
            for band = 1 : size(Flimit,1)
                
                [row,c] = find(pos{r,cmp}(Flimit(band,1):Flimit(band,2),:) == 1);
                c = unique(c);
                modulation{r,cmp,band,1} = mean(modu{r,cmp}(Flimit(band,1):Flimit(band,2),c),1);
                
                [row,c] = find(neg{r,cmp}(Flimit(band,1):Flimit(band,2),:) == 1);
                c = unique(c);
                modulation{r,cmp,band,2} = mean(modu{r,cmp}(Flimit(band,1):Flimit(band,2),c),1);
            end
        end
    end
end

if Args.plot
    figure;
    for r = 1 : 2;
        for cmp = 1:length(allcomp);
            subplot(2,length(allcomp),(r-1)*length(allcomp)+cmp);
            plot(f,sum(pos{r,allcomp(cmp)},2)/n(r));
            hold on;
            plot(f,-sum(neg{r,allcomp(cmp)},2)/n(r));
        end;
    end
    for sb = 1 : length(allcomp); subplot(2,length(allcomp),sb); title(compareChart(allcomp(sb),:)); end
    for sb = 1 : length(allcomp)*2; subplot(2,length(allcomp),sb); axis([10 50 -0.2 0.5]); grid on; end
    subplot(2,length(allcomp),1); ylabel('IDENTITY')
    subplot(2,length(allcomp),length(allcomp)+1); ylabel('LOCATION')
end

for r = 1 : 2
    for cmp = 1 : 6
        
        for band = 1 : size(Flimit,1)
            if isempty(pos{r,cmp}) || isempty(neg{r,cmp})
                pairs{r,cmp,band} = [];
            else
                
                if band == 1
                    [row,c] = find(pos{r,cmp}(Flimit(band,1):Flimit(band,2),:) == 1);
                elseif band == 2
                    [row,c] = find(neg{r,cmp}(Flimit(band,1):Flimit(band,2),:) == 1);
                elseif band == 3
                    [row,c] = find(neg{r,cmp}(Flimit(band,1):Flimit(band,2),:) == 1);
                end
                c = unique(c);
                pairs{r,cmp,band} = Index{r}(c,:);
                %             modulation{r,cmp,band} = mean(modu{r,cmp}(Flimit(band,1):Flimit(band,2),c),1);
            end
        end
    end
end

data.f = f;
data.Index = Index;
data.pos = pos;
data.neg = neg;
data.pairs = pairs;
data.modulation = modulation;
data.modu = modu;
data.coher = coher;
data.rdata = rdata;
data.Smod = Smod;
if Args.save
    
    save('list.mat','f','Index','pos','neg','pairs','modu','modulation')
    
end
for r = 1 : 2
    betaGamma{r} = intersect(pairs{r,3,1}, pairs{r,3,2},'rows');
    
    betadelay{r} = intersect(pairs{r,6,1}, pairs{r,6,1},'rows');
    
end

