function [varargout] = spectOverTrials(gr1, gr2,varargin)
% to be run in the sessio directory
% use 'BehResp',1








Args = struct('redo',0,'save',0,'window',50,'rule',1,'ML',0);
Args.flags = {'redo','save','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sdir = pwd;

rsFs = 200; % downsampling

params = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1,'err',[2 0.341]);

ndir = 'overTrials';
mkdir(ndir)

matfile = sprintf('%s/%s/cohInterTrialsg%04.fg%04.frule%d.mat',pwd,ndir,gr1,gr2,Args.rule);



if isempty(nptDir(matfile)) || Args.redo
    
    mtst = mtstrial('auto','redosetNames');
    
    
    
    neuroInfo = NeuronalChAssign;
    chs(1) = find(neuroInfo.groups == gr1);
    chs(2) = find(neuroInfo.groups == gr2);
    
    if Args.ML
        nblocks = length(find(diff(find(mtst.data.Index(:,1) == Args.rule)) > 1)) + 1;
        nblockso = length(find(diff(find(mtst.data.Index(:,1) == setdiff([1 2],Args.rule))) > 1)) + 1;
        totblocks = nblocks + nblockso;
        firstblock = mtst.data.Index(1,1);
        sblock = [firstblock:2:totblocks];
    else
        
        nblocks = 1;
       sblock = 1;
    end
    
    Ctrials = cell(nblocks,1);
    Phitrials = cell(nblocks,1);
    S1trials = cell(nblocks,1);
    S2trials = cell(nblocks,1);
    Cerrtrials = cell(nblocks,1);
    phistdtrials = cell(nblocks,1);
    
    for bl = 1 : nblocks
        alltrials = mtsgetTrials(mtst,modvarargin{:},'rule',Args.rule,'block',sblock(bl));
        fixationtrials = mtsgetTrials(mtst,'CueObj',55);
        alltrials = setdiff(alltrials,fixationtrials);
        ntrials = length(alltrials);
        if ntrials > Args.window
            tr = [(Args.window +1) : ntrials];
            [data,~] = lfpPcut(alltrials,chs,'fromFiles');
            Ctrials{bl} = nan(ntrials - Args.window,65,4);
            Phitrials{bl} = nan(ntrials - Args.window,65,4);
            S1trials{bl} = nan(ntrials - Args.window,65,4);
            S2trials{bl} = nan(ntrials - Args.window,65,4);
            Cerrtrials{bl} = nan(ntrials - Args.window,2,65,4);
            phistdtrials{bl} = nan(ntrials - Args.window,65,4);
            for sec = 1 : (ntrials - Args.window)
                
                lowLim = sec;
                
                highLim = Args.window+sec;
                for p = 1 : 4
                    
                    [C,phi,~,S1,S2,f,~,phistd,Cerr] = coherencyc(squeeze(data{p}(:,lowLim:highLim,1)),squeeze(data{p}(:,lowLim:highLim,2)),params);
                    Ctrials{bl}(sec,:,p) = single(C);
                    Phitrials{bl}(sec,:,p) = single(phi);
                    S1trials{bl}(sec,:,p) = single(S1);
                    S2trials{bl}(sec,:,p) = single(S2);
                    Cerrtrials{bl}(sec,:,:,p) = single(Cerr);
                    phistdtrials{bl}(sec,:,p) = single(phistd);
                end
                
                
            end
            
            
            
        else
            fprintf('Too few trials in the session/block \n')
            
        end
        
    end
    if Args.save
        save(matfile,'Ctrials','Phitrials','S1trials','S2trials','Cerrtrials','phistdtrials','f','tr');
        fprintf(matfile);
    end
    
else
    
    fprintf('the electrodes available do not satisfy the type of analysis')
end

cd(sdir)
