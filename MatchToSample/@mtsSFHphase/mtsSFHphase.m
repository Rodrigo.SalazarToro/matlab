function obj = mtsSFHphase(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'afterMatch',200,'pThres',0.01);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsSFHphase';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsSFH';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('site','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)


if ~isempty(findstr('betty',pwd))
    
    cd session01
    histology = NeuronalHist;
    neuroinfo = NeuronalChAssign;
    groups = nptDir('group0*');
    thegroups = zeros(length(groups),1);
    for gr =1 : length(groups); thegroups(gr) = str2double(groups(gr).name(end-1 : end)); end
    cd ..
    
else
    load rules
    groupn = cell(2,1);
    for s = 2 : 3;
        cd(['session0' num2str(s)])
        histology = NeuronalHist;
        neuroinfo = NeuronalChAssign;
        groups = nptDir('group0*');
        for gr =1 : length(groups); groupn{s-1}(gr) = str2double(groups(gr).name(end-1 : end)); end
        cd ..
    end
    thegroups = intersect(groupn{1}, groupn{2});
end
sdir = pwd;

thechs = find(ismember(neuroinfo.groups,thegroups) == 1);
careas = histology.number(ismember(neuroinfo.groups,thegroups));

allcomb = [nchoosek([1:length(thegroups)],2); fliplr(nchoosek([1:length(thegroups)],2)); (1:length(thegroups))' (1:length(thegroups))'];

data.Index =  [(1:size(allcomb,1))'  ones(size(allcomb,1),1) thegroups(allcomb) thechs(allcomb)];

data.hist = careas(allcomb);
data.setNames = repmat({sdir},size(allcomb,1),1);
data.numSets = size(allcomb,1);
if ~isempty(findstr('betty',pwd))
    data.sessions = ones(size(allcomb,1),2);
else
    data.sessions = repmat([find(r==1)+ 1 find(r==2)+ 1],size(allcomb,1),1) ;
end

epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};

for ep = 1 : 5;
    eval(sprintf('data.RulesDiff%s = nan(data.numSets,3);',epochs{ep}))
    
    
end
for ii = 1 : data.numSets
    lfpCH = data.Index(ii,6);
    unitgr = sprintf('/group%04.0f/',data.Index(ii,3));
    pathf = [regexprep(data.setNames{ii},'FITZGERALD','FUSION') '/session0' num2str(data.sessions(ii,1)) unitgr 'cluster01m/HBphase/'];
    
    
    for ep = 1 : 5
        %% spike to HB LFP phase pvalue
        statfile = nptDir(sprintf('%scompSpikeLFPphaseRules%s*.mat',pathf,epochs{ep}));
        load(sprintf('%s%s',pathf,statfile.name))
        thepval = pval(lfpCH);
        eval(sprintf('data.RulesDiff%s(ii,1) = thepval;',epochs{ep}))
        %% psth difference
        cd(pathf)
        cd ..
        
        statfile = nptDir(sprintf('compPSTHRules%s*.mat',epochs{ep}));
        psthstat = load(statfile(1).name);
        if ep ==5
            timebins = find(psthstat.Mbins > 0 & psthstat.Mbins < Args.afterMatch);
            
        else
            timebins = [1 : length(psthstat.pval)];
        end
        nsigtbins = sum(psthstat.pval(timebins) < (Args.pThres / length(timebins)));
        eval(sprintf('data.RulesDiff%s(ii,2) = nsigtbins;',epochs{ep}))
        cd (sdir)
        
        %% EP diff
        cd(pathf)
        cd ../../..
        cd lfp
        statfile = nptDir(sprintf('compEPRules%s*.mat',epochs{ep}));
        EPstat = load(statfile.name);
        nsigtbins = sum(EPstat.pval(timebins) < (Args.pThres / length(timebins)));
        eval(sprintf('data.RulesDiff%s(ii,3) = nsigtbins;',epochs{ep}))
        cd (sdir)
        
    end
end



% create nptdata so we can inherit from it
n = nptdata(data.numSets,0,pwd);
d.data = data;
obj = class(d,Args.classname,n);
if(Args.SaveLevels)
    fprintf('Saving %s object...\n',Args.classname);
    eval([Args.matvarname ' = obj;']);
    % save object
    eval(['save ' Args.matname ' ' Args.matvarname]);
end





function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];
epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
for ep = 1 : 5;
    eval(sprintf('data.RulesDiff%s = [];',epochs{ep}))
    
    
end
% useful fields for most objects
data.Index = [];
data.sessions = [];

data.numSets = 0;
data.setNames = '';
data.hist = [];
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
