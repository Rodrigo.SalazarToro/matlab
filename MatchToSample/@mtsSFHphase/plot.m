function obj = plot(obj,varargin)


Args = struct('pLevel',3,'xlim',[0 50],'plotType','standard','freqs',[1:4],'epoch','saccade','sfreq',1,'binSize',5,'pThres',0.05/60);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work soon
    ind = dataindices(n);
else
    
end

epochs = {'presample' 'sample' 'delay1' 'delay2'};
tuning = {'iCueLoc' 'iCueObj'};

itemC = {'b' 'r' 'k'};

[pathstr, name, ~] = fileparts(obj.data.setNames{ind});
lfp = name(8:end);

f = [8:4:44];
rules = {'IDE' 'LOC'};
lfpCH = obj.data.Index(ind,6);
unitgr = sprintf('/group%04.0f/',obj.data.Index(ind,3));
lfpgr = sprintf('/group%04.0f/',obj.data.Index(ind,4));
tout =[  0
    0
    0.3142
    0
    0
    0.3142
    0.6283
    0
    0
    0.6283
    0.9425
    0
    0
    0.9425
    1.2566
    0
    0
    1.2566
    1.5708
    0
    0
    1.5708
    1.8850
    0
    0
    1.8850
    2.1991
    0
    0
    2.1991
    2.5133
    0
    0
    2.5133
    2.8274
    0
    0
    2.8274
    3.1416
    0
    0
    3.1416
    3.4558
    0
    0
    3.4558
    3.7699
    0
    0
    3.7699
    4.0841
    0
    0
    4.0841
    4.3982
    0
    0
    4.3982
    4.7124
    0
    0
    4.7124
    5.0265
    0
    0
    5.0265
    5.3407
    0
    0
    5.3407
    5.6549
    0
    0
    5.6549
    5.9690
    0
    0
    5.9690
    6.2832
    0];
switch Args.plotType
    
    case 'standard'
        
        
        for r =1 : 2
            pathf = [regexprep(obj.data.setNames{ind},'FITZGERALD','FUSION') '/session0' num2str(obj.data.sessions(ind,r)) unitgr 'cluster01m/HBphase/'];
            load([pathf Args.epoch 'Phasesrule' num2str(r) '.mat']); % temporary solution for my ext drives
            
            
            %        load([pathf '/HBphase/' 'saccadePhasesrule' num2str(r)
            %        '.mat']);
            %            thech = find(obj.data.Index(ind,6) neuroinfo.groups);
            flim = max([max(max(rout(lfpCH,Args.freqs,:))) max(max(thr(lfpCH,Args.freqs,:,Args.pLevel)))]);
            sig = rout > squeeze(thr(:,:,:,Args.pLevel));
            for fi = Args.freqs
                subplot(2,length(Args.freqs),(r-1)*length(Args.freqs)+fi)
                polar(0,ceil(flim)/ sum(rout(lfpCH,fi,:))*200)
                hold on
                polar(vecc(tout),vecc(squeeze(rout(lfpCH,fi,:))) / sum(rout(lfpCH,fi,:))*200)
                hold on
                
                polar(vecc(tout),vecc(squeeze(thr(lfpCH,fi,:,Args.pLevel)))/ sum(rout(lfpCH,fi,:))*200,'r--') % surrogate
                polar(vecc(tout),vecc(squeeze(sig(lfpCH,fi,:))) *ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'m*') % significant bins
                
                mphase = circ_mean(vecc(tout),vecc(squeeze(rout(lfpCH,fi,:))));
                polar(mphase,ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'gp') % mean over all phase
                
                sigb = squeeze(sig(lfpCH,fi,:));
                mphase = circ_mean(vecc(tout(sigb)),vecc(squeeze(rout(lfpCH,fi,sigb))));
                if ~isempty(mphase)
                    polar(mphase,ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'kp') % mean of significant phases
                end
                if r == 1; title(['freq. ' num2str(f(fi)) 'Hz']) ;end
                if fi == Args.freqs(1); ylabel(rules{r}); end
            end
        end
        
    case 'withEP&PSTH'
        maxFR = zeros(2,1);
        limEP = zeros(2,2);
        maxtime = zeros(2,1);
        for r =1 : 2
            pathh = [regexprep(obj.data.setNames{ind},'FITZGERALD','FUSION') '/session0' num2str(obj.data.sessions(ind,r)) unitgr 'cluster01m/HBphase/'];
             pathf = [regexprep(obj.data.setNames{ind},'FITZGERALD','FUSION') '/session0' num2str(obj.data.sessions(ind,r)) unitgr 'cluster01m/'];
            load([pathh Args.epoch 'Phasesrule' num2str(r) '.mat']); % temporary solution for my ext drives
            
            
            flim = max([max(max(rout(lfpCH,Args.freqs,:))) max(max(thr(lfpCH,Args.freqs,:,Args.pLevel)))]);
            sig = rout > squeeze(thr(:,:,:,Args.pLevel));
            fi = Args.sfreq;
            subplot(2,3,(r-1)*3+fi)
            polar(0,ceil(flim)/ sum(rout(lfpCH,fi,:))*200)
            hold on
            polar(vecc(tout),vecc(squeeze(rout(lfpCH,fi,:))) / sum(rout(lfpCH,fi,:))*200)
            hold on
            
            polar(vecc(tout),vecc(squeeze(thr(lfpCH,fi,:,Args.pLevel)))/ sum(rout(lfpCH,fi,:))*200,'r--') % surrogate
            polar(vecc(tout),vecc(squeeze(sig(lfpCH,fi,:))) *ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'m*') % significant bins
            
            mphase = circ_mean(vecc(tout),vecc(squeeze(rout(lfpCH,fi,:))));
            polar(mphase,ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'gp') % mean over all phase
            
            sigb = squeeze(sig(lfpCH,fi,:));
            mphase = circ_mean(vecc(tout(sigb)),vecc(squeeze(rout(lfpCH,fi,sigb))));
            if ~isempty(mphase)
                polar(mphase,ceil(flim)/ sum(rout(lfpCH,fi,:))*200,'kp') % mean of significant phases
            end
            if r == 1; title(['freq. ' num2str(f(fi)) 'Hz']) ;end
            ylabel(rules{r});
            
            statfile = nptDir(sprintf('%scompSpikeLFPphaseRules%s*.mat',pathf,Args.epoch));
            load(sprintf('%s%s',pathf,statfile.name))
            thepval = pval(lfpCH);
            
            title(sprintf('Rules diff p=%6.4f; ratio spikes=%3.2f',thepval,nspikes(1)/nspikes(2)))
            %% psth
            sdir = pwd;
            cd(pathf)
            cd ..
            load(sprintf('nineStimPSTH%d.mat',r),'A','reference')
            statfile = nptDir(sprintf('compPSTHRules%s*.mat',Args.epoch));
            psthstat = load(statfile(1).name);
            
            switch Args.epoch
                case 'presample'
                    timing = [-400 0];
                    ref = 1;
                case 'sample'
                    timing = [100 500];
                    ref = 1;
                case 'delay1'
                    timing = [500 900];
                    ref = 1;
                case 'delay2'
                    timing = [-400 0];
                    ref = 2;
                case 'saccade'
                    timing = [-100 200];
                    ref = 2;
                    
            end
            
            
            alltrials = cat(1,A{:,ref});
            
            FR = 1000* sum(alltrials,1) ;
            
            FR = FR(abs(reference{1,ref}) + timing(1) : abs(reference{1,ref}) + timing(2));
            
            Mbins = [timing(1) : Args.binSize : timing(2)];
            Mspikefr = zeros(length(Mbins),1);
            for bi = 1 : length(Mbins)-1
                Mspikefr(bi) = sum(sum(FR(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2),1)/(Args.binSize * size(alltrials,1));
                
            end
            
            
            time = [timing(1) : timing(2)];
            
            subplot(2,3,(r-1)*3+2)
            plot(Mbins,Mspikefr)
            xlim([time(1) time(end)])
            hold on
            plot(Mbins(1:length(psthstat.pval)),(psthstat.pval < Args.pThres).*Mspikefr(1:length(psthstat.pval)),'r*')
            grid on
            title(sprintf('%d trials',size(alltrials,1)))
            maxFR(r) = max(Mspikefr);
            %% evoked potential
            
            cd(pathf)
            cd ../../..
            cd lfp
            gr = str2num(lfpgr(7:end-1));
            load(sprintf('EP%sgr%04.0frule%d.mat',Args.epoch,gr,r))
            
            statfile = nptDir(sprintf('compEPRules%s*.mat',Args.epoch));
            if isempty(statfile)
                statfile = nptDir(sprintf('compEPRules.mat',Args.epoch));
            end
            EPstat = load(statfile(1).name);
            subplot(2,3,(r-1)*3+3)
           minl = min([length(EPstat.Mbins) length(EP)]);
            plot(EPstat.Mbins(1:minl),EP(1,1:minl))
           
            hold on
            plot(EPstat.Mbins(1:minl),EP(1,1:(1:minl)) + EP(2,1:(1:minl)),'--')
            plot(EPstat.Mbins(1:minl),EP(1,1:(1:minl)) - EP(2,1:(1:minl)),'--')
            xlim([EPstat.Mbins(1) EPstat.Mbins(end)])
            
            xstop = min([size(EPstat.pval,1) EPstat.Mbins(end)]);
            
            plot(EPstat.Mbins(1:minl),(EPstat.pval(1:minl,lfpCH) < Args.pThres)' .* EP(1,1:minl),'r*')
            maxtime(r) = EPstat.Mbins(end);
            limEP(r,1) = min(EP(1,:) - EP(2,:));
            limEP(r,2) = max(EP(1,:) + EP(2,:));
            cd(sdir)
            
            subplot(2,3,5); xlabel('Time around match onset (ms)');ylabel('Firing rate (sp/s)')
            subplot(2,3,6); xlabel('Time around match onset (ms)'); ylabel('Evoked potential (arb. unit)')
            
            subplot(2,3,2); ylabel('Firing rate (sp/s)')
            subplot(2,3,3); ylabel('Evoked potential (arb. unit)')
            
            
        end
        subplot(2,3,2); ylim([0 max(maxFR)]); subplot(2,3,5); ylim([0 max(maxFR)]);
        for sb = [3 6]; subplot(2,3,sb); ylim([min(limEP(:,1)) max(limEP(:,2))]); xlim([EPstat.Mbins(1) min(maxtime)]); end
        for r = 1 : 2
            for sb =  [3 6]; subplot(2,3,sb); line([maxtime(r)-100 maxtime(r)-100],[min(limEP(:,1)) max(limEP(:,2))],'LineStyle','--');end
            for sb =  [2 5]; subplot(2,3,sb); line([maxtime(r)-100 maxtime(r)-100],[0 max(maxFR)],'LineStyle','--');end
        end
end

set(gcf,'Name',[obj.data.setNames{ind} unitgr  'UA ' lfpgr 'lfp ' Args.epoch] )
% good example 648