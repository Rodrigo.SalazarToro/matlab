function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'unitcortex',[],'lfpcortex',[],'unitType',[],'unitGroup',[],'lfpGroup',[],'hist',[],'RulesDiff',[]);
Args.flags = {'Number','ObjectLevel','sameElec'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.day)
        rtemp1 = find(obj.data.Index(:,2) == Args.day);
    else
        rtemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.unitcortex)
        rtemp = intersect(find(obj.data.Index(:,3) == cell2mat(Args.unitcortex)),rtemp);
        
    end
    if ~isempty(Args.lfpcortex)
        rtemp = intersect(find(obj.data.Index(:,4) == cell2mat(Args.lfpcortex)),rtemp);
        
    end
    if ~isempty(Args.unitType)
        rtemp = intersect(find(obj.data.Index(:,7) == cell2mat(Args.unitType)),rtemp);
    end
    if ~isempty(Args.hist)
        rtemph = [];
        for nf = 1 : length(Args.hist)
            [sind,col] = find(obj.data.hist == Args.hist);
            rtemph = [rtemph; sind];
        end
        rtemp = intersect(rtemp,rtemph);
    end
    if ~isempty(Args.RulesDiff)
        inds = rtemp;
        for el = 1 : size(Args.RulesDiff,1)
            inds = intersect(inds,eval(sprintf('find(obj.data.RulesDiff%s(:,%d) %s)',Args.RulesDiff{el,1},Args.RulesDiff{el,2},Args.RulesDiff{el,3})));
            
        end
        rtemp = inds;
    end
    
    varargout{1} = rtemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

