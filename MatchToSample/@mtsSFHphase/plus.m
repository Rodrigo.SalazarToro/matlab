function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
    % check if second input is the right kind of object
    if(~isa(q,classname))
        % both inputs are not the right kind of object so create empty
        % object and return it
        r = feval(classname);
    else
        % second input is the right kind of object so return that
        r = q;
    end
else
    if(~isa(q,classname))
        % p is the right kind of object but q is not so just return p
        r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
    else
        % both p and q are the right kind of objects so add them
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        %         r.data.spiketimes = [p.data.spiketimes; q.data.spiketimes];
        % 		r.data.spiketrials = [p.data.spiketrials; q.data.spiketrials];
        
        r.data.hist = [p.data.hist; q.data.hist];
        r.data.sessions = [p.data.sessions; q.data.sessions];
        q.data.Index(:,5) = q.data.Index(:,5) + p.data.Index(end,5);
        q.data.Index(:,1) = q.data.Index(:,1) + p.data.Index(end,1);
        
        r.data.Index = [p.data.Index; q.data.Index];
        
        epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
        for ep = 1 : 5; eval(sprintf('r.data.RulesDiff%s = [p.data.RulesDiff%s; q.data.RulesDiff%s];',epochs{ep},epochs{ep},epochs{ep})); end
        
        
        % useful fields for most objects
        r.data.numSets = p.data.numSets + q.data.numSets;
        r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};
        
        % add nptdata objects as well
        r.nptdata = plus(p.nptdata,q.nptdata);
    end
end
