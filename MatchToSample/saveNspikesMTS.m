function saveNspikesMTS(varargin)
% to be run in the session dir

Args = struct('save',0,'redo',0,'downSR',200,'trialJitter',20,'types','epochs','minSpikeTrials',4,'minTrials',10,'rule',1,'plength',400);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

mtst = mtstrial('auto','redosetNames');

NeuroInfo = NeuronalChAssign;
ch = [];
for c = 1 : length(NeuroInfo.groups)
    tgroup = sprintf('group%04.f',NeuroInfo.groups(c));
    if ~isempty(nptDir(tgroup))
        
        ch = [ch c];
        
    end
end

Tr = mtsgetTrials(mtst,'rule',Args.rule,'stable','BehResp',1,modvarargin{:});
grs = nptDir('group0*'); groups = cell(length(grs),1); for g = 1 : length(grs); groups{g} = sprintf('%s/%s',pwd,grs(g).name); end
if ~isempty(Tr)
    
    for g = 1:length(groups)
        
        [modtrials,cluster] = checkForStability(groups{g});
        
        cd(groups{g})
        for cl = 1 : size(cluster,1)
            
            cd(cluster(cl).name)
            
            stableTrials = intersect(modtrials{cl},Tr);
            
            if ~isempty(stableTrials)  && length(find(ismember(stableTrials,Tr) == 1)) >= (length(Tr)-Args.trialJitter) % argument to get only units stable during the selected trials
                
                filename = sprintf('Nspikes%s%sRule%dEpoch.mat',grs(g).name,cluster(cl).name(end-2:end),Args.rule);
                
                if Args.redo || isempty(nptDir(filename))
                    clear period
                    load ispikes.mat
                    
                    Nspikes = zeros(4,1);
                    for t = vecr(Tr);
                        
                        periods(1) = round((mtst.data.CueOnset(t) - Args.plength));
                        periods(2) = round(mtst.data.CueOnset(t));
                        periods(3) = round(mtst.data.CueOffset(t));
                        periods(4) = round((mtst.data.MatchOnset(t) - Args.plength));
                        
                        for p = 1 : length(periods)
                            d = vecc(sp.data.trial(t).cluster.spikes);
                            ind = (d>=periods(p) & d<=periods(p)+Args.plength);
                            if length(ind) > Args.minSpikeTrials
                                
                                Nspikes(p) = Nspikes(p) + sum(ind);
                            end
                            
                        end
                        
                    end
                    
                    if Args.save
                        save(filename,'Nspikes','Tr')
                        fprintf('saving %s \n',filename)
                        
                    end
                    
                end
                
            end
            cd ..
        end
         cd ..
    end
   
end

