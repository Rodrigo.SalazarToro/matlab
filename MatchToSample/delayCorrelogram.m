function delayCorrelogram(varargin)

%computes average correlogram for delay period (200 - 800ms after sample offset) for each channel pair across all correct stable trials.
%computes the surrogate thresholds for the average correlograms

Args = struct('ml',0,'correlograms',0,'surrogate',0);
Args.flags = {'ml','correlograms','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.ml
    N = NeuronalHist('ml');
    [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end
chnumb = N.chnumb;


cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;

%get index of trials
lfpdata = nptDir('*_lfp2.*');

cd(sesdir)
numpairs = nchoosek(N.chnumb,2);
delay_correlograms = cell(1,numpairs);

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

if Args.ml
    rules = [1 2];
else
    rules = [1];
end

if Args.correlograms
    
    for rule = rules
        if Args.ml
            if rule == 1
                trials = identity;
            elseif rule == 2
                trials = location;
            end
        else
            trials = tr;
        end
        
        if ~isempty(trials)
            
            cd(lfp2dir)
            numtrials = size(trials,2);
            
            %only uses correct stable trials to calculate the avg correlograms
            %trials are locked to the sample off.
            %1000ms is the sample off.
            for x = trials
                load ([lfpdata(x).name],'normdata');
                
                %skip first 200ms of delay period (sample off activity)
                normdata = normdata(:,(1201:1800));
                for sp = sortedpairs
                    c1 = chpairlist(sp,1);
                    c2 = chpairlist(sp,2);
                    [c,lags] = xcorr(normdata(c1,:),normdata(c2,:),50,'coef');
                    
                    if isempty(delay_correlograms{sp})
                        delay_correlograms{sp} =  c;
                    else
                        delay_correlograms{sp} = delay_correlograms{sp} + c;
                    end
                end
            end
            
            %Find central peak
            delay_phase = cell(1,numpairs);
            delay_corrcoef = cell(1,numpairs);
            
            for www = sortedpairs
                %average correlograms by the number of trials
                delay_correlograms{www} = delay_correlograms{www} / numtrials;
                
                %find peak closest to zero and get corrcoef and phase
                cc = delay_correlograms{www};
                max_lag = size(delay_correlograms{www},2);
                %find index of central positive peak
                pp = findpeaks(cc);
                for ppp = 1 : size(pp.loc,1)
                    if (cc(pp.loc(ppp)) < 0)
                        %set to max phase index
                        pp.loc(ppp) = max_lag;
                    end
                end
                [~,pos_index] = (min(abs(lags(pp.loc))));
                %get real phase value by indexing the location in the lags
                pos_phase = lags(pp.loc(pos_index));
                pos_corrcoef = cc(pp.loc(pos_index));
                %find index of central negative peak (inverts correlogram)
                
                negcc = cc*-1;
                np = findpeaks(negcc);
                for nnn = 1 : size(np.loc,1)
                    if (negcc(np.loc(nnn)) < 0)
                        %set to max phase index
                        np.loc(nnn) = max_lag;
                    end
                end
                [~,neg_index] = (min(abs(lags(np.loc))));
                neg_phase = lags(np.loc(neg_index));
                neg_corrcoef = negcc(np.loc(neg_index));
                
                %determines if the closest peak to zero is positive or negative.
                %stores the correlation coef and lag position.
                if abs(pos_phase) <= abs(neg_phase)
                    delay_phase{www} = pos_phase;
                    delay_corrcoef{www} = pos_corrcoef;
                else
                    delay_phase{www} = neg_phase;
                    delay_corrcoef{www} = (-1 * neg_corrcoef); %make negative
                end
            end
            
            write_info = writeinfo(dbstack);
            if Args.ml
                if rule == 1;
                    mkdir('identity')
                    cd('identity')
                    save delay_correlograms delay_correlograms delay_phase delay_corrcoef write_info
                else
                    mkdir('location')
                    cd('location')
                    save delay_correlograms delay_correlograms delay_phase delay_corrcoef write_info
                end
            else
                save delay_correlograms delay_correlograms delay_phase delay_corrcoef write_info
            end
        end
    end
end

cd(sesdir)

%make surrogates (make average correlograms first)
% % % if Args.surrogate
% % %     cd([ddd filesep 'lfp2'])
% % %     load channel_matrix channel_matrix
% % %     %load delay_correlograms
% % %     iterations = 1000;
% % %         %avg_surrogates = cell(1,size(delay_correlograms,2));
% % %     for rule = rules
% % %         if Args.ml
% % %             r = r + 1;
% % %             if rule == 1;
% % %                 trials = identity;
% % %             else
% % %                 trials = location;
% % %             end
% % %         end
% % %         cd([ddd filesep 'lfp2'])
% % %         num_trials = size(trials,2);
% % %
% % %         pair = 0;
% % %         for cc1 = 1 : Args.chnumb
% % %             %loads the each channel
% % %             data1 = channel_matrix{1,cc1};
% % %             for cc2 = (cc1 + 1) : Args.chnumb
% % %                 data2 = channel_matrix{1,cc2};
% % %                 pair = pair + 1
% % %                 all_correlations = zeros(iterations,1);
% % %                 for iter = 1 : iterations
% % %                     x1 = zeros(1,num_trials);
% % %                     t = 0;
% % %                     while(t == 0)
% % %                         t1 = randperm(num_trials);
% % %                         t2 = randperm(num_trials);
% % %                         for check = 1: num_trials
% % %                             if t1(check) == t2(check)
% % %                                 t = 0;
% % %                                 break
% % %                             else
% % %                                 t = 1;
% % %                             end
% % %                         end
% % %                     end
% % %                     for permut = 1 : num_trials
% % %                         tt1 = trials(1,t1(1,permut));
% % %                         tt2 = trials(1,t2(1,permut));
% % %                         d1 = data1(tt1,:);
% % %                         d2 = data2(tt2,:);
% % %                         x1(1,permut) = xcorr(d1,d2,0,'coef');
% % %                     end
% % %                     all_correlations(iter,:) = mean(x1);
% % %                 end
% % %                 delay_surrogates{pair} = all_correlations;
% % %             end
% % %         end
% % %
% % %         if Args.ml
% % %             if rule == 1;
% % %                 mkdir('identity')
% % %                 cd('identity')
% % %                 save delay_surrogates delay_surrogates
% % %             else
% % %                 mkdir('location')
% % %                 cd('location')
% % %                 save delay_surrogates delay_surrogates
% % %             end
% % %         else
% % %             save delay_surrogates delay_surrogatescoef
% % %         end
% % %     end
% % % end

fprintf(1,'\n')
fprintf(1,'Delay Correlograms and Surrogates Done.\n')


