function sliding_window_thresholds(varargin)

%computes the sliding window thresholds for each pair

Args = struct('ml',0,'ide_only',0);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    trials_identity = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1)';
    trials_location = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',2)';
    N = NeuronalHist('ml');
    lfp2_dirs = ['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
    num_lfp2 = [1 2];
    if Args.ide_only
        lfp2_dirs = ['identity']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
        num_lfp2 = [1];
    end
    [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
else
    mt=mtstrial('auto','redosetNames');
    trials_identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1)';
    trials_location = mtsgetTrials(mt,'BehResp',1,'stable','rule',2)';
    N = NeuronalHist;
    lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
    num_lfp2 = [1 2];
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end

chnumb = N.chnumb;
pairs = nchoosek(chnumb,2);
trials_all = [{trials_identity'} {trials_location'}];

for ide_loc = num_lfp2
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
    
    %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
    % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
    lfpdata = nptDir('*_lfp2.*');
    load ([lfp2dir filesep lfpdata(1).name],'data');
    numchannels = size(data,1);
   
    if isempty(trials_all{ide_loc})  %if there are no trials then do not run threshold calculation
        if ide_loc == 1
            fprintf(1,'No identity trials\n')
        else
            fprintf(1,'No location trials\n')
        end
    else
        rr_surrogate=cell(1,pairs);
        for pair = sortedpairs
            ii = chpairlist(pair,1);
            jj = chpairlist(pair,2);
            
            fprintf('\n%0.5g     ',ii)
            fprintf(' %0.5g',jj)
            %concatenate all trials for each channel
            data1 = [];
            data2 = [];
            for x = trials_all{ide_loc}
                load ([cd filesep lfpdata(x).name],'data');
                data1 = [data1 single(data(ii,:))];
                data2 = [data2 single(data(jj,:))];
            end
            
            if Args.ml
                [r_surrogate posneg] = randxcorr_peaks('x',data1,'y',data2,'pair_number',pair,'ml','mts_obj',mt,'ide_loc',ide_loc);
            else
                [r_surrogate posneg] = randxcorr_peaks('x',data1,'y',data2,'pair_number',pair);
            end
            
            rr_surrogate{pair} = r_surrogate;
            pn(pair) = posneg; %this is used to determine if positive or negative peaks were searched for
            
        end
        threshold = cell(1,pairs);
        for p = sortedpairs
            %Loads 10000 samples for each pairwise combination
            pp = rr_surrogate{p};
            if pn(p) == 1
                threshold{p} = prctile(pp,[95 99 99.9 99.99]);
            elseif pn(p) == -1
                tp = prctile(pp,[5 1 .1 .01]) * -1; %this makes the values all positive
                threshold{p} = tp;
            end
        end
        
        if Args.ml
            cd([lfp2dir filesep lfp2_dirs(ide_loc,:)])
        end
        write_info = writeinfo(dbstack);
        save threshold threshold write_info
    end
end

cd(sesdir)




