function epoch_correlograms_GS(varargin)

%run at session level

Args = struct('ml',0,'rule',1,'iterations',10000,'monkey','clark');
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));

N = NeuronalHist;
chnumb = N.chnumb;

if Args.ml
    mt = mtstrial('auto','ML','RTfromML');
    trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',Args.rule);
else
    mt = mtstrial('auto');
    trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',Args.rule);
end

cue_on = floor(mt.data.CueOnset);
cue_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

num_trials = size(trials,2);

all_iterations1 = zeros(Args.iterations,num_trials);
all_iterations2 = zeros(Args.iterations,num_trials);

cd(['lfp' filesep 'lfp2'])
lfpdata = nptDir([Args.monkey '*']);
%calculate iterations
for it = 1 : Args.iterations
    t = 0;
    while(t == 0)
        t1 = randperm(num_trials);
        t2 = randperm(num_trials);
        for check = 1: num_trials
            if t1(check) == t2(check)
                t = 0;
                break
            else
                t = 1;
            end
        end
    end
    all_iterations1(it,:) = t1;
    all_iterations2(it,:) = t2;
end

e_surrogates1 = zeros(num_trials,1);
e_surrogates2 = zeros(num_trials,1);
e_surrogates3 = zeros(num_trials,1);
e_surrogates4 = zeros(num_trials,1);


for iter = 1 : Args.iterations
    if rem(iter,25) ==0;fprintf(1,'\n');end
    fprintf(1,[num2str(iter) ' '])
    t1 = all_iterations1(iter,:);
    t2 = all_iterations2(iter,:);
    
    for permut = 1 : num_trials
        
        %randomize pair of channels on for each trial
        rch = randperm(chnumb);
        cc1 = rch(1);
        cc2 = rch(2);
        
        tt1 = trials(1,t1(1,permut));
        tt2 = trials(1,t2(1,permut));
        
        nd1 = load ([lfpdata(tt1).name],'normdata');
        normdata1 = nd1.normdata;
        
        nd2 = load ([lfpdata(tt2).name],'normdata');
        normdata2 = nd2.normdata;
        
        e_surrogates1(permut,1) = xcorr(normdata1(cc1,((cue_on(tt1)-400):cue_on(tt1))),normdata2(cc2,((cue_on(tt2)-400):cue_on(tt2))),0,'coef');
        e_surrogates2(permut,1) = xcorr(normdata1(cc1,(cue_off(tt1)-400:cue_off(tt1))),normdata2(cc2,(cue_off(tt2)-400:cue_off(tt2))),0,'coef');
        e_surrogates3(permut,1) = xcorr(normdata1(cc1,(cue_off(tt1):(cue_off(tt1)+400))),normdata2(cc2,(cue_off(tt2):(cue_off(tt2)+400))),0,'coef');
        e_surrogates4(permut,1) = xcorr(normdata1(cc1,((match(tt1)-400):(match(tt1)))),normdata2(cc2,((match(tt2)-400):(match(tt2)))),0,'coef');
    end
    
    %average for for each iteration at 4 epochs
    epoch1(iter) = mean(e_surrogates1(:,1));
    epoch2(iter) = mean(e_surrogates2(:,1));
    epoch3(iter) = mean(e_surrogates3(:,1));
    epoch4(iter) = mean(e_surrogates4(:,1));
end





write_info = writeinfo(dbstack);
if Args.ml
    if Args.rule == 1;
        cd('identity')
        save globoal_epoch_surrogates epoch1 epoch2 epoch3 epoch4 write_info
        cd ..
    elseif Args.rule == 2
        cd('location')
        save global_epoch_surrogates epoch1 epoch2 epoch3 epoch4 write_info
        cd ..
    end
else
    save global_epoch_surrogates epoch1 epoch2 epoch3 epoch4 write_info
end


