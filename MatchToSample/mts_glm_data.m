function mts_glm_data(varargin)

%run at day level
%runs glm_delay
%uses logistic regression to model delay period and fixation period

%epoch is passed to glm_delay
%   epoch == 0 (delay period)
%   epoch == 1 (fixation period)

%R code is matlab_glm_code_full

%Args.model: 9stims(1),location(2),identity(3)
%Args.rules: used for ml data. 'rules',[1] runs identity only

Args = struct('ml',0,'epoch',0,'sessions',[1],'model',1,'rules',[1],'perm',0,'train',0,'threshold',1,'block',[],'sorted',0);
Args.flags = {'ml','perm','train','sorted'};
[Args,modvarargin] = getOptArgs(varargin,Args);
rules = [{'identity' 'location'}];
daydir = pwd;
ses = nptDir('session0*');
RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
for sessions = Args.sessions
    %get sessions   ses.name
    cd ([daydir filesep ses(sessions).name]);
    sesdir = pwd;
    [NeuroInfo] = NeuronalChAssign;
    %get actual group number
    groups = NeuroInfo.groups;
    if ~Args.ml
        groups = 1:size(groups,2);
    end
    
    if Args.ml
        c = mtscpp('auto','ml','ide_only');
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    else
        c = mtscpp('auto');
        mt = mtstrial('auto','redosetNames');
    end
    
    if Args.sorted
        [i ii] = get(c,'Number','sorted'); %get list of sorted pairs
    else
        [i ii] = get(c,'Number');
    end
    
    %Get number of channels
    chnumb=length(NeuroInfo.channels);
    if ~Args.ml
        r=1;
    else
        r = Args.rules; %[1], ide only, [1,2] runs both rules
    end
    cd (['lfp' filesep 'lfp2']);
    lfp2 = pwd;
    %run for both rules if ML
    for rr = r
        pair = 0;
        counter = 0;
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                column = 0;
                pair = pair + 1;
                if intersect(pair, ii);
                    counter = counter + 1;
                    for br = 1%[1 0]   %behavioral response (correct, incorrect)
                        if br == 1
                            res = 'correct';
                        else
                            res = 'incorrect';
                        end
                        for stability = 1  %transition or stable (stable, with transition option)
                            if stability == 0
                                tss1= 'transition';
                            else
                                tss1= 'stable';
                            end
                            sessionname = 'channelpair12.mat';
                            thresh = Args.threshold;
                            cd(lfp2)
                            if Args.ml && ~Args.perm && ~Args.train
                                %Args.rule = 1 for identity and 2 for location
                                [glm_output] = cppStimuli_glm('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'ml',Args.ml,'rule',rr,'pair',pair,'model',Args.model);
                            elseif Args.perm
                                if counter == 1;
                                    %need to make the permutation list here so
                                    %that all of the pairs has the same set of
                                    %permutations. Also need to save the
                                    %permutation list.
                                    cd(sesdir)
                                    nperms = 3;
                                    for np = 1 : nperms
                                        if isempty(Args.block)
                                            ind_alltrials = mtsgetTrials(mt,'BehResp',br,tss1,'ml','rule',rr);
                                        else
                                            %determine number of blocks
                                            bdata = find(diff(mt.data.Index(:,1)))';
                                            nblocks = size(bdata,2) + 1;
                                            %determine block order
                                            bdata(:) = bdata(:) + 1;
                                            b_order = mt.data.Index([1,bdata],1);
                                            
                                            a_block = find(b_order == rr);
                                            block_num = a_block(Args.block);
                                            ind_alltrials = mtsgetTrials(mt,'BehResp',br,tss1,'ml','rule',rr,'block',block_num);
                                        end
                                        randtrials = randperm(size(ind_alltrials,2));
                                        rand_ind_alltrials(np,:) = ind_alltrials(randtrials);
                                        
                                        
                                        %determine trials to exclude
                                        numb_trials = size(rand_ind_alltrials,2);
                                        
                                        ex = round(numb_trials * .7);
                                        r = randperm(numb_trials);
                                        hold_trials(np,:)  = r(ex:end);
                                    end
                                    cd(lfp2)
                                    save rand_ind_alltrials rand_ind_alltrials hold_trials
                                    cd(sesdir)
                                end
                                cd(lfp2)
                                if isempty(Args.block)
                                    [glm_output] = cppStimuli_glm_permutation('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'ml',Args.ml,'rule',rr,'pair',pair,'model',Args.model,'num_perms',nperms,'perm_list',rand_ind_alltrials,'hold_trials',hold_trials);
                                else
                                    [glm_output] = cppStimuli_glm_permutation('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'ml',Args.ml,'rule',rr,'pair',pair,'model',Args.model,'block',Args.block,'num_perms',nperms,'perm_list',rand_ind_alltrials,'hold_trials',hold_trials);
                                end
                            elseif Args.train
                                %set to only use the first block
                                if isempty(Args.block)
                                    [glm_output] = cppStimuli_glm_train('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'ml',Args.ml,'rule',rr,'pair',pair,'model',Args.model);
                                else
                                    if Args.ml
                                        [glm_output] = cppStimuli_glm_train('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'ml',Args.ml,'rule',rr,'pair',pair,'model',Args.model,'block',Args.block);
                                    else
                                        [glm_output] = cppStimuli_glm_train('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'pair',pair,'model',Args.model,'rule',rr,'block',Args.block);
                                    end
                                end
                            else
                                [glm_output] = cppStimuli_glm('b1',br,'ts1',tss1,'c1',c1,'c2',c2,'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',Args.epoch,'pair',pair,'model',Args.model);
                            end
                            column = column + 1;
                            if Args.perm
                                glm_perm{pair,column} = glm_output;
                            else
                                if Args.epoch == 0
                                    glm_delay{pair,column} = glm_output;
                                elseif Args.epoch == 1
                                    glm_fix{pair,column} = glm_output;
                                end
                            end
                        end
                    end
                    
                else
                    column = column + 1;
                    if Args.perm
                        glm_perm{pair,column} ={};
                    else
                        if Args.epoch == 0
                            glm_delay{pair,column} = {};
                        elseif Args.epoch == 1
                            glm_fix{pair,column} = {};
                        end
                    end
                end
            end
        end
        cd(lfp2)
        
        if Args.ml
            cd(rules{rr})
        end
        
        if Args.train
            if Args.epoch == 0 %delay period
                if Args.model == 1
                    glm_delay_train = glm_delay;
                    save glm_delay_train glm_delay_train
                elseif Args.model == 2
                    glm_delay_loc_train = glm_delay;
                    save glm_delay_loc_train glm_delay_loc_train
                elseif Args.model == 3
                    glm_delay_obj_train = glm_delay;
                    save glm_delay_obj_train glm_delay_obj_train
                end
            elseif Args.epoch == 1 %fixation period
                if Args.model == 1
                    glm_fix_train = glm_fix;
                    save glm_fix_train glm_fix_train
                elseif Args.model == 2
                    glm_fix_loc_train = glm_fix;
                    save glm_fix_loc_train glm_fix_loc_train
                elseif Args.model == 3
                    glm_fix_obj_train = glm_fix;
                    save glm_fix_obj_train glm_fix_obj_train
                end
            end
        elseif Args.perm
            if Args.epoch == 0
                glm_delay_perm = glm_perm;
                save glm_delay_perm glm_delay_perm
            elseif Args.epoch == 1
                glm_fix_perm = glm_perm;
                save glm_fix_perm glm_fix_perm
            end
        else
            if Args.epoch == 0 %delay period
                if Args.model == 1
                    save glm_delay glm_delay
                elseif Args.model == 2
                    glm_delay_loc = glm_delay;
                    save glm_delay_loc glm_delay_loc
                elseif Args.model == 3
                    glm_delay_obj = glm_delay;
                    save glm_delay_obj glm_delay_obj
                end
            elseif Args.epoch == 1 %fixation period
                if Args.model == 1
                    save glm_fix glm_fix
                elseif Args.model == 2
                    glm_fix_loc = glm_fix;
                    save glm_fix_loc glm_fix_loc
                elseif Args.model == 3
                    glm_fix_obj = glm_fix;
                    save glm_fix_obj glm_fix_obj
                end
            end
        end
    end
end

cd(daydir)

