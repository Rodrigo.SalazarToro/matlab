function compare_var_fix_delay

load switchdays

time(:,1) = [1:17]';

numdays = size(switchdays,2);
monkeydir = pwd;
p=0;
for x = 1:numdays
    cd(monkeydir)
    
    cd(switchdays{x})
    
    cd(['session02' filesep 'lfp' filesep 'lfp2'])
    
    load glm_fix glm_fix
    load glm_delay glm_delay
    
    numpairs = size(glm_fix,1);
    
    for y = 1:numpairs
        p = p + 1;
        
        %delay
        coefs = str2double(glm_delay(y,1).interaction_coefs);
        
        res = [];
        res(:,1) = coefs(1) + time*coefs(2);
        res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
        res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
        res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
        res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
        res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
        res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
        res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
        res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
        
        delay(p) = var(diff(res((1:2),:)))
        
        
        
        
        %fix
        coefs= str2double(glm_fix(y,1).interaction_coefs);
        
        res = [];
        res(:,1) = coefs(1) + time*coefs(2);
        res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
        res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
        res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
        res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
        res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
        res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
        res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
        res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
    
        fix(p) = var(diff(res((1:2),:)));
        
    end
    
end

cd(monkeydir)
save fixVSdelay fix delay