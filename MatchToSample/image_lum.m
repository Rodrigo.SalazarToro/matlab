function im2 = image_lum(varargin)

%changes total luminance of image
% takes image and changes total luminance to total_lum OR (total_lum * percentage of non-zero pixes)
% image: image to convert
% total_lum: new luminance (100000-10000000)
% var: amount of variance between real lum and new lum (1000 - 10000)
% note: var may need to be increased in order for loop to converge
% norm: account for area(1), set new luminance to total_lum(0)
%
%old gray scale: .2989*R + 0.5870*G + 0.1140*B   REALLY OLD!

%new gray scale: .2126*R + .7152*G + .0722*B

%calculated from monitor: .2272*R + .7014*G + .0714*B

%800000 has been considered 100% luminance

Args = struct('image',[],'total_lum',[],'var',1000,'norm',1);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

pic_dir = pwd;
write_pic_dir = (['/home/ndotson/Desktop/ml_images/iso_' num2str(Args.total_lum) filesep]);
mkdir(write_pic_dir)
percent_lum = (Args.total_lum / 100) * 800000;

%adjusted based on monitor in use
r_coef = .2272;
g_coef = .7014;
b_coef = .0714;

if isempty(Args.image)
    all_images = nptdir('*.jpg');
    num_images = size(all_images,1);
else
    num_images = size(Args.image,2);
end

for ni = 1 : num_images
    
    cd(pic_dir)
    
    if isempty(Args.image)
        image_name = all_images(ni).name;
    else
        image_name = Args.image{ni};
    end
    
    im = imread(image_name);
    
    imagex = size(im,1);
    imagey = size(im,2);
    
    imr = im(:,:,1);
    total_r = sum(sum(imr));
    img = im(:,:,2);
    total_g = sum(sum(img));
    imb = im(:,:,3);
    total_b = sum(sum(imb));
    
    % account for area
    if Args.norm
        percent_area = size(find(im),1) / (imagex*imagey*3);
    else
        percent_area = 1;
    end
    
    total_lum = percent_lum * percent_area;
    
    
    %imgray = rgb2gray(im);
    imgray = (im(:,:,1)*r_coef) + (im(:,:,2)*g_coef) + (im(:,:,3)*b_coef );
    gray = sum(sum(imgray))

    new = 0;
    lum = round(total_lum)
    
    im2 = im/(gray/total_lum);
    
    imgray2 = (im2(:,:,1)*r_coef) + (im2(:,:,2)*g_coef) + (im2(:,:,3)*b_coef );
    gray2 = sum(sum(imgray2))
    
    
    
    cd(write_pic_dir)
    
    %num2str((Args.total_lum / 800000)*100)
    
    imwrite(im2,[image_name(1:(end-4)) '_' num2str(Args.total_lum) '.jpg'])
end
cd(pic_dir)

