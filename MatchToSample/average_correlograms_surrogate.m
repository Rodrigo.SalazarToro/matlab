function average_correlograms_surrogate(varargin)

%computes average correlogram surrogates for 7 different time periods
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

Args = struct('ml',0,'iterations',1000);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));

sesdir = pwd;
if Args.ml
    N = NeuronalHist('ml');
    [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end
chnumb = N.chnumb;

cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;

%get index of trials
lfpdata = nptDir('*_lfp2.*');

cd(sesdir)
numpairs = nchoosek(N.chnumb,2);
all_correlograms = cell(7,numpairs); %seven combinations
average_correlograms_surrogates = cell(7,numpairs); %seven combinations
if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)

sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if Args.ml
    rules = [1 2];
else
    rules = [1];
end

for rule = rules
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        
        cd(lfp2dir)
        numtrials = size(trials,2);
        for iter = 1 : Args.iterations
            iter
            r1 = randperm(numtrials);
            tr1 = trials(r1);
            
            r2 = randperm(numtrials);
            tr2 = trials(r2);
            
            tcounter = 0;
            for x = 1 : numtrials
                tcounter = tcounter + 1;
                
                %data1
                load([lfpdata(tr1(x)).name],'data');
                
                s_on = sample_on(tr1(x));
                s_off = sample_off(tr1(x));
                m = match(tr1(x));
                
                fix1 = data(:,[(s_on - 399) : (s_on)]);
                sample1 = data(:,[(s_off - 399) : (s_off)]);
                delay4001 = data(:,[(s_off) : (s_off + 399)]);
                delay8001 = data(:,[(s_off + 401) : (s_off + 800)]);
                delay1 = data(:,[(s_off + 201) : (s_off + 800)]);
                delaymatch1 = data(:,[(m - 399) : (m)]);
                fulltrial1 = data(:,[(s_on - 500) : (m)]);
                
                %data2
                load([lfpdata(tr2(x)).name],'data');
                s_on = sample_on(tr2(x));
                s_off = sample_off(tr2(x));
                m = match(tr2(x));
                
                fix2 = data(:,[(s_on - 399) : (s_on)]);
                sample2 = data(:,[(s_off - 399) : (s_off)]);
                delay4002 = data(:,[(s_off) : (s_off + 399)]);
                delay8002 = data(:,[(s_off + 401) : (s_off + 800)]);
                delay2 = data(:,[(s_off + 201) : (s_off + 800)]);
                delaymatch2 = data(:,[(m - 399) : (m)]);
                fulltrial2 = data(:,[(s_on - 500) : (m)]);
                
                %full trials are going to be different so make one shorter
                f1s = size(fulltrial1,2);
                f2s = size(fulltrial2,2);
                if f1s > f2s 
                    fulltrial1 = fulltrial1(:,(1:f2s));
                else
                    fulltrial2 = fulltrial2(:,(1:f1s));
                end
                
                for sp = sortedpairs
                    c1 = chpairlist(sp,1);
                    c2 = chpairlist(sp,2);
                    
                    [f] = xcorr(fix1(c1,:),fix2(c2,:),0,'coef');
                    [s] = xcorr(sample1(c1,:),sample2(c2,:),0,'coef');
                    [d400] = xcorr(delay4001(c1,:),delay4002(c2,:),0,'coef');
                    [d800] = xcorr(delay8001(c1,:),delay8002(c2,:),0,'coef');
                    [d] = xcorr(delay1(c1,:),delay2(c2,:),0,'coef');
                    [dm] = xcorr(delaymatch1(c1,:),delaymatch2(c2,:),0,'coef');
                    [full] = xcorr(fulltrial1(c1,:),fulltrial2(c2,:),0,'coef');
                    
                    all_correlograms{1,sp}(tcounter) =  f;
                    all_correlograms{2,sp}(tcounter) =  s;
                    all_correlograms{3,sp}(tcounter) =  d400;
                    all_correlograms{4,sp}(tcounter) =  d800;
                    all_correlograms{5,sp}(tcounter) =  d;
                    all_correlograms{6,sp}(tcounter) =  dm;
                    all_correlograms{7,sp}(tcounter) =  full;
                end
            end
            for sp = sortedpairs
                average_correlograms_surrogates{1,sp}(iter) = mean(all_correlograms{1,sp});
                average_correlograms_surrogates{2,sp}(iter) = mean(all_correlograms{2,sp});
                average_correlograms_surrogates{3,sp}(iter) = mean(all_correlograms{3,sp});
                average_correlograms_surrogates{4,sp}(iter) = mean(all_correlograms{4,sp});
                average_correlograms_surrogates{5,sp}(iter) = mean(all_correlograms{5,sp});
                average_correlograms_surrogates{6,sp}(iter) = mean(all_correlograms{6,sp});
                average_correlograms_surrogates{7,sp}(iter) = mean(all_correlograms{7,sp});
            end
        end

        
        write_info = writeinfo(dbstack);
        if Args.ml
            if rule == 1;
                mkdir('identity')
                cd('identity')
                save average_correlograms_surrogate average_correlograms_surrogates write_info
            else
                mkdir('location')
                cd('location')
                save average_correlograms_surrogate average_correlograms_surrogates write_info
            end
        else
            save save average_correlograms_surrogate average_correlograms_surrogates write_info
        end
    end
end


cd(sesdir)

fprintf(1,'\n')
fprintf(1,'Correlogram Surrogates Done.\n')


