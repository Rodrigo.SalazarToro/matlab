function bmf_epoch_correlograms_figure

cd('/media/bmf_raid/data/monkey/ethyl/')
load alldays
monkeydir = pwd;
all_phase_angles = [];
all_time_lags_pos = [];
all_time_lags_neg = [];

i = imread('ethyl_brainsketch.jpg');

%bmf grid xy
xy = [1141,330;
    1141,290;
    1141,250;
    1141,230;
    1141,205;
    1105,395;
    1105,340;
    1105,290;
    1105,245;
    1100,215;
    1100,190;
    1100,170;
    1065,410;
    1065,355;
    1065,305;
    1065,260;
    1065,225;
    1063,198;
    1063,170;
    1063,143;
    1028,438;
    1028,375;
    1028,330;
    1028,288;
    1025,250;
    1025,215;
    1025,185;
    1025,150;
    1025,120;
    988,478;
    988,415;
    988,358;
    988,315;
    990,272;
    988,232;
    988,198;
    987,162;
    986,136;
    986,105;
    950,460;
    950,400;
    950,348;
    950,300;
    950,260;
    950,218;
    950,175;
    950,145;
    947,118;
    945,92;
    910,518;
    910,450;
    910,390;
    910,338;
    910,290;
    910,245;
    910,200;
    910,162;
    910,125;
    910,100;
    910,70;
    871,505;
    871,442;
    871,382;
    872,330;
    872,282;
    871,238;
    871,195;
    871,153;
    871,120;
    870,87;
    870,57;
    831,493;
    831,430;
    831,373;
    831,325;
    832,278;
    832,230;
    832,185;
    832,150;
    832,115;
    831,81;
    830,50;
    795,480;
    795,420;
    795,363;
    794,317;
    794,270;
    793,222;
    793,182;
    793,142;
    793,108;
    793,78;
    791,48;
    754,476;
    755,410;
    755,355;
    755,306;
    755,260;
    755,215;
    755,175;
    754,135;
    753,100;
    753,70;
    751,40;
    713,472;
    713,412;
    714,358;
    714,308;
    714,260;
    715,215;
    715,175;
    714,137;
    714,100;
    713,70;
    712,38;
    675,475;
    675,415;
    675,361;
    675,310;
    675,262;
    675,218;
    675,176;
    675,138;
    675,102;
    674,68;
    674,36;
    635,475;
    635,418;
    636,362;
    636,313;
    636,269;
    636,222;
    635,179;
    635,138;
    635,102;
    635,68;
    635,36;
    595,480;
    595,418;
    595,362;
    595,313;
    595,269;
    597,224;
    597,179;
    597,138;
    596,100;
    594,66;
    594,36;
    558,486;
    556,425;
    557,368;
    557,315;
    557,267;
    558,220;
    558,178;
    557,138;
    557,100;
    557,66;
    557,35;
    517,495;
    517,433;
    518,369;
    518,312;
    518,263;
    518,218;
    518,178;
    518,142;
    517,105;
    517,66;
    516,34;
    478,437;
    478,375;
    484,313;
    480,270;
    480,225;
    478,182;
    478,142;
    478,105;
    476,68;
    476,37;
    440,440;
    438,390;
    438,340;
    438,285;
    438,235;
    438,190;
    438,145;
    438,110;
    438,73;
    437,40;
    400,453;
    400,403;
    400,353;
    398,295;
    401,244;
    398,195;
    398,152;
    398,114;
    398,78;
    397,46;
    360,415;
    360,358;
    360,300;
    360,250;
    360,200;
    360,160;
    358,120;
    358,85;
    358,50;
    320,430;
    320,368;
    320,310;
    320,260;
    320,210;
    322,168;
    320,128;
    320,90;
    318,60;
    280,375;
    280,320;
    280,270;
    280,223;
    280,183;
    280,138;
    280,103;
    278,69;
    240,385;
    242,332;
    241,279;
    241,231;
    240,188;
    240,150;
    240,118;
    240,85;
    200,348;
    200,270;
    200,253;
    200,210;
    200,170;
    200,135;
    200,102;
    160,311;
    160,270;
    160,225;
    160,188;
    160,154;
    160,122;
    122,285;
    121,242;
    121,205;
    120,172;
    120,145;
    84,252;
    84,223;
    84,197;
    84,177];


% hh = jet;
% h(:,:,1) = hh(:,1);h(:,:,2) = hh(:,2);h(:,:,3) = hh(:,3);
lc = linspace(0,180,64);

all_freq = [];
all_phase = [];
all_phase1 = [];
all_phase2 = [];
for x = 18%1:25%18
    figure
    imshow(i);hold on;
    
    cd([monkeydir filesep alldays{x} filesep 'session01'])
    sesdir = pwd;
    
    [~,~,gg,~,sortedpairs] = bmf_groups;
    cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'identity'])
    
    load all_gabors
    
    counter = 0;
    for n = sortedpairs
        counter = counter + 1;
        a = all_gabors{3,n};
        
        a1 = all_gabors{4,n};
        a2 = all_gabors{4,n};
        
        if a1.cross_corr >= .99 && abs(a1.peak) > .05 && a2.cross_corr >= .99 && abs(a2.peak) > .05 %a.cross_corr >= .99 && abs(a.peak) > .1
            n
            all_freq = [all_freq a.frequency];
            phase = abs(a.phase_angle_deg);
            real_phase = a.phase_angle_deg
            all_phase = [all_phase phase];
            
            all_phase1 = [all_phase1 abs(a1.phase_angle_deg)];
            all_phase2 = [all_phase2 abs(a2.phase_angle_deg)];
            
            if abs((abs(a1.phase_angle_deg)) - abs(a2.phase_angle_deg)) > 45
                plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',[0 0 0],'LineWidth',2)
            end
            
            
            %[~,cc] = min(abs(lc - phase));
            if phase < 45
                %plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',hh(cc,:),'LineWidth',2)
%                 plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',[0 1 0],'LineWidth',2)
            elseif phase < 90
%                 plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',[0 .5 1],'LineWidth',2)
            elseif phase < 135 
                plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',[1 .5 0],'LineWidth',2)
            else
%                 plot([xy(gg(counter,1),1) xy(gg(counter,2),1)],[xy(gg(counter,1),2) xy(gg(counter,2),2)],'color',[1 0 0],'LineWidth',2)
            end
            scatter(xy(gg(counter,1),1),xy(gg(counter,1),2),60,[0 0 0],'fill');hold on
            scatter(xy(gg(counter,2),1),xy(gg(counter,2),2),60,[0 0 0],'fill');hold on
        end
    end
end
% figure
% subplot(1,2,1)
% hist(abs(all_phase),50)
% axis tight
% subplot(1,2,2)
% hist(all_freq,50)
% axis tight

% figure;hist(all_time_lags_pos)
% figure;hist(all_time_lags_neg)
    figure
    subplot(1,2,1)
    hist(all_phase1,10)
    axis tight
    axis square
    subplot(1,2,2)
scatter(all_phase1,all_phase2,'s','fill')
axis([0 180 0 180])
hold on
plot([0 180],[0 180],'k')
axis square



cd(monkeydir)
