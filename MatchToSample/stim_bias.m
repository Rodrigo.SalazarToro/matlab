function stim_bias

b = bhv_read;

%moving window size
window = 50;

%get condition number
conditions = unique(b.ConditionNumber)';
num_conditions = size(conditions,2);
correct = find(b.TrialError == 0);
incorrect = find(b.TrialError == 6);

num_images = size(b.Stimuli.PIC,2);

total_correct_incorrect = size(correct,1) + size(incorrect,1);

fprintf(1,['total number of correct and incorrect trials = ' num2str(total_correct_incorrect) '\n'])

total_trials = size(b.TrialNumber,1);

figure
p = 0;
cond = 0;
for x = conditions
    cond = cond + 1;
    for to = 1:3
        p = p + 1;
        im = b.TaskObject{x,(to+1)};
        
        commas = strfind(im,',');
        parenth = strfind(im,')');
        parenth_start= strfind(im,'(');

        loc1 = str2double(im((commas(1)+1) : (commas(2)-1)));
        loc2 = im((commas(2)+1) : (parenth(1)-1));
        
        if (loc1 == 4)
            right = 1; %indicates the image is presented on the right
        else
            right = 0;
        end
        
        name = im((parenth_start(1)+1) : (commas(1)-1));
        if to == 1;
            sample = name;
        end
        
        if to == 2;
            if strmatch(sample,name) && right
                cond_match(cond) = 1;
            else
                cond_match(cond) = 0;
            end
        end
        
        for nm = 1 : num_images
            
            if strmatch(name,b.Stimuli.PIC(nm).Name)
                pp = nm;
                break
            end
            
        end
        
        if to == 1
            subplot(num_conditions,3,p)
            imshow(b.Stimuli.PIC(pp).Data)
            hold on
        else
            if (right) && (to == 2)
                subplot(num_conditions,3,(p+1))
                imshow(b.Stimuli.PIC(pp).Data)
                hold on
            elseif (~right) && (to == 2)
                subplot(num_conditions,3,p)
                imshow(b.Stimuli.PIC(pp).Data)
                hold on
            end
            
            if (~right) && (to == 3)
                subplot(num_conditions,3,(p-1))
                imshow(b.Stimuli.PIC(pp).Data)
                hold on
            elseif (right) && (to == 3)
                subplot(num_conditions,3,p)
                imshow(b.Stimuli.PIC(pp).Data)
                hold on
            end
        end
    end
end


for tt = 1:total_trials
    for nc = 1 : num_conditions
        trials_cond = find(b.ConditionNumber((1:tt),1) == conditions(nc));
        num_correct = size(intersect(trials_cond,correct),1);
        num_incorrect = size(intersect(trials_cond,incorrect),1);
        
        all_performance(nc,tt) = (num_correct / (num_correct + num_incorrect));
        all_conditions(nc,tt) = size(trials_cond,1) / tt;
        
        
        
    end
end






%make bar chart for percent correct on each condition
for nc = 1 : num_conditions
    trials_cond = find(b.ConditionNumber == conditions(nc));
    
    index_correct = intersect(trials_cond,correct);
    index_incorrect = intersect(trials_cond,incorrect);
    
    num_correct = size(intersect(trials_cond,correct),1);
    num_incorrect = size(intersect(trials_cond,incorrect),1);
    sdata(nc) = (num_correct / (num_correct + num_incorrect)) * 100;
    
    correct_totals(nc) = num_correct;
    
    trial_totals(nc) = num_correct + num_incorrect;
    count = 0;
    
    counter = 0;
    for ttt = (window+1) : (total_trials - window)
        counter = counter + 1;
        
        movwindow = [(ttt-window):(ttt+window)];
        
        i_correct = size(intersect(index_correct,movwindow),2);
        i_incorrect = size(intersect(index_incorrect,movwindow),2); 
        
        moving_perform(nc,counter) = i_correct / (i_correct + i_incorrect);
        moving_condition(nc,counter) = size(intersect(trials_cond,movwindow),2) / size(movwindow,2);
        
    end
    
end

figure
for y = 1 : num_conditions
    subplot(3,4,y)
    plot(moving_perform(y,:),'g')
    hold on
    plot(moving_condition(y,:))
    hold on
    axis([0 size(moving_perform,2) 0 1])
end








match_loc(1) = (sum(correct_totals(find(cond_match == 0))) / sum(trial_totals(find(cond_match == 0)))) * 100;
match_loc(2) = (sum(correct_totals(find(cond_match))) / sum(trial_totals(find(cond_match)))) * 100; %MATCH IS ON RIGHT


figure
bar(sdata)
axis([0 (nc+1) 0 100])

figure
bar(match_loc)
axis([0 (3) 0 100])


plotperformance(b)





