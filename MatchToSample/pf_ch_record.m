function pf_ch_record

load record record
load idedays

p = 0;
for x = 1:size(record,1)
    ch = find(record(x,:));
    num_ch = size(ch,2);
    pairs = [];
    dp = 0;
    dpairs = [];
    for c1 = 1:num_ch
        for c2 = (c1 +1) : num_ch
            if (ch(c1) < 33) && (ch(c2) >32)
                p = p + 1;
                dp = dp + 1;
                dpairs(dp,:) = [ch(c1) ch(c2)];
            end
        end
    end
    numb_pairs(x) = dp;
    days{x} = alldays{x};
    day_pairs{x} = dpairs;
end



save day_pairs numb_pairs days day_pairs
p