function obj = plot(obj,varargin)


Args = struct('nineComb',0,'pLevel',3);
Args.flags = {'nineComb'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];



c = 1; for cue = 1 : 9; labels{c} = sprintf('ide %d; loc %d',cueComb(2,c),cueComb(1,c)); c = c + 1; end

if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
ch = obj.data.Index(ind,8);

load SFC.mat
if Args.nineComb
    allcues = 10;
else
    allcues = [1 : 9];
end

allymin = [];
allymax = [];
maxc = [];
for cue = allcues
    for al = 1 : 4
        if length(allcues) ~= 1; subplot(length(allcues),4,(cue-1) * 4 + al); else; subplot(4,length(allcues),al); end
        plot(f,squeeze(C{al}(:,ch,cue)));
        
        
        hold on
        plot(f,squeeze(SurProb{al}(:,Args.pLevel,ch,cue)),'--');
        
        maxc = [maxc max(squeeze(C{al}(:,ch,cue))) max(SurProb{al}(:,Args.pLevel,ch,cue))];
        hold off
        
        if al == 1 & ~Args.nineComb; ylabel(labels{cue}); end
    end
end
if ~isnan(maxc)
    tit = true;
    for cue = allcues
        for al = 1 : 4
            if length(allcues) ~= 1; subplot(length(allcues),4,(cue-1) * 4 + al); else; subplot(4,length(allcues),al); end
            axis([f(1) f(end) 0 max(maxc)])
            if al ==1 & tit == true ; title(sprintf('%s%s%s unit %s lfp %s\n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2), obj.data.Index(ind,3),obj.data.Index(ind,4))); tit = false;end
        end
    end
end
warning off all
sprintf('%s%s%s \n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2))


