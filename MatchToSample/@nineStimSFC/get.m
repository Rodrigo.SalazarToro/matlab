function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'rule',[],'unitcortex',[],'lfpcortex',[],'unit',[],'maxC',[],'sameElec',[]);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.day)
        rtemp1 = find(obj.data.Index(:,1) == Args.day);
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.rule)
        rtemp2 = find(obj.data.Index(:,2) == cell2mat(Args.rule));
        else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.unitcortex)
        rtemp3 = find(obj.data.Index(:,3) == cell2mat(Args.unitcortex));
        else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.lfpcortex)
        rtemp4 = find(obj.data.Index(:,4) == cell2mat(Args.lfpcortex));
        else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.unit)
        rtemp5 = find(obj.data.Index(:,6) == cell2mat(Args.unit));
        else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
     
     if ~isempty(Args.maxC)
        rtemp6 = find(obj.data.Index(:,7) >= Args.maxC);
        else
        rtemp6 = [1 : size(obj.data.Index,1)];
     end
    
    if ~isempty(Args.sameElec)
        rtemp7 = find(obj.data.Index(:,9) == Args.sameElec);
        else
        rtemp7 = [1 : size(obj.data.Index,1)];
     end
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,rtemp7))))));
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});

end

