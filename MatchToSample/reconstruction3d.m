function reconstruction3d


%function makeGrids(seed,plot_days,color)

%cd('G:\data\betty')
cd('/media/Volumes/NOMICS/data/betty')

load betty_reconstruction

depths = size(betty_reconstruction,2);

%color code 
matterLN = zeros(64,40);
for x = 1:64
    HistInfo = betty_reconstruction(x,:);
    [HistNumber, HistLetter]=categorizeNeuronalHistML(HistInfo);
    matterN(x,:) = HistNumber;
    
    if sum(strncmpi(HistLetter,'m',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'m',1))) = .1;
    end
    if sum(strncmpi(HistLetter,'l',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'l',1))) = .8;
    end
    if sum(strncmpi(HistLetter,'v',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'v',1))) = .2;
    end
    if sum(strncmpi(HistLetter,'d',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'d',1))) = .9;
    end
    if sum(strncmpi(HistLetter,'i',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'i',1))) = .5;
    end
    if sum(strncmpi(HistLetter,'w',1)) > 0
    matterLN(x,find(strncmpi(HistLetter,'w',1))) = 0;
    end
end




%%
%PF grid
pf_order = [ ...
    
0; 1; 2; 3; 4; 5;
0; 6; 7; 8; 9;10;
11;12;13;14;15;16;
17;18;19;20;21;22;
0;23;24;25;26;27;
0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    
0;33;34;35;36;37;
0;38;39;40;41;42;
43;44;45;46;47;48;
49;50;51;52;53;54;
0;55;56;57;58;59;
0;60;61;62;63;64];
%%

%make figure
f = figure('position', [0 50 1250 600]);
%% Get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
        end
    end
end

%%
%plot reconstruction (white_gray_matter)
for d = 1:depths
    for grids = 1:2
        ch = 0;
        for x = [6 5 4 3 2 1]
            for xx = 1:6
                ch = ch + 1;
                
                if (matterN(ch,d) ~= 28) %28 is white matter
                    c = (matterN(ch,d) / 100) * 2;
                    l = matterLN(ch,d);
                if grids == 1
                    channel = pf_order(ch);
                    if channel ~= 0
                        scatter3(xx,x,d,500,[c l 0],'fill')
                    end
                else
                    channel = pp_order(ch);
                    if channel ~= 0
                        scatter3((xx+7),x,d,500,[0 l c],'fill')
                    end
                end  
                
                end
                hold on 
            end
        end 
    end
end

axis([-.5 14.5 0 7.5])






