function out = corr_phase_list(varargin)

%run at sessoin level
Args = struct('ml',0,'fix',0,'sample',0,'locations',[],'all',0);
Args.flags = {'ml','fix','sample','all'};
Args = getOptArgs(varargin,Args);


if Args.fix
    BIN_RANGE = [1:9];
elseif Args.sample
    BIN_RANGE = [11:19];
elseif Args.all
    BIN_RANGE = [1:33];
else
    BIN_RANGE = [25:33];
end

nbins = size(BIN_RANGE,2);

sesdir = pwd;

if Args.ml
    [~,total_pairs,combs,~] = sorted_groups('ml'); %get list of sorted pairs
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
else
    [~,total_pairs,~,combs] = sorted_groups;
    mt = mtstrial('auto','redosetNames');
end

if ~isempty(Args.locations)
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1,'iCueLoc',Args.locations);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueLoc',Args.locations);
    end
else
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    end
end

num_trials = size(total_trials,2);

trial_corrcoefs = cell(1,size(combs,1));
trial_phases = cell(1,size(combs,1));
% trial_correlograms = cell(1,size(combs,1));
for pair_num = total_pairs
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    
    chpair = (['channelpair' num2strpad(combs(pair_num,1),2) num2strpad(combs(pair_num,2),2)]);
    load(chpair)
    
    t_corrcoefs = zeros(num_trials,nbins);
    t_phases = zeros(num_trials,nbins);
%     t_correlograms = cell(1,num_trials);
    
    counter = 0;
    for ccc = total_trials
        counter = counter + 1;
        t_corrcoefs(counter,:) = corrcoefs(BIN_RANGE,ccc)';
        t_phases(counter,:) = phases(BIN_RANGE,ccc)';
%         t_correlograms{counter} = correlograms{ccc}(BIN_RANGE,:)';
    end
    trial_corrcoefs{pair_num} = t_corrcoefs;
    trial_phases{pair_num} = t_phases;
%     trial_correlograms{pair_num} = t_correlograms;
end


out.trial_corrcoefs = trial_corrcoefs;
out.trial_phases = trial_phases;
% out.trial_correlograms = trial_correlograms;
out.trials = total_trials;

cd(sesdir)