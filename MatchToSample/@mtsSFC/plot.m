function obj = plot(obj,varargin)


Args = struct('pLevel',2,'xlim',[0 50],'plotType','standard');
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work soon
    ind = dataindices(n);
else
    
end

epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
tuning = {'iCueLoc' 'iCueObj'};

itemC = {'b' 'r' 'k'};

[pathstr, name, ~,~] = fileparts(obj.data.setNames{ind});
lfp = name(8:end);

switch Args.plotType
    
    case 'tuning'
        
        cd(pathstr)
        
        for tune = 1 : 2
            cmax = [];
            for item = 1 : 3
                load(sprintf('SFC%s%d%s.mat',tuning{tune},item,lfp),'C','f')
                try
                    load(sprintf('SFC%s%d%sSur.mat',tuning{tune},item,lfp),'Prob','f')
                catch
                    Prob = 0.05 * ones(65,6,4);
                end
                cmax = [cmax max(max(C))*1.1];
                for p = 1 : 4
                    subplot(2,4,(tune-1)*4 + p)
                    plot(f,C(:,p),itemC{item})
                    hold on
                    plot(f,squeeze(Prob(:,Args.pLevel,p)),sprintf('%s--',itemC{item}))
                    xlim(Args.xlim)
                    if tune ==1; title(epochs{p}); end
                    
                    
                end
            end
            %             if tune ==1
            %                 legend('loc1','sur','loc2','sur','loc3','sur')
            %             else
            %                 legend('obj1','sur','obj2','sur','obj3','sur')
            %             end
            for sb = 1 : 4; subplot(2,4,(tune-1)*4 + sb); ylim([0 max(cmax)]); end
        end
        
        xlabel('Frequency [Hz]')
        ylabel('spike field coherence')
        subplot(2,4,1)
        
        ylabel('loc. tuning')
        
        subplot(2,4,5)
        
        ylabel('obj. tuning')
        
    case 'standard'
        
        load(obj.data.setNames{ind},'phi','C','f')
        try
            load(sprintf('%sSur.mat',obj.data.setNames{ind}(1:end-4)),'Prob');
        catch
            Prob = 0.05 * ones(65,6,4);
        end
        cmax = max(max(C))*1.1;
        
        for p = 1 : 4
            subplot(2,4,p)
            plot(f,C(:,p))
            hold on
            plot(f,squeeze(Prob((1:33),Args.pLevel,p)),'--')
            xlim(Args.xlim)
            title(epochs{p})
            ylim([0 cmax])
            
            subplot(2,4,p+4)
            plot(f,phi(:,p))
            
            xlim(Args.xlim)
            ylim([-3.2 3.2])
        end
        subplot(2,4,1)
        xlabel('Frequency [Hz]')
        ylabel('spike field coherence')
        
        subplot(2,4,5)
        xlabel('Frequency [Hz]')
        ylabel('Phase [rad.]')
    case 'onlyDelay'
        nname = regexprep(obj.data.setNames{ind},'all','onlyDelayall');
        load(nname,'phi','C','f')
        try
            load(sprintf('%sSur.mat',nname(1:end-4)),'Prob');
        catch
            Prob = 0.05 * ones(65,6,4);
        end
        cmax = max(max(C))*1.1;
        
        if ~isempty(C)
            subplot(2,1,1)
            plot(f,C)
            hold on
            plot(f,squeeze(Prob(:,Args.pLevel)),'--')
            xlim(Args.xlim)
            title('onlyDelay')
            ylim([0 cmax])
            set(gca,'TickDir','out')
            subplot(2,1,2)
            plot(f,phi)
            set(gca,'TickDir','out')
        end
        xlim(Args.xlim)
        ylim([-pi pi])
        
        
        xlabel('Frequency [Hz]')
        ylabel('spike field coherence')
        
        subplot(2,1,2)
        xlabel('Frequency [Hz]')
        ylabel('Phase [rad.]')
    case 'Hphase'
       pathf = fileparts(obj.data.setNames{ind});
       
       for r =1 : 2
           load([regexprep(pathf,'FITZGERALD','FUSION') '/HBphase/' 'saccadePhasesrule' num2str(r) '.mat']); % temporary solution for my ext drives
           
           %        load([pathf '/HBphase/' 'saccadePhasesrule' num2str(r)
           %        '.mat']);
%            thech = find(obj.data.Index(ind,6) neuroinfo.groups);
           
           subplot(1,2,r)
           polar(tout,rout)
           hold on
           
           polar(tout,thr)
       end
        
        
end

set(gcf,'Name',obj.data.setNames{ind})