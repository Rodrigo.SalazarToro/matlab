function obj = mtsSFC(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'rule',1);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsSFC';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsSFC';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

Amtst = mtstrial('auto',varargin{:});
groups = nptDir('group*');

neuroinfo = NeuronalChAssign;
sdir = pwd;
count = 1;
histology = NeuronalHist;
if  ~isempty(groups)
    
    for g = 1 : size(groups,1)
        cd(groups(g).name)
        unitgn = str2num(groups(g).name(end-3:end));
        unit = neuroinfo.cortex(find(unitgn == neuroinfo.groups));
        unitHist =  histology.number(find(unitgn == neuroinfo.groups));
        gdir = pwd;
        clusters = nptDir('cluster*');
        for c = 1 : size(clusters,1)
            cd(clusters(c).name)
            type = clusters(c).name(end);
            for r = Args.rule
                sfcfiles = nptDir(sprintf('SFCall*Rule%d.mat',r));
                
                for ff = 1 : length(sfcfiles)
                    lfpgn = str2num(sfcfiles(ff).name(9:12));
                    lfp = neuroinfo.cortex(find(lfpgn == neuroinfo.groups));
                    lfpHist = histology.number(find(lfpgn == neuroinfo.groups));
                    Index(count,1) = count;
                    Index(count,2) = 1;
                    Index(count,3) = unit;
                    Index(count,4) = lfp;
                    Index(count,5) = unitgn;
                    Index(count,6) = lfpgn;
                    Index(count,7) = type;
                    Index(count,8) = r;
                    data.setNames{count} = [pwd '/' sfcfiles(ff).name];
                    
            
                    data.hist(count,:) = [unitHist lfpHist];
                    count = count + 1;
                    
                end
            end
            cd(gdir)
        end%c = 1 : size(clusters,1)
        
        cd(sdir)%g = 1 : size(group,1)
    end
    
    if exist('Index')
        
        data.Index = Index;
        data.numSets = count-1; % nbr of trial
        % create nptdata so we can inherit from it
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        fprintf('No SFC files \n');
        obj = createEmptyObject(Args);
    end
else
    % create empty object
    fprintf('No sorted group \n');
    obj = createEmptyObject(Args);
end




function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
data.hist = [];
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
