function r = xcorr_find_peak(x,y,windowSize,steps)
%function r = RandXcorr(x,y,windowSize,steps)

r=NaN(1,steps);
for ii=1:steps
    
    start1 =  ceil(rand(1)*(length(x)-windowSize-2));
    start2 = ceil(rand(1)*(length(y)-windowSize-2));
    data1 = x(start1:start1+windowSize);
    data2 = y(start2:start2+windowSize);
    %r = [r (dot(data1,data2))/sqrt(dot(data1,data1)*dot(data2,data2))];
    r(ii) =  xcorr(data1,data2,0,'coeff');
   
end
    
%    [n,x]=hist(r,100);
%    stdev = std(r);
%    r=mean(r);
