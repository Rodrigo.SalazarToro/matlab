function [varargout] = mkCohgram(varargin)
% load switchdays.mat
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% coh = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');


Args = struct('redo',0','save',0,'effect',[1 : 4],'combineRuleEffect',0,'RuleEffect',[1 : 3],'InCor',0,'ML',0,'coh1',[],'coh2',[],'allPairs',0,'Fix',[],'avCoh',0,'ndelay',[],'noHist',0,'mintrials',50,'tuning',[],'ind',[],'postfix',[],'GCgram',0);
Args.flags = {'redo','save','combineRuleEffect','InCor','ML','allPairs','avCoh','noHist','GCgram'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});

if isempty(Args.coh1)
    coh1 = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');
else
    coh1 = Args.coh1;
    allcoh = 1;
    if ~isempty(Args.coh2)
        coh2 = Args.coh2;
        allcoh = 2;
    end
end

if isempty(Args.tuning)
    
    
    if Args.GCgram
        prefix = 'gcgram';
    else
        prefix = 'cohgram';
    end
else
    prefix = 'tunegram';
end
effectName = {'betaIncr' 'betaDecr' 'gammaIncr' 'gammaDecr'};
if isempty(Args.ind); effects = Args.effect;else; effects = 1; end
RuleEffect = Args.RuleEffect;

if isempty(Args.Fix); rs = 1:2; else; rs = 1;end

if isempty(Args.ind);anatLoc = {'md'  'ld'};else; anatLoc = 1;end
if Args.combineRuleEffect; nef = 1; else nef = length(RuleEffect); end
for anat = 1 : length(anatLoc)
    for rule = 1 : nef
        for effect = effects
            count = 1;
            for ncoh =1  : allcoh
                index = eval(sprintf('coh%d.data.Index;',ncoh));
                coh = eval(sprintf('coh%d;',ncoh));
                if ncoh ==2; Args.ML = true; end
                if isempty(Args.ind)
                    if Args.combineRuleEffect;
                        %                 ind = find(((index(:,effect) == 1) + (index(:,effect) ==
                        %                 2) + (index(:,effect) == 3)) & index(:,9) == anat); % all
                        %                 effects IDe and LOC
                        %                     ind = find(((index(:,effect) == 1) + (index(:,effect) == 3)) & index(:,9) == anat);
                        if Args.noHist
                            [r,ind] = get(coh,'snr',1.8,'Number',effectName{effect},[1 3]);
                        else
                            [r,ind] = get(coh,'snr',1.8,'Number','pairhist',anatLoc{anat},'rudeHist',effectName{effect},[1 3]);
                        end
                    elseif Args.allPairs
                        %                     ind = [1 : size(index,1)];
                        [r,ind] = get(coh,'Number');
                        %
                    else
                        [r,ind] = get(coh,'snr',1.8,'Number','pairhist',anatLoc{anat},'rudeHist',effectName{effect},RuleEffect(rule));
                    end
                else
                    
                    ind = Args.ind;
                end
                if ~isempty(ind)
                    
                    for i = vecr(ind)
                        %                     coh.data.setNames{i}
                        cd(coh.data.setNames{i});
                        if ~isempty(strfind(coh.data.setNames{i},'betty'))
                            Args.ML = true;
                        end
                        % day directory
                        pairg = index(i,10:11);
                        
                        for r = rs
                            %index(:,[10 11])
                            clear C phi t
                            if Args.InCor
                                matfile = sprintf('%sg%04.0fg%04.0fRule%dIncor%d%s.mat',prefix,pairg(1),pairg(2),r,Args.ndelay,Args.postfix);
                            elseif ~isempty(Args.Fix)
                                switch Args.Fix
                                    case 'Fix'
                                        matfile = sprintf('%sg%04.0fg%04.0fRule%dFix%d%s.mat',prefix,pairg(1),pairg(2),r,Args.ndelay,Args.postfix);
                                    case 'FixS'
                                        matfile = sprintf('%sg%04.0fg%04.0fFixS%d.mat',prefix,pairg(1),pairg(2),Args.ndelay,Args.postfix);
                                    case 'FixFixS'
                                        
                                        matfile = sprintf('%sg%04.0fg%04.0fFixTS%d%s.mat',prefix,pairg(1),pairg(2),Args.ndelay,Args.postfix);
                                end
                            else
                                matfile = sprintf('%sg%04.0fg%04.0fRule%d%d%s.mat',prefix,pairg(1),pairg(2),r,Args.ndelay,Args.postfix);
                            end
                            cd grams
                            file = nptDir(matfile);
                            cd ..
                            rfile = nptDir('rules.mat');
                            if ~isempty(rfile);
                                rules = load('rules.mat');
                                ses = find(rules.r == r) + 1;
                            elseif Args.ML;
                                if ~isempty(Args.Fix)
                                    switch Args.Fix
                                        
                                        case 'Fix'
                                            ses = 1;
                                        case 'FixS'
                                            ses = 3;
                                        case 'FixFixS'
                                            ses = [1 3] ;
                                    end
                                else
                                    ses = 1;
                                end;
                            else
                                ses = 2;
                            end
                            
                            
                            if (isempty(file) || Args.redo)
                                scount = 1;
                                ntrials = 0;
                                for s = ses
                                    
                                    if ~isempty(nptDir(sprintf('session0%d',s)))
                                        cd(sprintf('session0%d',s))
                                        NeuroInfo = NeuronalChAssign;
                                        for c =1  : 2; ch(c) = find(pairg(c) == NeuroInfo.groups); end
                                        if ~isempty(Args.Fix)
                                            switch Args.Fix
                                                case 'FixFixS'
                                                    mts{scount} = mtstrial('auto','redosetNames');
                                                    rules = unique(mts{scount}.data.Index(:,1));
                                                otherwise
                                                    mts = mtstrial('auto','redosetNames');
                                                    rules = unique(mts.data.Index(:,1));
                                            end
                                        else
                                            mts = mtstrial('auto','redosetNames');
                                            rules = unique(mts.data.Index(:,1));
                                        end
                                        
                                        if isempty(Args.tuning)
                                            if Args.InCor
                                                if s ==1
                                                    trials = mtsgetTrials(mts,'BehResp',0,'rule',r,'ML',modvarargin{:});
                                                else
                                                    trials = mtsgetTrials(mts,'BehResp',0,modvarargin{:});
                                                end
                                                ntrials = length(trials);
                                            elseif ~isempty(Args.Fix)
                                                switch Args.Fix
                                                    case {'Fix' 'FixS'}
                                                        trials = mtsgetTrials(mts,'CueObj',55,modvarargin{:});
                                                        ntrials = length(trials);
                                                    case 'FixFixS'
                                                        trials{scount} = mtsgetTrials(mts{scount},'CueObj',55,modvarargin{:});
                                                        ntrials = ntrials + length(trials{scount});
                                                end
                                            elseif ~isempty(Args.ndelay)
                                                if s ==1
                                                    trials = mtsgetTrials(mts,'BehResp',1,'rule',r,'ML',modvarargin{:});
                                                else
                                                    trials = mtsgetTrials(mts,'BehResp',1,modvarargin{:});
                                                end
                                                ntrials = length(trials);
                                            else
                                                if s ==1
                                                    trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',r,'ML',modvarargin{:});
                                                else
                                                    trials = mtsgetTrials(mts,'stable','BehResp',1,modvarargin{:});
                                                end
                                                ntrials = length(trials);
                                            end
                                        else
                                            [tunePow,tuneCoh,variable,f,trials] = mkTunegram(mts,s,ch,r,modvarargin{:});
                                        end
                                        cd ..
                                    end
                                    scount = scount + 1;
                                end
                                
                                if ntrials > Args.mintrials
                                    
                                    if Args.GCgram
                                        [Fx2y,Fy2x,Fxy,~,~,t,f] = MTScohgram(mts,trials,ch,'sessions',ses,modvarargin{:},'GCgram');% GCgram(data1,data2,params,movingwin,porder);
                                        
                                    else
                                        if sum(strcmp(modvarargin(:),'err') ~= 0)
                                            [Ct,phit,S12,S1,S2,t{1},f,~,~,phistdt,Cerrt] = MTScohgram(mts,trials,ch,'sessions',ses,modvarargin{:});
                                            C{1} = squeeze(Ct);
                                            phi{1} = squeeze(phit);
                                            Cerr{1} = squeeze(Cerrt);
                                            phistd{1} = squeeze(phistdt);
                                            
                                            
                                            [Ct,phit,S12,S1,S2,t{2},f,~,~,phistdt,Cerrt] = MTScohgram(mts,trials,ch,'matchAlign','sessions',ses,modvarargin{:});
                                            C{2} = squeeze(Ct);
                                            phi{2} = squeeze(phit);
                                            Cerr{2} = squeeze(Cerrt);
                                            phistd{2} = squeeze(phistdt);
                                        else
                                            [Ct,phit,S12,S1,S2,t{1},f] = MTScohgram(mts,trials,ch,'sessions',ses,modvarargin{:});
                                            C{1} = squeeze(Ct);
                                            phi{1} = squeeze(phit);
                                            
                                            
                                            [Ct,phit,S12,S1,S2,t{2},f] = MTScohgram(mts,trials,ch,'matchAlign','sessions',ses,modvarargin{:});
                                            C{2} = squeeze(Ct);
                                            phi{2} = squeeze(phit);
                                        end
                                    end
                                    display(matfile)
                                    cd grams
                                    if isempty(Args.tuning)
                                        if Args.GCgram
                                            save(matfile,'Fx2y','Fy2x','Fxy','t','f','trials')
                                        elseif sum(strcmp(modvarargin(:),'err') ~= 0)
                                             save(matfile,'C','phi','t','f','trials','Cerr','phistd')
                                        else
                                            
                                            save(matfile,'C','phi','t','f','trials')
                                        end
                                    else
                                        save(matfile,'tunePow','tuneCoh','variable','f','trials')
                                    end
                                    cd ..
                                end
                            elseif Args.avCoh && ~isempty(nptDir(sprintf('session0%d',s)))
                                cd grams
                                load(matfile)
                                if length(trials) > Args.mintrials
                                    loc(anat).rules(rule).effect(effect).pair(count).therule(r).C = C;
                                    if size(C,1) ==2 || ~iscell(t); delete(matfile); elseif isempty(C); keyboard;end
                                    loc(anat).rules(rule).effect(effect).pair(count).therule(r).phi = phi;
                                    loc(anat).rules(rule).effect(effect).pair(count).therule(r).name = sprintf('%s/%s',pwd,matfile);
                                    loc(anat).rules(rule).effect(effect).pair(count).therule(r).t = t;
                                end
                                cd ..
                                
                            end
                        end
                        count = count + 1;
                        
                        cd ..
                        
                    end
                    %                 else
                    %                     loc(anat).rules(rule).effect(effect-2).pair = [];
                end
                
            end
        end
    end
end
if exist('loc')
    varargout{1} = loc;
end
if Args.avCoh
    anatloc = {'PPm-PFd' 'PPl-PFd'};
    
    if Args.combineRuleEffect;ruleEffect = {'any incr.'}; else ruleEffect = {'only IDE' 'only LOC' 'both'};end
    align = {'Sample off-set aligned' 'Match aligned'};
    ref = [1 1.8];
    rules = {'IDENTITY' 'LOCATION'};
    theffects = {'beta incr' 'beta decr' 'gamma incr' 'gamma decr'};
    % and 3 for both rules
    if isempty(Args.ind)
        for anat = 1: length(anatLoc)
            if size(loc,2) >= anat
                for r = 1 : length(ruleEffect)
                    for e = effects
                        if size(loc(anat).rules(r).effect,2) >= e
                            n = size(loc(anat).rules(r).effect(e).pair,2);
                            if n > 1
                                h = figure;
                                set(h,'Name',sprintf('%s; %s; %s; n=%d',theffects{e},anatloc{anat},ruleEffect{r},n))
                                mil = [];
                                thet = [];
                                
                                if e > 2; clim = 0.18; else clim = 0.18; end
                                
                                for therule = 1 : 2
                                    
                                    for cond = 1 : 2
                                        for pa =  1 : n; if ~isempty(loc(anat).rules(r).effect(e).pair(pa).therule) && size(loc(anat).rules(r).effect(e).pair(pa).therule,2) >= therule ; mil = [mil size(loc(anat).rules(r).effect(e).pair(pa).therule(therule).C{cond},1)]; t = loc(anat).rules(r).effect(e).pair(pa).therule(therule).t{cond}; end; end;
                                        mil = min(mil);
                                        
                                        clear thedata thephi;
                                        cpa = 1;
                                        for pa =1 : n;
                                            if ~isempty(loc(anat).rules(r).effect(e).pair(pa).therule) && size(loc(anat).rules(r).effect(e).pair(pa).therule,2) >= therule
                                                thedata(cpa,:,:) = loc(anat).rules(r).effect(e).pair(pa).therule(therule).C{cond}(1:mil,:);
                                                thephi(cpa,:,:) = loc(anat).rules(r).effect(e).pair(pa).therule(therule).phi{cond}(1:mil,:);
                                                cpa = cpa + 1;
                                            end
                                        end;
                                        if exist('thedata')
                                            subplot(4,2,(therule-1) * 2 + cond)
                                            imagesc(t,f,squeeze(median(thedata,1))',[0 clim])
                                            colorbar
                                            hold on
                                            line([ref(cond) ref(cond)],[0 60],'Color','r')
                                            if therule == 1; title(align{cond}); end
                                            if cond == 1; ylabel(rules{therule}); end
                                            
                                            subplot(4,2,(therule-1) * 2 + cond + 4)
                                            imagesc(t,f,squeeze(median(thephi,1))',[-pi pi])
                                            colorbar
                                            hold on
                                            line([ref(cond) ref(cond)],[0 60],'Color','r')
                                            if therule == 1; title('Phase [rad.]'); end
                                            
                                            if cond == 1; ylabel(rules{therule}); end
                                        end
                                    end
                                end
                                
                                xlabel('Time [sec]')
                                ylabel('Frequency [Hz]')
                            end
                        end
                    end
                end
            end
        end
    else
        h = figure;
        
        mil = [];
        thet = [];
        anat = 1; r = 1; e = 1;
        if e > 2; clim = 0.18; else clim = 0.18; end
        
        n = size(loc(anat).rules(r).effect(e).pair,2);
        therule = 1;
        for cond = 1 : 2
            for pa =  1 : n; if ~isempty(loc(anat).rules(r).effect(e).pair(pa).therule) && size(loc(anat).rules(r).effect(e).pair(pa).therule,2) >= therule ; mil = [mil size(loc(anat).rules(r).effect(e).pair(pa).therule(therule).C{cond},1)]; t = loc(anat).rules(r).effect(e).pair(pa).therule(therule).t{cond}; end; end;
            mil = min(mil);
            
            clear thedata thephi;
            cpa = 1;
            for pa =1 : n;
                if ~isempty(loc(anat).rules(r).effect(e).pair(pa).therule) && size(loc(anat).rules(r).effect(e).pair(pa).therule,2) >= therule
                    thedata(cpa,:,:) = loc(anat).rules(r).effect(e).pair(pa).therule(therule).C{cond}(1:mil,:);
                    thephi(cpa,:,:) = loc(anat).rules(r).effect(e).pair(pa).therule(therule).phi{cond}(1:mil,:);
                    cpa = cpa + 1;
                end
            end;
            if exist('thedata')
                subplot(4,2,(therule-1) * 2 + cond)
                imagesc(t,f,squeeze(mean(thedata,1))',[0 clim])
                colorbar
                hold on
                line([ref(cond) ref(cond)],[0 60],'Color','r')
                if therule == 1; title(align{cond}); end
                if cond == 1; ylabel(rules{therule}); end
                
                subplot(4,2,(therule-1) * 2 + cond + 4)
                imagesc(t,f,squeeze(median(thephi,1))',[-pi pi])
                colorbar
                hold on
                line([ref(cond) ref(cond)],[0 60],'Color','r')
                if therule == 1; title('Phase [rad.]'); end
                
                if cond == 1; ylabel(rules{therule}); end
            end
        end
        
        
        xlabel('Time [sec]')
        ylabel('Frequency [Hz]')
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [tunePow,tuneCoh,variable,f,trials] = mkTunegram(mt,s,ch,r,varargin)

allloc = vecr(unique(mt.data.CueLoc));
allobj = unique(mt.data.CueObj);

for type = 1 : 3
    
    switch type
        case 1
            ncues = 3;
            arg{1} = 'allloc(cues)';
            arg{2} = 'allobj';
            var = 'loc';
        case 2
            ncues = 3;
            arg{1} = 'allloc';
            arg{2} = 'allobj(cues)';
            var = 'ide';
        case 3
            ncues = 9;
            cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
            arg{1} = 'allloc(cueComb(1,cues))';
            arg{2} = 'allobj(cueComb(2,cues))';
            var = 'allcues';
    end
    et = true;
    for cues = 1 : ncues
        if s ==1
            trials(type).list{cues} = single(mtsgetTrials(mt,'stable','BehResp',1,'rule',r,'ML','CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),varargin{:}));
        else
            trials(type).list{cues} = single(mtsgetTrials(mt,'stable','BehResp',1,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),varargin{:}));
        end
        if ~isempty(trials(type).list{cues})
            [C{cues},phi{cues},S12,S1,S2,t{cues},f,S{cues}] = MTScohgram(mt,trials(type).list{cues},ch,'forTuning',varargin{:});
            mint(cues) = length(t{cues});
            C{cues} = single(squeeze(C{cues}));
            S{cues} = single(squeeze(S{cues}));
            phi{cues} = single(squeeze(phi{cues}));
            t{cues} = single(t{cues});
        else
            et = false;
        end
    end
    if et
        clear Cm Cs
        for c = 1 : ncues; Cm(:,:,c) = mean(C{c}(1:min(mint),:,:),3); Cs(:,:,c) = std(C{c}(1:min(mint),:,:),[],3); end
        
        Tc = std(Cm,[],3) ./ mean(Cs,3);
        %     imagesc(t{2},f,flipud(rot90(squeeze(tunegram(:,:)))))
        
        clear Sm Ss
        for c = 1 : ncues; Sm(:,:,:,c) = mean(S{c}(:,1:min(mint),:,:),4); Ss(:,:,:,c) = std(S{c}(:,1:min(mint),:,:),[],4); end
        
        Ts= std(Sm,[],4) ./ mean(Ss,4);
        
        tuneCoh(type).V = C;
        tunePow(type).V = S;
        
        variable(type).t = t;
        tuneCoh(type).phi = phi;
        tuneCoh(type).T = single(Tc);
        tunePow(type).T = single(Ts);
        variable(type).type = var;
    else
        tuneCoh(type).V = [];
        tunePow(type).V = [];
        
        variable(type).t = [];
        tuneCoh(type).phi = [];
        tuneCoh(type).T = [];
        tunePow(type).T = [];
        variable(type).type = [];
        trials(type).list{cues} = [];
        if ~exist('f'); f = []; end
        
    end
    
    %     imagesc(t{2},f,flipud(rot90(squeeze(tunegram(1,:,:)))))
    
    %     for c = 1 : ncues
    %         subplot(3,1,c)
    %         imagesc(t{c},f,flipud(rot90(squeeze(mean(S{c}(2,:,:,:),4)))))
    %         colorbar
    %
    %     end
end