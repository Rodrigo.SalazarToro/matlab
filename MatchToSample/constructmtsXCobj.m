cd /media/raid/data/monkey/clark/
% load idedays
% days = idedays;
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjIDEonly.mat nobj
% 
% load locdays
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjLOConly.mat nobj

load switchdays

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjIDE.mat nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjLOC.mat nobj
%%
cd /media/raid/data/monkey/betty/acute
% load idedays.mat
% load histdays
% days = intersect(days,histdays);
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjIDEonly.mat nobj
% 
% load locdays.mat
% load histdays
% days = intersect(days,histdays);
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjLOConly.mat nobj


load switchdays
% load histdays
% days = intersect(days,histdays);

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjIDE.mat nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjLOC.mat nobj

%%

cd /media/raid/data/monkey/betty/
load idedays

% load longdays
% days = [idedays lcdays lbdays];
% days = days([1:37 39:48]);
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjIDEonly.mat nobj
% 
% load locdays
% 
% clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
% nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
% 
% save mtsXCobjLOConly.mat nobj

load switchdays

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjIDE.mat nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(mtsXCspikes,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end

save mtsXCobjLOC.mat nobj

%%

cd /media/raid/data/monkey/betty/
bettyIDE = loadObject('mtsXCobjIDE.mat');
% bettyIDEonly = loadObject('mtsXCobjIDEonly.mat');
bettyLOC = loadObject('mtsXCobjLOC.mat');
% bettyLOConly = loadObject('mtsXCobjLOConly.mat');

cd /media/raid/data/monkey/betty/acute
bettyacIDE = loadObject('mtsXCobjIDE.mat');
% bettyacIDEonly = loadObject('mtsXCobjIDEonly.mat');
bettyacLOC = loadObject('mtsXCobjLOC.mat');
% bettyacLOConly = loadObject('mtsXCobjLOConly.mat');

cd /media/raid/data/monkey/clark
clarkIDE = loadObject('mtsXCobjIDE.mat');
% clarkIDEonly = loadObject('mtsXCobjIDEonly.mat');
clarkLOC = loadObject('mtsXCobjLOC.mat');
% clarkLOConly = loadObject('mtsXCobjLOConly.mat');

cd /media/raid/data/monkey/betty/
nobj = plus(bettyIDE,bettyacIDE);
save mtsXCobjIDE&acute.mat nobj

% nobj = plus(bettyIDEonly,bettyacIDEonly);
% save mtsXCobjIDEonly&acute.mat nobj

nobj = plus(bettyLOC,bettyacLOC);
save mtsXCobjLOC&acute.mat nobj

% nobj = plus(bettyLOConly,bettyacLOConly);
% save mtsXCobjLOConly&acute.mat nobj

cd /media/raid/data/monkey/
nobj = plus(plus(bettyIDE,bettyacIDE),clarkIDE);
save mtsXCobjIDE.mat nobj

% nobj = plus(plus(bettyIDEonly,bettyacIDEonly),clarkIDEonly);
% save mtsXCobjIDEonly.mat nobj

nobj = plus(plus(bettyLOC,bettyacLOC),clarkLOC);
save mtsXCobjLOC.mat nobj

% nobj = plus(plus(bettyLOConly,bettyacLOConly),clarkLOConly);
% save mtsXCobjLOConly.mat nobj


%%
cd /media/raid/data/monkey/betty/acute
load switchdays
clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthIDEswitchObj.mat' nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthLOCswitchObj.mat' nobj

cd /media/raid/data/monkey/clark/
load switchdays
clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthIDEswitchObj.mat' nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthLOCswitchObj.mat' nobj

cd /media/raid/data/monkey/betty/
load switchdays
clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',1);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthIDEswitchObj.mat' nobj

clear obj;parfor d = 1 : length(days);obj{d} = ProcessDays(nineStimPsth,'days',days{d},'sessions',{'session99'},'NoSites','rule',2);end
nobj = obj{1};for d = 1 : length(days)-1; nobj = plus(nobj,obj{d+1}); end
save 'psthLOCswitchObj.mat' nobj


cd /media/raid/data/monkey/betty/
bettyIDE = loadObject('psthIDEswitchObj.mat');
bettyLOC = loadObject('psthLOCswitchObj.mat');

cd /media/raid/data/monkey/betty/acute
bettyacIDE = loadObject('psthIDEswitchObj.mat');
bettyacLOC = loadObject('psthLOCswitchObj.mat');


cd /media/raid/data/monkey/clark
clarkIDE = loadObject('psthIDEswitchObj.mat');
clarkLOC = loadObject('psthLOCswitchObj.mat');

cd /media/raid/data/monkey/betty/
nobj = plus(bettyIDE,bettyacIDE);
save psthIDEswitchObj&acute.mat nobj

nobj = plus(bettyLOC,bettyacLOC);
save psthLOCswitchObj&acute.mat nobj

cd /media/raid/data/monkey/
nobj = plus(plus(bettyIDE,bettyacIDE),clarkIDE);
save psthIDEswitchObj.mat nobj
nobj = plus(plus(bettyLOC,bettyacLOC),clarkLOC);
save psthLOCswitchObj.mat nobj
