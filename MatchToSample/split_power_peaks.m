function split_power_peaks

cd '/media/raid/data/monkey/betty/'

load alldays

% load rodrigo_power_peaks

% rp = rodrigo_power_peaks;

% cd '/media/raid/data/monkey/betty/091001/session01'
% 
% rp = power_obj('auto');


rp = processDays(power_obj,'days',alldays,'ml','NoSites')

h1counter = 0;
h2counter = 0;
peaks1 = [];
peaks2 = [];
for x = 1 : rp.data.numSets
    
    
    histn = rp.data.Index(x,3);
    rp.data.peaks{x,4};
    
    if histn == 8 || histn == 10 || histn == 12
        h1counter = h1counter + 1;
        p1 = rp.data.peaks{x,5}(find(rp.data.peaks{x,5} > 8 & rp.data.peaks{x,5} < 26));
        peaks1 = [peaks1 p1];
    elseif histn == 11 || histn == 13
        p2 = rp.data.peaks{x,5}(find(rp.data.peaks{x,5} > 8 & rp.data.peaks{x,5} < 26));
        h2counter = h2counter + 1;
        peaks2 = [peaks2 p2];
    end
   
end

peaks1;