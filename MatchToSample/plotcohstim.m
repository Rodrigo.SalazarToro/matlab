function plotcohstim(day,varargin)
% to be run in the monkey directory
cd(day)

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
tit = pwd;

load allCuesTuning.mat


for comb = 1 : size(Adz,2)
    f1 = figure;
    ylim = [];
    for stim = 1 : size(Adz,4)

        for p = 1 : 4

            ind = logical(~Adz(:,comb,p,stim));

            subplot(size(Adz,4),4,(stim-1)*4 + p)
            plot(f,squeeze(C1(:,comb,p,stim)),'b')
            hold on


            plot(f,squeeze(C2(:,comb,p,stim)),'r')
            if p == 4 & stim == size(Adz,4); legend('session02','session03'); end
            plot(f(ind),squeeze(C1(ind,comb,p,stim)),'k*')

            plot(f(ind),squeeze(C2(ind,comb,p,stim)),'k*')
            ylim = [ylim max([C1(:,comb,p,stim);C2(:,comb,p,stim)])];
            if p == 1; ylabel(sprintf('loc %d; id %d',cueComb(1,stim),cueComb(2,stim))); end
        end

    end
    for sb = 1 : size(Adz,4)*4; subplot(size(Adz,4),4,sb); axis([4 96 0 1.2*max(ylim)]); end
    set(f1,'Name',sprintf('%s; comb # %d',tit(end-5:end),comb))

end
cd ..
