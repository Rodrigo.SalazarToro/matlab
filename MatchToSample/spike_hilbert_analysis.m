function spike_hilbert_analysis(varargin)

%run at session level
%analyzes the results from run_hilbertphase_entropy_hsfpp.m
%looks for reductions in entropy when spikes are conditioned on plv values
Args = struct('ml',0,'redo',0);
Args.flags = {'ml','redo'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
N = NeuronalHist;
cd([sesdir filesep 'lfp' filesep 'lfp2'])

sph = nptdir('spikehilber*');
npairs = size(sph,1);
for p = 1 : npairs
    
    load(sph(p).name);
    nfreq = size(all_freq,2);
    
    spikes1 = spikehilbertentropy{1}{1}; %get data
    spikes2 = spikehilbertentropy{1}{2};
    
    t1 = 1200;
    t2 = 1800;
    %make distributions of all cross spike plv values
    allplvspike1 = [];
    allplvspike2 = [];
    for nt = 1 : spikes1.ntrials
        sp1 = spikes1.sp1{nt};
        goodsp1 = [min(find(sp1>t1)):max(find(sp1<t2))];  %delay
        
        sp2 = spikes2.sp2{nt};
        goodsp2 = [min(find(sp2>t1)):max(find(sp2<t2))];  %delay
        allplvspike1 = [allplvspike1 spikes1.plv{6,nt}(goodsp1)];
        allplvspike2 = [allplvspike2 spikes2.plv{6,nt}(goodsp2)];
    end
    %get quartiles
    prctcross1 = prctile(allplvspike1,[25,50,75]);
    prctcross2 = prctile(allplvspike2,[25,50,75]);

    cross1 = cell(1,4);
    cross2 = cell(1,4);
    crossall = cell(1,2);
    for x = 1 : spikes1.ntrials
        for nf = 3%1 : nfreq
            sp1 = spikes1.sp1{x};
            goodsp1 = [min(find(sp1>t1)):max(find(sp1<t2))];  %delay
            
            sp2 = spikes2.sp2{x};
            goodsp2 = [min(find(sp2>t1)):max(find(sp2<t2))];  %delay
            
            crossall{1} = [crossall{1} spikes1.cross{nf,x}(goodsp1((find(spikes1.plv{nf,x}(goodsp1) > 0))))];
            crossall{2} = [crossall{2} spikes2.cross{nf,x}(goodsp2((find(spikes2.plv{nf,x}(goodsp2) > 0))))];
            
            %run through all quartiles
            for qt = 1:4
                if qt == 1
                    cross1{qt} = [cross1{qt} spikes1.cross{nf,x}(goodsp1(find(spikes1.plv{nf,x}(goodsp1) <= prctcross1(1))))];
                    cross2{qt} = [cross2{qt} spikes2.cross{nf,x}(goodsp2(find(spikes2.plv{nf,x}(goodsp2) <= prctcross2(1))))];
                elseif qt == 2
                    cross1{qt} = [cross1{qt} spikes1.cross{nf,x}(goodsp1(find(spikes1.plv{nf,x}(goodsp1) > prctcross1(1) & spikes1.plv{nf,x}(goodsp1) <= prctcross1(2))))];
                    cross2{qt} = [cross2{qt} spikes2.cross{nf,x}(goodsp2(find(spikes2.plv{nf,x}(goodsp2) > prctcross2(1) & spikes2.plv{nf,x}(goodsp2) <= prctcross2(2))))];
                elseif qt == 3
                    cross1{qt} = [cross1{qt} spikes1.cross{nf,x}(goodsp1(find(spikes1.plv{nf,x}(goodsp1) > prctcross1(2) & spikes1.plv{nf,x}(goodsp1) <= prctcross1(3))))];
                    cross2{qt} = [cross2{qt} spikes2.cross{nf,x}(goodsp2(find(spikes2.plv{nf,x}(goodsp2) > prctcross2(2) & spikes2.plv{nf,x}(goodsp2) <= prctcross2(3))))];
                elseif qt == 4
                    cross1{qt} = [cross1{qt} spikes1.cross{nf,x}(goodsp1(find(spikes1.plv{nf,x}(goodsp1) > prctcross1(3))))];
                    cross2{qt} = [cross2{qt} spikes2.cross{nf,x}(goodsp2(find(spikes2.plv{nf,x}(goodsp2) > prctcross2(3))))];
                end
            end
        end
    end
    
    %get p-values for rayleigh test on all the spikes
    otestpval(1) = circ_otest(crossall{1});
    otestpval(2) = circ_otest(crossall{2});
    
    for qt = 1:4
        
        spikes1 = cross1{qt};
        spikes2 = cross2{qt};
        
        nspikes1 = size(spikes1,2);
        nspikes2 = size(spikes2,2);
        allspike_counts{1,qt} = nspikes1;
        allspike_counts{2,qt} = nspikes2;
        
        spikesall1 = crossall{1};
        spikesall2 = crossall{2};
        
        nbins = 10;
        
        bins = linspace(-pi,pi,nbins);
        maxent = log2(nbins);
        allhist1 = hist(spikes1,bins) ./ nspikes1;
        allhist2 = hist(spikes2,bins) ./ nspikes2;
        
        bin_spikes{1,qt} = allhist1;
        bin_spikes{2,qt} = allhist2;
        
        spent1  = -1 * nansum(allhist1.*log2(allhist1));
        spent1 = (maxent- spent1) / maxent;
        
        spent2  = -1 * nansum(allhist2.*log2(allhist2));
        spent2 = (maxent- spent2) / maxent;
        
        %calculate the surrogate distribution
        
        %         allpermhist = [];
        
        %only need to do this once since there are the same number of spikes in all quartiles
        if qt == 1;
            all_var1 = [];
            all_var2 = [];
            for xx = 1:1000
                n1 = randperm(size(spikesall1,2));
                n2 = randperm(size(spikesall2,2));
                
                randspikes1 = spikesall1(n1(1:nspikes1));
                randspikes2 = spikesall2(n2(1:nspikes2));
                
                bins = linspace(-pi,pi,nbins);
                
                permhist1 = hist(randspikes1,bins) ./ size(randspikes1,2);
                permhist2 = hist(randspikes2,bins) ./ size(randspikes2,2);
                
                av1 = -1 * nansum(permhist1.*log2(permhist1));
                av1 = (maxent - av1) / maxent;
                all_var1 = [all_var1 av1];
                
                av2 = -1 * nansum(permhist2.*log2(permhist2));
                av2 = (maxent - av2) / maxent;
                all_var2 = [all_var2 av2];
                
                
                %get information for binwise surrogate
                %             allpermhist = [allpermhist; permhist];
            end
        end
        %         bin_pvals{spcomb} = prctile(allpermhist,95);
        allpvals{1,qt} = prctile(all_var1,[95 99]);
        allpvals{2,qt} = prctile(all_var2,[95 99]);
        
        allmeans{1,qt} = mean(all_var1);
        allmeans{2,qt} = mean(all_var2);
        
        allnorm_entropy{1,qt} = spent1;
        allnorm_entropy{2,qt} = spent2;
        all_spikes{1,qt} = spikes1;
        all_spikes{2,qt} = spikes2;
    end
    
    
    norm_entropy = allnorm_entropy(1,:);
    
    biascorrected_entropy = cell2mat(allnorm_entropy(1,:)) - cell2mat(allmeans(1,:));
    
    otest_allspikes_pval = otestpval(1);
    
    allspikes = all_spikes(1,:);
    binspikes = bin_spikes(1,:);
    pvals = allpvals(1,:);
    spike_counts = allspike_counts(1,:);
    %save two files
    channel_pairs = [ 'threshspikehilbertentropy' num2strpad(groups(1),2) num2strpad(groups(2),2)];
    save(channel_pairs,'allspikes','binspikes','all_freq','groups','pvals','norm_entropy','nbins','spike_counts','otest_allspikes_pval','biascorrected_entropy')
    
    plot(biascorrected_entropy,'b')
    hold on
    
    norm_entropy = allnorm_entropy(2,:);
    
    biascorrected_entropy = cell2mat(allnorm_entropy(2,:)) - cell2mat(allmeans(2,:));
    
    plot(biascorrected_entropy,'b')
    
    otest_allspikes_pval = otestpval(2);
    
    
    cla

    allspikes = all_spikes(2,:);
    binspikes = bin_spikes(2,:);
    pvals = allpvals(2,:);
    spike_counts = allspike_counts(2,:);
    channel_pairs = [ 'threshspikehilbertentropy' num2strpad(groups(2),2) num2strpad(groups(1),2)];
    save(channel_pairs,'allspikes','binspikes','all_freq','groups','pvals','norm_entropy','nbins','spike_counts','otest_allspikes_pval','biascorrected_entropy')
end

cd(sesdir)

