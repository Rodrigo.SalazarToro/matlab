function [percent,varargout] = createWindComp(monkeydir,varargin)

Args = struct('windows',[1 3 4],'prctile',[25 50 75]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

allcomp = nchoosek(Args.windows,2);
dirChange = {'pos' 'neg'};
ranges = {'beta' 'gamma'};
clear Allindex

for monkey = 1 : length(monkeydir)
    cd(monkeydir{monkey})
    for comp = 1 : size(allcomp,1)
        for band = 1 : 2
            for change = 1 : 2
                
                [Ipairs,ntot,Iindex] = selectPairs('windows',allcomp(comp,:),'comparison',{dirChange{change}},'range',{ranges{band}},'rule',1);
                [Lpairs,ntot,Lindex] = selectPairs('windows',allcomp(comp,:),'comparison',{dirChange{change}},'range',{ranges{band}},'rule',2);
                %                 for band = 1 : 2
                Allindex{monkey,comp,band,change} = combineIndexes(Lindex,Lpairs,Iindex,Ipairs);
                selpairs(monkey,comp,band,change) = Allindex{monkey,comp,band,change}.number;
                
                %% [PPch,PFch,spectra{comp,band,change}] = getChInfo(Allindex{comp,band,change},'getspectra','plot');
                %%
                percent(monkey,comp,band,change) = 100 * selpairs(monkey,comp,band,change) / ntot;
                
            end
        end
    end
end

for comp = 1 : size(allcomp,1)
    for band = 1 : 2
        for change = 1 : 2
            Mindex{comp,band,change}.days = [Allindex{:,comp,band,change}.days];
            Mindex{comp,band,change}.npair = [Allindex{:,comp,band,change}.npair];
            Mindex{comp,band,change}.ch = [Allindex{:,comp,band,change}.ch];
            Mindex{comp,band,change}.number = length(Mindex{comp,band,change}.npair);
        end
    end
end
varargout{1} = Mindex;
comp =  find(allcomp(:,1) == 3 & allcomp(:,2) == 4);
band = 1;
change = 1;
days = unique(Mindex{comp,band,change}.days);

[sigvalue,sigcoh,sigphi,f,signcoh,ncount,totn,powerPP,powerPF] = coherence_summary(days,'power','plot','list',Mindex{comp,band,change},'prctile',Args.prctile);


comp =  find(allcomp(:,1) == 1 & allcomp(:,2) == 4);
band = 2;
change = 2;
days = unique(Mindex{comp,band,change}.days);

[sigvalue,sigcoh,sigphi,f,signcoh,ncount,totn,powerPP,powerPF] = coherence_summary(days,'power','plot','list',Mindex{comp,band,change},'prctile',Args.prctile);


function [Allindex,varargout] = combineIndexes(index1,pairs1,index2,pairs2,varargin)

[c, ia, ib] = union(pairs1,pairs2,'rows');
%

for i = 1 : length(ia)
    Allindex.days{i} = index1.days{ia(i)};
    
    Allindex.npair(i) = index1.npair(ia(i));
    
    Allindex.ch(i,:) = index1.ch(ia(i),:);
end

for ii = 1 : length(ib)
    Allindex.days{i + ii} = index2.days{ib(ii)};
    
    Allindex.npair(i + ii) = index2.npair(ib(ii));
    
    Allindex.ch(i + ii,:) = index2.ch(ib(ii),:);
end

Allindex.number = length(Allindex.npair);