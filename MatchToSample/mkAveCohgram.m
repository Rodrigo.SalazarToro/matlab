function [varargout] = mkAveCohgram(coh,ind,varargin)
% load switchdays.mat
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% coh = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');


Args = struct('redo',0','save',0,'InCor',0,'ML',0,'Fix',0,'FixS',0,'stim',[],'ndelay',[],'tuning',0,'clim',0.4,'philim',2*pi,'frange',[5 45],'minTrials',140,'mean',0,'varPhi',0,'FigTit',[],'surrogate',0);
Args.flags = {'redo','save','combineRuleEffect','InCor','ML','Fix','allPairs','avCoh','tuning','mean','varPhi','FixS','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});


if Args.tuning
    prefix = 'tunegram';
else
    prefix = 'cohgram';
end
if Args.Fix || Args.FixS; nr = 1; else nr = 2; end
index = coh.data.Index;
count = 1;
sind = cell(2,1);

if Args.surrogate
    
    [b,indt,n] = unique(coh.data.setNames(ind));
    ind = ind(indt);
end
for i = vecr(ind)
    %                     coh.data.setNames{i}
    cd(coh.data.setNames{i}); % day directory
    pairg = index(i,10:11);
    if isempty(strfind(coh.data.setNames{i},'betty')); Args.ML = false; else Args.ML = true; end
    for r = 1 : nr
        %index(:,[10 11])
        clear C phi t
        if Args.InCor
            matfile = sprintf('%sg%04.0fg%04.0fRule%dIncor%d.mat',prefix,pairg(1),pairg(2),r,Args.ndelay);
        elseif Args.Fix
            matfile = sprintf('%sg%04.0fg%04.0fFix%d.mat',prefix,pairg(1),pairg(2),Args.ndelay);
            s = 2;
        elseif Args.FixS
            matfile = sprintf('%sg%04.0fg%04.0fFixS%d.mat',prefix,pairg(1),pairg(2),Args.ndelay);
            s = 3;
        elseif ~isempty(Args.stim)
            if Args.surrogate
                matfile = sprintf('%sgeneralizedRule%d%d%s.mat',prefix,r,Args.ndelay,Args.stim);
            else
                matfile = sprintf('%sg%04.0fg%04.0fRule%d%d%s.mat',prefix,pairg(1),pairg(2),r,Args.ndelay,Args.stim);
            end
        else
            matfile = sprintf('%sg%04.0fg%04.0fRule%d%d.mat',prefix,pairg(1),pairg(2),r,Args.ndelay);
        end
        cd grams
        file = nptDir(matfile);
        cd ..
        
        rfile = nptDir('rules.mat');
        if ~isempty(rfile);
            rules = load('rules.mat'); s = find(rules.r == r) + 1;
        elseif Args.ML;
            if Args.FixS;
                s = 3;
            elseif Args.Fix || Args.InCor;
                s = 1;
            else s = 1;
            end
        else
            s = 2;
        end
        if ~isempty(nptDir(sprintf('session0%d',s)))
            if (isempty(file) || Args.redo)
                cd(sprintf('session0%d',s))
                NeuroInfo = NeuronalChAssign;
                [channels,comb,CHcomb] = checkChannels('cohInter');
                if Args.surrogate
                    ch = channels;
                else
                    for c =1  : 2; ch(c) = find(pairg(c) == NeuroInfo.groups); end
                end
                mts = mtstrial('auto','redosetNames');
                rules = unique(mts.data.Index(:,1));
                if ~Args.tuning
                    if Args.InCor
                        if s ==1
                            trials = mtsgetTrials(mts,'BehResp',0,'stable','rule',r,'ML',modvarargin{:});
                        else
                            trials = mtsgetTrials(mts,'BehResp',0,'stable',modvarargin{:});
                        end
                    elseif Args.Fix || Args.FixS
                        if s ==1
                            trials = mtsgetTrials(mts,'ML','CueObj',55,'lowThresRT',[],modvarargin{:});
                        else
                            trials = mtsgetTrials(mts,'lowThresRT',[],modvarargin{:});
                        end
                    elseif ~isempty(Args.ndelay)
                        if s ==1
                            trials = mtsgetTrials(mts,'BehResp',1,'rule',r,'ML','noCharlie',modvarargin{:});
                        else
                            trials = mtsgetTrials(mts,'BehResp',1,modvarargin{:});
                        end
                    else
                        if s ==1
                            trials = mtsgetTrials(mts,'rule',r,'ML',modvarargin{:});
                        else
                            trials = mtsgetTrials(mts,modvarargin{:});
                        end
                    end
                    if ~isempty(trials) & length(trials) > 5
                        
                        if Args.surrogate
                            [Ct,phit,S12,S1,S2,t{1},f,before,thres] = MTScohgram(mts,trials,ch,'surrogate',modvarargin{:});
                            C{1} = squeeze(Ct);
                            phi{1} = squeeze(phit);
                        else
                            
                            [Ct,phit,S12,S1,S2,t{1},f] = MTScohgram(mts,trials,ch,modvarargin{:});
                            C{1} = squeeze(Ct);
                            phi{1} = squeeze(phit);
                            
                            
                            [Ct,phit,S12,S1,S2,t{2},f,beforeMatch] = MTScohgram(mts,trials,ch,'matchAlign',modvarargin{:});
                            C{2} = squeeze(Ct);
                            phi{2} = squeeze(phit);
                        end
                    end
                else
                    [tunePow,tuneCoh,variable,f,trials] = mkTunegram(mts,s,ch,r,modvarargin{:});
                end
                cd ..
                if ~isempty(trials)  & length(trials) > 5
                    sprintf('%s %s',pwd,matfile)
                    cd grams
                    if ~Args.tuning
                        if Args.surrogate
                            save(matfile,'C','phi','t','f','trials','thres')
                        else
                            save(matfile,'C','phi','t','f','trials','beforeMatch')
                        end
                        
                    else
                        save(matfile,'tunePow','tuneCoh','variable','f','trials')
                    end
                    cd ..
                end
            else
                cd grams
                load(matfile)
                cd ..
                
            end
            if nargout >= 1
                if exist('C') & exist('phi') & exist('t') && length(trials) >= Args.minTrials
                    
                    pair(count).therule(r).C = C;
                    
                    for cc =1 : 2; pair(count).therule(r).phi{cc} = mod(2*pi + phi{cc},2*pi);end
                    pair(count).therule(r).name = sprintf('%s/%s',pwd,matfile);
                    pair(count).therule(r).t = t;
                    pair(count).therule(r).ind = i;
                    sind{r} = [sind{r} i];
                else
                    pair(count).therule(r).C = [];
                    
                    pair(count).therule(r).phi = [];
                    pair(count).therule(r).name = [];
                    pair(count).therule(r).t = [];
                    pair(count).therule(r).ind = [];
                end
            end
        else
            pair(count).therule(r).C = [];
            
            pair(count).therule(r).phi = [];
            pair(count).therule(r).name = [];
            pair(count).therule(r).t = [];
            pair(count).therule(r).ind = [];
        end
    end
    
    %     if ~isempty(nptDir(sprintf('session0%d',s)))
    count = count + 1;
    %     end
    cd ..
    
end

if exist('pair')
    varargout{1} = pair;
    varargout{2} = sind;
end
if nargout >= 1
    align = {'Sample off-set aligned' 'Match aligned'};
    if exist('beforeMatch')
        ref = [1 beforeMatch/1000];
    else
        ref = [1 1.8];
    end
    rules = {'IDENTITY' 'LOCATION'};
    
    
    thet = [];
    
    clim = Args.clim;
    if ~isempty(Args.FigTit); h1 = figure; set(h1,'Name',Args.FigTit); end
    for therule = 1 : nr
        
        for cond = 1 : 2
            mil = [];
            for pa =  1 : length(ind);
                if ~isempty(pair(pa).therule(therule).C) && size(pair(pa).therule,2) >= therule
                    mil = [mil size(pair(pa).therule(therule).C{cond},1)];
                    t = pair(pa).therule(therule).t{cond};
                end;
            end;
            mil = min(mil);
            t = t(1:mil);
            tv{therule,cond} = t;
            clear thedata thephi;
            cpa = 1;
            for pa =1 : length(ind)
                if ~isempty(pair(pa).therule(therule).C) && size(pair(pa).therule,2) >= therule
                    thedata(cpa,:,:) = pair(pa).therule(therule).C{cond}(1:mil,1:33);
                    thephi(cpa,:,:) = pair(pa).therule(therule).phi{cond}(1:mil,1:33);
                    cpa = cpa + 1;
                end
            end;
            if exist('thedata')
                subplot(4,2,(therule-1) * 2 + cond)
                if Args.mean
                    imagesc(t,f,squeeze(mean(thedata,1))',[0 clim])
                else
                    imagesc(t,f,squeeze(median(thedata,1))',[0 clim])
                end
                colorbar
                hold on
                line([ref(cond) ref(cond)],Args.frange,'Color','r')
                if therule == 1; title(sprintf('%s; n=%d',align{cond},size(thedata,1))); end
                if cond == 1; ylabel(rules{therule}); end
                ylim(Args.frange)
                subplot(4,2,(therule-1) * 2 + cond + 4)
                if Args.varPhi
                    imagesc(t,f,squeeze(std(thephi,[],1))',[0 2.5])
                else
                    if Args.mean
                        imagesc(t,f,squeeze(circ_mean(thephi,[],1))',[0 Args.philim])
                    else
                        imagesc(t,f,squeeze(circ_median(thephi,1))',[0 Args.philim])
                    end
                end
                colorbar
                hold on
                line([ref(cond) ref(cond)],Args.frange,'Color','r')
                if therule == 1; title('Phase [rad.]'); end
                ylim(Args.frange)
                if cond == 1; ylabel(rules{therule}); end
                alldata{therule,cond} = thedata;
            end
            
        end
    end
    
    xlabel('Time [sec]')
    ylabel('Frequency [Hz]')
end
if exist('alldata') && exist('tv') && exist('f')
    varargout{3} = alldata;
    varargout{4} = tv;
    varargout{5} = f;
    if exist('beforeMatch');
        varargout{6} = beforeMatch;
    else
        varargout{6} = nan;
    end
end

function [tunePow,tuneCoh,variable,f,trials] = mkTunegram(mt,s,ch,r,varargin)

allloc = vecr(unique(mt.data.CueLoc));
allobj = unique(mt.data.CueObj);

for type = 1 : 3
    
    switch type
        case 1
            ncues = 3;
            arg{1} = 'allloc(cues)';
            arg{2} = 'allobj';
            var = 'loc';
        case 2
            ncues = 3;
            arg{1} = 'allloc';
            arg{2} = 'allobj(cues)';
            var = 'ide';
        case 3
            ncues = 9;
            cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
            arg{1} = 'allloc(cueComb(1,cues))';
            arg{2} = 'allobj(cueComb(2,cues))';
            var = 'allcues';
    end
    et = true;
    for cues = 1 : ncues
        if s ==1
            trials(type).list{cues} = single(mtsgetTrials(mt,'stable','BehResp',1,'rule',r,'ML','CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),varargin{:}));
        else
            trials(type).list{cues} = single(mtsgetTrials(mt,'stable','BehResp',1,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),varargin{:}));
        end
        if ~isempty(trials(type).list{cues})
            [C{cues},phi{cues},S12,S1,S2,t{cues},f,beforeC,S{cues},Cerr{cues}] = MTScohgram(mt,trials(type).list{cues},ch,'forTuning',varargin{:});
            mint(cues) = length(t{cues});
            C{cues} = single(squeeze(C{cues}));
            S{cues} = single(squeeze(S{cues}));
            phi{cues} = single(squeeze(phi{cues}));
            t{cues} = single(t{cues});
        else
            et = false;
        end
    end
    if et
        clear Cm Cs
        for c = 1 : ncues; Cm(:,:,c) = C{c}(1:min(mint),:); Cs(:,:,c) = Cerr{c}(2,1:min(mint),:) - Cerr{c}(1,1:min(mint),:); end
        
        Tc = std(Cm,[],3) ./ mean(Cs,3);
        %     imagesc(t{2},f,flipud(rot90(squeeze(tunegram(:,:)))))
        
        clear Sm Ss
        for c = 1 : ncues; Sm(:,:,:,c) = mean(S{c}(:,1:min(mint),:,:),4); Ss(:,:,:,c) = std(S{c}(:,1:min(mint),:,:),[],4); end
        
        Ts= std(Sm,[],4) ./ mean(Ss,4);
        
        tuneCoh(type).V = C;
        tunePow(type).V = S;
        
        variable(type).t = t;
        tuneCoh(type).phi = phi;
        tuneCoh(type).T = single(Tc);
        tunePow(type).T = single(Ts);
        variable(type).type = var;
    else
        tuneCoh(type).V = [];
        tunePow(type).V = [];
        
        variable(type).t = [];
        tuneCoh(type).phi = [];
        tuneCoh(type).T = [];
        tunePow(type).T = [];
        variable(type).type = [];
        trials(type).list{cues} = [];
        if ~exist('f'); f = []; end
        
    end
    
end
