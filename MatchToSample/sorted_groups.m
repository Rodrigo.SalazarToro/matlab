function [sortedGroups,sorted_pairs,group_pair_list,channel_pair_list] = sorted_groups(varargin)

%run at session level
%outputs a list of all pairs
%outputs a vector of pair numbers that have been sorted

Args = struct('ml',0,'bettynlynx',0);
Args.flags = {'ml','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sortedGroups = [];
sorted_pairs = [];
if Args.ml
    
    %get list of sorted groups
    g = nptDir('group*');
    numg = size(g,1);
    for ng = 1 : numg
        sortedGroups = [sortedGroups str2double(g(ng).name(end-3:end))];
    end
    
    
    %make list of all pairs
    if Args.bettynlynx
        N = NeuronalHist('ml','bettynlynx');
    else
        N = NeuronalHist('ml');
    end
    groups = N.gridPos;
    ngroups = size(groups,2);
    
    p = 0;
    for g1 = 1 : ngroups
        for g2 = (g1+1) : ngroups
            p = p + 1;
            group_pair_list(p,:) = [groups(g1) groups(g2)];
            channel_pair_list(p,:) = [g1 g2];
            if ~isempty(intersect(sortedGroups,groups(g1))) && ~isempty(intersect(sortedGroups,groups(g2)))
                sorted_pairs = [sorted_pairs p];
            end
        end
    end
else
        %get list of sorted groups
    g = nptDir('group*');
    numg = size(g,1);
    for ng = 1 : numg
        sortedGroups = [sortedGroups str2double(g(ng).name(end-3:end))];
    end
    
    %make list of all pairs
    N = NeuronalChAssign();
    groups = N.groups;
    ngroups = size(groups,2);
    
    p = 0;
    for g1 = 1 : ngroups
        for g2 = (g1+1) : ngroups
            p = p + 1;
            group_pair_list(p,:) = [groups(g1) groups(g2)];
            channel_pair_list(p,:) = [g1 g2];
            if ~isempty(intersect(sortedGroups,groups(g1))) && ~isempty(intersect(sortedGroups,groups(g2)))
                sorted_pairs = [sorted_pairs p];
            end
        end
    end
end
