function bayes_stimulus_classifier_LOOCV_all(varargin)

%WORKS RUN ON 090701 (total 53%)

%run at session level
%computes LOOCV
Args = struct('ml',0,'loc',0,'ide',0,'fix',0,'sample',0,'mi_cutoff',0,'cor_range',[],'test',0,'redo_corr_list',0,'perm',0,'pp',0,'pf',0,'ff',0,'select_pairs',[],'locbyide',[]);
Args.flags = {'ml','comb','loc','ide','test','redo_corr_list','perm','pp','pf','ff','fix','sample'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
c = mtscpp('auto','ml','ide_only');
num_points = 101 * 9;

%get stimulus list
if ~isempty(Args.locbyide)
    if Args.ml
        if Args.ide
            stimulus = stimulus_list('ml','ide','locbyide',Args.locbyide);
        elseif Args.loc
            stimulus = stimulus_list('ml','loc','locbyide',Args.locbyide);
        else
            stimulus = stimulus_list('ml','locbyide',Args.locbyide);
        end
    else
        if Args.ide
            stimulus = stimulus_list('ide','locbyide',Args.locbyide);
        elseif Args.loc
            stimulus = stimulus_list('loc','locbyide',Args.locbyide);
        else
            stimulus = stimulus_list('locbyide',Args.locbyide);
        end
    end
else
    if Args.ml
        if Args.ide
            stimulus = stimulus_list('ml','ide');
        elseif Args.loc
            stimulus = stimulus_list('ml','loc');
        else
            stimulus = stimulus_list('ml');
        end
    else
        if Args.ide
            stimulus = stimulus_list('ide');
        elseif Args.loc
            stimulus = stimulus_list('loc');
        else
            stimulus = stimulus_list;
        end
    end
end

if Args.perm
    stimulus=stimulus(randperm(size(stimulus,2)));
end


if ~isempty(Args.locbyide)
    if Args.ml
        if Args.fix
            out = corr_phase_list('ml','fix','locations',Args.locbyide);
        elseif Args.sample
            out = corr_phase_list('ml','sample','locations',Args.locbyide);
        else
            out = corr_phase_list('ml','locations',Args.locbyide);
        end
    else
        if Args.fix
            out = corr_phase_list('fix','locations',Args.locbyide);
        elseif Args.sample
            out = corr_phase_list('sample','locations',Args.locbyide);
        else
            out = corr_phase_list('locations',Args.locbyide);
        end
    end
else
    if Args.ml
        if Args.fix
            out = corr_phase_list('ml','fix');
        elseif Args.sample
            out = corr_phase_list('ml','sample');
        else
            out = corr_phase_list('ml');
        end
    else
        if Args.fix
            out = corr_phase_list('fix');
        elseif Args.sample
            out = corr_phase_list('sample');
        else
            out = corr_phase_list;
        end
    end
end
cd([sesdir filesep 'lfp' filesep 'lfp2']);

trial_corrcoefs = out.trial_corrcoefs;
trial_phases = out.trial_phases;
trial_correlograms = out.trial_correlograms


if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    num_trials = size(trial_corrcoefs{pair_list(1)},1);
else
    num_pairs = size(trial_corrcoefs,2);
    num_trials = size(trial_corrcoefs{1},1);
    pair_list = [1 : num_pairs];
end

if Args.ff
    [~,pair_list] = get(c,'Number','correct','stable','ff','sorted');
elseif Args.pp
    [~,pair_list] = get(c,'Number','correct','stable','pp','sorted');
elseif Args.pf
    [~,pair_list] = get(c,'Number','correct','stable','pf','sorted');
end

if ~isempty(Args.select_pairs)
    pair_list = Args.select_pairs;
end

numb_pairs = size(pair_list,2);


if isempty(Args.cor_range)
    cor_range = [-1:.1:1];
else
    cor_range = Args.cor_range;
end
cor_range = single(cor_range);
c_range = size(cor_range,2);

sum_post_probs = zeros(1,3);
all_joint_cond = cell(1,num_pairs);
leave_predict = [];

m = 0;
for leave = 1 : num_trials
    %make distributions of conditional probabilities P(r|s)
    mutual_info = zeros(num_pairs,num_points);
    predictions = zeros(num_pairs,num_points);
    probs = zeros(num_pairs,num_points);
    
    pair_counter = 0;
    for p = pair_list
        pair_counter = pair_counter + 1;
        if Args.loc || Args.ide || ~isempty(Args.locbyide)
            joint_cond_dist = cell(3,num_points);
            joint_cum_dist = cell(1,num_points);
        else
            joint_cond_dist = cell(9,num_points);
            joint_cum_dist = cell(1,num_points);
        end
        

            if isempty(all_joint_cond{pair_counter})
                %calculate conditional distributions for all trials
                for t = 1 : num_trials
                    for tp = 1 : num_points

                        point = single((trial_correlograms{p}{t}(tp)));
                        po = [];
                        if point >= min(cor_range) && point <= max(cor_range)
                            [~,po]=min(abs(cor_range - point));
                        end
                        
                        if isempty(joint_cond_dist{stimulus(t),tp})
                            joint_cond_dist{stimulus(t),tp} = zeros(1,c_range);
                        end
                        
                        if ~isempty(po)
                            joint_cond_dist{stimulus(t),tp}(po) = joint_cond_dist{stimulus(t),tp}(po) + 1;
                        end
                    end
                end
                all_joint_cond{pair_counter} = joint_cond_dist;
            end
            

        
        
        %gets joint conditional distribution with all trials
        joint_cond_dist = all_joint_cond{pair_counter};
        
        %remove information from the trial to be left out
        for tp = 1 : num_points
            point = single((trial_correlograms{p}{leave}(tp)));
            po = [];
            if point >= min(cor_range) && point <= max(cor_range)
                [~,po]=min(abs(cor_range - point));
            end
            joint_cond_dist{stimulus(leave),tp}(po) = joint_cond_dist{stimulus(leave),tp}(po) - 1; %subtract 1 from the conditional distribution
        end
        
        
        for stims = 1 : max(stimulus) % 3 or 9
            for tp = 1 : num_points
                if isempty(joint_cum_dist{1,tp})
                    joint_cum_dist{1,tp} = zeros(1,c_range);
                end
                
                joint_cond_dist{stims,tp} = joint_cond_dist{stims,tp} ./ nansum(joint_cond_dist{stims,tp});
                
                if Args.loc || Args.ide || ~isempty(Args.locbyide)
                    prob_sample = 1/3;
%                     stm = stimulus;stm(leave) = [];
%                     prob_sample = size(find(stm == stims),2) / (size(stm,2));
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                    stm = stimulus;stm(leave) = [];
                    prob_sample = size(find(stm == stims),2) / (size(stm,2));
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
            end
        end
        
        
        if Args.loc || Args.ide || ~isempty(Args.locbyide)
            stim_probabilities = zeros(3,num_points);
        else
            stim_probabilities = zeros(9,num_points);
        end
        
        for ntime = 1 : num_points
           
            point = single((trial_correlograms{p}{leave}(ntime)));
            po = [];
            if point >= min(cor_range) && point <= max(cor_range)
                [~,po]=min(abs(cor_range - point));
            end
            
            cum_prob = joint_cum_dist{1,ntime}(po);
            for stims = 1 : max(stimulus)
                cond_prob = joint_cond_dist{stims,ntime}(po);
                
                if Args.loc || Args.ide || ~isempty(Args.locbyide) 
                    prob_sample = 1/3;
%                     stm = stimulus;stm(leave) = [];
%                     prob_sample = size(find(stm == stims),2) / (size(stm,2));
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                    stm = stimulus;stm(leave) = [];
                    prob_sample = size(find(stm == stims),2) / (size(stm,2));
                end
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                if ~isempty(cum_prob) && ~isempty(cond_prob)
                    stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                end
            end
            %get rid of nans
            stim_probabilities(isnan(stim_probabilities)) = 0;
            
            if ~isempty(cum_prob)
                if cum_prob ~= 0
                    post_probs = stim_probabilities(:,ntime)';
                    mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                    mutual_info(pair_counter,ntime) = mi;
                    
                    if mi >= Args.mi_cutoff
                        [time_prob,predict] = max(post_probs);
                        predictions(pair_counter,ntime) = predict;
                        probs(pair_counter,ntime) = time_prob;
                    end
                end
            end
        end
        sum_post_probs = sum_post_probs + sum(stim_probabilities');
    end
    
    %determine best match
    bb = zeros(num_pairs,max(stimulus));
    for ps = 1 : num_pairs
        for xs = 1 : max(stimulus);
            bb(ps,xs) = sum(mutual_info(predictions(ps,:)  == xs));
        end
    end
    
    [~,pairpre] = max(bb');
    
    b = zeros(1,max(stimulus));
    for xs = 1 : max(stimulus);
        b(xs) = sum(mutual_info(predictions == xs));
    end
    [~,pre] = max(b);
    
    
    
    
%     ?\[~,pre] = max(sum_post_probs);
    
    
    leave_predict(leave) = pre;
    
    if pre == stimulus(leave)
        m = m + 1;
    end
    
    leave
    (m/leave) * 100
end
m

performance = (m/leave) * 100;
if ~Args.test && ~Args.perm
    if Args.fix
        if Args.ide
            save bsc_loocv_performance_ide_all_fix performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_ide_all_pp_fix performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_ide_all_pf_fix performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_ide_all_ff_fix performance leave_predict stimulus
            end
        elseif Args.loc
            save bsc_loocv_performance_loc_all_fix performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_loc_all_pp_fix performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_loc_all_pf_fix performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_loc_all_ff_fix performance leave_predict stimulus
            end
        else
            save bsc_loocv_performance_all_fix performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_all_pp_fix performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_all_pf_fix performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_all_ff_fix performance leave_predict stimulus
            end
        end
    elseif Args.sample
        if Args.ide
            save bsc_loocv_performance_ide_all_sample performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_ide_all_pp_sample performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_ide_all_pf_sample performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_ide_all_ff_sample performance leave_predict stimulus
            end
        elseif Args.loc
            save bsc_loocv_performance_loc_all_sample performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_loc_all_pp_sample performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_loc_all_pf_sample performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_loc_all_ff_sample performance leave_predict stimulus
            end
        else
            save bsc_loocv_performance_all_sample performance leave_predict stimulus
            if Args.pp
                save bsc_loocv_performance_all_pp_sample performance leave_predict stimulus
            elseif Args.pf
                save bsc_loocv_performance_all_pf_sample performance leave_predict stimulus
            elseif Args.ff
                save bsc_loocv_performance_all_ff_sample performance leave_predict stimulus
            end
        end
    else
        if Args.perm
            if Args.ide
                save bsc_loocv_performance_ide_all_perm performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_ide_all_pp_perm performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_ide_all_pf_perm performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_ide_all_ff_perm performance leave_predict stimulus
                end
            elseif Args.loc
                save bsc_loocv_performance_loc_all_perm performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_loc_all_pp_perm performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_loc_all_pf_perm performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_loc_all_ff_perm performance leave_predict stimulus
                end
            else
                save bsc_loocv_performance_all_perm performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_all_pp_perm performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_all_pf_perm performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_all_ff_perm performance leave_predict stimulus
                end
            end
        else
            if Args.ide
                save bsc_loocv_performance_ide_all performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_ide_all_pp performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_ide_all_pf performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_ide_all_ff performance leave_predict stimulus
                end
            elseif Args.loc
                save bsc_loocv_performance_loc_all performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_loc_all_pp performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_loc_all_pf performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_loc_all_ff performance leave_predict stimulus
                end
            else
                save bsc_loocv_performance_all performance leave_predict stimulus
                if Args.pp
                    save bsc_loocv_performance_all_pp performance leave_predict stimulus
                elseif Args.pf
                    save bsc_loocv_performance_all_pf performance leave_predict stimulus
                elseif Args.ff
                    save bsc_loocv_performance_all_ff performance leave_predict stimulus
                end
            end
        end
    end
end

cd(sesdir)
