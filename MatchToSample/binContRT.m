cd /Volumes/raid/data/monkey/betty/
load longdays

mtsC = ProcessDays(mtstrial,'days',lcdays,'sessions',{'session01'},'NoSites','redo','ML');
mtsB = ProcessDays(mtstrial,'days',lbdays,'sessions',{'session01'},'NoSites','redo','ML');

delays = [900 1666;1667 2333; 2334 3100];

for del = 1 : 3
    selTrialsC = mtsgetTrials(mtsC,'BehResp',1,'stable','delay',delays(del,:));
    selTrialsB = mtsgetTrials(mtsB,'BehResp',1,'stable','delay',delays(del,:));
    
    subplot(2,3,del);
    hist(mtsC.data.FirstSac(selTrialsC),[80:10:300]);
    line([mean(mtsC.data.FirstSac(selTrialsC)) mean(mtsC.data.FirstSac(selTrialsC))], [0 1000],'Color','r')
    xlim([80 300]);
    title(sprintf('Delay %d sec binned',del))
    
    subplot(2,3,del+3);
    hist(mtsB.data.FirstSac(selTrialsB),[80:10:300]);
    line([mean(mtsB.data.FirstSac(selTrialsB)) mean(mtsB.data.FirstSac(selTrialsB))],[0 1000],'Color','r')
    xlim([80 300])
    title(sprintf('Delay %d third continous',del))
    
end
xlabel('Time [ms]')
ylabel('count [trials]')