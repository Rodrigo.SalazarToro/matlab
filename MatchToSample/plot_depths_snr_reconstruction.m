function plot_depths_snr_reconstruction

load depths_snr_reconstruction

d = depths_snr_reconstruction.depths;
s = depths_snr_reconstruction.snr;
r = depths_snr_reconstruction.reconstruction;

for x = 1 : 64
    figure
    days = [1:16];
    for y = 1:16
        if d(y,x) ~= 0
            if strncmpi(r{y,x},'WM',2)
                scatter(s(y,x),days(y),'r')
                hold on
            else
                scatter(s(y,x),days(y),'k')
                hold on
            end
        end
    end
    
    
end
