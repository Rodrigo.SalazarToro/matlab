function [a,b,c,d,e,f]=Nlx2MAT_UI
%[a,b,c,d,e,f]=Nlx2MAT_UI
% quick user interface function to read event or csc Nlx files.
%
%for csc files the return parameters are:
%[TimeStamp, ChannelNum, SampleFrequency, NumberValidSamples, Samples,NlxHeader]
%
%for event files the return parameters are:
%[TimeStamp, EventIDs, Nttls, Extras, EventStrings, NlxHeader]
%



[f,p] = uigetfile({'*.ncs;*.nev'},'GET CSC or Event file');

filename = [p f];
%filename ='C:\CheetahData\2010-03-01_16-18-50\CSC2.ncs'

FieldSelectionArray=[1 1 1 1 1 ];

ExtractHeaderValue =1;

ExtractionMode = 1;

ExtractionModeArray = [];

if strcmp(filename(end-3:end),'.ncs')
    %[TimeStamp, ChannelNum, SampleFrequency, NumberValidSamples, Samples, NlxHeader]
    [a,b,c,d,e,f]= Nlx2MatCSC( filename, FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
else
    %[TimeStamp, EventIDs, Nttls, Extras, EventStrings, NlxHeader]
    [a,b,c,d,e,f]= Nlx2MatEV( filename, FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
end
