function obj = plot(obj,varargin)

%Arguments
%phase: plots the phase of the coherence
%ide:   plots all 5 stimuli

Args = struct('phase',0,'ide',0,'sur',0,'surthresh',1);
Args.flags = {'phase','ide','sur'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end


%load cohgram info
load(obj.data.setNames{ind})
sday = strfind(obj.data.setNames{ind},'/');
ns = size(sday,2);
day = obj.data.setNames{ind}(sday(ns-4)+1:sday(ns-3)-1);

%load globablsurrogate
load([obj.data.setNames{ind}(1:sday(end)) 'globalsurcohgram'])

if Args.sur
    daynum = strmatch(day,obj.data.days);
    global_sur = obj.data.global_surrogate{daynum};
    gs = global_sur{Args.surthresh}; %(1).999 (2).9999 (3).99999
end

if Args.ide
    
    for x = 1:5
        subplot(5,1,x)
        
        time = round(idecohgram.t{x} * 1000);
        time = time(1:41);
        ntime = size(time,2);
        
        freq = fliplr(round(idecohgram.f{x}));
        nfreq = size(freq,2);
        
        if Args.phase
            cgram = idecohgram.phi{x}((1:41),:)';
        else
            cgram = idecohgram.C{x}((1:41),:)';
        end
        
        cgram = flipud(cgram);
        
        cgram = interp2(cgram);
        
        imagesc(cgram)
        
        set(gca,'XTick',[1:4:(ntime*2)-1])
        set(gca,'XTickLabel',time(1:2:ntime))
        
        set(gca,'YTick',[1:8:(nfreq*4)-1])
        set(gca,'YTickLabel',freq([1:4:nfreq]))
        
        %sample on
        hold on
        plot([17, 17],[0 100],'color','r')
        
        %sample off
        hold on
        plot([37, 37],[0 100],'color','r')
        
        %earliest match
        hold on
        plot([69, 69],[0 100],'color','r')
        
        hold on
        colorbar
        % set(gca, 'CLim', [0 .5]);
            title([day '   ntrials = ' num2str(size(idecohgram.trials{x},2)) '      groups:  ' num2str(idecohgram.groups{x})])
    end
    
    
else
    
    time = round(cohgram.t{1} * 1000);
    time = time(1:41);
    ntime = size(time,2);
    
    freq = fliplr(round(cohgram.f{1}));
    nfreq = size(freq,2);
    
    if Args.phase
        cgram = cohgram.phi{1}((1:41),:)';
    else
        cgram = cohgram.C{1}((1:41),:)';
    end
    
    
    
    
    sigim = zeros(size(cgram,1),size(cgram,2));
    if Args.sur
        subplot(2,1,1)
        sigim(find(cgram >= gs)) = 1;
        sigim = flipud(sigim);
        sigim = interp2(sigim);
        imagesc(sigim);colorbar
        
        subplot(2,1,2)
    end
    cgram = flipud(cgram);
    cgram = interp2(cgram); % INTERPOLATE
    imagesc(cgram)
    
    set(gca,'XTick',[1:4:(ntime*2)-1])
    set(gca,'XTickLabel',time(1:2:ntime))
    
    set(gca,'YTick',[1:4:(nfreq*2)-1])
    set(gca,'YTickLabel',freq([1:2:nfreq]))
    
    %sample on
    hold on
    plot([17, 17],[0 100],'color','k')
    
    %sample off
    hold on
    plot([37, 37],[0 100],'color','k')
    
    %earliest match
    hold on
    plot([69, 69],[0 100],'color','k')
    
    hold on
    colorbar
    % set(gca, 'CLim', [0 .5]);
    ylim([33 65])
    
    
    %get the areas
    hareas = reverse_categorizeNeuronalHistBMF(obj.data.Index(ind,40:41));
    
    title([day '   ntrials = ' num2str(size(cohgram.trials{1},2)) '      groups:  ' num2str(cohgram.groups{1}) '     areas: ' hareas{1} '   ' hareas{2}])
end





