function obj = bmfCohgram(varargin)

%makes bmfCohgram object

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'bmfCohgram';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'bmfCohgram';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

sesdir = pwd;
N = NeuronalHist('bmf');
[gg,~,pairs] = bmf_groups; %get list of good groups
cd([sesdir filesep 'lfp'])
lfpdir = pwd;

if exist('cohgrams','dir')
    cd([lfpdir filesep 'cohgrams'])
    cohdir = pwd;
    
    nfiles = size(pairs,1);
    if ~isempty(['cohgram' num2strpad(pairs(1,1),3),num2strpad(pairs(1,2),3)])
        for f = 1 : nfiles
            load(['cohgram' num2strpad(pairs(f,1),3),num2strpad(pairs(f,2),3)])
            
            %get groups
            g1 = cohgram.groups{1}(1);
            g2 = cohgram.groups{1}(2);
            data.Index(f,1) = g1;
            data.Index(f,2) = g2;
            
            %determine if both groups are good
            if ~isempty(intersect(gg,g1)) && ~isempty(intersect(gg,g2))
                data.Index(f,3) = 1;
            end
            data.setNames{f,1} = [cohdir filesep ['cohgram' num2strpad(pairs(f,1),3),num2strpad(pairs(f,2),3)]];
            
            %load information about epochs compared to global threshold
%             load(['epochthreshglobal' num2strpad(pairs(f,1),3),num2strpad(pairs(f,2),3)])
            
            load(['epochthresh' num2strpad(pairs(f,1),3),num2strpad(pairs(f,2),3)])
            
            %number sig bins
            data.Index(f,4) = sig.fcount(1,1); %epoch 1,freq band 1
            data.Index(f,5) = sig.fcount(1,2);
            data.Index(f,6) = sig.fcount(1,3);
            data.Index(f,7) = sig.fcount(1,4);
            data.Index(f,8) = sig.fcount(2,1);  %epoch 2,freq band 1
            data.Index(f,9) = sig.fcount(2,2);
            data.Index(f,10) = sig.fcount(2,3);
            data.Index(f,11) = sig.fcount(2,4);
            data.Index(f,12) = sig.fcount(3,1);  %epoch 3,freq band 1
            data.Index(f,13) = sig.fcount(3,2);
            data.Index(f,14) = sig.fcount(3,3);
            data.Index(f,15) = sig.fcount(3,4);
            data.Index(f,16) = sig.fcount(4,1); %epoch 4,freq band 1
            data.Index(f,17) = sig.fcount(4,2);
            data.Index(f,18) = sig.fcount(4,3);
            data.Index(f,19) = sig.fcount(4,4);
            
            %means
            data.Index(f,20) = sig.fmean(1,1); %epoch 1,freq band 1
            data.Index(f,21) = sig.fmean(1,2);
            data.Index(f,22) = sig.fmean(1,3);
            data.Index(f,23) = sig.fmean(1,4);
            data.Index(f,24) = sig.fmean(2,1);  %epoch 2,freq band 1
            data.Index(f,25) = sig.fmean(2,2);
            data.Index(f,26) = sig.fmean(2,3);
            data.Index(f,27) = sig.fmean(2,4);
            data.Index(f,28) = sig.fmean(3,1);  %epoch 3,freq band 1
            data.Index(f,29) = sig.fmean(3,2);
            data.Index(f,30) = sig.fmean(3,3);
            data.Index(f,31) = sig.fmean(3,4);
            data.Index(f,32) = sig.fmean(4,1); %epoch 4,freq band 1
            data.Index(f,33) = sig.fmean(4,2);
            data.Index(f,34) = sig.fmean(4,3);
            data.Index(f,35) = sig.fmean(4,4);
            
            data.Index(f,36) = sig.ftotalbins(1,1); %get information about how man bins are in each frequency range
            data.Index(f,37) = sig.ftotalbins(1,2);
            data.Index(f,38) = sig.ftotalbins(1,3);
            data.Index(f,39) = sig.ftotalbins(1,4);
            
            %find wich channel number the group corresponds
            %get histology information for both groups
            [~,gp1] = intersect(N.gridPos,g1);
            [~,gp2] = intersect(N.gridPos,g2);
            data.Index(f,40) = N.number(gp1); %this is the number for the specific area
            data.Index(f,41) = N.number(gp2);
            data.Index(f,42) = N.level2(gp1); %this is the number for the category (ie; prefrontal, occipital etc.)
            data.Index(f,43) = N.level2(gp2);

        end
        
        data.numSets = nfiles;
        
        %add surrogate information
        if exist('globalsurcohgram_thresh.mat','file')
            load globalsurcohgram_thresh.mat
            data.global_surrogate = {fitthresh};
        else
            data.global_surrogate = {};
        end
%         data.days = {daydir(end-5:end)};
        
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('*no chgram files files missing \n');
        obj = createEmptyObject(Args);
    end
else
    fprintf('EpochCoh directory missing \n');
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
