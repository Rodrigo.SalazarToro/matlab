function compareEPrule(varargin)


% to be run in the day folder



Args = struct('ML',0,'redo',0,'chosenFreq',1,'epoch','saccade');
Args.flags = {'ML','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);





sdir = pwd;

pathTosave = cell(2,1);
data = cell(2,1);
matfile = sprintf('compEPRules%s.mat',Args.epoch);
if Args.ML
    for r = 1 : 2
        cd(sdir)
        cd('session01')
        mts = mtstrial('auto','redosetNames');
        trials = mtsgetTrials(mts,'stable','BehResp',1,'ML');
        [~,~,unctrials] = checkFirstSac;
        trials = setdiff(trials,unctrials);
        cd lfp
        pathTosave{r} = pwd;
        
        files = nptDir('*_lfp.*');
        SR = 200;
        switch Args.epoch
            case 'presample'
                beforeSample = [-400 0];
                timing = [mts.data.CueOnset + beforeSample(1) mts.data.CueOnset + beforeSample(2)];
                Mbins = [beforeSample(1) : 1000/SR : beforeSample(2)];
            case 'sample'
                beforeSample = [100 500];
                timing = [mts.data.CueOnset + beforeSample(1) mts.data.CueOnset + beforeSample(2)];
                Mbins = [beforeSample(1) : 1000/SR : beforeSample(2)];
            case 'delay1'
                afterSample = [500 900];
                timing = [mts.data.CueOffset + afterSample(1) mts.data.CueOffset + afterSample(2)];
                Mbins = [afterSample(1) : 1000/SR : afterSample(2)];
            case 'delay2'
                afterSample = [-400 0];
                timing = [mts.data.MatchOnset + afterSample(1) mts.data.MatchOnset + afterSample(2)];
                Mbins = [afterSample(1) : 1000/SR : afterSample(2)];
            case 'saccade'
                aroundMatch = [-100 200];
                timing = [mts.data.MatchOnset + aroundMatch(1) mts.data.MatchOnset + aroundMatch(2)];
                Mbins = [aroundMatch(1) : 1000/SR : aroundMatch(2)];
        end
        
        for tr = 1 : length(trials)
            
            [datat,~,~,~,~] = nptReadStreamerFile(files(trials(tr)).name);
            for cc = 1 : size(datat,1)
                [datatt,SR] = preprocessinglfp(datat(cc,:),'No60Hz',1,'detrend',modvarargin{:});
                
                data{r}(tr,cc,:) = datatt(ceil(timing(trials(tr),1)*SR / 1000) : ceil(timing(trials(tr),2)*SR / 1000));
                
            end
        end
        
        
        
    end
    
    
else
    load rules.mat
    for s = 2 : 3
        cd(sdir)
        cd(['session0' num2str(s)])
        mts = mtstrial('auto','redosetNames');
        trials = mtsgetTrials(mts,'stable','BehResp',1);
        [~,~,unctrials] = checkFirstSac;
        trials = setdiff(trials,unctrials);
        cd lfp
        pathTosave{s-1} = pwd;
        
        files = nptDir('*_lfp.*');
        SR = 200;
        switch Args.epoch
            case 'presample'
                beforeSample = [-400 0];
                timing = [mts.data.CueOnset + beforeSample(1) mts.data.CueOnset + beforeSample(2)];
                Mbins = [beforeSample(1) : 1000/SR : beforeSample(2)];
            case 'sample'
                beforeSample = [100 500];
                timing = [mts.data.CueOnset + beforeSample(1) mts.data.CueOnset + beforeSample(2)];
                Mbins = [beforeSample(1) : 1000/SR : beforeSample(2)];
            case 'delay1'
                afterSample = [500 900];
                timing = [mts.data.CueOffset + afterSample(1) mts.data.CueOffset + afterSample(2)];
                Mbins = [afterSample(1) : 1000/SR : afterSample(2)];
            case 'delay2'
                afterSample = [-400 0];
                timing = [mts.data.MatchOnset + afterSample(1) mts.data.MatchOnset + afterSample(2)];
                Mbins = [afterSample(1) : 1000/SR : afterSample(2)];
            case 'saccade'
                aroundMatch = [-100 200];
                timing = [mts.data.MatchOnset + aroundMatch(1) mts.data.MatchOnset + aroundMatch(2)];
                Mbins = [aroundMatch(1) : 1000/SR : aroundMatch(2)];
        end
        
        for tr = 1 : length(trials)
            
            [datat,~,~,~,~] = nptReadStreamerFile(files(trials(tr)).name);
            for cc = 1 : size(datat,1)
                [datatt,SR] = preprocessinglfp(datat(cc,:),'No60Hz',1,'detrend',modvarargin{:});
                
                data{s-1}(tr,cc,:) = datatt(ceil(timing(trials(tr),1)*SR / 1000) : ceil(timing(trials(tr),2)*SR / 1000));
                
            end
        end
        
        
        
    end
end


pval = ones(size(data{1},3),size(data{1},2));

for bi = 1 : size(data{1},3)
    [~,pval(bi,:)] = ttest2(squeeze(data{1}(:,:,bi)),squeeze(data{2}(:,:,bi)));
end


for ss = 1 : 2
    cd(pathTosave{ss})
    save(matfile,'pval','Mbins')
    display([pwd '/' matfile])
end
cd(sdir)