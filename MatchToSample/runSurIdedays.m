% to be run in the monkey name folder

load idedays

sdir = pwd;

dbstop if error

%% for Betty
if isempty(findstr(pwd,'clark'))
    
    
    for d = 1 : length(idedays); idedays{d} = [pwd '/' sprintf('%s/',idedays{d})]; end
    % command to remove if local parallel does not work:
    matlabpool 4
    %
    parfor d = 1 : length(idedays); % change parfor to for
        cd(idedays{d});
        
        cd session01
        [status, result] = unix('find . -name ''skip.txt''');
        
        if isempty(result)
            
            makeFourierTransformSurrogate('ML')
            
        end
        cd ../..
    end
    
else
    
    %% for Clark
    % for d = 4 : length(idedays);
    for d = 4 : length(idedays); idedays{d} = [pwd '/' sprintf('%s/',idedays{d})]; end
    matlabpool  local 
    parfor d = 1 : length(idedays);
        cd(idedays{d});
        if isempty(nptDir('skip.txt'));
            
            sessions = nptDir('session0*');
            firsts = 2;
            
            for s = firsts : length(sessions)
                cd(sessions(s).name)
                [status, result] = unix('find . -name ''skip.txt''');
                
                
                makeFourierTransformSurrogate
                
                
                cd ..
            end
            
        end
        %cd ..
    end
    
    
end