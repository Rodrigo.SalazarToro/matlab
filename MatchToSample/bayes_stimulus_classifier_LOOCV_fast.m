function bayes_stimulus_classifier_LOOCV_fast(varargin)

%WORKS RUN ON 090701 (total 53%)

%run at session level
%computes LOOCV
Args = struct('ml',0,'loc',0,'ide',0,'mi_cutoff',0,'cor_range',[],'phase_range',[],'test',0,'sample',0,'perm',0,'fix',0);
Args.flags = {'ml','comb','loc','ide','test','sample','perm','fix'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%get stimulus list
if Args.ide
    stimulus = stimulus_list('ml','ide');
elseif Args.loc
    stimulus = stimulus_list('ml','loc');
else
    stimulus = stimulus_list('ml');
end

%make an even number of stimuli
for ns = 1 : max(stimulus)
    nstims(ns) = size(find(stimulus == ns),2);
end
minstim = min(nstims);

del = [];
for ns = 1 : max(stimulus)
    st = find(stimulus == ns);
    st = st(randperm(nstims(ns)));
    del = [del st(1 : nstims(ns) - minstim)];
end
                                                                           del = []
if Args.perm
    stimulus=stimulus(randperm(size(stimulus,2)));
end


if Args.ml
    if Args.fix
        out = corr_phase_list('ml','fix');
    elseif Args.sample
        out = corr_phase_list('ml','sample');
    else
        out = corr_phase_list('ml');
    end
else
    if Args.fix
        out = corr_phase_list('fix');
    elseif Args.sample
        out = corr_phase_list('sample');
    else
        out = corr_phase_list;
    end
end
cd([sesdir filesep 'lfp' filesep 'lfp2']);

trial_corrcoefs = out.trial_corrcoefs;
trial_phases = out.trial_phases;



if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    [num_trials,num_points] = size(trial_corrcoefs{pair_list(1)});
else
    num_pairs = size(trial_corrcoefs,2);
    [num_trials,num_points] = size(trial_corrcoefs{1});
    pair_list = [1 : num_pairs];
end

trials = 1 : num_trials;
trials(del) = [];

if isempty(Args.cor_range)
    cor_range = [0:.1:1];
else
    cor_range = Args.cor_range;
end
cor_range = single(cor_range);
c_range = size(cor_range,2);

if isempty(Args.phase_range)
    ph_range = [-50:1:50];
else
    ph_range = Args.phase_range;
end
ph_range = single(ph_range);
s_phase = size(ph_range,2);



all_joint_cond = cell(1,num_pairs);
stim_mmi = cell(1,max(stimulus));
leave_predict = [];
correct_stims = zeros(1,max(stimulus));
correct_timepoint = zeros(1,num_points);
if Args.ide || Args.loc
    correct_stimulus_maps = cell(1,3);
    sum_post_probs = zeros(1,3);
else
    correct_stimulus_maps = cell(1,9);
    sum_post_probs = zeros(1,9);
end
correct_index = zeros(1,num_trials);
all_pair_predictions = zeros(1,num_pairs);
m = 0;
leave_counter = 0;
for leave = trials
    leave_counter = leave_counter + 1;
    %make distributions of conditional probabilities P(r|s)
    all_joint_cum = cell(1,num_pairs);
    pair_counter = 0;
    for p = pair_list
        pair_counter = pair_counter + 1;
        if Args.loc || Args.ide
            joint_cond_dist = cell(3,9);
            joint_cum_dist = cell(1,9);
        else
            joint_cond_dist = cell(9,9);
            joint_cum_dist = cell(1,9);
        end
        
        %runs first time through only
        if isempty(all_joint_cond{pair_counter})
            %calculate conditional distributions for all trials
            for t = trials
                for tp = 1 : num_points
                    
                    point = single(abs(trial_corrcoefs{p}(t,tp)));
                    po = [];
                    if point >= min(cor_range) && point <= max(cor_range)
                        [~,po]=min(abs(cor_range - point));
                    end
                    
                    phase_point = single(trial_phases{p}(t,tp));
                    ph = [];
                    if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                        [~,ph]=min(abs(ph_range - phase_point));
                    end
                    
                    if isempty(joint_cond_dist{stimulus(t),tp})
                        joint_cond_dist{stimulus(t),tp} = zeros(s_phase,c_range);
                    end
                    
                    if ~isempty(ph) && ~isempty(po)
                        joint_cond_dist{stimulus(t),tp}(ph,po) = joint_cond_dist{stimulus(t),tp}(ph,po) + 1;
                    end
                end
            end
            all_joint_cond{pair_counter} = joint_cond_dist;
        end
        
        
        %gets joint conditional distribution with all trials
        joint_cond_dist = all_joint_cond{pair_counter};
        
        %remove information from the trial to be left out
        for tp = 1 : num_points
            
            point = single(abs(trial_corrcoefs{p}(leave,tp)));
            po = [];
            if point >= min(cor_range) && point <= max(cor_range)
                [~,po]=min(abs(cor_range - point));
            end
            
            phase_point = single(trial_phases{p}(leave,tp));
            ph = [];
            if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                [~,ph]=min(abs(ph_range - phase_point));
            end
            
            joint_cond_dist{stimulus(leave),tp}(ph,po) = joint_cond_dist{stimulus(leave),tp}(ph,po) - 1; %subtract 1 from the conditional distribution

        end
        
        
        for stims = 1 : max(stimulus) % 3 or 9
            for tp = 1 : num_points
                if isempty(joint_cum_dist{1,tp})
                    joint_cum_dist{1,tp} = zeros(s_phase,c_range);
                end
                
                joint_cond_dist{stims,tp} = joint_cond_dist{stims,tp} ./ sum(nansum(joint_cond_dist{stims,tp}));
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    prob_sample = 1/9;
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
            end
        end
        all_joint_cum{pair_counter} = joint_cum_dist;
        leave_all_joint_cond{pair_counter} = joint_cond_dist;
    end
    
    pair_counter = 0;
    mutual_info = zeros(size(pair_list,2),9);
    predictions = zeros(size(pair_list,2),9);
    probs = zeros(size(pair_list,2),9);
    for nump = pair_list
        pair_counter = pair_counter + 1;
        if Args.loc || Args.ide
            stim_probabilities = zeros(3,9);
        else
            stim_probabilities = zeros(9,9);
        end
        
        for ntime = 1 : num_points
            
            point = single(abs(trial_corrcoefs{nump}(leave,ntime)));
            po = [];
            if point >= min(cor_range) && point <= max(cor_range)
                [~,po]=min(abs(cor_range - point));
            end
            
            phase_point = single(trial_phases{nump}(leave,ntime));
            ph = [];
            if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                [~,ph]=min(abs(ph_range - phase_point));
            end
            
            cum_prob = all_joint_cum{pair_counter}{1,ntime}(ph,po);
            for stims = 1 : max(stimulus)
                cond_prob = leave_all_joint_cond{pair_counter}{stims,ntime}(ph,po);
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    prob_sample = 1/9;
                end
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                if ~isempty(cum_prob) && ~isempty(cond_prob)
                    stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                end
            end
            %get rid of nans
            stim_probabilities(isnan(stim_probabilities)) = 0;
            
            if ~isempty(cum_prob)
                %make sure all conditional probabilities ~= 0 along with the cumulative probability
                if cum_prob ~= 0 %&& sum(stim_probabilities(:,ntime) ~= 0) == 3
                    post_probs = stim_probabilities(:,ntime)';
                    mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                    mutual_info(pair_counter,ntime) = mi;
                    
                    if mi >= Args.mi_cutoff
                        [time_prob,predict] = max(post_probs);
                        predictions(pair_counter,ntime) = predict;
                        probs(pair_counter,ntime) = time_prob;
                    end
                end
            end
        end
        stmp = stim_probabilities;
        stmp(stmp == 1) = 0;
        sum_post_probs = sum_post_probs + max(stmp');
    end
    
    %single pair predictions
    pair_mi = zeros(num_pairs,max(stimulus));
    for each_pair = 1 : num_pairs
        for ep = 1 : max(stimulus);
            pair_mi(each_pair,ep) = sum(mutual_info(each_pair,predictions(each_pair,:)  == ep));
        end
    end
    
    [~,pp]=max(pair_mi');
    
    all_pair_predictions = [all_pair_predictions + (pp == stimulus(leave))];
    
    
    %determine best match
    b = zeros(1,max(stimulus));
    for xs = 1 : max(stimulus);
        b(xs) = sum(mutual_info(find(predictions == xs)));
    end
    [~,pre] = max(b);
    
    
    [~,pree] = max(sum_post_probs);
    leave_predict(leave) = pre;
    
    if pre == stimulus(leave)
        m = m + 1;
        
        xmi = stimulus(leave);
        mmi = mutual_info;
        mmi(predictions ~= xmi) = 0;
        if isempty(stim_mmi{xmi})
            stim_mmi{xmi} = zeros(num_pairs,num_points);
        end
        stim_mmi{xmi} = [stim_mmi{xmi} + mmi];
        
        correct_stims(stimulus(leave)) = correct_stims(stimulus(leave)) + 1;
        
        
        if isempty(correct_stimulus_maps{pre})
            correct_stimulus_maps{pre} = (predictions == pre);
        else
            correct_stimulus_maps{pre} = [correct_stimulus_maps{pre} + (predictions == pre)];
        end
    end
    
    leave_counter
    (m/leave_counter) * 100
    
    
    %predict for each time bin
    for tt = 1 : num_points
        %determine best match
        b = zeros(1,max(stimulus));
        for xs = 1 : max(stimulus);
            b(xs) = sum(mutual_info(find(predictions(:,tt)  == xs)));
        end
        [~,pre] = max(b);
        if pre == stimulus(leave)
            correct_timepoint(tt) = correct_timepoint(tt) + 1;
            correct_index(leave) = 1;
        end
    end
    
    
    % for x = 1:27;subplot(3,9,x);surf(joint_cond_dist{x});axis([1 5 0 100 0 .07]);end
    
    
end
m

for xmi = 1 : max(stimulus)
    stim_mmi{xmi} = stim_mmi{xmi} ./ correct_stims(xmi);
end


performance = (m/leave_counter) * 100;
if ~Args.test
    if Args.sample
        if Args.ide
            save bsc_loocv_performance_ide_fast_sample performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        elseif Args.loc
            save bsc_loocv_performance_loc_fast_sample performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        else
            save bsc_loocv_performance_fast_sample performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        end
    elseif Args.fix
        if Args.ide
            save bsc_loocv_performance_ide_fast_fix performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        elseif Args.loc
            save bsc_loocv_performance_loc_fast_fix performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        else
            save bsc_loocv_performance_fast_fix performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        end
    elseif Args.perm
        if Args.ide
            save bsc_loocv_performance_ide_fast_perm performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        elseif Args.loc
            save bsc_loocv_performance_loc_fast_perm performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        else
            save bsc_loocv_performance_fast_perm performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        end
    else
        if Args.ide
            save bsc_loocv_performance_ide_fast performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        elseif Args.loc
            save bsc_loocv_performance_loc_fast performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        else
            save bsc_loocv_performance_fast performance cor_range ph_range stim_mmi leave_predict correct_stims stimulus correct_timepoint correct_index correct_stimulus_maps all_pair_predictions
        end
    end
end

cd(sesdir)
