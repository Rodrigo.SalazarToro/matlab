function [sur_lower sur_upper] = fit_sta_globalsurrogate(varargin)

%run at lfp2 level

Args = struct('num_spikes',[],'upper',[],'lower',[]);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

log_spikes = log(Args.num_spikes);

load global_sta
s = size(increments,2);
p = prctile(global_sta',[Args.lower Args.upper]);
log_increments = log(increments)';
log_p_upper = log(p(2,:))';
log_p_lower = log(p(1,:))';


%calculate upper surrogate
b_upper = [ones(s,1) log_increments] \ log_p_upper;
intercept = b_upper(1);
slope = b_upper(2);
log_sur = log_spikes*slope + intercept;

sur_upper = exp(log_sur);


%calculate lower surrogate
b_lower = [ones(s,1) log_increments] \ log_p_lower;
intercept = b_lower(1);
slope = b_lower(2);
log_sur = log_spikes*slope + intercept;

sur_lower = real(exp(log_sur)); %keep only real part





% plot(increments,p(1,:),'k')
% hold on
% plot(increments,p(2,:),'r')
% hold on
% scatter(Args.num_spikes,sur_lower);
% hold on
% scatter(Args.num_spikes,sur_upper);