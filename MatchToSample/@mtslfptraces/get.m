function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'TrialTraces',0,'SessionBySession',0,'Location',0,'Identity',0,'Threshold',[],'Parameter',[],'Condition','>');
Args.flags = {'Number','SessionBySession','ObjectLevel','TrialTraces'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

SetIndex = obj.data.Index;


if Args.Number

    if Args.SessionBySession
        % return total number of events
        r = length(obj.data.setNames);
        % find the transition point between sessions
        sdiff = diff(SetIndex(:,2));
        stransition = [0; vecc(find(sdiff))];
        stransition = [stransition; length(SetIndex)];
        rind = [];
        cvalue = 0;
        for idx = 1:r
            value = vecc((stransition(idx)+1):stransition(idx+1));
            cvalue = cvalue(end) + value;
            % grab the indices corresponding to session idx
            rind = [rind; [repmat(idx,length(value),1) value cvalue]];
        end

    elseif Args.Location
        LocTrial = find(cuelocation == Args.Location);
        rind = SetIndex(LocTrial,[2 3 4 6]);

    elseif Args.Identity
        ObjTrial = find(cueobject == Args.Identity);
        rind = SetIndex(ObjTrial,[2 3 4 6]);

    else % case where all the data are given in block
        rind = [ones(size(SetIndex,1),1) SetIndex(:,[3 4 6])];

    end

    ind = mtsgetTrials(obj,varargin{:});
    combined = intersect(ind,rind(:,4));
    rind = rind(combined,:);

    if ~isempty(Args.Threshold) % needs to be done the idea is that one can use the threshold in addition to the following selection of data.
        measure = eval(sprintf('obj.data.%s',Args.Parameter));
        [rawindices,colindices] = eval(sprintf('find(measure %s %d)',Args.Condition,Args.Threshold));
        rindtemp = SetIndex(rawindices,[2 3 4 6]);
        rindlast = [];
        for i = 1 : size(rind,1)
            I = find(rindtemp(:,2) == rind(i,2));
            if ~isempty(I)
                rindlast = [rindlast; rind(i,:)];
            end
        end

    else
        rindlast = rind;
    end
    rt = unique(rindlast(:,2));
    cumt = 1;
    for t = 1 : length(rt)
        ct = rt(t);
        I = find(rindlast(:,2) == ct);
        rindlast(I,2) = cumt;
        cumt = cumt + 1;
    end
    if Args.SessionBySession
        r = length(unique(rind(:,1)));

    elseif Args.TrialTraces
        r = length(unique(rind(:,2)));
    else
        r = length(rindlast);
    end
    varargout(1) = {rindlast};

elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});

end

