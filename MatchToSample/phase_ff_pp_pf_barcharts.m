function phase_ff_pp_pf_barcharts(varargin)


Args = struct(); %xcthresh = unimodal_thresh_corrcoef
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/raid/data/monkey/betty/')
% load alldays
% cpp_b = processDays(mtscpp2,'days',alldays,'ml','NoSites')
% save cpp_b cpp_b
load cpp_b

cd('/media/raid/data/monkey/clark/')
% load idedays
% cpp_b = processDays(mtscpp2,'days',idedays,'NoSites')
% save cpp_c cpp_c
load cpp_c


epoch1 = 54; %identity delay


%% PFC
[i ii] = get(cpp_b,'Number','ml','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','ff');
[ic iic] = get(cpp_c,'Number','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','ff');

pangles_b = (cpp_b.data.Index(ii,epoch1));
pangles_c = (cpp_c.data.Index(iic,epoch1));

subplot(2,3,1)
hist(pangles_b,[-180:10:180])
hold on
title('PFC')

subplot(2,3,4)
hist(pangles_c,[-180:10:180])
hold on
title('PFC')


%% PPC
[i ii] = get(cpp_b,'Number','ml','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','pp');
[ic iic] = get(cpp_c,'Number','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','pp');

pangles_b = (cpp_b.data.Index(ii,epoch1));
pangles_c = (cpp_c.data.Index(iic,epoch1))

subplot(2,3,2)
hist(pangles_b,[-180:10:180])
hold on
title('PPC')

subplot(2,3,5)
hist(pangles_c,[-180:10:180])
hold on
title('PPC')


%% PFC-PPC
[i ii] = get(cpp_b,'Number','ml','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','pf');
[ic iic] = get(cpp_c,'Number','cross_corr',.99,'hsfpp_criteria','corr_mag_criteria','pf');

pangles_b = (cpp_b.data.Index(ii,epoch1));
pangles_c = (cpp_c.data.Index(iic,epoch1))

subplot(2,3,3)
hist(pangles_b,[-180:10:180])
hold on
title('PFC-PPC')

subplot(2,3,6)
hist(pangles_c,[-180:10:180])
hold on
title('PFC-PPC')


