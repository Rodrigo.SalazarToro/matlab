function [perf,ti,varargout] = getperformance(varargin)
% To be run inthe session directory. This fct computes the performance of
% a session by using a sliding trial window and put the performance at the
% end of the window. It also determines stable and transition
% phases over one session. It uses a performance threshold and a minimum
% number of trials to be above this threshold to be considered as stable.
% The resting trials are considered as transition.
%
% the arguments to be used are:
%
% - 'threshold' sets the level of the performance where the trials will be
% considered as stable or transition phases. defauls: 80 % correct
% responses
%
% - 'window' sets the number of trials used to compute the performance.
% defaults: 100 trials. This parameter will also set the limit of the first
% "transition phase" if the whole session is above threshold. Ex at
% 'window' = 100 the first 100 trials will be considered as transition even
% if the whold session is above 80 %CR.
%
% - 'phaseLim' sets the number for consecutive trials above 'threshold' to
% be considered as a stable phase. defaults: 100 meaning that the performance
% has to be above 80%CR for at least 100 consecutive trials.
%
% Flags:
%
% - 'getTheshCross' gives as optional output the trials # where the
% performance goes from transition to stable. Possibility of multiple crossings.
%
% - 'fit' uses a fitting procedure to reduce noisy variation in the
% performance estimate specially when the 'window' is small.
%
% - 'plot' plots the performance.


Args = struct('threshold',75,'microThreshold',55,'window',50,'getThreshCross',0,'plot',0,'fit',0,'phaseLim',50,'ML',0,'file',[],'ruleSwitch',0,'Nocharlie',[],'notfromMTStrial',0,'newCriterion',0,'response',[],'onlyGoodTrials',0);
Args.flags = {'getThreshCross','plot','fit','ML','ruleSwitch','notfromMTStrial','newCriterion','onlyGoodTrials'};
[Args,modvarargin] = getOptArgs(varargin,Args);

if isempty(Args.Nocharlie)
    %determine which trial selection criteria to use
    [ses,curdir] = getDataDirs('session','CDnow');
    cd(ses)
    if exist('longdays.txt','file') %longdays.txt is placed in all session dirs of long delay days
        Args.Nocharlie = 1;
    else
        Args.Nocharlie = 0;
    end
    cd(curdir)
end

warning off all
startdir = pwd;
[pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
% check for saved object
if(isempty(cdir))
    % if there is an eye subdirectory, we are probably in the session dir
    % so change to the eye subdirectory
    [r,a] = ispresent('session','dir','CaseInsensitive');
    if r
        cdir = pwd;
        cd(a);
    end
end

if Args.notfromMTStrial
    
    if Args.ML
        basedir = pwd;
        datafile = nptDir('*.bhv');
        
        
        if isempty(Args.file)
            BHV = bhv_read(sprintf('%s%s%s',basedir,filesep,datafile.name));
            try
            load(sprintf('.%sdelFiles%sdeletedfiles.mat',filesep,filesep));
            catch
                 trials = find((BHV.TrialError == 0 | BHV.TrialError == 6) & BHV.ConditionNumber ~= 6);
                
            end
            if strmatch(BHV.SubjectName,'betty')
                
            
            trials = intersect(trials,find(BHV.ConditionNumber ~= 217)); % exclude fixation trials from teh calculation of performance
            else
                trials = intersect(trials,[find(BHV.ConditionNumber ~= 47); find(BHV.ConditionNumber ~= 48); find(BHV.ConditionNumber ~= 6)]); % exclude fixation trials,blank trials and fixaion block trials from the calculation of performance
            end
        else
            BHV = bhv_read(sprintf('%s%s%s',basedir,filesep,Args.file));
            trials = find(BHV.TrialError == 0 | BHV.TrialError == 6 & BHV.ConditionNumber ~= 217);
        end
        response = BHV.TrialError(trials) == 0;
    else
        markerfile = nptDir('*.mrk','CaseInsensitive');
        if isempty(markerfile)
            markerfile = nptDir('*_MRK.mat','CaseInsensitive');
            load(markerfile.name)
        else
            [MARKERS,records] = ReadMarkerFile(markerfile.name);
            
            tmarkers = MarkersToTrials(MARKERS);
        end
        
        
        for t = 1: size(tmarkers,2)
            response(t) = tmarkers(t).response;
        end
    end
    
elseif ~isempty(Args.response)
    
    response = Args.response;
    transt = [];
    longeur = length(response);
else
    mts = mtstrial('auto','redosetNames');%,modvarargin{:});
    %     response = mts.data.BehResp(mts.data.CueObj ~= 55);
    
    if Args.onlyGoodTrials
        goodTrials = mtsgetTrials(mts,varargin{:});
        response = mts.data.BehResp(goodTrials);
    else
        response = mts.data.BehResp;
    end
    
   
    transt = find(diff(mts.data.Index(:,1)));
    longeur = mts.data.numSets;
end
hw = Args.window;
numwindow = (length(response)-hw);

mperf = 100 * length(find(response == 1)) / length((find(response == 1 | response == 0)));
for tw = 1 : numwindow
    ti(tw) = hw/2 + tw;
    %     endt = ti(tw) + hw;
    CR = length(find(response(ti(tw)-hw/2:ti(tw)+hw/2) == 1));
    IR = length(find(response(ti(tw)-hw/2:ti(tw)+hw/2) == 0));
    perf(tw) = 100 * CR/(CR+IR);
    
end
if ~isempty(response) && numwindow > 0
    y = perf;
    x = ti;
    [p,S] = polyfit(x,y,20);
    
    [y2,delta] = polyval(p,x,S);
    
    if Args.getThreshCross
        
        if ~Args.Nocharlie
            trans = [0; transt; longeur];
            if trans(end) - trans(end-1) < Args.window
                nblock = length(trans) - 2;
            else
                nblock = length(trans) - 1;
            end
            thecross = [];
            for block = 1 : nblock
                c = Args.window/2 + 1 + trans(block);
                mCR = 100 * sum(response(c:trans(block+1))) / length(response(c:trans(block+1)));
                while mCR <= Args.threshold && c <= trans(block+1)
                    
                    c = c + 1;
                    cres =  response(c:trans(block+1));
                    
                    mCR = 100 * sum(cres) / length(cres);
                end
                if c >= trans(block+1)- Args.window/2
                    lim = [];
                    crosst = [];
                    adcros = [];
                else
                    
                    crosst = [c - Args.window/2 : trans(block+1)-Args.window/2];
                    lim = [1; length(crosst)];
                    if block == nblock
                        adcros = [ti(crosst(1)); ti(end)];
                        
                    else
                        adcros = ti(crosst([1 end]))';
                        
                    end
                    
                end
                if ~isempty(adcros)
                    vti = adcros(1) - hw/2 : adcros(2) - hw/2;
                     [badperf,inB] = min(perf(vti));
                    
                    while badperf <= Args.microThreshold
                        elin = [vti(inB) - Args.window/2 : vti(inB) + Args.window/2];
                        vti = setdiff(vti,elin);
                        [badperf,inB] = min(perf(vti));
                    end
                    jumps = [0 find(diff(vti) ~= 1) length(vti)] ;
                    
                    for nbrj = 1 : length(jumps)-1
                        adcros(:,nbrj) = [vti(jumps(nbrj)+1) + hw/2; vti(jumps(nbrj+1)) + hw/2];
                        
                    end
                end
                thecross = [thecross adcros];
            end
            
        elseif Args.newCriterion
            
        else
            crosst = find(y2 >= Args.threshold);
            
            
            %     crosst = find(perf >= Args.threshold);
            %     below = true;
            %     count = 1;
            %     countlim = length(crosst);
            if ~isempty(crosst) % && crosst(end) == length(perf)
                jump = diff(crosst);
                I = find(jump ~= 1);
                if ~isempty(I)
                    i = 1;
                    while i <= length(I) % for i = 1 : length(I)
                        if (y2(crosst(I(i)) : crosst(I(i)+1)) + (delta(crosst(I(i)) : crosst(I(i)+1)))) >= Args.threshold
                            I(i) = [];
                            
                        else
                            i = i + 1;
                        end
                    end
                end
                
                
                trans = [1 I+1 length(crosst)];
                phase = find(diff(trans) > Args.phaseLim);
                if length(phase) > 1 | phase ~= 1
                    lim = [trans(phase); trans(phase+1)-1]; % -1
                else
                    lim = [trans(phase); trans(phase+1)-1]; % -1
                end
                if isvector(ti(crosst(lim)))
                    thecross = ti(crosst(lim))';
                else
                    thecross = ti(crosst(lim));
                end
                %         while below & count < countlim
                %
                %             if isempty(find(diff(crosst(count:end)) ~= 1))
                %                 lim = ti(crosst(count));
                %                 varargout{1} = [lim crosst(end)];
                %                 below = false;
                %             else
                %                 count = count + 1;
                %                 lim = [];
                %             end
                %         end
            else
                thecross = [];
            end
        end
        varargout{1}= thecross;
        if isempty(thecross)
            varargout{1} = [];
            fprintf(' No crossing of criterion \n')
        end
        
        
    else
       
        y = [];
       
        lim = [];
        varargout{1} = [];
    end
else
    perf = [];
    y = [];
    ti = [];
    lim = [];
    thecross = [];
end
if Args.plot && ~isempty(perf)
    
    plot(ti,perf,'o')
    hold on
    refline(0,Args.threshold)
    line([ti(1) ti(end)],[mperf mperf],'Color',[1 0 0]);
    if Args.ruleSwitch; mt = mtstrial('auto'); chr = find(diff(mt.data.Index(:,1)) ~= 0); if ~isempty(chr); line(repmat(chr',2,1),[zeros(size(chr')); 100 * ones(size(chr'))]); end; varagout{2} = chr;end
    if exist('thecross') && ~isempty(thecross)
        for p = 1: size(thecross,2)
            plot(thecross(1,p):thecross(2,p),perf(thecross(1,p) - hw/2 : thecross(2,p) -hw/2),'gx')
        end
    end
    if Args.fit
        plot(x,y2,'r'); plot(x,y2+delta,'r--'); plot(x,y2-delta,'r--')
    end
    hold off
    if isempty(Args.file); title(pwd); else; title(Args.file); end
    xlabel('Trial #')
    ylabel('Percent CR')
    axis([ti(1) ti(end) 0 100])
end
if exist('thecross') && ~isempty(thecross); thecross(2,size(thecross,2)) = length(response); varargout{1} = thecross;end

cd(startdir)

