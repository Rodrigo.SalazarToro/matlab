function [power,f,varargout] = power_summary(days,varargin)


Args = struct('save',0,'redo',0,'Faxis',[0 50],'plot',0,'prctile',[25 50 75 90],'list',[],'allch',0,'pvalue',0.01,'Fnorm',1,'selectComp',[],'NptSig',2,'Fband',[],'compareWind',0);
Args.flags = {'plot','allch','Fnorm','save','redo','compareWind'};
[Args,modvarargin] = getOptArgs(varargin,Args);

compChart = [1 2;1 3;1 4;2 3;2 4;3 4];
areaN = {'powerPP' 'powerPF'};

file = 'power_summary.mat';
matfile = nptDir(file);
%% acquiring the data

%% initialization of variable
if Args.redo || isempty(matfile)
%     power.S = cell(2,2);
power.S = [];
power.hist = [];
power.rule = [];
    power.Adz = cell(2,2,size(compChart,1));
    for a = 1 : 2; count(a,1) = 1; count(a,2) = 1; end;
    
    for d = 1 : size(days,2)
        clear rules
        cd(days{d});
        
        for area = 1 : 2
            
            file = nptDir(sprintf('%s.mat',areaN{area}));
            if ~isempty(file)
                [ich,iCHcomb,iflat,igroups] = getFlatCh(areaN{area},modvarargin{:});
                data = load(file.name);
                f = data.f;
                if Args.allch
                    chst = setdiff(data.Day.channels,iflat.ch);
                    clear chs;for c = 1 : length(chst); chs(c) = find(chst(c) == chst); end
                else
                    indpairs = load('indchannels.mat');
                    chs = indchannels(area).ch;
                    
                end
                
                %% needs some work
                if ~isempty(Args.list)
                    thelist = Args.list;
                    forday = strmatch(days{d},thelist.days);
                    ncomb =  length(forday);
                    chs = thelist.npair(forday);
                end
                %%
                nses = length(data.Day.session);
                
                for s = 1 : nses; rules(s) = data.Day.session(s).rule;  end
                
                for ses = 1 : nses
                   
                    cd session01;  histology = NeuronalHist; cd ..
                   
                    
                    rule = data.Day.session(ses).rule;
                    thes = find(rules == rule);
                    if ses == thes(end)
                        
                        S = data.Day.session(ses).S;
                        
                        if ~isempty(S)
                            S = mean(S(:,:,chs,:),2);
                            % Session(scount).S(:,:,cb,p)
                            sdata = squeeze(mean(S,2));
                            if length(size(sdata)) < 3; sdata = reshape(sdata,65,1,4); end
                            power.S = [power.S sdata];% reshape(sdata,size(sdata,1),size(sdata,2) * size(sdata,3))];
                            power.hist = [power.hist histology.locs(chst)];
                            power.rule = [power.rule repmat(rule,1,length(chst))];
                            
                            clear S
                            
                            if Args.compareWind
                                for cmp = 1 : size(compChart,1)
                                    Adz = nan(length(f),length(chs));
                                    for thec = 1 : length(chs)
                                        J1 = squeeze(data.Day.session(ses).J(:,:,:,chs(thec),compChart(cmp,1)));
                                        J2 = squeeze(data.Day.session(ses).J(:,:,:,chs(thec),compChart(cmp,2)));
                                        J1 = reshape(J1,size(J1,1),size(J1,2) * size(J1,3));
                                        J2 = reshape(J2,size(J2,1),size(J2,2) * size(J2,3));
                                        [~,~,Adz(:,thec)] = two_group_test_spectrum(J1,J2,Args.pvalue);
                                        
                                    end
                                    power.Adz{area,rule,cmp} = [power.Adz{area,rule,cmp} ~Adz];
                                    clear J1 J2 Adz
                                end
                            else
                                
                            end
                            count(area,rule) = count(area,rule) + length(chs);
                        end
                        
                        
                    end
                end
                clear data
            end
            
            
        end %for d = 1 : size(days,1)
        cd ..
    end
%     for a = 1 : 2; for r = 1 : 2; power.S{a,r} = squeeze(power.S{a,r}); end; end
    power.f = f;
else
    load(file)
    f = power.f;
end


if Args.save; save(file,'power'); end
%% Plotting
if Args.plot
    epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
    therules = {'Identity' 'Location'};
    if isempty(Args.Fband)
        Fband = [1 : length(f)];
    else
        Fband = find(f >= Args.Fband(1) & f <= Args.Fband(2));
    end
    for area = 1 : 2
        nh = area;
        
        h(nh) = figure;
        
        set(h(nh),'Name',sprintf('distribution of %s',areaN{area}));
        
        maxv = [];
        for r =1 : 2;
            
            for p = 1 : 4;
                subplot(2,4,(r-1)*4 + p);
                if Args.Fnorm
                    values = squeeze(power.S{area,r}(:,:,p)).*repmat(power.f',1,size(power.S{area,r},2));
                else
                    
                    values = squeeze(power.S{area,r}(:,:,p));
                end
                if ~isempty(Args.selectComp);
                    for rr = 1 : 2; sigs{rr} = find(sum(power.Adz{area,rr,Args.selectComp}(Fband,:),1) > Args.NptSig);end
                    sig = union(sigs{1},sigs{2});
                    values = values(:,sig);
                    
                end
                plot(f,prctile(values,Args.prctile,2));
                
                maxv = [maxv max(max(prctile(values,Args.prctile(end),2)))];
                grid on
                if p == 1; ylabel(therules{r}); end
                if r == 2; xlabel('Frequency [Hz]'); elseif r ==1; title(epochs{p}); end
            end
            
        end
        for sb = 1 : 8; subplot(2,4,sb); axis([Args.Faxis(1) Args.Faxis(2) 0 1.1 * max(maxv)]); grid on; end
        
        text(round(Args.Faxis(2)/2),max(maxv)*0.5,sprintf('n = %d',size(values,2)));
        for pr = 1 : length(Args.prctile); leg{pr} =  sprintf('%d prctile',Args.prctile(pr)); end
        legend(leg)
    end
    
end


%%