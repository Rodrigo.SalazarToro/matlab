function make_phase_portraits

%used to make cumulative phase portraits for each pair
%run at session level


%for locations = 1:3
%for objects = 1:3

cc = mtscpp('auto');
pvals = cc.data.Index(:,30);

ses_dir = pwd;
N = NeuronalHist;
cd([ses_dir filesep ('lfp')]);
mt = mtstrial('auto');
objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);
ind = mtsgetTrials(mt,'BehResp',1,'stable');%,'CueObj',objs(objects),'CueLoc',locs(locations));
num_trials = size(ind,2);
cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
cp = nptDir('channelpair*');
num_cp = size(cp,1);



combs = {'cohPP' 'cohPF' 'cohInter'};
titles = {'PP' 'FF' 'PF'};

for combinations = 1:3
    cd(ses_dir)
    [channels,comb,CHcomb,list] = checkChannels(combs{combinations});
    cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
    num_cp = size(CHcomb,1);
    max_val = 0;
    if CHcomb > 0
        for y = 1:num_cp
            load(['channelpair' num2str(CHcomb(y,1)) num2str(CHcomb(y,2))]);
            load threshold
            load all_pairs
            cumulative_xcorr = correlograms{ind(1)}((1:43),:);
            ind = ind(2:end);
            for x = ind
                cumulative_xcorr = cumulative_xcorr + correlograms{x}((1:43),:);
            end
            cx = cumulative_xcorr' ./num_trials;
            im{y} = cx;
            
            if max_val < max(max(abs(cx)))
                max_val = max(max(abs(cx)));
            end
        end
        
        figure('Name',titles{combinations})
        c = 0;
        for y = 1:(num_cp + 1) %+1 for empty box so that colorbar does not distort any real ones
            
            
            
            
            subplot(6,4,y);
            
            if y > num_cp
                y = y - 1;
                c = 1;
            else
                [i pair_num] = intersect(all_pairs,CHcomb(y,:),'rows');
            end
            thresh = threshold{pair_num}(1);
            %im{y}((1:10),(35:43)) = thresh;
            imagesc(im{y},[(-1*max_val) max_val])
            
            set(gca,'YTick',[1,25,50,75,100])
            set(gca,'YTicklabel',[-50 -25 0 25 50]);
            
            set(gca,'XTick',[1:7:42])
            set(gca,'XTicklabel',[150:350:2250])
            hold on
            
            %zero phase
            %im{y}(50,:) = max_val;
            plot([1 43],[50 50],'color','k')
            hold on
            %sample
            plot([8 8],[1 101],'color','k')
            hold on
            %delay
            plot([18 18],[1 101],'color','k')
            hold on
            %first match
            plot([34 34],[1 101],'color','k')
            hold on
            %last match
            plot([42 42],[1 101],'color','k')
            hold on
            
            
            if pvals(pair_num) < .02
                title([num2str(CHcomb(y,1)) '  ' num2str(CHcomb(y,2))],'Color','r','LineWidth',2)
            else
                title([num2str(CHcomb(y,1)) '  ' num2str(CHcomb(y,2))],'LineWidth',2)
            end
            
            if c 
                hold on
                colorbar
            end
            
        end
        
    end
    cd(ses_dir)
    
    
end
    %end
    
    
    
