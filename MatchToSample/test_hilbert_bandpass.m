function test_hilbert_bandpass



%time
t = [0:.001:.5];

%make sine wave 1
s1 = 2 * cos(2*pi*15*t);

%make sine wave 2
s2 = 1 * cos(2*pi*5*t);

%make sine wave 2
s3 = 1 * cos(2*pi*25*t);

s = s1+s2 + s3;
plot(s);
hold on

hs = hilbert(s)

plot(angle(hs),'r')