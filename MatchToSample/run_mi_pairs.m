function run_mi_pairs(varargin)


Args = struct('days',[],'ml',0,'run_mi',0,'perm',0);
Args.flags = {'ml','run_mi','perm'};
Args = getOptArgs(varargin,Args);


monkeydir = pwd;
num_days = size(Args.days,2);
number_total_pairs = 0;
number_pp_pairs = 0;
number_pf_pairs = 0;
number_ff_pairs = 0;

delay_cross = 0;
delay_cross_pp = 0;
delay_cross_pf = 0;
delay_cross_ff = 0;
day_mi = cell(num_days,1);
day_pair_percentiles = cell(num_days,1);
day_mi_delaycross = cell(num_days,1);
day_prct_delay = cell(num_days,1);

day_mi_delaycross_pp = cell(num_days,1);
day_prct_delay_pp = cell(num_days,1);
day_mi_delaycross_pf = cell(num_days,1);
day_prct_delay_pf = cell(num_days,1);
day_mi_delaycross_ff = cell(num_days,1);
day_prct_delay_ff = cell(num_days,1);

delay_pair_crossings = zeros(num_days,33);
pair_crossings = zeros(num_days,33);
pair_crossings_pp = zeros(num_days,33);
pair_crossings_pf = zeros(num_days,33);
pair_crossings_ff = zeros(num_days,33);

all_day_crossings = zeros(num_days,33);
all_day_crossings_pp = zeros(num_days,33);
all_day_crossings_pf = zeros(num_days,33);
all_day_crossings_ff = zeros(num_days,33);

all_daymi_cross_pp_bins = cell(num_days,33);
all_daymi_cross_pf_bins = cell(num_days,33);
all_daymi_cross_ff_bins = cell(num_days,33);

day_area = cell(num_days,1);
for d = 1 : num_days
    
    cd([monkeydir filesep Args.days{d}])
    
    if Args.ml
        cd([monkeydir filesep Args.days{d} filesep 'session01'])
    else
        cd([monkeydir filesep Args.days{d} filesep 'session02'])
        mt = mtstrial('auto','redosetNames');
        if mt.data.Index(1,1) ~= 1;
            cd([monkeydir filesep Args.days{d} filesep 'session03'])
        end
    end
    
    if Args.run_mi
        if Args.perm
            if Args.ml
                mi_single_pair('all','ml','perm')
            else
                mi_single_pair('all','perm')
            end
        else
            if Args.ml
                mi_single_pair('all','ml')
            else
                mi_single_pair('all')
            end
        end
    else
        
        if Args.ml
            c = mtscpp('auto','ml','ide_only');
            [~,all_pairs] = sorted_groups('ml');
        else
            c = mtscpp('auto','ide_only');
            %PP == 1; PF == 2; FF == 3
            [~,all_pairs] = sorted_groups;
        end
        %PP == 1; PF == 2; FF == 3
        area_pair_list = zeros(1,size(all_pairs));
        
        [~,pp] = get(c,'correct','stable','identity','pp','sorted');
        area_pair_list(intersect(pp,all_pairs)) = 1;
        
        
        [~,pf] = get(c,'correct','stable','identity','pf','sorted');
        area_pair_list(intersect(pf,all_pairs)) = 2;
        
        
        [~,ff] = get(c,'correct','stable','identity','ff','sorted');
        area_pair_list(intersect(ff,all_pairs)) = 3;
        
        
        area_pair_list(find(area_pair_list == 0)) = [];

        day_area{d} = area_pair_list';
        
%         try
            load mi_single_pair_permutations
            nperms = size(all_perms,2);
            npairs = size(all_perms{1},2);
            
            load mi_single_pair pair_mutual_info
            smi = size(pair_mutual_info,2);
            rmi = reshape(pair_mutual_info,smi,1);
            daymi = cell2mat(rmi);
            day_mi{d} = daymi;

            pair_perms = cell(1,npairs);
            for perms = 1: nperms
                for apairs = 1 : npairs
                    if isempty(pair_perms{apairs})
                        pair_perms{apairs} = zeros(nperms,33);
                    end
                    pair_perms{apairs}(perms,:) = all_perms{perms}{apairs};
                end
            end
            pair_prctiles = zeros(npairs,33);
            for apairs = 1 : npairs
                pair_prctiles(apairs,:) = prctile(pair_perms{apairs},95);
            end
            
            day_pair_percentiles{d} = pair_prctiles;
            
            
            daymi_cross = [];
            dayprct = [];
            
            daymi_cross_pp_bins = [];
            daymi_cross_pp = [];
            dayprct_pp = [];
            
            daymi_cross_pf_bins = [];
            daymi_cross_pf = [];
            dayprct_pf = [];
            
            daymi_cross_ff_bins = [];
            daymi_cross_ff = [];
            dayprct_ff = [];
            c = 0;
            ppc = 0;
            pfc = 0;
            ffc = 0;
            
            daymi_cross_pp_bins = zeros(npairs,33);
            daymi_cross_pf_bins = zeros(npairs,33);
            daymi_cross_ff_bins = zeros(npairs,33);
            
            for apairs = 1 : npairs
                if sum(daymi(apairs,(25:33))>pair_prctiles(apairs,(25:33))) >= 2
                    c = c + 1;
                    bins_cross = find(daymi(apairs,:)>pair_prctiles(apairs,:));
                    delay_cross = delay_cross + 1;
                    daymi_cross(c,:) = daymi(apairs,:);
                    dayprct(c,:) = pair_prctiles(apairs,:);
                    if area_pair_list(apairs) == 1
                        ppc = ppc + 1;
                        delay_cross_pp = delay_cross_pp + 1;
                        
                        daymi_cross_pp_bins(ppc,bins_cross) = daymi(apairs,bins_cross);
                        
                        daymi_cross_pp(ppc,:) = daymi(apairs,:);
                        dayprct_pp(ppc,:) = pair_prctiles(apairs,:);
                    elseif area_pair_list(apairs) == 2
                        pfc = pfc + 1;
                        delay_cross_pf = delay_cross_pf + 1;
                        
                        daymi_cross_pf_bins(pfc,bins_cross) = daymi(apairs,bins_cross);
                        
                        daymi_cross_pf(pfc,:) = daymi(apairs,:);
                        dayprct_pf(pfc,:) = pair_prctiles(apairs,:);
                    elseif area_pair_list(apairs) == 3
                        ffc = ffc + 1;
                        delay_cross_ff = delay_cross_ff + 1;
                        
                        daymi_cross_ff_bins(ffc,bins_cross) = daymi(apairs,bins_cross);
                        
                        daymi_cross_ff(ffc,:) = daymi(apairs,:);
                        dayprct_ff(ffc,:) = pair_prctiles(apairs,:);
                    end
                end
            end
            
            %get mi values for significant crossings
            
            all_daymi_cross_pp_bins{d} = daymi_cross_pp_bins;
            all_daymi_cross_pf_bins{d} = daymi_cross_pf_bins;
            all_daymi_cross_ff_bins{d} = daymi_cross_ff_bins;
            
            
            %all crossings
            all_day_crossings(d,:) = sum(daymi > pair_prctiles);
            
            pp_pairs = find(area_pair_list == 1);
            if size(pp_pairs,2) > 1
                all_day_crossings_pp(d,:) = sum(daymi(pp_pairs,:) > pair_prctiles(pp_pairs,:));
            elseif size(pp_pairs,2) > 0
                all_day_crossings_pp(d,:) = (daymi(pp_pairs,:) > pair_prctiles(pp_pairs,:));
            end
            
            pf_pairs = find(area_pair_list == 2);
            if size(pf_pairs,2) > 1
                all_day_crossings_pf(d,:) = sum(daymi(pf_pairs,:) > pair_prctiles(pf_pairs,:));
            elseif size(pf_pairs,2) > 0
                all_day_crossings_pf(d,:) = sum(daymi(pf_pairs,:) > pair_prctiles(pf_pairs,:));
            end
            
            ff_pairs = find(area_pair_list == 3);
            if size(ff_pairs,2) > 1
                all_day_crossings_ff(d,:) = sum(daymi(ff_pairs,:) > pair_prctiles(ff_pairs,:));
            elseif size(ff_pairs,2) > 0
                all_day_crossings_ff(d,:) = (daymi(ff_pairs,:) > pair_prctiles(ff_pairs,:));
            end

            
            
            

            day_mi_delaycross{d} = daymi_cross;
            day_prct_delay{d} = dayprct;
            pair_crossings(d,:) = sum(daymi>pair_prctiles);
            
            if size(daymi_cross,1) > 1
                delay_pair_crossings(d,:) = sum(daymi_cross > dayprct);
            elseif size(daymi_cross,1) > 0
                delay_pair_crossings(d,:) = (daymi_cross > dayprct);
            end
            
            day_mi_delaycross_pp{d} = daymi_cross_pp;
            if size(daymi_cross_pp,1) > 1
                pair_crossings_pp(d,:) = sum(daymi_cross_pp > dayprct_pp);
            elseif size(daymi_cross_pp,1) >0
                pair_crossings_pp(d,:) = (daymi_cross_pp > dayprct_pp);
            end
            
            day_mi_delaycross_pf{d} = daymi_cross_pf;
            if size(daymi_cross_pf,1) > 1
                pair_crossings_pf(d,:) = sum(daymi_cross_pf > dayprct_pf);
            elseif size(daymi_cross_pf,1) > 0
                pair_crossings_pf(d,:) = (daymi_cross_pf > dayprct_pf);
            end
            
            day_mi_delaycross_ff{d} = daymi_cross_ff;
            if size(daymi_cross_ff,1) > 1
                pair_crossings_ff(d,:) = sum(daymi_cross_ff > dayprct_ff);
            elseif size(daymi_cross_ff,1) > 0
                pair_crossings_ff(d,:) = (daymi_cross_ff > dayprct_ff);
            end

            number_total_pairs = number_total_pairs + npairs;
            number_pp_pairs = number_pp_pairs + size(pp,2);
            number_pf_pairs = number_pf_pairs + size(pf,2);
            number_ff_pairs = number_ff_pairs + size(ff,2);
            
%         catch
%             fprintf(1,['NO MI FOR ' Args.days{d}])
%         end
    end
end

%percentage of all pairs that cross at each bin
figure
plot(sum(all_day_crossings_pp) ./ number_pp_pairs,'r');
hold on
plot(sum(all_day_crossings_pf) ./ number_pf_pairs,'g');
hold on
plot(sum(all_day_crossings_ff) ./ number_ff_pairs,'b');

figure
plot(sum(all_day_crossings) ./ number_total_pairs)

all_crossings.pp = all_day_crossings_pp;
all_crossings.pp_npairs = number_pp_pairs;
all_crossings.pf = all_day_crossings_pf;
all_crossings.pf_npairs = number_pf_pairs;
all_crossings.ff = all_day_crossings_ff;
all_crossings.ff_npairs = number_ff_pairs;

figure
plot(sum(pair_crossings_pp)./delay_cross_pp,'r')
hold on
plot(sum(pair_crossings_pf)./delay_cross_pf,'g')
hold on
plot(sum(pair_crossings_ff)./delay_cross_ff,'b')

figure
plot(sum(delay_pair_crossings)./delay_cross)

crossings.pp = pair_crossings_pp;
crossings.pp_npairs = delay_cross_pp;
crossings.pf = pair_crossings_pf;
crossings.pf_npairs = delay_cross_pf;
crossings.ff = pair_crossings_ff;
crossings.ff_npairs = delay_cross_ff;



%plot real mi values for each area with the median 95th prctile
pp_areas = find(cell2mat(day_area) == 1);
pf_areas = find(cell2mat(day_area) == 2);
ff_areas = find(cell2mat(day_area) == 3);
daymi = cell2mat(day_mi);
dayprctiles = cell2mat(day_pair_percentiles);


figure
subplot(3,1,1)
boxplot(daymi(pp_areas,:))
hold on
plot(median(dayprctiles(pp_areas,:)))
title('pp')

subplot(3,1,2)
boxplot(daymi(pf_areas,:))
hold on
plot(median(dayprctiles(pf_areas,:)))
title('pf')

subplot(3,1,3)
boxplot(daymi(ff_areas,:))
hold on
plot(median(dayprctiles(ff_areas,:)))
title('ff')


figure
boxplot(daymi)
hold on
plot(median(dayprctiles))




cd(monkeydir)

save day_mi all_crossings crossings





