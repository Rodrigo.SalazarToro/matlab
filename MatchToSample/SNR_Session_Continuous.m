function SNR_Session_Continuous(varargin)

%       Run at session level
%
%           Arguments:
%
%           'chunks'     -  [chunk size in seconds]
%           'redo_snr'   -  recompute SNR for specific sessions
%           'std_snr'    -  standard deviation for snr (default is 4 in ProcessSession);
%
%        SNR = range(Signal)/range(Noise)
%
%        saves 'SNR_channels.mat' in highpass folder

Args = struct('chunks_snr',[],'redo_snr',0,'std_snr',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;

descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
groups = descriptor_info.group(descriptor_info.group ~= 0);

channel_snr_list = zeros(size(groups,2),2);
channel_snr_list(:,1) = groups;

if isempty(nptDir('skip.txt'))
    cd('highpass');
    if ~exist('SNR_channels.mat','file') || Args.redo_snr
        if isempty(Args.chunks_snr)
            trial_list = nptDir('*_highpass.****');
            %check to see if it is a bin file
            if isempty(trial_list)
                trial_list = nptDir('*.bin');%treat the .bin file as one trial
            end   
            dtype = DaqType(trial_list(1).name);
            if strcmp(dtype,'Streamer')
                [num_channels] = nptReadStreamerFileHeader(trial_list(1).name);
            elseif strcmp(dtype,'UEI')
                fprintf(1,'ADD CODE TO CALCULATE SNR FOR UEI FILES!!! \n')
                data = ReadUEIFile('FileName',trial_list(1).name,'Header');
                num_channels = data.numChannels;
            else
                error('unknown file type')
            end
        else
            bin_list = nptDir('*.bin');
            bin_name = bin_list.name;
            %get recording informations
            dtype = DaqType(bin_name);
            if strcmp(dtype,'Streamer')
                [num_channels,sampling_rate,scan_order] = nptReadStreamerFileHeader(bin_name);
                headersize = 73;
            elseif strcmp(dtype,'UEI')
                fprintf(1,'ADD CODE TO CALCULATE SNR FOR UEI FILES!!! \n')
                data = ReadUEIFile('FileName',bin_name,'Header');
                num_channels = data.numChannels;
                headersize = 90;
            else
                error('unknown file type')
            end

            number_chunks = ceil((bin_list.bytes-headersize)/2/num_channels/(Args.chunks_snr*sampling_rate)); %number of chunks
            total_data_points = (bin_list.bytes-headersize)/2;
            total_data_channel = total_data_points / num_channels;
            
            %make a list of data points to acquire for each chunk
            sample_size = sampling_rate * Args.chunks_snr;
            all_chunks = zeros(number_chunks,2);
            for nc = 1 : number_chunks
                if nc == 1
                    all_chunks(nc,:) = [1 sample_size];
                else
                    all_chunks(nc,:) = [(sample_size*(nc-1)+1) sample_size*nc];
                end
            end
            all_chunks(number_chunks,2) = total_data_channel; %make sure last chunk goes throught the end of the data
        end
        
        %run one channel at a time
        for ch = 1 : num_channels
            fprintf(1,['Computing SNR for channel ' num2str(ch) ' of ' num2str(num_channels)  '\n'])
            if isempty(Args.chunks_snr)
                SNR = SignalNoiseMUAContinuous('FileName',trial_list,'channel',ch,'std',Args.std_snr);
            else
                SNR = SignalNoiseMUAContinuous('FileName',bin_name,'channel',ch,'std',Args.std_snr,'chunk_points',all_chunks);
            end
            channel_snr_list(ch,2) = SNR;
        end
        save SNR_channels channel_snr_list
    else
        fprintf(1,[pwd '  SNR complete\n'])
    end
end

cd(sesdir)

