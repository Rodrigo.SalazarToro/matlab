function ppc_phase_angle

%look at phase angle for each channel with respect to itself with channels at one space for each 45degree increment
cd('/media/raid/data/monkey/betty')

load cpp_b


%order is clockwise, starting at A -> P



%look at rows
[npairs ii] = get(cpp_b,'Number','ips',1,'pprow');

%get x distance
xdist = cpp_b.data.Index(ii,19); %the x/y distances are flipped for the calculation and the anterior to posterior orientation

xdist = xdist ./ .86;

%each channel gets it's own list, with 8 possible directions
ch_angles0 = cell(5,33);
ch_angles180 = cell(5,33);
% ch_angles45

%run through all pairs
%row orientation (0 degrees) reference is always on the left
for np = 1 : npairs
    %get pair information
    ch1 = cpp_b.data.Index(ii(np),14);
    ch2 = cpp_b.data.Index(ii(np),15);

    phangle = cpp_b.data.Index(ii(np),54);%phase angle during the delay (200 - 800)
    opposite_phangle = phangle * -1;
    
    
    xdistance = single(xdist(np)); %in units of grid spaces
    
    ch_angles180{xdistance,(ch1 - 32)} = [ch_angles180{xdistance,(ch1-32)} phangle];
    ch_angles0{xdistance,(ch2 - 32)} = [ch_angles0{xdistance,(ch2-32)} opposite_phangle];
end



%look at rows
[npairs ii] = get(cpp_b,'Number','ips',1,'ppcolumn');
%get y distance
ydist = cpp_b.data.Index(ii,18); %the x/y distances are flipped for the calculation and the anterior to posterior orientation

ydist = ydist ./ .86;

%each channel gets it's own list, with 8 possible directions
ch_angles90 = cell(5,33);
ch_anglesneg90 = cell(5,33);
% ch_angles45

%run through all pairs
%row orientation (0 degrees) reference is always on the left
for np = 1 : npairs
    %get pair information
    ch1 = cpp_b.data.Index(ii(np),14);
    ch2 = cpp_b.data.Index(ii(np),15);

    phangle = cpp_b.data.Index(ii(np),54);%phase angle during the delay (200 - 800)
    
    opposite_phangle = phangle * -1;
    
    ydistance = single(ydist(np)); %in units of grid spaces
    
    ch_angles90{ydistance,(ch1 - 32)} = [ch_angles90{ydistance,(ch1-32)} phangle];
    ch_anglesneg90{ydistance,(ch2 - 32)} = [ch_anglesneg90{ydistance,(ch2-32)} opposite_phangle];
end
ch1
