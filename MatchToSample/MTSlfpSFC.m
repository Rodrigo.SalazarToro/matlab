function Day = MTSlfpSFC(varargin)
% Computes the coherence on defined epochs (500ms; fixation,cue,delay1, delay2).
% The type of coherence can be chosen with the arguments:
% - cohPP: within the parietal electrodes
% - cohPF: within the frontal electrodes
% - cohInter: between parietal and frontal
% - surrogate: computes the surrogate distribution
%
% Additional arguments are:
% - stable to only analyze teh stable performance phase
% - BehResp to specify correct 1 or incorrect 0
% - 'save' to save the results in the day directory
% - 'redo' to redo the analysis even though the results have already been
% saved.
%
% The preprocessiong of data includes 60Hz removal and a linear detrend of
% the data.


Args = struct('redo',0,'save',0,'type','PP','startDay',1,'days',[],'surrogate',0,'STA',0);
Args.flags = {'redo','save','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'days'});

rsFs = 200; % downsampling
plength = 500;

if isempty(Args.days)
    days = nptDir('0*');
else
    days = Args.days;
end

params = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1);


for d = Args.startDay : length(days)

    cd(days(d).name);
    skip = nptDir('skip.txt');
    if isempty(skip)
        if Args.STA
            if Args.surrogate
                matfile = sprintf('%s/SFC_STA%sSur.mat',pwd,Args.type);
            else
                matfile = sprintf('%s/SFC_STA%s.mat',pwd,Args.type);
            end

        else
            if Args.surrogate
                matfile = sprintf('%s/SFC%sSur.mat',pwd,Args.type);
            else
                matfile = sprintf('%s/SFC%s.mat',pwd,Args.type);
            end
        end
    end
    prematfile = nptDir(matfile);
    if isempty(prematfile) | Args.redo
        session = nptDir('session*');
        scount = 1;
        for s = 2 : size(session,1)

            cd(session(s).name)
            skip = nptDir('skip.txt');

            if  isempty(skip)
                if Args.surrogate
                    if Args.STA
                        [Session(s-1).C,Session(s-1).phi,f,Session(s-1).unit,Session(s-1).sur,Session(s-1).sta,Session(s-1).stasur] = spikeFieldCoh(params,modvarargin{:});
                    else
                        [Session(s-1).C,Session(s-1).phi,f,Session(s-1).unit,Session(s-1).sur] = spikeFieldCoh(params,modvarargin{:});
                    end
                else
                    if Args.STA
                        [Session(s-1).C,Session(s-1).phi,f,Session(s-1).unit,surtemp,Session(s-1).sta] = spikeFieldCoh(params,modvarargin{:});
                    else
                        [Session(s-1).C,Session(s-1).phi,f,Session(s-1).unit,surtemp] = spikeFieldCoh(params,modvarargin{:});
                    end
                end
            end
            cd .. % day

        end %s = 2 : size(session,1)
    end

    if exist('Session')
        Day.session = Session;

        Day.name = pwd;
        Day.option = varargin;
        clear Session
    end
    if Args.save & exist('Day'); fprintf('\n saving file %s \n',matfile);  save(matfile,'Day','f'); clear Day; end

    cd ..

end %for d = 1 : size(days,1)

