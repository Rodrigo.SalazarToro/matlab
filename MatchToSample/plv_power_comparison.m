function plv_power_comparison(varargin)


%run at session level
%computes Shannon Entropy and normalized to get the synchronization index
%in tass_1998
Args = struct('ml',1,'plot',0);
Args.flags = {};
Args = getOptArgs(varargin,Args);


cd('/media/raid/data/monkey/betty/091001/session01')

%phase_paper_iti_figures.m, investigage data from this figure

% bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
window = 300;
stepsize = 50;
nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
bins = linspace(0,1,nbins);
steps = [window/2:stepsize:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
nw = size(steps,2);

bandpasslow = 8;
bandpasshigh = 25;
% 
% bandpasslow = 1;
% bandpasshigh = 8;

sesdir = pwd;
%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

N = NeuronalHist('ml');

c = mtscpp2('auto','ml');
[~,pairs] = get(c,'ml','Number'); %use all pairs
mt = mtstrial('auto','ML','RTfromML','redosetNames');
sample_off = floor(mt.data.CueOffset); %lock to sample off
matchtime = floor(mt.data.MatchOnset) + floor(mt.data.FirstSac); %lock to saccade


cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
    iti_reject = rejecTrials;
else
    iti_reject = [];
end
cd(sesdir)


%Get trials
incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
%get rid of incorrect trials with bad itis if the artifact
%detection has been run on it
if ~isempty(iti_reject)
    [~,baditi] = intersect(incorrtrials,iti_reject);
    incorrtrials(baditi) = [];
end


pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

cd([sesdir filesep 'lfp'])
lfpdata = nptDir('*_lfp*');
cd(sesdir)
for p = 1% : npairs
    cd(sesdir)
%     ch1 = intersect(N.gridPos,pair_list(p,1));
%     ch2 = intersect(N.gridPos,pair_list(p,2));
    
    ch1 = 3;
    ch2 = 32;
    
    tr = incorrtrials;
    ntrials = size(tr,2);
    counter = 0;
    for t = tr
        counter = counter + 1;
        trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
        %betty = UEI
        orig_data = ReadUEIFile('FileName',trial);
        
        %lowpass
        hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,bandpasslow,bandpasshigh); 
        
        %get data ranges
        datarange = [sample_off(t) - 999 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
        bpdata = hdata(:,datarange);
        
        bpd1 = bpdata(ch1,:);
        bpd2 = bpdata(ch2,:);
        
        data = bpdata'; %need to do this before and after using hilbert, don't include during the transform
        %compute hilbert transform
        data = hilbert(data);
        
        %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
        %COMPENT IS FLIPPED
        %find instantaneous phase angles
        data = angle(data);
        data = data';

        d1 = unwrap(data(ch1,:)) ./ (2*pi);
        d2 = unwrap(data(ch2,:)) ./ (2*pi);
        
        %cyclic relative phase
        d = mod(d1 - d2,1);
        
        if size(d,2) < 5000
            d = single(padarray(d,[0 5000-size(d,2)],'post'));
            bpd1 = single(padarray(bpd1,[0 5000-size(bpd1,2)],'post'));
            bpd2 = single(padarray(bpd2,[0 5000-size(bpd2,2)],'post'));
        end
        
        
        max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
        %get data ready for hist (hist is aparently the most
        %time consuming function, found using profile
        cc = 0;
        allh = zeros(window,nw);
        for ww = steps
            cc = cc + 1;
            allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
            
            %calculate the fft for each window
            [power1(cc,:),freq1(cc,:),angles1(cc,:)] = get_fft(bpd1(ww-((window/2)-1):ww+(window/2))',1000);
            [power2(cc,:),freq2(cc,:),angles2(cc,:)]  = get_fft(bpd2(ww-((window/2)-1):ww+(window/2))',1000);
            
        end
        
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        plv = (max_ent - re) ./ max_ent;
        mp = circ_mean((allh * (2*pi)));
        
        allplv(:,counter) = single(plv);
        allmphase(:,counter) = single(mp);
        
        f1 = 5; %~8Hz
        f2 = 14; %~25Hz
        allfreq = freq1(1,f1:f2);
        
        meanpower1(:,counter) = mean(power1(:,f1:f2)');
        meanpower2(:,counter) = mean(power2(:,f1:f2)');
        
        allangles1(:,counter) = circ_mean(angles1(:,f1:f2)');
        allangles2(:,counter) = circ_mean(angles2(:,f1:f2)');
        
        
        [m1 mm1] = max(power1(:,f1:f2)');
        [m2 mm2] =max(power2(:,f1:f2)');
        
        maxpower1(:,counter) = m1;
        maxpower2(:,counter) = m2;
        
        for nc = 1 : cc
            peakf1(nc,counter) = allfreq(mm1(nc));
            peakf2(nc,counter) = allfreq(mm2(nc));
            
            if allfreq(mm1(nc)) > allfreq(mm2(nc))
                peak_ratio(nc,counter) = allfreq(mm1(nc)) / allfreq(mm2(nc));
            else
                peak_ratio(nc,counter) = allfreq(mm2(nc)) / allfreq(mm1(nc));
            end
        end
    end
    
    
    
end


plv_phase_distributions{1} = allmphase(1:64,:); %all
plv_phase_distributions{2} = allmphase(18:26,:); %delay 
plv_phase_distributions{3} = allmphase(54:62,:); %iti

plv_f1_distributions{1} = peakf1(1:64,:);
plv_f1_distributions{2} = peakf1(18:26,:);
plv_f1_distributions{3} = peakf1(54:62,:);

plv_f2_distributions{1} = peakf2(1:64,:);
plv_f2_distributions{2} = peakf2(18:26,:);
plv_f2_distributions{3} = peakf2(54:62,:);

plvs = mean(allplv(1:64,:));

time_steps = steps(1:64);

save plv_phase_distributions plv_phase_distributions time_steps
save plv_f1_distributions plv_f1_distributions time_steps
save plv_f2_distributions plv_f2_distributions time_steps
save plvs plvs

plvs =reshape(allplv(1:64,:),1,64*133);


exampletrial = find(tr == 1211); %1211,1279
figure
subplot(5,1,1)
plot(steps(1:64),allplv(1:64,exampletrial))
hold on
title('plv')

subplot(5,1,2)
plot(steps(1:64),abs(allmphase(1:64,exampletrial)),'k')
hold on
title('phase')

subplot(5,1,3)
plot(steps(1:64),maxpower1(1:64,exampletrial),'r')
hold on
plot(steps(1:64),maxpower2(1:64,exampletrial),'k')
hold on
plot(steps(1:64),meanpower1(1:64,exampletrial),'r')
hold on
plot(steps(1:64),meanpower2(1:64,exampletrial),'k')
hold on
title('max and mean freq (f1-red) (f2-black)')

subplot(5,1,4)
plot(steps(1:64),peakf1(1:64,exampletrial),'r')
hold on
plot(steps(1:64),peakf2(1:64,exampletrial),'k')
hold on
title('peak freq (f1-red) (f2-black)')

subplot(5,1,5)
plot(steps(1:64),peak_ratio(1:64,exampletrial),'b')
hold on
title('freq ratio (max/min)')


sigplv = find(allplv(1:64,exampletrial) > .2);

figure
subplot(2,2,1)
scatter(maxpower1(sigplv,exampletrial),allplv(sigplv,exampletrial),'s','fill','r')
subplot(2,2,2)
scatter(maxpower2(sigplv,exampletrial),allplv(sigplv,exampletrial),'s','fill','k')
subplot(2,2,3)
scatter(maxpower1(sigplv,exampletrial),abs(allmphase(sigplv,exampletrial)),'s','fill','r')
subplot(2,2,4)
scatter(maxpower2(sigplv,exampletrial),abs(allmphase(sigplv,exampletrial)),'s','fill','k')


figure
subplot(1,2,1)
scatter(peak_ratio(sigplv,exampletrial),allplv(sigplv,exampletrial),'s','fill','b')
subplot(1,2,2)
scatter(peak_ratio(sigplv,exampletrial),abs(allmphase(sigplv,exampletrial)),'s','fill','k')



delayt = 18:30; %1000:1600
itit = 54:62; %2800:3200

figure
subplot(2,2,1)
scatter(peakf1(delayt,exampletrial),allplv(delayt,exampletrial),'g')
hold on
scatter(peakf1(itit,exampletrial),allplv(itit,exampletrial),'b')

subplot(2,2,2)
scatter(peakf2(delayt,exampletrial),allplv(delayt,exampletrial),'g')
hold on
scatter(peakf2(itit,exampletrial),allplv(itit,exampletrial),'b')

subplot(2,2,3)
scatter(peakf1(delayt,exampletrial),abs(allmphase(delayt,exampletrial)),'g')
hold on
scatter(peakf1(itit,exampletrial),abs(allmphase(itit,exampletrial)),'b')

subplot(2,2,4)
scatter(peakf2(delayt,exampletrial),abs(allmphase(delayt,exampletrial)),'g')
hold on
scatter(peakf2(itit,exampletrial),abs(allmphase(itit,exampletrial)),'b')








cd(sesdir)


