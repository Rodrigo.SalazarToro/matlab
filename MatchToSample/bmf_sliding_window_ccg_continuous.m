function bmf_sliding_window_ccg_continuous

cd('/Volumes/bmf_raid/data/monkey/ethyl/110714/session01')

sesdir = pwd;

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');

incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','Nlynx');

corrtrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','Nlynx');

cd([sesdir '/lfp/lfp2'])
ntrials = size(corrtrials,2);


load('channelpair201248.mat')
ccgs1 = [];
xc1 = [];
for t = corrtrials %corrtrials%incorrtrials
    [ic,ii] = max(correlograms{t}(:,50:101)');
    ccgs1 = [ccgs1 ii];
    xc1 = [xc1 ic];
end

load('channelpair005018.mat')
ccgs2 = [];
for t = corrtrials %corrtrials%incorrtrials
    [~,ii] = max(correlograms{t}(:,50:101)');
    ccgs2 = [ccgs2 ii];
end


acounts = zeros(51,51);
for xx = 1 : size(ccgs1,2)
    acounts(ccgs1(xx),ccgs2(xx)) = acounts(ccgs1(xx),ccgs2(xx)) + 1;
end

[xi,yi] = ind2sub(size(acounts),find(acounts>50))
scatter(xi,yi)

