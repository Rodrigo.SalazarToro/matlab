function get_power_identity

cd('/media/raid/data/monkey/betty/091001/session01')

sesdir = pwd;

mt = mtstrial('auto','ML','RTfromML','redosetNames');
N = NeuronalHist;
g = sorted_groups;
[~,ch] = intersect(N.gridPos,g);
chnumb = size(ch,2);
counter = 0;
for allch = 12 %1 : chnumb
    counter = counter + 1;
%     subplot(4,6,counter)
figure

col = {'r','g','b'};
for obj = 1:3
for loc = 1:3
    ind = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',loc,'iCueObj',obj);

    cd([sesdir filesep 'lfp' filesep 'power'])
    
    powertrials = nptDir('*_power.*');
    ntrials = size(powertrials,1);
    %make data matrix
    trial_power = cell(1,chnumb);
    for nt = ind
        load(powertrials(nt).name)
        
        for c = 1 : chnumb
            if nt == ind(1)
                trial_power{c} = power.S_delay400(ch(c),:);
            else
                trial_power{c} = [trial_power{c}; power.S_delay400(ch(c),:)];
            end
        end
    end

    hold on;plot(power.f_delay800',log(median(trial_power{allch})),col{obj})
end
end
end
trial_power;





