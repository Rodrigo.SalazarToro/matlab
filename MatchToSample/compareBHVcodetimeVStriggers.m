function [crossings,times,dif] = compareBHVcodetimeVStriggers(varargin)

%run at day level
Args = struct('plot',0,'save',0,'redo',0,'nlynx',0,'sessions',[]);
Args.flags = {'plot','save','redo','nlynx'};
Args = getOptArgs(varargin,Args);

daydir = pwd;

savename = 'BHVvsTriggers.mat';

sessions = nptdir('session0*');
for x = Args.sessions
    cd([daydir filesep sessions(x).name])

    pres = nptDir(savename);
    skip = nptDir('skip.txt');
    
    if isempty(skip) & (Args.redo | isempty(pres))
        
        basedir = pwd;
        datafile = nptDir('*.bhv');
        bhv = bhv_read(sprintf('%s%s%s',basedir,filesep,datafile.name));
        
        if ~Args.nlynx
            load(sprintf('.%sDelFiles%sdeletedfiles.mat',filesep,filesep));
        else
           trials = sort([find(single(bhv.TrialError) == 0); find(single(bhv.TrialError) == 6)])'; %all correct and incorrect trials are written 
        end
        
        files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
        nfiles = size(files,1);
        descFile = nptDir('*descriptor*');
        descriptor_info = ReadDescriptor(descFile.name);
        thech = strmatch('trigger',descriptor_info.description);
        
        %determine daq type
        type = DaqType(files(1).name);
        
        times = zeros(nfiles,11);
        crossings = zeros(nfiles,11);
        dif = zeros(nfiles,11);
        
        for f = 1 : nfiles
            if strcmp(type,'Streamer')
                [all_data,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(files(f).name,thech);
                data.rawdata = all_data;
                data.samplingRate = sampling_rate;
                d = zscore(data.rawdata);
                trigs =  round(nptThresholdCrossings(d ,25,'rising')/data.samplingRate*1000);
                num_trigs = size(trigs,2); 
            else
                data = ReadUEIFile('FileName',files(f).name,'Channels',thech,'Units','MilliVolts');
                trigs =  round(nptThresholdCrossings(data.rawdata,1000,'rising')/data.samplingRate*1000);
                num_trigs = 11;
            end
            
            if size(trigs,2) == num_trigs
                crossings(f,1:num_trigs) = trigs;
            else
                crossings(f,:) = nan(1,11);
                fprintf('Warning!!! The number of triggers differs from the expected number at trial # %d. Check out! \n',f)
                fprintf(1,'\n')
                pwd
            end
            
            times(f,1:num_trigs) = bhv.CodeTimes{trials(f)} - bhv.CodeTimes{trials(f)}(1);
            time = times(f,1:num_trigs);
            dif(f,1:num_trigs) = crossings(f,(1:num_trigs)) - time;
        end
        
        if ~isempty(find(abs(single(dif)) > 1))
             fprintf(1,['Timing difference greater than 1 ms detected     ' pwd])
             fprintf(1,'\n')
        end
        
        if Args.plot
            
            for tt = 4 : 9
                subplot(1,6,tt-3)
                hist(dif(tt,:),100)
            end
        end
    elseif ~isempty(skip)
        fprintf('Session skipped! \n')
        crossings = [];
        times = [];
        dif = [];
        
    else
        load(savename)
    end
    
    if isempty(skip) & Args.save
    end
    
    if Args.plot
        
        for tt = 4 : 9
            subplot(1,6,tt-3)
            hist(dif(tt,:),100)
        end
    end
    
    if Args.save
        save('BHVvsTriggers.mat','crossings','times','dif')
    end
    
end
