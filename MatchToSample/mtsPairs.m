function [varargout] = mtsPairs(varargin)

% run at monkey level
%
% day_depths: days X channel matrix which contains the depths for all
%   useable electrodes (uses getFlatCh).
%
% pair_directory: contains a structure for each possible pair.
%   fields: depths - contains the depths that match the days.
%           days - contains a cell array of days for the pair.
%           comb - contains the location of the electrodes in the pair.
%                   ie: pf2 is a parietal/frontal pair.
%           groups - the group numbers of the pair.
%           channels - channel number for each day
%
% ind_pairs: contains informations about the independent pairs
%   fields: depths - contains the idpendent depths
%           days - used to reference pair_directory.days for indpendent pairs.
%                   ie: if there are three days in pair_directory(1).days
%                       and there are 2 independent pairs (ind_pairs(1).number = 2)
%                       then ind_pairs(1).days might be [1;1;2]. this indicates that
%                       pair_directory(1).days(1) and
%                       pair_directory(1).days(2) have the same depths. and
%                       pair_directory(1).days(3) has different depths.
%           number - number of independent pairs.
%           comb - contains the location of the electrodes in the pair.
%                   ie: fp2 is a frontal/parietal pair.
%           groups - the group numbers of the pair.
%           channels - channel number for each day
%
% Arguments
% Args.redo: redo
% Args.total_channels: total number of groups

Args = struct('redo',0,'total_channels',64,'first_chamber','frontal','keep_flat',0);
Args.flags = {'redo','keep_flat'};
Args = getOptArgs(varargin,Args);

<<<<<<< mtsPairs.m
days = switchDay%('ML','combNtrials',150);
=======
%days = switchDay('ML','combNtrials',150);
load switchdays days
>>>>>>> 1.14
if ((isempty(nptdir('day_depths.mat'))) || (Args.redo))
    
    num_days = size(days,2);
    
    daydir = pwd;
    
    %make daysXchannel(64) matrix containing the depth of each channel recorded. If
    %channel is not recorded the entry is a zero.
    day_depths = zeros(num_days,64);
    all_channels = {};
    all_groups = {};
    for d = 1 : num_days
        cd([daydir filesep days{d}])
        %get FLAT and BAD channels
        [ch,CHcomb,iflat,group_numbers] = getFlatCh;
        %iflat.ch = actual channel number
        %ch = channel numbers - noisy channels
        %group_numbers = group number for each channel

        
        if isempty(nptdir('skip.txt'))
            cd([daydir filesep days{d} filesep 'session01'])
            
            descriptor = nptdir('*descriptor.txt');
            descriptor_info = ReadDescriptor(descriptor(1).name);
            
            groups = descriptor_info.group;
            channel_depths = descriptor_info.recdepth;
            
            for c = group_numbers
                [number ii] = intersect(groups,c);
                day_depths(d,c) = channel_depths(ii);
            end
            
            if ~Args.keep_flat
                %remove flat channels from channel list
                [i ind] = intersect(ch,iflat.ch); %take out iflat channels
                ch(ind) = [];
                %make depths for FLAT channels = 0
                day_depths(d,group_numbers(ind)) = 0;
                %remove flat channels from group list
                group_numbers(ind) = [];
                

            end
            all_groups{d} = group_numbers;
            all_channels{d} = ch;
        end
    end
    
    cd(daydir)
    
    save day_depths day_depths
    
    %make a structure that contains all possible channel pairs
    pair_directory.depths = [];
    pair_directory.days = [];
    pair_directory.comb = [];
    pair_directory.groups = [];
    %make a list of channels for each day
    pair_directory.channels = [];
    pair = 0;
    flip_channels = flipdim([1:Args.total_channels],2);
    for cc1 = 1 : Args.total_channels
        
        if strcmp(Args.first_chamber,'frontal')
            c1 = flip_channels(cc1);
        end
        
        for cc2 = (cc1 + 1) : Args.total_channels
            pair = pair + 1;
            p = 0;
            
            if strcmp(Args.first_chamber,'frontal')
                c2 = flip_channels(cc2);
            end
            
            pair_directory(pair).depths = [];
            pair_directory(pair).days = [];
            for dd = 1 : num_days
                dps = day_depths(dd,:);
                day_groups = find(dps); %find all non-zero entries
                
                %get channel numbers that correspond to group numbers
                [i1 ind1] = intersect(all_groups{dd},c1);
                [i2 ind2] = intersect(all_groups{dd},c2);
                channelDays = all_channels{dd};
                d1 = channelDays(ind1);
                d2 = channelDays(ind2);
                
                if (dps(c1) == 0) || (dps(c2) == 0)
                else
                    p = p + 1;
                    pair_directory(pair).depths(p,:) = [dps(c1) dps(c2)];
                    pair_directory(pair).days{1,p} = [days{dd}];
                    pair_directory(pair).channels(p,:) =[d1 d2];
                end
            end
            
            %frontal groups are 1:32 and parietal groups are 33:64
            if (c1 <= 32) && (c2 <= 32)
                pair_directory(pair).comb = 'ff1';
            elseif (c1 >=33) && (c2 <= 32)
                pair_directory(pair).comb = 'pf2';
            elseif (c1 > 32) && (c2 > 32)
                pair_directory(pair).comb = 'pp3';
            end
            
            pair_directory(pair).groups = [c1 c2];
        end
    end
    save pair_directory pair_directory
    
else
    load day_depths
    load pair_directory
end
flip_channels = flipdim([1:Args.total_channels],2);
ind_pairs.depths = [];
ind_pairs.days = [];
ind_pairs.number = []; %NUMBER OF INDEPENDENT PAIRS
%make a list of channels for each day
ind_pairs.channels = [];
ind_pairs.comb = [];
pair = 0;
for cc1 = 1 : Args.total_channels
    
    if strcmp(Args.first_chamber,'frontal')
        c1 = flip_channels(cc1);
    end
    
    for cc2 = (cc1 + 1) : Args.total_channels
        pair = pair + 1;
        
        %add channel information to ind_pairs
        ind_pairs(pair).channels = pair_directory(pair).channels;
        if strcmp(Args.first_chamber,'frontal')
            c2 = flip_channels(cc2);
        end
        
        %B: depths
        %I: first occurence of new pair
        %J: pair index
        [B,I,J] = unique(pair_directory(pair).depths,'rows','first');
        
        %number of indpendent pairs
        n = size(I,1);
        
        ind_pairs(pair).depths(:,:) = B;
        ind_pairs(pair).days(:,:) = J;
        ind_pairs(pair).number(1,1) = n;
        ind_pairs(pair).groups = [c1 c2];
        
        %check to make sure the depths are in order
        s = sort(ind_pairs(pair).days);
        m = sum(s - ind_pairs(pair).days);
        
        if m ~=0
            pwd
            pause
        end
        
        
        if (c1 <= 32) && (c2 <= 32)
            ind_pairs(pair).comb = 'ff1';
        elseif (c1 >=33) && (c2 <= 32)
            ind_pairs(pair).comb = 'pf2';
        elseif (c1 > 32) && (c2 > 32)
            ind_pairs(pair).comb = 'pp3';
        end
        
        
    end
    
    
end
save ind_pairs ind_pairs

