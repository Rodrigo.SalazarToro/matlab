function [signt,sign2,count] = sigValuesStim(obj,varargin)

Args = struct('tuningDir','tuning','surrogates','combined','rule',1,'sigLevel',4,'ML',0,'figTit','Surrogate combined','ind',[],'EPstim',0);
Args.flags = {'ML','EPstim'};
[Args,modvarargin] = getOptArgs(varargin,Args);


count = 1;
plevel = 1;

tuneArgs = {'Obj' 'Loc'};

% % [r,indLoc] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0;'IDELoc' [3 4] '>=' 1});r
% % [r,indObj] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0; 'IDEObj' [3 4] '>=' 1});r
% % ind = union(indLoc,indObj);
if isempty(Args.ind)
    [r,ind] = get(obj,'snr',99,'Number',modvarargin{:});
else
    ind = Args.ind;
end
% tenHz = [];
for n = ind
    %     if ~isempty(nptDir(idedays{d}))
    cd(obj.data.setNames{n})
    pairg = obj.data.Index(n,10:11);
    if Args.ML
        s = Args.rule;
    else
        load rules.mat
        s = find(r == Args.rule);
    end
    switch Args.surrogates
        case {'combined'}
            
            load('cohInter.mat');
            value = squeeze(Day.session(s).C(:,obj.data.Index(n,1),:));
            load('generalSur.mat');
            sur = squeeze(Day.session(s).Prob(:,Args.sigLevel,1,:));
            clear Day
            for tune = 1 : 2; sigvalues(tune,:,:) = value >= sur;end
            clear value
        case {'stim'}
            if ~isempty(nptDir(Args.tuningDir))
                clear value
                for tune = 1 : 2
                    cd(Args.tuningDir)
                    if Args.EPstim
                        load(sprintf('iCue%sg%04.0fg%04.0fRule%dEPstim.mat',tuneArgs{tune},pairg(1),pairg(2),Args.rule));
                    else
                        load(sprintf('iCue%sg%04.0fg%04.0fRule%d.mat',tuneArgs{tune},pairg(1),pairg(2),Args.rule));
                    end
                    for p = 1 : 4
                        value(tune,:,p,:) = groupN(p).C;
                    end
                    sigvalues = zeros(2,65,4);
                    cd(obj.data.setNames{n})
                    for item = 1 : 3
                        load(sprintf('generalSur%s%d.mat',tuneArgs{tune},item))
                        sur = squeeze(Day.session(s).Prob(:,Args.sigLevel,1,:));
                        sigvalues(tune,:,:) = (squeeze(value(tune,:,:,item)) >= sur) + squeeze(sigvalues(tune,:,:));
                        
                    end
                    sigvalues = sigvalues ~= 0;
                    clear Day
                    
                end
            end
        case {'none'}
            
            sigvalues = ones(2,65,4);
    end
    cd(obj.data.setNames{n})
    if ~isempty(nptDir(Args.tuningDir))
        %         cd(Args.tuningDir)
        for tune = 1 : 2
            cd(Args.tuningDir)
            if Args.EPstim
                file = nptDir(sprintf('iCue%sg%04.0fg%04.0fRule%dEPstim.mat',tuneArgs{tune},pairg(1),pairg(2),Args.rule));
            else
                file = nptDir(sprintf('iCue%sg%04.0fg%04.0fRule%d.mat',tuneArgs{tune},pairg(1),pairg(2),Args.rule));
                
            end
            %             cd(obj.data.setNames{n})
            %             cd grams
            %             cfiles = nptDir(sprintf('cohgram%sRule1%s*.mat',files(i).name(8:17),files(i).name(5:7)));
            %             clear ntrials
            %             for fff = 1 : length(cfiles)
            %                 load(cfiles(fff).name)
            %                 ntrials(fff) = length(trials);
            %             end
            %             cd(obj.data.setNames{n})
            %             if ~isempty(cfiles) && min(ntrials) > 150
            
            load(file.name)
            for p = 1 : 4
                %                 signt(count,p,:) = groupN(p).rdiff >= groupN(p).pvalues(:,plevel);
                signt(count,p,:) = filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff) >= filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues(:,plevel));
                signt(count,p,:) = squeeze(signt(count,p,:)) .* squeeze(sigvalues(tune,:,p))';
                %             subplot(4,1,p)
                %             plot(f,filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff)); hold on;  plot(f,filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues))
                %             plot(f, squeeze(signt(count,p,:))./10,'.')
                %             ylim([0 0.2])
                temp = zeros(65,1);
                for c = 1 :3
                    value = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).rdiff);
                    difVar1 = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues(:,plevel+1));
                    
                    difVar2 = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues(:,4+plevel+1));
                    
                    temp = temp + (value <= difVar1) + (value >= difVar2);
                    %                     subplot(4,3,(p-1)*3+c)
                    %                     plot(f,value)
                    %                     hold on
                    %                     plot(f,filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues))
                    %                     ylim([-0.2 0.2])
                end
                %                 if p == 3 && signt(count,p,8) == 1;
                %                     tenHz = [tenHz n];
                %                 end
                sign2(count,p,:) = (temp ~= 0).* squeeze(sigvalues(tune,:,p))';
                %                 plot(f, squeeze(sign2(count,p,:))./10,'.')
            end
            count = count + 1;
            cd(obj.data.setNames{n})
            %             end
            %             pause
            %             clf
        end
        cd(obj.data.setNames{n})
        
    end
    
    % end
    cd(obj.data.setNames{n})
end
count = count -1;
cd(Args.tuningDir)

figure
for p = 1 : 4; subplot(2,4,p);plot(f,squeeze(sum(signt(:,p,:),1))/(count-1)); axis([0 100 0 0.2]);end

for p = 1 : 4; subplot(2,4,p+4);plot(f,squeeze(sum(sign2(:,p,:),1))/(count-1)); axis([0 100 0 0.2]);end
xlabel('Frequency [Hz]')
ylabel('Fraction of pairs with stimulus effect')
subplot(2,4,1); ylabel('STD bootstrap test')
subplot(2,4,5); ylabel('Two groups bootstrap test')
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
for sb = 1 : 4; subplot(2,4,sb);title(epochs{sb}); end
count = count/2;
if ~isempty(Args.figTit)
    set(gcf,'Name',Args.figTit)
else
    set(gcf,'Name',sprintf('n = %d only beta stim pairs',count))
end
