function [rc,bins] = spikeCountCorr(sp1,sp2,mts,trials,varargin)
% to be run in the xcorr directory
Args = struct('save',0,'redo',0,'binSize',50,'addName',[],'iterations',500,'surrogate',0,'xcorr',0,'tstartend',[-500 1300;-1600 200],'plot',0);
Args.flags = {'save','redo','surrogate','xcorr','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args);

bins = {[Args.tstartend(1,1) : Args.binSize : Args.tstartend(1,2)] [Args.tstartend(2,1):Args.binSize:Args.tstartend(2,2)]};


if Args.xcorr
    prefix = 'xccorr';
else
    prefix = 'sccorr';
end
if Args.surrogate
    matfile = sprintf('%sg%s%sg%s%sSur%s.mat',prefix,sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);
else
    matfile = sprintf('%sg%s%sg%s%s%s.mat',prefix,sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);
end
if isempty(nptDir(matfile)) || Args.redo
    if Args.surrogate
        rc = zeros(Args.iterations,2,length(bins{1}));
        
        trials1 = trials;
        for ii = 1 : Args.iterations
            trials2 = trials(randperm(length(trials)));
            rc(ii,:,:) = sccorr(sp1,sp2,mts,trials1,trials2,bins,modvarargin{:});
        end
        
    else
        if Args.plot
            [rc,mincoi] = sccorr(sp1,sp2,mts,trials,trials,bins,'plotSingleTrial',modvarargin{:});
        else
            [rc,mincoi] = sccorr(sp1,sp2,mts,trials,trials,bins,modvarargin{:});
        end
    end
    if Args.save
        if ~Args.surrogate & ~Args.xcorr
            save(matfile,'rc','bins','mincoi')
        else
            save(matfile,'rc','bins')
        end
        display(['Saving ' pwd '/' matfile])
    end
else
    load(matfile)
end


function [rc,mincoi] = sccorr(sp1,sp2,mts,trials1,trials2,bins,varargin)


Args = struct('xcorr',0,'BinSize',50,'xcorrBin',10,'tstartend',[-500 1300;-1600 200],'randLag',0,'plotSingleTrial',0,'removeMean',0);
Args.flags = {'xcorr','randLag','plotSingleTrial','removeMean'};
Args = getOptArgs(varargin,Args);

rc = zeros(2,length(bins{1}));
mincoi = zeros(2,length(bins{1}));
tbins = {[bins{1}(1)-10 bins{1} bins{1}(end)+10] [bins{2}(1)-10 bins{2} bins{2}(end)+10]};


nbin = abs((bins{1}(1) - bins{1}(2)) / Args.xcorrBin);
for al = 1 : 2
    if al == 1
        ref = mts.data.CueOnset;
    else
        ref = mts.data.MatchOnset;
    end
    
    if Args.xcorr
        txbins{1} = [tbins{1}(1) tbins{1}(2):Args.xcorrBin:tbins{1}(end-1) tbins{1}(end)];
        txbins{2} = [tbins{2}(1) tbins{2}(2):Args.xcorrBin:tbins{2}(end-1) tbins{2}(end)];
        c1 = zeros(length(trials1),length(txbins{al})-2);
        c2 = c1;
        for tr = 1 : length(trials1)
            thetrial1 = trials1(tr);
            thetrial2 = trials2(tr);
            sptime1 = sp1.data.trial(thetrial1).cluster(1).spikes - ref(thetrial1);
            sptime2 = sp2.data.trial(thetrial2).cluster(1).spikes - ref(thetrial2);
            
            c1t = hist(sptime1,txbins{al});
            c2t = hist(sptime2,txbins{al});
            
            c1(tr,:) = c1t(2:end-1);
            c2(tr,:) = c2t(2:end-1);
            
        end
        if Args.removeMean
            c1 = c1 - repmat(mean(c1),size(c1,1),1);
            c2 = c2 - repmat(mean(c2),size(c2,1),1);
      
        end
        for ti = 1 : size(tbins{al},2) - 3
            
            stbin = (ti-1)* (Args.BinSize / Args.xcorrBin) +1;
            endbin = stbin + (Args.BinSize / Args.xcorrBin) -1;
            if Args.randLag
                ibin = [stbin:endbin];
                nleng = length(ibin);
                vc1 = reshape(c1(:,ibin(randperm(nleng))),1,size(c1,1)*nbin);
                vc2 = reshape(c2(:,ibin(randperm(nleng))),1,size(c2,1)*nbin);
                
            else
                vc1 = reshape(c1(:,stbin:endbin),1,size(c1,1)*nbin);
                vc2 = reshape(c2(:,stbin:endbin),1,size(c2,1)*nbin);
            end
            
            R = corrcoef(vc1,vc2);
            rc(al,ti) = R(1,2);
            
            
        end
        if al == 1 && Args.plotSingleTrial && sum(sum(abs(rc(1,1:3)) > 0.3)) >= 3
            c1rs = reshape(c1(:,1:3),1, size(c1,1)*3);
            c2rs = reshape(c2(:,1:3),1, size(c2,1)*3);
            plot(c1rs,c2rs,'.')
            rc(1,1:3)
            pause
            clf
        end
    else
        % from Ponce-Alvarez 2013
        c1 = zeros(length(trials1),length(bins{al}));
        c2 = c1;
        ctot = zeros(2,length(trials1),length(bins{al}));
        for tr = 1 : length(trials1)
            thetrial1 = trials1(tr);
            thetrial2 = trials2(tr);
            sptime1 = sp1.data.trial(thetrial1).cluster(1).spikes - ref(thetrial1);
            sptime2 = sp2.data.trial(thetrial2).cluster(1).spikes - ref(thetrial2);
            
            c1t = hist(sptime1,tbins{al});
            c2t = hist(sptime2,tbins{al});
            c1(tr,:) = c1t(2:end-1);
            c2(tr,:) = c2t(2:end-1);
        end
        
        mc1 = mean(c1,1);
        mc2 = mean(c2,1);
        ctot(1,:,:) = c1;
        ctot(2,:,:) = c2;
        mincoi(al,:) = mean(squeeze(min(ctot,[],1)));
        for ti = 1 : size(c1,2) % loops over time bins
            upper = 0;
            low1 = 0;
            low2 = 0;
            for tr = 1 : length(trials1) % loop over trials
                upper = ((c1(tr,ti) - mc1(ti)) * (c2(tr,ti) - mc2(ti))) + upper;
                low1 = (c1(tr,ti) - mc1(ti))^2 + low1;
                low2 = (c2(tr,ti) - mc2(ti))^2 + low2;
            end
            rc(al,ti) = upper / sqrt(low1*low2);
        end
    end
end