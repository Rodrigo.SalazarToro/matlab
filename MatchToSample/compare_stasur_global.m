function compare_stasur_global


%run at day level
load idedays
s = processDays(sta,'days',idedays([1:2,4:11]),'NoSites')

%load s s

stds = s.data.Index(:,4);


%INDEX 3:determine if cluster is multi or single (multi = 1, single = 2,3 etc.)
s_count = s.data.Index(:,20); %spike count for all
numsta = size(s.data.Index,1);
bad = 0;
good = 0;
for n = 1: numsta
    %get epoch 4
     if s.data.Index(n,1) ~= s.data.Index(n,2) %get rid of all self sta's
        good = good + 1;
        sur(good) = s.data.surrogateslfpsta{n,1}(2,5) / stds(n); %get positive tail
     else
        bad = bad + 1;
        s_count_self(bad) = n;
    end
end

s_count(s_count_self) = [];

numg = size(s.data.global_surrogates_sta,1);

for g = 1 : numg
    
    global_p = prctile(s.data.global_surrogates_sta{g,1},[.05 99.95]);
    global_p = global_p(2,:);
    globals(g,:) = global_p;
end


increments = s.data.global_surrogates_sta{1,2};

subplot(2,2,1);
scatter(log(s_count),log(sur),'r')
%axis([100 30000 0 .25])
title('local')
subplot(2,2,2)
for g = 1 : numg
    scatter(log(increments),log(globals(g,:)),'b');
    hold on
end
%axis([100 30000 0 .25])
title('global')

subplot(2,2,3);
scatter(s_count,sur,'r')
axis([100 35000 0 .25])
title('local')
subplot(2,2,4)
for g = 1 : numg
    scatter(increments,globals(g,:),'b');
    hold on
end
axis([100 35000 0 .25])
title('global')



figure
scatter(log(s_count),log(sur),'r')
hold on
for g = 1 : numg
    scatter(log(increments),log(globals(g,:)),'b');
    hold on
end






