function run_bmf_coherence(varargin)

Args = struct('days',[],'sessions',[],'coherence',0,'redo_coherence',0,'theor_error',0,'surrogates',0,'fit_surrogates',0,'global_surrogate',0,'fit_global_surrogate',0,'threshold_coh',0,'threshold_coh_global',0,'match_trials',0);
Args.flags = {'coherence','redo_coherence','theor_error','surrogates','fit_surrogates','global_surrogate','fit_global_surrogate','threshold_coh','threshold_coh_global','match_trials'};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;

%parameters
if Args.theor_error
    % %     params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'err',[2 .341],'trialave',1); %these params work if data is downsampled
else
    params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1,'err',[1 .318]); %these params work if data is downsampled 1-p
end
movingwin = [.200 .05]; %this is in seconds


monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            rejected_trials = get_reject_trials;
            %this gets the groups that pass reject_filter.m
            [ch chpairs gpairs] = bmf_groups;
            nch = size(ch,2);
            
            total_pairs = size(chpairs,1);
            
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
            
            for ides = 1 : 5
                trials{ides} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'iCueObj',ides,'notfromMTStrial','Nlynx');
            end
            trials{6} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
            trials{7} = mtsgetTrials(mt,'BehResp',0,'Nlynx');
            
            %get interleaved fixation trials
            fixtrials = find(mt.data.CueObj == 56)'; %see ProcessSessionMTS
            [~,rej] = intersect(fixtrials,rejected_trials);
            fixtrials(rej) = [];
            trials{8} = fixtrials;
            
            
            sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
            sample_off = floor(mt.data.CueOffset); %sample off
            match = floor(mt.data.MatchOnset);    %match
            
            cd([sesdir filesep 'lfp']);
            lfpdir = pwd;
            lfptrials = nptDir('ethyl*');
            
            if Args.coherence
                for p = 1 : total_pairs
                    clc
                    total_pairs - p
                    cd(lfpdir)
                    
                    %run for 5 ides + all trials
                    if Args.match_trials
                        ide_run = 1:5;
                    else
                        ide_run = 1:8;
                    end
                    for all_ides = ide_run
                        idetrials = trials{all_ides};
                        ntrials = size(idetrials,2);
                        
                        if Args.match_trials
                            nt = cellfun(@length,trials);
                            min_nt = min(nt(1:5));
                            
                            %match the trial counts to the mininmum number of trials
                            permtrials = idetrials(randperm(ntrials));
                            idetrials = sort(permtrials(1:min_nt));
                            ntrials = min_nt;
                        end
                        
                        p1trials = zeros(ntrials,650); %data is downsampled
                        p2trials = zeros(ntrials,650);
                        counter = 0;
                        for t = idetrials
                            counter = counter + 1;
                            fix = sample_on(t) - 500; %this is approx fixation
                            if all_ides == 8 %fixation
                                sac = match(t);
                            else
                                sac = match(t) + 200; %match plus saccade
                            end
                            
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            p1t = data.rawdata(chpairs(p,1),:);
                            p2t = data.rawdata(chpairs(p,2),:);
                            
                            %                         params = struct('tapers',[2 3],'Fs',1000,'fpass',[50 70]);
                            %                         lfp1 = p1t(fix:sac);
                            %                         [S1,F1] = mtspectrumc(lfp1',params);
                            
                            
                            %cut, get rid of 60! and detrend
                            p1t = preprocessinglfp(p1t(fix:sac),'detrend')';
                            p2t = preprocessinglfp(p2t(fix:sac),'detrend')';
                            
                            
                            %                         params = struct('tapers',[2 3],'Fs',200,'fpass',[50 70]);
                            %                         [S2,F2] = mtspectrumc(p1t',params);
                            
                            
                            %                         plot(F1,S1redo_coherence);hold on,plot(F2,S2,'r')
                            %                         pause
                            %                         close all
                            %
                            
                            
                            
                            %get all trials for each channel
                            p1trials(counter,(1:size(p1t,2))) = p1t;
                            p2trials(counter,(1:size(p2t,2))) = p2t;
                        end
                        data1 = p1trials';
                        data2 = p2trials';
                        
                        if all_ides == 6
                            if Args.theor_error
                                [cohgram.C{1},cohgram.phi{1},~,~,~,cohgram.t{1},cohgram.f{1},idecohgram.confC{1},cohgram.phistd{1},idecohgram.Cerr{1}] = cohgramc(data1,data2,movingwin,params);
                            else
                                [cohgram.C{1},cohgram.phi{1},cohgram.S12{1},cohgram.S1{1},cohgram.S2{1},cohgram.t{1},cohgram.f{1},cohgram.confC{1},cohgram.phistd{1}] = cohgramc(data1,data2,movingwin,params);
                            end
                            cohgram.trials{1} = idetrials;
                            cohgram.groups{1} = gpairs(p,:);
                        else
                            if Args.theor_error
                                [idecohgram.C{all_ides},idecohgram.phi{all_ides},~,~,~,idecohgram.t{all_ides},idecohgram.f{all_ides},idecohgram.confC{all_ides},idecohgram.phistd{all_ides},idecohgram.Cerr{all_ides}] = cohgramc(data1,data2,movingwin,params);
                            else
                                [idecohgram.C{all_ides},idecohgram.phi{all_ides},~,~,~,idecohgram.t{all_ides},idecohgram.f{all_ides},idecohgram.confC{all_ides},idecohgram.phistd{all_ides}] = cohgramc(data1,data2,movingwin,params);
                            end
                            idecohgram.trials{all_ides} = idetrials;
                            idecohgram.groups{all_ides} = gpairs(p,:);
                        end
                    end
                    if ~exist('cohgrams','dir')
                        mkdir('cohgrams')
                    end
                    cd([lfpdir filesep 'cohgrams']);
                    
                    write_info = writeinfo(dbstack);
                    if ~Args.match_trials
                        coh_pairs = [ 'cohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(coh_pairs,'cohgram','idecohgram','write_info');
                    else
                        coh_pairs = [ 'matchtrialcohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(coh_pairs,'idecohgram','write_info');
                    end
                end
            end
            
            if Args.global_surrogate
                for all_perms = 1 : 1000
                    1000 - all_perms
                    cd(lfpdir)
                    sur_global = ['globalsurcohgram'];
                    idetrials = trials{6};
                    ntrials = size(idetrials,2);
                    perms = randperm(ntrials);
                    
                    p1trials = zeros(ntrials,650); %data is downsampled
                    p2trials = zeros(ntrials,650);
                    counter = 0;
                    for t = idetrials(perms) %randomize trials
                        rch = randperm(nch); %randomize channels
                        rch1 = rch(1);
                        rch2 = rch(2);
                        
                        counter = counter + 1;
                        fix = sample_on(t) - 500; %this is approx fixation
                        sac = match(t) + 200; %match plus saccade
                        
                        [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                        p1t = data.rawdata(rch1,:);
                        p2t = data.rawdata(rch2,:);
                        
                        %cut, get rid of 60! and detrend
                        p1t = preprocessinglfp(p1t(fix:sac),'detrend')';
                        p2t = preprocessinglfp(p2t(fix:sac),'detrend')';
                        
                        %get all trials for each channel
                        p1trials(counter,(1:size(p1t,2))) = p1t;
                        p2trials(counter,(1:size(p2t,2))) = p2t;
                    end
                    data1 = p1trials';
                    data2 = p2trials';
                    
                    alltrials = 1 : ntrials;
                    
                    [globalsurcohgram.C{all_perms,6},globalsurcohgram.phi{all_perms,6},~,~,~,globalsurcohgram.t{all_perms,6},globalsurcohgram.f{all_perms,6},globalsurcohgram.confC{all_perms,6},globalsurcohgram.phistd{all_perms,6}] = cohgramc(data1,data2,movingwin,params);
                    globalsurcohgram.trials{all_perms,6} = ntrials; %this is the
                    
                    
                    for ide = 1 : 5
                        idet =  size(trials{ide},2);
                        rdata1 = data1(:,alltrials(1:idet));
                        rdata2 = data2(:,alltrials(1:idet));
                        alltrials(1:idet) = [];
                        
                        [globalsurcohgram.C{all_perms,ide},globalsurcohgram.phi{all_perms,ide},~,~,~,globalsurcohgram.t{all_perms,ide},globalsurcohgram.f{all_perms,ide},globalsurcohgram.confC{all_perms,ide},globalsurcohgram.phistd{all_perms,ide}] = cohgramc(rdata1,rdata2,movingwin,params);
                        globalsurcohgram.trials{all_perms,ide} = idet;
                    end
                end
                cd([lfpdir filesep 'cohgrams']);
                
                write_info = writeinfo(dbstack);
                %                 save(sur_global,'globalsurcohgram','write_info');
            end
            if Args.surrogates
                cd([lfpdir filesep 'cohgrams']);
                
                for p = 1 : total_pairs
                    if ~exist([ 'suridecohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3) '.mat'],'file') || ~exist([ 'surcohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3) '.mat'],'file')
                        cd(lfpdir)
                        
                        idetrials = trials{6};
                        ntrials = size(idetrials,2);
                        
                        p1trials = zeros(ntrials,650); %data is downsampled
                        p2trials = zeros(ntrials,650);
                        counter = 0;
                        for t = idetrials
                            counter = counter + 1;
                            fix = sample_on(t) - 500; %this is approx fixation
                            sac = match(t) + 200; %match plus saccade
                            
                            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(t).name);
                            p1t = data.rawdata(chpairs(p,1),:);
                            p2t = data.rawdata(chpairs(p,2),:);
                            
                            %cut, get rid of 60! and detrend
                            p1t = preprocessinglfp(p1t(fix:sac),'detrend')';
                            p2t = preprocessinglfp(p2t(fix:sac),'detrend')';
                            
                            %get all trials for each channel
                            p1trials(counter,(1:size(p1t,2))) = p1t;
                            p2trials(counter,(1:size(p2t,2))) = p2t;
                        end
                        data1 = p1trials';
                        data2 = p2trials';
                        perms = [];
                        for all_perms = 1 : 1000
                            clc
                            1000 - all_perms
                            total_pairs - p
                            perms = randperm(ntrials);
                            perms1 = randperm(ntrials);
                            perms2 = randperm(ntrials);
                            
                            permdata1 = data1(:,perms1);
                            permdata2 = data2(:,perms2);
                            
                            [surcohgram.C{all_perms,1},surcohgram.phi{all_perms,1},~,~,~,surcohgram.t{all_perms,1},surcohgram.f{all_perms,1},surcohgram.confC{all_perms,1},surcohgram.phistd{all_perms,1}] = cohgramc(permdata1,permdata2,movingwin,params);
                            surcohgram.trials{all_perms,1} = ntrials; %this is the
                            
                            rdata1 = [];
                            rdata2 = [];
                            for ide = 1 : 5
                                %randomize the trials, with the correct trial count
                                idet =  size(trials{ide},2);
                                ptrials = perms(1:idet);
                                perms(1:idet) = [];
                                rdata1 = data1(:,ptrials);
                                rdata2 = data2(:,ptrials);
                                
                                [suridecohgram.C{all_perms,ide},suridecohgram.phi{all_perms,ide},~,~,~,suridecohgram.t{all_perms,ide},suridecohgram.f{all_perms,ide},suridecohgram.confC{all_perms,ide},suridecohgram.phistd{all_perms,ide}] = cohgramc(rdata1,rdata2,movingwin,params);
                                
                                suridecohgram.trials{all_perms,ide} = ptrials;
                                suridecohgram.groups{all_perms,ide} = gpairs(p,:);
                            end
                        end
                        
                        cd([lfpdir filesep 'cohgrams']);
                        
                        write_info = writeinfo(dbstack);
                        file = [ 'surcohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(file,'surcohgram','write_info');
                        
                        file = [ 'suridecohgram' num2strpad(gpairs(p,1),3) num2strpad(gpairs(p,2),3)];
                        save(file,'suridecohgram','write_info');
                    end
                end
            end
            
            %fit surrogates
            if Args.fit_surrogates
                cd([lfpdir filesep 'cohgrams']);
                fit_bmfcohgram_thresh('pairwise');
            end
            
            if Args.fit_global_surrogate
                cd([lfpdir filesep 'cohgrams']);
                fit_bmfcohgram_thresh('global');
            end
            
            if Args.threshold_coh
                cd([lfpdir filesep 'cohgrams']);
                threshold_coherence('pairwise');
            end
            
            if Args.threshold_coh_global
                cd([lfpdir filesep 'cohgrams']);
                threshold_coherence('global');
            end
            
        end
    end
end

cd(monkeydir)












