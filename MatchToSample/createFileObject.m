function createFileObject(obj,varargin)

Args = struct('redo',0,'wintermute',0,'plength',400,'thresh',2,'days',[],'ML',0,'rule',1,'sessions',[],'Nlynx',0,'blockBased',0,'session99',0);
Args.flags = {'redo','wintermute','ML','Nlynx','blockBased','session99'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'rule'});


params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1);

align = {'CueOnset','MatchOnset','FirstSac'};

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
rules = {'ide' 'loc'};
if isempty(Args.days)
    days = switchDay('notForLFP');
else
    days = Args.days;
end

sdir = pwd;
clear t C phi
for d = 1 : length(days)
    cd(sdir)
    cd(days{d})
    fprintf('%s',days{d})
    sessions = nptDir('session0*');
    if Args.ML
        if ~isempty(Args.sessions)
            clear sessions
            for ss = 1 : length(Args.sessions)
                sessions(ss).name = sprintf('session%s',Args.sessions{ss});
                st = 1;
                et = length(Args.sessions);
            end
        else
            st = 1;
            et = 1;
        end
    elseif Args.session99
        st = 1;
        et =1;
        sessions(1).name = 'session99';
    else
        st = 2;
        et = length(sessions);
    end
    dday = pwd;
    for s = st : et
        cd(dday)
        cd(sessions(s).name)
        %at the session level
        
        skip = nptDir('skip.txt');
        if isempty(skip)
            is = mtsispike('auto','redosetNames',varargin2{:},'redo');
            mtst = mtstrial('auto','redosetNames');
            
            allloc = vecr(unique(mtst.data.CueLoc));
            allobj = unique(mtst.data.CueObj);
            if ~Args.ML && ~Args.session99; Args.rule = mtst.data.Index(1,1); end
            switch obj
                
                case 'nineStimCohSpike'
                    comb = nchoosek([1:is.data.numSets],2);
                    
                    for cm = 1 : size(comb,1)
                        cd(is.data.setNames{comb(cm,1)})
                        load('ispikes.mat');
                        d1 = sp;
                        cd(is.data.setNames{comb(cm,2)})
                        load('ispikes.mat');
                        d2 = sp;
                        if str2num(d1.data.groupname(end-3:end)) <= 8
                            info.cortex(1) = 'P';
                        else
                            info.cortex(1) = 'F';
                        end
                        
                        if str2num(d2.data.groupname(end-3:end)) <= 8
                            info.cortex(2) = 'P';
                        else
                            info.cortex(2) = 'F';
                        end
                        info.unit = [d1.data.cellname(end) d2.data.cellname(end)];
                        mincov = [];
                        maxcov = [];
                        count = 1;
                        
                        %                     if Args.wintermute
                        %                         for c = 1 : 9; mtc{c} = mtst; dd1{c} = d1; dd2{c} = d2; var1{c} = 'CueObj'; param1{c} = allobj(cueComb(2,c)); var2{c} = 'CueLoc'; param2{c} = allloc(cueComb(1,c));var3{c} = 'stable'; var4{c} = 'BehResp'; param4{c} = 1; var5{c} = 'surrogate'; end
                        %                          [xcova,sxcova,lagst,trials,nevent,yfit,cPeak,sur] = dfeval(@xcorPcut,mtc,dd1,dd2,var1,param1,var2,param2,var3,var4,param4,var5,'lookupURL','wintermute.cns.montana.edu','configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                        %                          lags = lagst{1};
                        %
                        %                          for c = 1 : 9; mincov = [mincov min(min(xcova{cue}))]; maxcov = [maxcov max(max(xcova{cue}))]; end
                        %
                        %                     else
                        for cue = 1 : 9
                            
                            [xcova{cue},sxcova{cue},lags,trials{cue},nevent{cue},yfit{cue},cPeak{cue},popout{cue},append{cue}] = xcorPcut(mtst,d1,d2,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1,varargin2{:});
                            mincov = [mincov min(min(xcova{cue}))];
                            maxcov = [maxcov max(max(xcova{cue}))];
                            %                         thresh{cue} = nanmean(yfit{cue},2) + Args.thresh * nanstd(yfit{cue},[],2);
                        end
                        %                     end
                        minc = min(mincov);
                        maxc = max(maxcov);
                        cd ../..
                        newdir = sprintf('combg%s%sg%s%s',d1.data.groupname,d1.data.cellname,d2.data.groupname,d2.data.cellname);
                        mkdir(newdir);
                        cd(newdir)
                        save('nineStimCohSPIKES.mat','xcova','sxcova','lags','minc','maxc','info','trials','nevent','yfit','cPeak','popout','append')
                        clear xcova lags trials minc maxc info trials nevent yfit cPeak append
                        
                        cd ..
                    end
                case 'nineStimSFC'
                    
                    spikeFieldCoh(params,'save','nineStim','stable','BehResp',1,'surrogate','wintermute','SFCProb',[0.95 0.99 0.999 0.9999 0.99999]);
                    %                 spikeFieldCoh(params,'save','nineStim','stable','BehResp',1,'wintermute');
                case 'nineStimPsth'
                    
                    for g = 1 : is.data.numSets
                        
                       try
                        cd(is.data.setNames{g})
                        catch
                           cd(regexprep(is.data.setNames{g},'media','Volumes')) 
                        end
                        
                        load ispikes.mat
                        
                        allymin = [];
                        allymax = [];
                        
                        if Args.Nlynx
                            ncues = length(allobj);
                        else
                            ncues = 9;
                        end
                        bins = cell(ncues,3);
                        spikefr = cell(ncues,3);
                        A= cell(ncues,3);
                        ymin= cell(ncues,3);
                        ymax= cell(ncues,3);
                        reference= cell(ncues,3);
                        fr= cell(ncues,3);
                        if Args.blockBased
                            nbl = length(find(diff(mtst.data.Index(:,1)) ~= 0)) + 1;
                        else
                            nbl = 1;
                        end
                        for bl = 1 : nbl
                            for cue = 1 : ncues
                                for al = 1 : 3
                                    if Args.Nlynx
                                        [bins{cue,al},spikefr{cue,al},A{cue,al},ymin{cue,al},ymax{cue,al},reference{cue,al},ttspikes, tttrials, fr{cue,al}] = plotMTSpsth(mtst,sp,'CueObj',allobj(cue),'Nocharlie',1,'stable','BehResp',1,'alignement',align{al},varargin2{:});
                                        
                                    elseif Args.blockBased
                                        [bins{cue,al},spikefr{cue,al},A{cue,al},ymin{cue,al},ymax{cue,al},reference{cue,al},ttspikes, tttrials, fr{cue,al}] = plotMTSpsth(mtst,sp,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1,'alignement',align{al},'block',bl,varargin2{:});
                                    else
                                        [bins{cue,al},spikefr{cue,al},A{cue,al},ymin{cue,al},ymax{cue,al},reference{cue,al},ttspikes, tttrials, fr{cue,al}] = plotMTSpsth(mtst,sp,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1,'alignement',align{al},'rule',Args.rule,varargin2{:});
                                    end
                                end
                            end
                            if isempty(A{1,1}); error('dfasfdas'); end
                            if Args.blockBased
                                save(sprintf('nineStimPSTHb%d.mat',bl),'bins','spikefr','A','ymin','ymax','reference','fr')
                                display(['saving' ' ' pwd  '/' sprintf('nineStimPSTHb%d.mat',bl)])
                                
                            else
                                save(sprintf('nineStimPSTH%d.mat',Args.rule),'bins','spikefr','A','ymin','ymax','reference','fr')
                                display(['saving' ' ' pwd  '/' sprintf('nineStimPSTH%d.mat',Args.rule)])
                            end
                        end
                    end
                    cd(sdir)
                    
                case 'MatchPsth'
                    for g = 1 : is.data.numSets
                        
                        cd(is.data.setNames{g})
                        
                        load ispikes.mat
                        %%
                        matfile = sprintf('MatchLocPSTH%d.mat',Args.rule);
                        if Args.redo || isempty(nptDir(matfile))
                            locs = unique(mtst.data.CueLoc);
                            objs = unique(mtst.data.CueObj);
                            
                            clear bins spikefr A ymin ymax reference ttspikes tttrials fr
                            
                            for loc = 1 : size(locs)
                                
                                for al = 1 : 3
                                    
                                    [bins{loc,al},spikefr{loc,al},A{loc,al},ymin{loc,al},ymax{loc,al},reference{loc,al},ttspikes, tttrials, fr{loc,al}] = plotMTSpsth(mtst,sp,'MatchPos1',locs(loc),'MatchPos2',locs(loc),'unionSel','MatchPos1MatchPos2','stable','BehResp',1,'alignement',align{al},'rule',Args.rule,varargin2{:});
                                    
                                end
                                
                            end
                            save(matfile,'bins','spikefr','A','ymin','ymax','reference','fr')
                        end
                        %%
                        matfile = sprintf('MatchObjPSTH%d.mat',Args.rule);
                        if Args.redo || isempty(nptDir(matfile))
                            clear bins spikefr A ymin ymax reference ttspikes tttrials fr
                            for ob = 1 : size(objs)
                                for al = 1 : 3
                                    
                                    [bins{ob,al},spikefr{ob,al},A{ob,al},ymin{ob,al},ymax{ob,al},reference{ob,al},ttspikes, tttrials, fr{ob,al}] = plotMTSpsth(mtst,sp,'MatchObj1',objs(ob),'MatchObj2',objs(ob),'unionSel','MatchObj1MatchObj2','stable','BehResp',1,'alignement',align{al},'rule',Args.rule,varargin2{:});
                                    
                                end
                                
                            end
                            save(matfile,'bins','spikefr','A','ymin','ymax','reference','fr')
                        end
                        %%
                        matfile = sprintf('Match18PSTH%d.mat',Args.rule);
                        if Args.redo || isempty(nptDir(matfile))
                            clear bins spikefr A ymin ymax reference ttspikes tttrials fr
                            
                            for match = 1 : 18
                                for al = 1 : 3
                                    
                                    [bins{match,al},spikefr{match,al},A{match,al},ymin{match,al},ymax{match,al},reference{match,al},ttspikes, tttrials, fr{match,al}] = plotMTSpsth(mtst,sp,'Match',match,'stable','BehResp',1,'alignement',align{al},'rule',Args.rule,varargin2{:});
                                    
                                end
                                
                            end
                            save(matfile,'bins','spikefr','A','ymin','ymax','reference','fr')
                            display([pwd '/' matfile])
                        end
                    end
                    cd(sdir)
                    
                case 'threeSacPsth'
                    for g = 1 : is.data.numSets
                        
                        cd(is.data.setNames{g})
                        
                        load ispikes.mat
                        
                        allymin = [];
                        allymax = [];
                        
                        for sac = 1 : 3
                            for al = 1 : 3
                                
                                [bins{sac,al},spikefr{sac,al},A{sac,al},ymin{sac,al},ymax{sac,al},reference{sac,al},ttspikes, tttrials, fr{sac,al}] = plotMTSpsth(mtst,sp,'iMatchSac',sac,'stable','BehResp',1,'alignement',align{al},'rule',Args.rule,varargin2{:});
                                
                            end
                        end
                        
                        save(sprintf('threeSacPSTH%d.mat',Args.rule),'bins','spikefr','A','ymin','ymax','reference','fr')
                         display([pwd '/' sprintf('threeSacPSTH%d.mat',Args.rule)])
                        
                    end
                    
                case 'FR4epochs'
                    
                    for g = 1 : is.data.numSets
                        try
                        cd(is.data.setNames{g})
                        catch
                           cd(regexprep(is.data.setNames{g},'media','Volumes')) 
                        end
                        load ispikes.mat
                        data = [];
                        LocI = [];
                        IdeI = [];
                        perI = [];
                        baseline = [];
                        stims = [];
                        allspikes = cell(9,1);
                        for cue = 1 : 9
                            if Args.ML || Args.session99 
                                ind = mtsgetTrials(mtst,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'rule',Args.rule,'stable','BehResp',1,varargin2{:});
                            
                            else
                                
                                Args.rule = mtst.data.Index(1,1);
                                ind = mtsgetTrials(mtst,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1,varargin2{:});
                            end
                            
                            strials = stableSpikes(sp,'CueObj',allobj(cueComb(2,cue)),'CueLoc',allloc(cueComb(1,cue)),'stable','BehResp',1);
                            trials = intersect(ind,strials);
                            tcount = 1;
                            for t = vecr(trials)
                                periods(1) = round((mtst.data.CueOnset(t) - Args.plength));
                                periods(2) = round(mtst.data.CueOffset(t) - Args.plength);
                                periods(3) = round(mtst.data.CueOffset(t));
                                periods(4) = round((mtst.data.MatchOnset(t) - Args.plength));
                                periods(5) = round(mtst.data.MatchOnset(t));
                                
                                
                                for p = 1 : length(periods)
                                    
                                    spiketimes = getSpikeInPer(sp,periods,t,p,Args.plength);
                                    
                                    allspikes{cue}(tcount,p) = length(spiketimes);
                                    if p  < 5
                                        data = [data length(spiketimes)];
                                        LocI = [LocI cueComb(1,cue)];
                                        IdeI = [IdeI cueComb(2,cue)];
                                        perI = [perI p];
                                    end
                                    if p == 1
                                        baseline = [baseline length(spiketimes)];
                                        stims = [stims cue];
                                    end
                                end
                                tcount = tcount + 1;
                            end
                        end
                        if ~isempty(data) | ~isempty(LocI) | ~isempty(perI) | ~isempty(IdeI)
                            [pvalues,table,stats] = anovan(data,{LocI IdeI perI},'model','interaction','display','off');
                            [basep,ta,st] = anovan(baseline,{stims},'display','off');
                        else
                            pvalues = [];
                            table = [];
                            stats = [];
                            basep = [];
                        end
                        plength = Args.plength;
                        try
                        cd(is.data.setNames{g})
                        catch
                           cd(regexprep(is.data.setNames{g},'media','Volumes')) 
                        end
                        save(sprintf('FR4epochs%d.mat',Args.rule),'allspikes','plength','pvalues','table','stats','basep')
                        display([pwd '/' sprintf('FR4epochs%d.mat',Args.rule)])
                        
                    end
                    
                case 'binStat'
                    for g = 1 : is.data.numSets
                        
                        cd(is.data.setNames{g})
                        load ispikes.mat
                        load(sprintf('nineStimPSTH%d.mat',Args.rule))
                        
                        
                    end
                    
            end
        end
        
    end
    
end
cd(sdir)