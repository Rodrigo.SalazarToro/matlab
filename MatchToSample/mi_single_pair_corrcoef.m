function mi_single_pair_corrcoef(varargin)

%run at session level
%computes mutual information for each pair at the three locations across the 3 identities
Args = struct('ml',0,'cor_range',[],'phase',[],'test',0,'sample',0,'fix',0,'all',0,'perm',0);
Args.flags = {'ml','phase','test','sample','fix','all','perm'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;


%find sorted pairs
% [~,pair_list] = sorted_groups;

if Args.ml
    c = mtscpp2('auto','ml');
else
    c = mtscpp2('auto');
end
[~,pair_list] = get(c,'Number','pf');
num_pairs = size(pair_list,2);

stimulus_lists = cell(1,3);
out_list = cell(1,3);
% get stimulus list and xcorr information
for locations = 1 : 3
    if Args.ml
        if Args.fix
            out_list{locations} = corr_phase_list('ml','fix','locations',locations);
        elseif Args.sample
            out_list{locations} = corr_phase_list('ml','sample','locations',locations);
        elseif Args.all
            out_list{locations} = corr_phase_list('ml','all','locations',locations);
        else
            out_list{locations} = corr_phase_list('ml','locations',locations);
        end
        stimulus_lists{locations} = stimulus_list('ml','locbyide',locations);
    else
        if Args.fix
            out_list{locations} = corr_phase_list('fix','locations',locations);
        elseif Args.sample
            out_list{locations} = corr_phase_list('sample','locations',locations);
        elseif Args.all
            out_list{locations} = corr_phase_list('all','locations',locations);
        else
            out_list{locations} = corr_phase_list('locations',locations);
        end
        stimulus_lists{locations} = stimulus_list('locbyide',locations);
    end
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);

inc = 1;
if Args.phase
    range = single([-50:inc:50]);
else
    if isempty(Args.cor_range)
        cor_range = [0:.05:1];
    else
        cor_range = Args.cor_range;
    end
    range = single(cor_range);
end


if Args.perm
    nperms = [1:1000];
else
    nperms = 1;
end

for np = nperms
    pair_counter = 0;
    pair_mutual_info = cell(1,num_pairs);
    for p = pair_list
        pair_counter = pair_counter + 1;
        mutual_info = zeros(3,33);
        locs = [1:3];
        for locations = locs
            %         trial_corrcoefs = out_list{locations}.trial_corrcoefs{p};
            trial_phases = out_list{locations}.trial_phases{p};
            [num_trials, num_points] = size(trial_phases);
            
            stimulus = stimulus_lists{locations};
            
            if Args.perm
                stimulus = stimulus(randperm(size(stimulus,2)));
            end
            
            cond_dist = cell(3,num_points);
            %calculate conditional distributions for all trials
            for t = 1 : num_trials
                for tp = 1 : num_points
                    
                    if Args.phase
                        point = single((trial_phases(t,tp)));
                    else
                        %                     point = single(abs(trial_corrcoefs(t,tp)));
                    end
                    
                    if isempty(cond_dist{stimulus(t),tp})
                        cond_dist{stimulus(t),tp} = point;
                    else
                        cond_dist{stimulus(t),tp} = [cond_dist{stimulus(t),tp} point];
                    end
                end
            end
            
            %normalize conditional distributions and make cumulative distribution
            cumulative_dist = cell(1,num_points);
            for stims = 1 : 3
                for tp = 1 : num_points
                    
                    %make histogram and divide by number of responsess to get probability
                    h = zeros(1,size(range,2));
                    counter = 0;
                    for cr = (range ./ inc)
                        counter = counter + 1;
                        cond_dist{stims,tp} = single(round(cond_dist{stims,tp}*10)/10);
                        h(counter) = size(find( round(cond_dist{stims,tp} ./ inc) == single(cr)),2);
                    end
                    cond_dist{stims,tp} = h./ sum(h);
                    
                    prob_sample = 1/3;
                    
                    if isempty(cumulative_dist{1,tp})
                        cumulative_dist{1,tp} = cond_dist{stims,tp} * prob_sample;
                    else
                        cumulative_dist{1,tp} = cumulative_dist{1,tp} + (cond_dist{stims,tp} * prob_sample);
                    end
                end
            end
            
            posterior_distributions = cell(3,num_points);
            for stims = 1 : 3
                for tp = 1 : num_points
                    %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                    posterior_distributions{stims,tp} = (cond_dist{stims,tp} * prob_sample) ./ cumulative_dist{1,tp};
                    posterior_distributions{stims,tp}(isnan(posterior_distributions{stims,tp})) = 0;
                end
            end
            
            mi = mutual_info_pairs('posterior_distributions',posterior_distributions,'prob_response',cumulative_dist);
            mutual_info(locations,:)= mi;
        end
        pair_mutual_info{p} = mutual_info;
    end
    if Args.perm
        all_perms{np} = pair_mutual_info;
    end
end

if Args.perm
    save mi_single_pair_permutations all_perms
else
    save mi_single_pair pair_mutual_info
end


cd(sesdir)




% lc{1} = [];
% lc{2} = [];
% lc{3} = [];
% 
% for ap = 1:730
%     for l = 1:3
%         lc{l} = [lc{l};all_perms{ap}{4}(l,:)];
%     end
% end
% 
% c= {'r' 'g' 'b'};for x = 1:3;plot(prctile(lc{x},99),c{x});hold on;end
% 
% all_perms;
% 
% 
% 
% 
% 
% 
% 
% 
% pair_mutual_info;
% 
% figure
% c = {'r' 'g' 'b'};for x = 1:3;plot(pair_mutual_info{4}(x,:),c{x});hold on;end
% 
% a = [];
% aa = [];
% aaa = [];
% aii = [];
% b = [];
% for np = 1 : size(pair_mutual_info,2)
%     pmi = pair_mutual_info{np};
%     if ~isempty(pmi)
%         
%         [~,ii] = max(mean(pmi(:,25:33)'));
%         
%         a = [a ; pmi(1,:)];
%         aa = [aa ; pmi(2,:)];
%         aaa = [aaa ; pmi(3,:)];
%         aii = [aii ; pmi(ii,:)];
%         
%         b = [b ; pmi];
%     end
% end
% 
% figure
% plot(median(aii))
% 
% 
% 
% 
