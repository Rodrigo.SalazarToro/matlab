function [varargout] = checktuning(varargin)
% to be run inthe session directory

Args = struct('redo',0,'save',0,'remoteName',[],'locTuning',0,'ideTuning',0,'allCuesTuning',0,'surrogate',0,'porder',11,'freq',[1 : 100],'rule',[],'DS',0);
Args.flags = {'redo','save','locTuning','ideTuning','allCuesTuning','surrogate','DS'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'redo','save','locTuning','ideTuning','allCuesTuning','surrogate'});

if Args.DS
    mtst = dstrial('auto');
else
    mtst = ProcessSession(mtstrial,'auto');
end
elecm = {'cohPP','cohPF','cohInter','powerPP','powerPF'};

rsFs = 200; % downsampling
plength = 400;
downsamp = (1000/rsFs);
lplength = plength/downsamp;

if ~isempty(Args.remoteName)
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
    params{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1,'err',[2 0.05]);
    paramsS{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1);
    paramsP{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0);
else
    params = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1,'err',[2 0.05]);
    paramsS = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',1);
    paramsP = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0);
end


if Args.DS
    ncues = 8;
    
    matfile = 'locTuning.mat';
else
    allloc = vecr(unique(mtst.data.CueLoc));
    allobj = unique(mtst.data.CueObj);
    if isempty(Args.rule)
        Args.rule = unique(mtst.data.Index(:,1));
        
    end
    if Args.locTuning
        ncues = 3;
        arg{1} = 'allloc(cues)';
        arg{2} = 'allobj';
        matfile = sprintf('locTuningRule%d.mat',Args.rule);
    elseif Args.ideTuning
        ncues = 3;
        arg{1} = 'allloc';
        arg{2} = 'allobj(cues)';
        matfile = sprintf('ideTuningRule%d.mat',Args.rule);
    elseif Args.allCuesTuning
        ncues = 9;
        cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
        arg{1} = 'allloc(cueComb(1,cues))';
        arg{2} = 'allobj(cueComb(2,cues))';
        matfile = sprintf('allCuesTuningRule%d.mat',Args.rule);
    else
        ncues = 1;
        arg{1} = 'allloc';
        arg{2} = 'allobj';
        matfile = sprintf('allCuesCombRule%d.mat',Args.rule);
    end
end
skip = nptDir('skip.txt');
[pdir,cdir] = getDataDirs('lfp','relative','CDNow');
thefile = nptDir(matfile);
skiplfp = nptDir('skip.txt');
cd(cdir)

if (isempty(thefile) || Args.redo) && isempty(skip) && isempty(skiplfp)
    for measure = 1 : length(elecm)
        fprintf('%s \n',elecm{measure});
        [channels,comb,CHcomb] = checkChannels(elecm{measure});
        for cues = 1 : ncues
            if Args.DS
                [r,Trials] = get(mtst,'ObjPos',cues,'Number');
            else
                Trials = mtsgetTrials(mtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),'rule',Args.rule,modvarargin{:});
            end
            ntrials{cues} = Trials;
            if ~isempty(Trials) && length(Trials) > 2 && comb ~= -1
                [data,lplength] = lfpPcut(Trials,channels,'plength',plength,modvarargin{:}); % output in {periods} samples x trials x channel data
                
            else
                data = [];
                %             notCue = [notCue cues];
            end
            
            if comb == 2 && ~isempty(data)
                Nr = size(data{1},2);
                Nl = size(data{1},1);
                gcdata = cell(4,1);for p = 1 : 4; gcdata{p} = [gcdata{p} reshape(data{p},Nl * Nr,size(data{p},3))']; end
                
                for cb = 1 : size(CHcomb,1)
                    for p = 1 : 4
                        ch1 = find(CHcomb(cb,1) == channels);
                        ch2 = find(CHcomb(cb,2) == channels);
                        if ~isempty(Args.remoteName)
                            data1{p+(cb-1)*4} = data{p}(:,:,ch1);
                            data2{p+(cb-1)*4} = data{p}(:,:,ch2);
                            gcdata1{p+(cb-1)*4} = gcdata{p}([ch1 ch2],:);
                        else
                            [coh(cues).C(cb,:,p),coh(cues).phi(cb,:,p),coh(cues).S12(cb,:,p),coh(cues).S1(cb,:,p),coh(cues).S2(cb,:,p),f,confC,phierr,Cerr] = coherencyc(data{p}(:,:,ch1),data{p}(:,:,ch2),params);
                            
                            [pp,cohe,coh(cues).Fx2y(:,cb,p),coh(cues).Fy2x(:,cb,p),coh(cues).Fxy(:,cb,p),rp]= pwcausalrp(gcdata{p}([ch1 ch2],:),size(gcdata{p},2)/Nl,Nl,Args.porder,rsFs,Args.freq);
                            if Args.surrogate
                                [Surmean,Surstd,SurPhiMean,SurPhiStd,SurTile99,coh(cues).Prob(cb,:,:,p),f] = cohsurrogate(data{p}(:,:,ch1),data{p}(:,:,ch2),paramsS,modvarargin{:});
                            end
                        end
                    end
                    
                end
                clear data gcdata
                if ~isempty(Args.remoteName)
                    fprintf('Data sent to Wintermute \n')
                    [C,phi,S12,S1,S2,ftemp,confCt,phierr,Cerr] = dfeval(@coherencyc,data1,data2,repmat(params,1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                    
                    if Args.surrogate
                        [Surmeant,Surstdt,SurPhiMeant,SurPhiStdt,SurTile99t,Prob,ftemp] = dfeval(@cohsurrogate,data1,data2,repmat(paramsS,1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                    end
                    clear data1 data2
                    [pp,cohe,Fx2y,Fy2x,Fxy,rp]= dfeval(@pwcausalrp,gcdata1,repmat({size(gcdata1{p},2)/Nl},1,length(gcdata1)),repmat({Nl},1,length(gcdata1)),repmat({Args.porder},1,length(gcdata1)),repmat({rsFs},1,length(gcdata1)),repmat({Args.freq},1,length(gcdata1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                    clear gcdata1
                    var = {'C','phi','S12','S1','S2','Fx2y','Fy2x','Fxy'};
                    for cb = 1 : size(CHcomb,1)
                        for p = 1 : 4
                            for v = 1 : length(var)
                                
                                eval(sprintf('coh(cues).%s(cb,:,p) = %s{p+(cb-1)*4};',var{v},var{v}));
                                
                            end
                            f = ftemp{1};
                            if Args.surrogate; coh(cues).Prob(cb,:,:,p) = Prob{p+(cb-1)*4};end
                            %                         coh(cues).confC(cb,p) = confC{p+(cb-1)*4};
                            %                         coh(cues).Cerr(cb,:,:,p) = Cerr{p+(cb-1)*4};
                        end
                    end
                    clear C phi S1 S2 S12 ftemp confC Phierr Cerr Prob Fx2y Fy2x Fxy
                end
                
            elseif comb == 1 && ~isempty(data)
                
                for ch = 1 : length(channels)
                    chr = channels(ch);
                    for p = 1 : 4
                        
                        if ~isempty(Args.remoteName)
                            datap{p+(ch-1)*4} = data{p}(:,:,ch);
                        else
                            [power(cues).S(ch,:,:,p),f] = mtspectrumc(data{p}(:,:,ch),paramsP);% needs to be worked on
                        end
                    end
                end
                clear data
                if ~isempty(Args.remoteName)
                    fprintf('Data sent to %s \n',Args.remoteName)
                    [S,ftemp2] = dfeval(@mtspectrumc,datap,repmat(paramsP,1,length(datap)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                    clear datap
                    for ch = 1 : length(channels)
                        chr = channels(ch);
                        for p = 1 : 4
                            power(cues).S(ch,:,:,p) = S{p+(ch-1)*4};
                            %                         power(cues).Serr(ch,:,:,p) = Serr{p+(ch-1)*4};
                            f = ftemp2{1};
                        end
                    end
                    
                end
                
                
            else
                power(cues).S = [];%power(cues).Serr = [];
                
                coh(cues).C = []; coh(cues).phi = []; coh(cues).S12 = []; coh(cues).S1 = []; coh(cues).S2 = []; %coh(cues).confC = []; coh(cues).phierr = []; coh(cues).Cerr = [];
            end
        end
        if exist('coh')
            eval(sprintf('%s.coh = coh;',elecm{measure}));
            clear coh
        elseif exist('power')
            eval(sprintf('%s.power = power;',elecm{measure}));
            
        end
        if ~exist('f'); f = []; end
    end
    
    
    if Args.save
        [pdir,cdir] = getDataDirs('lfp','relative','CDNow');
        options = varargin;
        save(matfile,'cohPP','cohPF','cohInter','powerPP','powerPF','f','options','ntrials');
        cd(cdir)
    end
    varargout{1} = cohPP;
    varargout{2} = cohPF;
    varargout{3} = cohInter;
    varargout{4} = f;
end









