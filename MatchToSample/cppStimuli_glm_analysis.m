function [glm_output, probabilities]=cppStimuli_glm_analysis(varargin)


%Used by mtsCPP to view the averages percents for all stimuli for ONE
%CHANNEL PAIR

Args = struct('c1',[],'c2',[],'threshh',[],'sessionname',[],'chnumb',[],'ml',0,'pair',[],'model',[],'stimulus',[]);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

lfp2dir = pwd;
cd ../..
sesdir = pwd;
cd(lfp2dir);

stimulus = Args.stimulus;

if Args.ml
    cd identity
    load threshold threshold
    cd(lfp2dir)
else
    load threshold threshold
end

cd(sesdir)
if Args.ml
    out = corr_phase_list('ml');
else
    out = corr_phase_list;
end
cd(lfp2dir);


p_threshold = single(threshold{Args.pair}(Args.threshh));
pair_corrcoefs = abs(single(out.trial_corrcoefs{Args.pair}));

cross_matrix = zeros(size(pair_corrcoefs,1),size(pair_corrcoefs,2));
cross_matrix(pair_corrcoefs > p_threshold) = 1;

cross = [];
probabilities = [];
for s = 1 : max(stimulus)
    trial_stim = find(stimulus == s);
    cross(s,:) = sum(cross_matrix(trial_stim,:)); %rows are stimuli
    stim_number(s) = size(trial_stim,2);
    
    probabilities(s,:) = cross(s,:)./stim_number(s);
end

%GLM

glm_output = glm_r('stim_number',stim_number,'allcross',cross,'model',Args.model);






