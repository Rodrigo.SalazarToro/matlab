function di = day_info

%run at day level
%gets snr, channel depths and number of sua clusters
%all reported channels have atleast a mua cluster
%di(1,:) = channel
%di(2,:) = SNR (session01)
%di(3,:) = depth (microns)
%di(4,:) = # of single units
%currently only works for betty

daydir = pwd;

%go to session dir to get depth info
cd('session01')

N = NeuronalHist;
di(3,:) = N.recordedDepth;

%go to highpass dir to get snr
cd('highpass')

%load snr
load SNR_channels.mat

di((1:2),:) = single(channel_snr_list)';

%go to session99 to get group info
cd([daydir filesep 'session99'])
ses99dir = pwd;
%get group information
g = nptDir('group*');
sortedGroups = [];
numg = size(g,1);
singles = zeros(1,numg);
for ng = 1 : numg
    sortedGroups = [sortedGroups str2double(g(ng).name(end-3:end))];
    
    cd([ses99dir filesep g(ng).name])
    
    %there is always a multi unit, so every cluster after the first is a single unit.
    clusters = nptDir('cluster*');
    nclusters = size(clusters,1);
    
    if nclusters > 1
        singles(ng) = nclusters - 1;
    end
end


[~,ii] = intersect(di(1,:),sortedGroups);

di = di(:,ii);
di(4,:) = singles;

cd(daydir)

