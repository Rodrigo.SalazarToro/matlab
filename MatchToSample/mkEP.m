function mkEP(varargin)
% to be run in the session folder

Args = struct('rule',1,'timeFrame','MatchSac','outofBound',100,'ML',0,'stdTHR',12);
Args.flags = {'ML'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});




mts = mtstrial('auto','redosetNames');

if Args.ML
    rule = Args.rule;
    
else
    rule = mts.data.Index(1,1);
end

trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',rule);
sdir = pwd;

switch Args.timeFrame
    case 'presample'
        timing = [mts.data.CueOnset-400 mts.data.CueOnset];
    case 'sample'
        timing = [mts.data.CueOnset+100 mts.data.CueOnset+500];
    case 'delay1'
        timing = [mts.data.CueOffset  mts.data.CueOffset+400];
    case 'delay2'
         timing = [mts.data.MatchOnset-400 mts.data.MatchOnset];
    case 'saccade'
        timing = [mts.data.MatchOnset-Args.outofBound mts.data.MatchOnset+100+Args.outofBound];
        
end

neuroInfo = NeuronalChAssign;

cd lfp

files = nptDir('*_lfp.*');


minlength = min(diff(timing(trials,:)'));

time = [-Args.outofBound : 5: -Args.outofBound + minlength];


for tr = 1 : length(trials)
    [datat,~,~,~,~] = nptReadStreamerFile(files(trials(tr)).name);
    for cc = 1 : size(datat,1)
        [datatt,SR] = preprocessinglfp(datat(cc,:),'No60Hz',1,'detrend',varargin2{:});
        
        if tr == 1
            data(tr,cc,:) = datatt(ceil(timing(trials(tr),1)*SR / 1000) : ceil((timing(trials(tr),1) + minlength))*SR/1000);
        else
            data(tr,cc,:) = datatt(ceil(timing(trials(tr),1)*SR / 1000) : ceil(timing(trials(tr),1)*SR / 1000) + size(data,3)-1);
        end
    end
end



thr = Args.stdTHR * mean(std(data,[],3));

rejectTrialsRelativetotrials = find(sum(sum(abs(data(:,:,1:43)) > repmat(thr,[size(data,1) 1 43]),3),2) >0);

strials = setdiff([1:size(data,1)],rejectTrialsRelativetotrials); 

EPm = squeeze(mean(data(strials,:,:),1));
EPstd = squeeze(std(data(strials,:,:),[],1));


for ch = 1 : length(neuroInfo.channels)
    gr = neuroInfo.groups(ch);
    matfile = sprintf('EP%sgr%04.0frule%d.mat',Args.timeFrame,gr,rule);
    
    EP = [EPm(ch,:); EPstd(ch,:)];
    save(matfile,'EP','trials','time','rejectTrialsRelativetotrials')
    display(['saving ' pwd '/' matfile])
end
cd(sdir)