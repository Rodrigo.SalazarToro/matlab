function make_stimulus_avg_ccg_anova(varargin)

%used to make average ccg plots for each stimulus
%run at session level

Args = struct('pair',[],'plot',0,'redo',0);
Args.flags = {'plot','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

end_cg = 43;
ses_dir = pwd;
cd([ses_dir filesep ('lfp')]);
mt = mtstrial('auto');
objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);
cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
load all_pairs
num_cp = size(all_pairs,1);



if isempty(Args.pair)
    all_cp = [1 : size(all_pairs,1)];
else
    all_cp =Args.pair;
end

if isempty(nptdir('all_kw_pvals.mat')) || Args.redo
    for acp = all_cp
        
        im = [];
        cd(ses_dir)
        max_val = 0;
        
        stim = 0;
        for objects = 1 : 3
            for locations = 1 : 3
                cumulative_xcorr = [];
                stim = stim + 1;
                cd(ses_dir)
                %check for bhv file
                if ~isempty(nptdir('*.bhv'))
                    ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations),'ML');
                    ml = 1;
                else
                    ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations));
                    ml = 0;
                end
                trials(stim) = size(ind,2);
                cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
                
                
                if ml
                    load(['channelpair' num2strpad(all_pairs(acp,1),2) num2strpad(all_pairs(acp,2),2)]);
                else
                    load(['channelpair' num2str(all_pairs(acp,1)) num2str(all_pairs(acp,2))]);
                end
                
                cumulative_xcorr = correlograms{ind(1)}((1:end_cg),:);
                ind = ind(2:end);
                c_xcorr = [];
                cxc = 0;
                for x = ind
                    cxc = cxc + 1;
                    cumulative_xcorr = cumulative_xcorr + correlograms{x}((1:end_cg),:);
                    c_xcorr(:,:,cxc) = correlograms{x}((1:end_cg),:);
                    
                end
                cx = cumulative_xcorr' ./ size(ind,2);
                im{stim} = cx;
                stim_xcorr{stim} = c_xcorr;
                
                if max_val < max(max(abs(cx)))
                    max_val = max(max(abs(cx)));
                end
            end
        end
        
        %perform all possible kw tests
        
        numb_tests = 101 * end_cg;
        kw_pvals = zeros(end_cg,101);
        for xx = 1 : end_cg
            for x = 1 : 101
                
                
                test_vals = [];
                for y = 1 : 9
                    if y == 1;
                        test_vals(1,:) =  stim_xcorr{y}(xx,x,:);
                        stim_types = ones(1,size(test_vals,2));
                    else
                        t_vals = [];
                        t_vals(1,:) =  stim_xcorr{y}(xx,x,:);
                        
                        test_vals = [test_vals t_vals];
                        
                        stype = ones(1,size(t_vals,2)) * y;
                        stim_types = [stim_types stype];
                    end
                end
                
                k =  kruskalwallis(test_vals,stim_types,'off');
                kw_pvals(xx,x) = k;
                
                if k < .05
                    binary_kw_pvals(xx,x) =  1;
                else
                    binary_kw_pvals(xx,x) = 0;
                end
                
            end
        end
        
        all_kw_pvals{acp} = kw_pvals;
    end
    
    save all_kw_pvals all_kw_pvals
else
    load all_kw_pvals
end

if Args.plot
    
    for acp = all_cp
        figure
        
        imagesc(binary_kw_pvals')
        set(gca,'YTick',[1,25,50,75,100])
        set(gca,'YTicklabel',[-50 -25 0 25 50]);
        
        set(gca,'XTick',[1:7:42])
        set(gca,'XTicklabel',[150:350:2250])
        hold on
        
        %zero phase
        %im{stim}(50,:) = max_val;
        plot([1 43],[50 50],'color','w')
        hold on
        %sample
        plot([8 8],[1 101],'color','w')
        hold on
        %delay
        plot([18 18],[1 101],'color','w')
        hold on
        %first match
        plot([34 34],[1 101],'color','w')
        hold on
        %last match
        plot([42 42],[1 101],'color','w')
        hold on
        
        title(['pair  ',num2str(all_pairs(acp,1)) '  ' num2str(all_pairs(acp,2))])
    end
end




cd(ses_dir)


