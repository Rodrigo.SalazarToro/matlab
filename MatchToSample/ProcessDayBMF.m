function ProcessDayBMF(varargin)

%Run at monkey dir
%Used to process BMF data
%
%Directions:
%1: run 'streamer', this makes streamer files from the neuralynx CSC files file extension(.ncs)
%2: run 'highpass','lowpass', this makes highpass and lowpass directories
%3: run 'snr', this calculates the snr from the highpass files
%4: run 'artifact', this runs the artifact detection process
%5: run 'manual_channel_reject', a file containing the channels that should
%not be included in must be saved before this can be run (that file is
%manually_rejected_channels.mat with is a row vector containing the channel
%number as they appear on the bmf grid).
%6: run 'lost_records_check', this will kick out any trials that have lost
%records. A file (lost_trials.mat) with a  list of lost record segments
%must be made from the neuralynx file CheetahLostADRecords.txt (saved in
%the 20* file in the session dir).

%all commands can be used at once
%example:
%ProcessDayBMF('sessions',[1,2,3],'streamer','highpass','lowpass','snr','artifact','manual_channel_reject','lost_records_check')

Args = struct('days',[],'sessions',[],'highpass',0,'lowpass',0,'power',0,'mts',0,'clust',0,'check',0,'highpass_high',9000,'lowpass_high',150,'artifact',0,'snr',0,...
    'streamer',0,'redo_streamer',0,'redo_highpass',0,'redo_lowpass',0,'redo_snr',0,'manual_channel_reject',0,'lost_records_check',0,'bin_minutes',1,'lowf_power',0,'rej_filter',0,'extend_streamer_trial',0);
Args.flags = {'highpass','lowpass','power','mts','clust','check','artifact','snr','streamer','redo_streamer','redo_highpass','redo_lowpass','redo_snr','lost_records_check','manual_channel_reject','lowf_power','rej_filter','extend_streamer_trial'};
Args = getOptArgs(varargin,Args);
mycomp = computer;

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    
    sessions = nptDir('session0*');
    daydir = pwd;
    
    %make descriptor files
    makeDescriptor
    
    fs = strfind(daydir,filesep);
    numfs = size(fs,2);
    daystr = daydir(fs(numfs)+1:end);
    monkeystr = daydir(fs(numfs-1)+1:fs(numfs)-1);
    
    for s = Args.sessions
        
        
        cd([daydir filesep sessions(s).name])
        sesdir = pwd;
        cd(daydir)
        %WRITE STREAMER FILES
        if Args.streamer
            if Args.redo_streamer
                if Args.extend_streamer_trial
                    NlynxProcessing('sessions',Args.sessions,'redo','extend_trial','nlynx2streamer_mat')
                else
                    NlynxProcessing('sessions',Args.sessions,'redo','nlynx2streamer_mat')
                end
            else
                NlynxProcessing('sessions',Args.sessions)
            end
        end
        
        cd(sesdir)
        
        
        %HIGHPASS AND LOWPASS
        if Args.highpass
            if Args.redo_highpass
                ProcessSession('highpass','highpasshigh',Args.highpass_high,'redo')
                cd(sesdir)
                cd highpass
                system(['rm ' monkeystr daystr '0' num2str(s) '_highpass*'])
                cd ..
                system(['mv ' monkeystr daystr '0' num2str(s) '_highpass* highpass/'])
            else
                ProcessSession('highpass','highpasshigh',Args.highpass_high,'redo')
                cd(sesdir)
                system('mkdir highpass')
                system(['mv ' monkeystr daystr '0' num2str(s) '_highpass* highpass/'])
            end
        end
        cd(sesdir)
        if Args.lowpass
            if Args.redo_lowpass
                ProcessSession('lowpass','lowpasshigh',Args.lowpass_high,'redo')
                cd lowpass
                system(['rm ' monkeystr daystr '0' num2str(s) '_lowpass*'])
                cd ..
                system(['mv ' monkeystr daystr '0' num2str(s) '_lfp* lfp/'])
            else
                ProcessSession('lowpass','lowpasshigh',Args.lowpass_high,'redo') %needs a redo because of processedsession.txt
                system('mkdir lfp')
                system(['mv ' monkeystr daystr '0' num2str(s) '_lfp* lfp/'])
            end
        end
        cd(sesdir)
        
        if Args.power
            write_power_trials('ml','nlynx')
        end
        
        %run snr
        if Args.snr
            if Args.redo_snr
                ProcessSession('SNR_highpass','SNR_redo','redo')
            else
                ProcessSession('SNR_highpass','redo')
            end
        end
        cd(sesdir)
        
        if Args.mts
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        end
        cd(sesdir)
        
        %% KICK OUT BAD CHANNELS AND TRIALS
        %bad trials will be indicated but no channels will be removed
        if Args.artifact
            %             ArtRemTrialsLFP('threshold',20,'distr','sigma','Kthresh',20,'badCH',.10,'save','redo','bmf')
            ArtRemTrialsLFP('threshold',20,'distr','sigma','Kthresh',20,'badCH',.05,'save','redo','bmf','detect_transients')
            save artifact_detection_complete.txt
        end
        cd(sesdir)
        
        %the file lost_records.mat must be made manually before this can be run
        if Args.lost_records_check
            nlyx_lost_records
        end
        cd(sesdir)
        
        if Args.lowf_power
            lowfreq_power %need to run write_power_trials first
        end
        
        %run this last
        if Args.rej_filter
            reject_filter
        end
        cd(sesdir)
        %%
        
        if Args.clust
            ProcessSession('extraction','threshold',5,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','no'},'redo'); %previous threshold was 4
            if strcmp(mycomp,'PCWIN')
                status1 = MoveProcessedFiles(sessions,length(sessions));
            end
        end
        cd(sesdir)
    end
    cd(daydir)
    
    %move any out of place files, including sorting files
    MoveProcessedFiles(sessions,Args.sessions)
    
    if Args.check
        compareBHVcodetimeVSNlynxparallel('sessions',1)
        compareBHVcodetimeVStriggers('nlynx','sessions',1,'redo');
        compareBHV2MTS
        
        %best to run the plot option on each day after computations have run
        compareEYE('nlynx','sessions',1)
    end
end

cd(monkeydir)
