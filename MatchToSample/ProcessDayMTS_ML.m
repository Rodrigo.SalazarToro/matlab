function ProcessDayMTS_ML(varargin)

% Run at day level
% Used to process raw data.
% high_low_pass: high/low pass data and place in highpass and lfp folder
% clust: run clustering
% check: check for consistency
% highpass_high: specify highpass filter cut off

Args = struct('high_low_pass',0,'clust',0,'check',0,'highpass_high',7000,'artifact',0,'snr',0,'freeview',0,'nlynx',0,'nlynx_streamer',0);
Args.flags = {'high_low_pass','clust','check','artifact','snr','freeview','nlynx','nlynx_streamer'};
Args = getOptArgs(varargin,Args);
mycomp = computer;
%run in day directory
diary([pwd filesep 'diary.txt']);
sessions = nptDir('session0*');
day_dir = pwd;

%write streamer files for nlyx data (late betty days only)
if Args.nlynx_streamer
    NlynxProcessing_betty('sessions',1,'redo','nlynx2streamer_mat') %use matlab file parser
%     NlynxProcessing_betty('sessions',1,'redo')
end


if ~Args.freeview
    for s = 1 : length(sessions)
        cd(sessions(s).name)
        %check if session has already been cleaned
        cleaned = nptDir('cleanedSession.txt');
        if isempty(cleaned)
            if Args.nlynx
                status(s) = removeUEImissTrials('eyeML','nlynx');
            else
                status(s) = removeUEImissTrials('eyeML');
            end
        else
            status(s) = 1;
        end
        cd ..
    end
else
    status = 1;
end

if ~isempty(find(status == 1))
    
    if Args.high_low_pass
        if Args.nlynx
            ProcessDay('lowpass','highpass','highpasshigh',Args.highpass_high)
        else
            ProcessDay('lowpass','highpass','highpasshigh',Args.highpass_high,'UEI')
        end
        %ProcessDay('lowpass','highpass','UEI')
        delete processedday.txt
        for s = 1 : length(sessions)
            cd(sessions(s).name)
            delete processedsession.txt
            cd ..
        end
        
        if strcmp(mycomp,'PCWIN')
            status1 = MoveProcessedFiles(sessions,length(sessions));
        else
            mname = which('moveprocessedfiles.sh');
            [status1,w] = system(mname);
        end
    end
    
    if Args.clust
        ProcessDay('extraction','threshold',4,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','no'});
        if strcmp(mycomp,'PCWIN')
            status1 = MoveProcessedFiles(sessions,length(sessions));
        end
    end
    % display w so we can see what happened
    % fprintf('%s\n',w);
    
    % sessions = nptDir('session*'); sesSize =
    % length(sessions);MoveProcessedFiles(sessions,sesSize)
    %ProcessDay(nptdata,'nptSessionCmd','eyemvt(''auto'',''SacThresh'',60,''save'',''NoSites'');'); % mtstrial(''auto'',''save'');');
    mts =ProcessDay(mtstrial,'NoSites','auto','save','ML','RTfromML');
    
    if Args.check
        InspectGUI(mts,'ObjPos','percentCR','Hist')
        compareEYE
        for s = 1 : length(sessions)
            cd(sessions(s).name)
            if ~Args.nlynx
                [crossings,times,dif] = compareBHVcodetimeVStriggers('save','plot');
            else
                comapre_BHVcodetime_2_NLYNXevents
            end
            
            
            cd ..
            
            
        end
        compareBHV2MTS
    end
    
    %run artifact detection
    if Args.artifact
        if Args.nlynx
            for x = 1 : length(sessions)
                cd([day_dir filesep sessions(x).name]);
                ArtRemTrialsLFP('threshold',20,'save','distr','sigma','Kthresh',20,'redo')
                ArtRemTrialsLFP_bettyiti('threshold',20,'save','distr','sigma','Kthresh',20,'redo')
            end
        else
            for x = 1 : length(sessions)
                cd([day_dir filesep sessions(x).name]);
                ArtRemTrialsLFP('save','distr','sigma','Kthresh',9)
            end
        end
    end
    
    
    
    %run snr
    if Args.snr
        cd(day_dir)
        SNR_Day_Batch
    end
    
    
else
    fprintf('Warning: mismatch of trials and files')
end

diary off;

% mtsday = ProcessDay(mtstrial,'auto');
% InspectGUI(mtsday,'ObjPos','percentCR')