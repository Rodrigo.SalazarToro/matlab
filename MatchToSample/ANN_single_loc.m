function ANN_single_loc

cd('/media/raid/data/monkey/clark/060428/session02/lfp/lfp2')

load stimulus
load glm_fits_train



loc1_ind=sort([find(stimulus==1) find(stimulus==4) find(stimulus==7)],2);

loc1 = zeros(2,size(loc1_ind,2));
c = 0;
for i = loc1_ind
    c = c + 1;
    stim = stimulus(i);
    if stim == 1 %ide == 1
        loc1(1,c) = 1;
    elseif stim == 4 %ide == 2
        loc1(2,c) = 1;
    else
        loc1(3,c) = 1;
    end
end

%TARGETS
targets = loc1;
save targets targets

%INPUTS
pair = trial_fits_binary{1}';
inputs = pair(:,loc1_ind);
save inputs inputs

in(1,:) = sum(inputs((1:3),:));
in(2,:) = sum(inputs((4:6),:));
in(3,:) = sum(inputs((7:9),:));

inputs = in

% Create a Pattern Recognition Network
hiddenLayerSize =100;
net = patternnet(hiddenLayerSize);



% Set up Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 10/100;
net.divideParam.testRatio = 20/100;


% % % % Train the Network
net = train(net,inputs,targets);



