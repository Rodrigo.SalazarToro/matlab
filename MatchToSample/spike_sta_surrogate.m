function [surrogate_spikebins all_xcor] = spike_sta_surrogate(varargin)

%runs at session level
%called from run_STA
%calculates the spike bins
Args = struct('groups',[],'trials',[],'mt_obj',[],'binary',0,'crosscor',0,'perms',1000);
Args.flags = {'binary','crosscor'};
[Args,modvarargin] = getOptArgs(varargin,Args);

mt = Args.mt_obj;
num_trials = size(Args.trials,2);

sesdir = pwd;
%get trial timing information
cue_on = floor(mt.data.CueOnset);
cue_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%find groups with spikes
groups = nptDir('group*');

cd([sesdir filesep groups(Args.groups(1)).name])
gdir1 = pwd;
%find clusters
clusters1 = nptDir('cluster*');
nclusters1 = size(clusters1,1);

cd([sesdir filesep groups(Args.groups(2)).name])
gdir2 = pwd;
%find clusters
clusters2 = nptDir('cluster*');
nclusters2 = size(clusters2,1);

ccounter = 0;
for c = 1 : nclusters1
    cd([gdir1 filesep clusters1(c).name]);
    %get spikes
    load ispikes.mat
    %make spike rasters
    all_xcor = [];
    sp1 = sp;
    for cc = 1: nclusters2
        total_spikes = 0;
        ccounter = ccounter + 1;
        cd([gdir2 filesep clusters2(cc).name])
        load ispikes.mat
        sp2 = sp;
        %get a list of the clusters being compared
        cluster_list(ccounter,:) = [c cc];
        
        for perm = 1 : Args.perms
            perm1 = randperm(num_trials);
            perm2 = randperm(num_trials);
            bin1 = [];
            bin2 = [];
            bin3 = [];
            bin4 = [];
            all_bin = [];
            for ttrial = 1 : num_trials
                
                t1 = Args.trials(perm1(ttrial));
                t2 = Args.trials(perm2(ttrial));
                
                c1_on = cue_on(t1);
                c1_off = cue_off(t1);
                m1 = match(t1);
                sp1_trial = ceil(sp1.data.trial(t1).cluster.spikes); %ceil to avoid spikes a t = 0
                spikecount1 = sp1.data.trial(t1).cluster.spikecount;
                
                c2_on = cue_on(t2);
                c2_off = cue_off(t2);
                m2 = match(t2);
                sp2_trial = ceil(sp2.data.trial(t2).cluster.spikes); %ceil to avoid spikes a t = 0
                spikecount2 = sp2.data.trial(t2).cluster.spikecount;
                if Args.binary
                    %MAKE RASTERS 0 or 1
                    raster1 = zeros(1,5000);
                    raster1(unique(sp1_trial)) = 1;
                    
                    raster2 = zeros(1,5000);
                    raster2(unique(sp2_trial)) = 1;
                else
                    %rasterize (necessary because multi unit cluster can have multiple spikes at one time point
                    raster1 = hist(sp1_trial,[1:1:5000]);
                    raster2 = hist(sp2_trial,[1:1:5000]);
                end
                
                if Args.crosscor
                    txcor = xcorr(raster1(m - 400:m),raster2(m - 400:m),100,'coef');
                    all_xcor = [all_xcor;txcor];
                else
                    
                    %run through all the spikes
                    for spikes = 1 : spikecount1
                        spike = sp1_trial(spikes);
                        if spike > 101
                            spike_times = [(sp1_trial(spikes)-100) : (sp1_trial(spikes)+100)];
                            raster2_times = raster2(spike_times);
                            if spike > (c1_on - 400) && spike < c1_on && spike > (c2_on - 400) && spike < c2_on
                                bin1 = [bin1; raster2_times];
                                all_bin = [all_bin; raster2_times];
                                total_spikes = total_spikes + 1;
                            elseif spike > (c1_off - 400) && spike < c1_off && spike > (c2_off - 400) && spike < c2_off
                                bin2 = [bin2; raster2_times];
                                all_bin = [all_bin; raster2_times];
                                total_spikes = total_spikes + 1;
                            elseif spike > c1_off && spike < (c1_off + 400) && spike > c2_off && spike < (c2_off + 400)
                                bin3 = [bin3; raster2_times];
                                all_bin = [all_bin; raster2_times];
                                total_spikes = total_spikes + 1;
                            elseif spike > (m1 - 400) && spike < m1 && spike > (m2 - 400) && spike < m2 %make sure epochs are aligned for both channels
                                bin4 = [bin4; raster2_times];
                                all_bin = [all_bin; raster2_times];
                                total_spikes = total_spikes + 1;
                            end
                        end
                    end
                end
            end
            
            if size(bin1,1) > 1; bin1 = sum(bin1); end
            if size(bin2,1) > 1; bin2 = sum(bin2); end
            if size(bin3,1) > 1; bin3 = sum(bin3); end
            if size(bin4,1) > 1; bin4 = sum(bin4); end
            if size(all_bin,1) > 1; all_bin = sum(all_bin); end
            
            all_bins1(perm,:) = bin1;
            all_bins2(perm,:) = bin2;
            all_bins3(perm,:) = bin3;
            all_bins4(perm,:) = bin4;
            all_allbin(perm,:) = all_bin;
        end
        
            surrogate_spikebins.epoch1{ccounter} = all_bins1;
            surrogate_spikebins.epoch2{ccounter} = all_bins2;
            surrogate_spikebins.epoch3{ccounter} = all_bins3;
            surrogate_spikebins.epoch4{ccounter} = all_bins4;
            surrogate_spikebins.all{ccounter} = all_allbin;
            surrogate_spikebins.clusters = cluster_list
        
    end
end

cd(sesdir)