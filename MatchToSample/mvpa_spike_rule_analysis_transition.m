function mvpa_spike_rule_analysis_transition(varargin)


%run at session level

% computes mvpa analysis on prefrontal, parietal, and both spiking activity

% 1: presample    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

Args = struct('ml',0,'bin_spikes',0,'run_mvpa',0);
Args.flags = {'ml','bin_spikes','run_mvpa'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sesdir = pwd;

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    r1 = mtsgetTrials(mt,'ML','rule',1);
    r2 = mtsgetTrials(mt,'ML','rule',2);
    correct = mtsgetTrials(mt,'BehResp',1,'ML');
    N = NeuronalHist('ml');
else
    
end

%determine first transition
alltrials = sort([r1 r2]);
ntrials = size(alltrials,2);
rulelist = zeros(1,ntrials);
[~,r1list] = intersect(alltrials,r1);
rulelist(r1list) = 1;

[~,ii] = find(abs((diff(rulelist))) == 1);

trans = ii(1); %first transistion(trial, just prior to switch)

alltrials = alltrials(trans - 249 : trans + 270);
rulelist = rulelist(trans - 249 : trans + 270);

%get behvior for these trials
behtest = zeros(1,size(alltrials,2));
[~,blist] = intersect(alltrials,correct);
behtest(blist) = 1;


%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%get group information
NeuroInfo = NeuronalChAssign(); %get the "group" number that corresponds to each channel

%find groups with spikes
groups = nptDir('group*');
ngroups = [1 : size(groups,1)];

if Args.bin_spikes
    cluster_counter = 0;
    for g = ngroups %spike channel
        fprintf('\n%0.5g     ',g)
        
        %determine which channels the group number corresponds to
        g2ch = str2double(groups(g).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,g2ch);
        
        cd([sesdir filesep groups(g).name])
        %find clusters
        clusters = nptDir('cluster*');
        nclusters = size(clusters,1);
        
        for c = 1 : nclusters
            cluster_counter = cluster_counter + 1;
            cd([sesdir filesep groups(g).name])
            cd(clusters(c).name);
            
            %get spikes
            load ispikes.mat
            tcounter = 0;
            for ttrial = alltrials
                tcounter = tcounter + 1;
                
                spike_data = sp.data.trial(ttrial).cluster.spikes;
                s = ceil(spike_data); %ceil because a zero spike occured when using "round"

                %determine what epoch each spike is in
                s_on = sample_on(ttrial);
                s_off = sample_off(ttrial);
                m = match(ttrial);
                
                % 1: presample    [(sample_on - 399) : (sample_on)]
                % 2: sample      [(sample_off - 399) : (sample_off)]
                % 3: early delay [(sample_off) : (sample_off + 399)]
                % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
                % 5: delay       [(sample_off + 201) : (sample_off + 800)]
                % 6: delay match [(match - 399) : (match)]
                
                bins{1}(cluster_counter,tcounter) = sum(s >= (s_on - 399) & s <= s_on);
                bins{2}(cluster_counter,tcounter) = sum(s >= (s_off - 399) & s <= s_off);
                bins{3}(cluster_counter,tcounter) = sum(s >= s_off & s < (s_off + 399));
                bins{4}(cluster_counter,tcounter) = sum(s > (s_off+400) & s <= (s_off + 800));
                bins{5}(cluster_counter,tcounter) = sum(s > (s_off+200) & s <= (s_off + 800));
                bins{6}(cluster_counter,tcounter) = sum(s >= (m - 399) & s <= m);
            end
            
            %get anatomical information for each cluster
            [~,ii] = intersect(N.gridPos,str2double(groups(g).name(end-3:end)));
            NN.recordedDepth(cluster_counter) = N.recordedDepth(ii);
            NN.gridPos(cluster_counter) = N.gridPos(ii);
            NN.cortex(cluster_counter) = N.cortex(ii);
            NN.location{cluster_counter} = N.location{ii};
            NN.level1{cluster_counter} = N.level1{ii};
            NN.level2{cluster_counter} = N.level2{ii};
            NN.level3{cluster_counter} = N.level3{ii};
            NN.number(cluster_counter) = N.number(ii);
            
            
        end
    end
    cd(sesdir)
    save spike_bins_mvpa_transition bins NN
end



%%
if Args.run_mvpa
    
    testrange = 231:290;
    
    load spike_bins_mvpa_transition
    
    %determine the PFC and PPC channels
%     pfc_areas = strfind(NN.cortex,'F');
%     ppc_areas = strfind(NN.cortex,'P');
    
    pfc_areas = find(NN.number == 4);
    ppc_areas = [find(NN.number == 11) find(NN.number == 13)];
    
    %only use correct trials for training
    trainbeh = behtest;
    trainbeh(testrange) = [];
    corrtrain = find(single(trainbeh) == 1);
    
    %train with first 200 trials
    rlist = rulelist;
    testlist = rlist(testrange);
    rlist(testrange) = []; %use these trials for testing
    
    b1train = bins{1};
    b1train(:,(testrange)) = [];
    
    b2train = bins{2};
    b2train(:,(testrange)) = [];
    
    b3train = bins{3};
    b3train(:,(testrange)) = [];
    
    b4train = bins{4};
    b4train(:,(testrange)) = [];
    
    b5train = bins{5};
    b5train(:,(testrange)) = [];
    
    b6train = bins{6};
    b6train(:,(testrange)) = [];
    
    for a = 1:2
        if a == 1;
            alist = pfc_areas;
        else
            alist = ppc_areas;
        end
        
        if ~isempty(alist)
            SVMstructe1{a} = svmtrain(b1train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
            SVMstructe2{a} = svmtrain(b2train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
            SVMstructe3{a} = svmtrain(b3train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
            SVMstructe4{a} = svmtrain(b4train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
            SVMstructe5{a} = svmtrain(b5train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
            SVMstructe6{a} = svmtrain(b6train(alist,corrtrain),rlist(corrtrain),'Kernel_Function','mlp');
        end
    end
    
    test_trials = [testrange];
    for nt = 1:60


        testt = test_trials(nt);
        
        b1 = bins{1};
        testb1 = b1(:,testt);
        
        b2 = bins{2};
        testb2 = b2(:,testt);
        
        b3 = bins{3};
        testb3 = b3(:,testt);
        
        b4 = bins{4};
        testb4 = b4(:,testt);
        
        b5 = bins{5};
        testb5 = b5(:,testt);
        
        b6 = bins{6};
        testb6 = b6(:,testt);
       
        for a = 1:2
            
            if a == 1;
                alist = pfc_areas;
            else
                alist = ppc_areas;
            end
            if ~isempty(alist)
                newClassese1(a,nt) = svmclassify(SVMstructe1{a},testb1(alist)');
                newClassese2(a,nt) = svmclassify(SVMstructe2{a},testb2(alist)');
                newClassese3(a,nt) = svmclassify(SVMstructe3{a},testb3(alist)');
                newClassese4(a,nt) = svmclassify(SVMstructe4{a},testb4(alist)');
                newClassese5(a,nt) = svmclassify(SVMstructe5{a},testb5(alist)');
                newClassese6(a,nt) = svmclassify(SVMstructe6{a},testb6(alist)');
            else
                newClassese1(a,nt) = nan;
                newClassese2(a,nt) = nan;
                newClassese3(a,nt) = nan;
                newClassese4(a,nt) = nan;
                newClassese5(a,nt) = nan;
                newClassese6(a,nt) = nan;
                
            end
        end
    end
    
    
    ntest = size(testlist,2);
    perf_test_trials = behtest(testrange);
    
    %calculate moving average
    s1 = 0;
    s2 = 9;
    for mv = 1:51
        s1 = s1 + 1;
        s2 = s2 + 1;
        
        tmv = s1:s2;
        
        ntest = size((s1:s2),2);
        
        perf_pfc(1,mv) = (sum(newClassese1(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_pfc(2,mv) = (sum(newClassese2(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_pfc(3,mv) = (sum(newClassese3(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_pfc(4,mv) = (sum(newClassese4(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_pfc(5,mv) = (sum(newClassese5(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_pfc(6,mv) = (sum(newClassese6(1,tmv) == testlist(tmv)) ./ ntest) * 100;
        
        
        perf_ppc(1,mv) = (sum(newClassese1(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_ppc(2,mv) = (sum(newClassese2(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_ppc(3,mv) = (sum(newClassese3(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_ppc(4,mv) = (sum(newClassese4(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_ppc(5,mv) = (sum(newClassese5(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        perf_ppc(6,mv) = (sum(newClassese6(2,tmv) == testlist(tmv)) ./ ntest) * 100;
        
        behperf(1,mv) = (sum(perf_test_trials(s1:s2)) ./ ntest) * 100;
        
    end
    if rulelist(1) == 0
        firstrule = 'location';
    else
        firstrule = 'identity';
    end
    
    cd(sesdir)
    save mvpa_perf_transition perf_ppc perf_pfc behperf pfc_areas ppc_areas firstrule
end
