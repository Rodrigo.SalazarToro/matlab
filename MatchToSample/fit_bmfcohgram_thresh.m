function fit_bmfcohgram_thresh(varargin)


%run in cohgrams dir

Args = struct('global',0,'pairwise',0);
Args.flags = {'global','pairwise'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


if Args.global
    load globalsurcohgram.mat
    
    allsur = [];
    for xx = 1 : 1000;
        allsur(:,:,xx) = globalsurcohgram.C{xx,6}; %time/freq/perm
    end
    fitthresh = [];
    for as = 1 : 41
        fitsur = squeeze(allsur(as,:,:))';
        [datafit,xx,thresh,rsquare] = fitSurface(fitsur,globalsurcohgram.f{1,1},'Prob',[.999 .9999 .99999]);
        for th = 1 : size(thresh,2)
            fitthresh{th}(:,as) = thresh(:,th);
        end
    end
    save globalsurcohgram_surrogate fitthresh
end

if Args.pairwise
    sur = nptDir('surcohgram*');
    nump = size(sur,1);
    for np = 1 : nump
        nump-np
        load(sur(np).name)
        for p = 6 %1 : 6 %currently not run for the 5 stimuli
            allsur = [];
            for xx = 1 : 1000;
                allsur(:,:,xx) = surcohgram.C{xx,p}; %time/freq/perm
            end
            fitthresh = [];
            for as = 1 : 41
                fitsur = squeeze(allsur(as,:,:))';
                [datafit,xx,thresh,rsquare] = fitSurface(fitsur,surcohgram.f{1,6},'Prob',[.999 .9999 .99999]);
                for th = 1 : size(thresh,2)
                    fitthresh{th}(:,as) = thresh(:,th);
                end
            end
            all_thresh{p} = fitthresh;
        end
        sur_pairs = [ 'thresh' sur(np).name];
        save(sur_pairs,'all_thresh','write_info');
    end
end



