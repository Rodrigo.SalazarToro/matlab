function run_bandpass_filtering(varargin)


%run at session level
%saves band pass filtered data

Args = struct('ml',0,'redo',0,'bmf',0);
Args.flags = {'ml','redo','bmf'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
cd('lfp')
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp.*');
ntrials = size(lfpdata,1);
cd(sesdir)

%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    end
else
    mt = mtstrial('auto','redosetNames');
end

sample_off = floor(mt.data.CueOffset); %lock to sample off
matchtime = floor(mt.data.MatchOnset) + floor(mt.data.FirstSac); %lock to saccade

%run through all the frequencies for each pair
all_freq = [5 : 3 : 80];
nfreq = size(all_freq,2);

for t = 1 : ntrials
    if ~isnan(matchtime(t))
        cd([sesdir filesep 'lfp' filesep 'lfp2'])
        %make trial name
        for nf = 1 : nfreq
            trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
            
            cd(sesdir)
            if Args.ml
                if Args.bmf
                    [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
                else
                    %betty = UEI
                    orig_data = ReadUEIFile('FileName',trial);
                end
            else
                %clark = Streamer
                [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
            end
            
            cd([sesdir filesep 'lfp' filesep 'lfp2'])
            bandpassdata = [ lfpdata(t).name(1:(end-9)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata(t).name((end-4):end) '.mat'];
            if ~exist(bandpassdata) || Args.redo
                %lowpass
                hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,all_freq(nf)-2,all_freq(nf)+2); %6 total frequencies, freq+/-2
                
                %get data ranges
                datarange = [sample_off(t) - 999 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
                data = hdata(:,datarange);

                save(bandpassdata,'data')
            end
        end
    else
        cd([sesdir filesep 'lfp' filesep 'lfp2'])
        for nf = 1 : nfreq
            bandpassdata = [ lfpdata(t).name(1:(end-9)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata(t).name((end-4):end) '.mat'];
            save(bandpassdata) %save and empty files
        end
    end
end
cd(sesdir)
