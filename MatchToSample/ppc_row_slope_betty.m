function ppc_row_slope_betty

cd('/media/raid/data/monkey/betty')
monkeydir = pwd;
load alldays
all_i = [];
figure
for d = 1 : 48
   cd([monkeydir filesep alldays{d} filesep 'session01']) 
    
    c = mtscpp2('auto','ml')
    
    [i ii]=get(c,'Number','ips',1,'pprow');
    all_i = [all_i i]
 
    if ~isempty(ii)
        hold on
        scatter(c.data.Index(ii,16),c.data.Index(ii,56))
        if size(ii,2) > 1
            hold on
            lsline
        end
    end
    
    
end
all_i(2,:) = 1:48