function train_predict_stimulus_permutation(varargin)

%run at session level
Args = struct('fits',0,'stim_prediction',0,'stims',0,'p_cutoff',1,'logits',0,'rule',1,'xcor',0,'euclid',0,'snr',1.8,'threshold',...
    1,'pair_list',[],'obj1',[],'obj2',[],'hist_info',[],'block',1,'plotlle',0,'sorted',0,'fix',0,'no_offset',0,'no_interaction',0);

Args.flags = {'fits','stim_prediction','stims','logits','xcor','euclid','plotlle','sorted','fix','no_offset','no_interaction'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%run through each permutation
num_perms = 3;

%rule = 1(identity), rule = 0(location)
if Args.rule
    rule = 'identity';
else
    rule = 'location';
end

if isempty(Args.hist_info)
    N = NeuronalHist;
else
    N = Args.hist_info;
end

if isempty(Args.obj1)
    c = mtscpp('auto','ml','train','ide_only');
else
    c = Args.obj1;
end

if isempty(Args.obj2)
    mt = mtstrial('auto','ML','RTfromML');
else
    mt = Args.obj2;
end


cd(['lfp' filesep 'lfp2']);
lfp2dir = pwd;
cd(rule);
ruledir = pwd;

chnumb = N.chnumb;
total_pairs = nchoosek(chnumb,2);

combs = [];
cc = 0;
for c1 = 1 : chnumb
    for c2 = (c1+1) : chnumb
        cc = cc + 1;
        combs(cc,1) = N.gridPos(c1);
        combs(cc,2) = N.gridPos(c2);
    end
end

if Args.fix
    load glm_fix_perm
    glm_perm = glm_fix_perm;
else
    load glm_delay_perm
    glm_perm = glm_delay_perm;
end

if Args.sorted
    [i total_pairs] = get(c,'Number','sorted'); %get list of sorted pairs
else
    [i total_pairs] = get(c,'Number');
end

cd(sesdir)
if isempty(Args.block)
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    numb_trials = size(total_trials,2);
else
    %determine number of blocks
    bdata = find(diff(mt.data.Index(:,1)))';
    nblocks = size(bdata,2) + 1;
    %determine block order
    bdata(:) = bdata(:) + 1;
    b_order = mt.data.Index([1,bdata],1);
    
    a_block = find(b_order == Args.rule);
    block_num = a_block(Args.block);
    
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'block',block_num);
    numb_trials = size(total_trials,2);
end


if Args.fits
    for perms = 1 : num_perms
        for pair_num = total_pairs
            
            cd(ruledir)
            if perms == 1
                
                cd(lfp2dir)
                chpair = (['channelpair' num2strpad(combs(pair_num,1),2) num2strpad(combs(pair_num,2),2)]);
                cd(rule)
                load threshold threshold
                
                cd(lfp2dir)
                t = threshold{pair_num}(Args.threshold);
                load(chpair)
                binary_corrcoefs = [];
                for r = 1 : size(corrcoefs,1)
                    for ccc = 1: size(corrcoefs,2)
                        if abs(corrcoefs(r,ccc)) >= t
                            binary_corrcoefs(r,ccc) = 1;
                        else
                            binary_corrcoefs(r,ccc) = 0;
                        end
                    end
                end
                
                cd ../..
                if Args.fix
                    time(:,1) = (1:8)';
                else
                    time(:,1) = (1:13)';
                end
                
                counter = 0;
                
                for q = total_trials
                    counter = counter + 1;
                    x = binary_corrcoefs(:,q);
                    if Args.fix
                        response(:,1) = x((1:8));
                        response(:,2) = ones(1,8);
                    else
                        response(:,1) = x((22:34));
                        response(:,2) = ones(1,13);
                    end
                    B = glmfit(time,response,'binomial','link','logit');
                    if Args.no_offset
                        input = time * (B(2));
                    else
                        input = (B(1) + time * (B(2))); %works for no interaction as well
                    end
                    %convert to probability
                    if ~Args.logits
                        input = 1 ./ (1 + exp(-input));
                    end
                    trialFits(counter,:) = input;
                end
                trial_fits{pair_num} = trialFits;
            end
            
            cell_coefs = glm_perm{pair_num}{perms}.interaction_coefs;
            for all_coefs = 1:size(cell_coefs,1)
                coefs(all_coefs,1) = str2double(cell_coefs(all_coefs));
            end
            
            if Args.no_offset
                res(:,1) = time*coefs(2);
                res(:,2) = time*coefs(2) + coefs(3) + time* coefs(11);
                res(:,3) = time*coefs(2) + coefs(4) + time *coefs(12);
                res(:,4) = time*coefs(2) + coefs(5) + time *coefs(13);
                res(:,5) = time*coefs(2) + coefs(6) + time * coefs(14);
                res(:,6) = time*coefs(2) + coefs(7) + time * coefs(15);
                res(:,7) = time*coefs(2) + coefs(8) + time * coefs(16);
                res(:,8) = time*coefs(2) + coefs(9) + time *coefs(17);
                res(:,9) = time*coefs(2) + coefs(10) + time* coefs(18);
            elseif Args.no_interaction
                res(:,1) = coefs(1) + time*coefs(2);
                res(:,2) = coefs(1) + time*coefs(2);
                res(:,3) = coefs(1) + time*coefs(2);
                res(:,4) = coefs(1) + time*coefs(2);
                res(:,5) = coefs(1) + time*coefs(2);
                res(:,6) = coefs(1) + time*coefs(2);
                res(:,7) = coefs(1) + time*coefs(2);
                res(:,8) = coefs(1) + time*coefs(2);
                res(:,9) = coefs(1) + time*coefs(2);
            else
                res(:,1) = coefs(1) + time*coefs(2);
                res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
                res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
                res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
                res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
                res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
                res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
                res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
                res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
            end
            
            if ~Args.logits
                res = 1 ./ (1 + exp(-res));
            end
            pair_fits{pair_num}{perms} = res';
        end
    end
    
    if Args.fix
        cd(lfp2dir)
        pair_fits_perm = pair_fits;
        save pair_fits_perm_fix pair_fits_perm trial_fits
    else
        cd(lfp2dir)
        pair_fits_perm = pair_fits;
        save pair_fits_perm pair_fits_perm trial_fits
    end
else
    if Args.fix
        cd(lfp2dir)
        load pair_fits_perm_fix
        pair_fits = pair_fits_perm;
    else
        cd(lfp2dir)
        load pair_fits_perm
        pair_fits = pair_fits_perm;
    end
end

for perms = 1 : num_perms
    if Args.stims
        cd(sesdir)
        %get stimulus info
        locs = unique(mt.data.CueLoc);
        objs = unique(mt.data.CueObj);
        stim = 0;
        for object = 1:3
            %Run for all locations
            for location = 1:3
                stim = stim+1;
                if isempty(Args.block)
                    ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1);
                else
                    %determine number of blocks
                    bdata = find(diff(mt.data.Index(:,1)))';
                    nblocks = size(bdata,2) + 1;
                    %determine block order
                    bdata(:) = bdata(:) + 1;
                    b_order = mt.data.Index([1,bdata],1);
                    
                    a_block = find(b_order == Args.rule);
                    block_num = a_block(Args.block);
                    ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object),'rule',1,'block',block_num);
                end
                numstims(stim) = size(ind,2);
            end
        end
        stimulus = [];
        cd(lfp2dir)
        load rand_ind_alltrials
        rtrials = rand_ind_alltrials(perms,:);
        for st = 1:9
            stimulus(rtrials(1:numstims(st))) = st;
            rtrials(1:numstims(st)) = [];
        end
        
        [~, ~, iii] = find(stimulus);
        stimulus = iii;
        
        
        
        
    end
    
    
    numb_trials = size(stimulus,2);
    
    if Args.stim_prediction
        
        if Args.fix
            tp = [1 : 8];
        else
            tp = [1 : 13];
        end
        if Args.sorted
            %get pairs that have both groups sorted
            [i spairs] = get(c,'Number','identity','sorted');
        else
            [i spairs] = get(c,'Number','identity');
        end
        
        all_xc = zeros(9,13,9);
        all_xc_match = zeros(9,13,9);
        
        for t = 1:numb_trials
            if Args.euclid  %currently uses xcorr
                for timepoints = tp
                    
                    pp = 0;
                    for p = spairs
                        pp = pp + 1;
                        pair_timepoints(pp) = trial_fits{p}(t,timepoints); %get each trial time point for all pairs
                        pfits_timepoints(:,pp) = pair_fits{p}{perms}(:,timepoints); %get 9 stims for each time point for all pairs
                    end
                    all_t_timepoints(timepoints,:) = pair_timepoints;
                    
                    for xx = 1 : 9
                        all_pfits_timepoints(timepoints,:,xx) = pfits_timepoints(xx,:);
                    end
                    for allstims = 1:9
                        stim_euclid(allstims,timepoints) = sqrt(sum(((pair_timepoints - pfits_timepoints(allstims,:)).^2))); %get euclidean distance of each trial from all pairs for 9 stims
                        xc(allstims,timepoints) = xcorr(pair_timepoints,pfits_timepoints(allstims,:),0,'coef');
                    end
                    
                end
                s = sum(xc,2);
                [i ii] = max(s);
                stim_prediction(t) = ii;
                stim_corr(t) = i;
                
                if stimulus(t) == ii
                    all_xc_match(:,:,ii) = [all_xc_match(:,:,ii) + xc];
                end
                all_xc(:,:,stimulus(t)) = [all_xc(:,:,stimulus(t)) + xc];
                
                
                %                    ii
                %                    stimulus(t)
                %                     contourf(xc)
                %             pause
%             close all
            
                
                %
                %                             figure
                %             for x = 1:9;subplot(3,3,x);surface(all_pfits_timepoints(:,:,x));end
                %
                %             figure;surface(all_t_timepoints)
                %             pause
                
                
                % %                 figure
                % %                 for x = 1:9
                % %                     if x == ii
                % %                         plot(xc(x,:),'r')
                % %                     elseif x == stimulus(t)
                % %                         plot(xc(x,:),'g')
                % %                     else
                % %                         plot(xc(x,:))
                % %                     end
                % %                     hold on
                % %                 end
                % %                 stimulus(t) == ii
                % %                 pause
                % %                 close all
                % %                 clc
            end
        end
        
        
        %predict_trials = (numb_trials - round(numb_trials*.7)) + 1;
        cd(lfp2dir)
        
        htrials = hold_trials(perms,:); %this is an index of the permuted trials
        
        r_trials = rand_ind_alltrials(perms,:); %index this list with the hold_trials to get the actual trials that are withheld
        
        real_withheld_trial_numbers = r_trials(htrials);
        
        [i h_trials] = intersect(total_trials,real_withheld_trial_numbers);
        
        m = zeros(1,numb_trials);
        matches = 0;
        for x = 1 : numb_trials
            if stimulus(x) == stim_prediction(x)
                matches = matches + 1;
                m(x) = 1;
            end
        end

        correct_withheld = size(find(m(h_trials)),2);
        
        numb_withheld = size(h_trials,2);
        
        predict_perm(perms) = (correct_withheld / numb_withheld) * 100
        
        %determine how many correct trials where from the withheld trials
        non_withheld = matches - correct_withheld;
        numb = numb_trials - numb_withheld;
        total_perm(perms) = (non_withheld / numb) * 100
        
    end
    
end

write_info = writeinfo(dbstack);
save glm_score_permutation total_perm predict_perm write_info







