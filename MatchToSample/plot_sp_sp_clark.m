function plot_sp_sp_clark

cd /media/raid/data/monkey/clark/
mdir = pwd;
load idedays
load ideses
figure
for d = 21 : 46
    cd([mdir filesep idedays{d} filesep ses{d}])
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    
    cd([mdir filesep idedays{d} filesep ses{d} filesep 'lfp' filesep 'lfp2' filesep 'identity'])
    
    
    sp = nptDir('spikexcorr*');
    nsp = size(sp,1);
    
    
    counter = 0;
    for n = 1 : nsp
        counter = counter + 1;
        load(sp(n).name)
        idedays{d}
        
        a1 = N.level1(find(Nch.groups == spikexcorrs.groups(1,1)));
        a2 = N.level1(find(Nch.groups == spikexcorrs.groups(1,2)));
        
        sp_sp = spikexcorrs.delay(1,:);
        if counter > 20
            figure
            counter = 1;
        end
        
        subplot(4,5,counter)
        plot(smooth(sp_sp,10))
        title([a1{:} ' ' a2{:}])
    end
    
    pause
    close all
    
end