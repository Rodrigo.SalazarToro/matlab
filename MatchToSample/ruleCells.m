cd /Volumes/drobo1/data/monkey/
clear all
LOCobj = loadObject('psthLOCswitchObj.mat');
IDEobj = loadObject('psthIDEswitchObj.mat');

[~,LOCind] = get(LOCobj,'number');

[~,IDEind] = get(IDEobj,'number');
ttind = find(ismember(LOCobj.data.setNames(LOCind),IDEobj.data.setNames(IDEind),'rows') == 1);
tttind = find(ismember(IDEobj.data.setNames(IDEind),LOCobj.data.setNames(LOCind),'rows') == 1);
LOCind = LOCind(ttind);
IDEind = IDEind(tttind);


% epochs = {'presample' 'sample' 'delay1' 'delay2' 'saccade'};
epochs = {'allTrialSample' 'allTrialMatch'};
% allpvalues = ones(length(LOCind),5,20);
allpvalues = cell(2,1);
allhist = nan(length(LOCind),1);
sigMI = cell(2,1);
MI = cell(2,1);
difffr = cell(2,1);
mfr1 = nan(length(LOCind),1);
mfr2 = nan(length(LOCind),1);
for ii = 1 : length(LOCind)
    %     if strcmp(LOCobj.data.setNames{LOCind(ii)}(end),'s')
    cd(LOCobj.data.setNames{LOCind(ii)})
    allhist(ii) = LOCobj.data.Index(LOCind(ii),3);
    load('PSTHrulesMI.mat')
    for ep = 1 : length(epochs)
        statfile = nptDir(sprintf('compPSTHRules%s*.mat',epochs{ep}));
        if ~isempty(statfile)
            load(statfile.name)
            
            allpvalues{ep}(ii,:) = pval;
        end
        psth = load('nineStimPSTH1.mat','A');
        allspt=cat(1,psth.A{:,ep});
        allsp = zeros(size(allspt,1),60);
        
        for b = 1 : 60;allsp(:,b) = sum(allspt(:,(b-1)*50+1:(b-1)*50+50),2); end
        fr1 = (1000/50)*sum(allsp,1)/size(allsp,1);
        
        psth = load('nineStimPSTH2.mat','A');
        allspt=cat(1,psth.A{:,ep});
        allsp = zeros(size(allspt,1),60);
        for b = 1 : 60;allsp(:,b) = sum(allspt(:,(b-1)*50+1:(b-1)*50+50),2); end
        fr2 = (1000/50)*sum(allsp,1)/size(allsp,1);
        mfr2(ii) = LOCobj.data.Index(LOCind(ii),6);
        mfr1(ii) = IDEobj.data.Index(IDEind(ii),6);
        difffr{ep}(ii,:) = fr1-fr2;
        sigMI{ep}(ii,:) = mi{ep}(end,:) >prctile(mi{ep}(1:end-1,:),99.95,1);
        
        MI{ep}(ii,:) = mi{ep}(end,:) - getExpValue(mi{ep}(1:end-1,:),[1:size(mi{ep},2)]);
    end
end
%% firing rate and MI distribution
figure
areasN = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

subplot(3,1,1); boxplot(mfr1',allhist,'labels',areasN(unique(allhist)))
ylim([0 75])
title('IDE')
ylabel('firing rate (sp/s)')
set(gca,'TickDir','out')
subplot(3,1,2); boxplot(mfr2',allhist,'labels',areasN(unique(allhist)))
ylim([0 75])
title('LOC')
xlabel('cortical areas')
ylabel('firing rate (sp/s)')
set(gca,'TickDir','out')
subplot(3,1,3); boxplot(log10(max(MI{1}(:,1:36),[],2)'),allhist,'labels',areasN(unique(allhist)))
ylabel('log(MI)')
set(gca,'TickDir','out')
% ylim([-0.01 0.2])

%%
figure
trange = {[1:36] [21:36]};
timeleg = {'Time from sample onset (ms)'  'Time from match onset (ms)' 'Time from match onset (ms)'};
mvAv = 4;
for ep = 1 : 2
    subplot(2,2,ep)
    plot(time{ep},filtfilt(repmat(1/mvAv,1,mvAv),1,100*sum(sigMI{ep},1)/size(sigMI{ep},1)))
    legend(['n=' num2str(size(sigMI{ep},1))])
    subplot(2,2,ep+2)
    sigs = find(sum(sigMI{ep}(:,trange{ep}),2) >0);
    signdiff = sigMI{ep} .* sign(difffr{ep});
    pos = signdiff > 0;
    neg = signdiff < 0;
    plot(time{ep},filtfilt(repmat(1/mvAv,1,mvAv),1,100*sum(pos(sigs,:),1)/length(sigs)))
    hold on
    plot(time{ep},filtfilt(repmat(1/mvAv,1,mvAv),1,-100*sum(neg(sigs,:),1)/length(sigs)))
    legend(['n=' num2str(length(sigs))])
end
for sb = 1 : 2; subplot(2,2,sb); xlim([time{sb}(trange{sb}(1)) time{sb}(trange{sb}(end))]); ylim([-20 16]); set(gca,'TickDir','out'); end
for sb = 3 : 4; subplot(2,2,sb); xlim([time{sb-2}(trange{sb-2}(1)) time{sb-2}(trange{sb-2}(end))]); xlabel(timeleg{sb-2});ylim([-25 25]);set(gca,'TickDir','out');end
ylabel('% preference; > 0 IDE > LOC')
subplot(2,2,2); ylabel('incidence of sign MI');

%% cortical area specificity
timeleg = {'Time from sample onset (ms)'  'Time from match onset (ms)' 'Time from match onset (ms)'};
lb = {'r' 'b' 'k' 'y' 'g' 'c' 'm' 'r--'};
allareas = unique(allhist(~isnan(allhist)));
% allareas = [1 2 3 4 5 8 10 11 12 13 17];
f(1) = figure;
f(2) = figure;
f(3) = figure;

c=1;
pvalue = 0.001;
mvAv = 2;
trange = {[1:36] [21:37]};

for a = 1 : length(allareas)
    sa = find(allhist == allareas(a));
    if length(sa) >= 15
        for ep = 1 : length(epochs)
            statfile = nptDir(sprintf('compPSTHRules%s*.mat',epochs{ep}));
            load(statfile.name)
            figure(f(1))
            subplot(1,2,ep)
            xdata = 100*squeeze(sum(allpvalues{ep}(sa,:) < (pvalue/(2*length(Mbins))),1)/length(sa));
            xdata = filtfilt(repmat(1/mvAv,1,mvAv),1,xdata);
            plot(Mbins(2:end),xdata,lb{c})
            xlim([Mbins(2) Mbins(end)])
            hold on
            %             if ep==1;title([areasN{allareas(a)} '; n=' num2str(length(sa))]); end
            ylim([0 40])
            
            xlabel(timeleg{ep});
            set(f(1),'Name','test-ranksum');
            ylabel('Incidence of rule specific firing rate')
            set(gca,'TickDir','out');
            figure(f(2))
            subplot(1,2,ep)
            xdata = 100*sum(sigMI{ep}(sa,:)/length(sa));
            
            [sigU1,~] = find(sigMI{1}(sa,trange{1}) == 1);
            [sigU2,~] = find(sigMI{2}(sa,trange{2}) == 1);
            sigU = unique([sigU1; sigU2]);
            xdata = filtfilt(repmat(1/mvAv,1,mvAv),1,xdata);
            plot(time{ep},xdata,lb{c})
            
            hold on
            xlim([Mbins(2) Mbins(end)])
            ylim([0 40])
            xlabel(timeleg{ep});
            set(f(2),'Name','MI significance');
            ylabel('Incidence of rule specific firing rate')
            set(gca,'TickDir','out');
            figure(f(3))
            subplot(1,2,ep)
            %             xdata = 100*mean(MI{ep}(sa(sigU),:)/length(sa),1);
            xdata = median(MI{ep}(sa,:));
            xdata = filtfilt(repmat(1/mvAv,1,mvAv),1,xdata);
            plot(time{ep},xdata,lb{c})
            hold on
            xlim([Mbins(2) Mbins(end)])
            ylim([0 0.04])
            xlabel(timeleg{ep});
            set(f(3),'Name','MI magnitude');
            ylabel('Mean bits')
            set(gca,'TickDir','out');
        end
        sareas{c} = [areasN{allareas(a)} '; n=' num2str(length(sa))];
        c=c+1;
        
    end
    
end
for ff = 1 : 3
    figure(f(ff))
    
    legend(sareas)
end
areasN = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
clear sa
sa{1} = find(allhist == 11);
sa{2} = find(allhist == 4);
sa{3} = find(allhist == 13);
sa{4} = find(allhist == 8);
sa{5} = find(allhist == 10);
sa{6} = find(allhist == 3);
sa{7} = find(allhist == 2);
h = ones(6,1);
pcp = ones(6,1);
for cp = 1 : 6
    [h(cp),pcp(cp)] = kstest2(max(MI{1}(sa{cp},1:36),[],2)',max(MI{1}(sa{cp+1},1:36),[],2)',0.01,'smaller');
end
pcp
sigCells = union(find(sum(allpvalues{1} < (pvalue/36),2) > 1),find(sum(allpvalues{2}< (pvalue/17),2) > 0));% based on WRS
[tot,~] = hist(allhist,[0:18]);

sigCode = zeros(314,1);
sigCode(sigCells) = 1; % randome shuffled of cortical areas assignments
for ii = 1 : 1000; nsig = sigCode(randperm(314)); [nit(ii,:),~] = hist(allhist(find(nsig==1)),[0:18]);end
nanmean(100*prctile(nit(:,[3 4 5 9 11 12 14]),99.5,1)./tot([3 4 5 9 11 12 14]))
nanmean(100*prctile(nit(:,[3 4 5 9 11 12 14]),.5,1)./tot([3 4 5 9 11 12 14]))
figure;
subplot(4,1,1)
[n,~] = hist(allhist(sigCells),[0:18]);
[tot,~] = hist(allhist,[0:18]);
tot(tot<15) = 0;
bar(100*n./tot)

set(gca,'XTick',[2:18])
set(gca,'XTickLabel',areasN)
ylabel('% cells')
title('Incidence of significant cells with rule WR specific firing rate')
hold on
line([0 18],[30 30],'LineStyle','--')
xlim([0 15])
ylim([0 70])
set(gca,'TickDir','out');
subplot(4,1,2)
pref = mean(difffr{1}(:,trange{1}),2) + mean(difffr{2}(:,trange{2}),2);
idepref = find(pref > 0);
locpref = find(pref < 0);
[n(1,:),~] = hist(allhist(intersect(sigCells,idepref)),[0:18]);
[n(2,:),~] = hist(allhist(intersect(sigCells,locpref)),[0:18]);
[tot,~] = hist(allhist,[0:18]);
tot(tot<15) = 0;

ksdt = (100*n./repmat(tot,2,1));
clear ksd
ksd(1,:) = ksdt(1,isfinite(ksdt(1,:)));
ksd(2,:) = ksdt(2,isfinite(ksdt(2,:)));
[h,p] = kstest2(ksd(1,:),ksd(2,:));p

bar((100*n./repmat(tot,2,1))')
set(gca,'XTick',[2:18])
set(gca,'XTickLabel',areasN)
ylabel('% cells')
title('Incidence of significant cells with rule WR specific firing rate')
hold on
line([0 18],[30 30],'LineStyle','--')
xlim([0 15])
ylim([0 70])
set(gca,'TickDir','out');

sigCells = union(find(sum(sigMI{1}(:,trange{1}),2) > 1),find(sum(sigMI{2}(:,trange{2}),2) > 0));
sigCode = zeros(314,1);
sigCode(sigCells) = 1; % randome shuffled of cortical areas assignments
for ii = 1 : 1000; nsig = sigCode(randperm(314)); [nit(ii,:),~] = hist(allhist(find(nsig==1)),[0:18]);end
nanmean(100*prctile(nit(:,[3 4 5 9 11 12 14]),99.5,1)./tot([3 4 5 9 11 12 14])) % upper limit
nanmean(100*prctile(nit(:,[3 4 5 9 11 12 14]),.5,1)./tot([3 4 5 9 11 12 14])) % lower limit
subplot(4,1,3)

[n(1,:),~] = hist(allhist(intersect(sigCells,idepref)),[0:18]);
[n(2,:),~] = hist(allhist(intersect(sigCells,locpref)),[0:18]);
[tot,~] = hist(allhist,[0:18]);
tot(tot<15) = 0;
bar(100*sum(n,1)./tot)
% ksd = 100*sum(n,1)./tot;
%
% [h,p,ksstat,cv] = kstest(ksd(isfinite(ksd)));p
set(gca,'XTick',[2:18])
set(gca,'XTickLabel',areasN)
ylabel('% cells')
title('Incidence of significant cells with rule MI specific firing rate')
hold on
line([0 18],[30 30],'LineStyle','--')
xlim([0 15])
ylim([0 70])
set(gca,'TickDir','out');

subplot(4,1,4)
bar((100*n./repmat(tot,2,1))')
set(gca,'XTick',[2:18])
set(gca,'XTickLabel',areasN)
ylabel('% cells')
title('Incidence of significant cells with rule MI specific firing rate')
hold on
line([0 18],[30 30],'LineStyle','--')
xlim([0 15])
ylim([0 70])
legend('IDE higher FR','LOC higher FR')
ksdt = (100*n./repmat(tot,2,1));
clear ksd
ksd(1,:) = ksdt(1,isfinite(ksdt(1,:)));
ksd(2,:) = ksdt(2,isfinite(ksdt(2,:)));
[h,p] = kstest2(ksd(1,:),ksd(2,:));p
set(gca,'TickDir','out');

%% rule preference for each cortical area

f101 = figure;

% difffr ide -loc
thediff = nan(314,1); 
for ii = 1 : 314
    seq = [difffr{1}(ii,trange{1}) difffr{2}(ii,trange{2})];
    [value,ic] = max(abs(seq));
    thediff(ii) = seq(ic);
    
    
end
boxplot(thediff,allhist)
hold on
set(gca,'XTick',[1:11])
set(gca,'XTickLabel',areasN([1 2 3 4 5 8 10 11 12 13 17]))


count = 1;
pareaFR =  ones(11,1);
for area = [1 2 3 4 5 8 10 11 12 13 17]
   pareaFR(count) = signtest(thediff(allhist == area),0,'method','approximate');
   text(count,40,num2str(pareaFR(count)))
   count = count + 1;
end
 set(gca,'TickDir','out');

%% sample selectivity
cd /Volumes/raid/data/monkey/
clear all
LOCobj = loadObject('psthLOCswitchObj.mat');
IDEobj = loadObject('psthIDEswitchObj.mat');

gr1 = [4 11 13];
gr2 = [2 3 8 10];
[~,LOCind] = get(LOCobj,'number','hist',gr2);

[~,IDEind] = get(IDEobj,'number','hist',gr2);
ttind = find(ismember(LOCobj.data.setNames(LOCind),IDEobj.data.setNames(IDEind),'rows') == 1);
tttind = find(ismember(IDEobj.data.setNames(IDEind),LOCobj.data.setNames(LOCind),'rows') == 1);
LOCind = LOCind(ttind);
IDEind = IDEind(tttind);
miss = 0;
MIide = nan(2,length(LOCind), 30);
MIloc = nan(2,length(LOCind), 30);
MIideSig = nan(2,length(LOCind), 30);
MIlocSig = nan(2,length(LOCind), 30);
match = 'MatchAlign';
% match = '';
time = [-1750:100:1150];
time = [-450:100:2450];
parfor ii = 1 : length(LOCind)
    cd(LOCobj.data.setNames{LOCind(ii)})
    for r = 1 : 2
        
        ide = load(sprintf('PSTHIde%srule%d.mat',match,r));
        loc = load(sprintf('PSTHLoc%srule%d.mat',match,r));
        
        MIide(r,ii,:) = ide.mi(end,:) - getExpValue(ide.mi(1:end-1,:),[1:size(ide.mi,2)]);
        MIloc(r,ii,:) = loc.mi(end,:) - getExpValue(loc.mi(1:end-1,:),[1:size(loc.mi,2)]);
        MIideSig(r,ii,:) = ide.mi(end,:) > prctile(ide.mi(1:end-1,:),99.9);
        MIlocSig(r,ii,:) = loc.mi(end,:)  > prctile(loc.mi(1:end-1,:),99.9);
        
    end
end
figure
rules = {'IDE' 'LOC'};
la = {'b' 'r'};
for r = 1 : 2;
    subplot(2,2,r);
    boxplot(squeeze(MIide(r,:,:)))
    %   title('ide MI')
    ylim([-0.05 0.20])
    subplot(2,2,2+r);
    boxplot(squeeze(MIloc(r,:,:)))
    ylim([-0.05 0.45])
end



figure
subplot(2,1,1);
boxplot(squeeze(MIide(1,:,:)) - squeeze(MIide(2,:,:)))
for tbin = 1 : 30; p1(tbin) = signtest(squeeze(MIide(1,:,tbin)) - squeeze(MIide(2,:,tbin))); end
%   title('ide MI')
title('ide MI')
ylim([-0.3 0.30])
ylabel('IDE - LOC')
subplot(2,1,2);
boxplot(squeeze(MIloc(1,:,:)) - squeeze(MIloc(2,:,:)))
for tbin = 1 : 30; p2(tbin) = signtest(squeeze(MIloc(1,:,tbin)) - squeeze(MIloc(2,:,tbin))); end
ylim([-0.3 0.30])
title('loc MI')
ylabel('IDE - LOC')

figure
subplot(2,1,1);
plot(time,prctile(squeeze(MIide(1,:,:)),[25 50 75]),'b')
hold on
plot(time,prctile(squeeze(MIide(2,:,:)),[25 50 75]),'r')

%   title('ide MI')
title('ide MI')

subplot(2,1,2);
plot(time,prctile(squeeze(MIloc(1,:,:)),[25 50 75]),'b')
hold on
plot(time,prctile(squeeze(MIloc(2,:,:)),[25 50 75]),'r')

title('loc MI')
legend('IDE','LOC')
xlabel('Time from Match (ms)')
ylabel('MI')
figure
rules = {'IDE' 'LOC'};
la = {'b' 'r'};
for r = 1 : 2;
    subplot(2,1,1);
    plot(time,nansum(squeeze(MIideSig(r,:,:))),la{r})
    hold on
    title('ide MI')
    ylim([0 8])
    subplot(2,1,2);
    plot(time,nansum(squeeze(MIlocSig(r,:,:))),la{r})
    hold on
    title('loc MI')
    ylim([0 40])
end
legend('IDE','LOC')

save('MatchAlignIncidence.mat','time','MIideSig','MIlocSig')
save('Incidence.mat','time','MIideSig','MIlocSig')

idep = ranksum(nansum(squeeze(MIideSig(1,:,6:10))),nansum(squeeze(MIideSig(2,:,6:10))))
locp = ranksum(nansum(squeeze(MIlocSig(1,:,6:10))),nansum(squeeze(MIlocSig(2,:,6:10))))

idep = ranksum(nansum(squeeze(MIideSig(1,:,11:18))),nansum(squeeze(MIideSig(2,:,11:18))))
locp = ranksum(nansum(squeeze(MIlocSig(1,:,11:18))),nansum(squeeze(MIlocSig(2,:,11:18))))

idep = ranksum(nansum(squeeze(matchAl.MIideSig(1,:,11:18))),nansum(squeeze(matchAl.MIideSig(2,:,11:18))))
locp = ranksum(nansum(squeeze(matchAl.MIlocSig(1,:,11:18))),nansum(squeeze(matchAl.MIlocSig(2,:,11:18))))

Gr1matchAl = load('MatchAlignIncidence.mat');
Gr1 = load('Incidence.mat');
for r = 1: 2
%     ideSAp(r) = ranksum(nansum(squeeze(MIideSig(r,:,6:10))),nansum(squeeze(Gr1.MIideSig(r,:,6:10))))
%     locpSA(r) = ranksum(nansum(squeeze(MIlocSig(r,:,6:10))),nansum(squeeze(Gr1.MIlocSig(r,:,6:10))))
%     
%     idepDE1(r) = ranksum(nansum(squeeze(MIideSig(r,:,11:15))),nansum(squeeze(Gr1.MIideSig(r,:,11:15))))
%     locpDE1(r) = ranksum(nansum(squeeze(MIlocSig(r,:,11:15))),nansum(squeeze(Gr1.MIlocSig(r,:,11:15))))
    
    idepDE2(r) = ranksum(nansum(squeeze(MIideSig(r,:,14:18))),nansum(squeeze(Gr1matchAl.MIideSig(r,:,14:18))))
    locpDE2(r) = ranksum(nansum(squeeze(MIlocSig(r,:,14:18))),nansum(squeeze(Gr1matchAl.MIlocSig(r,:,14:18))))
end

% figName = {'test ranksum' 'MI' 'magniture MI'};
% for fi = 4 : 6
%     h=figure(fi);
%     set(h,'Name',figName{fi})
%     subplot(1,2,1);legend(sareas)
%     for sb = 1 : 2; subplot(1,2,sb); xlabel(timeleg{sb});
%         if fi == 1
%             ylim([0 18]);
%         elseif fi == 2
%             ylim([3 26]);
%         elseif fi == 3
%             ylim([0.005 0.1]);
%         end
%     end
%     if fi ~=3; ylabel('% cells'); else ylabel('MI (bits)');end
%     subplot(1,2,1); line([0 0],[0 25]); xlim([-400 1200])
%     subplot(1,2,1); line([500 500],[0 25])
%     subplot(1,2,2); line([0 0],[0 25]);xlim([-700 100])
% end


break
% %% diff xcorr spike between rules
% allpvalues = cell(2,1);
% allhist = nan(length(LOCind),1);
% epochs = {'presample' 'sample' 'delay1' 'delay2'};
% bad = zeros(4,1);
% badii =cell(4,1);
%
% for ii = 1 : length(LOCind)
%     %     if strcmp(LOCobj.data.setNames{LOCind(ii)}(end),'s')
%     cd(LOCobj.data.setNames{LOCind(ii)})
%     cd ../..
%     cd xcorr
%     cl = LOCobj.data.setNames{LOCind(ii)}(end-2 : end);
%     gr = LOCobj.data.setNames{LOCind(ii)}(end-14 : end-11);
%     thefileIDE = sprintf('xcorrg%s%sg%s%sRule1.mat',gr,cl,gr,cl);
%     surISIIDE = sprintf('xcorrg%s%sg%s%sRule1SurspikeTime.mat',gr,cl,gr,cl);
%     thefileLOC = sprintf('xcorrg%s%sg%s%sRule2.mat',gr,cl,gr,cl);
%     surISILOC = sprintf('xcorrg%s%sg%s%sRule2SurspikeTime.mat',gr,cl,gr,cl);
%
%     allhist(ii) = LOCobj.data.Index(LOCind(ii),3);
%     ide = load(thefileIDE);
%     idesur = load(surISIIDE);
%     loc = load(thefileLOC);
%     locsur = load(surISILOC);
%
%     for ep = 1 : length(epochs)
%         pval = ones(90,1);
%
%         if iscell(ide.S1) && iscell(loc.S1) && size(ide.S1{ep},2) > 10 && size(loc.S1{ep},2) > 10 && size(ide.S1{ep},1) > 10 && size(loc.S1{ep},1) > 10 && ~isempty(idesur.S1{ep}) && ~isempty(locsur.S1{ep})
%             data1 = ide.S1{ep} - repmat(nanmean(ide.S1{ep},1),101,1);
%             data2 = loc.S1{ep} - repmat(nanmean(loc.S1{ep},1),101,1);
%             %             data1 = ide.S1{ep} - repmat(getExpValue(idesur.S1{ep},ide.f)',1,size(ide.S1{ep},2));
%             %             data2 = loc.S1{ep} - repmat(getExpValue(locsur.S1{ep},loc.f)',1,size(loc.S1{ep},2));
%             for ff = 1 : 90
%                 pval(ff) = ranksum(data1(ff,:),data2(ff,:));
%             end
%             allpvalues{ep}(ii,:) = pval;
%         else
%             if  sum(badii{ep} == ii) == 0
%                 bad(ep) = bad(ep) +1;
%                 badii{ep} = [badii{ep} ii];
%             end
%         end
%     end
%     %     end
% end
%
%
% figure
% allbad = unique(cat(2,badii{:}));
% lb = {'b' 'g' 'k' 'r'};
% for ep = 1 : length(epochs);
%     ind = setdiff((1:550),allbad);
%     n = length(ind);
%     plot(100*(nansum(allpvalues{ep}(ind,:) < (0.05/(n*90)),1))./(n),lb{ep});
%     hold on
%     xlim([0 90]); ylim([0 20])
%
% end
% xlabel('Frequency (Hz)')
% ylabel('%sign. between teh 2 rules')
% legend(epochs)
%
% allareas = unique(allhist(~isnan(allhist)));
% areasN = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
% figure
% c=1;
% for a = 1 : length(allareas)
%     sa = find(allhist == allareas(a));
%     sa = intersect(sa,ind);
%     if length(sa) > 20
%         for ep = 1 : length(epochs);
%             subplot(7,1,c);plot(100*(nansum(allpvalues{ep}(sa,:) < (0.05/(n*90)),1))./(length(sa)),lb{ep});
%             xlim([0 90]); ylim([0 40])
%
%             hold on
%         end
%         title([areasN{allareas(a)} '; n=' num2str(length(sa))])
%         c = c +1;
%     end
% end
% xlabel('Frequency (Hz)')
% ylabel('%sign. between teh 2 rules')
% legend(epochs)
%
% %% with MI rule
