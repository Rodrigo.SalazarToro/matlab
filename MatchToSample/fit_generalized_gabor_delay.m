function fit_generalized_gabor_delay(varargin)

%run at session level
Args = struct('ml',0,'rule',1);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args);

sesdir = pwd;

[~,sorted_pairs,~,~] = sorted_groups('ml');

cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;

if Args.ml
    if Args.rule == 1
        cd identity
    else
        cd location
    end
end


load delay_correlograms 
numb_pairs = size(delay_correlograms,2);
counter = 0;
for x = sorted_pairs
    counter = counter + 1;
    corr_pair = delay_correlograms{x};
    corr_peak = delay_corrcoef{x};
    corr_lag = delay_phase{x};
    
    [cf, estimates, sse, p_angle_deg, peak, xc] = generalized_gabor_fmin('correlogram',corr_pair,'peak',corr_peak,'lag',corr_lag);
    
    gen_gabor.epoch_correlogram = corr_pair;
    gen_gabor.cf = cf;
    gen_gabor.frequency = estimates(3) * 1000; %Hz
    gen_gabor.phase_shift = estimates(4); %ms
    gen_gabor.sse = sse;
    gen_gabor.phase_angle_deg = p_angle_deg; %degrees
    gen_gabor.peak = peak;
    gen_gabor.cross_corr = xc;

    delay_gabors{x} = gen_gabor;
end

write_info = writeinfo(dbstack);
save delay_gabors delay_gabors write_info

cd(sesdir)




