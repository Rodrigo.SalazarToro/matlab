function [r pos_neg] = randxcorr_peaks(varargin)

Args = struct('x',[],'y',[],'windowSize',200,'perms',10000,'lags',50,'ml',0,'pair_number',[],'mts_obj',[],'ide_loc',0);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

%finds peaks for randomization distribution rather than just zero time lag corrcoefs
mt = Args.mts_obj;
r=NaN(1,Args.perms);
for ii=1:Args.perms
    
    start1 =  ceil(rand(1)*(length(Args.x)-Args.windowSize-2));
    start2 = ceil(rand(1)*(length(Args.y)-Args.windowSize-2));
    data1 = Args.x(start1:start1+(Args.windowSize-1));
    data2 = Args.y(start2:start2+(Args.windowSize-1));
    %r = [r (dot(data1,data2))/sqrt(dot(data1,data1)*dot(data2,data2))];
    cors =  xcorr(data1,data2,Args.lags,'coeff');

    if Args.ml
        if Args.ide_loc %ide_loc = 1 if identity else, ide_loc = 0
            cd('identity')
        else
            cd('location')
        end
        load all_correlograms
        cd .. %go back to lfp2 directory
    else
        load all_correlograms
    end

    pos_or_neg = all_corrcoef{7,Args.pair_number}; %USE FULL TRIAL
    %if the average correlogram has a negative peak that is closest to zero
    %then invert the correlograms to find the negative peaks that are
    %closest to zero
    if pos_or_neg < 0
        cors = cors * -1;
        inverted = 1;
        pos_neg = -1;
    else
        inverted = 0;
        pos_neg = 1;
    end
    full_lags = [(-1*Args.lags): Args.lags];

    %Find index of central positive peak
    peak = findpeaks(cors);
    for ppp = 1 : size(peak.loc,1)
        if (cors(peak.loc(ppp)) < 0)
            %set to max phase index
            peak.loc(ppp) = length(full_lags);
        end
    end
    [phase, index] = (min(abs(full_lags(peak.loc))));
    %get real phase value by indexing the location in the full_lags
    %get corr coef by indexing the correlogram
    peak_corrcoef = cors(peak.loc(index));
    
    if size(peak.loc,1) > 0
        r(ii) = peak_corrcoef;
    else
        r(ii) = nan;
    end
    
    %if the correlograms are inverted then make the corrcoefs
    %negative to correspond to their actual values
    if inverted == 1
        r(ii) = r(ii) * -1;
    end
end


