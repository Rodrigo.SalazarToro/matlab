function lfp3d(varargin)

Args = struct('ml',0);
Args.flags = {'ml',};
Args = getOptArgs(varargin,Args);

cd('/Volumes/raid/data/monkey/betty/091001/session01')

% sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames');
trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);

cd('lfp/lfp2/')
% lfp2dir = pwd;
t = nptDir('betty*');

sample_on = floor(mt.data.CueOnset); %sample on

figure;
pcounter = 0;
for tt = trials
    load(t(tt).name)
    
    s_on = round(sample_on(tt));
    data = data(:,(s_on - 499) : (s_on + 1300));
    d1 = round((data(1,:) ./ 10000));
    d2 = round((data(3,:) ./ 10000));
    d3 = round((data(5,:) ./ 10000)); %need all positive numbers
    
    
    %     d1 = round(data(1,1:2000) ./10000);
    %     d2 = round(data(2,1:2000) ./10000);
    %     d3 = round(data(3,1:2000) ./10000);
    %
%     if tt < 10
%         plot3(d1,d2,d3,'b')
%         hold on
%     end
    for dd = 1:1800
        pcounter = pcounter + 1;
        tmat(pcounter,:) = [d1(dd) d2(dd) d3(dd)];
        timemat(pcounter) = dd;
    end    
end

u(1) = (size(unique(single(tmat),'rows'),1) / size(tmat,1)) * 100

tmat = single(tmat);
for uu = 1:4
    [~,uu] = unique(tmat,'rows');
    tmat(uu,:) = [];
end

tmatu = tmat;
tms = [];
for tm = 1:size(tmatu,1)
    ftm = find(ismember(tmatu,tmatu(tm,:),'rows'));
    tms(tm) = size(ftm,1) - 1;
end

pp = prctile(tms,99);
[~, ee] = find(tms >= pp);

gradls = linspace(.1,1,size(ee,2))
tcounter = 0;
for tmm = ee
    tcounter = tcounter + 1;
    scatter3(tmatu(tmm,1),tmatu(tmm,2),tmatu(tmm,3),1000,[gradls(tcounter) 0 0],'filled','s');
    hold on
end


tmm;






