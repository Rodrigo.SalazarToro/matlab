function [crosscorr,timelag,varargout] = mkxcorrMTS(sp2,sp1,trials,mts,varargin)

Args = struct('save',0,'redo',0,'endtrial','MatchOnset','addName',[],'timeWindow',120,'step',6,'epochBased',0,'surrogate',0,'epochLength',500,'iterations',1000,'surType','trial','AC',0);
Args.flags = {'save','redo','epochBased','surrogate','AC'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );

if Args.surrogate
    matfile = sprintf('xcorrg%s%sg%s%s%sSur%s.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName,Args.surType);
else
    matfile = sprintf('xcorrg%s%sg%s%s%s.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);
end

if Args.epochBased
    
    nep = 6;
else
    nep = 1;
end
if Args.AC
  params = struct('Fs',10000,'fpass',[0 100],'tapers',[2 3],'trialave',0);  
else
params = struct('Fs',10000,'fpass',[0 100],'tapers',[2 3],'trialave',1);
end
bins = [(-Args.timeWindow -Args.step ) [-Args.timeWindow  : Args.step : Args.timeWindow ] (Args.timeWindow  + Args.step)];
% for ep = 1: 4; allepochs2(ep).times = [];end

if isempty(nptDir(matfile)) || Args.redo
    
    if Args.surrogate && strcmp('trial',Args.surType)
        crosscorr  = zeros(nep,length(trials)-1,length(bins));
    else
        crosscorr  = zeros(nep,length(bins),length(trials));
    end
    if Args.epochBased
    cti = ones(5,1);
    else
        cti = ones(1,1);
    end
    for ti = 1 : length(trials)
        if ~isempty(sp1.data.trial(trials(ti)).cluster(1).spikes) && ~isempty(sp2.data.trial(trials(ti)).cluster(1).spikes)
            if Args.epochBased
                tlim(1) = mts.data.CueOnset(trials(ti));
                tlim(2) = mts.data.CueOffset(trials(ti));
                tlim(3) = mts.data.CueOffset(trials(ti)) + Args.epochLength;
                tlim(4) = mts.data.MatchOnset(trials(ti));
                tlim(5) = mts.data.MatchOnset(trials(ti)) + 200;
            else
                tlim(1) = mts.data.MatchOnset(trials(ti));
            end
            
            for ep = 1 : length(tlim)
               
                selSpikes1 = find(sp1.data.trial(trials(ti)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp1.data.trial(trials(ti)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                selSpikes2 = find(sp2.data.trial(trials(ti)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials(ti)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                if ~isempty(selSpikes1) && ~isempty(selSpikes2) && length(selSpikes1) > 2 && length(selSpikes2) > 2
                    nStime1 = zeros(Args.iterations,length(selSpikes1));
%                     nStime2 = zeros(Args.iterations,length(selSpikes2));
                    if Args.surrogate
                        switch Args.surType
                           case 'ISI'
                                % only for auto correlations
                                ISI = diff(sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1));
                                for ii = 1 : Args.iterations
                                    nISIi = randperm(length(ISI));
                                    nISI = ISI(nISIi);
                                    nStime1(ii,:) = [sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1(1)) cumsum(nISI) + sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1(1))];
                                end
                            case 'spikeTime'
                                % only for auto correlations
                                rtimes1 = sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1);
                                nStime1 = repmat(rtimes1,Args.iterations,1);
                                rfrP1 = min(diff(sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1)));
                                for ii = 1 : Args.iterations
                                    nStime1(:,1) = rtimes1(1);
                                    nStime1(:,end) = rtimes1(end);
                                    for nsp = 2 : length(selSpikes1)-1
                                        isi = ceil(nStime1(ii,nsp+1) - nStime1(ii,nsp-1) - rfrP1);
                                        if isi > 0
                                            nisi = randi(isi,1,1);
                                            nStime1(ii,nsp) = rfrP1 + nStime1(ii,nsp-1) + nisi;
                                            
                                        end
                                    end
                                end
                        end
                    end
                    ccs = 1;
                    if Args.surrogate && strcmp('trial',Args.surType)
                        xct = zeros(length(trials) - 1,length(bins));
                    else
                        xct = zeros(1,length(bins));
                    end
                    for spi =  selSpikes1 % loop over each spike fromce cell
                        if Args.surrogate && strcmp('trial',Args.surType)
                            for ii = 1 : length(trials)-1
                                if ti+ii < length(trials)
                                    selSpikes2 = find(sp2.data.trial(trials(ti+ii)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials(ti+ii)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                                    rtimes = sp2.data.trial(trials(ti+ii)).cluster(1).spikes(selSpikes2) - sp1.data.trial(trials(ti)).cluster(1).spikes(spi);
                                elseif ti+ii >= length(trials)
                                    selSpikes2 = find(sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                                    rtimes = sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes(selSpikes2) - sp1.data.trial(trials(ti)).cluster(1).spikes(spi);
                                end
                                if ~isempty(rtimes)
                                    [n,~] = hist(rtimes,bins);
                                    xct(ii,:) = xct(ii,:) + n;
                                end
                            end
                        else
                            rtimes = sp2.data.trial(trials(ti)).cluster(1).spikes(selSpikes2) - sp1.data.trial(trials(ti)).cluster(1).spikes(spi);
                            if ~isempty(rtimes)
                                [n,~] = hist(rtimes,bins);
                                xct = xct + n;
                            end
                        end
                    end
                    
                    if Args.surrogate && strcmp('trial',Args.surType)
                        crosscorr(ep,:,:,ti) =xct;
                        for ii = 1 : length(trials)-1
                            if ti+ii < length(trials)
                                 selSpikes2 = find(sp2.data.trial(trials(ti+ii)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials(ti+ii)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                                allepochs1(ep).iter(ii).trial(cti(ep)).times = vecc(sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1)/1000);
                                allepochs2(ep).iter(ii).trial(cti(ep)).times = vecc(sp2.data.trial(trials(ti+ii)).cluster(1).spikes(selSpikes2)/1000);
                            elseif ti+ii >= length(trials)
                                selSpikes2 = find(sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes > (tlim(ep) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes < (tlim(ep) - Args.timeWindow));
                                sptrain = vecc(sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1)/1000);    
                                allepochs1(ep).iter(ii).trial(cti(ep)).times = sptrain;
                                sptrain = vecc(sp2.data.trial(trials(ti+ii-length(trials)+1)).cluster(1).spikes(selSpikes2)/1000);
                                allepochs2(ep).iter(ii).trial(cti(ep)).times = sptrain;
                            end
                        end
                    elseif Args.surrogate && (strcmp('ISI',Args.surType) || strcmp('spikeTime',Args.surType))
                        for ii = 1 : Args.iterations
                            selSpikes1 = find(nStime1(ii,:) > (tlim(ep) - Args.epochLength + Args.timeWindow) & nStime1(ii,:) < (tlim(ep) - Args.timeWindow));
                            allepochs1(ep).iter(ii).trial(cti(ep)).times = vecc(nStime1(ii,selSpikes1)/1000);
                              allepochs2(ep).iter(ii).trial(cti(ep)).times = vecc(nStime1(ii,selSpikes1)/1000);
                         end
                        
                    else
                        crosscorr(ep,:,ti) =xct';
                        sptrain1 = vecc(sp1.data.trial(trials(ti)).cluster(1).spikes(selSpikes1)/1000);
                        sptrain2 = vecc(sp2.data.trial(trials(ti)).cluster(1).spikes(selSpikes2)/1000);
                        
                        allepochs1(ep).trial(cti(ep)).times = sptrain1;
                        
                        allepochs2(ep).trial(cti(ep)).times = sptrain2;
                        
                    end
                    %                     allepochs2(ep).times = [allepochs2(ep).times selSpikes];
               cti(ep) = cti(ep) + 1;
                
                end
                
            end
        end
    end
    f = [0:100];
    if exist('allepochs1','var') && exist('allepochs2','var')
        S = cell(length(tlim),1);
        S1 = cell(length(tlim),1);
        S2 = cell(length(tlim),1);
        phi = cell(length(tlim),1);
        J1 = cell(length(tlim),1);
        J2 = cell(length(tlim),1);
        rfreq = cell(length(tlim),1);
        if Args.surrogate 
            
            if (strcmp('ISI',Args.surType) || strcmp('spikeTime',Args.surType))
                nit = Args.iterations;
            else
                nit = length(trials)-1;
            end
            for ii = 1 : nit
%             
                 for ep = 1: length(tlim)
                    if  ep <= size(allepochs1,2) && ~isempty(allepochs1(ep).iter) && ~isempty(allepochs2(ep).iter) && ~isempty(allepochs1(ep).iter(ii).trial) && ~isempty(allepochs2(ep).iter(ii).trial)
                        try
                            %                         [Stt,freq,R(ep)]=mtspectrumpt(allepochs(ep).trial,params,0,[]);
                            params.trialave = 1;
                            [Stt,phitt,~,S1t,S2t,~,~,freq]=coherencypt(allepochs1(ep).iter(ii).trial,allepochs2(ep).iter(ii).trial,params);
                            S{ep}(ii,:) = interp1(freq,Stt,f);
                            S1{ep}(ii,:) = interp1(freq,S1t,f);
                            S2{ep}(ii,:) = interp1(freq,S2t,f);
                            phi{ep}(ii,:) = interp1(freq,phitt,f);
                        end
                    end
                end
            end
        else
          for ep = 1: length(tlim)
%                 if size(allepochs1,2) >= ep && ~isempty(allepochs1(cc).trial) && size(allepochs2,2) >= ep && ~isempty(allepochs2(cc).trial)
                    try
                        %                         [Stt,freq,R(ep)]=mtspectrumpt(allepochs(ep).trial,params,0,[]);
                        [Stt,phitt,~,S1t,S2t,J1t,J2t,freq]=coherencypt(allepochs1(ep).trial,allepochs2(ep).trial,params);
                        if Args.AC && size(S1t,2) > 1
                            for ti = 1 : length(allepochs1(ep).trial);
                                if length(allepochs1(ep).trial(ti).times) > 1
                                    S1ttt = S1t(:,ti)/(length(allepochs1(ep).trial(ti).times)/(0.001*Args.epochLength));
                                    S{ep} = [];
                                    S1{ep}(:,ti) = interp1(freq,S1ttt,f);
                                    S2{ep} = [];
                                    phi{ep} = [];
                                end
                            end
                            S2t = S1t;
                        else
                            S{ep} = interp1(freq,Stt,f);
                            S1{ep} = interp1(freq,S1t,f);
                            S2{ep} = interp1(freq,S2t,f);
                            phi{ep} = interp1(freq,phitt,f);
                            J1{ep} = J1t;
                            J2{ep} = J2t;
                            rfreq{ep} = freq;
                        end
                        
                    end
%                 end
            end
        end
    else
        S = [];
        S1 = [];
        S2 = [];
        phi = [];
        R = [];
        J1 = [];
        J2 = [];
        rfreq=[];
    end
    if Args.surrogate
        crosscorr = crosscorr(:,[2: end-1],:);
    else
        crosscorr = crosscorr(:,[2: end-1],:);
    end
    %     crosscorr = crosscorr ./ sum(crosscorr);
    timelag = bins(2:end-1);
    
    if Args.save
        save(matfile,'crosscorr','timelag','trials','S','f','S1','S2','phi','J1','J2','rfreq')
        display(sprintf('Saving %s %s',pwd,matfile))
    end
    
else
    load(matfile)
    
end
