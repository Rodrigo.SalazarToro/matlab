function [pp pf] = get_grid_info

%gets grid information for clark
%run at session level
sesdir = pwd;
cd ..
daydir = pwd;
day = daydir(end-5:end);
cd ..

load clark_grid_reference

for x = 1 : 29
    if strmatch(clark_grid_reference.days{x},day)
        pp = clark_grid_reference.pp_grid(x);
        pf = clark_grid_reference.pf_grid(x);
    end
end

cd(sesdir)