function area_pair_info


cd('/media/drobo/drobo2/vol1/data/monkey/betty')
%Determines number of pairs for each histological combination
%run at monkey level
%total areas = 28, total combinations = 378

numb_areas = 28;

days = [nptDir('09*'); nptDir('10*')];
numb_days = size(days,1);

%make list of area combinations
a_c = 0;
for c = 1:28
    for cc = (c+1) : 28
        a_c = a_c + 1;
        a_combs(a_c,:) = [c cc];
        if (c <= 9) && (cc <= 9) %if pp, 1
            pf_combs(a_c) = 1;
        elseif (c <= 9) && (cc > 9) %if pf, 2
            pf_combs(a_c) = 2;
        elseif (c > 9) && (cc > 9) %if ff, 3
            pf_combs(a_c) = 3;
        end   
    end
end

numb_combinations = nchoosek(numb_areas,2);
area_combinations = zeros(numb_days,numb_combinations);
total_pairs  = 0;
tp = 0;
total_days = 0;
monkeydir = pwd;
for x = 1:numb_days
    try
        cd([monkeydir filesep days(x).name filesep 'session01'])
        sesdir = pwd;
        cd highpass
        load SNR_channels
    catch
        days(x).name
        fprintf(1,'No SNR, or unprocessed\n')
        continue
    end
    total_days = total_days + 1
    cd(sesdir)

    groups = [];
    [channels,comb,CHcomb,groups] = checkChannels('cohInter');
    
    number_groups = size(groups,2);
    
    %check snrs
    for y = 1 : number_groups
        snr = channel_snr_list(find(channel_snr_list(:,1) == groups(y)),2);
        if snr <= 1.8
            groups(y) = 0;
        end
    end
    
    groups = groups(find(groups));%kick out bad groups
    good_groups = size(groups,2);
    
    N = NeuronalHist;
    
    for g = 1 : good_groups
        for gg = (g+1) : good_groups
            total_pairs = total_pairs + 1
            h_number1 = N.number(find(groups(g) == N.gridPos))
            h_number2 = N.number(find(groups(gg) == N.gridPos))
            
            if isempty(intersect(h_number1,[1:28])) || isempty(intersect(h_number2,[1:28]))
                fprintf(1,'bad classification\n')
            end
            
            if (groups(g) <= 32) && (groups(gg) >32)
                for z = 1:numb_combinations
                    pair = a_combs(z,:);
                    if isempty(intersect(pair,[1,3,5,6,7,9,10,12,19:28 ]))
                        if ~isempty(intersect(h_number1,pair)) && ~isempty(intersect(h_number2,pair))
                            if z == 34
                                p=1;
                            end
                            area_combinations(x,z) = area_combinations(x,z) + 1;
                            tp = tp + 1
                        end
                    end
                end
            end
        end
    end
end

totals = sum(area_combinations);

cd(monkeydir)
save area_combinations area_combinations a_combs pf_combs totals
