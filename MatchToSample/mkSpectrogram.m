function mkSpectrogram(day,varargin)
% load switchdays.mat
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% coh = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');


Args = struct('redo',0','InCor',0,'ML',0,'rule',1,'movingwin',[0.2 0.05]);
Args.flags = {'redo','InCor','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});

params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',0);

stdir = pwd;
cd(day)
sdir = pwd;
cd grams
thefiles = nptDir('spectrogram*Rule*.mat');
efiles = cell(length(thefiles),1);
for ff = 1 : length(thefiles); efiles{ff} = thefiles(ff).name; end; clear thefiles
cd(sdir)

if Args.ML
    cd session01
else
    load rules.mat
    cd(sprintf('session0%d',1+find(r==Args.rule)))
end

groups = nptDir('group*');
mts = mtstrial('auto','redosetNames');
trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',Args.rule,modvarargin{:});

ntrials = length(trials);
NeuroInfo = NeuronalChAssign;

cd lfp
lfpdir = pwd;
files = nptDir('*_lfp*');

align = {'CueOnset' 'MatchOnset'};

before = [500 1800];
after = [1600 250];
for gr = 1 : length(groups)
    ch = find(NeuroInfo.groups == str2num(groups(gr).name(end-2:end)));
    matfile = sprintf('spectrogram%sRule%d.mat',groups(gr).name,Args.rule);
    
    if isempty(strmatch(matfile,efiles)) || Args.redo
        cd(lfpdir)
        S = cell(2,1);
        t = cell(2,1);
        
        dlfp = cell(2,1);
        for tr = 1 : ntrials
            [lfp,~,~,~,~] = nptReadStreamerChannel(files(trials(tr)).name,ch);
            for al = 1 : 2
                
                
                ref = eval(sprintf('mts.data.%s(trials(tr))',align{al}));
                
                
                [dlfp{al}(:,tr),~] = preprocessinglfp(lfp(round(ref-before(al)) : round(ref+after(al))),modvarargin{:},'detrend');
                
            end
            
            
        end
        for al = 1 : 2
            [S{al},t{al},f]=mtspecgramc(dlfp{al},Args.movingwin,params);
        end
        cd(sdir)
        cd grams
        save(matfile,'S','t','f')
        display([pwd '/' matfile])
    end
end

cd(stdir)