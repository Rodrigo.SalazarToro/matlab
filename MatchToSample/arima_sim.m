function arima_sim(varargin)  

%simulates an arma(4,2) model
Args = struct('length',0,'burn',1000,'ar',[],'ma',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

total_length = Args.burn + Args.length;

ar = Args.ar;
num_ar = size(Args.ar,2);

ma = Args.ma;
num_ma = size(Args.ma,2)

y = zeros(1,total_length);
wt = zeros(1,total_length);
%burn
for x = 10 : Args.burn
    
    new_error = randn; %normally distributed random number
    wt(x) = new_error;
    
    %ar process
    for ar_p = 1 : num_ar
        y(x) = y(x) + ar(ar_p) * y(x-ar_p);
    end
    
    %ma process
    for ma_p = 1 : num_ma
        y(x) = y(x) + ma(ma_p) * wt(x-ma_p);
    end
    
    y(x) = y(x) + new_error;
end

plot(y)
