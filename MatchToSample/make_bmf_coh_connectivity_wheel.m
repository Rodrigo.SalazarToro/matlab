function make_bmf_coh_connectivity_wheel

% cd('/media/bmf_raid/data/monkey/ethyl/')
% load allbmf

cd('/media/bmf_raid/data/monkey/ethyl/110810/session01')
% bmfcoh = bmfCohgram('auto')
% save bmfcoh bmfcoh

load bmfcoh
allbmf = bmfcoh;

j = jet; %use colormap jet
hh = jet;
h(:,:,1) = hh(:,1);h(:,:,2) = hh(:,2);h(:,:,3) = hh(:,3);
lc = linspace(0,1,64);
colors = {'g' 'b' 'k'};

occipital = {'DPT','OPT','PO','V1','V2','V3','V4'}; %1
temporal = {'MT','TEO','TEOM','TPO','TPOC','TPT'}; %2
postparietal = {'LIPE','MIP','PE','PEC','PECG','PFCX','POAE','PPT'}; %3
prefrontal = {'a44','a45B','a46D','a46V','a6/32','a6DC','a6DR','a8/32','a8AD','a8AV','a8B','a9/32','a9/46D','a9/46V','a9L','a9M'}; %4
motor = {'24D','a24C','a4','a6M','a6VC','a6VR'}; %5
somatosensory = {'PAAL','PFOP','PGOP','a1','a2','a2VE','a2e','a3a','a3b','aS2E'}; %6
areas = [occipital,temporal,postparietal,prefrontal,motor,somatosensory];



[npairs,allpairs] = get(allbmf,'Number');
allgroups1 = allbmf.data.Index(allpairs,1);
allgroups2 = allbmf.data.Index(allpairs,2);
uallgroups = unique([allgroups1;allgroups2]);
ngroups = size(uallgroups,1);

%get area for each group (uses the first group info);
for ng = 1 : ngroups
    if ~isempty(intersect(allgroups1,uallgroups(ng)))
        [~,gg] = intersect(allgroups1,uallgroups(ng));
    else
        [~,gg] = intersect(allgroups2,uallgroups(ng));
    end
    allareas(ng) = allbmf.data.Index(gg(1),40);
    cats(ng) = allbmf.data.Index(gg(1),42);
end
close all
allm = [];
for fbands = 2%1:3 %go through all frequency bands
    figure
    allplots = 0;
    for eacharea = 1%[1 3 4]
        [i,gorder] = sort(cats);
        
        pairs = [];
        [i pairs{1}] = get(allbmf,'Number','epoch_sig_coh',1,'epoch_sig_percent',75,'freq_band',fbands);
        [i pairs{2}] = get(allbmf,'Number','epoch_sig_coh',2,'epoch_sig_percent',75,'freq_band',fbands);
        [i pairs{3}] = get(allbmf,'Number','epoch_sig_coh',3,'epoch_sig_percent',75,'freq_band',fbands);
        [i pairs{4}] = get(allbmf,'Number','epoch_sig_coh',4,'epoch_sig_percent',75,'freq_band',fbands);
        
        for allepochs = 1 : 4
            allplots = allplots + 1;
            subplot(3,4,allplots)
            angle = (360 / ngroups)*(pi/180);
            center = [12 12];
            radius = 10;
            
            %order the channels
            xycoord = [];
            c = 0;
            allc = [];
            for np = 1 : ngroups
                x = center(1) + 10 * cos(angle * np);
                y = center(2) + 10 * sin(angle * np);
                scatter(center(1),center(2),50,[0 0 0],'filled');hold on
                scatter(x,y,75,j((cats(gorder(np))*10),:),'filled'); hold on
                xycoord(uallgroups(gorder(np)),:) = [x y];
                
                %                 if isempty(intersect(allc,cats(gorder(np))));
                %                     text(x,y,num2str(cats(gorder(np))));
                %                     allc = [allc cats(gorder(np))];
                %                 end
                text(x,y,areas(allareas(gorder(np))));
            end
            
            for x = pairs{allepochs}
                g1 = allbmf.data.Index(x,1);
                g2 = allbmf.data.Index(x,2);
                
                cat1 = allbmf.data.Index(x,42);
                cat2 = allbmf.data.Index(x,43);
                m = allbmf.data.Index(x,17+(allepochs*4));
                if cat1 == eacharea || cat2 == eacharea
                    if cat1 ~= cat2
                        if ~isnan(m)
                            
                            [~,cc] = min(abs(lc - m));
                            cc = cc + 40;
                            allm = [allm m];
                            if m < .05
                                m
                            end
                            
                            %                             plot([xycoord(g1,1) xycoord(g2,1)],[xycoord(g1,2) xycoord(g2,2)],'color',hh(cc,:),'LineWidth',2);hold on
                            
                            
                            X = [xycoord(g1,1) xycoord(g2,1)];
                            Y = [xycoord(g1,2) xycoord(g2,2)];
                            
                            
                            %determine euclid distance
                            eu = sqrt(((X(1)-X(2))^2)+((Y(1)-Y(2))^2));
                            rr = radius - (eu/2); %this is the desired distance form center
                            
                            %find midpoint
                            x0 = (X(1)+X(2))/2;
                            y0 = (Y(1)+Y(2))/2;
                            
                            
                            %distance from midpoint to center
                            mc = sqrt(((x0-center(1))^2)+((y0-center(2))^2));
                            
                            
                            
                            if (x0-(Y(2)-Y(1))/eu*(mc-rr) - center(1))^2 < (x0+(Y(2)-Y(1))/eu*(mc-rr) - center(1))^2
                                x3 = x0-(Y(2)-Y(1))/eu*(mc-rr);
                            else
                                x3 = x0+(Y(2)-Y(1))/eu*(mc-rr);
                            end
                            if (y0-(X(2)-X(1))/eu*(mc-rr) - center(1))^2 < (y0+(X(2)-X(1))/eu*(mc-rr) - center(1))^2
                                y3 = y0-(X(2)-X(1))/eu*(mc-rr);
                            else
                                y3 = y0+(X(2)-X(1))/eu*(mc-rr);
                            end
                            
                            Xa = [X(1) x3 X(2)];
                            Ya = [Y(1) y3 Y(2)];
                            
                            t  = 1:numel(Xa);
                            ts = linspace(min(t),max(t),numel(Xa)*10); % has to be a fine grid
                            xx = spline(t,Xa,ts);
                            yy = spline(t,Ya,ts);
                            plot(xx,yy,'color',hh(cc,:),'LineWidth',2); hold on; % curve
                            
                            
                            %                     scatter(xycoord(g1,1),xycoord(g1,2),100,j(cat1+10,:),'filled'); hold on
                            %                     scatter(xycoord(g2,1),xycoord(g2,2),100,j(cat2+10,:),'filled'); hold on
                            
                        end
                    end
                end
            end
            
            
            axis([0 24 0 24])
        end
    end
    
end
% figure;
% imagesc(h)
