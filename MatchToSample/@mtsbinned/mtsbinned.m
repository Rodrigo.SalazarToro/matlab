function obj = mtsbinned(varargin)
%   @mtsbinned Constructor function for mtsbinned class.
%
%   OBJ = mtsbinned('auto','Trialsbinned',N) attempts to create a mtsbinned object by
%   using the mtstrial object (which is assumed to be
%   in the current directory). The  object contains all the
%   following fields binned by N (default = 20):
%
%   data.perf - percent correct responses
%   data.RTCR - reaction time of correct responses
%   data.RTIR - reaction time of incorrect responses
%   data.setNames - directory path
%   data.Index - bin information column wise:
%                1. rule (1 for Identity and 2 for Location)
%                2. session 
%                3. cummulative bins
%                4. bins realtive to the session
%                5. trial type (0 for NoGo and 1 for Go)
%                If a bin contain trials from different rule/session/trial
%                type an average will be set for that bin
%   data.numSets = Nbins;
%   data.binned =  Args.Trialsbinned;
%
%
%   The data are raw vectors of the binned trials for a given session. 
%
%   Dependencies: mtstrial, ProcessSessionMTS, nptdata.

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'Trialsbinned',20);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsbinned';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsb';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

trialobj = mtstrial('auto',varargin{:});

rest = mod(length(trialobj.data.FirstSac),Args.Trialsbinned);

Nbins = (length(trialobj.data.FirstSac) - rest) / Args.Trialsbinned;
% check if the right conditions (the total number of trials should be divided by the number of trials in a bin) were met to create object

if ~isempty(trialobj)
    if rest ~= 0
        fprintf('Warning !!!!!!! Number of binned trials do not match the total number of trials. Total number of trials has been trunctated \n')
    end
    [numevents,dataindices] = get(trialobj,'Number',varargin{:});
    limit = dataindices(:,2);
    Thebin = Args.Trialsbinned;
    Nbins = floor(length(trialobj.data.BehResp(limit)) / Thebin);

    for b = 1 : Nbins
        range = ((b-1)*Thebin+1) : ((b-1)*Thebin+Thebin);
        totResp = length(find((trialobj.data.BehResp(limit(range)) == 1) | trialobj.data.BehResp(limit(range)) == 0));
        CR = length(find(trialobj.data.BehResp(limit(range)) == 1));
        perf(b) = 100*(CR / totResp);
    end

    RT = trialobj.data.FirstSac;

    warning off MATLAB:divideByZero
    for b = 1 : Nbins
        range = ((b-1)*Thebin+1) : ((b-1)*Thebin+Thebin);
        CRlat = find(trialobj.data.BehResp(limit(range)) == 1);
        RTselCR = RT(limit(range(CRlat)));
        RTbin(b,1)= mean(RTselCR(~isnan(RTselCR)));
        IRlat = find(trialobj.data.BehResp(limit(range)) == 0);
        RTselIR = RT(limit(range(IRlat)));
        RTbin(b,2) = mean(RTselIR(~isnan(RTselIR)));
        rule = unique(trialobj.data.Index(limit(range),1));
        index(b,1) = mean(rule);
        if length(rule) > 1

            fprintf(sprintf('The bin #%d is using trials from different rules in the same bin \n',b));

        end
        session = unique(trialobj.data.Index(limit(range),2));
        index(b,2) = mean(session);
        if length(session) > 1

            fprintf(sprintf('The bin #%d is using trials from different session in the same bin \n',b));

        end
        trial = unique(trialobj.data.Index(limit(range),5));
        index(b,3) = mean(trial);
        if length(trial) > 1

            fprintf(sprintf('The bin #%d is using trials from Go and NoGo trials in the same bin \n',b));

        end
    end
    data.perf = perf';
    data.RTCR = RTbin(:,1);
    data.RTIR = RTbin(:,2);
    data.setNames{1} = pwd;
    data.Index = [index(:,1:2) (1:Nbins)' (1:Nbins)' index(:,3)];
    data.numSets = Nbins;
    data.binned =  Args.Trialsbinned;
    
%     data.Type{1} = trialobj.data.Type{1};

    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end

else
    % create empty object
    obj = createEmptyObject(Args);
    fprintf('The mtstrial object is empty!!!!!!!!!\n'); 

end

function obj = createEmptyObject(Args)

% useful fields for most objects


% these are object specific fields


data.perf = [];
data.RTCR = [];
data.RTIR = [];
data.binned = [];

data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
