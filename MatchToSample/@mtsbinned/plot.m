function obj = plot(obj,varargin)
% @mtsbinned/plot Plots data from mtsbinned object
%   OBJ = plot(OBJ,N) plots the % correct responses over time. 
%
%   The following optional input arguments are valid:
%   
%   - NtrialsShift : plots an hist about the number of bins before a rule
%     shift.
%
%   - RT : plots the reaction time over time
%
%  
%
%   Dpendencies: mtsbinned, @mtsbinned/get.

Args = struct('NtrialsShift',0,'RT',0);
Args.flags = {'NtrialsShift','RT'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'NtrialsShift','RT'});

[numevents,dataindices,Mark] = get(obj,'Number',varargin2{:});


if ~isempty(Args.NumericArguments)

    n = Args.NumericArguments{1}; % to work oon
    ind = find(dataindices(:,1) == n);
    limit = dataindices(ind,2);
    session = obj.data.setNames(unique(obj.data.Index(limit,2)));
    display(session(:))
else
    limit = dataindices(:,2);
end

der = diff(obj.data.Index(limit,1));
I = find(der ~= 0) + 1;
if ~isempty(I)
    ntrials(1,:) = der(I); % with 1 equals from IDENTITY  to LOCATION and inverse for -1
end
I = [1;I];

if Args.NtrialsShift
    ntrials(2,:) = diff(I);

    bar(ntrials(2,:))
    if ntrials(1,1) == 1
        xlabel('Odd numbers correspond to shifts that are from IDENTITY to LOCATION')
    elseif ntrials(1,1) == -1
        xlabel('Odd numbers correspond to shifts that are from LOCATION to IDENTITY')
    end
    ylabel(sprintf('Number of bins with bin size = %d',obj.data.binned))

else
   
    if Args.RT
        data(:,1) = obj.data.RTCR(limit);
        data(:,2) = obj.data.RTIR(limit);
        plot(data(:,1),'b')
        hold on
        plot(data(:,2),'g')
        label = 'Reaction time [ms]';
        legend('Correct responses','Incorrect responses')
    else 
        data = obj.data.perf(limit);
        plot(data)
        hold on

        label = '% correct responses';

    end


    low = floor(min(min(data)));
    high = ceil(max(max(data)));

    task{1} = 'IDENTITY';
    task{2} = 'LOCATION';

    for i = 1 : length(I)
        plot(repmat(I(i),length([low:high]),1),[low:high],'k--')
        type = obj.data.Index(limit(I(i)),1);
        text(I(i),mean([low high]),sprintf('%s',task{type}))
    end

    hold off
    labels = sprintf('%d trials-bin',obj.data.binned);

    xlabel(labels);
    ylabel(label);
end








