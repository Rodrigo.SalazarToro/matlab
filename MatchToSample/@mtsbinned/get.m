function [r,varargout] = get(obj,varargin)
%   mtstbinned/get Get function for mtsbinned objects
%   [numevents,dataindices] = get(obj,'Number') returns 
%   information about all bins. the format is column-wise:
%   1. Cummulative session
%   2. Cummulative bins
%   3. Real bins
%
%   The following arguments can be used:
%
%   - SessionBySession: returns the bins belonging to each session.
% 
%   
%   All the arguments above can be combined with 
%
%   - 'Threshold',T,'Parameter','P','Condition','C', which enables to select a set of 
%     trials according to the threshold T applied to the parameter P and with a certain
%     condition C.
%   
%   P has to be one of the following parameter:
%                     
%     SetIndex = obj.data.Index;
%     performance = obj.data.perf;
%     rtcorrect = obj.data.RTCR;
%     rtincorrect = obj.data.RTIR;
%
%   Object level is session object  
%
%   
%   Dependencies: @mtsbinned
%
Args = struct('Number',0,'SessionBySession',0,'ObjectLevel',0,'Threshold',[],'Parameter',[],'Condition','>');
Args.flags = {'Number','SessionBySession','ObjectLevel'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

SetIndex = obj.data.Index;
performance = obj.data.perf;
rtcorrect = obj.data.RTCR;
rtincorrect = obj.data.RTIR;

if Args.Number
    
    if Args.SessionBySession
        % return total number of events
        r = length(obj.data.setNames);
        % find the transition point between sessions
        sdiff = diff(SetIndex(:,2));
        stransition = [0; vecc(find(sdiff))];
        stransition = [stransition; length(SetIndex)];
        rind = [];
        cvalue = 0;
        for idx = 1:r
            value = vecc((stransition(idx)+1):stransition(idx+1));
            cvalue = cvalue(end) + value;
            % grab the indices corresponding to session idx
            rind = [rind; [repmat(idx,length(value),1) value cvalue]];
        end
    
    else
        rind = [ones(size(SetIndex,1),1) SetIndex(:,3:4)];
        
    end
    
    if ~isempty(Args.Threshold) % needs to be done the idea is that one can use the threshold in addition to the following selection of data.
        measure = eval(Args.Parameter);
        [rawindices,colindices] = eval(sprintf('find(measure %s %d)',Args.Condition,Args.Threshold));
        rindtemp = SetIndex(rawindices,[2 3 4]);
        rindlast = [];
        for i = 1 : size(rind,1)
            I = find(rindtemp(:,2) == rind(i,2));
            if ~isempty(I)
                rindlast = [rindlast; rind(i,:)];
            end
        end
        
    else 
        rindlast = rind;
    end
    
    if Args.SessionBySession
        r = length(unique(rind(:,1)));
    elseif ~Args.All & ~Args.SessionBySession
        r = length(rindlast);
    else 
        r = 1;
    end
    varargout(1) = {rindlast};
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

