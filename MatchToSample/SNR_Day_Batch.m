function SNR_channels = SNR_Day_Batch(varargin)

%run at day level

Args = struct('ml',0,'monkey','betty','sessions',[1],'concat',0,'redo',0,'annie_semichronic',0,'chunks',[]);
Args.flags = {'ml','concat','redo','annie_semichronic'};
[Args,modvarargin] = getOptArgs(varargin,Args);


%arguments
% 'chunks' : size of chunks in seconds

% example: SNR_Day_Batch('monkey','annie','sessions',3,'annie_semichronic','chunks',1)



%use to concat all trials for each channel
%session = nptDir('session0*');
global mt
day = pwd;
pwd

if Args.annie_semichronic
    all_ses = nptDir('session*');
    ses = (1:size(all_ses,1));
else
    ses = Args.sessions; 
end

for s = ses(Args.sessions)                                                   
    channel_snr_list = [];
    cd(day)
    
    if Args.annie_semichronic
        cd(all_ses(s).name)
        if ~isempty(nptDir('skip.txt.txt'))
            delete skip.txt.txt
            save skip.txt
        end
    else
        cd(['session0' num2str(s)])
    end

    if isempty(nptDir('skip.txt')) 
            
            if Args.ml
                mt = mtstrial('auto','ML','RTfromML');%passed to SignalNoiseMUA through global mt
            else
                mt = mtstrial('auto');
            end
            
            %get channel information. channels = list
            %[channels,comb,CHcomb,list] = checkChannels('cohInter');  %DO
            %NOT USE UNLESS SignalNoiseMUA is corrected
            
            H = NeuronalChAssign;
            list = H.groups;
            
            cd('highpass');
            if ~exist('SNR_channels.mat') || Args.redo
                trial_list = nptDir([Args.monkey '*.*']);
                
                all_chunks = [];
                if Args.chunks
                    chunkSize = Args.chunks;
                    
                    dtype = DaqType(trial_list(1).name);
                    if strcmp(dtype,'Streamer')
                        [num_channels,sampling_rate,scan_order] = nptReadStreamerFileHeader(trial_list(1).name);
                        headersize = 73;
                    elseif strcmp(dtype,'UEI')
                        data = ReadUEIFile('FileName',trial_list(1).name,'Header');
                        sampling_rate = data.samplingRate;
                        num_channels = data.numChannels;
                        headersize = 90;
                    else
                        error('unknown file type')
                    end
                    chunkSize = ceil(chunkSize); %round up to the nearest second
                    all_trials = ceil((trial_list(1).bytes-headersize)/2/num_channels/(chunkSize*sampling_rate)); %number of chunks (trials)
                    
                    total_data_points = (trial_list(1).bytes-headersize)/2/num_channels;
                    
                    %make a list of data points to acquire for each chunk
                    sample_size = sampling_rate * chunkSize;
                    for at = 1 : all_trials
                        if at == 1
                            all_chunks(at,:) = [1 sample_size];
                        else
                            all_chunks(at,:) = [(sample_size*(at-1)+1) sample_size*at];
                        end
                    end
                    all_chunks(all_trials,2) = total_data_points;
                else
                    %get only the specified trial indices (correct and stable)
                    all_trials = size(trial_list,1);
                end
                

                
                if Args.concat
                    SNR = SignalNoiseMUA('Channels',list,'concat')
                else
                    SNR_channels = zeros(size(all_trials,2),size(list,2)); %trial X channel
                    t=0;
                    for a = 1 : all_trials
                        t = t + 1;
                        if Args.chunks
                            trial = trial_list(1).name;
                        else
                            trial = trial_list(a).name;
                        end
                        if Args.annie_semichronic
                            
                            if isempty(Args.chunks)
                                SNR = SignalNoiseMUA('FileName',trial,'Channels',list,'monkey',Args.monkey,'annie_semichronic');
                            else
                                SNR = SignalNoiseMUA('FileName',trial,'Channels',list,'monkey',Args.monkey,'annie_semichronic','chunk_points',all_chunks(a,:));
                            end
                        else
                            SNR = SignalNoiseMUA('FileName',trial,'Channels',list,'monkey',Args.monkey);
                        end
                        SNR_channels(t,:) = SNR.SNR_99_99;
                    end
                end
                
                %make trials with zero snr == 0 instead of NaN
                %SNR_channels(isnan(SNR_channels)) = 0;
                
                %check for outliers wih Kurtosis, make them NaN
                for ch =1 : size(SNR_channels,2)
                    [threshold,outliers,kdist] = getKurtosisThresh(SNR_channels(:,ch));
                    SNR_channels(outliers,ch) = nan;
                    
                    SNR_channels(find(SNR_channels(:,ch) == 0),ch) = nan;
                    
                end
                
                
                
                channel_snr_list(:,1) = list';
                channel_snr_list(:,2) = nanmedian(SNR_channels,1)';
                try
                save SNR_channels SNR_channels channel_snr_list
                catch
                    fprintf(1,'CAN NOT WRITE');
                    pwd
                end
            else
                fprintf(1,[pwd '  SNR complete\n'])
            end
    end
end
cd(day)

