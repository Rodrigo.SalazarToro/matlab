function write_power_trials(varargin)


%run at session level
%computes power for 7 different time windows using detrended lfp sampled at 1KHz.
%fix
%sample
%delay400
%delay800
%delay
%delaymatch
%fulltrial

Args = struct('ml',0,'fft',0,'nlynx',0,'normalize',0);
Args.flags = {'ml','nlynx','fft','normalize'};
Args = getOptArgs(varargin,Args);

params = struct('tapers',[2 3],'Fs',200,'fpass',[0 150]); %should only be 100, because down sampled to 200Hz
sesdir = pwd;

if Args.nlynx
    mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
else
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
end
sample_on = floor(mt.data.CueOnset); %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

cd('lfp')
lfpdir = pwd;

if isempty(nptDir('power'))
    mkdir('power')
else
    rmdir('power','s')
     mkdir('power')
end
lfptrials = nptDir('*_lfp.*');
ntrials = size(lfptrials,1);
%make data matrix
alldata = [];
if Args.normalize
    for nt = 1 : ntrials
        cd(lfpdir)
        data = nptReadStreamerFile(lfptrials(nt).name);
        
        d = [];
        for nch = 1 : size(data,1);
            d(nch,:) = preprocessinglfp(data(nch,:),'detrend');
        end
        alldata = [alldata d];
    end
    allstd = std(alldata'); %calculate standard deviation over all trials
    clear alldata
end
for nt = 1 : ntrials
    
    s_on = round(sample_on(nt) / 5);
    s_off = round(sample_off(nt) / 5);
    m = round(match(nt) / 5);
    if mt.data.CueObj(nt) ~= 55 && m - s_off >= 160 %these are blank trials for nlyx data and the fixation trials with poor intial fixation estimates (see ProcessSessionMTS.m)
        cd(lfpdir)
        data = nptReadStreamerFile(lfptrials(nt).name);
        
        d = [];
        for nch = 1 : size(data,1);
            d(nch,:) = preprocessinglfp(data(nch,:),'detrend'); %down samples to 200Hz
            if Args.normalize
                d(nch,:) = (d(nch,:) - mean(d(nch,:))) ./ allstd(nch);
            end
        end
        data = d;
        
        
        if Args.nlynx
            fix = data(:,[(s_on - 79) : (s_on)])';
            sample = data(:,[(s_off - 79) : (s_off)])';
            delay400 = data(:,[(s_off) : (s_off + 79)])';
            delay800 = data(:,[(s_off + 81) : (s_off + 160)])';
            delay = data(:,[(s_off + 41) : (s_off + 160)])';
            delaymatch = data(:,[(m - 79) : (m)])';
            fulltrial = data(:,[(s_on - 99) : (m)])';
        else
            fix = data(:,[(s_on - 79) : (s_on)])';
            sample = data(:,[(s_off - 79) : (s_off)])';
            delay400 = data(:,[(s_off) : (s_off + 79)])';
            delay800 = data(:,[(s_off + 81) : (s_off + 160)])';
            delay = data(:,[(s_off + 41) : (s_off + 160)])';
            delaymatch = data(:,[(m - 79) : (m)])';
            fulltrial = data(:,[(s_on - 99) : (m)])';
        end
        
        if Args.fft
            [Sfix,ffix] = get_fft(fix,200);
            [Ssample,fsample] = get_fft(sample,200);
            [Sdelay400,fdelay400] = get_fft(delay400,200);
            [Sdelay800,fdelay800,fangledelay800] = get_fft(delay800,200);
            [Sdelay,fdelay] = get_fft(delay,200);
            [Sdelaymatch,fdelaymatch] = get_fft(delaymatch,200);
            [Sfulltrial,ffulltrial] = get_fft(fulltrial,200);
        else
            
            [Sfix,ffix] = mtspectrumc(fix,params);
            [Ssample,fsample] = mtspectrumc(sample,params);
            [Sdelay400,fdelay400] = mtspectrumc(delay400,params);
            [Sdelay800,fdelay800] = mtspectrumc(delay800,params);
            [Sdelay,fdelay] = mtspectrumc(delay,params);
            [Sdelaymatch,fdelaymatch] = mtspectrumc(delaymatch,params);
            [Sfulltrial,ffulltrial] = mtspectrumc(fulltrial,params);
            
            
        end
        
        power.S{1} = Sfix';
        power.S{2} = Ssample';
        power.S{3} = Sdelay400';
        power.S{4} = Sdelay800';
        power.S{5} = Sdelay';
        power.S{6} = Sdelaymatch';
        power.S{7} = Sfulltrial';
        
        power.f{1} = ffix';
        power.f{2} = fsample';
        power.f{3} = fdelay400';
        power.f{4} = fdelay800';
        power.f{5} = fdelay';
        power.f{6} = fdelaymatch';
        power.f{7} = ffulltrial';
        
        if Args.fft
            power.fangle{4} = fangledelay800';
        end
        
        
        if Args.normalize
            if Args.fft
                trial2 = [lfptrials(nt).name(1:end-9) '_normalized_fftpower.' num2strpad(nt,4) '.mat'];
            else
                trial2 = [lfptrials(nt).name(1:end-9) '_normalized_power.' num2strpad(nt,4) '.mat'];
            end
        else
            if Args.fft
                trial2 = [lfptrials(nt).name(1:end-9) '_fftpower.' num2strpad(nt,4) '.mat'];
            else
                trial2 = [lfptrials(nt).name(1:end-9) '_power.' num2strpad(nt,4) '.mat'];
            end
        end
        write_info = writeinfo(dbstack);
        
        cd([lfpdir filesep 'power'])
        save(trial2,'power','write_info')
        
    else
        
        %blank trials, don't save anything
        if Args.normalize
            if Args.fft
                trial2 = [lfptrials(nt).name(1:end-9) '_normalized_fftpower.' num2strpad(nt,4) '.mat'];
            else
                trial2 = [lfptrials(nt).name(1:end-9) '_normalized_power.' num2strpad(nt,4) '.mat'];
            end
        else
            if Args.fft
                trial2 = [lfptrials(nt).name(1:end-9) '_fftpower.' num2strpad(nt,4) '.mat'];
            else
                trial2 = [lfptrials(nt).name(1:end-9) '_power.' num2strpad(nt,4) '.mat'];
            end
        end
        
        power = [];
        cd([lfpdir filesep 'power'])
        
        save(trial2,'power')
        fprintf(1,'bad')
    end
end



cd(sesdir)