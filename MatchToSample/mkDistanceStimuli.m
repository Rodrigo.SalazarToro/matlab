function [distLocC,distObjC,f,varargout] = mkDistanceStimuli(obj,ind,varargin)

Args = struct('sortedStim',4);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


MLrules = {'IDE' 'LOC'};
for r = 1: 2
    c = 1;
    for i = ind
        groupsN = obj.data.Index(i,10:11);
        cd(obj.data.setNames{i})
        cd tuning
        
        filename1 = sprintf('iCueLocg%04.0fg%04.0fRule%d.mat',groupsN(1),groupsN(2),r);
        filename2 = sprintf('iCueObjg%04.0fg%04.0fRule%d.mat',groupsN(1),groupsN(2),r);
        orderObj = eval(sprintf('obj.data.%sObjSeq(i,%d,:)',MLrules{r},Args.sortedStim));
        orderLoc = eval(sprintf('obj.data.%sLocSeq(i,%d,:)',MLrules{r},Args.sortedStim));
        if ~isempty(nptDir(filename1)) && ~isempty(nptDir(filename2))
            loc = load(filename1,'groupN','f');
            ide = load(filename2,'groupN');
            f = loc.f;
            for p = 1 : 4
                distLocC(:,c,p,r) = loc.groupN(p).C(:,orderLoc(3)) - loc.groupN(p).C(:,orderLoc(1));
                distObjC(:,c,p,r) = ide.groupN(p).C(:,orderObj(3)) - loc.groupN(p).C(:,orderObj(1));
                
            end
            clear loc ide
            c = c + 1;
        end
    end
    cd ..
end