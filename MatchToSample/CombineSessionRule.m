function CombineSessionRule(varargin)

Args = struct('deleteOriSession',0);
Args.flags = {'deleteOriSession'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'deleteOriSession'});

combine = nptDir('combinedsession.txt');
skip = nptDir('skip.txt');

if isempty(combine) & isempty(skip)

    daydir = pwd;
    namedir = 'originalSessionFormat';
    mkdir(namedir);
    % [SUCCESS,MESSAGE,MESSAGEID] = fileattrib(namedir,'w','a','s');

    backup = sprintf('%s%s%s',daydir,filesep,namedir);
    sessiondir = nptDir('session*','CaseInsensitive');

    for ss = 1 : size(sessiondir,1)
        %%%%%%%%%%%%%%%%% removes the last trial of a session if empty
        cd(sessiondir(ss).name)
        inifile = nptDir('*.ini','CaseInsensitive');
        [INI,status] = ReadIniCognitive(inifile.name);
        if ss ~= 1 && INI.DesiredNumberTrials ~= INI.NumberOfTrials
            fprintf('Number of desired trials is different from the number of trial presented \n');
            files = nptDir(sprintf('*.0*')); % assumes that each session has less than 1000 trials
            [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(files(end).name);
            markerfile = nptDir('*.mrk','CaseInsensitive');
            [MARKERS,records] = ReadMarkerFile(markerfile.name);
            mt = MarkersToTrials(MARKERS);
            markert  = size(mt,2);

            if markert < size(files,1) | isempty(data)
                for lefttrial = markert+1 : size(files,1)
                    file = files(lefttrial).name;
                    dot = strfind(file,'.');
                    dfile = [file(1:dot-1) file(dot+1:end)];
                    movefile(file,dfile);
                    fprintf('%s moved to %s \n',file,dfile);
                end
            end
            seqfile = nptDir('*.seq','CaseInsensitive');
            copyfile(seqfile.name,sprintf('%sOrig',seqfile.name));
            fid = fopen(seqfile.name,'r');
            A = textread(seqfile.name,'%s','whitespace','','bufsize',1000000);
            fclose(fid);
            fid = fopen(seqfile.name,'w');
            fprintf('%s moved to %s \n',seqfile.name,sprintf('%sOrig',seqfile.name));
            NewA = A{1}(1:(60+markert*14));

            fprintf(fid,'%s',NewA);% the 60th firt characters are the info at the beginning of the seq file and then each trial occupies 14 characters
            fclose(fid);

            cd ..
        end
        cd(daydir)
        %%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('moving %s to %s \n',sessiondir(ss).name,namedir);
        movefile(sessiondir(ss).name,backup,'f');
    end
    cd(backup)
    sfid = fopen('skip.txt','wt');
    fclose(sfid);
    d = nptDir('session*');
    for ses = 1 : size(d,1) % assumes that session 01 is the eye calibration
        cd(d(ses).name)
        seqfile = nptDir('*.seq','CaseInsensitive');
        if isempty(seqfile)
            type(ses) = -1;
        else
            [SEQUENCE,SessionType] = ReadSequenceFile(seqfile.name);

            if strcmp(SessionType,'IDENTITY')

                type(ses) = 1;

            elseif strcmp(SessionType,'LOCATION')

                type(ses) = 2;
            end
        end %isempty(seq)

        cd(backup)

    end %ses = 1 : size(d,1)

    trans = [0 find((diff(type)) ~= 0) size(type,2)]; % find the transition trials between rules

    for sec = 1 : size(trans,2) - 1
        cd(daydir) % in the day dir
        newdir = sprintf('session%02.0f',sec);
        mkdir(newdir)
        cd(backup)
        cd(d(trans(sec)+1).name)

        feature = {'.0','.snc0','_','.ini','.seq'};

        for fe = 1 : size(feature,2)
            sfiles = nptDir(sprintf('*%s*',feature{fe}));
            if fe ==1
                nfiles = size(sfiles,1);
            end
            if ~isempty(sfiles)
                dot = strfind(sfiles(1).name,feature{fe}); % find the dot inthe filename


                for sf = 1 : size(sfiles,1)

                    cfiles = sfiles(sf).name;
                    cfiles(dot-2:dot-1) =  sprintf('%02.0f',sec);
                    des = sprintf('%s%s%s%s%s',daydir,filesep,newdir,filesep,cfiles);
                    fprintf('Copying %s to %s \n',sfiles(sf).name,des);
                    copyfile(sfiles(sf).name,des)
                end
            end
        end

        markerfile = nptDir('*.mrk','CaseInsensitive');
        if ~isempty(markerfile)
            nomarker = true;
            [MARKERS,records] = ReadMarkerFile(markerfile.name);
            dotmrk = strfind(markerfile.name,'.MRK');
            markermat = markerfile.name;
            markermat(dotmrk) = '_';
            markermat(dotmrk-2:dotmrk-1) =  sprintf('%02.0f',sec);
            tmarkers = MarkersToTrials(MARKERS);
            save(sprintf('%s%s%s%s%s.mat',daydir,filesep,newdir,filesep,markermat),'tmarkers');
            %     copyfile(sprintf('%s%s%s',backup,filesep,d(trans(sec)+1).name),newdir);
        else
            nomarker = false;
        end
        ns = trans(sec+1) - trans(sec);

        if ns > 1
            for n = 2 : ns
                cd(sprintf('%s%s%s',backup,filesep,d(trans(sec)+n).name)) % in the
                seqfilec = nptDir('*.seq','CaseInsensitive');
                markerfile = nptDir('*.mrk','CaseInsensitive');


                for fe = 1 : 2
                    sfiles = nptDir(sprintf('*%s*',feature{fe}));
                    if fe ==1
                        ntrial = size(sfiles,1);
                    end
                    if ~isempty(sfiles)
                        dot = strfind(sfiles(1).name,feature{fe});
                        if ntrial ~= size(sfiles,1)
                            modtrial = size(sfiles,1);
                            disp('Number of sync trials differs from the number of trials; be cautious the two numbers will not match!!!!!!!');
                        else
                            modtrial = ntrial;
                        end

                        for sf = 1 : modtrial

                            cfiles = sfiles(sf).name;
                            cfiles(dot-2:dot-1) =  sprintf('%02.0f',sec);
                            cfiles((dot+size(feature{fe},2)-1):(dot+size(feature{fe},2)+2)) =  sprintf('%04.0f',nfiles+sf);
                            des = sprintf('%s%s%s%s%s',daydir,filesep,newdir,filesep,cfiles);
                            fprintf('Copying %s to %s \n',sfiles(sf).name,des);
                            copyfile(sfiles(sf).name,des)
                        end
                    end
                end

                [MARKERS,records] = ReadMarkerFile(markerfile.name);

                cptmarkers = MarkersToTrials(MARKERS);

                A = textread(seqfilec.name,'%s','whitespace','','bufsize',100000);

                cd(sprintf('%s%s%s',daydir,filesep,newdir))

                seqfile = nptDir('*.seq','CaseInsensitive');

                fid = fopen(seqfile.name,'a');

                ln = findstr(A{1},'MP2') + 5;
                fprintf(fid,'%s',A{1}(ln:end));
                fclose(fid);

                if nomarker
                    load(markermat)
                    tmarkers = [tmarkers cptmarkers];
                    save(sprintf('%s%s%s%s%s.mat',daydir,filesep,newdir,filesep,markermat),'tmarkers');
                end

                nfiles = nfiles + ntrial;
            end  %ns > 1
            %         theinifile = nptDir('*.ini','CaseInsensitive');
            %         writekeys = {'ACTUAL NUMBER OF TRIALS SHOWN','','Number Of Trials',nfiles};
            %         inifile(theinifile.name,'write',writekeys,'plain');
            %         writekeys = {'TMatchToSampleGUI','','Desired Number Trials',ns*100};
            %         inifile(theinifile.name,'write',writekeys,'plain');
        end
    end %sec = 1 : size(trans,2) -1
    cd(daydir)


    sfid = fopen('combinedsession.txt','wt');
    fclose(sfid);

    if Args.deleteOriSession
        rmdir(backup,'s')
    end
end