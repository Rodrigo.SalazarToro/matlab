function obj = plot(obj,varargin)


Args = struct('pLevel',3,'xlim',[0 50],'plotType','standard','freqs',1,'epoch','saccade','lfpgr',[],'unitgr',[],'cluster','01m');
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});


if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work soon
    ind = dataindices(n);
else
    
end



switch Args.epoch
    
    case 'saccade'
        ep1 = 3;
        ep2 = 4;
        
end
sses = findstr(obj.data.setNames{n},'session');

cd(obj.data.setNames{n}(1:sses+9))
neuroinfo = NeuronalChAssign;

if isempty(Args.lfpgr)
    Args.lfpgr = neuroinfo.groups(1);
    lfpch = 1;
    
else
    lfpch = find(neuroinfo.groups == Args.lfpgr);
    
end

groups = nptDir('group0*');
if isempty(Args.unitgr)
    unitgr = str2num(groups(1).name(end-3:end));
    
else
    unitgr = Args.unitgr;
end

theMUA = fileparts(obj.data.setNames{ind});
theMUA = regexprep(theMUA,'lfp',sprintf('group%04.f',unitgr));
theMUA = regexprep(theMUA,'mband',sprintf('cluster%s/ispikes.mat',Args.cluster));


load(theMUA)

switch Args.plotType
    case 'standard'
        load(obj.data.setNames{ind},'-mat');
        tbin = 1000/SR;
        
        times = [0:tbin:size(hb,2)*tbin-1];
        
        blfp = real(hb(lfpch,:,Args.freqs));
        plot(times,blfp)
        hold on
        
        thespikes = sp.data.trial(ind).cluster.spikes;
        
        line([thespikes; thespikes],[repmat(min(blfp),1,length(thespikes)); repmat(max(blfp),1,length(thespikes))],'color','r');
        line([obj.data.Index(ind,3)+obj.data.Index(ind,4); obj.data.Index(ind,3)+obj.data.Index(ind,4)],[min(blfp); max(blfp)],'color','k','LineStyle','--'); 
        line([obj.data.Index(ind,3); obj.data.Index(ind,3)],[min(blfp); max(blfp)],'color','k','LineStyle','--'); 
        
        switch Args.epoch
            case 'saccade'
                
                xlim([obj.data.Index(ind,3)-100 obj.data.Index(ind,3)+400]);
                
        end
end
xlabel('time (ms)')

set(gcf,'Name',[obj.data.setNames{ind} ' lfpgr' num2str(Args.lfpgr) ' unitgr' num2str(unitgr)])
% good example 648