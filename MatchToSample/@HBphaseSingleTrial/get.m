function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'unitcortex',[],'lfpcortex',[],'unitType',[],'unitGroup',[],'lfpGroup',[],'hist',[]);
Args.flags = {'Number','ObjectLevel','sameElec'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rtemp = [1:obj.data.numSets];
    
    
    varargout{1} = rtemp;
    r = length(varargout{1});
    

else
    r = get(obj.nptdata,varargin{:});
    
end

