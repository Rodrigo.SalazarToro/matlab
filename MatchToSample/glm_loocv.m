function glm_loocv(varargin)

%run at day level
%uses logistic regression to model delay period with LOOCV

%Args.model: 9stims(1),location(2),identity(3)

Args = struct('ml',0,'model',1,'threshold',1,'sorted',1);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

daydir = pwd;

cd('session01')
sesdir = pwd;

[NeuroInfo] = NeuronalChAssign;
%get actual group number
groups = NeuroInfo.groups;
if ~Args.ml
    groups = 1:size(groups,2);
end

if Args.ml
    c = mtscpp('auto','ml','ide_only');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
else
    c = mtscpp('auto');
    mt = mtstrial('auto','redosetNames');
end

%get trial information
if Args.ml
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
else
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

%get pair information
if Args.sorted
    [~,ii] = get(c,'Number','sorted'); %get list of sorted pairs
else
    [~,ii] = get(c,'Number');
end

%get stimulus list (obj[1 1 1 2 2 2 3 3 3], loc[1 2 3 1 2 3 1 2 3])
if Args.model == 1
    stimulus = stimulus_list('ml');
elseif Args.model == 2
    stimulus = stimulus_list('ml','loc');
elseif Args.model == 3
    stimulus = stimulus_list('ml','ide');
end

%Get number of channels
chnumb=length(NeuroInfo.channels);

cd (['lfp' filesep 'lfp2']);
lfp2 = pwd;

%run for each trial
m = 0;
for t = 1 : size(total_trials,2);
    pair = 0;
    counter = 0;
    for c1 = 1 : chnumb
        for c2 = (c1+1) : chnumb
            column = 0;
            pair = pair + 1;
            if intersect(pair, ii);
                counter = counter + 1;
                thresh = Args.threshold;
                cd(lfp2)
                if Args.ml
                    %Args.rule = 1 for identity and 2 for location
                    [glm_output,stim,cross,probabilities] = cppStimuli_glm_loocv('c1',groups(c1),'c2',groups(c2),'threshh',thresh,'chnumb',chnumb,'ml',Args.ml,'rule',1,'pair',pair,'model',Args.model,'leave_trial',t,'stimulus',stimulus);
                end
                
                column = column + 1;
                glm_delay_loocv{counter,column} = glm_output;
                trial_cross(counter,:) = cross;
                
                probs{counter,column} = probabilities;
            end
        end
    end
    
    cd(sesdir)
    
    %run LRCA for the left out trial
    p = predict_glm_loocv('ml','fits',glm_delay_loocv,'trial_stim',stimulus(t),'trial_cross',trial_cross,'model',Args.model,'probs',probs);
    if p
        m = m + 1;
    end
    t
    (m/t) * 100
end
performance = (m/t) * 100;
cd(lfp2)

if Args.model == 1
    save glm_loocv_performance performance
elseif Args.model == 2
    save glm_loocv_performance_loc performance
elseif Args.model == 3
    save glm_loocv_performance_ide performance
end

cd(daydir)


