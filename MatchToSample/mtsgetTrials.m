function ind = mtsgetTrials(ep,varargin)
% ind = getTrials(ep,TrialType,Response,salience,channel)
% finds the indices that corresponds to those conditions.
% Inputs:
%
% - ep : @lfptraces obj. or @mtstrial
%
%
%
% - stable or transition : stable and transition periods.
%
% - channel : channel number starting at 1.


Args = struct('ML',0,'lowThresRT',80,'highThresRT',inf,'CueObj',[],'CueLoc',[],'iCueObj',[],'iCueLoc',[],'iMatchSac',[],'sumMatchObj',[],'sumMatchPos',[],'unionSel',[],'MatchObj1',[],'MatchPos1',[],'MatchObj2',[],'MatchPos2',[],'BehResp',[],'Nchannels',[],'stable',0,'transition',0,'noRejNoisyT',0,'lowLim',[],'highLim',[],'rule',[],'block',[],'delay',[],'Nlynx',0);
Args.flags = {'stable','transition','noRejNoisyT','ML','Nlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% switching phase based on getperformance fct: stable phase is based on < 80% CR and start no sooner than # 101 trial the resting trials are considered as transition
if Args.stable || Args.transition
    index = [];
    for ss = 1 : length(ep.data.setNames)
        cd(ep.data.setNames{ss})
        
%         strials = find(ep.data.Index(:,2) == ss);
        strials = [1 :size(ep.data.Index,1)];
        tindex = ep.data.Index(strials,:);
        [perf,ti,cross] = getperformance('getThreshCross','fit',modvarargin{:});
        
        if ~isempty(cross)
            stableT = [];
            
            for ln = 1 : size(cross,2);
                if length(unique(ep.data.Index(strials,1))) < 2
                    newind = vecr(tindex(find(tindex >= cross(1,ln) & tindex <= cross(2,ln))));
                    
                    stableT = [stableT newind ]; % stable phase
                    %             transind{ln} = index(find(index < cross(1,ln) | index > cross(2,ln))); % unstable phase
                else
                    if length(unique(ep.data.Index(strials,1))) ~= 1
                        
                        eor = find(diff(tindex(cross(1,ln):end,1)) ~= 0);
                        if ~isempty(eor); cross(2,ln) = cross(1,ln) + eor(1) - 1; end
                        
                    end
                    newind = vecr(tindex(find(tindex >= cross(1,ln) & tindex <= cross(2,ln))));
                    stableT = [stableT newind ];
                end
            end
            transT = setdiff(strials,stableT);
        else
            stableT = [];
            transT = tindex;
        end
        if Args.stable
            sindex = stableT;
        elseif Args.transition
            sindex = transT;
        end
        
        index = [index sindex];
    end
else
    index = [1 : ep.data.numSets];
end
%%
%% selection of specific cues and match
fields = fieldnames(ep.data);
nindex = [];
nindexexist = false;
orindex = [];
fieldbefore = [];
for f = 1 : length(fields)
    if ~isempty(strfind('CueObjCueLocMatchObj1MatchPos1MatchObj2MatchPos2BehResp',fields{f}))
        if ~isempty(eval(sprintf('Args.%s',fields{f})))
            nindexexist = true;
            value = eval(sprintf('ep.data.%s',fields{f}));
            tvalue = eval(sprintf('Args.%s',fields{f}));
            if isempty(tvalue)
                tvalue = unique(eval(sprintf('ep.data.%s',fields{f})));
            end
            eval(sprintf('%s = [];',fields{f}))
            for t = 1 : length(tvalue)
                eval(sprintf('t%s = find(value == tvalue(t));',fields{f}))
                eval(sprintf('%s = [%s; t%s];',fields{f},fields{f},fields{f}))
            end
            fieldbefore = [fieldbefore; f];
            %             if isempty(strfind('MatchObj1MatchPos1MatchObj2MatchPos2',fields{f}))
            if isempty(Args.unionSel) || isempty(strfind(Args.unionSel,fields{f}))
                if length(fieldbefore) == 1
                    nindex = eval(sprintf('%s',fields{f}));
                else
                    nindex = eval(sprintf('intersect(%s,nindex)',fields{f}));
                end
            else
                nindex = eval(sprintf('union(%s,nindex)',fields{f}));
            end
            %             elseif length(fieldbefore) == 1
            %                 index = eval(sprintf('%s',fields{f}));
            %         elseif length(fieldbefore) > 1 && ~isempty(strfind('MatchObj1MatchPos1MatchObj2MatchPos2',fields{f}))
            %             orindex = eval(sprintf('[orindex; %s];',fields{f}));
            %             end
            
        end
    end
end
if nindexexist
    index = intersect(index,nindex);
end

if ~isempty(Args.sumMatchObj)
    tindex = find((ep.data.MatchObj1 + ep.data.MatchObj2) == Args.sumMatchObj);
    index = intersect(index,tindex);
end

if ~isempty(Args.sumMatchPos)
    tindex = find((ep.data.MatchPos1 + ep.data.MatchPos2) == Args.sumMatchPos);
    index = intersect(index,tindex);
end


if Args.Nlynx
    if ~isempty(Args.iCueObj)
        objs = unique(ep.data.CueObj)';
        objs = setdiff(objs,[55 56 57]); %kick out blank(55), interleaved fixation(56), and fixation blocks(57), see ProcessSessionMTS
        
        icueindex = find(ep.data.CueObj == objs(Args.iCueObj))';
        index = intersect(index,icueindex);
    end
else
    if ~isempty(Args.iCueLoc) || ~isempty(Args.iCueObj)
        fields = {'CueLoc' 'CueObj'};
        for f = 1 : 2
            if ~isempty(eval(sprintf('Args.i%s',fields{f})))
                allitems = vecr(unique(eval(sprintf('ep.data.%s',fields{f}))));
                if Args.ML
                    allitems = setdiff(allitems,[7 55]); % remove fixation trials and blocks for Nlynx and ML
                end
                value = eval(sprintf('ep.data.%s',fields{f}));
                tvalue = eval(sprintf('Args.i%s',fields{f}));
                eval(sprintf('i%s = [];',fields{f}))
                for t = 1 : length(tvalue)
                    eval(sprintf('t%s = find(value == allitems(tvalue(t)));',fields{f}))
                    eval(sprintf('i%s = [i%s; t%s];',fields{f},fields{f},fields{f}))
                end
                index = eval(sprintf('intersect(i%s,index)',fields{f}));
            end
        end
    end
end
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% reaction time threshold
if ~isempty(Args.lowThresRT)
    indRT = find(ep.data.FirstSac > Args.lowThresRT & ep.data.FirstSac < Args.highThresRT | (isnan(ep.data.FirstSac) &  ~isempty(find(Args.CueObj == 55))) | (isnan(ep.data.FirstSac) &  ~isempty(find(Args.CueObj == 7))));
else
    indRT = index;
end
index = intersect(index,indRT);
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% only for mtslfptraces object might disappear in future versions
if isa(ep,'mtslfptraces')
    if isempty(Args.Nchannels)
        channel = unique(ep.data.Nchannels);
    else
        channel = Args.Nchannels;
    end
    
    ch = [];
    
    for a = 1 : length(channel)
        ampch = channel(a);
        %     if length(ep.data.Nchannels) == 1
        succh = find(ampch == ep.data.Nchannels);
        
        c = vecc(succh : length(ep.data.Nchannels) : ep.data.numSets);
        ch = [ch; c];
        
    end
else
    ch = index;
end
indt = intersect(index,ch);
%%
%% noisy trial rejection based on lfp traces and on the ArtRemTrialsLFP fct
if ~Args.noRejNoisyT
    [pdir,cdir] = getDataDirs('lfp','relative','CDNow');
    
    artfile = nptDir('rejectedTrials.mat');
    
    if isempty(artfile)
        cd ..
        rejecTrials = ArtRemTrialsLFP('save',modvarargin{:});
        [pdir,cdir] = getDataDirs('lfp','relative','CDNow');
    else
        load rejectedTrials.mat
    end
    v = whos('-file','rejectedTrials.mat');
    for s =  1 : length(v);
        t = strfind(v(s).name,'rejec');
        tt = strfind(v(s).name,'Trials');
        ttt = strfind(v(s).name,'PW');
        if ~isempty(t) & ~isempty(tt) & isempty(ttt)
            rejectTrials = eval(v(s).name);
        end;
    end
    ind = setdiff(indt,rejectTrials);
    cd(cdir)
else
    ind = indt;
end
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% specific interval trials >= lowLim and <= highLim
if ~isempty(Args.lowLim)
    
    indl = ind(ind >= Args.lowLim);
else
    
    indl = ind;
end

if ~isempty(Args.highLim)
    indh = ind(ind <= Args.highLim);
else
    indh = ind;
    
end


ind = intersect(indl,indh);
%% rule selection

if ~isempty(Args.rule)
    [r,theind] = get(ep,'Number','rule',Args.rule,modvarargin{:});
    
    ind = intersect(theind(:,2),ind);
end
if ~isempty(Args.block)
    [r,theind] = get(ep,'Number','block',Args.block,modvarargin{:});
    
    ind = intersect(theind(:,2),ind);
end

%% specific delays

if ~isempty(Args.delay)
    dind = find(ep.data.MatchOnset-ep.data.CueOffset >= Args.delay(1) & ep.data.MatchOnset-ep.data.CueOffset <= Args.delay(2));
    ind = intersect(dind,ind);
end

%% trials with saccadic response to specific location
if ~isempty(Args.iMatchSac)
    
    %% for IDE rule
        
        % coorect responses
        match1 = ep.data.CueObj == ep.data.MatchObj1;
        sac1 = match1.*(ep.data.MatchPos1+1) ;
        match2 = ep.data.CueObj == ep.data.MatchObj2;
        sac2 = match2.*(ep.data.MatchPos2+1) ;
        sac = sac1 + sac2;
        
        sacC = sac .* ep.data.BehResp;
        
        % incorrect responses
        match1 = ep.data.CueObj ~= ep.data.MatchObj1;
        sac1 = match1.*(ep.data.MatchPos1+1);
        match2 = ep.data.CueObj ~= ep.data.MatchObj2;
        sac2 = match2.*(ep.data.MatchPos2+1);
        otherrule = (match1+match2) > 1;
        
        sac = sac1 + sac2;
        sac(otherrule) = 0;
        
        sacI = sac .* ~ep.data.BehResp;
       
        sac = sacC + sacI; % combined correct and incorrect coded 1 : 6 and not 0 to 5
        fix = ep.data.CueObj ~=55;
        sac = sac .* fix;
        sacRes = unique(sac);
        if length(sacRes) == 4; st = 2; else st= 1; end
        c = 1;for item=  sacRes(st:end)'; msac(:,c) = (sac == item) * c; c = c + 1; end
        sacIDE = sum(msac,2);
        sacIDE = sacIDE .* (ep.data.Index(:,1) == 1);
    %% for LOC rule
        % need to work on this one
        % coorect responses
        sacC = (ep.data.CueLoc+1) .* ep.data.BehResp;
        
         % incorrect responses
        loc1 = ep.data.CueLoc ~= ep.data.MatchPos1;
        sac1 = loc1.*(ep.data.MatchPos1+1) ;
        loc2 = ep.data.CueLoc ~= ep.data.MatchPos2;
        sac2 = loc2.*(ep.data.MatchPos2+1) ;
        sac = sac1 + sac2;
        
        sacI = sac .* ~ep.data.BehResp;
        sac = sacC + sacI; % combined correct and incorrect coded 1 : 6 and not 0 to 5
        
        sacRes = unique(sac);
        if length(sacRes) == 4; st = 2; else st= 1; end
        c = 1;for item=  sacRes(st:end)'; msac(:,c) = (sac == item) * c; c = c + 1; end
        sacLOC = sum(msac,2);
     sacLOC = sacLOC .* (ep.data.Index(:,1) == 2);
    sac = sacIDE + sacLOC;
    sind = find(sac == Args.iMatchSac);
    
    ind = intersect(sind,ind);
end



