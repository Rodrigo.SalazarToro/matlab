function obj = plv(varargin)

%makes phase locking value (plv) object

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'plv';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'plv';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)
sesdir = pwd;


%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end


if ~isempty(mtst)
    
    if Args.ml
        N = NeuronalHist('ml');
        
        %determine which groups have been sorted and which groups are noisy
        [goodgroups,~,pair_list] = sorted_groups('ml');
        noisy = noisy_groups('ml');
    else
        N = NeuronalHist;
        
        %determine which groups have been sorted and which groups are noisy
        [goodgroups,~,~,pair_list] = sorted_groups;
        goodgroups = 1:size(goodgroups,2);
        noisy = [];
    end
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
    hpairs = nptDir('hilbertentropy*');
    npairs = size(hpairs,1);
    data.Index = zeros(npairs,56);
    if npairs > 0
        nfiles = 0;
        for f = 1 : npairs
            %         if isempty(intersect(pair_list(f,:),noisy)) & ~isempty(intersect(pair_list(f,1),goodgroups)) & ~isempty(intersect(pair_list(f,2),goodgroups))
            nfiles = nfiles + 1;
            %             load(['hilbertentropy' num2strpad(pair_list(f,1),2) num2strpad(pair_list(f,2),2)])
            load(hpairs(f).name)
            
            data.setNames{nfiles,1} = ([lfp2dir filesep hpairs(f).name]);
            
            if Args.ml
                
                g1 = pair_list(nfiles,1);
                g2 = pair_list(nfiles,2);
                %find wich channel number the group corresponds
                %get histology information for both groups
                [~,gp1] = intersect(N.gridPos,g1);
                [~,gp2] = intersect(N.gridPos,g2);
                data.Index(nfiles,3) = N.number(gp1); %this is the number for the specific area
                data.Index(nfiles,4) = N.number(gp2);
            else
                g1 = groups(1);
                g2 = groups(2);
                data.Index(nfiles,3) = N.number(groups(1)); %this is the number for the specific area
                data.Index(nfiles,4) = N.number(groups(2));
            end
            data.Index(nfiles,1) = g1;
            data.Index(nfiles,2) = g2;
            
            if exist(['epochthresh_hilbertentropy' num2strpad(g1,3),num2strpad(g2,3) '.mat'])
                load(['epochthresh_hilbertentropy' num2strpad(g1,3),num2strpad(g2,3)])
                
                
                %number sig bins
                data.Index(f,5) = sig.fcount(1,1); %epoch 1,freq band 1
                data.Index(f,6) = sig.fcount(1,2);
                data.Index(f,7) = sig.fcount(1,3);
                data.Index(f,8) = sig.fcount(1,4);
                data.Index(f,9) = sig.fcount(2,1);  %epoch 2,freq band 1
                data.Index(f,10) = sig.fcount(2,2);
                data.Index(f,11) = sig.fcount(2,3);
                data.Index(f,12) = sig.fcount(2,4);
                data.Index(f,13) = sig.fcount(3,1);  %epoch 3,freq band 1
                data.Index(f,14) = sig.fcount(3,2);
                data.Index(f,15) = sig.fcount(3,3);
                data.Index(f,16) = sig.fcount(3,4);
                data.Index(f,17) = sig.fcount(4,1); %epoch 4,freq band 1
                data.Index(f,18) = sig.fcount(4,2);
                data.Index(f,19) = sig.fcount(4,3);
                data.Index(f,20) = sig.fcount(4,4);
                
                %means
                data.Index(f,21) = sig.fmean(1,1); %epoch 1,freq band 1
                data.Index(f,22) = sig.fmean(1,2);
                data.Index(f,23) = sig.fmean(1,3);
                data.Index(f,24) = sig.fmean(1,4);
                data.Index(f,25) = sig.fmean(2,1);  %epoch 2,freq band 1
                data.Index(f,26) = sig.fmean(2,2);
                data.Index(f,27) = sig.fmean(2,3);
                data.Index(f,28) = sig.fmean(2,4);
                data.Index(f,29) = sig.fmean(3,1);  %epoch 3,freq band 1
                data.Index(f,30) = sig.fmean(3,2);
                data.Index(f,31) = sig.fmean(3,3);
                data.Index(f,32) = sig.fmean(3,4);
                data.Index(f,33) = sig.fmean(4,1); %epoch 4,freq band 1
                data.Index(f,34) = sig.fmean(4,2);
                data.Index(f,35) = sig.fmean(4,3);
                data.Index(f,36) = sig.fmean(4,4);
                
                %means
                data.Index(f,37) = sig.fphase(1,1); %epoch 1,freq band 1
                data.Index(f,38) = sig.fphase(1,2);
                data.Index(f,39) = sig.fphase(1,3);
                data.Index(f,40) = sig.fphase(1,4);
                data.Index(f,41) = sig.fphase(2,1);  %epoch 2,freq band 1
                data.Index(f,42) = sig.fphase(2,2);
                data.Index(f,43) = sig.fphase(2,3);
                data.Index(f,44) = sig.fphase(2,4);
                data.Index(f,45) = sig.fphase(3,1);  %epoch 3,freq band 1
                data.Index(f,46) = sig.fphase(3,2);
                data.Index(f,47) = sig.fphase(3,3);
                data.Index(f,48) = sig.fphase(3,4);
                data.Index(f,49) = sig.fphase(4,1); %epoch 4,freq band 1
                data.Index(f,50) = sig.fphase(4,2);
                data.Index(f,51) = sig.fphase(4,3);
                data.Index(f,52) = sig.fphase(4,4);
                
                data.Index(f,53) = sig.ftotalbins(1,1); %get information about how man bins are in each frequency range
                data.Index(f,54) = sig.ftotalbins(1,2);
                data.Index(f,55) = sig.ftotalbins(1,3);
                data.Index(f,56) = sig.ftotalbins(1,4);
            end
            
            data.numSets = nfiles;
            n = nptdata(data.numSets,0,pwd);
            d.data = data;
            obj = class(d,Args.classname,n);
            if(Args.SaveLevels)
                fprintf('Saving %s object...\n',Args.classname);
                eval([Args.matvarname ' = obj;']);
                % save object
                eval(['save ' Args.matname ' ' Args.matvarname]);
            end
            %         end
        end
    else
        fprintf('hilbert entropy files missing\n');
        obj = createEmptyObject(Args);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
