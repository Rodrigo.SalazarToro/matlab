function obj = plot(obj,varargin)

%Arguments
Args = struct('ml',0,'location_rule',0,'fixtrial',0,'incorrect',0,'locations',0,'ides',0,'category',[],'no_bias',0,'std',0,'plv_correlation',0);
Args.flags = {'ml','location_rule','fixtrial','incorrect','locations','ides','no_bias','std','plv_correlation'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end

%load cohgram info
load(obj.data.setNames{ind})
if ~Args.plv_correlation
    if ~Args.no_bias
        %loac surrogate and bias correction
        load([obj.data.setNames{ind}(1:end-22) 'threshsurrogate_' obj.data.setNames{ind}(end-21:end)])
    end
    
    sday = strfind(obj.data.setNames{ind},'/');
    ns = size(sday,2);
    day = obj.data.setNames{ind}(sday(ns-4)+1:sday(ns-3)-1);
    
    area1 = reverse_categorizeNeuronalHist(obj.data.Index(ind,3));
    area2 = reverse_categorizeNeuronalHist(obj.data.Index(ind,4));
    
    
    
    %plot the average hilbert entropy values
    subplot(2,1,1)
    
    
    if isempty(Args.category)
        
        if Args.location_rule
            hentropy = hilbertentropy{2};
            mphases = mean_phases{2};
        elseif Args.fixtrial
            hentropy = hilbertentropy{3};
            mphases = mean_phases{3};
        elseif Args.incorrect
            hentropy = hilbertentropy{4};
            mphases = mean_phases{4};
        else
            hentropy = hilbertentropy{1};
            mphases = mean_phases{1};
        end
    else
        
        hentropy = hilbertentropy{Args.category};
        mphases = mean_phases{Args.category};
    end
    
    if ~Args.std
        for x = 1:16 %run through all the frequencies
            allhe(x,:) = mean(squeeze(hentropy(x,1:36,:)),2);
        end
    else
        for x = 1:16 %run through all the frequencies
            allhe(x,:) = std(squeeze(hentropy(x,1:36,:)),0,2);
        end
    end
    
    if ~Args.no_bias
        %subtract the entropy bias
        allhe = allhe - all_bias{1};
    end
    
    
    imagesc(interp2(flipud(allhe)));
% % %     mph = squeeze(mphases(6,1:36,:));mmph = [];for x = 1:36;mmph = [mmph;mph(:,x)];end;hist(mmph,[-pi:.1:pi])
    
    set(gca,'XTick',[1:4:72])
    set(gca,'XTickLabel',steps([1:2:36]))
    
    set(gca,'YTick',[1:2:(16*2)])
    set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
    %sample on
    hold on
    plot([15, 15],[0 33],'color','k')
    %sample off
    hold on
    plot([35, 35],[0 33],'color','k')
    %earliest match
    hold on
    plot([67, 67],[0 33],'color','k')
    hold on
    colorbar
    % if max(max(allhe)) < .1
    %     set(gca, 'CLim', [0 .1]);
    % end
    % set(gca, 'CLim', [0 .15]);
    
    title( [day '     groups: ' , num2str(obj.data.Index(ind,1)) ' vs ' num2str(obj.data.Index(ind,2)), '   areas: ',area1{1}, ' and ' ,area2{1}])
    
    
    % subplot(3,1,2)
    % for x = 1:16 %run through all the frequencies
    %     allhe(x,:) = std(squeeze(hentropy(x,1:36,:)),0,2);
    % end
    % imagesc(interp2(flipud(allhe)));
    %
    % set(gca,'XTick',[1:4:72])
    % set(gca,'XTickLabel',steps([1:2:36]))
    % 3
    % set(gca,'YTick',[1:2:(16*2)])
    % set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
    % %sample on
    % hold on
    % plot([17, 17],[0 33],'color','k')
    % %sample off
    % hold on
    % plot([37, 37],[0 33],'color','k')
    % %earliest match
    % hold on
    % plot([69, 69],[0 33],'color','k')
    % hold on
    % colorbar
    % if max(max(allhe)) < .15
    %     set(gca, 'CLim', [0 .15]);
    % end
    
    
    
    subplot(2,1,2);
    allph = [];
    
    if ~Args.std
        for x = 1:16
            allph(x,:) = circ_mean((squeeze(mphases(x,1:36,:))'));
        end
        
    else
        for x = 1:16
            
            allph(x,:) = circ_std((squeeze(mphases(x,1:36,:))'));
        end
    end
    imagesc(interp2(flipud(allph)).*(180/pi));
    
    
    set(gca,'XTick',[1:4:72])
    set(gca,'XTickLabel',steps([1:2:36]))
    
    set(gca,'YTick',[1:2:32])
    set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
    %sample on
    hold on
    plot([15, 15],[0 33],'color','k')
    %sample off
    hold on
    plot([35, 35],[0 33],'color','k')
    % %earliest match
    hold on
    plot([67, 67],[0 33],'color','k')
    % hold off
    
    % set(gca, 'CLim', [-180 180]);
    % set(gca, 'CLim', [-.25*(180/pi) .25*(180/pi)]);
    colorbar
    
    %figure;for xx = 1:754;imagesc(interp2(flipud(hentropy(:,1:36,xx))));hold on;colorbar;pause;end
    
    %figure;for xx = 1:754;imagesc(interp2(flipud(mphases(:,1:36,xx))));hold on;colorbar;pause;end
    
    % figure;hist(squeeze(hentropy(6,17,:)),[0:.01:1])
    x;
    
    %  m = [];figure;c = 0;for x = [1:2:36];c = c+1;subplot(3,6,c);hist(squeeze(hentropy(6,x,:)),[0:.1:1]);m(c) = median(squeeze(hentropy(6,x,:)));axis tight;end
    
    
    
else
    
    hentropy = hilbertentropy{1};
    
    %     for c = 9%5:13
    % hentropy = hilbertentropy{c};
    
    
    
    f = 6; %frequency(20Hz,6)
    t = 30;
    cutoff = 0.90 ^2;
    
    % figure;plot(squeeze(hentropy(f,t,:)))
    %
    % figure;for x = 1:36;subplot(6,6,x);scatter(squeeze(hentropy(6,x,:)),squeeze(hentropy(6,25,:)));lsline;end
    %
    %     figure;corrs = [];for x = 1:36;corrs(x) = xcorr(squeeze(hentropy(6,x,:)),squeeze(hentropy(6,25,:)),0,'coef');end;plot(corrs)
    
    
    
    %show time and frequency correlation
    figure
    counter = 0;
    allmin = 1;
    for allx = 1:2:36
        counter = counter + 1;
        for x = 1:36;
            for y = 1:16
                allcorrs{counter}(y,x) = xcorr(squeeze(hentropy(f,x,:)),squeeze(hentropy(y,allx,:)),0,'coef') ^2;
                
                %             run 100 surrogates
                %                                 surr = [];
                %                                 for p = 1:100
                %                                     h1 = squeeze(hentropy(f,x,:))';
                %                                     h2 = squeeze(hentropy(y,allx,:))';
                %                                     p1 = randperm(size(h1,2));
                %                                     p2 = randperm(size(h2,2));
                %                                     surr(p) = xcorr(h1(p1),h2(p2),0,'coef') ^2;
                %                                 end
                %                                 if allcorrs{counter}(y,x) > prctile(surr,99.9)
                %                                     allcorrsthresh{counter}(y,x) = 1;
                %                                 else
                %                                     allcorrsthresh{counter}(y,x) = 0;
                %                                 end
                
            end
        end
        m = min(min(allcorrs{counter}));
        if m < allmin
            allmin = m;
        end
    end
    for c = 1 : counter
        subplot(3,6,c)
        corrs = allcorrs{c};
        corrs(find(corrs > cutoff)) = nan;
        
        imagesc((flipud(corrs)))
        set(gca,'XTick',[1:2:36])
        
        set(gca,'XTickLabel',steps([1:2:36]))
        
        set(gca,'YTick',[1:16])
        set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
        %sample on
        hold on
        plot([8, 8],[0 33],'color','k')
        %sample off
        hold on
        plot([18, 18],[0 33],'color','k')
        %earliest match
        hold on
        plot([34, 34],[0 33],'color','k')%         figure
        %         for c = 1 : counter
        %             subplot(3,6,c)
        %             corrs = allcorrsthresh{c};
        %
        %             imagesc((flipud(corrs)))
        %             set(gca,'XTick',[1:2:36])
        %
        %             set(gca,'XTickLabel',steps([1:2:36]))
        %
        %             set(gca,'YTick',[1:16])
        %             set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
        %             %sample on
        %             hold on
        %             plot([8, 8],[0 33],'color','k')
        %             %sample off
        %             hold on
        %             plot([18, 18],[0 33],'color','k')
        %             %earliest match
        %             hold on
        %             plot([34, 34],[0 33],'color','k')
        %             hold on
        %
        %         end
        
        hold on
        
        set(gca, 'CLim', [allmin cutoff+.01]);
        
        allsums(c) = sum(sum(allcorrs{c}));
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
end

