function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'rule',[],'cortex',[],'unit',[]);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    
    if ~isempty(Args.day)
        rtemp1 = find(obj.data.Index(:,1) == Args.day);
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.rule)
        rtemp2 = find(obj.data.Index(:,2) == Args.rule);
        else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.cortex)
        rtemp3 = find(obj.data.Index(:,3) == Args.cortex);
        else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.unit)
        rtemp4 = find(obj.data.Index(:,4) == Args.unit);
        else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,rtemp4)));
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});

end

