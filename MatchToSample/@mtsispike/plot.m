function obj = plot(obj,varargin)
%   @mtslfptraces/plot plots by default the first raw lfp trace
%   Other option are :
%
% - spectrogram: plots a spectrogram from the trace. By default,
%   it uses the multi taper method. If this option is followed
%   by 'fft', it will use the traditional fast Fourier instead.
%   The parameters 'window' (512 ms),'noverlap'(502 ms),'nfft' (512),
%   'NW' (2),'k' (3) can be specified. Defaults are shown in parentheses.
%   Another option is 'normalized', which will transform the
%   spectrogram in a Z-score (the mean and standard deviation are
%   calculated from a period that can be specified by the arguments
%   'bstart' and 'bend', which are by default 1 and 300 ms, respectively).
%   Finally, the colorscale (default [-50 50 ) can be specified with
%   'colorscale'.
%
% - TrialTraces: plots all the traces of the session. This option
%   should be followed by a selection criteria (see the get fct).
%
%
% - 'SacAlign' will align all the data according to the onset of the
%   saccade and can be combined with the arguments above.
%
%   Dpendencies: alignData, spectrogram, mtspecgramc
%


Args = struct();
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    
    limit = dataindices(n,3);
    
else
    limit = dataindices(:,3);
end


cd(obj.data.setNames{limit})

load('ispikes.mat');

cdir = getDataDirs('session','Relative');
cd(cdir)


mts = mtstrial('auto');

 plotMTSpsth(mts,sp,varargin2{:},'axis','plot')
