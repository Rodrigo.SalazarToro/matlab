function obj = mtsispike(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'rule',1);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsispike';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtsisp';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
            
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

mtst = mtstrial('auto',varargin{:});
groups = nptDir('group*');
sdir = pwd;
clark = findstr(sdir,'clark');
betty = findstr(sdir,'betty');
acuteB = findstr(sdir,'acute');
count = 1 ;
if  ~isempty(groups) && ~isempty(mtst)
    if ((~isempty(acuteB) || ~isempty(clark)) && Args.rule == mtst.data.Index(1,1)) || (isempty(acuteB) || isempty(clark))
    
    data.Index = [];
    data.phase = [];
    data.period = [];
    for g = 1 : size(groups,1)
        cd(groups(g).name)
        gdir = pwd;
        clusters = nptDir('cluster*');
        for c = 1 : size(clusters,1)
            cd(clusters(c).name)
            spfile = nptDir('ispikes.mat');
            if ~isempty(spfile)
                
                load(spfile.name)
                %                 if clusters(c).name(end) == 'm'
                %
                %                     data.unit(count,1) = 1; % 1 for multi unit
                %                 else
                %                     data.unit(count,1) = 0; % for single unit
                %                 end
                data.unit(count,1) = clusters(c).name(end);
                if ~isempty(clark) || ~isempty(acuteB)
                    if str2num(groups(g).name(end-3:end)) <= 8
                        data.Index(count,1) = 'P';
                    else
                        data.Index(count,1) = 'F';
                    end
                    
                elseif ~isempty(betty)
                    if str2num(groups(g).name(end-3:end)) <= 32
                        data.Index(count,1) = 'F';
                    else
                        data.Index(count,1) = 'P';
                    end
                    
                end
                
                
                [phase,period] = mtsStatPSTH(sp,mtst,varargin{:});
                data.phase = [data.phase; phase];
                data.period = [data.period; period];
                
%                 data.Index = [data.Index; [1 1 count]];
                data.Index = [data.Index; [count]];
                data.setNames{count} = pwd;
                count = count + 1;
                cd(gdir)
            end
        end %c = 1 : size(clusters,1)
        cd(sdir)
    end %g = 1 : size(group,1)
    
    end
    data.numSets = count-1; % nbr of trial
    
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
    
else
    % create empty object
    fprintf('The mtstrial object is empty or the group directory does not exist \n');
    obj = createEmptyObject(Args);
    
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];
data.unit = [];
data.phase = [];
data.period = [];
data.cortex = [];
% data.group = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
