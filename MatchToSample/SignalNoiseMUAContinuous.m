function SNR = SignalNoiseMUAContinuous(varargin)

%       Run at session level
%
%           Arguments:
%
%           FileName     -  'FileName.bin'
%           channel      -  [channel indices]
%           std          -  indicates the standard deviation to be used
%           chunk_points -  index of chunk data points to use
%
%        SNR = range(Signal)/range(Noise)

% Get Input Arguments
Args = struct('FileName',[],'channel',[],'std',[],'chunk_points',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

trial = 0;
if isempty(Args.chunk_points)
    trial = 1; %indicates that the data is trial based
end

if trial
    filename = Args.FileName;
    channel = Args.channel;
    num_chunks = size(filename,1); %this is the number of trials
else
    %number of chunks
    num_chunks = size(Args.chunk_points,1);
    filename = Args.FileName;
    channel = Args.channel;
    chunk_points = Args.chunk_points;
end

running_mean = 0;
%calculate mean
m_points = 0;
for c = 1 : num_chunks
    if trial
        d = nptReadStreamerFile(filename(c).name);
        data = d(channel,:);
    else
        d = nptReadStreamerFileChunk(filename,chunk_points(c,:));
        data = d(channel,:);
    end
    running_mean = running_mean + sum(data);
    m_points = m_points + size(data,2);
end
r_mean = running_mean / m_points;

sq_error = 0;
%calculate std
m_points = 0;
for c = 1 : num_chunks
    %formula sqrt(sum((y-mean(y)).^2) / (size(y,2)-1))
    if trial
        d = nptReadStreamerFile(filename(c).name);
        data = d(channel,:);
    else
        d = nptReadStreamerFileChunk(filename,chunk_points(c,:));
        data = d(channel,:);
    end
    sq_error = sq_error + sum((data - r_mean).^2);
    m_points = m_points + size(data,2);
end
r_std = sqrt( sq_error / (m_points - 1) );

computing = 1;
while computing
    %loop through chunks to re-calculate mean and std after removing the signal
    running_mean = 0;
    m_points = 0;
    for c = 1 : num_chunks
        ind = [];
        if trial
            d = nptReadStreamerFile(filename(c).name);
            data = d(channel,:);
        else
            d = nptReadStreamerFileChunk(filename,chunk_points(c,:));
            data = d(channel,:);
        end
        [ind] = find(data > (r_mean+Args.std*r_std) | data < (r_mean-Args.std*r_std));
        if ~isempty(ind)
            data(ind) = [];
        end
        running_mean = running_mean + sum(data);
        m_points = m_points + size(data,2);
    end
    
    %new mean
    r_mean = running_mean / m_points;
    
    sq_error = 0;
    s_points = 0;
    %calculate std
    for c = 1 : num_chunks
        ind = [];
        %formula sqrt(sum((y-mean(y)).^2) / (size(y,2)-1))
        if trial
            d = nptReadStreamerFile(filename(c).name);
            data = d(channel,:);
        else
            d = nptReadStreamerFileChunk(filename,chunk_points(c,:));
            data = d(channel,:);
        end
        [ind] = find(data > (r_mean+Args.std*r_std) | data < (r_mean-Args.std*r_std));
        if ~isempty(ind)
            data(ind) = [];
        end
        sq_error = sq_error + sum((data - r_mean).^2);
        s_points = s_points + size(data,2);
    end
    
    old_std = r_std;
    
    %new std
    r_std = sqrt( sq_error / (s_points - 1) );
    new_std = r_std;
    if new_std == old_std
        computing = 0;
    end
end

%calculate signal
signal_bins = [];
for c = 1 : num_chunks
    ind = [];
    %formula sqrt(sum((y-mean(y)).^2) / (size(y,2)-1))
    if trial
        d = nptReadStreamerFile(filename(c).name);
        data = d(channel,:);
    else
        d = nptReadStreamerFileChunk(filename,chunk_points(c,:));
        data = d(channel,:);
    end
    [ind] = find(data > (r_mean+Args.std*r_std) | data < (r_mean-Args.std*r_std));
    if ~isempty(ind)
        signal = data(ind);
        %round signal valuse to 10th place
        %signal = roundn(signal,1);
        max_abs = max(abs(signal));
        [sb xout] = hist(signal,(max_abs * -1):1:max_abs);
        if isempty(signal_bins)
            signal_bins = sb;
            centers = xout;
        else
            bin_length = (size(signal_bins,2) - 1) / 2;
            sb_length = (size(sb,2) - 1) / 2;
            l_diff = (bin_length - sb_length);
            if l_diff > 0
                sb = padarray(sb,[0 l_diff],'both');
            else
                l_diff = abs(l_diff);
                signal_bins = padarray(signal_bins,[0 l_diff],'both');
                centers = xout;
            end
            signal_bins = signal_bins + sb;
        end
    end
end

%%% Calculate the Signal Values %%%%
center = find(centers == 0);

left_tail = signal_bins(1,(1:(center -1)));
right_tail = signal_bins(1,((center+1) : end));

left_values = centers(1,(1:(center -1)));
right_values = centers(1,((center+1) : end));

%calucate left 99th percentile
%flip distribution so that always going toward tail
l_index = 0;
left_tail = flipdim(left_tail,2);
left_values = flipdim(left_values,2);
for lp = 1 : size(left_tail,2);
    l_points = round(sum(left_tail) * .99);
    l_index = l_index + left_tail(lp);
    if l_index >= l_points
        left_prct = lp;
        break
    end
end

%calculate right 99th percentile
r_index = 0;
for rp = 1 : size(right_tail,2);
    r_points = round(sum(right_tail) * .99);
    r_index = r_index + right_tail(rp);
    if r_index >= r_points;
        right_prct = rp;
        break
    end
end

negative_signal = left_values(left_prct);
positive_signal = right_values(right_prct);

signal_range = range([positive_signal negative_signal]);

%%% Calculate the Noise Values %%%%
positive_noise = r_mean + (Args.std * r_std);
negative_noise = r_mean - (Args.std * r_std);

noise_range = range([positive_noise negative_noise]);

%%% Calculate the SNR Values %%%%
SNR = signal_range / noise_range;



