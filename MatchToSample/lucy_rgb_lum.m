function lucy_rgb_lum

red = [0.3400000,0.3700000,0.4500000,0.5600000,0.7000000,0.9300000,1.200000,1.590000,2,2.570000,3.130000,3.730000,4.380000,4.950000,5.700000,6.400000,7.200000,7.960000,8.800000,9.710000,10.72000,11.63000,12.60000,13.60000,14.75000];

green = [0.3400000,0.3700000,0.5400000,0.8500000,1.310000,1.960000,2.870000,3.960000,5.340000,7.010000,8.850000,10.56000,12.40000,14.30000,16.41000,18.66000,21.06000,23.60000,26.20000,28.90000,31.70000,34.70000,37.65000,40.93000,44.14000];

blue = [0.3500000,0.3600000,0.4000000,0.4400000,0.5100000,0.5900000,0.6800000,0.8300000,0.9600000,1.110000,1.290000,1.460000,1.630000,1.820000,2.010000,2.200000,2.420000,2.700000,2.960000,3.230000,3.540000,3.810000,4.110000,4.410000,4.750000];


%fit each curve with a cubic polynomial
x = 1:25;
xx = [1:.1:25];
xxx = linspace(15,255,size(xx,2));
pr = polyfit(x,red,3);
redvals = polyval(pr,xx);
% red_fx = inline('(pr(1)*x^3) + (pr(2)*x^2) + (pr(3)*x) + pr(4)','pr','x');

pg = polyfit(x,green,3);
greenvals = polyval(pg,xx);
% green_fx = inline('(pg(1)*x^3) + (pg(2)*x^2) + (pg(3)*x) + pg(4)','pg','x');

pb = polyfit(x,blue,3);
bluevals = polyval(pb,xx);
% blue_fx = inline('(pb(1)*x^3) + (pb(2)*x^2) + (pb(3)*x) + pb(4)','pb','x');


%the max color is blue 255, so find percentages that correspond to the max blue
maxlum = max(bluevals);
all_lums = [40:10:100];
inc_lums = (all_lums ./100) * maxlum;
nlums = [1 : size(inc_lums,2)];

%load shapes
cd('~/Desktop/lucy_rgb_lum')
circle = imread('circle.jpg');
triangle = imread('triangle.jpg');
rectangle = imread('rectangle.jpg');


%make each shape three colors and multiple luminances
shapes = {'circle.jpg','triangle.jpg','rectangle.jpg'};
colors = {'red','green','blue'};

for c = 1 : 3
    for x = nlums
        for sh = 1:3
            shape = imread(shapes{sh});
            
            for ss = 1:3
                sf = shape(:,:,ss);
                sf(find(sf < 5)) = 0;
                sf(find(sf > 5)) = 254;
                shape(:,:,ss) = sf;
            end
            
            if c == 1
                [~,ii] = min(abs(redvals - inc_lums(x)));
                rval = xxx(ii);
                
                s = shape(:,:,1);
                s(find(s < 254)) = rval;
                
                shape(:,:,1) = s;
            elseif c == 2
                [~,ii] = min(abs(greenvals - inc_lums(x)));
                gval = xxx(ii);
                
                s = shape(:,:,2);
                s(find(s < 254)) = gval;
                
                shape(:,:,2) = s;
            else
                [~,ii] = min(abs(bluevals - inc_lums(x)));
                bval = xxx(ii);
                
                s = shape(:,:,3);
                s(find(s < 254)) = bval;
                
                shape(:,:,3) = s;
            end
            
            for ss = 1:3
                sf = shape(:,:,ss);
                sf(find(sf == 254)) = 0;
                shape(:,:,ss) = sf;
            end
            
            imwrite(shape,[shapes{sh}(1:end-4) '_' colors{c} '_' num2str(all_lums(x)) '.jpg'])
        end
    end
end


%make custom colors
%red
[~,ii] = min(abs(redvals - (4.7428/3)));
rval = xxx(ii)

%green
[~,ii] = min(abs(greenvals - (4.7428/3)));
gval = xxx(ii)

%blue
[~,ii] = min(abs(bluevals - (4.7428/3)));
bval = xxx(ii)

















