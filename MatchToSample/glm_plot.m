function glm_plot(c1,c2,chnumb,glm_output,plotpercents)




time(:,1) = [1:17]';

colors = ['mcrgbk'];

%plot logits with interaction model
% % % % figure
% % % % 
% % % % plotp = plotpercents(:,(18:34));
% % % % for z=1:17
% % % %     elogits(:,z) = log(plotp(:,z)./(1 - plotp(:,z)));
% % % % end
% % % % 

hold on
cell_coefs = glm_output.interaction_coefs;
for all_coefs = 1:size(cell_coefs,1)
    coefs(all_coefs,1) = str2double(cell_coefs(all_coefs));
end

res = [];
res(:,1) = coefs(1) + time*coefs(2);
res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);

%plot log odds
% % % % % for zz = 1:9
% % % % %     subplot(3,3,zz);
% % % % %     if zz < 7
% % % % %         plot(elogits(zz,:),colors(zz))
% % % % %     elseif zz == 7
% % % % %         plot(elogits(zz,:),'Color',[0 .5 1])
% % % % %     elseif zz == 8
% % % % %         plot(elogits(zz,:),'Color',[.5 1 .5])
% % % % %     elseif zz == 9
% % % % %         plot(elogits(zz,:),'Color',[.5 .5 0])
% % % % %     end
% % % % %     hold on
% % % % % end
% % % % % 
% % % % % 
% % % % % 
% % % % % hold on
% % % % % for r = 1 : 9
% % % % %     subplot(3,3,r)
% % % % %     axis([1 17 -4 4])
% % % % %     if r < 7
% % % % %         plot(res(:,r),colors(r));
% % % % %     elseif r == 7
% % % % %         plot(res(:,r),'Color',[0 .5 1]);
% % % % %     elseif r == 8
% % % % %         plot(res(:,r),'Color',[.5 1 .5]);
% % % % %     elseif r == 9
% % % % %         plot(res(:,r),'Color',[.5 .5 0]);
% % % % %     end
% % % % %     hold on
% % % % % end


%probability
% % % figure

% % % for zz = 1:9
% % %     subplot(3,3,zz);
% % %     
% % %     if zz < 7
% % %         plot(plotp(zz,:),colors(zz))
% % %     elseif zz == 7
% % %         plot(plotp(zz,:),'Color',[0 .5 1])
% % %     elseif zz == 8
% % %         plot(plotp(zz,:),'Color',[.5 1 .5])
% % %     elseif zz == 9
% % %         plot(plotp(zz,:),'Color',[.5 .5 0])
% % %     end
% % %     axis([1 18 0 1])
% % %     hold on
% % % end


res = 1 ./ (1 + exp(-res));



% % % 
% % % hold on
% % % for r = 1 : 9
% % %     subplot(3,3,r)
% % %     axis([1 17 0 1])
% % %     if r < 7
% % %         plot(res(:,r),colors(r));
% % %     elseif r == 7
% % %         plot(res(:,r),'Color',[0 .5 1]);
% % %     elseif r == 8
% % %         plot(res(:,r),'Color',[.5 1 .5]);
% % %     elseif r == 9
% % %         plot(res(:,r),'Color',[.5 .5 0]);
% % %     end
% % %     hold on
% % % end
% % % 
% % % linkaxes([subplot(3,3,1),subplot(3,3,2),subplot(3,3,3),subplot(3,3,4),subplot(3,3,5),subplot(3,3,6),subplot(3,3,7),subplot(3,3,8),subplot(3,3,9)],'x')

%figure;plot(std(res))

figure; for x = 1:9;plot(res(:,x));hold on;end

