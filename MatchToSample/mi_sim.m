function mi_sim

all_mi_std = [];
counter = 0;
for s = [.01:.005:.1]
    counter = counter + 1;
    all_mi = [];
    for perms = 1 : 1000
        
        %make 3 random means between 0 and 1
        for x = 1:3
            means(x) = rand;
        end
        
        cumulative_dist = zeros(1,101);
        for x = 1:3
            cond_dist(x,:) = normpdf([0:.01:1],means(x),s);
            
            cond_dist(x,:) = cond_dist(x,:) ./ sum(cond_dist(x,:)); %make prob dist
            
            cumulative_dist = cumulative_dist + (cond_dist(x,:) * 1/3);
            
            %         subplot(1,2,1)
            %         hold on
            %         plot([0:.1:1],cond_dist(x,:))
        end
        
        for x = 1 : 3
            
            post_dist(x,:) = (cond_dist(x,:) * (1/3)) ./ cumulative_dist;
            mi(x) = nansum(cumulative_dist .* post_dist(x,:) .* log2(post_dist(x,:) ./ (1/3)));
        end
        
        all_mi(perms) = sum(mi);
    end
    all_mi_std(counter,:) = all_mi;
end

all_mi_std;
plot([.01:.005:.1],median(all_mi_std'))
hold on;
plot([.01:.005:.1],prctile(all_mi_std',95),'r')
hold on
plot([.01:.005:.1],prctile(all_mi_std',5),'r')

mm = median(all_mi_std');
mp = prctile(all_mi_std',95);

%introduce real specificity at specific std's
all_mi_std = [];
counter = 0;

s = .09;



for allm = [.05:.01:.4]
    counter = counter + 1;
    
    %make 3 random means between 0 and 1
    means(1) = .5 - allm;
    means(2) = .5;
    means(3) = .5 + allm;
    
    cumulative_dist = zeros(1,101);
    for x = 1:3
        cond_dist(x,:) = normpdf([0:.01:1],means(x),s);
        
        cond_dist(x,:) = cond_dist(x,:) ./ sum(cond_dist(x,:)); %make prob dist
        
        cumulative_dist = cumulative_dist + (cond_dist(x,:) * 1/3);
        
        %         subplot(1,2,1)
        %         hold on
        %         plot([0:.1:1],cond_dist(x,:))
    end
    
    for x = 1 : 3
        
        post_dist(x,:) = (cond_dist(x,:) * (1/3)) ./ cumulative_dist;
        mi(x) = nansum(cumulative_dist .* post_dist(x,:) .* log2(post_dist(x,:) ./ (1/3)));
    end
    
    all_mi_std(counter,:) = sum(mi);
    
end
all_mi_std;
figure
plot([.05:.01:.4],all_mi_std')
hold on
% plot([.05:.01:.4],ones(1,size([.05:.01:.4],2))*mp(9),'r')



