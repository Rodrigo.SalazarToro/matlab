function phase_paper_single_trial_corr_vs_stim(varargin)


Args = struct(); %xcthresh = unimodal_thresh_corrcoef
Args.flags = {};
Args = getOptArgs(varargin,Args);


pcutoff = .01; 
th = .05;

cd('/Volumes/raid/data/monkey/clark/')
% load idedays
% cpp_b = processDays(mtscpp2,'days',idedays,'NoSites')
% save cpp_c cpp_c
load cpp_c


%% frontal
[ii iii] = get(cpp_c,'Number','in_phase','ff','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

ffinr = [];
ffinp = [];
pcounter = 0;
for x = iii
    pcounter = pcounter + 1;
    cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
    load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
    
    
    if exp(median(log(resp_ent(4,:)))) > th
        for xx = 1:4
            
            %find max loc
            lo = unique(locs);
            alllocs = [];
            stimcounter = 0;
            for ll = 1 : 3
                stimcounter = stimcounter + 1;
                alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
            end
            
            [~,ii] = max(alllocs);

            ffinp(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
        end
    else
        
        pcounter = pcounter -1;
    end
end


[ii iii] = get(cpp_c,'Number','anti_phase','ff','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

ffantir = [];
ffantip = [];
pcounter = 0;
if ii ~= 0
    for x = iii
        pcounter = pcounter + 1;
        cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
        load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
        if exp(median(log(resp_ent(4,:)))) > th
            for xx = 1:4
                
                %find max loc
                lo = unique(locs);
                alllocs = [];
                stimcounter = 0;
                for ll = 1 : 3
                    stimcounter = stimcounter + 1;
                    alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
                end
                
                [~,ii] = max(alllocs);
                
                ffantip(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
            end
        else
            
            pcounter = pcounter -1;
        end
    end
end



%% fronto-parietal
[ii iii] = get(cpp_c,'Number','in_phase','pf','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

pfinr = [];
pfinp = [];
pcounter = 0;
for x = iii
    pcounter = pcounter + 1;
    cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
    load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
    
    if exp(median(log(resp_ent(4,:)))) > th
        for xx = 1:4
            
            %find max loc
            lo = unique(locs);
            alllocs = [];
            stimcounter = 0;
            for ll = 1 : 3
                stimcounter = stimcounter + 1;
                alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
            end
            
            [~,ii] = max(alllocs);
            
            pfinp(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
        end
    else
        
        pcounter = pcounter -1;
    end
end


[ii iii] = get(cpp_c,'Number','anti_phase','pf','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

pfantir = [];
pfantip = [];
pcounter = 0;
for x = iii
    pcounter = pcounter + 1;
    cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
    load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
    
    if exp(median(log(resp_ent(4,:)))) > th
        for xx = 1:4
            
            %find max loc
            lo = unique(locs);
            alllocs = [];
            stimcounter = 0;
            for ll = 1 : 3
                stimcounter = stimcounter + 1;
                alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
            end
            
            [~,ii] = max(alllocs);
            
            pfantip(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
        end
    else
        
        pcounter = pcounter -1;
    end
end



%% parietal
[ii iii] = get(cpp_c,'Number','in_phase','pp','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

ppinr = [];
ppinp = [];
pcounter = 0;
for x = iii
    pcounter = pcounter + 1;
    cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
    load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
    
    if exp(median(log(resp_ent(4,:)))) > th
        for xx = 1:4
            
            %find max loc
            lo = unique(locs);
            alllocs = [];
            stimcounter = 0;
            for ll = 1 : 3
                stimcounter = stimcounter + 1;
                alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
            end
            
            [~,ii] = max(alllocs);
            
            ppinp(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
        end
    else
        
        pcounter = pcounter -1;
    end
end


[ii iii] = get(cpp_c,'Number','anti_phase','pp','hsfpp_criteria','corr_mag_criteria','cross_corr',.99);

ppantir = [];
ppantip = [];
pcounter = 0;
for x = iii
    pcounter = pcounter + 1;
    cd(['/Volumes' cpp_c.data.setNames{x}(7:end-20)])
    load(['single_trial_plv_stim' num2strpad(cpp_c.data.Index(x,23),3) num2strpad(cpp_c.data.Index(x,24),3)])
    
    if exp(median(log(resp_ent(4,:)))) > th
        for xx = 1:4
            
            %find max loc
            lo = unique(locs);
            alllocs = [];
            stimcounter = 0;
            for ll = 1 : 3
                stimcounter = stimcounter + 1;
                alllocs(stimcounter) = median(log(resp_ent(4,(find(single(locs) == lo(ll))))));
            end
            
            [~,ii] = max(alllocs);
            
            ppantip(xx,pcounter) = kruskalwallis(log(resp_ent(xx,find(single(locs) == lo(ii)))),stims(find(single(locs) == lo(ii))),'off');
        end
    else
        
        pcounter = pcounter -1;
    end
end



figure
for xp = 1:4
    subplot(1,4,xp)
    
    percentsig = [];
    
    percentsig(1) = sum((ffinp(xp,:))<pcutoff) / size(ffinp,2) * 100;
    % percentsig(2) = size(find(ffantip(xp,:)>rcutoff),1) / size(ffantip,2) * 100;
    percentsig(3) = sum((pfinp(xp,:))<pcutoff) / size(pfinp,2) * 100;
    percentsig(4) = sum((pfantip(xp,:))<pcutoff) / size(pfantip,2) * 100;
    percentsig(5) = sum((ppinp(xp,:))<pcutoff) / size(ppinp,2) * 100;
    percentsig(6) = sum((ppantip(xp,:))<pcutoff) / size(ppantip,2) * 100;
    
    bar(percentsig); axis([0 7 0 100])
    set(gca,'XTickLabel',{'FF IN', 'FF ANTI', 'PF IN', 'PF ANTI', 'PP IN', 'PP ANTI'})
    hold on
    
end



