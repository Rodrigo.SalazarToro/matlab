function make_psth_sig_plot

cd('/media/bmf_raid/data/monkey/ethyl/')

load allpsth

%get the desired psth's
[~,allpsths] = get(allpsth,'Number','correct','multi');

% cd('/media/bmf_raid/data/monkey/ethyl/110811/session01')
% 
% load allpsth
binsize = allpsth.data.Index(1,6);

thresh = .001;
binx = [-500+(binsize/2):binsize:1500-(binsize/2)];
nbins = size(binx,2);
bintime = 2000 / nbins;

%run through selected groups and make a summary figure for signigificant
%differences from the interleaved fixation trials
allcrossings = [];
allresp = [];
abovebin = zeros(1,nbins);
belowbin = zeros(1,nbins);
for x = allpsths
    
    load(allpsth.data.setNames{x},'correct_fixation_resp','correct_fixation_pvals','numnc')
    cluster = allpsth.data.Index(x,4);
    
    pvals = correct_fixation_pvals{cluster};
    
    resp = correct_fixation_resp(cluster,:);
    
    ii = find(pvals <= thresh);
    allcrossings = [allcrossings ii];
    
    abovebin(ii(find(resp(ii) == 1))) = abovebin(ii(find(resp(ii) == 1))) + 1;
    belowbin(ii(find(resp(ii) == -1))) = belowbin(ii(find(resp(ii) == -1))) + 1;
    
    allresp = [allresp resp(ii)];
end
figure
subplot(3,1,1)
h = hist(allcrossings,[1:nbins]);
bar(h,'hist'); hold on
nticks = size([1:4:nbins],2);
set(gca,'XTick',[1:4:nbins])
bintime = 2000 / nbins;
set(gca,'XTickLabel',[-500+(bintime/2):2000/nticks:1500-(binsize/2)])

plot([(500/bintime) + .5, (500/bintime) + .5],[0 100],'color','r'); hold on %sample on
plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 100],'color','r'); hold on %sample off
plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 100],'color','r'); hold on %earliest match
title('significant bins (p < .001)')
axis tight
xlabel('time (ms)')
ylabel('count')
text(30,80,'n = 817')
text(4,80,'presample')
text(15,80,'sample')
text(25,80,'delay')

%make plot to indicate if the significant bins were above or below the
%baseline
subplot(3,1,2)
bar(abovebin,'hist'); hold on
bar(belowbin*-1,'hist'); hold on
nticks = size([1:4:nbins],2);
set(gca,'XTick',[1:4:nbins])
bintime = 2000 / nbins;
set(gca,'XTickLabel',[-500+(bintime/2):2000/nticks:1500-(binsize/2)])

plot([(500/bintime) + .5, (500/bintime) + .5],[-50 100],'color','r'); hold on %sample on
plot([(1000/bintime) + .5, (1000/bintime) + .5],[-50 100],'color','r'); hold on %sample off
plot([(1800/bintime) + .5, (1800/bintime) + .5],[-50 100],'color','r'); hold on %earliest match
title('orientation of significant bins')
xlabel('time (ms)')
ylabel('count')
axis tight


%make plot for stimulus specific activity
allcrossingside = [];
for x = allpsths
    
    load(allpsth.data.setNames{x})
    cluster = allpsth.data.Index(x,4);
    pvals = ide_pvals{cluster};
    
    ii = find(pvals <= thresh);
    allcrossingside = [allcrossingside ii];
end
subplot(3,1,3)
h = hist(allcrossingside,[1:nbins]);
bar(h,'hist'); hold on
nticks = size([1:4:nbins],2);
set(gca,'XTick',[1:4:nbins])
bintime = 2000 / nbins;
set(gca,'XTickLabel',[-500+(bintime/2):2000/nticks:1500-(binsize/2)])

plot([(500/bintime) + .5, (500/bintime) + .5],[0 60],'color','r'); hold on %sample on
plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 60],'color','r'); hold on %sample off
plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 60],'color','r'); hold on %earliest match
title('stimulus specificity')
hold on
xlabel('time (ms)')
ylabel('count')
axis tight





%NOW ADD ON THE SINGLE UNITS



% 
% 
% 
% 
% figure
% %get the desired psth's
% [~,allpsths] = get(allpsth,'Number','correct','single');
% 
% %run through selected groups and make a summary figure for signigificant
% %differences from the interleaved fixation trials
% allcrossings = [];
% allresp = [];
% abovebin = zeros(1,nbins);
% belowbin = zeros(1,nbins);
% for x = allpsths
%     
%     load(allpsth.data.setNames{x},'correct_fixation_resp','correct_fixation_pvals','numnc')
%     cluster = allpsth.data.Index(x,4);
%     
%     pvals = correct_fixation_pvals{cluster};
%     
%     resp = correct_fixation_resp(cluster,:);
%     
%     ii = find(pvals <= thresh);
%     allcrossings = [allcrossings ii];
%     
%     abovebin(ii(find(resp(ii) == 1))) = abovebin(ii(find(resp(ii) == 1))) + 1;
%     belowbin(ii(find(resp(ii) == -1))) = belowbin(ii(find(resp(ii) == -1))) + 1;
%     
%     allresp = [allresp resp(ii)];
% end
% 
% hold on
% subplot(3,1,1)
% h = hist(allcrossings,[1:nbins]);
% bar(h,'r'); hold on
% nticks = size([1:1:nbins],2);
% set(gca,'XTick',[1:1:nbins])
% bintime = 2000 / nbins;
% set(gca,'XTickLabel',[-500+bintime:2000/nticks:1500])
% 
% plot([(500/bintime) + .5, (500/bintime) + .5],[0 40],'color','r'); hold on %sample on
% plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 40],'color','r'); hold on %sample off
% plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 40],'color','r'); hold on %earliest match
% title('significant bins')
% 
% 
% %make plot to indicate if the significant bins were above or below the
% %baseline
% hold on
% subplot(3,1,2)
% bar(abovebin,'r'); hold on
% bar(belowbin*-1,'hist'); hold on
% nticks = size([1:1:nbins],2);
% set(gca,'XTick',[1:1:nbins])
% bintime = 2000 / nbins;
% set(gca,'XTickLabel',[-500+bintime:2000/nticks:1500])
% 
% plot([(500/bintime) + .5, (500/bintime) + .5],[-40 40],'color','r'); hold on %sample on
% plot([(1000/bintime) + .5, (1000/bintime) + .5],[-40 40],'color','r'); hold on %sample off
% plot([(1800/bintime) + .5, (1800/bintime) + .5],[-40 40],'color','r'); hold on %earliest match
% title('orientation of significant bins')
% 
% 
% 
% %make plot for stimulus specific activity
% allcrossingside = [];
% for x = allpsths
%     
%     load(allpsth.data.setNames{x})
%     cluster = allpsth.data.Index(x,4);
%     pvals = ide_pvals{cluster};
%     
%     ii = find(pvals <= thresh);
%     allcrossingside = [allcrossingside ii];
% end
% hold on
% subplot(3,1,3)
% h = hist(allcrossingside,[1:nbins]);
% bar(h,'r'); hold on
% nticks = size([1:1:nbins],2);
% set(gca,'XTick',[1:1:nbins])
% bintime = 2000 / nbins;
% set(gca,'XTickLabel',[-500+bintime:2000/nticks:1500])
% 
% plot([(500/bintime) + .5, (500/bintime) + .5],[0 40],'color','r'); hold on %sample on
% plot([(1000/bintime) + .5, (1000/bintime) + .5],[0 40],'color','r'); hold on %sample off
% plot([(1800/bintime) + .5, (1800/bintime) + .5],[0 40],'color','r'); hold on %earliest match
% title('stimulus specificity')
% 
% 
% 









