function [nind,rind,varind,varargout] = tuningGC(day,varargin)

Args = struct('plot',0,'pairedAnalysis',0,'type','PF','allpairs',[]);
Args.flags = {'plot','pairedAnalysis'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'plot','pairedAnalysis'});

wind = {'presample','sample','delay1','delay2'};

%% identification of the rule for each session
sdir = pwd;
% cd(sprintf('/Volumes/snowboard.cns.montana.edu/COBRA/data/clark/%s',day));
% path = uigetdir(pwd,'Select the clark folder');
cd ..;
cd(day)
sessions = nptDir('session0*');
for s = 2 : length(sessions); cd(sessions(s).name); mts = mtstrial('auto'); rule(s-1) = unique(mts.data.Index(:,1)); cd ..; end;
cd(sdir)
%% Initialisation
meanbeta = load('gc/clark_mean.mat');
surr = load('gc/clark_01_mean_perm.mat');


theday = strcmp(surr.date,day);
surro = squeeze(surr.gcmean(theday,:,:,:));
meandata{1} = squeeze(meanbeta.meank2l(theday,:,:,:,:));
meandata{2} = squeeze(meanbeta.meanl2k(theday,:,:,:,:));

allthepairs = squeeze(surr.pairnames(theday,1,1,:));

nday = find(theday == 1);

% cd(day)
% cd('gc')
% file = nptDir('clark*.mat');
%
% load(file.name)

for pp = 1 : 4; for r = 1 : 2; for p = 1 : 4; for direc = 1 : 2; nind{pp,r,p,direc} = [];rind{pp,r,p,direc} = []; varind{pp,r,p,direc} = []; index{pp,r,direc} = []; windcorr{pp,r,direc} = [];end; end; end; end
%%
if isempty(Args.allpairs)
    allpairs = [1 : length(allthepairs)];
    %%
    switch Args.type
        case 'PF'
            condition = '~isempty(strfind(allthepairs{pair},''P'')) & ~isempty(strfind(allthepairs{pair},''F''))';
        case 'PP'
            condition = 'length(strfind(allthepairs{pair},''P'')) == 2';
        case 'FF'
            condition = 'length(strfind(allthepairs{pair},''F'')) == 2';
    end
else
    alp = 1;
    notfound = false;
    while alp <= 36 & notfound
        if ~isempty(strfind(allthepairs{alp},'P')) & ~isempty(strfind(allthepairs{alp},'F'))
            notfound = false;
        else
            alp = alp + 1;
        end
    end
    
    allpairs = alp - 1 + Args.allpairs;
    condition = 'true';
end

for pair = allpairs
    if eval(condition)
        if Args.plot
            ff = figure;
        end
        ymax = [];
        for s = 1 : 2;
            for pp = 1 : 4;
                for direc = 1 : 2
                    if Args.pairedAnalysis
                        datad1 = squeeze(meandata{1}(s,pp,:,pair));
                        datad2 = squeeze(meandata{2}(s,pp,:,pair));
                        datad = [datad1; datad2];
                        
                    else
                        
                        datad = squeeze(meandata{direc}(s,pp,:,pair));
                        
                    end
                    
                    if sum(datad > surro(s,p,pair)) > 0
                        for p = 1 : 4; data(:,p) = squeeze(meandata{direc}(s,p,:,pair));end
                        if Args.pairedAnalysis
                            maxdata = max(max([squeeze(meandata{1}(s,:,:,pair)) squeeze(meandata{2}(s,:,:,pair))]));
                        else
                            maxdata = max(max(data));
                        end
                        ndata = data ./ repmat(maxdata,[size(data,1) size(data,2)]);
                        [rdatat,ix] = sort(squeeze(meandata{direc}(s,pp,:,pair)));
                        
                        for p = 1 : 4
                            
                            rdata(:,p) = ndata(ix,p);
                            vardata = std(ndata(:,p));
                            nind{pp,rule(s),p,direc} = [nind{pp,rule(s),p,direc} ndata(:,p)];
                            rind{pp,rule(s),p,direc} = [rind{pp,rule(s),p,direc} rdata(:,p)];
                            varind{pp,rule(s),p,direc} = [varind{pp,rule(s),p,direc} vardata];
                            
                        end
                        if pp == 3 | pp == 4; [b,bint,r,rint,stats] = regress(rdata(:,setdiff([3 4],pp)),[1 1;1 2;1 3;1 4;1 5;1 6;1 7;1 8;1 9]);
                        elseif pp == 1 | pp == 2
                            [b,bint,rr,rint,stats] = regress(rdata(:,setdiff([1 2],pp)),[1 1;1 2;1 3;1 4;1 5;1 6;1 7;1 8;1 9]);
                        end
                        %                         [cor,corp] = corrcoef(rdata(:,3),rdata(:,4));
                        windcorr{pp,rule(s),direc} = [windcorr{pp,rule(s),direc} [b(2);stats(3)]];
                        index{pp,rule(s),direc} = [index{pp,rule(s),direc} [nday; pair]];
                    end
                    
                end;
                
                if Args.plot
                    for p = 1 : 4
                        subplot(2,4,(r-1)*4+p);
                        
                        plot(squeeze(data(:,:,p)))
                        title(sprintf('%g %g',tind{r,p}(1,end),tind{r,p}(2,end)));
                    end
                end
            end
            if Args.plot
                legend('to','from')
                for sb = 1: 8; subplot(2,4,sb); axis([1 9 0 1]); end
                set(ff,'Name',sprintf('%s-%s',chani{ch1},chanj{ch2}))
                %                 for sb = 1 : 4; subplot(2,4,sb); title(wind{sb}); end
                subplot(2,4,1); ylabel('IDE; mvar'); subplot(2,4,5); ylabel('LOC; mvar');
                for sb = 5 : 8; subplot(2,4,sb);xlabel('stimuli'); end
            end
        end
    end
    
end
varargout{1} = windcorr;
varargout{2} = index;
%
% pair = 0;
% for ch1 = 1 : length(chani)
%
%     for ch2 = (ch1+1) : length(chanj)
%         pair = pair + 1;
%         if ~isempty(strfind([chani{ch1} chanj{ch2}],'P')) & ~isempty(strfind([chani{ch1} chanj{ch2}],'F'))
%             if Args.plot
%                 ff = figure;
%             end
%             ymax = [];
%             for r = 1 : 2;
%                 for pp = 3 : 4;
%                     datad(1,:) = squeeze(integrali2j(r,:,p,ch1,ch2));
%                     datad(2,:) = squeeze(integralj2i(r,:,p,ch1,ch2));
%
%                     if sum(datad(1,:) > surro(1,p,pair)) > 0 | sum(datad(2,:) > surro(2,p,pair)) > 0
%                         for p = 1 : 4
%                             data(:,1,p) = squeeze(integrali2j(r,:,p,ch1,ch2));
%                             data(:,2,p) = squeeze(integralj2i(r,:,p,ch1,ch2));
%
%                         end
%                         [maxdata,maxind] = max(max(max(data,[],1),[],3));
%                         ndata = data ./ repmat(maxdata,[size(data,1) size(data,2) size(data,3)]);
%                         [rdatato,ix] = sort(datad(maxind,:));
%
%                         for p = 1 : 4
%
%                             for direc = 1 : 2;
%                                 rdata = ndata(ix,direc,p);
%                                 vardata = std(ndata(:,direc,p));
%                                 nind{pp-2,rule(r),p,direc} = [nind{pp-2,rule(r),p,direc} ndata(:,direc,p)];
%                                 rind{pp-2,rule(r),p,direc} = [rind{pp-2,rule(r),p,direc} rdata];
%                                 varind{pp-2,rule(r),p,direc} = [varind{pp-2,rule(r),p,direc} vardata];
%                             end
%                         end
%                     end
%                     %                     mx = max(data(:,:,p));
%                     %                     mn = min(data(:,:,p));
%                     %                     tuningInd = (mx-mn) ./ (mx + mn);
%                     %                     tind{r,p} = [tind{r,p} tuningInd'];
%
%                     %                 data = data ./ repmat(max(max(data)),size(data,1),2);
%                     %                 plot(data);
%
%
%                 end;
%
%                 %             mx =  max(max(data,[],3),[],1);
%                 %             mn = min(min(data,[],3),[],1);
%                 %             tuningInd(r,:) = (mx-mn) ./ (mx + mn);
%                 %                 for p = 1 : 4;
%                 %                     mx = max(data(:,:,p));
%                 %                     mn = min(data(:,:,p));
%                 %                     tuningInd = (mx-mn) ./ (mx + mn);
%                 %                     tind{r,p} = [tind{r,p} tuningInd'];
%                 %                 end
%                 if Args.plot
%                     for p = 1 : 4
%                         subplot(2,4,(r-1)*4+p);
%
%                         plot(squeeze(data(:,:,p)))
%                         title(sprintf('%g %g',tind{r,p}(1,end),tind{r,p}(2,end)));
%                     end
%                 end
%             end
%             if Args.plot
%                 legend('to','from')
%                 for sb = 1: 8; subplot(2,4,sb); axis([1 9 0 1]); end
%                 set(ff,'Name',sprintf('%s-%s',chani{ch1},chanj{ch2}))
%                 %                 for sb = 1 : 4; subplot(2,4,sb); title(wind{sb}); end
%                 subplot(2,4,1); ylabel('IDE; mvar'); subplot(2,4,5); ylabel('LOC; mvar');
%                 for sb = 5 : 8; subplot(2,4,sb);xlabel('stimuli'); end
%             end
%         end
%     end
% end
% cd ../..
%
