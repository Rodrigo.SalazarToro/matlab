function cohgramSurrogate(varargin)
% to run in the session

Args = struct('save',0,'redo',0,'iterations',1000,'mwind',[0.2 0.05],'Fs',200,'tapers',[2 3],'fpass',[0 50],'rule',1,'type','nine');
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'save','redo'});

switch Args.type
    case 'nine'
        
        ncues = 9;
        cuecomb=[1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    case 'Loc'
        ncues = 3;
        cuecomb=[1 2 3];
    case 'Ide'
        ncues = 3;
        cuecomb=[1 2 3];
    case 'Fix'
        
        ncues = 1;
end

sdir = pwd;


params = struct('tapers',Args.tapers,'Fs',Args.Fs,'fpass',Args.fpass,'trialave',1);
mts = mtstrial('auto','redosetNames');


NeuroInfo = NeuronalChAssign;
ch = [];
for c = 1 : length(NeuroInfo.groups)
    tgroup = sprintf('group%04.f',NeuroInfo.groups(c));
    if ~isempty(nptDir(tgroup))
        
        ch = [ch c];
        
    end
end
group = nptDir('group*');
for g = 1 : length(group); groups{g} = sprintf('%s/%s',pwd,group(g).name); end

cd lfp

files = nptDir('*_lfp.*');
if ~isempty(ch)
    
    for cue =1 : ncues;
        switch Args.type
            case 'nine'
                matfile = sprintf('cohgramGenSurRule%dloc%dide%d.mat',Args.rule,cuecomb(1,cue),cuecomb(2,cue));
            case 'Loc'
                matfile = sprintf('cohgramGenSurRule%dloc%d.mat',Args.rule,cuecomb(1,cue));
            case 'Ide'
                matfile = sprintf('cohgramGenSurRule%dIde%d.mat',Args.rule,cuecomb(1,cue));
            case 'Fix'
                 matfile = 'cohgramGenSurRuleFix.mat';
        end
        cd(sdir)
        cd ..
        cd grams
        if isempty(nptDir(matfile)) || Args.redo
            cd(sdir)
            switch Args.type
                case 'nine'
                    trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',Args.rule,'iCueLoc',cuecomb(1,cue),'iCueObj',cuecomb(2,cue),modvarargin{:});
                case 'Loc'
                    trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',Args.rule,'iCueLoc',cuecomb(1,cue),modvarargin{:});
                case 'Ide'
                    trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',Args.rule,'iCueObj',cuecomb(1,cue),modvarargin{:});
                case 'Fix'
                    trials = mtsgetTrials(mts,'rule',Args.rule,'ML','lowThresRT',[],'CueObj',55,modvarargin{:});
            end
            count = 1;
            lfp = zeros(381,length(trials),length(ch));
            cd lfp
            for t = trials
                [tlfp,~,~,~,~] = nptReadStreamerChannel(files(t).name,ch);
                
                for thech = 1 : length(ch)
                    [lfp(:,count,thech),~] = preprocessinglfp(tlfp(thech,round(mts.data.CueOnset(t)) - 500 : round(mts.data.CueOnset(t)) - 500 + 1900),modvarargin{:},'detrend');
                end
                count =count + 1;
            end
            [Ct,~,~,~,~,~,~] = cohgramc(lfp(:,randperm(length(trials)),1),lfp(:,randperm(length(trials)),end),Args.mwind,params);
            C = zeros(Args.iterations,size(Ct,1),size(Ct,2));
            phi = zeros(Args.iterations,size(Ct,1),size(Ct,2));
            C(1,:,:) = Ct;
            
            phi(1,:,:) = Ct;
            for ii = 2 : Args.iterations
                ordT1 = randperm(length(trials));
                ordT2 = randperm(length(trials));
                thech = [1; 1];
                while diff(thech) == 0
                    thech = randi(length(ch),[2,1]);
                end
                sur1 = lfp(:,ordT1,thech(1));
                sur2 = lfp(:,ordT2,thech(2));
                [C(ii,:,:),phi(ii,:,:),~,~,~,t,f] = cohgramc(squeeze(sur1),squeeze(sur2),Args.mwind,params);
            end
            cd(sdir)
            cd ..
            cd grams
            save(matfile,'C','phi','t','f','Args','trials','ch')
            display([pwd matfile])
        end
    end
end
cd(sdir)