function all_bmf_grids(varargin)

%run at monkey level
%used to make bmf_girds for all days
%save .jpegs in day and monkey folder

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;

mkdir bmf_grids
cd bmf_grids
griddir = pwd;

cd(monkeydir)

d = nptdir('11*');
numdays = size(d,1);

days = cell(1,numdays);
for x = 1 : numdays
    cd([monkeydir filesep d(x).name]);
    
    try
    bmf_grid

    saveas(gcf,[d(x).name '.fig'])
    
    cd(griddir)
    saveas(gcf,[d(x).name '.fig'])
    
    close all
    catch
        %skip days that grids can not be made for yet
    end
end
    
    
    
    
    
    
  
