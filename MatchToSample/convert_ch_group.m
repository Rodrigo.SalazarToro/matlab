function out = convert_ch_group(varargin)

%run at session level
%used to convert channels <-> groups

Args = struct('channels',[],'groups',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

[NeuroInfo] = NeuronalChAssign;

if ~isempty(Args.channels)
    out = NeuroInfo.groups(Args.channels);
end

if ~isempty(Args.groups)
    [i out] = intersect(NeuroInfo.groups,Args.groups);
end