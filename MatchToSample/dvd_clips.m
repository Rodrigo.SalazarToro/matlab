
function dvd_clips(varargin)

%make series of clips from a dvd
%1.) first copy vob files from dvd
%go to the directory files should be saved in then type 
%vobcopy -m

%2.) convert to .avi
%ffmpeg -i in.VOB -f avi -vcodec mpeg4 -maxrate 1000 -b 700 -qmin 3 -qmax 5 -bufsize 4096 -g 300 -an -ab 192 out.avi
% ffmpeg -i VTS_04_2.VOB -f avi -vcodec rawvideo -an -cropleft 120 -cropright 120 out.avi
%ffmpeg -i VTS_04_2.VOB -f avi -vcodec rawvideo -s 1280x1024 -an -b 64k -r 15 out.avi



%3.) run the following code in the directory where .avi file is saved
%mencoder -ss 00:00:00 -fps 15 -endpos 00:00:03 -ovc copy -oac copy out.avi -o clip.avi

%repeat for each desired clip