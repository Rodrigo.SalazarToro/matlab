function [Cmean,Cstd,phimean,phistd,tile99,Prob,f,varargout] = cohsurrogate(data1,data2,params,varargin)
% data1 and data2 should be in sample x trials
% if 'generalized' then sample x trials x channels and data1 == data2;
% the modvarargin goes into fitSurface
% By default runs the coherencyc but with the args 'SFC' or 'STA' will run
% them instead

Args = struct('redoFile',0,'nrep',1000,'Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2,'SFC',0,'STA',0,'saveName',[],'generalized',0);
Args.flags = {'SFC','STA','generalized','redoFile'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'nrep','SFC','STA'});

ntrials = size(data1,2);
params.trialave = 1;

if Args.redoFile || (~isempty(Args.saveName) && isempty(nptDir(Args.saveName))) || isempty(Args.saveName)
    
    if Args.SFC
        [Ct,phit,~,~,~,f,~] = coherencycpt(data1,data2,params);
    elseif Args.STA
        [~,f,~,Ct] = spikeTrigAve(data1,data2,params.Fs,'average');
    elseif Args.generalized
        [Ct,phit,~,~,~,f] = coherencyc(data1(:,:,1),data2(:,:,2),params);
    else
        [Ct,phit,~,~,~,f] = coherencyc(data1,data2,params);
        
    end
    
    C = nan(Args.nrep,length(Ct));
    if ~Args.STA; phi = nan(Args.nrep,length(phit)); end
    
    
    for rep = 1 : Args.nrep
        ordT1 = randperm(ntrials);
        ordT2 = randperm(ntrials);
        
        if Args.generalized
            thech = [1; 1];
            while diff(thech) == 0
                thech = randi(size(data1,3),[2,1]);
            end
            sur1 = data1(:,ordT1,thech(1));
            sur2 = data2(:,ordT2,thech(2));
        else
            sur1 = data1(:,ordT1);
            sur2 = data2(:,ordT2);
        end
        if Args.SFC || Args.STA
            sur2 = data2(ordT2);
            
        end
        
        if Args.SFC
             [C(rep,:),phi(rep,:),~,~,~,f,~] = coherencycpt(sur1,sur2,params);
        elseif Args.STA
            [~,f,~,C(rep,:)] = spikeTrigAve(sur1,sur2,params.Fs,'average');
            %         elseif Args.GC
            %                seq =  (reshape(repmat(ordT,Nl,1),1,Nl * length(ordT)) - 1) * Nl + repmat([1 : Nl],1,ntrials);
            %              [~,~,Fx2y(rep,:),Fy2x(rep,:),Fxy(rep,:),~] = pwcausalrp(sur,Nr,Nl,Args.porder,fs,freq);
        else
            [C(rep,:),phi(rep,:),~,~,~,f] = coherencyc(sur1,sur2,params);
        end
    end
    nsave = true;
elseif ~isempty(Args.saveName) && ~isempty(nptDir(Args.saveName))
    load(Args.saveName)
    nsave = false;
end

if unique(isnan(C))
    thresh = [];
elseif ~exist('thresh')
    if Args.STA
        [~,~,thresh,~] = fitSurface(C,f,'Prob',Args.Prob,'Distribution','Norm',modvarargin{:});
    else
        [~,~,thresh,~] = fitSurface(C,f,'Prob',Args.Prob,'FSpread',Args.FSpread,modvarargin{:});
    end
end
Cmean = mean(C,1);
Cstd = std(C,[],1);
if Args.STA
    phimean = [];
    phistd = [];
    
else
    phimean = mean(phi,1);
    phistd = std(phi,[],1);
    varargout{2} = phi;
end
tile99 = prctile(C,99,1);
Prob = thresh;

if ~isempty(Args.saveName) && nsave
    display(sprintf('saving %s \n',Args.saveName))
    save(Args.saveName,'C','phi','f','thresh','Args')
end
varargout{1} = C;
% varargout{3} = Ct;
% varargout{4} = phit;
