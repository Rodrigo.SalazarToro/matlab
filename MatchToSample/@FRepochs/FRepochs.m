function obj = FR4epochs(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedLFP',[],'NoHisto',0);
Args.flags = {'Auto','NoHisto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'FRepochs';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'FRe';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

mtst = mtstrial('auto',varargin{:});
groups = nptDir('group*');
[thegr,comb] = NeuronalChAssign('onlyPP');
[channels,comb,CHcomb,gr] = checkChannels('cohInter','notCareForLFP');
sdir = pwd;
rules = {'I','L'};
if  ~isempty(groups) & ~isempty(mtst)
    count = 1 ;
    data.Index = [];
    if ~Args.NoHisto; histology = NeuronalHist; end
    for g = 1 : size(groups,1)
        cd(groups(g).name)
        gdir = pwd;
        clusters = nptDir('cluster*');
        if ~isempty(Args.selectedLFP)
            list = Args.selectedLFP;
            %             dlevel = strread(list.days,'%s','delimiter','/');
            %             theday = dlevel{strncmp('0',dlevel,1)};
            indg = strmatch(gdir(1:length(list.days{1})),list.days);
            if ~isempty(indg)
                thech = unique(list.ch(indg,:));
                tgroup = thegr.groups(thech);
                if ~isempty(find(str2num(gdir(end-3:end)) == tgroup));
                    skip = false;
                else
                    skip = true;
                end
            else
                skip = true;
            end
        else
            skip = false;
        end
        if ~skip
            ch = find(str2num(groups(g).name(end-3: end)) == thegr.groups);
            for c = 1 : size(clusters,1)
                cd(clusters(c).name)
                tindex = getIndexUnits(mtst,histology,ch,'rudeHist',varargin{:});
                if ~isempty(tindex)
                    Index(count,:) = tindex;
                    Index(count,5) = count;
                    data.Index = Index;
                    data.setNames{count} = pwd;
                    count = count + 1;
                end
                cd(gdir)
            end
        end %c = 1 : size(clusters,1)
        cd(sdir)
    end %g = 1 : size(group,1)
    
    
    data.numSets = count-1; % nbr of trial
    
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
    
else
    % create empty object
    fprintf('The mtstrial object is empty or the group directory does not exist \n');
    obj = createEmptyObject(Args);
    
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
