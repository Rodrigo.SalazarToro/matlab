function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'rule',[],'cortex',[],'unit',[],'mfr',[],'ntrials',[],'baseline',0.05,'cue',[],'delay',[],'match',[],'effect',[],'pLoc',[],'pIde',[],'pPer',[],'pIntStim',[],'pairhist',[]);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    
    if ~isempty(Args.day)
        rtemp1 = find(obj.data.Index(:,1) == Args.day);
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.rule)
        rtemp2 = find(obj.data.Index(:,2) == cell2mat(Args.rule));
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.cortex)
        rtemp3 = find(obj.data.Index(:,3) == cell2mat(Args.cortex));
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.unit)
        rtemp4 = find(obj.data.Index(:,4) == cell2mat(Args.unit));
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.mfr)
        rtemp5 = find(obj.data.Index(:,6) > Args.mfr);
    else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.ntrials)
        rtemp6 = find(obj.data.Index(:,7) > Args.ntrials);
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.baseline)
        rtemp7 = find(obj.data.Index(:,8) > Args.baseline);
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.cue)
        if ~isempty(Args.effect)
            if Args.effect == 1
                direct = 15;
            else
                direct = 16;
            end
            rtemp8 = find((abs(obj.data.Index(:,9)) < Args.cue & (sign(obj.data.Index(:,9)) == Args.effect))  | (obj.data.Index(:,12) < Args.cue  & obj.data.Index(:,direct)));
        else
            rtemp8 = find(abs(obj.data.Index(:,9)) < Args.cue | abs(obj.data.Index(:,12)) < Args.cue);
        end
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.delay)
        
        if ~isempty(Args.effect)
            if Args.effect == 1
                direct = 17;
            else
                direct = 18;
            end
            rtemp9 = find((abs(obj.data.Index(:,10)) < Args.delay & (sign(obj.data.Index(:,10)) == Args.effect))  | (obj.data.Index(:,13) < Args.delay  & obj.data.Index(:,direct)));
        else
            rtemp9 = find(abs(obj.data.Index(:,10)) < Args.delay | abs(obj.data.Index(:,13)) < Args.delay);
        end
    else
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.match)
        if Args.effect == 1
            direct = 19;
        else
            direct = 20;
        end
        if ~isempty(Args.effect)
            rtemp10 = find((abs(obj.data.Index(:,11)) < Args.match & (sign(obj.data.Index(:,11)) == Args.effect))  | (obj.data.Index(:,14) < Args.match  & obj.data.Index(:,direct)));
        else
            rtemp10 = find(abs(obj.data.Index(:,11)) < Args.match | abs(obj.data.Index(:,14)) < Args.match);
        end
    else
        rtemp10 = [1 : size(obj.data.Index,1)];
    end
     if ~isempty(Args.pLoc)
        rtemp11 = find(obj.data.Index(:,21) < Args.pLoc);
    else
        rtemp11 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.pIde)
        rtemp12 = find(obj.data.Index(:,22) < Args.pIde);
    else
        rtemp12 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.pPer)
        rtemp13 = find(obj.data.Index(:,23) < Args.pPer);
    else
        rtemp13 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.pIntStim)
        rtemp14 = find(obj.data.Index(:,24) < Args.pIntStim);
    else
        rtemp14 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.pairhist)
        switch Args.pairhist
            case 'l'
                
                code = 1;
            case 'm'
                code = 2;
            case 'd'
                code = 3;
            case 'v'
                code = 4;
            case 's'
                code = 99;
        end
        rtemp15 = find(obj.data.Index(:,27) == code);
    else
        rtemp15 = [1 : size(obj.data.Index,1)];
    end
    %     if ~isempty(Args.cue)
    %         if ~isempty(Args.effect)
    %             rtemp8 = find((abs(obj.data.Index(:,9)) < Args.cue | abs(obj.data.Index(:,12)) < Args.cue) & (sign(obj.data.Index(:,9)) == Args.effect | sign(obj.data.Index(:,12)) == Args.effect));
    %         else
    %             rtemp8 = find(abs(obj.data.Index(:,9)) < Args.cue | abs(obj.data.Index(:,12)) < Args.cue);
    %         end
    %     else
    %         rtemp8 = [1 : size(obj.data.Index,1)];
    %     end
    %
    %     if ~isempty(Args.delay)
    %         if ~isempty(Args.effect)
    %             rtemp9 = find((abs(obj.data.Index(:,10)) < Args.delay | abs(obj.data.Index(:,13)) < Args.delay) & (sign(obj.data.Index(:,10)) == Args.effect | sign(obj.data.Index(:,12)) == Args.effect));
    %         else
    %             rtemp9 = find(abs(obj.data.Index(:,10)) < Args.delay | abs(obj.data.Index(:,13)) < Args.delay);
    %         end
    %     else
    %         rtemp9 = [1 : size(obj.data.Index,1)];
    %     end
    %
    %     if ~isempty(Args.match)
    %         if ~isempty(Args.effect)
    %             rtemp10 = find((abs(obj.data.Index(:,11)) < Args.match | abs(obj.data.Index(:,14)) < Args.match) & (sign(obj.data.Index(:,11)) == Args.effect | sign(obj.data.Index(:,14)) == Args.effect));
    %         else
    %             rtemp10 = find(abs(obj.data.Index(:,11)) < Args.match | abs(obj.data.Index(:,14)) < Args.match);
    %         end
    %     else
    %         rtemp10 = [1 : size(obj.data.Index,1)];
    %     end
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect(rtemp10,intersect(rtemp11,intersect(rtemp12,intersect(rtemp13,intersect(rtemp14,rtemp15))))))))))))));
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

