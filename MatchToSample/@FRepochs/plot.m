function obj = plot(obj,varargin)


Args = struct('pThresh',0.05);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];

if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
lb = {'pC' 'C' 'D1' 'D2' 'M'};
load FR4epochs.mat

mag = 1000/plength;

allymax = [];
for loc = 1 : 3
    nspike = [];
    for ide = 1 : 3
        subplot(4,4,(loc-1) * 4 + ide);
        indc = find(cueComb(1,:) == loc & cueComb(2,:) == ide);
        nt((loc-1) * 4 + ide) = length(allspikes{indc});
        m = nanmean(allspikes{indc} * mag,1) ;
        st = nanstd(allspikes{indc} * mag,1);
        if ~isempty(m);
            bar(m);
            nspike = [nspike; allspikes{indc}];
            hold on
            whisk = m + st;
            plot(whisk,'o')
            %         text(5,max(whisk),sprintf('n = %d',nt))
        end
        hold off
        
        if loc == 1; title(sprintf('Ide %d',ide)); end
        if ide == 1; ylabel(sprintf('Loc %d',loc)); end
        allymax = [allymax max(m+st)];
    end
    nspike = nspike * mag;
    nt(loc*4) = length(nspike);
    subplot(4,4,loc*4)
    bar(nanmean(nspike ,1));
    hold on
    line([1 5],[nanmean(nanmean(nspike)) nanmean(nanmean(nspike))],'Color','r');
    wh = nanmean(nspike,1)+nanstd(nspike,1);
    
    plot(wh,'o')
    %      text(5,max(wh),sprintf('n = %d',nt))
    allymax = [allymax max(wh)];
end

for ide = 1 : 3
    nspike = [];
    for loc = 1 : 3
        indc = find(cueComb(1,:) == loc & cueComb(2,:) == ide);
        nspike = [nspike; allspikes{indc}];
    end
    
    nspike = nspike * mag;
    nt(12+ide) = length(nspike);
    subplot(4,4,12+ide)
    bar(nanmean(nspike,1));
    hold on
    wh = nanmean(nspike,1) + nanstd(nspike,1);
    plot(wh,'o')
    %      text(5,max(wh),sprintf('n = %d',nt))
    allymax = [allymax max(wh)];
    line([1 5],[nanmean(nanmean(nspike)) nanmean(nanmean(nspike))],'Color','r');
end

subplot(4,4,16)
nspike = [];
for c = 1 : 9;  nspike = [nspike; allspikes{c}];end
nspike = nspike * mag;
nt(16) = length(nspike);
bar(nanmean(nspike,1));
hold on
wh = nanmean(nspike,1)+nanstd(nspike,1);
plot(wh,'o')
%  text(5,max(wh),sprintf('n = %d',nt))
allymax = [allymax max(wh)];
ydis = max(allymax);
for sb = 1 : 16; subplot(4,4,sb);ylim([0 1.1*max(allymax)]);  text(4,ydis,sprintf('n = %d',nt(sb)));end

for sb = [1 2 3 5 6 7 9 10 11]; subplot(4,4,sb);text(1,ydis,sprintf('p = %.4f',pvalues(4))); end
for sb = [4 8 12]; subplot(4,4,sb);text(1,ydis,sprintf('p = %.4f',pvalues(1))); end
for sb = [13 14 15]; subplot(4,4,sb);text(1,ydis,sprintf('p = %.4f',pvalues(2))); end
subplot(4,4,16); text(1,ydis,sprintf('p = %.4f',pvalues(3)));
if pvalues(5) < Args.pThresh | pvalues(6) < Args.pThresh
    thecue = 1;
    for sb = [1 2 3 5 6 7 9 10 11];
        thep = anova1(allspikes{thecue},[],'off');
        subplot(4,4,sb);text(1,4*ydis/5,sprintf('p = %.4f',thep));
        thecue = thecue + 1;
    end
    
end

fprintf('%s%s%s \n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2))


