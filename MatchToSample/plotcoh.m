function plotcoh(type,day,r,varargin)



Args = struct('switchDay',0,'cohgram',0,'movingwin',[0.4 0.01],'sigLevel',2,'save',0,'redo',0,'cmb',[],'plot',0,'ML',0,'mvAvg',[],'pause',0);
Args.flags = {'cohgram','switchDay','save','redo','plot','ML','pause'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'cohgram','switchDay','save','redo','plot'});

params = struct('tapers',[3 5],'Fs',200,'fpass',[0 100],'trialave',1);
mwind = Args.movingwin;
% cd(day(end-5:end))
cd(day)
skip = nptDir('skip.txt');
basefile = nptDir(sprintf('%s.mat',type));

if ~isempty(basefile) & isempty(skip)
    load(basefile.name);

    if isempty(Args.cmb); allcmb = [1 : size(Day.comb,1)]; else; allcmb = Args.cmb; end

    for cmb = allcmb
        fileWP = nptDir(sprintf('windowSpectraComb%d.fig',cmb));

        if Args.cohgram; fileCoh = nptDir(sprintf('cohgramComb%d.fig',cmb)); end

        if isempty(fileWP) | Args.redo


            sur = load(sprintf('%sSur.mat',type));

            session = nptDir('session0*');

            if Args.ML
                MTS = false;
                thes = 1;
                while ~MTS
                    cd(session(thes).name)
                    thetask = nptDir('MTS*');
                    if ~isempty(thetask)
                        MTS = true;
                        mtst = mtstrial('auto','ML');
                    else
                        thes = thes + 1;
                        cd ..
                    end

                end
                if Args.cohgram; cohgram(mtst,Day,cmb,mwind,s,params,Args.switchDay); end

                for s = 1 : 2; [ymax(s)] = plotWin(type,day,Day,sur,f,s,cmb,Args.sigLevel,mtst,Args.switchDay,Args); end
                cd ..
            elseif length(session) == 2 | length(Day.session) == 1
                s = 1;
                cd(session(2).name);
                %                 mtst = ProcessSession(mtstrial,'auto');
                mtst = mtstrial('auto');

                cd ..
                if Args.cohgram; cohgram(mtst,Day,cmb,mwind,s,params,Args.switchDay); end
                [ymax(s)] = plotWin(type,day,Day,sur,f,s,cmb,Args.sigLevel,mtst,Args.switchDay,Args,session)

            else
                for ss = 2 : length(session)
                    if ss <= length(Day.session) + 1

                        cd(session(ss).name);
                        mtst = mtstrial('auto');

                        %                         mtst = ProcessSession(mtstrial,'auto');

                        s = ss-1;
                        if size(r,2) == 1 && r == unique(mtst.data.Index(:,1))
                            if Args.cohgram; cohgram(mtst,Day,cmb,mwind,s,params,Args.switchDay); end

                            [ymax(s)] = plotWin(type,day,Day,sur,f,s,cmb,Args.sigLevel,mtst,Args.switchDay,Args)
                        elseif size(r,2) == 2
                            if Args.cohgram; cohgram(mtst,Day,cmb,mwind,s,params,Args.switchDay); end
                            [ymax(s)] = plotWin(type,day,Day,sur,f,s,cmb,Args.sigLevel,mtst,Args.switchDay,Args,session);

                        end
                        cd ..
                    end
                end
            end

            h1 = figure(99);if Args.cohgram;h2 = figure(100);end
            if Args.switchDay;

                for sb = 1 : 8; subplot(2,4,sb); axis([4 96 0 max(ymax)+0.1*max(ymax)]); end;

            end
            if Args.save; saveas(h1,sprintf('windowSpectraComb%d',cmb),'fig'); if Args.cohgram; saveas(h2,sprintf('cohgramComb%d',cmb),'fig'); end; end;if Args.pause;pause; end;close all;

        elseif Args.plot
            
            newh = open(fileWP.name);
            if Args.cohgram; open(fileCoh.name); end
            pause
            close(newh)
            
        end
    end
else
    fprintf('The %s.mat does not exist. Cannot compute the desired graphs! \n',type)
end
cd ..
%%
function [varargout] = plotWin(type,day,Day,sur,f,s,cmb,sigLevel,mtst,switchDay,Args,varargin)
wind = {'fixation' 'Cue' 'delay1' 'delay2'};
ymax = 0;
MLrules = {'IDENTITY' 'LOCATION'};
if switchDay; if isempty(varargin) | ~isempty(findstr(type,'cS')); col = 2; else col = length(varargin{1}) - 1; end; f1 = figure(99);ss = s; else; col = 1;f1 = figure;ss = 1;end
if ~isempty(Day.session(s).C)
    for p = 1: 4
        subplot(col,4,(ss-1)*4+p)
        
            data = Day.session(s).C(:,cmb,p);
        if ~isempty(Args.mvAvg)
            ff = repmat(1/Args.mvAvg,1,Args.mvAvg);
            fdata = filtfilt(ff,1,Day.session(s).C(:,cmb,p));
            plot(f,fdata,'r')
            hold on
        end
        plot(f,data);
        ymax = [ymax max(data)];
        hold on;
        if isempty(Args.mvAvg)
            thesur = sur.Day.session(s).Prob(:,sigLevel,cmb,p);
        else
            thesur = filtfilt(ff,1,sur.Day.session(s).Prob(:,sigLevel,cmb,p));
        end
        plot(f,thesur,'--');
        %     plot(f,Day.session(s).C(:,cmb,1),'r');
        %     plot(f,Day.session(s).Cerr(2,:,cmb,1),'r--')
        %     plot(f,Day.session(s).Cerr(1,:,cmb,1),'r--')
        %     plot(f,Day.session(s).Cerr(2,:,cmb,p),'--')
        %     plot(f,Day.session(s).Cerr(1,:,cmb,p),'--')
        ylabel(wind{p})

    end

    for p = 1 : 4; subplot(col,4,(ss-1)*4+p); axis([4 96 0 max(ymax)]); end
    hold off

    xlabel('Frequency [Hz]')
    if unique(mtst.data.Index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'IDENTITY'); elseif length(unique(mtst.data.Index(:,1)) == 1) > 1; rule = MLrules{s};else; rule = sprintf('session%d = %s',s+1,'LOCATION'); end
    subplot(col,4,(ss-1)*4+p); title(rule);
    set(f1,'Name',sprintf('%s; %s; combination %d',day(end-5:end),type,cmb))

    varargout{1} = max(ymax);
else
    varargout{1} = NaN;
end
%%
function cohgram(mtst,Day,cmb,mwind,s,params,switchDay)

if switchDay; col = 2;f2 = figure(100); else; col = 1;f2 = figure;end
trials = mtsgetTrials(mtst,'stable','BehResp',1);
cd lfp
channels = Day.comb(cmb,:);
files = nptDir('*_lfp.*');
c = 1;
for ch = vecr(channels)
    tt = 1;
    ltrial = [];

    for t = vecr(trials)
        [lfp,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(files(t).name,ch);
        [dlfp{tt}(:,c),SR] = preprocessinglfp(lfp);
        ltrial = [ltrial size(dlfp{tt},1)];
        tt = tt + 1;

    end
    c = c + 1;
end
cd ..
minlength = min(ltrial);
for t = 1 : length(trials)
    data1(:,t) = dlfp{t}(1:minlength,1);
    data2(:,t) = dlfp{t}(1:minlength,2);
end
%% removal of the EP

data1 = data1 - repmat(mean(data1,2),1,size(data1,2));
data2 = data2 - repmat(mean(data2,2),1,size(data2,2));
%%

[C,phi,S12,S1,S2,t,f]=cohgramc(data1,data2,mwind,params);

subplot(col,1,s)
imagesc(t,fliplr(f),rot90(C)); colorbar; axis([t(1) t(end) 5 95])
if unique(mtst.data.Index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'IDENTITY'); else; rule = sprintf('session%d = %s',s+1,'LOCATION'); end
title(rule)
set(f2,'Name',sprintf('%s; combination %d',Day.name(end-5:end),cmb))
xlabel('Time [s]')
ylabel('Frequency [Hz]')
