function compare_hsfpp(varargin)

%run at session level

Args = struct('ml',0,'filter_low',8,'filter_high',25,'redo',0,'filter',0); %10 - 20?
Args.flags = {'ml','redo','filter'};
[Args,modvarargin] = getOptArgs(varargin,Args);

cd('/media/raid/data/monkey/betty/090702/session01')

sesdir = pwd;

params = struct('tapers',[2 3],'Fs',1000,'fpass',[1 50],'trialave',0);

if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    r = 1; %only identity need to save in identity dir
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
else
    mt = mtstrial('auto','redosetNames');
    %determine rule
    r = mt.data.Index(1,1);
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
end

%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%get group information
NeuroInfo = NeuronalChAssign(); %get the "group" number that corresponds to each channel
N = NeuronalHist('ml');


%find groups with spikes
groups = nptDir('group*');
ngroups = size(groups,1);
counter = 0;
for g = 11%1 : ngroups  
    fprintf('\n%0.5g     ',g)
    reverse_categorizeNeuronalHist(N.number(find(N.gridPos == str2double(groups(g).name(6:end)))))
    for gg = 19%g+1 : ngroups         
        reverse_categorizeNeuronalHist(N.number(find(N.gridPos == str2double(groups(gg).name(6:end)))))
        auto1 = [];
        auto2 = [];
        
        cross_s1_f2 = [];
        cross_s2_f1 = [];
        
        phangles = [];
        allxcorrs = [];
        
        spike1xcorr = zeros(1,201);
        sp1xcorr_counter = 0;
        spike2xcorr = zeros(1,201);
        sp2xcorr_counter = 0;
        
        spike1xcorr_preferred = zeros(1,201);
        spike1xcorr_prefcounter = 0;
        
        sta1 = zeros(1,201);
        sta1cross = zeros(1,201);
        sta2 = zeros(1,201);
        sta2cross = zeros(1,201);
        
        spec1 = zeros(1,12);
        spec1cross = zeros(1,12);
        spec2 = zeros(1,12);
        spec2cross = zeros(1,12);
        
        phangles_s1_f2 = [];
        phangles_s2_f1 = [];

        
        counter = counter + 1;
        
        fprintf(' %0.5g',gg)
        
        %determine which channels the group number corresponds to
        g2ch = str2double(groups(g).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,g2ch);
        channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)

        %get lfp groups information
        lfpg = str2double(groups(gg).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,lfpg);
        channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)

        cd([sesdir filesep groups(g).name])
        clusters = nptDir('cluster*');
        cd(clusters(1).name);
        %get spikes
        load ispikes.mat
        sp1 = sp;
        
        
        cd([sesdir filesep groups(gg).name])
        clusters = nptDir('cluster*');
        cd(clusters(1).name);
        %get spikes
        load ispikes.mat
        sp2 = sp;
        
        cd([sesdir filesep 'lfp' filesep 'lfp2'])
        %get trials
        lfp = nptDir('*_lfp2*');
        
        tcounter = 0;
        for ttrial = total_trials
            tcounter = tcounter + 1;
            s_on = sample_on(ttrial);
            s_off = sample_off(ttrial);
            m = match(ttrial);
            
            load(lfp(ttrial).name);
            
            lfp1 = data(channels(1),:);% ./ max(data( channels(1),:));
            lfp2 = data(channels(2),:);% ./ max(data(channels(2),:));
            
            if Args.filter
                %lowpass
                [lfp1]=nptLowPassFilter(lfp1,1000,Args.filter_low,Args.filter_high);
                [lfp2]=nptLowPassFilter(lfp2,1000,Args.filter_low,Args.filter_high);
            end
            
            
            allxcorrs(tcounter,:) = xcorr(lfp1(s_on-499:m),lfp2(s_on-499:m),100,'coef');
            
            spike_data1 = sp1.data.trial(ttrial).cluster.spikes;
            s1 = ceil(spike_data1); %ceil because a zero spike occured when using "round"
            
            spike_data2 = sp2.data.trial(ttrial).cluster.spikes;
            s2 = ceil(spike_data2); %ceil because a zero spike occured when using "round"
            
            %use hilbert transform to determine instantaneous phase angle for each spike
            h1 = hilbert(lfp1); %don't need to transpose if only one channel
            h2 = hilbert(lfp2);
            
            phangles = [phangles mod(unwrap(angle(h1))-unwrap(angle(h2)),2*pi)];
            
            %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
            %COMPENT IS FLIPPED
            ha1 = angle(h1);
            ha2 = angle(h2);
            
            %compute spike triggered xcorrs, and stas
            sx1 = s1(s1 >= 101 & s1 <= size(lfp1,2) - 101);
%             sx1 = s1(s1 >= s_off-900 & s1 <= s_off-500);
            cross_s1_f2 = [cross_s1_f2 ha2(sx1)]; 
            auto1 = [auto1 ha1(sx1)];
            
            nsx1 = size(sx1,2);
            for n = 1 : nsx1
                sp1xcorr_counter = sp1xcorr_counter + 1; %use for stas as well
                
                spike1xcorr = spike1xcorr + xcorr(lfp1(sx1(n)-100 :sx1(n)+100), lfp2(sx1(n)-100 : sx1(n)+100),100,'coef');
                
                if (ha2(sx1(n)) < .78 & ha2(sx1(n)) > -1.92) && (ha1(sx1(n)) < -1.54 | ha1(sx1(n)) > 2.2432) %abs(ha1(sx1(n))) > (pi/2) & abs(ha2(sx1(n))) < (pi/2)
                    spike1xcorr_preferred = spike1xcorr_preferred + xcorr(lfp1(sx1(n)-100 :sx1(n)+100), lfp2(sx1(n)-100 : sx1(n)+100),100,'coef');
                    spike1xcorr_prefcounter =  spike1xcorr_prefcounter + 1;
                    
                end
                
                uha1 = unwrap(ha1);
                uha2 = unwrap(ha2);
                phangles_s1_f2 = [phangles_s1_f2 mod(uha1(sx1(n))-uha2(sx1(n)),(2*pi))];
                
                
                
                sta1 = sta1 + lfp1(sx1(n)-100 :sx1(n)+100);
                sta1cross = sta1cross + lfp2(sx1(n)-100 : sx1(n)+100);
                
                
                %get spike tiggered power spectrums
                [ss1,f1] = mtspectrumc(lfp1(sx1(n)-100 :sx1(n)+100),params);
                spec1 = spec1 + ss1';
                [s1cross,f1cross] = mtspectrumc(lfp2(sx1(n)-100 : sx1(n)+100),params);
                spec1cross = spec1cross  + s1cross';
                
            end
            
            
            sx2 = s2(s2 >= 101 & s2 <= size(lfp2,2) - 101);
%             sx2 = s2(s2 >= s_off-900 & s2 <= s_off-500);
            cross_s2_f1 = [cross_s2_f1 ha1(sx2)];
            auto2 = [auto2 ha2(sx2)];
            
            nsx2 = size(sx2,2);
            for n = 1 : nsx2
                sp2xcorr_counter = sp2xcorr_counter + 1;
                spike2xcorr = spike2xcorr + xcorr(lfp1(sx2(n)-100 :sx2(n)+100), lfp2(sx2(n)-100 : sx2(n)+100),100,'coef');
                
                sta2 = sta2 + lfp2(sx2(n)-100 :sx2(n)+100);
                sta2cross = sta2cross + lfp1(sx2(n)-100 : sx2(n)+100);
                
                %get spike tiggered power spectrums
                [ss2,f2] = mtspectrumc(lfp2(sx2(n)-100 :sx2(n)+100),params);
                spec2 = spec2 + ss2';
                [s2cross,f2cross] = mtspectrumc(lfp1(sx2(n)-100 : sx2(n)+100),params);
                spec2cross = spec2cross  + s2cross';
                
            end
            
        end
        spike1xcorr = spike1xcorr ./ sp1xcorr_counter;
        spike2xcorr = spike2xcorr ./ sp2xcorr_counter;
        
        sta1 = sta1 ./ sp1xcorr_counter;
        sta1cross = sta1cross ./ sp1xcorr_counter;
        
        sta2 = sta2 ./ sp2xcorr_counter;
        sta2cross = sta2cross ./ sp2xcorr_counter;
        
        
        spec1 = spec1 ./ sp1xcorr_counter;
        spec1cross = spec1cross ./ sp1xcorr_counter;
        
        spec2 = spec2 ./ sp2xcorr_counter;
        spec2cross = spec2cross ./ sp2xcorr_counter;
        
           
        
        figure
        subplot(3,2,1)
        plot(mean(allxcorrs))
        subplot(3,2,2)
        plot([-100:100],spike1xcorr,'r')
        hold on
        plot([-100:100],spike2xcorr,'k')
        subplot(3,2,3)
        plot(sta1)
        hold on
        plot(sta1cross,'k')
        subplot(3,2,4)
        plot(sta2)
        hold on
        plot(sta2cross,'k')
        
        subplot(3,2,5)
        plot(f1,spec1,'k')
        hold on
        plot(f1,spec1cross,'r')
        
        subplot(3,2,6)
        plot(f2,spec2,'k')
        hold on
        plot(f2,spec2cross,'r')
        
        
        
        figure
        subplot(4,2,1)
        imagesc(circshift(hist3([auto1;cross_s1_f2]')',[5 5]));axis xy
        subplot(4,2,2)
        imagesc(circshift(hist3([auto2;cross_s2_f1]')',[5 5]));axis xy
        subplot(4,2,3)
        rose(auto1)
        subplot(4,2,4)
        rose(auto2)
        subplot(4,2,5)
        rose(cross_s1_f2)
        
        
        subplot(4,2,6)
        rose(cross_s2_f1)
        
        subplot(4,2,7)
        hist(phangles,[0:pi/10:2*pi])
        axis tight
        
        subplot(4,2,8)
        plot([-100:100],mean(allxcorrs))
        
        
        figure
        plot([-100:100],spike1xcorr,'r')
        hold on
        plot([-100:100],spike1xcorr_preferred./spike1xcorr_prefcounter,'k')
        
    end
end





cd(sesdir)