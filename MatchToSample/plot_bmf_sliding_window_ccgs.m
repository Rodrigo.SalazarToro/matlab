function plot_bmf_sliding_window_ccgs

cd('/media/bmf_raid/data/monkey/ethyl/110714/session01')

sesdir = pwd;

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');

incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','Nlynx');

corrtrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','Nlynx');




cd([sesdir '/lfp/lfp2'])

chpairs = nptdir('channelpair*')
npairs = size(chpairs,1);

figure

for np = 407% 1: npairs %%%%%407
    load(chpairs(np).name) %172248

    
        ntrials = size(incorrtrials,2);
%     ntrials = size(corrtrials,2);
    
    
    allccgs = zeros(71,101);
    for t = incorrtrials %corrtrials%incorrtrials 
        ccgs = correlograms{t}(1:71,:);
        allccgs = allccgs + ccgs;
    end
    allccgs = allccgs ./ ntrials; %make mean
%     imagesc(allccgs',[-.3 .3])
    imagesc(allccgs')
    colorbar
    
    set(gca,'YTick',[1,25,50,75,100])
    set(gca,'YTicklabel',[-50 -25 0 25 50]);
    
    set(gca,'XTick',[1:7:65])
    set(gca,'XTicklabel',[150:350:3300])
    hold on
    %zero phase
    %im{stim}(50,:) = max_val;
    plot([1 71],[50 50],'color','k')
    hold on
    %sample
    plot([8 8],[1 101],'color','k')
    hold on
    %delay
    plot([18 18],[1 101],'color','k')
    hold on
    %first match
    plot([34 34],[1 101],'color','k')
    hold on
    %last match
    plot([42 42],[1 101],'color','k')
    hold on
    
    %first iti start
%     plot([44 44],[1 101],'color','b')
 plot([52 52],[1 101],'color','b')
    hold on
    
    %last iti end
%     plot([59 59],[1 101],'color','g')
    plot([67 67],[1 101],'color','g')
    hold on
%     
%     np
%     pause
%     cla
end
