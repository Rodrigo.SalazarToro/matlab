function [data,events,varargout] = mtsProcessSessionNeural(mtst,varargin)
% process the neural data either LFP or highpass. The output data is a
% structure with the same fields as the mtstrial object
%
% Dep.: nptReadStreamerFile
Args = struct('bband',0,'hpass',0);
Args.flags = {'bband','hpass'};
Args = getOptArgs(varargin,Args);

amplification = 2000 /1000 ; % 2000 due to the amplifier (on rig C) and 1000 due to the change from mV to uV
maxlength = 4;% assume that a mts trial should not be longer than 3 sec
numTrials = mtst.data.numSets;
% maxpoints = 3000;% max(points);

if Args.bband
    files = [nptDir('*.0*'); nptDir('*.1*')];
else
    files = nptDir('*_*');
end

totc = 0;
if Args.hpass
    [ini,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(files(1).name);
    for t = 1 : numTrials
        trace{t} = repmat(1,num_channels,2);
        points(t) = 2;
    end
else
    for t = 1 : numTrials

        [trace{t},num_channels,sampling_rate,scan_order,points(t)]=nptReadStreamerFile(files(t).name);

    end
end
maxpoints = maxlength * sampling_rate;

fields = fieldnames(mtst.data);

for f = 1 : length(fields)
    if ~strcmp(fields(f),'setNames') & ~strcmp(fields(f),'numSets')
        eval(sprintf('all%s = mtst.data.%s;',fields{f},fields{f}));
        eval(sprintf('%s = [];',fields{f}));
    end
end
neural = [];
events = 0;
for t = 1 : numTrials
    fprintf('%d ',t)
    for ch = 1 : num_channels
        tracenan = ones(1,maxpoints) * NaN;
        tracenan(1:size(trace{t}(ch,:),2)) = trace{t}(ch,:); 
        neural = [neural;tracenan];
        
        lindex = [allIndex(t,:) ((t-1)*num_channels + ch)];
        Index = [Index;lindex];
        for f = 1 : length(fields)
            if ~strcmp(fields(f),'setNames') & ~strcmp(fields(f),'Index') & ~strcmp(fields(f),'numSets') 
                
                eval(sprintf('%s = [%s;all%s(t)];',fields{f},fields{f},fields{f}));
            end
        end
        events = events + 1;
    end
end

neural = neural / amplification; % 
data.neural = single(neural);
if Args.hpass
    data = rmfield(data,'neural');
end

data.SamplingRate = sampling_rate;
data.Nchannels = scan_order';

for f = 1 : length(fields)
    if ~strcmp(fields(f),'setNames') & ~strcmp(fields(f),'numSets')
        
        eval(sprintf('data.%s = %s;',fields{f},fields{f}));
    end
end

