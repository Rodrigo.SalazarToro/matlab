function makeDescriptor(varargin)

%Used to make descriptor.txt
%run at day level
Args = struct();
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args);

load electrode_depths
depths = electrode_depths ./.0125; %convert to microns
load electrode_recorded
channels = electrode_recorded;
numb_channels = size(channels,2);

%get day
day_dir = pwd;
day = day_dir((end-5):end);

%get monkey
i = strfind(day_dir,filesep);
monkey = day_dir((i(end-1)+1):(i(end)-1));

%make descriptor for each session
sessions = nptdir('session0*');
num_sessions = size(sessions,1);

for s = 1 : num_sessions
    
    cd([day_dir filesep sessions(s).name])
    
    fid = fopen([monkey num2str(day) num2str(0) num2str(s) '_descriptor.txt'],'w+');
    
    %print header
    fprintf(fid,'%s\n','Preprocessed Raw Data');
    fprintf(fid,'%s%d\n','Number of Channels ',numb_channels);
    fprintf(fid,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Channel Number','Group Number','Grid Number','Signal Type','RF Number','Start Depth','Rec Depth','State');
    
    %print channel information
    for ch = 1 : numb_channels
        channel = channels(ch);
        if channel < 257
            fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,channel,channel,'broadband',-1,0,depths(channel),'Active');
        else
            if channel == 257
                fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'trigger',-1,0,0,'Inactive');
            elseif channel == 258
                fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'vertical1',-1,0,0,'Active');
            elseif channel == 259
                fprintf(fid,'%d\t\t%d\t\t%d\t\t%s\t\t%d\t\t%d\t\t%d\t\t%s\n',channel,0,0,'horizontal1',-1,0,0,'Active');
            else
                
            end
        end
    end
    
    fclose(fid);
    
    %delete([monkey num2str(Args.day) '_descriptor.txt~']) %delete backup if one is made
end

cd(day_dir)
