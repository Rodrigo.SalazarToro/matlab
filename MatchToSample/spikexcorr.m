function spikexcorr(varargin)

%run at session level
Args = struct('ml',0,'ide_only',1,'bmf',0,'rules',[],'bin_width',1);
Args.flags = {'ml','bmf','rules'};
[Args,modvarargin] = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
sesdir = pwd;
cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
    iti_reject = rejecTrials;
else
    iti_reject = [];
end

if Args.ml
    
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx'); %incorrect/identity
        
        N = NeuronalHist('bmf');
        sortedgroups = bmf_groups('ml'); %already kicks out noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames','save','redo');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(find(mt.data.CueObj == 55),get_reject_trials);
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        
        %get rid of incorrect trials with bad itis if the artifact
        %detection has been run on it
        if ~isempty(iti_reject)
            [~,baditi] = intersect(incorrtrials,iti_reject);
            incorrtrials(baditi) = [];
        end
        
        N = NeuronalHist('ml');
        noisy = noisy_groups;
        sortedgroups = sorted_groups('ml');
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    noisy = noisy_groups;
    [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
    [~,ii] = intersect(sortedgroups,noisy);
    sortedgroups(ii) = []; %get rid of noisy groups
    
    [~,chgroups] = intersect(N.gridPos,sortedgroups);
end
%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1 4];
        else
            rules = [2 3 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        elseif rule == 3
            trials = fixtrials;
        elseif rule == 4
            trials = incorrtrials;
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        counter = 0;
        for g = sortedgroups
            counter = counter + 1;
            fprintf('\n%0.5g     ',g)
            for gg = sortedgroups(counter:end)
                spikexcorrs = [];
                fprintf(' %0.5g',gg)
                
                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                
                %determine if pair has already been written
                spair = ['spikexcorr_g' num2strpad(g,4) 'g' num2strpad(gg,4) '.mat'];
                cd(sesdir)
                
                cd([sesdir filesep 'group' num2strpad(g,4)])
                gdir1 = pwd;
                %find clusters
                clusters1 = nptDir('cluster*');
                nclusters1 = size(clusters1,1);
                
                cd([sesdir filesep 'group' num2strpad(gg,4)])
                gdir2 = pwd;
                %find clusters
                clusters2 = nptDir('cluster*');
                nclusters2 = size(clusters2,1);
                
                ccounter = 0;
                for c = 1 : nclusters1
                    cd([gdir1 filesep clusters1(c).name]);
                    %get spikes
                    load ispikes.mat
                    sp1 = sp;
                    for cc = 1: nclusters2
                        ccounter = ccounter + 1;
                        cd([gdir2 filesep clusters2(cc).name])
                        load ispikes.mat
                        sp2 = sp;
                        
                        nbins = size([-200:Args.bin_width:200],2);
                        
                        spike_xcorr  = zeros(7,nbins);
                        spike_counter = zeros(1,7);
                        for ttrial = trials
                            s_on = sample_on(ttrial);
                            s_off = sample_off(ttrial);
                            m = match(ttrial);
                            sp1_trial = ceil(sp1.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
                            spikecount1 = sp1.data.trial(ttrial).cluster.spikecount;
                            sp2_trial = ceil(sp2.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
                            
                            for spikes = 1 : spikecount1
                                spike1 = sp1_trial(spikes);
                                stimes = sp2_trial - spike1;
                                
                                stimes([find(stimes <= -200),find(stimes >= 200)]) = [];
                                if isempty(stimes)
                                    xh = zeros(1,size([-200:Args.bin_width:200],2));
                                else
                                    xh = hist(stimes,[-200:Args.bin_width:200]);
                                end
                                
                                if spike1 > (s_on - 400) && spike1 <= s_on %fix
                                    spike_xcorr(1,:) = spike_xcorr(1,:) + xh;
                                    spike_counter(1) = spike_counter(1) + 1;
                                end
                                if spike1 > (s_off - 400) && spike1 <= s_off %sample  lock to sample off
                                    spike_xcorr(2,:) = spike_xcorr(2,:) + xh;
                                    spike_counter(2) = spike_counter(2) + 1;
                                end
                                if spike1 > s_off && spike1 < (s_off + 401) %early delay (delay400)
                                    spike_xcorr(3,:) = spike_xcorr(3,:) + xh;
                                    spike_counter(3) = spike_counter(3) + 1;
                                end
                                if spike1 > (s_off + 400) && spike1 < (s_off + 801) %sample locked delay (delay800)
                                    spike_xcorr(4,:) = spike_xcorr(4,:) + xh;
                                    spike_counter(4) = spike_counter(4) + 1;
                                end
                                if spike1 > (s_off + 200) && spike1 < (s_off + 801) %delay
                                    spike_xcorr(5,:) = spike_xcorr(5,:) + xh;
                                    spike_counter(5) = spike_counter(5) + 1;
                                end
                                if spike1 > (m - 400) && spike1 <= m %match locked delay
                                    spike_xcorr(6,:) = spike_xcorr(6,:) + xh;
                                    spike_counter(6) = spike_counter(6) + 1;
                                end
                                if spike1 > (s_on - 500) && spike1 <= m %whole trial
                                    spike_xcorr(7,:) = spike_xcorr(7,:) + xh;
                                    spike_counter(7) = spike_counter(7) + 1;
                                end
                            end
                        end
                        
                        
                        spikexcorrs.fix(ccounter,:) = single(spike_xcorr(1,:));
                        spikexcorrs.sample(ccounter,:) = single(spike_xcorr(2,:));
                        spikexcorrs.delay400(ccounter,:) = single(spike_xcorr(3,:));
                        spikexcorrs.delay800(ccounter,:) = single(spike_xcorr(4,:));
                        spikexcorrs.delay(ccounter,:) = single(spike_xcorr(5,:));
                        spikexcorrs.delaymatch(ccounter,:) = single(spike_xcorr(6,:));
                        spikexcorrs.fulltrial(ccounter,:) = single(spike_xcorr(7,:));
                        
                        spikexcorrs.fix_count(ccounter) = single(spike_counter(1));
                        spikexcorrs.sample_count(ccounter) = single(spike_counter(2));
                        spikexcorrs.delay400_count(ccounter) = single(spike_counter(3));
                        spikexcorrs.delay800_count(ccounter) = single(spike_counter(4));
                        spikexcorrs.delay_count(ccounter) = single(spike_counter(5));
                        spikexcorrs.delaymatch_count(ccounter) = single(spike_counter(6));
                        spikexcorrs.fulltrial_count(ccounter) = single(spike_counter(7));
                        
                        spikexcorrs.clusters{ccounter} = single([clusters1(c).name; clusters2(cc).name]);
                        if strmatch(clusters1(c).name(end),'s')
                            spikexcorrs.unittype1(ccounter) = 1; %single unit
                        else
                            spikexcorrs.unittype1(ccounter) = 2; %multi unit
                        end
                        if strmatch(clusters2(cc).name(end),'s')
                            spikexcorrs.unittype2(ccounter) = 1; %single unit
                        else
                            spikexcorrs.unittype2(ccounter) = 2; %multi unit
                        end
                        spikexcorrs.groups(ccounter,:) = [g gg];
                    end
                end
                
                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                
                write_info = writeinfo(dbstack);
                
                if rule == 1;
                    cd('identity')
                    save(spair,'spikexcorrs','write_info')
                elseif rule == 2;
                    cd('location')
                    save(spair,'spikexcorrs','write_info')
                elseif rule == 3
                    cd('fixation')
                    save(spair,'spikexcorrs','write_info')
                elseif rule == 4
                    cd('incorrect')
                    save(spair,'spikexcorrs','write_info')
                end
            end
        end
    end
    
end


cd(sesdir)
