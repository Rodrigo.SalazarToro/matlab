function makeBarGraphs2

load cc cc
%calculate critical p-vals (corrected)
identity = find(cc.data.Index(:,4));

delay_pvals = cc.data.Index(identity,30);
fix_pvals = cc.data.Index(identity,29);

[h, dcrit_p]=fdr_bh(delay_pvals);
[h, fcrit_p]=fdr_bh(fix_pvals);

glmdf = {'glm_delay','glm_fix'};
crit_p = [dcrit_p,fcrit_p];

cp = 1;


%medial PPC
x1 = get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 0]);
xx1 = get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 0],glmdf{cp},crit_p(cp));
yy1 = get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 0],'in_phase');
glm(1) = xx1/x1;
phase(1) = yy1/x1;

%medial/lateral PPC
x2=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[0 0]);
xx2=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[0 0],glmdf{cp},crit_p(cp));
yy2=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[0 0],'in_phase');
glm(2) = xx2/x2;
phase(2) = yy2/x2;

%lateral PPC
x3=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 1]);
xx3=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 1],glmdf{cp},crit_p(cp));
yy3=get(cc,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 1],'in_phase');
glm(3) = xx3/x3;
phase(3) = yy3/x3;


%PFC
x4=get(cc,'Number','threshold',1,'correct','stable','identity','ff');
xx4=get(cc,'Number','threshold',1,'correct','stable','identity','ff',glmdf{cp},crit_p(cp));
yy4=get(cc,'Number','threshold',1,'correct','stable','identity','ff','in_phase');
glm(4) = xx4/x4;
phase(4) = yy4/x4;

%lateral vs PFC
x5=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 0]);
xx5=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 0],glmdf{cp},crit_p(cp));
yy5=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 0],'in_phase');

x6=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 1]);
xx6=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 1],glmdf{cp},crit_p(cp));
yy6=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 1],'in_phase');

glm(5) = (xx5+xx6) / (x5+x6);
phase(5) = (yy5+yy6) / (x5+x6);


%medial vs PFC
x7=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 0]);
xx7=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 0],glmdf{cp},crit_p(cp));
yy7=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 0],'in_phase');

x8=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 1]);
xx8=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 1],glmdf{cp},crit_p(cp));
yy8=get(cc,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 1],'in_phase');

glm(6) = (xx7+xx8) / (x7+x8);
phase(6) = (yy7+yy8) / (x7+x8);

totals = sum(xx1+xx2+xx3+xx4+xx5+xx6+xx7+xx8) / sum(x1+x2+x3+x4+x5+x6+x7+x8) ;


bar_labels = {['medial PPC n = ' num2str(x1)];['mPPC / lPPC n = ' num2str(x2)];['lateral PPC n = ' num2str(x3)];['PFC n = ' num2str(x4)];['lPPC / PFC n = ' num2str(x5+x6)];['mPPC / PFC n = ' num2str(x7+x8)]};



figure

bar1 =bar((glm*100),'FaceColor','r','EdgeColor','r');
set(bar1,'BarWidth',.65)
hold on
ylim([0 100])
set(gca,'XTickLabel',bar_labels)
ylabel('%')
figure

bar1 =bar((phase*100),'FaceColor','r','EdgeColor','r');
set(bar1,'BarWidth',.65)
hold on
ylim([0 100])
set(gca,'XTickLabel',bar_labels)
ylabel('%')







