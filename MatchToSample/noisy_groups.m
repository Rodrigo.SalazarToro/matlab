function noisy = noisy_groups(varargin)

%run at session level
%outputs a list of pairs that are rejected because they are noisy

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;
noisy = [];

cd([sesdir filesep 'lfp'])

load rejectedTrials.mat

if ~isempty(rejectCH)
    noisy = rejectCH;
end

cd(sesdir)