function test_xcorr_diff_freq


%frequency
f1 = 15;
%time
windows = .1:.1:1;
wcounter = 0;
frange = 15%1:2:30;

allperms= zeros(1,1000);
for w = windows
    wcounter = wcounter + 1;
    for x = 1:1000
        t = 0:.001:w;
        counter = 0;
        ppeaks = [];
        for f2 = frange %0 to 360 degrees
            counter = counter + 1;
            
            xc = [];
            for xx = 1 : 500
                %make sine wave 1
                s1 = 1 * sin(2*pi*f1*t);
                
                s2 = 1 * sin(2*pi*f2*t + (rand*2*pi));
                
                %         plot(s1);hold on;plot(s2,'r')
                
                xc(xx,:) = xcorr(s1,s2,50,'coef');
            end
            [ppeaks(counter) llag(counter)] = find_correlogram_peak('correlogram',mean(xc));
        end
% %         hold on
% %         scatter3(frange,abs(ppeaks),wcounter*ones(1,size(frange,2)),'k','fill','s')
% %         hold on
% %         
        allperms(wcounter,x) = ppeaks(1);
    end
end
ppeaks;