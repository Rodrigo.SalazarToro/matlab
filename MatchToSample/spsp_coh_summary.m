function spsp_coh_summary

cd /media/raid/data/monkey/betty
mdir = pwd;
load alldays

allf = [];
allphi = [];
allphiwindow = [];
for d = 26%1 : 46
 cd([mdir filesep alldays{d} filesep 'session01'])
 try

scoh = spike_spike_coh('auto','ml')

numclusters = size(scoh.data.Index,1);

epoch = 5;


for nc = 1 : numclusters
    
    allf = [allf scoh.data.sigf{epoch,nc}];
    allphi = [allphi scoh.data.sigphi{epoch,nc}];
    %find
    if size(find(scoh.data.sigf{epoch,nc} >= 5 & scoh.data.sigf{epoch,nc} <= 25),2) > 5
        
       
            
        
        phi = scoh.data.sigphi{epoch,nc}(scoh.data.sigf{epoch,nc} >= 5 & scoh.data.sigf{epoch,nc} <= 25);
        allphiwindow = [allphiwindow phi];
        scoh.data.Index(nc,5:6)

    end
end

 catch
 end


end
figure
hist(allphiwindow.*(180/pi),[-180:10:180]),axis tight


allphi(allf < 5 |allf > 100) = [];
allf(allf < 5 | allf > 100) = [];
figure
hist(allf,[5:1:100])

figure
scatter(allphi,allf)

allphi;

