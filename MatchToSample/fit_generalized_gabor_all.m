function fit_generalized_gabor_all(varargin)

%run at session level
Args = struct('ml',0,'rule',1,'bmf',0,'correct_iti',0,'bettynlynx',0);
Args.flags = {'ml','bmf','correct_iti','bettynlynx'};
[Args,varargin2] = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.bmf
    N = NeuronalHist('bmf');
else
    if Args.ml
        if Args.bettynlynx
            N = NeuronalHist('ml','bettynlynx');
        else
            N = NeuronalHist('ml');
        end
    else
        N = NeuronalHist;
    end
end
npairs = nchoosek(N.chnumb,2);

if Args.bmf
    [~,~,~,~,sorted_pairs] = bmf_groups;
else
    if Args.ml
        if Args.bettynlynx
            [~,sorted_pairs,~,~] = sorted_groups('ml','bettynlynx');
        else
            [~,sorted_pairs,~,~] = sorted_groups('ml');
        end
    else
        [~,sorted_pairs,~,~] = sorted_groups;
    end
end

cd([sesdir filesep 'lfp' filesep 'lfp2'])

if Args.ml
    if Args.rule == 1
        cd identity
    elseif Args.rule == 2
        cd location
    elseif Args.rule == 3
        cd fixation
    elseif Args.rule == 4
        cd incorrect
    end
end

if Args.rule ~= 4 && ~Args.bmf
    if Args.correct_iti
        all_gabors = cell(8,npairs);
    else
        all_gabors = cell(7,npairs);
    end
else
    all_gabors = cell(9,npairs);
end

if exist('all_correlograms.mat','file')
    load all_correlograms
    counter = 0;
    for x = sorted_pairs
        for xx = 1 : size(all_gabors,1) %run through all time windows
            counter = counter + 1;
            corr_pair = avg_correlograms{xx,x};
            corr_peak = all_corrcoef{xx,x};
            corr_lag = all_phase{xx,x};
            
            
            [cf, estimates, sse, p_angle_deg, peak, xc, peak_lag, chi2] = generalized_gabor_fmin('correlogram',corr_pair,'peak',corr_peak,'lag',corr_lag);
            
            %only take real because rarely estimates are strange and an
            %imginary component is included
            gen_gabor.epoch_correlogram = corr_pair;
            gen_gabor.cf = cf;
            gen_gabor.frequency = real(estimates(3) * 1000); %Hz
            gen_gabor.sse = real(sse);
            gen_gabor.phase_angle_deg = real(p_angle_deg); %degrees
            gen_gabor.peak = real(peak);
            gen_gabor.cross_corr = real(xc);
            gen_gabor.peak_lag = real(peak_lag);
            gen_gabor.chi2 = real(round(chi2));
            
            all_gabors{xx,x} = gen_gabor;
            
%                             plot([-50:50],corr_pair);hold on;plot([-50:50],cf,'r');
%                             gen_gabor
%                             x
%                             pause
%                             close all
        end
    end
    
    write_info = writeinfo(dbstack);
    save all_gabors all_gabors write_info
end


%AUTO
if exist('all_auto_correlograms1.mat','file')
    load all_auto_correlograms1
    counter = 0;
    for x = sorted_pairs
        for xx = 1 : size(all_gabors,1) %run through all time windows
            counter = counter + 1;
            corr_pair = avg_auto_correlograms1{xx,x};
            corr_peak = all_auto_corrcoef1{xx,x};
            corr_lag = all_auto_phase1{xx,x};
            
            
            [cf, estimates, sse, p_angle_deg, peak, xc, peak_lag, chi2] = generalized_gabor_fmin('correlogram',corr_pair,'peak',corr_peak,'lag',corr_lag);
            
            %only take real because rarely estimates are strange and an
            %imginary component is included
            gen_gabor.epoch_correlogram = corr_pair;
            gen_gabor.cf = cf;
            gen_gabor.frequency = real(estimates(3) * 1000); %Hz
            gen_gabor.sse = real(sse);
            gen_gabor.phase_angle_deg = real(p_angle_deg); %degrees
            gen_gabor.peak = real(peak);
            gen_gabor.cross_corr = real(xc);
            gen_gabor.peak_lag = real(peak_lag);
            gen_gabor.chi2 = real(round(chi2));
            
            all_auto_gabors1{xx,x} = gen_gabor;
            
            %                 plot([-50:50],corr_pair);hold on;plot([-50:50],cf,'r');
            %                 round(chi2)
            %                 xc
            %                 pause
            %                 close all
        end
    end
    
    write_info = writeinfo(dbstack);
    save all_auto_gabors1 all_auto_gabors1 write_info
end


if exist('all_auto_correlograms2.mat','file')
    load all_auto_correlograms2
    counter = 0;
    for x = sorted_pairs
        for xx = 1 : size(all_gabors,1) %run through all time windows
            counter = counter + 1;
            corr_pair = avg_auto_correlograms2{xx,x};
            corr_peak = all_auto_corrcoef2{xx,x};
            corr_lag = all_auto_phase2{xx,x};
            
            
            [cf, estimates, sse, p_angle_deg, peak, xc, peak_lag, chi2] = generalized_gabor_fmin('correlogram',corr_pair,'peak',corr_peak,'lag',corr_lag);
            
            %only take real because rarely estimates are strange and an
            %imginary component is included
            gen_gabor.epoch_correlogram = corr_pair;
            gen_gabor.cf = cf;
            gen_gabor.frequency = real(estimates(3) * 1000); %Hz
            gen_gabor.sse = real(sse);
            gen_gabor.phase_angle_deg = real(p_angle_deg); %degrees
            gen_gabor.peak = real(peak);
            gen_gabor.cross_corr = real(xc);
            gen_gabor.peak_lag = real(peak_lag);
            gen_gabor.chi2 = real(round(chi2));
            
            all_auto_gabors2{xx,x} = gen_gabor;
            
            %                 plot([-50:50],corr_pair);hold on;plot([-50:50],cf,'r');
            %                 round(chi2)
            %                 xc
            %                 pause
            %                 close all
        end
    end
    
    write_info = writeinfo(dbstack);
    save all_auto_gabors2 all_auto_gabors2 write_info
end



cd(sesdir)




