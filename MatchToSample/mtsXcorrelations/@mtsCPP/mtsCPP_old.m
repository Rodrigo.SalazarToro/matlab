function obj = mtsCPP(varargin)

%Creates mtscpp object
%
% To make object at session level:
% if monkeylogic:                 c = mtsCPP('ml','auto')
% else:                           c = mtsCPP('auto')
% 
% To make object at monkey level:
%                                 cc = processDays(mtsCPP,'days',{cell array of days},'NoSites');
%index
%   parietal Vs parietal
%   parietal Vs frontal
%   frontal Vs frontal
%   trial type
%   average correlogram crossing

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsCPP';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'cpp';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end


%%
function obj = createObject(Args,varargin)
sdir = pwd;
%CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb)
%try to make object if there is an error then create and empty object
%make mts trial object
mtst = mtstrial('auto',varargin{:});

if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML');
    else
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
    end
end
%find flat channels
% % % cd ..
% % % [ich,iCHcomb,iflat,igroups] = getFlatCh;
% % % flat_channels = iflat.ch;
% % % cd(sdir)


%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sdir(end) == '1') && (Args.ml)) || ~Args.ml 
    match2sample = 1;
end

if ~isempty(mtst) && match2sample && ~isempty(all_trials)
    
    %get snr
    cd('highpass')
    load SNR_channels channel_snr_list
    snrs = channel_snr_list
    cd(sdir)
    
    %get trial info, identity == 1, location == 0
    if Args.ml
        types = [1 0]; %identity first then location
        num_rules = 2;
        rule_dirs = {'identity' 'location'};
        rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
    else %pertains to ML sessions where both rules are in one session
        types = mtst.data.Index(1,1);
        num_rules = 1;
        rule_dirs = {};
        rule_index = {'1:pairs'};
    end
    sesdir = pwd;
    %get channel info
    N = NeuronalHist;
    %get channel locations
    [parietalchannels,pcomb, parietal] = checkChannels('cohPP');
    [interchannels, icomb, inter] = checkChannels('cohInter');
    [frontalchannels, fcomb, frontal] = checkChannels('cohPF');
    %all_channels are the good channels
    if Args.ml
        all_channels = N.gridPos; %Takes all channels, bad channels must be slected out later.
    else
        all_channels = 1 : N.chnumb;
    end
    groups = all_channels;
    chnumb = N.chnumb;
    %get number of pairs
    pairs = nchoosek(chnumb,2);
    total_pairs = pairs * num_rules; %if both rules are included mult by 2
    %index contains
    %   1.)parietal parietal pairs
    %   2.)parietal frontal pairs
    %   3.)frontal frontal pairs
    %   4.)identity = 1, location = 0
    %   5.)avgerage correlogram information
    %   6-9.)epoch 1 - 4
    %   10 - 17.) slope_info
    %23 first channel
    %24 second channel
    %25 chunmbs
    pp = zeros(pairs,1);
    pf = zeros(pairs,1);
    ff =  zeros(pairs,1);
    cc1 = zeros(pairs,1);
    cc2 = zeros(pairs,1);
    chnumbs = zeros(pairs,1);
    for nr = 1 : num_rules
        cd(sesdir)
        type = types(nr);
        ri = eval(rule_index{nr}); %get indices for each rule
        p = 0;
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                %index to good channels
                cn1 = all_channels(c1);
                cn2 = all_channels(c2);
                cc = [cn1 cn2];
                p = p + 1;
                cc1(p,1) = cn1;
                cc2(p,1)= cn2;
                
                if ~isempty(snrs)
                    %add snr
                    snr_one = snrs(c1,2);
                    snr_two = snrs(c2,2);
                    if ~isempty(snr_one)
                        snr1(p,1) = snr_one;
                    else
                        snr1(p,1) = nan;
                    end
                    if ~isempty(snr_two)
                        
                        snr2(p,1) = snr_two;
                    else
                        snr2(p,1) = nan;
                    end
                end
                
                chnumbs(p,1) = chnumb;
                interc = 0;
                if (sum(c1 == parietalchannels) + sum(cn2 == parietalchannels)) == 2
                    pp(p,1) = 1;
                else
                    pp(p,1) = 0;
                    interc = 1;
                end
                if (sum(cn1 == frontalchannels) + sum(cn2 == frontalchannels)) == 2
                    ff(p,1) = 1;
                else
                    ff(p,1) = 0;
                    interc = interc + 1;
                end
                if interc == 2;
                    pf(p,1) = 1;
                else
                    pf(p,1) = 0;
                end
            end
        end
        data.Index(ri,23) = cc1;
        data.Index(ri,24) = cc2;
        
        if ~isempty(snrs)
            %add snrs
            data.Index(ri,41) = snr1;
            data.Index(ri,42) = snr2;
        end
        
        %indicate if pair has a flat channel
        for fc = 1 : pairs
% % % %             if (~isempty(intersect(cc1(fc),igroups(iflat.ch)))) || (~isempty(intersect(cc2(fc),igroups(iflat.ch))))
                fc_pairs(1,fc) = 0;
% % % %             else
% % % %                 fc_pairs(1,fc) = 1;   %%ignoring flat channels
% % % %             end
        end
        data.Index(ri,40) = fc_pairs;
        
        data.Index(ri,25) = chnumbs;
        %parietal Vs parietal
        data.Index(ri,1) = pp;
        %parietal Vs frontal
        data.Index(ri,2) = pf;
        %frontal Vs frontal
        data.Index(ri,3) = ff;
        if type == 1 %identity
            data.Index(ri,4) = ones(pairs,1);
        else %location
            data.Index(ri,4) = zeros(pairs,1);
        end
        %get average correlogram information
        cd (['lfp' filesep 'lfp2' filesep])
        lfp2dir = cd;
        if Args.ml
            cd (rule_dirs{nr});
            ruledir = pwd;
        end
        
        if ~isempty(nptdir('glm_fix.mat'))
            load glm_fix glm_fix
            load bh_fix h_fix crit_p_fix
        end
        if ~isempty(nptdir('glm_fix_loc.mat'))
            load glm_fix_loc glm_fix_loc
        end
        if ~isempty(nptdir('glm_fix_obj.mat'))
            load glm_fix_obj glm_fix_obj
        end
        if ~isempty(nptdir('glm_delay.mat'))
            load glm_delay glm_delay
            if ~isempty (nptdir('bh_time.mat'))
                load bh_delay h_delay crit_p_delay
                load bh_time h_time crit_p_time
            end
        end
        if ~isempty(nptdir('glm_delay_loc.mat'))
            load glm_delay_loc glm_delay_loc
        end
        if ~isempty(nptdir('glm_delay_obj.mat'))
            load glm_delay_obj glm_delay_obj
        end
        
        
        if Args.ml
            cd .. %get back to lfp2 folder where channelpairs are written
            %             cpairs = nptDir('channelpair**.mat');
            count = 0;
            for cp = 1:chnumb
                for cpp = (cp+1):chnumb
                    count = count + 1;
                    cpairs(count).name = [ 'channelpair' num2strpad(groups(cp),2) num2strpad(groups(cpp),2)];
                end
            end
            
            
            
            cd(lfp2dir)
        else
            cpairs = nptDir('channelpair**.mat');
        end
        
        try
            load ccTable pair_info e1_info e2_info e3_info e4_info
            %1 if average Correlogram crosses 2std, 0 if not.
            data.Index(ri,5) = pair_info;
            %epoch 1 correlogram
            data.Index(ri,6) = e1_info;
            %epoch 2 correlogram
            data.Index(ri,7) = e2_info;
            %epoch 3 correlogram
            data.Index(ri,8) = e3_info;
            %epoch 4 correlogram
            data.Index(ri,9) = e4_info;
        catch
        end
        try
            load slope_info slopes pvals
            %columns
            %1.)c s t1
            %2.)c s t2
            %3.)i s t1
            %4.)i s t2
            %slope info (positive and negative)
            data.Index(ri,10) = slopes(:,1);
            data.Index(ri,11) = slopes(:,2);
            data.Index(ri,12) = slopes(:,3);
            data.Index(ri,13) = slopes(:,4);
            %pvals(Hnull: slope == 0)
            data.Index(ri,14) = pvals(:,1);
            data.Index(ri,15) = pvals(:,2);
            data.Index(ri,16) = pvals(:,3);
            data.Index(ri,17) = pvals(:,4);
        catch
        end

            
            
% % %             load avg_correlograms avg_corrcoef
% % %             %Determines whether the average correlogram has a positive
% % %             %or a negative peak.
% % %             pos_neg_avg = cell2mat(avg_corrcoef)';
% % %             data.Index(ri,18) = pos_neg_avg;


            
if Args.ml
    
    if nr == 1 %identity rule
        cd(rule_dirs{1})
        load threshold threshold
        data.thresholds(ri,1) = threshold';
        %now using epoch 4 to asses postive and negative peaks
        load avg_correlograms_epochs
        
        %if there are [1X0 double] values then cell2mat does not work
        %(090921)
        try
            e = cell2mat(avg_corrcoef_epochs);
        catch
            for x = 1: 946
                try
                    e = cell2mat(avg_corrcoef_epochs(:,x));
                catch
                    fprintf(1,['BAD avg_correlogram_epoch ' num2str(x)])
                    for allbc = 1:4
                        if isempty(cell2mat(avg_corrcoef_epochs(allbc,x)))
                            avg_corrcoef_epochs{allbc,x} = nan;
                        end
                    end
                end
            end
        end
        e = cell2mat(avg_corrcoef_epochs);
        data.Index(ri,18) = e(4,:)';  %this is the second half of the delay period
        cd ..
    elseif nr == 2 %location rule
        cd(rule_dirs{2})
        load threshold threshold
        data.thresholds(ri,1) = threshold';
        %now using epoch 4 to asses postive and negative peaks
        load avg_correlograms_epochs
        e = cell2mat(avg_corrcoef_epochs);
        data.Index(ri,18) = e(4,:)';
        cd ..
    end
    
else
    load threshold threshold
    data.thresholds(ri,1) = threshold';
    %now using epoch 4 to asses postive and negative peaks
    load avg_correlograms_epochs
    e = cell2mat(avg_corrcoef_epochs);
    data.Index(ri,18) = e(4,:)';
end
 
        
        for q = 1 : pairs
            if nr == 1;
                data.setNames{q,1} = [lfp2dir filesep cpairs(q).name];
            else
                qq = q + pairs;
                data.setNames{qq,1} = [lfp2dir filesep cpairs(q).name];
            end
        end
        cd(sdir);
        try %if histology information is present include it in the Index. If it not then skip it
            %loads euclidean distances of pairs
            %pairs are 0 if they are not in the same area and the same
            %side of the sulcus (PS or IPS)
            %distances are in Millimeters
            load pair_distances pair_distances
            load depths depths
            
            data.Index(ri,19) = pair_distances;
            %loads the depth differences for each pair, same criterian
            %as the pair_distances (written w/ euclidPairs)
            %IN MILLIMETERS
            data.Index(ri,20) = depths;

        catch
        end
        
        
        load hist hist
        %loads histology information (written w/ euclidPairs)
        %opposite side of sulcus == 0
        %same side of sulcus == 1
        %one channel stradles sulcus == 2
        %both channels stradle sulcus == 3
        %Inter-arial == -1
        data.Index(ri,21) = hist(:,1);
        %Indicates which side of sulcus iff both channels are on
        %the same side of the sulcus.  == 1 (see above)
        %dPFC/mPPC == 0, vPFC/lPPC == 1
        data.Index(ri,22) = hist(:,2);
        
        
        
        
        ses_dirs = cd;
        try
            cd (['lfp',filesep,'lfp2']);
            load kwStats kwStats
            cd(ses_dirs)
            %stims
            data.Index(ri,26) = kwStats(1,:)';
            %location
            data.Index(ri,27) = kwStats(2,:)';
            %object
            data.Index(ri,28) = kwStats(3,:)';
        catch
        end
        %add glm info
       
            glm_f =[]; glm_d=[]; glm_interaction_delay={};
            for glms = 1:size(ri,2)
                if exist('glm_fix') ~= 0
                    glm_f(glms) = str2double(glm_fix(glms).interaction);
                end
                
                if exist('glm_delay') ~= 0
                    glm_d(glms) = str2double(glm_delay(glms).interaction);
                    glm_interaction_delay{glms} = glm_delay(glms).interaction_coefs;
                    glm_pval_delay{glms} = glm_delay(glms).interaction_coefs_pval;
                    
                    %get time term p-val
                    glm_time_delay{glms} = str2double(glm_delay(glms).time_coefs);
                    glm_pval_time_delay{glms} = str2double(glm_delay(glms).time_coefs_pval);
                    
                end
                
                %get interaction term p-vals for location and identity
                if exist('glm_fix_loc') ~= 0
                    glm_f_l(glms) = str2double(glm_fix_loc(glms).interaction);
                end
                if exist('glm_fix_obj') ~= 0
                    glm_f_o(glms) = str2double(glm_fix_obj(glms).interaction);
                end
                
                if exist('glm_delay_loc') ~= 0
                    glm_d_l(glms) = str2double(glm_delay_loc(glms).interaction);
                end
                if exist('glm_delay_obj') ~= 0
                    glm_d_o(glms) = str2double(glm_delay_obj(glms).interaction);
                end

            end
            if exist('glm_fix') ~= 0
% % %                 data.Index(ri,29) = glm_f;   %FIXATION 29 (correct stable, threshold 1)
                data.Index(ri,29) = h_fix';
            end
            if exist('glm_delay') ~= 0
                
                
                %substitute actual p-values with bh correct 
                %instead of p-values, h, is a binary vector with ones
                %indicating that p-val <= crit_p (information from bh_delay.mat
                %in identity and location folders)
                
% % %                 data.Index(ri,30) = glm_d;   %DELAY    30 (correct stable, threshold 1)

                data.Index(ri,30) = h_delay';

                data.interaction_coefs(ri,1) = glm_interaction_delay;    %save coefs for full model in a 18X1 cell
                data.interaction_pval(ri,1) = glm_pval_delay;
                
                data.time_coefs(ri,1) = glm_time_delay;
                data.time_pval(ri,1) = glm_pval_time_delay;
                               
                data.Index(ri,43) = h_time'; %this is a binary vector indicating that slope is non-zero for the time only model
                for tslope = 1:size(ri,2)
                    data.Index(tslope,44) = glm_time_delay{tslope}(2); %get coefficient from time term from time only model to determine if time is positive
                end
                
            end
            
            %fix for location model
            if exist('glm_fix_loc') ~= 0
                data.Index(ri,31) = glm_f_l;
            end
            
            %fix for identity model
            if exist('glm_fix_obj') ~= 0
                data.Index(ri,32) = glm_f_o;
            end
            
            %delay for location model
            if exist('glm_delay_loc') ~= 0
                data.Index(ri,33) = glm_d_l;
            end
            
            %delay for identity model
            if exist('glm_delay_obj') ~= 0
                data.Index(ri,34) = glm_d_o;
            end
            
    end
    data.numSets = total_pairs;
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)


data.Index = [];
data.interaction_coefs = {};
data.interaction_pvals = {};
data.time_coefs = {};
data.time_pval = {};
data.setNames = {};
data.thresholds={};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
