function obj = mtsCPP(varargin)

%Creates mtscpp object
%
% To make object at session level:
% if monkeylogic:                 c = mtsCPP('ml','auto')
% else:                           c = mtsCPP('auto')
%
% To make object at monkey level:
%                                 cc = processDays(mtsCPP,'days',{cell array of days},'NoSites');
%index
%   parietal Vs parietal
%   parietal Vs frontal
%   frontal Vs frontal
%   trial type
%   average correlogram crossing

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'train',0,'ide_only',0,'glm_data',0,'fix',0);
Args.flags = {'Auto','ml','train','ide_only','glm_data','fix'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsCPP';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'cpp';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end



function obj = createObject(Args,varargin)
sdir = pwd;

%make mts trial object
mtst = mtstrial('auto','redosetNames');
if ~isempty(mtst)
    types = mtst.data.Index(1,1);
    %for non ML data make sure that the session is ide if ide_only
    if Args.ide_only && ~Args.ml
        if types == 1
            ide = 1;
        else
            ide = 0; %location session
        end
    else
        ide = 1; %both rules are in same session for ML data
    end
end

if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML');
    else
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
    end
end

%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end

if ~isempty(mtst) && match2sample && ~isempty(all_trials) && ide
    
    
    
    
    
    %CLARK ONLY, FIND DISTANCE OF EACH CHANNEL FROM THE SULCUS (PARIETAL)
    if ~Args.ml
        ch_sulcus = make_clark_grids;
    end
    
    %get snr
    cd('highpass')
    if Args.ml
        load SNR_channels channel_snr_list
        snrs = channel_snr_list;
    else
        snrs = []; %ignore snr information for clark
    end
    cd(sdir)
    
    %get noisy channels
    cd('lfp')
    load rejectedTrials
    noisy = rejectCH; %this is the channel number, index with the groups
    cd(sdir)
    
    %get trial info, identity == 1, location == 0
    if Args.ml
        if Args.ide_only
            types = [1 0]; %identity first then location
            num_rules = 1;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        else
            types = [1 0]; %identity first then location
            num_rules = 2;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        end
    else %pertains to ML sessions where both rules are in one session
        types = mtst.data.Index(1,1);
        num_rules = 1;
        rule_dirs = {};
        rule_index = {'1:pairs'};
    end
    sesdir = pwd;
    %get channel info
    N = NeuronalHist;
    %get channel locations
    [parietalchannels,pcomb, parietal] = checkChannels('cohPP');
    [frontalchannels, fcomb, frontal] = checkChannels('cohPF');
    %all_channels are the good channels
    if Args.ml
        all_channels = N.gridPos; %Takes all channels, bad channels must be slected out later.
    else
        all_channels = 1 : N.chnumb;
    end
    
    %determine which groups have been sorted
    if Args.ml
        g = check_groups('ml');
    else
        g = check_groups;
    end
    
    [i ii] =intersect(all_channels,g);
    sorted_groups = zeros(1,size(all_channels,2));
    sorted_groups(ii) = 1;
    
    
    groups = all_channels;
    chnumb = N.chnumb;
    
    %make index of noisy channels
    noisy_ch = zeros(1,chnumb);
    noisy_ch(rejectCH) = 1;
    
    
    %depths
    rdepth = (N.recordedDepth./1000); %depths are in mm
    
    %get histology info
    mlhistology = N.number;
    gridpos = N.gridPos;
    
    
    %get number of pairs
    pairs = nchoosek(chnumb,2);
    total_pairs = pairs * num_rules; %if both rules are included mult by 2
    pp = zeros(pairs,1);
    pf = zeros(pairs,1);
    ff =  zeros(pairs,1);
    cc1 = zeros(pairs,1);
    cc2 = zeros(pairs,1);
    chnumbs = zeros(pairs,1);
    noisy_pairs = ones(pairs,1);
    ch1_sulcus = zeros(pairs,1);
    ch2_sulcus = zeros(pairs,1);
    for nr = 1 : num_rules
        cd(sesdir)
        type = types(nr);
        ri = eval(rule_index{nr}); %get indices for each rule
        p = 0;
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                %index to good groups
                cn1 = all_channels(c1); %these are group numbers
                cn2 = all_channels(c2);
                cc = [cn1 cn2];
                p = p + 1;
                cc1(p,1) = cn1;
                cc2(p,1)= cn2;
                
                %get noisy channel info
                if noisy_ch(c1) || noisy_ch(c2)
                    noisy_pairs(p,1) = 0; %1 indicates that both channels are GOOD
                end
                
                %find pairs that both groups have been sorted
                if sorted_groups(c1) && sorted_groups(c2)
                    sorted_pairs(p,1) = 1; %indicateds that both channels have been sorted
                else
                    sorted_pairs(p,1) = 0;
                end
                
                %get recorded depths
                record_depth(p,1) = rdepth(c1);
                record_depth(p,2) = rdepth(c2);
                
                mlhist(p,1) = mlhistology(c1);
                mlhist(p,2) = mlhistology(c2);
                
                gridpos1(p,1) = gridpos(c1);
                gridpos2(p,1) = gridpos(c2);
                
                
                
                %add snr
                if Args.ml
                    snr_one = snrs(c1,2);
                    snr_two = snrs(c2,2);
                else
                    snr_one = [];
                    snr_two = [];
                end
                if ~isempty(snr_one)
                    snr1(p,1) = snr_one;
                else
                    snr1(p,1) = nan;
                end
                if ~isempty(snr_two)
                    
                    snr2(p,1) = snr_two;
                else
                    snr2(p,1) = nan;
                end
                
                chnumbs(p,1) = chnumb;
                interc = 0;
                
                if Args.ml
                    
                    parietalchannels = all_channels(all_channels > 32);
                    frontalchannels = all_channels(all_channels < 33);
                    if (sum(cn1 == parietalchannels) + sum(cn2 == parietalchannels)) == 2
                        pp(p,1) = 1;
                    else
                        pp(p,1) = 0;
                        interc = 1;
                    end
                    if (sum(cn1 == frontalchannels) + sum(cn2 == frontalchannels)) == 2
                        ff(p,1) = 1;
                    else
                        ff(p,1) = 0;
                        interc = interc + 1;
                    end
                    if interc == 2;
                        pf(p,1) = 1;
                    else
                        pf(p,1) = 0;
                    end
                else
                    if strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'P',1)
                        pp(p,1) = 1;
                        %get distance from sulcus for each channel, CLARK ONLY
                        ch1_sulcus(p,1) = ch_sulcus(c1);
                        ch2_sulcus(p,1) = ch_sulcus(c2);
                    else
                        pp(p,1) = 0;
                        interc = 1;
                    end
                    if strncmpi(N.cortex(c1),'F',1) && strncmpi(N.cortex(c2),'F',1)
                        ff(p,1) = 1;
                    else
                        ff(p,1) = 0;
                        interc = interc + 1;
                    end
                    if strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'F',1) %parietal channels are always first
                        pf(p,1) = 1;
                    else
                        pf(p,1) = 0;
                    end
                end
            end
        end
        data.Index(ri,10) = sorted_pairs;
        data.Index(ri,40) = noisy_pairs; %keep track of all the noisy pairs, 1 indicates that both channels are GOOD
        
        
        %clark sulcus information
        data.Index(ri,16) = ch1_sulcus;
        data.Index(ri,17) = ch2_sulcus;
        
        %parietal Vs parietal
        data.Index(ri,1) = pp;
        %parietal Vs frontal
        data.Index(ri,2) = pf;
        %frontal Vs frontal
        data.Index(ri,3) = ff;
        
        if type == 1 %identity
            data.Index(ri,4) = ones(pairs,1);
        else %location
            data.Index(ri,4) = zeros(pairs,1);
        end
        
        
        data.Index(ri,20) = 1:p; %keep track of the pair number for each pair on each day, useful in get and plot functions
        
        %channel numbers
        data.Index(ri,23) = cc1;
        data.Index(ri,24) = cc2;
        
        %grid numbers
        data.Index(ri,14) = gridpos1;
        data.Index(ri,15) = gridpos2;
       
        %depth informatino
        data.Index(ri,5) = record_depth(:,1); %first channel
        data.Index(ri,6) = record_depth(:,2); %second channel
        
        %histology information
        data.Index(ri,7) = mlhist(:,1); %first channel
        data.Index(ri,8) = mlhist(:,2); %second channel
        
        %if pp, indicate which side of the ips
        for sulcus = ri
            if data.Index(sulcus,1) == 1
                hnumber1 = data.Index(sulcus,7);
                hnumber2 = data.Index(sulcus,8);
                if (hnumber1 == 8 || hnumber1 == 10 || hnumber1 == 12 || hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 8 || hnumber2 == 10 || hnumber2 == 12 || hnumber2 == 11 || hnumber2 == 13)
                    if (hnumber1 == 8 || hnumber1 == 10 || hnumber1 == 12) && (hnumber2 == 8 || hnumber2 == 10 || hnumber2 == 12)
                        data.Index(sulcus,13) = 1; %medial/medial
                    elseif (hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 11 || hnumber2 == 13)
                        data.Index(sulcus,13) = 2; %lateral/lateral
                    else
                        data.Index(sulcus,13) = 3; %medial/lateral
                    end
                end
            end
        end
        
        
        
        
        if ~isempty(snrs)
            %add snrs
            data.Index(ri,41) = snr1;
            data.Index(ri,42) = snr2;
        end
        
        
        
        data.Index(ri,25) = chnumbs;
        
        %get average correlogram information
        cd (['lfp' filesep 'lfp2' filesep])
        lfp2dir = cd;
        if Args.ml
            cd (rule_dirs{nr});
            ruledir = pwd;
        end
        % %
        % %
        % %         if exist('glm_fix.mat')
        % %             load glm_fix glm_fix
        % %             load bh_fix h_fix crit_p_fix
        % %         end
        if ~isempty(nptdir('glm_fix_loc.mat'))
            load glm_fix_loc glm_fix_loc
        end
        if ~isempty(nptdir('glm_fix_obj.mat'))
            load glm_fix_obj glm_fix_obj
        end
        
        
        if Args.train
            if Args.fix
                if exist('glm_fix_train.mat')
                    load glm_fix_train glm_fix_train
                    glm_delay = glm_fix_train;
                end
            else
                if exist('glm_delay_train.mat')
                    load glm_delay_train glm_delay_train
                    glm_delay = glm_delay_train;
                end
            end
        else
            if exist('glm_delay.mat')
                
                load glm_delay glm_delay
                
                % % %             load bh_delay h_delay crit_p_delay
                % % %             load bh_time h_time crit_p_time
            end
        end
        if ~isempty(nptdir('glm_delay_loc.mat'))
            load glm_delay_loc glm_delay_loc
        end
        if ~isempty(nptdir('glm_delay_obj.mat'))
            load glm_delay_obj glm_delay_obj
        end
        
        
        if Args.ml
            cd .. %get back to lfp2 folder where channelpairs are written
            %             cpairs = nptDir('channelpair**.mat');
            count = 0;
            for cp = 1:chnumb
                for cpp = (cp+1):chnumb
                    count = count + 1;
                    cpairs(count).name = [ 'channelpair' num2strpad(groups(cp),2) num2strpad(groups(cpp),2)];
                end
            end
            cd(lfp2dir)
        else
            cpairs = nptDir('channelpair**.mat');
        end
        
        %get rule specific information below
        if Args.ml
            cd(rule_dirs{nr})
        end
        
        %get epoch phase information from generalized gabor fitting
        if exist('epoch_gabors.mat')
            load epoch_gabors
            %INDEX 9: phase_angle for epoch 4
            for pa = ri
                pangle(pa) = epoch_gabors{pa}{4}.phase_angle_deg;
            end
            data.Index(ri,9) = pangle;
        end
        
        load threshold threshold
        data.thresholds(ri,1) = threshold';
        
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         load delay_gabors
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         for rri = ri
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             if rri <= size(delay_gabors,2)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 if isempty(delay_gabors{rri})
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     data.Index(rri,27) = nan;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 else
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                     data.Index(rri,27) = delay_gabors{rri}.phase_angle_deg;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         
        
        %now using epoch 4 to asses postive and negative peaks
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         load delay_correlograms
        
        %if there are [1X0 double] values then cell2mat does not work
        %(090921)
        
        %find any empty cells
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         empty_cells_c = cellfun(@isempty,delay_corrcoef);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         %replace with nan
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         for emptycells = 1 : size(delay_corrcoef,2)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             if empty_cells_c(emptycells)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 delay_corrcoef{emptycells} = nan;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %                 delay_phase{emptycells} = nan;
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %             end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         end
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         e = cell2mat(delay_corrcoef);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         ep = cell2mat(delay_phase);
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         %INDEX: 18,19 corrcoefs and phases for average epoch correlograms
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         data.Index(ri,18) = e';  %this is the second half of the delay period (epoch 4)
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %         data.Index(ri,19) = ep';
  
        %INDEX 11: epoch_surrogates positive threshold epoch4
        if exist('epoch_surrogates.mat')
            load epoch_surrogates
            p = prctile(epoch1',[.5 99.5]);
            data.Index(ri,11) = p(2,:)';
        end
        
        %INDEX 12: global_epoch_surrogates
        if exist('global_epoch_surrogates.mat')
            load global_epoch_surrogates
            p = prctile(epoch1,[.5 99.5]);
            data.Index(ri,12) = p(2);
        end
        
        cd(lfp2dir)
        
        for q = 1 : pairs
            if nr == 1;
                data.setNames{q,1} = [lfp2dir filesep cpairs(q).name];
            else
                qq = q + pairs;
                data.setNames{qq,1} = [lfp2dir filesep cpairs(q).name];
            end
        end
        cd(sdir);

        %add glm info
        if Args.glm_data  %this needs to be removed
            glm_f =[]; glm_d=[]; glm_interaction_delay={};
            for glms = 1:size(ri,2)
                %
                %             glm_f(glms) = str2double(glm_fix(glms).interaction);
                %
                
                if ~isempty(glm_delay{glms})
                    empty = 0;
                    glm_d(glms) = str2double(glm_delay{glms}.interaction);
                    glm_interaction_delay{glms} = glm_delay{glms}.interaction_coefs;
                    glm_pval_delay{glms} = glm_delay{glms}.interaction_coefs_pval;
                    
                    %get time term p-val
                    glm_time_delay{glms} = str2double(glm_delay{glms}.time_coefs);
                    glm_pval_time_delay{glms} = str2double(glm_delay{glms}.time_coefs_pval);

                    dnd_delay(glms) = str2double(glm_delay{glms}.interaction);
                else
                    empty = 1;
                    glm_d(glms) = nan;
                    glm_interaction_delay{glms} = nan;
                    glm_pval_delay{glms} = nan;
                    
                    %get time term p-val
                    glm_time_delay{glms} = nan;
                    glm_pval_time_delay{glms} = nan; 
                    
                     dnd_delay(glms) = nan;
                end
                
                %get interaction term p-vals for location and identity
                if exist('glm_fix_loc') ~= 0
                    glm_f_l(glms) = str2double(glm_fix_loc(glms).interaction);
                end
                if exist('glm_fix_obj') ~= 0
                    glm_f_o(glms) = str2double(glm_fix_obj(glms).interaction);
                end
                
                if exist('glm_delay_loc') ~= 0
                    glm_d_l(glms) = str2double(glm_delay_loc(glms).interaction);
                end
                if exist('glm_delay_obj') ~= 0
                    glm_d_o(glms) = str2double(glm_delay_obj(glms).interaction);
                end
                
            end
            %real interaction p-vals for delay period
            data.Index(ri,28) = glm_d;
            
            %data.Index(ri,29) = h_fix';
            %substitute actual p-values with bh correct
            %instead of p-values, h, is a binary vector with ones
            %indicating that p-val <= crit_p (information from bh_delay.mat
            %in identity and location folders)
            
            %these are the p-values for the drop in deviance test
            data.Index(ri,30) = dnd_delay';
            
            data.interaction_coefs(ri,1) = glm_interaction_delay;    %save coefs for full model in a 18X1 cell
            data.interaction_pval(ri,1) = glm_pval_delay;
            
            data.time_coefs(ri,1) = glm_time_delay;
            data.time_pval(ri,1) = glm_pval_time_delay;
            
            for tslope = 1:size(ri,2)
                if ~isnan(glm_time_delay{tslope})
                    data.Index(tslope,44) = glm_time_delay{tslope}(2); %get coefficient from time term from time only model to determine if time is positive
                else
                    data.Index(tslope,44) = nan;
                end
            end
            
            
            %fix for location model
            if exist('glm_fix_loc') ~= 0
                data.Index(ri,31) = glm_f_l;
            end
            
            %fix for identity model
            if exist('glm_fix_obj') ~= 0
                data.Index(ri,32) = glm_f_o;
            end
            
            %delay for location model
            if exist('glm_delay_loc') ~= 0
                data.Index(ri,33) = glm_d_l;
            end
            
            %delay for identity model
            if exist('glm_delay_obj') ~= 0
                data.Index(ri,34) = glm_d_o;
            end
            
        else
            data.interaction_coefs = {};
            data.interaction_pval = {};
            data.time_coefs = {};
            data.time_pval = {};
        end
    end
    
    % % %     data.Index(ri,26) = [];
   
    data.numSets = total_pairs;
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)


data.Index = [];
data.interaction_coefs = {};
data.interaction_pvals = {};
data.time_coefs = {};
data.time_pval = {};
data.setNames = {};
data.thresholds={};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
