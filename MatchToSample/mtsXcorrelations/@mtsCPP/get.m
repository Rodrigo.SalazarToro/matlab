function [r,varargout] = get(obj,varargin)

%get function for mtsCPP
%Object level is session object
%gets channel pairs, plot function is then used to get specific trials
Args = struct('Number',1,'ObjectLevel',0,'pp',0,'pf',0,'ff',0,'hist_numbers',[],'identity',0,'location',0,...
    'slope',[],'threshold',0,'correct',0,'incorrect',0,...
    'in_phase',0,'anti_phase',0,'distance',[],'mlhist',[],'glm_fix',0,'glm_delay',[],'glm_fix_identity',[],'glm_fix_location',[],...
    'glm_delay_identity',[],'glm_delay_location',[],'snr_cutoff',[],'keep_noisy',0,'ml',0,'rule',0,'record_depth',[],'sorted',0,'sig_epoch_correlogram',0,'ips',[],'sulcus',[]);


%rule: identity(1), location(2)

Args.flags = {'ObjectLevel','pp','pf','ff','identity','location','threshold','correct','incorrect','in_phase','anti_phase','keep_noisy','ml','time_pval','glm_fix','sorted','sig_epoch_correlogram'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;
%Arguments
%   'pp': parietal Vs parietal
%   'pf': parietal Vs frontal
%   'ff': frontal Vs frontal
%   'hist_numbers': selects only pairs with both channels matching the hist_number/s. Numbers are made in NeuronalHist.m

if Args.Number
    
    %rtemp1
    if Args.pp
        rtemp1 = find(obj.data.Index(:,1) == 1);
    else
        rtemp1 = [1 : length(obj.data.Index(:,1))];
    end
    
    %rtemp2
    if Args.pf
        rtemp2 = find(obj.data.Index(:,2) == 1);
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp3
    if Args.ff
        rtemp3 = find(obj.data.Index(:,3) == 1);
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp4
    if Args.location || Args.identity
        if Args.location
            rtemp4 = find(obj.data.Index(:,4) == 0);
        end
        if Args.identity
            rtemp4 = find(obj.data.Index(:,4) == 1);
        end
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp5
    if ~isempty(Args.hist_numbers)
        rtemp5 = [];
        %histology information is in Index 7,8
        for hn = Args.hist_numbers
            for hnn = Args.hist_numbers
                histi = find(obj.data.Index(:,7) == hn);
                histii = find(obj.data.Index(:,8) == hnn);
                rtemp5 = [rtemp5 intersect(histi,histii)'];
            end
        end
        rtemp5 = sort(rtemp5);
    else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp6
    if Args.sig_epoch_correlogram
         rtemp6 = find(abs(obj.data.Index(:,18)) > .035);  %this is the second half of the delay period
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    
    
    %rtemp7
    if ~isempty(Args.ips)
        rtemp7 = find(obj.data.Index(:,13) == Args.ips);
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp8,rtemp9
    %checks to see if both channels are Args.sulcus distance from the ips
    if ~isempty(Args.sulcus) && ~Args.ml
        rtemp8 = find(obj.data.Index(:,16) > Args.sulcus);
        rtemp9 = find(obj.data.Index(:,17) > Args.sulcus);
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    
    
    if ~isempty(Args.slope)
        rtemp11 = find(obj.data.Index(:,44) >= Args.slope);
    else
        rtemp11 = [1 : size(obj.data.Index,1)];
    end
    %%
    
    if Args.in_phase || Args.anti_phase
        if Args.in_phase
            rtemp12 = find(obj.data.Index(:,18) >= 0 );
        else
            rtemp12 = find(obj.data.Index(:,18) <= 0 );
        end
    else
        rtemp12 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.distance)
        %Finds pairs with euclidean distances inbetween entries [x y]
        %THESE ARE DIFFERENCES
        r = find(obj.data.Index(:,19) >= Args.distance(:,1));%x
        rr = find(obj.data.Index(:,19) <= Args.distance(:,2));%y
        rtemp13 = intersect(r,rr);
    else
        rtemp13 = [1 : size(obj.data.Index,1)];
    end
    
    
    %fixation with 9stim model
    if Args.glm_fix
        % %         rtemp18 = find(obj.data.Index(:,29) <= Args.glm_fix);
        %substituted binary vector at .05 cut off (bh corrected)
        rtemp18 = find(obj.data.Index(:,29));
    else
        rtemp18 = [1 : size(obj.data.Index,1)];
    end
    
    %fix for location model
    if Args.glm_fix_location
        rtemp19 = find(obj.data.Index(:,31) <= Args.glm_fix_location);
    else
        rtemp19 = [1 : size(obj.data.Index,1)];
    end
    
    %fix for identity model
    if Args.glm_fix_identity
        rtemp20 = find(obj.data.Index(:,32) <= Args.glm_fix_identity);
    else
        rtemp20 = [1 : size(obj.data.Index,1)];
    end
    
    
    
    
    %delay for 9stim model
    if Args.glm_delay
% %         rtemp21 = find(obj.data.Index(:,30) <= Args.glm_delay);
        %substituted binary vector at .05 cut off (bh corrected)
        rtemp21 = find(obj.data.Index(:,30) < Args.glm_delay);

    else
        rtemp21 = [1 : size(obj.data.Index,1)];
    end
    
    %delay for location model
    if Args.glm_delay_location
        rtemp22 = find(obj.data.Index(:,33) <= Args.glm_delay_location);
    else
        rtemp22 = [1 : size(obj.data.Index,1)];
    end
    
    %delay for identity model
    if Args.glm_delay_identity
        rtemp23 = find(obj.data.Index(:,34) <= Args.glm_delay_identity);
    else
        rtemp23 = [1 : size(obj.data.Index,1)];
    end
    

    if ~isempty(Args.snr_cutoff)
        snr_ch1 = find(obj.data.Index(:,41) >= Args.snr_cutoff);
        snr_ch2 = find(obj.data.Index(:,42) >= Args.snr_cutoff);
        rtemp24 = intersect(snr_ch1,snr_ch2)';
    else
        rtemp24 = [1 : size(obj.data.Index,1)];
    end
    
        %KICK OUT NOISY CHANNELS unless otherwise specified
    if Args.keep_noisy
        rtemp25 = find(obj.data.Index(:,40))'; %index has a 1 if both channels are NOT flat
    else
        rtemp25 = [1 : size(obj.data.Index,1)];
    end
    
    
    %only take channels that are > specific depths
    if ~isempty(Args.record_depth)
        rtemp26 = find(obj.data.Index(:,5) > Args.record_depth(1))';
        rtemp27 = find(obj.data.Index(:,6) > Args.record_depth(2))';
    else
        rtemp26 = [1 : size(obj.data.Index,1)];
        rtemp27 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.mlhist)
        if Args.mlhist(1) == Args.mlhist(2)
            rtemp28 = [find(obj.data.Index(:,7) == Args.mlhist(1))]';
            rtemp29 = [find(obj.data.Index(:,8) == Args.mlhist(2))]';
        else
            rtemp28 = [find(obj.data.Index(:,7) == Args.mlhist(1)); find(obj.data.Index(:,8) == Args.mlhist(1))]';
            rtemp29 = [find(obj.data.Index(:,7) == Args.mlhist(2)); find(obj.data.Index(:,8) == Args.mlhist(2))]';
        end
    else
        rtemp28 = [1 : size(obj.data.Index,1)];
        rtemp29 = [1 : size(obj.data.Index,1)];
    end
    
    
    %takes on pairs with both groups sorted
    if Args.sorted
        rtemp30 = find(obj.data.Index(:,10) == 1);
    else
        rtemp30 = [1 : size(obj.data.Index,1)];
    end
    
    
    
    %unused rtemps
    
    

    
    rtemp10 = [1 : size(obj.data.Index,1)];
    rtemp14 = [1 : size(obj.data.Index,1)];
    rtemp15 = [1 : size(obj.data.Index,1)];
    rtemp16 = [1 : size(obj.data.Index,1)];
    rtemp17 = [1 : size(obj.data.Index,1)];
    
    
    
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect...
        (rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect...
        (rtemp10,intersect(rtemp11,intersect(rtemp12,intersect(rtemp13,intersect...
        (rtemp14,intersect(rtemp15,intersect(rtemp16,intersect(rtemp17,intersect...
        (rtemp18,intersect(rtemp19,intersect(rtemp20,intersect(rtemp21,intersect...
        (rtemp22,intersect(rtemp23,intersect(rtemp24,intersect(rtemp25,intersect(rtemp26,intersect(rtemp27,intersect(rtemp28,intersect(rtemp29,rtemp30)))))))))))))))))))))))))))));
    
    r = length(varargout{1});
    
    
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end

