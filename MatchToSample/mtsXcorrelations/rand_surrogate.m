function rand_surrogate

iterations = 10000;
trials = 600;
chnumb = 9;

%%%%%%%%%%%%%%Make Surrogates%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    load channel_matrix channel_matrix
    avg_surrogates = cell(1,36);
    

tic
    surrogate = 0;
    for cc1 = 1 : chnumb
      
        %loads the each channel 
        data1 = channel_matrix{cc1};
        
        for cc2 = (cc1 + 1) : chnumb
         
            data2 = channel_matrix{cc2};
            
            surrogate = surrogate + 1;
            pair = 0;
            all_correlations = zeros(trials,1);
            
            
            
            
            
            for iter = 1 : iterations
                
                
                
                                  t1 = randperm(trials);
                                  t2 = randperm(trials);
                                    
                                  d1 = data1(t1(1,1),:);

                                  d2 = data2(t2(1,1),:);

                                  c = xcorr(d1,d2,0,'coef');

                                  pair = pair + 1;  
                                  all_correlations(pair,:) = c;
                                  
                 
                  
            end
            
            
            
            
            
            
            
            avg_surrogates{surrogate} = all_correlations;
            surrogate
    
        end
        
    end

    toc
    save avg_surrogates avg_surrogates


        
   