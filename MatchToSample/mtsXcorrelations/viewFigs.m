function viewFigs

%used to view .fig files for channelpairs and stimuli

global figs scrsz fig_num

figs = nptdir('*.fig');

scrsz = get(0,'ScreenSize');
fig_num=  1;
open(figs(1).name);
h = gcf;
%Global handle definitions
global  t_fig  t_back t_next t_c1 
 

%Create figure
t_fig = figure('Position',[1075 615 200 100]);  


%Create edit text box
t_c1 = uicontrol(t_fig,'Style','edit','String',1,'units','normalized',...
    'Position',[.25 .5 .5 .2],'Callback',@c1_callback,'fontsize',10,'fontweight','b');

%Create pushbutton 'back'
t_back = uicontrol(t_fig,'Style','pushbutton','String','Back','units','normalized',...
    'Position',[.0 .5 .2 .2],'Callback',@back_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'next'
t_next = uicontrol(t_fig,'Style','pushbutton','String','Next','units','normalized',...
    'Position',[.8 .5 .2 .2],'Callback',@next_callback,'fontsize',10,'fontweight','b');

    
    function next_callback(hObject,eventdata)
        
                fig_num = fig_num + 1;
                
                delete(h)
               
            	open(figs(fig_num).name);
                h = gcf;
                
                UISTACK(t_fig, 'up')
                set(t_c1,'String',num2str(fig_num));

    end



    function back_callback(hObject,eventdata) 
        
                fig_num = fig_num - 1;
                
                delete(h)
            	open(figs(fig_num).name);
                h = gcf;
                UISTACK(t_fig, 'up')
                set(t_c1,'String',num2str(fig_num));
        
    end

    function c1_callback(hObject,eventdata) 
        
                c = get(t_c1,'String');
                
                delete(h)
            	open(c);
                h = gcf;
                UISTACK(t_fig, 'up')
        
        
    end


end
