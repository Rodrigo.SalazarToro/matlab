function delaySlopes(ml)

%calculate slope during the delay period
daydir = cd;
ses = nptDir('session0*');
for sessions = 1
    %get sessions   ses.name
    cd ([daydir filesep ses(sessions).name]);
    [NeuroInfo] = NeuronalChAssign;
    %Get number of channels
    chnumb=length(NeuroInfo.channels);
    %calculate number of pairs (n choose 2)
    pairs = (chnumb*(chnumb-1)) / 2;
    %create trial object
    if ~ml
        mt=mtstrial('auto');
        rules = {};
        r=[1];
    else
        mt = mtstrial('auto','ML','RTfromML');
        rules = [{'identity' 'location'}];
        r=[1 2];
    end
    %Get number of trials
    NumberOfTrials = mt.data.numSets;
    
    cd ('lfp');
    cd ('lfp2');
    lfp2dir = pwd;
    
    %run for both rules if ML
    for rr = r
        cd([lfp2dir filesep rules{rr}])
        load threshold threshold
        if ml
            ruledir = cd;
            cd ..
        else
            ruledir = cd;
        end
        cd ..
        lfp = cd;
        cd ..
        cd(lfp)
        slopes = [];
        pvals = [];
        pair = 0;
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                column = 0;
                pair = pair + 1;
                
                %get rule information from mts object
                type = mt.data.Index(:,1);
                trials_identity = (find(type == 1));
                trials_location = (find(type == 2));
                trials_all = [{trials_identity'} {trials_location'}];
                
                
                for br = [1 0]   %behavioral response (correct, incorrect)
                    if br == 1
                        res = 'correct';
                    else
                        res = 'incorrect';
                    end
                    for stability = 1  %transition or stable (stable, with transition option)
                        cd(lfp)
                        if stability == 0
                            tss1= 'transition';
                        else
                            tss1= 'stable';
                        end
                        if ~ml
                            ind = mtsgetTrials(mt,'BehResp',br,tss1);
                        else
                            ind = mtsgetTrials(mt,'BehResp',br,tss1,'ML');
                        end
                        
                        
                        allTrials = trials_all{rr};
                        index = [];
                        for i = 1 : size(trials_all{rr},2)
                            if ~isempty(intersect(allTrials(i),ind))
                                index = [index i];
                            end
                        end
                        
                        %these ar the trials to use
                        ind = index;
                        
                        
                        cd(ruledir)
                        sessionname = 'channelpair12.mat';
                        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
                        load(trial, 'corrcoefs','phases')
                        corrcoefs = abs(corrcoefs(:,ind));
                        phases = phases(:,ind);  %necessary for trials with negative peaks
                        for thresh = 1:2  %thresholds
                            th = threshold{pair};
                            thresholds = th(:,thresh);
                            t=num2str(thresholds);
                            [rows columns] = size(corrcoefs);  %will be the same for the phases
                            
                            cors = [];
                            phase = [];
                            number=[];
                            
                            for yyy = 1:columns
                                corrss = [];
                                phasess= [];
                                x=[];
                                ntrials = [];
                                for xxx = 1:rows %50ms bins w/ a max at 3000ms
                                    %creates a row vector that contains the number of trials
                                    %for each bin
                                    ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                                    %Determine if threshold is crossed
                                    if  corrcoefs(xxx,yyy) > thresholds
                                        %If it is then keep the number
                                        bb = corrcoefs(xxx,yyy);
                                        pp = phases(xxx,yyy);
                                        yy = 1;
                                    else
                                        %Else, input NaN
                                        bb = NaN;
                                        pp = NaN;
                                        yy = 0;
                                    end
                                    x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
                                    corrss = cat(1,corrss, bb);
                                    phasess = cat(1,phasess,pp);
                                end
                                number = cat(2,number,x);
                                cors = cat(2,cors,corrss);
                                phase = cat(2,phase,phasess);
                            end
                            
                            %%Calculate the mean for the corcoefs
                            xy=(cors');
                            
                            [r c] = size(xy);
                            meanss=[];
                            for ccc = 1:c
                                mmeans=[];
                                for rrr = 1:r
                                    q =(xy(rrr,ccc));
                                    qq = isnan(q);
                                    if qq == 1
                                    else
                                        mmeans = cat(1,mmeans,q);
                                    end
                                    xxxx = mean(mmeans);
                                end
                                meanss = [meanss xxxx];
                            end
                            %%Calculate the means for the phases
                            xys=(phase');
                            [rs cs] = size(xys);
                            meansss=[];
                            for cccs = 1:cs
                                mmeanss=[];
                                for rrrs = 1:rs
                                    qs =(xys(rrrs,cccs));
                                    qqs = isnan(qs);
                                    if qqs == 1
                                    else
                                        mmeanss = cat(1,mmeanss,qs);
                                    end
                                    xxxxs = mean(mmeanss);
                                end
                                meansss = [meansss xxxxs];
                            end
                            n =[];
                            c = length(ntrials);
                            for z = 1 : c
                                
                                n = cat(2,n,length(ind));
                                
                            end
                            numbertrials = n - ntrials;
                            number = number';
                            tttt = [];
                            for zz = 1 : length(numbertrials)
                                
                                if numbertrials(1,zz) == 0
                                    tt = 0;
                                else
                                    tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
                                    %Determines acutal percentage of trials based on trials left
                                end
                                tttt = cat(2,tttt,tt);
                            end
                            %separate out the end of sample to first match
                            t = tttt(18:34);
                            time = [1:17];
                            stats = regstats(t,time);
                            pval = stats.fstat.pval;
                            %finds the slope of the regression line
                            s = diff(stats.yhat);
                            slope = s(1);
                            column = column + 1;
                            pvals(pair,column) = pval;
                            slopes(pair,column) = slope;
                            
                        end
                        
                    end
                end
                
            end
        end
        save slope_info slopes pvals
    end
end
    
    
    
    
    
    