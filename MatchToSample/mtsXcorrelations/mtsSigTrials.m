function mtsSigTrials(c1,c2,threshh,sessionname,object,location,p1,p2)

b1 = 1;
ts1 =1;
match = 0;

%p1 and p2 are for the subplot(p1,p2,x)

%Object = [0 - 3] 0, will be all objects and 1-3 is individual objects
%Location =  "  "
%Thresh is a character ie: '1'
%function  avgPercents(b1,ts1,c1,c2,threshh,sessionname,match,object,location)

%Used by mtsCPP to view the averages for either pp,ff,or pf, depending on which being viewed (c1,c2) 
load threshold threshold
 
    directory = cd; 
    cd(directory(1:end-4))

    mt=mtstrial('auto');
    
    objs =  unique(mt.data.CueObj);
    
    locs =  unique(mt.data.CueLoc);
    [frontalchannels, comb, frontal] = checkChannels('cohPF');
    [interchannels, comb, inter] = checkChannels('cohInter');
    [parietalchannels,comb, parietal] = checkChannels('cohPP');

    cd(directory)
    
    if ts1 == 0
        tss1= 'transition';
    else
        tss1= 'stable';
    end

    %Determine which stimulus to use objects then locations 0 - 9
    if object == 0 && location == 0
        %Get files based on behavioral response and stimulus
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0);
    elseif object == 0
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueLoc',locs(location));
    elseif location == 0
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object));
    else
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object),'CueLoc',locs(location));
    end


    %Get files based on match time, 4 times:
    %1800-1900,1901-2000,2001,2100,2101,2200ms
    m = mt.data.MatchOnset(ind,:);
    matchonset = round(m);

    if match == 0
        ind = ind;
    elseif match == 1
        matches = find(1800 <= matchonset & matchonset <= 1900);
        ind = ind(:,matches);
    elseif match == 2
        matches = find(1900 <= matchonset & matchonset <= 2000);
        ind = ind(:,matches);
    elseif match == 3
        matches = find(2000 <= matchonset & matchonset <= 2100);
        ind = ind(:,matches);
    elseif match == 4
        matches = find(2100 <= matchonset);
        ind = ind(:,matches);
    end

     
    %Determine whether the pair is pp,ff,pf
    if sum(c1 == frontal(:,1)) > 0
        cc = frontal;
        dd = size(frontal,1);
        t = 'Frontal Vs Frontal';

    elseif sum(c2 == parietal(:,2)) > 0
        cc = parietal;
        dd = size(parietal,1);
        t = 'Parietal Vs Parietal';

    else
        cc = inter;
        dd = size(inter,1);
        t = 'Parietal Vs Frontal';
    end

   
 percents = [];

 for  allpairs = 1:dd
     
     c1 = cc(allpairs,1)
     c2 = cc(allpairs,2)

        

        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
        
        load(trial, 'corrcoefs')
        corrcoefs = corrcoefs(:,ind);
    
        
        
     thresh = str2double(threshh);   
    
     %Index threshold for the correct threshold   
     if thresh > 0 
        c1c2=0;
        for cc1 = 1:c1
            for cc2 = (cc1+1) : c2
                
               c1c2 = c1c2 +1; 
                
            end
        end
    threshind = (c1c2*4 - 4)+ thresh;
    thresholds =(threshold(1,threshind));
    else
        thresholds = 0;
    end


   
    [rows columns] = size(corrcoefs);  %will be the same for the phases
    cors = [];

    number=[];

    for yyy = 1:columns
        corrss = [];
     
        x=[];
        ntrials = [];

             for xxx = 1:rows %50ms bins w/ a max at 3000ms
                  
                 %creates a row vector that contains the number of trials
                 %for each bin
                 ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                 %Determine if threshold is crossed  
                 if  corrcoefs(xxx,yyy) > thresholds
                 %If it is then keep the number
                 bb = corrcoefs(xxx,yyy);
           
                 yy = 1;
                 else
                 %Else, input NaN
                 bb = NaN; 
             
                 yy = 0;
                 end
      
             x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
             
             corrss = cat(1,corrss, bb);
        
             
             end
             
         number = cat(2,number,x); 
         cors = cat(2,cors,corrss);
   
    end
    


    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    n =[];
    c = length(ntrials);
    for z = 1 : c
        n = cat(2,n,length(ind));
    end
    
   numbertrials = n - ntrials;
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end
 
      percents = cat(1,percents,tttt);%%%%%%%%%THESE ARE THE INDIVDIDUAL PAIRS
 
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 numpairs = size(percents,1);
 
 maxpercents = max(max(percents(:,(1:end-10))));
 

        %used for tick labeling. Starts at 150ms
        q = (length(tttt)*50)+100;
        
        yyy = [1:10:60];
       
        xx = [150:500:q];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plots
if object == 0 && location == 0
stims = ', All Stimuli';
elseif object == 0;
stims = [',  All Objects,  Location = ',num2str(location)];
else
stims = [',  All Locations,  Object = ',num2str(object)];    
end

tt = ([t,' % of Significant Trials, ',' Threshold = ',threshh,stims]);
figure('Name',tt,'NumberTitle','off')
for p =  1:numpairs
   
    
    
subplot(p1,p2,p); 
m2 =  percents(p,:);

plot(m2);
hold on
               

                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = 0; yMax = maxpercents;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = maxpercents;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = maxpercents;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = 0; yMax = maxpercents;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')  
    
                
                
    
     %Determines pair number   
     qq = cc(p,1);
     qqq = cc(p,2);           
                
                
    hold off
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    title(['Channels  ',num2str(qq),'  and  ',num2str(qqq)],'fontsize',10,'fontweight','b')
    ylim([0 maxpercents]);
    xlabel('Time(ms)')
     
end       




%ylabel(subplot(4,5,1),tt,'fontsize',12,'fontweight','b');
zoom on
linkzoom
       %  linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');
                
 

  
    
end

  

  
 

 