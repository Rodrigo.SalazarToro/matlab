function figureStimuli




%make .figs for all pairs for the stimuli
%run and save figs in lfp2 folder

%function cppStimuli(b1,ts1,c1,c2,threshh,sessionname,match,chnumb)

d = cd;
cd ..
cd ..

[NeuroInfo] = NeuronalChAssign;
chnumb=length(NeuroInfo.channels);

cd(d);

%for all pairs
for c1 = 1 : chnumb
    
    for c2 = (c1+1) : chnumb
        
        %for thresholds 1:3
        for t = 1 : 3
        
        
        
        cppStimuli(1,1,c1,c2,num2str(t),'channelpair12.mat',0,chnumb)

        saveas(gcf, ['stimuli_T',num2str(t),'_',num2str(c1),num2str(c2),'.fig'])
        
        delete(gcf)

  

        end



    end 
end