function writeCPP

%Run in day directory  ...monkey/day
%Files are saved in the ...day/session/lfp
%Writes all pairwise channel combinations for the thresholds, corrcoefs
%and phases.

%WRITES  channelpair
directory = cd;

%Filter
LowPassLow = 10; %10
LowPassHigh = 100; %40

%Xcorr
windowSize = 200; %200
windowStep = 50; %50
phaseSize = 50; %50

%Run on sessions 2 and 3
for sessions = 2:3
    sesion = ['session0' num2str(sessions)];
    directs = [directory '\' sesion];
    cd(directs)
    
[NeuroInfo] = NeuronalChAssign;

%Get number of channels
chnumb=length(NeuroInfo.channels);

mt=mtstrial('auto');
%Get number of trials
NumberOfTrials = mt.data.numSets;

directss = [directs '\lfp'];

cd(directss)

%%%%%%%%%%%%%%%%%%%%%channelpairs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 for c1=1:chnumb

    for c2=(c1+1):chnumb

        corrcoefs=[]; phases=[]; 
        for x = 1:NumberOfTrials
  
        sessionname  = [directss((end-25):(end-21)) directss((end-19):(end-14)) directss((end-5):(end-4)) '_lfp.0000']; 
        if x > 999
        y = 4;    
        elseif x > 99
        y = 3;
        elseif x > 9
        y = 2;
        else
        y = 1;
        end
    
   %load data
    [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile([sessionname(1:end-y) num2str(x)]);
    %Creates structure 'd' w/ streamer data
    d=struct;
    d.rawdata = data;
    d.samplingRate = sampling_rate;
    d.numChannels = num_channels;
    d.scanOrder = scan_order;
    [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
    
    %calculate number of bins
     steps = floor((length(data)-windowSize)/windowStep);
     start =  1:windowStep:steps*windowStep+1;
     endd = start+windowSize-1;
     centers = start+windowSize/2-1;

     %%%%%Normalize Data
    c1mean = mean(data(c1,:));
    c2mean = mean(data(c2,:));
    c1std = std(data(c1,:));
    c2std = std(data(c2,:));
    
    normdata = [((data(c1,:)-c1mean)/c1std) ; ((data(c2,:)-c2mean)/c2std)]; 
    r = GrayXxcorr(normdata(1,:),normdata(2,:),windowSize,windowStep,phaseSize,start,endd);

    %find closest peak to zero lag
            p=zeros(size(r,1),2);
            for kk=1:size(r,1)
                rr=r(kk,:);
                t=fpeak(1:length(rr),rr,1,[1,length(rr),0,inf]);
                phase=t(:,1)-phaseSize-1;
                [y,ind]=min(abs(phase));
                if isempty(t) %all negative correlations
                    p(kk,1)=0;
                    p(kk,2)=0;
                elseif y==phaseSize %max correlation is at max phase
                    p(kk,1)=0;
                    p(kk,2)=0;
                else
                    p(kk,1) = t(ind,2);
                    p(kk,2) = phase(ind);
                end
            end
    
            %%coefs = the correlation coefs for the trial pair
            coefs=(p(:,1));
            
            %%phases = the phases
            phase=(p(:,2));
            
            %Find the length of the coefs column.
            cc = length(coefs);
            dd = length(phase);
            
            %Determines how many buffer NaN's are necessary
            ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
            ddd = (3000/windowStep) - dd;

            %make sure there are only 60 bins
            if ccc >= 0 
            
            %Creates NaN buffer
            cccc = NaN(1,ccc)';
            dddd = NaN(1,ddd)';
            
            %%Concatenates NaN buffer on to the end of the column in order
            %%for the matrix dimensions to match
            coefs = cat(1,coefs,cccc);
            phase = cat(1,phase,dddd);
            
            else
                
            coefs = coefs((1:end+ccc),1);     
            phase = phase((1:end+ddd),1);

            end
     
            %%makes a matrix with all trials correlation coef for the
            %%specific pair
            corrcoefs = cat(2,corrcoefs, coefs);
            phases = cat(2,phases, phase);
   
        end
    
 pairs = [ 'channelpair' num2str(c1) num2str(c2) ]
 
 save(pairs,'corrcoefs','phases') 


    end    
end

end

end
   




        





 
    

             
   
    


    
    
    
    
    
    





    

    
    
    
    