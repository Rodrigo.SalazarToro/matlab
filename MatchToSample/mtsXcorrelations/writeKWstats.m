function writeKWstats

%uses stimStats to write kruskalwallis statistics for 9 stimuli


cd('/media/MONTY/data/clark')
monkey = cd;
days = nptDir('06*');

num_days = size(days,1);

global p_all p_obj p_loc



all_kwStats = [];

for ddd = 1:num_days
    
    cd([monkey filesep days(ddd).name]);
    sessions = nptDir('session0*');
    
    for y = 2:3
        pair_number = 0;
        kwStats = [];
        sessionname = sessions(y).name;
        
        %same code as cppStimuli
        tss1 = 'stable';
        threshh = '1';
        
        
        cd([monkey filesep days(ddd).name filesep sessionname filesep ('lfp/lfp2')]);
        
        
        load threshold threshold
        
        lfp2 = cd;
        cd ..
        lfp = cd;
        cd ..
        
        mt=mtstrial('auto');
        
        objs =  unique(mt.data.CueObj);
        
        locs =  unique(mt.data.CueLoc);
        [frontalchannels, comb, frontal] = checkChannels('cohPF');
        [interchannels, comb, inter] = checkChannels('cohInter');
        [parietalchannels,comb, parietal] = checkChannels('cohPP');
        
        cd(lfp)
        
        all_channels = [parietalchannels frontalchannels];
        chnumb = size(all_channels,2);
        
        %run for all channels
        
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                clc
                pair_number = pair_number + 1
                
                %should be a 9xn matrix
                plotpercents = [];
                %Run for all objects
                for object = 1:3
                    
                    %Run for all locations
                    for location = 1:3
                        cd(lfp)
                        %initial index is determined base on object and location
                        ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(object),'CueLoc',locs(location));
                        
                        %Get files based on match time, 4 times:
                        %1800-1900,1901-2000,2001,2100,2101,2200ms
                        m = mt.data.MatchOnset(ind,:);
                        matchonset = round(m);
                        
                       
                        percents = [];
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%RUN FOR ONE PAIR%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        
                        cd(lfp2)
                        trial = (['channelpair' num2str(c1) num2str(c2)]);
                        
                        if (size(ind,2) == 0)
                            
                            load(trial, 'corrcoefs','phases')
                            corrcoefs = abs(corrcoefs(:,1));
                            phases = phases(:,1);
                            threshh = '0';
                        else
                            load(trial, 'corrcoefs','phases')
                            corrcoefs = abs(corrcoefs(:,ind));
                            phases = phases(:,ind);
                            
                        end
                        
                        thresh = str2double(threshh);
                        
                        %Index threshold for the correct threshold
                        if thresh > 0
                            
                            pair = 0;
                            for x = 1 : c1
                                if x == c1
                                    q = c2;
                                else
                                    q = chnumb;
                                end
                                
                                for y = (x+1) : q
                                    pair = pair + 1;
                                    
                                end
                            end
                            th = threshold{pair};
                            
                            thresholds = th(:,thresh);
                            
                        else
                            thresholds = 0;
                        end
                        [rows columns] = size(corrcoefs);  %will be the same for the phases
                        number=[];
                        
                        for yyy = 1:columns
                            x=[];
                            ntrials = [];
                            
                            for xxx = 1:rows %50ms bins w/ a max at 3000ms
                                
                                %creates a row vector that contains the number of trials
                                %for each bin
                                ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                                %Determine if threshold is crossed
                                if  corrcoefs(xxx,yyy) > thresholds
                                    %If it is then keep the number
                                    bb = corrcoefs(xxx,yyy);
                                    pp = phases(xxx,yyy);
                                    yy = 1;
                                else
                                    %Else, input NaN
                                    bb = NaN;
                                    pp = NaN;
                                    yy = 0;
                                end
                                
                                x = cat(1,x, yy);%%%Determines number of trials by not counting the else
                                
                            end
                            
                            number = cat(2,number,x);
                        end
                        %tttt = percent of ind trials
                        %tttt = ((sum(number')/length(ind))*100);
                        n =[];
                        c = length(ntrials);
                        for z = 1 : c
                            n = cat(2,n,length(ind));
                        end
                        
                        numbertrials = n - ntrials;
                        number = number';
                        
                        tttt = [];
                        for zz = 1 : length(numbertrials)
                            
                            if numbertrials(1,zz) == 0
                                tt = 0;
                            else
                                tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
                                %Determines acutal percentage of trials based on trials left
                            end
                            
                            tttt = cat(2,tttt,tt);
                        end
                        percents = cat(1,percents,tttt);
                        plotpercents = cat(1,plotpercents,percents);
                    end
                end
                
                %run kw for 9 stims
                stimStats(plotpercents)
                
                kwStats(1,pair_number) = p_all
                kwStats(2,pair_number) = p_loc
                kwStats(3,pair_number) = p_obj

            end
                            
                
        end
        save kwStats kwStats
        all_kwStats = [all_kwStats kwStats];
    end
    
    
end





cd(monkey)

save all_kwStats all_kwStats


