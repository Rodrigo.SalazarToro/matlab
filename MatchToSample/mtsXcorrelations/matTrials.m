function matTrials(channel)


%makes large matrix for channels trials X sample
%run in lfp2 folder


lfp2data = nptDir('clark*');

s = 0;
for xxx = 1 : size(lfp2data,1)
    
    load (lfp2data(xxx).name, 'normdata')

    sss = size(data2,2);
    %find trial with the least number of samples
    if s
    s = sss;
    end
    if sss < s
        s = sss;
    end   
end

allTrials2 = [];
for xxx = 1 : size(lfp2data,1)
    
    load(lfp2data(xxx).name, 'normdata')
    
    allTrials2 = cat(1,allTrials2,data2(channel,(1:s)));
end

cd ..

save allTrials2 allTrials2
