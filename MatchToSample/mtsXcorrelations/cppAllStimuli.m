function cppAllStimuli(b1,ts1,c1,c2,threshh,sessionname,match)

%Used by mtsCPP to view the averages percents for all stimuli for ONE
%CHANNEl PAIR
load threshold threshold
 
    directory = cd; 
    cd(directory(1:end-4))

    mt=mtstrial('auto');
    
    objs =  unique(mt.data.CueObj);
    
    locs =  unique(mt.data.CueLoc);
    [frontalchannels, comb, frontal] = checkChannels('cohPF');
    [interchannels, comb, inter] = checkChannels('cohInter');
    [parietalchannels,comb, parietal] = checkChannels('cohPP');

    cd(directory)
    
    if ts1 == 0
        tss1= 'transition';
    else
        tss1= 'stable';
    end

%should be a 9xn matrix 
plotpercents = [];       
%Run for all objects    
for object = 1:3
    
    %Run for all locations
    for location = 1:3
    
    %initial index is determined base on object and location
    ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object),'CueLoc',locs(location));
    


    %Get files based on match time, 4 times:
    %1800-1900,1901-2000,2001,2100,2101,2200ms
    m = mt.data.MatchOnset(ind,:);
    matchonset = round(m);

    if match == 0
        ind = ind;
    elseif match == 1
        matches = find(1800 <= matchonset & matchonset <= 1900);
        ind = ind(:,matches);
    elseif match == 2
        matches = find(1900 <= matchonset & matchonset <= 2000);
        ind = ind(:,matches);
    elseif match == 3
        matches = find(2000 <= matchonset & matchonset <= 2100);
        ind = ind(:,matches);
    elseif match == 4
        matches = find(2100 <= matchonset);
        ind = ind(:,matches);
    end

    %Determine whether the pair is pp,ff,pf
    if sum(c1 == frontal(:,1)) > 0
        t = 'Frontal Vs Frontal';
    elseif sum(c2 == parietal(:,2)) > 0
        t = 'Parietal Vs Parietal';
    else
        t = 'Parietal Vs Frontal';
    end

  
 percents = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%RUN FOR ONE PAIR%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
        
        load(trial, 'corrcoefs','phases')
        corrcoefs = corrcoefs(:,ind);
        phases = phases(:,ind);
        
        
     thresh = str2double(threshh);   
    
     %Index threshold for the correct threshold   
     if thresh > 0 
        c1c2=0;
        for cc1 = 1:c1
            for cc2 = (cc1+1) : c2
                
               c1c2 = c1c2 +1; 
                
            end
        end
    threshind = (c1c2*4 - 4)+ thresh;
    thresholds =(threshold(1,threshind));
    else
        thresholds = 0;
    end


   
    [rows columns] = size(corrcoefs);  %will be the same for the phases
    number=[];

    for yyy = 1:columns
        x=[];
        ntrials = [];

             for xxx = 1:rows %50ms bins w/ a max at 3000ms
                  
                 %creates a row vector that contains the number of trials
                 %for each bin
                 ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                 %Determine if threshold is crossed  
                 if  corrcoefs(xxx,yyy) > thresholds
                 %If it is then keep the number
                 bb = corrcoefs(xxx,yyy);
                 pp = phases(xxx,yyy);
                 yy = 1;
                 else
                 %Else, input NaN
                 bb = NaN; 
                 pp = NaN;
                 yy = 0;
                 end
      
             x = cat(1,x, yy);%%%Determines number of trials by not counting the else
             
             end
             
         number = cat(2,number,x); 
    end
    

  
    
 
    
    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    n =[];
    c = length(ntrials);
    for z = 1 : c
        n = cat(2,n,length(ind));
    end
    
   numbertrials = n - ntrials;
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end

      percents = cat(1,percents,tttt);

      
      
      
    plotpercents = cat(1,plotpercents,percents); 
    end
 
 
 
 end 
maxpercents = max(max(plotpercents(:,(1:end-10))));

figure
title(gca,'Stimuli')

        %used for tick labeling. Starts at 150ms
        q = (length(number)*50)+100;
        yyy = [1:10:60];
        xx = [150:500:q];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 1-9

%line color
%c = ['k','b','g','y','r','c','m','y','k','b'];

%p = 0;
%for pp = 1:9
   
        
 %   p = p + 1;

%m = plotpercents(p,:);

%plot(m,c(:,p));
%hold on

%end    
    %Draw Vertical Lines
    
    surface(plotpercents)
                

linkzoom
 
end

  

  
 

 