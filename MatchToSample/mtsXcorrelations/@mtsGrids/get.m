function [r,varargout] = get(obj,varargin)

%get function for mtsCPP
%Object level is session object
%gets channel pairs, plot function is then used to get specific trials

%Arguments
%   'pp': parietal Vs parietal
%   'pf': parietal Vs frontal
%   'ff': frontal Vs frontal
%   'sig': 1 if average correlogram crosses 2std, else 0
%   'identity': used for loading across sessions
%   'location': used for loading across sessions
%   'e1': 1 if first epoch correlogram crosses 2std, else 0
%   'e2'
%   'e3'
%   'e4'
%   'pval': p-value for regression of the line for the delay period
%       Hnull: slope == 0
%   'slope': slope during delay period
%   'threshold': for the corrcoefs 1(95), 2(99), 3(99.9), 4(99.99)
%   'correct': plots only correct trials
%   'incorrect': plots only incorrect trials
%   'in_phase' | 'anti_phase': in phase, or 180 out of phase pairs
%   'distance',[x y]: euclidean distance between pairs (x>=pairs<=y)
%   'depths',[x y]: depth differences between pairs (x>=pairs<=y)
%   'hist',[x y]: uses histology information about the postion of the channels relative to the sulcus in PFC(PS) and PPC(IPS)
%       IF pp OR ff
%       FIRST NUMBER(x)
%           opposite side of sulcus == 0
%           same side of sulcus == 1
%           one channel in sulcus == 2
%           both channels in sulcus == 3
%           Inter-arial == -1
%       SECOND NUMBER(y)
%           PFC ventral / PPC lateral == 1
%           PFC dorsal / PPC medial == 0
%       IF pf
%       FIRST NUMBER(x)
%           PPC medial / PPC lateral == 0/1
%       SECOND NUMBER(y)
%           PFC dorsal / PFC ventral == 0/1


Args = struct('Number',0,'ObjectLevel',0,'pp',0,'pf',0,'ff',0,'sig',0,'identity',0,'location',0,'e1',0,'e2',0,'e3',0,'e4',0,'pval',[],'slope',[],'threshold',0,'correct',0,'incorrect',0,'in_phase',0,'anti_phase',0,'distance',[],'depths',[],'hist',[],'kw_all',0,'kw_loc',0,'kw_obj',0);
Args.flags = {'Number','ObjectLevel','pp','pf','ff','sig','identity','location','e1','e2','e3','e4','threshold','correct','incorrect','in_phase','anti_phase'};
Args = getOptArgs(varargin,Args);


varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    
    %parietal Vs parietal
    if Args.pp
        rtemp1 = find(obj.data.Index(:,1) == 1);
    else
        rtemp1 = [1 : length(obj.data.Index(:,1))];
    end
    
    if Args.pf
        rtemp2 = find(obj.data.Index(:,2) == 1);
    else
        rtemp2 = [1: length(obj.data.Index(:,2))];
    end
    
    if Args.ff
        rtemp3 = find(obj.data.Index(:,3) == 1);
    else
        rtemp3 = [1 : length(obj.data.Index(:,3))];
    end
    
    % if Args.identity
    %
    %     rtemp4 = find(obj.data.Index(:,4) == 1);
    %
    % else
    %     rtemp4 = [1 : length(obj.data.Index(:,4))];
    %
    % end
    
    if Args.location || Args.identity
        if Args.location
            rtemp4 = find(obj.data.Index(:,4) == 0);
        end
        if Args.identity
            rtemp4 = find(obj.data.Index(:,4) == 1);
        end
    else
        rtemp4 = [1 : length(obj.data.Index(:,4))];
        
    end
    
    if Args.sig
        rtemp5 = find(obj.data.Index(:,5) == 1);
    else
        rtemp5 = [1 : length(obj.data.Index(:,5))];
    end
    
    if Args.e1
        rtemp6 = find(obj.data.Index(:,6) == 1);
    else
        rtemp6 = [1 : length(obj.data.Index(:,6))];
    end
    
    
    if Args.e2
        rtemp7 = find(obj.data.Index(:,7) == 1);
    else
        rtemp7 = [1 : length(obj.data.Index(:,7))];
    end
    
    if Args.e3
        rtemp8 = find(obj.data.Index(:,8) == 1);
    else
        rtemp8 = [1 : length(obj.data.Index(:,8))];
    end
    
    if Args.e4
        rtemp9 = find(obj.data.Index(:,9) == 1);
    else
        rtemp9 = [1 : length(obj.data.Index(:,9))];
    end
    
    %%
    if ~isempty(Args.pval)
        
        %     if Args.incorrect
        %         br = 0;
        %     else
        %         br = 1; %default to a correct response
        %     end
        
        br = 1;  %%%%gets the same pairs for correct and incorrect
        
        if Args.threshold
            t = Args.threshold;
            if t > 2
                fprintf(1,'threshold value must be 1 or 2 \n');
            end
        else
            t = 1;
        end
        
        %columns
        %1.)c s t1
        %2.)c s t2
        %3.)i s t1
        %4.)i s t2
        
        if (br == 1) && (t == 1)
            c = 14;
        elseif (br == 1) && (t == 2)
            c = 15;
        elseif (br == 0) && (t == 1)
            c = 16;
        else
            c = 17;
        end
        
        rtemp10 = find(obj.data.Index(:,c) <= Args.pval);
    else
        rtemp10 = [1 : length(obj.data.Index(:,17))];
    end
    
    %%
    
    if ~isempty(Args.slope)
        
        %     if Args.incorrect
        %         br = 0;
        %     else
        %         br = 1; %default to a correct response
        %     end
        
        br = 1;
        
        
        if Args.threshold
            t = Args.threshold;
            if t > 2
                fprintf(1,'threshold value must be 1 or 2 \n');
                t = 1;
            end
        else
            t = 1;
        end
        
        %columns
        %1.)c s t1
        %2.)c s t2
        %3.)i s t1
        %4.)i s t2
        
        if (br == 1) && (t == 1)
            c = 10;
        elseif (br == 1) && (t == 2)
            c = 11;
        elseif (br == 0) && (t == 1)
            c = 12;
        else
            c = 13;
        end
        
        
        rtemp11 = find(obj.data.Index(:,c) >= Args.slope);
        
        
    else
        rtemp11 = [1 : length(obj.data.Index(:,13))];
    end
    %%
    
    if Args.in_phase || Args.anti_phase
        if Args.in_phase
            rtemp12 = find(obj.data.Index(:,18) >= 0 );
        else
            rtemp12 = find(obj.data.Index(:,18) <= 0 );
        end
    else
        rtemp12 = [1 : length(obj.data.Index(:,18))];
    end
    
    if ~isempty(Args.distance)
        %Finds pairs with euclidean distances inbetween entries [x y]
        %THESE ARE DIFFERENCES
        r = find(obj.data.Index(:,19) >= Args.distance(:,1));%x
        rr = find(obj.data.Index(:,19) <= Args.distance(:,2));%y
        rtemp13 = intersect(r,rr);
    else
        rtemp13 = [1 : length(obj.data.Index(:,19))];
    end
    
    %%
    if ~isempty(Args.depths)
        %Finds depths between entries [x y]
        %THESE ARE DIFFERENCES
        d = find(obj.data.Index(:,20) >= Args.depths(:,1));%x
        dd = find(obj.data.Index(:,20) <= Args.depths(:,2));%y
        rtemp14 = intersect(d,dd);
    else
        rtemp14 = [1 : length(obj.data.Index(:,20))];
    end
    
    %%
    if ~isempty(Args.hist)
        %INTRA-ARIAL
        if (Args.pp || Args.ff)
            
            if (Args.hist(:,1) == 1)
                rtemp15 = find(obj.data.Index(:,21) == Args.hist(:,1));
                rtemp16 = find(obj.data.Index(:,22) == Args.hist(:,2));
            else
                rtemp15 = find(obj.data.Index(:,21) == Args.hist(:,1));
                rtemp16 = [1 : length(obj.data.Index(:,22))];
            end
            
            %INTER-ARIAL
        elseif (Args.pf)
            %'Args.hist',[x y]
            %x == PPC cortex (0 == medial, 1 == lateral) 2 == sulcus)
            %y == PFC cortex (0 == dorsal, 1 == ventral, 2 == sulcus)
            hc1 = Args.hist(:,1);%PARIETAL
            hc2 = Args.hist(:,2);%PRE-FRONTAL
            
            %[0 0]medial VS dorsal == -1
            %[1 0]lateral VS dorsal == -2
            %[0 1]medial VS ventral == -3
            %[1 1]lateral VS ventral == -4
            %[0 2]medial VS sulcus == -5
            %[1 2]lateral VS sulcus == -6
            %[2 0]sulcus VS dorsal== -7
            %[2 1]sulcus VS ventral== -8
            %[2 2]sulcus VS sulcus == -9
            
            if hc1 == 0 && hc2 == 0 %md
                h = -1;
            elseif hc1 == 1 && hc2 == 0 %ld
                h = -2;
            elseif hc1 == 0 && hc2 == 1 %mv
                h = -3;
            elseif hc1 == 1 && hc2 == 1 %lv
                h = -4;
            elseif hc1 == 0 && hc2 == 2 %ms
                h = -5;
            elseif hc1 == 1 && hc2 == 2 %ls
                h = -6;
            elseif hc1 == 2 && hc2 == 0 %sd
                h = -7;
            elseif hc1 == 2 && hc2 == 1 %sv
                h = -8;
            elseif hc1 == 2 && hc2 == 2 %ss
                h = -9;
            end
            
            rtemp15 = find(obj.data.Index(:,21) == h);
            rtemp16 = [1 : length(obj.data.Index(:,22))];
        else
            rtemp15 = [1 : length(obj.data.Index(:,21))];
            rtemp16 = [1 : length(obj.data.Index(:,22))];
        end
    else
        rtemp15 = [1 : length(obj.data.Index(:,21))];
        rtemp16 = [1 : length(obj.data.Index(:,22))];
    end
    
    
    if Args.kw_all > 0
        rtemp17 = find(obj.data.Index(:,26) <= Args.kw_all);
    elseif Args.kw_loc > 0
        rtemp17 = find(obj.data.Index(:,27) <= Args.kw_loc);
    elseif Args.kw_obj > 0
        rtemp17 = find(obj.data.Index(:,28) <= Args.kw_obj);
    else
        rtemp17 = [1 : length(obj.data.Index(:,26))];
    end
    
    
    %%
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect...
        (rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect...
        (rtemp10,intersect(rtemp11,intersect(rtemp12,intersect(rtemp13,intersect(rtemp14,intersect(rtemp15,intersect(rtemp16,rtemp17))))))))))))))));
    
    r = length(varargout{1});
    
    
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

