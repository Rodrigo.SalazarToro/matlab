function obj = mtsGrids(varargin)
%creates CPP object
%run in session directory

%index
%   parietal Vs parietal
%   parietal Vs frontal
%   frontal Vs frontal
%   trial type
%   average correlogram crossing

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtsGrids';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'grids';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end


%%
function obj = createObject(Args,varargin)

sdir = pwd;

%CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb)

%try to make object if there is an error then create and empty object


%make mts trial object
mtst = mtstrial('auto','ML','RTfromML');

if ~isempty(mtst)
    %get trial info, identity == 1, location == 0
    type = mtst.data.Index(1,1);
    
    %get channel info
    [NeuroInfo] = NeuronalChAssign();
    

    %get channel locations
    [parietalchannels,pcomb, parietal] = checkChannels('cohPP');
    [interchannels, icomb, inter] = checkChannels('cohInter');
    [frontalchannels, fcomb, frontal] = checkChannels('cohPF');
    
    %all_channels are the good channels
    all_channels = [parietalchannels frontalchannels];
    chnumb = size(NeuroInfo.groups,2);
    
    %get number of pairs
    pairs = (chnumb * (chnumb-1)) / 2;
    
    %index contains
    %   1.)parietal parietal pairs
    %   2.)parietal frontal pairs
    %   3.)frontal frontal pairs
    %   4.)identity = 1, location = 0
    %   5.)avgerage correlogram information
    %   6-9.)epoch 1 - 4
    %   10 - 17.) slope_info
    
    
    
    
    %23 first channel
    %24 second channel
    %25 chunmbs
    
    Index = zeros(pairs,28);
    
    pp = zeros(pairs,1);
    pf = zeros(pairs,1);
    ff =  zeros(pairs,1);
    cc1 = zeros(pairs,1);
    cc2 = zeros(pairs,1);
    chnumbs = zeros(pairs,1);
    p = 0;
    for c1 = 1 : chnumb
        for c2 = (c1+1) : chnumb
            
            %index to good channels
            c1 = all_channels(c1);
            c2 = all_channels(c2);
            
            cc = [c1 c2];
            
            p = p + 1;
            
            
            cc1(p,1) = c1;
            cc2(p,1)= c2;
            chnumbs(p,1) = chnumb;
            
            interc = 0;
            if (sum(c1 == parietalchannels) + sum(c2 == parietalchannels)) == 2
                pp(p,1) = 1;
            else
                pp(p,1) = 0;
                interc = 1;
            end
            
            if (sum(c1 == frontalchannels) + sum(c2 == frontalchannels)) == 2
                ff(p,1) = 1;
            else
                ff(p,1) = 0;
                interc = interc + 1;
            end
            
            if interc == 2;
                pf(p,1) = 1;
            else
                pf(p,1) = 0;
            end
        end
    end
    
    data.Index(:,23) = cc1;
    data.Index(:,24) = cc2;
    data.Index(:,25) = chnumbs;
    
    
    
    %parietal Vs parietal
    data.Index(:,1) = pp;
    %parietal Vs frontal
    data.Index(:,2) = pf;
    %frontal Vs frontal
    data.Index(:,3) = ff;
    
    if type == 1 %identity
        data.Index(:,4) = ones(pairs,1);
    else %location
        data.Index(:,4) = zeros(pairs,1);
    end
    
    %get average correlogram information
    cd (['lfp',filesep,'lfp2']);
    
    lfp2dir = cd;
    load ccTable pair_info e1_info e2_info e3_info e4_info
    
    cpairs = nptDir('channelpair**.mat');
    
    %1 if average Correlogram crosses 2std, 0 if not.
    data.Index(:,5) = pair_info;
    
    %epoch 1 correlogram
    data.Index(:,6) = e1_info;
    
    %epoch 2 correlogram
    data.Index(:,7) = e2_info;
    
    %epoch 3 correlogram
    data.Index(:,8) = e3_info;
    
    %epoch 4 correlogram
    data.Index(:,9) = e4_info;
    
    
    load slope_info slopes pvals
    
    %columns
    %1.)c s t1
    %2.)c s t2
    %3.)i s t1
    %4.)i s t2
    
    
    %slope info (positive and negative)
    data.Index(:,10) = slopes(:,1);
    data.Index(:,11) = slopes(:,2);
    data.Index(:,12) = slopes(:,3);
    data.Index(:,13) = slopes(:,4);
    
    %pvals(Hnull: slope == 0)
    data.Index(:,14) = pvals(:,1);
    data.Index(:,15) = pvals(:,2);
    data.Index(:,16) = pvals(:,3);
    data.Index(:,17) = pvals(:,4);
    
    load avg_correlograms avg_corrcoef
    
    
    %Determines whether the average correlogram has a positive
    %or a negative peak.
    pos_neg_avg = cell2mat(avg_corrcoef)';
    
    data.Index(:,18) = pos_neg_avg;
    
    load threshold threshold
    
    data.thresholds = threshold';
    
    for q = 1 : pairs
        data.setNames{q,1} = [lfp2dir filesep cpairs(q).name];
    end
    
    cd(sdir);
    try %if histology information is present include it in the Index. If it not then skip it
        %loads euclidean distances of pairs
        %pairs are 0 if they are not in the same area and the same
        %side of the sulcus (PS or IPS)
        %distances are in Millimeters
        load pair_distances pair_distances
        load depths depths
        load hist hist
        
        
        data.Index(:,19) = pair_distances;
        
        %loads the depth differences for each pair, same criterian
        %as the pair_distances (written w/ euclidPairs)
        %IN MILLIMETERS
        data.Index(:,20) = depths;
        
        %loads histology information (written w/ euclidPairs)
        %opposite side of sulcus == 0
        %same side of sulcus == 1
        %one channel stradles sulcus == 2
        %both channels stradle sulcus == 3
        %Inter-arial == -1
        
        data.Index(:,21) = hist(:,1);
        
        %Indicates which side of sulcus iff both channels are on
        %the same side of the sulcus.  == 1 (see above)
        %dPFC/mPPC == 0, vPFC/lPPC == 1
        
        data.Index(:,22) = hist(:,2);
       
    catch
    end
    
    ses_dirs = cd;
    try
        cd (['lfp',filesep,'lfp2']);
        load kwStats kwStats
        cd(ses_dirs)
        %stims
        data.Index(:,26) = kwStats(1,:)';
        %location
        data.Index(:,27) = kwStats(2,:)';
        %object
        data.Index(:,28) = kwStats(3,:)';
    catch
    end
    
    
        
    
    
  
    data.numSets = pairs;
    
    
    
    %%
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
else
    obj = createEmptyObject(Args);
end

%%
function obj = createEmptyObject(Args)


data.Index = [];
data.setNames = {};
data.thresholds={};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
