function obj = plot(obj,varargin)

%used to plot mtsCPP objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct' 


Args = struct('threshold',0,'stable',0,'transition',0,'correct',0,'incorrect',0,'avgCorr',[]);
Args.flags = {'stable','transition','correct','incorrect'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)

    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
   
end

%loads the channelpair specified in the index

channel_pair = obj.data.setNames{ind};

if ~isempty(Args.avgCorr)
%%

s = findstr('channel',channel_pair);
cd(channel_pair(1:(s-1)));

load avg_correlograms avg_correlograms avg_phase avg_corrcoef

load avg_surrogates avg_surrogates

load avg_correlograms_epochs avg_correlograms_epochs

    

        channel1 = obj.data.Index(ind,23);
        channel2 = obj.data.Index(ind,24);
    
        pair = 0;
    pair = 0;

    for x = 1 : channel1
        if x == channel1 
            q = channel2; 
        else
            q= obj.data.Index(ind,25);
        end
        
        for y = (x+1) : q
            
            pair = pair + 1;
            
        end
    end
        
 
        

%get correlogram    
c = avg_correlograms{pair};
corrcoef = avg_corrcoef{pair};
phase = avg_phase{pair};

%get surrogate
s = avg_surrogates{pair};
m = mean(s);
pos2std = 2*(std(s));


if Args.avgCorr == 0


        plot(-50:50,c) 
        hold on
        
        
    %draw phase line
    yMin = -1; yMax = 1;
    xVal = phase;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    
    %draw correlation coef line          
    xMin = -50; xMax = 50;
    yVal = corrcoef;
    plot([xMin xMax],[yVal, yVal],'color','r')
    
    %draw mean          
    xMin = -50; xMax = 50;
    yVal = m;
    plot([xMin xMax],[yVal, yVal],'color','k')
    
    %draw 2nd standard deviation positive          
    xMin = -50; xMax = 50;
    yVal = m + pos2std;
    plot([xMin xMax],[yVal, yVal],'color','g')
    
    %draw 2nd standard deviation positive          
    xMin = -50; xMax = 50;
    yVal = m - pos2std;
    plot([xMin xMax],[yVal, yVal],'color','g')
    

    
        t = [obj.data.setNames{ind},'.   Average Correlogram Across all Trials.'];

        title(gca,t,'fontsize',12,'fontweight','b');
        
        if (pos2std > abs(corrcoef))
            ylimits = pos2std;
        else
            ylimits = abs(corrcoef);
        end
            
        axis([-50 50 (-1*(ylimits+.1)) ylimits+.1]) 
        
else
        
        
%%%%%%%%%%%%%%%%%%%%%PLOT EPOCHS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c_epochs = avg_correlograms_epochs{pair};


lags = [-50:50];

yRange = 0;

x = Args.avgCorr;


  
        plot(lags,c_epochs(x,:))
        hold on
    
        

        cc = c_epochs(x,:);
        
        max_lag = size(c_epochs(x,:),2);
        
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

        %Find index of central positive peak
        pp = findpeaks(cc);
        
                for ppp = 1 : size(pp.loc,1)

                    if (cc(pp.loc(ppp)) < 0)
                         %set to max phase index
                        pp.loc(ppp) = max_lag;
                    end
                    
                end
        
        
        [phase, pos_index] = (min(abs(lags(pp.loc))));
        %%%%%%%%%%get real phase value by indexing the location in the lags
        pos_phase = lags(pp.loc(pos_index));
        pos_corrcoef = cc(pp.loc(pos_index));
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

        %Find index of central negative peak (inverts correlogram)
        cc = cc*-1;
        np = findpeaks(cc);
        
                for nnn = 1 : size(np.loc,1)

                    if (cc(np.loc(nnn)) < 0)
                         %set to max phase index
                        np.loc(nnn) = max_lag;
                    end
                    
                end
        
        
        [phase, neg_index] = (min(abs(lags(np.loc))));
        
        neg_phase = lags(np.loc(neg_index));
        neg_corrcoef = cc(np.loc(neg_index));    


        %Determines if the closest peak to zero is positive or negative.
        %Stores the correlation coef and lag position.
        if abs(pos_phase) <= abs(neg_phase)
            avg_phase = pos_phase;
            avg_corrcoef = pos_corrcoef;
        else
            avg_phase = neg_phase;
            avg_corrcoef = (-1 * neg_corrcoef); %make negative
        end

      
 
        
    %draw phase line
    yMin = -1; yMax = 1;
    xVal = avg_phase;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    
    %draw correlation coef line          
    xMin = -50; xMax = 50;
    yVal = avg_corrcoef;
    plot([xMin xMax],[yVal, yVal],'color','r')
    hold on
        
    %draw mean          
    xMin = -50; xMax = 50;
    yVal = m;
    plot([xMin xMax],[yVal, yVal],'color','k')
    hold on
    
    %draw 2nd standard deviation positive          
    xMin = -50; xMax = 50;
    yVal = m + pos2std;
    plot([xMin xMax],[yVal, yVal],'color','g')
    hold on
    
    %draw 2nd standard deviation positive          
    xMin = -50; xMax = 50;
    yVal = m - pos2std;
    plot([xMin xMax],[yVal, yVal],'color','g')
    
    if abs(corrcoef) > yRange
        yRange = abs(corrcoef);
    end
    if pos2std > yRange
        yRange = pos2std;
    end
      
        

        
            t = [obj.data.setNames{ind},'.   Average Correlogram for Epoch ' num2str(x)];

            title(t,'fontsize',12,'fontweight','b');
            

            
            axis([-50 50 (-1*(yRange+.1)) yRange+.1])    

end
return
end
%%














%load (channel_pair,'corrcoefs','phases','indexes')

load(channel_pair,'plot_data','pair_thresholds');




% %t = [channel_pair(1 : (end-17)), 'threshold'];

t = Args.threshold;
if t>0
    thresholds = pair_thresholds(:,t);
else
    thresholds = 0;
end

%no criterion == 1
%correct/stable == 2
%correct/transition == 3
%incorrect/stable == 4
%incorrect/transition == 5

if Args.correct || Args.incorrect || Args.stable || Args.transition
    if Args.correct && Args.stable
        %trial_index = indexes{1};
        p_data = plot_data{2,(t+1)};
    elseif Args.correct && Args.transition
        %trial_index = indexes{2};
        p_data = plot_data{3,(t+1)};
    elseif Args.incorrect && Args.stable
        %trial_index = indexes{3};
        p_data = plot_data{4,(t+1)};
    elseif Args.incorrect && Args.transition
        %trial_index = indexes{4};
        p_data = plot_data{5,(t+1)};
    else
        fprintf(1,['\n','Must enter a combination of correct/incorrect and stable/transition','\n','\n']);
        %trial_index = 0;
        p_data = plot_data{1,(t+1)};
    end
    
    
end

number = p_data.number;
cors = p_data.cors;
phase = p_data.phase;
ntrials = p_data.ntrials;


%if ~trial_index  
  %  fprintf(1,['Number of Trials: ',num2str(size(p_data,2),'\n']);
 %   fprintf(1,'Must enter a combination of corret/incorrect and stable/unstable agruments.\n');
%else

trials = size(cors,2);


    fprintf(1,['Number of Trials: ',num2str(trials),'\n']);
    fprintf(1,'\n');
%end
% 
% if trial_index == 0
%     corrcoefs = abs(corrcoefs);
%     phases = phases;
%     
%    
%     
% else    
%     corrcoefs = abs(corrcoefs(:,trial_index));  %necessary for trials with negative peaks
%     phases = phases(:,trial_index); 
% end
% 
% 
% thresh = Args.threshold;   
% 
% %Index threshold for the correct threshold   
% if thresh > 0 
% 
%     th = obj.data.thresholds{ind};
% 
%     thresholds = th(:,thresh);
% 
% else
%     thresholds = 0;
% end
% 



% [rows columns] = size(corrcoefs);  %will be the same for the phases
% 
% cors = [];
% phase = [];
% number=[];
% 
% for yyy = 1:columns
%     corrss = [];
%     phasess= [];
%     x=[];
%     ntrials = [];
% 
% 
%          for xxx = 1:rows %50ms bins w/ a max at 3000ms
% 
%               %creates a row vector that contains the number of trials
%               %for each bin
%               ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
% 
%              %Determine if threshold is crossed  
%              if  corrcoefs(xxx,yyy) > thresholds
% 
%              %If it is then keep the number
%              bb = corrcoefs(xxx,yyy);  
% 
%              pp = phases(xxx,yyy);
% 
%              yy = 1;
%              else
% 
%              %Else, input NaN
%              bb = NaN;
% 
%              pp = NaN;
% 
%              yy = 0;
%              end
% 
%          x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
% 
%          corrss = cat(1,corrss, bb);
%          phasess = cat(1,phasess,pp);
% 
%          end
% 
%      number = cat(2,number,x); 
%      cors = cat(2,cors,corrss);
%      phase = cat(2,phase,phasess);
% end





%%Calculate the mean for the corcoefs
xy=(cors');

[r c] = size(xy);
meanss=[];
for ccc = 1:c
   mmeans=[]; 
    for rrr = 1:r
        q =(xy(rrr,ccc));
        qq = isnan(q);

         if qq == 1

         else

         mmeans = cat(1,mmeans,q);    

         end

       xxxx = mean(mmeans);
    end

    meanss = [meanss xxxx];

end
 
%%Calculate the means for the phases
xys=(phase');

[rs cs] = size(xys);
meansss=[];
for cccs = 1:cs
   mmeanss=[]; 
    for rrrs = 1:rs
        qs =(xys(rrrs,cccs));
        qqs = isnan(qs);

         if qqs == 1

         else

         mmeanss = cat(1,mmeanss,qs);    

         end

       xxxxs = mean(mmeanss);
    end


    meansss = [meansss xxxxs];

end
    
%tttt = percent of ind trials
%tttt = ((sum(number')/length(ind))*100);

n =[];
c = length(ntrials);
for z = 1 : c

    n = cat(2,n,trials);

end

numbertrials = n - ntrials;

number = number';

tttt = [];
for zz = 1 : length(numbertrials)

    if numbertrials(1,zz) == 0
        tt = 0;
    else
        tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
        %Determines acutal percentage of trials based on trials left
    end

    tttt = cat(2,tttt,tt);
end




    %used for tick labeling. Starts at 150ms
    q = (length(meansss)*50)+100;
    xxx = [150:50:q];
    yyy = [1:2:60];

    xx = [150:100:q];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 1
    cla
    subplot(3,1,1)
    title(subplot(3,1,1),obj.data.setNames{ind},'fontsize',10,'fontweight','b');
    hold on
    boxplot((cors'),'notch','on','symbol','w+');
    hold on
    plot(meanss,'r')
    hold on

            %Threshold
            xMin = 0; xMax = 60;

            yVal = thresholds;
            plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
            hold on

            %Draw Vertical Lines

            %Sample On(500ms)
            yMin = 0; yMax = 1;
            xVal = 8;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %Sample Off(1000ms)
            yMin = 0; yMax = 1;
            xVal = 18;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %First match onset (1800ms)
            yMin = 0; yMax = 1;
            xVal = 34;
            plot([xVal, xVal],[yMin yMax],'color','g')
            hold on

            %last match onset (2200ms)
            yMin = 0; yMax = 1;
            xVal = 42;
            plot([xVal, xVal],[yMin yMax],'color','r')


    hold off

    if t == 0
    ylim([.0 1]); 

    else
    ylim([.2 1]);   
    end  


    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)

    %set(gca,'XTickLabel',xxx,'FontSize',7)
    xlabel('Time(ms)')
    ylabel('Correlation Coefficients')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 2    
subplot(3,1,2)

qq = max(tttt(1:(end-10))) + 10;

plot(tttt,'r')
hold on
%plot(sum(number));
%hold on

%% plot regression line

            %separate out the end of sample to first match

            t = tttt(18:34);
            time = [1:17];

            stats = regstats(t,time);
            
            
            pval = stats.fstat.pval;
            
            %finds the slope of the regression line
            s = diff(stats.yhat);
            
            slope = s(1);
         
    

    hold on
    plot([nan(1,17) stats.yhat'],'color','k','LineWidth',2)

hold on
%%








             %Draw Vertical Lines

            %Sample On(500ms)
            %yMin = 0; yMax = q;
            yMin = 0; yMax =qq;
            xVal = 8;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %Sample Off(1000ms)
            yMin = 0; yMax = qq;
            xVal = 18;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %First match onset (1800ms)
            yMin = 0; yMax = qq;
            xVal = 34;
            plot([xVal, xVal],[yMin yMax],'color','g')
            hold on

            %Last match onset (2200ms)
            yMin = 0; yMax = qq;
            xVal = 42;
            plot([xVal, xVal],[yMin yMax],'color','r')  


hold off
set(gca,'XTick',yyy)
set(gca,'XTickLabel',xx)
ylabel('% of Trials W/ Siginficant Corr.')
ylim([0 qq]);
xlabel('Time(ms)')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 3  
    subplot(3,1,3)

    boxplot((phase'),'notch','on','symbol','r+');
    hold on
    plot(xxx,meansss,'r')
    ylabel('Time Lag of Max Correlation (ms)')
    hold on


    %set(gca,'XTickLabel',xxx)


            %Plot zero line
            xMin = 0; xMax = 60;

            yVal = 0;
            plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
            hold on

            %Draw Vertical Lines

            %Sample On(500ms)
            yMin = -25; yMax = 25;
            xVal = 8;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %Sample Off(1000ms)
            yMin = -25; yMax = 25;
            xVal = 18;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on

            %First match onset (1800ms)
            yMin = -25; yMax = 25;
            xVal = 34;
            plot([xVal, xVal],[yMin yMax],'color','g')
            hold on

            %Last match onset (2200ms)
            yMin = -25; yMax = 25;
            xVal = 42;
            plot([xVal, xVal],[yMin yMax],'color','r')



    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    xlabel('Time(ms)')
    ylim([-25 25]);
    hold off
    


    %linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');


    


%xlim([1 50])










