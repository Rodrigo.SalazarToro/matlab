function mtsMatchReaction


%determines correlation coefficients centered at reaction time

%START IN THE DAY DIRECTORY
cd('G:\data\clark');%\clark');

%Xcorr
windowSize = 200; 
windowStep = 10; 
phaseSize = 50; 

daydir = cd;
%Get all the switch days
%switchdays = switchDay;
load s s
switchdays = s;

f = filesep; %%File seperator (MAC VS PC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%Run for all switch days

for alldays = 6 : 6 %length(switchdays) %skip 14

cd (daydir); 
%indicate number of days remaining

DaysLeft = length(switchdays) - alldays
cd (cell2mat(switchdays(alldays)))
days = pwd;

%Run on sessions 2 and 3
ses = nptDir('session*');
for sessions = 2:2

%try

cd (days);
%get sessions   ses.name
cd (ses(sessions).name);
[NeuroInfo] = NeuronalChAssign;
%Get number of channels
chnumb=length(NeuroInfo.channels);
%calculate number of pairs (n choose 2)
pairs = (chnumb*(chnumb-1)) / 2;
%create trial object    
mt=mtstrial('auto');
%Get number of trials
NumberOfTrials = mt.data.numSets;


cd ('lfp');

ind = mtsgetTrials(mt,'BehResp',1,'stable');

load rejectedTrials rejectTrials
cd('lfp2')

save rejectedTrials rejectTrials %saves index of rejected trials in lfp2 folder

load avg_correlograms avg_corrcoef 

lfpdata = nptDir('clark*');





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function writeCPP
%WRITES  channelpair
%%%%%%%%%%%%%%%%%%%%%channelpairs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%get first saccade information
%take 5 windows and center the last on the first saccade
match = mt.data.MatchOnset;

pair_number = 0;   
allcorrs = cell(1,pairs);
for c1=1:chnumb

for c2=(c1+1):chnumb

    pair_number = pair_number + 1

    corrcoefs=[]; phases=[]; correlograms={};
    for x = ind

        
             
                load ([cd f lfpdata(x).name],'normdata');
                
                
                match_data = floor(match(x));
                
            
                
                normdata = normdata(:,((match_data-100) : (match_data+200)));
                
                

                %calculate number of bins
                steps = floor((length(normdata)-windowSize)/windowStep);
                start =  1:windowStep:steps*windowStep+1;
                endd = start+windowSize-1;

r = GrayXxcorr(normdata(c1,:),normdata(c2,:),windowSize,windowStep,phaseSize,start,endd);

pos_or_neg = avg_corrcoef{pair_number};

        %if the average correlogram has a negative peak that is closest to zero
        %then invert the correlograms to find the negative peaks that are
        %closest to zero
        if pos_or_neg < 0
            r = r * -1;
            inverted = 1;
        else
            inverted = 0;
        end

        lags = [(-1*phaseSize): phaseSize];

        %find closest peak to zero lag
        p=zeros(size(r,1),2);
        for kk=1:size(r,1)
            rr=r(kk,:);  

            %Find index of central positive peak
            peak = findpeaks(rr);

            for ppp = 1 : size(peak.loc,1)

                if (rr(peak.loc(ppp)) < 0)
                     %set to max phase index
                    peak.loc(ppp) = length(lags);
                end

            end

          [phase, index] = (min(abs(lags(peak.loc))));



            %get real phase value by indexing the location in the lags
            %get corr coef by indexing the correlogram
            peak_phase = lags(peak.loc(index));
            peak_corrcoef = rr(peak.loc(index));

            
            
            if size(peak.loc,1) > 0 
            %Stores the correlation coef and lag position.
                p(kk,2) = peak_phase;
                p(kk,1) = peak_corrcoef;
            
            else
                p(kk,1) = nan;
                p(kk,1) = nan;  
            end
        end


         %if the correlograms are inverted then make the corrcoefs
            %negative to correspond to their actual values
            if inverted == 1
                p(:,1) = p(:,1) * -1;

                r = r * -1; %reverse correlogram back   
            end


        %%coefs = the correlation coefs for the trial pair
        coefs=(p(:,1));

        %%phases = the phases
        phase=(p(:,2));

%         %Find the length of the coefs column.
%         cc = length(coefs);
%         dd = length(phase);

%         %Determines how many buffer NaN's are necessary
%         ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
%         ddd = (3000/windowStep) - dd;
% 
%         %make sure there are only 60 bins
%         if ccc >= 0 
% 
%         %Creates NaN buffer
%         cccc = NaN(1,ccc)';
%         dddd = NaN(1,ddd)';
% 
%         %%Concatenates NaN buffer on to the end of the column in order
%         %%for the matrix dimensions to match
%         coefs = cat(1,coefs,cccc);
%         phase = cat(1,phase,dddd);
% 
%         else
% 
%         coefs = coefs((1:end+ccc),1);     
%         phase = phase((1:end+ddd),1);
% 
%         end

        %%makes a matrix with all trials correlation coef for the
        %%specific pair
        corrcoefs = cat(2,corrcoefs, coefs);
        phases = cat(2,phases, phase);

        %places r in the cell location corresponding to the trial number
        correlograms{x} = r; 
        
           
    end
    all_corrs{pair_number} = corrcoefs;
    all_phases{pair_number} = phases;
end
end

end
end

save match_reaction_corrcoefs  all_corrs all_phases
%%