function lowPass4Xcorr(chnumb)

%filters the data from 10 - 100hz and normalizes((subtract mean) / std)
%creates a cell "channel_matrix" that contains matrices for each channel
%with all of the trials (trial X samples). columns = shortest trial length
%Filter
LowPassLow = 10; 
LowPassHigh = 50; 

lfpdir = cd;

plength = length(lfpdir);

%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp.****');

%make lfp2 directory
mkdir('lfp2')

%cross platform file seperator
f = filesep;
s = -1;
    

    %find shortest trial length
    for q = 1:length(lfpdata)
        
        cd(lfpdir)
        
        %load data
        trial = [cd f lfpdata(q).name];
        [datas]=nptReadStreamerFile(trial);
        
        sss = size(datas,2);
        
        %find trial with the least number of samples (s = length)
        if s < 0
            s = sss;
        end
        
        if sss < s
            s = sss;
        end 
        
    end

    channel_matrix = cell(1,chnumb);
    for x = 1 : length(lfpdata)
        
        cd(lfpdir)
        
        %load data
        trial = [cd f lfpdata(x).name];
        [datas,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(trial);


        %Creates structure 'd' w/ streamer data
        d.rawdata = datas;
        d.samplingRate = sampling_rate;
        d.numChannels = num_channels;
        d.scanOrder = scan_order;

        [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
        
        
        
    %normalize data by subtacting the mean and dividing by the standard deviation    
    normdata = zeros(size(data,1),size(data,2));
    for channels = 1 : chnumb
    
        %%%%%Normalize Data
        c1mean = mean(data(channels,:));
        c1std = std(data(channels,:));


        normdata(channels,:) = ((data(channels,:)-c1mean)/c1std);
    end

                 
         cd ('lfp2')
         
         %names the file the same as the previous file with the addition of
         %lfp2. Also, saves as a mat file

            
         trial2 = [trial((plength+2):end-5) '2' trial(end-4:end) '.mat'];

         save(trial2,'normdata') 
         
         
         %make a matrix with all trials for each channel. Used to make
         %surrgate for the average correlograms

         for xxx = 1 : num_channels

             channel_matrix{xxx} = cat(1,channel_matrix{xxx},normdata(xxx,(1:s)));

         end

    end
    save channel_matrix channel_matrix 
    
    
    fprintf(1,'Lfp2 directory made.\n')
        
        