function [c,surrogate,lag] = avgCorrelogram(data1,data2,varargin)

%calculates the average correlogram for data1 vs data2
%data1 and data2 are in (trails X sample)
%Args. lags: number of lags
%      type: xcorr = 0, xcov = 1
%      iterations: number trial shuffled correlograms to make for the surrogate 
%      surrogate: default = 0, run surrogate = 1

Args = struct('lags',50,'type',0,'iterations',1000,'surrogate',0);
Args.flags = {'type','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args);

%concatenate all trials(rows) to compute average correlogram
trials = size(data1,1);
samples = size(data1,2);

d1 = [];
d2 = [];
for xxx = 1 : trials
    d1 = cat(2,d1,data1(xxx,:));
    d2 = cat(2,d2,data2(xxx,:));
end

%calculate xcorr or xcov    
if Args.type 
   [c,lag] = xcov(d1,d2,Args.lags,'coef');
else  
   [c,lag] = xcorr(d1,d2,Args.lags,'coef'); 
end

%create surrogate matrix (surrogates X lags)
if Args.surrogate
    surrogate = zeros(Args.iterations,(2*Args.lags + 1));
    for yyy = 1 : Args.iterations
        
        %randomize trial order
        ordT1 = randperm(trials);
        ordT2 = randperm(trials);
            
            %concatenate the data1 trial shuffle
            s1 = [];
            for r1 = ordT1
                s1 = cat(2,s1,data1(r1,:));
            end
            
            %concatenate the data2 trial shuffle
            s2 = []; 
            for r2 = ordT2
                s2 = cat(2,s2,data2(r2,:));
            end
               
            %calculate xcorr or xcov surrogates  
            if Args.type 
                [sur_cor] = xcov(s1,s2,Args.lags,'coef');
            else  
                [sur_cor] = xcorr(s1,s2,Args.lags,'coef'); 
            end
            
            %surrogate = cat(1,surrogate,sur_cor);
            surrogate(yyy,:) = sur_cor;
            
    end
end






