function [peak,lag] = peaks(varargin)


Args = struct('data',[],'lags',[],'threshold',[]);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args);

data = Args.data;
if isempty(Args.lags)
    lags = 1 : max(size(data));
else
    lags = Args.lags;
end
f = findpeaks(data);
p = size(f.loc,1);

peak = [];
lag = [];
for x = 1:p
    peak = [peak;data(f.loc(x))];
    lag = [lag;lags(f.loc(x))];
end
    