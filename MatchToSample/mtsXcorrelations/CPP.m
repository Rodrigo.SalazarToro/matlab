function CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb,noplot,trialtype)


%Select channelpair12 from lfp folder
%Plots the correlation coefs, percents, and phases for viewCPP

global ind trial ml phase




if ml
    if strmatch(trialtype,'identity')
        ruletype = 1;
    elseif strmatch(trialtype,'location')
        ruletype = 2;
    end
    
    cd(trialtype)
    %Load the threshold file
    load threshold threshold
    cd ../../..
    sesdir = pwd;
    
    [all_channels,sorted_pairs,group_pair_list,~] = sorted_groups('ml');
    %only use sorted pairs
    all_pairs = group_pair_list(sorted_pairs,:)
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
else
    load threshold threshold
    load all_channels all_channels %these are the group numbers for each channel
    load all_pairs all_pairs
end

lfp2 = pwd;
cd ..
lfp = cd;
cd ..

if ~ml
    mt=mtstrial('auto','redosetnames');
else
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
end

objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);

cd(lfp)

if ts1 == 0
    tss1= 'transition';
else
    tss1= 'stable';
end

    %Determine which stimulus to use objects then locations 0 - 9
    if object == 0 && location == 0
        %Get files based on behavioral response and stimulus
        if ~ml
            ind = mtsgetTrials(mt,'BehResp',b1,tss1);   %'noRejNoisyT'
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'ML','rule',ruletype);
        end
    elseif object == 0
        if ~ml
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueLoc',locs(location));
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueLoc',locs(location),'ML','rule',ruletype);
        end
    elseif location == 0
        if ~ml
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object));
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'ML','rule',ruletype);
        end
    else
        if ~ml
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'CueLoc',locs(location));
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'CueLoc',locs(location),'ML','rule',ruletype);
        end
    end
    
    %Get files based on match time, 4 times:
    %1800-1900,1901-2000,2001,2100,2101,2200ms
    m = mt.data.MatchOnset(ind,:);
    matchonset = round(m);
    
    if match == 0
        ind = ind;
    elseif match == 1
        matches = find(1800 <= matchonset & matchonset <= 1900);
        ind = ind(:,matches);
    elseif match == 2
        matches = find(1900 <= matchonset & matchonset <= 2000);
        ind = ind(:,matches);
    elseif match == 3
        matches = find(2000 <= matchonset & matchonset <= 2100);
        ind = ind(:,matches);
    elseif match == 4
        matches = find(2100 <= matchonset);
        ind = ind(:,matches);
    end

cd(lfp2)

try
    trial = ([sessionname(1:end-6) num2strpad(all_channels(c1),2) num2strpad(all_channels(c2),2)]);
catch
    return
end

load(trial, 'corrcoefs','phases')
corrcoefs = abs(corrcoefs(:,ind));
phases = phases(:,ind);  %necessary for trials with negative peaks


thresh = str2double(threshh);

%Index threshold for the correct threshold
if thresh > 0
    if ~ml
        pair = 0;
        for x = 1 : c1
            if x == c1
                q = c2;
            else
                q= chnumb;
            end
            
            for y = (x+1) : q
                
                pair = pair + 1;
                
            end
        end
    else
        [i ii pair] =intersect([all_channels(c1) all_channels(c2)],all_pairs,'rows');
    end
    
    th = threshold{pair};
    if isempty(th)
        return
    end
    thresholds = th(:,thresh);
    
else
    thresholds = 0;
end
[rows columns] = size(corrcoefs);  %will be the same for the phases

cors = [];
phase = [];
number=[];

for yyy = 1:columns
    corrss = [];
    phasess= [];
    x=[];
    ntrials = [];
    
    
    for xxx = 1:rows %50ms bins w/ a max at 3000ms
        
        %creates a row vector that contains the number of trials
        %for each bin
        ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
        
        %Determine if threshold is crossed
        if  corrcoefs(xxx,yyy) > thresholds
            
            %If it is then keep the number
            bb = corrcoefs(xxx,yyy);
            
            pp = phases(xxx,yyy);
            
            yy = 1;
        else
            
            %Else, input NaN
            bb = NaN;
            
            pp = NaN;
            
            yy = 0;
        end
        
        x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
        
        corrss = cat(1,corrss, bb);
        phasess = cat(1,phasess,pp);
        
    end
    
    number = cat(2,number,x);
    cors = cat(2,cors,corrss);
    phase = cat(2,phase,phasess);
end

%%Calculate the mean for the corcoefs
xy=(cors');

[r c] = size(xy);
meanss=[];
for ccc = 1:c
    mmeans=[];
    for rrr = 1:r
        q =(xy(rrr,ccc));
        qq = isnan(q);
        
        if qq == 1
            
        else
            
            mmeans = cat(1,mmeans,q);
            
        end
        
        xxxx = mean(mmeans);
    end
    
    meanss = [meanss xxxx];
    
end

%%Calculate the means for the phases
xys=(phase');

[rs cs] = size(xys);
meansss=[];
for cccs = 1:cs
    mmeanss=[];
    for rrrs = 1:rs
        qs =(xys(rrrs,cccs));
        qqs = isnan(qs);
        
        if qqs == 1
            
        else
            
            mmeanss = cat(1,mmeanss,qs);
            
        end
        
        xxxxs = mean(mmeanss);
    end
    
    
    meansss = [meansss xxxxs];
    
end

%tttt = percent of ind trials
%tttt = ((sum(number')/length(ind))*100);

n =[];
c = length(ntrials);
for z = 1 : c
    
    n = cat(2,n,length(ind));
    
end

numbertrials = n - ntrials;

number = number';

tttt = [];
for zz = 1 : length(numbertrials)
    
    if numbertrials(1,zz) == 0
        tt = 0;
    else
        %Determines acutal percentage of trials based on trials left
        tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
        
    end
    
    tttt = cat(2,tttt,tt);
end


%used for tick labeling. Starts at 150ms
q = (length(meansss)*50)+50;
xxx = [100:50:q];
yyy = [1:2:60];

xx = [100:100:q];
if ~noplot
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 1
    cla
    subplot(3,1,1)
    boxplot((cors'),'notch','on');
    hold on
    plot(meanss,'r')
    hold on
    
    
    %             c_coefs = cors';
    %
    %             shadeplot('bins',c_coefs)
    
    
    %Threshold
    xMin = 0; xMax = 60;
    
    yVal = thresholds;
    plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
    hold on
    
    %Draw Vertical Lines
    
    %Sample On(500ms)
    yMin = 0; yMax = 1;
    xVal = 9;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %Sample Off(1000ms)
    yMin = 0; yMax = 1;
    xVal = 19;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %First match onset (1800ms)
    yMin = 0; yMax = 1;
    xVal = 35;
    plot([xVal, xVal],[yMin yMax],'color','g')
    hold on
    
    %last match onset (2200ms)
    yMin = 0; yMax = 1;
    xVal = 43;
    plot([xVal, xVal],[yMin yMax],'color','r')
    
    
    hold off
    
    if str2double(threshh) == 0
        ylim([.0 1]);
        
    else
        ylim([.2 1]);
    end
    
    
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    
    %set(gca,'XTickLabel',xxx,'FontSize',7)
    xlabel('Time(ms)')
    ylabel('Correlation Coefficients')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 2
    subplot(3,1,2)
    
    log_odds = 0;
    if log_odds == 1
        tttt = tttt./100;
        tttt = log(tttt./(1-tttt));
        qq =1;
        qqq = -1;
    else
        
        qq = max(tttt(1:46)) + 10;
        qqq=0;
    end
    
    plot(tttt,'r')
    hold on
    % %     plot(sum(number));
    % %     hold on
    
    
    %separate out the end of sample to first match
    
    t = tttt(25:33);
    time = [1:9];
    
    stats = regstats(t,time);
    
    
    pval = stats.fstat.pval;
    
    %finds the slope of the regression line
    s = stats.tstat.beta(2);
    
    
    slope = s(1);
    
    
    
    hold on
    plot([nan(1,24) stats.yhat'],'color','k','LineWidth',2)
    
    
    
    %Draw Vertical Lines
    
    %Sample On(500ms)
    %yMin = 0; yMax = q;
    yMin = qqq; yMax =qq;
    xVal = 9;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %Sample Off(1000ms)
    yMin = qqq; yMax = qq;
    xVal = 19;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %First match onset (1800ms)
    yMin = qqq; yMax = qq;
    xVal = 35;
    plot([xVal, xVal],[yMin yMax],'color','g')
    hold on
    
    %Last match onset (2200ms)
    yMin = qqq; yMax = qq;
    xVal = 43;
    plot([xVal, xVal],[yMin yMax],'color','r')
    
    
    hold off
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    ylabel('% of Trials W/ Siginficant Corr.')
    ylim([qqq qq]);
    xlabel('Time(ms)')
    
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 3
    subplot(3,1,3)
    %
    boxplot((phase'),'notch','on');
    hold on
    plot(xxx,meansss,'r')
    ylabel('Phase Shift of Max correlation (ms)')
    hold on
    
    
    
    % %             phase_vals = phase';
    % %
    % %             shadeplot('bins',phase_vals)
    
    %             phase_vals = phase';
    %
    %             shadeplot('bins',phase_vals)
    
    
    %set(gca,'XTickLabel',xxx)
    
    
    %Plot zero line
    xMin = 0; xMax = 60;
    
    yVal = 0;
    plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
    hold on
    
    %Draw Vertical Lines
    
    %Sample On(500ms)
    yMin = -25; yMax = 25;
    xVal = 9;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %Sample Off(1000ms)
    yMin = -25; yMax = 25;
    xVal = 19;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %First match onset (1800ms)
    yMin = -25; yMax = 25;
    xVal = 35;
    plot([xVal, xVal],[yMin yMax],'color','g')
    hold on
    
    %Last match onset (2200ms)
    yMin = -25; yMax = 25;
    xVal = 43;
    plot([xVal, xVal],[yMin yMax],'color','r')
    
    
    
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    xlabel('Time(ms)')
    ylim([-25 25]);
    hold off
    
    % % % % %         sc = size(cors,2);
    % % % % %         perm_trials = randperm(sc);
    % % % % %         p = perm_trials(1:10);
    % % % % %         figure;subplot(2,1,1);plot(nanmean(cors(:,(p))'));subplot(2,1,2);plot(cors(:,11)')
    % % % % %
    % % % % %
    % % % % %         sc = size(phase,2);
    % % % % %         perm_trials = randperm(sc);
    % % % % %         p = perm_trials(1:10);
    % % % % %         figure;subplot(2,1,1);plot(nanmean(phase(:,(p))'));subplot(2,1,2);plot(phase(:,11)')
    
    
    
    
    
    % % % % % % % % % % % % % % %
    % % % % % % % % % % % % % % % ttt = phase(:,(31:34))';
    % % % % % % % % % % % % % % % figure;plot(nanstd(ttt'));
    % % % % % % % % % % % % % % % vartestn(ttt')
    % % % % % % % % % % % % % % %
    %figure,plot(meansss/(max(meansss))),hold on, plot(tttt/(max(tttt)),'r')
    
    %linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');
end
%zoom on
%linkzoom
end
