function writeCPPalldays


%Writes thresholds and channelpairs for all switch days
%Files are saved in the ...day/session/lfp

%START IN THE DAY DIRECTORY
cd('F:\data\clark');
daydir = cd;


%Filter
LowPassLow = 10; 
LowPassHigh = 100; 


%Xcorr
windowSize = 200; 
windowStep = 50; 
phaseSize = 50; 


%Get all the switch days
switchdays = switchDay;


f = filesep; %%File seperator (MAC VS PC)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   


%Run for all switch days

for alldays = 1:1%length(switchdays) %skip 14
    
    cd (daydir); 
    %indicate number of days remaining
    DaysLeft = length(switchdays) - alldays
    
    
    cd (cell2mat(switchdays(alldays)));
    days = pwd;

%Run on sessions 2 and 3
ses = nptDir('session*');
for sessions = 2:3
    
    cd (days);
    %get sessions   ses.name
    cd (ses(sessions).name);
    
    [NeuroInfo] = NeuronalChAssign;

    %Get number of channels
    chnumb=length(NeuroInfo.channels);

    %create trial object    
    mt=mtstrial('auto');
    
    %Get number of trials
    NumberOfTrials = mt.data.numSets;

    cd ('lfp');


%writes thresholds for channelpair files
%%%%%%%%%%%%%%%%%%%%%%%%Thresholds%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ddata = [];
    lfpdata = nptDir('clark*');
    for x = 1:NumberOfTrials
        
        %get index of all lfp trials  lfpdata.name
        lfpdata = nptDir('clark*');


        %load data
        [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile([cd f lfpdata(x).name]);


        %Creates structure 'd' w/ streamer data
        d.rawdata = data;
        d.samplingRate = sampling_rate;
        d.numChannels = num_channels;
        d.scanOrder = scan_order;

        [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);

        %Concatenates the data from all lfp files to run surrogate analysis
        ddata = [ddata data];

    end


    rr_surrogate=[];
    for ii=1:chnumb
        fprintf('\n%0.5g     ',ii)
        for jj=ii+1:chnumb
            fprintf(' %0.5g',jj)

            %find xcorr at random shifts
            r_surrogate = RandXxcorr(ddata(ii,:),ddata(jj,:),windowSize,round(10000));

            rr_surrogate = [rr_surrogate r_surrogate];

        end
    end
    
    threshold=[];
    %%this determines the number of rand0m samples for all pairwise combos.
    numbrandsamples = ((chnumb * (chnumb-1)) / 2) * 10000;

    for p = 1:10000:numbrandsamples

        %Loads 10000 samples for each pairwise combination
        pp = rr_surrogate(1,(p:(p+9999)));

        percentiles = prctile(pp,[95 99 99.9 99.99]);

        threshold = [threshold percentiles];

    end
    save threshold threshold


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function writeCPP
%WRITES  channelpair
%%%%%%%%%%%%%%%%%%%%%channelpairs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 for c1=1:chnumb

    for c2=(c1+1):chnumb

        corrcoefs=[]; phases=[]; correlograms={};
        for x = 1:NumberOfTrials
    
    [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile([cd f lfpdata(x).name]);
    %Creates structure 'd' w/ streamer data
    d=struct;
    d.rawdata = data;
    d.samplingRate = sampling_rate;
    d.numChannels = num_channels;
    d.scanOrder = scan_order;
    [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
    
    %calculate number of bins
    steps = floor((length(data)-windowSize)/windowStep);
    start =  1:windowStep:steps*windowStep+1;
    endd = start+windowSize-1;
    centers = start+windowSize/2-1;

    %%%%%Normalize Data
    c1mean = mean(data(c1,:));
    c2mean = mean(data(c2,:));
    c1std = std(data(c1,:));
    c2std = std(data(c2,:));

    normdata = [((data(c1,:)-c1mean)/c1std) ; ((data(c2,:)-c2mean)/c2std)];
    r = GrayXxcorr(normdata(1,:),normdata(2,:),windowSize,windowStep,phaseSize,start,endd);

    %find closest peak to zero lag
            p=zeros(size(r,1),2);
            for kk=1:size(r,1)
                rr=r(kk,:);
                t=fpeak(1:length(rr),rr,1,[1,length(rr),0,inf]);
                phase=t(:,1)-phaseSize-1;
                [y,ind]=min(abs(phase));
                if isempty(t) %all negative correlations
                    p(kk,1)=0;
                    p(kk,2)=0;
                elseif y==phaseSize %max correlation is at max phase
                    p(kk,1)=0;
                    p(kk,2)=0;
                else
                    p(kk,1) = t(ind,2);
                    p(kk,2) = phase(ind);
                end
            end
    
            
            
            %%coefs = the correlation coefs for the trial pair
            coefs=(p(:,1));
            
            %%phases = the phases
            phase=(p(:,2));
            
            %Find the length of the coefs column.
            cc = length(coefs);
            dd = length(phase);
            
            %Determines how many buffer NaN's are necessary
            ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
            ddd = (3000/windowStep) - dd;

            %make sure there are only 60 bins
            if ccc >= 0 
            
            %Creates NaN buffer
            cccc = NaN(1,ccc)';
            dddd = NaN(1,ddd)';
            
            %%Concatenates NaN buffer on to the end of the column in order
            %%for the matrix dimensions to match
            coefs = cat(1,coefs,cccc);
            phase = cat(1,phase,dddd);
            
            else
                
            coefs = coefs((1:end+ccc),1);     
            phase = phase((1:end+ddd),1);

            end
     
            %%makes a matrix with all trials correlation coef for the
            %%specific pair
            corrcoefs = cat(2,corrcoefs, coefs);
            phases = cat(2,phases, phase);
            
            %places r in the cell location corresponding to the trial number
            correlograms{x} = r; 
        end
    
 pairs = [ 'channelpair' num2str(c1) num2str(c2) ]
 
 save(pairs,'corrcoefs','phases','correlograms') 


    end 
    
end

end

end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












