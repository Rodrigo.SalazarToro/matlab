function avg_plots(t)

%make mtsCPP object first
%cortex: 'pp','pf','ff'
%behresp: 'correct', 'incorrect'
%type: 'percents','phases'
cd('/media/MONTY/data/clark')

daydir = cd;



load ss ss

%cc = processDays(mtsCPP,'Days',ss,'NoSites')

load cc cc
 
 

for c = 1:3
    
    if c ==1
        cortex = 'pp';
    elseif c == 2
        cortex = 'pf';
    else
        cortex = 'ff';
    end

    for b= 1 : 2
        
        
            behresp = 'correct'; %selects the same trials for correct and incorrect responses
   

        [r,id] = get(cc,'Number','threshold',t,behresp,'stable','pval',.01,'slope',.5,cortex);
        
        id
        
        
        

        if b == 1 
            
            if t == 1

                load avg_info_correct_t1 all_percents all_phases
            else
                load avg_info_correct_t2 all_percents all_phases
            end

        elseif b == 2
            
            if t == 1

                load avg_info_incorrect_t1 all_percents all_phases
                behresp = 'incorrect';
            
            else 
                load avg_info_incorrect_t2 all_percents all_phases
                behresp = 'incorrect';
            end
            
        end


        percent_pairs = all_percents(id,:);

        phase_pairs = all_phases(id,:);


    

%%

figure

        %used for tick labeling. Starts at 150ms
        q = (length(percent_pairs)*50)+100;
        xxx = [150:50:q];
        yyy = [1:2:60];
       
        xx = [150:100:q];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 1
subplot(2,1,1); 
m2 =  mean(percent_pairs);
s2 = std(percent_pairs);
plot(m2);
hold on
plot(m2+s2,'r'); 
hold on
plot(m2-s2,'r');
hold on
                

                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = 0; yMax = 100;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = 100;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = 100;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = 0; yMax = 100;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')  
    
    hold off
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    ylabel('Mean plus/minus 1 std Of All Percents')
    ylim([0 100]);
    xlabel('Time(ms)')
     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 2
subplot(2,1,2); 
m3 = mean(phase_pairs);
s3 = std(phase_pairs);
plot(m3);
hold on;
plot(m3+s3,'r'); hold on; plot(m3-s3,'r');
hold on



                %Plot zero line
                xMin = 0; xMax = 60;
                
                yVal = 0;
                plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
                hold on
                
                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = -25; yMax = 25;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = -25; yMax = 25;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = -25; yMax = 25;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = -25; yMax = 25;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')
                hold off

        ylabel('Mean plus/minus 1 std For The Std of All Phases')
        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        xlabel('Time(ms)')
        ylim([-25 25]);
        
        
        ttt = [cortex,'    ',behresp,'  ',num2str(t)];
 
        title(subplot(2,1,1),ttt,'fontsize',10,'fontweight','b');
        
        

        linkaxes([subplot(2,1,1),subplot(2,1,2)],'x');
                
zoom on   









    end
end







    











    
    