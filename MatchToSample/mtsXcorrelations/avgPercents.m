function  avgPercents(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb)


%Used by mtsCPP to view the averages for either pp,ff,or pf, depending on which being viewed (c1,c2) 
load threshold threshold
 
    lfp2 = cd; 
    cd ..
    lfp = cd;
    cd ..

    mt=mtstrial('auto');
    
    objs =  unique(mt.data.CueObj);
    
    locs =  unique(mt.data.CueLoc);
    [frontalchannels, comb, frontal] = checkChannels('cohPF');
    [interchannels, comb, inter] = checkChannels('cohInter');
    [parietalchannels,comb, parietal] = checkChannels('cohPP');

    cd(lfp)
    
    if ts1 == 0
        tss1= 'transition';
    else
        tss1= 'stable';
    end

    %Determine which stimulus to use objects then locations 0 - 9
    if object == 0 && location == 0
        %Get files based on behavioral response and stimulus
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0);
    elseif object == 0
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueLoc',locs(location));
    elseif location == 0
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object));
    else
        ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object),'CueLoc',locs(location));
    end


    %Get files based on match time, 4 times:
    %1800-1900,1901-2000,2001,2100,2101,2200ms
    m = mt.data.MatchOnset(ind,:);
    matchonset = round(m);

    if match == 0
        ind = ind;
    elseif match == 1
        matches = find(1800 <= matchonset & matchonset <= 1900);
        ind = ind(:,matches);
    elseif match == 2
        matches = find(1900 <= matchonset & matchonset <= 2000);
        ind = ind(:,matches);
    elseif match == 3
        matches = find(2000 <= matchonset & matchonset <= 2100);
        ind = ind(:,matches);
    elseif match == 4
        matches = find(2100 <= matchonset);
        ind = ind(:,matches);
    end

     
    %Determine whether the pair is pp,ff,pf
    if sum(c1 == frontal(:,1)) > 0
        cc = frontal;
        dd = size(frontal,1);
        t = 'Frontal Vs Frontal';

    elseif sum(c2 == parietal(:,2)) > 0
        cc = parietal;
        dd = size(parietal,1);
        t = 'Parietal Vs Parietal';

    else
        cc = inter;
        dd = size(inter,1);
        t = 'Parietal Vs Frontal';
    end
    
%%
cc = [1 5;1 6;1 8;1 9;2 5;2 6;2 8;2 9;3 5;3 6;3 8; 3 9; 4 5;4 7;4 8]
dd = 15



%%
    
    
 cd(lfp2)
 corcoefss = [];   
 percents = [];
 phasesss = [];
 for  allpairs = 1:dd
     
     c1 = cc(allpairs,1)
     c2 = cc(allpairs,2)

        
        
        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
        
        load(trial, 'corrcoefs','phases')
        corrcoefs = abs(corrcoefs(:,ind));
        phases = phases(:,ind);
        
        
     thresh = str2double(threshh);   
    
     %Index threshold for the correct threshold   
     if thresh > 0 
    pair = 0;
    for x = 1 : c1
        if x == c1 
            q = c2; 
        else
            q = chnumb;
        end
        
        for y = (x+1) : q
            
            pair = pair + 1;
            
        end
    end
    th = threshold{pair};
 
    thresholds = th(:,thresh);
    
    else
        thresholds = 0;
    end

   
    [rows columns] = size(corrcoefs);  %will be the same for the phases
    cors = [];
    phase = [];
    number=[];

    for yyy = 1:columns
        corrss = [];
        phasess= [];
        x=[];
        ntrials = [];

             for xxx = 1:rows %50ms bins w/ a max at 3000ms
                  
                 %creates a row vector that contains the number of trials
                 %for each bin
                 ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                 %Determine if threshold is crossed  
                 if  corrcoefs(xxx,yyy) > thresholds
                 %If it is then keep the number
                 bb = corrcoefs(xxx,yyy);
                 pp = phases(xxx,yyy);
                 yy = 1;
                 else
                 %Else, input NaN
                 bb = NaN; 
                 pp = NaN;
                 yy = 0;
                 end
      
             x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
             
             corrss = cat(1,corrss, bb);
             phasess = cat(1,phasess,pp);
             
             end
             
         number = cat(2,number,x); 
         cors = cat(2,cors,corrss);
         phase = cat(2,phase,phasess);
    end
    

    %%Calculate the mean for the corcoefs
    xy=(cors');
    [r c] = size(xy);
    meanss=[];
    for ccc = 1:c
       mmeans=[]; 
        for rrr = 1:r
            q =(xy(rrr,ccc));
            qq = isnan(q);
             if qq == 1  
             else 
             mmeans = cat(1,mmeans,q);             
             end
           xxxx = mean(mmeans);
        end
        meanss = [meanss xxxx];
    end
    
    %%Calculate the means for the phases
    xys=(phase');
    [rs cs] = size(xys);
    meansss=[];
    for cccs = 1:cs
       mmeanss=[]; 
        for rrrs = 1:rs
            qs =(xys(rrrs,cccs));
            qqs = isnan(qs);
             if qqs == 1 
             else              
             mmeanss = cat(1,mmeanss,qs);       
             end 
           xxxxs = mean(mmeanss);
        end
        meansss = [meansss xxxxs];
    end  
    
    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    n =[];
    c = length(ntrials);
    for z = 1 : c
        n = cat(2,n,length(ind));
    end
    
   numbertrials = n - ntrials;
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end
      corcoefss = cat(1,corcoefss,meanss);
      percents = cat(1,percents,tttt);
      phasesss = cat(1,phasesss,meansss);
 end 
 
figure
title(gca,'title')

        %used for tick labeling. Starts at 150ms
        q = (length(meansss)*50)+100;
        xxx = [150:50:q];
        yyy = [1:2:60];
       
        xx = [150:100:q];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 1
 
subplot(3,1,1); 
m1 = mean(corcoefss);
s1 = std(corcoefss);
m1s1 = m1 + s1;
mstd1 = max(m1s1(1,(1:end-10))) + .1;
plot(m1);
title(t,'fontsize',12,'fontweight','b');
hold on;
plot(m1+s1,'r'); 
hold on
plot(m1-s1,'r'); 
hold on
        
                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = 0; yMax = mstd1;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = mstd1;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = mstd1;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %last match onset (2200ms)
                yMin = 0; yMax = mstd1;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')
      

        hold off
        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        ylim([0 mstd1]);
        xlabel('Time(ms)')
        ylabel('Avgerage Correlation Coefficients')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 2
subplot(3,1,2); 
m2 =  mean(percents);
s2 = std(percents);
m2s2 = m2 + s2;
mstd = max(m2s2(1,(1:end-10))) + 10;
plot(m2);
hold on
plot(m2+s2,'r'); 
hold on
plot(m2-s2,'r');
hold on
                

                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = 0; yMax = mstd;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = mstd;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = mstd;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = 0; yMax = mstd;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')  
    
    hold off
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    ylabel({'Avgerage Percent of Trials';'with Siginficant Correlation'})
    ylim([0 mstd]);
    xlabel('Time(ms)')
     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 3
subplot(3,1,3); 
m3 = mean(phasesss);
s3 = std(phasesss);
plot(m3);
hold on;
plot(m3+s3,'r'); hold on; plot(m3-s3,'r');
hold on

                %Plot zero line
                xMin = 0; xMax = 60;
                
                yVal = 0;
                plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
                hold on
                
                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = -25; yMax = 25;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = -25; yMax = 25;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = -25; yMax = 25;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = -25; yMax = 25;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')
                hold off

        ylabel({'Avgerage Phase Shift';'of Max correlation (ms)'})
        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        xlabel('Time(ms)')
        ylim([-25 25]);
        

        linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');
                
zoom on   

  
    
end

  

  
 

 