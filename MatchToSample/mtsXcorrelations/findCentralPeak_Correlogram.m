function centralpeak = findCentralPeak_Correlogram(data1,data2,lag)      

%Creates a correlogram using the xcorr function with the specfied lag.
%Finds the peak of the correlogram (positive or negative) that is closest to the center.
%Outputs the correlogram and the lags (used for plotting)
%along with the value of the central peaks lag and its correlation coefficent as a 1 X 4 cell.

%Create cell for storage
centralpeak =  cell(1,3);

%Create and store correlogram
[c, lags] = xcorr(data1,data2,lag);
centralpeak{1} = c;
centralpeak{2} = lags;

%Calculate center
center = size(lags,2) / 2;
 
%Find index of central positive peak
pp = findpeaks(c);
[pos_peak, pos_index] = (min(abs(pp.loc - center)));

%Find index of central negative peak (inverts correlogram)
np = findpeaks((c*-1));
[neg_peak, neg_index] = (min(abs(np.loc - center)));

%Determines if the closest peak to zero is positive or negative.
%Stores the correlation coef and lag position.
if pos_peak <= neg_peak
    peak_lag = pp.loc(pos_index);
    corr_coef = c(1,peak_lag);
else
    peak_lag = np.loc(neg_index);
    corr_coef = c(1,peak_lag);
end

%Lag value of central peak 
centralpeak{3} = peak_lag;
%Correlation Coef at lag
centralpeak{4} = corr_coef;

