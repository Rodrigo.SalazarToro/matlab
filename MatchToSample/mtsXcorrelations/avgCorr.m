function avgCorr(channel1,channel2,chnumb)


%load avg_correlograms avg_correlograms avg_phase avg_corrcoef

%load avg_surrogates avg_surrogates

load avg_correlograms_epochs avg_correlograms_epochs

    
    pair = 0;
    ylimits = 0;
    for x = 1 : channel1
        if x == channel1 
            q = channel2; 
        else
            q= chnumb;
        end
        
        for y = (x+1) : q
            
            pair = pair + 1
            
        end
    end
        
    
    

% % % %get correlogram    
% % % c = avg_correlograms{pair};
% % % corrcoef = avg_corrcoef{pair};
% % % phase = avg_phase{pair};
% % % 
% % % %get surrogate
% % % s = avg_surrogates{pair};
% % % m = mean(s);
% % % pos2std = 2*(std(s));
% % % 
% % % 
% % % 
% % % 
% % %         figure
% % %         plot(-50:50,c) 
% % %         hold on
% % %         
% % %         
% % %     %draw phase line
% % %     yMin = -1; yMax = 1;
% % %     xVal = phase;
% % %     plot([xVal, xVal],[yMin yMax],'color','k')
% % %     hold on
% % %     
% % %     
% % %     %draw correlation coef line          
% % %     xMin = -50; xMax = 50;
% % %     yVal = corrcoef;
% % %     plot([xMin xMax],[yVal, yVal],'color','r')
% % %     
% % %     %draw mean          
% % %     xMin = -50; xMax = 50;
% % %     yVal = m;
% % %     plot([xMin xMax],[yVal, yVal],'color','k')
% % %     
% % %     %draw 2nd standard deviation positive          
% % %     xMin = -50; xMax = 50;
% % %     yVal = m + pos2std;
% % %     plot([xMin xMax],[yVal, yVal],'color','g')
% % %     
% % %     %draw 2nd standard deviation positive          
% % %     xMin = -50; xMax = 50;
% % %     yVal = m - pos2std;
% % %     plot([xMin xMax],[yVal, yVal],'color','g')
% % %     
% % %     
% % %         t = ['Channelpair ',num2str(channel1),' ',num2str(channel2),'.   Average Correlogram Across all Trials.'];
% % % 
% % %         title(gca,t,'fontsize',12,'fontweight','b');
% % %         
% % %         if (pos2std > abs(corrcoef))
% % %             ylimits = pos2std;
% % %         else
% % %             ylimits = abs(corrcoef);
% % %         end
% % %             
% % %         axis([-50 50 (-1*(ylimits+1)) ylimits+1]) 
% % %         
        
        
        
%%%%%%%%%%%%%%%%%%%%%PLOT EPOCHS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c_epochs = avg_correlograms_epochs{pair};

figure


lags = [-50:50];

yRange = 0;
for x = 1 : 4, 
    
    subplot(4,1,x), 


  
        plot(lags,c_epochs(x,:))
        hold on
    
        

        cc = c_epochs(x,:);
        
        max_lag = size(c_epochs(x,:),2);
        
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

        %Find index of central positive peak
        pp = findpeaks(cc);
        
                for ppp = 1 : size(pp.loc,1)

                    if (cc(pp.loc(ppp)) < 0)
                         %set to max phase index
                        pp.loc(ppp) = max_lag;
                    end
                    
                end
        
        
        [phase, pos_index] = (min(abs(lags(pp.loc))));
        %%%%%%%%%%get real phase value by indexing the location in the lags
        pos_phase = lags(pp.loc(pos_index));
        pos_corrcoef = cc(pp.loc(pos_index));
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

        %Find index of central negative peak (inverts correlogram)
        cc = cc*-1;
        np = findpeaks(cc);
        
                for nnn = 1 : size(np.loc,1)

                    if (cc(np.loc(nnn)) < 0)
                         %set to max phase index
                        np.loc(nnn) = max_lag;
                    end
                    
                end
        
        
        [phase, neg_index] = (min(abs(lags(np.loc))));
        
        neg_phase = lags(np.loc(neg_index));
        neg_corrcoef = cc(np.loc(neg_index));    


        %Determines if the closest peak to zero is positive or negative.
        %Stores the correlation coef and lag position.
        if abs(pos_phase) <= abs(neg_phase)
            avg_phase = pos_phase;
            avg_corrcoef = pos_corrcoef;
        else
            avg_phase = neg_phase;
            avg_corrcoef = (-1 * neg_corrcoef); %make negative
        end

      
 
        
    %draw phase line
    yMin = -1; yMax = 1;
    xVal = avg_phase;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    
    %draw correlation coef line          
    xMin = -50; xMax = 50;
    yVal = avg_corrcoef;
    plot([xMin xMax],[yVal, yVal],'color','r')
    hold on
        
%     %draw mean          
%     xMin = -50; xMax = 50;
%     yVal = m;
%     plot([xMin xMax],[yVal, yVal],'color','k')
%     hold on
%     
%     %draw 2nd standard deviation positive          
%     xMin = -50; xMax = 50;
%     yVal = m + pos2std;
%     plot([xMin xMax],[yVal, yVal],'color','g')
%     hold on
%     
%     %draw 2nd standard deviation positive          
%     xMin = -50; xMax = 50;
%     yVal = m - pos2std;
%     plot([xMin xMax],[yVal, yVal],'color','g')
    
%     if abs(corrcoef) > yRange
%         yRange = abs(corrcoef);
%     end
%     if pos2std > yRange
%         yRange = pos2std;
%     end
      
        
end
        
            t = ['Channelpair ',num2str(channel1),' ',num2str(channel2),'.   Average Correlogram Across all Trials for Epochs 1 - 4.'];

            title(subplot(4,1,1),t,'fontsize',12,'fontweight','b');
            
            linkaxes([subplot(4,1,1),subplot(4,1,2),subplot(4,1,3),...
            subplot(4,1,4)],'y');
%             
%             axis([-50 50 (-1*(yRange+1)) yRange+1])    

  
        
        
        
        
        
        
        
        
        
        
    