function averageCorrelogram(varargin)
%computes average correlogram for each channel pair across all trials.
%computes the surrogate thresholds for the average correlograms

Args = struct('ml',[],'chnumb',[],'monkey',[],'correlograms',[],'surrogate',[],'all_channels',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

ddd = pwd;
cd([ddd filesep 'lfp2'])
%get index of trials
lfpdata = nptDir([Args.monkey '*']);
%trials =  size(lfpdata,1);
cd(ddd)
number_pairs = ((Args.chnumb*(Args.chnumb-1)) / 2);
avg_correlograms = cell(1,number_pairs);
d = cd;
cd ..

if ~Args.ml
    mt=mtstrial('auto');
    %get only the specified trial indices (correct and stable)
    trials = mtsgetTrials(mt,'BehResp',1,'stable');
else
    mt = mtstrial('auto','ML','RTfromML');
    
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
end
cd(d)
r = 0;
if Args.ml
    rules = [1 2];
else
    rules = [1];
end

if Args.correlograms
    for rule = rules
        if Args.ml
            r = r + 1;
            if rule == 1;
                trials = identity;
            else
                trials = location;
            end
        end
        cd([ddd filesep 'lfp2'])
        num_trials = size(trials,2);
        %only uses correct stable trials to calculate the avg correlograms
        for x = trials
            load ([lfpdata(x).name],'normdata');
            %make sure that no artifacts are in the trial
            %         times = b.CodeTimes{x};
            %         match = times(8) - times(1);
            %         normdata = normdata(:,(1:match));
            normdata = normdata(:,(1:end-200));
            ccc = 0;
            for c1 = 1:Args.chnumb
                for c2 = (c1 + 1) : Args.chnumb
                    ccc = ccc + 1;
                    [c,lags] = xcorr(normdata(c1,:),normdata(c2,:),50,'coef');
                    if isempty(avg_correlograms{ccc})
                        avg_correlograms{ccc} =  c;
                    else
                        avg_correlograms{ccc} = avg_correlograms{ccc} + c;
                    end
                end
            end
        end
        %Find central peak
        avg_phase = cell(1,number_pairs);
        avg_corrcoef = cell(1,number_pairs);
        pos_phases = cell(1,number_pairs);
        pos_corrcoefs = cell(1,number_pairs);
        for www = 1 : size(avg_correlograms,2)
            %average correlograms by the number of trials
            avg_correlograms{www} = avg_correlograms{www} / num_trials;
            %find Peak closest to zero and get corrcoef and phase
            cc = avg_correlograms{www};
            max_lag = size(avg_correlograms{www},2);
            %Find index of central positive peak
            pp = findpeaks(cc);
            for ppp = 1 : size(pp.loc,1)
                if (cc(pp.loc(ppp)) < 0)
                    %set to max phase index
                    pp.loc(ppp) = max_lag;
                end
            end
            [phase, pos_index] = (min(abs(lags(pp.loc))));
            %get real phase value by indexing the location in the lags
            pos_phase = lags(pp.loc(pos_index));
            pos_corrcoef = cc(pp.loc(pos_index));
            %find index of central negative peak (inverts correlogram)
            cc = cc*-1;
            np = findpeaks(cc);
            for nnn = 1 : size(np.loc,1)
                if (cc(np.loc(nnn)) < 0)
                    %set to max phase index
                    np.loc(nnn) = max_lag;
                end
            end
            [phase, neg_index] = (min(abs(lags(np.loc))));
            neg_phase = lags(np.loc(neg_index));
            neg_corrcoef = cc(np.loc(neg_index));
            %determines if the closest peak to zero is positive or negative.
            %stores the correlation coef and lag position.
            if abs(pos_phase) <= abs(neg_phase)
                avg_phase{www} = pos_phase;
                avg_corrcoef{www} = pos_corrcoef;
            else
                avg_phase{www} = neg_phase;
                avg_corrcoef{www} = (-1 * neg_corrcoef); %make negative
            end
            %% Only for positive phase
            pos_phases{www} = pos_phase;
            pos_corrcoefs{www} = pos_corrcoef;
            %%
        end
        
        all_channels = Args.all_channels;  %THESE ARE THE CHANNELS USED!
        all_pairs = [];
        ap = 0;
        for ap1 = 1: Args.chnumb
            for ap2 = (ap1+1) : Args.chnumb
                ap = ap + 1;
                all_pairs(ap,:) = [all_channels(ap1) all_channels(ap2)]; 
            end
        end
        if Args.ml
            if rule == 1;
                mkdir('identity')
                cd('identity')
                save avg_correlograms avg_correlograms avg_phase avg_corrcoef pos_phases pos_corrcoefs
                save all_channels all_channels
                save all_pairs all_pairs
            else
                mkdir('location')
                cd('location')
                save avg_correlograms avg_correlograms avg_phase avg_corrcoef pos_phase pos_corrcoef
                save all_channels all_channels
                save all_pairs all_pairs
            end
        else
            save avg_correlograms avg_correlograms avg_phase avg_corrcoef pos_phase pos_corrcoef
            save all_channels all_channels
            save all_pairs all_pairs
        end
    end
end

%make surrogates (make average correlograms first)
if Args.surrogate
    cd([ddd filesep 'lfp2'])
    load channel_matrix channel_matrix
    %load avg_correlograms
    iterations = 1000;
        %avg_surrogates = cell(1,size(avg_correlograms,2));
    for rule = rules
        if Args.ml
            r = r + 1;
            if rule == 1;
                trials = identity;
            else
                trials = location;
            end
        end
        cd([ddd filesep 'lfp2'])
        num_trials = size(trials,2);

        pair = 0;
        for cc1 = 1 : Args.chnumb
            %loads the each channel
            data1 = channel_matrix{1,cc1};
            for cc2 = (cc1 + 1) : Args.chnumb
                data2 = channel_matrix{1,cc2};
                pair = pair + 1
                all_correlations = zeros(iterations,1);
                for iter = 1 : iterations
                    x1 = zeros(1,num_trials);
                    t = 0;
                    while(t == 0)
                        t1 = randperm(num_trials);
                        t2 = randperm(num_trials);
                        for check = 1: num_trials
                            if t1(check) == t2(check)
                                t = 0;
                                break
                            else
                                t = 1;
                            end
                        end
                    end
                    for permut = 1 : num_trials
                        tt1 = trials(1,t1(1,permut));
                        tt2 = trials(1,t2(1,permut));
                        d1 = data1(tt1,:);
                        d2 = data2(tt2,:);
                        x1(1,permut) = xcorr(d1,d2,0,'coef');
                    end
                    all_correlations(iter,:) = mean(x1);
                end
                avg_surrogates{pair} = all_correlations;
            end
        end
        
        if Args.ml
            if rule == 1;
                mkdir('identity')
                cd('identity')
                save avg_surrogates avg_surrogates
            else
                mkdir('location')
                cd('location')
                save avg_surrogates avg_surrogates
            end
        else
            save avg_surrogates avg_surrogatescoef
        end
    end
end

fprintf(1,'\n')
fprintf(1,'Average Correlograms and Surrogates Done.\n')


