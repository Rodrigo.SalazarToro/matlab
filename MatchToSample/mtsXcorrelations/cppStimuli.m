function cppStimuli(b1,ts1,c1,c2,threshh,sessionname,match,chnumb,trialtype,ml)

plot_on = 0;
delay = 0;

%Used by mtsCPP to view the averages percents for all stimuli for ONE
%CHANNEL PAIR
cd(trialtype)
load threshold threshold
cd ..


lfp2 = cd;
cd ..
lfp = cd;
cd ..

N = NeuronalHist;

mt=mtstrial('auto');
objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);
[frontalchannels, comb, frontal] = checkChannels('cohPF');
[interchannels, comb, inter] = checkChannels('cohInter');
[parietalchannels,comb, parietal] = checkChannels('cohPP');

cd(lfp)
if ts1 == 0
    tss1= 'transition';
else
    tss1= 'stable';
end

%should be a 9xn matrix
plotpercents = [];
all_number = [];
%Run for all objects
a = 0;
for object = 1:3
    
    %Run for all locations
    for location = 1:3
        a=a+1;
        cd(lfp)
        %initial index is determined base on object and location
        if ml
            ind = mtsgetTrials(mt,'ML','BehResp',b1,tss1,'CueObj',objs(object),'CueLoc',locs(location));
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'CueLoc',locs(location));
        end
        
        %Get files based on match time, 4 times:
        %1800-1900,1901-2000,2001,2100,2101,2200ms
        m = mt.data.MatchOnset(ind,:);
        matchonset = round(m);
        
        if match == 0
            ind = ind;
        elseif match == 1
            matches = find(1800 <= matchonset & matchonset <= 1900);
            ind = ind(:,matches);
        elseif match == 2
            matches = find(1900 <= matchonset & matchonset <= 2000);
            ind = ind(:,matches);
        elseif match == 3
            matches = find(2000 <= matchonset & matchonset <= 2100);
            ind = ind(:,matches);
        elseif match == 4
            matches = find(2100 <= matchonset);
            ind = ind(:,matches);
        end
        
        %Determine whether the pair is pp,ff,pf
% %         if sum(c1 == frontal(:,1)) > 0
% %             t = 'Frontal Vs Frontal';
% %         elseif sum(c2 == parietal(:,2)) > 0
% %             t = 'Parietal Vs Parietal';
% %         else
% %             t = 'Parietal Vs Frontal';
% %         end
t=''; %different for betty and clark
        percents = [];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%RUN FOR ONE PAIR%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cd(lfp2)
        trial = ([sessionname(1:end-6) num2strpad(N.gridPos(c1),2) num2strpad(N.gridPos(c2),2)]);
        if (size(ind,2) == 0)
            load(trial, 'corrcoefs','phases')
            corrcoefs = abs(corrcoefs(:,1));
            phases = phases(:,1);
            threshh = '0';
        else
            load(trial, 'corrcoefs','phases')
            corrcoefs = abs(corrcoefs(:,ind));
            phases = phases(:,ind);
            
        end
        thresh = str2double(threshh);
        
        %Index threshold for the correct threshold
        if thresh > 0
            
            pair = 0;
            for x = 1 : c1
                if x == c1
                    q = c2;
                else
                    q = chnumb;
                end
                for y = (x+1) : q
                    pair = pair + 1;
                end
            end
            th = threshold{pair};
            thresholds = th(:,thresh);
        else
            thresholds = 0;
        end
        [rows columns] = size(corrcoefs);  %will be the same for the phases
        number=[];
        for yyy = 1:columns
            x=[];
            ntrials = [];
            for xxx = 1:rows %50ms bins w/ a max at 3000ms
                %creates a row vector that contains the number of trials
                %for each bin
                ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                %Determine if threshold is crossed
                if  corrcoefs(xxx,yyy) > thresholds
                    %If it is then keep the number
                    bb = corrcoefs(xxx,yyy);
                    pp = phases(xxx,yyy);
                    yy = 1;
                else
                    %Else, input NaN
                    bb = NaN;
                    pp = NaN;
                    yy = 0;
                end
                x = cat(1,x, yy);%%%Determines number of trials by not counting the else
            end
            number = cat(2,number,x);
        end
        s= size(number,2);
        stim_number(a) = s;
        all_number = [all_number;number'];
        
        %tttt = percent of ind trials
        %tttt = ((sum(number')/length(ind))*100);
        n =[];
        c = length(ntrials);
        for z = 1 : c
            n = cat(2,n,length(ind));
        end
        
        numbertrials = n - ntrials;
        number = number';
        
        tttt = [];
        for zz = 1 : length(numbertrials)
            
            if numbertrials(1,zz) == 0
                tt = 0;
            else
                tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
                %Determines acutal percentage of trials based on trials left
            end
            
            tttt = cat(2,tttt,tt);
        end
        
        percents = cat(1,percents,tttt);
        
        plotpercents = cat(1,plotpercents,percents);
    end
    
end
maxpercents = max(max(plotpercents(:,(1:end-10))));
if plot_on == 1
    figure('Position',[18 40 1250 650]);
    title(gca,'Stimuli')
    
    %used for tick labeling. Starts at 150ms
    q = (length(number)*50)+100;
    yyy = [1:10:60];
    xx = [150:500:q];
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Plot 1-9
    q = [];
    p = 0;
    for pp = 1:3
        for ppp = 1:3
            
            p = p + 1;
            subplot(3,3,p);
            m = plotpercents(p,:);
            %separate out the end of sample to first match
            
            t = m(18:34);
            time = [1:17];
            
            q(p,:) = t;
            
            stats = regstats(t,time);
            
            %get p-value for Beta1 (slope)
            %large p-value indicates equal means which would be a slope of
            %zero.
            pval = stats.tstat.pval(2,:);
            
            %finds the slope of the regression line
            s = diff(stats.yhat);
            
            slope = s(1);
            pair_rank(1,p) = slope;
            
            plot(m(1:45));
            hold on
            % % % %     %plot([nan(1,17) stats.yhat'],'color','r','LineWidth',2)
            med = median(m(18:34));
            
            
            %Sample On(500ms)
            xMin = 18; xMax = 34;
            yVal = med;
            %plot([xMin, xMax],[yVal yVal],'color','r')
            
            hold on
            %Draw Vertical Lines
            
            %Sample On(500ms)
            yMin = 0; yMax = maxpercents;
            xVal = 8;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on
            
            %Sample Off(1000ms)
            yMin = 0; yMax = maxpercents;
            xVal = 18;
            plot([xVal, xVal],[yMin yMax],'color','k')
            hold on
            
            %First match onset (1800ms)
            yMin = 0; yMax = maxpercents;
            xVal = 34;
            plot([xVal, xVal],[yMin yMax],'color','g')
            hold on
            
            %Last match onset (2200ms)
            yMin = 0; yMax = maxpercents;
            xVal = 42;
            plot([xVal, xVal],[yMin yMax],'color','r')
            
            
            set(gca,'XTick',yyy)
            set(gca,'XTickLabel',xx)
            ylabel(['Obj: ',num2str(pp),'    Loc: ',num2str(ppp)])
            xlabel('Time(ms)')
            ylim([0 ceil(maxpercents)]);
            xlim([0 60]);
        end
    end
    
    linkaxes([subplot(3,3,1),subplot(3,3,2),subplot(3,3,3),...
        subplot(3,3,4),subplot(3,3,5),subplot(3,3,6),subplot(3,3,7),...
        subplot(3,3,8),subplot(3,3,9)],'x');
    
    titles = ['Channel Pair ',num2str(c1),' ',num2str(c2),'.   Percent of Trials with Siginficant Correlation for all 9 Stimuli.','   Threshold = ',threshh,];
    
    title(subplot(3,3,2),titles,'fontsize',10,'fontweight','b');
    zoom on
    %linkzoom
    
end


%run kruskalwallis for objects and identities
%stimStats(plotpercents)

%%%     GLM
glm_output = glm_delay_full('all_number',all_number,'stim_number',stim_number,'plotpercents',plotpercents,'epoch',delay);

plotpercents = plotpercents ./ 100;

glm_plot(c1,c2,chnumb,glm_output,plotpercents)





