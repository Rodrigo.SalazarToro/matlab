function matchCPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location) 


%Plots the correlation coefs, percents, and phases for viewCPP

 %Load the threhold file
 load threshold threshold
 
global ind trial

    directory = cd; 
    cd(directory(1:end-4))

    mt=mtstrial('auto');
    
    objs =  unique(mt.data.CueObj);
    
    locs =  unique(mt.data.CueLoc);

    cd(directory)

        if ts1 == 0
            tss1= 'transition';
        else
            tss1= 'stable';
        end
        
        %Determine which stimulus to use objects then locations 0 - 9
        if object == 0 && location == 0 
            %Get files based on behavioral response and stimulus
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,0);
        elseif object == 0
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueLoc',locs(location));
        elseif location == 0
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object));
        else
            ind = mtsgetTrials(mt,'BehResp',b1,tss1,0,'CueObj',objs(object),'CueLoc',locs(location));
        end
 
    %Get files based on match time, 4 times:
    %1800-1900,1901-2000,2001,2100,2101,2200ms
     m = mt.data.MatchOnset(ind,:);
     matchonset = round(m);
        
    if match == 0
        ind = ind;
    elseif match == 1
        matches = find(1800 <= matchonset & matchonset <= 1900);
        ind = ind(:,matches);
    elseif match == 2
        matches = find(1900 <= matchonset & matchonset <= 2000);
        ind = ind(:,matches);
    elseif match == 3
        matches = find(2000 <= matchonset & matchonset <= 2100);
        ind = ind(:,matches);
    elseif match == 4
        matches = find(2100 <= matchonset);
        ind = ind(:,matches);
    end

    thresh = str2double(threshh);

        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
        
        load(trial, 'corrcoefs','phases')
        corrcoefs = corrcoefs(:,ind);
        phases = phases(:,ind);
     
     if thresh > 0 
        c1c2=0;
        for cc1 = 1:c1
            for cc2 = (cc1+1) : c2
                
               c1c2 = c1c2 +1; 
                
            end
        end
        
    threshind = (c1c2*4 - 4)+ thresh;
 
    thresholds =(threshold(1,threshind));
    
    else
        thresholds = 0;
    end
    
    [rows columns] = size(corrcoefs);  %will be the same for the phases
    
    cors = [];
    phase = [];
    number=[];
    for yyy = 1:columns
        corrss = [];
        phasess= [];
        x=[];
        ntrials = [];
       
        
             for xxx = 1:rows %50ms bins w/ a max at 3000ms
                  
                  %creates a row vector that contains the number of trials
                  %for each bin
                  ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                 
                 %Determine if threshold is crossed  
                 if  corrcoefs(xxx,yyy) > thresholds
      
                 %If it is then keep the number
                 bb = corrcoefs(xxx,yyy);  
                 
                 pp = phases(xxx,yyy);
                 
                 yy = 1;
                 else
          
                 %Else, input NaN
                 bb = NaN;
                 
                 pp = NaN;
                 
                 yy = 0;
                 end
      
             x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
             
             corrss = cat(1,corrss, bb);
             phasess = cat(1,phasess,pp);
             
             end
             
         number = cat(2,number,x); 
         cors = cat(2,cors,corrss);
         phase = cat(2,phase,phasess);
    end
  
    %%Calculate the mean for the corcoefs
    xy=(cors');
    
    [r c] = size(xy);
    meanss=[];
    for ccc = 1:c
       mmeans=[]; 
        for rrr = 1:r
            q =(xy(rrr,ccc));
            qq = isnan(q);
            
             if qq == 1
                 
             else
              
             mmeans = cat(1,mmeans,q);    
                 
             end
             
           xxxx = mean(mmeans);
        end
      
        meanss = [meanss xxxx];
        
    end
 
  %%Calculate the means for the phases
    xys=(phase');
    
    [rs cs] = size(xys);
    meansss=[];
    for cccs = 1:cs
       mmeanss=[]; 
        for rrrs = 1:rs
            qs =(xys(rrrs,cccs));
            qqs = isnan(qs);
            
             if qqs == 1
                 
             else
              
             mmeanss = cat(1,mmeanss,qs);    
                 
             end
             
           xxxxs = mean(mmeanss);
        end
       
        meansss = [meansss xxxxs];
        
    end
    
    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    
    n =[];
    c = length(ntrials);
    for z = 1 : c
        
        n = cat(2,n,length(ind));
        
    end
    
    numbertrials = n - ntrials;
    
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end
    
        %used for tick labeling. Starts at 150ms
        q = (length(meansss)*50)-900;
        xxx = [-850:50:q];
        yyy = [1:2:60];
       
        xx = [-850:100:q];
       
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 1
        cla
        subplot(3,1,1)
        boxplot((cors'),'notch','on');
        hold on
        plot(meanss,'r')
        hold on

                %Draw Vertical Lines
  
                %Match
                yMin = 0; yMax = 1;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
    
        hold off
    
        if str2double(threshh) == 0
        ylim([.2 .8]); 
        
        else
        ylim([.3 .8]);   
        end  
  
        
            set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
        
        %set(gca,'XTickLabel',xxx,'FontSize',7)
        xlabel('Time(ms)')
    ylabel('Correlation Coefficients')
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 2    
    subplot(3,1,2)
   
    q = max(sum(number));
    qq = max(tttt);
    plot(tttt,'r')
    hold on
    %plot(sum(number));
    %hold on
  
%vert = [34 0;34 10;42 0;42 10];
%face = [1 3 4 2];
%patch('Vertices',vert,'Faces',face)

                %Draw Vertical Lines
                %Match
                %yMin = 0; yMax = q;
                yMin = 0; yMax = qq;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on

    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    hold off
    %ylabel({'Number of Trials / Bin (Blue)';'';'Percent Total Trials (Red)'})
    ylabel('% of Trials W/ Siginficant Corr.')
    %ylim([0 300]);
    axis tight
        xlabel('Time(ms)')
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 3  
        subplot(3,1,3)

        boxplot((phase'),'notch','on');
        hold on
        %plot(xxx,meansss,'r')
        ylabel('Phase Shift of Max correlation (ms)')
        hold on

        %set(gca,'XTickLabel',xxx)
        
        
        
                %Plot threshold
                xMin = 0; xMax = 28;
                
                yVal = 0;
                plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
                hold on
        
                %Draw Vertical Lines

                %Match
                yMin = -25; yMax = 25;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                

        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        
        xlabel('Time(ms)')
        ylim([-25 25]);
        hold off

        linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');
                
zoom on
%linkzoom
    end
