function lowPass4XcorrML(chnumb)

%filters the data from 10 - 50hz and normalizes((subtract mean) / std)
%creates a cell "channel_matrix" that contains matrices for each channel
%with all of the trials (trial X samples). columns = shortest trial length
%Filter

LowPassLow = 10;
LowPassHigh = 50;
lfpdir = cd;
plength = length(lfpdir);
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp.****');
%make lfp2 directory
mkdir('lfp2')
mt = mtstrial('auto','ML','RTfromML');
type = mt.data.Index(:,1);
%determine if their are fixation trials and find their indices
%CueObj 55 is the fixation cue object
fix_trials = find(mt.data.CueObj == 55);

%cross platform file seperator
f = filesep;
s_ide = -1; s_loc = -1;

%find shortest trial length
for q = 1:size(lfpdata,1)
    cd(lfpdir)
    %load data
    trial = [cd f lfpdata(q).name];
    [datas]=nptReadStreamerFile(trial);
    %take out known artifact during reward
    if isempty(intersect(fix_trials, q))
        t_end = (mt.data.MatchOnset(q) + mt.data.FirstSac(q));% Match + FirstSac
    else
        t_end = (mt.data.MatchOnset(q));% Match
    end
    datas = datas(:,(1:floor(t_end)));
    sss = size(datas,2);
    
    if sss < 2000
        q
    end
    
    %find trial with the least number of samples (s = length)
    if s_ide < 0
        s_ide = sss;
    end
    
    if sss < s_ide
        s_ide = sss;
    end
end

channel_matrix = cell(1,chnumb);
for x = 1 : length(lfpdata)
    cd(lfpdir)
    %load data
    trial = [cd f lfpdata(x).name];
    [datas,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(trial);
    %Creates structure 'd' w/ streamer data
    d.rawdata = datas;
    d.samplingRate = sampling_rate;
    d.numChannels = num_channels;
    d.scanOrder = scan_order;
    [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);
    %normalize data by subtacting the mean and dividing by the standard deviation
    
    %take out known artifact during reward
    if isempty(intersect(fix_trials, x))
        t_end = (mt.data.MatchOnset(x) + mt.data.FirstSac(x));% Match + FirstSac
    else
        t_end = (mt.data.MatchOnset(x));% Match
    end
    data = data(:,(1:floor(t_end)));
    
    normdata = zeros(size(data,1),size(data,2));
    for channels = 1 : chnumb   
        %%%%%Normalize Data
        c1mean = mean(data(channels,:));
        c1std = std(data(channels,:));
        normdata(channels,:) = ((data(channels,:)-c1mean)/c1std);
    end
    %names the file the same as the previous file with the addition of
    %lfp2. Also, saves as a mat file
    trial2 = [trial((plength+2):end-5) '2' trial(end-4:end) '.mat'];
    if strcmp(computer,'PCWIN')
        cd('lfp2')
    end
    save(trial2,'normdata')
    %make a matrix with all trials for each channel. Used to make
    %surrgate for the average correlograms
    for xxx = 1 : chnumb
        channel_matrix{1,xxx} = cat(1,channel_matrix{1,xxx},normdata(xxx,(1:s_ide)));
    end
end

if ~strcmp(computer,'PCWIN')
   ! rsync *_lfp2.* lfp2
   ! rm *_lfp2.*
end
cd('lfp2')
save channel_matrix channel_matrix
fprintf(1,'Lfp2 directory made.\n')

































