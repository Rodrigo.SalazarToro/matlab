function cumulativePlots

%run at monkey level
cd('/media/MONTY/data/clark')
monkey = pwd;
%make mtsCPP object
% % load s s
% % cc = processDays(mtsCPP,'days',s,'NoSites');
load cc
threshold = 1;

global phase

[numb_pairs pairs]=get(cc,'Number','threshold',threshold,'correct','stable','pf','pval',.05,'slope',0,'identity')

phase_std = zeros(numb_pairs,60);
phase_mean = zeros(numb_pairs,60);

for x = 1 : numb_pairs
    
    ch1 = cc.data.Index(x,23);
    ch2 = cc.data.Index(x,24);
    cd(cc.data.setNames{x}(1:(end-18)))
    cd ../..
    [NeuroInfo] = NeuronalChAssign;
    chnumb=length(NeuroInfo.channels);
    cd(cc.data.setNames{x}(1:(end-18)))
    
    %CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb,noplot) 
    CPP(1,1,ch1,ch2,num2str(threshold),['channelpair' num2str(ch1) num2str(ch2) '.mat'],0,0,0,chnumb,1)
    delete(gcf)
    ps=(nanstd(phase'));
    ps(isnan(ps)) = [];
    
    ps = ps/max(ps(26:34));
    
    s = size(ps,2)
    phase_std(x,(1:s)) = ps;

end
cd(monkey)
% % % % subplot(2,1,1)
boxplot(phase_std(:,(26:34)))
% % % subplot(2,1,2)
% % % % boxplot(phase_std)
time = [1:9];
slr_phase=median(phase_std(:,(26:34)));

            stats = regstats(slr_phase,time);
            
            
            pval = stats.fstat.pval
            
            %finds the slope of the regression line
            s = diff(stats.yhat);
            
            slope = s(1);
            
            hold on
            plot([stats.yhat'],'color','k','LineWidth',1)




save cumulative_phase_info phase_std 