function correlogramThreshold


%computes correlogram thresholds for each channel pair across all trials.
%run in the lfp2 file   


avgCorrelogram_surrogate = {};


    load allTrials allTrials
    load allTrials2 allTrials2
    
    data1 = allTrials;
    data2 = allTrials2;


    %get index of trials
    trials = size(data1,1);


tic

all_correlations = zeros(((trials*(trials-1)/2)),1);
        
        pair = 0;
        for t1 = 1 : trials
            t1
                
            d1 = data1(t1,:);

                  for t2 = (t1 + 1) : trials
                              
                              pair = pair + 1;
          
                              d2 = data2(t2,:);

                             c = xcorr(d1,d2,0,'coef');

              
                              all_correlations(pair,:) = c;
                              
                  end
                  
                  
                  
        end


    toc

cd ('lfp2')

        
   
            

    save all_correlations all_correlations
    

  
     