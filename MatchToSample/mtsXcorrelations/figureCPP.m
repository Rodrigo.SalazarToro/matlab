function figureCPP

%make .figs for all pairs at specified thresholds
%run and save figs in lfp2 folder

%CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb)

d = cd;
cd ..
cd ..

[NeuroInfo] = NeuronalChAssign;
chnumb=length(NeuroInfo.channels);

cd(d);

%for all pairs
for c1 = 1 : chnumb
    
    for c2 = (c1+1) : chnumb
        
        %for thresholds 1:3
        for t = 1 : 3
        
        %figure
        figure('Position',[18 40 1250 650]);
        
        CPP(1,1,c1,c2,num2str(t),'channelpair12.mat',0,0,0,chnumb)
        
        tt = ['Channel Pair ',num2str(c1),' ',num2str(c2),'   Threshold = ',num2str(t)];

        title(subplot(3,1,1),tt,'fontsize',10,'fontweight','b');

% % %         saveas(gcf, ['T',num2str(t),'_',num2str(c1),num2str(c2),'.fig'])
% % %         
% % %         delete(gcf)

  

        end



    end 
end