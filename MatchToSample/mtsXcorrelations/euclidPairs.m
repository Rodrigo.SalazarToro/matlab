function euclidPairs

%calculates the euclidean distance of pairs
%also saves histology information of pairs


cd('G:\data\betty')

ml = 1; %indicates monkey logic

daydir = cd;
%Get all the switch days
if ~ml
switchdays = switchDay;
%load s s
%switchdays = s;
else
    switchdays = nptdir('09****');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%Run for all switch days

for alldays = 1 : length(switchdays) 
  
cd (daydir); 

DaysLeft = length(switchdays) - alldays
if ~ml

cd (cell2mat(switchdays(alldays)));
else
    cd(switchdays(alldays).name);
end
days = pwd;

%Run on sessions 2 and 3
ses = nptDir('session*');
for sessions = 2:3

%try

cd (days);
%get sessions   ses.name
cd (ses(sessions).name);
[NeuroInfo] = NeuronalChAssign;
%Get number of channels
chnumb=length(NeuroInfo.channels);
%create trial object    
if ~ml  
    mt=mtstrial('auto');
else
     mt = mtstrial('auto','ML','RTfromML');
end
%Get number of trials
NumberOfTrials = mt.data.numSets;

load hist_pp hist_pp
load hist_pf hist_pf



%% calculate the x,y coordinates of the group(corresponds to grid)
numb_groups = size(NeuroInfo.groups,2);
coordinates = [];
for n = 1 : numb_groups
    pos = 0;
    for y = 1 : 8
        for x =  1 : 8
            pos = pos + 1;
           if pos == (NeuroInfo.groups(n))
               
               coordinates = cat(1,coordinates,[x y]);
           end
        end

    end
    
end


%%

pair = 0;
pair_distances = [];
depths = [];
hist = [];
for c1 = 1 : chnumb
    
    for c2 = (c1+1) : chnumb
        
        pair = pair + 1;
        
        c1x = coordinates(c1,1);
        c1y = coordinates(c1,2);

        c2x = coordinates(c2,1);
        c2y = coordinates(c2,2);

        %get depths
        d1 = (NeuroInfo.recordedDepth(c1)*10^(-3));
        d2 = (NeuroInfo.recordedDepth(c2)*10^(-3));

        %grid holes are 860 microns apart
        euclid = sqrt( ((c1x*.86)-(c2x*.86))^2 + ((c1y*.86) - (c2y*.86))^2 + (d1 - d2)^2 );

        dep = sqrt((d1-d2)^2);

        pair_distances(pair,1) = euclid;
        depths(pair,1) = dep;
        
        %make an index to indicate if the pairs are are on same side of the
        %sulcus or on opposite, and if one of them stradles the sulcus
        
        %opposite side of sulcus == 0
        %same side of sulcus == 1
        %one channel stradles sulcus == 2
        %both channels stradle sulcus == 3
        %Inter-arial == -1
        if (((NeuroInfo.cortex(c1) == 'P' && NeuroInfo.cortex(c2) == 'P') || (NeuroInfo.cortex(c1) == 'F' && NeuroInfo.cortex(c2) == 'F')))
            
            if ((NeuroInfo.cortex(c1) == 'F' && NeuroInfo.cortex(c2) == 'F'))
                hc1 = hist_pf(NeuroInfo.gridPos(1,c1),1);
                hc2 = hist_pf(NeuroInfo.gridPos(1,c2),1);
            end
            
            if ((NeuroInfo.cortex(c1) == 'P' && NeuroInfo.cortex(c2) == 'P'))
                hc1 = hist_pp(NeuroInfo.gridPos(1,c1),1);
                hc2 = hist_pp(NeuroInfo.gridPos(1,c2),1);
            end
            
            if hc1 == 0 && hc2 == 0 %same side
                h = 1;
                hh = 0; %used to indicate which side of sulcus
            elseif hc1 == 1 && hc2 == 1 %same side
                h = 1;
                hh = 1; %used to indicate which side of sulcus
            elseif hc1 == 2 || hc2 == 2 %both in sulcus
                h = 2;
                hh = -1;
            elseif hc1 == 2 && hc2 == 2
                h = 3;
                hh = -1;
            else
                h = 0;
                hh = -1;
            end
        
        else 
        %Inter-arial
        %medial VS dorsal == -1
        %lateral VS dorsal == -2
        %medial VS ventral == -3
        %lateral VS ventral == -4
        %medial VS sulcus == -5
        %lateral VS sulcus == -6
        %sulcus VS dorsal== -7
        %sulcus VS ventral== -8
        %sulcus VS sulcus == -9
            hc1 = hist_pp(NeuroInfo.gridPos(1,c1),1); %PARIETAL
            hc2 = hist_pf(NeuroInfo.gridPos(1,c2),1); %PRE-FRONTAL
            
            if hc1 == 0 && hc2 == 0 %md
                h = -1;
            elseif hc1 == 1 && hc2 == 0 %ld
                h = -2;
            elseif hc1 == 0 && hc2 == 1 %mv
                h = -3;
            elseif hc1 == 1 && hc2 == 1 %lv
                h = -4;
            elseif hc1 == 0 && hc2 == 2 %ms
                h = -5;
            elseif hc1 == 1 && hc2 == 2 %ls
                h = -6;
            elseif hc1 == 2 && hc2 == 0 %sd
                h = -7;
            elseif hc1 == 2 && hc2 == 1 %sv
                h = -8;
            elseif hc1 == 2 && hc2 == 2 %ss
                h = -9;           
            end
            hh = -10;
        end
        
        hist(pair,1) = h;
        hist(pair,2) = hh;
            
    end 
end

save pair_distances pair_distances 
save depths depths
save hist hist
end




end
