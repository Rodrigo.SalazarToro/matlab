function avgPP

%runs for all days, sessions 2-3







                thresh = 2 %thresholds
                br = 1   %behavioral response (correct, incorrect)




%global chnumb

%START IN THE DAY DIRECTORY
cd('G:\data\clark');%\clark');

daydir = cd;
%Get all the switch days
%switchdays = switchDay;
load ss ss
switchdays = ss;

f = filesep; %%File seperator (MAC VS PC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%Run for all switch days

all_percents = [];
all_phases = [];


for alldays = 1 : length(switchdays) %skip 14
    
cd (daydir); 
%indicate number of days remaining
DaysLeft = length(switchdays) - alldays
cd (cell2mat(switchdays(alldays)));
days = cd;

%Run on sessions 2 and 3
ses = nptDir('session*');
for sessions = 2:3

%try

cd (days);
%get sessions   ses.name
cd (ses(sessions).name);
[NeuroInfo] = NeuronalChAssign;
%Get number of channels
chnumb=length(NeuroInfo.channels);
%calculate number of pairs (n choose 2)
pairs = (chnumb*(chnumb-1)) / 2;
%create trial object    
mt=mtstrial('auto');
%Get number of trials
NumberOfTrials = mt.data.numSets;



cd ('lfp');
cd ('lfp2');




%%







%function CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb) 


%Select channelpair12 from lfp folder
%Plots the correlation coefs, percents, and phases for viewCPP

 %Load the threshold file
 load threshold threshold


    lfp2 = cd; 
    cd ..
    lfp = cd;
    cd ..
    
    cd(lfp)
    
    

    

pair = 0;
for c1 = 1 : chnumb
    

    for c2 = (c1+1) : chnumb
        
        corcoefss = [];   
        percents = [];
        phasesss = [];
        column = 0;

        pair = pair + 1;

            if br == 1
                res = 'correct';
            else
                res = 'incorrect';
            end

             stability = 1;  %transition or stable (stable, with transition option)

                cd(lfp)


                    tss1= 'stable';
            

                
                ind = mtsgetTrials(mt,'BehResp',br,tss1);   



                cd(lfp2)
                
                sessionname = 'channelpair12.mat';
                
                trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);

                load(trial, 'corrcoefs','phases')
                corrcoefs = abs(corrcoefs(:,ind));
                phases = phases(:,ind);  %necessary for trials with negative peaks




                th = threshold{pair};

                thresholds = th(:,thresh);
                
                t=num2str(thresholds);



                [rows columns] = size(corrcoefs);  %will be the same for the phases

                cors = [];
                phase = [];
                number=[];

                for yyy = 1:columns
                    corrss = [];
                    phasess= [];
                    x=[];
                    ntrials = [];


                         for xxx = 1:rows %50ms bins w/ a max at 3000ms

                              %creates a row vector that contains the number of trials
                              %for each bin
                              ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));

                             %Determine if threshold is crossed  
                             if  corrcoefs(xxx,yyy) > thresholds

                             %If it is then keep the number
                             bb = corrcoefs(xxx,yyy);  

                             pp = phases(xxx,yyy);

                             yy = 1;
                             else

                             %Else, input NaN
                             bb = NaN;

                             pp = NaN;

                             yy = 0;
                             end

                         x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.

                         corrss = cat(1,corrss, bb);
                         phasess = cat(1,phasess,pp);

                         end

                     number = cat(2,number,x); 
                     cors = cat(2,cors,corrss);
                     phase = cat(2,phase,phasess);
                end
                
%%                

    %%Calculate the mean for the corcoefs
    xy=(cors');
    [r c] = size(xy);
    meanss=[];
    for ccc = 1:c
       mmeans=[]; 
        for rrr = 1:r
            q =(xy(rrr,ccc));
            qq = isnan(q);
             if qq == 1  
             else 
             mmeans = cat(1,mmeans,q);             
             end
           xxxx = mean(mmeans);
        end
        meanss = [meanss xxxx];
    end
    
    %%Calculate the std for the phases
    xys=(phase');
    [rs cs] = size(xys);
    meansss=[];
    for cccs = 1:cs
       mmeanss=[]; 
        for rrrs = 1:rs
            qs =(xys(rrrs,cccs));
            qqs = isnan(qs);
             if qqs == 1 
             else              
             mmeanss = cat(1,mmeanss,qs);       
             end 
           xxxxs = std(mmeanss); %STD
        end
        meansss = [meansss xxxxs];
    end  
    
    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    n =[];
    c = length(ntrials);
    for z = 1 : c
        n = cat(2,n,length(ind));
    end
    
   numbertrials = n - ntrials;
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end
      corcoefss = cat(2,corcoefss,meanss);
      percents = cat(2,percents,tttt);
      phasesss = cat(2,phasesss,meansss);  %actually the standard deviations
               
 
%%
               

               
               
     
               


            
           

       
        all_percents = cat(1,all_percents,percents);
        all_phases = cat(1,all_phases,phasesss);
        
    end 
 



     
end 

end
end  
 
cd(daydir)

save avg_info_correct_t2 all_percents all_phases
   
   
   
   
    