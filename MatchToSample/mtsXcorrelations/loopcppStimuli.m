function loopcppStimuli

%runs for all days, sessions 2-3

%global chnumb

%START IN THE DAY DIRECTORY
cd('G:\data\clark');%\clark');

daydir = cd;
%Get all the switch days
switchdays = switchDay;

f = filesep; %%File seperator (MAC VS PC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%Run for all switch days

for alldays = 1 : length(switchdays) %skip 14
    
cd (daydir); 
%indicate number of days remaining
DaysLeft = length(switchdays) - alldays
cd (cell2mat(switchdays(alldays)));
days = cd;

%Run on sessions 2 and 3
ses = nptDir('session*');
for sessions = 2:3

%try

cd (days);
%get sessions   ses.name
cd (ses(sessions).name);
[NeuroInfo] = NeuronalChAssign;
%Get number of channels
chnumb=length(NeuroInfo.channels);
%calculate number of pairs (n choose 2)
pairs = (chnumb*(chnumb-1)) / 2;
%create trial object    
mt=mtstrial('auto');
%Get number of trials
NumberOfTrials = mt.data.numSets;



cd ('lfp');
cd ('lfp2');




%%



 

for c1 = 1 : chnumb

    for c2 = (c1+1) : chnumb

        for br = 1  %behavioral response (correct, incorrect)
            if br == 1
                res = 'correct';
            else
                res = 'incorrect';
            end
               

                sessionname = 'channelpair12.mat';


                for thresh = 1:2  %thresholds

                
                t=num2str(thresh);
                
%%                
            
           
        %figure
        %figure('Position',[18 40 1250 650]);
        cppStimuli(br,1,c1,c2,t,sessionname,0,chnumb)  
       
        ttt = ['Channel Pair ',num2str(c1),' ',num2str(c2),' ',res,'/stable','   Threshold = ',t];
 
        title(subplot(3,3,2),ttt,'fontsize',10,'fontweight','b');

        saveas(gcf, ['T',t,'_',res,'-stable','_',num2str(c1),num2str(c2),'.fig'])
        g = gcf;
        
        delete(g)
                
                
   
%%

       


 
    


                end 

       
        end

    end
end 
 

   
   
end 

end
   

        %figure
        figure('Position',[18 40 1250 650]);
        
        
        %cppStimuli(b1,ts1,c1,c2,threshh,sessionname,match,chnumb)
        
        
        cppStimuli(1,1,c1,c2,threshh,'channelpair12.mat',0)
        
        tt = ['Channel Pair ',num2str(c1),' ',num2str(c2),'   Threshold = ',num2str(t)];
        
        
        d = ('C:\Users\Nick\Desktop\Desktop\Desktop\Correlation Analysis\stims');
        
        cd (d);
    
     

        title(subplot(3,1,1),tt,'fontsize',10,'fontweight','b');

        saveas(gcf, ['T',num2str(t),'_',num2str(c1),num2str(c2),'.fig'])
        
        delete(gcf)

  














