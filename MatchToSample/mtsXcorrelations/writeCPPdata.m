function writeCPPdata

%Writes thresholds and channelpairs for all switch days
%Files are saved in the ...day/session/lfp

%uses the average correlograms to determine whether to use positive or
%negative peaks closest to zero

%START IN THE DAY DIRECTORY
cd('G:\data\betty');

global ml monkey
ml = 1; %indicates monkey logic
monkey = 'betty'; %indicates which monkey


%Xcorr
windowSize = 200; 
windowStep = 50; 
phaseSize = 50; 

daydir = cd;
%Get all the switch days
if ~ml
switchdays = switchDay;
% load s s
% switchdays = s;
else
                    switchdays = nptdir('09****');
end           

f = filesep; %%File seperator (MAC VS PC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
%Run for all switch days

for alldays = [1]% : length(switchdays) 
    
tic    
cd (daydir); 

%indicate number of days remaining

DaysLeft = length(switchdays) - alldays
if ~ml
    
cd (cell2mat(switchdays(alldays)))
else
                    cd(switchdays(alldays).name);
                    switchdays(alldays).name
end
days = pwd;

%Run on sessions 
ses = nptDir('session*');
for sessions = 1

%try

cd (days);
%get sessions   ses.name
cd (ses(sessions).name);
[NeuroInfo] = NeuronalChAssign;
%Get number of channels
chnumb=length(NeuroInfo.channels);
%calculate number of pairs (n choose 2)
pairs = (chnumb*(chnumb-1)) / 2;
if ~ml
    %create trial object    
    mt=mtstrial('auto');
else
     mt = mtstrial('auto','ML','RTfromML');
end
%Get number of trials
NumberOfTrials = mt.data.numSets;



cd ('lfp');
    if ~ml
    load rejectedTrials rejectTrials
    end
%%%%%%%%%%%%%%%%%%%%%%%%%Low Pass and Normalize%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%lowpass and normalized trial data in new folder (lfp2)
if ~ml
    lowPass4Xcorr(chnumb) %leaves in lfp2 directory
else
    lowPass4XcorrML(chnumb) %leaves in lfp2 directory

end
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Make Average Correlograms%%%%%%%%%%%%%%%%%

%Calls averageCorrelograms function (makes average correlograms and surrogates

averageCorrelogram(chnumb) %leaves in lfp2 directory
cd ..


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Make Epoch Correlograms%%%%%%%%%%%%%%%%%%%%%

%makes 4 correlograms for each epoch (400ms each)

epochCorrelograms(chnumb) %leaves in lfp directory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Get Avg Correlogram Data%%%%%%%%%%%%%%%%%%%

%checks to see if the average correlogram crosses 
cd('lfp2')
avg_cross(chnumb) %leaves in lfp2 directory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~ml

    save rejectedTrials rejectTrials %saves index of rejected trials in lfp2 folder

end



if ~ml
    lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
    num_lfp2 = [1];
else
    lfp2_dirs = ['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
    num_lfp2 = [1 2];
    mkdir('identity') %saved in lfp2 folder
    mkdir('location') %saved in lfp2 folder
    cd ..
end


load avg_correlograms avg_corrcoef 


for ide_loc = num_lfp2
    
 type = mt.data.Index(:,1);

trials_identity = (find(type == 1));
trials_location = (find(type == 2));

trials_all = [{trials_identity'} {trials_location'}];


    


%%%%%%%%%%%%%%%%%%%%%%%%Thresholds%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%write thresholds for channelpair files


%% NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
%% RESPICTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX

lfpdata = nptDir([monkey '*']);
cd ..
mt = mtstrial('auto','ML','RTfromML');
cd('lfp2')

load ([cd f lfpdata(1).name],'normdata');
numchannels = size(normdata,1);
ddata = cell(numchannels,1);
for x = trials_all{ide_loc}

    load ([cd f lfpdata(x).name],'normdata');
    for y = 1: numchannels
    %Concatenates the data from all lfp files to run surrogate analysis
    if y == 1
        ddata{y} = normdata(y,:);
    end
 
    ddata{y} = [ddata{y} normdata(y,:)];

    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Calculate Moving Surrogate%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

rr_surrogate=cell(1,pairs);

pair = 0;
for ii=1:chnumb
    fprintf('\n%0.5g     ',ii)
    for jj=ii+1:chnumb
        fprintf(' %0.5g',jj)

        pair = pair + 1;
        %find xcorr at random shifts
        r_surrogate = RandXxcorr(ddata{ii},ddata{jj},windowSize,round(10000));

        rr_surrogate{pair} = r_surrogate;

    end
end

threshold = cell(1,pairs);
for p = 1:pairs

    %Loads 10000 samples for each pairwise combination
    pp = rr_surrogate{p};

    threshold{p} = prctile(pp,[95 99 99.9 99.99]);

end
    if ml
        cd(lfp2_dirs(ide_loc,:))
        save threshold threshold
        cd ..
    else
        save threshold threshold
    end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function writeCPP
%WRITES  channelpair
%%%%%%%%%%%%%%%%%%%%%channelpairs%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pair_number = 0;
total_pairs = size(avg_corrcoef,2);
for c1=1:chnumb

for c2=(c1+1):chnumb

    pair_number = pair_number + 1;
    fprintf(1,[num2str(pair_number) '  of  ' num2str(total_pairs)])
    fprintf(1,'\n')

    corrcoefs=[]; phases=[]; correlograms={};
    for x = trials_all{ide_loc}

                load ([cd f lfpdata(x).name],'normdata');

                %calculate number of bins
                steps = floor((length(normdata)-windowSize)/windowStep);
                start =  1:windowStep:steps*windowStep+1;
                endd = start+windowSize-1;

        r = GrayXxcorr(normdata(c1,:),normdata(c2,:),windowSize,windowStep,phaseSize,start,endd);

        pos_or_neg = avg_corrcoef{pair_number};

        %if the average correlogram has a negative peak that is closest to zero
        %then invert the correlograms to find the negative peaks that are
        %closest to zero
        if pos_or_neg < 0
            r = r * -1;
            inverted = 1;
        else
            inverted = 0;
        end

        lags = [(-1*phaseSize): phaseSize];

        %find closest peak to zero lag
        p=zeros(size(r,1),2);
        for kk=1:size(r,1)
            rr=r(kk,:);  

            %Find index of central positive peak
            peak = findpeaks(rr);

            for ppp = 1 : size(peak.loc,1)

                if (rr(peak.loc(ppp)) < 0)
                     %set to max phase index
                    peak.loc(ppp) = length(lags);
                end

            end

          [phase, index] = (min(abs(lags(peak.loc))));



            %get real phase value by indexing the location in the lags
            %get corr coef by indexing the correlogram
            peak_phase = lags(peak.loc(index));
            peak_corrcoef = rr(peak.loc(index));

            
            
            if size(peak.loc,1) > 0 
            %Stores the correlation coef and lag position.
                p(kk,2) = peak_phase;
                p(kk,1) = peak_corrcoef;
            
            else
                p(kk,1) = nan;
                p(kk,1) = nan;  
            end
        end









% 
%         find closest peak to zero lag
%         p=zeros(size(r,1),2);
%         for kk=1:size(r,1)
%            rr=r(kk,:);
%            t=fpeak(1:length(rr),rr,1,[1,length(rr),0,inf]);
%            phase=t(:,1)-phaseSize-1;
%             [y,ind]=min(abs(phase));
%             if isempty(t) %all negative correlations
%                p(kk,1)=0;
%                p(kk,2)=0;
%             elseif y==phaseSize %max correlation is at max phase
%                p(kk,1)=0;
%                p(kk,2)=0;
%             else
%                p(kk,1) = t(ind,2);
%                p(kk,2) = phase(ind);
%             end
%         end

        
        
        
        
        
        
        
        
        
        
        

            %if the correlograms are inverted then make the corrcoefs
            %negative to correspond to their actual values
            if inverted == 1
                p(:,1) = p(:,1) * -1;

                r = r * -1; %reverse correlogram back   
            end


        %%coefs = the correlation coefs for the trial pair
        coefs=(p(:,1));

        %%phases = the phases
        phase=(p(:,2));

        %Find the length of the coefs column.
        cc = length(coefs);
        dd = length(phase);

        %Determines how many buffer NaN's are necessary
        ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
        ddd = (3000/windowStep) - dd;

        %make sure there are only 60 bins
        if ccc >= 0 

        %Creates NaN buffer
        cccc = NaN(1,ccc)';
        dddd = NaN(1,ddd)';

        %%Concatenates NaN buffer on to the end of the column in order
        %%for the matrix dimensions to match
        coefs = cat(1,coefs,cccc);
        phase = cat(1,phase,dddd);

        else

        coefs = coefs((1:end+ccc),1);     
        phase = phase((1:end+ddd),1);

        end

        %%makes a matrix with all trials correlation coef for the
        %%specific pair
        corrcoefs = cat(2,corrcoefs, coefs);
        phases = cat(2,phases, phase);

        %places r in the cell location corresponding to the trial number
        correlograms{x} = r; 
    end

%%%%%%%%%%%%%Make Cells for Trial Information%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%indexes will be a 4X1 cell
%   cell 1: correct,stable
%   cell 3: correct,transition
%   cell 4: incorrect,stable
%   cell 5: incorrect,transition

indexes = cell(1,4);    

lfp2 = cd; 
cd ..
lfp_dir = cd;
cd ..

if ~ml
    mt=mtstrial('auto');
else
    mt = mtstrial('auto','ML','RTfromML');
end

cd(lfp_dir)










%%
%correct, stable
allTrials = trials_all{ide_loc};
index = [];
if ml
    ind = mtsgetTrials(mt,'BehResp',1,'stable','ML');
else
    ind = mtsgetTrials(mt,'BehResp',1,'stable');
end  
for i = 1 : size(trials_all{ide_loc},2)
    if ~isempty(intersect(allTrials(i),ind))
            index = [index i]; 
    end 
end
indexes{1} = index;

%correct, transition
index = [];
if ml
    ind = mtsgetTrials(mt,'BehResp',1,'transition','ML'); 
else
    ind = mtsgetTrials(mt,'BehResp',1,'transition');
end
for i = 1 : size(trials_all{ide_loc},2)
    if ~isempty(intersect(allTrials(i),ind))
            index = [index i]; 
    end 
end
indexes{2} = index;

%incorrect, stable
index = [];
if ml
    ind = mtsgetTrials(mt,'BehResp',0,'stable','ML');
else
    ind = mtsgetTrials(mt,'BehResp',0,'stable');
end
for i = 1 : size(trials_all{ide_loc},2)  
    if ~isempty(intersect(allTrials(i),ind))
            index = [index i]; 
    end
end
indexes{3} = index;

%incorrect, transtion
index = [];
if ml
    ind = mtsgetTrials(mt,'BehResp',0,'transition','ML');
else
    ind = mtsgetTrials(mt,'BehResp',0,'transition');
end
for i = 1 : size(trials_all{ide_loc},2)
    if ~isempty(intersect(allTrials(i),ind))
            index = [index i]; 
    end
end
indexes{4} = index;

cd(lfp2)


%%%%%%%%%%%%%Write Indexes For Each Threshold%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plot_data = cell(5,5);

%rows are the criteria
%   all
%   correct/stable
%   correct/transition
%   incorect/stable
%   incorrect/transition

%columns are the thresholds (0 - 4)


if ml
    cd(lfp2_dirs(ide_loc,:))
    load threshold threshold
    cd(lfp2)
else
    load threshold threshold
end

for thresh = 0:4


    for allindexes = 0 : 4
        if allindexes == 0
            cc = abs(corrcoefs);
            ppp = phases;
        else    
            cc = abs(corrcoefs(:,indexes{allindexes}));  %necessary for trials with negative peaks
            ppp = phases(:,indexes{allindexes}); 
        end

        %Index threshold for the correct threshold   
        if thresh > 0 

            th = threshold{pair_number};

            thresholds = th(:,thresh);

        else
            thresholds = 0;
        end




        [rows columns] = size(cc);  %will be the same for the phases

        cors = [];
        phase = [];
        number=[];

        for yyy = 1:columns
            corrss = [];
            phasess= [];
            x=[];
            ntrials = [];


                 for xxx = 1:rows %50ms bins w/ a max at 3000ms

                      %creates a row vector that contains the number of trials
                      %for each bin
                      ntrials = cat(2,ntrials,(sum(isnan(cc(xxx,:)))));

                     %Determine if threshold is crossed  
                     if  cc(xxx,yyy) > thresholds

                     %If it is then keep the number
                     bb = cc(xxx,yyy);  

                     pp = ppp(xxx,yyy);

                     yy = 1;
                     else

                     %Else, input NaN
                     bb = NaN;

                     pp = NaN;

                     yy = 0;
                     end

                 x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.

                 corrss = cat(1,corrss, bb);
                 phasess = cat(1,phasess,pp);

                 end

             number = cat(2,number,x); 
             cors = cat(2,cors,corrss);
             phase = cat(2,phase,phasess);
        end
        
        d.number = number;
        d.cors = cors;
        d.phase = phase;
        d.ntrials = ntrials;
        
        
        
        plot_data{(allindexes+1),(thresh+1)} = d;

        
    end

end
%%
%cd(lfp2)

pair_thresholds = threshold{pair_number}; 
if ml
    cd(lfp2_dirs(ide_loc,:))
end

channel_pairs = [ 'channelpair' num2str(c1) num2str(c2) ];
save(channel_pairs,'corrcoefs','phases','correlograms','indexes','plot_data','pair_thresholds')
if ml
    cd ..
end
end
end 



cd ..


end


% catch
%     error_message = [ 'Error in day ' num2str(alldays) ', session ' num2str(sessions)];
%     fprintf(1,error_message);
%     fprintf(1,'\n');
% 
% end


end

end


delaySlopes

%euclidPairs
toc
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












