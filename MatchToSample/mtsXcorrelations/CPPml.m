function CPPml(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb) 


%Select channelpair12 from lfp folder
%Plots the correlation coefs, percents, and phases for viewCPP

 %Load the threshold file
 load threshold threshold
 
global ind trial ml trialtype


    trialDir = cd;
    cd ..
    lfp2 = cd; 
    cd ..
    lfp = cd;
    cd ..
    

    mt = mtstrial('auto','ML','RTfromML');
    
     type = mt.data.Index(:,1);
    if strmatch(trialtype,'identity')
     trials_type = (find(type == 1))';
    elseif strmatch(trialtype,'location')
     trials_type = (find(type == 2))';
    end


    objs =  unique(mt.data.CueObj); 
    locs =  unique(mt.data.CueLoc);
    
    cd(lfp)

        if ts1 == 0
            tss1= 'transition';
        else
            tss1= 'stable';
        end
        
        %Determine which stimulus to use objects then locations 0 - 9
        if object == 0 && location == 0 
            %Get files based on behavioral response and stimulus
                ind = mtsgetTrials(mt,'BehResp',b1,tss1,'ML');
        elseif object == 0
                ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueLoc',locs(location),'ML');
        elseif location == 0
                ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'ML');
        else
                ind = mtsgetTrials(mt,'BehResp',b1,tss1,'CueObj',objs(object),'CueLoc',locs(location),'ML');
        end
        
        index = [];
        for i = 1 : size(trials_type,2)
            if ~isempty(intersect(trials_type(i),ind))
                index = [index i]; 
            end 
        end
        ind = index;

        cd(lfp2)
        cd(trialDir)
        trial = ([sessionname(1:end-6) num2str(c1) num2str(c2)]);
        
        load(trial, 'corrcoefs','phases')
        corrcoefs = abs(corrcoefs(:,ind));
        phases = phases(:,ind);  %necessary for trials with negative peaks
    
        
     thresh = str2double(threshh);   
    
     %Index threshold for the correct threshold   
     if thresh > 0 
    pair = 0;
    for x = 1 : c1
        if x == c1 
            q = c2; 
        else
            q= chnumb;
        end
        
        for y = (x+1) : q
            
            pair = pair + 1;
            
        end
    end
        
        
    th = threshold{pair};
 
    thresholds = th(:,thresh);
    
    else
        thresholds = 0;
    end
    [rows columns] = size(corrcoefs);  %will be the same for the phases

    cors = [];
    phase = [];
    number=[];

    for yyy = 1:columns
        corrss = [];
        phasess= [];
        x=[];
        ntrials = [];
       
        
             for xxx = 1:rows %50ms bins w/ a max at 3000ms
                  
                  %creates a row vector that contains the number of trials
                  %for each bin
                  ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                 
                 %Determine if threshold is crossed  
                 if  corrcoefs(xxx,yyy) > thresholds
      
                 %If it is then keep the number
                 bb = corrcoefs(xxx,yyy);  
                 
                 pp = phases(xxx,yyy);
                 
                 yy = 1;
                 else
          
                 %Else, input NaN
                 bb = NaN;
                 
                 pp = NaN;
                 
                 yy = 0;
                 end
      
             x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
             
             corrss = cat(1,corrss, bb);
             phasess = cat(1,phasess,pp);
             
             end
             
         number = cat(2,number,x); 
         cors = cat(2,cors,corrss);
         phase = cat(2,phase,phasess);
    end

    %%Calculate the mean for the corcoefs
    xy=(cors');
    
    [r c] = size(xy);
    meanss=[];
    for ccc = 1:c
       mmeans=[]; 
        for rrr = 1:r
            q =(xy(rrr,ccc));
            qq = isnan(q);
            
             if qq == 1
                 
             else
              
             mmeans = cat(1,mmeans,q);    
                 
             end
             
           xxxx = mean(mmeans);
        end
  
        meanss = [meanss xxxx];

    end
 
  %%Calculate the means for the phases
    xys=(phase');
    
    [rs cs] = size(xys);
    meansss=[];
    for cccs = 1:cs
       mmeanss=[]; 
        for rrrs = 1:rs
            qs =(xys(rrrs,cccs));
            qqs = isnan(qs);
            
             if qqs == 1
                 
             else
              
             mmeanss = cat(1,mmeanss,qs);    
                 
             end
             
           xxxxs = mean(mmeanss);
        end
        
        
        meansss = [meansss xxxxs];

    end
    
    %tttt = percent of ind trials
    %tttt = ((sum(number')/length(ind))*100);
    
    n =[];
    c = length(ntrials);
    for z = 1 : c
        
        n = cat(2,n,length(ind));
        
    end
    
   numbertrials = n - ntrials;
    
   number = number';
    
   tttt = [];
   for zz = 1 : length(numbertrials)
       
        if numbertrials(1,zz) == 0
            tt = 0;
        else
            tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
            %Determines acutal percentage of trials based on trials left
        end
        
        tttt = cat(2,tttt,tt);
   end
    
    
        %used for tick labeling. Starts at 150ms
        q = (length(meansss)*50)+100;
        xxx = [150:50:q];
        yyy = [1:2:60];
       
        xx = [150:100:q];
        
        
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 1
        cla
        subplot(3,1,1)
        boxplot((cors'),'notch','on');
        hold on
        plot(meanss,'r')
        hold on
        
                %Threshold
                xMin = 0; xMax = 60;
                
                yVal = thresholds;
                plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
                hold on
        
                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = 0; yMax = 1;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = 1;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = 1;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %last match onset (2200ms)
                yMin = 0; yMax = 1;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')
      

        hold off
    
        if str2double(threshh) == 0
        ylim([.0 1]); 
        
        else
        ylim([.2 1]);   
        end  
  
        
        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        
        %set(gca,'XTickLabel',xxx,'FontSize',7)
        xlabel('Time(ms)')
        ylabel('Correlation Coefficients')
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 2    
    subplot(3,1,2)
   
    qq = max(tttt(1:(end-10))) + 10;

plot(tttt,'r')
    hold on
    %plot(sum(number));
    %hold on
%% plot regression line

            %separate out the end of sample to first match

            t = tttt(18:34);
            time = [1:17];

            stats = regstats(t,time);
            
            
            pval = stats.fstat.pval;
            
            %finds the slope of the regression line
            s = stats.tstat.beta(2);

            
            slope = s(1);
         
    

    hold on
    plot([nan(1,17) stats.yhat'],'color','k','LineWidth',2)

hold on
%%    
    
                 %Draw Vertical Lines
                
                %Sample On(500ms)
                %yMin = 0; yMax = q;
                yMin = 0; yMax =qq;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = 0; yMax = qq;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = 0; yMax = qq;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = 0; yMax = qq;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')  
    
    
    hold off
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    ylabel('% of Trials W/ Siginficant Corr.')
    ylim([0 qq]);
    xlabel('Time(ms)')
     
    
   
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 3  
        subplot(3,1,3)

        boxplot((phase'),'notch','on');
        hold on
        plot(xxx,meansss,'r')
        ylabel('Phase Shift of Max correlation (ms)')
        hold on

        
        %set(gca,'XTickLabel',xxx)
        
                
                %Plot zero line
                xMin = 0; xMax = 60;
                
                yVal = 0;
                plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
                hold on
        
                %Draw Vertical Lines
                
                %Sample On(500ms)
                yMin = -25; yMax = 25;
                xVal = 8;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %Sample Off(1000ms)
                yMin = -25; yMax = 25;
                xVal = 18;
                plot([xVal, xVal],[yMin yMax],'color','k')
                hold on
                
                %First match onset (1800ms)
                yMin = -25; yMax = 25;
                xVal = 34;
                plot([xVal, xVal],[yMin yMax],'color','g')
                hold on
                
                %Last match onset (2200ms)
                yMin = -25; yMax = 25;
                xVal = 42;
                plot([xVal, xVal],[yMin yMax],'color','r')
    
    
    
        set(gca,'XTick',yyy)
        set(gca,'XTickLabel',xx)
        xlabel('Time(ms)')
        ylim([-25 25]);
        hold off

        %linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');
                
%zoom on
%linkzoom
    end
