function [HistInfo,varargout] = NeuronalHist(varargin)

%Provides histological information about recording sites
%Run at session level
%Histology information must first be entered manually
%histology files are column vectors w/ entries for all grid positions
%numbers indicate positions w/ respect to the sulcus
%medial/ventral == 0 (PPC/PFC)
%lateral/dorsal == 1 (PPC/PFC)
%in sulcus == 2
%pic argument will retrieve jpegs from day directory
%Jpeg pics must be located in the day directory
%pics are saved as (ex: PP_hist_060428)


%If monkey logic, use 'ml' argument
%uses betty_reconstruction.mat
%uses channel_offsets.mat
%uses categorizeNeuronalHist

Args = struct('pics',0,'bmf',0,'bettynlynx',0);
Args.flags = {'pics','bmf','bettynlynx'};
Args = getOptArgs(varargin,Args);

%determine if ML session
mlogic = nptDir('*.bhv');
if isempty(mlogic)
    monkey_logic = 0;
else
    monkey_logic = 1;
    Args.pics = 0; %no pics yet for ml sessions
end

%gets channel info
[NeuroInfo] = NeuronalChAssign();

chnumb = size(NeuroInfo.groups,2);

HistInfo.chnumb = chnumb; %number of channels
HistInfo.recordedDepth = NeuroInfo.recordedDepth; %add back start depth
HistInfo.gridPos = NeuroInfo.gridPos;

if ~Args.bmf
    HistInfo.cortex = NeuroInfo.cortex;
end

ses = pwd;
fs = find(ses == '/');
day = ses(fs(end-1)+1:fs(end)-1);


if ~monkey_logic
    cd ..
    
    hist_dir = nptDir('histology');
    
    if isempty(hist_dir)
        HistInfo.locs = nan;
        HistInfo.number = nan;
    else
        cd(hist_dir.name)
        load hist_pp hist_pp
        load hist_pf hist_pf
        
        hist_info = []; hhist_info = [];
        for c1 = 1 : chnumb
            depth = NeuroInfo.recordedDepth(c1);
            if NeuroInfo.cortex(c1) == 'P'
                hc1 = hist_pp(NeuroInfo.gridPos(1,c1),1);
                if hc1 == 0
                    if depth < 3000
                        l1 = 'PEC'; %PE same categorization
                        l2 = 'ms'; %medial superficial
                        l3 = 'm'; %medial
                        hn = 8;
                    else
                        l1 = 'MIP';
                        l2 = 'md'; %medial deep
                        l3 = 'm'; %medial
                        hn = 12;
                    end
                elseif hc1 == 1
                    if depth < 3000
                        l1 = 'PG';
                        l2 = 'ls'; %lateral superficial
                        l3 = 'l'; %lateral
                        hn = 11;
                    else
                        l1 = 'LIP';
                        l2 = 'ld'; %lateral deep
                        l3 = 'l'; %lateral
                        hn = 13;
                    end
                end
            else
                hc1 = hist_pf(NeuroInfo.gridPos(1,c1),1);
                if hc1 == 0
                    l1 = 'dPFC';
                    l2 = 'd'; %dorsal
                    l3 = 'd'; %dorsal
                    hn = 4;
                elseif hc1 == 1
                    l1 = 'vPFC';
                    l2 = 'v'; %ventral
                    l3 = 'v'; %ventral
                    hn = 5;
                elseif hc1 == 3 %8B
                    l1 = '8B';
                    l2 = 'i'; %NA
                    l3 = 'd'; %NA
                    hn = 3;
                elseif hc1 == 4 %9L
                    l1 = '9L';
                    l2 = 'i'; %NA
                    l3 = 'd'; %NA
                    hn = 17; %NOTE: this is not an area in betty
                end
            end
            lev1{c1} = l1;
            lev2{c1} = l2;
            lev3{c1} = l3;
            h_number(c1) = hn;
        end
        HistInfo.number = h_number;
        HistInfo.level1 = lev1;
        HistInfo.level2 = lev2;
        HistInfo.level3 = lev3;
    end
else
    
    if ~Args.bmf
        cd ..
        cd ..
        monkeydir = pwd;
        load betty_reconstruction
        %load channel_offsets
        cd(ses)
    else
        cd ../.. %this should be the etyhl dir
        cd('histology')
        load LookUpTable
        cd(ses)
    end
    
    all_depths = (NeuroInfo.recordedDepth) ./ 1000; %get to mm
    deps = floor(all_depths);
    numch = size(all_depths,2);
    for a = 1 : numch
        ch_depth = all_depths(a);
        ch_floor_depth = deps(a);
        if (ch_depth>(ch_floor_depth + .75))
            all_depths(a) = ch_floor_depth + 1;
        elseif (ch_depth<=(ch_floor_depth + .25))
            all_depths(a) = ch_floor_depth;
        else
            all_depths(a) = ch_floor_depth + .5;
        end
    end
    
    depths = (all_depths);
    
    groups = NeuroInfo.groups;
    
    %offsets = channel_offsets(groups);
    
    %HistInfo.offsets = offsets;
    
    %offsets are not longer subtracted from bettys data as of 101201
    %histology information was lined up with snr's so offsets are no longer
    %necessary
    %%%%%%%%%%%depths = depths - offsets; %offsets are in mm
    
    depths = (depths ./ .5);
    
    if ~Args.bmf
        num_channels = size(groups,2);
        if Args.bettynlynx
            load([monkeydir filesep 'nlynx_betty_reconstruction.mat'])
            for x = 1 : num_channels
                HistInfo.location(x,:) = nlynx_betty_reconstruction(groups(x),depths(x));
            end
        else
            for x = 1 : num_channels
                HistInfo.location(x,:) = betty_reconstruction(groups(x),depths(x));
            end
        end
        HistInfo.location = HistInfo.location';
        [h_number,lev1,lev2,lev3] = categorizeNeuronalHistML(HistInfo.location);
        
        HistInfo.number = h_number;
        HistInfo.level1 = lev1;
        HistInfo.level2 = lev2;
        HistInfo.level3 = lev3;
        
    else
        num_channels = size(groups,2);
        for x = 1 : num_channels
            HistInfo.location{x} = ethyl_lookup('table',lut,'day',day,'groups',NeuroInfo.gridPos(x));
        end
        [h_number,lev1,lev2,lev3] = categorizeNeuronalHistBMF(HistInfo.location);
        
        HistInfo.number = h_number;
        HistInfo.level1 = lev1;
        HistInfo.level2 = lev2;
        HistInfo.level3 = lev3;
    end
    
end

if Args.pics
    pics = nptDir('**_hist_****');
    d = imread(pics(1).name);
    dd = imread(pics(2).name);
    figure
    image(d)
    title('PFC','fontsize',12,'fontweight','b')
    zoom on
    figure
    image(dd)
    title('PPC','fontsize',12,'fontweight','b')
    zoom on
end


cd(ses)





