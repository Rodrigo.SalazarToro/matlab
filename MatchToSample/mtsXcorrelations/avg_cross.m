function avg_cross(chnumb,ml)

if ml
    ide_loc = [{'identity'} {'location'}];
    il = [1 2];
else
    ide_loc = {};
    il = [1];
end
lfp2_dir = pwd;
for t = il
    cd([lfp2_dir filesep ide_loc{t}])
    
    %load avg_correlograms avg_correlograms avg_phase avg_corrcoef
    
    load avg_correlograms avg_corrcoef
    
    load avg_surrogates avg_surrogates
    
    %load avg_correlograms_epochs avg_correlograms_epochs avg_phase_epochs avg_corrcoef_epochs
    load avg_correlograms_epochs avg_corrcoef_epochs
    
    cc = [];
    cross = [];
    pair_info = [];
    e1_info = [];
    e2_info = [];
    e3_info = [];
    e4_info = [];

    for c1 = 1 : chnumb
        for c2 = (c1+1) : chnumb
            p = 0;
            for x = 1 : c1
                if x == c1
                    q = c2;
                else
                    q= chnumb;
                end
                
                for y = (x+1) : q
                    
                    p = p + 1;
                    
                end
            end
            
            %get corrcoef
            corrcoef = avg_corrcoef{t,p};
            c  = avg_corrcoef_epochs{t,p};
            e1 = c(1); e2 = c(2); e3 = c(3); e4 = c(4);
            
            %get surrogates
            s = avg_surrogates{t,p};
            
            %calculate 2 standard deviations
            pos2std = 2*(std(s));
            
            if (abs(corrcoef) > pos2std)
                cross(c1,c2) = 99999;
                pair_info(p,t) =  1;
            else
                cross(c1,c2) = 0;
                pair_info(p,t) = 0;
            end
            cc(c1,c2) = abs(corrcoef);
            
            
            if (abs(e1) > pos2std)
                e1_info(p,t) =  1;
            else
                e1_info(p,t) =  0;
            end
            
            if (abs(e2) > pos2std)
                e2_info(p,t) =  1;
            else
                e2_info(p,t) =  0;
            end
            
            if (abs(e3) > pos2std)
                e3_info(p,t) =  1;
            else
                e3_info(p,t) =  0;
            end
            
            if (abs(e4) > pos2std)
                e4_info(p,t) =  1;
            else
                e4_info(p,t) =  0;
            end
            
        end
        
    end
    
    
    save ccTable cc cross pair_info e1_info e2_info e3_info e4_info
end
cd(lfp2_dir)
