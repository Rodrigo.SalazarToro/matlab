function surrogateCorrelogram



    %get index of trials
    lfpdata = nptDir('clark*');
    trials =  size(lfpdata,1);
    chnumb = 9;

    
    ddata = [];
    for xxx = 1 : trials
        
        load (lfpdata(xxx).name, 'normdata')
        
        %Concatenates the data from all lfp files to run surrogate analysis
        ddata = [ddata data2];
        
        %calculate the average trial length
        windowSize = ceil(size(ddata,2) / trials); 
        
    end


    surrogate=cell(1,36);
    
    pair = 0;
    for ii=1:chnumb
        fprintf('\n%0.5g     ',ii)
        for jj=ii+1:chnumb
            fprintf(' %0.5g',jj)
            
            pair = pair + 1;
            %find xcorr at random shifts
            r_surrogate = RandXxcorr(ddata(ii,:),ddata(jj,:),windowSize,round(10000));

            surrogate{pair} = r_surrogate;

        end
    end
    



save surrogate surrogate



