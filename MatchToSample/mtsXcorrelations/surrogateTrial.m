function surrogateTrial(chnumb)

%computes the surrogate thresholds for the average correlograms
global ml monkey day_lfp2

%cd('/media/MONTY/data/clark/060428/session02/lfp')

%ml = 0;
%monkey = 'clark'

ddd = pwd;
if ml
    cd([day_lfp2 filesep 'lfp2'])
    write_dir = pwd;
else
    cd([ddd filesep 'lfp2'])
end

%get index of trials
lfpdata = nptDir([monkey '*']);
%trials =  size(lfpdata,1);
cd(ddd)
number_pairs = ((chnumb*(chnumb-1)) / 2);



d = cd;
cd ..
if ~ml
    %cd ..
end

if ~ml
    mt=mtstrial('auto');
    %get only the specified trial indices (correct and stable)
    trials = mtsgetTrials(mt,'BehResp',1,'stable');
else
    mt = mtstrial('auto','ML','RTfromML');
    %get only the specified trial indices (correct and stable)
    all_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML');
    
    
    
    type = mt.data.Index(:,1);
    t_ide = find(type == 1);
    t_loc = find(type == 2);
    
    identity = intersect(all_trials,t_ide);
    location = intersect(all_trials,t_loc);
    
    %     bhvFile = nptDir('*.bhv');
    %
    %     b = bhv_read([pwd filesep bhvFile(1).name]);
    
end




cd(write_dir)

%%%%%%%%%%%%%%Make Surrogates%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load channel_matrix channel_matrix

if ml
    num_rules = [1 2];
else
    num_rules = [1];
end

for rule = num_rules
    if rule == 1
        trials = identity;
    else
        trials = location;
    end

n = nptdir('betty*');
num_trials = size(trials,2);



iterations = 1000;

avg_surrogates = cell(1,number_pairs);

pair = 0;
for cc1 = 1 : chnumb
    fprintf('\n%0.5g     ',cc1)
    %loads the each channel
    data1 = channel_matrix{1,cc1};
    
    
    for cc2 = (cc1 + 1) : chnumb
         
        data2 = channel_matrix{1,cc2};
         fprintf(' %0.5g',cc2)
        
        pair = pair + 1;
        %             pair = 0;
        %             for x = 1 : cc1
        %                 if x == cc1
        %                     q = cc2;
        %                 else
        %                     q= chnumb;
        %                 end
        %                 for y = (x+1) : q
        %                     pair = pair + 1;
        %                 end
        %             end
        
        all_correlations = zeros(iterations,1);
        
        for iter = 1 : iterations
            
            x1 = zeros(1,num_trials);
            %%
            t = 0;
            while(t == 0)
                t1 = randperm(num_trials);
                t2 = randperm(num_trials);
                for check = 1: num_trials
                    if t1(check) == t2(check)
                        t = 0;
                        break
                    else
                        t = 1;
                    end
                end
            end
            
            
            %%
            for permut = 1 : num_trials
                tt1 = trials(1,t1(1,permut));
                tt2 = trials(1,t2(1,permut));
                d1 = data1(tt1,:);
                d2 = data2(tt2,:);
                
                x1(1,permut) = xcorr(d1,d2,0,'coef');
                
            end
            
            all_correlations(iter,:) = mean(x1);
            
        end
        
        avg_surrogates{pair} = all_correlations;
        
        if ml
            if rule == 1;
                cd([write_dir filesep 'identity'])
                save avg_surrogates avg_surrogates
            else
                cd([write_dir filesep 'location'])
                save avg_surrogates avg_surrogates
            end
        else
            save avg_surrogates avg_surrogates
        end
        
        
        
        
        
        
        
    end
    
end


end

fprintf(1,'\n')
fprintf(1,'Average Correlograms and Surrogates Done.\n')






























