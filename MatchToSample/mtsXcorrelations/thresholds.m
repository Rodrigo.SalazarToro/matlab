function thresholds


%writes thresholds for channelpair files

%%%%%%%%%%%%%%%%%%%%%%%%Thresholds%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

directory = cd;

%Filter
LowPassLow = 10; %10
LowPassHigh = 100; %40

%Xcorr
windowSize = 200; %200
windowStep = 50; %50
phaseSize = 50; %50

%Run on sessions 2 and 3

for sessions = 2:3

    sesion = ['session0' num2str(sessions)];

    directs = [directory '\' sesion];

    cd(directs)


    [NeuroInfo] = NeuronalChAssign;

    %Get number of channels
    chnumb=length(NeuroInfo.channels);

    mt=mtstrial('auto');

    matchonset = mt.data.MatchOnset;

    firstsac =  mt.data.FirstSac;

    %Get number of trials
    NumberOfTrials = mt.data.numSets;


    directss = [directs '\lfp'];

    cd(directss)


    ddata = [];

    for x = 1:NumberOfTrials


        sessionname  = [directss((end-25):(end-21)) directss((end-19):(end-14)) directss((end-5):(end-4)) '_lfp.0000'];

        if x > 999
            y = 4;
        elseif x > 99
            y = 3;
        elseif x > 9
            y = 2;
        else
            y = 1;
        end


        %load data
        [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile([sessionname(1:end-y) num2str(x)]);


        %Creates structure 'd' w/ streamer data

        d.rawdata = data;
        d.samplingRate = sampling_rate;
        d.numChannels = num_channels;
        d.scanOrder = scan_order;



        [data,samplingRate]=nptLowPassFilter(d.rawdata,d.samplingRate,LowPassLow,LowPassHigh);




        %Concatenates the data from all files
        ddata = [ddata data];



    end


    rr_surrogate=[];

    for ii=1:chnumb
        fprintf('\n%0.5g     ',ii)
        for jj=ii+1:chnumb
            fprintf(' %0.5g',jj)


            %find xcorr at random shifts
            r_surrogate = RandXxcorr(ddata(ii,:),ddata(jj,:),windowSize,round(10000));

            rr_surrogate = [rr_surrogate r_surrogate];



        end
    end

    threshold=[];


    %%this determines the number of randam samples for all pairwise combos.
    numbrandsamples = ((chnumb * (chnumb-1)) / 2) * 10000;

    for p = 1:10000:numbrandsamples


        %Loads 10000 samples for each pairwise combination
        pp = rr_surrogate(1,(p:(p+9999)));

        percentiles = prctile(pp,[95 99 99.9 99.99]);

        threshold = [threshold percentiles];


    end
    save threshold threshold
end




