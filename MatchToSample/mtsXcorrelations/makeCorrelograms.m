function makeCorrelograms(c1,c2,number)


pair = ['channelpair' c1 c2];

load (pair, 'phases','corrcoefs','correlograms')

r = correlograms{number};

s = size(r,1);

if s > 48
   
    %if there are more than 48 data points
    r = r((1:48),:);
    s = 48;

end
figure
t = 150; %used to label the plots
for x = 1 : s
    
    subplot(6,8,x)
    
  
    %plot the correlogram centered at zero
    plot([-50:50],r(x,:))
    
    hold on
    
    
    %draw phase line
    yMin = -1; yMax = 1;
    xVal = phases(x,number);
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    %draw line for -25 phase
    yMin = -1; yMax = 1;
    xVal = -25;
    plot([xVal, xVal],[yMin yMax],'color','g')
    hold on
    
    %draw line for +25 phase
    yMin = -1; yMax = 1;
    xVal = 25;
    plot([xVal, xVal],[yMin yMax],'color','g')
    hold on
    
    %draw correlation coef line          
    xMin = -50; xMax = 50;
    yVal = corrcoefs(x,number);
    plot([xMin xMax],[yVal, yVal],'color','r')
    
    
    %draw horizontal zero line          
    xMin = -50; xMax = 50;
    yVal = 0;
    plot([xMin xMax],[yVal, yVal],'color','k')
    
    
    %draw vertical zero line 
    yMin = -1; yMax = 1;
    xVal = 0;
    plot([xVal, xVal],[yMin yMax],'color','k')
    hold on
    
    
    
    
    
    
    ylabel(num2str(t))
   
    t = t + 50;     
    
    tt = ['Channel Pair ',num2str(c1),' ',num2str(c2),',  Trial: ',int2str(number),',  Correlograms'];

    title(subplot(6,8,4),tt,'fontsize',10,'fontweight','b');
                
end





