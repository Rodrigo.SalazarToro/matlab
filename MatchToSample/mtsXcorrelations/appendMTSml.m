function appendMTSml

%appends mts and ds sessions
%creates one session with all trials and a behavior file saved as bhv.mat
%run at day level

% for task_type = 1 : 2
%         
%     task = 'MTS*.bhv';
%     if task_type == 2
%         task = 'DS*.bhv';
%     end
% 
%     d = pwd;
%     day = d(end-5:end);
%     %determine which sessions are mts
%     sessions = nptdir('session0*');
%     ses = [];
%     for s = 1 : size(sessions,1)
%         cd(sessions(s).name)
%         if ~(isempty(nptDir(task)))
%             ses = [ses s];
%         end
%         cd ..
%     end
% 
%         cd(sessions(ses(1)).name)
%         ses1 = pwd; %session01 directory
%         bhv_file = nptDir(task);
%         bhv1 = bhv_read([cd filesep bhv_file(1).name]);
%         cd ..
% 
%         for ss = ses(2:end)
% 
%             cd(sessions(ss).name)
%             trial_list =  nptDir(['*' day '*.****']);
%             ses_dir = pwd; %current session directory
%             bhv_file = nptDir(task);
%             bhv = bhv_read([cd filesep bhv_file(1).name]);
%             new_trials = size(trial_list,1);
% 
%             bhv1.BlockOrder = [bhv1.BlockOrder; bhv.BlockOrder];
%             bhv1.FinishTime = bhv.FinishTime;
%             for x = 1 : 1143%new_trials
%                 t = max(bhv1.TrialNumber);
%                 bhv1.TrialNumber(t+1) = t + 1;
%                 bhv1.AbsoluteTrialStartTime((t+1),6) = bhv.AbsoluteTrialStartTime(x);
%                 bhv1.BlockNumber(t+1) = bhv.BlockNumber(x);
%                 bhv1.BlockIndex(t+1) = bhv.BlockIndex(x);
%                 bhv1.ConditionNumber(t+1) = bhv.ConditionNumber(x);
%                 bhv1.TrialError(t+1) = bhv.TrialError(x);
%                 bhv1.CycleRate(t+1) = bhv.CycleRate(x);
%                 bhv1.NumCodes(t+1) = bhv.NumCodes(x);
%                 bhv1.CodeNumbers{t+1} = bhv.CodeNumbers{x};
%                 bhv1.CodeTimes{t+1} = bhv.CodeTimes{x};
%                 bhv1.AnalogData{t+1} = bhv.AnalogData{x};
%                 bhv1.ReactionTime(1,(t+1)) = bhv.ReactionTime(1,x);
%                 bhv1.ObjectStatusRecord(1,(t+1)) = bhv.ObjectStatusRecord(1,x);
%                 bhv1.RewardRecord(1,(t+1)) = bhv.RewardRecord(1,x);
% 
%                 q = size(num2str(t+1),2);
% 
%                 qq = size(trial_list(x).name,2) - 1;
%                 newTrial = [trial_list(x).name((end-qq):(end-q)) num2str(t+1)]
% 
%                 movefile([ses_dir filesep trial_list(x).name], [ses1 filesep newTrial])
% 
%             end
%             %move bhv file to first session
%             movefile([ses_dir filesep bhv_file(1).name], [ses1 filesep bhv_file(1).name])
%             cd ..
%             %remove empty session dir
%             rmdir(sessions(ss).name,'s')
%         end
% 
%         %save bhv structure in first session
%         cd(sessions(ses(1)).name)
%         bhv = bhv1;
%         save bhv bhv
% 
% 
%     
% end
%     

load b b
    
 fidbhv = fopen('MTS-betty-06-16-2009.bhv','a')
 


 
 
 
 
 
 
 
 
 
 
 bhvwrite(2, bhvfid, WriteData(trial));

fclose(fidbhv)
b = bhv_read('G:\data\betty\MTS-betty-06-16-2009.bhv')
 

