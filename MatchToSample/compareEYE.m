function compareEYE(varargin)
%compares eye traces
%run in day directory
%saves eyecompare (3 cells) in mlDAqmatch.mat
%   -Vertical
%   -Horizontal
%   -Bad Trials
%trials are considered bad if the correlation coefficient for either the
%vertical or horizontal is < .7
%   Args
%   -'plotBad' will plot the bad trials after the computations are
%   complete
%
%   -'redo' will redo computations (if 'redo' and 'plotBad' are both
%   arguments then the computations will be done first.
%
%   -'check' will provide an option to write a eyeSkip.txt file to indicate
%   that the eye information is incorrect for that session.

Args = struct('Hist',0,'plotBad',0,'redo',0,'plotAll',0,'check',0,'nlynx',0,'sessions',[]);
Args.flags = {'Hist','plotBad','redo','plotAll','check','nlynx'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'Hist','plotBad'});

global discrepancyEYE

%create session list
sessions = nptdir('session0*');
wrote = 0;
for x = Args.sessions
    
    cd(sessions(x).name)
    skip = nptDir('skip.txt');
    if isempty(skip)
        %determine if mlDAQmatch file is written
        written = (~(isempty(nptDir('mlDAQmatch.mat'))));
        
        %get behavioral data to determine when the fixation point appears
        bhv = nptDir('*.bhv');
        b = BHV_read([cd filesep bhv.name]);
        
        
        if(Args.check)
            reply = input('Is eye data correct? y/n.  If [n] then eyeSkip.txt is written in session folder. : ', 's');
            if strmatch('n',reply)
                save eyeSkip.txt
            end
            if strmatch('y',reply)
                save check.txt
            end
        end
        
        %used when running checkConsistency to determine if their are errors
        %and if the trials have been reviewed.
        if (written) && (~Args.redo) && (~Args.plotBad) && (~Args.plotAll)
            load mlDAQmatch eyecompare
            if (isempty(eyecompare{1})) || (~isempty(nptDir('check.txt')));
                discrepancyEYE(1,x) = 0;
            elseif (~isempty(eyecompare{1})) || (~isempty(nptDir('eyeSkip.txt')))
                discrepancyEYE(1,x) = 1;
            end
            cd ..
            continue
        end
        
        %determine eye channels using the descriptor file
        descriptor_file = nptDir('*_descriptor.txt');
        descriptor_info = ReadDescriptor(descriptor_file.name);
        
        vertical = strmatch('vertical1',descriptor_info.description);
        horizontal = strmatch('horizontal1',descriptor_info.description);
        
        if ~Args.nlynx
            %get trials
            cd delfiles
            load deletedfiles trials
            num_trials = size(trials,1);
            cd ..
        end
        %get list of good trials
        dirlist = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*'); nptDir('*.3*')];
        if Args.nlynx
            num_trials = size(dirlist,1);
            trials = sort([find(single(b.TrialError) == 0); find(single(b.TrialError) == 6)])'; %all correct and incorrect trials are written
        end
        
        
        %BHV!!!
        % % % % % % % %
        % % % % % % % %      eobj = eyemvt('auto');
        % % % % % % % %      trials = eobj.data.numSets;
        % % % % % % % %      c = cd;
        % % % % % % % %      cc = ['*' c(end-15:end-10) c(end-1:end) '.****'];
        % % % % % % % %      dirlist = nptDir(cc);
        % % % % % % % %
        % % % % % % % %      cd('eye')
        % % % % % % % %      e = ['*' c(end-15:end-10) c(end-1:end) '_eye.****'];
        % % % % % % % %      eyedir = nptDir(e);
        % % % % % % % %
        % % % % % % % %      cd ..
        
        eyecompare = cell(1,1);
        eyecomp1 = [];
        eyecomp2 = [];
        h=[];
        badtrials = [];
        
        if (written) && (Args.plotBad) && (~Args.redo)
            %plot bad trials
            load mlDAQmatch
            i = eyecompare{1}';
        else
            i = [1:num_trials];
        end
        
        type = DaqType(dirlist(1).name);
        for ii = i
            trial = ii;

            %get daq eye traces
            
            if strcmp(type,'Streamer')
                [all_data,num_channels,sampling_rate,scan_order,points] = nptReadStreamerFile(dirlist(ii).name);
                c = num_channels;
                d = (resample(all_data',1000,sampling_rate))';
            else
                data = ReadUEIFile('FileName',dirlist(ii).name,'Units','MilliVolts');
                c = data.numChannels;
                d = (resample(data.rawdata',1000,data.samplingRate))';
            end
            
            if Args.nlynx
                eye1 = d(vertical,:) ./ 1000 * -1;
                eye2 = d(horizontal,:) ./ 1000 * -1;
            else
                eye1 = d(vertical,:);
                eye2 = d(horizontal,:);
            end
            
            %get fixation time
            times = b.CodeTimes{trials(ii)};
            t1 = times(1);
            times = times - t1;
            fix = times(4);
            match = times(5);
            
            %get BHV eye traces
            eyedata = b.AnalogData{trials(ii)}.EyeSignal;
            eyedata = (eyedata((t1:end),:))'; %subtract first bhv code time
            
            horizontal_bhv = eyedata(1,:); %HORIZONTAL
            vertical_bhv = eyedata(2,:); %VERTICAL
            
            %match bhv data with daq
            eyedata1 = vertical_bhv;
            eyedata2 = horizontal_bhv;
            
            difference = size(eye1,2)-size(eyedata,2);
            h(1,ii) = difference;
            le = min([size(eye1,2) size(eyedata,2)]);
            %take 50 points off the end to account for daq errors (both ends)
            le = le-50;
            
            
            % % % %
            % % % %         cd('eye')
            % % % %
            % % % %         %data used to plot eyemvt object
            % % % %         filename = eyedir(ii).name;
            % % % %         eyedata= nptReadDataFile(filename);
            % % % %         %make same length as UEIeye
            % % % %         difference = length(eye1)-length(eyedata);
            % % % %         h(1,ii) = difference;
            % % % %     %     if length(eye1) < length(eyedata)
            % % % %     %         raweye1 = eyedata(1,(1:length(eye1)));
            % % % %     %         raweye2 = eyedata(2,(1:length(eye2)));
            % % % %     %     else
            % % % %     %         raweye1 = eyedata(1,:);
            % % % %     %         raweye2 = eyedata(2,:);
            % % % %     %         eye1 = eye1(1,(1:length(raweye1)));
            % % % %     %         eye2 = eye2(1,(1:length(raweye2)));
            % % % %     %     end
            % % % %         le = min([length(eye1) length(eyedata)]);
            % % % %         %take 15 points off the end to account for daq errors (both ends)
            % % % %         le = le-15;
            % % % %         cd ..
            
            
            
            %find zero time lag corrcoef and subtract data before fixation point
            eyecorr1 = corrcoef(eye1(50:le-110),eyedata1(50:le-110));
            eyecorr2 = corrcoef(eye2(50:le-110),eyedata2(50:le-110));
            
            eyecomp1(ii,:) = eyecorr1(1,2);
            eyecomp2(ii,:) = eyecorr2(1,2);
            
            if (Args.plotBad) && (~Args.redo)
                
            elseif (abs(eyecorr1(1,2)) < .7) || (abs(eyecorr2(1,2)) < .7)
                badtrials = ([badtrials; ii]);
                fprintf(1,'\n')
                fprintf(1,['Bad correlation eye trace correlation  ' num2str(ii) '  '   pwd]);
                fprintf(1,'\n')
                discrepancyEYE = 1;
            end
            
            if ((written) && (Args.plotBad) && (~Args.redo)) || (Args.plotAll)
                fprintf(1,'\n')
                fprintf(1,['Session: ', num2str(x),'  Trial: ',num2str(trial)])
                fprintf(1,'\n')
                fprintf(1,['Vertical: ', num2str(eyecorr1(1,2))])
                fprintf(1,'\n')
                fprintf(1,['Horizontal: ', num2str(eyecorr2(1,2))])
                fprintf(1,'\n')
                fprintf(pwd)
                ti = [1 : le];
                subplot(2,1,1)
                plotyy([1:length(eye1(fix:le))],eye1(fix:le),[1:length(eyedata1(fix:le))],eyedata1((fix:le)))
                legend([{'DAQ'} {'BHV'}])
                grid on
                
                subplot(2,1,2)
                plotyy([1:length(eye2(fix:le))],eye2(fix:le),[1:length(eyedata2(fix:le))],eyedata2((fix:le)))
                legend([{'DAQ'} {'BHV'}])
                grid on
                pause
                
            end
            
        end
        
        %      eyecompare{1} = eyecomp1;
        %      eyecompare{2} = eyecomp2;
        %      eyecompare{3} = badtrials;
        
        eyecompare{1} = badtrials;
        
        
        if (~written) || (Args.redo)
            save mlDAQmatch eyecompare
            wrote = 1;
        else
            wrote = 0;
        end
    end
    cd ..
    
end

%if plot trial argument is selected and the comparisons have not been made
%then rerun after they have been.
if (wrote == 1) && (Args.plotBad)
    compareEYE('plotBad')
end

