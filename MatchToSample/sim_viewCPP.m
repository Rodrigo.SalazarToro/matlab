 function viewCPP(varargin)
 
%FIRST run writeCPPdata and threshold in the day directory 
%Dependent on writeCPP, CPP. 
%Run at session level
%Plots corrcoefs, %trials above theshold, and phases for all trials of
%specific channel pair combination.

%Global variables
global sessionname directory ind trial ml

Args = struct('ml',0,'identity',0,'location',0);
Args.flags = {'ml','identity','location'};
[Args,modvarargin] = getOptArgs(varargin,Args);

ml = Args.ml;
%[sessionname, directory] = uigetfile('channelpair12.mat','Select First Pair');
sesdir = pwd;
cd('lfp')
cd('lfp2')
sessionname = 'sim_channelpair12.mat';
directory  = pwd;
cd(sesdir)
[NeuroInfo] = NeuronalChAssign;
global chnumb
chnumb=length(NeuroInfo.channels);


%Determine trial type
if ~ml
    mt=mtstrial('auto');
else
    mt = mtstrial('auto','ML','RTfromML');
end

cd('lfp')
if ~ml
    if mt.data.Index(1,1) == 1;
        trialtype = 'identity';
    else
        trialtype = 'location';
    end
else
    if Args.identity
        trialtype = 'identity';
    elseif Args.location
        trialtype = 'location';
    else
        fprintf(1,['Enter rule type (location or identity)\n'])
    end
    
    
end

cd('lfp2') %go to lfp2 folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%GUI SETUP%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Global handle definitions
global  h_fig  h_back h_next h_c1 h_c2 h_channels h_title h_threshval h_BehResp h_ts h_BehResp2 h_ts2 h_n h_threshold h_type h_match...
    h_gets h_m h_object h_location h_ob h_lo h_avg h_stims h_avgcorr h_summary h_histology

%Create figure
h_fig = figure('name','Pairs');  
%Create text box
h_channels = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.0 .0 .15 .05],'fontsize',10,'fontweight','b');
%Create text box
h_title = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.45 .95 .08 .05],'fontsize',10,'fontweight','b');
%Create text box
h_n = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.0 .4 .06 .05],'fontsize',9,'fontweight','b');    
%Create edit text box
h_c1 = uicontrol(h_fig,'Style','edit','String','1','units','normalized',...
    'Position',[.32 .0 .05 .05],'Callback',@c1_callback,'fontsize',10,'fontweight','b');
%Create edit text box
h_c2 = uicontrol(h_fig,'Style','edit','String','1','units','normalized',...
    'Position',[.38 .0 .05 .05],'Callback',@c2_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'back'
h_back = uicontrol(h_fig,'Style','pushbutton','String','Back','units','normalized',...
    'Position',[.16 .0 .15 .05],'Callback',@back_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'next'
h_next = uicontrol(h_fig,'Style','pushbutton','String','Next','units','normalized',...
    'Position',[.44 .0 .15 .05],'Callback',@next_callback,'fontsize',10,'fontweight','b');
%Create edit text box
h_threshval = uicontrol(h_fig,'Style','edit','string','1','units','normalized',...
'Position',[.94 .8 .06 .05],'Callback',@threshval_callback,'fontsize',10,'fontweight','b');
%Create edit text box
h_BehResp = uicontrol(h_fig,'Style','edit','String','1','units','normalized',...
    'Position',[.74 .0 .05 .05],'Callback',@BehResp_callback,'fontsize',10,'fontweight','b');
%Create edit text box
h_ts = uicontrol(h_fig,'Style','edit','String','1','units','normalized',...
    'Position',[.92 .0 .05 .05],'Callback',@ts_callback,'fontsize',10,'fontweight','b');
%Create text box
h_BehResp2 = uicontrol(h_fig,'Style','text','String','Correct(1) / Incorrect(0)','units','normalized',...
    'Position',[.62 .0 .12 .05],'fontweight','b','fontsize',10);
%Create text box
h_ts2 = uicontrol(h_fig,'Style','text','String','Stable(1) / Transition(0)','units','normalized',...
    'Position',[.8 .0 .12 .05],'fontweight','b','fontsize',10);
%Create text box
h_threshold = uicontrol(h_fig,'Style','text','String',{'Threshold';'';'0%(0)';'';'95%(1)';'';'99%(2)';'';'99.9%(3)';'';'99.99%(4)'},'units','normalized',...
    'Position',[.94 .52 .06 .28],'fontweight','b','fontsize',9);
%Create text box
h_m = uicontrol(h_fig,'Style','text','String',{'Match';'';'All(0)';'';'1800-1900(1)';'';'1900-2000(2)';'';'2000-2100(3)';'';'2100-2200(4)'},'units','normalized',...
    'Position',[.0 .52 .06 .28],'fontweight','b','fontsize',9);
%Create text box
h_type = uicontrol(h_fig,'Style','text','String','','units','normalized',...
   'Position',[.0 .95 .1 .05],'fontsize',10,'fontweight','b');
%Create edit text box
h_match = uicontrol(h_fig,'Style','edit','String','0','units','normalized',...
    'Position',[.0 .8 .06 .05],'Callback',@match_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'Get'
 h_gets = uicontrol(h_fig,'Style','pushbutton','String','Get','units','normalized',...
    'Position',[.0 .3 .05 .05],'Callback',@gets_callback,'fontsize',10,'fontweight','b');
%Create edit text box
h_object = uicontrol(h_fig,'Style','edit','String','0','units','normalized',...
    'Position',[.94 .27 .06 .05],'Callback',@object_callback,'fontsize',10,'fontweight','b');
%Create text box
h_ob = uicontrol(h_fig,'Style','text','String',{'Object';'';'0,1,2,3'},'units','normalized',...
    'Position',[.94 .32 .06 .1],'fontsize',9,'fontweight','b');
%Create edit text box
h_location = uicontrol(h_fig,'Style','edit','String','0','units','normalized',...
    'Position',[.94 .1 .06 .05],'Callback',@location_callback,'fontsize',10,'fontweight','b');
%Create text box
h_lo = uicontrol(h_fig,'Style','text','String',{'Location';'';'0,1,2,3'},'units','normalized',...
    'Position',[.94 .15 .06 .1],'fontsize',9,'fontweight','b');
%Create pushbutton 'Avg'
h_avg = uicontrol(h_fig,'Style','pushbutton','String','Avg','units','normalized',...
    'Position',[.0 .2 .05 .05],'Callback',@avg_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'Stims'
h_stims = uicontrol(h_fig,'Style','pushbutton','String','Stim','units','normalized',...
    'Position',[.0 .1 .05 .05],'Callback',@stims_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'Average Correlogram'
h_avgcorr = uicontrol(h_fig,'Style','pushbutton','String','Average Correlogram','units','normalized',...
    'Position',[.15 .95 .15 .05],'Callback',@avgcorr_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'Summary'
h_summary = uicontrol(h_fig,'Style','pushbutton','String','Summary','units','normalized',...
    'Position',[.85 .95 .15 .05],'Callback',@summary_callback,'fontsize',10,'fontweight','b');
%Create pushbutton 'Histology'
h_histology = uicontrol(h_fig,'Style','pushbutton','String','Histology','units','normalized',...
    'Position',[.65 .95 .15 .05],'Callback',@histology_callback,'fontsize',10,'fontweight','b');


%Display trial type
set(h_type,'String',['Trial Type: ',trialtype]);
%Display the number of channels
set(h_channels,'String',[num2str(chnumb),' Channels ', NeuroInfo.cortex]);

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CALLBACKS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function threshval_callback(hObject,eventdata)
         c2_callback(hObject,eventdata)
    end
    function BehResp_callback(hObject,eventdata)    
         c2_callback(hObject,eventdata)   
    end
    function ts_callback(hObject,eventdata)
         c2_callback(hObject,eventdata)
    end
    function c1_callback(hObject,eventdata)   
         c2_callback(hObject,eventdata)   
    end
    function match_callback(hObject,eventdata)      
         c2_callback(hObject,eventdata)    
    end
    function gets_callback(hObject,eventdata)   
        [sessionname, directory] = uigetfile('sim_channelpair12.mat','Select First Pair');
        c2_callback(hObject,eventdata)   
    end
    function object_callback(hObject,eventdata)
         c2_callback(hObject,eventdata)
    end
    function location_callback(hObject,eventdata)
         c2_callback(hObject,eventdata)
    end

    function avg_callback(hObject,eventdata)
    %%get criteria
    b1 = str2double(get(h_BehResp,'String'));
    ts1 = str2double(get(h_ts,'String'));
    c1 = str2double(get(h_c1,'String'));
    c2 = str2double(get(h_c2,'String'));
    threshh = get(h_threshval,'String');
    match = str2double(get(h_match,'String'));
    object = str2double(get(h_object,'String'));
    location = str2double(get(h_location,'String'));
         avgPercents(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb)
    end

    function stims_callback(hObject,eventdata)
    %%get criteria
    b1 = str2double(get(h_BehResp,'String'));
    ts1 = str2double(get(h_ts,'String'));
    c1 = str2double(get(h_c1,'String'));
    c2 = str2double(get(h_c2,'String'));
    threshh = get(h_threshval,'String');
    match = str2double(get(h_match,'String'));
         cppStimuli(b1,ts1,c1,c2,threshh,sessionname,match,chnumb,trialtype,ml)
    end
    %Used to plot the average correlogram for the specific pair
    function avgcorr_callback(hObject,eventdata)
        c1 = str2double(get(h_c1,'String'));
        c2 = str2double(get(h_c2,'String'));
        
        avgCorr(c1,c2,chnumb)
        
    end
    %Used to plot a summary table for avg correlogram threshold
    function summary_callback(hObject,eventdata)
        ccTable(chnumb)   
    end
    
    %Get histology information
    function histology_callback(hObject,eventdata)
        d = pwd;
        cd ..
        cd ..
        [HistInfo] = NeuronalHist('pics')
        cd(d)
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function next_callback(hObject,eventdata)
  
    %%get criteria
    b1 = str2double(get(h_BehResp,'String'));
    ts1 = str2double(get(h_ts,'String'));
    c1 = str2double(get(h_c1,'String'));
    c2 = str2double(get(h_c2,'String'));
    threshh = get(h_threshval,'String');
    match = str2double(get(h_match,'String'));
    object = str2double(get(h_object,'String'));
    location = str2double(get(h_location,'String'));
            if c2<chnumb                
                c2=c2+1; 
            else
                c1=c1+1;
                c2=c1+1;  
            end
    trials = ([sessionname(1:end-6)]);
    if strcmp(trials,'sim_channelpair') || strcmp(trials,'start2match')
        CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb,0,trialtype)
        t = 'Trial Locked';
    else 
        matchCPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location) 
        t = 'Match Locked';
    end

    set(h_n,'String',['# Trials: ' num2str(length(ind))]);    
    set(h_title,'String',t);
    set(h_c1,'String',num2str(c1));
    set(h_c2,'String',num2str(c2));
    set(h_match,'String',num2str(match));
    end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function back_callback(hObject,eventdata)
            
    %%get criteria
    b1 = str2double(get(h_BehResp,'String'));
    ts1 = str2double(get(h_ts,'String'));
    c1 = str2double(get(h_c1,'String'));
    c2 = str2double(get(h_c2,'String'));
    threshh = get(h_threshval,'String');
    match = str2double(get(h_match,'String'));
    object = str2double(get(h_object,'String'));
    location = str2double(get(h_location,'String'));
            if c2 > (c1+1)
                c2=c2-1;
            else
                c1=c1-1;
                c2=chnumb;
            end
    trials = ([sessionname(1:end-6)]);       
    if strcmp(trials,'sim_channelpair') || strcmp(trials,'start2match')
        CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb,0,trialtype)
        t = 'Trial Locked';
    else 
        matchCPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,0)
        t = 'Match Locked';
    end

    set(h_n,'String',['# Trials: ' num2str(length(ind))]);    
    set(h_title,'String',t);
    set(h_c1,'String',num2str(c1));
    set(h_c2,'String',num2str(c2));
    set(h_match,'String',num2str(match)); 
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    function c2_callback(hObject,eventdata)
        
    %%get criteria
    b1 = str2double(get(h_BehResp,'String'));
    ts1 = str2double(get(h_ts,'String'));
    c1 = str2double(get(h_c1,'String'));
    c2 = str2double(get(h_c2,'String'));         
    threshh = get(h_threshval,'String');
    match = str2double(get(h_match,'String'));
    object = str2double(get(h_object,'String'));
    location = str2double(get(h_location,'String'));
    
    trials = ([sessionname(1:end-6)]);
    if strcmp(trials,'sim_channelpair') || strcmp(trials,'start2match')
        CPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,chnumb,0,trialtype)
        t = 'Trial Locked';
    else 
        matchCPP(b1,ts1,c1,c2,threshh,sessionname,match,object,location,0)
        t = 'Match Locked';
    end
   
    set(h_n,'String',['# Trials: ' num2str(length(ind))]);    
    set(h_title,'String',t);
    set(h_c1,'String',num2str(c1));
    set(h_c2,'String',num2str(c2));
    set(h_match,'String',num2str(match)); 
    end



end


 





