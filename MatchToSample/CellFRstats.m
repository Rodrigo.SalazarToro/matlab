function [cue,delay,varargout] = CellFRstats(allspikes,varargin)


ba = [];
sa = [];
d1 = [];
d2 = [];
stims = [];
mat = [];
for c = 1 : length(allspikes)
    if ~isempty(allspikes{c})
        ba = [ba; allspikes{c}(:,1)];
        sa = [sa; allspikes{c}(:,2)];
        d1 = [d1; allspikes{c}(:,3)];
        d2 = [d2; allspikes{c}(:,4)];

        stims = [stims; repmat(c,size(allspikes{c},1),1)];

        [h,cue.sample(c).p] = ttest2(allspikes{c}(:,1),allspikes{c}(:,2));
        cue.sample(c).effect = allspikes{c}(:,1) - allspikes{c}(:,2);

        [h,delay.sample(c).p] = ttest2(allspikes{c}(:,3),allspikes{c}(:,4));
        delay.sample(c).effect = allspikes{c}(:,3) - allspikes{c}(:,4);

        if size(allspikes{c},2) == 5
            mat = [mat; allspikes{c}(:,5)];
            [h,match.sample(c).p] = ttest2(allspikes{c}(:,4),allspikes{c}(:,5));
            match.sample(c).effect = mean(allspikes{c}(:,4) - allspikes{c}(:,5));
        else
            match.sample(c).effect = NaN;
            match.sample(c).p = NaN;
        end
    else
        cue.sample(c).effect = NaN;
        cue.sample(c).p = NaN;
        
        delay.sample(c).effect = NaN;
        delay.sample(c).p = NaN;
        
        match.sample(c).effect = NaN;
        match.sample(c).p = NaN;
    end
end

%% effect of the cue
% [h,cue.p] = ttest2(ba,sa);
try
[cue.p,table,stats] = anovan([ba; sa],{[zeros(length(ba),1); ones(length(sa),1)] repmat(stims,2,1)},'model','interaction','display','off');

cue.effect = mean(ba - sa);
catch
    cue.effect = nan(1,1);
end

%% effect of the delay
try
% [h,delay.p] = ttest2(d1,d2);
[delay.p,table,stats] = anovan([d1; d2],{[zeros(length(d1),1); ones(length(d2),1)] repmat(stims,2,1)},'model','interaction','display','off');

delay.effect = mean(d1 - d2);
catch
     delay.effect = nan(1,1);
end

%% effect of the match

% [h,delay.p] = ttest2(d1,d2);
if size(allspikes{c},2) == 5
    [match.p,table,stats] = anovan([d2; mat],{[zeros(length(d2),1); ones(length(mat),1)] repmat(stims,2,1)},'model','interaction','display','off');

    match.effect = mean(d2 - mat);
else
    match.p = nan(1,3);
    match.effect = NaN;
end

if nargout == 3

    varargout{1} = match;
end
varargout{1} = match;

