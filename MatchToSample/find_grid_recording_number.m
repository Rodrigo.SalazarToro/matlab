function find_grid_recording_number


monkeydir = pwd;

load idedays
load longdays

alldays = [idedays lbdays lcdays];

numdays = size(alldays,2);
all_groups = [];
for x = 1 : numdays
    
    
    cd([monkeydir filesep alldays{x} filesep 'session01'])
    
    g = nptdir('group*');
    
    numg = size(g,1);
    
    for ng = 1 : numg
        all_groups = [all_groups str2double(g(ng).name(end-3:end))];
    end
end

grid_counts = zeros(1,64);
for grid = 1 : 64
    grid_counts(grid) = size(find(all_groups == grid),2);
end


cd(monkeydir)

save all_groups all_groups grid_counts