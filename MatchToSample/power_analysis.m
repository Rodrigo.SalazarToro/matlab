function [all_freq_peaks, fp, m, frequencies, alpha_band_means, beta_band_means, gamma_band_means] = power_analysis(varargin)

%run at session level
%computes mean power for each channel for 7 different time windows using detrended lfp sampled at 1KHz.
%fix
%sample
%delay400
%delay800
%delay
%delaymatch
%fulltrial

Args = struct('ml',0,'nlynx',0,'allchannels',0,'ff',0,'pp',0,'bettynlynx',0);
Args.flags = {'ml','nlynx','allchannels','ff','pp','bettynlynx'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml || Args.nlynx
    if Args.nlynx
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

if Args.ml
    if Args.nlynx
        cd(sesdir)
        g = bmf_groups;
        N = NeuronalHist('bmf');
        if Args.allchannels
            ch = [1 : size(N.gridPos,2)];
        else
            [~,ch] = intersect(N.gridPos,g);
        end
    elseif Args.bettynlynx
        N = NeuronalHist('ml','bettynlynx');
        noisy = noisy_groups;
        g = sorted_groups('bettynlynx');
        [~,ii] = intersect(g,noisy);
        g(ii) = []; %get rid of noisy groups
        
        if Args.ff
            [~,ch] = intersect(N.gridPos,g(find(g < 33)));
        elseif Args.pp
            [~,ch] = intersect(N.gridPos,g(find(g > 32 )));
        else
            [~,ch] = intersect(N.gridPos,g);
        end
        
    else
        
        N = NeuronalHist;
        noisy = noisy_groups;
        g = sorted_groups;
        [~,ii] = intersect(g,noisy);
        g(ii) = []; %get rid of noisy groups
        
        if Args.ff
            [~,ch] = intersect(N.gridPos,g(find(g < 33)));
        elseif Args.pp
            [~,ch] = intersect(N.gridPos,g(find(g > 32 )));
        else
            [~,ch] = intersect(N.gridPos,g);
        end
    end
else
    N = NeuronalChAssign;
    noisy = noisy_groups;
    ch = sorted_groups;
    [~,ii] = intersect(ch,N.groups(noisy));
    ch(ii) = [];
    [~,ii] = intersect(N.groups,ch);
    ch = ii;
    %GET RID OF NOISY CHANNELS
end


chnumb = size(ch,2);

cd([sesdir filesep 'lfp' filesep 'power'])

powertrials = nptDir('*normalized_power.*');
ntrials = size(powertrials,1);

%run trhough 6 of the 7 epochs

%make data matrix
trial_power = cell(chnumb,6);
for nt = identity
    load(powertrials(nt).name)
    for e = 1 : 6 %only run through the first 6 epochs, uneven results for the full trial
        for c = 1 : chnumb
            if nt == identity(1)
                trial_power{c,e} = power.S{e}(ch(c),:);
            else
                trial_power{c,e} = [trial_power{c,e}; power.S{e}(ch(c),:)];
            end
        end
    end
end

m = cellfun(@mean,trial_power,'UniformOutput',0);
% hold on
% for x = 1:size(m,2);plot(power.f_delay',smooth(m{x},3));hold on;end
all_freq_peaks = cell(chnumb,6);
for e = 1 : 6 %only run through the first 6 epochs, uneven results for the full trial
    for c = 1 : chnumb
        
        %interpolate spectrum so that there is one observation per Hz after
        %smoothing
        
        frange = min(power.f{e}) : max(power.f{e});
        
        spec = interp1(power.f{e},m{c,e},frange);
        smoothspec = smooth(spec,3);
        
        f = findpeaks(smoothspec);
% f = findpeaks(m{c,e});
        frqp = frange(f.loc)'
        frequencies{c,e} = frange;
        
        if isempty(all_freq_peaks{c,e})
            all_freq_peaks{c,e} = [];
        end
        all_freq_peaks{c,e} = [all_freq_peaks{c,e} frqp];
        fp{c,e} = frqp;
        
        %find frequency range
        arange = [find(power.f{e} >= 8 & power.f{e} <= 13)];
        brange = [find(power.f{e} >= 14 & power.f{e} <= 30)];
        grange = [find(power.f{e} >= 31 & power.f{e} <= 42)];
        
        alpha_band_means{c,e} = mean(m{c,e}(arange));
        beta_band_means{c,e} = mean(m{c,e}(brange));
        gamma_band_means{c,e} = mean(m{c,e}(grange));
        
%         plot(frequencies{c,e},smooth(m{c,e}))
%         pause
%         cla
    end
end




cd(sesdir)

















