function t = GetNLXTrialTimes_betty(events,startNum,stopNum)

startIndex = find(events.Nttls == startNum);
stopIndex = find(events.Nttls == stopNum);

num_trials = size(startIndex,2);

%remove all stops before the first start
stopIndex(stopIndex<startIndex(1))=[];

%get record of correct trials
trial_record = zeros(1,num_trials);

%check to make sure start and stop are the same length
if length(startIndex)~=length(stopIndex)
    error('Not same number of stop and start triggers')
end

t.start = events.TimeStamp(startIndex);
t.stop = events.TimeStamp(stopIndex);
t.trial_record = trial_record;


