function compare_correlation_for_co_in_and_antiphase_channel(varargin)

%run at session level
%computes relative phase angle tass_1998 and lavenquen
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/Volumes/raid/data/monkey/clark/060503/session02')

sesdir = pwd;

mt = mtstrial('auto','redosetNames');
alltrials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1); %IDENTITY
sample_on = floor(mt.data.CueOnset); %lock to sample on

cd([sesdir filesep 'lfp/lfp2'])

%get index of all lfp trials  lfpdata2.name
lfpdata = nptDir('*_lfp2.*');
ntrials = size(lfpdata,1);


all_resp_ent = [];
allchcomb = [];
all_circ_mean = [];
pcounter = 0;
for c1 = 1:8%9
    for c2 = c1+1 : 8%9
        c1,c2
        pcounter = pcounter + 1;
        resp_ent1 = [];
        mp = [];
        for t = 1: ntrials %alltrials
            %lowpass
            hdata = load(lfpdata(t).name);
            %get data ranges
            %     datarange = [sample_on(t) - 499 : size(hdata.data,2)];
% %             datarange = [sample_on(t) + 800 : sample_on(t) + 1300];
                datarange = [(sample_on(t) -499) : sample_on(t)];
% %                 datarange = [sample_on(t) : (sample_on(t)+499)];
            data = hdata.data(:,datarange);
            
            data = data'; %need to do this before and after using hilbert, don't include during the transform
            %compute hilbert transform
            data = hilbert(data);
            
            %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
            %COMPENT IS FLIPPED
            %find instantaneous phase angles
            data = angle(data);
            data = data';
            
            
            d1 = unwrap(data(c1,:)) ./ (2*pi);
            d2 = unwrap(data(c2,:)) ./ (2*pi);
            
            
            
            %cyclic relative phase
            dacyc = mod(d1 - d2,1);
            
            %%
            
            % bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
            window = 500;
            stepsize = 300;
            nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
            
            bins = linspace(0,1,nbins);
            
            steps = [window/2:stepsize:size(data,2)-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
            nw = size(steps,2);
            
            
            max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
            %get data ready for hist (hist is aparently the most
            %time consuming function, found using profile
            cc = 0;
            allh = zeros(window,nw);
            for ww = steps
                cc = cc + 1;
                allh(:,cc) = dacyc(ww-((window/2)-1):ww+(window/2));%calculate response distribution
            end
            
            allhist = hist(allh,bins) ./ window; %calculate response distribution
            re  = -1 * nansum(allhist.*log2(allhist));
            resp_ent1 = [resp_ent1 (max_ent - re) ./ max_ent];
            mp = [mp circ_mean((allh * (2*pi)))];
        end
        all_resp_ent(pcounter,:) = resp_ent1;
        all_circ_mean(pcounter) = circ_mean(mp');
        allchcomb(pcounter,:) = [c1 c2]; 
    end
end

for x = 1:28;scatter(smooth(mt.data.BehResp,25),smooth((log(all_resp_ent(x,:))),25),'r','fill','s');x,axis square;lsline;pause;cla;end

allr = [];allp = [];for x = 1:28;[r p] = corrcoef(smooth(mt.data.BehResp,25),smooth(all_resp_ent(x,:),25));allr(x) = r(1,2);allp(x) = p(1,2);end









