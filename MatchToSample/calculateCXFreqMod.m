function calculateCXFreqMod(days,varargin)

Args = struct('redo',0,'ML',0,'parloopIn',0,'addName',[],'fromCohInterObj',[],'sind',[],'PFCAmplPPCphi',0);
Args.flags = {'ML','redo','parloopIn','PFCAmplPPCphi'};
[Args,modvarargin] = getOptArgs(varargin,Args);


odir = pwd;

if ~isempty(Args.fromCohInterObj)
    
    for ii = 1:length(Args.sind)
        cd(Args.fromCohInterObj.data.setNames{Args.sind(ii)})
        if ~isempty(strfind(Args.fromCohInterObj.data.setNames{Args.sind(ii)},'clark'))
            load rules
            cd(['session0' num2str(find(r==1)+1)])
            mts = mtstrial('auto','redosetNames');
            trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',1);
        else
            cd session01
            mts = mtstrial('auto','redosetNames');
            trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',1,'ML');
        end
        NeuroInfo = NeuronalChAssign;
        if Args.PFCAmplPPCphi
            % amplitude for first channel and phase for second channel in PhaseAmplMTS
            thegr1 = find(Args.fromCohInterObj.data.Index(Args.sind(ii),11) == NeuroInfo.groups); % frontal channel
            thegr2 = find(Args.fromCohInterObj.data.Index(Args.sind(ii),10) == NeuroInfo.groups); % parietal channel
            
        else
           % amplitude for first channel and phase for second channel in PhaseAmplMTS
            thegr1 = find(Args.fromCohInterObj.data.Index(Args.sind(ii),10) == NeuroInfo.groups); % parietal channel
            thegr2 = find(Args.fromCohInterObj.data.Index(Args.sind(ii),11) == NeuroInfo.groups); % frontal channel 
        end
        [data,~] = lfpPcut(trials,[thegr1 thegr2],'fromFiles');
        [m_normt,m_norm_lengtht,m_norm_phaset,f] = PhaseAmplMTS(data{1}(:,:,[1 2]),modvarargin{:});
        cd cxFreqMod
        nfile = sprintf('cxFreqModg%04gg%04g%s.mat',Args.fromCohInterObj.data.Index(Args.sind(ii),10),Args.fromCohInterObj.data.Index(Args.sind(ii),11),Args.addName);
        if Args.redo || isempty(nptDir(nfile))
            iterateEpochs(nfile,m_normt,m_norm_lengtht,m_norm_phaset,1,data,[1 2],NeuroInfo,modvarargin{:})
        end
    end
    cd(odir)
else
    for d = 1 : length(days)
        cd(days{d})
        
        if Args.ML
            cd session01
        else
            load rules
            cd(['session0' num2str(find(r==1)+1)])
        end
        mts = mtstrial('auto','redosetNames');
        [NeuroInfo] = NeuronalChAssign;
        mkdir cxFreqMod
        if Args.ML
            trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',1,'ML');
        else
            trials = mtsgetTrials(mts,'stable','BehResp',1,'rule',1);
        end
        try
            [data,~] = lfpPcut(trials,1:length(NeuroInfo.groups),'fromFiles');
        catch
            lfpPcut([1:mts.data.numSets],1:length(NeuroInfo.groups),'savelfpPcut');
            [data,~] = lfpPcut(trials,1:length(NeuroInfo.groups),'fromFiles');
            cd ..
        end
        if length(NeuroInfo.groups) == size(data{1},3)
            CHcomb = nchoosek([1:size(data{1},3)],2);
            [m_normt,m_norm_lengtht,m_norm_phaset,f] = PhaseAmplMTS(data{1}(:,:,[CHcomb(1,1) CHcomb(1,2)]),modvarargin{:});
            cd cxFreqMod
            if Args.parloopIn
                parfor cm = 1 : size(CHcomb,1);
                    nfile = sprintf('cxFreqModg%04gg%04g%s.mat',NeuroInfo.groups(CHcomb(cm,1)),NeuroInfo.groups(CHcomb(cm,2)),Args.addName);
                    if Args.redo || isempty(nptDir(nfile))
                        iterateEpochs(nfile,m_normt,m_norm_lengtht,m_norm_phaset,cm,data,CHcomb,NeuroInfo,modvarargin{:})
                    end
                end
            else
                for cm = randperm(size(CHcomb,1))
                    nfile = sprintf('cxFreqModg%04gg%04g%s.mat',NeuroInfo.groups(CHcomb(cm,1)),NeuroInfo.groups(CHcomb(cm,2)),Args.addName);
                    if Args.redo || isempty(nptDir(nfile))
                        iterateEpochs(nfile,m_normt,m_norm_lengtht,m_norm_phaset,cm,data,CHcomb,NeuroInfo,modvarargin{:})
                    end
                end
            end
        else
            
            display('error; mismatch number of channels')
        end
        cd(odir)
    end
end
end

%%
function iterateEpochs(nfile,m_normt,m_norm_lengtht,m_norm_phaset,cm,data,CHcomb,NeuroInfo,varargin)


m_norm = zeros(4,size(m_normt,1),size(m_normt,2));
surrogate = zeros(4,size(m_norm_lengtht,1),size(m_norm_lengtht,2),size(m_norm_lengtht,3));
threshold = zeros(4,size(m_norm_phaset,1),size(m_norm_phaset,2),size(m_norm_phaset,3));

for ep = 1 : 4
    %     [m_normAP(ep,:,:),m_norm_lengthAP(ep,:,:),m_norm_phaseAP(ep,:,:),f,m_rawAP(ep,:,:)] = PhaseAmplMTS(data{ep}(:,:,[CHcomb(cm,1) CHcomb(cm,2)]));
    %     [~,m_normAA(ep,:,:),~,f,m_rawAA(ep,:,:)] = PhaseAmplMTS(data{ep}(:,:,[CHcomb(cm,1) CHcomb(cm,2)]),'onlyAmplAmpl');
    [m_norm(ep,:,:),surrogate(ep,:,:,:),threshold(ep,:,:,:),f] = PhaseAmplMTS(data{ep}(:,:,[CHcomb(cm,1) CHcomb(cm,2)]),varargin{:});
end

save(nfile,'m_norm','surrogate','threshold','f');
display([pwd '/' nfile])
end