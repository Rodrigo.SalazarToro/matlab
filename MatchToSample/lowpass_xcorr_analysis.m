function lowpass_xcorr_analysis(varargin)

%run at session level
%filters the data from 8-40hz and saves as lfp2 file

Args = struct('ml',0,'lowpasslow',8,'lowpasshigh',40,'bmf',0);
Args.flags = {'ml','bmf'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

cd('lfp')
lfpdir = pwd;
%get index of all lfp trials  lfpdata.name
lfpdata = nptDir('*_lfp.*');
%make lfp2 directory
mkdir('lfp2')

for x = 1 : size(lfpdata,1)
    data = [];
    cd(sesdir)
    
    %determine which channels are neuronal
    descriptor_file = nptDir('*_descriptor.txt');
    descriptor_info = ReadDescriptor(descriptor_file.name);
    neuronalCh = find(descriptor_info.group ~= 0);
    
    %make trial name
    trial = [sesdir filesep lfpdata(x).name(1:(end-9)) lfpdata(x).name((end-4):end)];
    if Args.ml
        if Args.bmf
            %ethyl = Streamer
            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(trial);
            
        else
            %betty = UEI
            data = ReadUEIFile('FileName',trial);
        end
    else
        %clark = Streamer
        [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(trial);
    end
    %lowpass
    [data]=nptLowPassFilter(data.rawdata(neuronalCh,:),data.samplingRate,Args.lowpasslow,Args.lowpasshigh);
    
    %names the file the same as the previous file with the addition of lfp2
    trial2 = [lfpdata(x).name(1:end-5) '2.' num2strpad(x,4) '.mat'];
    
    cd([lfpdir filesep 'lfp2'])
    save(trial2,'data')
    try 
        load(trial2)
    catch
        clc
        trial2
    end
end
fprintf(1,'Lfp2 directory made.\n')
cd(sesdir)
































