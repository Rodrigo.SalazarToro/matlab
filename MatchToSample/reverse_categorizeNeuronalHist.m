function [lev1 lev2 lev3] = reverse_categorizeNeuronalHist(HistNumbers)

%input HistNumber to get back location

num_hists = size(HistNumbers,2);
for x = 1 : num_hists
    numb = HistNumbers(x);
    % % % % % % % % Prefrontal
    if numb == 1;
        lev1{x} = '6DR';
        lev2{x} = 'f';
        lev3{x} = 'f';
    elseif numb == 2
        lev1{x} = '8AD';
        lev2{x} = 'f';
        lev3{x} = 'f';
    elseif numb == 3
        lev1{x} = '8B';
        lev2{x} = 'i';
        lev3{x} = 'd';
    elseif numb == 4
        lev1{x} = 'dPFC';
        lev2{x} = 'd';
        lev3{x} = 'd';
    elseif numb == 5
        lev1{x} = 'vPFC';
        lev2{x} = 'v';
        lev3{x} = 'v';
    elseif numb == 6
        lev1{x} = 'PS';
        lev2{x} = 'i';
        lev3{x} = 'i';
    elseif numb == 7
        lev1{x} = 'AS';
        lev2{x} = 'i';
        lev3{x} = 'i';
        
        % % % % % % % % %Parietal
    elseif numb == 8
        lev1{x} = 'PEC';
        lev2{x} = 'ms';
        lev3{x} = 'm';
    elseif numb == 9
        lev1{x} = 'PGM';
        lev2{x} = 'i';
        lev3{x} = 'm';
    elseif numb == 10
        lev1{x} = 'PE';
        lev2{x} = 'ms';
        lev3{x} = 'm';
    elseif numb == 11
        lev1{x} = 'PG';
        lev2{x} = 'ls';
        lev3{x} = 'l';
    elseif numb == 12
        lev1{x} = 'MIP';
        lev2{x} = 'md';
        lev3{x} = 'm';
    elseif numb == 13
        lev1{x} = 'LIP';
        lev2{x} = 'ld';
        lev3{x} = 'l';
    elseif numb == 14
        lev1{x} = 'PEcg';
        lev2{x} = 'i';
        lev3{x} = 'm';
    elseif numb == 15
        lev1{x} = 'IPS';
        lev2{x} = 'i';
        lev3{x} = 'i';
    elseif numb == 16 %White Matter
        lev1{x} = 'WM';
        lev2{x} = 'i';
        lev3{x} = 'i';
    elseif numb == 99
        lev1{x} = 'unknown';
        lev2{x} = 'i';
        lev3{x} = 'i';
    else numb == 89
        lev1{x} = 'i';
        lev2{x} = 'i';
        lev3{x} = 'i';
    end
    
end