function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'ml',0);
Args.flags = {'Number','ObjectLevel','ml'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rt = [1 : size(obj.data.Index,1)]';
%     
%     %get only groups that pass the SNR, rejectCH, and POWER criteria
%     if Args.bmf_groups
%         rtemp1 = find(obj.data.Index(:,3) == 1)';
%     else
%         rtemp1 = rt;
%     end
%     rtemp2 = rt;
%     
%     
%     if ~isempty(Args.epoch_sig_coh)
%         ind = (Args.epoch_sig_coh * 4) + (Args.freq_band-1);
%         rtemp3 = find(obj.data.Index(:,ind) >= (obj.data.Index(1,Args.freq_band+35)*(Args.epoch_sig_percent/100)))';
%     else
%         rtemp3 = rt;
%     end
%     
%     varargout{1} = intersect(rtemp1,intersect(rtemp2,rtemp3));
    
    varargout{1} = rt;
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end


