function obj = plot(obj,varargin)

%Arguments
Args = struct('ml',0,'identity',0,'incorrect',0,'tmask',0,'fmask',0,'bettynlynx',0,'bmf',0,'both_rules',0);
Args.flags = {'ml','identity','incorrect','tmask','fmask','bettynlynx','bmf','both_rules'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end

if Args.identity
    load([obj.data.setNames{ind} filesep 'identity' filesep 'powergram' num2strpad(obj.data.Index(ind,1),2)])
elseif Args.incorrect
    load([obj.data.setNames{ind} filesep 'incorrect' filesep 'powergram' num2strpad(obj.data.Index(ind,1),2)])
elseif Args.both_rules
    load([obj.data.setNames{ind} filesep 'identity' filesep 'powergram' num2strpad(obj.data.Index(ind,1),2)])
    ide_sampleoff_locked = sampleoff_locked;
    
    load([obj.data.setNames{ind} filesep 'location' filesep 'powergram' num2strpad(obj.data.Index(ind,1),2)])
    loc_sampleoff_locked = sampleoff_locked;
end

sday = strfind(obj.data.setNames{ind},'/');
ns = size(sday,2);
monkey = obj.data.setNames{ind}(sday(ns-4)+1:sday(ns-3)-1);
day = obj.data.setNames{ind}(sday(ns-3)+1:sday(ns-2)-1);
% ses = obj.data.setNames{ind}(sday(ns-2)+1:sday(ns-1)-1);

if Args.bmf
    area1 = reverse_categorizeNeuronalHistBMF(obj.data.Index(ind,3));
else
    area1 = reverse_categorizeNeuronalHist(obj.data.Index(ind,3));
end

group = obj.data.Index(ind,1);

if Args.identity

        %% sample locked
        subplot(3,1,1)
        
        r = repmat(sampleoff_locked.f,(size(sampleoff_locked.S,1)),1);
        spec = (sampleoff_locked.S .* r)';
        
        if Args.tmask
            spec(:,(30:50)) = 1;
        end
        
        if Args.fmask
            spec(1:2,:) = 1;
        end
        
        imagesc(flipud(spec));
        tp = size(sampleoff_locked.t,2);
        set(gca,'XTick',[1:4:tp])
        set(gca,'XTickLabel',round(sampleoff_locked.t([1:4:end])*1000))
        
        tf = size(sampleoff_locked.f,2);
        set(gca,'YTick',[1:tf])
        set(gca,'YTickLabel',fliplr(round(sampleoff_locked.f([1:end]))))
        
        %sample on
        hold on
        plot([9, 9],[0 17],'color','k')
        %sample off
        hold on
        plot([19, 19],[0 17],'color','k')
        % %earliest match
        hold on
        plot([35, 35],[0 17],'color','k')
        colorbar
        
        title([monkey '  ' day '  area: ' area1{:} '  group: ' num2str(group)])
        
        %% match locked
        subplot(3,1,2)
        r = repmat(match_locked.f,(size(match_locked.S,1)),1);
        spec = (match_locked.S .* r)';
        if Args.tmask
            spec(:,(1:30)) = 1;
        end
        
        if Args.fmask
            spec(1:2,:) = 1;
        end
        imagesc(flipud(spec));
        
        tp = size(match_locked.t,2);
        set(gca,'XTick',[1:4:tp])
        set(gca,'XTickLabel',round(match_locked.t([1:4:end])*1000))
        
        tf = size(match_locked.f,2);
        set(gca,'YTick',[1:tf])
        set(gca,'YTickLabel',fliplr(round(match_locked.f([1:end]))))
        
        %match
        hold on
        plot([19, 19],[0 17],'color','k')
        
        colorbar
        
        %% sacc locked
        subplot(3,1,3)
        r = repmat(firstsacc_locked.f,(size(firstsacc_locked.S,1)),1);
        spec = (firstsacc_locked.S .* r)';
        if Args.tmask
            spec(:,(1:30)) = 1;
        end
        
        if Args.fmask
            spec(1:2,:) = 1;
        end
        
        imagesc(flipud(spec));
        
        tp = size(firstsacc_locked.t,2);
        set(gca,'XTick',[1:4:tp])
        set(gca,'XTickLabel',round(firstsacc_locked.t([1:4:end])*1000))
        
        tf = size(firstsacc_locked.f,2);
        set(gca,'YTick',[1:tf])
        set(gca,'YTickLabel',fliplr(round(firstsacc_locked.f([1:end]))))
        
        %saccade
        hold on
        plot([19, 19],[0 17],'color','k')
        colorbar

elseif Args.incorrect 
    
    %% sample locked
    subplot(3,1,1)

    r = repmat(sampleoff_locked.f,(size(sampleoff_locked.S,1)),1);
    spec = (sampleoff_locked.S .* r)';
    
    if Args.tmask
        spec(:,(1:55)) = 1;
    end
    
    if Args.fmask
        spec(1:2,:) = 1;
    end
    
    imagesc(flipud(spec));
    tp = size(sampleoff_locked.t,2);
    set(gca,'XTick',[1:4:tp])
    set(gca,'XTickLabel',round(sampleoff_locked.t([1:4:end])*1000))
    
    tf = size(sampleoff_locked.f,2);
    set(gca,'YTick',[1:tf])
    set(gca,'YTickLabel',fliplr(round(sampleoff_locked.f([1:end]))))
    
    %sample on
    hold on
    plot([9, 9],[0 17],'color','k')
    %sample off
    hold on
    plot([19, 19],[0 17],'color','k')
    % %earliest match
    hold on
    plot([35, 35],[0 17],'color','k')
    colorbar
    title([monkey '  ' day '  area: ' area1{:} '  group: ' num2str(group)])
    
    
    %% match locked
    subplot(3,1,2)
    r = repmat(match_locked.f,(size(match_locked.S,1)),1);
    spec = (match_locked.S .* r)';
    if Args.tmask
        spec(:,(1:30)) = 1;
    end
    
    if Args.fmask
        spec(1:2,:) = 1;
    end
    imagesc(flipud(spec));
    
    tp = size(match_locked.t,2);
    set(gca,'XTick',[1:4:tp])
    set(gca,'XTickLabel',round(match_locked.t([1:4:end])*1000))
    
    tf = size(match_locked.f,2);
    set(gca,'YTick',[1:tf])
    set(gca,'YTickLabel',fliplr(round(match_locked.f([1:end]))))
    
    %match
    hold on
    plot([19, 19],[0 17],'color','k')
    
    colorbar
    
    %% sacc locked
    subplot(3,1,3)
    r = repmat(firstsacc_locked.f,(size(firstsacc_locked.S,1)),1);
    spec = (firstsacc_locked.S .* r)';
    if Args.tmask
        spec(:,(1:30)) = 1;
    end
    
    if Args.fmask
        spec(1:2,:) = 1;
    end
    
    imagesc(flipud(spec));
    
    tp = size(firstsacc_locked.t,2);
    set(gca,'XTick',[1:4:tp])
    set(gca,'XTickLabel',round(firstsacc_locked.t([1:4:end])*1000))
    
    tf = size(firstsacc_locked.f,2);
    set(gca,'YTick',[1:tf])
    set(gca,'YTickLabel',fliplr(round(firstsacc_locked.f([1:end]))))
    
    %saccade
    hold on
    plot([19, 19],[0 17],'color','k')
    colorbar
    
end

if Args.both_rules
    
     %% sample locked identity
        subplot(2,1,1)
        
        r = repmat(ide_sampleoff_locked.f,(size(ide_sampleoff_locked.S,1)),1);
        spec = (ide_sampleoff_locked.S .* r)';
        
        if Args.tmask
            spec(:,(30:50)) = 1;
        end
        
        if Args.fmask
            spec(1:2,:) = 1;
        end
        
        imagesc(flipud(spec(4:end,1:35)));
        maxide = max(max(spec(4:end,1:35)));
        minide = min(min(spec(4:end,1:35)));
        
        tp = size(ide_sampleoff_locked.t,2);
        set(gca,'XTick',[1:4:tp])
        set(gca,'XTickLabel',round(ide_sampleoff_locked.t([1:4:end])*1000))
        
        tf = size(ide_sampleoff_locked.f,2);
        set(gca,'YTick',[1:tf])
        set(gca,'YTickLabel',fliplr(round(ide_sampleoff_locked.f([1:end]))))
        
        %sample on
        hold on
        plot([9, 9],[0 17],'color','k')
        %sample off
        hold on
        plot([19, 19],[0 17],'color','k')
        % %earliest match
        hold on
        plot([35, 35],[0 17],'color','k')
        colorbar
        
        title([monkey '  ' day '  area: ' area1{:} '  group: ' num2str(group)])
        
         %% sample locked location
        subplot(2,1,2)
        
        r = repmat(loc_sampleoff_locked.f,(size(loc_sampleoff_locked.S,1)),1);
        spec = (loc_sampleoff_locked.S .* r)';
        
        if Args.tmask
            spec(:,(30:50)) = 1;
        end
        
        if Args.fmask
            spec(1:2,:) = 1;
        end
        
        imagesc(flipud(spec(4:end,1:35)));
        
        maxloc = max(max(spec(4:end,1:35)));
        minloc = min(min(spec(4:end,1:35)));
        
        tp = size(loc_sampleoff_locked.t,2);
        set(gca,'XTick',[1:4:tp])
        set(gca,'XTickLabel',round(loc_sampleoff_locked.t([1:4:end])*1000))
        
        tf = size(loc_sampleoff_locked.f,2);
        set(gca,'YTick',[1:tf])
        set(gca,'YTickLabel',fliplr(round(loc_sampleoff_locked.f([1:end]))))
        
        %sample on
        hold on
        plot([9, 9],[0 17],'color','k')
        %sample off
        hold on
        plot([19, 19],[0 17],'color','k')
        % %earliest match
        hold on
        plot([35, 35],[0 17],'color','k')
        colorbar
        
        title([monkey '  ' day '  area: ' area1{:} '  group: ' num2str(group)])
        
        if maxloc > maxide
            mmax = maxloc;
        else
            mmax = maxide;
        end
        
        if minloc < minide
            mmin = minloc;
        else
            mmin = minide;
        end
        
        subplot(2,1,1)
        caxis([mmin mmax])
        subplot(2,1,2)
        caxis([mmin mmax])
        
end

