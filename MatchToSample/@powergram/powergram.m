function obj = powergram(varargin)


Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'bmf',0,'bettynlynx',0);
Args.flags = {'Auto','ml','bmf','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'powergram';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'powergram';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)
sesdir = pwd;

identity = [];
%make mts trial object
if Args.ml
    if Args.bmf
        mtst = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        if ~isempty(mtst)
            identity = mtsgetTrials(mtst,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        end
    else
        mtst = mtstrial('auto','ML','RTfromML','redosetNames','redo');
        if ~isempty(mtst)
            identity = mtsgetTrials(mtst,'BehResp',1,'stable','ML','rule',1); %correct/identity
        end
    end
else
    mtst = mtstrial('auto','redosetNames');
    if ~isempty(mtst)
        identity = mtsgetTrials(mtst,'BehResp',1,'stable','rule',1); %correct/identity
    end
end


if ~isempty(identity) && exist([sesdir filesep 'lfp' filesep 'lfp2'])
    
    if Args.ml
        if Args.bmf
            N = NeuronalHist('ml','bmf');
            [sortedgroups,sortedpairs,~,chpairlist] = bmf_groups;
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
            nch = size(chgroups,2);
        elseif Args.bettynlynx
            N = NeuronalHist('ml','bettynlynx');
            [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml','bettynlynx');
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
            nch = size(chgroups,2);
        else
            N = NeuronalHist('ml');
            [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml');
            noisy = noisy_groups;
            
            [i ii] = intersect(sortedgroups,noisy);
            sortedgroups(ii) = [];
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
            nch = size(chgroups,2);
        end
    else
        N = NeuronalHist;
        Nch = NeuronalChAssign;
        [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
        noisy = noisy_groups;
        
        [i ii] = intersect(sortedgroups,Nch.groups(noisy));
        sortedgroups(ii) = [];
        [~,chgroups] = intersect(Nch.groups,sortedgroups);
        nch = size(chgroups,2);
    end
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
   
    for f = 1 : nch
        data.setNames{f,1} = ([lfp2dir]);
        
        if Args.ml
            data.Index(f,3) = N.number(chgroups(f)); %this is the number for the specific area
        else
        end
        
        data.Index(f,1) = sortedgroups(f); %group
        
      
        data.numSets = f;
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    end
    cd(sesdir)
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
