function obj = plot(obj,varargin)
%   @mtslfptraces/plot plots by default the first raw lfp trace
%   Other option are :
%
% - spectrogram: plots a spectrogram from the trace. By default,
%   it uses the multi taper method. If this option is followed
%   by 'fft', it will use the traditional fast Fourier instead.
%   The parameters 'window' (512 ms),'noverlap'(502 ms),'nfft' (512),
%   'NW' (2),'k' (3) can be specified. Defaults are shown in parentheses.
%   Another option is 'normalized', which will transform the
%   spectrogram in a Z-score (the mean and standard deviation are
%   calculated from a period that can be specified by the arguments
%   'bstart' and 'bend', which are by default 1 and 300 ms, respectively).
%   Finally, the colorscale (default [-50 50 ) can be specified with
%   'colorscale'.
%
% - TrialTraces: plots all the traces of the session. This option
%   should be followed by a selection criteria (see the get fct).
%
%
% - 'SacAlign' will align all the data according to the onset of the
%   saccade and can be combined with the arguments above.
%
%   Dpendencies: alignData, spectrogram, mtspecgramc
%
%

Args = struct('fft',0,'spectrogram',0,'cohgram',0,'phase',0,'TrialTraces',0,'normalized',0,'offset',[],'colorscale',[],'bstart',1,'bend',300,'window',512,'noverlap',502,'nfft',512,'Flimit',100,'NW',2,'k',3);
Args.flags = {'spectrogram','cohgram','TrialTraces','SacAlign','fft','normalized','phase'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'spectrogram','fft','normalized'});

[numevents,dataindices,Mark] = get(obj,'Number',varargin2{:});


step = (Args.window - Args.noverlap);
params.tapers = [Args.NW Args.k];

bstart = round(Args.bstart/step) + 1;
bend = round(Args.bend/step) -1;
SR = unique(obj.data.SamplingRate);
sampling_cor = SR/1000;
if Args.TrialTraces
    if (~isempty(Args.NumericArguments))
        n = Args.NumericArguments{1};
        ind  = find(dataindices(:,2) == n);
        limit = dataindices(ind,4);
    else
        n = numevents;
        limit = dataindices(:,1);
    end

    b = unique(obj.data.CueOnset(limit))* sampling_cor;

    
    params.Fs = SR;

    if ~isempty(ind)
        %         [traces,error] = alignData(obj.data.neural(limit,:),events,'before',b,'maxpoints',SR*2.9);
        [pdir,cdir] = getDataDirs('highpass','relative','CDNow');
        file = nptDir(sprintf('*.%04g',unique(dataindices(ind,2))));
        [allelec,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(file.name);
        rch = mod(limit,num_channels);
        i = find(rch == 0);
        rch(i) = num_channels;
        traces = allelec(rch,:);
        
        if Args.spectrogram | Args.cohgram

            maxxlim = [];
            maxylim = [];
            maxzlim = [];
            for i = 1 : length(limit)

                if Args.fft
                    if Args.spectrogram
                        [S,F,T,P{i}] = spectrogram(traces(i,~isnan(traces(i,:))),Args.window,Args.noverlap,Args.nfft,SR);

                    elseif Args.cohgram

                        [P{1},Ang{1},F,T] = cohgram(traces(1,~isnan(traces(1,:)))',traces(2,~isnan(traces(2,:)))',Args.nfft,SR,Args.window,Args.noverlap);
                        if Args.phase
                            P{1} = Ang{1};
                        end
                        i = 1;
                    end


                else
                    if Args.spectrogram
                        [S,T,F] = mtspecgramc(traces(i,~isnan(traces(i,:)))',[Args.window/SR step/SR],params);
                        P{i} = S';
                        F = F';
                        T = T';
                    elseif Args.cohgram
                        [S,A,S12,S1,S2,T,F] = cohgramc(traces(1,~isnan(traces(1,:)))',traces(2,~isnan(traces(2,:)))',[Args.window/SR step/SR],params);
                        P{1} = S';
                        Ang{1} = A';
                        if Args.phase
                            P{1} = Ang{1};
                        end
                        i = 1;
                    end
                end



                if Args.normalized
                    baseline = std(P{i}(:,bstart:bend),0,2);
                    baselinem = mean(P{i}(:,bstart:bend),2);
                    P{i} = (P{i} - (repmat(baselinem,1,size(P{i},2))))./(repmat(baseline,1,size(P{i},2)));
                end
                I = find(F <= Args.Flimit);
                maxxlim = max([maxxlim size(P{i},2)]);
                maxylim = max([maxylim length(I)]);
                maxzlim = max([maxzlim max(max(P{i}))]);
            end

            allP = nan(length(limit) * maxylim,maxxlim);
            ytick = [];
            for i = 1 : length(limit)
                if Args.cohgram
                    i = 1;
                    ytick = [];
                    allP = nan(maxylim,maxxlim);
                end
                xt = size(P{i},2);
                allP((i-1)* maxylim + 1 : (i-1)* maxylim + maxylim, 1 : xt) = P{end-i+1}(1: maxylim,1:xt);
                ytick = [ytick; I([1 end])+(i-1)*maxylim];
            end

            T = T * SR; % to have th evalues in ms
            if isempty(Args.colorscale)
                imagesc(allP)
            else
                imagesc(allP,Args.colorscale)
            end
            hold on
            hax = gca;

            set(hax,'YLim',[0.5 size(allP,1)+0.5]);

            set(hax,'YTick',ytick);

            set(hax,'Xtick',[1:floor(length(T)/10):length(T)]);
            if Args.cohgram
                set(hax,'YTickLabel',repmat(round(F(I([1 end]))),1,1));
            else
                set(hax,'YTickLabel',repmat(round(F(I([1 end]))),length(limit),1));
            end

            set(hax,'XtickLabel',T(1:floor(length(T)/10):end));
            ylabel('Frequency [Hz]');
            xlabel('Time [ms]');
            range = maxylim;
            xxlim = maxxlim;


        else
            maxlim = max(max(traces));
            minlim = min(min(traces));
            if isempty(Args.offset)
                range = 2*max([maxlim abs(minlim)]);
            else
                range = Args.offset;
            end
            nnan = sum(~isnan(traces),1);
            xxlim = find(nnan == 0);
            if ~isempty(xxlim)
                xxlim = xxlim;
            else
                xxlim = size(traces,2);
            end
        end


        x = [0 : (xxlim(1)-1) ];
        
        RT = round(obj.data.FirstSac(limit(1)) + obj.data.MatchOnset(limit(1))) * sampling_cor;
        %         cueonset = round(obj.data.CueOnset(limit(1)));
        cueoffset = round(obj.data.CueOffset(limit(1)))* sampling_cor;
        match = round(obj.data.MatchOnset(limit(1)))* sampling_cor;
        if ~Args.spectrogram & ~Args.cohgram
            for i = 1 : length(limit)
                plot(x,traces(i,1:xxlim(1)) + (i-1) * range);
                hold on
                text(x(end), (i-1) * range,sprintf('ch %d',obj.data.Nchannels(i)))
            end
            hax = gca;
            hrange = ceil(range/2);
            set(hax,'YLim',[-hrange range*(length(limit)-1) + hrange]);

            set(hax,'YTick',[-hrange : hrange: range*(length(limit)-1) + hrange]);
            set(hax,'YTickLabel',repmat([-hrange;0],length(limit),1));
        end


        allrange = ceil(-range/2) : floor(length(limit)*range);

        if Args.spectrogram | Args.cohgram
            b = ceil((b - Args.window/2) / (Args.window - Args.noverlap)) + 1;
            cueoffset = ceil((cueoffset - Args.window/2) / (Args.window - Args.noverlap)) + 1;
            RT = ceil((RT - Args.window/2) / (Args.window - Args.noverlap)) + 1;
            match = ceil((match - Args.window/2) / (Args.window - Args.noverlap)) + 1;


            allrange = [1 : size(allP,1)];

        end

        plot(repmat(b,1,length(allrange)),allrange,'k')
        plot(repmat(cueoffset,1,length(allrange)),allrange,'k-.')
        plot(repmat(match,1,length(allrange)),allrange,'m')
        plot(repmat(RT,1,length(allrange)),allrange,'m-.')

        hold off
    else
        plot(NaN)
    end
    fprintf('%s \n',obj.data.setNames{unique(dataindices(ind,1))})
    title(sprintf('trial %d; Beh. resp %d; Cue pos. %d; Cue obj. %d; Match pos %d %d; Match obj %d %d',unique(dataindices(ind,3)),obj.data.BehResp(limit(1)),obj.data.CueLoc(limit(1)),obj.data.CueObj(limit(1)),obj.data.MatchPos1(limit(1)),obj.data.MatchPos2(limit(1)),obj.data.MatchObj1(limit(1)),obj.data.MatchObj2(limit(1))));
else

    if (~isempty(Args.NumericArguments))
        n = Args.NumericArguments{1};
        ind  =  n;
        limit = dataindices(ind,4);
    else
        n = numevents;
        limit = dataindices(:,1);
    end

    if length(limit) == 1;
         [pdir,cdir] = getDataDirs('highpass','relative','CDNow');
        file = nptDir(sprintf('*.%04g',unique(dataindices(ind,2))));
        [allelec,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(file.name);
        rch = mod(limit,num_channels);
        i = find(rch == 0);
        rch(i) = num_channels;
        trace = allelec(rch,:);
%         trace = double(obj.data.neural(limit,~isnan(obj.data.neural(limit,:))));
        

        if Args.spectrogram
            SR = unique(obj.data.SamplingRate(dataindices(ind,1)));
            params.Fs = SR;

            if Args.fft
                [S,F,T,P] = spectrogram(trace,Args.window,Args.noverlap,Args.nfft,SR);

            else
                [S,T,F] = mtspecgramc(trace',[Args.window/SR step/SR],params);
                P = S';
                F = F';
                T = T';
            end
            if Args.normalized
                baseline = std(P(:,bstart:bend),0,2);
                P = P./(repmat(baseline,1,size(P,2)));
                baseline = std(P(:,bstart:bend),0,2);
                baselinem = mean(P(:,bstart:bend),2);
                P = (P - (repmat(baselinem,1,size(P,2))))./(repmat(baseline,1,size(P,2)));
            end
            T = T * SR; % to have th evalues in ms
            if isempty(Args.colorscale)
                imagesc(P)
            else
                imagesc(P,Args.colorscale);
            end
            hax = gca;
            I = find(F <= Args.Flimit);
            set(hax,'YLim',[0.5 I(end)+0.5]);
            set(hax,'YTick',I(1:10:end));

            set(hax,'Xtick',[1:10:length(T)]);

            set(hax,'YTickLabel',F(I(1:10:end)));
            set(hax,'XtickLabel',T(1:10:end));
            ylabel('Frequency [Hz]');
            xlabel('Time [ms]');
            yrange = I(1 : 10 :end);
            hold on
            sac = ceil((obj.data.FirstSac(limit) + obj.data.MatchOnset(limit) - Args.window/2)/step)* sampling_cor;
            cueon = ceil((obj.data.CueOnset(limit)- Args.window/2)/step)* sampling_cor;
            cueoff = ceil((obj.data.CueOffset(limit)- Args.window/2)/step)* sampling_cor;
            match = ceil((obj.data.MatchOnset(limit)- Args.window/2)/step)* sampling_cor;
            plot(repmat(sac,1,length(yrange)),yrange,'m-.');
            plot(repmat(cueon,1,length(yrange)),yrange,'k');
            plot(repmat(cueoff,1,length(yrange)),yrange,'k-.');
            plot(repmat(match,1,length(yrange)),yrange,'m');
            %             axis([Args.window/2-step/2 (length(trace)-1-Args.window/2-step/2) -500 500])
            hold off
        else
            %             nnan = ~isnan(obj.data.neural(limit,:));
            %             xxlim = find(nnan == 0);
            %             x = [0 : xxlim(1) - 1];

            %             plot(x,obj.data.neural(limit,1:xxlim(1)));
            plot(trace)
            hold on
            thech = mod(obj.data.Index(limit,6),length(obj.data.Nchannels));
            if thech == 0
                thech = length(obj.data.Nchannels);
            end
            text(length(trace), 0,sprintf('ch %d',obj.data.Nchannels(thech)))
            xlabel('Time [ms]')
            ylabel('Voltage [uV]')
%             yrange = [ceil(min(obj.data.neural(limit,:))) : floor(max(obj.data.neural(limit,:)))];
            yrange = [ceil(min(trace)) : floor(max(trace))];
            plot(repmat((obj.data.FirstSac(limit) + obj.data.MatchOnset(limit))* sampling_cor,1,length(yrange)),yrange,'m-.');
            plot(repmat(obj.data.MatchOnset(limit)* sampling_cor,1,length(yrange)),yrange,'m');
            plot(repmat(obj.data.CueOnset(limit)* sampling_cor,1,length(yrange)),yrange,'k');
            plot(repmat(obj.data.CueOffset(limit)* sampling_cor,1,length(yrange)),yrange,'k-.');

            hold off
        end

        %         title(sprintf('%s/trial %d',obj.data.setNames{dataindices(ind,1)},dataindices(ind,2)));
        fprintf('%s \n',obj.data.setNames{unique(dataindices(ind,1))})
        title(sprintf('trial %d; Beh. resp %d; Cue pos. %d; Cue obj. %d; Match pos %d %d; Match obj %d %d',unique(dataindices(ind,3)),obj.data.BehResp(limit(1)),obj.data.CueLoc(limit(1)),obj.data.CueObj(limit(1)),obj.data.MatchPos1(limit(1)),obj.data.MatchPos2(limit(1)),obj.data.MatchObj1(limit(1)),obj.data.MatchObj2(limit(1))));
    else
        fprintf('Warning!! The @lfptraces/get selected more than one trace \n');
    end
end