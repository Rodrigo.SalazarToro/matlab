function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
	% check if second input is the right kind of object
	if(~isa(q,classname))
		% both inputs are not the right kind of object so create empty
		% object and return it
		r = feval(classname);
	else
		% second input is the right kind of object so return that
		r = q;
	end
else
	if(~isa(q,classname))
		% p is the right kind of object but q is not so just return p
		r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
	else
		% both p and q are the right kind of objects so add them 
		% together
		% assign p to r so that we can be sure we are returning the right
		% object
		r = p;
%         r.data.neural = [p.data.neural; q.data.neural];
		r.data.CueObj = [p.data.CueObj; q.data.CueObj];
		r.data.CueLoc = [p.data.CueLoc; q.data.CueLoc];
		r.data.MatchObj1 = [p.data.MatchObj1; q.data.MatchObj1];
		r.data.MatchPos1 = [p.data.MatchPos1; q.data.MatchPos1];
		r.data.MatchObj2 = [p.data.MatchObj2; q.data.MatchObj2]; 
		r.data.MatchPos2 = [p.data.MatchPos2; q.data.MatchPos2];
		r.data.CueOnset = [p.data.CueOnset; q.data.CueOnset];
		r.data.CueOffset = [p.data.CueOffset; q.data.CueOffset];
		r.data.MatchOnset = [p.data.MatchOnset; q.data.MatchOnset];
		r.data.BehResp = [p.data.BehResp; q.data.BehResp];
        r.data.FirstSac = [p.data.FirstSac; q.data.FirstSac];
		r.data.LastSac = [p.data.LastSac; q.data.LastSac];
        r.data.NumSac = [p.data.NumSac; q.data.NumSac];
%         r.data.Type = [p.data.Type; q.data.Type];
        
		q.data.Index(:,3) = q.data.Index(:,3) + p.data.Index(end,3);
        q.data.Index(:,2) = q.data.Index(:,2) + p.data.Index(end,2);
        q.data.Index(:,6) = q.data.Index(:,6) + p.data.Index(end,6);
		r.data.Index = [p.data.Index; q.data.Index];
        
		r.data.SamplingRate = [p.data.SamplingRate; q.data.SamplingRate];
        
        r.data.Nchannels = [p.data.Nchannels; q.data.Nchannels];
%         r.data.Type = [p.data.Type; q.data.Type];
        
		
		% useful fields for most objects
		r.data.numSets = p.data.numSets + q.data.numSets;
		r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};
		
		% add nptdata objects as well
		r.nptdata = plus(p.nptdata,q.nptdata);
	end
end
