function make_stimulus_avg_ccg(varargin)

%used to make average ccg plots for each stimulus
%run at day level

%Args.rules: [1] identity, [1,2] identity and location
%Args.mts_cpp used to indicate that function is being called from mtsCPP/plot
Args = struct('ml',0,'pair',[],'plot',0,'sessions',[1],'rules',[1],'mts_cpp',0,'sorted',0);
Args.flags = {'ml','plot','mts_cpp','sorted'};
[Args,modvarargin] = getOptArgs(varargin,Args);
rules = [{'identity' 'location'}];
daydir = pwd;

if ~Args.ml
    r=1;
else
    r = Args.rules; %[1], ide only, [1,2] runs both rules
end

ses = nptDir('session0*');
for sessions = Args.sessions
    if ~Args.mts_cpp
        cd ([daydir filesep ses(sessions).name]);
    end
    
    if Args.ml
        [~,~,all_pairs,~] = sorted_groups('ml');
    else
        [~,~,~,all_pairs] = sorted_groups();
    end
    ses_dir = pwd;
    cd([ses_dir filesep ('lfp')]);
    

    cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
    
    written = ~isempty(nptDir('stimulus_avg_ccg.mat'));
    num_cp = size(all_pairs,1);
    
    if isempty(Args.pair)
        cpairs = [1:num_cp];
    else
        cpairs = Args.pair;
    end
    
    im = [];
    cd(ses_dir)
    if ~Args.plot
        if Args.ml
            mt = mtstrial('auto','ML','RTfromML','redosetNames');
        else
            mt = mtstrial('auto','redosetNames');
        end
        objs =  unique(mt.data.CueObj);
        locs =  unique(mt.data.CueLoc);
        if Args.ml
            c = mtscpp('Auto','ml','ide_only');
        else
            c = mtscpp('Auto');
        end
        
        if Args.sorted
            [~, cpairs] = get(c,'Number','sorted'); %get list of sorted pairs
        else
            [~, cpairs] = get(c,'Number');
        end
        
        
        
        
        
        for rr = r
            for all_cp = cpairs
                max_val = 0;
                size(cpairs,2) - all_cp
                stim = 0;
                for objects = 1 : 3
                    for locations = 1 : 3
                        cumulative_xcorr = [];
                        stim = stim + 1;
                        cd(ses_dir)
                        %check for bhv file
                        if Args.ml
                            ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations),'ml','rule',rr);
                        else
                            ind = mtsgetTrials(mt,'BehResp',1,'stable','CueObj',objs(objects),'CueLoc',locs(locations),'rule',rr);
                        end
                        trials(stim) = size(ind,2);
                        cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
                        if Args.ml
                            load(['channelpair' num2strpad(all_pairs(all_cp,1),2) num2strpad(all_pairs(all_cp,2),2)]);
                        else
                            load(['channelpair' num2strpad(all_pairs(all_cp,1),2) num2strpad(all_pairs(all_cp,2),2)]);
                        end
                        
                        if ~isempty(ind)
                        %find size of smallest correlograms
                        smallest = inf;
                        for xx = ind
                            s = size(correlograms{xx},1);
                            if s < smallest
                                smallest = s;
                            end
                        end
                        
                        cumulative_xcorr = correlograms{ind(1)}((1:smallest),:);
                        ind = ind(2:end);
                        for x = ind
                            cumulative_xcorr = cumulative_xcorr + correlograms{x}((1:smallest),:);
                        end
                        cx = cumulative_xcorr' ./ (size(ind,2) + 1);
                        im{stim} = single(cx); %save a single precision
                        if max_val < max(max(abs(cx)))
                            max_val = max(max(abs(cx)));
                        end
                        else
                            im{stim} = []; %save a single precision
                            
                        end
                    end
                end
                stimulus_avg_ccg{1,all_cp} = im;
                stimulus_avg_ccg{2,all_cp} = single(trials);
                stimulus_avg_ccg{3,all_cp} = single(max_val);
                
                
                
                ind = mtsgetTrials(mt,'BehResp',1,'stable','rule',rr);
                sm = min(cellfun('size',correlograms,1));
                cumulative_xcorr = zeros(sm,101);
                for x = ind
                    cumulative_xcorr = cumulative_xcorr + correlograms{x}((1:sm),:);
                end
                stimulus_avg_ccg{4,all_cp} = single(cumulative_xcorr' ./ (size(ind,2)));
            end
            if Args.ml
                cd ([ses_dir filesep ('lfp') filesep ('lfp2') filesep (rules{rr})]);
                save stimulus_avg_ccg stimulus_avg_ccg
            else
                cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
                save stimulus_avg_ccg stimulus_avg_ccg
            end
        end
    else
        if Args.ml
            cd ([ses_dir filesep ('lfp') filesep ('lfp2') filesep (rules{Args.rules})]);
            load stimulus_avg_ccg
        else
            cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
            load stimulus_avg_ccg
        end
        
    end
    
    objs = ([1 1 1 2 2 2 3 3 3]);
    ides = ([1 2 3 1 2 3 1 2 3]);
    if Args.plot 
        cla

        im = stimulus_avg_ccg{1,Args.pair};
        trials = stimulus_avg_ccg{2,Args.pair};
        max_val = stimulus_avg_ccg{3,Args.pair};
        for all_stims = 1:9
            subplot(3,3,all_stims)
            imagesc(im{all_stims},[(-1*max_val) max_val])
% %             imagesc(im{all_stims})
            set(gca,'YTick',[1,25,50,75,100])
            set(gca,'YTicklabel',[-50 -25 0 25 50]);
            
            set(gca,'XTick',[1:7:42])
            set(gca,'XTicklabel',[150:350:2250])
            hold on
            
            %zero phase
            %im{stim}(50,:) = max_val;
            plot([1 43],[50 50],'color','k')
            hold on
            %sample
            plot([8 8],[1 101],'color','k')
            hold on
            %delay
            plot([18 18],[1 101],'color','k')
            hold on
            %first match
            plot([34 34],[1 101],'color','k')
            hold on
            %last match
            plot([42 42],[1 101],'color','k')
            hold on
            colorbar
            title(['obj:',num2str(objs(all_stims)),' loc:',num2str(ides(all_stims)) '  trials=' num2str(trials(all_stims))])
        end
        hold off
    end
    cd(daydir)
end





