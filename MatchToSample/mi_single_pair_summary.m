function [crossings avg_mi mi_no_bias] = mi_single_pair_summary(varargin)


%run in sesdir
Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    c = mtscpp2('auto','ml');
else
    c = mtscpp2('auto');
end

[~,pair_list] = get(c,'Number','pf');

cd('lfp/lfp2')

load mi_single_pair.mat
load mi_single_pair_permutations.mat

crossings = zeros(3,33);
avg_mi = cell(1,3);
mi_no_bias = cell(1,3);
surr = cell(1,3);
all_mi = [];
all_ap = [];
for q = pair_list;
    c = {'r' 'g' 'b'};
    for y = 1:3
        ap=[];
        for x = 1 : size(all_perms,2)
            ap = [ap ; all_perms{x}{q}(y,:)];
        end
        %         plot(pair_mutual_info{q}(y,:) - mean(ap),c{y});
        %         hold on
        %         plot([1:33],prctile(ap,99)-mean(ap),'k');
        %         hold on
        
        
        pp = single(prctile(ap,95));
        
        if isempty(avg_mi{y})
            avg_mi{y} = pair_mutual_info{q}(y,:) - mean(ap);
            mi_no_bias{y} = pair_mutual_info{q}(y,:);
            surr{y} = pp - mean(ap);
        else
            avg_mi{y} = [avg_mi{y}; (pair_mutual_info{q}(y,:) - mean(ap))];
            mi_no_bias{y} = [mi_no_bias{y}; (pair_mutual_info{q}(y,:))];
            surr{y} = [surr{y}; pp - mean(ap)];
        end
        
        
        all_mi = [all_mi; (pair_mutual_info{q}(y,:) - mean(ap))];
        
        
        crossings(y,find(pair_mutual_info{q}(y,:) >= pp)) = crossings(y,find(single(pair_mutual_info{q}(y,:)) >= pp)) + 1;
        
        all_ap = [all_ap; median(ap)./max(median(ap))];
        
    end
end

cd(sesdir)


