function [seldays,ntrials,varargout] = ruleDay(varargin)

Args = struct('combNtrials',140,'notForLFP',0,'days',[],'ML',0,'delayRange',[800 1300],'rule',1);
Args.flags = {'notForLFP','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'combNtrials','notForLFP'});


if isempty(Args.days)
    if Args.ML
        tdays = [nptDir('09*'); nptDir('1*')];
    else
        tdays = [nptDir('06*'); nptDir('07*'); nptDir('08*')];
    end
    for d = 1 : length(tdays); days{d} = tdays(d).name; end
else
    days = Args.days;
end
sd = [];
ntrials = [];
for d = 1 : length(days)
    
    cd(days{d});
    if isempty(nptDir('skip.txt'));
        if Args.ML && ~isempty(nptDir('session01'))
            cd session01
            [status, result] = unix('find . -name ''skip.txt''');
            if isempty(result);
                mts = mtstrial('auto');
                delay = mts.data.MatchOnset - mts.data.CueOffset;
                if min(delay) > Args.delayRange(1) && max(delay) < Args.delayRange(2)
                    swblock = [0; find(diff(mts.data.Index(:,1)) ~= 0)];
                    nblock = length(swblock);
                    crosst = [];
                    for nb = 1 : nblock
                        
                        btrials = mtsgetTrials(mts,'block',nb);
                        ftrials = mtsgetTrials(mts,'block',nb,'CueObj',55);
                        trials = setdiff(btrials,ftrials);
                        if  unique(mts.data.Index(trials,1)) == Args.rule
                            response = mts.data.BehResp(trials);
                            [perf,ti,st] = getperformance('getThreshCross','response',response,modvarargin{:});
                            crosst = [crosst st + swblock(nb)];
                        end
                    end
                    nv = [];for pe = 1 : size(crosst,2); nv = [nv crosst(1,pe) : crosst(2,pe)]; end
                    if sum(mts.data.BehResp(nv)) >= Args.combNtrials
                        sd = [sd d];
                        ntrials = [ntrials sum(mts.data.BehResp(nv))];
                    end
                end
                
            end
            cd ..
        else
            sessions = nptDir('session0*');
            
            for s = 2 : length(sessions)
                cd(sessions(s).name)
                [status, result] = unix('find . -name ''skip.txt''');
                if isempty(result);
                    
                    mts = mtstrial('auto');
                    if unique(mts.data.Index(:,1)) == Args.rule
                        [perf,ti,crosst] = getperformance('getThreshCross',modvarargin{:});
                        nv = [];for pe = 1 : size(crosst,2); nv = [nv crosst(1,pe) : crosst(2,pe)]; end
                        if sum(mts.data.BehResp(nv)) >= Args.combNtrials
                            sd = [sd d];
                            ntrials = [ntrials sum(mts.data.BehResp(nv))];
                        end
                    end
                    
                    
                end
                cd ..
            end
        end
    end
    cd ..
end

seldays = days(sd);