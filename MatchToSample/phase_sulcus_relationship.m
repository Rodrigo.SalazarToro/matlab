function phase_sulcus_relationship(varargin)

%run at monkey level

Args = struct('ml',0,'hist',0);
Args.flags = {'ml','hist'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;

if Args.ml
    if exist('mtscpp_betty.mat')
        load mtscpp_betty
    else
        load idedays
        mtscpp_betty = processDays(mtsCPP,'days',idedays,'ml','ide_only','NoSites')
    end
    cc = mtscpp_betty;
    xy = get_grid_coordinates('ml');
    xy = [xy;xy]; %two chambers
else
    if exist('mtscpp_clark.mat')
        load mtscpp_clark
    else
        load idedays
        mtscpp_clark = processDays(mtsCPP,'days',idedays,'ml','ide_only','NoSites')
    end
    cc = mtscpp_clark;
    xy = get_grid_coordinates;
end

numb_pairs = size(cc.data.Index,1);

%grid numbers, same as channel numbers for betty
gridpos1 = cc.data.Index(:,14);
gridpos2 = cc.data.Index(:,15);

%depths
depths1 = cc.data.Index(:,5)'; %first channel
depths2 = cc.data.Index(:,6)'; %second channel

%phase angles
phase_angle = cc.data.Index(:,9)';

%peak corrcoef for epoch 4
peaks = cc.data.Index(:,18)';

for npairs = 1 : numb_pairs
    %get the xy coordinates for the grid
    x1(npairs) = xy(gridpos1(npairs),1);
    x2(npairs) = xy(gridpos2(npairs),1);
    
    y1(npairs) = xy(gridpos1(npairs),2);
    y2(npairs) = xy(gridpos2(npairs),2);
end

[i sig_pp_mm] = get(cc,'Number','pp','sorted','ips',1);
[i sig_pp_ll] = get(cc,'Number','pp','sorted','ips',2);
[i sig_pp_ml] = get(cc,'Number','pp','sorted','ips',3);

sul = .5;

[i sig_pp_mm_s] = get(cc,'Number','pp','sorted','ips',1,'sulcus',sul);
[i sig_pp_ll_s] = get(cc,'Number','pp','sorted','ips',2,'sulcus',sul);
[i sig_pp_ml_s] = get(cc,'Number','pp','sorted','ips',3,'sulcus',sul);

if Args.ml
    dist = 1.5;
else
    dist = .8;
end
%find euclidean distances (grid spacing is set at .8mm for clark)
euclid_mm = sqrt( ((x1(sig_pp_mm) .*dist) - (x2(sig_pp_mm)) .*dist) .^2) + sqrt( ((y1(sig_pp_mm) .*dist) - (y2(sig_pp_mm)) .*dist) .^2) + sqrt((depths1(sig_pp_mm)-depths2(sig_pp_mm)).^2);
euclid_ll = sqrt( ((x1(sig_pp_ll) .*dist) - (x2(sig_pp_ll)) .*dist) .^2) + sqrt( ((y1(sig_pp_ll) .*dist) - (y2(sig_pp_ll)) .*dist) .^2) + sqrt((depths1(sig_pp_ll)-depths2(sig_pp_ll)).^2);
%15mm is the estimated depth of the sulcus (from atlas)
euclid_ml = sqrt( ((x1(sig_pp_ml) .*dist) - (x2(sig_pp_ml)) .*dist) .^2) + sqrt( ((y1(sig_pp_ml) .*dist) - (y2(sig_pp_ml)) .*dist) .^2) + sqrt(((15-depths1(sig_pp_ml))+(15-depths2(sig_pp_ml))).^2);


%find euclidean distances (grid spacing is set at .8mm for clark)
euclid_mm_s = sqrt( ((x1(sig_pp_mm_s) .*dist) - (x2(sig_pp_mm_s)) .*dist) .^2) + sqrt( ((y1(sig_pp_mm_s) .*dist) - (y2(sig_pp_mm_s)) .*dist) .^2) + sqrt((depths1(sig_pp_mm_s)-depths2(sig_pp_mm_s)).^2);
euclid_ll_s = sqrt( ((x1(sig_pp_ll_s) .*dist) - (x2(sig_pp_ll_s)) .*dist) .^2) + sqrt( ((y1(sig_pp_ll_s) .*dist) - (y2(sig_pp_ll_s)) .*dist) .^2) + sqrt((depths1(sig_pp_ll_s)-depths2(sig_pp_ll_s)).^2);
%15mm is the estimated depth of the sulcus (from atlas)
euclid_ml_s = sqrt( ((x1(sig_pp_ml_s) .*dist) - (x2(sig_pp_ml_s)) .*dist) .^2) + sqrt( ((y1(sig_pp_ml_s) .*dist) - (y2(sig_pp_ml_s)) .*dist) .^2) + sqrt(((15-depths1(sig_pp_ml_s))+(15-depths2(sig_pp_ml_s))).^2);



pangle_mm = phase_angle(sig_pp_mm);
pangle_ll = phase_angle(sig_pp_ll);
pangle_ml = phase_angle(sig_pp_ml);

pangle_mm_s = phase_angle(sig_pp_mm_s);
pangle_ll_s = phase_angle(sig_pp_ll_s);
pangle_ml_s = phase_angle(sig_pp_ml_s);

corr_mm = peaks(sig_pp_mm);
corr_ll = peaks(sig_pp_ll);
corr_ml = peaks(sig_pp_ml);

corr_mm_s = peaks(sig_pp_mm_s);
corr_ll_s = peaks(sig_pp_ll_s);
corr_ml_s = peaks(sig_pp_ml_s);

if Args.hist
    figure
    subplot(2,3,1)
    scatter(euclid_ml,pange_ml)
    title('ml')
    subplot(2,3,2)
    scatter(euclid_ll,pangle_ll)
    title('ll')
    subplot(2,3,3)
    scatter(euclid_mm,pangle_mm)
    title('mm')
    
    subplot(2,3,4)
    hist(phase_angle(sig_pp_ml))
    subplot(2,3,5)
    hist(phase_angle(sig_pp_ll))
    subplot(2,3,6)
    hist(phase_angle(sig_pp_mm))
end


% % figure
% % scatter(peaks(sig_pp_ml),phase_angle(sig_pp_ml))

figure
scatter(euclid_mm,(corr_mm),'k','fill');
hold on
scatter(euclid_ml,(corr_ml),'r','fill');
hold on
scatter(euclid_ll,(corr_ll),'g','fill');
hold on 
scatter(euclid_mm_s,(corr_mm_s),'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','k');
hold on
scatter(euclid_ml_s,(corr_ml_s),'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','r');
hold on
scatter(euclid_ll_s,(corr_ll_s),'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','g');
legend('medial-medial','medial-lateral','lateral-lateral')
xlabel('distance (mm)')
ylabel('corr. coef.')




figure
scatter(euclid_mm,pangle_mm,'k','fill');
hold on
scatter(euclid_ml,pangle_ml,'r','fill');
hold on
scatter(euclid_ll,pangle_ll,'g','fill');
hold on 
scatter(euclid_mm_s,pangle_mm_s,'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','k');
hold on
scatter(euclid_ml_s,pangle_ml_s,'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','r');
hold on
scatter(euclid_ll_s,pangle_ll_s,'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','g');
legend('medial-medial','medial-lateral','lateral-lateral')
xlabel('distance (mm)')
ylabel('phase angle (deg.)')





cd(monkeydir)











