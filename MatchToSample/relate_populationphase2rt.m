function relate_populationphase2rt



cd('/media/raid/data/monkey/betty/091001/session01')

mt = mtstrial('auto','ML','RTfromML','redosetNames');
identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);

ntrials = size(identity,2);


N = NeuronalHist('ml');

cd lfp/lfp2/hilbert

hfiles = nptDir('*hilbert*');
counter = 0;
for x = identity
    
    counter = counter + 1;
    
    data = load([hfiles(x).name]);
    
    m = mt.data.MatchOnset(x);
    
    d = angle(data.data);

    
for dd = 1: size(d,1);
    sacc_angle(counter,dd) = d(dd,mt.data.MatchOnset(x) + mt.data.FirstSac(x));
    match_angle(counter,dd) = d(dd,mt.data.MatchOnset(x));
    diff_angle(counter,dd) = match_angle(counter,dd)-sacc_angle(counter,dd);
end
    
    match_time(counter) = mt.data.FirstSac(x);    
end
figure
for xx = 1:48;subplot(6,8,xx);hist(sacc_angle(:,xx));axis([-pi pi 0 100]); end
figure
for xx = 1:48;subplot(6,8,xx);scatter(match_angle(:,xx),match_time);end
