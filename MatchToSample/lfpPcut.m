function [data,lplength,varargout] = lfpPcut(trials,channels,varargin)
% to run in the session directory
% output: data{period}(samples x trials x channels)

Args = struct('plength',400,'downSR',200,'beforeSac',0,'EPremoved',0,'noDetrend',0,'fromFiles',0,'savelfpPcut',0,'epochs',[],'localDif',0);
Args.flags = {'beforeSac','EPremoved','fromFiles','savelfpPcut','localDif'};
[Args,modvarargin] = getOptArgs(varargin,Args);

plength = Args.plength;

downsamp = 1000/Args.downSR;
lplength = plength/downsamp;

mtst = ProcessSession(mtstrial,'auto');
if isempty(mtst); mtst = dstrial('auto'); end
[pdir,cdir] = getDataDirs('lfp','relative','CDNow');

files = nptDir('*_lfp.*');

count = 1;

if Args.fromFiles
    alldata = load('lfpPcut.mat');
    
    for p = 1 : 4
        data{p} = alldata.data{p}(:,trials,channels);
        if Args.localDif
            nch = size(data{p},3);
            for ch = 1 : nch
                data{p}(:,:,ch) = data{p}(:,:,ch) - mean(data{p}(:,:,setdiff([1:nch],ch)),3);
            end
        end
        lplength = alldata.lplength;
        periods = alldata.periods;
        norm = alldata.norm;
    end
else
    for t = vecr(trials)
        fprintf(' %d',t)
        if mtst.data.FirstSac(t) > 300
            saccade = round((mtst.data.MatchOnset(t) + 300)/downsamp);
        else
            saccade = round((mtst.data.MatchOnset(t) + mtst.data.FirstSac(t))/downsamp);
        end
        if isnumeric(Args.plength)
            if isempty(Args.epochs)
                periods(1) = round((mtst.data.CueOnset(t) - plength)/downsamp);
                periods(2) = round(mtst.data.CueOnset(t)/downsamp);
                periods(3) = round(mtst.data.CueOffset(t)/downsamp);
                periods(4) = round((mtst.data.MatchOnset(t) - plength)/downsamp);
            else
                for p = 1 : length(Args.epochs{2})
                    endtime = Args.epochs{2}(p);
                    refer = eval(sprintf('mtst.data.%s(t)',Args.epochs{1}));
                   periods(p) =  round((refer+endtime) /downsamp);
                    
                end
            end
        else
            lplength = 1880/downsamp;
            periods = saccade - lplength;
            
        end
        if Args.beforeSac; periods(5) = (saccade  - plength) / downsamp; end
        nch = 1;
        for ch = channels
            [lfp,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(files(t).name,ch);
            
            [dlfp,SR,norm(count,nch,:)] = preprocessinglfp(lfp,'downSR',Args.downSR,'ZendCut',saccade * downsamp , modvarargin{:});
            
            %         datacut = [];
            for p = 1 : length(periods)
                %             datacut = [datacut vecr(dlfp(periods(p):periods(p) + lplength))];
                
                prelfp = dlfp(periods(p):periods(p) + lplength);
                if ~Args.noDetrend
                    prelfp = lfp_detrend(vecc(prelfp),modvarargin{:}); % take off teh linear portion of the signal
                end
                data{p}(:,count,nch) = prelfp;
                %             data{p}(:,count,nch) = dlfp(periods(p):periods(p) + lplength);
                
            end
            %         data{ch}(:,count) = single(datacut);
            nch = nch + 1;
        end
        count = count + 1;
        clear lfp rslfp flfp dlfp
    end
    
    if Args.EPremoved
        for p = 1 : length(periods)
            pdata = permute(data{p},[1 3 2]);
            ndata = lfp_normalize(pdata,1); % remove teh evoked potential
            data{p} = permute(ndata,[1 3 2]);
            
        end
    end
end
if Args.savelfpPcut
    save('lfpPcut.mat','data','Args','modvarargin','lplength','periods','norm')
    display(sprintf('\n saving lfpPcut in %s \n',pwd))
end



cd(cdir)
varargout{1} = periods;
varargout{2} = norm;