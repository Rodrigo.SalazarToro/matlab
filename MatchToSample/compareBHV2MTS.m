function compareBHV2MTS(varargin)

%compares the information in the mtstrial object with the BHV file from
%monkey logic

Args = struct('lum',[0],'nlynx',0);
Args.flags = {'nlynx'};
[Args,varargin2] = getOptArgs(varargin,Args);


global discrepancyMTS
if Args.nlynx
    ObjIdentity = ProcessSessionMTS('ObjectLibrary','ML','RTfromML','Nlynx');
else
    ObjIdentity = ProcessSessionMTS('ObjectLibrary','ML');
end
%identities of objects based on their number in the mtstrial object
% ObjIdentity = {'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'';'A';'B';'C';'D';'E';'F';'G';'H';'I';'J';'K';'L';'M';'N';'O';'A75';'B75';'C75';'D75';'E75';'F75';'G75';'H75';'I75';'J75';'K75';'L75';'M75';'N75';'O75'};

sessions = nptDir('session0*');
day_dir = pwd;
for y = 1 : (size(sessions,1))
    cd(day_dir)
    cd(sessions(y).name)
    
    %select BHV file for the session
    bhv = nptDir('*.bhv');
    skip = nptDir('skip.txt');
    b = BHV_read([cd filesep bhv.name]);
    if ((isempty((strmatch(b.ExperimentName,'MTS')))) || (~isempty(skip)))
        continue
    end
    
    if Args.nlynx
        m = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
    else
        m = mtstrial('auto','ML','RTfromML');
    end
    
    numtrials = m.data.numSets;
    
    if Args.nlynx
        trials = sort([find(single(b.TrialError) == 0); find(single(b.TrialError) == 6)])'; %all correct and incorrect trials are written
    else
        %get original trial information for bhv data
        cd('delFiles')
        load deletedfiles trials
        cd ..
    end
    
    bhv2mts = [];
    for x = 1 : numtrials
        %fprintf(1,'\n')
        %fprintf(1,num2str(x))
        %positions corresponding to 0-5 in mtstrial object
        if Args.nlynx
            allpos = [0.000 0.000; 5.000 0.000; -5.000 0.000];
        else
            allpos = [0.000 5.000; 4.330 -2.500; -4.330 -2.500; 0.000 -5.000; -4.330 2.500; 4.330 2.500];
        end
        
        %mtstrial object
        CueObj= ObjIdentity(m.data.CueObj(x));
        CueLoc = allpos((m.data.CueLoc(x)+1),:);
        MatchObj1 = ObjIdentity(m.data.MatchObj1(x)); %returns actual letter
        MatchPos1 = allpos(m.data.MatchPos1(x)+1,:);
        MatchObj2 = ObjIdentity(m.data.MatchObj2(x)); %returns acutal letter
        MatchPos2 = allpos((m.data.MatchPos2(x)+1),:);
        CueOnset = m.data.CueOnset(x);
        CueOffset = m.data.CueOffset(x);
        MatchOnset = m.data.MatchOnset(x);
        BehResp = m.data.BehResp(x); %correct = 1, incorrect = 0
        FirstSac = m.data.FirstSac(x);
        
        trial = trials(x); %actual trial
        
        %get bhv timing data
        t = b.CodeTimes{trial};
        times = t - t(1); %correct for offset
        
        %get condition
        condition = b.ConditionNumber(trial);
        if ~condition == 48 && ~Args.nlynx
            c1 =cell2mat(b.TaskObject(condition,1)); %fix
            
            c2 = cell2mat(b.TaskObject(condition,2)); %cue
            commas = strfind(c2,',');
            parenth = strfind(c2,')');
            if isempty(commas)
                c2_1 = [];
                c2_2 = [];
                c2_all = [];
            else
                c2_1 = [commas(1):commas(2)];
                c2_2 = [commas(2):(parenth-1)];
                c2_all = [c2_1(1):c2_2(end)];
            end
            
            c3 = cell2mat(b.TaskObject(condition,3)); %match obj1
            commas = strfind(c3,',');
            parenth = strfind(c3,')');
            if isempty(commas)
                c3_1 = [];
                c3_2 = [];
                c3_all = [];
            else
                c3_1 = [commas(1):commas(2)];
                c3_2 = [commas(2):(parenth-1)];
                c3_all = [c3_1(1):c3_2(end)];
            end
            
            
            c4 = cell2mat(b.TaskObject(condition,4)); %match obj2
            commas = strfind(c4,',');
            parenth = strfind(c4,')');
            if isempty(commas)
                c4_1 = [];
                c4_2 = [];
                c4_all = [];
            else
                c4_1 = [commas(1):commas(2)];
                c4_2 = [commas(2):(parenth-1)];
                c4_all = [c4_1(1):c4_2(end)];
            end
            
            if isempty(c2_1)
                c2 = 'Black0,0'; c2_all = 6:8; obj_pic{1} = 1:5;
                c3 = 'Black0,0'; c3_all = 6:8; obj_pic{2} = 1:5;
                c4 = 'Black0,0'; c4_all = 6:8; obj_pic{3} = 1:5;
                
                
            else
                if Args.nlynx
                    for o = 1 : 2; endO = findstr(c2,','); eval(sprintf('obj_pic{o} = [findstr(c%d,''('')+1 : endO(1)-1];',o+1)); end
                    endO = findstr(c4,',');
                    parenth = findstr(c4,'(');
                    obj_pic{3} = (parenth(1)+1:endO(1)-1);
                else
                    for o = 1 : 3; endO = findstr(c2,','); eval(sprintf('obj_pic{o} = [findstr(c%d,''('')+1 : endO(1)-1];',o+1)); end
                    if Args.lum == 100
                        obj_pic = [6];
                    end
                end
            end
            
            
            %bhv data
            bhvCueObj =c2(obj_pic{1});
            bhvCueLoc = str2num(c2(c2_all));
            bhvMatchObj1 = c3(obj_pic{2});
            bhvMatchPos1 = str2num(c3(c3_all));
            bhvMatchObj2 = c4(obj_pic{3});
            bhvMatchPos2 = str2num(c4(c4_all));
            bhvCueOnset = times(5);
            bhvCueOffset = times(6);
            bhvMatchOnset = times(7);
            bhvBR = b.TrialError(trial); %correct = 0, incorrect = 6
            if bhvBR == 0
                bhvBehResp = 1;
            elseif bhvBR == 6
                bhvBehResp = 0;
            else
                bhvBehResp = nan;
            end
            bhvFirstSac = b.ReactionTime(trial);
            
            if cell2mat(CueObj) == bhvCueObj
                bhv2mts(1,x) = 0;
            else
                bhv2mts(1,x) = 1;
            end
            
            if CueLoc == bhvCueLoc
                bhv2mts(2,x) = 0;
            elseif strmatch(bhvCueObj,'Black')
                bhv2mts(2,x) = 0;
            else
                bhv2mts(2,x) = 1;
            end
            
            if cell2mat(MatchObj1) == bhvMatchObj1
                bhv2mts(3,x) = 0;
            else
                bhv2mts(3,x) = 1;
            end
            
            if MatchPos1 == bhvMatchPos1
                bhv2mts(4,x) = 0;
            elseif strmatch(bhvCueObj,'Black')
                bhv2mts(4,x) = 0;
            else
                bhv2mts(4,x) = 1;
            end
            
            if cell2mat(MatchObj2) == bhvMatchObj2
                bhv2mts(5,x) = 0;
            else
                bhv2mts(5,x) = 1;
            end
            
            if MatchPos2 == bhvMatchPos2
                bhv2mts(6,x) = 0;
            elseif strmatch(bhvCueObj,'Black')
                bhv2mts(6,x) = 0;
            else
                bhv2mts(6,x) = 1;
            end
            
            if isempty(c2_1) %currently there is no sample in fixation trials
                bhv2mts(7,x) = 0;
            else
                if CueOnset == bhvCueOnset
                    bhv2mts(7,x) = 0;
                else
                    bhv2mts(7,x) = 1;
                end
            end
            
            if CueOffset == bhvCueOffset
                bhv2mts(8,x) = 0;
            else
                bhv2mts(8,x) = 1;
            end
            
            if MatchOnset == bhvMatchOnset
                bhv2mts(9,x) = 0;
            else
                bhv2mts(9,x) = 1;
            end
            
            if BehResp == bhvBehResp
                bhv2mts(10,x) = 0;
            else
                bhv2mts(10,x) = 1;
            end
            
            if FirstSac == bhvFirstSac
                bhv2mts(11,x) = 0;
            elseif isnan(bhvFirstSac) && isnan(FirstSac)
                bhv2mts(11,x) = 0;
            else
                bhv2mts(11,x) = 1;
            end
        else
            bhv2mts(1:11,x) = 0;
        end
    end
    
    if (sum(sum(bhv2mts))) ~= 0
        discrepancyMTS = 1;
        fprintf(1,'\n')
        fprintf(1,['WARNING! Differences between BHV and mtstrial object in ' sessions(y).name])
        fprintf(1,'\n')
        save bhv2mts bhv2mts
    else
        discrepancyMTS = 0;
        fprintf(1,'\n')
        fprintf(1,['No Difference between BHV and mtstrial object in ' sessions(y).name])
        fprintf(1,'\n')
    end
    cd ..
end
cd ..

