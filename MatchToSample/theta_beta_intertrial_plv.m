function theta_beta_intertrial_plv

%used write_power_trials

cd('/media/raid/data/monkey/clark/060511/session02')
mt = mtstrial('auto','ML','RTfromML','redosetNames');
tr = mtsgetTrials(mt,'BehResp',1,'stable');

cd('/media/raid/data/monkey/clark/060511/session02/lfp/power')

nfftp = nptdir('*normalized_fftpower*');


%get the phase of the 4 hz band

thband_angle = [];
betaband_angle = [];
cross_f_pval = zeros(20,20);
for ch = 1:4
    crossf = zeros(20,20);
    
    for a1 = 1:15
        for a2 = a1+5:20
            tcounter = 0;
            for t = tr
                tcounter = tcounter + 1;
                
                load(nfftp(t).name);
                
                angle1(tcounter) = power.fangle{4}(ch,a1);
                angle2(tcounter) = power.fangle{4}(ch,a2);
            end
            
            crossf(a1,a2) = abs(sum(exp(i.*(angle2-angle1)))) / size(tr,2);
            
            all_perms = [];
            %run surrogate
            for perm = 1 : 1000
                all_perms(perm) = abs(sum(exp(i.*(angle2(randperm(size(tr,2)))-angle1)))) / size(tr,2);
            end
            
            if crossf(a1,a2) > prctile(all_perms,99.9)
                cross_f_pval(a1,a2) =  cross_f_pval(a1,a2) + 1;
            end
        end
    end
    %     figure
    %     subplot(4,1,1);hist(thband_angle);subplot(4,1,2);hist(betaband_angle);subplot(4,1,3);%hist(abs(betaband_angle - thband_angle));
    %
    %     subplot(4,1,4)
    %     allpower = [];for x = tr;load(nfftp(x).name);allpower(x,:) = power.S{4}(ch,1:30);end
    %     plot(power.f{4}(1:30),smooth(mean(allpower)))
    %
    %     power.f{4}(5)
    %
    %     power.f{4}(14)
    %
    %     ph_diffs = betaband_angle - thband_angle;
    %
    %     abs(sum(exp(i.*a2-a1))) / size(tr,2)
    
    
end
imagesc(crossf./9)
colorbar
power.f{4}(1:15)