function Day = MTSlfp3(varargin)
% Computes the coherence on defined epochs (500ms; fixation,cue,delay1, delay2).
% The type of coherence can be chosen with the arguments:
% - cohPP: within the parietal electrodes
% - cohPF: within the frontal electrodes
% - cohInter: between parietal and frontal
% - surrogate: computes the surrogate distribution
%
% Additional arguments are:
% - stable to only analyze teh stable performance phase
% - BehResp to specify correct 1 or incorrect 0
% - 'save' to save the results in the day directory
% - 'redo' to redo the analysis even though the results have already been
% saved.
%
% The preprocessiong of data includes 60Hz removal and a linear detrend of
% the data.


Args = struct('redo',0,'save',0,'remoteName',[],'powerPP',0,'powerPF',0,'cohPP',0,'cohPF',0,'cohInter',0,'general',0,'surrogate',0,'startDay',1,'days',[],'compareWindows',0,'combineSession',0,'ML',0,'trialave',1,'noFlat',0,'sameNtrials',0,'InCor',0,'fixation',0,'fixationS',0,'separateDelays',[],'GCcohInter',0,'GCgeneralSur',0,'GCorder',20,'GCfreq',[1 : 100],'trans',0,'addName',[]);
Args.flags = {'redo','save','surrogate','ML','compareWindows','fixationS','combineSession','noFlat','sameNtrials','InCor','fixation','trans','powerPP','powerPF','cohPP','cohPF','cohInter','GCcohInter','general','GCgeneralSur'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto','trans'});

rsFs = 200; % downsampling
plength = 400;
compareChart = [1 2;1 3;1 4;2 3;2 4;3 4];

if isempty(Args.days)
    tdays = [nptDir('0*'); nptDir('1*')];
    for d = 1 : length(tdays); days{d} = tdays(d).name; end
else
    days = Args.days;
end

if Args.combineSession; supname = 'cS'; elseif Args.trans; supname = 'Trans'; else supname = []; end
if Args.InCor == 1; BehResp = 'InCor';  elseif Args.fixation; if Args.fixationS; BehResp = 'fixS'; else BehResp = 'fix'; end;else BehResp = []; end

if ~isempty(Args.remoteName)
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
    params{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',Args.trialave);
    %     paramsP{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0,'err',[2 0.05]);
    % the poewr cannot and does not need to be run with remoteName
else
    params = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',Args.trialave);
    paramsP = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0);
end
sdir = pwd;
for d = Args.startDay : length(days)
    cd(sdir)
    cd(days{d});
    ddir = pwd;
    skip = nptDir('skip.txt');
    if isempty(skip)
        for flag =  find(strcmp(Args.flags,'powerPP') == 1) : size(Args.flags,2); if eval(sprintf('Args.%s',Args.flags{flag})); selOpt = flag; end; end;
        
        if Args.surrogate; matfile = sprintf('%s/%s%s%s%sSur.mat',pwd,Args.flags{selOpt},supname,BehResp,Args.addName);elseif Args.compareWindows; matfile = sprintf('%s/%sPcomp%s%s%s.mat',pwd,Args.flags{selOpt},supname,BehResp,Args.addName); else matfile = sprintf('%s/%s%s%s%s.mat',pwd,Args.flags{selOpt},supname,BehResp,Args.addName);end
        if ~isempty(Args.separateDelays); matfile = sprintf('%s%sDelay%d.mat',matfile(1:end-4),Args.addName,Args.separateDelays); end
        prematfile = nptDir(matfile);
        if isempty(prematfile) || Args.redo
            %             [ch,CHcombt,iflat,groups] = getFlatCh(Args.flags{selOpt});
            %             [spairs,ntot,index] = selectPairs('type','windowSpectra','rule',[1 2],'windows',[3 4],'fband',[],'gui',0,'year',[],'data',[],'ruleComb',{'union'},'comparison',{'pos'},'range',{'beta'});
            session = nptDir('session0*');
            scount = 1;
            clear allrules rsession
            if Args.ML
                ses = [];
                for s = 1 : size(session,1);cd(session(s).name);
                    mtsfile = nptDir('*.bhv');
                    skip = nptDir('skip.txt');
                    if strcmp('MTS',mtsfile(1).name(1:3)) && isempty(skip)
                        tmtst = mtstrial('auto',modvarargin{:},'redosetNames');
                        %                         nblock = 1 + length(find(tmst.data.Index(:,1)));
                        if unique(tmtst.data.Index(:,1)) < 3
                            
                            if Args.combineSession; allrules = unique(tmtst.data.Index(:,1)); else allrules = unique(tmtst.data.Index(:,1));end%[tmtst.data.Index(find(diff(tmtst.data.Index(:,1)) ~= 0),1); tmtst.data.Index(end,1)];
                            for r = 1 : 2; if Args.fixation; minTrials(r) = length(mtsgetTrials(tmtst,modvarargin{:},'rule',r,'CueObj',55)); else; minTrials(r) = length(mtsgetTrials(tmtst,modvarargin{:},'rule',r));end;end; minTrials = min(minTrials);
                            ses = [ses repmat(s,1,length(allrules))];
                        end
                        if length(allrules) >= 2
                            cd ..
                            break
                        end
                    end
                    cd ..;
                end
            else
                for s = 2 : size(session,1);
                    cd(session(s).name);
                    tmtst = mtstrial('auto','redosetNames'); allrules(s-1) = unique(tmtst.data.Index(:,1));
                    minTrials(s-1) = length(mtsgetTrials(tmtst,modvarargin{:}));
                    cd ..;
                end
                minTrials = min(minTrials);
                
            end
            if Args.fixationS
                ses = 3;
            else
            rsession = cell(2,1);if Args.combineSession && length(session) > 2; ses = [2 3]; for s = 1 : 2; rsession{s} = find(allrules == allrules(s));end; elseif ~Args.ML; ses = [2 : length(session)]; end
            end
            if Args.noFlat ; [ch,allCHcomb,iflat,groups] = getFlatCh('cohtype',Args.flags{selOpt}); end
            
            
            for s = ses
                cd(ddir)
                cd(session(s).name)
                
                skip = nptDir('skip.txt');
                
                if isempty(skip)
                    %                     [allchannels,allcomb,allCHcomb] = checkChannels(Args.flags{selOpt});
                    [channels,comb,CHcomb] = checkChannels(Args.flags{selOpt});
                    %                     channels = setdiff(ch,ch(iflat.ch)); CHcomb = allCHcomb(find(sum(ismember(allCHcomb,channels),2) == 2),:);
                    if  comb ~= -1
                        mtst = mtstrial('auto','redosetNames');
                        
                        cd('lfp')
                        skip = nptDir('skip.txt');
                        if isempty(skip)
                            files = nptDir('*_lfp.*');
                            if Args.ML
                                ftrials = mtsgetTrials(mtst,modvarargin{:},'CueObj',55,'lowThresRT',10);
                                ctrials = mtsgetTrials(mtst,modvarargin{:},'rule',allrules(scount));
                                if Args.fixation || Args.fixationS
                                    selectTrials =ftrials;
                                elseif ~isempty(ftrials)
                                    selectTrials = setdiff(ctrials,ftrials);
                                else
                                    selectTrials = ctrials;
                                end
                                if Args.sameNtrials; norder = selectTrials(randperm(length(selectTrials))); selectTrials = sort(norder(1:minTrials)); end
                            else
                                selectTrials = mtsgetTrials(mtst,modvarargin{:});
                                if Args.sameNtrials; norder = selectTrials(randperm(length(selectTrials))); selectTrials = sort(norder(1:minTrials)); end
                            end
                            if size(files,1) == mtst.data.numSets && ~isempty(selectTrials) && ~isempty(mtst)
                                
                                [data,~,periods,Session(scount).norm] = lfpPcut(selectTrials,channels,'plength',plength,modvarargin{:});
                                
                                if Args.combineSession
                                    startd = pwd;
                                    for ns = rsession{s-1}(2:end)
                                        
                                        cd ../..
                                        cd(session(ns).name)
                                        [channels,comb,CHcomb] = checkChannels('flatGr',Args.flags{selOpt});
                                        skip = nptDir('skip.txt');
                                        
                                        if comb ~= -1 && isempty(skip)
                                            
                                            nmtst = mtstrial('auto');
                                            
                                            cd('lfp')
                                            skip = nptDir('skip.txt');
                                            if isempty(skip)
                                                nfiles = nptDir('*_lfp.*');
                                                
                                                nselectTrials = mtsgetTrials(nmtst,modvarargin{:});
                                                if size(nfiles,1) == nmtst.data.numSets && ~isempty(nselectTrials) && ~isempty(nmtst)
                                                    
                                                    [ndata,~,periods] = lfpPcut(nselectTrials,channels,'plength',plength,modvarargin{:});
                                                    for p = 1 : 4; data{p} = [data{p} ndata{p}]; end
                                                end
                                                
                                            end
                                            cd ..
                                        end
                                        cd(startd);
                                    end
                                end
                                clear files
                                if Args.compareWindows && ~isempty(Args.remoteName)
                                    s1 = size(compareChart,1);
                                else
                                    s1 = 4;
                                end
                                
                                if comb == 2
                                    data1 = cell(s1+(size(CHcomb,1)-1)*s1,1);
                                    data2 = cell(s1+(size(CHcomb,1)-1)*s1,1);
                                   if ~Args.general && ~Args.GCgeneralSur
                                    thencomb = size(CHcomb,1);
                                   else
                                    thencomb = 1;
                                   end
%                                    cd fourierTrans/
%                                    files = nptDir('*_fft_*');
%                                    load(files(1).name,'gcomb')
%                                    cd ../..
%                                    calculateCoherence(sprintf('Rule%d',allrules(scount)),selectTrials,'save',modvarargin{:});
                                   
%                                    cd lfp
                                 
                                    for cb = 1 : thencomb
                                        if Args.compareWindows && ~isempty(Args.remoteName)
                                            for cmp = 1 : size(compareChart,1)
                                                data1{cmp+(cb-1)*size(compareChart,1)} = single(data{compareChart(cmp,1)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]));
                                                data2{cmp+(cb-1)*size(compareChart,1)} = single(data{compareChart(cmp,2)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]));
                                            end
                                        end
                                          %% this section o fth ecode will
                                   %% disappear
                                        for p = 1 : length(periods)
                                            %                                     lowLim = (p-1)*lplength +1;
                                            %                                     highLim = lowLim + lplength;
                                            
                                            if ~isempty(Args.remoteName)
                                                if ~Args.compareWindows
                                                    data1{p+(cb-1)*length(periods)} = data{p}(:,:,find(channels == CHcomb(cb,1))); % data{CHcomb(cb,1)}(lowLim:highLim,:);
                                                    data2{p+(cb-1)*length(periods)} = data{p}(:,:,find(channels == CHcomb(cb,2))); % data{CHcomb(cb,2)}(lowLim:highLim,:);
                                                end
                                                %                                     elseif Args.surrogate
                                                %                                         [Session.Cmean(:,cb,p),Session.Cstd(:,cb,p),Session.phimean(:,cb,p),Session.phistd(:,cb,p)] = cohsurrogate(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params,'wintermute');
                                            else
                                                
                                                if ~Args.compareWindows
                                                    if Args.surrogate && ~Args.GCcohInter
                                                        
                                                        if Args.general
                                                            [Session(scount).Cmean(:,cb,p),Session(scount).phCstd(:,cb,p),Session(scount).phimean(:,cb,p),Session(scount).phistd(:,cb,p),Session(scount).tile99(:,cb,p),Session(scount).Prob(:,:,cb,p),f] = cohsurrogate(data{p},data{p},params,'generalized','saveName',sprintf('generalSurRule%dp%d.mat',allrules(scount),p),'nrep',10000,modvarargin{:});%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                                                            
                                                        else
                                                            [Session(scount).Cmean(:,cb,p),Session(scount).phCstd(:,cb,p),Session(scount).phimean(:,cb,p),Session(scount).phistd(:,cb,p),Session(scount).tile99(:,cb,p),Session(scount).Prob(:,:,cb,p),f] = cohsurrogate(data{p}(:,:,find(channels == CHcomb(cb,1))),data{p}(:,:,find(channels == CHcomb(cb,2))),params,modvarargin{:});%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                                                        end
                                                    elseif Args.GCcohInter || Args.GCgeneralSur%|| Args.GCcohPP || Args.GCcohPF
                                                        Nr = size(data{1},2);
                                                        Nl = size(data{1},1);
                                                        ndata = [];ndata = [ndata reshape(data{p},Nl * Nr,size(data{p},3))'];
                                                        
                                                        if Args.GCgeneralSur
                                                            [Session(scount).Fx2y(:,:,p),Session(scount).Fy2x(:,:,p),Session(scount).Fxy(:,:,p)]= GCcohsurrogate(ndata,size(ndata,2)/Nl,Nl,Args.GCorder,rsFs,Args.GCfreq,'nrep',10000,'general');
                                                        else
                                                        ch1 = find(channels == CHcomb(cb,1));
                                                        ch2 = find(channels == CHcomb(cb,2));
                                                        [~,~,Session(scount).Fx2y(:,cb,p),Session(scount).Fy2x(:,cb,p),Session(scount).Fxy(:,cb,p),~]= pwcausalrp(ndata([ch1 ch2],:),size(ndata,2)/Nl,Nl,Args.GCorder,rsFs,Args.GCfreq);
                                                        end
                                                        f = Args.GCfreq;
                                                   else
                                                        %% basic coherence
                                                        %% old way
                                                       [Session(scount).C(:,cb,p),Session(scount).phi(:,cb,p),Session(scount).S12(:,cb,p),Session(scount).S1(:,cb,p),Session(scount).S2(:,cb,p),f] = coherencyc(data{p}(:,:,find(channels == CHcomb(cb,1))),data{p}(:,:,find(channels == CHcomb(cb,2))),params);%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                                                    
                                                    end
                                                end
                                            end
                                        end
%                                        tf = ismember(ccomb,CHcomb(cb,:),'rows');
%                                         load(sprintf('fourierTrans/Rule%dgr%04.0fgr%04.0f.mat',allrules(scount),gcomb(tf,1),gcomb(tf,2)))
%                                         Session(scount).C(:,cb,:) = C;
%                                         Session(scount).phi(:,cb,:) = phi;
                                        if Args.compareWindows && isempty(Args.remoteName)
                                            for cmp = 1 : size(compareChart,1)
                                                
                                                [Session(scount).dz(:,cb,cmp),Session(scount).vdz(:,cb,cmp),Session(scount).Adz(:,cb,cmp),f] = compareCoherence(data{compareChart(cmp,1)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]),data{compareChart(cmp,2)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]),params,modvarargin{:});
                                                
                                            end
                                        end
                                        
                                    end
                                    %%
                                    clear data
                                    if ~isempty(Args.remoteName) && ~isempty(data1) && ~isempty(data2)
                                        fprintf('Data sent to %s on %g %g %gth at %gh%g %g sec. \n',Args.remoteName,clock)
                                        if Args.surrogate
                                            
                                            [Cmean,Cstd,phimean,phistd,tile99,Prob,ftemp] = dfeval(@cohsurrogate,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                            
                                        elseif Args.compareWindows
                                            [dz,vdz,Adz,ftemp] = dfeval(@compareCoherence,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                        else
                                            [C,phi,S12,S1,S2,ftemp,confC,Phierr,Cerr] = dfeval(@coherencyc,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                        end
                                        clear data1 data2
                                        if Args.surrogate; var = {'Cmean','Cstd','phimean','phistd','tile99','Prob'}; elseif Args.compareWindows; var = {'dz','vdz','Adz'}; else var = {'C','phi','S12','S1','S2','Phierr'};end
                                        for cb = 1 : size(CHcomb,1)
                                            if Args.compareWindows; countV = size(compareChart,1); else countV = length(periods);end
                                            for p = 1 : countV
                                                for v = 1 : length(var)
                                                    if strcmp(var{v},'Prob')
                                                        eval(sprintf('Session(scount).%s(:,:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                    else
                                                        eval(sprintf('Session(scount).%s(:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                    end
                                                end
                                                if ~Args.surrogate && ~Args.compareWindows
                                                    
                                                    
                                                    Session(scount).confC(cb,p) = confC{p+(cb-1)*length(periods)};
                                                    Session(scount).Cerr(:,:,cb,p) = Cerr{p+(cb-1)*length(periods)};
                                                end
                                                
                                            end
                                        end
                                        f = ftemp{1};
                                        clear C phi S1 S2 S12 ftemp confC Phierr Cerr dz vdz Adz
                                    end
                                elseif comb == 1
                                    for p = 1 : 4
                                        for cb = 1 : size(data{p},3)
                                            [Session(scount).S(:,:,cb,p),f] = mtspectrumc(squeeze(data{p}(:,:,cb)),paramsP);
                                        end
                                    end
                                    %power computation
                                end
                                
                            else
                                fprintf('!!!!!!Problem: different # of trials in lfp folder than in main folder or no trial selected');
                            end
                            
                        end
                        
                        cd .. % session
                        if exist('Session','var')
                            if Args.ML
                                Session(scount).rule = scount;
                                
                            else
                                Session(scount).rule = unique(mtst.data.Index(:,1));
                            end
                            Session(scount).trials = selectTrials;
                            scount = scount + 1;
                        end
                        
                    end
                    
                    cd .. % day
                else
                    cd ..
                end %s = 2 : size(session,1)
                
                %cd ..
            end
            
        end
    end
    if exist('Session','var')
        Day.session = Session;
        if CHcomb == -1 ; Day.channels = channels; else Day.comb = CHcomb;end
        Day.name = pwd;
        Day.option = varargin;
        clear Session
    end
    if Args.save && exist('Day','var') && exist('f','var'); fprintf('\n saving file %s \n',matfile);  save(matfile,'Day','f'); clear Day; end
    
    cd ..
    
end %for d = 1 : size(days,1)

