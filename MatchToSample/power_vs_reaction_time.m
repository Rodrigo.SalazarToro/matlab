function power_vs_reaction_time(varargin)

%computes time frequency power, locked to sample, match and first saccade
%saves a file for each channel in the appropriate folder

cd /media/raid/data/monkey/betty/091001/session01

% cd /media/raid/data/monkey/clark/060414/session02

Args = struct('ml',0,'bmf',0,'rules',[],'bettynlynx',0);
Args.flags = {'ml','bmf','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args);


cd /media/raid/data/monkey/betty/
load alldays

% cd /media/raid/data/monkey/clark/
% load idedays
% load ideses

% figure
spcounter = 0;
for d = 27%15 : 31%1:29
    clf
    if Args.ml
        cd(['/media/raid/data/monkey/betty' filesep alldays{d} filesep 'session01'])
    else
        
        cd(['/media/raid/data/monkey/clark' filesep idedays{d} filesep ses{d}])
    end
    spcounter = spcounter + 1;
    %     subplot(3,3,spcounter)
    
    sesdir = pwd;
    if Args.ml
        if Args.bettynlynx
            N = NeuronalHist('ml','bettynlynx');
            noisy = noisy_groups;
            [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml','bettynlynx');
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
            
        elseif Args.bmf
            N = NeuronalHist('bmf');
            [sortedgroups,chpairlist,~,~,sortedpairs] = bmf_groups('ml'); %already kicks out noisy groups
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
        else
            N = NeuronalHist('ml');
            noisy = noisy_groups;
            [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups('ml');
            [~,ii] = intersect(sortedgroups,noisy);
            sortedgroups(ii) = []; %get rid of noisy groups
            [~,chgroups] = intersect(N.gridPos,sortedgroups);
        end
    else
        N = NeuronalHist;
        Nch = NeuronalChAssign;
        noisy = noisy_groups;
        [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
        
        [~,chgroups] = intersect(Nch.groups,sortedgroups);
    end
    
    cd([sesdir filesep 'lfp']);
    if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
        load('iti_rejectedTrials.mat','rejecTrials') %this looks at the entire trial and is necessary to get rid of iti artifacts
    else
        load('rejectedTrials.mat','rejecTrials') %use the non-incorrect specific rejected trials
    end
    
    lfpdir = pwd;
    lfptrials = nptDir('*_lfp.*');
    
    
    if Args.ml
        if Args.bmf
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
            identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
            incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        else
            mt = mtstrial('auto','ML','RTfromML','redosetNames');
            %get only the specified trial indices (correct and stable)
            identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
            location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
            fixtrials = find(mt.data.CueObj == 55)';
            [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
            fixtrials(rej) = [];
            incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
            
            [~,baditi] = intersect(incorrtrials,rejecTrials);
            incorrtrials(baditi) = [];
        end
    else
        mt=mtstrial('auto','redosetNames');
        %get only the specified trial indices (correct and stable)
        tr = mtsgetTrials(mt,'BehResp',1,'stable');
    end
    
    trialorder = {'identity','location','fixation','incorrect'};
    
    %get trial timing information
    %all trials are aligned to sample off (sample off is 1000)
    sample_off = floor((mt.data.CueOffset) ./ 5); %sample off
    match = floor(floor(mt.data.MatchOnset) ./5);    %match
    sacc = floor((mt.data.MatchOnset + mt.data.FirstSac) ./ 5);
    
    %determine hazard rate for each trial
    hrates = (((match - sample_off) -160) ./ max(((match - sample_off) -160))) * 100;
    
    if isempty(Args.rules)
        if Args.ml
            if Args.bmf
                rules = [1, 4];
            else
                rules = [1 4]; %3 and 4 are used for the fixation trials and the incorrect trials
            end
        else
            rules = [1];
        end
    else
        rules = Args.rules;
    end
    
    for sg = 1 : size(chgroups,2);
        for rule = rules
            cd(lfpdir)
            if Args.ml
                if rule == 1
                    trials = identity;
                elseif rule == 2
                    trials = location;
                elseif rule == 3
                    trials = fixtrials;
                elseif rule == 4
                    trials = incorrtrials;
                end
            else
                trials = tr;
            end
            
            if ~isempty(trials)
                cd(lfpdir)
                %get correct channel
                c1 = chgroups(sg);
                tcounter = 0;
                dmatch = [];
                for x = trials
                    
                    tcounter = tcounter + 1;
                    m = match(x);
                    
                    
                    [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(x).name);
                    p1t = data.rawdata(c1,:);
                    
                    
                    %get rid of 60! and detrend
                    data1 = preprocessinglfp(p1t,'detrend')';
                    
                    dmatch((1:size(data1((m - 80) : m),2)),tcounter) = data1((m - 80) : m);
                    
                end
                
                params = struct('tapers',[2 3],'Fs',200,'fpass',[1 100]);
                [S,f] = mtspectrumc(dmatch,params);
                
                params = struct('tapers',[2 3],'Fs',200,'fpass',[1 25],'trialave',1);
                [S_ave,fave] = mtspectrumc(dmatch,params);
                
                rts = floor(mt.data.FirstSac(trials));
                
            end
            if rule == 1
                Side = S;
                Saveide = S_ave;
                rtside = rts;
            else
                Sloc = S;
                Saveloc = S_ave;
                rtsloc = rts;
            end
            
        end
        
        for freqs = 3%1 : 20
            % %         subplot(2,1,1)
            % %
            % %         if size(Args.rules,2) == 2
            % %             scatter(rtside,Side(freqs,:),'b','fill','s')
            % %             hold on
            % %             scatter(rtsloc,Sloc(freqs,:),'r','fill','s')
            % %         elseif Args.rules == 1
            % %             scatter(rtside,Side(freqs,:),'b','fill','s')
            % %         else
            % %             scatter(rtsloc,Sloc(freqs,:),'r','fill','s')
            % %         end
            % %         hold on
            % %         title(num2str(f(freqs)))
            % %         subplot(2,1,2)
            % %         hold on
            if size(Args.rules,2) == 2
                rtvpw = [];
                c = 0;
                for rt = min(rtsloc) : max(rtsloc)
                    c = c + 1;
                    find(rtsloc == rt);
                    rtvpw(c) = mean(Sloc(freqs,find(rtsloc == rt)));
                end
                plot(min(rtsloc) : max(rtsloc),rtvpw ./ max(rtvpw),'r')
                hold on
                
                rtvpw = [];
                c = 0;
                for rt = min(rtside) : max(rtside)
                    c = c + 1;
                    find(rtside == rt);
                    rtvpw(c) = mean(Side(freqs,find(rtside == rt)));
                end
                plot(min(rtside) : max(rtside),rtvpw ./ max(rtvpw),'b')
                
            elseif Args.rules == 1
                rtvpw = [];
                
                c = 0;
                for rt = min(rtside) : max(rtside)
                    c = c + 1;
                    
                    %                     if ~isempty(find(rtside == rt))
                    rtvpw(c) = mean(Side(freqs,find(rtside == rt)));
                    %                     else
                    %                         rtvpw(c) = 0;
                    %                     end
                end
                
                plot([min(rtside):max(rtside)],rtvpw ./ max(rtvpw))
                
                %                                 plot([min(rtside):max(rtside)],smooth(rtvpw,5) ./ max(rtvpw))
                %
                %                 pp = struct('tapers',[2 3],'Fs',1000,'fpass',[1 100]);
                %                 [Ss,ff] = mtspectrumc(smooth(rtvpw,5),pp);
                %
                %                 plot(ff,Ss,'r')
                hold on
            else
                rtvpw = [];
                
                c = 0;
                for rt = min(rtsloc) : max(rtsloc)
                    c = c + 1;
                    
                    %                     if ~isempty(find(rtside == rt))
                    rtvpw(c) = mean(Sloc(freqs,find(rtsloc == rt)));
                    %                     else
                    %                         rtvpw(c) = 0;
                    %                     end
                end
                
                plot([min(rtsloc):max(rtsloc)],rtvpw ./ max(rtvpw),'r')
                hold on
            end
            
        end
        
        
    end
    ['/media/raid/data/monkey/betty' filesep alldays{d} filesep 'session01']
end
cd(sesdir)
