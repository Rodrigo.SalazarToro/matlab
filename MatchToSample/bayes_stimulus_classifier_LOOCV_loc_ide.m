function bayes_stimulus_classifier_LOOCV_loc_ide(varargin)

%WORKS RUN ON 090701 (total 53%)

%run at session level
%computes LOOCV
Args = struct('ml',0,'mi_cutoff',0,'cor_range',[],'phase_range',[]);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%get stimulus list
%locations
loc_stimulus = stimulus_list('ml','loc');
%identities
stimulus = stimulus_list('ml','ide');

if Args.ml
    delay = corr_phase_list('ml');
else
    delay = corr_phase_list;
end
cd([sesdir filesep 'lfp' filesep 'lfp2']);


trial_corrcoefs= delay.trial_corrcoefs;
trial_phases = delay.trial_phases;


if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    [num_trials,num_points] = size(trial_corrcoefs{pair_list(1)});
else
    num_pairs = size(trial_corrcoefs,2);
    [num_trials,num_points] = size(trial_corrcoefs{1});
    pair_list = [1 : num_pairs];
end


if isempty(Args.cor_range)
    cor_range = [0:.1:1];
else
    cor_range = Args.cor_range;
end
cor_range = single(cor_range);
c_range = size(cor_range,2);

if isempty(Args.phase_range)
    ph_range = [-50:1:50];
else
    ph_range = Args.phase_range;
end
ph_range = single(ph_range);
s_phase = size(ph_range,2);

%run for each location

%set up trial list
for xl = 1 : 3;
    loc_trials{xl} = find(loc_stimulus == xl);
end


for locations = 1 : 3
    
    trials = loc_trials{locations};
    
    all_joint_cond = cell(1,num_pairs);
    stim_mmi = cell(1,3);
    leave_predict = [];
    correct_stims = zeros(1,3);
    correct_timepoint = zeros(1,num_points);
    correct_stimulus_maps = cell(1,3);
    correct_index = zeros(1,num_trials);
    all_pair_predictions = zeros(1,num_pairs);
    m = 0;
    for leave = trials
        %make distributions of conditional probabilities P(r|s)
        all_joint_cum = cell(1,num_pairs);
        pair_counter = 0;
        for p = pair_list
            pair_counter = pair_counter + 1;
            joint_cond_dist = cell(3,9);
            joint_cum_dist = cell(1,9);
            
            %runs first time through only
            if isempty(all_joint_cond{pair_counter})
                %calculate conditional distributions for all trials
                for t = trials
                    for tp = 1 : num_points
                        
                        point = single(abs(trial_corrcoefs{p}(t,tp)));
                        po = [];
                        if point >= min(cor_range) && point <= max(cor_range)
                            [~,po]=min(abs(cor_range - point));
                        end
                        
                        phase_point = single(trial_phases{p}(t,tp));
                        ph = [];
                        if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                            [~,ph]=min(abs(ph_range - phase_point));
                        end
                        
                        if isempty(joint_cond_dist{stimulus(t),tp})
                            joint_cond_dist{stimulus(t),tp} = zeros(s_phase,c_range);
                        end
                        
                        if ~isempty(ph) && ~isempty(po)
                            joint_cond_dist{stimulus(t),tp}(ph,po) = joint_cond_dist{stimulus(t),tp}(ph,po) + 1;
                        end
                    end
                end
                all_joint_cond{pair_counter} = joint_cond_dist;
            end
            
            
            %gets joint conditional distribution with all trials
            joint_cond_dist = all_joint_cond{pair_counter};
            
            %remove information from the trial to be left out
            for tp = 1 : num_points
                
                point = single(abs(trial_corrcoefs{p}(leave,tp)));
                po = [];
                if point >= min(cor_range) && point <= max(cor_range)
                    [~,po]=min(abs(cor_range - point));
                end
                
                phase_point = single(trial_phases{p}(leave,tp));
                ph = [];
                if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                    [~,ph]=min(abs(ph_range - phase_point));
                end
                
                joint_cond_dist{stimulus(leave),tp}(ph,po) = joint_cond_dist{stimulus(leave),tp}(ph,po) - 1; %subtract 1 from the conditional distribution
            end
            
            
            for stims = 1 : 3 % 3 or 9
                for tp = 1 : num_points
                    if isempty(joint_cum_dist{1,tp})
                        joint_cum_dist{1,tp} = zeros(s_phase,c_range);
                    end
                    
                    joint_cond_dist{stims,tp} = joint_cond_dist{stims,tp} ./ sum(nansum(joint_cond_dist{stims,tp}));
                    
                    
                    prob_sample = 1/3;
                    
                    
                    
                    joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
                end
            end
            all_joint_cum{pair_counter} = joint_cum_dist;
            leave_all_joint_cond{pair_counter} = joint_cond_dist;
        end
        
        pair_counter = 0;
        mutual_info = zeros(size(pair_list,2),9);
        predictions = zeros(size(pair_list,2),9);
        probs = zeros(size(pair_list,2),9);
        for nump = pair_list
            pair_counter = pair_counter + 1;
            
            stim_probabilities = zeros(3,9);
            
            
            for ntime = 1 : num_points
                
                point = single(abs(trial_corrcoefs{nump}(leave,ntime)));
                po = [];
                if point >= min(cor_range) && point <= max(cor_range)
                    [~,po]=min(abs(cor_range - point));
                end
                
                phase_point = single(trial_phases{nump}(leave,ntime));
                ph = [];
                if phase_point >= min(ph_range) && phase_point <= max(ph_range)
                    [~,ph]=min(abs(ph_range - phase_point));
                end
                
                cum_prob = all_joint_cum{pair_counter}{1,ntime}(ph,po);
                for stims = 1 : 3
                    cond_prob = leave_all_joint_cond{pair_counter}{stims,ntime}(ph,po);
                    
                    
                    prob_sample = 1/3;
                    
                    
                    %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                    if ~isempty(cum_prob) && ~isempty(cond_prob)
                        stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                    end
                end
                %get rid of nans
                stim_probabilities(isnan(stim_probabilities)) = 0;
                
                if ~isempty(cum_prob)
                    %make sure all conditional probabilities ~= 0 along with the cumulative probability
                    if cum_prob ~= 0 && sum(stim_probabilities(:,ntime) ~= 0) == 3
                        post_probs = stim_probabilities(:,ntime)';
                        mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                        mutual_info(pair_counter,ntime) = mi;
                        
                        if mi >= Args.mi_cutoff
                            [time_prob,predict] = max(post_probs);
                            predictions(pair_counter,ntime) = predict;
                            probs(pair_counter,ntime) = time_prob;
                        end
                    end
                end
            end
        end
        
        %determine best match
        b = zeros(1,3);
        for xs = 1 : 3;
            b(xs) = sum(mutual_info(find(predictions  == xs)));
        end
        [~,pre] = max(b);
        
        
        leave_predict(leave) = pre;
        
        
        if pre == stimulus(leave)
            m = m + 1;
        end
        
        leave
        (m/leave) * 100
        
    end
    performance(locations) = (m/leave) * 100;
end

save bsc_loocv_loc_ide performance
cd(sesdir)
