function [allrdiff, allpvalues,indexes,alltrials] = mkCompMTSFix(Obj,varargin)
% FixTfixSnotCombined option not yet made


Args = struct('FixTfixSnotCombined',0,'plotSig',0,'save',0,'redo',0,'rule',1,'parfor',0,'mintrials',50);
Args.flags = {'FixTfixSnotCombined','plotSig','save','redo','parfor'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});



[r,Loc3] = get(Obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 3 '>=' 1},'delay','cont');
[r,Loc4] = get(Obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 4 '>=' 1},'delay','cont');
[r,Obj3] = get(Obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 3 '>=' 1},'delay','cont');
[r,Obj4] = get(Obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 4 '>=' 1},'delay','cont');


tune = {'Obj' 'Loc'};

c = 1 ;
indexes= [];
params = struct('tapers',[2 3],'Fs',200,'trialave',1);

if Args.rule ==1; prefix = 'IDE'; else prefix = 'LOC'; end
for tu = 1 : 2
    
    for p = [4 3]
        sel = eval(sprintf('%s%d',tune{tu},p));
        for ii = sel
            pairg = Obj.data.Index(ii,10:11);
            
            cd(Obj.data.setNames{ii})
            if ~isempty(nptDir('session01')) && ~isempty(nptDir('session03'))
                cd tuning
                stim = eval(sprintf('Obj.data.%s%sSeq(ii,p,3)',prefix,tune{tu}));
                thefile = sprintf('compg%04.0fg%04.0fRule%d%s%dvsFixTS.mat',pairg(1),pairg(2),Args.rule,tune{tu},stim);
                
                if Args.redo || isempty(nptDir(thefile))
                    for s = [1 3]
                        cd(Obj.data.setNames{ii})
                        
                        cd(sprintf('session0%d',s))
                        info = NeuronalCHAssign;
                        channels = find(ismember(info.groups,pairg) == 1);
                        mts =mtstrial('auto');
                        
                        if s == 1
                            trials = mtsgetTrials(mts,sprintf('iCue%s',tune{tu}),stim,'stable','BehResp',1,modvarargin{:});
                            [data,lplength] = lfpPcut(trials,channels,'fromFiles');
                            trialsF{s} = mtsgetTrials(mts,'CueObj',55);
                            [data1,lplength] = lfpPcut(trialsF{s},channels,'fromFiles');
                        else
                            trialsF{s} = mtsgetTrials(mts,'CueObj',55);
                            [data1,lplength] = lfpPcut(trialsF{s},channels,'EPremoved');
                        end
                        
                        if s ==1
                            dataF = data1;
                        else
                            for p = 1 : 4
                                dataF{p} = cat(2,dataF{p},data1{p});
                            end
                        end
                        clear data1
                        cd ..
                    end
                    clear rdiff pvalues C
                    if Args.parfor
                        eval('parfor p = 1 : 4; [rdiff(:,p),pvalues(:,:,p),levelst{p},ft{p},C{p}] = compareTwoGroupCoh(dataF{p},data{p},params,modvarargin{:}); end')
                        levels = levelst{1};
                        f = ft{1};
                    else
                        eval('for p = 1 : 4; [rdiff(:,p),pvalues(:,:,p),levels,f,C{p}] = compareTwoGroupCoh(dataF{p},data{p},params,modvarargin{:}); end')
                        
                    end
                    if Args.save
                        cd tuning
                        save(thefile,'rdiff','pvalues','levels','f','C','trials','trialsF');
                        display(sprintf('saving %s/%s',pwd,thefile))
                        cd ..
                    end
                else
                    cd(Obj.data.setNames{ii})
                    cd tuning
                    load(thefile);
                    cd ..
                end
                n = 0;
                for s = 1 : length(trialsF); n = n + length(trialsF{s}); end
                alltrials(c,1) = n;
                alltrials(c,2) = length(trials);
                if (sum(alltrials(c,:) > Args.mintrials) == 2) && isempty(find(ii == indexes))
                    allrdiff(c,:,:) = rdiff;
                    allpvalues(c,:,:,:) = pvalues;
                    
                    c = c + 1;
                    indexes = [indexes ii];
                end
                
            end
        end
    end
end

%% stats comparison with fix

if Args.plotSig
    allrdiff =filtfilt(repmat(1/2,1,2),1,allrdiff);
    allpvalues = filtfilt(repmat(1/2,1,2),1,allpvalues);
    epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
    figure
    for p = 1 : 4
        
        subplot(1,4,p)
        plot(f,100*sum(squeeze(allrdiff(:,:,p)) > squeeze(allpvalues(:,:,5,p)),1) / size(allrdiff,1))
        hold on
        subplot(1,4,p)
        plot(f,-100*sum(squeeze(allrdiff(:,:,p)) < squeeze(allpvalues(:,:,1,p)),1) / size(allrdiff,1))
        axis([10 32 -5 20])
        title(epochs{p})
    end
    
    xlabel('Frequency [Hz]')
    ylabel('% sign. different with fix')
    
    fbeta = find(f>=13 & f<30);
    
    for p = 3 : 4
        sigdiff = squeeze(allrdiff(:,:,p)) < squeeze(allpvalues(:,:,1,p));
        
        [row,col] = find(sigdiff(:,fbeta) == 1);
        nsig{p-2} = unique(row);
        
    end
    length(union(nsig{1},nsig{2}))
end
