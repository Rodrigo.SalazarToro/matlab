function manual_ch_reject

%run at day level
%adds channels that were rejected by visual inspection(manually_rejected_channels.mat)
%to the list of rejected trials made by ArtRemTrialsLFP.

sesdir = pwd;

cd ..
load manually_rejected_channels.mat
cd(ses)

N = NeuronalHist('bmf');

%get list of rejected channels
[~,mrch] = intersect(N.gridPos,manually_rejected_channels);

cd('lfp')
if exist('rejectedTrials.mat')
    load rejectedTrials.mat
    
    %save the original calculations so that artifact detection does not
    %have to be re-run if changes are made to the rejected channels
    save original_rejectedTrials rejecTrials PWTrials rejecTrials rejectCH stdTrials
    
    %remove channel from PWTrials and stdTrials.
    %if any trials were rejected because of a manually rejected channel check all other channels and remove from
    %rejectCH if there are no other reasons to reject that trial
    
    %run through PWTrials and stdTrials remove trials if ch is bad otherwise make a list
    rj_trials = [];
    for ch = 1 : N.chnumb
        
        if ~isempty(intersect(mrch,ch))
            PWTrials{ch} = [];
            stdTrials{ch} = [];
        end
        
        rj_trials = [rj_trials PWTrials{ch} stdTrials{ch}];
    end
else
    fprintf(1,'Run artifact detection before running manual_ch_reject.m \n')
end

rejecTrials = unique(rj_trials);
rejectCH = union(rejectCH,mrch');

%overwrite original files with the changes made from manual rejection
save rejectedTrials PWTrials rejecTrials rejectCH stdTrials

cd(sesdir)








