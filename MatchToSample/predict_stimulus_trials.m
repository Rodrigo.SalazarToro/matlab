function predict_stimulus_trials

cd('/media/MONTY/data/clark/060602/session02/lfp/lfp2')
cd ../..
mt = mtstrial('auto');

total_trials = mtsgetTrials(mt,'BehResp',1,'stable');
for q = total_trials

        stim = stim+1;
        ind = mtsgetTrials(mt,'BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object));
        for q = ind
            counter = counter + 1;
            response(:,1) = x((18:34));
            response(:,2) = ones(1,17);
            
            B = glmfit(time,response,'binomial','link','logit');
            
            input = (B(1) + time * (B(2)));
            
            %convert to probability
            input = 1 ./ (1 + exp(-input));
        end
    end
end








