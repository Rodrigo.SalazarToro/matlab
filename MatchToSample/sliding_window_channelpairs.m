function sliding_window_channelpairs(varargin)

%computes sliding window crosscorrelation analysis

Args = struct('ml',0,'redo_channelpairs',0,'window',200,'step',50,'lag',50,'bmf',0);
Args.flags = {'ml','redo_channelpairs','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
        N = NeuronalHist('ml','bmf');
        groups = N.gridPos;
        [~,chpairlist] = bmf_groups;
        sortedpairs = 1 : size(chpairlist,1);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        N = NeuronalHist('ml');
        groups = N.gridPos;
        [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
    end
else
    mt=mtstrial('auto','redosetNames');
    N = NeuronalHist;
    groups = N.gridPos;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
chnumb = size(groups,2);

cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;

lfpdata = nptDir('*_lfp2.*');
trials_all = size(lfpdata,1);

total_pairs = nchoosek(chnumb,2);

for pair_number = sortedpairs
    c1 = chpairlist(pair_number,1);
    c2 = chpairlist(pair_number,2);
    
    fprintf(1,[num2str(pair_number) '  of  ' num2str(total_pairs)])
    fprintf(1,'\n')
    
    %check to see if the channel_pair has already been written
    if Args.ml
        if Args.bmf
            channel_pairs = [ 'channelpair' num2strpad(groups(c1),3) num2strpad(groups(c2),3)]
        else
            channel_pairs = [ 'channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
        end
    else
        channel_pairs = [ 'channelpair' num2strpad(c1,2) num2strpad(c2,2)]
    end
    
    if isempty(nptDir([channel_pairs '.mat'])) || Args.redo_channelpairs
        corrcoefs=[]; phases=[]; correlograms={};
        for x = 1 : trials_all
            cd(lfp2dir)
            %get avg_correlograms
            if Args.ml
                if Args.bmf
                    cd('identity')
                else
                    if mt.data.Index(x,1) == 1
                        cd('identity');
                    elseif mt.data.Index(x,1) == 2
                        cd('location')
                    end
                end
            end
            load all_correlograms
            cd(lfp2dir)
            load (lfpdata(x).name,'data');
            
            %cut at sampleon - 499
            data = data(:,((sample_on(x)-499) : end));
            
            %calculate number of bins
            steps = floor((size(data,2) - Args.window)/Args.step);
            start =  1:Args.step:steps*Args.step+1;
            endd = start+Args.window-1;
            r = GrayXxcorr(data(c1,:),data(c2,:),Args.window,Args.step,Args.lag,start,endd);
            pos_or_neg = all_corrcoef{5,pair_number}; %use the delay period average correlogram
            
            %if the average correlogram has a negative peak that is closest to zero
            %then invert the correlograms to find the negative peaks that are
            %closest to zero
            if pos_or_neg < 0
                r = r * -1;
                inverted = 1;
            else
                inverted = 0;
            end
            lags = [(-1*Args.lag): Args.lag];
            
            %find closest peak to zero lag
            p=zeros(size(r,1),2);
            for kk=1:size(r,1)
                rr=r(kk,:);
                
                %Find index of central positive peak
                peak = findpeaks(rr);
                for ppp = 1 : size(peak.loc,1)
                    if (rr(peak.loc(ppp)) < 0)
                        %set to max phase index
                        peak.loc(ppp) = length(lags);
                    end
                end
                [phase, index] = (min(abs(lags(peak.loc))));
                %get real phase value by indexing the location in the lags
                %get corr coef by indexing the correlogram
                peak_phase = lags(peak.loc(index));
                peak_corrcoef = rr(peak.loc(index));
                
                if size(peak.loc,1) > 0
                    %Stores the correlation coef and lag position.
                    p(kk,2) = peak_phase;
                    p(kk,1) = peak_corrcoef;
                else
                    p(kk,1) = nan;
                    p(kk,1) = nan;
                end
            end
            %if the correlograms are inverted then make the corrcoefs
            %negative to correspond to their actual values
            if inverted == 1
                p(:,1) = p(:,1) * -1;
                r = r * -1; %reverse correlogram back
            end
            %%coefs = the correlation coefs for the trial pair
            coefs=(p(:,1));
            %%phases = the phases
            phase=(p(:,2));
            %Find the length of the coefs column.
            cc = length(coefs);
            dd = length(phase);
            %Determines how many buffer NaN's are necessary
            ccc = (4000/Args.step) - cc; %3000/windowStep determines #of bins
            ddd = (4000/Args.step) - dd;
            %make sure there are only 50 bins
            if ccc >= 0
                %Creates NaN buffer
                cccc = NaN(1,ccc)';
                dddd = NaN(1,ddd)';
                %%Concatenates NaN buffer on to the end of the column in order
                %%for the matrix dimensions to match
                coefs = cat(1,coefs,cccc);
                phase = cat(1,phase,dddd);
            else
                coefs = coefs((1:end+ccc),1);
                phase = phase((1:end+ddd),1);
            end
            
            %%makes a matrix with all trials correlation coef for the
            %%specific pair
            corrcoefs = cat(2,corrcoefs, coefs);
            phases = cat(2,phases, phase);
            
            %places r in the cell location corresponding to the trial number
            correlograms{x} = r;
        end
        
        if Args.ml
            if Args.bmf
                channel_pairs = [ 'channelpair' num2strpad(groups(c1),3) num2strpad(groups(c2),3)]
            else
                channel_pairs = [ 'channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
            end
        else
            channel_pairs = [ 'channelpair' num2strpad(c1,2) num2strpad(c2,2)]
        end
        
        write_info = writeinfo(dbstack);
        
        save(channel_pairs,'corrcoefs','phases','correlograms','write_info')%,'pair_thresholds')
    end
end

cd(sesdir)
