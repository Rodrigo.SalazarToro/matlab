function write_power_trials_lfp2(varargin)


%run at monkey level
%computes power for 7 different time windows using detrended lfp sampled at 1KHz.

Args = struct('ml',0,'days',[],'sessions',[],'fft',0);
Args.flags = {'ml','fft'};
Args = getOptArgs(varargin,Args);

params = struct('tapers',[2 3],'Fs',1000,'fpass',[0 150]);
monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    
    for s = Args.sessions;
        cd(daydir)
        if exist(['session0' num2str(s)])
            cd ([daydir filesep 'session0' num2str(s)]);
            sesdir = pwd;
            mt = mtstrial('auto','ML','RTfromML','redosetNames');
            sample_on = floor(mt.data.CueOnset); %sample on
            sample_off = floor(mt.data.CueOffset); %sample off
            match = floor(mt.data.MatchOnset);    %match
            
            cd('lfp/lfp2')
            lfp2dir = pwd;
            mkdir('power')
            
            lfptrials = nptDir('*_lfp2.*');
            ntrials = size(lfptrials,1);
            %make data matrix
            for nt = 1 : ntrials
                cd(lfp2dir)
                s_on = sample_on(nt);
                s_off = sample_off(nt);
                m = match(nt);
                data = load(lfptrials(nt).name);
                data = data.data;
                data = lfp_detrend(data');
                data = data';
                
                
                fix = data(:,[(s_on - 399) : (s_on)])';
                sample = data(:,[(s_off - 399) : (s_off)])';
                delay400 = data(:,[(s_off) : (s_off + 399)])';
                delay800 = data(:,[(s_off + 401) : (s_off + 800)])';
                delay = data(:,[(s_off + 201) : (s_off + 800)])';
                delaymatch = data(:,[(m - 399) : (m)])';
                fulltrial = data(:,[(s_on - 500) : (m)])';
                
                if Args.fft
                    [Sfix,ffix] = get_fft(fix);
                    [Ssample,fsample] = get_fft(sample);
                    [Sdelay400,fdelay400] = get_fft(delay400);
                    [Sdelay800,fdelay800] = get_fft(delay800);
                    [Sdelay,fdelay] = get_fft(delay);
                    [Sdelaymatch,fdelaymatch] = get_fft(delaymatch);
                    [Sfulltrial,ffulltrial] = get_fft(fulltrial);
                else
                    [Sfix,ffix] = mtspectrumc(fix,params);
                    [Ssample,fsample] = mtspectrumc(sample,params);
                    [Sdelay400,fdelay400] = mtspectrumc(delay400,params);
                    [Sdelay800,fdelay800] = mtspectrumc(delay800,params);
                    [Sdelay,fdelay] = mtspectrumc(delay,params);
                    [Sdelaymatch,fdelaymatch] = mtspectrumc(delaymatch,params);
                    [Sfulltrial,ffulltrial] = mtspectrumc(fulltrial,params);
                end
                
                power.S_fix = Sfix';
                power.S_sample = Ssample';
                power.S_delay400 = Sdelay400';
                power.S_delay800 = Sdelay800';
                power.S_delay = Sdelay';
                power.S_delaymatch = Sdelaymatch';
                power.S_fulltrial = Sfulltrial';
                
                power.f_fix = ffix';
                power.f_sample = fsample';
                power.f_delay400 = fdelay400';
                power.f_delay800 = fdelay800';
                power.f_delay = fdelay';
                power.f_delaymatch = fdelaymatch';
                power.f_fulltrial = ffulltrial';
                
                if Args.fft
                    trial2 = [lfptrials(nt).name(1:end-9) '_fftpower.' num2strpad(nt,4) '.mat'];
                else
                    trial2 = [lfptrials(nt).name(1:end-9) '_power.' num2strpad(nt,4) '.mat'];
                end
                
                cd([lfp2dir filesep 'power'])
                save(trial2,'power')
            end
        end
    end
end

cd(monkeydir)