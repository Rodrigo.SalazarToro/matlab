function compareEyesAndEvents(t)


eye1=54  %26
eye2=55  %27




%get directory for trial files
sessionDir = uigetdir('','Choose a session directory');
cd(sessionDir)

%read bhv file
dirlist = nptdir('*.bhv')
bhv = bhv_read(dirlist(1).name);

%get trial names
dirlist=nptDir('*.0001')
[p,n,e]= fileparts(dirlist(1).name);

dirlist = nptdir([n '.*'])
for tt=1:length(dirlist)
    ttML=1/bhv.AnalogInputFrequency:1/bhv.AnalogInputFrequency:size(bhv.AnalogData{tt}.EyeSignal,1)/bhv.AnalogInputFrequency;
    plot(ttML,bhv.AnalogData{tt}.EyeSignal(:,1))
    hold on
    plot(ttML,bhv.AnalogData{tt}.EyeSignal(:,2))
    
    %read trial file
[data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(dirlist(tt).name);   
ttNLX = 1/sampling_rate:1/sampling_rate:size(data,2)/sampling_rate;
plot(ttNLX,data(eye1,:)/1000,'r')
plot(ttNLX,data(eye2,:)/1000,'r')
         





end
    
