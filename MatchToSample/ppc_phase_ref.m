function ppc_phase_ref(varargin)




Args = struct('ref',[],'grid',0,'make_contour',0,'sim',0);
Args.flags = {'grid','make_contour','sim'};
Args = getOptArgs(varargin,Args);

%  c = mtscpp2('auto','ml')
load cpp_b
% cpp_b = c

%PF grid
pf_order = [ ...
    0; 1; 2; 3; 4; 5;
    0; 6; 7; 8; 9;10;
    11;12;13;14;15;16;
    17;18;19;20;21;22;
    0;23;24;25;26;27;
    0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    0;33;34;35;36;37;
    0;38;39;40;41;42;
    43;44;45;46;47;48;
    49;50;51;52;53;54;
    0;55;56;57;58;59;
    0;60;61;62;63;64];

%get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
            if cch == Args.ref
                xyref = xy(cch,:);
            end
        end
    end
end

[npp,pp_pairs] = get(cpp_b,'Number','ml','pp','ips',1);

%find pairs that have ref channel as first channel
r1 = find(cpp_b.data.Index(pp_pairs,23) == Args.ref);

%find pairs that have ref channel as second channel and reverse
r2 = find(cpp_b.data.Index(pp_pairs,24) == Args.ref);
mult(pp_pairs(r1)) = 1;
mult(pp_pairs(r2')) = -1;

pp_pairs = [pp_pairs(r1) pp_pairs(r2)];


npp = size(pp_pairs,2);

if Args.grid
    mphangle = cell(13,13);
% % % % % % % % % % % % % %     figure
% % % % % % % % % % % % % %     for ch = 33 : 64
% % % % % % % % % % % % % %         if ch == Args.ref
% % % % % % % % % % % % % %             scatter(xy(ch,1),xy(ch,2),200,[1 0 0],'filled')
% % % % % % % % % % % % % %             hold on
% % % % % % % % % % % % % %         else
% % % % % % % % % % % % % %             scatter(xy(ch,1),xy(ch,2),100,[0 0 0],'filled')
% % % % % % % % % % % % % %             hold on
% % % % % % % % % % % % % %         end
% % % % % % % % % % % % % %     end
% % % % % % % % % % % % % % 
% % % % % % % % % % % % % %     hold on
    for x = 1 : npp
        g1 = xy(cpp_b.data.Index(pp_pairs(x),23),:);
        g2 = xy(cpp_b.data.Index(pp_pairs(x),24),:);
        
        
        if size(intersect(g1,xyref),2) == 2
            ref = g2;
        else
            ref = g1;
        end
        
        
        
        ph_angle = cpp_b.data.Index(pp_pairs(x),50); %match locked , also try using the real correlogram lag
        peak = cpp_b.data.Index(pp_pairs(x),57);
        
%         if abs(peak) > .1
            if isempty(mphangle{ref(1),ref(2)})
                mphangle{ref(1),ref(2)} = ph_angle * (pi/180) * mult(pp_pairs(x));
            else
                mphangle{ref(1),ref(2)} = [mphangle{ref(1),ref(2)} (ph_angle * (pi/180) * mult(pp_pairs(x)))];
            end
%         end
        %convert angle to radian
        ph_angle = ph_angle * (pi/180) * mult(pp_pairs(x));
        
        %draw line with hypotenuse = .5
        xx = cos(ph_angle) * .75 * abs(peak);
        yy = sin(ph_angle) * .75 * abs(peak);
        
        
% % % % % % % % % % % % % % % % % % % % % % % % %         plot([ref(1) ref(1)+xx],[ref(2) ref(2)+yy],'color','r','LineWidth',1)
% % % % % % % % % % % % % % % % % % % % % % % % %         hold on
    end
    figure
    for ch = 33 : 64
        if ch == Args.ref
            scatter(xy(ch,1),xy(ch,2),200,[1 0 0],'filled')
            hold on
        else
            scatter(xy(ch,1),xy(ch,2),100,[0 0 0],'filled')
            hold on
        end
    end

    for r = 1:13
        for c = 1:13
            if ~isempty(mphangle{r,c})

                if size(mphangle{r,c},2) > 1
                    mean_angle = atan2(mean(sin(mphangle{r,c})),mean(cos(mphangle{r,c})));
                else
                    mean_angle = mphangle{r,c};
                end
                mean_angle = mean_angle + (pi/2);
                xx = cos(mean_angle) * .5;
                yy = sin(mean_angle) * .5;
                
                plot([r r+xx],[c c+yy],'color','r','LineWidth',3)
                hold on
            end
        end
    end
end

if Args.make_contour
    
    %     for ch = 33 : 64
%         if ch == Args.ref
%             scatter(xy(ch,1),xy(ch,2),200,[1 0 0],'filled')
%             hold on
%         else
%             scatter(xy(ch,1),xy(ch,2),100,[0 0 0],'filled')
%             hold on
%         end
%     end
    
    mphangle = cell(13,13);
    
    %calculate mean phase angle
    hold on
    for x = 1 : npp
        g1 = xy(cpp_b.data.Index(pp_pairs(x),23),:);
        g2 = xy(cpp_b.data.Index(pp_pairs(x),24),:);
        
        
        if size(intersect(g1,xyref),2) == 2
            ref = g2;
        else
            ref = g1;
        end
        
        
        
        ph_angle = cpp_b.data.Index(pp_pairs(x),50); %match locked , also try using the real correlogram lag
        peak = cpp_b.data.Index(pp_pairs(x),57);
        
        
        if isempty(mphangle{ref(1),ref(2)})
            mphangle{ref(1),ref(2)} = ph_angle * (pi/180) * mult(pp_pairs(x));
        else
            mphangle{ref(1),ref(2)} = [mphangle{ref(1),ref(2)} (ph_angle * (pi/180) * mult(pp_pairs(x)))];
        end
        
        
        %convert angle to radian
        ph_angle = ph_angle * (pi/180) * mult(pp_pairs(x));
        
        %draw line with hypotenuse = .5
        xx = cos(ph_angle) * .75 * abs(peak);
        yy = sin(ph_angle) * .75 * abs(peak);
        
        
%         plot([ref(1) ref(1)+xx],[ref(2) ref(2)+yy],'color','r','LineWidth',1)
%         hold on
    end
    
    
    contour_grid = [];
    for r = 1:13
        for c = 1:13
            if ~isempty(mphangle{r,c})
                if size(mphangle{r,c},2) > 1
                    mean_angle = atan2(mean(sin(mphangle{r,c})),mean(cos(mphangle{r,c})));
                else
                    mean_angle = mphangle{r,c};
                end
                contour_grid(c,r-7) = mean_angle;
            end
        end
    end
    
    izero = find(contour_grid == 0);
    contour_grid = cos(contour_grid);
    contour_grid(izero) = 0;
    contour_grid = rot90(flipud(contour_grid),3);
% contour_grid = (flipud(contour_grid));
    
    %flip back
     contourf(contour_grid)
     
    
%     if ~isempty(contour_grid)
%         imagesc(((contour_grid((3:6),:)))); colorbar
%     end
end


if Args.sim
    
    f = 15;
    %time
    times = [0:.001:.5];
    ntimes = size(times,2);
    s1 = 1 * sin(2*pi*f*times);
    for t = 100 : ntimes-100
        gridv = [];
        %make reference wave 1
        ref_time = s1(t)
        
        for x = 1 : npp
            g1 = xy(cpp_b.data.Index(pp_pairs(x),23),:);
            g2 = xy(cpp_b.data.Index(pp_pairs(x),24),:);
            
            
            if size(intersect(g1,xyref),2) == 2
                ref = g2;
            else
                ref = g1;
            end
            
            
            
            ph_angle = cpp_b.data.Index(pp_pairs(x),55); %match locked , also try using the real correlogram lag
            
            
            %convert angle to radian
            %             ph_angle = ph_angle * (pi/180) * mult(pp_pairs(x));
            
            delt_t = round((ph_angle / (360 * 15)) * 1000);
            
            gridv(ref(2),ref(1)-8) = s1(t + delt_t);
                
            hold on
        end
        gridv(xyref(2),xyref(1)-8) = s1(t);
        
        gridv(6,4) = 1;
        
        imagesc(gridv)
        
        F(t)= getframe(gcf);
        
    end
end



