#called by glm_delay.m
#output
#	DnD test for interaction

#	Coefficients for interaction model
#	Coefficients converted to probability

#uses quasibinomial distributions
library(faraway)
options(scipen=999) #suppresses scientific notation

#get data
stims.data <- read.csv("glm_location.csv", head=TRUE)
attach(stims.data)

#make names for columns
TIME = X
LOCATIONS = X.1
CROSS = X.2
TOTAL = X.3

#make stims a factor
LOCATIONS = factor(LOCATIONS)

#make response
cross.y = cbind(CROSS,TOTAL-CROSS)


#make models
#fit glm model with logit link
full = glm(cross.y ~ TIME + LOCATIONS + TIME*LOCATIONS,family=quasibinomial)
summary.full = summary(full)

#get p-values for DnD tests 
a = anova(full,test="Chisq")

#p-value for DnD interaction
a$P[4]



#get coefficients for full model
#there are 6 coefficients. default stim is number STIMS1

coefficients.full <- array(1:6, dim=c(6,1))
for(i in 1:6){
coefficients.full[i]=summary.full$coef[i,1]
}

coefficients.full



#get p-vals for full model

pval.full <- array(1:6, dim=c(6,1))
for(i in 1:6){
pval.full[i]=summary.full$coef[i,4]
}

pval.full



#attributes(summary.full)
#summary.full$dispersion






















