function SNR_channels = SNR_Session_Batch

%run at the session level
concat = 0; %use to concat all trials for each channel

monkey = 'betty';
%run at day level
global mt 

    bhv = nptDir('*.bhv');
    mt = mtstrial('auto','ML','RTfromML','redo'); %passed to SignalNoiseMUA through global mt
    if isempty(nptDir('skip.txt')) && strcmp('MTS',bhv.name(1:3))
        
        %get channel information. channels = list
        [channels,comb,CHcomb,list] = checkChannels('cohInter');
        cd('highpass');
        trial_list = nptDir([monkey '*.*']);
        %get only the specified trial indices (correct and stable)
        all_trials = size(trial_list,1);
        
        if concat
            SNR = SignalNoiseMUA('Channels',list,'concat')
        else
            SNR_channels = zeros(size(all_trials,2),size(list,2)); %trial X channel
            t=0;
            for a = 1 : all_trials
                t = t + 1;
                
                trial = trial_list(a).name;
                SNR = SignalNoiseMUA('FileName',trial,'Channels',list);
                SNR_channels(t,:) = SNR.SNR_99_99;
            end
        end
        
        %make trials with zero snr == 0 instead of NaN
        SNR_channels(isnan(SNR_channels)) =0;
        
        %check for outliers wih Kurtosis, make them NaN
        for ch =1 : size(SNR_channels,2)
            [threshold,outliers,kdist] = getKurtosisThresh(SNR_channels(:,ch));
            SNR_channels(outliers,ch) = nan;
        end
        
        %channel list contains at list of the groups with corresponding SNR
        channel_snr_list(:,1) = list';
        channel_snr_list(:,2) = nanmedian(SNR_channels,1)';
        
        save SNR_channels SNR_channels channel_snr_list
        
    end



