function classify_avg_ccg_loocv(varargin)

%used to classify each trial with average correlograms
%run at the session level

Args = struct('ml',0,'sorted',0,'ide',0,'loc',0);
Args.flags = {'ml','sorted','ide','loc'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sesdir = pwd;

%get stimulus list
if Args.ide
    stimulus = stimulus_list('ml','ide');
elseif Args.loc
    stimulus = stimulus_list('ml','loc');
else
    stimulus = stimulus_list('ml');
end

if Args.ml
    c = mtscpp('auto','ml','ide_only');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
else
    c = mtscpp('auto');
    mt = mtstrial('auto','redosetNames');
end

%get trial information
if Args.ml
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
else
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end
numtrials = size(total_trials,2);

%get pair information
if Args.sorted
    [~,cpairs] = get(c,'Number','sorted'); %get list of sorted pairs
else
    [~,cpairs] = get(c,'Number');
end

m = 0;
for t = 1 : numtrials
    
    cd(sesdir)
    
    
    [stimulus_avg_ccg, trial_ccg] = make_avgccg_loocv('ml','leave_trial',t,'pairs',cpairs,'stimulus',stimulus,'real_trials',total_trials);
    
    
    
    %classify
    allp = [];
    allt = [];
    for s = 1 : max(stimulus)
        for p = 1 : size(stimulus_avg_ccg,2)
            if p == 1
                allp=[stimulus_avg_ccg{p}{s}];
                allt=[trial_ccg{p}];
            else
                
                allp=[allp stimulus_avg_ccg{p}{s}];
                allt=[allt trial_ccg{p}];
            end
        end
        all_stim_cors(s) = corr2(allp,allt);
    end
    [~,predict] = max(all_stim_cors);
    
    if stimulus(t) == predict
        m = m + 1;
    end
    
    t
    m/t
end


performance = (m/t) * 100;

cd([sesdir filesep 'lfp' filesep 'lfp2'])

if Args.ide
    save avg_ccg_loocv_performance_ide performance
elseif Args.loc
    save avg_ccg_loocv_performance_loc performance
else
    save avg_ccg_loocv_performance performance
end

cd(sesdir)





