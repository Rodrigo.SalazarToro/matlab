function glm_analysis(varargin)

%run at session level
%uses logistic regression to model delay period

%Args.model: 9stims(1),location(2),identity(3)

Args = struct('ml',0,'model',1,'threshold',1,'sorted',1);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    [groups,sorted_pairs,group_pair_list,~] = sorted_groups('ml');
    chnumb = size(groups,2);
else
    [groups,sorted_pairs,~,group_pair_list] = sorted_groups();
    chnumb = size(groups,2);
end

%get stimulus list (obj[1 1 1 2 2 2 3 3 3], loc[1 2 3 1 2 3 1 2 3])
if Args.model == 1
    if Args.ml
        stimulus = stimulus_list('ml');
    else
        stimulus = stimulus_list();
    end
elseif Args.model == 2
    stimulus = stimulus_list('ml','loc');
elseif Args.model == 3
    stimulus = stimulus_list('ml','ide');
end

cd (['lfp' filesep 'lfp2']);
lfp2dir = pwd;

%run for each trial

glm_analysis_out = cell(2,size(group_pair_list,1));
for p = sorted_pairs;
    thresh = Args.threshold;
    cd(lfp2dir)
    if Args.ml
        %Args.rule = 1 for identity and 2 for location
        [glm_output,probabilities] = cppStimuli_glm_analysis('c1',group_pair_list(p,1),'c2',group_pair_list(p,2),'threshh',thresh,'chnumb',chnumb,'ml',Args.ml,'rule',1,'pair',p,'model',Args.model,'stimulus',stimulus);
    else
        [glm_output,probabilities] = cppStimuli_glm_analysis('c1',group_pair_list(p,1),'c2',group_pair_list(p,2),'threshh',thresh,'chnumb',chnumb,'rule',1,'pair',p,'model',Args.model,'stimulus',stimulus);
    end
    
    glm_analysis_out{1,p} = glm_output;
    glm_analysis_out{2,p} = probabilities;
end

cd(lfp2dir)

if Args.ml
    cd('identity')
    if Args.model == 1
        save glm_analysis_all glm_analysis_out
    elseif Args.model == 2
        save glm_analysis_loc glm_analysis_out
    elseif Args.model == 3
        save glm_analysis_ide glm_analysis_out
    end
else
    if Args.model == 1
        save glm_analysis_all glm_analysis_out
    elseif Args.model == 2
        save glm_analysis_loc glm_analysis_out
    elseif Args.model == 3
        save glm_analysis_ide glm_analysis_out
    end
end

cd(sesdir)


