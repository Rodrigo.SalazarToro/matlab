function [sfc,sfcSur,varargout] = mkAveSFC(obj,ind,varargin)


Args = struct('save',0,'redo',0,'sigLevel',2,'rule',1,'addNameFile',[],'plot',0,'pSFC',0,'type','onlyDelay','prctiles',[10 25 50 75 90],'cohObj',[],'cohind',[],'gram',0,'noSur',0,'minTrials',200,'minSpikes',1000);
Args.flags = {'save','redo','plot','pSFC','gram','noSur'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

r = length(ind);
count = 1;
if Args.gram
    
    gram = 'gram';
else
    gram = Args.type;
end

sdir = pwd;
epochs = {'presample' 'sample' 'delay1' 'delay2'};

matfile = sprintf('%sAveSFC.mat',Args.addNameFile);
missPair = 0;
if isempty(nptDir(matfile)) || Args.redo
    for ii = 1 : r
        
        starst = findstr(obj.data.setNames{ind(ii)},'group00');
        cd(obj.data.setNames{ind(ii)})
        if ~isempty(Args.cohObj)
            daydir = obj.data.setNames{ind(ii)}(1:findstr('session0',obj.data.setNames{ind(ii)}) -2);
            cohinds = find(cellfun(@isempty,strfind(Args.cohObj.data.setNames,daydir)) == 0);
            cohinds = intersect(cohinds,Args.cohind); % gets the indexes for selecxted inds from input
            sgroups = Args.cohObj.data.Index(cohinds,10:11);
            if ~isempty(sgroups); cont = true; else cont = false; end
        else
            cont = true;
        end
        if cont
            files = nptDir(sprintf('SFC%sall1g00*Rule%d.mat',gram,Args.rule));
            if Args.noSur
                sfiles = [1: length(files)];
            else
                %                 try
                sfiles = nptDir(sprintf('SFC%sall1g00*Rule%dSur.mat',gram,Args.rule));
                %                 catch
                %                     sfiles = nptDir(sprintf('SFCall1g00*Rule%dSur.mat',Args.rule)); % temporary solution
                %                 end
            end
            %% recover the Clfp
            [pdir,cdir] = getDataDirs('site','relative','CDNow','NoSites');
            
            if ~isempty(files) && ~isempty(nptDir('cohInter.mat'))
                load('cohInter.mat','Day')
                if isfield(Day,'comb')
                    if Args.pSFC
                        
                        
                        load cohInter.mat
                        if Day.session(1).rule == Args.rule
                            thes = 1;
                        else
                            thes = 2;
                        end
                        allClfp = Day.session(thes).C;
                        allcombs = Day.comb;
                        load generalSur.mat
                        ClfpSur = squeeze(Day.session(thes).Prob(:,4,1,:));
                        
                        cd(obj.data.setNames{ind(ii)})
                    end
                    
                    sameEl = sprintf('g%s',obj.data.setNames{ind(ii)}(starst+5:starst+8));
                    grn = str2double(obj.data.setNames{ind(ii)}(starst+6:starst+8));
                    allfiles = strvcat(files(:).name);
                    startg = strfind(allfiles(1,:), 'g00');
                    allgr = str2num(allfiles(:,startg+3:startg+4));
                    
                    if isempty(findstr('betty',pwd))
                        if grn <= 8
                            delCH = find(allgr <= 8);
                        else
                            delCH = find(allgr > 8);
                        end
                    else
                        if grn <= 32
                            delCH = find(allgr <= 32);
                        else
                            delCH = find(allgr > 32);
                        end
                        
                    end
                    if ~isempty(Args.cohObj)
                        [row,col] = find(grn == sgroups);
                        if isempty(row)
                            delCH = [1 :length(allgr)];
                        else
                            goodgr = unique(sgroups(row,:));
                            delGR = setdiff(allgr,goodgr);
                            
                            delCH = unique([delCH; find(ismember(allgr,delGR) ==1)]);
                        end
                    end
                    cd(obj.data.setNames{ind(ii)})
                    if Args.pSFC
                        sameFile = sprintf(sprintf('SFC%sall1%sRule%d.mat',gram,sameEl,Args.rule));
                        load(sameFile,'C');
                        Csame = C;
                        sameFile = sprintf(sprintf('SFC%sall1%sRule%dSur.mat',gram,sameEl,Args.rule));
                        load(sameFile,'Prob');
                        CsameSur = squeeze(Prob(:,Args.sigLevel,:));
                    end
                    files(delCH) = [];
                    
%                     sfiles(delCH) = [];
                    
                    npairs = length(files);
                    
                    for ff = 1 : npairs
                        if Args.gram
                            load(files(ff).name,'C','f','phi','time')
                            %                             imagesc(time,f,C'); caxis([0 0.1]);title(sprintf('%s %s',pwd,files(ff).name));colorbar;pause
                        else
                            load(files(ff).name,'C','f','phi','nspikes')
                            warning off
                           lfpgr = str2num(files(ff).name(19:21));
                           cd ../..
                           neuroInfo=NeuronalHist;
                           
                          
                           if isempty(findstr('clark',pwd))
                                lfparea = neuroInfo.number(find(neuroInfo.gridPos == lfpgr));
                               allgroups(count,:) = [grn lfpgr];
                               alldepths(count,1) = neuroInfo.recordedDepth(find(grn == neuroInfo.gridPos));
                               alldepths(count,2) = neuroInfo.recordedDepth(find(lfpgr == neuroInfo.gridPos));
                               alldays(count,1) = 0;
                               allunittype(count,1) = obj.data.Index(ind(ii),4);
                               nSUA(count,1) = obj.data.setNames{ind(ii)}(end-1);
                           else
                                lfparea = neuroInfo.number(find(lfpgr == allgr));
                               allgroups(count,:) = [grn lfpgr];
                               alldepths(count,1) = neuroInfo.recordedDepth(find(grn == allgr));
                               alldepths(count,2) = neuroInfo.recordedDepth(find(lfpgr == allgr));
                               alldays(count,1) = obj.data.Index(ind(ii),1);
                               allunittype(count,1) = obj.data.Index(ind(ii),4);
                               nSUA(count,1) = obj.data.setNames{ind(ii)}(end-1);
                           end
                           cd(obj.data.setNames{ind(ii)})
                        end
                        if ~Args.noSur
%                             load /Volumes/raid/data/monkey/SFCgenSurFit.mat
%                             if length(keeptrials) > Args.minTrials && nspikes > Args.minSpikes
%                             plevel = fittedmodel1(keeptrials,nspikes);
%                             
%                             Prob = repmat(plevel,65,6);
%                             else
%                                 C = [];
%                                 Prob = [];
%                             end
                            try
                            load(sfiles(ff).name,'Prob')
                            catch 
                                Prob = [];
                                missPair = missPair + 1;
                            end
                        end
                        if Args.pSFC
                            othergrn = str2double(files(ff).name(9:12));
                            cd ../..
                            neuroInfo =  NeuronalChAssign;
                            ch1 = find(grn == neuroInfo.groups);
                            ch2 = find(othergrn == neuroInfo.groups);
                            thecomb = find(ismember(allcombs,[ch1 ch2],'rows') ==1);
                            if isempty(thecomb)
                                thecomb = find(ismember(allcombs,[ch2 ch1],'rows') ==1);
                            end
                            Clfp = squeeze(allClfp(:,thecomb,:));
                            if ~isempty(Clfp) && ~isempty(Csame)
                                sfc(count,:,:) = (C - Clfp .* Csame) ./ sqrt((1 - Clfp.^2) .* (1 - Csame));
                                C = squeeze(Prob(:,Args.sigLevel,:));
                                sfcSur(count,:,:) = (C - ClfpSur .* CsameSur) ./ sqrt((1 - ClfpSur.^2) .* (1 - CsameSur));
                                phisfc(count,:,:) = mod(2*pi + phi,2*pi);
                                count = count + 1;
                            end
                            cd(obj.data.setNames{ind(ii)})
                        else
                            if ~isempty(C) && ~isempty(Prob)
                                
                                if Args.noSur
                                    sfcSur = [];
                                else
                                    
                                    try
                                        sfcSur(count,:) = squeeze(Prob(:,Args.sigLevel));
                                        sfc(count,:) = C;
                                        phisfc(count,:) = phi;
                                        sfcLFP(count) = lfparea;
                                        
                                    end
                                    
                                end
                                
                                count = count + 1;
                            end
                        end
                    end
                end
            end
        end
    end
else
    load(matfile)
end

cd(sdir)
if exist('f')
    varargout{1} = f;
end

if ~exist('sfc') || ~exist('sfcSur') || ~exist('phisfc')
    sfc = [];
    sfcSur = [];
    phisfc = [];
end
varargout{2} = phisfc;
if Args.gram
    varargout{3} = time;
    
end
if ~exist('sfcLFP')
    sfcLFP =[];
end
varargout{4} = sfcLFP;

varargout{5} = allgroups;
varargout{6} = alldepths;
varargout{7} = alldays;
varargout{8} = allunittype;
varargout{9} = nSUA;


if r ~= 0
    if Args.save
        save(matfile,'sfc','sfcSur','f','ind','phisfc')
        display(sprintf('saving %s',matfile))
    end
    
    if Args.plot
        figure
        set(gcf,'Name',matfile)
        for p = 1 : 4
            subplot(2,4,p)
            sigSFC = sfc(:,:,p) > sfcSur(:,:,p);
            plot(f,sum(sigSFC,1)/size(sigSFC,1))
            title(sprintf('%d pairs',size(sigSFC,1)))
            ylim([0 0.4])
            xlim([0 50])
            title(epochs{p})
            ylabel('fraction of significance')
            
            subplot(2,4,p+4)
            
            plot(f,squeeze(prctile(sfc(:,:,p),Args.prctiles,1)))
            ylim([0 0.05])
            xlim([0 50])
            ylabel('SFC coherency')
        end
        
    end
else
    sfc = [];
    sfcSur = [];
end
%  [r,locTuned] = get(obj,'Number','LocTuned',allcc(cc),'epochTuned',1,'tuneLog','==','stimOnset',10);
%     [r,ideTuned] = get(obj,'Number','ideTuned',allcc(cc),'epochTuned',1,'tuneLog','==','stimOnset',10);
%
%     [r,locTunedD] = get(obj,'Number','LocTuned',allcc(cc),'epochTuned',2,'tuneLog','==','stimOnset',10);
%     [r,ideTunedD] = get(obj,'Number','ideTuned',allcc(cc),'epochTuned',2,'tuneLog','==','stimOnset',10);