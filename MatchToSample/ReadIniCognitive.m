function [r,status] = ReadIniCognitive(filename)

%ReadIniCognitive Reads information from INI files from the MTS task

%   [R,STATUS] = ReadIniCognitive(FILENAME) opens FILENAME and reads 

%    information.



section = 'TMatchToSampleGUI';

section2 = 'TIMING INFO';

section3 = 'ACTUAL NUMBER OF TRIALS SHOWN';



readSet = {section,'','Section Version','i',''; ...

			section,'','Condition Index','i',''; ...

			section,'','Position','i',''; ...

			section,'','Probability Position 1','d',''; ...

			section,'','Probability Position 2','d',''; ...

			section,'','Probability Position 3','d',''; ...

			section,'','Eccentricity','i',''; ...

			section,'','Size','i',''; ...

			section,'','Object Window Size','i',''; ...

			section,'','Object 1 File Name','',''; ...

			section,'','Object 2 File Name','',''; ...

			section,'','Object 3 File Name','',''; ...

            section,'','Probability Same Object ID','d',''; ... 

			section,'','Choose Alternate Incorrect Luminance','i',''; ...

			section,'','Incorrect Object Luminance','i',''; ...

			section,'','Use Training Fix','i',''; ...

			section,'','Human Trial','i',''; ...

			section,'','Stats File Name','',''; ...

			section,'','Desired Number Trials','i',''; ...

			section,'','Percent Go Trials','i',''; ...

%             section,'','Monitor Fix During Match','i','';...

            section,'','Screen Resolution Index','i',''; ...

			section,'','Screen Resolution','s',''};

    

readSet2 = {section2,'','Section Version','i',''; ...

			section2,'','Inter-trial Interval','i',''; ...

			section2,'','Inter-trial Interval Max','i',''; ...

			section2,'','Randomize ITI','i',''; ...

			section2,'','Audio Cue Interval','i',''; ...

			section2,'','Fixation Time Limit','i',''; ...

			section2,'','Cue Latency','i',''; ...

			section2,'','Cue Latency Max','i',''; ...

			section2,'','Randomize Cue Latency','i',''; ...

			section2,'','Cue Duration','i',''; ...

			section2,'','Cue Duration Max','i',''; ...

			section2,'','Randomize Cue Duration','i',''; ...

            section2,'','Post Cue Duration','i',''; ...

			section2,'','Post Cue Duration Max','i',''; ...

			section2,'','Randomize Post Cue Duration','i',''; ...

			section2,'','Stimulus Latency','i',''; ...

			section2,'','Stimulus Latency Max','i',''; ...

			section2,'','Randomize Stimulus Latency','i',''; ...

			section2,'','Stimulus Duration','i',''; ...

			section2,'','Stimulus Duration Max','i',''; ...

            section2,'','Randomize Stimulus Duration','i','';...

            section2,'','Post Stimulus Duration','i',''; ...

			section2,'','Post Stimulus Duration Max','i','';...

            section2,'','Randomize Post Stimulus Duration','i','';...

            section2,'','Match Latency','i','';...

            section2,'','Match Latency Max','i','';...

            section2,'','Randomize Match Latency','i','';...

            section2,'','Match Time Limit','i','';...

            section2,'','Choice Duration','i','';...
            
            section2,'','Post Choice Duration','i','';...
            
            section2,'','Reward Duration','i','';...

            section2,'','Reward Duration Max','i','';...

            section2,'','Randomize Reward Duration','i','';...

            section2,'','Penalty Duration','i','';...

            section2,'','Penalty Duration Max','i','';...

            section2,'','Randomize Penalty Duration','i','';...

            section2,'','Incorrect Penalty Duration','i','';...

            section2,'','Incorrect Penalty Duration Max','i','';...

            section2,'','Randomize Incorrect Penalty Duration','i','';...

            section2,'','Eye-tracker Fixation Duration','i','';...

            section2,'','Eye-tracker Stray Duration','i',''};

    
readSet3 = {section3,'','Number Of Trials','i',''};
% set default return arguments

r = {};

status = 1;



if nargin == 0

  fprintf('Must provide filename!\n');

  return

end



% first read session info

[r0,status] = ReadIniSession(filename);



if(status)

	% there was a problem so just exit

	return

end



% now read grating info

[val,s] = inifile(filename,'read',readSet);



[val2,s2] = inifile(filename,'read',readSet2);

[val3,s3] = inifile(filename,'read',readSet3);

% check to make sure there were no problems. status should be 1 if

% there were no problems

status = isempty(find(~cellfun('isempty',s)));

status2 = isempty(find(~cellfun('isempty',s2)));

status3 = isempty(find(~cellfun('isempty',s3)));


% continue only if status is 1

if(status && status2 && status3)

	% check to make sure this is a grating session

% 	if(strcmp(val{1},'Sparse Noise') && strcmp(val{9},'Grating'))	

		% concatenate both r0 and r1 structures

		% get the fieldnames

		f0 = fieldnames(r0);

		% get rid of spaces in the field names for grating

		fnames = strrep({readSet{:,3}},' ','');

        fnames2 = strrep(strrep({readSet2{:,3}},' ',''),'-','');
        fnames3 = strrep(strrep({readSet3{:,3}},' ',''),'-','');

		% convert structures to cell arrays

		c0 = struct2cell(r0);

		% combine both cell arrays and create new structure

		r = cell2struct({c0{:} val{2:end} val2{2:end} val3{1:end}},{f0{:} fnames{2:end} fnames2{2:end} fnames3{1:end}},2);

% 	else

% 		% return error since this is not a grating session

% 		status = 1;

% 		return

% 	end

end

