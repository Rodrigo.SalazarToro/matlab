function NaiveBayes_xcorr(varargin)


%run at session level
%computes LOOCV
Args = struct('ml',0,'loc',0,'ide',0,'mi_cutoff',0,'cor_range',[],'test',0,'redo_corr_list',0,'perm',0,'sample',0,'pp',0,'pf',0,'ff',0,'fix',0,'select_pairs',[],'locations',[]);
Args.flags = {'ml','comb','loc','ide','test','redo_corr_list','perm','sample','pp','pf','ff','fix'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
c = mtscpp('auto','ml','ide_only');
num_points = 101 * 9;

%get stimulus list
if ~isempty(Args.locations)
    if Args.ml
        if Args.ide
            stimulus = stimulus_list('ml','ide','locbyide',Args.locations);
        elseif Args.loc
            stimulus = stimulus_list('ml','loc','locbyide',Args.locations);
        else
            stimulus = stimulus_list('ml','locbyide',Args.locations);
        end
    else
        if Args.ide
            stimulus = stimulus_list('ide','locbyide',Args.locations);
        elseif Args.loc
            stimulus = stimulus_list('loc','locbyide',Args.locations);
        else
            stimulus = stimulus_list('locbyide',Args.locations);
        end
    end
else
    if Args.ml
        if Args.ide
            stimulus = stimulus_list('ml','ide');
        elseif Args.loc
            stimulus = stimulus_list('ml','loc');
        else
            stimulus = stimulus_list('ml');
        end
    else
        if Args.ide
            stimulus = stimulus_list('ide');
        elseif Args.loc
            stimulus = stimulus_list('loc');
        else
            stimulus = stimulus_list;
        end
    end
end

if Args.perm
    %     stimulus=stimulus(randperm(size(stimulus,2)));
    for ps = 1 : size(stimulus,2)
        rp = randperm(9);
        rp(rp == stimulus(ps)) = [];
        stimulus(ps) = rp(1);
    end
end

if ~isempty(Args.locations)
    if Args.ml
        if Args.fix
            out = corr_phase_list('ml','fix','locations',Args.locations);
        elseif Args.sample
            out = corr_phase_list('ml','sample','locations',Args.locations);
        else
            out = corr_phase_list('ml','locations',Args.locations);
        end
    else
        if Args.fix
            out = corr_phase_list('fix','locations',Args.locations);
        elseif Args.sample
            out = corr_phase_list('sample','locations',Args.locations);
        else
            out = corr_phase_list('locations',Args.locations);
        end
    end
else
    if Args.ml
        if Args.fix
            out = corr_phase_list('ml','fix');
        elseif Args.sample
            out = corr_phase_list('ml','sample');
        else
            out = corr_phase_list('ml');
        end
    else
        if Args.fix
            out = corr_phase_list('fix');
        elseif Args.sample
            out = corr_phase_list('sample');
        else
            out = corr_phase_list;
        end
    end
end
cd([sesdir filesep 'lfp' filesep 'lfp2']);

trial_corrcoefs = out.trial_corrcoefs;
trial_phases = out.trial_phases;
trial_correlograms = out.trial_correlograms;

if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    num_trials = size(trial_corrcoefs{pair_list(1)},1);
else
    num_pairs = size(trial_corrcoefs,2);
    num_trials = size(trial_corrcoefs{1},1);
    pair_list = [1 : num_pairs];
end

if Args.ff
    [~,pair_list] = get(c,'Number','correct','stable','ff','sorted');
elseif Args.pp
    [~,pair_list] = get(c,'Number','correct','stable','pp','sorted');
elseif Args.pf
    [~,pair_list] = get(c,'Number','correct','stable','pf','sorted');
end

if ~isempty(Args.select_pairs)
    pair_list = Args.select_pairs;
end

numb_pairs = size(pair_list,2);

% all_trial_corrcoefs = cell2mat(trial_corrcoefs);

corrcoefs = zeros((num_trials*numb_pairs),9);
phases = zeros((num_trials*numb_pairs),9);
m = 0;
for leave = 1 : num_trials
    
    corrcoefs = cell2mat(trial_corrcoefs);
    phases = cell2mat(trial_phases);
    
    leave_corrcoefs  = corrcoefs(leave,:);
    leave_phases = phases(leave,:);
    
    corrcoefs(leave,:) = [];
    phases(leave,:) = [];
    
    st = stimulus;
    st(leave) = [];
    
    
%     NB = NaiveBayes.fit([corrcoefs phases],st,'Distribution','kernel');
NB = NaiveBayes.fit([corrcoefs],st,'Distribution','kernel');
    
%     pre = predict(NB,[leave_corrcoefs leave_phases]);
pre = predict(NB,[leave_corrcoefs]);
    
    clear NB
    
    if pre == stimulus(leave)
        m = m + 1;
    end
    performance = (m/leave) * 100
end
performance = (m/leave) * 100
if Args.ide
    save nb_performance_ide performance pair_list
elseif Args.loc
    save nb_performance_loc performance pair_list
else
    save nb_performance performance pair_list
end
cd(sesdir)


