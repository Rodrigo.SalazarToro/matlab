function [glm_output,leave_stim,leave_cross,probabilities]=cppStimuli_glm_loocv(varargin)


%Used by mtsCPP to view the averages percents for all stimuli for ONE
%CHANNEL PAIR

Args = struct('c1',[],'c2',[],'threshh',[],'sessionname',[],'chnumb',[],'ml',0,'pair',[],'model',[],'leave_trial',[],'stimulus',[]);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

stimulus = Args.stimulus;

if Args.ml
    cd identity
    load threshold threshold
    cd ..
else
    load threshold threshold
end

%load correlation coefficients for delay period
cpd = nptDir('corr_phase_delay.mat');
if isempty(cpd)
    cd(sesdir)
    if Args.ml
        corr_phase_list('ml')
    else
        corr_phase_list
    end
    cd([sesdir filesep 'lfp' filesep 'lfp2']);
    load corr_phase_delay
else
    %load correlation coefficients and phases
    load corr_phase_delay %compute with corr_phase_list.m
end


p_threshold = single(threshold{Args.pair}(Args.threshh));
pair_corrcoefs = abs(single(trial_corrcoefs{Args.pair}));

cross_matrix = zeros(size(pair_corrcoefs,1),size(pair_corrcoefs,2));
cross_matrix(pair_corrcoefs > p_threshold) = 1;

%leave out trial information THIS IS VERY VERY IMPORTANT
leave_stim = stimulus(Args.leave_trial);
leave_cross = cross_matrix(Args.leave_trial,:);

%% MAKE SURE TO DELETE TRIAL INFORMATION
stimulus(Args.leave_trial) = [];
cross_matrix(Args.leave_trial,:) = [];
%% 
cross = [];
probabilities = [];
for s = 1 : max(stimulus)
    trial_stim = find(stimulus == s);
    cross(s,:) = sum(cross_matrix(trial_stim,:)); %rows are stimuli
    stim_number(s) = size(trial_stim,2);
    
    probabilities(s,:) = cross(s,:)./stim_number(s);
end

%GLM

glm_output = glm_delay_loocv('stim_number',stim_number,'allcross',cross,'model',Args.model);






