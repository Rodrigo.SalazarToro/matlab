function freq_phi_relationship_xcorr


% epochs = [4,8];

epochs = [3,6];

cd /media/raid/data/monkey/betty
mdir = pwd;
load alldays
figure
counter = 0;
for ee = 1 : 2
    e = epochs(ee);
    counter = counter + 1;
    allphi = [];
    allpeak = [];
    allfreq = [];
    allpeaks = [];
    freq1 = [];
    freq2 = [];
    
    
    for d = 1:46
        if ee == 1;
            cd([mdir filesep alldays{d} '/session01/lfp/lfp2/identity'])
        else
            cd([mdir filesep alldays{d} '/session01/lfp/lfp2/incorrect'])
        end
        cd([mdir filesep alldays{d} '/session01/lfp/lfp2/identity'])
        
        load all_auto_gabors1
        load all_auto_gabors2
        load all_gabors
        
        numg = size(all_gabors,2);
        for ng = 1 : numg
            g = all_gabors{e,ng};
            if ~isempty(g)
                
                if all_gabors{5,ng}.cross_corr >= .99 && abs(all_gabors{5,ng}.peak) > .1
                    allphi = [allphi g.phase_angle_deg];
                    allpeak = [allpeak g.peak];
                    allfreq = [allfreq g.frequency];
                    allpeaks = [allpeaks g.peak];
                    freq1 = [freq1 all_auto_gabors1{e,ng}.frequency];
                    freq2 = [freq2 all_auto_gabors2{e,ng}.frequency];
                end
                
            end
        end
    end
    
    subplot(1,3,1)
    if ee == 1
        scatter((freq1-freq2),allphi,20,[1 0 0],'+')
        hold on
    else
        scatter((freq1-freq2),allphi,20,[0 0 0],'+')
        hold on
    end
    
        subplot(1,3,2)
    if ee == 1
        scatter((freq1-freq2),allpeak,20,[1 0 0],'+')
        hold on
    else
        scatter((freq1-freq2),allpeak,20,[0 0 0],'+')
        hold on
    end
    
    
        subplot(1,3,3)
    if ee == 1
        scatter((freq1-freq2),allfreq,20,[1 0 0],'+')
        hold on
    else
        scatter((freq1-freq2),allfreq,20,[0 0 0],'+')
        hold on
    end
end


