function comparePSTHrule(grUnit,cluster,varargin)



% to be run in the day folder



Args = struct('ML',0,'redo',0,'chosenFreq',1,'epoch','saccade','binSize',50,'session99',0);
Args.flags = {'ML','redo','session99'};
[Args,modvarargin] = getOptArgs(varargin,Args);

allspikes = cell(2,1);
sdir = pwd;

pathTosave = cell(2,1);
matfile = sprintf('compPSTHRules%sgr%04.0f.mat',Args.epoch,grUnit);

switch Args.epoch
    case 'presample'
        timing = [-400 0];
        ref = 1;
    case 'sample'
        timing = [100 500];
        ref = 1;
    case 'delay1'
        timing = [500 900];
        ref = 1;
    case 'delay2'
        timing = [-400 0];
        ref = 2;
    case 'saccade'
        timing = [-100 200];
        ref = 2;
    case 'allTrialSample'
        timing = [-499 1300];
        ref = 1;
    case 'allTrialMatch'
        timing = [-799 200];
        ref = 2;
end


if Args.ML || Args.session99
    if Args.ML
        cd session01
    elseif Args.session99
        cd session99
    end
    cd(sprintf('group%04.0f',grUnit))
    cd(sprintf('cluster%s',cluster))
    
    for s = 1 : 2
        pathTosave{s} = pwd;
        load(['nineStimPSTH' num2str(s) '.mat'])
        spikes = cat(1,A{:,2});
        
        Ispikes = spikes(:,abs(reference{1,ref}) + timing(1) : abs(reference{1,ref}) + timing(2));
        
        Mbins = [timing(1) : Args.binSize : timing(2)];
        
        for bi = 1 : length(Mbins)-1
            allspikes{s}(:,bi) = sum(Ispikes(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2);
            
        end
        
        
    end
else
    load rules.mat
    for s = 2 : 3
        cd(sdir)
        cd(['session0' num2str(s)])
        
        
        cd(sprintf('group%04.0f',grUnit))
        cd(sprintf('cluster%s',cluster))
        
        
        pathTosave{s-1} = pwd;
        
        load(['nineStimPSTH' num2str(r(s-1)) '.mat'])
        spikes = cat(1,A{:,2});
        
        Ispikes = spikes(:,abs(reference{1,ref}) + timing(1) : abs(reference{1,ref}) + timing(2));
        
        Mbins = [timing(1) : Args.binSize : timing(2)];
        
        for bi = 1 : length(Mbins)-1
            allspikes{s-1}(:,bi) = sum(Ispikes(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2);
            
        end
        
        
        
    end
end


pval = ones(length(Mbins)-1,1);

for bi = 1 : length(Mbins)-1
    pval(bi) = ranksum(allspikes{1}(:,bi),allspikes{2}(:,bi));
end


for ss = 1 : 2
    cd(pathTosave{ss})
    save(matfile,'pval','Mbins')
    display([pwd '/' matfile])
end


cd(sdir)
