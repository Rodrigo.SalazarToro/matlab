function epoch_spike_spike_coh(varargin)


%run at session level
Args = struct('ml',0,'ide_only',1,'bmf',0,'rules',[],'bin_width',1,'redo',0);
Args.flags = {'ml','bmf','rules','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);


params = struct('tapers',[2 3],'fpass',[0 100],'Fs',1000,'trialave',1);
sesdir = pwd;
cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
    iti_reject = rejecTrials;
else
    iti_reject = [];
end

if Args.ml
    
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx'); %incorrect/identity
        
        N = NeuronalHist('bmf');
        sortedgroups = bmf_groups('ml'); %already kicks out noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames','save','redo');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(find(mt.data.CueObj == 55),get_reject_trials);
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        
        %get rid of incorrect trials with bad itis if the artifact
        %detection has been run on it
        if ~isempty(iti_reject)
            [~,baditi] = intersect(incorrtrials,iti_reject);
            incorrtrials(baditi) = [];
        end
        
        N = NeuronalHist('ml');
        noisy = noisy_groups;
        sortedgroups = sorted_groups('ml');
        [~,ii] = intersect(sortedgroups,noisy);
        sortedgroups(ii) = []; %get rid of noisy groups
        [~,chgroups] = intersect(N.gridPos,sortedgroups);
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
%     tr = mtsgetTrials(mt,'BehResp',1,'stable');
    identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    location = mtsgetTrials(mt,'BehResp',1,'stable','rule',2);
    N = NeuronalHist;
    Nch = NeuronalChAssign;
    noisy = noisy_groups;
    [sortedgroups,sortedpairs,~,chpairlist] = sorted_groups;
    [~,ii] = intersect(sortedgroups,noisy);
    sortedgroups(ii) = []; %get rid of noisy groups
    
    [~,chgroups] = intersect(N.gridPos,sortedgroups);
end
%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1 4];
        else
            rules = [1 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        elseif rule == 3
            trials = fixtrials;
        elseif rule == 4
            trials = incorrtrials;
        end
    else
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        end
    end
    
    if ~isempty(trials)
        if Args.redo
            rulelist = {'identity','location','fixtrials','incorrect'};
            cd([sesdir filesep 'lfp' filesep 'lfp2' filesep rulelist{rule}])
%             !rm spikecoh*
        end
        
        counter = 0;
        for g = sortedgroups
            counter = counter + 1;
            fprintf('\n%0.5g     ',g)
            for gg = sortedgroups(counter + 1:end)
                spikecoh = [];
                fprintf(' %0.5g',gg)
                
                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                
                %determine if pair has already been written
                spair = ['spikecoh_g' num2strpad(g,4) 'g' num2strpad(gg,4) '.mat'];
                cd(sesdir)
                
                cd([sesdir filesep 'group' num2strpad(g,4)])
                gdir1 = pwd;
                %find clusters
                clusters1 = nptDir('cluster*');
                nclusters1 = size(clusters1,1);
                
                cd([sesdir filesep 'group' num2strpad(gg,4)])
                gdir2 = pwd;
                %find clusters
                clusters2 = nptDir('cluster*');
                nclusters2 = size(clusters2,1);
                
                
                
                %                 if nsp1 > 0 & nsp2 > 0
                ccounter = 0;
                for c = 1 : nclusters1
                    for cc = 1 : nclusters2
                        ccounter = ccounter + 1;
                        %get spikes
                        load([gdir1 filesep clusters1(c).name filesep 'ispikes.mat'])
                        sp1 = sp;
                        nsp1 = 0;for nt = trials; nsp1 = nsp1 + sp1.data.trial(nt).cluster.spikecount;end
                        
                        load([gdir2 filesep clusters2(cc).name filesep 'ispikes.mat'])
                        sp2 = sp;
                        nsp2 = 0;for nt = trials; nsp2 = nsp2 + sp2.data.trial(nt).cluster.spikecount;end
                        
                        totalspikes1 = 0;
                        totalspikes2 = 0;
                        tcounter = 0;
                        for ttrial = trials
                            ntrials = size(trials,2);
                            tcounter = tcounter + 1;
                            s_on = sample_on(ttrial);
                            s_off = sample_off(ttrial);
                            m = match(ttrial);
                            %                             data1(ccounter).times = vecc(ceil(sp1.data.trial(ttrial).cluster.spikes)) ./ 1000; %ceil to avoid spikes a t = 0
                            %                             data2(ccounter).times = vecc(ceil(sp2.data.trial(ttrial).cluster.spikes)) ./ 1000; %ceil to avoid spikes a t = 0
                            
                            spikes1 = ceil(sp1.data.trial(ttrial).cluster.spikes);
                            spikes2 = ceil(sp2.data.trial(ttrial).cluster.spikes);
                            totalspikes1 = totalspikes1 + size(spikes1,2);
                            totalspikes2 = totalspikes2 + size(spikes2,2);
                            
                            %fix
                            fix1(tcounter).times = vecc(spikes1(find(spikes1 > (s_on - 400) & spikes1 <= s_on))) ./ 1000;
                            fix2(tcounter).times = vecc(spikes2(find(spikes2 > (s_on - 400) & spikes2 <= s_on))) ./ 1000;
                            
                            %sample
                            sample1(tcounter).times = vecc(spikes1(find(spikes1 > (s_off - 400) & spikes1 <= s_off))) ./ 1000;
                            sample2(tcounter).times = vecc(spikes2(find(spikes2 > (s_off - 400) & spikes2 <= s_off))) ./ 1000;
                            
                            %delay 400
                            delay4001(tcounter).times = vecc(spikes1(find(spikes1 > s_off & spikes1 <= (s_off + 400)))) ./ 1000;
                            delay4002(tcounter).times = vecc(spikes2(find(spikes2 > s_off & spikes2 <= (s_off + 400)))) ./ 1000;
                            
                            %sample locked delay (delay800)
                            delay8001(tcounter).times = vecc(spikes1(find(spikes1 > (s_off + 400) & spikes1 <= (s_off + 800)))) ./ 1000;
                            delay8002(tcounter).times = vecc(spikes2(find(spikes2 > (s_off + 400) & spikes2 <= (s_off + 800)))) ./ 1000;
                            
                            
                            %delay
                            delay1(tcounter).times = vecc(spikes1(find(spikes1 > (s_off + 200) & spikes1 <= (s_off + 800)))) ./ 1000;
                            delay2(tcounter).times = vecc(spikes2(find(spikes2 > (s_off + 200) & spikes2 <= (s_off + 800)))) ./ 1000;
                            
                            %match locked delay
                            delaymatch1(tcounter).times = vecc(spikes1(find(spikes1 > (m - 400) & spikes1 <= m))) ./ 1000;
                            delaymatch2(tcounter).times = vecc(spikes2(find(spikes2 > (m - 400) & spikes2 <= m))) ./ 1000;
                            
                            %full trial
                            fulltrial1(tcounter).times = vecc(spikes1(find(spikes1 > (s_on - 500) & spikes1 <= m))) ./ 1000;
                            fulltrial2(tcounter).times = vecc(spikes2(find(spikes2 > (s_on - 500) & spikes2 <= m))) ./ 1000;
                        end
                        
                        if (totalspikes1 / ntrials) > 5 && (totalspikes2 / ntrials) > 5
                            [spikecoh.fix_c{ccounter},spikecoh.fix_phi{ccounter},S12,S1,S2,spikecoh.fix_f{ccounter},zerosp] = coherencypt(fix1,fix2,params);
                            [spikecoh.sample_c{ccounter},spikecoh.sample_phi{ccounter},S12,S1,S2,spikecoh.sample_f{ccounter},zerosp] = coherencypt(sample1,sample2,params);
                            [spikecoh.delay400_c{ccounter},spikecoh.delay400_phi{ccounter},S12,S1,S2,spikecoh.delay400_f{ccounter},zerosp] = coherencypt(delay4001,delay4002,params);
                            [spikecoh.delay800_c{ccounter},spikecoh.delay800_phi{ccounter},S12,S1,S2,spikecoh.delay800_f{ccounter},zerosp] = coherencypt(delay8001,delay8002,params);
                            [spikecoh.delay_c{ccounter},spikecoh.delay_phi{ccounter},S12,S1,S2,spikecoh.delay_f{ccounter},zerosp] = coherencypt(delay1,delay2,params);
                            [spikecoh.delaymatch_c{ccounter},spikecoh.delaymatch_phi{ccounter},S12,S1,S2,spikecoh.delaymatch_f{ccounter},zerosp] = coherencypt(delaymatch1,delaymatch2,params);
                            [spikecoh.fulltrial_c{ccounter},spikecoh.fulltrial_phi{ccounter},S12,S1,S2,spikecoh.fulltrial_f{ccounter},zerosp] = coherencypt(fulltrial1,fulltrial2,params);
                        end
                        
%                         spikecoh.clusters{ccounter} = single([clusters1(c).name; clusters2(cc).name]);
                        if strmatch(clusters1(c).name(end),'s')
                            spikecoh.unittype1(ccounter) = 1; %single unit
                        else
                            spikecoh.unittype1(ccounter) = 2; %multi unit
                        end
                        if strmatch(clusters2(cc).name(end),'s')
                            spikecoh.unittype2(ccounter) = 1; %single unit
                        else
                            spikecoh.unittype2(ccounter) = 2; %multi unit
                        end
                        spikecoh.groups(ccounter,:) = [g gg];
                        spikecoh.totalspikes1(ccounter) = totalspikes1;
                        spikecoh.totalspikes2(ccounter) = totalspikes2;
                        spikecoh.ntrials(ccounter) = ntrials;
                    end
                end
                
                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                
                write_info = writeinfo(dbstack);
                
                if rule == 1;
                    cd('identity')
                    save(spair,'spikecoh','write_info')
                elseif rule == 2;
                    cd('location')
                    save(spair,'spikecoh','write_info')
                elseif rule == 3
                    cd('fixation')
                    save(spair,'spikecoh','write_info')
                elseif rule == 4
                    cd('incorrect')
                    save(spair,'spikecoh','write_info')
                end
            end
        end
    end
    
end


cd(sesdir)