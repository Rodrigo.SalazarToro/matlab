function obj = mtscpp2(varargin)

%Creates mtscpp object
%
% To make object at session level:
% if monkeylogic:                 c = mtsCPP('ml','auto')
% else:                           c = mtsCPP('auto')
%
% To make object at monkey level:
%                                 cc = processDays(mtsCPP,'days',{cell array of days},'NoSites');
%index
%   parietal Vs parietal
%   parietal Vs frontal
%   frontal Vs frontal
%   trial type
%   average correlogram crossing

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'ide_only',1,'bmf',0,'bettynlynx',0);
Args.flags = {'Auto','ml','bmf','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtscpp2';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'cpp';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end



function obj = createObject(Args,varargin)
sesdir = pwd;

%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end

if ~isempty(mtst)
    types = mtst.data.Index(1,1);
    %for non ML data make sure that the session is ide if ide_only
    if Args.ide_only && ~Args.ml
        if types == 1
            ide = 1;
        else
            ide = 0; %location session
        end
    else
        ide = 1; %both rules are in same session for ML data
    end
end

if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ml');
    else
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
    end
end

%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sesdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end

if ~isempty(mtst) && match2sample && ~isempty(all_trials) && ide
    
    %get trial info, identity == 1, location == 0
    if Args.ml
        if Args.ide_only
            types = [1 0]; %identity first then location
            num_rules = 1;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        else
            types = [1 0]; %identity first then location
            num_rules = 2;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        end
    else %pertains to ML sessions where both rules are in one session
        types = mtst.data.Index(1,1);
        num_rules = 1;
        rule_dirs = {};
        rule_index = {'1:pairs'};
    end
    
    %get channel info
    if Args.bmf
        N = NeuronalHist('ml','bmf');
    elseif Args.bettynlynx
        N = NeuronalHist('ml','bettynlynx');
    else
        N = NeuronalHist;
    end
    
    if Args.ml
        groups = N.gridPos; %Takes all channels, bad channels must be slected out later.
    else
        groups = 1 : N.chnumb;
    end
    gridpos = N.gridPos;
    chnumb = N.chnumb;
    %get histology info
    mlhistology = N.number;
    
    
    %determine which groups have been sorted and which ones were rejected for noise
    if Args.ml
        if Args.bmf
            [g,~,~,~] = bmf_groups;
        elseif Args.bettynlynx
            [g,~,~,~] = sorted_groups('ml','bettynlynx');
        else
            [g,~,~,~] = sorted_groups('ml');
        end
        [~,ii] =intersect(groups,g);
        sortgroups = zeros(1,size(groups,2));
        sortgroups(ii) = 1;
        
        noisy = noisy_groups;
        [~,ii] =intersect(groups,noisy);
        rejectgroups = ones(1,size(groups,2));
        rejectgroups(ii) = 0; %these are the REJECTED groups
    else
        [g,~,~,~] = sorted_groups;
        NN = NeuronalChAssign;
        [i ii] =intersect(NN.groups,g);
        sortgroups = zeros(1,size(groups,2));
        sortgroups(ii) = 1;
        
        noisy = noisy_groups;
        [~,ii] =intersect(groups,noisy);
        rejectgroups = ones(1,size(groups,2));
        rejectgroups(ii) = 0; %these are the REJECTED groups
    end
    
    %depths
    rdepth = (N.recordedDepth./1000); %depths are in mm
    
    %get number of pairs
    pairs = nchoosek(chnumb,2);
    total_pairs = pairs * num_rules; %if both rules are included mult by 2
    pp = zeros(pairs,1);
    pf = zeros(pairs,1);
    ff =  zeros(pairs,1);
    cc1 = zeros(pairs,1);
    cc2 = zeros(pairs,1);
    bettypp_row = zeros(pairs,1);
    bettypp_col = zeros(pairs,1);
    br_diagonal = zeros(pairs,1);
    bl_diagonal = zeros(pairs,1);
    chnumbs = zeros(pairs,1);
    noisy_pairs = ones(pairs,1);
    ch1_sulcus = zeros(pairs,1);
    ch2_sulcus = zeros(pairs,1);
    for nr = 1 : num_rules
        cd(sesdir)
        type = types(nr);
        ri = eval(rule_index{nr}); %get indices for each rule
        p = 0;
        sorted_pairs = [];
        noreject = [];
        for c1 = 1 : chnumb
            for c2 = (c1+1) : chnumb
                %index to good groups
                cn1 = groups(c1); %these are group numbers
                cn2 = groups(c2);
                cc = [cn1 cn2];
                p = p + 1;
                cc1(p,1) = cn1;
                cc2(p,1)= cn2;
                
                %find pairs that both groups have been sorted
                if sortgroups(c1) && sortgroups(c2)
                    sorted_pairs(p,1) = 1; %indicateds that both channels have been sorted
                else
                    sorted_pairs(p,1) = 0;
                end
                
                %find pairs with no rejected channels (lfp noise)
                if rejectgroups(c1) && rejectgroups(c2)
                    noreject(p,1) = 1;
                else
                    noreject(p,1) = 0;
                end
                
                if Args.ml
                    %determine if channels are in the same row or column for betty parietal
                    gridrc = [0 0 49 43 0 0; 60 55 50 44 38 33; 61 56 51 45 39 34; 62 57 52 46 40 35; 63 58 53 47 41 36; 64 59 54 48 42 37];
                    
                    %determine if they are in the same row, and if they are give row number
                    if cn1 > 32 && cn2 > 32
                        for rc = 1 : 6
                            [~,rcn1] = intersect(gridrc(rc,:),cn1);
                            [~,rcn2] = intersect(gridrc(rc,:),cn2);
                            
                            [~,ccn1] = intersect(gridrc(:,rc),cn1);
                            [~,ccn2] = intersect(gridrc(:,rc),cn2);
                            
                            if ~isempty(rcn1) && ~isempty(rcn2)
                                bettypp_row(p) = rc;
                            end
                            
                            if ~isempty(ccn1) && ~isempty(ccn2)
                                bettypp_col(p) = rc;
                            end
                            
                            %determine if they are in the same diagonal and bottom_left starts at bottom left of grid with anatomical orientation
                            bottom_right_diagonals = [0 0 0 36 42;0 0 35 41 48;0 34 40 47 54;33 39 46 53 59;38 45 52 58 64;44 51 57 63 0; 43 50 56 62 0;49 55 61 0 0];
                            bottom_left_diagonals = [0 0 0 63 59;0 0 62 58 54;0 61 57 53 48;60 56 52 47 42;55 51 46 41 37; 50 45 40 36 0; 49 44 39 35 0;43 38 34 0 0];
                            
                            [~,bld1] = intersect(bottom_left_diagonals(rc,:),cn1);
                            [~,bld2] = intersect(bottom_left_diagonals(rc,:),cn2);
                            
                            [~,brd1] = intersect(bottom_right_diagonals(rc,:),cn1);
                            [~,brd2] = intersect(bottom_right_diagonals(rc,:),cn2);
                            
                            if ~isempty(bld1) && ~isempty(bld2)
                                bl_diagonal(p) = rc;
                            end
                            
                            if ~isempty(brd1) && ~isempty(brd2)
                                br_diagonal(p) = rc;
                            end
                        end
                    end
                end
                
                
                %get recorded depths
                record_depth(p,1) = rdepth(c1);
                record_depth(p,2) = rdepth(c2);
                
                mlhist(p,1) = mlhistology(c1);
                mlhist(p,2) = mlhistology(c2);
                
                gridpos1(p,1) = gridpos(c1);
                gridpos2(p,1) = gridpos(c2);
                
                chnumbs(p,1) = chnumb;
                interc = 0;
                
                if Args.ml
                    if cn1 > 32 && cn2 > 32
                        pp(p,1) = 1;
                    else
                        pp(p,1) = 0;
                        interc = 1;
                    end
                    if cn1 <= 32 && cn2 <= 32
                        ff(p,1) = 1;
                    else
                        ff(p,1) = 0;
                        interc = interc + 1;
                    end
                    if interc == 2;
                        pf(p,1) = 1;
                    else
                        pf(p,1) = 0;
                    end
                else
                    if strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'P',1)
                        pp(p,1) = 1;
                    else
                        pp(p,1) = 0;
                        interc = 1;
                    end
                    if strncmpi(N.cortex(c1),'F',1) && strncmpi(N.cortex(c2),'F',1)
                        ff(p,1) = 1;
                    else
                        ff(p,1) = 0;
                        interc = interc + 1;
                    end
                    if strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'F',1) %parietal channels are always first
                        pf(p,1) = 1;
                    else
                        pf(p,1) = 0;
                    end
                end
                
                if sorted_pairs(p,1)
                    %% get information about the spike to field relationship of each channel, 1 indicates that both channels are near 180 and non-uniform (self spike-field), 0 indicates otherwise
                    cd([sesdir filesep 'lfp' filesep 'lfp2'])
                    if exist(['hilbertpair' num2strpad(cn1,2) num2strpad(cn1,2) '.mat'],'file')
                        load(['hilbertpair' num2strpad(cn1,2) num2strpad(cn1,2) '.mat'])
                        m1 = hilbert_groups.mean{1}(5) * (180/pi); %MAKE SURE THIS IS ALWAYS THE MUA CHANNEL
                        r1 = hilbert_groups.rtest{1}(5); %MAKE SURE THIS IS ALWAYS THE MUA CHANNEL
                        
                        load(['hilbertpair' num2strpad(cn2,2) num2strpad(cn2,2) '.mat'])
                        m2 = hilbert_groups.mean{1}(5) * (180/pi); %MAKE SURE THIS IS ALWAYS THE MUA CHANNEL
                        r2 = hilbert_groups.rtest{1}(5); %MAKE SURE THIS IS ALWAYS THE MUA CHANNEL
                        
                        load(['hilbertpair' num2strpad(cn1,2) num2strpad(cn2,2) '.mat'])
                        for hp = 1:5
                            hpaircm1(p,hp) = hilbert_groups.mean{1}(hp) * (180/pi);
                            hpairr1(p,hp) = hilbert_groups.rtest{1}(hp);
                        end
                        
                        load(['hilbertpair' num2strpad(cn2,2) num2strpad(cn1,2) '.mat'])
                        for hp = 1:5
                            hpaircm2(p,hp) = hilbert_groups.mean{1}(hp) * (180/pi);
                            hpairr2(p,hp) = hilbert_groups.rtest{1}(hp);
                        end
                        
                        
                        if r1 < .05 && r2 < .05
                            if abs(m1) > 90 && abs(m2) > 90
                                hsfpp_criteria(p,1) = 1;
                            else
                                hsfpp_criteria(p,1) = 0;
                            end
                        else
                            hsfpp_criteria(p,1) = 0;
                        end
                    else
                        hsfpp_criteria(p,1) = 0;
                        hpaircm1(p,1:5) = 0;
                        hpairr1(p,1:5) = 0;
                        hpaircm2(p,1:5) = 0;
                        hpairr2(p,1:5) = 0;
                        
                    end
                else
                    hsfpp_criteria(p,1) = 0;
                    hpaircm1(p,1:5) = 0;
                    hpairr1(p,1:5) = 0;
                    hpaircm2(p,1:5) = 0;
                    hpairr2(p,1:5) = 0;
                end
                cd(sesdir)
            end
        end
        data.Index(ri,10) = sorted_pairs;
        
        data.Index(ri,11) = noreject;
        
        
        %HSFPP
        data.Index(ri,254) = hsfpp_criteria;
        
        %circular means, pre-sample, sample, delay1, delay2, delay (1.2 - 1.8)
        %c1spikes to c2lfp
        data.Index(ri,293) = hpaircm1(:,1);
        data.Index(ri,294) = hpaircm1(:,2);
        data.Index(ri,295) = hpaircm1(:,3);
        data.Index(ri,296) = hpaircm1(:,4);
        data.Index(ri,297) = hpaircm1(:,5);
        %rayleigh test p-vals
        data.Index(ri,298) = hpairr1(:,1);
        data.Index(ri,299) = hpairr1(:,2);
        data.Index(ri,300) = hpairr1(:,3);
        data.Index(ri,301) = hpairr1(:,4);
        data.Index(ri,302) = hpairr1(:,5);
        
        
        %circular means, pre-sample, sample, delay1, delay2, delay (1.2 - 1.8)
        %c2spikes to c1lfp
        data.Index(ri,303) = hpaircm2(:,1);
        data.Index(ri,304) = hpaircm2(:,2);
        data.Index(ri,305) = hpaircm2(:,3);
        data.Index(ri,306) = hpaircm2(:,4);
        data.Index(ri,307) = hpaircm2(:,5);
        %rayleigh test p-vals
        data.Index(ri,308) = hpairr2(:,1);
        data.Index(ri,309) = hpairr2(:,2);
        data.Index(ri,310) = hpairr2(:,3);
        data.Index(ri,311) = hpairr2(:,4);
        data.Index(ri,312) = hpairr2(:,5);
        
        
        %parietal Vs parietal
        data.Index(ri,1) = pp;
        %parietal Vs frontal
        data.Index(ri,2) = pf;
        %frontal Vs frontal
        data.Index(ri,3) = ff;
        
        if type == 1 %identity
            data.Index(ri,4) = ones(pairs,1);
        else %location
            data.Index(ri,4) = zeros(pairs,1);
        end
        
        data.Index(ri,20) = 1:p; %keep track of the pair number for each pair on each day, useful in get and plot functions
        
        %channel numbers
        data.Index(ri,23) = cc1;
        data.Index(ri,24) = cc2;
        
        %group numbers
        data.Index(ri,14) = gridpos1;
        data.Index(ri,15) = gridpos2;
        
        if Args.ml && ~Args.bmf
            %euclidean distance
            [xy xyz xxx yyy] = euclid_dist(gridpos1,gridpos2,record_depth);
            data.Index(ri,16) = xy; %xy
            data.Index(ri,17) = xyz; %xyz
            data.Index(ri,18) = xxx; %x
            data.Index(ri,19) = yyy; %y
            
            
            
            data.Index(ri,26) = bettypp_row;
            data.Index(ri,27) = bettypp_col;
            data.Index(ri,100) = br_diagonal;
            data.Index(ri,101) = bl_diagonal;
        end
        
        if ~Args.ml
            %get information about clarks grids
            [pp pf] = get_grid_info;
            
            data.Index(ri,102) = pp;
            data.Index(ri,103) = pf;
        end
        %depth information
        data.Index(ri,5) = record_depth(:,1); %first channel
        data.Index(ri,6) = record_depth(:,2); %second channel
        
        %histology information
        data.Index(ri,7) = mlhist(:,1); %first channel
        data.Index(ri,8) = mlhist(:,2); %second channel
        
        
        %DOUBLE CHECK THIS
        %if pp, indicate which side of the ips
        for sulcus = ri
            if data.Index(sulcus,1) == 1
                hnumber1 = data.Index(sulcus,7);
                hnumber2 = data.Index(sulcus,8);
                if (hnumber1 == 8 || hnumber1 == 9 || hnumber1 == 10 || hnumber1 == 12 || hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 8 || hnumber2 == 9 || hnumber2 == 10 || hnumber2 == 12 || hnumber2 == 11 || hnumber2 == 13)
                    if (hnumber1 == 8 || hnumber1 == 9 || hnumber1 == 10 || hnumber1 == 12) && (hnumber2 == 8 || hnumber1 == 9 || hnumber2 == 10 || hnumber2 == 12)
                        data.Index(sulcus,13) = 1; %medial/medial
                    elseif (hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 11 || hnumber2 == 13)
                        data.Index(sulcus,13) = 2; %lateral/lateral
                    else
                        data.Index(sulcus,13) = 3; %medial/lateral
                    end
                end
            end
        end
        data.Index(ri,25) = chnumbs;
        
        %LFP2 DIR
        cd (['lfp' filesep 'lfp2'])
        lfp2dir = cd;
        
        if Args.ml
            cd(lfp2dir) %get back to lfp2 folder where channelpairs are written
            count = 0;
            for cp = 1:chnumb
                for cpp = (cp+1):chnumb
                    count = count + 1;
                    cpairs(count).name = [ 'channelpair' num2strpad(groups(cp),2) num2strpad(groups(cpp),2)];
                end
            end
            cd(lfp2dir)
        else
            cpairs = nptDir('channelpair**.mat');
        end
        
        for q = 1 : pairs
            if nr == 1;
                data.setNames{q,1} = [lfp2dir filesep cpairs(q).name];
            else
                qq = q + pairs;
                data.setNames{qq,1} = [lfp2dir filesep cpairs(q).name];
            end
        end
        
        %RULE DIR
        if Args.ml
            cd (rule_dirs{nr});
            ruledir = pwd;
        end
        
        %get average correlogram information
        % 1: fixation    [(sample_on - 399) : (sample_on)]
        % 2: sample      [(sample_off - 399) : (sample_off)]
        % 3: early delay [(sample_off) : (sample_off + 399)]
        % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
        % 5: delay       [(sample_off + 201) : (sample_off + 800)]
        % 6: delay match [(match - 399) : (match)]
        % 7: full trial  [(sample_on - 500) : (match)]
        
        load all_correlograms
        load all_gabors
        load all_auto_gabors1
        load all_auto_gabors2
        for dg = 1 : size(ri,2)
            if ~isempty(all_gabors{1,dg})
                data.Index(dg,50) = all_gabors{1,dg}.phase_angle_deg; %modeled
                data.Index(dg,51) = all_gabors{2,dg}.phase_angle_deg; %modeled
                data.Index(dg,52) = all_gabors{3,dg}.phase_angle_deg; %modeled
                data.Index(dg,53) = all_gabors{4,dg}.phase_angle_deg; %modeled
                data.Index(dg,54) = all_gabors{5,dg}.phase_angle_deg; %modeled
                data.Index(dg,55) = all_gabors{6,dg}.phase_angle_deg; %modeled
                data.Index(dg,56) = all_gabors{7,dg}.phase_angle_deg; %modeled
                
                
                data.Index(dg,57) = all_gabors{1,dg}.peak; %modeled
                data.Index(dg,58) = all_gabors{2,dg}.peak; %modeled
                data.Index(dg,59) = all_gabors{3,dg}.peak; %modeled
                data.Index(dg,60) = all_gabors{4,dg}.peak; %modeled
                data.Index(dg,61) = all_gabors{5,dg}.peak; %modeled
                data.Index(dg,62) = all_gabors{6,dg}.peak; %modeled
                data.Index(dg,63) = all_gabors{7,dg}.peak; %modeled
                
                
                data.Index(dg,64) = all_gabors{1,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                data.Index(dg,65) = all_gabors{2,dg}.cross_corr; %modeled
                data.Index(dg,66) = all_gabors{3,dg}.cross_corr; %modeled
                data.Index(dg,67) = all_gabors{4,dg}.cross_corr; %modeled
                data.Index(dg,68) = all_gabors{5,dg}.cross_corr; %modeled
                data.Index(dg,69) = all_gabors{6,dg}.cross_corr; %modeled
                data.Index(dg,70) = all_gabors{7,dg}.cross_corr; %modeled
                
                
                data.Index(dg,71) = all_gabors{1,dg}.peak_lag; %modeled
                data.Index(dg,72) = all_gabors{2,dg}.peak_lag; %modeled
                data.Index(dg,73) = all_gabors{3,dg}.peak_lag; %modeled
                data.Index(dg,74) = all_gabors{4,dg}.peak_lag; %modeled
                data.Index(dg,75) = all_gabors{5,dg}.peak_lag; %modeled
                data.Index(dg,76) = all_gabors{6,dg}.peak_lag; %modeled
                data.Index(dg,77) = all_gabors{7,dg}.peak_lag; %modeled
                
                
                data.Index(dg,78) = all_gabors{1,dg}.frequency; %modeled
                data.Index(dg,79) = all_gabors{2,dg}.frequency; %modeled
                data.Index(dg,80) = all_gabors{3,dg}.frequency; %modeled
                data.Index(dg,81) = all_gabors{4,dg}.frequency; %modeled
                data.Index(dg,82) = all_gabors{5,dg}.frequency; %modeled
                data.Index(dg,83) = all_gabors{6,dg}.frequency; %modeled
                data.Index(dg,84) = all_gabors{7,dg}.frequency; %modeled
                
                
                data.Index(dg,85) = pvals_correlograms{1,dg}(all_gabors{1,dg}.peak_lag);
                data.Index(dg,86) = pvals_correlograms{2,dg}(all_gabors{2,dg}.peak_lag);
                data.Index(dg,87) = pvals_correlograms{3,dg}(all_gabors{3,dg}.peak_lag);
                data.Index(dg,88) = pvals_correlograms{4,dg}(all_gabors{4,dg}.peak_lag);
                data.Index(dg,89) = pvals_correlograms{5,dg}(all_gabors{5,dg}.peak_lag);
                data.Index(dg,90) = pvals_correlograms{6,dg}(all_gabors{6,dg}.peak_lag);
                data.Index(dg,91) = pvals_correlograms{7,dg}(all_gabors{7,dg}.peak_lag);
                
                
                data.Index(dg,92) = all_gabors{1,dg}.chi2; %modeled
                data.Index(dg,93) = all_gabors{2,dg}.chi2; %modeled
                data.Index(dg,94) = all_gabors{3,dg}.chi2; %modeled
                data.Index(dg,95) = all_gabors{4,dg}.chi2; %modeled
                data.Index(dg,96) = all_gabors{5,dg}.chi2; %modeled
                data.Index(dg,97) = all_gabors{6,dg}.chi2; %modeled
                data.Index(dg,98) = all_gabors{7,dg}.chi2; %modeled
                
                if size(all_gabors,1) > 7
                    data.Index(dg,194) = all_gabors{8,dg}.phase_angle_deg; %modeled, only for nlynx betty days
                    data.Index(dg,195) = all_gabors{8,dg}.peak; %modeled, only for nlynx betty days
                    data.Index(dg,196) = all_gabors{8,dg}.cross_corr; %modeled, only for nlynx betty days
                    data.Index(dg,197) = all_gabors{8,dg}.peak_lag; %modeled, only for nlynx betty days
                    data.Index(dg,198) = all_gabors{8,dg}.frequency; %modeled, only for nlynx betty days
                    data.Index(dg,199) = pvals_correlograms{8,dg}(all_gabors{7,dg}.peak_lag);
                    data.Index(dg,200) = all_gabors{8,dg}.chi2; %modeled, only for nlynx betty days
                end
                
                data.Index(dg,261) = all_auto_gabors1{1,dg}.frequency; %modeled
                data.Index(dg,262) = all_auto_gabors1{2,dg}.frequency; %modeled
                data.Index(dg,263) = all_auto_gabors1{3,dg}.frequency; %modeled
                data.Index(dg,264) = all_auto_gabors1{4,dg}.frequency; %modeled
                data.Index(dg,265) = all_auto_gabors1{5,dg}.frequency; %modeled
                data.Index(dg,266) = all_auto_gabors1{6,dg}.frequency; %modeled
                data.Index(dg,267) = all_auto_gabors1{7,dg}.frequency; %modeled
                
                data.Index(dg,268) = all_auto_gabors2{1,dg}.frequency; %modeled
                data.Index(dg,269) = all_auto_gabors2{2,dg}.frequency; %modeled
                data.Index(dg,270) = all_auto_gabors2{3,dg}.frequency; %modeled
                data.Index(dg,271) = all_auto_gabors2{4,dg}.frequency; %modeled
                data.Index(dg,272) = all_auto_gabors2{5,dg}.frequency; %modeled
                data.Index(dg,273) = all_auto_gabors2{6,dg}.frequency; %modeled
                data.Index(dg,274) = all_auto_gabors2{7,dg}.frequency; %modeled
            end
            
        end
        
        data.Index(size(ri,2),253) = 0; %indicate that location rule information is not present
        cd(lfp2dir)
        if exist('fixation','dir') %% CHANGE THIS BACK TO FIXATION
            cd fixation
            if exist('all_gabors.mat','file')
                load all_correlograms
                load all_gabors
                for dg = 1 : size(ri,2)
                    if ~isempty(all_gabors{1,dg})
                        data.Index(dg,104) = all_gabors{1,dg}.phase_angle_deg; %modeled
                        data.Index(dg,105) = all_gabors{2,dg}.phase_angle_deg; %modeled
                        data.Index(dg,106) = all_gabors{3,dg}.phase_angle_deg; %modeled
                        data.Index(dg,107) = all_gabors{4,dg}.phase_angle_deg; %modeled
                        data.Index(dg,108) = all_gabors{5,dg}.phase_angle_deg; %modeled
                        data.Index(dg,109) = all_gabors{6,dg}.phase_angle_deg; %modeled
                        data.Index(dg,110) = all_gabors{7,dg}.phase_angle_deg; %modeled
                        
                        data.Index(dg,111) = all_gabors{1,dg}.peak; %modeled
                        data.Index(dg,112) = all_gabors{2,dg}.peak; %modeled
                        data.Index(dg,113) = all_gabors{3,dg}.peak; %modeled
                        data.Index(dg,114) = all_gabors{4,dg}.peak; %modeled
                        data.Index(dg,115) = all_gabors{5,dg}.peak; %modeled
                        data.Index(dg,116) = all_gabors{6,dg}.peak; %modeled
                        data.Index(dg,117) = all_gabors{7,dg}.peak; %modeled
                        
                        data.Index(dg,118) = all_gabors{1,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                        data.Index(dg,119) = all_gabors{2,dg}.cross_corr; %modeled
                        data.Index(dg,120) = all_gabors{3,dg}.cross_corr; %modeled
                        data.Index(dg,121) = all_gabors{4,dg}.cross_corr; %modeled
                        data.Index(dg,122) = all_gabors{5,dg}.cross_corr; %modeled
                        data.Index(dg,123) = all_gabors{6,dg}.cross_corr; %modeled
                        data.Index(dg,124) = all_gabors{7,dg}.cross_corr; %modeled
                        
                        data.Index(dg,125) = all_gabors{1,dg}.peak_lag; %modeled
                        data.Index(dg,126) = all_gabors{2,dg}.peak_lag; %modeled
                        data.Index(dg,127) = all_gabors{3,dg}.peak_lag; %modeled
                        data.Index(dg,128) = all_gabors{4,dg}.peak_lag; %modeled
                        data.Index(dg,129) = all_gabors{5,dg}.peak_lag; %modeled
                        data.Index(dg,130) = all_gabors{6,dg}.peak_lag; %modeled
                        data.Index(dg,131) = all_gabors{7,dg}.peak_lag; %modeled
                        
                        data.Index(dg,132) = all_gabors{1,dg}.frequency; %modeled
                        data.Index(dg,133) = all_gabors{2,dg}.frequency; %modeled
                        data.Index(dg,134) = all_gabors{3,dg}.frequency; %modeled
                        data.Index(dg,135) = all_gabors{4,dg}.frequency; %modeled
                        data.Index(dg,136) = all_gabors{5,dg}.frequency; %modeled
                        data.Index(dg,137) = all_gabors{6,dg}.frequency; %modeled
                        data.Index(dg,138) = all_gabors{7,dg}.frequency; %modeled
                        
                        data.Index(dg,139) = pvals_correlograms{1,dg}(all_gabors{1,dg}.peak_lag);
                        data.Index(dg,140) = pvals_correlograms{2,dg}(all_gabors{2,dg}.peak_lag);
                        data.Index(dg,141) = pvals_correlograms{3,dg}(all_gabors{3,dg}.peak_lag);
                        data.Index(dg,142) = pvals_correlograms{4,dg}(all_gabors{4,dg}.peak_lag);
                        data.Index(dg,143) = pvals_correlograms{5,dg}(all_gabors{5,dg}.peak_lag);
                        data.Index(dg,144) = pvals_correlograms{6,dg}(all_gabors{6,dg}.peak_lag);
                        data.Index(dg,145) = pvals_correlograms{7,dg}(all_gabors{7,dg}.peak_lag);
                        
                        data.Index(dg,253) = 1; %indicate that fixation trials are present
                    end
                end
            end
        end
        
        
        cd(lfp2dir)
        if exist('incorrect','dir')
            cd incorrect
            load all_correlograms
            load all_gabors
            load all_auto_gabors1
            load all_auto_gabors2
            for dg = 1 : size(ri,2)
                if ~isempty(all_gabors{1,dg})
                    data.Index(dg,146) = all_gabors{1,dg}.phase_angle_deg; %modeled
                    data.Index(dg,147) = all_gabors{2,dg}.phase_angle_deg; %modeled
                    data.Index(dg,148) = all_gabors{3,dg}.phase_angle_deg; %modeled
                    data.Index(dg,149) = all_gabors{4,dg}.phase_angle_deg; %modeled
                    data.Index(dg,150) = all_gabors{5,dg}.phase_angle_deg; %modeled
                    data.Index(dg,151) = all_gabors{6,dg}.phase_angle_deg; %modeled
                    data.Index(dg,152) = all_gabors{7,dg}.phase_angle_deg; %modeled
                    data.Index(dg,153) = all_gabors{8,dg}.phase_angle_deg; %modeled
                    data.Index(dg,255) = all_gabors{9,dg}.phase_angle_deg; %modeled
                    
                    data.Index(dg,154) = all_gabors{1,dg}.peak; %modeled
                    data.Index(dg,155) = all_gabors{2,dg}.peak; %modeled
                    data.Index(dg,156) = all_gabors{3,dg}.peak; %modeled
                    data.Index(dg,157) = all_gabors{4,dg}.peak; %modeled
                    data.Index(dg,158) = all_gabors{5,dg}.peak; %modeled
                    data.Index(dg,159) = all_gabors{6,dg}.peak; %modeled
                    data.Index(dg,160) = all_gabors{7,dg}.peak; %modeled
                    data.Index(dg,161) = all_gabors{8,dg}.peak; %modeled
                    data.Index(dg,256) = all_gabors{9,dg}.peak; %modeled
                    
                    
                    data.Index(dg,162) = all_gabors{1,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,163) = all_gabors{2,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,164) = all_gabors{3,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,165) = all_gabors{4,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,166) = all_gabors{5,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,167) = all_gabors{6,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,168) = all_gabors{7,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,169) = all_gabors{8,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    data.Index(dg,257) = all_gabors{9,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                    
                    data.Index(dg,170) = all_gabors{1,dg}.peak_lag; %modeled
                    data.Index(dg,171) = all_gabors{2,dg}.peak_lag; %modeled
                    data.Index(dg,172) = all_gabors{3,dg}.peak_lag; %modeled
                    data.Index(dg,173) = all_gabors{4,dg}.peak_lag; %modeled
                    data.Index(dg,174) = all_gabors{5,dg}.peak_lag; %modeled
                    data.Index(dg,175) = all_gabors{6,dg}.peak_lag; %modeled
                    data.Index(dg,176) = all_gabors{7,dg}.peak_lag; %modeled
                    data.Index(dg,177) = all_gabors{8,dg}.peak_lag; %modeled
                    data.Index(dg,258) = all_gabors{9,dg}.peak_lag; %modeled
                    
                    data.Index(dg,178) = all_gabors{1,dg}.frequency; %modeled
                    data.Index(dg,179) = all_gabors{2,dg}.frequency; %modeled
                    data.Index(dg,180) = all_gabors{3,dg}.frequency; %modeled
                    data.Index(dg,181) = all_gabors{4,dg}.frequency; %modeled
                    data.Index(dg,182) = all_gabors{5,dg}.frequency; %modeled
                    data.Index(dg,183) = all_gabors{6,dg}.frequency; %modeled
                    data.Index(dg,184) = all_gabors{7,dg}.frequency; %modeled
                    data.Index(dg,185) = all_gabors{8,dg}.frequency; %modeled
                    data.Index(dg,259) = all_gabors{9,dg}.frequency; %modeled
                    
                    data.Index(dg,186) = pvals_correlograms{1,dg}(all_gabors{1,dg}.peak_lag);
                    data.Index(dg,187) = pvals_correlograms{2,dg}(all_gabors{2,dg}.peak_lag);
                    data.Index(dg,188) = pvals_correlograms{3,dg}(all_gabors{3,dg}.peak_lag);
                    data.Index(dg,189) = pvals_correlograms{4,dg}(all_gabors{4,dg}.peak_lag);
                    data.Index(dg,190) = pvals_correlograms{5,dg}(all_gabors{5,dg}.peak_lag);
                    data.Index(dg,191) = pvals_correlograms{6,dg}(all_gabors{6,dg}.peak_lag);
                    data.Index(dg,192) = pvals_correlograms{7,dg}(all_gabors{7,dg}.peak_lag);
                    data.Index(dg,193) = pvals_correlograms{8,dg}(all_gabors{8,dg}.peak_lag);
                    data.Index(dg,260) = pvals_correlograms{9,dg}(all_gabors{9,dg}.peak_lag);
                    
                    data.Index(dg,275) = all_auto_gabors1{1,dg}.frequency; %modeled
                    data.Index(dg,276) = all_auto_gabors1{2,dg}.frequency; %modeled
                    data.Index(dg,277) = all_auto_gabors1{3,dg}.frequency; %modeled
                    data.Index(dg,278) = all_auto_gabors1{4,dg}.frequency; %modeled
                    data.Index(dg,279) = all_auto_gabors1{5,dg}.frequency; %modeled
                    data.Index(dg,280) = all_auto_gabors1{6,dg}.frequency; %modeled
                    data.Index(dg,281) = all_auto_gabors1{7,dg}.frequency; %modeled
                    data.Index(dg,282) = all_auto_gabors1{8,dg}.frequency; %modeled
                    data.Index(dg,283) = all_auto_gabors1{9,dg}.frequency; %modeled
                    
                    data.Index(dg,284) = all_auto_gabors2{1,dg}.frequency; %modeled
                    data.Index(dg,285) = all_auto_gabors2{2,dg}.frequency; %modeled
                    data.Index(dg,286) = all_auto_gabors2{3,dg}.frequency; %modeled
                    data.Index(dg,287) = all_auto_gabors2{4,dg}.frequency; %modeled
                    data.Index(dg,288) = all_auto_gabors2{5,dg}.frequency; %modeled
                    data.Index(dg,289) = all_auto_gabors2{6,dg}.frequency; %modeled
                    data.Index(dg,290) = all_auto_gabors2{7,dg}.frequency; %modeled
                    data.Index(dg,291) = all_auto_gabors2{8,dg}.frequency; %modeled
                    data.Index(dg,292) = all_auto_gabors2{9,dg}.frequency; %modeled
                end
            end
        end
        
        
        
        data.Index(size(ri,2),201:251) = nan;
        data.Index(size(ri,2),252) = 0; %indicate that location rule information is not present
        
        cd(lfp2dir)
        if exist('location','dir') %% CHANGE THIS BACK TO FIXATION
            cd location
            if exist('all_gabors.mat','file')
                load all_correlograms
                load all_gabors
                for dg = 1 : size(ri,2)
                    if ~isempty(all_gabors{1,dg})
                        data.Index(dg,201) = all_gabors{1,dg}.phase_angle_deg; %modeled
                        data.Index(dg,202) = all_gabors{2,dg}.phase_angle_deg; %modeled
                        data.Index(dg,203) = all_gabors{3,dg}.phase_angle_deg; %modeled
                        data.Index(dg,204) = all_gabors{4,dg}.phase_angle_deg; %modeled
                        data.Index(dg,205) = all_gabors{5,dg}.phase_angle_deg; %modeled
                        data.Index(dg,206) = all_gabors{6,dg}.phase_angle_deg; %modeled
                        data.Index(dg,207) = all_gabors{7,dg}.phase_angle_deg; %modeled
                        
                        data.Index(dg,208) = all_gabors{1,dg}.peak; %modeled
                        data.Index(dg,209) = all_gabors{2,dg}.peak; %modeled
                        data.Index(dg,210) = all_gabors{3,dg}.peak; %modeled
                        data.Index(dg,211) = all_gabors{4,dg}.peak; %modeled
                        data.Index(dg,212) = all_gabors{5,dg}.peak; %modeled
                        data.Index(dg,213) = all_gabors{6,dg}.peak; %modeled
                        data.Index(dg,214) = all_gabors{7,dg}.peak; %modeled
                        
                        data.Index(dg,215) = all_gabors{1,dg}.cross_corr; % this is the cross correlation between the real xcorr and the modeled gabor
                        data.Index(dg,216) = all_gabors{2,dg}.cross_corr; %modeled
                        data.Index(dg,217) = all_gabors{3,dg}.cross_corr; %modeled
                        data.Index(dg,218) = all_gabors{4,dg}.cross_corr; %modeled
                        data.Index(dg,219) = all_gabors{5,dg}.cross_corr; %modeled
                        data.Index(dg,220) = all_gabors{6,dg}.cross_corr; %modeled
                        data.Index(dg,221) = all_gabors{7,dg}.cross_corr; %modeled
                        
                        data.Index(dg,222) = all_gabors{1,dg}.peak_lag; %modeled
                        data.Index(dg,223) = all_gabors{2,dg}.peak_lag; %modeled
                        data.Index(dg,224) = all_gabors{3,dg}.peak_lag; %modeled
                        data.Index(dg,225) = all_gabors{4,dg}.peak_lag; %modeled
                        data.Index(dg,226) = all_gabors{5,dg}.peak_lag; %modeled
                        data.Index(dg,227) = all_gabors{6,dg}.peak_lag; %modeled
                        data.Index(dg,228) = all_gabors{7,dg}.peak_lag; %modeled
                        
                        data.Index(dg,229) = all_gabors{1,dg}.frequency; %modeled
                        data.Index(dg,230) = all_gabors{2,dg}.frequency; %modeled
                        data.Index(dg,231) = all_gabors{3,dg}.frequency; %modeled
                        data.Index(dg,232) = all_gabors{4,dg}.frequency; %modeled
                        data.Index(dg,233) = all_gabors{5,dg}.frequency; %modeled
                        data.Index(dg,234) = all_gabors{6,dg}.frequency; %modeled
                        data.Index(dg,235) = all_gabors{7,dg}.frequency; %modeled
                        
                        data.Index(dg,236) = pvals_correlograms{1,dg}(all_gabors{1,dg}.peak_lag);
                        data.Index(dg,237) = pvals_correlograms{2,dg}(all_gabors{2,dg}.peak_lag);
                        data.Index(dg,238) = pvals_correlograms{3,dg}(all_gabors{3,dg}.peak_lag);
                        data.Index(dg,239) = pvals_correlograms{4,dg}(all_gabors{4,dg}.peak_lag);
                        data.Index(dg,240) = pvals_correlograms{5,dg}(all_gabors{5,dg}.peak_lag);
                        data.Index(dg,250) = pvals_correlograms{6,dg}(all_gabors{6,dg}.peak_lag);
                        data.Index(dg,251) = pvals_correlograms{7,dg}(all_gabors{7,dg}.peak_lag);
                        
                        data.Index(dg,252) = 1;
                    end
                end
            end
        end
        
        
        
        % %
        % %         load threshold threshold
        % %         data.thresholds(ri,1) = threshold';
        % %
        % %
        % %         load glm_analysis_all
        % %         for g = 1:size(ri,2)
        % %             if isempty(glm_analysis_out{1,g})
        % %                 data.Index(g,28) = nan;
        % %                 data.Index(g,30) = nan;
        % %                 data.Index(g,44) = nan;
        % %             else
        % %                 %time p-vals from drop in deviance test
        % %                 data.Index(g,28) = str2double(glm_analysis_out{1,g}.time);
        % %                 %9stims p-values for the drop in deviance test
        % %                 data.Index(g,30) = str2double(glm_analysis_out{1,g}.interaction);
        % %                 %get slope for time only model
        % %                 data.Index(g,44) = str2double(glm_analysis_out{1,g}.time_coefs{2});
        % %             end
        % %         end
        %
        %             data.interaction_coefs = {};
        %             data.interaction_pval = {};
        %             data.time_coefs = {};
        %             data.time_pval = {};
        %
    end
    
    data.numSets = total_pairs;
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)


data.Index = [];
data.interaction_coefs = {};
data.interaction_pvals = {};
data.time_coefs = {};
data.time_pval = {};
data.setNames = {};
data.thresholds={};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
