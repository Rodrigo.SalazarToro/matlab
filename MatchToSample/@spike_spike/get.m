function [r,varargout] = get(obj,varargin)

%get function for spike_spike object
%Object level is session object
%gets channel pairs, plot function is then used to get specific trials
Args = struct('Number',0,'ObjectLevel',0,'pp',0,'pf',0,'ff',0,'ml',0,'auto',0,'no_auto',0,'multi',0,'single',0,'auto_diffc',0,'mlhist',[],'sig',0,'fix',0,'sample',0,'delay400',0,'delay800',0,'delay',0,'delaymatch',0,'fulltrial',0,'minspikes',0);
Args.flags = {'Number','ObjectLevel','pp','pf','ff','ml','auto','no_auto','auto_diffc','single','multi','sig','fix','sample','delay400','delay800','delay','delaymatch','fulltrial','minspikes'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;
%Arguments
%   'pp': parietal Vs parietal
%   'pf': parietal Vs frontal
%   'ff': frontal Vs frontal
%   'hist_numbers': selects only pairs with both channels matching the hist_number/s. Numbers are made in NeuronalHist.m

if Args.Number
    
    %rtemp1
    if Args.pp
        rtemp1 = find(obj.data.Index(:,1) == 1);
    else
        rtemp1 = [1 : length(obj.data.Index(:,1))];
    end
    
    %rtemp2
    if Args.pf
        rtemp2 = find(obj.data.Index(:,2) == 1);
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp3
    if Args.ff
        rtemp3 = find(obj.data.Index(:,3) == 1);
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.auto || Args.no_auto
        rtemp4 = find(obj.data.Index(:,15) == 1);
        if Args.auto
            rtemp4 = find(obj.data.Index(:,15) == 1);
        elseif Args.no_auto
            rtemp4 = find(obj.data.Index(:,15) == 0);
        end
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.single
        rtemp5 = intersect(find(obj.data.Index(:,19)),find(obj.data.Index(:,20)));
    else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.multi
        rtemp6 = intersect(find(obj.data.Index(:,19) == 0),find(obj.data.Index(:,20) == 0));
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.auto_diffc || Args.no_auto
        if Args.auto_diffc
            rtemp7 = find(obj.data.Index(:,21) == 1);
        elseif Args.no_auto
            rtemp7 = find(obj.data.Index(:,21) == 0);
        end
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.mlhist)
        if Args.mlhist(1) == Args.mlhist(2)
            rtemp8 = [find(obj.data.Index(:,11) == Args.mlhist(1))]';
            rtemp9 = [find(obj.data.Index(:,12) == Args.mlhist(2))]';
        else
            rtemp8 = [find(obj.data.Index(:,11) == Args.mlhist(1)); find(obj.data.Index(:,12) == Args.mlhist(1))]';
            rtemp9 = [find(obj.data.Index(:,11) == Args.mlhist(2)); find(obj.data.Index(:,12) == Args.mlhist(2))]';
        end
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.sig
        if Args.fix
            rtemp10 = find(abs(cellfun(@isempty,obj.data.fix_shiftpred_thresh) - 1))';
        elseif Args.sample
            rtemp10 = find(abs(cellfun(@isempty,obj.data.sample_shiftpred_thresh) - 1))';
        elseif Args.delay400
            rtemp10 = find(abs(cellfun(@isempty,obj.data.delay400_shiftpred_thresh) - 1))';
        elseif Args.delay800
            rtemp10 = find(abs(cellfun(@isempty,obj.data.delay800_shiftpred_thresh) - 1))';
        elseif Args.delay
            rtemp10 = find(abs(cellfun(@isempty,obj.data.delay_shiftpred_thresh) - 1))';
        elseif Args.delaymatch
            rtemp10 = find(abs(cellfun(@isempty,obj.data.delaymatch_shiftpred_thresh) - 1))';
        end
    else
        rtemp10 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.minspikes
                                                    rtemp11 = find(cellfun(@sum,obj.data.fix) > 1000  & cellfun(@sum,obj.data.fix) < 2000); %TEMPORARY
    else
        rtemp11 = [1 : size(obj.data.Index,1)];
    end
    
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect(rtemp10,rtemp11))))))))));
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end

