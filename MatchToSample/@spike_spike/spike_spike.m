function obj = spike_spike(varargin)

%Creates mtscpp object
%
% To make object at session level:
% if monkeylogic:                 c = spike_spike('ml','auto')
% else:                           c = spike_spike('auto')
%
% To make object at monkey level:
%                                 cc = processDays(spike_spike,'days',{cell array of days},'NoSites');


Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'ide_only',1);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'spike_spike';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'spike_spike';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end



function obj = createObject(Args,varargin)
sesdir = pwd;

%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end

if ~isempty(mtst)
    types = mtst.data.Index(1,1);
    %for non ML data make sure that the session is ide if ide_only
    if Args.ide_only && ~Args.ml
        if types == 1
            ide = 1;
        else
            ide = 0; %location session
        end
    else
        ide = 1; %both rules are in same session for ML data
    end
end

if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ml');
    else
        all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
    end
end

%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sesdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end

if ~isempty(mtst) && match2sample && ~isempty(all_trials) && ide
    
    %get trial info, identity == 1, location == 0
    if Args.ml
        if Args.ide_only
            types = [1 0]; %identity first then location
            num_rules = 1;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        else
            types = [1 0]; %identity first then location
            num_rules = 2;
            rule_dirs = {'identity' 'location'};
            rule_index = {'(1 : pairs)' '((pairs+1) : total_pairs)'};
        end
    else %pertains to ML sessions where both rules are in one session
        types = mtst.data.Index(1,1);
        num_rules = 1;
        rule_dirs = {};
        rule_index = {'1:pairs'};
    end
    
    %get channel info
    N = NeuronalHist;
    g = N.gridPos;
    %get histology info
    allhistology = N.number;
    alldepths = N.recordedDepth;
    %find sorted groups
    if Args.ml
        [sortg] = sorted_groups('ml');
    else
        [sortg] = sorted_groups;
    end
    
    %get the histology information for sorted groups only
    [~,ii] =intersect(g,sortg);
    mlhistology = allhistology(ii);
    depths = alldepths(ii);
    
    %make list of pairs
    %only use one direction for each pair (ie. ch1 -> ch2, and not ch2 -> ch1)
    counter = 0;
    ngroups = size(sortg,2);
    for cc1 = 1:ngroups
        for cc2 = cc1:ngroups
            counter = counter + 1;
            glist(counter,:) = [sortg(cc1) sortg(cc2)];
            clist(counter,:) = [cc1 cc2];
        end
    end
    nsx = size(glist,1);
    
    for nr = 1 : num_rules
        cd([sesdir filesep 'highpass'])
        hpassdir = pwd;
        counter = 0;
        for x = 1 : nsx
            
            cn1 = glist(x,1);
            cn2 = glist(x,2);
            
            c1 = clist(x,1);
            c2 = clist(x,2);
            
            load(['spikexcorr_g' num2strpad(cn1,4) 'g' num2strpad(cn2,4) '.mat'])
            if cn1 ~= cn2
                load(['shiftspikexcorr_g' num2strpad(cn1,4) 'g' num2strpad(cn2,4) '.mat'])
            end
            ncluster_pairs = size(spikexcorr.clusters,2);
            
            for ncp = 1 : ncluster_pairs
                counter = counter + 1;
                
                data.fix{counter,1} = spikexcorr.fix(ncp,150:250);
                data.sample{counter,1} = spikexcorr.sample(ncp,150:250);
                data.delay400{counter,1} = spikexcorr.delay400(ncp,150:250);
                data.delay800{counter,1} = spikexcorr.delay800(ncp,150:250);
                data.delay{counter,1} = spikexcorr.delay(ncp,150:250);
                data.delaymatch{counter,1} = spikexcorr.delaymatch(ncp,150:250);
                data.fulltrial{counter,1} = spikexcorr.fulltrial(ncp,150:250);
                data.clusters{counter,1} = spikexcorr.clusters{ncp};
                
                if cn1 ~= cn2
                    data.fix_shiftpred{counter,1} = shiftpredict{ncp,1};
                    data.fix_shiftpred_thresh{counter,1} = find(spikexcorr.fix(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,1}));
                    
                    data.sample_shiftpred{counter,1} = shiftpredict{ncp,2};
                    data.sample_shiftpred_thresh{counter,1} = find(spikexcorr.sample(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,2}));
                    
                    data.delay400_shiftpred{counter,1} = shiftpredict{ncp,3};
                    data.delay400_shiftpred_thresh{counter,1} = find(spikexcorr.delay400(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,3}));
                    
                    data.delay800_shiftpred{counter,1} = shiftpredict{ncp,4};
                    data.delay800_shiftpred_thresh{counter,1} = find(spikexcorr.delay800(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,4}));
                    
                    data.delay_shiftpred{counter,1} = shiftpredict{ncp,5};
                    data.delay_shiftpred_thresh{counter,1} = find(spikexcorr.delay(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,5}));
                    
                    data.delaymatch_shiftpred{counter,1} = shiftpredict{ncp,6};
                    data.delaymatch_shiftpred_thresh{counter,1} = find(spikexcorr.delaymatch(ncp,150:250) >= poissinv(0.99,shiftpredict{ncp,6}));
                else
                    data.fix_shiftpred{counter,1} = {};
                    data.fix_shiftpred_thresh{counter,1} = {};
                    
                    data.sample_shiftpred{counter,1} = {};
                    data.sample_shiftpred_thresh{counter,1} = {};
                    
                    data.delay400_shiftpred{counter,1} = {};
                    data.delay400_shiftpred_thresh{counter,1} = {};
                    
                    data.delay800_shiftpred{counter,1} = {};
                    data.delay800_shiftpred_thresh{counter,1} = {};
                    
                    data.delay_shiftpred{counter,1} = {};
                    data.delay_shiftpred_thresh{counter,1} = {};
                    
                    data.delaymatch_shiftpred{counter,1} = {};
                    data.delaymatch_shiftpred_thresh{counter,1} = {};
                    
                end
                
                data.Index(counter,4) = spikexcorr.fix_count(ncp);
                data.Index(counter,5) = spikexcorr.sample_count(ncp);
                data.Index(counter,6) = spikexcorr.delay400_count(ncp);
                data.Index(counter,7) = spikexcorr.delay800_count(ncp);
                data.Index(counter,8) = spikexcorr.delay_count(ncp);
                data.Index(counter,9) = spikexcorr.delaymatch_count(ncp);
                data.Index(counter,10) = spikexcorr.fulltrial_count(ncp);
                
                
                pp = 0;
                pf = 0;
                ff =  0;
                
                if Args.ml
                    if cn1 > 32 && cn2 > 32
                        pp = 1;
                    elseif cn1 <= 32 && cn2 <= 32
                        ff = 1;
                    else
                        pf = 1;
                    end
                else
                    if strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'P',1)
                        pp = 1;
                    elseif strncmpi(N.cortex(c1),'F',1) && strncmpi(N.cortex(c2),'F',1)
                        ff = 1;
                    elseif (strncmpi(N.cortex(c1),'P',1) && strncmpi(N.cortex(c2),'F',1)) || (strncmpi(N.cortex(c1),'F',1) && strncmpi(N.cortex(c2),'P',1))
                        pf = 1;
                    end
                end
                
                %parietal Vs parietal
                data.Index(counter,1) = pp;
                %parietal Vs frontal
                data.Index(counter,2) = pf;
                %frontal Vs frontal
                data.Index(counter,3) = ff;
                
                %histology numbers
                data.Index(counter,11) = mlhistology(c1);
                data.Index(counter,12) = mlhistology(c2);
                
                %group information
                data.Index(counter,13) = cn1;
                data.Index(counter,14) = cn2;
                
                %auto
                if cn1 == cn2 && ~isempty(strmatch(spikexcorr.clusters{ncp}(1,:),spikexcorr.clusters{ncp}(2,:)))
                    data.Index(counter,15) = 1;
                else
                    data.Index(counter,15) = 0;
                end
                
                %multi units
                if ~isempty(strfind(spikexcorr.clusters{ncp}(1,10),'m'))
                    data.Index(counter,17) = 1;
                else
                    data.Index(counter,17) = 0;
                end
                if ~isempty(strfind(spikexcorr.clusters{ncp}(2,10),'m'))
                    data.Index(counter,18) = 1;
                else
                    data.Index(counter,18) = 0;
                end
                
                %single units
                if ~isempty(strfind(spikexcorr.clusters{ncp}(1,10),'s'))
                    data.Index(counter,19) = 1;
                else
                    data.Index(counter,19) = 0;
                end
                if ~isempty(strfind(spikexcorr.clusters{ncp}(2,10),'s'))
                    data.Index(counter,20) = 1;
                else
                    data.Index(counter,20) = 0;
                end
                
                %same channel different clusters
                if cn1 == cn2 && isempty(strmatch(spikexcorr.clusters{ncp}(1,:),spikexcorr.clusters{ncp}(2,:)))
                    data.Index(counter,21) = 1;
                else
                    data.Index(counter,21) = 0;
                end
                
                %depth information
                data.Index(counter,22) = depths(c1);
                data.Index(counter,23) = depths(c2);
                
                
                if nr == 1
                    data.setNames{counter,1} = [hpassdir filesep ['spikexcorr_g' num2strpad(cn1,4) 'g' num2strpad(cn2,4) '.mat']];
                else
                    qq = nsx + counter;
                    data.setNames{qq,1} = [hpassdir filesep ['spikexcorr_g' num2strpad(cn1,4) 'g' num2strpad(cn2,4) '.mat']];
                end
            end
        end
    end
    
    data.numSets = counter;
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

data.Index = [];
data.fix = {};
data.sample = {};
data.delay400 = {};
data.delay800 = {};
data.delay = {};
data.delaymatch = {};
data.fulltrial = {};
data.clusters = {};
data.numSets = 0;
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
