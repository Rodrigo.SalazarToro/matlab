function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
	% check if second input is the right kind of object
	if(~isa(q,classname))
		% both inputs are not the right kind of object so create empty
		% object and return it
		r = feval(classname);
	else
		% second input is the right kind of object so return that
		r = q;
	end
else
	if(~isa(q,classname))
		% p is the right kind of object but q is not so just return p
		r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
	else
		% both p and q are the right kind of objects so add them 
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        r.data.Index = [p.data.Index; q.data.Index];
        r.data.numSets = p.data.numSets + q.data.numSets;
        r.data.setNames = {p.data.setNames{:} q.data.setNames{:}};
        
        
        %concat cells
        r.data.fix = {p.data.fix{:} q.data.fix{:}};
        r.data.sample = {p.data.sample{:} q.data.sample{:}};
        r.data.delay400 = {p.data.delay400{:} q.data.delay400{:}};
        r.data.delay800 = {p.data.delay800{:} q.data.delay800{:}};
        r.data.delay = {p.data.delay{:} q.data.delay{:}};
        r.data.delaymatch = {p.data.delaymatch{:} q.data.delaymatch{:}};
        r.data.fulltrial = {p.data.fulltrial{:} q.data.fulltrial{:}};
        r.data.clusters = {p.data.clusters{:} q.data.clusters{:}};
        
        r.data.fix_shiftpred = {p.data.fix_shiftpred{:} q.data.fix_shiftpred{:}};
        r.data.sample_shiftpred = {p.data.sample_shiftpred{:} q.data.sample_shiftpred{:}};
        r.data.delay400_shiftpred = {p.data.delay400_shiftpred{:} q.data.delay400_shiftpred{:}};
        r.data.delay800_shiftpred = {p.data.delay800_shiftpred{:} q.data.delay800_shiftpred{:}};
        r.data.delay_shiftpred = {p.data.delay_shiftpred{:} q.data.delay_shiftpred{:}};
        r.data.delaymatch_shiftpred = {p.data.delaymatch_shiftpred{:} q.data.delaymatch_shiftpred{:}};

        r.data.fix_shiftpred_thresh = {p.data.fix_shiftpred_thresh{:} q.data.fix_shiftpred_thresh{:}};
        r.data.sample_shiftpred_thresh = {p.data.sample_shiftpred_thresh{:} q.data.sample_shiftpred_thresh{:}};
        r.data.delay400_shiftpred_thresh = {p.data.delay400_shiftpred_thresh{:} q.data.delay400_shiftpred_thresh{:}};
        r.data.delay800_shiftpred_thresh = {p.data.delay800_shiftpred_thresh{:} q.data.delay800_shiftpred_thresh{:}};
        r.data.delay_shiftpred_thresh = {p.data.delay_shiftpred_thresh{:} q.data.delay_shiftpred_thresh{:}};
        r.data.delaymatch_shiftpred_thresh = {p.data.delaymatch_shiftpred_thresh{:} q.data.delaymatch_shiftpred_thresh{:}};
       
      
        
        % add nptdata objects as well
        r.nptdata = plus(p.nptdata,q.nptdata);
	end
end
