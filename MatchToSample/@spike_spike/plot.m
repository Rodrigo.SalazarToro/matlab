function obj = plot(obj,varargin)

%used to plot spike_spike objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct'

Args = struct('ml',0,'fix',0,'sample',0,'delay400',0,'delay800',0,'delay',0,'delaymatch',0,'fulltrial',0,'smoothing',3,'shiftp',0);
Args.flags = {'ml','fix','sample','delay400','delay800','delay','delaymatch','fulltrial','shiftp'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

%rule: identity(1), location(2)
if Args.ml
    if Args.identity
        Args.rule = 1;
    elseif Args.location
        Args.rule = 2;
    else
        fprintf(1,'enter identity or location')
    end
end

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end
%loads the channelpair specified in the index

cluster_pair = obj.data.setNames{ind};


%get histology information
[hist1] = reverse_categorizeNeuronalHist(obj.data.Index(ind,11));
[hist2] = reverse_categorizeNeuronalHist(obj.data.Index(ind,12));

cla
subplot(3,1,1)
if Args.fix
    sp_sp = (obj.data.fix{ind});
    sp_sp_shiftp = (obj.data.fix_shiftpred{ind});
    thresh = (obj.data.fix_shiftpred_thresh{ind});; %assume poission
    m = max(obj.data.fix{ind});
elseif Args.sample
    sp_sp = (obj.data.sample{ind});
    sp_sp_shiftp = (obj.data.sample_shiftpred{ind});
    thresh = (obj.data.sample_shiftpred_thresh{ind}); %assume poission
    m = max(obj.data.sample{ind});
elseif Args.delay400
    sp_sp = (obj.data.delay400{ind});
    sp_sp_shiftp = (obj.data.delay400_shiftpred{ind});
    thresh = (obj.data.delay400_shiftpred_thresh{ind}); %assume poission
    m = max(obj.data.delay400{ind});
elseif Args.delay800
    sp_sp = (obj.data.delay800{ind});
    sp_sp_shiftp = (obj.data.delay800_shiftpred{ind});
    thresh = (obj.data.delay800_shiftpred_thresh{ind}); %assume poission
    m = max(obj.data.delay800{ind});
elseif Args.delay
    sp_sp = (obj.data.delay{ind});
    sp_sp_shiftp = (obj.data.delay_shiftpred{ind});
    thresh = (obj.data.delay_shiftpred_thresh{ind}); %assume poission
    m = max(obj.data.delay{ind});
elseif Args.delaymatch
    sp_sp = (obj.data.delaymatch{ind});
    sp_sp_shiftp = (obj.data.delaymatch_shiftpred{ind});
    thresh = (obj.data.delaymatch_shiftpred_thresh{ind}); %assume poission
    m = max(obj.data.delaymatch{ind});
elseif Args.fulltrial
    sp_sp = (obj.data.fulltrial{ind});
    m = max(obj.data.fulltrial{ind});
end

% sp_sp(52) = 0;
% 
% m = max(sp_sp);

if Args.shiftp & (obj.data.Index(ind,13) ~= obj.data.Index(ind,14))
    plot(smooth(sp_sp - sp_sp_shiftp,Args.smoothing,'moving'));
    hold on
    if ~isempty(thresh)
        empt = zeros(1,size(sp_sp,2));
        empt(thresh) = 1;
        scatter(thresh,zeros(1,size(thresh,2)));
    end
    hold on
else
    plot(smooth(sp_sp,Args.smoothing,'moving'));
    hold on
end




if obj.data.Index(ind,19) == 1
    units{1} = 's';
else
    units{1} = 'm';
end

if obj.data.Index(ind,20) == 1
    units{2} = 's';
else
    units{2} = 'm';
end

day = obj.data.setNames{ind}(end-50:end-45);

ses = obj.data.setNames{ind}(end-43:end-35);

depth1 = obj.data.Index(ind,22);
depth2 = obj.data.Index(ind,23);

title([day '  ' ses '       g' num2str(obj.data.Index(ind,13)) units{1} ' ' cell2mat(hist1) ' ' num2str(depth1) '   VS   g' num2str(obj.data.Index(ind,14)) units{2} ' ' cell2mat(hist2) ' ' num2str(depth2)])
hold on

if m ~= 0
    axis([0 100 -m m])
    
    plot([52 52],[-m m],'r')
end
subplot(3,1,2)


NFFT = 2^nextpow2(size(sp_sp,2));
f = fft(sp_sp,NFFT);
power = abs(f(1:NFFT/2+1)).^2;
freq = 1000/2 * linspace(0,1,NFFT/2+1);



plot(freq(2:30),power(2:30))

empt = zeros(1,101);
for allsig = dataindices
    if Args.fix
        empt(obj.data.fix_shiftpred_thresh{allsig}) = empt(obj.data.fix_shiftpred_thresh{allsig}) + 1;
    elseif Args.sample
        empt(obj.data.sample_shiftpred_thresh{allsig}) = empt(obj.data.sample_shiftpred_thresh{allsig}) + 1;
    elseif Args.delay400
        empt(obj.data.delay400_shiftpred_thresh{allsig}) = empt(obj.data.delay400_shiftpred_thresh{allsig}) + 1;
    elseif Args.delay800
        empt(obj.data.delay800_shiftpred_thresh{allsig}) = empt(obj.data.delay800_shiftpred_thresh{allsig}) + 1;
    end
end
subplot(3,1,3)
plot(empt);hold on
plot([52 52],[0 max(empt)],'r')
axis([0 100 0 max(empt)])




  