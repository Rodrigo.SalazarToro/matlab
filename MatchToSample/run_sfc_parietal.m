function run_sfc_parietal


%runs sfc_parietal 
cd('/media/raid/data/monkey/betty/')
load alldays

monkeydir = pwd;

mip_lip_phase = [];
lip_mip_phase = [];

pe_pg_phase = [];
pg_pe_phase = [];

pe_pe_phase = [];
pg_pg_phase = [];

pg_mip_phase = [];
mip_pg_phase = [];

pg_lip_phase = [];
lip_pg_phase = [];

pe_lip_phase = [];
lip_pe_phase = [];

pe_mip_phase = [];
mip_pe_phase = [];

pec_pe_phase = [];
pe_pec_phase = [];

pec_pg_phase = [];
pg_pec_phase = [];

pec_mip_phase = [];
mip_pec_phase = [];

pec_lip_phase = [];
lip_pec_phase = [];

for d = 1 : 48
    
    cd([monkeydir filesep alldays{d} filesep 'session01'])
    alldays{d}
    sfc = sfc_parietal;

    if ~isempty(sfc)
        nmip_lip = size(sfc.mip_lip,2);
        for ml = 1 : nmip_lip
            mip_lip_phase = [mip_lip_phase sfc.mip_lip{ml}'];
        end
        nlip_mip = size(sfc.lip_mip,2);
        for ml = 1 : nlip_mip
            lip_mip_phase = [lip_mip_phase sfc.lip_mip{ml}'];
        end
        
        
        
        npe_pg = size(sfc.pe_pg,2);
        for ml = 1 : npe_pg
            pe_pg_phase = [pe_pg_phase sfc.pe_pg{ml}'];
        end
        npg_pe = size(sfc.pg_pe,2);
        for ml = 1 : npg_pe
            pg_pe_phase = [pg_pe_phase sfc.pg_pe{ml}'];
        end
        
        
        
        
        npe_pe = size(sfc.pe_pe,2);
        for ml = 1 : npe_pe
            pe_pe_phase = [pe_pe_phase sfc.pe_pe{ml}'];
        end
        npg_pg = size(sfc.pg_pg,2);
        for ml = 1 : npg_pg
            pg_pg_phase = [pg_pg_phase sfc.pg_pg{ml}'];
        end
        
        
        
        npg_mip = size(sfc.pg_mip,2);
        for ml = 1 : npg_mip
            pg_mip_phase = [pg_mip_phase sfc.pg_mip{ml}'];
        end
        nmip_pg = size(sfc.mip_pg,2);
        for ml = 1 : nmip_pg
            mip_pg_phase = [mip_pg_phase sfc.mip_pg{ml}'];
        end
        
        
        
        npe_lip = size(sfc.pe_lip,2);
        for ml = 1 : npe_lip
            pe_lip_phase = [pe_lip_phase sfc.pe_lip{ml}'];
        end
        nlip_pe = size(sfc.lip_pe,2);
        for ml = 1 : nlip_pe
            lip_pe_phase = [lip_pe_phase sfc.lip_pe{ml}'];
        end
        
        
        
        
        npe_mip = size(sfc.pe_mip,2);
        for ml = 1 : npe_mip
            pe_mip_phase = [pe_mip_phase sfc.pe_mip{ml}'];
        end
        nmip_pe = size(sfc.mip_pe,2);
        for ml = 1 : nmip_pe
            mip_pe_phase = [mip_pe_phase sfc.mip_pe{ml}'];
        end
        
        
        
        npg_lip = size(sfc.pg_lip,2);
        for ml = 1 : npg_lip
            pg_lip_phase = [pg_lip_phase sfc.pg_lip{ml}'];
        end
        nlip_pg = size(sfc.lip_pg,2);
        for ml = 1 : nlip_pg
            lip_pg_phase = [lip_pg_phase sfc.lip_pg{ml}'];
        end
        
        
        npec_pe = size(sfc.pec_pe,2);
        for ml = 1 : npec_pe
            pec_pe_phase = [pec_pe_phase sfc.pec_pe{ml}'];
        end
        npe_pec = size(sfc.pe_pec,2);
        for ml = 1 : npe_pec
            pe_pec_phase = [pe_pec_phase sfc.pe_pec{ml}'];
        end
        
        
        
        npec_pg = size(sfc.pec_pg,2);
        for ml = 1 : npec_pg
            pec_pg_phase = [pec_pg_phase sfc.pec_pg{ml}'];
        end
        npg_pec = size(sfc.pg_pec,2);
        for ml = 1 : npg_pec
            pg_pec_phase = [pg_pec_phase sfc.pg_pec{ml}'];
        end
        
        
        npec_mip = size(sfc.pec_mip,2);
        for ml = 1 : npec_mip
            pec_mip_phase = [pec_mip_phase sfc.pec_mip{ml}'];
        end
        nmip_pec = size(sfc.mip_pec,2);
        for ml = 1 : nmip_pec
            mip_pec_phase = [mip_pec_phase sfc.mip_pec{ml}'];
        end
        
        
        
        npec_lip = size(sfc.pec_lip,2);
        for ml = 1 : npec_lip
            pec_lip_phase = [pec_lip_phase sfc.pec_lip{ml}'];
        end
        nlip_pec = size(sfc.lip_pec,2);
        for ml = 1 : nlip_pec
            lip_pec_phase = [lip_pec_phase sfc.lip_pec{ml}'];
        end
        
    end
end
figure
subplot(4,6,1)
rose(mip_lip_phase)
title('MIP  LIP')
subplot(4,6,2)
rose(lip_mip_phase)
title('LIP  MIP')


subplot(4,6,3)
rose(pe_pg_phase)
title('PE  PG')
subplot(4,6,4)
rose(pg_pe_phase)
title('PG  PE')



subplot(4,6,5)
rose(pe_pe_phase)
title('PE  PE')
subplot(4,6,6)
rose(pg_pg_phase)
title('PG  PG')



subplot(4,6,7)
rose(pe_lip_phase)
title('PE  LIP')
subplot(4,6,8)
rose(lip_pe_phase)
title('LIP  PE')


subplot(4,6,9)
rose(pg_mip_phase)
title('PG  MIP')
subplot(4,6,10)
rose(mip_pg_phase)
title('MIP  PG')



subplot(4,6,11)
rose(pg_lip_phase)
title('PG  LIP')
subplot(4,6,12)
rose(lip_pg_phase)
title('LIP  PG')


subplot(4,6,13)
rose(pe_mip_phase)
title('PE  MIP')
subplot(4,6,14)
rose(mip_pe_phase)
title('MIP  PE')




subplot(4,6,15)
rose(pec_pe_phase)
title('PEC  PE')
subplot(4,6,16)
rose(pe_pec_phase)
title('PE  PEC')

subplot(4,6,17)
rose(pec_pg_phase)
title('PEC  PG')
subplot(4,6,18)
rose(pg_pec_phase)
title('PG  PEC')

subplot(4,6,19)
rose(pec_mip_phase)
title('PEC  MIP')
subplot(4,6,20)
rose(mip_pec_phase)
title('MIP  PEC')

subplot(4,6,21)
rose(pec_lip_phase)
title('PEC  LIP')
subplot(4,6,22)
rose(lip_pec_phase)
title('LIP  PEC')





mip_lip_phase;
