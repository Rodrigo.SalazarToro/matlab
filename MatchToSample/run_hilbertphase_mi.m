function run_hilbertphase_mi(varargin)

%run at session level
%computes MI based on normalized Shannon Entropy tass_1998
% Mutual Information is computed for:
% 1. 9stimuli
% 2. 3locations
% 3. 3ides
% 4,5,6. each ide across locations
% 7,8,9. each location for all ides
% 10. correct vs incorrect
Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
combinationorder = {'9stimuli','3identities','3locations','1ide1loc','1ide2loc','1ide3loc','3ide1loc','identity:correct/incorrect',};
combinantion_numbers = {[5:13],[14:16],[17:19],[5,6,7],[8,9,10],[11,12,13],[1,4]

window = 200;
steps = [window/2:50:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
stepsacc = [window/2:50:1000-(window/2)];
ntime = size(steps,2);

%this is the number of bins used to calculate
bins = [0:.05:1];

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);
if Args.ml
    c = mtscpp2('auto','ml');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
else
    c = mtscpp2('auto');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
end
pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

cd([sesdir filesep 'lfp' filesep 'lfp2'])

for p = 1 : npairs
    groups = pair_list(p,:);
    load(['hilbertentropy' num2strpad(groups(1),2) num2strpad(groups(2),2)])
    for allcombinations = 1 : 10
        
        %save the "noise entropy", this is just the entropy that is already
        %calculated, for each set of comparisons and calculate the mutual
        %information of the comparison.
        
        ents = hilbertentropy{alltypes};
        ntrials = size(ents,3);
        
        response_entropy = zeros(nfreq,ntime);
        %calculate response entropy H(R)
        for nf = 1 : nfreq
            for t = 1 : ntime
                h = hist(squeeze(ents(nf,t,tr)),bins) ./ ntrials; %normalize it by the number of responses (in this case trials), this is response distribution P(r)
                response_entropy(nf,t) = -1 * nansum( (h .* log2(h)) ); %see panzeri_2007
            end
        end
        
        %calculate the noise entropy
        for nf = 1 : nfreq
            for t = 1 : ntime
                h = hist(squeeze(ents(nf,t,tr)),bins) ./ ntrials;
                %calculate conditional distributions
                for ll = 1 : 3
                    cond_distributions{ll} = hist(squeeze(ents(nf,t,locs{ll})),bins) ./ size(locs{ll},2); %normalize it by the number of responses (in this case trials)
                end
                
                %sum over stimuli
                for ll = 1 : 3
                    ps =  size(locs{ll},2)/ntrials;
                    nr(ll,:) =   ps .* cond_distributions{ll} .* log2(cond_distributions{ll});
                end
                
                %sum over responses
                noise_entropy(nf,t) = -1 * nansum(nansum(nr)); %this is the noise entropy H(R|S)
                
                for ll = 1:3
                    ps = size(locs{ll},2)/ntrials;
                    mi(ll,:) = ps .* cond_distributions{ll} .* log2((cond_distributions{ll} ./ h));
                end
                
                mutual_information(nf,t) = nansum(nansum(mi));
            end
        end
    end
    
    mi_pairs = [ 'hilbertmi' num2strpad(groups(1),2) num2strpad(groups(2),2)];
    save(mi_pairs,'mi','all_freq','steps','window','trialorder','groups')
    
end
cd(sesdir)














