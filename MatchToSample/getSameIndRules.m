function [sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj)
    
[~,locind] = get(LOCobj,'Number');
[~,ideind] = get(IDEobj,'Number');
LOCl = regexprep(LOCobj.data.setNames(locind),['acute' filesep],'');

LOC2 = [cell2mat(cat(1,LOCl')) LOCobj.data.Index(locind,1:10)];


IDEl = regexprep(IDEobj.data.setNames(ideind),['acute' filesep],'');
IDE2 = [cell2mat(cat(1,IDEl')) IDEobj.data.Index(ideind,1:10)];


brUnits = intersect(LOC2,IDE2,'rows');

LOCind = find(ismember(LOC2,brUnits,'rows') == 1);
IDEind = find(ismember(IDE2,brUnits,'rows') == 1);

sindIDE = ideind(IDEind);
sindLOC = locind(LOCind);

