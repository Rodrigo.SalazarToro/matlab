function [r,varargout] = get(obj,varargin)

%get function for sta
Args = struct('Number',0,'ObjectLevel',0,'window',5,'multi',0,'single',0,'hist_numbers',[],'sig',0,'self',0,'pp',0,'pf',0,'ff',0,'ips',[],'no_self',0,'ch1s_ch2f',0,'sig_p',[],'incorrect',0);
Args.flags = {'Number','ObjectLevel','multi','single','sig','self','pp','pf','ff','no_self','ch1s_ch2f','incorrect'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    all = [1 : length(obj.data.Index(:,1))];
    
    ind1 = all;
    ind3 = all;

    
    %INDEX 3: single, multi, or all
    if Args.multi
        ind2 = find(obj.data.Index(:,3) == 1);
    elseif Args.single
        ind2 = find(obj.data.Index(:,3) ~= 1);
    else
        ind2 = all;
    end
    
    %spike channel is first, then lfp channel
    %rtemp5
    if ~isempty(Args.hist_numbers)
        ind4 = [];
        %histology information is in Index 7,8
        for hn = 1 : size(Args.hist_numbers,1)
            histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,1));
            histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,2));
            ind4 = [ind4 intersect(histi,histii)'];
            if ~Args.ch1s_ch2f
                %             keep this commented out if you only want ch1_spike -> ch2_field
                histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,2));
                histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,1));
                ind4 = [ind4 intersect(histi,histii)'];
            end
        end
        ind4 = sort(ind4);
    else
        ind4 = all;
    end
    
    
    if Args.self
        selfs = obj.data.Index(:,1) - obj.data.Index(:,2);
        ind5 = find(selfs == 0);
    else
        ind5 = all;
    end
    
    if Args.no_self
        selfs = obj.data.Index(:,1) - obj.data.Index(:,2);
        ind11 = find(selfs ~= 0);
    else
        ind11 = all;
    end
    
    %take only pairs that pass Rayleigh test, for self comparison
    if Args.sig
        
        if Args.incorrect
            ind_rtest = [55:62];
            if isempty(Args.sig_p)
                ind6 = find(obj.data.Index(:,ind_rtest(Args.window)) < .05);
            else
                ind6 = find(obj.data.Index(:,ind_rtest(Args.window)) < Args.sig_p);
            end
        else
            ind_rtest = [30:36];
            if isempty(Args.sig_p)
                ind6 = find(obj.data.Index(:,ind_rtest(Args.window)) < .05);
            else
                ind6 = find(obj.data.Index(:,ind_rtest(Args.window)) < Args.sig_p);
            end
        end
    else
       ind6 = all; 
    end
    
    if Args.pp
        ind7 =find(obj.data.Index(:,4));
    else
        ind7 = all; 
    end
    
    if Args.pf
        ind8 =find(obj.data.Index(:,5)); 
    else
        ind8 = all; 
    end
    
    if Args.ff
        ind9 =find(obj.data.Index(:,6));
    else
        ind9 = all;
    end
    
    if ~isempty(Args.ips)
        %1 = medial/medial
        %2 = lateral/lateral
        %3 = medial/lateral
        ind10 = find(obj.data.Index(:,11) == Args.ips);
    else
       ind10 = all; 
    end
    
    varargout{1} = intersect(ind1,intersect(ind2,intersect(ind3,intersect(ind4,intersect(ind5,intersect(ind6,intersect(ind7,intersect(ind8,intersect(ind9,intersect(ind10,ind11))))))))));
    r = length(varargout{1});
    
    
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end