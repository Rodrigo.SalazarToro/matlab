function obj = plot(obj,varargin)

%used to plot sta objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct'

Args = struct('ml',0);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end
%loads the channelpair specified in the index

cluster_pair = obj.data.setNames{ind};

%get histology information
hn1 = obj.data.Index(ind,7);
hn2 = obj.data.Index(ind,8);
[hist1] = reverse_categorizeNeuronalHist(hn1);
[hist2] = reverse_categorizeNeuronalHist(hn2);

load(obj.data.setNames{ind})

    sta_mean = obj.data.Index(ind,27);


plot([-50:50],sta_mean);

hold on
day_ses = cell2mat(cellstr(obj.data.setNames{ind}(end-49:end-28)));

title(([day_ses '       spike ch' num2str(obj.data.Index(ind,1)) ' ' cell2mat(hist1) '    field ch' num2str(obj.data.Index(ind,2)) ' ' cell2mat(hist2)]))


