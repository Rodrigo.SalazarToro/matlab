function obj = hilbertphase(varargin)

%creates sta object
%run at session level

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'ide_only',1);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'hilbertphase';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'hilbertphase';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end


function obj = createObject(Args,varargin)
sdir = pwd;

%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end
if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        if Args.ide_only
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML','rule',1);
        else
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML');
        end
    else
        if Args.ide_only
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','rule',1);
        else
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
        end
    end
end
%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end
sets = 0;
if ~isempty(mtst) && match2sample && ~isempty(all_trials)
    
    N = NeuronalHist;
    %get list of noisy channels and kick them out
    cd([sdir filesep 'lfp'])
    load rejectedTrials
    
    
    %go to lfp2 dir
    cd([sdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
    %get list of hilbertpairs
    hilbert_pairs = nptDir('hilbertpair*.mat');
    
    incorr_hilbert_pairs = nptDir('incorrect_hilbertpair*.mat');
    
    
    numb_pairs = size(hilbert_pairs,1);
    %run through all pairs
    for sp = 1:numb_pairs
        
        load(hilbert_pairs(sp).name)
        
        ch1 = hilbert_groups.channels(1);
        ch2 = hilbert_groups.channels(2);
        
        %Gets rid of channels that were rejected due to LFP noise
        if isempty(intersect(ch1, rejectCH)) && isempty(intersect(ch2, rejectCH))
            c = size(hilbert_groups.cluster_info,2);
            
            %EACH CLUSTER GETS ITS OWN ENTRY
            %run through the clusters for each group
            for x = 1 : c; %this is the number of clusters the reference channel has
                sets = sets + 1;
                
                %channels
                if Args.ml
                    [~,ch1] = intersect(N.gridPos,hilbert_groups.channels(1));
                    [~,ch2] = intersect(N.gridPos,hilbert_groups.channels(2));
                end
                data.Index(sets,1) = ch1;
                data.Index(sets,2) = ch2;
                
                
                %INDEX 7-8: histology number
                data.Index(sets,7) = N.number(ch1);
                
                if data.Index(sets,7) == 17
                    pwd
                end
                
                data.Index(sets,8) = N.number(ch2);
                
                %INDEX 9-10: grid position
                data.Index(sets,9) = N.gridPos(ch1);
                data.Index(sets,10) = N.gridPos(ch2);
                
                %INDEX 37-38: recoriding depths
                data.Index(sets,37) = N.recordedDepth(ch1);
                data.Index(sets,38) = N.recordedDepth(ch2);
                
                %INDEX 3:determine if cluster is multi or single (multi = 1, single = 2,3 etc.)
                if strmatch(hilbert_groups.cluster_info{x},'cluster01m')
                    data.Index(sets,3) = 1;
                else
                    data.Index(sets,3) = x; %single units
                end
                
                
                if Args.ml
                    if N.gridPos(ch1) > 32 && N.gridPos(ch2) > 32
                        pp = 1;
                    else
                        pp = 0;
                    end
                    if N.gridPos(ch1) <= 32 && N.gridPos(ch2) <= 32
                        ff = 1;
                    else
                        ff = 0;
                    end
                    if (N.gridPos(ch1) > 32 && N.gridPos(ch2) <= 32) || (N.gridPos(ch1) <= 32 && N.gridPos(ch2) > 32)
                        pf = 1;
                    else
                        pf = 0;
                    end
                else
                    if strncmpi(N.cortex(ch1),'P',1) && strncmpi(N.cortex(ch2),'P',1)
                        pp = 1;
                    else
                        pp = 0;
                    end
                    if strncmpi(N.cortex(ch1),'F',1) && strncmpi(N.cortex(ch2),'F',1)
                        ff = 1;
                    else
                        ff = 0;
                    end
                    if strncmpi(N.cortex(ch1),'P',1) && strncmpi(N.cortex(ch2),'F',1) %parietal channels are always first
                        pf = 1;
                    else
                        pf = 0;
                    end
                end
                
                %parietal Vs parietal
                data.Index(sets,4) = pp;
                %parietal Vs frontal
                data.Index(sets,5) = pf;
                %frontal Vs frontal
                data.Index(sets,6) = ff;
                
                %DOUBLE CHECK THIS
                %if pp, indicate which side of the ips
                if pp
                    hnumber1 = N.number(ch1);
                    hnumber2 = N.number(ch2);
                    if (hnumber1 == 8 || hnumber1 == 9 || hnumber1 == 10 || hnumber1 == 12 || hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 8 || hnumber1 == 9 || hnumber2 == 10 || hnumber2 == 12 || hnumber2 == 11 || hnumber2 == 13)
                        if (hnumber1 == 8 || hnumber1 == 9 || hnumber1 == 10 || hnumber1 == 12) && (hnumber2 == 8 || hnumber1 == 9 || hnumber2 == 10 || hnumber2 == 12)
                            data.Index(sets,11) = 1; %medial/medial
                        elseif (hnumber1 == 11 || hnumber1 == 13) && (hnumber2 == 11 || hnumber2 == 13)
                            data.Index(sets,11) = 2; %lateral/lateral
                        else
                            data.Index(sets,11) = 3; %medial/lateral
                        end
                    end
                else
%                     data.Index(sets,7) = 0;
                end
                
                %number of spikes
                nspikes = hilbert_groups.nspikes{x};
                data.Index(sets,16) = nspikes(1);
                data.Index(sets,17) = nspikes(2);
                data.Index(sets,18) = nspikes(3);
                data.Index(sets,19) = nspikes(4);
                data.Index(sets,20) = nspikes(5);
                data.Index(sets,21) = nspikes(6);
                data.Index(sets,22) = nspikes(7);
                
                
                means = hilbert_groups.mean{x};
                %mean instantaneous phase
                data.Index(sets,23) = means(1);
                data.Index(sets,24) = means(2);
                data.Index(sets,25) = means(3);
                data.Index(sets,26) = means(4);
                data.Index(sets,27) = means(5);
                data.Index(sets,28) = means(6);
                data.Index(sets,29) = means(7);
                
                rtest = hilbert_groups.rtest{x};
                %pval Rayleigh test
                data.Index(sets,30) = rtest(1);
                data.Index(sets,31) = rtest(2);
                data.Index(sets,32) = rtest(3);
                data.Index(sets,33) = rtest(4);
                data.Index(sets,34) = rtest(5);
                data.Index(sets,35) = rtest(6);
                data.Index(sets,36) = rtest(7);
                
                %%
                %incorrect trials, includes ITI
                load(incorr_hilbert_pairs(sp).name)
                %number of spikes
                nspikes = hilbert_groups.nspikes{x};
                data.Index(sets,39) = nspikes(1);
                data.Index(sets,40) = nspikes(2);
                data.Index(sets,41) = nspikes(3);
                data.Index(sets,42) = nspikes(4);
                data.Index(sets,43) = nspikes(5);
                data.Index(sets,44) = nspikes(6);
                data.Index(sets,45) = nspikes(7);
                data.Index(sets,46) = nspikes(8);
                
                means = hilbert_groups.mean{x};
                %mean instantaneous phase
                data.Index(sets,47) = means(1);
                data.Index(sets,48) = means(2);
                data.Index(sets,49) = means(3);
                data.Index(sets,50) = means(4);
                data.Index(sets,51) = means(5);
                data.Index(sets,52) = means(6);
                data.Index(sets,53) = means(7);
                data.Index(sets,54) = means(8);
                
                rtest = hilbert_groups.rtest{x};
                %pval Rayleigh test
                data.Index(sets,55) = rtest(1);
                data.Index(sets,56) = rtest(2);
                data.Index(sets,57) = rtest(3);
                data.Index(sets,58) = rtest(4);
                data.Index(sets,59) = rtest(5);
                data.Index(sets,60) = rtest(6);
                data.Index(sets,61) = rtest(7);
                data.Index(sets,62) = rtest(8);
                %%
                
                
                %set name 1 : hilbert
                data.setNames{sets,1} = [lfp2dir filesep hilbert_pairs(sp).name];
            end
        end
    end
    data.numSets = sets;
    %     load global_sta
    %     data.global_surrogates_sta{1,1} = global_sta';
    %     data.global_surrogates_sta{1,2} = increments;
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end


function obj = createEmptyObject(Args)
data.Index = [];
data.numSets = 0;
%data.global_surrogates_sta = {};
data.global_surrogates = {};
data.channel_std = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
