function days_neuronalhist

%run at betty level
%summarizes histology information


load idedays
numdays = size(alldays,2);
monkeydir = pwd;
all_areas = [];
for x = 1 : numdays
    cd([monkeydir filesep alldays{x} filesep 'session01'])
    N = NeuronalHist
    %get snr informatin
    cd('highpass')
    load SNR_channels
    snrs = channel_snr_list(:,2);
    %get rid of channels with snr below 1.8
    N.number(snrs < 1.8) = [];
    %make list of all areas
    all_areas = [all_areas N.number];
  
end

u = unique(all_areas)
numareas = size(u,2)
for nu = 1 : numareas
    bins(nu) = size(find(all_areas == u(nu)),2);
    xlab{nu} = num2str(u(nu));
end

cd(monkeydir)

bar(bins)

