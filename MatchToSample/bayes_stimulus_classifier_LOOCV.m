function bayes_stimulus_classifier_LOOCV(varargin)

%WORKS RUN ON 090701 (total 53%)

%run at session level
%computes LOOCV
Args = struct('ml',0,'loc',0,'ide',0,'mi_cutoff',0,'cor_range',[],'phase_range',[]);
Args.flags = {'ml','comb','loc','ide'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%get stimulus list
if Args.ide
    stimulus = stimulus_list('ml','ide');
elseif Args.loc
    stimulus = stimulus_list('ml','loc');
else
    stimulus = stimulus_list('ml');
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);
cpd = nptDir('corr_phase_delay.mat');
if isempty(cpd)
    cd(sesdir)
    if Args.ml
        corr_phase_list('ml')
    else
        corr_phase_list
    end
    cd([sesdir filesep 'lfp' filesep 'lfp2']);
    load corr_phase_delay
else
    %load correlation coefficients and phases
    load corr_phase_delay %compute with corr_phase_list.m
end

if Args.ml
    pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    [num_trials,num_points] = size(trial_corrcoefs{pair_list(1)});
else
    num_pairs = size(trial_corrcoefs,2);
    [num_trials,num_points] = size(trial_corrcoefs{1});
    pair_list = [1 : num_pairs];
end

if isempty(Args.cor_range)
    cor_range = [0:.1:1];
else
    cor_range = Args.cor_range;
end
cor_range = single(cor_range);
c_range = size(cor_range,2);

if isempty(Args.phase_range)
    ph_range = [-50:1:50];
else
    ph_range = Args.phase_range;
end
ph_range = single(ph_range);
s_phase = size(ph_range,2);


m = 0;
for leave = 1 : num_trials
    %make distributions of conditional probabilities P(r|s)
    for p = pair_list
        if Args.loc || Args.ide
            joint_cond_dist = cell(3,9);
            joint_cum_dist = cell(1,9);
        else
            joint_cond_dist = cell(9,9);
            joint_cum_dist = cell(1,9);
        end
        
        for t = 1 : num_trials
            if t ~= leave
                for tp = 1 : num_points
                    point = single(abs(trial_corrcoefs{p}(t,tp)));
                    
                    
                    
                    
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%
                    point = round(point*10)/10; %need to change this so that the cor_range can be changes
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%5
                    
                    
                    
                    
                    [~,po] = intersect(cor_range ,point);
                    
                    phase_point = single(trial_phases{p}(t,tp));
                    [~,ph]=min(abs(ph_range - phase_point));
                    
                    if isempty(joint_cond_dist{stimulus(t),tp})
                        joint_cond_dist{stimulus(t),tp} = zeros(s_phase,c_range);
                    end
                    if isempty(joint_cum_dist{1,tp})
                        joint_cum_dist{1,tp} = zeros(s_phase,c_range);
                    end
                    if ~isempty(ph) && ~isempty(po)
                        joint_cond_dist{stimulus(t),tp}(ph,po) = joint_cond_dist{stimulus(t),tp}(ph,po) + 1;
                    end
                end
            end
        end
        
        for stims = 1 : max(stimulus) % 3 or 9
            for tp = 1 : num_points
                
                joint_cond_dist{stims,tp} = joint_cond_dist{stims,tp} ./ sum(nansum(joint_cond_dist{stims,tp}));
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
            end
        end
        
        all_joint_cond{p} = joint_cond_dist;
        all_joint_cum{p} = joint_cum_dist;
    end
    
    pair_counter = 0;
    mutual_info = zeros(size(pair_list,2),9);
    predictions = zeros(size(pair_list,2),9);
    probs = zeros(size(pair_list,2),9);
    for nump = pair_list
        pair_counter = pair_counter + 1;
        if Args.loc || Args.ide
            stim_probabilities = zeros(3,9);
        else
            stim_probabilities = zeros(9,9);
        end
        
        for ntime = 1 : num_points
            point = single(abs(trial_corrcoefs{nump}(leave,ntime)));
            point = round(point*10)/10;
            [~,po] = intersect(cor_range,point);
            
            phase_point = single(trial_phases{nump}(leave,ntime));
            [~,ph]=min(abs(ph_range - phase_point));
            
            cum_prob = all_joint_cum{nump}{1,ntime}(ph,po);
            for stims = 1 : max(stimulus)
                cond_prob = all_joint_cond{nump}{stims,ntime}(ph,po);
                
                if Args.loc || Args.ide
                    prob_sample = 1/3;
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                end
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                if ~isempty(cum_prob) && ~isempty(cond_prob)
                    stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                end
            end
            %get rid of nans
            stim_probabilities(isnan(stim_probabilities)) = 0;
            
            if ~isempty(cum_prob)
                post_probs = stim_probabilities(:,ntime)';
                mi = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
                mutual_info(pair_counter,ntime) = mi;
                
                if mi > Args.mi_cutoff
                    [time_prob,predict] = max(post_probs);
                    predictions(pair_counter,ntime) = predict;
                    probs(pair_counter,ntime) = time_prob;
                end
            end
        end
    end
    
    %determine best match
    b = zeros(1,max(stimulus));
    for xs = 1 : max(stimulus);
        %         b(xs) = size(find(predictions  == xs),1);
        b(xs) = sum(mutual_info(find(predictions  == xs)));
    end
    [~,pre] = max(b);
    
    if pre == stimulus(leave)
        m = m + 1;
    end
    leave
    (m/leave) * 100
end
m

performance = (m/leave) * 100;

if Args.ide
    save bsc_loocv_performance_ide performance cor_range ph_range
elseif Args.loc
    save bsc_loocv_performance_loc performance cor_range ph_range
else
    save bsc_loocv_performance performance cor_range ph_range
end

cd(sesdir)
