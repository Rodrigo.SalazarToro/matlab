function ANN_classify_trials

%run at session level
cd('/media/raid/data/monkey/betty/090701/session01/')

sesdir = pwd;

mt = mtstrial('auto','ml','RTfromML','redosetNames');
total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
numb_trials = size(total_trials,2);

c = mtscpp('auto','ml','train','ide_only','glm_data');
%[~, total_pairs] = get(c,'Number','sorted'); %get list of sorted pairs
% [~, total_pairs] = get(c,'Number','ide_only','sorted','glm_delay',.01);
[~, total_pairs] = get(c,'Number','ide_only','sorted','pp');

npairs = size(total_pairs,2)

cd(['lfp' filesep 'lfp2'])
load glm_fits_train 

observ = npairs * 9; %nine is the number of time bins

%make input data

for t = 1 : numb_trials
    in = [];
    iin = [];
    for n = total_pairs
 in = [in trial_fits_binary{n}(t,:)];
        

        %in = [in trial_corrcoefs{n}(t,:)];
        
        iin = [iin ; trial_fits_train{n}(t,:)];
  iin = [iin ; trial_fits_binary{n}(t,:)];
        
    end
    inputs((1:observ),t) = in';

    
    iinputs{t} = iin;

    
end

load stimulus
targets = [];
for stims = 1 : size(stimulus,2);
    targets(stimulus(stims),stims) = 1;
end

save targets targets
save inputs inputs
save iinputs iinputs

load targets
load inputs


r = randperm(size(inputs,2));

test_inputs = inputs(:,(r(1:132)));

train_inputs = inputs(:,(r(133:end)));
test_targets = targets(:,(r(1:132)));
train_targets = targets(:,(r(133:end)));




% Create a Pattern Recognition Network
hiddenLayerSize = 1000;
net = patternnet(hiddenLayerSize);



% Set up Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 10/100;
net.divideParam.testRatio = 20/100;


% % % % Train the Network
net = train(net,inputs,targets);
% % % 
% % % 
% % % % Test the Network
% % % classes = net(test_inputs);
%performance = perform(net,targets,outputs)

% View the Network
%view(net)

% Plots
% Uncomment these lines to enable various plots.
%figure, plotperform(tr)
%figure, plottrainstate(tr)
%figure, plotconfusion(targets,outputs)
%figure, ploterrhist(errors)

% % size(find((test_targets+classes) == 2),1) / 132

% y = net(test_inputs);
% 
classes = vec2ind(y)












