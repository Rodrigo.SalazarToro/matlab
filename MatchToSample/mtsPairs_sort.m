function [ind_pairs_sort pair_directory_sort] = mtsPairs_sort(varargin)

% sorts through pair_directory and ind_pairs created by mtsPairs
% saves only the specified pairs
% run at monkey level
Args = struct('PP',0,'PF',0,'FF',0,'IND',0);
Args.flags = {'PP','PF','FF','IND'};
[Args,modvarargin] = getOptArgs(varargin,Args);

load ind_pairs
load pair_directory

%pp = 3;
%pf = 2;
%ff = 1;

num_pairs = size(pair_directory,2);
newp=0;
if Args.PP
    g = 3;
elseif Args.PF
    g = 2;
else
    g = 1;
end

for pair = 1 : num_pairs
    group_info = str2double(pair_directory(pair).comb(3));
    if str2double(ind_pairs(pair).comb(3)) ~= group_info
        fprintf(1,'warning: pair_direcotry and ind_pairs mismatch\n')
    end
    
    if group_info == g
        if ind_pairs(pair).number > 0
            if ((size(ind_pairs(pair).depths,1)) ~= (size(pair_directory(pair).depths,1)) && Args.IND) || ~Args.IND
                newp = newp+1;
                ind_pairs_sort(newp) = ind_pairs(pair);
                pair_directory_sort(newp) = pair_directory(pair);
            end
        end
    end
end

 
