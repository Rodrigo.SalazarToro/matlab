function run_STA_SXC(varargin)

%run at monkey level
Args = struct('ml',0,'days',[],'spike_spike',0,'redo_spike_spike',0,'spike_spike_shift',0,'ide_only',0);
Args.flags = {'ml','spike_spike','ide_only','redo_spike_spike','ide_only','spike_spike_shift'};
[Args,modvarargin] = getOptArgs(varargin,Args);

RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    if Args.ml
        ses = 1; sessions = {'session01'};
    else
        ses = [1:2]; sessions = {'session02','session03'};
    end
    
    for sesnumb = ses
        cd([monkeydir filesep Args.days{d}]); daydir = pwd;
        if ~isempty(nptdir(sessions{sesnumb}))
            cd([sessions{sesnumb}]); sesdir = pwd;
            if Args.ml
                mt = mtstrial('auto','ML','RTfromML','redosetNames');
                r = 1; %only identity need to save in identity dir
                total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',r);
            else
                mt=mtstrial('auto','redosetNames');
                %determine rule
                r = mt.data.Index(1,1);
                total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
            end
            
            %if ide only then skip session if it is location(2)
            if ((Args.ide_only && r == 1) || (~Args.ide_only)) && ~isempty(total_trials)
                
                %get trial timing information
                cue_on = floor(mt.data.CueOnset);
                cue_off = floor(mt.data.CueOffset);
                match = floor(mt.data.MatchOnset);
                
                num_trials = size(total_trials,2);
                %get group information
                N = NeuronalHist;
                [NeuroInfo] = NeuronalChAssign(); %get the "group" number that corresponds to each channel
                
                N = NeuronalHist;
                if Args.ml
                    [g] = sorted_groups('ml');
                else
                    [g] = sorted_groups;
                end
                
                
                %find groups with spikes
                groups = nptDir('group*');
                ngroups = [1 : size(groups,1)];
                
                if Args.spike_spike
                    pwd
                    if r == 1
                        fprintf(1,'spike spike identity \n')
                    else
                        fprintf(1,'spike spike location \n')
                    end
                    for g = ngroups
                        fprintf('\n%0.5g     ',g)
                        for gg = ngroups
                            fprintf(' %0.5g',gg)
                            
                            cd([sesdir filesep 'lfp' filesep 'lfp2'])
                            g1ch = str2double(groups(g).name(6:end));
                            [~,ii] = intersect(NeuroInfo.groups,g1ch);
                            channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                            
                            g2ch = str2double(groups(gg).name(6:end));
                            [~,ii] = intersect(NeuroInfo.groups,g2ch);
                            channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                            
                            %determine if pair has already been written
                            spair = ['spikexcorr_g' num2strpad(g1ch,4) 'g' num2strpad(g2ch,4) '.mat'];
                            cd(sesdir)
                            
                            if isempty(nptdir(spair)) || Args.redo_spike_spike
                                %run spike spike calculations
                                spikexcorr = spike_spike_xcorr('groups',[g gg],'trials',total_trials,'mt_obj',mt,'binary');
                                cd([sesdir filesep 'highpass'])
                                save(spair,'spikexcorr')
                            end
                        end
                    end
                end
                
                
                % still need to write this code
                if Args.spike_spike_shift
                    pwd
                    if r == 1
                        fprintf(1,'spike spike shift identity \n')
                    else
                        fprintf(1,'spike spike shift location \n')
                    end
                    for g = ngroups
                        fprintf('\n%0.5g     ',g)
                        for gg = ngroups
                            fprintf(' %0.5g',gg)
                            
                            cd([sesdir filesep 'lfp' filesep 'lfp2'])
                            g1ch = str2double(groups(g).name(6:end));
                            [~,ii] = intersect(NeuroInfo.groups,g1ch);
                            channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                            
                            g2ch = str2double(groups(gg).name(6:end));
                            [~,ii] = intersect(NeuroInfo.groups,g2ch);
                            channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
                            
                            %determine if pair has already been written
                            spair = ['spikexcorr_shift_g' num2strpad(g1ch,4) 'g' num2strpad(g2ch,4) '.mat'];
                            cd(sesdir)
                            
                            if isempty(nptdir(spair)) || Args.redo_spike_spike
                                %run spike spike calculations
                                spikexcorr_shift = spike_spike_shift_xcorr('groups',[g gg],'trials',total_trials,'mt_obj',mt,'binary');
                                cd([sesdir filesep 'highpass'])
                                save(spair,'spikexcorr_shift')
                            end
                        end
                    end
                end
                
            end
        end
    end
end
%for x = 1:9;subplot(3,3,x);plot(sta_groups{x});p = prctile(surrogate_sta_groups{x},[.05 99.95]);hold on;plot([0 201],[p(1) p(1)],'r');hold on;plot([0 201],[p(2) p(2)],'r');hold on;plot([0 201],[0 0],'k');axis([1 201 -40 40]);end

