function mi_single_pair(varargin)

%run at session level
%computes mutual information for each pair at the three locations across the 3 identities
Args = struct('ml',0,'cor_range',[],'test',0,'sample',0,'perm',0,'fix',0,'all',0);
Args.flags = {'ml','test','sample','perm','fix','all'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

%find sorted pairs
[~,pair_list] = sorted_groups;
num_pairs = size(pair_list,2);

stimulus_lists = cell(1,3);
out_list = cell(1,3);
%get stimulus list and xcorr information
for locations = 1 : 3
    if Args.ml
        if Args.fix
            out_list{locations} = corr_phase_list('ml','fix','locations',locations);
        elseif Args.sample
            out_list{locations} = corr_phase_list('ml','sample','locations',locations);
        elseif Args.all
            out_list{locations} = corr_phase_list('ml','all','locations',locations);
        else
            out_list{locations} = corr_phase_list('ml','locations',locations);
        end
        stimulus_lists{locations} = stimulus_list('ml','locbyide',locations);
    else
        if Args.fix
            out_list{locations} = corr_phase_list('fix','locations',locations);
        elseif Args.sample
            out_list{locations} = corr_phase_list('sample','locations',locations);
        elseif Args.all
            out_list{locations} = corr_phase_list('all','locations',locations);
        else
            out_list{locations} = corr_phase_list('locations',locations);
        end
        stimulus_lists{locations} = stimulus_list('locbyide',locations);
    end
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);

if isempty(Args.cor_range)
    cor_range = [0:.1:1];
else
    cor_range = Args.cor_range;
end
cor_range = single(cor_range);
c_range = size(cor_range,2);

for np = 1 : nperms
    pair_counter = 0;
    pair_mutual_info = cell(1,num_pairs);
    if ~Args.perm
        pair_preferred_loc = cell(1,num_pairs);
    end
    for p = pair_list
        pair_counter = pair_counter + 1;
        
        
        mutual_info = zeros(3,33);
        if Args.perm
            locs = pair_preferred_loc{pair_counter};
        else
            locs = [1:3];
        end
        
        for locations = locs
            trial_corrcoefs = out_list{locations}.trial_corrcoefs{p};
            trial_phases = out_list{locations}.trial_phases{p};
            [num_trials, num_points] = size(trial_corrcoefs);
            
            stimulus = stimulus_lists{locations};
            
            if Args.perm
                stimulus = stimulus(randperm(size(stimulus,2)));
            end
            
            
            
            joint_cond_dist = cell(3,num_points);
            %calculate conditional distributions for all trials
            for t = 1 : num_trials
                for tp = 1 : num_points
                    
                    point = single(abs(trial_corrcoefs(t,tp)));

                    if isempty(joint_cond_dist{stimulus(t),tp})
                        joint_cond_dist{stimulus(t),tp} = [point; phase_point];
                    else
                        joint_cond_dist{stimulus(t),tp} = [joint_cond_dist{stimulus(t),tp} [point; phase_point]];
                        
                    end
                end
            end
            
            %normalize conditional distributions and make cumulative distribution
            joint_cum_dist = cell(1,num_points);
            for stims = 1 : 3
                for tp = 1 : num_points
                    
                    data = joint_cond_dist{stims,tp}';
                    
                    MIN_XY=[0,-50];
                    MAX_XY=[1,50];
                    
                    ngrid = 32;
                    
                    [~,density]=kde2d(data,ngrid,MIN_XY,MAX_XY);

                    density = density ./ sum(nansum(density));
                    
                    %                 surf(X,Y,density+1,'LineStyle','none'), view([0,60])
                    %                 colormap hot, hold on, alpha(.8)
                    %                 plot(data(:,1),data(:,2),'w.','MarkerSize',5)
                    
                    density(density < 0) = 0;
                    density(density < .0001) = 0;
                    joint_cond_dist{stims,tp} = density;

                    prob_sample = 1/3;
                    
                    if isempty(joint_cum_dist{1,tp})
                        joint_cum_dist{1,tp} = zeros(ngrid,ngrid);
                    end
                    
                    joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + (joint_cond_dist{stims,tp} * prob_sample);
                end
            end
            
            posterior_distributions = cell(3,num_points);
            for stims = 1 : 3
                for tp = 1 : num_points
                    %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                    posterior_distributions{stims,tp} = (joint_cond_dist{stims,tp} * prob_sample) ./ joint_cum_dist{1,tp};
                    posterior_distributions{stims,tp}(isnan(posterior_distributions{stims,tp})) = 0;
                end
            end
            
            mi = mutual_info_pairs('posterior_distributions',posterior_distributions,'prob_response',joint_cum_dist);
            mutual_info(locations,:)= mi;
        end
        if ~Args.perm
            %determine best location
            [~,ii] = max(sum(mutual_info(:,(25:33))'));
            pair_mutual_info{pair_counter} = mutual_info(ii,:);
            pair_preferred_loc{pair_counter} = ii;
        else
            pair_mutual_info{pair_counter} = mutual_info(locs,:);
        end
    end
    
    % colors = {'r' 'g' 'b'};for y = 1 : num_pairs;for x = 1:3;plot(sum(pair_mutual_info{y}{x}),colors{x});hold on;end;pause;close all;end
    if Args.perm
        all_perms{np} = pair_mutual_info;
    end
end
cd(sesdir)

if ~Args.test
    if Args.perm
        save mi_single_pair_permutations all_perms
        %     mi_pair_surrogates %make prctiles
    else
        save mi_single_pair pair_mutual_info pair_preferred_loc
    end
end
