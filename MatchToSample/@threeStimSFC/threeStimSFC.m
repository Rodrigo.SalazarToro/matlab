function obj = threeStimPsth(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'threeStimSFC';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'threeStSFC';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedCells',[],'selectedLFP',[],'noSurrogate',0,'classname','threeStimSFC','matvarname','threeStSFC');
Args.flags = {'Auto','noSurrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto','noSurrogate'});
Args.matname = [Args.classname '.mat'];

mtst = mtstrial('auto',varargin{:});
groups = nptDir('group*');
[thegr,comb] = NeuronalChAssign('onlyPP');
%
% [PPch,comb,CHcomb] = checkChannels('cohPP');
% [PFch,comb,CHcomb] = checkChannels('cohPF');
sdir = pwd;
rules = {'I','L'};
if  ~isempty(groups) & ~isempty(mtst)
    count = 1 ;
    data.Index = [];
    fband = [12 25;26 45];
    
    for g = 1 : size(groups,1)
        cd(groups(g).name)
        gdir = pwd;
        if ~isempty(Args.selectedLFP)
            list = Args.selectedLFP;
            %             dlevel = strread(list.days,'%s','delimiter','/');
            %             theday = dlevel{strncmp('0',dlevel,1)};
            indg = strmatch(gdir(1:length(list.days{1})),list.days);
            if ~isempty(indg)
                thech = unique(list.ch(indg,:));
                tgroup = thegr.groups(thech);
                if ~isempty(find(str2num(gdir(end-3:end)) == tgroup));
                    skip = false;
                else
                    skip = true;
                end
            else
                skip = true;
            end
        else
            skip = false;
        end
        
        if ~skip
            clusters = nptDir('cluster*');
            suaarea = thegr.cortex(g);
            
            for c = 1 : size(clusters,1)
                cd(clusters(c).name)
                
                if ~isempty(Args.selectedCells)
                    
                    
                    if ~isempty(strmatch(sprintf('%s%s%s',gdir,filesep,clusters(c).name),Args.selectedCells)) % if args selected cells for only make an object out the whole
                        
                        select = true;
                    else
                        select = false;
                    end
                else
                    select = true;
                end
                if select
                    ideSFCfile = nptDir('SFCideTuning11*.mat');
                    locSFCfile = nptDir('SFClocTuning11*.mat');
                    
                    SFCfile = nptDir('SFCallCuesComb11*.mat');
                    
                    if ~isempty(ideSFCfile) && ~isempty(locSFCfile) && ~isempty(SFCfile)
                        for r = 1 : length(ideSFCfile)
                            ide = load(ideSFCfile(r).name);
                            loc = load(locSFCfile(r).name);
                            allc = load(SFCfile(r).name,'SurProb','C');
                            f = ide.f;
                            limitB = find(f>= fband(1,1) & f<= fband(1,2));
                            for ch = 1 : size(ide.C{1},2)
                                if ~isempty(Args.selectedLFP)
                                    if ~isempty(find(ch == thech))
                                        cont = true;
                                    else
                                        cont = false;
                                    end
                                else
                                    cont = true;
                                end
                                if cont
                                    lfparea = thegr.cortex(ch);
                                    bad = []; for p = 1 : 4; if length(ide.C{p}(:,ch,:)) == 1 || isempty(ide.C{p}(:,ch,:)) || (length(unique(isnan(squeeze(ide.C{p}(:,ch,:))))) == 1 && unique(isnan(squeeze(ide.C{p}(:,ch,:)))) == 1) ; bad = [bad 1]; else bad = [bad 0]; end; end;
                                    if length(unique(bad)) > 1 || unique(bad) == 0
                                        data.Index(count,1) = 1;
                                        data.Index(count,2) = mtst.data.Index(1,1);% ideSFCfile.name(end-4); % the rule
                                        
                                        data.Index(count,3) = suaarea;
                                        data.Index(count,4) = lfparea;
                                        
                                        data.Index(count,5) = count;
                                        data.Index(count,6) = clusters(c).name(end);
                                        
                                        maxc = []; for p = 1 : 4; maxc = [maxc max(max(max(ide.C{p}(:,ch,:))))]; end
                                        data.Index(count,7) = max(maxc);
                                        data.Index(count,8) = ch;
                                        data.Index(count,10) = thegr.groups(ch);
                                        if ch == g
                                            data.Index(count,9) = 1;
                                        else
                                            data.Index(count,9) = 0;
                                        end
                                        
                                        %% surrogate
                                        if Args.noSurrogate
                                            for p = 1 : 4; ab(p) = length(find(squeeze(allc.C{p}(limitB,ch)) > squeeze(allc.SurProb{p}(limitB,3,ch)))); end
                                            if sum(ab(3:4) == 0)
                                                co = 0;
                                            elseif ab(3) ~= 0 && ab(4) ~= 0
                                                co = 3;
                                            elseif sum(ab(3:4) == ab(3))
                                                co = 1;
                                                
                                            elseif sum(ab(3:4) == ab(4))
                                                co = 2;
                                            end
                                        else
                                            co = 3;
                                        end
                                        data.Index(count,10) = co;
                                        % sfc above surrogate in the beta range for window 3 (1), 4 (2), both (3) or none (0)
                                        %% tuning index
                                        % Idex(:,11) ide tuning index
                                        % during IDE rule for window 4 cell
                                        % Idex(:,12) ide tuning index
                                        % during LOC rule for window 4 cell
                                        % Idex(:,13) loc tuning index
                                        % during IDE rule for window 4 cell
                                        % Idex(:,14) loc tuning index
                                        % during LOC rule for window 4 cell
                                        % Idex(:,15) ide tuning index
                                        % during IDE rule for window 4 lfp
                                        % Idex(:,16) ide tuning index
                                        % during LOC rule for window 4 lfp
                                        % Idex(:,17) loc tuning index
                                        % during IDE rule for window 4 lfp
                                        % Idex(:,18) loc tuning index
                                        % during LOC rule for window 4 lfp
                                        
                                        % allspikes{cue}(trial,period) =
                                        % nbr of spikes
                                        % cueComb = [1 2 3 1 2 3 1 2 3;1 1
                                        % 1 2 2 2 3 3 3]; first raw =
                                        % locations
                                        
                                        % cell
                                        %                                         ite = {'ide1' 'ide2' 'loc1' 'loc2'};
                                        %                                         asum = [15 19];
                                        %                                         for cases = 1 : 4
                                        %                                             for a = 1 : 2
                                        %                                                 for cu = 1 : 3
                                        %                                                     thedata = eval(sprintf('squeeze(mean(%s.power(cu).S(Day.comb(pa,a),limitB,:,4),3));',ite{cases}));
                                        %                                                     tarea(:,cu) = sum(thedata);
                                        %
                                        %                                                 end
                                        %                                                 Index(pa,asum(a) + cases) = reshape((max(tarea,[],2) - min(tarea,[],2)) ./ min(tarea,[],2),1,size(tarea,1));
                                        %                                             end
                                        %                                         end
                                        % %
                                        % %                                         % lfp
                                        %                                         ite = {'ide1' 'ide2' 'loc1' 'loc2'};
                                        %                                         asum = [15 19];
                                        %                                         for cases = 1 : 4
                                        %                                             for a = 1 : 2
                                        %                                                 for cu = 1 : 3
                                        %                                                     thedata = eval(sprintf('squeeze(mean(%s.power(cu).S(Day.comb(pa,a),limitB,:,4),3));',ite{cases}));
                                        %                                                     tarea(:,cu) = sum(thedata);
                                        %
                                        %                                                 end
                                        %                                                 Index(pa,asum(a) + cases) = reshape((max(tarea,[],2) - min(tarea,[],2)) ./ min(tarea,[],2),1,size(tarea,1));
                                        %                                             end
                                        %                                         end
                                        %
                                        %%
                                        
                                        data.setNames{count} = pwd;
                                        count = count + 1;
                                        
                                    end
                                end
                                
                            end
                        end
                    end
                end%c = 1 : size(clusters,1)
                cd(gdir)
                
            end
        end
        cd(sdir)%g = 1 : size(group,1)
        
    end
    
    data.numSets = count-1; % nbr of trial
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
    
else
    % create empty object
    fprintf('The mtstrial object is empty or the group directory does not exist \n');
    obj = createEmptyObject(Args);
    
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
