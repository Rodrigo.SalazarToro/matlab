function obj = plot(obj,varargin)


Args = struct('pLevel',3,'xaxis',[0 60],'pvalue',2,'allCues',0);
Args.flags = {'allCues'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
ch = obj.data.Index(ind,8);
Index = obj.data.Index;
if Args.allCues
    files = {sprintf('SFCallCuesComb11%d.mat',Index(ind,2)) sprintf('SFCallCuesComb11%d.mat',Index(ind,2))};
    maxtune = 1;
else
    files = {sprintf('SFCideTuning11%d.mat',Index(ind,2)) sprintf('SFClocTuning11%d.mat',Index(ind,2))};
    maxtune = 2;
end
labels = {'Identities' 'Locations'};
legs = {'ide' 'loc'};
winds = {'PreSample' 'Sample' 'Delay1' 'Delay2'};
sur = load(sprintf('SFCallCuesComb11%d.mat',Index(ind,2)));
maxc = [];

for tuning = 1 : maxtune
    load(sprintf('%s',files{tuning}))
    frange = find(f >= Args.xaxis(1) & f <= Args.xaxis(2));
    for al = 1 : 4
        subplot(4,maxtune,(tuning-1)*4 + al);
        plot(f,squeeze(C{al}(:,ch,:)));
        maxc = [maxc max(max(squeeze(C{al}(:,ch,:)))) max(squeeze(sur.SurProb{al}(frange,Args.pvalue,ch)))];
        hold on
        plot(f,squeeze(sur.SurProb{al}(:,Args.pvalue,ch)),'--')
    end
    
end

for sb = 1 : (4 * maxtune)
    subplot(4,maxtune,sb);
    axis([Args.xaxis 0 max(maxc)])
    if Args.allCues
        ylabel('Spike-field Coherence');
    else
        
        if sb == 1
            ylabel(labels{1});
            for it = 1  : 3; legsi{it} = sprintf('%s%d',legs{1},it); end; legend(legsi{:})
            
        elseif sb == 2
            ylabel(labels{2});
            for it = 1  : 3; legsi{it} = sprintf('%s%d',legs{2},it); end; legend(legsi{:})
        end
    end
    if mod(sb,2) == 1; title(winds{(sb-1)/2 + 1}); xlabel('Frequency [Hz]'); end
end


% display(obj.data.setNames{ind})

warning off all


fprintf('%s%s%s lfp %s group %d \n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2),obj.data.Index(ind,4),obj.data.Index(ind,10));
