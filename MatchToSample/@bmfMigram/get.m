function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'bmf_groups',1);
Args.flags = {'Number','ObjectLevel'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rt = [1 : size(obj.data.Index,1)]';
    
    %get only groups that pass the SNR, rejectCH, and POWER criteria
    if Args.bmf_groups
        rtemp1 = find(obj.data.Index(:,3) == 1)';
    else
        rtemp1 = rt;
    end
    rtemp2 = rt;
    
    varargout{1} = intersect(rtemp1,rtemp2);
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end

