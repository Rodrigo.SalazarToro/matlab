function obj = bmfMihgram(varargin)

%makes bmfCohgram object

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'bmfMigram';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'bmfMigram';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

daydir = pwd;
cd([daydir filesep 'session01'])
sesdir = pwd;
gg = bmf_groups; %get list of good groups
cd([sesdir filesep 'lfp'])
lfpdir = pwd;

if exist('cohgrams','dir')
    cd([lfpdir filesep 'cohgrams' filesep 'migrams'])
    midir = pwd
    files = nptDir('migram*');
    nfiles = size(files,1);
    if ~isempty(files)
        for f = 1 : nfiles
            load(files(f).name)
            %get groups
            g1 = migram.groups(1);
            g2 = migram.groups(2);
            data.Index(f,1) = g1;
            data.Index(f,2) = g2;
            
            %determine if both groups are good
            if ~isempty(intersect(gg,g1)) && ~isempty(intersect(gg,g2))
                data.Index(f,3) = 1;
            end
            data.setNames{f,1} = [midir filesep files(f).name];
        end
        
        data.numSets = nfiles;
        
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('*no migram files files missing \n');
        obj = createEmptyObject(Args);
    end
else
    fprintf('migram directory missing \n');
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields

% useful fields for most objects
data.Index = [];
data.cohgrams = {};
data.numSets = 0;
data.setNames = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
