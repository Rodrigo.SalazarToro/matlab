function obj = plot(obj,varargin)

%Arguments
%phase: plots the phase of the coherence
%ide:   plots all 5 stimuli

Args = struct('allgrams',1);
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end


%load migram info
load(obj.data.setNames{ind})


time = round(migram.t * 1000);
time = time(1:41);
ntime = size(time,2);

freq = fliplr(round(migram.f));
nfreq = size(freq,2);

gram = migram.migram((1:41),:)';
gram = flipud(gram);
gram = interp2(gram);

imagesc(gram)

set(gca,'XTick',[1:4:ntime*2-1])
set(gca,'XTickLabel',time(1:2:ntime))

set(gca,'YTick',[1:8:nfreq*4-1])
set(gca,'YTickLabel',freq(1:4:nfreq))
%sample on
hold on
plot([17, 17],[0 100],'color','r')

%sample off
hold on
plot([37, 37],[0 100],'color','r')

%earliest match
hold on
plot([69, 69],[0 100],'color','r')

hold on
colorbar
% set(gca, 'CLim', [0 1]);

title(['groups ' num2str(migram.groups)])


if Args.allgrams
% 
%     allgrams = zeros(33,41);
%     for y = dataindices
%         
%         load(obj.data.setNames{y})
%         
%         
%         time = round(migram.t * 1000);
%         time = time(1:41);
%         ntime = size(time,2);
%         
%         freq = fliplr(round(migram.f));
%         nfreq = size(freq,2);
%         
%         gram = migram.migram((1:41),:)';
%         gram = flipud(gram);
%         
% %         gram(single(gram) > .3) = 1;
%         
%         allgrams = allgrams + gram;
%     end
%     
%     figure
%     
%     imagesc(allgrams)
%     hold on
%     set(gca,'XTick',[1:2:ntime-1])
%     set(gca,'XTickLabel',time(1:2:ntime-1))
%     
%     set(gca,'YTick',[1:2:nfreq-1])
%     set(gca,'YTickLabel',freq(1:2:nfreq-1))
%     %sample on
%     hold on
%     plot([9, 9],[0 100],'color','r')
%     
%     %sample off
%     hold on
%     plot([19, 19],[0 100],'color','r')
%     
%     %earliest match
%     hold on
%     plot([35, 35],[0 100],'color','r')
%     
%     hold on
%     colorbar
%     set(gca, 'CLim', [0 2000]);
end







