function hilbertphase_analysis_script(varargin)

%script for running the hilbert phase analsysis based on tass_1998
%runs the following functions run_hilbertphase_mi, run_hilbertphase_mi

cd('/media/raid/data/monkey/betty')

Args = struct('ml',0,'days',[],'sessions',[],'relative_phase_angle',0,'hilbert_entropy',0,'hilbert_mi',0,'hilbert_mi_surr',0);
Args.flags = {'ml','relative_phase_angle','hilbert_entropy','hilbert_mi','hilbert_mi_surr'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        cd(monkeydir)
        
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            
            %calculate the relative phase angles
            if Args.relative_phase_angle
                if Args.ml
                    run_relative_phase_angle('ml')
                else
                    
                end
            end
            cd(sesdir)
            %calculate the normalized shannon entropy
            if Args.hilbert_entropy
                if Args.ml
                    run_hilbertphase_entropy('ml')
                else
                    run_hilbertphase_entropy
                end
            end
            cd(sesdir)
            %calculate mi based on the normalized shannon entropies
            if Args.hilbert_mi
                if Args.ml
                    run_hilbertphase_mi('ml')
                else
                    run_hilbertphase_mi
                end
            end
            cd(sesdir)
            %calculate mi surrogates based on the normalized shannon entropies
            if Args.hilbert_mi_surr
                if Args.ml
                    run_hilbertphase_mi('ml','surrogate')
                else
                    run_hilbertphase_mi('surrogate')
                end
            end
            cd(sesdir)
        end
    end
end

cd(monkeydir)

