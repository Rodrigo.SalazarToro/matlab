function sim_writeCPPdataML(varargin)

cd('/media/MONTY/data/clark')
%run at monkey level
%used on simulated lfp data

Args = struct('ml',0,'ide_only',0,'days',[],'sessions',[],'monkey','betty','windowSize',200,'windowStep',50,'phaseSize',50,...
    'avg_correlograms',0,'epoch_correlograms',0,...
    'epoch_correlograms_surrogates',0,'cross',0,'threshold',0,'channelpairs',0,'redo_channelpairs',0);

Args.flags = {'ml','ide_only','epoch_correlograms',...
    'cross','threshold','channelpairs','redo_channelpairs'};
Args = getOptArgs(varargin,Args);

%Xcorr
windowSize = Args.windowSize;
windowStep = Args.windowStep;
phaseSize = Args.phaseSize;

daydir = cd;
%Get all the switch days


    run_days = [1:size(Args.days,2)];

for alldays = run_days
    alldays
    cd ([daydir filesep Args.days{alldays}])
    days = pwd;
    numb_sessions = nptDir('*session0*')
    
    for sessions = Args.sessions;
        cd ([days filesep numb_sessions(sessions).name]);
        ses = pwd;
        [NeuroInfo] = NeuronalChAssign;
        
        %GET A LIST OF CHANNELS THAT WERE USED FOR ANALYSIS, THIS WILL
        %INCLUDE "FLAT" CHANNELS.
        
        %MAKE AND INDEX TO SAVE IN RULE FOLDERS
        if Args.ml
            all_channels = NeuroInfo.groups;
        else
            all_channels = (1:size(NeuroInfo.groups,2));
        end
        
        
        %Get number of channels
        chnumb=length(NeuroInfo.channels);
        
        %if there aren't enough channels then do nothing
        if chnumb < 2
            fprintf(1,'ONLY ONE CHANNEL!\n')
            return
        end
         
        %calculate number of pairs (n choose 2)
        pairs = (chnumb*(chnumb-1)) / 2;
        
        %create trial object
        if Args.ml
            mt = mtstrial('auto','ML','RTfromML');
        else
            mt=mtstrial('auto');
        end
        
        if ~Args.ml
            cd ([ses filesep 'lfp']);
            load rejectedTrials stdTrials PWTrials rejectCH rejecTrials
        end

        %Make Epoch Correlograms
        if Args.epoch_correlograms || Args.epoch_correlograms_surrogates
            cd([ses filesep 'lfp'])
            %makes 4 correlograms for each epoch (400ms each)
            sim_epochCorrelograms('ml',Args.ml,'chnumb',chnumb,'monkey',Args.monkey,'correlograms',Args.epoch_correlograms,'surrogate',Args.epoch_correlograms_surrogates) %leaves in lfp directory
        end
        
        if ~Args.ml
            cd([ses filesep 'lfp' filesep 'lfp2'])
            save rejectedTrials stdTrials PWTrials rejectCH rejecTrials %saves index of rejected trials in lfp2 folder
        end
        
        %write thresholds for channelpair files
        if Args.threshold
            
            if Args.ml
                lfp2_dirs = ['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                num_lfp2 = [1 2];
                if Args.ide_only
                    lfp2_dirs = ['identity']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                    num_lfp2 = [1];
                end
            else
                lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
                num_lfp2 = [1 2]; 
            end
            
            cd([ses filesep 'lfp'])
            for ide_loc = num_lfp2
                type = mt.data.Index(:,1);
                trials_identity = (find(type == 1));
                trials_location = (find(type == 2));
                trials_all = [{trials_identity'} {trials_location'}];
                
                
                cd([ses filesep 'lfp' filesep 'lfp2'])
                
                %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
                % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
                lfpdata = nptDir(['sim_' Args.monkey '*']);
                load ([cd filesep lfpdata(1).name],'sim_trials');
                numchannels = size(sim_trials,1);
                ddata = cell(numchannels,1);
                if isempty(trials_all{ide_loc})  %if there are no trials then do not run threshold calculation
                    if ide_loc == 1
                        fprintf(1,'No identity trials\n')
                    else
                        fprintf(1,'No location trials\n')
                    end
                else
                    
                    for x = trials_all{ide_loc}
                        load ([cd filesep lfpdata(x).name],'sim_trials');
                        for y = 1: numchannels
                            %Concatenates the data from all lfp files to run surrogate analysis
                            if y == 1
                                ddata{y} = sim_trials(y,:);
                            end
                            ddata{y} = [ddata{y} sim_trials(y,:)];
                        end
                    end
                    %Calculate Moving Surrogate
                    rr_surrogate=cell(1,pairs);
                    pair = 0;
                    for ii=1:chnumb
                        fprintf('\n%0.5g     ',ii)
                        for jj=ii+1:chnumb
                            fprintf(' %0.5g',jj)
                            pair = pair + 1;
                            %find xcorr at random shifts
                            r_surrogate = RandXxcorr(ddata{ii},ddata{jj},windowSize,round(10000));
                            rr_surrogate{pair} = r_surrogate;
                        end
                    end
                    threshold = cell(1,pairs);
                    for p = 1:pairs
                        %Loads 10000 samples for each pairwise combination
                        pp = rr_surrogate{p};
                        threshold{p} = prctile(pp,[95 99 99.9 99.99]);
                    end
                    if Args.ml
                        cd(lfp2_dirs(ide_loc,:))
                    end
                    save sim_threshold threshold
                end
                cd([ses filesep 'lfp' filesep 'lfp2'])
                
            end
        end
        
        %write channelpairs
        if Args.channelpairs
            
            if Args.ml
                lfp2_dirs =['lfp2']; %['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                num_lfp2 = [1]; %[1 2];
                
                N = NeuroInfo;
                groups = N.gridPos;
                
            else
                lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
                num_lfp2 = [1];
            end
            
            cd([ses filesep 'lfp'])
            for ide_loc = 1%;num_lfp2
                trials_all = [1:size(mt.data.Index,1)]; %just run for every trial
                cd([ses filesep 'lfp' filesep 'lfp2'])
                lfp2directory = pwd;
                %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
                % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
                lfpdata = nptDir(['sim_' Args.monkey '*']);
                load ([cd filesep lfpdata(1).name],'sim_trials');
                numchannels = size(sim_trials,1);
                ddata = cell(numchannels,1);
                
                pair_number = 0;
                total_pairs = pairs;
                for c1=1:chnumb
                    for c2=(c1+1):chnumb
                        
                        pair_number = pair_number + 1;
                        fprintf(1,[num2str(pair_number) '  of  ' num2str(total_pairs)])
                        fprintf(1,'\n')
                        
                        %check to see if the channel_pair has already been written
                        if Args.ml
                            channel_pairs = ['sim_channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
                        else
                            channel_pairs = ['sim_channelpair' num2strpad(c1,2) num2strpad(c2,2)]
                        end
                        
                        if isempty(nptDir([channel_pairs '.mat'])) || Args.redo_channelpairs
                            
                            corrcoefs=[]; phases=[]; correlograms={};
                            
                            for x = trials_all
                                cd(lfp2directory)
                                %get avg_correlograms
                                if Args.ml
                                    if mt.data.Index(x,1) == 1
                                        cd('identity');
                                    elseif mt.data.Index(x,1) == 2
                                        cd('location')
                                    end
                                    load avg_correlograms_epochs avg_corrcoef_epochs
                                    cd .. %go back to lfp2 directory
                                else
                                    load avg_correlograms_epochs avg_corrcoef_epochs
                                end
                                load ([cd filesep lfpdata(x).name],'sim_trials');
                                %calculate number of bins
                                steps = floor((length(sim_trials)-windowSize)/windowStep);
                                start =  1:windowStep:steps*windowStep+1;
                                endd = start+windowSize-1;
                                r = GrayXxcorr(sim_trials(c1,:),sim_trials(c2,:),windowSize,windowStep,phaseSize,start,endd);
                                pos_or_neg = avg_corrcoef_epochs{4,pair_number}; %THIS IS EPOCH 4 (DELAY 2)
                                %if the average correlogram has a negative peak that is closest to zero
                                %then invert the correlograms to find the negative peaks that are
                                %closest to zero
                                if pos_or_neg < 0
                                    r = r * -1;
                                    inverted = 1;
                                else
                                    inverted = 0;
                                end
                                lags = [(-1*phaseSize): phaseSize];
                                
                                %find closest peak to zero lag
                                p=zeros(size(r,1),2);
                                for kk=1:size(r,1)
                                    rr=r(kk,:);
                                    
                                    %Find index of central positive peak
                                    peak = findpeaks(rr);
                                    for ppp = 1 : size(peak.loc,1)
                                        if (rr(peak.loc(ppp)) < 0)
                                            %set to max phase index
                                            peak.loc(ppp) = length(lags);
                                        end
                                    end
                                    [phase, index] = (min(abs(lags(peak.loc))));
                                    %get real phase value by indexing the location in the lags
                                    %get corr coef by indexing the correlogram
                                    peak_phase = lags(peak.loc(index));
                                    peak_corrcoef = rr(peak.loc(index));
                                    
                                    if size(peak.loc,1) > 0
                                        %Stores the correlation coef and lag position.
                                        p(kk,2) = peak_phase;
                                        p(kk,1) = peak_corrcoef;
                                    else
                                        p(kk,1) = nan;
                                        p(kk,1) = nan;
                                    end
                                end
                                %if the correlograms are inverted then make the corrcoefs
                                %negative to correspond to their actual values
                                if inverted == 1
                                    p(:,1) = p(:,1) * -1;
                                    
                                    r = r * -1; %reverse correlogram back
                                end
                                %%coefs = the correlation coefs for the trial pair
                                coefs=(p(:,1));
                                %%phases = the phases
                                phase=(p(:,2));
                                %Find the length of the coefs column.
                                cc = length(coefs);
                                dd = length(phase);
                                %Determines how many buffer NaN's are necessary
                                ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
                                ddd = (3000/windowStep) - dd;
                                %make sure there are only 60 bins
                                if ccc >= 0
                                    %Creates NaN buffer
                                    cccc = NaN(1,ccc)';
                                    dddd = NaN(1,ddd)';
                                    %%Concatenates NaN buffer on to the end of the column in order
                                    %%for the matrix dimensions to match
                                    coefs = cat(1,coefs,cccc);
                                    phase = cat(1,phase,dddd);
                                else
                                    
                                    coefs = coefs((1:end+ccc),1);
                                    phase = phase((1:end+ddd),1);
                                end
                                
                                %%makes a matrix with all trials correlation coef for the
                                %%specific pair
                                corrcoefs = cat(2,corrcoefs, coefs);
                                phases = cat(2,phases, phase);
                                
                                %places r in the cell location corresponding to the trial number
                                correlograms{x} = r;
                            end
                            
                            %indexes will be a 4X1 cell
                            %   cell 1: correct,stable
                            %   cell 3: correct,transition
                            %   cell 4: incorrect,stable
                            %   cell 5: incorrect,transition
                            
                            indexes = cell(1,4);
                            lfp2 = cd;
                            cd ..
                            lfp_dir = cd;
                            cd ..
                            
                            if ~Args.ml
                                mt=mtstrial('auto');
                            else
                                mt = mtstrial('auto','ML','RTfromML');
                            end
                            
                            cd(lfp_dir)
                            %correct, stable
                            allTrials = trials_all;
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',1,'stable');
                            else
                                ind = mtsgetTrials(mt,'BehResp',1,'stable','ML');
                            end
                           
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];  
                                end
                            end
                            indexes{1} = index;  %index 1 is correct stable trials
                            
                            %correct, transition
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',1,'transition');
                            else
                                ind = mtsgetTrials(mt,'BehResp',1,'transition','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{2} = index;
                            
                            %incorrect, stable
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',0,'stable');
                            else
                                ind = mtsgetTrials(mt,'BehResp',0,'stable','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{3} = index;
                            
                            %incorrect, transtion
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',0,'transition');
                            else
                                ind = mtsgetTrials(mt,'BehResp',0,'transition','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{4} = index;
                            cd(lfp2)
                            
                            %Write Indexes For Each Threshold
                            plot_data = cell(5,5);
                            
                            %rows are the criteria
                            %   all
                            %   correct/stable
                            %   correct/transition
                            %   incorect/stable
                            %   incorrect/transition
                            
                            %columns are the thresholds (0 - 4)
                            if Args.ml
                                if mt.data.Index(x,1) == 1
                                    cd('identity');
                                elseif mt.data.Index(x,1) == 2
                                    cd('location')
                                end
                                load threshold threshold
                                cd(lfp2) %go back to lfp2 directory
                            else
                                load threshold threshold
                            end
                            
                            for thresh = 0:4
                                for allindexes = 0 : 4
                                    if allindexes == 0
                                        cc = abs(corrcoefs);
                                        ppp = phases;
                                    else
                                        cc = abs(corrcoefs(:,indexes{allindexes}));  %necessary for trials with negative peaks
                                        ppp = phases(:,indexes{allindexes});
                                    end
                                    %Index threshold for the correct threshold
                                    if thresh > 0
                                        th = threshold{pair_number};
                                        thresholds = th(:,thresh);
                                    else
                                        thresholds = 0;
                                    end
                                    [rows columns] = size(cc);  %will be the same for the phases
                                    cors = [];
                                    phase = [];
                                    number=[];
                                    for yyy = 1:columns
                                        corrss = [];
                                        phasess= [];
                                        x=[];
                                        ntrials = [];
                                        for xxx = 1:rows %50ms bins w/ a max at 3000ms
                                            %creates a row vector that contains the number of trials
                                            %for each bin
                                            ntrials = cat(2,ntrials,(sum(isnan(cc(xxx,:)))));
                                            %Determine if threshold is crossed
                                            if  cc(xxx,yyy) > thresholds
                                                %If it is then keep the number
                                                bb = cc(xxx,yyy);
                                                pp = ppp(xxx,yyy);
                                                yy = 1;
                                            else
                                                %Else, input NaN
                                                bb = NaN;
                                                pp = NaN;
                                                yy = 0;
                                            end
                                            x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
                                            corrss = cat(1,corrss, bb);
                                            phasess = cat(1,phasess,pp);
                                        end
                                        number = cat(2,number,x);
                                        cors = cat(2,cors,corrss);
                                        phase = cat(2,phase,phasess);
                                    end
                                    d.number = number;
                                    d.cors = cors;
                                    d.phase = phase;
                                    d.ntrials = ntrials;
                                    plot_data{(allindexes+1),(thresh+1)} = d;
                                end
                            end
                            %pair_thresholds = threshold{pair_number};   %can not save threshold because it is different for location and identity tasks
                            %cd(lfp2_dirs(ide_loc,:))
                            if Args.ml
                                channel_pairs = [ 'sim_channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
                            else
                                channel_pairs = [ 'sim_channelpair' num2strpad(c1,2) num2strpad(c2,2)]
                            end
                            
                            
                            %reduce data size
                            corrcoefs = single(corrcoefs);
                            phases = single(phases);
                            

                                save(channel_pairs,'corrcoefs','phases','correlograms','indexes','plot_data')%,'pair_thresholds')

                            cd ..
                            
                        end
                        
                        
                    end
                end
            end
        end
        cd ..
    end
    
end

cd(daydir)

