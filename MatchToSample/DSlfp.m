function Day = DSlfp(varargin)
% Computes the coherence on defined epochs (500ms; fixation,cue,delay1, delay2).
% The type of coherence can be chosen with the arguments:
% - cohPP: within the parietal electrodes
% - cohPF: within the frontal electrodes
% - cohInter: between parietal and frontal
% - surrogate: computes the surrogate distribution
%
% Additional arguments are:
% - stable to only analyze teh stable performance phase
% - BehResp to specify correct 1 or incorrect 0
% - 'save' to save the results in the day directory
% - 'redo' to redo the analysis even though the results have already been
% saved.
%
% The preprocessiong of data includes 60Hz removal and a linear detrend of
% the data.


Args = struct('redo',0,'save',0,'remoteName',[],'powerPP',0,'powerPF',0,'cohPP',0,'cohPF',0,'cohInter',0,'surrogate',0,'startDay',1,'days',[],'compareWindows',0,'trialave',1,'noFlat',0);
Args.flags = {'redo','save','surrogate','ML','compareWindows','noFlat','powerPP','powerPF','cohPP','cohPF','cohInter'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

rsFs = 200; % downsampling
plength = 400;
compareChart = [1 2;1 3;1 4;2 3;2 4;3 4];

if isempty(Args.days)
    tdays = nptDir('0*');
    for d = 1 : length(tdays); days{d} = tdays(d).name; end
else
    days = Args.days;
end
if ~isempty(Args.remoteName)
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
    params{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',Args.trialave,'err',[2 0.05]);
    %     paramsP{1} = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0,'err',[2 0.05]);
    % the poewr cannot and does not need to be run with remoteName
else
    params = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',Args.trialave,'err',[2 0.05]);
    paramsP = struct('tapers',[2 3],'Fs',rsFs,'fpass',[0 100],'trialave',0,'err',[2 0.05]);
end
supname = 'DS';
for d = Args.startDay : length(days)
    
    cd(days{d});
    skip = nptDir('skip.txt');
    if isempty(skip)
        session = nptDir('session0*');
        ses = [];
        for s = 1 : size(session,1);
            cd(session(s).name);
            mtsfile = nptDir('*.bhv');
            skip = nptDir('skip.txt');
            if strcmp('DS',mtsfile(1).name(1:2)) && isempty(skip)
                
                ses = [ses s];
            end
            cd ..;
        end
        
        for flag =  find(strcmp(Args.flags,'powerPP') == 1) : size(Args.flags,2); if eval(sprintf('Args.%s',Args.flags{flag})); selOpt = flag; end; end;
        
        if Args.surrogate; matfile = sprintf('%s/%s%sSur.mat',pwd,Args.flags{selOpt},supname);elseif Args.compareWindows; matfile = sprintf('%s/%sPcomp%s.mat',pwd,Args.flags{selOpt},supname); else matfile = sprintf('%s/%s%s.mat',pwd,Args.flags{selOpt},supname);end
        
        prematfile = nptDir(matfile);
        if isempty(prematfile) || Args.redo
            session = nptDir('session0*');
            scount = 1;
            clear allrules rsession
            
            if Args.noFlat ; [ch,allCHcomb,iflat,groups] = getFlatCh('cohtype',Args.flags{selOpt}); end
            for s = ses
                
                cd(session(s).name)
                [channels,comb,CHcomb] = checkChannels(Args.flags{selOpt});
                  if Args.noFlat; channels = setdiff(ch,iflat.ch); CHcomb = setdiff(allCHcomb,allCHcomb(iflat.pairs,:),'rows'); end
                if  comb ~= -1
                    mtst = dstrial('auto');
                    
                    cd('lfp')
                    skip = nptDir('skip.txt');
                    if isempty(skip)
                        files = nptDir('*_lfp.*');
                        rtrials = load('rejectedTrials.mat','rejecTrials');
                        if size(files,1) == mtst.data.numSets && ~isempty(mtst)
                            Trials = setdiff([1:length(files)],rtrials.rejecTrials);
                            [data,~,periods,Session(scount).norm] = lfpPcut(Trials,channels,'plength',plength,modvarargin{:});
                            clear files
                            if Args.compareWindows && ~isempty(Args.remoteName)
                                s1 = size(compareChart,1);
                            else
                                s1 = 4;
                            end
                            
                            if comb == 2
                                data1 = cell(s1+(size(CHcomb,1)-1)*s1,1);
                                data2 = cell(s1+(size(CHcomb,1)-1)*s1,1);
                                for cb = 1 : size(CHcomb,1)
                                    if Args.compareWindows && ~isempty(Args.remoteName)
                                        for cmp = 1 : size(compareChart,1)
                                            data1{cmp+(cb-1)*size(compareChart,1)} = single(data{compareChart(cmp,1)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]));
                                            data2{cmp+(cb-1)*size(compareChart,1)} = single(data{compareChart(cmp,2)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]));
                                        end
                                    end
                                    for p = 1 : length(periods)
                                        %                                     lowLim = (p-1)*lplength +1;
                                        %                                     highLim = lowLim + lplength;
                                        if ~isempty(Args.remoteName)
                                            if ~Args.compareWindows
                                                data1{p+(cb-1)*length(periods)} = data{p}(:,:,find(channels == CHcomb(cb,1))); % data{CHcomb(cb,1)}(lowLim:highLim,:);
                                                data2{p+(cb-1)*length(periods)} = data{p}(:,:,find(channels == CHcomb(cb,2))); % data{CHcomb(cb,2)}(lowLim:highLim,:);
                                            end
                                            %                                     elseif Args.surrogate
                                            %                                         [Session.Cmean(:,cb,p),Session.Cstd(:,cb,p),Session.phimean(:,cb,p),Session.phistd(:,cb,p)] = cohsurrogate(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params,'wintermute');
                                        else
                                            
                                            if ~Args.compareWindows
                                                if Args.surrogate
                                                    [Session(scount).Cmean(:,cb,p),Session(scount).phCstd(:,cb,p),Session(scount).phimean(:,cb,p),Session(scount).phistd(:,cb,p),Session(scount).tile99(:,cb,p),Session(scount).Prob(:,:,cb,p),f] = cohsurrogate(data{p}(:,:,find(channels == CHcomb(cb,1))),data{p}(:,:,find(channels == CHcomb(cb,2))),params);%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                                                    
                                                else
                                                    [Session(scount).C(:,cb,p),Session(scount).phi(:,cb,p),Session(scount).S12(:,cb,p),Session(scount).S1(:,cb,p),Session(scount).S2(:,cb,p),f,Session(scount).confC(cb,p),Session(scount).phierr(:,cb,p),Session(scount).Cerr(:,:,cb,p)] = coherencyc(data{p}(:,:,find(channels == CHcomb(cb,1))),data{p}(:,:,find(channels == CHcomb(cb,2))),params);%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                                                end
                                            end
                                        end
                                    end
                                    if Args.compareWindows && isempty(Args.remoteName)
                                        for cmp = 1 : size(compareChart,1)
                                            
                                            [Session(scount).dz(:,cb,cmp),Session(scount).vdz(:,cb,cmp),Session(scount).Adz(:,cb,cmp),f] = compareCoherence(data{compareChart(cmp,1)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]),data{compareChart(cmp,2)}(:,:,[find(channels == CHcomb(cb,1)) find(channels == CHcomb(cb,2))]),params,modvarargin{:});
                                            
                                        end
                                    end
                                end
                                clear data
                                if ~isempty(Args.remoteName)
                                    fprintf('Data sent to %s on %g %g %gth at %gh%g %g sec. \n',Args.remoteName,clock)
                                    if Args.surrogate
                                        
                                        [Cmean,Cstd,phimean,phistd,tile99,Prob,ftemp] = dfeval(@cohsurrogate,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                        
                                    elseif Args.compareWindows
                                        [dz,vdz,Adz,ftemp] = dfeval(@compareCoherence,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                    else
                                        [C,phi,S12,S1,S2,ftemp,confC,Phierr,Cerr] = dfeval(@coherencyc,data1,data2,repmat(params,size(data1,1),size(data1,2)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                    end
                                    clear data1 data2
                                    if Args.surrogate; var = {'Cmean','Cstd','phimean','phistd','tile99','Prob'}; elseif Args.compareWindows; var = {'dz','vdz','Adz'}; else var = {'C','phi','S12','S1','S2','Phierr'};end
                                    for cb = 1 : size(CHcomb,1)
                                        if Args.compareWindows; countV = size(compareChart,1); else countV = length(periods);end
                                        for p = 1 : countV
                                            for v = 1 : length(var)
                                                if strcmp(var{v},'Prob')
                                                    eval(sprintf('Session(scount).%s(:,:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                else
                                                    eval(sprintf('Session(scount).%s(:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                end
                                            end
                                            if ~Args.surrogate && ~Args.compareWindows
                                                
                                                
                                                Session(scount).confC(cb,p) = confC{p+(cb-1)*length(periods)};
                                                Session(scount).Cerr(:,:,cb,p) = Cerr{p+(cb-1)*length(periods)};
                                            end
                                            
                                        end
                                    end
                                    f = ftemp{1};
                                    clear C phi S1 S2 S12 ftemp confC Phierr Cerr dz vdz Adz
                                end
                            elseif comb == 1
                                for p = 1 : 4
                                    for cb = 1 : size(data{p},3)
                                        [Session(scount).S(:,:,cb,p),f,Serr,Session(scount).J(:,:,:,cb,p)] = mtspectrumc(squeeze(data{p}(:,:,cb)),paramsP);
                                    end
                                end
                                %power computation
                            end
                            
                        else
                            fprintf('!!!!!!Problem: different # of trials in lfp folder than in main folder or no trial selected');
                        end
                        
                    end
                    
                    cd .. % session
                    if exist('Session','var')
                        
                        Session(scount).rule = scount;
                        Session(scount).trials = Trials;
                        
                        scount = scount + 1;
                    end
                    
                end
                
                cd .. % day
                
            end %s = 2 : size(session,1)
            
            
        end
        
    end
    
    if exist('Session','var')
        Day.session = Session;
        if CHcomb == -1 ; Day.channels = channels; else Day.comb = CHcomb;end
        Day.name = pwd;
        Day.option = varargin;
        clear Session
    end
    if Args.save && exist('Day','var'); fprintf('\n saving file %s \n',matfile);  save(matfile,'Day','f'); clear Day; end
    
    cd ..
    
end %for d = 1 : size(days,1)

