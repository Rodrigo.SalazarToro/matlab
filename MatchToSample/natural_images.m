function im = natural_images(varargin)

Args = struct('image',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

cd('/home/ndotson/Desktop/MonkeyPics')
pic_dir = pwd;
write_pic_dir = ('/home/ndotson/Desktop/MonkeyPics/normalized');

if isempty(Args.image)
    imagesJPG = nptdir('*.JPG');
    imagesjpg = nptdir('*.jpg');
    all_images = [imagesJPG; imagesjpg];
    num_images = size(all_images,1);
else
    num_images = size(Args.image,2);
end

for ni = 1:num_images
    cd(pic_dir)
    if isempty(Args.image)
        image_name = all_images(ni).name;
    else
        image_name = Args.image{ni};
    end
    image = imread(image_name);
    imagex = (size(image,1)) / 2;
    imagey = (size(image,2)) / 2;
    if imagex > 510 && imagey > 510

        im = image(((imagex-510):(imagex+509)),((imagey-510):(imagey+509)),:); %1020 X 1020
        
        cd(write_pic_dir)
        imwrite(im,[image_name(1:(end-4)) '.JPG']);
    end
end

cd(pic_dir)
