



cd /Volumes/raid/data/monkey/clark
load idedays
rtsame = [];
rtelse = [];
for d = 1 :29;
    cd(idedays{d});
    load rules.mat;
    ses = find(r == 1);
    cd(sprintf('session0%d',ses+1));
    mts = mtstrial('auto','redoSetNames');
    
    ind = [];
    
    for stim =1 : 3;
        ind = [ind mtsgetTrials(mts,'iMatchSac',stim,'rule',1,'iCueLoc',stim,'BehResp',1,'stable')];
    end
    goodtrials = mtsgetTrials(mts,'rule',1,'BehResp',1,'stable');
    rtsame = [rtsame vecr(mts.data.FirstSac(ind,:))];
    rtelse = [rtelse vecr(mts.data.FirstSac(setdiff(goodtrials,ind)))];
    cd ../..
end
% allrt = [rtsame rtelse];
% group = [ones(1,length(rtsame)) zeros(1,length(rtelse))];
% [p,table,stats] = anovan(allrt,{group});
mean(rtelse)
mean(rtsame)

cd /Volumes/raid/data/monkey/betty/
load idedays
% rtsame = [];
% rtelse = [];
for d = 1 :27;
    cd(idedays{d});
   cd session01
    mts = mtstrial('auto','redoSetNames');
    
    ind = [];
    
    for stim =1 : 3;
        ind = [ind mtsgetTrials(mts,'iMatchSac',stim,'rule',1,'iCueLoc',stim,'BehResp',1,'stable','ML')];
    end
    goodtrials = mtsgetTrials(mts,'rule',1,'BehResp',1,'stable');
    rtsame = [rtsame vecr(mts.data.FirstSac(ind,:))];
    rtelse = [rtelse vecr(mts.data.FirstSac(setdiff(goodtrials,ind)))];
    cd ../..
end
% allrt = [rtsame rtelse];
% group = [ones(1,length(rtsame)) zeros(1,length(rtelse))];
% [p,table,stats] = anovan(allrt,{group});
mean(rtelse)
mean(rtsame)

load longdays
idedays = [lcdays lbdays];
% rtsame = [];
% rtelse = [];
for d = 1 :21;
    cd(idedays{d});
   cd session01
    mts = mtstrial('auto','redoSetNames');
    
    ind = [];
    
    for stim =1 : 3;
        ind = [ind mtsgetTrials(mts,'iMatchSac',stim,'rule',1,'iCueLoc',stim,'BehResp',1,'stable','ML')];
    end
    goodtrials = mtsgetTrials(mts,'rule',1,'BehResp',1,'stable');
    rtsame = [rtsame vecr(mts.data.FirstSac(ind,:))];
    rtelse = [rtelse vecr(mts.data.FirstSac(setdiff(goodtrials,ind)))];
    cd ../..
end
mean(rtelse)
mean(rtsame)
allrt = [rtsame rtelse];
group = [ones(1,length(rtsame)) zeros(1,length(rtelse))];
[p,table,stats] = anovan(allrt,{group});


figure; 
subplot(2,1,1);
hist(rtelse,[80 :10:350]);
xlim([80 300])
title(sprintf('Match in other location than sample: mean %d',mean(rtelse)))


subplot(2,1,2);
hist(rtsame,[80 :10:600]);
xlim([80 300])
title(sprintf('Match in same location than sample: mean %d',mean(rtsame)))
xlabel('Reaction time [msec]')
ylabel('# trials')