function [dz,vdz,Adz,f,CHcomb,varargout] = compareCohRule(day,varargin)
% to be run in the days directory. Assumes that day is a switching day
% teh varagin goes into mtsgetTrials
% output:
% Adz(:,ncomb,p,cues) : comparison between the two rules for each cue,
% epochs and electrode pairs ( 1 == no difference
% C1 coherence for IDENTITY rule
% C2 coherence for location rule
% CueAdz(:,ncomb,p,comp,mod,r) significance for difference between the cues
%     comp is for the different comparison ncomp = nchoosek(ncues,2)
%     mod is for
Args = struct('cohtype','cohInter','redo',0,'save',0,'remoteName',[],'downSR',200,'locTuning',0,'ideTuning',0,'allCuesTuning',0,'dropC',0,'plength',400,'ML',0,'stable',1,'BehResp',1,'noFlat',0);
Args.flags = {'redo','save','ML','locTuning','ideTuning','allCuesTuning','dropC','noFlat'};
[Args,modvarargin] = getOptArgs(varargin,Args);

if ~isempty(Args.remoteName)
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
    params{1} = struct('tapers',[2 3],'Fs',Args.downSR,'fpass',[0 100],'trialave',1,'err',[2 0.05]);
else
    params = struct('tapers',[2 3],'Fs',Args.downSR,'fpass',[0 100],'trialave',1,'err',[2 0.05]);
end

if Args.locTuning
    ncues = 3;
    arg{1} = 'allloc(cues)';
    arg{2} = 'allobj';
    matfile = 'locTuning';
elseif Args.ideTuning
    ncues = 3;
    arg{1} = 'allloc';
    arg{2} = 'allobj(cues)';
    matfile = 'ideTuning';
elseif Args.allCuesTuning
    ncues = 9;
    cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    arg{1} = 'allloc(cueComb(1,cues))';
    arg{2} = 'allobj(cueComb(2,cues))';
    matfile = 'allCuesTuning';
else
    ncues = 1;
    arg{1} = 'allloc';
    arg{2} = 'allobj';
    matfile = 'allCuesComb';
end

thematfile = sprintf('%s%d%d.mat',matfile,Args.stable,Args.BehResp);


%% Collection of the data and the trial info
cd(day)
al = nptDir(thematfile);
skip = nptDir('skip.txt');

if isempty(skip)
    if isempty(al) || Args.redo
        
        
        [ch,allCHcomb,iflat,groups] = getFlatCh('cohtype',Args.cohtype);
        if Args.noFlat; channels = setdiff(ch,iflat.ch); CHcomb = setdiff(allCHcomb,allCHcomb(iflat.pairs,:),'rows'); end
        session = nptDir('session*');
        if Args.ML;
            for ses = 1 : length(session);
                cd(session(ses).name); bhvFile = nptDir('*.bhv') ;
                if isempty(nptDir('skip.txt')) && ~isempty(bhvFile) && ~isempty(findstr(bhvFile.name,'MTS')); alls = ses; cd ..; break;
                end; cd ..;
            end;
            notCue = [];
        else;
            alls = [2 : length(session)];
        end
        
        for cues = 1 : ncues
            if Args.ML
                cd(session(alls).name)
                tmtst = mtstrial('auto');
                allloc = unique(tmtst.data.CueLoc);
                allobj = unique(tmtst.data.CueObj);
                for r = 1 : 2;
                    minTrials(r) = length(mtsgetTrials(tmtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),'rule',r,modvarargin{:},'BehResp',Args.BehResp,'stable',Args.stable));
                end;
                cd ..
            else
                for s =alls %size(session,1);
                    cd(session(s).name);
                    tmtst = mtstrial('auto');
                    allloc = unique(tmtst.data.CueLoc);
                    allobj = unique(tmtst.data.CueObj);
                    minTrials(s-1) = length(mtsgetTrials(tmtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),modvarargin{:},'BehResp',Args.BehResp,'stable',Args.stable));
                    cd ..;
                end
            end
            CueminT(cues) = min(minTrials);
        end
        notCue = [];
        for ses = alls
            cd(session(ses).name)
            [channels,comb,CHcomb] = checkChannels('cohInter');
            
            %     if comb ~= -1
            
            mtst = ProcessSession(mtstrial,'auto');
            if Args.ML; rules = [1 2];else; rules = unique(mtst.data.Index(:,1)); end
            
            
            cd('lfp')
            files = nptDir('*_lfp.*');
            for rule = rules
                for cues = 1 : ncues
                    Trials = mtsgetTrials(mtst,'CueLoc',eval(arg{1}),'CueObj',eval(arg{2}),'rule',rule,modvarargin{:},'BehResp',Args.BehResp,'stable',Args.stable);
                    norder = Trials(randperm(length(Trials))); Trials = sort(norder(1:CueminT(cues)));
                    if ~isempty(Trials)
                        [data,lplength] = lfpPcut(Trials,channels,'plength',Args.plength,modvarargin{:}); % output in {periods} samples x trials x channel data
                    else
                        data = [];
                        notCue = [notCue cues];
                    end
                    if rule == 1; cue(cues).data1 = data;elseif rule == 2; cue(cues).data2 = data; end
                end
            end
            cd ..
            %     end
            cd ..
        end
        %% Setting the appropiate format and comparing the two rules
        
        data1w = cell(1,ncues * 4 * size(CHcomb,1));
        data2w = cell(1,ncues *4 * size(CHcomb,1));
        if CHcomb ~= -1
            for ncomb = 1 : size(CHcomb,1)
                ch = CHcomb(ncomb,:);
                for thech = 1 :2; newch(thech) = find(ch(thech) == channels); end; ch = newch;
                for cues = 1 : ncues
                    if isempty(find(cues == notCue))% && comb ~= -1
                        if ~isempty(Args.remoteName)
                            for p = 1 : 4
                                
                                data1w{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p} = single(cue(cues).data1{p}(:,:,ch));
                                data2w{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p} = single(cue(cues).data2{p}(:,:,ch));
                            end
                        else
                            for p = 1 : 4
                                if Args.dropC
                                    [dz(:,ncomb,p,cues),vdz(:,ncomb,p,cues),Adz(:,ncomb,p,cues),f] = compareCoherence(cue(cues).data1{p}(:,:,ch),cue(cues).data2{p}(:,:,ch),params,modvarargin{:});
                                    
                                else
                                    [dz(:,ncomb,p,cues),vdz(:,ncomb,p,cues),Adz(:,ncomb,p,cues),f,C1(:,ncomb,p,cues),C2(:,ncomb,p,cues)] = compareCoherence(cue(cues).data1{p}(:,:,ch),cue(cues).data2{p}(:,:,ch),params,modvarargin{:});
                                    
                                end
                            end
                        end
                    else
                        for p = 1 : 4
                            le = 65;%length(f);
                            f = [];
                            dz(:,ncomb,p,cues) = nan(le,1);
                            vdz(:,ncomb,p,cues) = nan(le,1);
                            Adz(:,ncomb,p,cues) = nan(le,1);
                            C1(:,ncomb,p,cues) = nan(le,1);
                            C2(:,ncomb,p,cues) = nan(le,1);
                        end
                    end
                end
                
                
            end
        else
            le = 65;%length(f);
            f = [];
            dz = nan(le,1,4,ncues);
            vdz = nan(le,1,4,ncues);
            Adz = nan(le,1,4,ncues);
            C1 = nan(le,1,4,ncues);
            C2 = nan(le,1,4,ncues);
        end
        %% Sending the data to wintermute
        if ~isempty(Args.remoteName)
            if Args.dropC
                [dzt,vdzt,Adzt,ft] = dfeval(@compareCoherence,data1w,data2w,repmat(params,1,length(data1w)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
            else
                
                [dzt,vdzt,Adzt,ft,C1t,C2t] = dfeval(@compareCoherence,data1w,data2w,repmat(params,1,length(data1w)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                
                [Cmean,Cstd,phimean,phistd,tile99,Prob,ftemp] = dfeval(@cohsurrogate,data1w,data2w,repmat(params,1,length(data1w)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
            end
            
            for cues = 1 : ncues
                for ncomb = 1 : size(CHcomb,1)
                    for p = 1 : 4
                        Adz(:,ncomb,p,cues) = Adzt{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p};
                        dz(:,ncomb,p,cues) = dzt{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p};
                        vdz(:,ncomb,p,cues) = Adzt{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p};
                        
                        %                 sur(:,:,ncomb,p,cues) = Prob{p+(ncomb-1)*4};
                        if ~Args.dropC
                            C1(:,ncomb,p,cues) = C1t{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p};
                            C2(:,ncomb,p,cues) = C2t{(cues-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p};
                        end
                    end
                end
            end
            f = ft{1};
        end
        
        if ~Args.dropC
            varargout{1} = C1;
            varargout{2} = C2;
        end
        
        
        % clear dz vdz Adz f sur C1 C2
        
        %% Comparison between the different cues
        
        if CHcomb ~= -1 & (Args.locTuning | Args.ideTuning | Args.allCuesTuning)
            ncomp = nchoosek(ncues,2);
            allmod = {'allloc' 'allobj'};
            for r =  1 : 2;
                
                %         for mod = 1 : 2
                allcues = [ 1 : ncues];
                
                allcomp = nchoosek(allcues,2);
                data1w = cell(1,ncomp * 4 * size(CHcomb,1));
                data2w = cell(1,ncomp *4 * size(CHcomb,1));
                
                for ncomb = 1 : size(CHcomb,1)
                    ch = CHcomb(ncomb,:);
                    ch = find(ismember(channels,ch) == 1);
                    for comp = 1 : ncomp
                        cues = allcomp(comp,:);
                        if isempty(notCue) || unique(ismember(cues,notCue)) == 0
                            
                            if ~isempty(Args.remoteName)
                                for p = 1 : 4
                                    thedata1 = eval(sprintf('cue(cues(1)).data%d{p}(:,:,ch)',r));
                                    thedata2 = eval(sprintf('cue(cues(2)).data%d{p}(:,:,ch)',r));
                                    data1w{(comp-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p} = cue(cues(1)).data1{p}(:,:,ch);
                                    data2w{(comp-1) * size(CHcomb,1) * 4 + (ncomb-1)*4 + p} = cue(cues(2)).data1{p}(:,:,ch);
                                end
                            else
                                for p = 1 : 4
                                    thedata1 = eval(sprintf('cue(cues(1)).data%d{p}(:,:,ch)',r));
                                    thedata2 = eval(sprintf('cue(cues(2)).data%d{p}(:,:,ch)',r));
                                    if Args.dropC
                                        [Cuedz(:,ncomb,p,comp,r),Cuevdz(:,ncomb,p,comp,r),CueAdz(:,ncomb,p,comp,r),f] = compareCoherence(thedata1,thedata2,params,modvarargin{:});
                                        
                                    else
                                        [Cuedz(:,ncomb,p,comp,r),Cuevdz(:,ncomb,p,comp,r),CueAdz(:,ncomb,p,comp,r),f,CueC1(:,ncomb,p,comp,r),CueC2(:,ncomb,p,comp,r)] = compareCoherence(thedata1,thedata2,params,modvarargin{:});
                                        
                                    end
                                end
                            end
                        else
                            for p = 1 : 4
                                le = length(f);
                                Cuedz(:,ncomb,p,comp) = nan(le,1);
                                Cuevdz(:,ncomb,p,comp) = nan(le,1);
                                CueAdz(:,ncomb,p,comp) = nan(le,1);
                                CueC1(:,ncomb,p,comp) = nan(le,1);
                                CueC2(:,ncomb,p,comp) = nan(le,1);
                            end
                        end
                    end
                end
                %         end
            end
            
        else
            le = 65;
            Cuedz = nan(le,1,4,1);
            Cuevdz = nan(le,1,4,1);
            CueAdz = nan(le,1,4,1);
            CueC1 = nan(le,1,4,1);
            CueC2 = nan(le,1,4,1);
        end
        
        
        %%
        if Args.save
            if Args.dropC
                
                if Args.locTuning | Args.ideTuning | Args.allCuesTuning
                    save(thematfile,'dz','vdz','Adz','f','Cuedz','Cuevdz','CueAdz');
                else
                    save(thematfile,'dz','vdz','Adz','f');
                end
            else
                if Args.locTuning | Args.ideTuning | Args.allCuesTuning
                    save(thematfile,'dz','vdz','Adz','f','C1','C2','Cuedz','Cuevdz','CueAdz','CueC1','CueC2');
                else
                    save(thematfile,'dz','vdz','Adz','f','C1','C2');
                end
            end
            display(sprintf('saving %s in %s',thematfile,pwd))
        end
    else
        dz = [];
        vdz = [];
        Adz = [];
        f = [];
        CHcomb = [];
    end
else
    dz = [];
    vdz = [];
    Adz = [];
    f = [];
    CHcomb = [];
end
cd ..


