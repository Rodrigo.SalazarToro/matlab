function [response_entropy,noise_entropy,mutual_information] = hilbertphase_mi(varargin)

Args = struct('ml',0,'ent',[],'plot',0);
Args.flags = {'ml','plot'};
Args = getOptArgs(varargin,Args);

ents = Args.ent;
bins = [0:.05:1];

nfreq = size(ents,1);
ntime = size(ents,2);

sesdir = pwd;
N = NeuronalHist('ml');

if Args.ml
    c = mtscpp2('auto','ml');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %these are the identity trials
    
    %three locs
    for ll = 1:3
        locs{ll} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',ll); %these are the identity trials
        nlocs(ll) = size(locs{ll},2); %used for surrogates
    end
else
    c = mtscpp2('auto');
    mt=mtstrial('auto','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    %three locs
    for ll = 1:3
        locs{ll} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueLoc',ll); %these are the identity trials
        nlocs(ll) = size(locs{ll},2); %used for surrogates
    end
end

ntrials = size(tr,2);
response_entropy = zeros(nfreq,ntime);
%calculate response entropy H(R)
for f = 1 : nfreq
    for t = 1 : ntime
        h = hist(squeeze(ents(f,t,tr)),bins) ./ ntrials; %normalize it by the number of responses (in this case trials), this is response distribution P(r)
        response_entropy(f,t) = -1 * nansum( (h .* log2(h)) ); %see panzeri_2007
    end
end

%calculate the noise entropy
for f = 1 : nfreq
    for t = 1 : ntime
        h = hist(squeeze(ents(f,t,tr)),bins) ./ ntrials;
        %calculate conditional distributions
        for ll = 1 : 3
            cond_distributions{ll} = hist(squeeze(ents(f,t,locs{ll})),bins) ./ size(locs{ll},2); %normalize it by the number of responses (in this case trials)
        end
        
        %sum over stimuli
        for ll = 1 : 3
            ps =  size(locs{ll},2)/ntrials;
            nr(ll,:) =   ps .* cond_distributions{ll} .* log2(cond_distributions{ll});
        end
        
        %sum over responses
        noise_entropy(f,t) = -1 * nansum(nansum(nr)); %this is the noise entropy H(R|S)
        
        for ll = 1:3
            ps = size(locs{ll},2)/ntrials;
            mi(ll,:) = ps .* cond_distributions{ll} .* log2((cond_distributions{ll} ./ h));
        end
        
        mutual_information(f,t) = nansum(nansum(mi));
    end
end



if Args.plot
    all_freq = [10 : 1 : 35];
    w = 50:25:1850;
    figure
    subplot(5,1,1)
    imagesc(response_entropy(4:end,:));colorbar;title('response entropy H(R)')
    set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:35);set(gca,'XTickLabel',w)
    subplot(5,1,2)
    imagesc(noise_entropy(4:end,:));colorbar;title('noise entropy H(R|S)')
    set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:35);set(gca,'XTickLabel',w)
    subplot(5,1,3)
    imagesc(mutual_information(4:end,:));colorbar;title('MI I(S;R)');
    set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:35);set(gca,'XTickLabel',w)
    subplot(5,1,4)
    imagesc(mean(ents((4:end),:,tr),3));colorbar;title('Mean Synchronization Index (all correct/stable trials)')
    set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:35);set(gca,'XTickLabel',w)
    subplot(5,1,5)
    imagesc(std(ents((4:end),:,tr),[],3));colorbar;title('STD Synchronization Index (all correct/stable trials)')
    set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:35);set(gca,'XTickLabel',w)
    
    
    %plot the mean response for the 3 stimuli (this is like mean coherence)
    figure
    for x = 1:3
        subplot(3,1,x)
        imagesc(interp2(mean(ents(:,:,locs{x}),3)))
%         set(gca,'YTick',1:26);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:73);set(gca,'XTickLabel',w)
        colorbar
        caxis([.7 .8])
       
    end
    
    % % imagesc(mutual_information);colorbar;title('MI I(S;R)');
    % % set(gca,'YTick',1:14);set(gca,'YTickLabel',all_freq);set(gca,'XTick',1:33);set(gca,'XTickLabel',w)
    
    
    figure
    imagesc(flipdim(interp2(mutual_information),1));
    title('MI I(S;R)');
     set(gca,'YTick',1:2:52);set(gca,'YTickLabel',fliplr(all_freq(1:end)));set(gca,'XTick',1:2:146);set(gca,'XTickLabel',w)
    colorbar
%     caxis([.02 .06])
    
end



