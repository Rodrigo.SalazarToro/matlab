function [new_condition real_performance new_prob] = simulate_condition_prob_weighted(TrialRecord)

%total number of trials completed
total_trials = TrialRecord.CurrentTrialNumber;
conditions_played = TrialRecord.ConditionsPlayed;
cond = TrialRecord.ConditionsThisBlock;
error_record = TrialRecord.TrialErrors;
%get record for each condition
number_conditions = size(cond,2);

back_trials = 20; %number of trials to look back at
constrict = 1;

new_prob = 0;

upper_cutoff = .75;
lower_cutoff = .25;

%find lowest possible prob and highest possible prob
upper(1:(number_conditions-1)) = 1 / ((back_trials * upper_cutoff) / back_trials);
upper(number_conditions) = 1 / ((back_trials * lower_cutoff) / back_trials);

highest = upper(number_conditions) / sum(upper)

upper(1:(number_conditions-1)) =1 / ((back_trials * lower_cutoff) / back_trials);
upper(number_conditions) = 1 / ((back_trials * upper_cutoff) / back_trials);

lowest = upper(number_conditions) / sum(upper)




%set random number generator seed
if total_trials == 1
    RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
end

%calculate performance if 10 or more correct and incorrect conditions exist
for x = 1 : number_conditions
    i = find(conditions_played == cond(x)); %index for condition
    all = sort( [intersect(i,find(error_record ==0)) intersect(i,find(error_record == 6))] ); %find all correct and incorrect trials
    number_all = size(all,2);
    
    if number_all >= back_trials
        all_back = all((end-(back_trials-1)):end); %last back_trials correct and incorrect trials
        
        correct = size(find(error_record(all_back) ==0),2);
        incorrect = size(find(error_record(all_back) == 6),2);
        
        if correct ~= 0
            in_performance(x) = 1 / (correct / (correct + incorrect)); %get inverse of performance (high performance == low inverse)
        end
        
        if constrict == 1
            
            %make all performances 25 - 75%, this avoids omitting certain conditions for large strings of trials
            if correct >= (back_trials * upper_cutoff)
                in_performance(x) = 1 / ((back_trials * upper_cutoff) / back_trials); %if perfect performance set performance to be 80% ie: 1/( ( .8*10  /10) = 1/(8/10)
            elseif  correct <= (back_trials * lower_cutoff)
                in_performance(x) = 1 / ((back_trials * lower_cutoff) / back_trials); %if zero correct trials then set in_performance to ie: 1/(1/10)
            end
            
        else
            if  correct == 0
                in_performance(x) = 1 / (1 / back_trials); %if zero correct trials then set in_performance to ie: 1/(1/10)
            end
        end
        
        
        
        
    else
        in_performance(x) = nan;
    end
    
    real_performance(x) = 1 / in_performance(x);
end

if sum(isnan(in_performance)) == 0 %if no nan then augment probabilities, else do nothing
    
    for y = 1 : number_conditions
        new_prob(y) = (in_performance(y) / sum(in_performance)) %find new probabilities for each condition
    end
    
    r = rand; %find random number between 0 and 1
    probs = 0;
    for z = 1 : number_conditions
        new_condition = cond(z);
        if z == 1
            probs(z) = new_prob(z);
            if r < new_prob(z);
                break
            end
        else
            probs(z) = probs(z-1) + new_prob(z);
            if r < probs(z) 
                break
            end
        end
    end
else
    
    perm = randperm(number_conditions);
    new_condition = cond(perm(1));
end


