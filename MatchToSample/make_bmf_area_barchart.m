function make_bmf_area_barchart(varargin)

%loads power information written from "write_power_trials" and plot
%information in a grid


Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;
load alldays

occipital = {'V1','V2','V3','V4','DPT','OPT','PO',};
temporal = {'MT','TEO','TEOM','TPO','TPOC','TPT'};
postparietal = {'LIPE','MIP','PE','PEC','PECG','PFCX','POAE','PPT'};
prefrontal = {'a44','a45B','a46D','a46V','a6/32','a6DC','a6DR','a8/32','a8AD','a8AV','a8B','a9/32','a9/46D','a9/46V','a9L','a9M'};
motor = {'24D','a24C','a4','a6M','a6VC','a6VR'}; 
somatosensory = {'PAAL','PFOP','PGOP','a1','a2','a2VE','a2e','a3a','a3b','aS2E'};
areas = [occipital,postparietal,temporal,somatosensory,motor,prefrontal];


allrecordings = [];
for x = 1 : size(alldays,2);
    cd([monkeydir filesep alldays{x} filesep 'session01'])
    i = bmf_groups;
    N = NeuronalHist('bmf');
    
    [~,ii]= intersect(N.gridPos,i); %get index of good groups

    allrecordings = [allrecordings [N.location(ii)]];
end
n = size(allrecordings,2);
u = unique(allrecordings);
uniquen = size(u,2);


for un = 1 : uniquen
    counts(un) = sum(strcmp([allrecordings],u{un}));
end

%rearrange counts
newcounts = []
c = 0;
for alla = 1 : size(areas,2);
   f = find(strcmp(u,areas{alla}));
   if ~isempty(f)
       c = c+1;
       newcounts(c) = counts(f);
       newlist{c} = areas{alla};
   end
end

figure
bar(newcounts,'hist')
set(gca,'XTick',[1:1:uniquen])
set(gca,'XTickLabel',newlist)

text(4,205,['n = ',num2str(n)]);
text(4,198,['n areas = ',num2str(un)]);

figure
bar(counts,'hist')
set(gca,'XTick',[1:1:uniquen])
set(gca,'XTickLabel',u)

text(4,205,['n = ',num2str(n)]);
text(4,198,['n areas = ',num2str(un)]);
