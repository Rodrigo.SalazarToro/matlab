function [mugram,time,f,ntrials,varargout] = mkAveMugram(obj,ind,varargin)
% to be run in /Volumes/raid/data/monkey


Args = struct('save',0,'redo',0,'prctile',98,'minTbin',35,'relFreq',[10 42],'type','IdePerLoc','rule','1','noSur',0,'delayRange',[1.1 1.705],'addNameFile',[],'MatchAlign',0,'plot',0,'IncorCorrected',0,'FixCorrected',0,'Interpol',0,'biasCorr','ExpV','NodelaySelection',0,'chance',0,'getPrefStim',0,'locSel','max','plotDetails',0,'betarange',[10 42],'onlyAdjBins',1,'minTrials',0,'prctileCoh',99.9,'sigBinCoh',0.15,'noStimRanked',0,'onlySigCohPairs',0,'exclAlphaPairs',[]);
Args.flags = {'save','redo','noSur','MatchAlign','plot','IncorCorrected','FixCorrected','Interpol','NodelaySelection','getPrefStim','plotDetails','noStimRanked','onlySigCohPairs'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

presampleLim = 0.455;
r = length(ind);
sdir = pwd;
switch Args.type
    case 'Loc'
        ntype = 1;
        name = 'Loc';
        prefix = [];
    case 'Ide'
        ntype = 1;
        name = 'Ide';
        prefix = [];
    case 'LocPerIde'
        ntype = 3;
        name = 'LocPerIde';
        prefix = [];
    case {'IdePerLoc' 'IdePerLocIncor'}
        ntype = 3;
        name = 'IdePerLoc';
        prefix = [];
    case 'Fix'
        ntype = 1;
        name = 'Fix';
        prefix = [];
    case 'Rules'
        ntype = 1;
        name = 'all';
        Args.rule = 's';
        prefix = 'Stim';
        nstim = 1;
end

if Args.MatchAlign; addname =  'MatchAlign'; else addname = [];end

thefile = sprintf('migram%sRule%sprctile%g%s.mat',Args.type,Args.rule,Args.prctile,Args.addNameFile);
if strcmp(Args.type,'Rules')
    tlength = 2;
    flength = 33;
else
    tlength = 3;
    flength = 17;
end
ntrials = zeros(r,tlength);
sigcondStim = zeros(r,3);
alpha = zeros(r,1);
if isempty(nptDir(thefile)) || Args.redo
    mugram = zeros(r,Args.minTbin,flength);
    scoh = zeros(r,Args.minTbin,flength);
    mCstim = zeros(r,Args.minTbin,flength);
    vCstim = zeros(r,Args.minTbin,flength);
    alphaExp = zeros(r,Args.minTbin,flength);
    prefStim = zeros(3,r,Args.minTbin,flength);
    prefStimPhase = zeros(3,r,Args.minTbin,flength);
    prefStimPhaseSTD = zeros(3,r,Args.minTbin,flength);
    prefStimS = cell(2,1);
    stimdiff = zeros(r,1);
    if ~Args.noSur; sur = zeros(r,Args.minTbin,flength);msur = cell(2,1); end; %zeros(r,Args.minTbin,flength);end
    selectLoc = zeros(r,1);
    for ii = 1 : r
        cd(obj.data.setNames{ind(ii)})
        cd grams
        gr = obj.data.Index(ind(ii),10:11);
        
        if strcmp(Args.type,'IdePerLoc') || strcmp(Args.type,'IdePerLocIncor') ||  strcmp(Args.type,'LocPerIde')
            meanCStim = zeros(ntype,1);
            %% selection of the location of object to test MI
            for l = 1 : ntype
                matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%d%s.mat',gr(1),gr(2),Args.rule,name,l,addname);
                switch Args.type
                    case 'IdePerLoc'
                        extramatfile = sprintf('cohgramg%04.0fg%04.0fRule%sLoc%d%s.mat',gr(1),gr(2),Args.rule,l,addname);
                        extraSurfile = sprintf('cohgramGenSurRule%sloc%d%s.mat',Args.rule,l,addname);
                    case 'LocPerIde'
                        extramatfile = sprintf('cohgramg%04.0fg%04.0fRule%sIde%d%s.mat',gr(1),gr(2),Args.rule,l,addname);
                        extraSurfile = sprintf('cohgramGenSurRule%sIde%d%s.mat',Args.rule,l,addname);
                end
                
                %% check for significance for coherence at that
                % location/object
                if ~isempty(nptDir(extramatfile))
                    
                    load(extramatfile,'C','t','f')
                    cdata = C{1};
                    load(extraSurfile,'C')
                    sdata = squeeze(prctile(C,Args.prctileCoh,1));
                    thedelay = find(t{1} >= Args.delayRange(1) & t{1} <= Args.delayRange(2));
                    relFreq = find(f >= Args.relFreq(1) & f <= Args.relFreq(2));
                    
                    sigScond = find(cdata(thedelay,relFreq) > sdata(thedelay,relFreq));
                    
                    
                    sigcondStim(ii,l) = length(sigScond); % variable about which pair is significant for coherence
                    
                end
                %%
                load(matfile,'C','time','f')
                %                 surname = sprintf('migramSurGenInterRule%s%s%d%s.mat',Args.rule,name,l,addname);
                %                 surtemp = load(surname,'mutualgram','f');
                %                 expVi = getExpValue(surtemp.mutualgram,surtemp.f);
                %                 mutualgram = mutualgram(1:size(expVi,1),:) ./ expVi;
                
                presample = time <= presampleLim;
                
                time = time(1:Args.minTbin);
                if Args.plotDetails
                    subplot(2,1,1)
                    imagesc(time,f,mutualgram(1:Args.minTbin,:)')
                    colorbar
                end
                %                 switch Args.biasCorr
                %                     case 'PresampleDiv'
                %                         mutualgram = mutualgram(1:Args.minTbin,:) ./ repmat(nanmean(mutualgram(presample,:),1),[Args.minTbin 1]);
                %                     case 'ExpV'
                %
                %                         surt = load(surname,'mutualgram');
                %                         texpv = getExpValue(surt.mutualgram,f);
                %                         mutualgram = mutualgram(1:Args.minTbin,:) ./ texpv(1:Args.minTbin,:);
                %                     case 'PresampleZscore'
                %                          mutualgram = (mutualgram(1:Args.minTbin,:) - repmat(nanmean(mutualgram(presample,:),1),[Args.minTbin 1])) ./ repmat(nanstd(mutualgram(presample,:),[],1),[Args.minTbin 1]);
                %
                %                         %                  mugram = (mugram - msur{1}) ./ msur{2};
                %                 end
                if Args.plotDetails
                    subplot(2,1,2)
                    imagesc(time,f,mutualgram(1:Args.minTbin,:)')
                    colorbar
                    pause
                end
                relFreq = find(f >= Args.relFreq(1) & f <= Args.relFreq(2));
                thedelay = time >= Args.delayRange(1) & time <= Args.delayRange(2);
                meanC = zeros(3,1);
                for cStim = 1 : 3; meanC(cStim) = mean(mean(C{cStim}(thedelay,relFreq)));end
                meanCStim(l) = mean(meanC);
            end
            %%
            switch Args.locSel
                case 'max'
                    if Args.noStimRanked
                        selectLoc(ii) = 1;
                    else
                        [~,selectLoc(ii)] = max(meanCStim);
                    end
                case 'min'
                    if Args.noStimRanked
                        selectLoc(ii) = 3;
                    else
                        [~,selectLoc(ii)] = min(meanCStim);
                    end
                case 'inter'
                    if Args.noStimRanked
                        selectLoc(ii) = 2;
                    else
                        [~,maxLoc] = max(meanCStim);
                        [~,minLoc] = min(meanCStim);
                        selectLoc(ii) = setdiff([1:3],[maxLoc minLoc]);
                    end
            end
            
            if strcmp(Args.type,'IdePerLocIncor')
                matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%dIncor.mat',gr(1),gr(2),Args.rule,name,selectLoc(ii));
            else
                if Args.IncorCorrected
                    matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%dsameTIncor.mat',gr(1),gr(2),Args.rule,name,selectLoc(ii));
                elseif Args.FixCorrected
                    matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%dsameTFix.mat',gr(1),gr(2),Args.rule,name,selectLoc(ii));
                else
                    matfile = sprintf('migramg%04.0fg%04.0fRule%s%s%d%s.mat',gr(1),gr(2),Args.rule,name,selectLoc(ii),addname);
                end
            end
            surname = sprintf('migramSurGenInterRule%s%s%d%s.mat',Args.rule,name,selectLoc(ii),addname);
        else
            matfile = sprintf('migramg%04.0fg%04.0f%sRule%s%s%s.mat',gr(1),gr(2),prefix,Args.rule,name,addname);
            surname = sprintf('migramSurGenInter%sRule%s%s%s.mat',prefix,Args.rule,name,addname);
        end
        
        if isempty(nptDir(matfile))
            mutualgram = nan(Args.minTbin,size(mugram,3));
            display(sprintf('Missing file %s; not included! \n',matfile))
        else
            load(matfile,'mutualgram','time','f','trials')
            relFreq = find(f >= Args.relFreq(1) & f <= Args.relFreq(2));
            reltime = find(time >= Args.delayRange(1) & time <= Args.delayRange(2));
            
            if Args.getPrefStim
                warning off
                load(matfile,'C','phi','phistd','S1','S2')
                scoht = zeros(Args.minTbin,length(f));
                cC = zeros(3,Args.minTbin,length(f));
                mcoh = zeros(3,1);
                for stim = 1 : 3
                    mcoh(stim) = nanmean(nanmean(C{stim}(reltime,relFreq)));
                    switch Args.type
                        
                        case 'LocPerIde'
                            surcohfile = sprintf('cohgramGenSurRule%sloc%dide%d.mat',Args.rule,stim,selectLoc(ii));
                            
                        case 'IdePerLoc'
                            surcohfile = sprintf('cohgramGenSurRule%sloc%dide%d.mat',Args.rule,selectLoc(ii),stim);
                    end
                    surcoh = load(surcohfile,'C');
                    surcohlim = squeeze(prctile(surcoh.C,Args.prctileCoh,1));
                    scoht = scoht + (C{stim}(1:Args.minTbin,:) > surcohlim(1:Args.minTbin,:));
                    cC(stim,:,:) = C{stim}(1:Args.minTbin,:);
                end
                mCstim(ii,:,:) = squeeze(mean(cC,1));
                vCstim(ii,:,:) = squeeze(std(cC,[],1));
                scoh(ii,:,:) = scoht;
                [maxv,indmax] = max(mcoh);
                [minv,indmin] = min(mcoh);
                [~,stimorder] = sort(mcoh);
                for stimn = 1 : length(stimorder)
                    prefStim(stimn,ii,:,:) = C{stimorder(stimn)}(1:Args.minTbin,:);
                    prefStimPhase(stimn,ii,:,:) = phi{stimorder(stimn)}(1:Args.minTbin,:);
                    prefStimPhaseSTD(stimn,ii,:,:) = phistd{stimorder(stimn)}(1:Args.minTbin,:);
                    
                    if exist('S1')
                        prefStimS{1}(stimn,ii,:,:) = S1{stimorder(stimn)}(1:Args.minTbin,:);
                        prefStimS{2}(stimn,ii,:,:) = S2{stimorder(stimn)}(1:Args.minTbin,:);
                    else
                        prefStimS = cell(2,1);
                    end
                end
                stimdiff(ii) = maxv-minv;
            end
            
            
            if size(trials,2) == 1
                ntrials(ii,:) = cellfun(@length,trials);
            else
                ntrials(ii,:) = cellfun(@length,trials);
            end
        end
        %         if iscell(mutualgram)
        %             mugram(ii,:,:) = mutualgram{nstim}(1:Args.minTbin,:);
        %         else
        
        mugram(ii,:,:) = mutualgram(1:Args.minTbin,:);
        %         end
        if ~Args.noSur
            load(surname,'mutualgram')
            time = time(1:Args.minTbin);
            if iscell(mutualgram)
                mutualgram = mutualgram{nstim}(:,1:Args.minTbin,:);
            else
                mutualgram = mutualgram(:,1:Args.minTbin,:);
            end
            presample = time <= presampleLim;
            texpv =  getExpValue(mutualgram,f);
            
            alpha(ii) = squeeze(sum(sum(mugram(ii,presample,:),2),3)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';
%             alpha = repmat(alphaExpf',size(texpv,1),1);
            alphaExp(ii,:,:) =  alpha(ii)*texpv;
            sur(ii,:,:) = squeeze(prctile(alpha(ii)*mutualgram,Args.prctile,1));
            %            msur(ii,:,:) = prctile(mutualgram,50,1);
            %             msur(ii,:,:) = mean(mutualgram,1);
            
        end
    end
else
    load(thefile)
    
end
presample = time <= presampleLim;
betaFreq = find(f >= Args.betarange(1) & f <= Args.betarange(2));
%% select for the pairs with delay effect
if ~Args.noSur || ~Args.NodelaySelection
    
    
    delay(1,:,:) = repmat((time(1:Args.minTbin) >= Args.delayRange(1) & time(1:Args.minTbin) <= Args.delayRange(2))',1,length(betaFreq));
    %% correction with significant coherence
    sig = (mugram > sur);
    
    sig = sig .* (scoh >= 1);
    
    %% adjecenat bins only
    if ~isempty(Args.onlyAdjBins)
        seltime = find(time >= Args.delayRange(1) & time <= Args.delayRange(2));
        sigpair = zeros(size(sig,1),1);
        for pp = 1 : size(sig,1)
            [row,col] = find(squeeze(sig(pp,seltime,betaFreq)) == 1);
            [nu,~] = hist(row,length(unique(row)));
            adjrow = sum(nu(nu > Args.onlyAdjBins));
            [nu,~] = hist(col,length(unique(col)));
            adjcol = sum(nu(nu > Args.onlyAdjBins));
            sigpair(pp) = adjcol + adjrow;
            %             sigpair(pp) = adjrow;
        end
    else
        %%
        sigpair = sum(sum(sig(:,:,betaFreq) .* repmat(delay,[size(sur,1) 1 1]),3),2); % get the sig bins duringhte delay
    end
    sigg = squeeze(sum(sig,1))./r;
    
    
    
    ndbin = sum(time(1:Args.minTbin) >= Args.delayRange(1) & time(1:Args.minTbin) <= Args.delayRange(2)); % number of time bins to account for
    if isempty(Args.chance)
        chance = mean(mean(sigg(presample,betaFreq),2),1);% get the number of sig. bins by chance
    else
        chance = Args.chance;
    end
    nsiglim = ndbin*length(betaFreq)*chance; % number of bins by chance
    %     sigpair = sigpair * (min(ntrials,2) > Args.minTrials);
    thepairs = find(sigpair > floor(nsiglim)); % get the pairs with higher number of significant bins than expected by chance
    
    thepairs = intersect(thepairs,find(sigcondStim(:,selectLoc) > round(Args.sigBinCoh * ndbin*length(betaFreq)))); % combined MI selected pairs with pairs with signi. coherence for the location/object
    
    probPS = mean(mean(sigg(presample,betaFreq),2),1);
    varargout{1} = sig; % significant values
    varargout{2} = thepairs; % selected pairs
    varargout{3} = probPS; % false positive rate
    varargout{4} = sur; % bias correction values
end
if exist('prefStim')
    varargout{5} = prefStim;
    varargout{6} = stimdiff;
    varargout{7} = prefStimPhase;
    varargout{8} = selectLoc;
    varargout{9} = prefStimPhaseSTD;
    varargout{10} = prefStimS;
end
%%
cd(sdir)
if Args.save
    if Args.noSur
        save(thefile,'mugram','time','f','obj','ind','selectLoc','ntrials','prefStim','stimdiff')
    else
        save(thefile,'mugram','time','f','obj','ind','sur','msur','sig','thepairs','probPS','alpha','alphaExp','selectLoc','sigcondStim','ntrials','prefStim','stimdiff','prefStimPhase','prefStimPhaseSTD','prefStimS','scoh','mCstim','vCstim')
    end
    display(sprintf('saving %s',thefile))
end

if Args.plot
    if Args.MatchAlign
        ntime = [-1.6:0.02:0.1550];
        xunits = [-1.6 0.1550];
    else
        ntime = [0:0.02:1.8];
        xunits = [0.11 1.8];
    end
    figure
    if ~Args.noSur
        
        subplot(2,1,1)
        %         thedata = squeeze(nansum(sig,1))./squeeze(sum(~isnan(sig),1));
        thedata = squeeze(sum(sig,1))./size(sig,1);
        if Args.Interpol
            
            nf = [0 :50];
            thedata = interp2(f,time(1:Args.minTbin),thedata,nf,ntime');
            imagesc(ntime,nf,thedata',[0 0.05]);
        else
            imagesc(time(1:Args.minTbin),f,thedata',[0 0.1]);
        end
        colorbar
        ylim([0 50])
        set(gca,'TickDir','out')
        xlim(xunits);
        if Args.NodelaySelection
            thepairs = [1 : size(mugram,1)]; % reset the selected pairs to all for the distribution of magnitudes
            nf = [0 :50];
            
        end
    else
        
        thepairs = [1 : size(mugram,1)];
        nf = [0 :50];
        
    end
    switch Args.biasCorr
        case 'PresampleDiv'
            mugram = mugram(:,1:Args.minTbin,:) ./ repmat(nanmean(mugram(:,presample,:),2),[1 Args.minTbin 1]);
            
        case 'ExpV'
            %             clear texpv
            %             for ii = 1 : size(mugram,1)
            %                 texpv(ii,:,:) = getExpValue(squeeze(msur(ii,:,:)),f);
            %
            %                 alphaExpV = repmat(sum(mugram(:,presample,:),2) ./ sum(texpv(:,presample,:),2),1,Args.minTbin,1);
            %             end
            mugram = mugram - alphaExp;
        case 'PresampleZscore'
            mugram = (mugram(:,1:Args.minTbin,:) - repmat(nanmean(mugram(:,presample,:),2),[1 Args.minTbin 1])) ./ repmat(nanstd(mugram(:,presample,:),[],2),[1 Args.minTbin 1]);
            
            %                  mugram = (mugram - msur{1}) ./ msur{2};
    end
    % norm of the surrogate
    
    subplot(2,1,2)
    if Args.onlySigCohPairs
        sl = unique(selectLoc);
        if length(sl) > 1
            error('more than one location selected') 
        end
        [row,col] = find(sigcondStim(:,sl) > Args.sigBinCoh);
        if ~isempty(Args.exclAlphaPairs)
           row = intersect(row,find(alpha < prctile(alpha,Args.exclAlphaPairs))); 
        end
        thedata = squeeze(nanmean(mugram(row,:,:),1));
    else
        thedata = squeeze(nanmean(mugram,1));
    end
    if Args.Interpol
        thedata = interp2(f,time(1:Args.minTbin),thedata,nf,ntime');
        imagesc(ntime,nf,thedata');
    else
        imagesc(time,f,thedata');
    end
    colorbar
    % imagesc(time,f,squeeze(nanmean(mugram(thepairs,:,:),1))',[0.15 0.5]); colorbar
    ylim([0 50])
    caxis([-0.05 0.05]);
    xlim(xunits);
    set(gca,'TickDir','out')
    
end
%
%
%     [r,betapos] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});
%     [mugram,time,f,ntrials,sig,idePairs,presample] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','delayRange',[1.3 1.8]);
%     [mugram,time,f,ntrials,sig,LocPairs,presample] = mkAveMugram(obj,ind,'type','LocPerIde','plot','delayRange',[1.3 1.8]);
%     tuned = union(idePairs,LocPairs);
%     length(intersect(betapos,ind(tuned)))

% figure;
% subplot(2,1,1)
% imagesc(time,f,squeeze(prctile(mugram(idePairs,:,:),50,1))',[0.15 0.4]); colorbar
% subplot(2,1,2)
% imagesc(time,f,squeeze(prctile(mugram(idePairs,:,:),50,1))',[0.15 0.4]); colorbar

function ExpValue = getExpValue(mutualgram,f)
for ff = 1 : length(f)
    [distr,values] = hist(squeeze(mutualgram(:,:,ff)), 500) ;
    distr = distr ./ repmat(sum(distr,1),500,1);
    ExpValue(:,ff) = sum(distr .* repmat(values,1,size(distr,2)),1);
end










