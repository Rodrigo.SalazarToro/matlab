function xy = get_grid_coordinates(varargin)


%gets xy coordinates of grids for acute and semichronic 32 recordings

Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

if Args.ml
    %semichronic grid
    sch_order = [ ...
        0; 1; 2; 3; 4; 5;
        0; 6; 7; 8; 9;10;
        11;12;13;14;15;16;
        17;18;19;20;21;22;
        0;23;24;25;26;27;
        0;28;29;30;31;32];
    
    %get XY coordinates for all channels
    xy = [];
    cch = 0;
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if sch_order(ch) ~= 0
                cch = cch +1;
                xy(cch,1) = xx;
                xy(cch,2) = x;
            end
        end
    end
    
else
    
    %64 channel grids are the same for parietal and frontal
    order = [ ...
        1;2;3;4;5;6;7;8;
        9;10;11;12;13;14;15;16;
        17;18;19;20;21;2;23;24;
        25;26;27;28;29;30;31;32;
        33;34;35;36;37;38;39;40;
        41;42;43;44;45;46;47;48;
        49;50;51;52;53;54;55;56;
        57;58;59;60;61;62;63;64];
    
    %get XY coordinates for all channels
    xy = [];
    ch = 0;
    for x = 1:8
        for xx = 1:8
            ch = ch + 1;
            xy(ch,1) = xx;
            xy(ch,2) = x;
        end
    end
    
end