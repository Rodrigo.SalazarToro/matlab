function xcorrSpikeTrialBased(alltrials,sp1,sp2,mts,varargin)
% does the spikecountcorr using a sliding window over a fixed number of
% trials
Args = struct('save',0,'redo',0,'nTrials',50,'addName',[]);
Args.flags = {'save','redo','surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'save','redo'});

matfile = sprintf('xccorrg%s%sg%s%s%sTrialBased.mat',sp1.sp.data.groupname,sp1.sp.data.cellname,sp2.sp.data.groupname,sp2.sp.data.cellname,Args.addName);


if isempty(nptDir(matfile)) || Args.redo
    rc = zeros(length(alltrials) - Args.nTrials,2,4);% rc(trialsxalignmentxbin)
    for tbin = 1 : length(alltrials) - Args.nTrials
        strials = alltrials(tbin : Args.nTrials + tbin);
        
        [rc(tbin,:,:),bins] = spikeCountCorr(sp1.sp,sp2.sp,mts,strials,'xcorr','binSize',500,'tstartend',[-500 1000;-1000 0],modvarargin{:},'redo');
    end
    
    [allR,P] = perfXCspike(alltrials,sp1,sp2,mts,rc,modvarargin{:});
    if Args.save
        save(matfile,'rc','bins','allR','P')
        display([pwd '/' matfile])
    end
else
    load(matfile)
end