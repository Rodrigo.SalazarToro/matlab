function makeBPandHBLFP(varargin)
% in the session folder

Args = struct('band',[8 44 4]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

cd lfp
files = nptDir('*_lfp.*');


% switch Args.band
%     case 'beta'
%        bandpassed = [12 22];
%     case 'gamma'
%         bandpassed = [25 38];
% end
f = [Args.band(1):Args.band(3):Args.band(2)];
for ff = 1 : length(files)
    [data,~,~,~,~,~]=nptReadStreamerFile(files(ff).name);
    [prelfp,SR] = preprocessinglfp(data(1,:),'bandP',[f(1)-Args.band(3) f(1)+Args.band(3)],'No60Hz',1,'detrend',modvarargin{:});
    hb = zeros(size(data,1),length(prelfp),length(f));
    
    for ch =1 : size(data,1)
        fc = 1;
        for fi = f
            [prelfp,~] = preprocessinglfp(data(ch,:),'bandP',[fi-Args.band(3) fi+Args.band(3)],'No60Hz',1,'detrend',modvarargin{:});
            hb(ch,:,fc) = hilbert(prelfp);
            fc = fc + 1;
        end
    end
    matfile = regexprep(files(ff).name, 'lfp', 'mband');
    save(matfile,'SR','f','hb')
    display(['saving ' pwd '/' matfile])
end

mkdir('mband')
unix(sprintf('mv *%s.* %s/','mband','mband'))
cd ..