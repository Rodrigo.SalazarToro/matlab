function glm_output = glm_r(varargin)

%prepares model for R
Args = struct('stim_number',[],'allcross',[],'model',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

all_cross = Args.allcross; %rows are stimuli

if Args.model == 1
    nstims = 9;
    time = 1:9;
    ntime = 9;
else
    nstims = 3;
    time = 1:9;
    ntime = 9;
end

all_times = [];
for yy = 1 : nstims
    asm = size(all_times,1) + 1;
    all_times = [all_times; time'];
    all_stim_numbers((asm:((asm-1)+ntime)),1) = Args.stim_number(yy);
    stims((asm:((asm-1)+ntime)),1) = yy;
    cross((asm:((asm-1)+ntime)),1)  = all_cross(yy,:);
end

glm_stimuli(:,1) = all_times;
glm_stimuli(:,2) = stims;
glm_stimuli(:,3) = cross;
glm_stimuli(:,4) = all_stim_numbers;

if Args.model == 1
    
    csvwrite('glm_stimuli.csv',glm_stimuli,1,0)
    %run R batch
    %!R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/matlab_glm_code_full.R
    system(['R CMD BATCH --slave --no-timing ' which('matlab_glm_code_full.R')]);
    
    
    %get R output and place in a structure
    fid = fopen('matlab_glm_code_full.Rout');
    
    %p-vals : DnD test for interaction model(respons~TIME+STIM+TIME*STIM) VS model(response~TIME+STIM)
    %       : DnD test for STIM model(response~TIME+STIM) VS model(response~TIME)
    %       : walds test for time model(response~TIME)
    
    %coefficients : all three models
    
    e=0;
    while e == 0
        time = fgetl(fid);
        if ~isnan(str2double(time(5:end))) %make sure any messages that appear at top of output are not reported as p-values
            e = 1;
        end
    end
    
    glm_output.time = time(5:end);
    
    stims = fgetl(fid);
    glm_output.stims = stims(5:end);
    
    interaction = fgetl(fid);
    glm_output.interaction = interaction(5:end);
    
    aicfull = fgetl(fid);
    glm_output.aic_full = aicfull(5:end);
    
    aicreduced = fgetl(fid);
    glm_output.aic_reduced = aicreduced(5:end);
    
    fgetl(fid); %discard
    
    %interaction coefficients
    for ic = 1:18
        interaction_coefs{ic,1} = fgetl(fid);
        glm_output.interaction_coefs{ic,1} = interaction_coefs{ic,1}(6:end);
    end
    
    fgetl(fid); %discard
    
    %interaction coefficients, pval
    for ic = 1:18
        interaction_coefs_pval{ic,1} = fgetl(fid);
        glm_output.interaction_coefs_pval{ic,1} = interaction_coefs_pval{ic,1}(6:end);
    end
    
    fgetl(fid); %discard
    
    for ic = 1 : 2
        time_coefs{ic,1} = fgetl(fid);
        glm_output.time_coefs{ic,1} = time_coefs{ic,1}(6:end);
    end
    
    fgetl(fid); %discard
    
    for ic = 1 : 2
        time_coefs_pval{ic,1} = fgetl(fid);
        glm_output.time_coefs_pval{ic,1} = time_coefs_pval{ic,1}(6:end);
    end
    
    fclose(fid);
    
    
    
elseif Args.model == 2 || Args.model == 3
    if Args.model == 2
        csvwrite(['glm_location.csv'],glm_stimuli,1,0)
        %run R batch
        system(['R CMD BATCH --slave --no-timing ' which('matlab_glm_code_location.R')]);
        fid = fopen('matlab_glm_code_location.Rout');
    elseif Args.model == 3
        csvwrite(['glm_identity.csv'],glm_stimuli,1,0)
        %run R batch
        system(['R CMD BATCH --slave --no-timing ' which('matlab_glm_code_identity.R')]);
        %get R output and place in a structure
        fid = fopen('matlab_glm_code_identity.Rout');
    end
    
    e=0;
    while e == 0
        interaction = fgetl(fid);
        if ~isnan(str2double(interaction(5:end))) %make sure any messages that appear at top of output are not reported as p-values
            e = 1;
        end
    end
    
    glm_output.interaction = interaction(5:end);
    
    fgetl(fid); %discard
    
    %interaction coefficients
    for ic = 1:6
        interaction_coefs{ic,1} = fgetl(fid);
        glm_output.interaction_coefs{ic,1} = interaction_coefs{ic,1}(7:end);
    end
    
    fgetl(fid); %discard
    
    %interaction coefficients, pval
    for ic = 1:6
        interaction_coefs_pval{ic,1} = fgetl(fid);
        glm_output.interaction_coefs_pval{ic,1} = interaction_coefs_pval{ic,1}(7:end);
    end
    
    fclose(fid);
end








