function [r,varargout] = get(obj,varargin)

%get function for mtsCPP
%Object level is session object
%gets channel pairs, plot function is then used to get specific trials
Args = struct('Number',0,'ObjectLevel',0,'pp',0,'pf',0,'ff',0,'hist_numbers',[],'identity',0,'location',0,...
    'in_phase',0,'anti_phase',0,'mlhist',[],'ml',0,'rule',0,'record_depth',[],'sorted',1,'ips',[],'sulcus',...
    [],'glm_delay_pval',[],'time_slope_pval',[],'time_slope',[],'sig_avg_corr_sse',0,'ppcolumn',0,'br_diag',0,...
    'bl_diag',0,'pprow',0,'avg_corr_pval',[],'cross_corr',[],'chi2_gof',[],'noreject',1,'fixation',0,'incorr_iti',0,'correct_delay_incorr_iti',0);


%rule: identity(1), location(2)

Args.flags = {'Number','ObjectLevel','pp','pf','ff','identity','location','threshold','in_phase','anti_phase','ml',...
    'sig_avg_corr_sse','ppcolumn','pprow','br_diag','bl_diag','fixation','incorr_iti','correct_delay_incorr_iti'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;
%Arguments
%   'pp': parietal Vs parietal
%   'pf': parietal Vs frontal
%   'ff': frontal Vs frontal
%   'hist_numbers': selects only pairs with both channels matching the hist_number/s. Numbers are made in NeuronalHist.m

if Args.Number
    
    if Args.sorted
        rtemp6 = find(obj.data.Index(:,10) == 1)';
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.noreject
        rtemp11 = find(obj.data.Index(:,11) == 1)';
    else
        rtemp11 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp1
    if Args.pp
        rtemp1 = find(obj.data.Index(:,1) == 1)';
    else
        rtemp1 = [1 : length(obj.data.Index(:,1))];
    end
    
    %rtemp2
    if Args.pf
        rtemp2 = find(obj.data.Index(:,2) == 1)';
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp3
    if Args.ff
        rtemp3 = find(obj.data.Index(:,3) == 1)';
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp4
    if Args.location || Args.identity
        if Args.location
            rtemp4 = find(obj.data.Index(:,4) == 0);
        end
        if Args.identity
            rtemp4 = find(obj.data.Index(:,4) == 1);
        end
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp5
    if ~isempty(Args.hist_numbers)
        rtemp5 = [];
        %histology information is in Index 7,8
        for hn = 1 : size(Args.hist_numbers,1)
            histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,1));
            histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,2));
            rtemp5 = [rtemp5 intersect(histi,histii)'];
            
            histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,2));
            histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,1));
            rtemp5 = [rtemp5 intersect(histi,histii)'];
        end
        rtemp5 = sort(rtemp5);
    else
        %kick out 9L channels from clark
        rtemp5 = intersect(find(obj.data.Index(:,7) ~= 17),find(obj.data.Index(:,8) ~= 17));
        
        %         rtemp5 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp7
    if ~isempty(Args.ips)
        rtemp7 = find(obj.data.Index(:,13) == Args.ips)';
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp8,rtemp9
    %checks to see if both channels are Args.sulcus distance from the ips
    if ~isempty(Args.sulcus) && ~Args.ml
        rtemp8 = find(obj.data.Index(:,16) > Args.sulcus);
        rtemp9 = find(obj.data.Index(:,17) > Args.sulcus);
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    
    %rtemp10
    if Args.sig_avg_corr_sse
        rtemp10 = [find(obj.data.Index(:,70) < .01)]; %this is the full trial
    else
        rtemp10 = [1 : size(obj.data.Index,1)];
    end
    
    %this is the delay (600 ms)
    if Args.in_phase || Args.anti_phase
        if ~Args.fixation
            if Args.in_phase
                rtemp12 = find(obj.data.Index(:,61) >= 0 );
            else
                rtemp12 = find(obj.data.Index(:,61) < 0 );
            end
        else
            if Args.in_phase
                rtemp12 = find(obj.data.Index(:,115) >= 0 );
            else
                rtemp12 = find(obj.data.Index(:,115) < 0 );
            end
        end
    else
        rtemp12 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.pprow
        rtemp14 = find(obj.data.Index(:,26))';
    else
        rtemp14 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.ppcolumn
        rtemp15 = find(obj.data.Index(:,27))';
    else
        rtemp15 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.br_diag
        rtemp16 = find(obj.data.Index(:,100))';
    else
        rtemp16 = [1 : size(obj.data.Index,1)];
    end
    
    if Args.bl_diag
        rtemp17 = find(obj.data.Index(:,101))';
    else
        rtemp17 = [1 : size(obj.data.Index,1)];
    end
    
    %delay for 9stim model
    if ~isempty(Args.glm_delay_pval)
        rtemp21 = find(obj.data.Index(:,30) < Args.glm_delay_pval);
    else
        rtemp21 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.time_slope_pval)
        rtemp22 = find(obj.data.Index(:,28) < Args.time_slope_pval);
    else
        rtemp22 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.time_slope)
        rtemp23 = find(obj.data.Index(:,44) > Args.time_slope(1));
        rtemp24 = find(obj.data.Index(:,44) < Args.time_slope(2));
    else
        rtemp23 = [1 : size(obj.data.Index,1)];
        rtemp24 = [1 : size(obj.data.Index,1)];
    end
    
    
    %only take channels that are > specific depths
    if ~isempty(Args.record_depth)
        rtemp26 = find(obj.data.Index(:,5) > Args.record_depth(1))';
        rtemp27 = find(obj.data.Index(:,6) > Args.record_depth(2))';
    else
        rtemp26 = [1 : size(obj.data.Index,1)];
        rtemp27 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.mlhist)
        if Args.mlhist(1) == Args.mlhist(2)
            rtemp28 = [find(obj.data.Index(:,7) == Args.mlhist(1))]';
            rtemp29 = [find(obj.data.Index(:,8) == Args.mlhist(2))]';
        else
            rtemp28 = [find(obj.data.Index(:,7) == Args.mlhist(1)); find(obj.data.Index(:,8) == Args.mlhist(1))]';
            rtemp29 = [find(obj.data.Index(:,7) == Args.mlhist(2)); find(obj.data.Index(:,8) == Args.mlhist(2))]';
        end
    else
        rtemp28 = [1 : size(obj.data.Index,1)];
        rtemp29 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.avg_corr_pval)
        if Args.incorr_iti
            rtemp30 = intersect(find(obj.data.Index(:,182) <= Args.avg_corr_pval), find(obj.data.Index(:,185) <= Args.avg_corr_pval))'; %check both delay and iti(incorrect)
        elseif Args.fixation
            rtemp30 = [find(obj.data.Index(:,143) <= Args.avg_corr_pval)]';
        elseif Args.correct_delay_incorr_iti
            rtemp30 = intersect(find(obj.data.Index(:,89) <= Args.avg_corr_pval), find(obj.data.Index(:,185) <= Args.avg_corr_pval))'; %check both delay(correct) and iti(incorrect)
        else
            rtemp30 = [find(obj.data.Index(:,89) <= Args.avg_corr_pval)]'; %check both delay(correct) and iti(incorrect)
        end
    else
        rtemp30 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.cross_corr)
        if Args.incorr_iti
            rtemp25 = intersect(find(obj.data.Index(:,158) >= Args.cross_corr), find(obj.data.Index(:,161) >= Args.cross_corr))'; %check both the delay and iti for incorrect trials
        elseif Args.fixation
            rtemp25 = [find(obj.data.Index(:,122) >= Args.cross_corr)]';
            
        elseif Args.correct_delay_incorr_iti
            rtemp25 = intersect(find(obj.data.Index(:,68) >= Args.cross_corr), find(obj.data.Index(:,161) >= Args.cross_corr))'; %check both the delay(correct) and iti(incorrect)
        else
            %look at delay 200:800
            rtemp25 = find(obj.data.Index(:,68) >= Args.cross_corr); 
        end
    else
        
        
        rtemp25 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.chi2_gof)
        %look at delay 200:800
        rtemp20 = [find(obj.data.Index(:,96) <= chi2inv(Args.chi2_gof,92))]'; %example Args.chi2gof = .0001
    else
        rtemp20 = [1 : size(obj.data.Index,1)];
    end
    
    
    %unused rtemps
    rtemp13 = [1 : size(obj.data.Index,1)];
    rtemp18 = [1 : size(obj.data.Index,1)];
    rtemp19 = [1 : size(obj.data.Index,1)];
    
    
    
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect...
        (rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect...
        (rtemp10,intersect(rtemp11,intersect(rtemp12,intersect(rtemp13,intersect...
        (rtemp14,intersect(rtemp15,intersect(rtemp16,intersect(rtemp17,intersect...
        (rtemp18,intersect(rtemp19,intersect(rtemp20,intersect(rtemp21,intersect...
        (rtemp22,intersect(rtemp23,intersect(rtemp24,intersect(rtemp25,intersect...
        (rtemp26,intersect(rtemp27,intersect(rtemp28,intersect(rtemp29,rtemp30)))))))))))))))))))))))))))));
    
    r = length(varargout{1});
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end

