function obj = plot(obj,varargin)

%used to plot mtsCPP objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct'

Args = struct('threshold',0,'stable',0,'transition',0,'correct',0,'incorrect',0,'avgCorr',0,'epochs',0,'glm',0,'avg_stim_ccg',0,'ml',0,'identity',0,'location',0,'day_gabors',0);
Args.flags = {'stable','transition','correct','incorrect','avgCorr','epochs','glm','avg_stim_ccg','ml','identity','location','day_gabors'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

%rule: identity(1), location(2)
if Args.ml
    if Args.identity
        Args.rule = 1;
    elseif Args.location
        Args.rule = 2;
    else
        fprintf(1,'enter identity or location')
    end
end

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end
%loads the channelpair specified in the index

channel_pair = obj.data.setNames{ind};

pair = obj.data.Index(ind,20);

%get histology information
[hist1 h1] = reverse_categorizeNeuronalHist(obj.data.Index(ind,7));
[hist2 h2] = reverse_categorizeNeuronalHist(obj.data.Index(ind,8));



%determine the unique days so that whole days can be plotted at the same time
daylist = [];
dcounter = 0;
counter = 0;
for d = dataindices
    counter = counter + 1;
    sd = findstr('session',obj.data.setNames{d});
    day = str2double(obj.data.setNames{d}(sd-7:sd-2));
    
    if d == 1
        dcounter = dcounter + 1;
        alldays(dcounter,:) = day;
        daylist(counter) = dcounter;
    else
        if day == alldays(dcounter,:)
            daylist(counter) = dcounter;
        else
            dcounter = dcounter + 1;
            alldays(dcounter,:) = day;
            daylist(counter) = dcounter;
        end
    end
end

if Args.day_gabors
    
    inds = dataindices(find(daylist == n));
    ninds = size(inds,2);
    
    %plot all the gabors for each day during specific epochs
    s = findstr('channel',obj.data.setNames{inds(1)});
    slash = findstr(filesep,obj.data.setNames{inds(1)});
    cd(obj.data.setNames{inds(1)}(1:(s-1)));
    dayname = num2str(obj.data.setNames{inds(1)}(slash(end-4)+1:(slash(end-3))-1));  
    
    cd('identity')
    load all_gabors
    cd ..
    idegabors = all_gabors;
    
    cd('incorrect')
    load all_gabors
    cd ..
    incorrgabors = all_gabors;
    
    nrows = ceil(ninds / 5);
    if nrows > 5
        ncolumns = 10;
    else
        ncolumns = 5;
    end
    pcounter = 0;
    for allind = 1 : ninds
        pcounter = pcounter + 1;
        if allind == 41 || allind == 81 || allind == 121 || allind == 161
            figure;
            pcounter = 1;
        end
        subplot(4,10,pcounter)
        [dayhist1] = reverse_categorizeNeuronalHist(obj.data.Index(inds(allind),7));
        [dayhist2] = reverse_categorizeNeuronalHist(obj.data.Index(inds(allind),8));

        indpair = obj.data.Index(inds(allind),20);
        
        %get gabor information
        cgabor = idegabors{5,indpair}.cf;
        peak = idegabors{5,indpair}.peak;
        phase = idegabors{5,indpair}.phase_angle_deg;
        
        plot(-50:50,cgabor,'b')
        hold on
        
        
        %get gabor information
        cgabor = incorrgabors{5,indpair}.cf;
        peak = incorrgabors{5,indpair}.peak;
        phase = incorrgabors{5,indpair}.phase_angle_deg;
        
        plot(-50:50,cgabor,'g')
        hold on
        
        %get gabor information
        cgabor = incorrgabors{8,indpair}.cf;
        peak = incorrgabors{8,indpair}.peak;
        phase = incorrgabors{8,indpair}.phase_angle_deg;
        
        plot(-50:50,cgabor,'r')
        hold on
        
        %draw phase line
        yMin = -1; yMax = 1;
        xVal = phase;
        plot([0 0],[-1 1],'color','k')
        hold on
        
        %draw correlation coef line
        plot([-50 50],[0 0])
        axis tight
        
        if pcounter == 1
            title(gca,[dayname ' ' dayhist1{1} '/' dayhist2{1}]);
        else
        title(gca,[ dayhist1{1} '/' dayhist2{1}]);
        end
    end
    return
end





if Args.avgCorr || Args.epochs
    %%
    s = findstr('channel',channel_pair);
    cd(channel_pair(1:(s-1)));
    
    channel1 = obj.data.Index(ind,23);
    channel2 = obj.data.Index(ind,24);
    
    
    
    if Args.avgCorr
        cd('identity')
        load all_gabors
        cd ..
        
        %get correlogram and gabor information, plot full trial
        c = all_gabors{7,pair}.epoch_correlogram;
        cgabor = all_gabors{7,pair}.cf;
        peak = all_gabors{7,pair}.peak;
        phase = all_gabors{7,pair}.phase_angle_deg;
        
        cross_corr = all_gabors{7,pair}.cross_corr;
        
        %get surrogate
        plot(-50:50,c)
        hold on
        plot(-50:50,cgabor,'r')
        %draw phase line
        yMin = -1; yMax = 1;
        xVal = phase;
        plot([0 0],[-1 1],'color','k')
        hold on
        
        %draw correlation coef line
        plot([-50 50],[0 0])
        
        %         scatter(phase,peak,'hg');
        
        
        t = [obj.data.setNames{ind}];
        
        title(gca,[t  '  ' num2str(cross_corr)],'fontsize',12,'fontweight','b');
        
        
        
    elseif Args.epochs
        if Args.identity
            cd identity
        elseif Args.location
            cd location
        end
        
        load avg_correlograms_epochs avg_correlograms_epochs
        load epoch_gabors
        
        es = 0;
        if exist('epoch_surrogates.mat')
            es = 1;
            load epoch_surrogates
            
            %determine local surrogates
            local(1,:) = prctile(epoch1(pair,:),[.05 99.95]);
            local(2,:) = prctile(epoch2(pair,:),[.05 99.95]);
            local(3,:) = prctile(epoch3(pair,:),[.05 99.95]);
            local(4,:) = prctile(epoch4(pair,:),[.05 99.95]);
        end
        
        ges = 0;
        if exist('global_epoch_surrogates.mat')
            ges = 1;
            load global_epoch_surrogates
            
            %determine global surrogates
            globals(1,:) = prctile(epoch1,[.05 99.95]);
            globals(2,:) = prctile(epoch2,[.05 99.95]);
            globals(3,:) = prctile(epoch3,[.05 99.95]);
            globals(4,:) = prctile(epoch4,[.05 99.95]);
        end
        
        
        eg = epoch_gabors{pair};
        freq(1) = eg{1}.frequency;
        freq(2) = eg{2}.frequency;
        freq(3) = eg{3}.frequency;
        freq(4) = eg{4}.frequency;
        
        p_angle(1) = eg{1}.phase_angle_deg;
        p_angle(2) = eg{2}.phase_angle_deg;
        p_angle(3) = eg{3}.phase_angle_deg;
        p_angle(4) = eg{4}.phase_angle_deg;
        
        c_epochs = avg_correlograms_epochs{1,pair};
        
        lags = [-50:50];
        yRange = max(max(abs(c_epochs)));
        for x = 1:4
            subplot(1,4,x)
            plot(lags,c_epochs(x,:))
            hold on
            axis([-50 50 (-1*yRange) yRange])
            
            
            %             yMin = (-1*yRange); yMax = yRange;
            %             xVal = 0;
            %             plot([xVal, xVal],[yMin yMax],'color','k')
            plot([0 0],[-.5 .5],'color','k')
            hold on
            
            %plot gabor fits in red
            plot(lags,eg{x}.cf,'r')
            
            if es
                %plot local surrogates
                plot([-50, 50],[local(x,1) local(x,1)],'b')
                plot([-50, 50],[local(x,2) local(x,2)],'b')
            end
            
            if ges
                %plot global surrogates
                plot([-50, 50],[globals(x,1) globals(x,1)],'r')
                plot([-50, 50],[globals(x,2) globals(x,2)],'r')
            end
            
            hold on
            
            text(-45,-.45,['frequency: ' num2str(freq(x))])
            text(-45,-.48,['phase angle: ' num2str(p_angle(x))])
            
            hold on
            
            
            
            text(-48,.45,[hist1 ' ' h1])
            text(-35,.45,[hist2 ' ' h2])
            
            axis([-50 50 -.5 .5])
        end
        
        %axis([-50 50 (-1*yRange) yRange])
        
        name = obj.data.setNames{ind};
        
        slash = strfind(name,filesep);
        
        ch_slash = size(slash,2);
        cs = slash(ch_slash);
        
        day_slash = ch_slash - 5;
        ds = slash(day_slash);
        
        plot_title = [name((ds+1):(ds+12)) '   ' name((cs+1):end)];
        
        
        
        
        
        title(subplot(1,4,2),plot_title,'fontsize',12,'fontweight','b');
        
        % %         cc = c_epochs(x,:);
        % %
        % %         max_lag = size(c_epochs(x,:),2);
        % %
        % %         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % %
        % %         %Find index of central positive peak
        % %         pp = findpeaks(cc);
        % %
        % %         for ppp = 1 : size(pp.loc,1)
        % %
        % %             if (cc(pp.loc(ppp)) < 0)
        % %                 %set to max phase index
        % %                 pp.loc(ppp) = max_lag;
        % %             end
        % %
        % %         end
        % %
        % %
        % %         [phase, pos_index] = (min(abs(lags(pp.loc))));
        % %         %%%%%%%%%%get real phase value by indexing the location in the lags
        % %         pos_phase = lags(pp.loc(pos_index));
        % %         pos_corrcoef = cc(pp.loc(pos_index));
        % %
        % %         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % %         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % %
        % %         %Find index of central negative peak (inverts correlogram)
        % %         cc = cc*-1;
        % %         np = findpeaks(cc);
        % %
        % %         for nnn = 1 : size(np.loc,1)
        % %
        % %             if (cc(np.loc(nnn)) < 0)
        % %                 %set to max phase index
        % %                 np.loc(nnn) = max_lag;
        % %             end
        % %
        % %         end
        % %
        % %         [phase, neg_index] = (min(abs(lags(np.loc))));
        % %
        % %         neg_phase = lags(np.loc(neg_index));
        % %         neg_corrcoef = cc(np.loc(neg_index));
        % %
        % %
        % %         %Determines if the closest peak to zero is positive or negative.
        % %         %Stores the correlation coef and lag position.
        % %         if abs(pos_phase) <= abs(neg_phase)
        % %             avg_phase = pos_phase;
        % %             avg_corrcoef = pos_corrcoef;
        % %         else
        % %             avg_phase = neg_phase;
        % %             avg_corrcoef = (-1 * neg_corrcoef); %make negative
        % %         end
        % %
        % %         %draw phase line
        % %         yMin = -1; yMax = 1;obj.data.setNames
        % %         xVal = avg_phase;
        % %         plot([xVal, xVal],[yMin yMax],'color','k')
        % %         hold on
        % %
        % %         %draw correlation coef line
        % %         xMin = -50; xMax = 50;
        % %         yVal = avg_corrcoef;
        % %         plot([xMin xMax],[yVal, yVal],'color','r')
        % %         hold on
        % %
        % %         %draw mean
        % %         xMin = -50; xMax = 50;
        % %         yVal = m;
        % %         plot([xMin xMax],[yVal, yVal],'color','k')
        % %         hold on
        % %
        % %         %draw 2nd standard deviation positive
        % %         xMin = -50; xMax = 50;
        % %         yVal = m + pos2std;
        % %         plot([xMin xMax],[yVal, yVal],'color','g')
        % %         hold on
        % %
        % %         %draw 2nd standard deviation positive
        % %         xMin = -50; xMax = 50;
        % %         yVal = m - pos2std;
        % %         plot([xMin xMax],[yVal, yVal],'color','g')
        % %
        % %         if abs(corrcoef) > yRange
        % %             yRange = abs(corrcoef);
        % %         end
        % %         if pos2std > yRange
        % %             yRange = pos2std;
        % %         end
        % %
        % %         t = [obj.data.setNames{ind},'.   Average Correlogram for Epoch ' num2str(x)];
        % %
        % %         title(t,'fontsize',12,'fontweight','b');
        % %
        % %         axis([-50 50 (-1*(yRange+.1)) yRange+.1])
        
    end
    return
end
%%

if Args.avg_stim_ccg
    
    s = findstr('channel',channel_pair);
    cd(channel_pair(1:(s-1)));
    lfp2dir = pwd;
    cd ..
    cd ..
    
    channel1 = obj.data.Index(ind,23);
    channel2 = obj.data.Index(ind,24);
    
    if Args.ml
        make_stimulus_avg_ccg('pair',pair,'ml',Args.ml,'plot','mts_cpp')
    else
        make_stimulus_avg_ccg('pair',pair,'plot','mts_cpp')
    end
    
    cd(lfp2dir)
    set(gcf,'name',[obj.data.setNames{ind}(end-14:end) '         ' hist1{1} '    ' hist2{1}])
    
    return
end



t = Args.threshold;
%load (channel_pair,'corrcoefs','phases','indexes')
if ~Args.ml
    load(channel_pair,'plot_data','pair_thresholds');
    % %t = [channel_pair(1 : (end-17)), 'threshold'];
    if t>0
        thresholds = pair_thresholds(:,t);
    else
        thresholds = 0;
    end
else
    load(channel_pair,'plot_data','indexes','corrcoefs')
    curdir = pwd;
    
    
    cd(channel_pair(1:(end-15))) %go to lfp2 dir
    
    if Args.rule == 1
        cd('identity');
        load threshold
        cd ..
    elseif Args.rule == 2
        cd('location')
        load threshold
        cd ..
    end
    
    cd(channel_pair(1:(end-25))) %go to session dir
    
    thresholds = threshold{pair}(t);
    
    mt = mtstrial('auto','ML','RTfromML');
    
    if Args.correct && Args.stable
        in = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',Args.rule);
        index = indexes{1};
    elseif Args.correct && Args.transition
        in = mtsgetTrials(mt,'BehResp',1,'transition','ML','rule',Args.rule);
        index = indexes{2};
    elseif Args.incorrect && Args.stable
        in = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',Args.rule);
        index = indexes{3};
    elseif Args.incorrect && Args.transition
        in = mtsgetTrials(mt,'BehResp',0,'transistion','ML','rule',Args.rule);
        index = indexes{4};
    else
        fprintf(1,['\n','Must enter a combination of correct/incorrect and stable/transition','\n','\n']);
        in = mtsgetTrials(mt,'ML');
    end
    
    [final_trials final_ind] = intersect(index,in);
    
    cd(curdir);
end

%no criterion == 1
%correct/stable == 2
%correct/transition == 3
%incorrect/stable == 4
%incorrect/transition == 5

if Args.correct || Args.incorrect || Args.stable || Args.transition
    if Args.correct && Args.stable
        %trial_index = indexes{1};
        p_data = plot_data{2,(t+1)};
    elseif Args.correct && Args.transition
        %trial_index = indexes{2};
        p_data = plot_data{3,(t+1)};
    elseif Args.incorrect && Args.stable
        %trial_index = indexes{3};
        p_data = plot_data{4,(t+1)};
    elseif Args.incorrect && Args.transition
        %trial_index = indexes{4};
        p_data = plot_data{5,(t+1)};
    else
        fprintf(1,['\n','Must enter a combination of correct/incorrect and stable/transition','\n','\n']);
        %trial_index = 0;
        p_data = plot_data{1,(t+1)};
    end
end

if Args.ml
    number = p_data.number(:,final_ind);
    cors = p_data.cors(:,final_ind);
    phase = p_data.phase(:,final_ind);
    trials = size(final_ind,2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    
    ntrials = sum(isnan(corrcoefs(:,final_trials))');
    
    %ntrials = p_data.ntrials;
    %%%%%%%%%%%%%%%%%%%%%%%%
else
    number = p_data.number;
    cors = p_data.cors;
    phase = p_data.phase;
    trials = size(cors,2);
    ntrials = p_data.ntrials;
end

%if ~trial_index
%  fprintf(1,['Number of Trials: ',num2str(size(p_data,2),'\n']);
%   fprintf(1,'Must enter a combination of corret/incorrect and stable/unstable agruments.\n');
%else



fprintf(1,['Number of Trials: ',num2str(trials),'\n']);
fprintf(1,'\n');
%end
%
% if trial_index == 0
%     corrcoefs = abs(corrcoefs);
%     phases = phases;
%
%
%
% else
%     corrcoefs = abs(corrcoefs(:,trial_index));  %necessary for trials with negative peaks
%     phases = phases(:,trial_index);
% end
%
%
% thresh = Args.threshold;
%
% %Index threshold for the correct threshold
% if thresh > 0
%
%     th = obj.data.thresholds{ind};
%
%     thresholds = th(:,thresh);
%
% else
%     thresholds = 0;
% end
%



% [rows columns] = size(corrcoefs);  %will be the same for the phases
%
% cors = [];
% phase = [];
% number=[];
%
% for yyy = 1:columns
%     corrss = [];
%     phasess= [];
%     x=[];
%     ntrials = [];
%
%
%          for xxx = 1:rows %50ms bins w/ a max at 3000ms
%
%               %creates a row vector that contains the number of trials
%               %for each bin
%               ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
%
%              %Determine if threshold is crossed
%              if  corrcoefs(xxx,yyy) > thresholds
%
%              %If it is then keep the number
%              bb = corrcoefs(xxx,yyy);
%
%              pp = phases(xxx,yyy);
%
%              yy = 1;
%              else
%
%              %Else, input NaN
%              bb = NaN;
%
%              pp = NaN;
%
%              yy = 0;
%              end
%
%          x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
%
%          corrss = cat(1,corrss, bb);
%          phasess = cat(1,phasess,pp);
%
%          end
%
%      number = cat(2,number,x);
%      cors = cat(2,cors,corrss);
%      phase = cat(2,phase,phasess);
% end

%%Calculate the mean for the corcoefs
xy=(cors');

[r c] = size(xy);
meanss=[];
for ccc = 1:c
    mmeans=[];
    for rrr = 1:r
        q =(xy(rrr,ccc));
        qq = isnan(q);
        if qq == 1
        else
            mmeans = cat(1,mmeans,q);
        end
        xxxx = mean(mmeans);
    end
    meanss = [meanss xxxx];
end

%%Calculate the means for the phases
xys=(phase');
[rs cs] = size(xys);
meansss=[];
for cccs = 1:cs
    mmeanss=[];
    for rrrs = 1:rs
        qs =(xys(rrrs,cccs));
        qqs = isnan(qs);
        if qqs == 1
        else
            mmeanss = cat(1,mmeanss,qs);
        end
        xxxxs = mean(mmeanss);
    end
    meansss = [meansss xxxxs];
end
%tttt = percent of ind trials
%tttt = ((sum(number')/length(ind))*100);

nn =[];
c = length(ntrials);
for z = 1 : c
    
    nn = cat(2,nn,trials);
    
end

numbertrials = nn - ntrials;
number = number';
tttt = [];
for zz = 1 : length(numbertrials)
    if numbertrials(1,zz) == 0
        tt = 0;
    else
        tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
        %Determines acutal percentage of trials based on trials left
    end
    tttt = cat(2,tttt,tt);
end

%used for tick labeling. Starts at 150ms
q = (length(meansss)*50)+100;
xxx = [150:50:q];
yyy = [1:2:60];
xx = [150:100:q];

if Args.glm
    %get coef info
    coefs = obj.data.interaction_coefs{ind};
    for x =1:18
        int_coefs(x) = str2double(coefs(x)); %get coefs
    end
    
    terms{1} = [1 2];terms{2} = [1 2 3 11];terms{3} = [1 2 4 12];terms{4} = [1 2 5 13];terms{5}=[1 2 6 14];terms{6}=[1 2 7 15];terms{7}=[1 2 8 16];terms{8}=[1 2 9 17];terms{9}=[1 2 10 18];
    
    for x = 1:9
        for z = 1 : 17 %number observations per stimuli
            t = int_coefs(terms{x});
            lines(x,z) = sum(z*t);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 1
cla
subplot(3,1,1)
title(subplot(3,1,1),obj.data.setNames{ind},'fontsize',10,'fontweight','b');
hold on
boxplot((cors'),'notch','on','symbol','w+');
hold on
plot(meanss,'r')
hold on

%Threshold
xMin = 0; xMax = 60;

yVal = thresholds;
plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
hold on

%Draw Vertical Lines

%Sample On(500ms)
yMin = 0; yMax = 1;
xVal = 8;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%Sample Off(1000ms)
yMin = 0; yMax = 1;
xVal = 18;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%First match onset (1800ms)
yMin = 0; yMax = 1;
xVal = 34;
plot([xVal, xVal],[yMin yMax],'color','g')
hold on

%last match onset (2200ms)
yMin = 0; yMax = 1;
xVal = 42;
plot([xVal, xVal],[yMin yMax],'color','r')
hold off

if t == 0
    ylim([.0 1]);
    
else
    ylim([.2 1]);
end
set(gca,'XTick',yyy)
set(gca,'XTickLabel',xx)

%set(gca,'XTickLabel',xxx,'FontSize',7)
xlabel('Time(ms)')
ylabel('Correlation Coefficients')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 2
subplot(3,1,2)

qq = max(tttt(1:(end-10))) + 10;

if ~Args.glm
    plot(tttt,'r')
else
    tttt = tttt/100;
    emp_logits = log(tttt./(1-tttt));
    plot(emp_logits)
    hold on
    for l = 1:9
        plot([18:34],lines(l,:))
        hold on
    end
    qq = 1;
end

hold on
%plot(sum(number));
%hold on

%% plot regression line

%separate out the end of sample to first match
if Args.glm
    
    time = [1:13];
    b0 = obj.data.time_coefs{ind}(1);
    b1 = obj.data.time_coefs{ind}(2);
    
    res = b0 + b1*time;
    logistic_regression = (1 ./ (1 + exp(-res))) *100;
    hold on
    plot([22:34],logistic_regression,'color','k','LineWidth',2)
end
hold on
%%

%Draw Vertical Lines

%Sample On(500ms)
%yMin = 0; yMax = q;
yMin = 0; yMax =qq;
xVal = 8;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%Sample Off(1000ms)
yMin = 0; yMax = qq;
xVal = 18;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%First match onset (1800ms)
yMin = 0; yMax = qq;
xVal = 34;
plot([xVal, xVal],[yMin yMax],'color','g')
hold on

%Last match onset (2200ms)
yMin = 0; yMax = qq;
xVal = 42;
plot([xVal, xVal],[yMin yMax],'color','r')

hold off

if ~ Args.glm
    set(gca,'XTick',yyy)
    set(gca,'XTickLabel',xx)
    ylabel('% of Trials W/ Siginficant Corr.')
    ylim([0 qq]);
    xlabel('Time(ms)')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%PLOT 3
subplot(3,1,3)

boxplot((phase'),'notch','on','symbol','r+');
hold on
plot(xxx,meansss,'r')
ylabel('Time Lag of Max Correlation (ms)')
hold on

%set(gca,'XTickLabel',xxx)


%Plot zero line
xMin = 0; xMax = 60;

yVal = 0;
plot([xMin xMax],[yVal, yVal],'color','k','LineStyle','--','LineWidth',1)
hold on

%Draw Vertical Lines

%Sample On(500ms)
yMin = -25; yMax = 25;
xVal = 8;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%Sample Off(1000ms)
yMin = -25; yMax = 25;
xVal = 18;
plot([xVal, xVal],[yMin yMax],'color','k')
hold on

%First match onset (1800ms)
yMin = -25; yMax = 25;
xVal = 34;
plot([xVal, xVal],[yMin yMax],'color','g')
hold on

%Last match onset (2200ms)
yMin = -25; yMax = 25;
xVal = 42;
plot([xVal, xVal],[yMin yMax],'color','r')

set(gca,'XTick',yyy)
set(gca,'XTickLabel',xx)
xlabel('Time(ms)')
ylim([-25 25]);
hold off



%linkaxes([subplot(3,1,1),subplot(3,1,2),subplot(3,1,3)],'x');





%xlim([1 50])











