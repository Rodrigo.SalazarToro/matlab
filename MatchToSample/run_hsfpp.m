function run_hsfpp(varargin)

%run at session level
%previously known as run_hilbert_phase.m

%computes HSFPP for all clusters with all channels for 7 different time windows
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

% 8: ITI (incorrect, Betty only)
Args = struct('ml',0,'filter_low',8,'filter_high',25,'redo',0,'incorrect',0); %10 - 20?
Args.flags = {'ml','redo','incorrect'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;


if Args.ml
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    r = 1; %only identity need to save in identity dir
    
    if Args.incorrect
        total_trials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',r);
        
        cd([sesdir filesep 'lfp']);
        if exist('iti_rejectedTrials.mat','file') %this is currently only run on incorrect trials for betty days
            load('iti_rejectedTrials.mat','rejecTrials')
            iti_reject = rejecTrials;
            if ~isempty(iti_reject)
                [~,baditi] = intersect(total_trials,iti_reject);
                total_trials(baditi) = [];
            end
        end
        cd(sesdir)
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
    end
else
    mt = mtstrial('auto','redosetNames');
    %determine rule
    r = mt.data.Index(1,1);
    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
end


%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%get group information
NeuroInfo = NeuronalChAssign(); %get the "group" number that corresponds to each channel

%find groups with spikes
groups = nptDir('group*');
ngroups = [1 : size(groups,1)];
pwd
for g = ngroups %spike channel
    fprintf('\n%0.5g     ',g)
    for gg = ngroups %lfp channels
        fprintf(' %0.5g',gg)
        
        cluster_info = {};
        
        %determine which channels the group number corresponds to
        g2ch = str2double(groups(g).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,g2ch);
        channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
        
        
        
        %get lfp groups information
        lfpg = str2double(groups(gg).name(6:end));
        [~,ii] = intersect(NeuroInfo.groups,lfpg);
        channels(2) = ii; %these are the channels that the groups correspond to (using NeuroInfo)
        
        %determine if pair has already been written
        if Args.ml
            if Args.incorrect
                h_pairs = [ 'incorrect_hilbertpair' num2strpad(g2ch,2) num2strpad(lfpg,2)];
            else
                h_pairs = [ 'hilbertpair' num2strpad(g2ch,2) num2strpad(lfpg,2)];
            end
        else
            h_pairs = [ 'hilbertpair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
        end
        cd([sesdir filesep 'lfp' filesep 'lfp2'])
        spair = nptDir([h_pairs '.mat']);
        cd(sesdir)
        
        if isempty(spair) || Args.redo
            cd([sesdir filesep groups(g).name])
            %find clusters
            clusters = nptDir('cluster*');
            nclusters = size(clusters,1);
            
            for c = 1 : nclusters
                cd([sesdir filesep groups(g).name])
                cd(clusters(c).name);
                cluster_info{1,c} = clusters(c).name;
                
                %get spikes
                load ispikes.mat
                %get lfp group number
                lfp_group = channels(2);
                cd([sesdir filesep 'lfp' filesep 'lfp2'])
                %get trials
                lfp = nptDir('*_lfp2*');
                
                if Args.incorrect
                    s_angle = cell(1,8);
                else
                    s_angle = cell(1,7);
                end
                for ttrial = total_trials
                    load(lfp(ttrial).name);
                    
                    LFP = data(lfp_group,:);
                    
                    spike_data = sp.data.trial(ttrial).cluster.spikes;
                    s = ceil(spike_data); %ceil because a zero spike occured when using "round"
                    
                    %use hilbert transform to determine instantaneous phase angle for each spike
                    h = hilbert(LFP); %don't need to transpose if only one channel
                    
                    %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
                    %COMPENT IS FLIPPED
                    ha = angle(h);
                    sha = ha(s);
                    
                    %determine what epoch each spike is in
                    s_on = sample_on(ttrial);
                    s_off = sample_off(ttrial);
                    m = match(ttrial);
                    
                    % 1: fixation    [(sample_on - 399) : (sample_on)]
                    % 2: sample      [(sample_off - 399) : (sample_off)]
                    % 3: early delay [(sample_off) : (sample_off + 399)]
                    % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
                    % 5: delay       [(sample_off + 201) : (sample_off + 800)]
                    % 6: delay match [(match - 399) : (match)]
                    % 7: full trial  [(sample_on - 500) : (match)]
                    
                    s_angle{1} = [s_angle{1}, sha(s >= (s_on - 399) & s <= s_on)];
                    s_angle{2} = [s_angle{2}, sha(s >= (s_off - 399) & s <= s_off)];
                    s_angle{3} = [s_angle{3}, sha(s >= s_off & s < (s_off + 399))];
                    s_angle{4} = [s_angle{4}, sha(s > (s_off+400) & s <= (s_off + 800))];
                    s_angle{5} = [s_angle{5}, sha(s > (s_off+200) & s <= (s_off + 800))];
                    s_angle{6} = [s_angle{6}, sha(s >= (m - 399) & s <= m)];
                    %                                 s_angle{7} = [s_angle{7}, sha(s >= (s_on - 500) & s <= m)];
                    
                    
                    % 8: iti  [(match + 501) : (match + 900)]
                    if Args.incorrect
                        s_angle{8} = [s_angle{8}, sha(s >= (m - 501) & s <= (m + 900))];    
                    end
                    
                end
                
                for sa = 1 : size(s_angle,2)
                    if size(s_angle{sa} > 1)
                        %                                     c_median(sa) = circ_median(s_angle{sa}');
                        c_mean(sa) = circ_mean(s_angle{sa}');
                        c_rtest(sa) = circ_rtest(s_angle{sa});
                        nspikes(sa) = size(s_angle{sa},2);
                    else
                        %                                     c_median(sa) = nan;
                        c_mean(sa) = nan;
                        c_rtest(sa) = nan;
                        nspikes(sa) = nan;
                    end
                end
                %                             cluster_med{1,c} = c_median;
                cluster_mean{1,c} = c_mean;
                cluster_rtest{1,c} = c_rtest;
                cluster_nspikes{1,c} = nspikes;
   
            end
            %                         hilbert_groups.median = cluster_med;
            hilbert_groups.mean = cluster_mean;
            hilbert_groups.rtest = cluster_rtest;
            hilbert_groups.nspikes = cluster_nspikes;
            if Args.ml
                hilbert_groups.channels = [g2ch lfpg];
            else
                hilbert_groups.channels = channels;
            end
            hilbert_groups.cluster_info = cluster_info;
            
            write_info = writeinfo(dbstack);
            save(h_pairs,'hilbert_groups','write_info')
        end
    end
end






cd(sesdir)