function [status,varargout] = CheckMarkersSeq(varargin)
% To be run in the session directory


Args = struct('RedoSeq',0);
Args.flags = {'RedoSeq'};
Args = getOptArgs(varargin,Args);

markerfile = nptDir('*.mrk','CaseInsensitive');
if isempty(markerfile)
    markerfile = nptDir('*_MRK.mat','CaseInsensitive');
    load(markerfile.name)
else
    [MARKERS,records] = ReadMarkerFile(markerfile.name);

    tmarkers = MarkersToTrials(MARKERS);
end

nmarkers = size(tmarkers,2);

seqfile = nptDir('*.seq','CaseInsensitive');
[SEQUENCE,SessionType] = ReadSequenceFile(seqfile.name);
nseq = size(SEQUENCE,1);

files = [nptDir('*.0*'); nptDir('*.1*')];
nfiles = size(files,1);
pwd
if nfiles ~= nseq | nfiles ~= nmarkers | nmarkers ~= nseq

    status = 0;
    fprintf('The number of markers, sequence or trial-files do not match \n');
    if Args.RedoSeq
        RedoSeqFile;
        fprintf('Correcting the sequence file \n');
        CheckMarkersSeq;
    end
else
    fprintf('The number of markers, sequence or trial-files match \n');
    status = 1;
end

varargout{1} = nmarkers;

varargout{2} = nseq;

varargout{3} = nfiles;