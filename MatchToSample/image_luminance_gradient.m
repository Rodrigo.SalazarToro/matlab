function image_luminance_gradient

gradient = [0:.05:1];
hdir = pwd;

mkdir lum_gradient


%get list of images
all_images = [nptDir('*.bmp') ; nptDir('*.jpg')];
nimages = size(all_images,1);

for ni = 1 : nimages
    cd(hdir)
    ii = imread(all_images(ni).name);
    
    ii(find(single(ii) < 150)) = 0; %get rid of borders
    ii(find(single(ii) >= 150)) = 255;
    
    for ng = gradient
        newii = ii * ng;
        
        cd([hdir filesep 'lum_gradient'])
        imwrite(newii,[all_images(ni).name(1 : end-4) num2str(ng*100) all_images(ni).name(end-3 : end)])
    end
end

cd(hdir)



