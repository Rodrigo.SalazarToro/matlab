function [sig_fix sig_sample sig_delay] = compare_coh


cgrams = nptDir('cohgram*');
ncgrams = size(cgrams,1);

pfix = [];
pfix_rs = [];
psample = [];
psample_rs = [];
pdelay = [];
pdelay_rs = [];
all_groups = [];
for nc = 1 : ncgrams
    
    load(cgrams(nc).name);
    all_groups(nc,:) = idecohgram.groups{1};
    
    allc_fix = [];
    allc_sample = [];
    allc_delay = [];
    allx = [];
    for x = 1:5
        c = idecohgram.C{x};
        
        cf = c(3:9,5:8); %fix
        cf = reshape(cf,1,size(cf,1)*size(cf,2));
        allc_fix = [allc_fix cf];
        allc_f{x} = cf;
        
        cs = c(10:16,5:8); %sample
        cs = reshape(cs,1,size(cs,1)*size(cs,2));
        allc_sample = [allc_sample cs];
        allc_s{x} = cs;
        
        cde = c(27:33,5:8); %delay
        cde = reshape(cde,1,size(cde,1)*size(cde,2));
        allc_delay = [allc_delay cde];
        allc_d{x} = cde;
        
%         subplot(1,5,x);
%         hist(cde)
%         axis([0 .3 0 7])
        
        
        allx = [allx ones(1,4*13)*x];
    end
    
    
    %perform pairwise ks test
    counter = 0;
    for x = 1:5
        for xx = x+1:5
            counter = counter + 1;
            allf(counter) = kstest2((allc_f{x} - median(allc_f{x})),(allc_f{xx} - median(allc_f{xx})));
            allf_rs(counter) = ranksum(allc_f{x},allc_f{xx});
            
            alls(counter) = kstest2((allc_s{x} - median(allc_s{x})),(allc_s{xx} - median(allc_s{xx})));
            alls_rs(counter) = ranksum(allc_s{x},allc_s{xx});
            
            alld(counter) = kstest2((allc_d{x} - median(allc_d{x})),(allc_d{xx} - median(allc_d{xx})));
            alld_rs(counter) = ranksum(allc_d{x},allc_d{xx});
        end
    end
    
    
    if sum(allf) == 0 & sum(alls) == 0 & sum(alld) == 0
        p = kruskalwallis(allc_fix,allx,'off');
        allp_fix(nc) = p;
        pfix = [pfix p];
        pfix_rs = [pfix_rs size(find(allf_rs <.01),2)];
        
        p = kruskalwallis(allc_sample,allx,'off');
        allp_sample(nc) = p;
        psample = [psample p];
        psample_rs = [psample_rs size(find(alls_rs <.01),2)];
        
        p = kruskalwallis(allc_delay,allx,'off');
        allp_delay(nc) = p;
        pdelay = [pdelay p];
        pdelay_rs = [pdelay_rs size(find(alld_rs <.01),2)];
    end
end



[~,pval] = fdr_bh([pfix psample pdelay],.001);


sig_fix = size(find(pfix < pval),2);
sig_sample = size(find(psample < pval),2);
sig_delay = size(find(pdelay < pval),2);

% sig_fix = sum(fdr_bh(pfix,.001));
% sig_sample = sum(fdr_bh(psample,.001));
% sig_delay = sum(fdr_bh(pdelay,.001));


% 
% int = size(intersect(find(allp_fix < .00001),intersect(find(allp_delay < .00001),find(allp_sample < .00001))),2)
% 
% bar([sig_fix sig_sample sig_delay])
