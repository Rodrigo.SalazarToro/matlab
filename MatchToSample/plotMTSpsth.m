function [bins,spikefr,A,ymin,ymax,reference,varargout] = plotMTSpsth(mts,sp,varargin)




Args = struct('plot',0,'bin',5,'raster',0,'ylim',[],'psthaxis',[],'color','b','alignement','CueOnset','sortFeature','MatchOnset','axis',0,'maxt',3000,'checkForStableUnit',0,'Match',[]);
Args.flags = {'plot','raster','axis','checkForStableUnit'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'plot','raster','axis'});

sdir = pwd;
switch Args.alignement
    
    case 'CueOnset'
        
        reference = -500;
    case 'CueOffset'
        reference = -1000;
        
    case 'MatchOnset'
        reference = -1800;
    case 'FirstSac'
        reference = -1900;
        
    case 'LastSac'
        reference = -2000;
end
if isempty(Args.Match)
ind = mtsgetTrials(mts,varargin2{:});
else
    [allmatches,matches] = getMatches(mts);
    ind = [];
    for mm =1 : size(matches,2)
        ind = [ind mtsgetTrials(mts,'MatchObj1',allmatches(Args.Match,1),'MatchObj2',allmatches(Args.Match,2),'MatchPos1',allmatches(Args.Match,3),'MatchPos2',allmatches(Args.Match,4),varargin2{:})];
    end
    ind = unique(ind);
end
if Args.checkForStableUnit
    strials = stableSpikes(sp,varargin2{:});
else
    strials = ind;
end

% [perf,ti,crossperf] = getperformance('getThreshCross');
%
%
% if Args.stable
%     seltrials = ind(ind >= crossperf(1));
% elseif Args.transition
%     seltrials = ind(ind < crossperf(2));
% else
%     seltrials = ind;
% end

trials = intersect(ind,strials);


[B,tind] = eval(sprintf('sort(mts.data.%s(trials))',Args.sortFeature));
sind = trials(tind);
trials = sind;

if ~isempty(trials)
    spiketimes = [];
    for t = 1 : length(trials)
        tr = trials(t);
        if strcmp(Args.alignement,'FirstSac') | strcmp(Args.alignement,'LastSac')
            stimes = sp.data.trial(tr).cluster.spikes - eval(sprintf('mts.data.%s(tr)',Args.alignement)) - mts.data.MatchOnset(tr);
        else
            stimes = sp.data.trial(tr).cluster.spikes - eval(sprintf('mts.data.%s(tr)',Args.alignement));
        end
        spiketimes = [spiketimes stimes];
        fr(t) = sp.data.trial(tr).cluster.spikecount / ((mts.data.MatchOnset(tr) + mts.data.FirstSac(tr)) /1000);
        spikes(t).times = stimes;
        A(t,:) = histcie(stimes,[reference : (Args.maxt + reference) ]);
    end
    
    if Args.raster & Args.plot
        
        imagesc(A == 0)
        colormap(gray)
        
        hax = gca;
        set(hax,'Xtick',[1 : abs(reference) : size(A,2)]);
        set(hax,'XTickLabel',[reference : abs(reference) : size(A,2)+reference]);
        
    else
        bins = [reference : Args.bin : (Args.maxt+ reference) ];
        spikecount = histcie(spiketimes,bins);
        
        spikefr = ((spikecount/length(trials)) * (1000/Args.bin))';
        if unique(isnan(spikefr) | (spikefr == 0))
            maxi2 = 1;
        else
            maxi2 = max(spikefr);
        end
        ymax = ceil(maxi2);
        if isnan(unique(spikefr)); ymin = 0; else  ymin = floor(min(spikefr));end
        %     axes('position',[.11 .62 .8 .31])
        
        if Args.plot;
            plot(bins,spikefr,Args.color);
            if length(bins) > 1
                axis([bins(1) bins(end) ymin ymax])
            end
            if Args.axis
                ylabel('Firing rate [spike/s]')
            end
            
            hold on
            line([0 0],[0 max(spikefr)],'Color','r')
            hold off
            if ~isempty(Args.psthaxis)
                axis(Args.psthaxis)
            end
        end
        
        if Args.axis
            %     title(obj.data.setNames(limit))
            xlabel('Time [ms]')
        end
    end
else
    ymin = 0;
    ymax = 1;
    bins= 0;
    spikefr = 0;
    A = 0;
    reference = 0;
    spikes = [];
    fr = [];
end
varargout{1} = spikes;
varargout{2} = trials;
varargout{3} = fr;
cd(sdir)
