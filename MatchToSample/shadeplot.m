function shadeplot(varargin)

%makes shadded error bars around the mean

Args = struct('bins',[],'std',1,'shade','k','line','r','alpha',.1,'x',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);


cmean = nanmean(Args.bins);
cmean(find(isnan(cmean))) = 0;

cstd = nanstd(Args.bins);
cstd(find(isnan(cstd))) = 0;

upper = cmean + (Args.std * cstd);
lower = cmean - (Args.std * cstd);

if isempty(Args.x)
x = [1 : size(cmean,2)];
else
    x = Args.x;
end

xcoords = [x x(end:-1:1)];
ycoords = [upper lower(end:-1:1)];

pa = patch(xcoords,ycoords,Args.shade);
set(pa,'linestyle','none','linewidth',2)
alpha(pa,Args.alpha)
hold on
plot(x,cmean,'color',Args.line,'linewidth',3)
hold on