function [r,varargout] = get(obj,varargin)
% Index(:,1) = pair number
% Index(:,2) = day number
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% all of the above is either 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules

% Index(:,7) = beta differences between the two rules (0 for no, 1 for delay 1, 2 for delay 2 and 3 for both)
% Index(:,8) = gamma differences between the two rules (0 for no, 1 for
% fix, 2 for delay 2 and 3 for both)
% Index(:,9) = histology crude (1: PPCm-PFCd; 2: PPCm-PFCv; 3 PPCl-PFCd;
% 4 : PPCl-PFCv; 5: PPCs-PFCd; 6: PPCs-PFCv; 7: PPCm-PFCs; 8: PPCl-PFCs)
%
% Index(:,10:11) = group number
% Index(:,12) = IDENTITY rule, no tuning (0), tuning for the
% identity (1) or the location (2) or (3) both location and idenities tuning of the sample only for the beta
% range of course
% Index(:,13) = LOCATION rule, no tuning (0), tuning for the
% identity (1) or the location (2) of the sample only for the beta
% range of course
% Index(:,14) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 3
% Index(:,15) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 4
% Idex(:,16) ide tuning index during IDE rule for window 4 PP
% Idex(:,17) ide tuning index during LOC rule for window 4 PP
% Idex(:,18) loc tuning index during IDE rule for window 4 PP
% Idex(:,19) loc tuning index during LOC rule for window 4 PP
% Idex(:,20) ide tuning index during IDE rule for window 4 PF
% Idex(:,21) ide tuning index during LOC rule for window 4 PF
% Idex(:,22) loc tuning index during IDE rule for window 4 PF
% Idex(:,23) loc tuning index during LOC rule for window 4 PF
% Idex(:,24) snr for parietal channel
% Idex(:,25) snr for frontal channel
% Index(:,26) alpha peak 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules
% index(:,27) DC 0 or 1
% Index(:,28) histology middle
% Index(:,29) sorted
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'betaIncr',[],'betaDecr',[],'gammaIncr',[],'gammaDecr',[],'betaRuleEffect',[],'gammaRuleEffect',[],'pairhist',[],'rudeHist',0,'IDEruleTuning',[],'LOCruleTuning',[],'snr',99,'alphaPeak',[],'DC',[],'removeGr',[],'type',[],'sigSur',[],'sigSurPIntersect',0,'tuning',[],'tuningPIntersect',0,'hist',[],'phaseRange',[],'delay',[]);
Args.flags = {'Number','ObjectLevel','rudeHist','sigSurPIntersect','tuningPIntersect'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    cdays = {'060328'
        '060404'
        '060406'
        '060407'
        '060411'
        '060412'
        '060414'
        '060426'
        '060427'
        '060428'
        '060502'
        '060503'
        '060509'
        '060511'
        '060531'
        '060601'
        '060602'
        '060824'
        '060825'
        '060829'
        '060831'
        '060907'
        '061123'
        '061212'
        '061213'
        '061214'
        '061215'
        '061220'
        '061221'};
    bdays = {'090615'
        '090616'
        '090617'
        '090618'
        '090622'
        '090623'
        '090625'
        '090626'
        '090629'
        '090630'
        '090701'
        '090702'
        '090706'
        '090708'
        '090709'
        '090901'
        '090903'
        '090916'
        '090917'
        '090918'
        '090921'
        '090923'
        '090924'
        '090928'
        '090929'
        '090930'
        '091001'};
    bdaysL = { '091002'
        '100108'
        '100112'
        '100114'
        '100115'
        '100119'
        '100122'
        '100125'
        '100127'
        '100128'
        '100129'
        '100201'
        '100203'
        '100204'
        '100205'
        '100208'
        '100209'
        '100212'
        '100216'
        '100217'
        '100218'}; % days 1: 12 = continous;
    if ~isempty(Args.delay)
        [b, m, n]=unique(obj.data.setNames(:));
        [B,IX]=sort(m);
        odays = b(IX);
        
        switch Args.delay
            case 'short'
                sdays = [bdays; cdays];
            case 'cont'
                sdays = bdaysL(1:12);
            case 'bin'
                sdays = bdaysL(13:end);
            case 'long'
                 sdays = bdaysL(1:end);
                
        end
        nday = [];for d = 1 : size(odays,1); if sum(strcmp(odays{d}(end-5:end),sdays)) ~=0; nday = [nday d]; end; end
        dtemp = find(ismember(obj.data.Index(:,2),nday) == 1);
    else
        dtemp = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.betaIncr)
        rtemp1 = [];
        for nf = 1 : length(Args.betaIncr)
            
            rtemp1 = [rtemp1; find(obj.data.Index(:,3) == Args.betaIncr(nf))];
        end
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.betaDecr)
        rtemp2 = [];
        for nf = 1 : length(Args.betaDecr)
            rtemp2 = [rtemp2; find(obj.data.Index(:,4) == Args.betaDecr(nf))];
        end
    else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaIncr)
        rtemp3 = [];
        for nf = 1 : length(Args.gammaIncr)
            rtemp3 = [rtemp3; find(obj.data.Index(:,5) == Args.gammaIncr(nf))];
        end
    else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaDecr)
        rtemp4 = [];
        for nf = 1 : length(Args.gammaDecr)
            rtemp4 = [rtemp4; find(obj.data.Index(:,6) == Args.gammaDecr(nf))];
        end
    else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.betaRuleEffect)
        rtemp5 = [];
        for nf = 1 : length(Args.betaRuleEffect)
            rtemp5 = [rtemp5; find(obj.data.Index(:,7) == Args.betaRuleEffect(nf))];
        end
    else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.gammaRuleEffect)
        rtemp6 = [];
        for nf = 1 : length(Args.gammaRuleEffect)
            rtemp6 = [rtemp6; find(obj.data.Index(:,8) == Args.gammaRuleEffect(nf))];
        end
    else
        rtemp6 = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.pairhist)
        if Args.rudeHist
            switch Args.pairhist
                case 'md'
                    
                    code = 1;
                case 'mv'
                    code = 2;
                case 'ld'
                    code = 3;
                case 'lv'
                    code = 4;
                case 'sd'
                    code = 5;
                case 'sv'
                    code = 6;
                case 'ms'
                    code = 7;
                case 'ls'
                    code = 8;
                case 'other'
                    code = 99;
            end
        else
            switch Args.pairhist
                case '54'
                    code = 1;
                case '56'
                    code = 2;
                case '58'
                    code = 3;
                case 'l4'
                    code = 4;
                case 'l6'
                    code = 5;
                case 'l8'
                    code = 6;
                case 'm4'
                    code = 7;
                case 'm6'
                    code = 8;
                case 'm8'
                    code = 9;
                case '74'
                    code = 10;
                case '76'
                    code = 11;
                case '78'
                    code = 12;
                case 'other'
                    code = 99;
            end
        end
        if ~exist('code'); code = nan; end
        
        if Args.rudeHist; hCol = 9; else hCol = 28; end
        
        rtemp7 = find(obj.data.Index(:,hCol) == code);
    else
        rtemp7 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.hist)
        rtemph = [];
        for nf = 1 : size(Args.hist,1)
            for area = 1 : 2
                if isnan(Args.hist(nf,area))
                    row1{area} = [1 : size(obj.data.hist,1)];
                else
                    [row1{area},col] = find(obj.data.hist == Args.hist(nf,area));
                end
            end
            row = intersect(row1{1},row1{2});
            rtemph = [rtemph; row];
        end
    else
        rtemph = [1 : size(obj.data.Index,1)];
    end
    
    if ~isempty(Args.IDEruleTuning)
        rtemp8 = [];
        for nf = 1 : length(Args.IDEruleTuning)
            rtemp8 = [rtemp8; find(obj.data.Index(:,12) == Args.IDEruleTuning(nf))];
        end
    else
        rtemp8 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.LOCruleTuning)
        rtemp9 = [];
        for nf = 1 : length(Args.LOCruleTuning)
            rtemp9 = [rtemp9; find(obj.data.Index(:,13) == Args.LOCruleTuning(nf))];
        end
    else
        rtemp9 = [1 : size(obj.data.Index,1)];
    end
    
    ttem = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,intersect(rtemp7,intersect(rtemp8,intersect(rtemp9,intersect(dtemp,rtemph))))))))));
    %%SNR
    if ~isempty(Args.snr)
        % new code only temporary until we sort all the data
        if Args.snr == 99
            x = cell2mat(obj.data.setNames);
            uni = length(obj.data.setNames{1});
            %             indclark = floor(strfind(x,'clark') / uni) + 1; % previously
            %             but now sorted
            indclark = [];
            for d = 1 : 29
                indclark = [indclark floor(strfind(x,cdays{d}) / uni) + 1];
            end
            indclark = intersect(indclark,find(obj.data.Index(:,29) == 1));
            indbetty = [];
            for d = 1 : 27
                indbetty = [indbetty floor(strfind(x,bdays{d}) / uni) + 1];
            end
            indbetty = intersect(indbetty,find(obj.data.Index(:,29) == 1));
            indbettyL = [];
            for d = 1 : 21
                indbettyL = [indbettyL floor(strfind(x,bdaysL{d}) / uni) + 1];
            end
            indbettyL = intersect(indbettyL,(find(obj.data.Index(:,24) >= 1.8 & obj.data.Index(:,25) >= 1.8))); % previously but now sorted
             indbettyL = intersect(indbettyL,find(obj.data.Index(:,29) == 1));
            indsnr = [indclark indbetty indbettyL];
        else
            indsnr = find(obj.data.Index(:,24) >= Args.snr & obj.data.Index(:,25) >= Args.snr);
        end
    else
        indsnr = find(obj.data.Index(:,29) == 1); % ttem;
    end
    
    %%
    bind = intersect(ttem,indsnr);
    
    %     indAlpha = [];
    %     for nf = [1 3]
    %
    %         indAlpha = [indAlpha; find(obj.data.Index(:,26) == nf)];
    %     end
    if ~isempty(Args.alphaPeak)
        indAlpha = [];
        for nf = 1 : length(Args.alphaPeak)
            
            indAlpha = [indAlpha; find(obj.data.Index(:,26) == Args.alphaPeak(nf))];
        end
        indAlpha = intersect(bind,indAlpha);
        
    else
        %           indAlpha = setdiff(bind,indAlpha);
        indAlpha = bind;
    end
    
    
    
    if ~isempty(Args.DC)
        indDC = [];
        
        %         for nf = Args.DC
        %
        %             indDC = [indDC; find(obj.data.Index(:,27) == nf)];
        %         end
        for nf = 1 : length(Args.DC)
            
            indDC = [indDC; find(obj.data.Index(:,27) == Args.DC(nf))];
        end
        lastind = intersect(indDC,indAlpha);
    else
        %         lastind = setdiff(indAlpha,indDC);
        lastind = indAlpha;
    end
    if ~isempty(Args.removeGr)
        indGr = find(ismember(obj.data.Index(:,10),Args.removeGr) + ismember(obj.data.Index(:,11),Args.removeGr) ==1);
        lastind = setdiff(lastind,indGr);
    end
    
    if ~isempty(Args.type)
        news = lastind;
        for c = 1 : size(Args.type,1)
            tnews = [];
            for e =  1 : length(Args.type{c,2})
                eval(sprintf('tnews = [tnews; vecc(find(obj.data.%s == %d))];',Args.type{c,1},Args.type{c,2}(e)));
            end
            news = intersect(news,tnews);
        end
        lastind = news;
    end
    
    if ~isempty(Args.sigSur)
        news = lastind;
        for c = 1 : size(Args.sigSur,1)
            
            for p = 1: length(Args.sigSur{c,2})
                tnews2{p} = [];
                for e =  1 : length(Args.sigSur{c,3})
                    eval(sprintf('tnews2{p} = [tnews2{p}; vecc(find(obj.data.%s(:,%d) == %d))];',Args.sigSur{c,1},Args.sigSur{c,2}(p),Args.sigSur{c,3}(e)));
                end
                if p > 1; if Args.sigSurPIntersect; tnews2{1} = intersect(tnews2{1},tnews2{p}); else; tnews2{1} = union(tnews2{1},tnews2{p}); end;end
            end
            tnews = tnews2{1};
            news = intersect(news,tnews);
        end
        lastind = news;
    end
    
    if ~isempty(Args.tuning)
        news = lastind;
        for c = 1 : size(Args.tuning,1)
            tnews2 = cell(Args.tuning{c,2});
            for p = 1: length(Args.tuning{c,2})
                
                eval(sprintf('tnews2{p} = vecc(find(obj.data.%s(:,%d) %s %d));',Args.tuning{c,1},Args.tuning{c,2}(p),Args.tuning{c,3},Args.tuning{c,4}));
                if p > 1 && Args.tuningPIntersect; tnews2{1} = intersect(tnews2{1},tnews2{p}); else tnews2{1} = union(tnews2{1},tnews2{p}); end;
            end
            
            news = intersect(news,tnews2{1});
        end
        lastind = news;
    end
    
    if ~isempty(Args.phaseRange)
        news = lastind;
        for c = 1 : size(Args.phaseRange,1)
            tnews = [];
            
            for e =  1 : length(Args.phaseRange{c,3})
                np = Args.phaseRange{c,3}(e);
                [row,col] = eval(sprintf('find(obj.data.%sPhase(:,np) >= %d & obj.data.%sPhase(:,np) <= %d);',Args.phaseRange{c,1},Args.phaseRange{c,2}(1),Args.phaseRange{c,1},Args.phaseRange{2}(2)));
                tnews = [tnews; vecc(row)];
            end
            news = intersect(news,tnews);
        end
        lastind = news;
        
    end
    
    varargout{1} = lastind;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

