function obj = mtscohInter(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'Tuning',0,'noSur',0,'mainFile','cohInter','surFile','general');
Args.flags = {'Auto','Tuning','noSur'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtscohInter';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'cohInter';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)


datafile = nptDir(sprintf('%s.mat',Args.mainFile));

if length(Args.mainFile) > 8
    compfile = nptDir(sprintf('%sPcomp%s.mat',Args.mainFile(1:8),Args.mainFile(9:end)));
else
    compfile = nptDir(sprintf('%sPcomp.mat',Args.mainFile));
end

if ~Args.Tuning
    
    idefile = 0; % from the compareCohRule.m
    locfile = 0;
    rulefile = 0;
else
    % from the compareCohRule.m
    idefile = nptDir('ideTuning11.mat'); % from the compareCohRule.m
    locfile = nptDir('locTuning11.mat');
    rulefile = nptDir('allCuesComb11.mat');
end

if Args.noSur
    surfile = 0;
elseif ~isempty(Args.surFile)
     surfile = nptDir(sprintf('%sSur.mat',Args.surFile));
else
    surfile = nptDir(sprintf('%sSur.mat',Args.mainFile));
    
end


if  (isempty(datafile) + isempty(surfile) + isempty(compfile) + isempty(rulefile) + isempty(idefile) + isempty(locfile)) == 0
    
    data = getIndexCohInter(datafile,surfile,compfile,rulefile,idefile,locfile,varargin{:});
    if ~isempty(data)
        data.setNames = repmat({pwd},1,size(data.Index,1));
        
        data.numSets = size(data.Index,1); % nbr of trial
        data.surfile = Args.surFile;
        data.mainfile = Args.mainFile;
        % create nptdata so we can inherit from it
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        obj = createEmptyObject(Args);
    end
    
else
    % create empty object
    fprintf('*mat files missing \n');
    obj = createEmptyObject(Args);
    
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];
data = struct('betapos34',[],...
    'betapos14',[],...
    'betapos12',[],...
    'betapos23',[],...
    'betaneg34',[],...
    'betaneg14',[],...
    'betaneg12',[],...
    'betaneg23',[],...
    'gammapos34',[],...
    'gammapos14',[],...
    'gammapos12',[],...
    'gammapos23',[],...
    'gammaneg34',[],...
    'gammaneg14',[],...
    'gammaneg12',[],...
    'gammaneg23',[],...
    'alphapos34',[],...
    'alphapos14',[],...
    'alphapos12',[],...
    'alphapos23',[],...
    'alphaneg34',[],...
    'alphaneg14',[],...
    'alphaneg12',[],...
    'alphaneg23',[],...
    'betamodIDE',[],...
    'betamodLOC',[],...
    'gammamodIDE',[],...
    'gammamodLOC',[]);


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
