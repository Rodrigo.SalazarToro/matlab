function obj = plot(obj,varargin)


Args = struct('mvAvg',[],'sigLevel',4,'phase',0,'stdfold',1,'caxis',[0 0.25],'plotType','default','power',[],'frange',[7 50],'gram',[],'maxTimeScale',0,'minTrials',100,'matchAlign',0,'ptune',99.9,'sortedStim',4);
Args.flags = {'phase','maxTimeScale','matchAlign'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'raster','cues9'});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(dataindices)
    if ~isempty(Args.NumericArguments)
        
        n = Args.NumericArguments{1}; % to work oon
        ind = dataindices(n);
        
    else
        
    end
    cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];
    wind = {'pre-sample' 'sample' 'delay1' 'delay2'};
    ymax = [];
    ymin = [];
    MLrules = {'IDE' 'LOC'};
    tunel = {'-' '-.'};
    lrules = {'r' 'k'};
    align = {'Sample off-set aligned' 'Match aligned'};
    fname = {'Correct' 'Incorrect' 'Fixation'};
    tuningN = {'loc tuning' 'ide tuning'};
    area = {'Parietal' 'Frontal'};
    tuning = {'Loc' 'Obj'};
    tuning2 = {'loc' 'obj'};
    stims = {'unpreferred' 'intermediate' 'preferred'};
    cmb = obj.data.Index(ind,1);
    pairg = obj.data.Index(ind,10:11);
    groupsN = obj.data.Index(ind,10:11);
    areasN = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
    if obj.data.hist(ind,1) ~= 99
        area1 = areasN{obj.data.hist(ind,1)};
    else
        area1 = '??';
    end
    
    if obj.data.hist(ind,2) ~= 99
        area2 = areasN{obj.data.hist(ind,2)};
    else
        area2 = '??';
    end
    info = sprintf('%s pair # %d; groups # %d %s and %d %s ',obj.data.setNames{ind},cmb,groupsN(1),area1,groupsN(2),area2);
    fprintf('%s \n',info)
    h = gcf;
    set(h,'Name',info)
    cd(obj.data.setNames{ind})
    try
        load(lower(sprintf('%s.mat',obj.data.mainfile)));
    catch
        load(sprintf('%s.mat',obj.data.mainfile));
    end
    
    for s = 1 : size(Day.session,2); if isfield(Day.session(s),'trials'); ntrials(s) = length(Day.session(s).trials); else; ntrials(s) = nan; end; end
    surfile = nptDir(lower([obj.data.surfile 'Sur.mat']));
    frange = find(f >= Args.frange(1) & f <= Args.frange(2));
    if ~isempty(surfile)
        
        sur = load(surfile.name);
    end
    %%
    
    switch Args.plotType
        case 'cues'
            cd tuning
            for r = 1: 2
                filename1 = sprintf('iCueLocg%04.0fg%04.0fRule%d.mat',groupsN(1),groupsN(2),r);
                filename2 = sprintf('iCueObjg%04.0fg%04.0fRule%d.mat',groupsN(1),groupsN(2),r);
                if ~isempty(nptDir(filename1)) && ~isempty(nptDir(filename2))
                    loc = load(filename1,'groupN','f');
                    ide = load(filename2,'groupN');
                    f = loc.f;
                    for p = 1 : 4
                        locC(:,:,p,r) = loc.groupN(p).C;
                        objC(:,:,p,r) = ide.groupN(p).C;
                        Locsigdif{p,r} = find(filtfilt(repmat(1/2,1,2),1,loc.groupN(p).rdiff) >= filtfilt(repmat(1/2,1,2),1,loc.groupN(p).pvalues(:,1)));
                        Objsigdif{p,r} = find(filtfilt(repmat(1/2,1,2),1,ide.groupN(p).rdiff) >= filtfilt(repmat(1/2,1,2),1,ide.groupN(p).pvalues(:,1)));
                    end
                    clear loc ide
                end
            end
            maxc = [max(max(max(max(locC(frange,:))))); max(max(max(max(objC(frange,:)))))];
            
            for r = 1 : size(locC,4)
                if ~isempty(Args.sortedStim)
                    orderObj = eval(sprintf('obj.data.%sObjSeq(ind,%d,:)',MLrules{r},Args.sortedStim));
                    orderLoc = eval(sprintf('obj.data.%sLocSeq(ind,%d,:)',MLrules{r},Args.sortedStim));
                else
                    orderObj = [1 : 3];
                    orderLoc = [1 : 3];
                end
                for p = 1 : 4
                    subplot(size(locC,4)*2,4,(r-1)*8 + p)
                    plot(f,squeeze(locC(:,orderLoc,p,r)),'--')
                    hold on
                    plot(f(Locsigdif{p,r}),zeros(1,length(Locsigdif{p,r})),'x')
                    ylim([ 0 max(maxc)])
                    xlim(Args.frange)
                    set(gca,'TickDir','out');
                    if r == 1; title(wind{p}); end
                    if p == 1; ylabel(sprintf('%s tuning %s',MLrules{r},tuning{1})); end
                    
                    
                    subplot(size(locC,4)*2,4,(r-1)*8 + p+4)
                    plot(f,squeeze(objC(:,orderObj,p,r)))
                    hold on
                    plot(f(Objsigdif{p,r}),zeros(1,length(Objsigdif{p,r})),'x')
                    set(gca,'TickDir','out');
                    ylim([ 0 max(maxc)])
                    xlim(Args.frange)
                    
                    if p == 1; ylabel(sprintf('%s tuning %s',MLrules{r},tuning{2})); end
                    
                    
                end
            end
            cd ..
            xlabel('Frequency [Hz]'); ylabel('coherence');
            legend('unpreferred stim','intermediate stim','preferred stim')
            
            %             legs(1).cues = {'ide1' 'ide2' 'ide3'};
            %             legs(2).cues = {'loc1' 'loc2' 'loc3'};
            %             tune(1).data = load('ideTuning11.mat');
            %             tune(2).data = load('locTuning11.mat');
            %
            %             for s = 1 : 2; rules(s) = Day.session(s).rule; end
            %             f = tune(1).data.f;
            %             for rt = 1 : 2
            %
            %                 if ~isempty(tune(1).data) || ~isempty(tune(2).data)
            %                     for p = 1: 4
            %                         rdata{1} = squeeze(tune(rt).data.C1(:,cmb,p,:));
            %                         rdata{2} = squeeze(tune(rt).data.C2(:,cmb,p,:));
            %                         for r = 1 : 2
            %                             data = rdata{r};
            %                             subplot(4,4,(r-1)* 8 +((rt-1)*4)+p)
            %
            %                             if ~isempty(Args.mvAvg)
            %                                 ff = repmat(1/Args.mvAvg,1,Args.mvAvg);
            %                                 data = filtfilt(ff,1,data);
            %                                 %                             plot(f,fdata)
            %                                 %                             hold on
            %                             end
            %                             plot(f,data);
            %                             ymax = [ymax max(max(data))];
            %                             ymin = [ymin min(min(data))];
            %                             hold on;
            %                             if ~isempty(surfile)
            %                                 if isempty(Args.mvAvg)
            %                                     thesur = sur.Day.session(find(rules == r)).Prob(:,Args.sigLevel,cmb,p);
            %                                 else
            %                                     thesur = filtfilt(ff,1,sur.Day.session(find(rules == r)).Prob(:,Args.sigLevel,cmb,p));
            %                                 end
            %                                 plot(f,thesur,'--');
            %                             end
            %                             ylabel(wind{p})
            %                             if p == 4;subplot(4,4,(r-1)* 8 + (rt-1)*4+p); title(MLrules{r}); legend(legs(rt).cues{:});end
            %                         end
            %                     end
            %                     xlabel('Frequency [Hz]')
            %                     %         if unique(mtst.data.Index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'IDENTITY'); elseif length(unique(mtst.data.Index(:,1)) == 1) > 1; rule = MLrules{s};else; rule = sprintf('session%d = %s',s+1,'LOCATION'); end
            %
            %                 end
            %             end
            %             for sb = 1 : 16; subplot(4,4,sb); axis([Args.frange min(ymin) max(ymax)]); set(gca,'TickDir','out');end
            %%
        case 'cues9'
            
            legs(1).cues = {'ide1' 'ide2' 'ide3'};
            legs(2).cues = {'loc1' 'loc2' 'loc3'};
            
            try
                tune(1).data = load('allCuesTuning11.mat');
                for s = 1 : 2; rules(s) = Day.session(s).rule; end
                
                f = tune(1).data.f;
            catch
                tune(1).data = [];
            end
            
            
            
            if ~isempty(tune(1).data)
                for p = 1: 4
                    rdata{1} = squeeze(tune(1).data.C1(:,cmb,p,:));
                    rdata{2} = squeeze(tune(1).data.C2(:,cmb,p,:));
                    
                    for stim = 1 : 9
                        for r = 1 : 2
                            data = rdata{r}(:,stim);
                            subplot(9,4,(stim -1) * 4+p)
                            
                            if ~isempty(Args.mvAvg)
                                ff = repmat(1/Args.mvAvg,1,Args.mvAvg);
                                data = filtfilt(ff,1,data);
                                %                             plot(f,fdata)
                                %                             hold on
                            end
                            plot(f,data,lrules{r});
                            ymax = [ymax max(max(data))];
                            ymin = [ymin min(min(data))];
                            hold on;
                            if ~isempty(surfile)
                                if isempty(Args.mvAvg)
                                    thesur = sur.Day.session(find(rules == r)).Prob(:,Args.sigLevel,1,p);
                                else
                                    thesur = filtfilt(ff,1,sur.Day.session(find(rules == r)).Prob(:,Args.sigLevel,1,p));
                                end
                            end
                            plot(f,thesur,'--');
                        end
                        if p == 1; ylabel(sprintf('%s; %s',legs(1).cues{cueComb(2,stim)},legs(2).cues{cueComb(1,stim)})); end
                    end
                    
                    subplot(9,4,(stim -1) * 4+p); title(wind{p});
                end
                legend('IDE','LOC','SUR');
                ylabel('coherence')
                xlabel('Frequency [Hz]')
                %         if unique(mtst.data.Index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'IDENTITY'); elseif length(unique(mtst.data.Index(:,1)) == 1) > 1; rule = MLrules{s};else; rule = sprintf('session%d = %s',s+1,'LOCATION'); end
                for sb = 1 : 36; subplot(9,4,sb); axis([0 60 min(ymin) max(ymax)]); set(gca,'TickDir','out');end
            end
            
            
            %%
        case 'powerT'
            pp = load('powerpp.mat');
            pf = load('powerpf.mat');
            f = pp.f;
            dir = pwd;
            clark = findstr(dir,'clark');
            betty = findstr(dir,'betty');
            
            if ~isempty(clark)
                sessions = {'session02' 'session03'};
            elseif ~isempty(betty)
                sessions = {'session01' 'session01'};
            end
            ts = size(pp.Day.session,2);
            for s =1 : ts
                r(s) = pp.Day.session(s).rule;
                cd(sessions{s})
                mt = mtstrial('auto');
                sampleO = mt.data.CueObj(pp.Day.session(s).trials);
                sampleL = mt.data.CueLoc(pp.Day.session(s).trials);
                if ~isempty(sampleO) && ~isempty(sampleL)
                    switch Args.power
                        case 'locTuning'
                            ncues = 3;
                            cues = unique(sampleL);
                            selec = sampleL;
                            legs = {'loc1' 'loc2' 'loc3'};
                        case 'ideTuning'
                            ncues = 3;
                            cues = unique(sampleO);
                            selec = sampleO;
                            legs = {'ide1' 'ide2' 'ide3'};
                        case 'allCuesTuning'
                            ncues = 9;
                            selec = [sampleL sampleO];
                            cues = unique(selec,'rows');
                            legs = {'loc1;ide1' 'loc1;ide2' 'loc1;ide3' 'loc2;ide1' 'loc2;ide2' 'loc2;ide3' 'loc3;ide1' 'loc3;ide2' 'loc3;ide3'};
                        case 'allCuesComb'
                            ncues = 1;
                            cues = 1;
                            selec = ones(length(sampleL),1);
                            legs = {'allCuesCombined'};
                    end
                    
                    for c = 1 : ncues
                        for p = 1 : 4
                            subplot(ncues,4,(c-1)*4 + p)
                            st = find(cues(c) == selec);
                            frange = find(f >= Args.frange(1) & f <= Args.frange(2));
                            ppch = pp.Day.channels; %unique(Day.comb(:,1));
                            pfch = pf.Day.channels; %unique(Day.comb(:,2));
                            data(1,:) = mean(squeeze(pp.Day.session(s).S(:,st,find(Day.comb(cmb,1) == ppch),p)),2);
                            data(2,:) = mean(squeeze(pf.Day.session(s).S(:,st,find(Day.comb(cmb,2) == pfch),p)),2);
                            ymax = [ymax max(max(data(:,frange)))];
                            
                            for area = 1 : 2; plot(f,data(area,:),sprintf('%s%s',lrules{r(s)},tunel{area})); hold on;end
                            if c == 1; title(wind{p}); end
                            if p == 1; ylabel(legs{c}); end
                            
                        end
                        
                    end
                end
                for sb = 1 : ncues*4; subplot(ncues,4,sb); axis([Args.frange 0 max(ymax)]); set(gca,'TickDir','out');end
                cd ..
            end
            xlabel('Frequency [Hz]');
            ylabel('Power');
            if ts == 2
                legend(sprintf('PP:%s',MLrules{r(1)}),sprintf('PF:%s',MLrules{r(1)}),sprintf('PP:%s',MLrules{r(2)}),sprintf('PF:%s',MLrules{r(2)}))
            else
                legend(sprintf('PP:%s',MLrules{r(1)}),sprintf('PF:%s',MLrules{r(1)}))
            end
            %%
        case 'TimeFreq'
            cd grams
            ref = [1 1.8];
            
            maxc = [];
            for r = 1 : 1
                switch Args.gram
                    case {'coh','phi'}
                        file{2} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%dIncor.mat',pairg(1),pairg(2),r));
                        
                        file{3} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%dFix.mat',pairg(1),pairg(2),r));
                        if ~isempty(file{3}); n = 3; else n = 2; end
                        file{1} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),r));
                    case {'tunePow','tuneCoh'}
                        file{1} = nptDir(sprintf('tunegramg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),r));
                        n = 1;
                    case 'migram'
                        file{1} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dLoc.mat',pairg(1),pairg(2),r));
                        for fff = 1 : 3;
                            file{1+fff} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dIdePerLoc%d.mat',pairg(1),pairg(2),r,fff));
                            file{4+fff} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dIdePerLoc%dIncor.mat',pairg(1),pairg(2),r,fff));
                            file{7+fff} = nptDir(sprintf('migramSurGenInterRule%dIdePerLoc%d.mat',r,fff));
                        end;
                        file{11} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dFix.mat',pairg(1),pairg(2),r));
                        file{12} = nptDir(sprintf('migramSurGenInterRule%dLoc.mat',r));
                        n = 12;
                        nmax = sum(~cellfun(@isempty,file));
                    case 'S12'
                        file{1} = nptDir(sprintf('spectrogramgroup%04.0fRule%d.mat',pairg(1),r));
                        file{2} = nptDir(sprintf('spectrogramgroup%04.0fRule%d.mat',pairg(2),r));
                        n = 2;
                end
                maxtime = cell(2,1);
                count = 1;
                for fi = 1 : n
                    if ~isempty(file{fi})
                        switch Args.gram
                            case 'coh'
                                load(file{fi}.name);
                                thedata = C;
                                lim = Args.caxis;
                            case 'phi'
                                load(file{fi}.name);
                                thedata = phi;
                                lim = [-pi pi];
                            case 'tuneCoh'
                                load(file{fi}.name,'tuneCoh','f','variable');
                                thedata = tuneCoh;
                                lim = [0 0.5];
                                ch = 1;
                            case 'tunePow'
                                load(file{fi}.name,'tunePow','f','variable');
                                thedata = tunePow;
                                lim = [0 0.5];
                                ch = 2;
                            case 'migram'
                                load(file{fi}.name,'trials','f','time','mutualgram');
                                if length(size(mutualgram)) == 2
                                    thedata = mutualgram;
                                else
                                    thedata = squeeze(prctile(mutualgram,98,1));
                                end
                            case 'S12'
                                load(file{fi}.name,'S','f','t');
                                for al = 1 : 2;
                                    %                                     thedata{al} = squeeze(mean(S{al},3));%./ squeeze(std(S{al},[],3));
                                    %                                      thedata{al} = squeeze(std(S{al},[],3));
                                    thedata{al} = squeeze(mean(S{al},3)).* repmat(f,size(S{al},1),1);
                                end
                                %                                 lim = [0 1];
                                
                                ch = 2;
                                trials = [1: 101]; % fake number of trials for now
                                time = t; clear t
                        end
                        
                        if ~isempty(thedata)
                            if ~isempty(findstr(Args.gram,'tune'))
                                for tune = 1 : 2
                                    for c = 1 : ch
                                        subplot(ch * 2,2,(r-1)*2+tune+(c-1) * 4)
                                        
                                        switch Args.gram
                                            case 'tunePow'
                                                pdata =   squeeze(thedata(tune).T(c,:,:));
                                                %                                 imagesc(variable(tune).t{1}(1:size(thedata(tune).T,ch)),f,squeeze(thedata(tune).T(c,:,:))',[lim(1) lim(2)])
                                            case 'tuneCoh'
                                                pdata = thedata(tune).T(:,:);
                                                
                                        end
                                        imagesc(variable(tune).t{1}(1:size(thedata(tune).T,ch)),f,pdata',[lim(1) lim(2)])
                                        maxc = [maxc max(max(pdata))];
                                        if r == 1; title(tuningN{tune}); end
                                        if tune == 1; ylabel(sprintf('%s; %s',MLrules{r},area{c})); end
                                        ylim(Args.frange)
                                        colorbar
                                        set(gca,'TickDir','out');
                                    end
                                end
                            elseif strcmp('migram',Args.gram)
                                
                                subplot(nmax,2,(count-1)*2+1)
                                
                                imagesc(time,f,thedata',[0 1.5])
                                title(file{fi}.name)
                                xlim([0.1 2])
                            elseif strcmp('S12',Args.gram)
                                for al = 1 : 2
                                    subplot(2,2,(fi-1)*2+al)
                                    imagesc(time{al},f,thedata{al}')
                                    title(file{fi}.name)
                                    ylim(Args.frange)
                                    set(gca,'TickDir','out')
                                end
                                
                            else
                                for cond = 1 : 2
                                    if length(trials) > Args.minTrials
                                        maxtime{cond} = [maxtime{cond}; [min(t{cond}) max(t{cond})]];
                                        subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi)
                                        imagesc(t{cond},f,thedata{cond}',[lim(1) lim(2)])
                                        flim = find(f >Args.frange(1) & f < Args.frange(2));
                                        if max(max(thedata{cond}(:,flim))) > 0.4; amaxc = 0.4; else amaxc = max(max(thedata{cond}(:,flim)));end
                                        maxc = [maxc amaxc];
                                        xlim([0.2 1.8])
                                        colorbar
                                        hold on
                                        line([ref(cond) ref(cond)],Args.frange,'Color','r')
                                        ylim(Args.frange)
                                        if cond == 1; title(sprintf('%s; %s; n=%d',fname{fi},align{cond},length(trials)));else title(align{cond}); end
                                        if fi == 1; ylabel(MLrules{r}); end
                                        set(gca,'TickDir','out');
                                    end
                                end
                            end
                            
                        end
                        count = count+1;
                    end
                end
                if strcmp('migram',Args.gram)
                elseif strcmp('S12',Args.gram)
                    ylabel('Frequency (Hz)')
                    xlabel('Time (s)')
                else
                    if ~Args.maxTimeScale && (~isempty(maxtime{1}) || ~isempty(maxtime{2})); for r = 1 : 2; for fi = 1 : n;  for cond = 1 : 2;  subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi);  xlim([0.2 1.8]); end; end; end; end
                    if isempty(findstr(Args.gram,'tune')); for r = 1 : 2; for fi = 1 : n;  for cond = 1 : 2;  subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi); caxis([lim(1) lim(2)]); end; end; end;end % max(maxc)
                end
            end
            
            xlabel('Time [sec]')
            ylabel('Frequency [Hz]')
            cd ..
            
        case 'longDelays'
            cd grams
            ref = [1 1.8];
            align = {'Sample off-set aligned' 'Match aligned'};
            titD = {'delay 1 sec or 1-1.6 sec' 'delay 2 sec or 1.6-2.3 sec' 'delay 1 sec or 2.3-3 sec'};
            maxc = [];
            
            for nd = 1 : 3; load(sprintf('cohgramg%04.0fg%04.0fRule1%d.mat',pairg(1),pairg(2),nd)); maxc = [maxc max(max(C{1}(:,frange)))]; end
            for nd = 1 : 3
                load(sprintf('cohgramg%04.0fg%04.0fRule1%d.mat',pairg(1),pairg(2),nd));
                if exist('beforeMatch') && ~isnan(beforeMatch);
                    ref = [1 beforeMatch/1000];
                else
                    ref = [1 1.8];
                end
                for al = 1 : 2
                    subplot(5,2,(nd-1)*2+al)
                    
                    imagesc(t{al},f,C{al}',[0 max(maxc)])
                    line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
                    if al == 1
                        title(titD{nd})
                    else
                        title('Match aligned')
                    end
                    ylim(Args.frange)
                    
                end
            end
            
            fix = load(sprintf('cohgramg%04.0fg%04.0fFix.mat',pairg(1),pairg(2)));
            fixS = load(sprintf('cohgramg%04.0fg%04.0fFixS.mat',pairg(1),pairg(2)));
            for al = 1 : 2
                subplot(5,2,6+al)
                
                imagesc(fix.t{al},f,fix.C{al}',[0 max(maxc)])
                line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
                title('Fix inter')
                ylim(Args.frange)
                
                subplot(5,2,8+al)
                
                imagesc(fixS.t{al},f,fixS.C{al}',[0 max(maxc)])
                line([ref(al) ref(al)],[f(1) f(end)],'Color','r')
                title('Fix Ses')
                ylim(Args.frange)
            end
            xlabel('Time [sec]')
            ylabel('Frequency [hz]')
            
        case 'StimGrams'
            load cohInter.mat
            ns = size(Day.session,2);
            sr = []; for s = 1 : ns; if ~isempty(Day.session(s).trials); sr =  [sr Day.session(s).rule]; end; end
            nr = length(sr);
            %             for r = 1 : nr
            %                 perf{r} = load(sprintf('samplePerf%d.mat',r));
            %
            %             end
            cd grams
            maxc = [];
            
            for r = sr
                for tune = 1 : 2
                    stimOrder = eval(sprintf('obj.data.%s%sSeq(ind,%d,:)',MLrules{r},tuning{tune},Args.sortedStim));
                    count = 1;
                    for stim = stimOrder
                        sur = load(sprintf('cohgramgeneralizedRule%d%s%d.mat',r,tuning{tune},stim),'thres');
                        if Args.matchAlign
                            if ~exist('beforeMatch')
                                beforeMatch= 1800;
                                
                            end
                            align = beforeMatch;
                            setA = 2;
                        else
                            align = [500 1000];
                            setA = 1;
                        end
                        load(sprintf('cohgramg%04.0fg%04.0fRule%d%s%d.mat',pairg(1),pairg(2),r,tuning{tune},stim));
                        
                        fakeSur{r,tune,stim} =  (C{setA}(1:33,:) > squeeze(sur.thres(:,1:33,1)));
                        subplot(2*nr,4,4*(tune-1)  + (r-1)*8 + count)
                        maxc = [maxc max(max(C{setA}(:,frange)))];
                        imagesc(t{setA},f,C{setA}')
                        hold on
                        
                        %
                        for l = 1 : length(align); line([align(l)/1000 align(l)/1000],Args.frange,'Color','r'); end
                        line([0.5 0.5],Args.frange,'Color','r')
                        line([1 1],Args.frange,'Color','r')
                        ylim(Args.frange)
                        %                         xlabel(sprintf('%d trials; %2.0f CR',length(trials),100*eval(sprintf('perf{r}.%s(stim)',tuning2{tune}))))
                        if tune == 1 && r == 1; title(stims{count}); end
                        if count ==1; ylabel(sprintf('%s; tuning %s',MLrules{r},tuning{tune})); end
                        count = count + 1;
                    end
                end
            end
            for r = 1 : nr;for sb = [1:3 5:7]; subplot(2*nr,4,sb+(r-1)*8); caxis([0.15 max(maxc)]); end; end
            maxc = [];
            for r = 1 : nr
                load(sprintf('tunegramg%04.0fg%04.0fRule%d%s%d.mat',pairg(1),pairg(2),r));
                if exist('beforeMatch')
                    beforeMatch= 1800;
                end
                
                for tune = 1 : 2
                    subplot(2*nr,4,4*(tune-1)  + (r-1)*8  + 4)
                    tlim = find(t{1} <= 0.5);
                    chanceT = reshape(tuneCoh(tune).T(tlim,:),1,length(tlim)* size(C{1},2));
                    thres = prctile(chanceT,Args.ptune);
                    mintime = min([size(fakeSur{r,tune,1},1) size(fakeSur{r,tune,2},1) size(fakeSur{r,tune,3},1)]);
                    minfakeSur = fakeSur{r,tune,1}(1:mintime,:) + fakeSur{r,tune,2}(1:mintime,:) + fakeSur{r,tune,3}(1:mintime,:);
                    
                    %                     sigTune = (minfakeSur ~= 0) .* tuneCoh(tune).T';
                    sigTune = tuneCoh(tune).T .* (tuneCoh(tune).T >= thres) .*(minfakeSur ~= 0);
                    imagesc(variable(tune).t{1}(1:size(tuneCoh(tune).T,1)),f,sigTune')
                    
                    maxc = [maxc max(max(tuneCoh(tune).T(:,frange)))];
                    hold on
                    %                     line([beforeMatch/1000 beforeMatch/1000],Args.frange,'Color','r')
                    for l = 1 : length(align); line([align(l)/1000 align(l)/1000],Args.frange,'Color','r'); end
                    line([1 1],Args.frange,'Color','r')
                    ylim(Args.frange)
                    if r == 1 && tune == 1; title('tunegram'); end
                end
            end
            for r = 1 : nr;for sb = [4 8]; subplot(2*nr,4,sb+(r-1)*8); caxis([0 max(maxc)]); end; end
            xlabel('Time [sec]')
            ylabel('Frequency [Hz]')
            cd ..
            
        case 'blocks'
            cd grams
            files = nptDir(sprintf('cohgramg%04.0fg%04.0fRule*block*.mat',pairg(1),pairg(2)));
            
            nfiles = length(files);
            blocks = zeros(nfiles,1);
            for ff = 1 : nfiles; blocks(ff) = str2double(files(ff).name(end-4)); end
            [~,norder] = sort(blocks);
            
            for ff = 1 : nfiles
                load(files(norder(ff)).name)
                sfile = load(sprintf('cohgramgeneralizedRule%sblock%s.mat',files(norder(ff)).name(22),files(norder(ff)).name(28)));
                for al = 1 : 1
                    subplot(nfiles,2,(ff-1)*2+al)
                    switch Args.gram
                        case 'coh'
                            % z-transform
                            
                            %                             zt = 1.5 * (sqrt(-(v-2) * log(1-abs(C)^2)) -1.5); % v=ntrials*ntapers
                            zt = (C{al} - squeeze(mean(sfile.C{al},1))) ./ squeeze(std(sfile.C{al},[],1));
                            imagesc(t{al},f,zt')
                            %                             imagesc(t{al},f,(C{al}./squeeze((diff(Cerr{al},1,1))/2))')
                            caxis(Args.caxis)
                        case 'phi'
                            imagesc(t{al},f,phi{al}',[-pi pi])
                    end
                    
                    
                    if al == 1; line([0.5 0.5],[0 50],'Color','r'); line([1 1],[ 0 50],'Color','r'); else line([1.8 1.8],[ 0 50],'Color','r'); title('Match aligned'); end
                    ylim(Args.frange)
                    title(files(norder(ff)).name)
                end
                
            end
            ylabel('Frequency (Hz)')
            xlabel('Time (ms)')
            
            %%
        case 'default'
            mainfile = lower(sprintf('%s.mat',obj.data.mainfile));
            surfile = lower(sprintf('%sSur.mat',obj.data.surfile));
            incorfile = lower(sprintf('%sInCor.mat',obj.data.mainfile));
            fixfile = lower(sprintf('%sfix.mat',obj.data.mainfile));
            DSfile = lower(sprintf('%sDS.mat',obj.data.mainfile));
            fixSesfile = lower(sprintf('%sfixS.mat',obj.data.mainfile));
            
            thefiles = {mainfile surfile incorfile fixfile DSfile fixSesfile};
            thelegs = {'correct' 'surrogate' 'incorrect' 'fixation' 'del. sac.' 'fix. session'};
            tco = {'b' '--' 'r' 'g' 'm' 'k'};
            count = 1;
            for ff = 1 : length(thefiles)
                pfile = nptDir(thefiles{ff},'CaseInsensitive');
                if ~isempty(pfile)
                    files{count} = pfile.name;
                    legs{count} = thelegs{ff};
                    colo{count} = tco{ff};
                    count = count + 1;
                end
            end
            %                     files{1} = nptDir(mainfile);
            %                     files{2} = nptDir(incorfile);if isempty(files{2}); files{2} = nptDir('cohIntercSInCor.mat'); end;
            %                     files{3} = nptDir(mainfile);
            %         if Args.DS; files{4} = nptDir('cohInterDS.mat');end
            %         if Args.FIX; files{5} = nptDir('cohInterfixSes.mat'); end
            try
                load(lower(sprintf('%s.mat',obj.data.mainfile)))
            catch
                load(sprintf('%s.mat',obj.data.mainfile))
            end
            ncomb = size(Day.comb,1);
            thecombs = Day.comb;
            thepair = Day.comb(cmb,:);
            countf = 1;
            for fi = 1 : length(files)
                
                load(files{fi})
                if fi == 2 && ncomb ~= size(Day.session(1).Prob,3)
                    
                    for s = 1 : length(Day.session); Day.session(s).Prob = repmat(Day.session(s).Prob,[1 1 ncomb 1]); end
                    apair = find(ismember(thecombs,thepair,'rows') ==1);
                else
                    apair = find(ismember(Day.comb,thepair,'rows') ==1);
                end
                
                %             switch files{fi}
                %                 case {'cohInterfix.mat' 'cohInterDS.mat' 'cohInterfixSes.mat'}
                if fi > 3
                    cs = [1 1];
                    %                 otherwise
                else
                    if ~exist('ns');
                        ns = size(Day.session,2);
                    end
                    tns = size(Day.session,2);
                    cs = [1 : tns];
                    
                    tns = size(Day.session,2);
                    if ~exist('ns')
                        
                        ns = tns;
                    end
                    cs = [1 : tns];
                    
                end
                
                for s = 1 : tns
                    if strcmp(lower(files{fi}),lower(surfile))
                        if Args.sigLevel > size(Day.session(s).Prob,2); Args.sigLevel = 1; end
                        if Args.phase; alldata = Day.session(s).phimean(:,:,:);
                        elseif ~isempty(Day.session(s).Prob); alldata = squeeze(Day.session(s).Prob(:,Args.sigLevel,:,:));
                            
                        else alldata = [];
                        end
                    else
                        if Args.phase; alldata = Day.session(cs(s)).phi; else alldata = Day.session(cs(s)).C; end
                    end
                    if ~isempty(alldata) && ~isempty(apair)
                        if length(size(alldata)) < 3; alldata = reshape(alldata,[size(alldata,1) 1 size(alldata,2)]); end
                        for p = 1: 4
                            subplot(ns,4,(s-1)*4+p)
                            
                            fdata = alldata(:,apair,p);
                            
                            if ~isempty(Args.mvAvg)
                                ff = repmat(1/Args.mvAvg,1,Args.mvAvg);
                                fdata = filtfilt(ff,1,fdata);
                                
                            end
                            plot(f,fdata,colo{fi})
                            hold on
                            ymax = [ymax max(fdata)];
                            ymin = [ymin min(fdata)];
                            hold on;
                            ylabel(wind{p})
                            
                        end
                        xlabel('Frequency [Hz]')
                        %         if unique(mtst.data.Index(:,1)) == 1; rule = sprintf('session%d = %s',s+1,'IDENTITY'); elseif length(unique(mtst.data.Index(:,1)) == 1) > 1; rule = MLrules{s};else; rule = sprintf('session%d = %s',s+1,'LOCATION'); end
                        if fi == 1; subplot(ns,4,(s-1)*4+p); title(sprintf('%s',MLrules{Day.session(cs(s)).rule})); end
                        %         set(f1,'Name',sprintf('%s; %s; combination %d',day(end-5:end),,cmb))
                        
                        varargout{1} = max(ymax);
                        if isfield(Day.session,'trials')
                            nt =  length(Day.session(cs(s)).trials);
                        else
                            
                            nt = nan;
                        end
                        
                        strleg(s).name{fi} = sprintf('%s; n=%d',legs{fi},nt);
                        if fi == length(files); for s = 1 : length(strleg); subplot(ns,4,(s-1)*4 + 4);legend(strleg(s).name{:});end;end
                    else
                        varargout{1} = NaN;
                        strleg(s).name{fi} = sprintf('%s; n=0',legs{fi});
                    end
                    
                    
                end
                if max(ymax) > 0.9; themax = 0.9; else; themax = max(ymax);end
                for sb = 1 : ns*4; subplot(ns,4,sb); axis([Args.frange min(ymin) themax]); set(gca,'TickDir','out');end
                countf = countf + 1;
            end
            
        case 'cxFreqMod'
            direcs = {'PA' 'AP'};
            
            if ~isempty(strfind(obj.data.setNames{ind},'clark'))
                b=load('rules.mat');
                cd(['session0' num2str(find(b.r==1)+1)])
            else
                cd session01
            end
            cd cxFreqMod
            for direct = 1 : 2
                file = sprintf('cxFreqModg%04gg%04g%s.mat',groupsN(1),groupsN(2),direcs{direct});
                try
                    load(file);
                    if length(f) == 1; f = [3:4:f]; end
                    for ep = 1 : 4
                        sig = squeeze(m_norm(ep,1:end-1,1:end-1)) > squeeze(threshold(ep,4,1:end-1,1:end-1));
                        ndata = squeeze(m_norm(ep,1:end-1,1:end-1)) .* sig;
                        subplot(2,4,(direct-1)*4+ep)
                        
                        nf = f(1) : 1: f(end);
                        [nf1,nf2] = meshgrid(nf);
                        [X,Y] = meshgrid(f);
                        ZI = interp2(X,Y,ndata,nf1,nf2);
                        
                        imagesc(nf,nf,ZI)
                        
                        axis([f(1) 90 f(1) 90])
                        if direct == 1
                            title( wind {ep})
                        end
                        caxis([0 0.001])
                    end
                    if ep == 4 && strcmp('PA',direcs{direct})
                        xlabel('Amplitude in Frontal channel (Hz)')
                        ylabel('Phase in Parietal channel (Hz)')
                    elseif ep == 4 && strcmp('AP',direcs{direct})
                        xlabel('Amplitude in Parietal channel (Hz)')
                        ylabel('Phase in Frontal channel (Hz)')
                    end
                end
            end
    end
end

