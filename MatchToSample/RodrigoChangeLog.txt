
September-14-2005

September-15-2005

September-26-2005
ReadIniCognitive.m: 
MTSTrials.m: 
ReadSequenceFile.m: 
ProcessSessionMTS.m: 

September-27-2005
@mtstrial/get.m: 
@mtstrial/plot.m: 
@mtsbinned/mtsbinned.m: Solution to use sessions even if they are not a multiplicative of the number of trials binned chosen. The data will be truncated
ProcessSessionMTS.m: Correction to enable the use of incomplete sessions

September-28-2005
@mtsbinned/mtsbinned.m: 
RodrigoChangeLog.txt: 
ProcessSessionMTS.m: 

September-30-2005
@mtsbinned/mtsbinned.m: 
@mtstrial/plot.m: 

October-03-2005
@mtsbinned/mtsbinned.m: 
@mtstrial/plot.m: 
@mtstrial/mtstrial.m: 
ProcessSessionMTS.m: 
@mtsbinned/plot.m: 
@mtstrial/get.m: 
@mtstrial/plot.m: 
@mtstrial/plus.m: 
ProcessSessionMTS.m: 

October-04-2005
@mtsbinned/plus.m: 
@mtsbinned/mtsbinned.m: 
@mtsbinned/get.m: 
@mtsbinned/plot.m: 
@mtstrial/get.m: 
@mtsbinned/plot.m: addition of the plot fct of the mtsbinned object to include the session type and the number of trials before a shift in rule  
ProcessSessionMTS.m: addition of an error message and the return to the keyboard

October-05-2005

October-13-2005
@mtstrial/plot.m: changes to adapt the plot version to the new mtstrial object
RodrigoChangeLog.txt: 

October-28-2005
ProcessSessionMTS.m: typing corrections for the library'goat';'heart';'icecreamcone';'lobster';'monkey';'skiing';'smblckfilix';'spade';'SquareWtOnBlck';'TriangleWtOnBlck';'turtle';'CrossWhiteonBlack';'flower';'gear';'HorizontalStripes';'NewCheck';'rectangle';'starfish';'sun';'triangle';'VerticleStripes';'Wheel'}; % the first three are skipped in order to avoid problem with the original coding 0 to 2 

October-31-2005
@mtstrial/plot.m: Adjustment of the code to set the onset of the first saccade as the default reaction time. No need to specify it anymore in InspectGUIbar([1:2:size(allperf,2)-1],allperf(:,[1:2:size(allperf,2)-1]),0.5,'b')
ProcessSessionMTS.m: insertion of a code to selected trials with a specified number of saccades and with a reaction time above a specific threshold

November-09-2005
@mtsbinned/plot.m: Mainly adding the help and defining the default function
@mtstrial/plot.m: adding the help and more legend on the some graph
@mtsrule/mtsrule.m: adding the help function and default function
ProcessSessionMTS.m: 
@mtsbinned/get.m: Mainly including the help and comments and define default functions
@mtsbinned/mtsbinned.m: Mainly adding the help and debugging the transformation of index from the mtstrial object
@mtstrial/get.m: adding the help and default function
@mtsrule/plot.m: adding the help function and default fct
@mtstrial/mtstrial.m: changes of the way to set the info on the trial rule identity or location

November-22-2005
@mtstrial/get.m: correction of the code to get rid of the Args.All for simpification
@mtsbinned/mtsbinned.m: adaptation to put the error message on a new line

December-20-2005
@mtsbinned/mtsbinned.m: 
ProcessSessionMTS.m: adaptation to have the library output in the same format than the plotting

December-27-2005

January-05-2006

test


April-20-2006
ReadIniCognitive.m: some details about eh help and one field that was commentd???
@mtstrial/mtstrial.m: To handle days where the sessions were combined according to the rule
ProcessSessionMTS.m: to handle days where the sessions were combined according to the rule

June-15-2006
CombineSessionRule.m: corrected some typing errors
@mtstrial/mtstrial.m: small change of the code to handle the new marker files created from the CombineSessionRule.m
ProcessSessionMTS.m: 

June-21-2006
@mtsbbandtraces/mtsbbandtraces.m: 
@mtshpasstraces/mtshpasstraces.m: 

September-14-2006
@mtslfptraces/plot.m: 
@mtshpasstraces/get.m: 
@mtsbbandtraces/plot.m: 
CombineSessionRule.m: correction of the code for dealing with incomplete sessions
@mtshpasstraces/plot.m: 
@mtsbbandtraces/plus.m: 
mtsProcessSessionNeural.m: 
@mtshpasstraces/mtshpasstraces.m: 
ProcessSessionMTS.m: 
@mtshpasstraces/plus.m: 
ReadIniCognitive.m: adding the actual number of trials in the reading
@mtsbbandtraces/get.m: 
@mtslfptraces/get.m: 

September-19-2006
CombineSessionRule.m: correction of a small typing error +1 at the wrong line

November-08-2006
getperformance.m: includes a fitting curve of teh performance and several algorithms to better identify the stable performance
mtsgetTrials.m: modification of the function to be used fo the selection of trials only without using channel number.
