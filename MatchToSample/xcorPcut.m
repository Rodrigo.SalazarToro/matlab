function [cor,vcor,lags,varargout] = xcorPcut(mts,sp1,sp2,varargin)

Args = struct('plength',400,'maxlag',100,'surrogate',0,'nrep',1000,'Prob',[0.025 0.975 0.005 0.995],'k',1,'frame',5,'smoothingLevel',2,'wintermute',0,'binning',2);
Args.flags = {'surrogate','wintermute'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'surrogate','wintermute'});

ind = mtsgetTrials(mts,modvarargin{:});
strials1 = stableSpikes(sp1,modvarargin{:});
strials2 = stableSpikes(sp2,modvarargin{:});

trials = intersect(ind,intersect(strials1,strials2));

nevent = zeros(1,5);
bins = [0:Args.plength];
themaxlag = Args.maxlag / Args.binning;
nbin = length(bins);
if ~isempty(trials)
    tcount = 1;
    for p = 1 : 5; allts1{p} = []; allts2{p} = [];end
    for t = vecr(trials);
        periods(1) = round((mts.data.CueOnset(t) - Args.plength));
        periods(2) = round(mts.data.CueOffset(t) - Args.plength);
        periods(3) = round(mts.data.CueOffset(t));
        periods(4) = round((mts.data.MatchOnset(t) - Args.plength));
        periods(5) = round(mts.data.MatchOnset(t));

        for p = 1 : length(periods)
            spiketimes1 = getSpikeInPer(sp1,periods,t,p,Args.plength);
            spiketimes2 = getSpikeInPer(sp2,periods,t,p,Args.plength);
            nevent(p) = nevent(p) + length(spiketimes1) + length(spiketimes2);
            ts1 = histcie(spiketimes1,bins);
            ts2 = histcie(spiketimes2,bins);
            [xcova(p,tcount,:),lags] = xcov(ts1,ts2,themaxlag,'coeff');
            yfitt(p,tcount,:) = sgolayfilt(xcova(p,tcount,:),Args.k,Args.frame);
            allts1{p} = [allts1{p}; ts1];
            allts2{p} = [allts2{p}; ts2];

        end
        tcount = tcount +1;
    end
    
%% code for append then scov
    for p = 1 : length(periods)
        [append.cor(p,:),lags] = xcov(allts1{p},allts2{p},themaxlag,'coeff');
        append.yfit(p,:) = sgolayfilt(append.cor(p,:),Args.k,Args.frame);
        if isnan(append.yfit(p,:)); append.cPeak(p,1) = nan; else; [append.cPeak(p,1),append.cPeak(p,2)] = findCntPeak(append.yfit(p,:),lags); end

    end
%%
    cor = squeeze(nanmean(xcova,2));
    vcor = squeeze(nanstd(xcova,[],2));
    yfit = squeeze(nanmean(yfitt,2));
    for sm =  1 : Args.smoothingLevel;  for p = 1 : length(periods); yfit(p,:) = sgolayfilt(yfit(p,:),Args.k,Args.frame); end; end
    for p = 1 : 5; if isnan(yfit(p,:)); cPeak(p,1) = nan; popout(p) = nan;else; [cPeak(p,1),cPeak(p,2),popout(p)] = findCntPeak(yfit(p,:),lags); end; end
else
    cor = nan;
    vcor = nan;
    lags = nan;
    yfit = nan;
    cPeak = nan;
    popout = nan;
    append = [];
end

lags = lags * Args.binning;
varargout{1} = trials;
varargout{2} = nevent;
varargout{3} = yfit;
varargout{4} = cPeak;
varargout{5} = popout;
varargout{6} = append;
if Args.surrogate
    % needs some work to ge the bilateral > or < p
    ntrials = length(trials);

    for p = 1 : 5
        allts1{p} = reshape(allts1{p},nbin,ntrials);
        allts2{p} = reshape(allts2{p},nbin,ntrials);
        for rep = 1 : Args.nrep
            ordT1 = randperm(ntrials);
            ordT2 = randperm(ntrials);

            %             tt1 = reshape(allts1{p}(:,ordT1),nbin*ntrials,1);
            %             tt2 = reshape(allts2{p}(:,ordT2),nbin*ntrials,1);
            clear surt;
            for tt =  1 : length(ordT1);

                if Args.wintermute
                    data1{tt} = allts1{p}(:,ordT1(tt));
                    data2{tt} = allts2{p}(:,ordT2(tt));
                else
                    [surt(:,tt),lags] = xcov(allts1{p}(:,ordT1(tt)),allts2{p}(:,ordT2(tt)),Args.maxlag,'coeff');
                end
            end

            if Args.wintermute;
                la{1} = Args.maxlag; [surw,lagsw] = dfeval(@xcov,data1,data2,repmat(la,1,length(ordT1)),repmat({'coeff'},1,length(ordT1)));
                surt = cell2mat(surw);
                surt = reshape(surt,length(surw{1}),length(surw));
                lags = lagsw{1};
            end

            corSur(rep,:) = nanmean(surt,2);
        end
        if unique(isnan(corSur)) ~= 1; [datafit,xx,thresh(p,:,:),rsquare] = fitSurface(corSur,lags,'Prob',Args.Prob,modvarargin{:}); else; thresh(p,:,:) = nan(length(lags),length(Args.Prob)); end
    end
    varargout{6} = thresh;
end









%                     [Ct,phit,S12,S1,S2,ti,f,zerosp]=cohgrampt(spike1,spike2,[0.4 0.1],params,1);
%                     mincoh = [mincoh min(Ct)];
%                     maxcoh = [maxcoh max(Ct)];
%                     t{cue,al} = ti - 3;
%                     C{cue,al} = flipud(rot90(Ct)); %format times for x and freq for y
%                     phi{cue,al} = flipud(rot90(phit));
