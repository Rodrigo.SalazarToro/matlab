function [hkb_function,empirical_hkb, estimates, sse] = hkb_fmin(varargin)

Args = struct('pp_dist',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);


% % % % % empirical_hkb = hist(Args.pp_dist,[-pi:pi/10:pi]);
% % % % % empirical_hkb = (empirical_hkb ./ sum(empirical_hkb)) * -1; 

empirical_hkb = Args.pp_dist';



%initial values
arg_in(1) = 0;
arg_in(2) = 0; 
arg_in(3) = 0; 
arg_in(4) = 0;
% arg_in(5) = 0;

options=optimset('MaxFunEvals',100000); %default is 200 * number_of_variables (200*8)


[estimates,sse] = fminsearch(@hkb,arg_in, options, empirical_hkb);


nbins = size(empirical_hkb,2);


a = estimates(1);
b = estimates(2);
offset = estimates(3);

phi_bins = linspace(-pi,pi,nbins);
for p = 1 : nbins
    %hkb, see Haken et. al., 1985
    phi = phi_bins(p);

    hkb_function(p) = (-(phi)*offset) - a*cos(phi) - b*cos(2*phi);
end

