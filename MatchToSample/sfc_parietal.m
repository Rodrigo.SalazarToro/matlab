function sfc = sfc_parietal

%run at session level
sfc = [];
sesdir = pwd;
N = NeuronalHist;
sg = sorted_groups;
locations = N.location;

%get sorted parietal groups
parietalch = intersect(sg(sg>33),N.gridPos(N.gridPos>33));
ngroups = size(parietalch,2);
counter = 0;
for ng = 1 : ngroups
    [~,iis] = intersect(N.gridPos,parietalch(ng));
%     spike = locations(iis);
    cd([sesdir filesep 'group' num2strpad(parietalch(ng),4)])
    %only look at MUA
    cd('cluster01m')
    
    %look at SFC for all combinations
    for ngg = 1 : ngroups
        [~,iif] = intersect(N.gridPos,parietalch(ngg));
%         field = locations(iif);

        load(['SFCall1g' num2strpad(parietalch(ngg),4) 'Rule1.mat'])
        load(['SFCall1g' num2strpad(parietalch(ngg),4) 'Rule1Sur.mat'])
%         figure
%         for x = 1:4
%             %             phi(C(:,x) > Prob(:,2,x))
%             %             plot(f,C(:,x));
%             %             hold on
%             %             plot(f,Prob(:,2,x),'r');
%             %
%             sigphi = phi(C(9:17,x) > Prob(9:17,3,x))
%             subplot(1,4,x)
%             rose(sigphi)
%             
%         end
        
        %
        %         title(['spike ' spike '   field ' field])
        %                     pause
        %             close all
        
        %delay period
        
        hist_numbers = [N.number(iis); N.number(iif)];
        sigphis = phi(C(11,4) > Prob(11,2,4));
        
%         if size(sigphis,1) < 5
%             sigphis = [];
%         end

        hn1 = hist_numbers(1);
        hn2 = hist_numbers(2);
        
        if ng ~= ngg
            counter = counter + 1;
            sfc.numbers(counter,:) = hist_numbers;
            sfc.sigphi{counter} = sigphis;
            
            %MIP(12) VS LIP(13)
            if hn1 == 12 && hn2 == 13
                sfc.mip_lip{counter} = sigphis;
            else
                sfc.mip_lip{counter} = [];
            end
            %LIP(13) VS MIP(12)
            if hn1 == 13 && hn2 == 12
                sfc.lip_mip{counter} = sigphis;
            else
                sfc.lip_mip{counter} = [];
            end
            

            
            %PE(10) VS PG(11)
            if hn1 == 10 && hn2 == 11
                sfc.pe_pg{counter} = sigphis;
            else
                sfc.pe_pg{counter} = [];
            end
            %PG(11) VS PE(10)
            if hn1 == 11 && hn2 == 10
                sfc.pg_pe{counter} = sigphis;
            else
                sfc.pg_pe{counter} = [];
            end
            
            

            %PG(11) VS PG(11)
            if hn1 == 11 && hn2 == 11
                sfc.pg_pg{counter} = sigphis;
            else
                sfc.pg_pg{counter} = [];
            end
            %PE(10) VS PE(10)
            if hn1 == 10 && hn2 == 10
                sfc.pe_pe{counter} = sigphis;
            else
                sfc.pe_pe{counter} = [];
            end
            
            
            
            %PG(11) VS MIP(12)      
            if hn1 == 11 && hn2 == 12
                sfc.pg_mip{counter} = sigphis;
            else
                sfc.pg_mip{counter} = [];
            end
            %MIP(12) VS PG(11)
            if hn1 == 12 && hn2 == 11
                sfc.mip_pg{counter} = sigphis;
            else
                sfc.mip_pg{counter} = [];
            end
            
            
            
            %PG(11) VS LIP(13)
            if hn1 == 11 && hn2 == 13
                sfc.pg_lip{counter} = sigphis;
            else
                sfc.pg_lip{counter} = [];
            end
            %LIP(13) VS PG(11)
            if hn1 == 13 && hn2 == 11
                sfc.lip_pg{counter} = sigphis;
            else
                sfc.lip_pg{counter} = [];
            end
            
            
            
            %PE(10) VS LIP(13)
            if hn1 == 10 && hn2 == 13
                sfc.pe_lip{counter} = sigphis;
            else
                sfc.pe_lip{counter} = [];
            end
            %LIP(13) VS PE(10)
            if hn1 == 13 && hn2 == 10
                sfc.lip_pe{counter} = sigphis;
            else
                sfc.lip_pe{counter} = [];
            end
            
            
            
            %PE(10) VS MIP(12)
            if hn1 == 10 && hn2 == 12
                sfc.pe_mip{counter} = sigphis;
            else
                sfc.pe_mip{counter} = [];
            end
            %MIP(12) VS PE(10)
            if hn1 == 12 && hn2 == 10
                sfc.mip_pe{counter} = sigphis;
            else
                sfc.mip_pe{counter} = [];
            end
            
            
            
            %PEC(8) VS PE(10)
            if hn1 == 8 && hn2 == 10
                sfc.pec_pe{counter} = sigphis;
            else
                sfc.pec_pe{counter} = [];
            end
            %PEC(8) VS PG(11)
            if hn1 == 8 && hn2 == 11
                sfc.pec_pg{counter} = sigphis;
            else
                sfc.pec_pg{counter} = [];
            end
            %PEC(8) VS MIP(12)
            if hn1 == 8 && hn2 == 12
                sfc.pec_mip{counter} = sigphis;
            else
                sfc.pec_mip{counter} = [];
            end
            %PEC(8) VS LIP(13)
            if hn1 == 8 && hn2 == 13
                sfc.pec_lip{counter} = sigphis;
            else
                sfc.pec_lip{counter} = [];
            end
            
            
            
            %PE(10) VS PEC(8)
            if hn1 == 10 && hn2 == 8
                sfc.pe_pec{counter} = sigphis;
            else
                sfc.pe_pec{counter} = [];
            end
            %PG(11) VS PEC(8)
            if hn1 == 11 && hn2 == 8
                sfc.pg_pec{counter} = sigphis;
            else
                sfc.pg_pec{counter} = [];
            end
            %MIP(12) VS PEC(8)
            if hn1 == 12 && hn2 == 8
                sfc.mip_pec{counter} = sigphis;
            else
                sfc.mip_pec{counter} = [];
            end
            %LIP(13) VS PEC(8)
            if hn1 == 13 && hn2 == 8
                sfc.lip_pec{counter} = sigphis;
            else
                sfc.lip_pec{counter} = [];
            end
            
            
            
        end
    end
end