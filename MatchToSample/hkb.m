function [sse] = hkb(varargin)

%A,decay,frequency,phase_shift,offset,exponent, B, sig2,c

v = varargin{1}; %intial arguments
empirical_hkb = varargin{2}; %correlogram

nbins = size(empirical_hkb,2);


a = v(1);
b = v(2);
offset = v(3);

phi_bins = linspace(-pi,pi,nbins);
for p = 1 : nbins
    %hkb, see Haken et. al., 1985, and fuchs 2000 (i added offset, and the -1 to flip it)
    phi = phi_bins(p);
    
    hkb_function(p) = (-(phi)*offset) - a*cos(phi) - b*cos(2*phi);
end

error= empirical_hkb - hkb_function;
sse = sum(error.^2);












