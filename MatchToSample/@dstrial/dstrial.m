function obj = dstrial(varargin)
% @dstrial Constructor function for DSTRIAL class
%   OBJ = dstrial('auto') attempts to create a dstrial object by
%   using the files *.seq *.mrk *.ini and *timing.mat (which are assumed to be
%   in the current directory). The  object contains all the
%   following fields for each trial:
%
%   Session.CueLoc - cCue location
%   Session.CueOnset - Cue onset
%   Session.CueOffset - Cue offset
%   Session.MatchOnset - Match onset
%   Session.FirstSac - First saccadic response onset
%   Session.Index - has all the information about each trial; column wise:
%      1. cummulative trial number
%      2. real trial number
%      3. cue location
%      4. cue onset
%      5. cue offset
%      6. match onset
%      7. first saccade
%   In addition, the following arguments can be used:
%
%   - NumOfSac: Creates the object with the specified number of saccades.
%               Dep. ProcessSessionMTS.
%
%   - RTLimit: Creates the object with reaction times above or equal to the specified threshold.
%              Dep. ProcessSessionMTS.
%
%   The data are raw vectors of all the trials for a given session.
%
%   Dependencies: ProcessSessionMTS,nptdata.


Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'dstrial';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'ds';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end


%%
function obj = createObject(Args,varargin)

sdir = pwd;


%try to make object if there is an error then create and empty object
%check to see if the session is a DS session
s = nptDir('DS*.bhv');
if ~isempty(s)
    
    %get BHV data
    bhv = nptDir('*.bhv');
    b = bhv_read([cd filesep bhv.name]);
    
    %get index of completed trials
    cd('delFiles')
    load deletedfiles trials
    cd ..
    
    numTrials = size(trials,1);
    
    data.CueLoc = zeros(numTrials,1);
    data.CueOnset = zeros(numTrials,1);
    data.CueOffset = zeros(numTrials,1);
    data.MatchOnset = zeros(numTrials,1);
    data.FirstSac = zeros(numTrials,1);
    data.AnalogData = cell(numTrials,1);
    data.Index = zeros(numTrials,7);
    
    t= 0;
    for x = trials'
        t = t + 1;
        
        %condition order corresponding to monkey logic (1 : 8)
        %b.TaskObject(:,2)
        locsML = num2str([5 0; -3.53 3.536; 0 5; 3.536 3.536; -5 0; 3.536 -3.53; 0 -5; -3.53 -3.53]);
        
        %get condition number
        condition = b.ConditionNumber(x);
        l = (num2str(locsML(condition,:)));
        %get location
        
        %Cue Locations (CLOCKWISE starting at top 0,5) numbers 1 : 8
        locs = num2str([0 5; 3.536 3.536; 5 0; 3.536 -3.53; 0 -5; -3.53 -3.53; -5 0; -3.53 3.536]);
        
        %saves number 1 : 8 based on coverted sequence
        %1 : 8 from [0 5] to [-3.5 3.5] CLOCKWISE
        data.CueLoc(t) = strmatch(l,locs);
        
        %Determine reaction time based on b.CodeTimes
        data.FirstSac(t) = b.ReactionTime(x);
        
        %get eyeposition data
        e = b.AnalogData{x};
        %get only data from FIX OFF to MATCH (correct)
        times = b.CodeTimes{x};
        times = times - times(1);
        fixOff = times(7);
        match = times(8);
        
        data.AnalogData{t,1} = e.EyeSignal((fixOff:match),:);
    end
    
    sampleOn = unique(cell2mat(cellfun(@(x) find(x == 25),b.CodeNumbers(trials),'UniformOutput', false))); %cue onset
    sampleOff = unique(cell2mat(cellfun(@(x) find(x == 26),b.CodeNumbers(trials),'UniformOutput', false))); %cue offset
    matchOn = unique(cell2mat(cellfun(@(x) find(x == 27),b.CodeNumbers(trials),'UniformOutput', false))); %match
    
    data.CueOnset = cell2mat(cellfun(@(x) (x(sampleOn) - x(1)),b.CodeTimes(trials),'UniformOutput', false))';
    data.CueOffset = cell2mat(cellfun(@(x) (x(sampleOff) - x(1)),b.CodeTimes(trials),'UniformOutput', false))';
    data.MatchOnset = cell2mat(cellfun(@(x) (x(matchOn) - x(1)),b.CodeTimes(trials),'UniformOutput', false))';
    
    data.setNames{1} = pwd;
    data.numSets = numTrials;
    
    data.Index(:,1) = [1:numTrials];
    data.Index(:,2) = [1:numTrials];
    data.Index(:,3) = data.CueLoc;
    data.Index(:,4) = data.CueOnset;
    data.Index(:,5) = data.CueOffset;
    data.Index(:,6) = data.MatchOnset;
    data.Index(:,7) = data.FirstSac;
    
    %%
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    
else
    obj = createEmptyObject(Args);
end

%%
function obj = createEmptyObject(Args)


data.CueLoc = [];
data.CueOnset = [];
data.CueOffset = [];
data.MatchOnset = [];
data.FirstSac = [];
data.setNames = {};
data.AnalogData = {};
data.numSets = 0;
data.Index = [];
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
