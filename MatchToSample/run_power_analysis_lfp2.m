function run_power_analysis_lfp2(varargin)

%run at monkey level
%calculates mean power and beta peak for for each channel
%uses power trials written with write_power_trials.m

Args = struct('ml',0,'days',[],'sessions',[]);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);
all_peak_frequencies = [];
for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    for s = Args.sessions;
        cd ([daydir filesep sessions(s).name]);
        sesdir = pwd;
        
        pf = power_analysis_lfp2;
        all_peak_frequencies = [all_peak_frequencies pf];
    end
end

cd(monkeydir)