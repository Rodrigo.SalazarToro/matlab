function epoch_granger(varargin)

%computes granger for 7 different time periods
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]
% 8: ititrial [(match + 201) : (match + 800)], for incorrect, betty trials only

Args = struct('ml',0,'bmf',0,'rules',[]);
Args.flags = {'ml','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args);

movingwin = [.2 .05]; %this is in seconds
params.Fs = 200;

sesdir = pwd;
if Args.ml
    if Args.bmf
        N = NeuronalHist('bmf');
        [~,chpairlist,~,~,sortedpairs] = bmf_groups('ml');
    else
        N = NeuronalHist('ml');
        [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
    end
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end

cd([sesdir filesep 'lfp']);
lfpdir = pwd;
lfptrials = nptDir('*_lfp.*');
numpairs = nchoosek(N.chnumb,2);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)

sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1, 4];
        else
            rules = [1 2 3 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    cd(lfpdir)
    if Args.ml
        if rule == 1
            trials = identity;
            granger_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 2
            trials = location;
            granger_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 3
            trials = fixtrials;
            granger_epochs = cell(7,numpairs); %seven combinations
        elseif rule == 4
            trials = incorrtrials;
            granger_epochs = cell(8,numpairs); %seven combinations
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        ntrials = size(trials,2);
        counter = 0;
        for sp = sortedpairs
            if Args.bmf
                counter = counter + 1;
                c1 = chpairlist(counter,1);
                c2 = chpairlist(counter,2);
            else
                c1 = chpairlist(sp,1);
                c2 = chpairlist(sp,2);
            end
            
            tcounter = 0;
            fix1 = []; sample1 = []; delay4001 = []; delay8001 = []; delay1 = []; iti1 = [];
            fix2 = []; sample2 = []; delay4002 = []; delay8002 = []; delay2 = []; iti2 = [];
            for x = trials
                tcounter = tcounter + 1;
                s_on = sample_on(x);
                s_off = sample_off(x);
                
                [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(x).name);
                p1t = data.rawdata(c1,:);
                p2t = data.rawdata(c2,:);
                
                %get rid of 60! and detrend
                data1 = preprocessinglfp(p1t,'detrend')';
                data2 = preprocessinglfp(p2t,'detrend')';
                
                fixd1 = data1((floor(s_on/5) - 80) : ceil(s_on/5));
                sampled1 = data1((floor(s_off/5) - 80) : ceil(s_off/5));
                delay400d1 = data1(floor((s_off/5) : (ceil(s_off/5) + 80)));
                delay800d1 = data1((floor(s_off/5) + 80) : (ceil(s_off/5) + 160));
                delayd1 = data1((floor(s_off/5) + 40) : (ceil(s_off/5) + 160));
                
                fix1(1:80,tcounter) = fixd1(1:80);
                sample1(1:80,tcounter) = sampled1(1:80);
                delay4001(1:80,tcounter) = delay400d1(1:80);
                delay8001(1:80,tcounter) = delay800d1(1:80);
                delay1(1:120,tcounter) = delayd1(1:120);
                
                
                fixd2 = data2((floor(s_on/5) - 80) : ceil(s_on/5));
                sampled2 = data2((floor(s_off/5) - 80) : ceil(s_off/5));
                delay400d2 = data2(floor((s_off/5) : (ceil(s_off/5) + 80)));
                delay800d2 = data2((floor(s_off/5) + 80) : (ceil(s_off/5) + 160));
                delayd2 = data2((floor(s_off/5) + 40) : (ceil(s_off/5) + 160));
                
                fix2(1:80,tcounter) = fixd2(1:80);
                sample2(1:80,tcounter) = sampled2(1:80);
                delay4002(1:80,tcounter) = delay400d2(1:80);
                delay8002(1:80,tcounter) = delay800d2(1:80);
                delay2(1:120,tcounter) = delayd2(1:120);
                
                if rule == 4
                    itid1 = data1((floor(match/5) + 140) : (ceil(match/5) + 260)); %match + 700 : match + 1300
                    iti1(1:120,tcounter) = itid1(1:120); %match + 700 : match + 1300
                    
                    itid2 = data2((floor(match/5) + 140) : (ceil(match/5) + 260)); %match + 700 : match + 1300
                    iti2(1:120,tcounter) = itid2(1:120); %match + 700 : match + 1300
                end
            end
            
            
            [f.fx2y,f.fy2x,f.fxy,f.freq] = GCepoch(fix1,fix2,params,20);
            
            [s.fx2y,s.fy2x,s.fxy,s.freq] = GCepoch(sample1,sample2,params,20);
            
            [d400.fx2y,d400.fy2x,d400.fxy,d400.freq] = GCepoch(delay4001,delay4002,params,20);
            
            [d800.fx2y,d800.fy2x,d800.fxy,d800.freq] = GCepoch(delay8001,delay8002,params,20);
            
            [d.fx2y,d.fy2x,d.fxy,d.freq] = GCepoch(delay1,delay2,params,20);
            
            if rule == 4
                [iti.fx2y,iti.fy2x,iti.fxy,iti.freq] = GCepoch(iti1,iti2,params,20);
            end
            
            granger_epochs{1,sp} = f;
            granger_epochs{2,sp} =  s;
            granger_epochs{3,sp} =  d400;
            granger_epochs{4,sp} =  d800;
            granger_epochs{5,sp} =  d;
            granger_epochs{6,sp} =  [];
            granger_epochs{7,sp} =  [];
            
            if rule ==4
                granger_epochs{8,sp} =  iti;
            end
        end
    end
    
    
    cd([lfpdir filesep 'lfp2'])
    write_info = writeinfo(dbstack);
    if Args.ml
        if rule == 1;
            mkdir('identity')
            cd('identity')
            save granger_epochs granger_epochs write_info
        elseif rule == 2;
            mkdir('location')
            cd('location')
            save granger_epochs granger_epochs write_info
        elseif rule == 3
            mkdir('fixation')
            cd('fixation')
            save granger_epochs granger_epochs write_info
        elseif rule == 4
            mkdir('incorrect')
            cd('incorrect')
            save granger_epochs granger_epochs write_info
        end
    else
        save granger_epochs granger_epochs write_info
    end
end

cd(sesdir)

fprintf(1,'\n')
fprintf(1,'Epoch Granger Done.\n')