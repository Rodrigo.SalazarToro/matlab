function compare_correlation_with_stimulus(varargin)

%run at session level
%computes relative phase angle tass_1998 and lavenquen
Args = struct('ml',0,'bmf',0);
Args.flags = {'ml','bmf'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
        N = NeuronalHist('ml','bmf');
        groups = N.gridPos;
        [~,chpairlist] = bmf_groups;
        sortedpairs = 1 : size(chpairlist,1);
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        alltrials = mtsgetTrials(mt,'ml','BehResp',1,'stable','ML','rule',1); %IDENTITY
        N = NeuronalHist('ml');
        groups = N.gridPos;
        [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
    end
else
    mt=mtstrial('auto','redosetNames');
    alltrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY
    
    N = NeuronalHist;
    groups = N.gridPos;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
npairs = size(chpairlist,1);
locs = mt.data.CueLoc(alltrials);
stims = mt.data.CueObj(alltrials);

cd([sesdir filesep 'lfp' filesep 'lfp2'])

%get index of all lfp trials  lfpdata2.name
lfpdata = nptDir('*_lfp2.*');


pcounter = 0;
for np = 1 : npairs
    c1 = chpairlist(np,1);
    c2 = chpairlist(np,2);
    pcounter = pcounter + 1;
    resp_ent = [];
    c_mean = [];
    tcounter = 0;
    for t = alltrials
        tcounter = tcounter + 1;
        %lowpass
        hdata = load(lfpdata(t).name);
        
        data = hdata.data;
        
        data = data'; %need to do this before and after using hilbert, don't include during the transform
        %compute hilbert transform
        data = hilbert(data);
        
        %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
        %COMPENT IS FLIPPED
        %find instantaneous phase angles
        data = angle(data);
        data = data';
        
        
        d1 = unwrap(data(c1,:)) ./ (2*pi);
        d2 = unwrap(data(c2,:)) ./ (2*pi);
        
        
        
        %cyclic relative phase
        dacyc = mod(d1 - d2,1);
        
        
        window = 400;
        nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
        bins = linspace(0,1,nbins);
        max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
        
        %% presample
        allh = dacyc((sample_on(t) - 399) : (sample_on(t)))';
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        resp_ent(1,tcounter) = (max_ent - re) ./ max_ent;
        c_mean(1,tcounter) = circ_mean((allh * (2*pi)));
        
        %% sample
        allh = dacyc((sample_on(t)+101) : (sample_on(t)+500))';
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        resp_ent(2,tcounter) = (max_ent - re) ./ max_ent;
        c_mean(2,tcounter) = circ_mean((allh * (2*pi)));
        
        %% d1
        allh = dacyc((sample_on(t) + 501) : (sample_on(t)+900))';
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        resp_ent(3,tcounter) = (max_ent - re) ./ max_ent;
        c_mean(3,tcounter) = circ_mean((allh * (2*pi)));
        
        %% d2
        allh = dacyc((sample_on(t)+901) : (sample_on(t)+1300))';
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        resp_ent(4,tcounter) = (max_ent - re) ./ max_ent;
        c_mean(4,tcounter) = circ_mean((allh * (2*pi)));
    end
    
    if Args.ml
        channel_pairs = [ 'single_trial_plv_stim' num2strpad(N.gridPos(chpairlist(np,1)),3) num2strpad(N.gridPos(chpairlist(np,2)),3)];
    else
        channel_pairs = [ 'single_trial_plv_stim' num2strpad(chpairlist(np,1),3) num2strpad(chpairlist(np,2),3)];
    end
    save(channel_pairs,'resp_ent','c_mean','locs','stims')
end

% for x = 1:36;scatter(smooth(mt.data.BehResp(alltrials),25),smooth((log(all_resp_ent(x,:))),25),'r','fill','s');x,axis square;lsline;pause;cla;end
%
% allr = [];allp = [];for x = 1:36;[r p] = corrcoef(smooth(mt.data.BehResp(alltrials),25),smooth(all_resp_ent(x,:),25));allr(x) = r(1,2);allp(x) = p(1,2);end









