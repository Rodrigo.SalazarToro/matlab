

monkeys = {'D:\workingmemory\data\monkey\clark' 'D:\workingmemory\data\monkey\betty' 'D:\workingmemory\data\monkey\betty\acute'};
nanfiles = [];
cn = 1;
for m = [3 2]
    cd D:\workingmemory\data\monkey\
    cd(monkeys{m})
    sdir = pwd;
    load switchdays
    %     for d = 1 : length(days)
    for d = fliplr(1 : length(days))
        cd([sdir filesep days{d} filesep 'session99' filesep 'xcorr'])
        files = nptDir('*Sur*');
%         files = nptDir('*RuleComp500ms.mat');
        %         for ff = 1 : length(files)
        %             sur = load(files(ff).name);
        %             rv = load(regexprep(files(ff).name,'Sur',''));
        %             filename = regexprep(files(ff).name,'Sur','pvalues');
        %             %             if isempty(nptDir(filename))
        %             pvalues = nan(size(sur.rc,2),size(sur.rc,3));
        %             for dim1 = 1: size(sur.rc,2)
        %                 for dim2 = 1:size(sur.rc,3)
        %                     if sum(isnan(squeeze(sur.rc(:,dim1,dim2)))) == 0 && ~isnan(rv.rc(dim1,dim2))
        %                         pvalues(dim1,dim2) = pvalueEstimateFromShuffle(squeeze(sur.rc(:,dim1,dim2)),rv.rc(dim1,dim2));
        %                     end
        %                 end
        %             end
        %
        %             if sum(sum(~isnan(pvalues))) == 0
        %                 display([pwd filesep filename 'NANANANANANANANANANANA!!!!!!!!!!!!!'])
        %                 nanfiles{cn} = [pwd filesep filename];
        %                 cn = cn + 1;
        %             else
        %                 save(filename,'pvalues');
        %                 display([pwd filesep filename])
        %             end
        %             %             end
        %         end
        
        for ff = 1 : length(files)
            try
            sur = load(files(ff).name);
            rdata = load(regexprep(files(ff).name,'Sur',''));
            filename = regexprep(files(ff).name,'.mat','pvalues.mat');
%             datarc = sur.diffrc;
            datarc = sur.rc;
%            
            pvalues = nan(size(datarc,2),size(datarc,3));
            pds = cell(size(datarc,1),size(datarc,2));
%             
            for dim1 = 1: size(rdata.rc,1)
                for dim2 = 1:size(rdata.rc,2)
%                     if sum(isnan(squeeze(sur.diffrcSur(:,dim1,dim2)))) == 0 && ~isnan(datarc(dim1,dim2))
%                         [pvalues(dim1,dim2),pds{dim1,dim2}] = pvalueEstimateFromShuffle(squeeze(sur.diffrcSur(:,dim1,dim2)),datarc(dim1,dim2));
%                     end
                    if sum(isnan(squeeze(datarc(:,dim1,dim2)))) == 0 && ~isnan(rdata.rc(dim1,dim2))
                        [pvalues(dim1,dim2),pds{dim1,dim2}] = pvalueEstimateFromShuffle(squeeze(datarc(:,dim1,dim2)),rdata.rc(dim1,dim2));
                    end
                end
            end
            
            save(filename,'pvalues','pds');
            display([pwd filesep filename])
            end
        end
    end
    
end