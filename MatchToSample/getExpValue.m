function ExpV = getExpValue(mutualgram,f)

if length(size(mutualgram)) == 3
    
    
    for ff = 1 : length(f)
        [distr,values] = hist(squeeze(mutualgram(:,:,ff)), 500) ;
        distr = distr ./ repmat(sum(distr,1),500,1);
        ExpV(:,ff) = sum(distr .* repmat(values,1,size(distr,2)),1);
    end
    
elseif length(size(mutualgram)) == 2
    if size(mutualgram,2) ~= length(f); mutualgram = mutualgram';end
    for ff = 1 : length(f)
        [distr,values] = hist(squeeze(mutualgram(:,ff)), 500) ;
        distr = distr / sum(distr);
        ExpV(ff) = sum(distr .* values);
    end
end