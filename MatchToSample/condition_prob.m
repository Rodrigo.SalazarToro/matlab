function new_condition = condition_prob(TrialRecord)

%total number of trials completed
total_trials = TrialRecord.CurrentTrialNumber;
conditions_played = TrialRecord.ConditionsPlayed;
cond = TrialRecord.ConditionsThisBlock;
error_record = TrialRecord.TrialErrors;
%get record for each condition
number_conditions = size(cond,2);

if total_trials >= 1%100

    for x = 1 : number_conditions
       i = find(conditions_played == cond(x)); %index for condition
       correct = find(error_record(i) == 0);
       incorrect = find(error_record(i) == 6);
       performance = correct / (correct + incorrect);
       if performance < .4 ;
           cond = [cond cond(x)];
       end
    end

        number_conditions = size(cond,2);
        perm = randperm(number_conditions);
        new_condition = cond(perm(1));

else
    perm = randperm(number_conditions);
    new_condition = perm(1);
end


