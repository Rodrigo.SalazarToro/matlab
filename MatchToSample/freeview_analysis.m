function freeview_analysis(varargin)


%run at freeview level
%sliding window analysis

Args = struct('monkey','betty','windowSize',200,'windowStep',50,'phaseSize',50,'lowpass_normalize',0,'threshold',0,'channelpairs',0,'redo_channelpairs',0,'pair_info',0,'makeneuronalhist',0);
Args.flags = {'lowpass_normalize','threshold','channelpairs','redo_channelpairs','pair_info','makeneuronalhist'};
Args = getOptArgs(varargin,Args);

%Xcorr
windowSize = Args.windowSize;
windowStep = Args.windowStep;
phaseSize = Args.phaseSize;

freedir = pwd;

N = NeuronalHist;
chnumb = N.chnumb;

%calculate number of pairs (n choose 2)
pairs = (chnumb*(chnumb-1)) / 2;


%Low Pass and Normalize
%cuts out everything after first sac on mts trials
if Args.lowpass_normalize
    cd([freedir filesep 'lfp'])
    lowPass4Xcorr(chnumb) %leaves in lfp2 directory
end

all_channels = N.gridPos;
all_pairs = [];
ap = 0;
for ap1 = 1 : chnumb
    for ap2 = (ap1+1) : chnumb
        ap = ap + 1;
        all_pairs(ap,:) = [all_channels(ap1) all_channels(ap2)];
    end
end

if Args.pair_info
    cd([freedir filesep 'lfp' filesep 'lfp2'])
    save all_channels all_channels
    save all_pairs all_pairs
end


%write channelpairs
if Args.channelpairs
    
    cd([freedir filesep 'lfp' filesep 'lfp2'])
    lfp2directory = pwd;
    %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
    % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
    lfpdata = nptDir([Args.monkey '*']);
    num_trials = size(lfpdata,1);
    load ([cd filesep lfpdata(1).name],'normdata');
    numchannels = size(normdata,1);
    ddata = cell(numchannels,1);
    
    pair_number = 0;
    total_pairs = pairs;
    for c1=1:chnumb
        for c2=(c1+1):chnumb
            pair_number = pair_number + 1;
            fprintf(1,[num2str(pair_number) '  of  ' num2str(total_pairs)])
            fprintf(1,'\n')
            %check to see if the channel_pair has already been written
            
            channel_pairs = [ 'channelpair' num2strpad(all_channels(c1),2) num2strpad(all_channels(c2),2)]
            
            if isempty(nptDir([channel_pairs '.mat'])) || Args.redo_channelpairs
                
                corrcoefs=[]; phases=[]; correlograms={};
                
                
                load channel_matrix
                
                data1 = channel_matrix{c1};
                [rw c] = size(data1);
                data2 = channel_matrix{c2};
                
                d1 = reshape(data1',1,(rw*c));
                d2 = reshape(data2',1,(rw*c));
                
                %calculate number of bins
                steps = floor(((rw*c)-windowSize)/windowStep);
                start =  1:windowStep:steps*windowStep+1;
                endd = start+windowSize-1;
                r = GrayXxcorr(d1,d2,windowSize,windowStep,phaseSize,start,endd);
                lags = [(-1*phaseSize): phaseSize];
      
                %find closest peak to zero lag
                p=zeros(size(r,1),2);
                for kk=1:size(r,1)
                    rr=r(kk,:);
                    %Find index of central peak (positive or negative)
                    for flip = 1 : 2
                        if flip == 2
                            rr = rr * -1;
                        end
                        peak = findpeaks(rr);
                        for ppp = 1 : size(peak.loc,1)
                            if (rr(peak.loc(ppp)) < 0)
                                %set to max phase index
                                peak.loc(ppp) = length(lags);
                            end
                        end
                        [phase, index] = min(abs(lags(peak.loc)));
                        %get real phase value by indexing the location in the lags
                        %get corr coef by indexing the correlogram
                        if ~isempty(peak.loc(index))
                            p_phases(flip) = lags(peak.loc(index));
                            p_corrcoefs(flip) = rr(peak.loc(index));
                        else
                            p_phases(flip) = 0;
                            p_corrcoefs(flip) = 0;
                        end
                    end
                    
                    if abs(p_phases(1)) < abs(p_phases(2))
                        peak_phase = p_phases(1);
                        peak_corrcoef = p_corrcoefs(1);
                    else
                        peak_phase = p_phases(2);
                        peak_corrcoef = p_corrcoefs(2) * -1;
                    end
                    
                    if p_corrcoefs(1) ~= 0 && p_corrcoefs(2) ~= 0
                        %Stores the correlation coef and lag position.
                        p(kk,1) = peak_corrcoef;
                        p(kk,2) = peak_phase;
                    else
                        p(kk,1) = nan;
                        p(kk,2) = nan;
                    end
                end
                
                
                corrcoefs = p(:,1)';
                phases = p(:,2)';

                correlograms = r;
                
    
                write_info = writeinfo(dbstack);
                channel_pairs = [ 'channelpair' num2strpad(all_channels(c1),2) num2strpad(all_channels(c2),2)]
                save(channel_pairs,'corrcoefs','phases','correlograms','write_info')%,'pair_thresholds')
            end
        end
    end
end



