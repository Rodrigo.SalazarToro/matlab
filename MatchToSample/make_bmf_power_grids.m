function make_bmf_power_grids(varargin)

%loads power information written from "write_power_trials" and plot
%information in a grid


Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')


% load alldays
% all_po = ProcessDays(power_obj,'days',alldays,'ml','nlynx','NoSites')
% save all_po all_po
load all_po

%first plot histogram of the spectral peaks
allpeaks = [];
for x = 1 : all_po.data.numSets
    allpeaks = [allpeaks all_po.data.peaks{x,5}];
end

% hist(allpeaks)

%run through all the pairs and find the peak power at each frequency band
%at each epoch for the first recording from each channel

bands = [5 12;13 30;31 55;65 100];

max_powers = cell(1,256);

peaks = cell(1,6);
nrecord = zeros(1,256);
for x = 1 : all_po.data.numSets
    ch = all_po.data.Index(x,1);
    nrecord(ch) = nrecord(ch) + 1; %keep track of the number of time the channle was recorded
    %run through the 6 of the 7 epochs, excluding the full trial
    for e = 1:6
        if isempty(peaks{e})
            peaks{e} = zeros(4,256);
        end
        [~,ii] = intersect(all_po.data.frequencies{x,e},all_po.data.peaks{x,e});
        for b = 1:4
            freq_range = (max(find(round(all_po.data.frequencies{x,e})<=bands(b,1))):min(find(round(all_po.data.frequencies{x,e})>=bands(b,2))));
            
            %see if peaks occur in the frequency range
            p = intersect(freq_range,(ii));
            if ~isempty(p)
                peaks{e}(b,ch) = peaks{e}(b,ch) + 1;
            end
        end
    end
end
for e = 1:6
    for bb = 1 : 4
        peaks{e}(bb,:) = peaks{e}(bb,:) ./ nrecord * 100;
    end
end


%bmf grid
grid = [ ...
    
0, 0,  0,  0,  0,  0,  50, 61, 72, 83, 94,  105, 116, 127, 138, 149, 160, 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  0,  30, 40, 51, 62, 73, 84, 94,  106, 117, 128, 139, 150, 161, 171, 181, 191, 0,   0,   0,   0,   0,   0,   0,   0;
0, 0,  0,  21, 31, 41, 52, 63, 74, 85, 96,  107, 118, 129, 140, 151, 162, 172, 182, 192, 201, 210, 0,   0,   0,   0,   0,   0;
0, 0,  13, 22, 32, 42, 53, 64, 75, 86, 97,  108, 119, 130, 141, 152, 163, 173, 183, 193, 202, 211, 219, 227, 0,   0,   0,   0;
0, 6,  14, 23, 33, 43, 54, 65, 76, 87, 98,  109, 120, 131, 142, 153, 164, 174, 184, 194, 203, 212, 220, 228, 235, 0,   0,   0;
0, 7,  15, 24, 34, 44, 55, 66, 77, 88, 99,  110, 121, 132, 143, 154, 165, 175, 185, 195, 204, 213, 221, 229, 236, 242, 0,   0;
1, 8,  16, 25, 35, 45, 56, 67, 78, 89, 100, 111, 122, 133, 144, 155, 166, 176, 186, 196, 205, 214, 222, 230, 237, 243, 248, 0;
2, 9,  17, 26, 36, 46, 57, 68, 79, 90, 101, 112, 123, 134, 145, 156, 167, 177, 187, 197, 206, 215, 223, 231, 238, 244, 249, 253;
3, 10, 18, 27, 37, 47, 58, 69, 80, 91, 102, 113, 124, 135, 146, 157, 168, 178, 188, 198, 207, 216, 224, 232, 239, 245, 250, 254;
4, 11, 19, 28, 38, 48, 59, 70, 81, 92, 103, 114, 125, 136, 147, 158, 169, 179, 189, 199, 208, 217, 225, 233, 240, 246, 251, 255;
5, 12, 20, 29, 39, 49, 60, 71, 82, 93, 104, 115, 126, 137, 148, 159, 170, 180, 190, 200, 209, 218, 226, 234, 241, 247, 252, 256];


 % Get XY coordinates for all channels
 xy = [];
 cch = 0;
 for column = 1:28
     for row = 1:11
         if grid(row,column) ~= 0
             cch = cch +1;
             xy(cch,1) = row;
             xy(cch,2) = column;
         end
     end
 end
 
 
 for e = 5%1:6
     figure('Position',[150 200 1400 700])
     for b = 1:4
         subplot(2,2,b)
         for channels = 1 : 256
             %              text((xy(channels,2) - .25),(11-xy(channels,1)),num2str(channels),'FontSize',9,'FontWeight','bold')
             if ~isempty(peaks{e}(b,channels))
                 scatter(xy(channels,2),(11-xy(channels,1)),500,peaks{e}(b,channels),'filled')
                 hold on
             end
         end
         hold on
         axis([-1 30 -1 12])
         colorbar
     end
 end




figure
all_peaks = [];
for x = 1 : all_po.data.numSets
    all_peaks = [all_peaks all_po.data.peaks{x,5}]; %this will be the delay 200:800
end

hist(all_peaks,50)