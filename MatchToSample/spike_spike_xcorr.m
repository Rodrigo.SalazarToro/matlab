function [spikexcorr] = spike_spike_xcorr(varargin)

%runs at session level
%called from run_STA_SXC
%calculates the spike bins
Args = struct('groups',[],'trials',[],'mt_obj',[],'bin_width',1);
Args.flags = {'binary','crosscor'};
[Args,modvarargin] = getOptArgs(varargin,Args);

mt = Args.mt_obj;

sesdir = pwd;
%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

%find groups with spikes
groups = nptDir('group*');

cd([sesdir filesep groups(Args.groups(1)).name])
gdir1 = pwd;
%find clusters
clusters1 = nptDir('cluster*');
nclusters1 = size(clusters1,1);

cd([sesdir filesep groups(Args.groups(2)).name])
gdir2 = pwd;
%find clusters
clusters2 = nptDir('cluster*');
nclusters2 = size(clusters2,1);

ccounter = 0;
for c = 1 : nclusters1
    cd([gdir1 filesep clusters1(c).name]);
    %get spikes
    load ispikes.mat
    sp1 = sp;
    for cc = 1: nclusters2
        ccounter = ccounter + 1;
        cd([gdir2 filesep clusters2(cc).name])
        load ispikes.mat
        sp2 = sp;
        
        nbins = size([-200:Args.bin_width:200],2);
        
        spike_xcorr  = zeros(7,nbins);
        spike_counter = zeros(1,7);
        for ttrial = Args.trials
            s_on = sample_on(ttrial);
            s_off = sample_off(ttrial);
            m = match(ttrial);
            sp1_trial = ceil(sp1.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
            spikecount1 = sp1.data.trial(ttrial).cluster.spikecount;
            sp2_trial = ceil(sp2.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
            
            for spikes = 1 : spikecount1
                spike1 = sp1_trial(spikes);
                stimes = sp2_trial - spike1;
                
                stimes([find(stimes <= -200),find(stimes >= 200)]) = [];
                if isempty(stimes)
                    xh = zeros(1,size([-200:Args.bin_width:200],2));
                else
                    xh = hist(stimes,[-200:Args.bin_width:200]);
                end
                
                if spike1 > (s_on - 400) && spike1 <= s_on %fix   
                    spike_xcorr(1,:) = spike_xcorr(1,:) + xh;
                    spike_counter(1) = spike_counter(1) + 1;
                end
                if spike1 > (s_off - 400) && spike1 <= s_off %sample  lock to sample off
                    spike_xcorr(2,:) = spike_xcorr(2,:) + xh;
                    spike_counter(2) = spike_counter(2) + 1;
                end
                if spike1 > s_off && spike1 < (s_off + 401) %early delay (delay400)  
                    spike_xcorr(3,:) = spike_xcorr(3,:) + xh;
                    spike_counter(3) = spike_counter(3) + 1;
                end
                if spike1 > (s_off + 400) && spike1 < (s_off + 801) %sample locked delay (delay800)
                    spike_xcorr(4,:) = spike_xcorr(4,:) + xh;
                    spike_counter(4) = spike_counter(4) + 1;
                end
                if spike1 > (s_off + 200) && spike1 < (s_off + 801) %delay
                    spike_xcorr(5,:) = spike_xcorr(5,:) + xh;
                    spike_counter(5) = spike_counter(5) + 1;
                end
                if spike1 > (m - 400) && spike1 <= m %match locked delay
                    spike_xcorr(6,:) = spike_xcorr(6,:) + xh;
                    spike_counter(6) = spike_counter(6) + 1;
                end
                if spike1 > (s_on - 500) && spike1 <= m %whole trial
                    spike_xcorr(7,:) = spike_xcorr(7,:) + xh;
                    spike_counter(7) = spike_counter(7) + 1;
                end
            end
        end
        
        
        spikexcorr.fix(ccounter,:) = single(spike_xcorr(1,:));
        spikexcorr.sample(ccounter,:) = single(spike_xcorr(2,:));
        spikexcorr.delay400(ccounter,:) = single(spike_xcorr(3,:));
        spikexcorr.delay800(ccounter,:) = single(spike_xcorr(4,:));
        spikexcorr.delay(ccounter,:) = single(spike_xcorr(5,:));
        spikexcorr.delaymatch(ccounter,:) = single(spike_xcorr(6,:));
        spikexcorr.fulltrial(ccounter,:) = single(spike_xcorr(7,:));
        
        spikexcorr.fix_count(ccounter) = single(spike_counter(1));
        spikexcorr.sample_count(ccounter) = single(spike_counter(2));
        spikexcorr.delay400_count(ccounter) = single(spike_counter(3));
        spikexcorr.delay800_count(ccounter) = single(spike_counter(4));
        spikexcorr.delay_count(ccounter) = single(spike_counter(5));
        spikexcorr.delaymatch_count(ccounter) = single(spike_counter(6));
        spikexcorr.fulltrial_count(ccounter) = single(spike_counter(7));
        
        spikexcorr.clusters{ccounter} = single([clusters1(c).name; clusters2(cc).name]); 
    end
end

cd(sesdir)