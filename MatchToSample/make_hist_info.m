function make_hist_info(varargin)

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

N = NeuronalHist;
chnumb = N.chnumb;
pair = 0;
for c1 = 1 : chnumb
    for c2 = (c1+1) : chnumb
        pair = pair + 1;
        %make an index to indicate if the pairs are are on same side of the
        %sulcus or on opposite, and if one of them stradles the sulcus
        
        %opposite side of sulcus == 0
        %same side of sulcus == 1
        %one channel stradles sulcus == 2
        %both channels stradle sulcus == 3
        %Inter-arial == -1
        if (((N.cortex(c1) == 'P' && N.cortex(c2) == 'P') || (N.cortex(c1) == 'F' && N.cortex(c2) == 'F')))
            
            hc1 = N.locs{c1};
            hc2 = N.locs{c2};
            
            
            if hc1 == 'm' && hc2 == 'm' %same side
                h = 1;
                hh = 0; %used to indicate which side of sulcus
            elseif hc1 == 'l' && hc2 == 'l' %same side
                h = 1;
                hh = 1; %used to indicate which side of sulcus
            elseif hc1 == 's' || hc2 == 's' 
                h = 2;
                hh = -1;
            elseif hc1 == 's' && hc2 == 's'%both in sulcus
                h = 3;
                hh = -1;
            elseif hc1 == 'i' || hc2 == 'i' 
                h = 2;
                hh = -1;
            elseif hc1 == 'i' && hc2 == 'i'%both in sulcus
                h = 3;
                hh = -1;
            else
                h = 0;
                hh = -1;
            end
            
        else
            %Inter-arial
            %medial VS dorsal == -1
            %lateral VS dorsal == -2
            %medial VS ventral == -3
            %lateral VS ventral == -4
            %medial VS sulcus == -5
            %lateral VS sulcus == -6
            %sulcus VS dorsal== -7
            %sulcus VS ventral== -8
            %sulcus VS sulcus == -9
            hc1 = N.locs{c1};
            hc2 = N.locs{c2};
            
            
            if (hc1 == 'm' && hc2 == 'd') || (hc1 == 'd' && hc2 == 'm')  %md 
                h = -1;
            elseif (hc1 == 'l' && hc2 == 'd') || (hc1 == 'd' && hc2 == 'l')%ld
                h = -2;
            elseif (hc1 == 'm' && hc2 == 'v') || (hc1 == 'v' && hc2 == 'm') %mv
                h = -3;
            elseif (hc1 == 'l' && hc2 == 'v') || (hc1 == 'v' && hc2 == 'l') %lv
                h = -4;
            elseif hc1 == 's' || hc2 == 's' %ss
                h = -9;
            elseif hc1 == 'i' || hc2 == 'i' %ss
                h = -9;
            end
            hh = -10;
        end
        
        hist(pair,1) = h;
        hist(pair,2) = hh;
        
    end
end

save hist hist