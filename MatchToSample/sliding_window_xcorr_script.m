function sliding_window_xcorr_script(varargin)

%run at monkey level
%final script for the sliding window cross-correlation analysis
%lowpasses (no longer normalizes) each trial from 8-40Hz, saves in lfp2 folder
%calculates average correlogram during the 7 windows
%performs sliding window xcorr (200ms +/- 50ms time lag) with 50 ms steps
%sliding window xcorr is centered at [(sampleoff - 1000ms) : end of trial)]


%NOTE: Clarks data uses channel number and not group number. With parietal
%channels first and then frontal channels.

%Bettys data uses GROUP numbers with original orientation 1:32 for Frontal
%and 33:64 for Parietal

%ide_only indicates that only the identity rule should be run



Args = struct('ml',0,'ide_only',0,'days',[],'sessions',[],'monkey','betty','windowSize',200,'windowStep',50,'lag',50,'lowpass',0,...
    'channelpairs',0,'redo_channelpairs',0,'threshold',0,'fit_gabors',0,...
    'glm',0,'average_corr',0,'average_corr_surrogate',0,'hilbert',0,'fixation',0,'bmf',0);

Args.flags = {'ml','ide_only','windowSize','windowStep','lag','lowpass','channelpairs','redo_channelpairs','threshold','fit_gabors','glm','average_corr','average_corr_surrogate','hilbert','fixation','bmf'};
Args = getOptArgs(varargin,Args);


monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        cd(monkeydir)
        go = 1; %%%TEMPORARY!!!!!!!!!!!!!1
        if ~Args.ml && Args.ide_only && Args.ide_only && (s <= str2double(sessions(size(sessions,1)).name(end)))
            load ideses
            if single(str2double(ses{d}(end))) ~= single(str2double(sessions(s).name(end)))
                go = 0;
            end
        end
        
        if s <= str2double(sessions(size(sessions,1)).name(end)) && go
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            
            %lowpass
            if Args.lowpass
                if Args.ml
                    if Args.bmf
                        lowpass_xcorr_analysis('ml','bmf','lowpasslow',8,'lowpasshigh',40)
                    else
                        lowpass_xcorr_analysis('ml','lowpasslow',8,'lowpasshigh',25)
                    end
                else
                    lowpass_xcorr_analysis('lowpasslow',8,'lowpasshigh',25)
                end
            end
            
            %make all average correlograms
            if Args.average_corr
                cd(sesdir)
                if Args.ml
                    if Args.bmf
                        average_correlograms('ml','bmf')
                    else
                        average_correlograms('ml')
                    end
                else
                    average_correlograms
                end
            end
            
                        %make all average correlograms
            if Args.average_corr_surrogate
                cd(sesdir)
                if Args.ml
                    average_correlograms_surrogate('ml')
                else
                    average_correlograms_surrogate
                end
            end
            
            
            if Args.fit_gabors
                if Args.ml
                    if Args.bmf
                        fit_generalized_gabor_all('ml','bmf')
                    else
                        if Args.fixation
                            fit_generalized_gabor_all('ml','rule',3)
                        else
                            fit_generalized_gabor_all('ml')
                        end
                    end
                else
                    fit_generalized_gabor_all
                end
            end
            
            %write channelpairs
            if Args.channelpairs
                cd(sesdir)
                if Args.ml
                    sliding_window_channelpairs('ml','redo_channelpairs')
                else
                    sliding_window_channelpairs('redo_channelpairs')
                end
            end
            
            %write thresholds for channelpair files
            if Args.threshold
                cd(sesdir)
                if Args.ml
                    sliding_window_thresholds('ml','ide_only',Args.ide_only)
                else
                    sliding_window_thresholds('ide_only',Args.ide_only)
                end
            end
            

            if Args.glm
                if Args.ml
                    glm_analysis('ml');
                else
                    glm_analysis;
                end
            end
            
            if Args.hilbert
                if Args.ml
                    write_hilbert_trials('ml')
                else
                    write_hilbert_trials
                end
            end
        end
    end
    
end

cd(monkeydir)

