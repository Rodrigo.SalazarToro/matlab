function [Index,varargout] = getIndexUnits(mtst,histology,ch,varargin)
% Index(:,1) = day
% Index(:,2) = rule
% Index(:,3) = area number
% Index(:,4) = multi or single info
% Index(:,5) = cell number
% Index(:,6) = min FR
% Index(:,7) = min number of trials (data split in the 9 stims)
% Index(:,8) = anova result to test for difference in the pre-sample period
% between the 9 stimuli
% Index(:,9) = cue pvalue * effect (difference between pre-sample and
% sample)
% Index(:,10) = delay pvalue * effect
% Index(:,11) = match pvalue * effect
% Index(:,12) = cue pvalue * effect ( intersaction with the nine cues)
% Index(:,13) = delay pvalue * effect ( intersaction with the nine cues)
% Index(:,14) = match pvalue * effect ( intersaction with the nine cues)
% Index(:,15) = cue pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for increase FR)
% Index(:,16) = cue pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for decrease FR)
% Index(:,17) = delay pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for increase FR)
% Index(:,18) = delay pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for decrease FR)
% Index(:,19) = match pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for increase FR)
% Index(:,20) = match pvalue * effect (difference between pre-sample and
% sample but test each stimulus independently for decrease FR)
% Index(count,21:26) = pvalues' stat for identity or location effects
% Index(:,27) histo
% Index(:,28:32) tuning index for identity for the 5 epochs
% Index(:,33:37) tuning index for location for the 5 epochs
% Index(:38:39) Number of bins significant for the mutual information for ides
% Index(:40:41) Number of bins significant for the mutual information for locs
% Index(:,42) first significant bin sample onset response


%cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3]; 1st row location


Args = struct('NoHisto',0,'rudeHist',0,'rule',1,'MIprctile',99,'bmf',0);
Args.flags = {'NoHisto','rudeHist','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

rules = {'I','L','C'};
spfile = nptDir('ispikes.mat');
nSfile = nptDir(sprintf('nineStimPSTH%d.mat',Args.rule));
frfile = nptDir(sprintf('FR4epochs%d.mat',Args.rule));
if (~isempty(spfile) && ~isempty(nSfile) && ~isempty(frfile)) || (~isempty(spfile) && ~isempty(nSfile) && Args.bmf)
    % Index column wise: day,rule,cortex,unit,count
    load(spfile.name)
    load(nSfile.name)
    if Args.bmf
        pvalues = nan(1,6);
        ncues = size(A,1);
    else
        load(frfile.name)
        ncues = 9;
    end
    
        bad = []; for c = 1 : ncues; for a = 1 : 3; if length(A{c,a}) == 1 | unique(A{c,a}) == 0; bad = [bad 1]; else bad = [bad 0]; end; end; end
        if length(unique(bad)) > 1 || unique(bad) ~= 1
            Index(1) = 1;
            Index(2) = rules{unique(mtst.data.Index(:,1))};
            
            try
                Index(3) = histology.number(ch);
            catch
                Index(3) = histology.gridPos(ch);
            end
            Index(4) = sp.data.cellname(end);
            Index(5) = nan;
            
            %%
            load(sprintf('nineStimPSTH%d.mat',Args.rule));
            raster = cat(1,A{:,1});
            nspikes = sum(sum(raster(:,1:1800)));
            
            fr1 = nspikes / (1.8*size(raster,1));
            
           load(sprintf('nineStimPSTH%d.mat',Args.rule));
            raster = cat(1,A{:,2});
            nspikes = sum(sum(raster(:,1000:1800)));
            
            fr2 = nspikes / (0.8*size(raster,1));
            
           
            
            Index(6) = min([fr1 fr2]);
            %%
            if ~Args.bmf
                Index(7) = min([size(A{1,1},1) size(A{2,1},1) size(A{3,1},1) size(A{4,1},1) size(A{5,1},1) size(A{6,1},1) size(A{7,1},1) size(A{8,1},1) size(A{9,1},1)]);
                try 
                    Index(8) = basep;
                [cue,delay,match] = CellFRstats(allspikes);
                Index(9) = sign(cue.effect) * cue.p(1);
                Index(10) = sign(delay.effect) * delay.p(1);
                Index(11) = sign(match.effect) * match.p(1);
                Index(12) = sign(cue.effect) * cue.p(3);
                Index(13) = sign(delay.effect) * delay.p(3);
                Index(14) = sign(match.effect) * match.p(3);
                
              
                for c = 1 : 9; if cue.sample(c).p < 0.05 & cue.sample(c).effect > 0; Index(15) = 1; break; else Index(15) = 0;end; end;
                for c = 1 : 9; if cue.sample(c).p < 0.05 & cue.sample(c).effect < 0; Index(16) = 1; break; else Index(16) = 0;end; end;
                
                for c = 1 : 9; if delay.sample(c).p < 0.05 & delay.sample(c).effect > 0; Index(17) = 1; break; else Index(17) = 0;end; end;
                for c = 1 : 9; if delay.sample(c).p < 0.05 & delay.sample(c).effect < 0; Index(18) = 1; break; else Index(18) = 0;end; end;
                
                for c = 1 : 9; if match.sample(c).p < 0.05 & match.sample(c).effect > 0; Index(19) = 1; break; else Index(19) = 0;end; end;
                for c = 1 : 9; if match.sample(c).p < 0.05 & match.sample(c).effect < 0; Index(20) = 1; break; else Index(20) = 0;end; end;
                catch Index(8:20) = nan(13,1);
                end
                if ~isempty(pvalues)
                Index(21:26) = pvalues';
                else
                    Index(21:26) = ones(6,1);
                end
                if Args.NoHisto
                    Index(count,27) = NaN;
                else
                    if isempty(strmatch('area',fieldnames(histology))) || Args.rudeHist
                        
%                         pairhist =  histology.level3(ch);
%                         switch cell2mat(pairhist)
%                             case 'l'
%                                 
%                                 Index(27) = 1;
%                             case 'm'
%                                 Index(27) = 2;
%                             case 'd'
%                                 Index(27) = 3;
%                             case 'v'
%                                 Index(27) = 4;
%                             otherwise
%                                 Index(27) = 99;
%                                 
%                         end
Index(27) = nan;
                    end
                    %% tuning indexes
                    ideCues = [1 2 3; 4 5 6; 7 8 9];
                    locCues = [1 4 7; 2 5 8; 3 6 9];
                    clear ideFR locFR
                    for stim = 1 : 3
                        allidet = [allspikes{ideCues(stim,1)};allspikes{ideCues(stim,2)};allspikes{ideCues(stim,3)}];
                        allloct = [allspikes{locCues(stim,1)};allspikes{locCues(stim,2)};allspikes{locCues(stim,3)}];
                        
                        if ~isempty(allidet);ideFR(1,stim,:) = mean(allidet,1);ideFR(2,stim,:) = std(allidet,[],1);else ideFR(:,stim,:)= nan(2,5);end
                        if ~isempty(allloct);locFR(1,stim,:) = mean(allloct,1);locFR(2,stim,:) = std(allloct,[],1);else locFR(:,stim,:)= nan(2,5);end
                    end
                    %                 maxide = max(ideFR,[],2);
                    %                 minide = min(ideFR,[],2);
                    %
                    %                 maxloc = max(locFR,[],2);
                    %                 minloc = min(locFR,[],2);
                    
                    %                 Index(28:32) = (maxide - minide) ./ minide;
                    %                 Index(33:37) = (maxloc - minloc) ./ minloc;
                    Index(28:32) = std(squeeze(ideFR(1,:,:)),[],1) ./ mean(squeeze(ideFR(2,:,:)),1);
                    Index(33:37) = std(squeeze(locFR(1,:,:)),[],1) ./ mean(squeeze(locFR(2,:,:)),1);
                    %% mutual info on loc and ide for ide rule only
                end
                dims = {'IdePerLoc' 'LocPerIde'};
                ndims= 3;
                
                dims = {'Ide' 'Loc'};
                ndims= 1;
               
                for ep = 1 : 2
                    for di = 1 : 2
                         sigpt= zeros(1,3);
                        for item = 1 : ndims
                            if ndims == 1; item = []; end
                            fil = sprintf('PSTH%s%drule%d.mat',dims{di},item,Args.rule);
                            if ~isempty(nptDir(fil))
                                load(sprintf('PSTH%s%drule%d.mat',dims{di},item,Args.rule),'mi','time')
                                epoch = {find(time >= 0 & time <=500) find(time >= 500 & time <=1300)};
                                miSur = prctile(mi(1:size(mi,1)-1,:),Args.MIprctile,1);
                                if ndims == 1; item = 1; end
                                sigpt(item) = sum(mi(end,epoch{ep}) > miSur(epoch{ep}));
                            else
                                sigpt(item) = nan;
                            end
                        end
                        if ep == 1
                            Index(37+di) = sum(sigpt);
                        else
                            Index(39+di) = sum(sigpt);
                        end
                    end
                end
                [~,firstSigBin] = psthTest(A,reference);
                if ~isempty(firstSigBin)
                    Index(42) = firstSigBin;
                else
                    Index(42) = 0;
                end
            end
        end
    
end
if ~exist('Index'); Index = []; end
%