function [stdTrials,PWTrials,rejectCH,varargout] = ArtRemTrialsLFP_bettyiti(varargin)
% to be run into the session directory
% determine the trials in which any of the channels exceed 12 fold the
% standard deviation in the lfp.
%
% option to use: - 'save' to save the file
%                - 'threshold',x for the x fold STD threshold
%
% Dependencies: getOptArgs, nptDir, getDataDirs, nptReadStreamerFile
%
%   ArtRemTrialsLFP_bettyiti('save','sigma','redo'), this is what was run
%   on Betty incorrect ITI trials


% This is specifically used to run on the iti portion of incorrect trials
% and the correct/incorret itis of betty trials recorded with the nlynx
% hardware. Uses the mtstrial object to determine the incorrect trials.
% The original artifact rejection uses all trials and does not include the
% itis.

%currently only using 'sigma' with threshold 20, lots of trials are kicked out for power
%because there are not very many incorrect trials
%ArtRemTrialsLFP_bettyiti('save','distr')

Args = struct('threshold',20,'save',0,'redo',0,'distr',0,'sigma',0,'badCH',0.04,'freqThresh',100,'plot3D',0,'resample',200,'bmf',0,'detect_transients',0,'trans_thresh',6.75);%6.8
Args.flags = {'save','redo','distr','plot3D','sigma','bmf','detect_transients'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'distr','plot3D','sigma'});

matfile = 'iti_rejectedTrials.mat';
RS = 1000/ Args.resample;

mt = mtstrial('auto','ML','RTfromML','redosetNames');
%get only the specified trial indices (correct and stable)
identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %correct/identity
incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity


[~,cdir] = getDataDirs('lfp','relative','CDNow');
rejfile = nptDir(matfile);
params = struct('tapers',[6 11],'Fs',Args.resample,'fpass',[0 100],'pad',2);
if isempty(rejfile) || Args.redo
    
    files = nptDir('*_lfp.*');
    files = files(incorrtrials); %only look at bad trials
    
    max_trial = 0;
    totpt = 0;
    for t = 1 : size(files,1)
        [lfp,num_channels,~,~,points]=nptReadStreamerChannel(files(t).name,1);
        
        lfp = resample(lfp,Args.resample,1000);
        
        max_trial = max(max_trial,length(lfp));
        totpt = totpt + points;
    end
    [St,f] = mtspectrumc(lfp',params);
    mdata = zeros(num_channels,1);
    maxdata = zeros(num_channels, size(files,1));
    S = zeros(size(St,1),num_channels,size(files,1));
    transTrials = cell(num_channels,1);
    for t = 1 : size(files,1)
        params = struct('tapers',[6 11],'Fs',Args.resample,'fpass',[0 100],'pad',2);
        
        [lfp,num_channels,~,~,~,~] = nptReadStreamerFile(files(t).name);
        
        if Args.bmf
            if natimage
                lfp = lfp(:,(tobj.data.image_on(t)-500):end);
            else
                lfp = lfp(:,(tobj.data.CueOnset(t)-500):(tobj.data.MatchOnset(t)));
            end
        end
        lfp = resample(lfp',Args.resample,1000);
        prelfp = lfp_detrend(lfp)';
        
        if Args.sigma;
            mdata = mdata + sum(prelfp.^2,2);
            maxdata(:,t) = max(abs(prelfp),[],2);
        end
        
        %use to detect transients
        %takes first derivative and then z-scores
        %a threshold is set to detect fast deviations (transients)
        if Args.detect_transients
            dd = diff(prelfp')';
            dd = zscore(dd(:,(3:end)),0,2);
            md = min(dd(:,(3:end))'); %cut off first 2 observations
            tran = find(md <= (Args.trans_thresh * -1));
            for tt = tran
                transTrials{tt} = [transTrials{tt} t];
            end
        end
        
        notok = true;
        while notok && params.pad >= 0
            try
                [S(:,:,t),f] = mtspectrumc(prelfp',params);
                notok = false;
            catch
                params.pad = params.pad-1;
                
                if params.pad < 0 && Args.bmf %try more pads
                    params.pad = 3;
                end
            end
        end
        clear prelfp
        
    end
    noisyt = cell(num_channels,1);
    if Args.sigma
        %         chstd = std(data,0,2);
        chstd = sqrt(mdata/totpt);
        badT =  maxdata > repmat(Args.threshold .* chstd,1,size(maxdata,2));
        for ch = 1 : num_channels
            noisyt{ch} = find(badT(ch,:) ==1);
        end
    end
    clear data
    if Args.distr
        rejectCH = zeros(num_channels,1);
        flimit = find(round(f) == Args.freqThresh);
        trials = [1:size(files,1)];
        badT = cell(num_channels,1);
        kdist = cell(num_channels,1);
        for ch = 1 : num_channels
            fprintf(' Analyzing channel #%d',ch);
            
            all_outliers = zeros(flimit(end),size(trials,2));
            for freq = 1 : flimit(end)
                %                 trials = setdiff(setdiff([1:size(files,1)],rejectTrials),badT{ch});
                
                if ~isempty(trials)
                    data = squeeze(S(freq,ch,trials)); %smooth the data
                    %                     data = squeeze(S(freq+1,ch,trials));
                    [~,outliers,kdist{ch}(freq,:)] = getKurtosisThresh(data,modvarargin{:});
                    all_outliers(freq,outliers) = 1;
                    badT{ch} = unique([badT{ch} trials(outliers)]);
                else
                    badT{ch} = [1:size(files,1)];
                    break
                end
            end
            if length(badT{ch}) + length(noisyt{ch}) > Args.badCH*(size(files,1))
                rejectCH(ch) = 1;
                %                 badT{ch} = [];
            end
            
        end
        rejectCH = find(rejectCH ==1);
        if Args.plot3D
            for ch = 1 : num_channels
                figure;
                
                surf([101:101+size(kdist{ch},2)],fliplr(f(1:flimit(end))),flipud(kdist{ch}))
            end
            %
        end
        
        
    end
    
    if ~Args.distr
        rejectCH = [];
        for ch = 1 : num_channels
            if length(noisyt{ch}) > Args.badCH*(size(files,1))
                rejectCH(ch) = 1;
            end
        end
        rejectCH = find(rejectCH ==1);
    end
    
    PWTrials = cell(num_channels,1);
    stdTrials = cell(num_channels,1);
    alltrials = cell(num_channels,1);
    if Args.sigma;
        stdTrials = noisyt
    end
    
    if Args.distr
        PWTrials = badT;
    end
    
    for ch = 1 : num_channels
        alltrials{ch} = unique([PWTrials{ch} stdTrials{ch}]);
    end
    varargout{1} = alltrials;
    goodCH = setdiff([1:num_channels],rejectCH);
    rejecTrials = [];
    for nch = 1 : length(goodCH)
        ch = goodCH(nch);
        rejecTrials = [rejecTrials PWTrials{ch} stdTrials{ch}];
    end
    rejecTrials = incorrtrials(unique(rejecTrials)); %make sure to index into incorrtrials to get real trial numbers
    varargout{2} = rejecTrials;
    if Args.save
        save(matfile,'rejecTrials')
    end
    
else
    load(matfile)
end
cd(cdir)


