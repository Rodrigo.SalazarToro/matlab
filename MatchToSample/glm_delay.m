function glm_output = glm_delay(varargin)

%prepares model for R

Args = struct('all_number',[],'stim_number',[],'plotpercents',[],'epoch',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);


if Args.epoch == 0
    delay = [18:34];
elseif Args.epoch == 1
    delay = [1:8];
end

points = size(delay,2);
time = [1:points];

all_responses = Args.all_number(:,delay);
all_percents = Args.plotpercents(:,delay);

[s1 s2] = size(all_responses);

response = reshape(all_responses,1,s1*s2)';
all_times = [];
for yy = 1 : 9
    asm = size(all_times,1) + 1;
    all_times = [all_times; time'];
    all_stim_numbers((asm:((asm-1)+s2)),1) = Args.stim_number(yy);
    stims((asm:((asm-1)+s2)),1) = yy;
    cross((asm:((asm-1)+s2)),1)  = (all_percents(yy,:)/100) * Args.stim_number(yy);
end

glm_stimuli(:,1) = all_times;
glm_stimuli(:,2) = stims;
glm_stimuli(:,3) = cross;
glm_stimuli(:,4) = all_stim_numbers;



csvwrite('glm_stimuli.csv',glm_stimuli,1,0)
%run R batch
!R CMD BATCH --slave --no-timing /home/ndotson/R/matlab_glm_code.R


%get R output and place in a structure
fid = fopen('matlab_glm_code.Rout');

%p-vals : DnD test for interaction model(respons~TIME+STIM+TIME*STIM) VS model(response~TIME+STIM)
%       : DnD test for STIM model(response~TIME+STIM) VS model(response~TIME)
%       : walds test for time model(response~TIME)

%coefficients : all three models

e=0;
while e == 0
    interaction = fgetl(fid);
    if ~isnan(str2double(interaction(5:end))) %make sure any messages that appear at top of output are not reported as p-values
        e = 1;
    end
end

glm_output.interaction = interaction(5:end)

stim = fgetl(fid);
glm_output.stim = stim(5:end)

time = fgetl(fid);
glm_output.time = time(5:end)

fgetl(fid); %discard

%interaction coefficients
for ic = 1:18
    interaction_coefs{ic,1} = fgetl(fid);
    glm_output.interaction_coefs{ic,1} = interaction_coefs{ic,1}(7:end);  
end

fgetl(fid); %discard

%no_inter coefficients
for ic = 1:10
    stim_coefs{ic,1} = fgetl(fid);
    glm_output.stim_coefs{ic,1} = stim_coefs{ic,1}(7:end);  
end

fgetl(fid); %discard

%time only coefficients
for ic = 1:2
    time_coefs{ic,1} = fgetl(fid);
    glm_output.time_coefs{ic,1} = time_coefs{ic,1}(6:end);  
end

fclose(fid);

















