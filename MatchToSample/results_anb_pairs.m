function results_anb_pairs(varargin)


%run at monkey level
%gets results for bayes_stimulus_classifier_LOOCV

Args = struct('days',[],'ml',0,'sorted',1);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;

num_days = size(Args.days,2);

ide_results = [];
ide_pvals = [];
for d = 1 : num_days
    cd([monkeydir filesep Args.days{d} filesep 'session01'])
    
    [~,total_pairs,combs,~] = sorted_groups('ml');

    n_pairs = size(total_pairs,2);
    
    if Args.ml
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    else
        mt = mtstrial('auto','redosetNames');
    end
    
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    end
    
    %get results
    cd(['lfp' filesep 'lfp2'])
    
    try
        load nb_performance_ide performance
        ide_results =[ide_results performance];
        
        idpval = [];
        for sigp = 1 : n_pairs
            %calculate p-value (see Quiroga_2009, pg 176 Box 2)
            n = size(total_trials,2);
            K = 3;
            p = 1 / K;
            c_trials = round((n * (performance(sigp) / 100))); %number of correct trials
            pvalue = 0;
            for k = c_trials : n
                P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
                pvalue = pvalue + P;
            end
            
            idpval(sigp) = pvalue;
        end
        
        ide_pvals = [ide_pvals idpval];
    catch
        fprintf(['skipped   ' pwd])
    end
end

cd(monkeydir)

figure
scatter(ide_pvals(ide_pvals <.1),ide_results(ide_pvals <.1))


