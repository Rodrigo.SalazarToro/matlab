function NlynxProcessing_betty(varargin)

%used to make streamer files
%run at day level

Args = struct('sessions',[],'redo',0,'bin_minutes',[],'nlynx2streamer_mat',0);
Args.flags = {'redo','nlynx2streamer_mat'};
[Args,modvarargin] = getOptArgs(varargin,Args);

daydir = pwd;
%find record "sessions"

all_sessions = nptDir('session*');
numb_ses = size(all_sessions,1);

fsep = strfind(daydir,filesep);
monkey = daydir((fsep(end-1)+1):(fsep(end)-1));
day = daydir((fsep(end)+1):end);
sessionname = [monkey day];

if isempty(Args.sessions)
    run_sessions = 1 : numb_ses;
else
    run_sessions = Args.sessions;
end

for s = run_sessions
    cd([daydir filesep all_sessions(s).name])
    sesdir = pwd;
    
    
    descriptor_file = nptDir('*_descriptor.txt');
    descriptor_info = ReadDescriptor(descriptor_file.name);
    electrode_recorded = descriptor_info.channel
    
    %load the list of channels that should be written
    channels = [1:64]; %all channels, this is used for indexing purposes and reflects the number of .ncs files
    
    %find CSC files.
    dirlist = nptDir('*.ncs');
    
    %sort them into numerical order
    CSC =struct2cell(dirlist);
    [~,ind]=sortn(CSC(1,:)); %sort into numerical order
    CSC = CSC(:,ind);
    
    %read events file
    FieldSelectionArray = [1 1 1 1 1 ]; %get everything
    ExtractHeaderValue = 1;%get header also
    ExtractionMode = 1;%all times
    ExtractionModeArray = []; %not needed since we are getting all times
    
    if strcmp(computer,'GLNXA64')
        [events.TimeStamp, events.EventIDs, events.Nttls, events.Extras, events.EventStrings, events.NlxHeader] ...
            = Nlx2MatEV_v3('Events.nev', FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
    else
        [events.TimeStamp, events.EventIDs, events.Nttls, events.Extras, events.EventStrings, events.NlxHeader] ...
            = Nlx2MatEV('Events.nev', FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
    end
    
    %save the events as a matlab structure
    save events events
    
    
    %MonkeyLogic uses 9 and 18 as triggers.
    startTriggerNum = 9; %9 on monkey logic, with strobe on bit 8 it is 137
    stopTriggerNum = 18; %18 on monkey logic, with strobe on bit 8 it is 146
    
    %search for trial triggers
    %determine which trials are correct/incorrect and only process those.
    t = GetNLXTrialTimes_betty(events,startTriggerNum,stopTriggerNum);
    save timing t channels CSC
    
    ntrials = size(t.trial_record,2);
    trial_counter = 0;
    for tt = 1 : ntrials
        trial_counter = trial_counter + 1;
        fprintf('\nTrial #%i\nChannels ',trial_counter)
        
        cd(sesdir)
        filename = [sessionname '0' num2str(s) '.' num2strpad(trial_counter,4)];
        %if trial already exists then skip it
        if isempty(nptDir(filename)) || Args.redo
            channel = 0;
            tic
            for cc = electrode_recorded
                channel = channel + 1;
                fprintf('%i ',cc)
                
                if Args.nlynx2streamer_mat
                    csc_channel = ['CSC' num2str(cc) '.ncs'];
                    csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.stop(tt) + 1500000);
                    d = csc.Samples;
                else
                    [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC_v3(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt)-16000 t.stop(tt)+1500000]); %add 1.5 seconds
                    %                     [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC_v3(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt)-16000 t.stop(tt)+16000]); %add an extra record on to either end to make sure that all data is retreived
                    d = reshape(csc.Samples,1,numel(csc.Samples));
                end
                sampling_rate = csc.SampleFrequencies(1);
                samples = csc.NumberValidSamples(1);
                
                %remove data before start trigger
                d(1:(round((t.start(tt)-csc.TimeStamps(1))*(sampling_rate/1000000))))=[];
                
                if t.start(tt)-csc.TimeStamps(1) < 0
                    fprintf(1,'bad timing');
                end
                
                %keep data, 1500 ms after stop trigger
                d(end - (samples - (round(((t.stop(tt) + 1500000)-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                
                if ((t.stop(tt) + 1500000)-csc.TimeStamps(end)) > 0
                    fprintf(1,'bad timing');
                end
                
                %remove data after stop trigger
                %                     d(end - (csc.NumberValidSamples(1) - (round((t.stop(tt)-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                
                if channel == 1
                    numb_samples = size(d,2);
                    data=NaN(size(electrode_recorded,2),numb_samples);
                end
                
                data(channel,:) = d;
            end
            
            if find(isnan(data))
                warning('preallocation is incorrect')
            end
            toc
            %save streamer files in session folder
            cd(sesdir)
            nptWriteStreamerFile(filename,sampling_rate,data,electrode_recorded'); %scan order must be a column
            
            data=[];
        end
    end
end

cd(daydir)