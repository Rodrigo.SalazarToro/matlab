function run_bmf_coherence_threshold_evaluation(varargin)

Args = struct('days',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl/')
monkeydir = pwd;
num_days = size(Args.days,2);



for d = 1 : num_days
    allthresh = cell(1,5);
    cd ([monkeydir filesep Args.days{d} filesep 'session01' filesep 'lfp' filesep 'cohgrams'])
    
    load idecohthresh
    
    
    
    files = nptDir('cohgram*');
    nfiles = size(files,1);
    if ~isempty(files)
        for f = 1 : nfiles
            load(files(f).name)
            g1 = cohgram.groups{1}(1);
            g2 = cohgram.groups{1}(2);
            if (g1>=82) && (g2>=82)
                for ide = 1: 5
                    th = single(idecohgram.C{ide}(1:48,:)) - single(idecohthresh{ide});
                    th(find(th <= 0)) = 0;
                    th(find(th > 0)) = 1;
                    
                    %determine instances where there are 5 bins in a row above
                    %threshold for each frequency
                    thsmooth = zeros(48,33);
                    for freqs = 1 : 33
                        thsmooth(find(single(smooth(th(:,freqs),5)) == 1),freqs) = 1;
                    end
                    threshidecohgram{ide} = thsmooth;
                    if isempty(allthresh{ide})
                        allthresh{ide} = zeros(48,33);
                    end
                    allthresh{ide} = allthresh{ide} + thsmooth;
                end
            end
        end
        
    end
    figure;surface(allthresh{1}');colorbar
end

cd(monkeydir)











