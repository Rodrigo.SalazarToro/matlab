function [index,data,factors,surro,ntrials] = tuningFreqAve(varargin)
% to be run in the day directory and assumes that it is switching day
% index rows: - pair comb (1 to ...
%             - location (1 to 3)
%             - identity (1 to 3)
%             - window (1 TO 4)
%             - rule (1 or 2)
%             - significance at p<.05
%             - significance at p<.01
%             - significance at p<.001

Args = struct('save',0,'redo',0,'freqRange',[12 25],'pLevel',[1 2 3],'plot',0,'indSpect',0,'SepFigCoh',0,'tuningType','allCuesTuning');
Args.flags = {'save','redo','plot','indSpect','SepFigCoh'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'indSpect','SepFigCoh'});

switch Args.tuningType
    case 'allCuesTuning'
        cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3]; % cueComb(1,:) = location and cueComb(2,:) = identity

    case {'locTuning','ideTuning'}
        cueComb = [1 2 3];
        dime = Args.tuningType;
end

wind = {'presample' 'sample' 'delay1' 'delay2'};
rules = {'IDENTITY' 'LOCATION'};
cohtype = {'cohInter' 'cohPP' 'cohPF'};
cohcolor = {'r' 'b' 'y'};
matfile = sprintf('%sFreqAve.mat',Args.tuningType);

prematfile = nptDir(matfile);
if isempty(prematfile) | Args.redo

    data = [];
    index = [];
    for pvalue = 1 : length(Args.pLevel); surro{pvalue} = [];end
    realComb = 1;
    for coh = 1 : length(cohtype)


        for s = 1 : 2
            cd(sprintf('session0%d',s+1));
            mtst = ProcessSession(mtstrial,'auto');
            rule(s) = unique(mtst.data.Index(:,1));
            cd('lfp')
            load(sprintf('%s.mat',Args.tuningType));

            session(s).data = eval(sprintf('%s',cohtype{coh}));
            if exist('ntrials'); if iscell(ntrials); for stim = 1 : length(ntrials);  ntrialst(stim,rule(s)) = length(ntrials{stim}); end; clear ntrials;else ntrialst = ntrials; end; end
            cd ../..
        end

        ncomb = size(eval(sprintf('%s.coh(1).C',cohtype{coh})),1);

        limit = find(f >= Args.freqRange(1) & f <= Args.freqRange(2));

        for comb = 1 : ncomb

            for stim = 1 : size(cueComb,2)

                for p = 1 : 4
                    for s = 1 : 2
                        if length(size(session(s).data.coh(stim).C)) > 3
                            S12 = squeeze(session(s).data.coh(stim).S12(comb,:,:,p));
                            S1 = squeeze(session(s).data.coh(stim).S1(comb,:,:,p));
                            S2 = squeeze(session(s).data.coh(stim).S2(comb,:,:,p));
                            ntrials(stim,rule(s)) = size(S1,2);
                            if Args.indSpect
                                Cind{stim,p,s} = abs(S12./sqrt(S1.*S2));

                            end
                            S12=squeeze(mean(S12(limit,:),2)); S1=squeeze(mean(S1(limit,:),2)); S2=squeeze(mean(S2(limit,:),2));
                            C{s}=abs(S12./sqrt(S1.*S2));
                        else
                            C{s} = squeeze(session(s).data.coh(stim).C(comb,limit,p));
                            clear ntrials
                            ntrials = ntrialst;
                        end

                    end
                    C1 = C{1}; % session(1).data.coh(stim).C(comb,limit,p);
                    C2 = C{2}; % session(2).data.coh(stim).C(comb,limit,p);

                    for pvalue = 1 : length(Args.pLevel)
                        sur1 = session(1).data.coh(stim).Prob(comb,limit,pvalue,p);
                        sur2 = session(2).data.coh(stim).Prob(comb,limit,pvalue,p);
                        surro{pvalue} = [surro{pvalue} mean(sur1) mean(sur2)];
                    end
                    data = [data mean(C1) mean(C2)];

                    index = [index [repmat(realComb,1,2); repmat(cueComb(:,stim),1,2); repmat(p,1,2); rule; repmat(coh,1,2)]];
                end
            end
            if Args.indSpect
                for s = 1 :2; for stim = 1 : size(size(session(s).data.coh),2);
                        fig1 = figure;
                        for t = 1: size(Cind{stim,p,s},2)
                            for p = 1 : 4;

                                subplot(4,1,p)

                                plot(f,Cind{stim,p,s}(:,t))


                            end
                            pause;clf
                        end;
                        close(fig1)
                    end; end;

            end
            realComb = realComb + 1;
        end


        clear session
    end


    for pvalue = 1 : length(Args.pLevel)
        aboveSur = data > surro{pvalue};
        index(size(index,1)+1,:) = aboveSur;
    end
    if size(index,1) == 9
        factors = {'pairs' 'location' 'identity' 'windows' 'rule' 'cohtype' 'p<.05' 'p<.01' 'p<.001'};
    else
        factors = {'pairs' dime(1:3) 'windows' 'rule' 'cohtype' 'p<.05' 'p<.01' 'p<.001'};
    end
    if Args.save; fprintf('\n saving file %s \n',matfile);  save(matfile,'index','data','factors','surro','ntrials','varargin'); end
else
    fprintf('\n loading file %s \n',matfile);
    load(prematfile.name)
end

if Args.plot
    % ntrials = [35 43 35 36 22 35 35 30 40; 58 53 45 55 43 55 37 37 49];
    for r = 1 : 2
        nstim = size(cueComb,2);
        day = pwd;
        if Args.SepFigCoh
            for coh = 1 : 3; f1{coh} = figure; set(f1{coh},'Name',[day(end-5:end) rules{r} cohtype{coh}]); ymax{coh} = []; end
        else
            f1{1} = figure;
            set(f1{1},'Name',[day(end-5:end) rules{r}])
            ymax{1} = [];
        end


        c = 1;

       
        %         for loc = 1 : 3
        %             for id = 1 : 3
        for stim = 1 : size(cueComb,2)

        for p = 1 : 4
            counter = 1;
            for coh = 1 : 3
                if Args.SepFigCoh; figure(f1{coh}); counter = 1; end
                if size(index,1) == 9
                    ind = find(index(2,:) == cueComb(1,stim) & index(3,:) == cueComb(2,stim)  & index(4,:) == p & index(5,:) == r & index(6,:) == coh);
                else
                    ind = find(index(2,:) == stim & index(3,:) == p & index(4,:) == r & index(5,:) == coh);
                end

                subplot(nstim,4,c); bar([counter: counter + length(ind)-1],data(ind),1,cohcolor{coh}); hold on;
                plot([counter: counter + length(ind)-1],surro{1}(ind),'r.');
                plot([counter: counter + length(ind)-1],surro{2}(ind),'g.');

                counter = length(ind) + counter;

                if Args.SepFigCoh
                    ymax{coh} = [ymax{coh} max(data(ind))];
                    xmax(coh) = counter;
                else
                    ymax{1} = [ymax{1} max(data(ind))];
                    xmax = counter;
                end
            end
            c = c + 1;
        end
        %             end
        %         end

       
        end

        for fg = 1 : length(f1)
            figure(f1{fg})

            for sb = 1 : nstim*4; subplot(nstim,4,sb); axis([0 xmax(fg)+1 0 max(ymax{fg})+0.1]); if sb <=4; title(wind{sb});end; if sb >= (nstim*4-3); xlabel('Frequency [Hz]'); end; end
            stim = 1;
            for sb = [1 : 4 : (nstim*4-3)];
                subplot(nstim,4,sb);
                if size(cueComb,1) == 2; ylabel(sprintf('loc%d id%d',cueComb(1,stim),cueComb(2,stim)));
                else; ylabel(sprintf('%s%d',dime(1:3),cueComb(1,stim))); end
                subplot(nstim,4,sb+3); text(xmax(fg)-round(xmax(fg)/3),max(ymax{fg})-0.2,sprintf('n = %d',ntrials(stim,r)));stim = stim + 1;
            end
        end
    end
end


