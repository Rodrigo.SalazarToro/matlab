function glm_stim_predict = predicts_glm(varargin)

%run at session level
Args = struct('gstimuli',[],'stim_prediction',0,'stims',0,'p_cutoff',1,'logits',0,'rule',1,'pair',[]);
Args.flags = {'stim_prediction','stims','logits'};
Args = getOptArgs(varargin,Args);

%rule = 1(identity), rule = 0(location)
if Args.rule
    rule = 'identity';
else
    rule = 'location';
end

cd ..

N = NeuronalHist;
chnumb = N.chnumb;
glm_stimuli_predict = Args.gstimuli;

combs = [];
cc = 0;
for c1 = 1 : chnumb
    for c2 = (c1+1) : chnumb
        cc = cc + 1;
        combs(cc,1) = N.gridPos(c1);
        combs(cc,2) = N.gridPos(c2);
    end
end


int_pvals = [];
cd('lfp/lfp2')

lfp2dir = pwd;

chpair = (['channelpair' num2strpad(combs(Args.pair,1),2) num2strpad(combs(Args.pair,2),2)]);

cd(rule)
load threshold threshold
cd(lfp2dir)
t = threshold{Args.pair}(1);

load(chpair,'corrcoefs')

binary_corrcoefs = [];
for r = 1 : size(corrcoefs,1)
    for c = 1: size(corrcoefs,2)
        if abs(corrcoefs(r,c)) >= t
            binary_corrcoefs(r,c) = 1;
        else
            binary_corrcoefs(r,c) = 0;
        end
    end
end

cd ../..
mt = mtstrial('auto','ML','RTfromML');
c = mtscpp('auto','ml');
time(:,1) = [1:13]';

counter = 0;
total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML');
for q = total_trials
    counter = counter + 1;
    Args.pair
    size(total_trials,2) - counter
    x = binary_corrcoefs(:,q);
    response(:,1) = x((22:34));
    response(:,2) = ones(1,13);
    
% % %     response(find(response(:,1)),1) = glm_stimuli_predict(1,4);
    
    glm_stimuli_predict((14:26),1) = 1:13;
    glm_stimuli_predict((14:26),2) = 2;
    glm_stimuli_predict((14:26),3) = response(:,1);
    glm_stimuli_predict((14:26),4) = 1;
% % %     glm_stimuli_predict((14:26),4) = glm_stimuli_predict((1:13),4);
    
    csvwrite('glm_stimuli_predict.csv',glm_stimuli_predict,1,0)
    %run R batch
    !R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/matlab_glm_code_full_predict.R
    %%%%!R CMD BATCH --slave --no-timing /home/ndotson/matlab_code/MatchToSample/matlab_gam_code_full.R
    
    %get R output and place in a structure
    fid = fopen('matlab_glm_code_full_predict.Rout');
    
    gstim = fgetl(fid);
    glm_stim_predict(counter) = str2double(gstim(5:end));
    fclose(fid)
end

cd('lfp')