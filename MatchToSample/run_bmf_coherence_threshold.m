function run_bmf_coherence_threshold(varargin)

Args = struct('days',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d} filesep 'session01' filesep 'lfp' filesep 'cohgrams'])
    
    load globalsurcohgram
    f = globalsurcohgram.f{1,1};
    
    for ides = 1 : 6 %run for each sample object
        idect = [];
        for t = 1 : 48 %run for each time bin
            idec = [];
            for perms = 1 : 1000
                idec(perms,:) = globalsurcohgram.C{perms,ides}(t,:);
            end
            %make sure no nan's exist, find out why they occur in the first
            %place
            if sum(sum(isnan(idec))) > 0
                Args.days{d}
            end
            idec(isnan(idec)) = 0;
            [datafit,xx,thresh,rsquare] = fitSurface(idec,f,'Prob',.999999);
            idect(t,:) = thresh;
        end
        if ides == 6
            threshcoh{1} = idect;
        else
            idethreshcoh{ides} = idect;
        end
    end
    save threshcoh threshcoh idethreshcoh
end

cd(monkeydir)








