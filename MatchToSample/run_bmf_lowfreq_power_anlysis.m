function run_bmf_lowfreq_power_anlysis(varargin)

%run at monkey level
%used to compare different ratios

Args = struct('days',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);
all_low10 = [];
all_high10 = [];
all_low5 = [];
all_high5 = [];
counter = 0;
alldays = [];
for d = 1 : num_days
    counter = counter + 1;
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    cd ([daydir filesep sessions(1).name]);
    sesdir = pwd;
    
    [~,~,m,freq] = power_analysis('ml','nlynx');
    
    for x = 1: size(m,2)
        low10 = sum(m{x}(1:max(find(freq<=10)))); %0 to 10 Hz
        high10 = sum(m{x}(min(find(freq>10)):max(find(freq<=55)))); %10 to 55
        
        all_low10 = [all_low10 low10]
        all_high10 = [all_high10 high10];
        
        
        low5 = sum(m{x}(1:max(find(freq<=5)))); %0 to 5 Hz
        high5 = sum(m{x}(min(find(freq>5)):max(find(freq<=55)))); %5 to 55
        
        all_low5 = [all_low5 low5];
        all_high5 = [all_high5 high5];
    end
    alldays = [alldays (ones(1,size(m,2))*counter)];
    
end

all_ratios_5_55;

cd(monkeydir)
