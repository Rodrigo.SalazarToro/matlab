function makeGrids_mtscpp_epochs(varargin)

%run at monkey level
%
%Args
%epoch: fix(1),sample(2),delay1(3),delay2(4)
%rule: indicates rule

Args = struct('rule','identity','epoch',4,'mtscpp_obj','betty_mtscpp');
Args.flags = {};
Args = getOptArgs(varargin,Args);

load(Args.mtscpp_obj)


figure
for x = 1
%     if x == 1;
        [i ii] = get(betty_mtscpp,'Number','ml','correct','stable','identity','snr_cutoff',1.8,'glm_delay','pp')

%     elseif x == 2
%         [i ii] = get(betty_mtscpp,'Number','ml','correct','stable','identity','snr_cutoff',1.8,'pp','in_phase')
%     elseif x == 3
%         [i ii] = get(betty_mtscpp,'Number','ml','correct','stable','identity','snr_cutoff',1.8,'pp','anti_phase')
%     elseif x == 4
%         [i ii] = get(betty_mtscpp,'Number','ml','correct','stable','identity','snr_cutoff',1.8)
%     end
%     subplot(1,2,x)
numb_pairs = i;

channel_depths = betty_mtscpp.data.Index(ii,(5:6));

all_channels = unique(betty_mtscpp.data.Index(ii,(23:24)));

pairs = betty_mtscpp.data.Index(ii,(23:24));

avg_corrcoef_epochs = betty_mtscpp.data.Index(ii,(18));
avg_phase_epochs = betty_mtscpp.data.Index(ii,(19));
    

%PF grid
pf_order = [ ...
    
0; 1; 2; 3; 4; 5;
0; 6; 7; 8; 9;10;
11;12;13;14;15;16;
17;18;19;20;21;22;
0;23;24;25;26;27;
0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    
0;33;34;35;36;37;
0;38;39;40;41;42;
43;44;45;46;47;48;
49;50;51;52;53;54;
0;55;56;57;58;59;
0;60;61;62;63;64];



%get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
        end
    end
end



%plot groups with colors indicating location
for n = all_channels'
    g = xy(n,:);
    for columns = 1:2
        index = find(pairs(:,columns) == n);
        depths = channel_depths(index,columns);
        for nn = 1 : size(find(pairs(:,columns) == n),1)
            %this is the xy coordinates for the channel on the grid
            scatter3(g(1),g(2),(depths(nn)*(-1)),(100),[0 0 0],'filled')
            hold on
            scatter(g(1),g(2),(500),[0 0 0])
            hold on
        end
    end
end


%plot all corrcoefs and phases
for n = 1 : numb_pairs
    
    
    c = avg_corrcoef_epochs(n); %EPOCH 4 (delay 2)
    p = avg_phase_epochs(n);
    
    c1 = pairs(n,1);
    g1 = xy(c1,:);
    
    c2 = pairs(n,2);
    g2 = xy(c2,:);
    
    z1 = channel_depths(n,1) * (-1);
    z2 = channel_depths(n,2) * (-1);
    
    if c >= 0 %in phase RED<-PURPLE->BLUE  ([1 0 0]<-[.5 0 .5]->[0 0 1])
%         if Args.phase
%             if p > 0
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 0 0],'LineWidth',c*10)
%                 hold on
%             elseif p < 0
%                 p = abs(p) / 5;
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 0 1],'LineWidth',c*10)
%                 hold on
%             else
%                 c = abs(c);
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[.5 0 .5],'LineWidth',c*10)
%                 hold on
%             end
%         else
            plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 1 0],'LineWidth',c*5)
            hold on
%         end
    else %out of phase GREEN<-ORANGE->YELLOW
%         if Args.phase
%             if p > 0
%                 c = abs(c);
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[0 1 0],'LineWidth',c*10)
%                 hold on
%             elseif p < 0
%                 c = abs(c);
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 1 0],'LineWidth',c*10)
%                 hold on
%             else
%                 c = abs(c);
%                 plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 .5 0],'LineWidth',c*10)
%                 hold on
%             end
%         else
            c = abs(c);
            plot3([g1(1) g2(1)],[g1(2) g2(2)],[z1 z2],'color',[1 0 0],'LineWidth',c*5)
            hold on
%         end
    end
% %     l1 = location_depths{c1};
% %     l2 = location_depths{c2};
% %     text([g1(1)+.3],[g1(2)],[z1],l1(1:(end-3)),'FontSize',10,'FontWeight','bold')
% %     text([g2(1)+.3],[g2(2)],[z2],l2(1:(end-3)),'FontSize',10,'FontWeight','bold')
% %     
    
end

axis([-.5 14.5 0 7.5])
end
