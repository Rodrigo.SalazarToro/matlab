function run_hilbertphase_entropy_hsfpp(varargin)

%run at session level
%computes Shannon Entropy and normalized to get the synchronization index
%then assigns each spike to field a phase angle and a plv value
%in tass_1998
Args = struct('ml',0,'redo',0,'plot',0);
Args.flags = {'ml','redo','plot'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
NeuroInfo = NeuronalChAssign;

window = 300;
stepsize = 50;
nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?

bins = linspace(0,1,nbins);

steps = [window/2:stepsize:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
nw = size(steps,2);

if Args.ml
    c = mtscpp2('auto','ml');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    [alltr trialorder] = get_trial_list('ml');
    N = NeuronalHist('ml');
else
    c = mtscpp2('auto');
    [~,pairs] = get(c,'Number'); %use all pairs
    mt = mtstrial('auto','redosetNames');
    
    [alltr trialorder] = get_trial_list;
end


%BAND PASSED FILTERED DATA USED IS FROM SAMPLE_OFF-999 (SEE run_bandpass_filtering.m) TO END. MAKE SURE TO
%ACCOUNT FOR THIS WITH THE SPIKES
sample_off = floor(mt.data.CueOffset); %lock to sample off


pair_list = c.data.Index(pairs,(23:24));
group_list = c.data.Index(pairs,(14:15));
npairs = size(pairs,2);

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);

for p = 1 : npairs
    
    if ~Args.ml
        cd([sesdir filesep 'group' num2strpad(NeuroInfo.groups(pair_list(p,1)),4) filesep 'cluster01m']) %only get multi unit
        %get spikes
        load ispikes.mat
        spike1 = sp.data;
        
        cd([sesdir filesep 'group' num2strpad(NeuroInfo.groups(pair_list(p,2)),4) filesep 'cluster01m'])
        load ispikes.mat
        spike2 = sp.data;
    else
        cd([sesdir filesep 'group' num2strpad(pair_list(p,1),4) filesep 'cluster01m']) %only get multi unit
        %get spikes
        load ispikes.mat
        spike1 = sp.data;
        
        cd([sesdir filesep 'group' num2strpad(pair_list(p,2),4) filesep 'cluster01m'])
        load ispikes.mat
        spike2 = sp.data;
    end
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    lfpdata2 = nptDir('*_lfp2.*');
    %     entfile = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2) '.mat'];
    %     if ~exist(entfile,'file') || Args.redo
    load(['relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)])
    for alltypes = 1 %: 19
        tr = alltr{alltypes};
        ntrials = size(tr,2);
        counter = 0;
        for t = tr
            counter = counter + 1;
            sp1 = ceil(spike1.trial(t).cluster.spikes) - (sample_off(t) - 999);%should work for both monkeys
            sp1(find(sp1>1800)) = [];
            sp1(find(sp1<1)) = [];
            sp2 = ceil(spike2.trial(t).cluster.spikes) - (sample_off(t) - 999);
            sp2(find(sp2>1800)) = []; %only include earliest delay
            sp2(find(sp2<1)) = [];
            
            spikes1.sp1{counter} = sp1;
            spikes2.sp2{counter} = sp2;
            
            fprintf(1,['Pair ' num2str(p) ' of ' num2str(npairs) ', Trials to go = ' num2str(ntrials-counter) '\n']);
            for nf = 1 : nfreq
                d = relative_phases(nf,:,t);
                
                max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                
                cc = 0;
                allh = zeros(window,nw);
                for ww = steps
                    cc = cc + 1;
                    allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
                end
                
                allhist = hist(allh,bins) ./ window; %calculate response distribution
                re  = -1 * nansum(allhist.*log2(allhist));
                resp_ent = (max_ent - re) ./ max_ent;
                
                resp_ent_interp = interp1(steps,resp_ent,[1:5000]);
                
                %%
                spikes1.plv{nf,counter} = resp_ent_interp(sp1);
                spikes2.plv{nf,counter} = resp_ent_interp(sp2);
                
                %%
                
                if ~Args.ml
                    %need the phases for each channel, not the relative phase
                    bpdata = [lfpdata2(t).name(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata2(t).name((end-8):end)];
                    load(bpdata)
                    
                else
                    cd bandpass %betty bandpass files are in a special folder
                    bpdata = [lfpdata2(t).name(1:(end-14)) '_bandpass_' num2strpad(all_freq(nf)-2,2) '_' num2strpad(all_freq(nf)+2,2) lfpdata2(t).name((end-8):end)];
                    load(bpdata)
                    cd ..
                end
                if Args.ml
                    [~,ch(1)] = intersect(N.gridPos,pair_list(p,1));
                    [~,ch(2)] = intersect(N.gridPos,pair_list(p,2));
                else
                    ch(1) = pair_list(p,1);
                    ch(2) = pair_list(p,2);
                end
                
                data = data'; %need to do this before and after using hilbert, don't include during the transform
                %compute hilbert transform
                data = hilbert(data);
                %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FINDING THE ANGLE,THE SIGN OF IMAGINARY
                %COMPONENT IS FLIPPED
                %find instantaneous phase angles
                data = angle(data);
                
                data = data';
                d1 = data(ch(1),:);
                d2 = data(ch(2),:);
                
                %%
                spikes1.auto{nf,counter} = d1(sp1);
                spikes1.cross{nf,counter} = d2(sp1);
                
                spikes2.cross{nf,counter} = d1(sp2);
                spikes2.auto{nf,counter} = d2(sp2);
                
            end
        end
        spikes1.ntrials = counter;
        spikes2.ntrials = counter;
        spikehilbertentropy{alltypes} = {spikes1,spikes2};
    end
    groups = pair_list(p,:);
    channel_pairs = [ 'spikehilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    save(channel_pairs,'spikehilbertentropy','all_freq','trialorder','groups')
end

cd(sesdir)















