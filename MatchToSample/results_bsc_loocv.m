function results_bsc_loocv(varargin)


%run at monkey level
%gets results for bayes_stimulus_classifier_LOOCV

Args = struct('days',[],'ml',0,'sorted',1,'ide',0,'loc',0);
Args.flags = {'ml','ide','loc'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;

num_days = size(Args.days,2);

n_pairs = zeros(1,num_days);
n_trials = zeros(1,num_days);
all_results = zeros(1,num_days);
loc_results = zeros(1,num_days);
ide_results = zeros(1,num_days);
all_pvals = zeros(1,num_days);
ide_pvals = zeros(1,num_days);
loc_pvals = zeros(1,num_days);

ide_pairs_performance = [];
loc_pairs_performance = [];
pairs_performance = [];

ide_pairs_pvals = [];
loc_pairs_pvals = [];
pairs_pvals = [];

for d = 1 : num_days
    cd([monkeydir filesep Args.days{d} filesep 'session01'])
    
    [~,total_pairs,combs,~] = sorted_groups('ml');
    
    n_pairs(d) = size(total_pairs,2);
    
    if Args.ml
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    else
        mt = mtstrial('auto','redosetNames');
    end
    
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    end
    
    n_trials(d) = size(total_trials,2);
    
    %get results
    cd(['lfp' filesep 'lfp2'])
    if Args.ide
     
            load bsc_loocv_performance_ide_fast performance all_pair_predictions
            ide_results(d) = performance;
            
            %calculate p-value (see Quiroga_2009, pg 176 Box 2)
            n = size(total_trials,2);
            K = 3;
            p = 1 / K;
            c_trials = round((n * (performance / 100))); %number of correct trials
            pvalue = 0;
            for k = c_trials : n
                P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
                pvalue = pvalue + P;
            end
            
            ide_pvals(d) = pvalue;
            
            ap_perf = ((all_pair_predictions ./ n_trials(d))*100);
            ide_pairs_performance = [ide_pairs_performance ap_perf];
            
            %         idpv = [];
            %         for pvals = 1 : n_pairs(d)
            %             performance = ap_perf(pvals);
            %             n = size(total_trials,2);
            %             K = 3;
            %             p = 1 / K;
            %             c_trials = round((n * (performance / 100))); %number of correct trials
            %             pvalue = 0;
            %             for k = c_trials : n
            %                 P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
            %                 pvalue = pvalue + P;
            %             end
            %
            %             idpv(pvals) = pvalue;
            %         end
            %         ide_pairs_pvals = [ide_pairs_pvals idpv];
            
            
            
            
             load bsc_loocv_performance_ide_fast_fix performance 
            ide_fix_results(d) = performance;
            
            %calculate p-value (see Quiroga_2009, pg 176 Box 2)
            n = size(total_trials,2);
            K = 3;
            p = 1 / K;
            c_trials = round((n * (performance / 100))); %number of correct trials
            pvalue = 0;
            for k = c_trials : n
                P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
                pvalue = pvalue + P;
            end
            
            ide_fix_pvals(d) = pvalue;
            
            
            
%             load bsc_loocv_performance_ide_fast_sample performance 
%             ide_sample_results(d) = performance;
%             
%             %calculate p-value (see Quiroga_2009, pg 176 Box 2)
%             n = size(total_trials,2);
%             K = 3;
%             p = 1 / K;
%             c_trials = round((n * (performance / 100))); %number of correct trials
%             pvalue = 0;
%             for k = c_trials : n
%                 P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
%                 pvalue = pvalue + P;
%             end
%             
%             ide_sample_pvals(d) = pvalue;
            

    elseif Args.loc
        try
            load bsc_loocv_performance_loc_fast performance all_pair_predictions
            loc_results(d) = performance;
            
            %calculate p-value
            n = size(total_trials,2);
            K = 3;
            p = 1 / K;
            c_trials = round((n * (performance / 100))); %number of correct trials
            pvalue = 0;
            for k = c_trials : n
                P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
                pvalue = pvalue + P;
            end
            
            loc_pvals(d) = pvalue;
            
            ap_perf = ((all_pair_predictions ./ n_trials(d))*100);
            
            loc_pairs_performance = [loc_pairs_performance ap_perf];
        catch
            fprintf(['skipped   ' pwd])
        end
        
    else
        
        try
            load bsc_loocv_performance_fast performance all_pair_predictions
            all_results(d) = performance;
            
            %calculate p-value
            n = size(total_trials,2);
            K = 9;
            p = 1 / K;
            c_trials = round((n * (performance / 100))); %number of correct trials
            pvalue = 0;
            for k = c_trials : n
                P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
                pvalue = pvalue + P;
            end
            
            all_pvals(d) = pvalue;
            
            ap_perf = ((all_pair_predictions ./ n_trials(d))*100);
            pairs_performance = [pairs_performance ap_perf];
            
        catch
            fprintf(['skipped   ' pwd])
        end
    end
end

cd(monkeydir)

% save bsc_loocv ide_results loc_results all_results n_pairs n_trials ide_pvals loc_pvals all_pvals

p_cutoff = .05;

figure
subplot(1,3,1)
scatter(n_trials,loc_results)
hold on
scatter(n_trials(loc_pvals < p_cutoff),loc_results(loc_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,2)
scatter(n_trials,ide_results)
hold on
scatter(n_trials(ide_pvals < p_cutoff),ide_results(ide_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,3);
scatter(n_trials,all_results)
hold on
scatter(n_trials(all_pvals < p_cutoff),all_results(all_pvals < p_cutoff),50,[.5 0 0],'filled')

figure

subplot(1,3,1)
scatter(n_pairs,loc_results)
hold on
scatter(n_pairs(loc_pvals < p_cutoff),loc_results(loc_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,2)
scatter(n_pairs,ide_results)
hold on
scatter(n_pairs(ide_pvals < p_cutoff),ide_results(ide_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,3);
scatter(n_pairs,all_results)
hold on
scatter(n_pairs(all_pvals < p_cutoff),all_results(all_pvals < p_cutoff),50,[.5 0 0],'filled')

figure
hist(ide_pairs_performance,50)
