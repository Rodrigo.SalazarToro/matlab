function [Sall,Sallnorm,f,t, varargout] = mkAvepower(obj,inds,varargin)


Args = struct('sortRelCoh',0,'prctTrials',75,'frange',[],'time',[],'rule',1,'plot',0);
Args.flags = {'sortRelCoh','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});



Sall = cell(2,2);
Sallfn = cell(2,2);
Sallnorm = cell(2,2);
% SallnormSTD = cell(2,2);
indCH = [];
c= [1 1];

for ii = 1 : length(inds)
    cd(sprintf('%s/grams/',obj.data.setNames{inds(ii)}))
    cohfile = sprintf('cohgramg%04.0fg%04.0fRule%d.mat',obj.data.Index(inds(ii),10),obj.data.Index(inds(ii),11),Args.rule);
    
    if Args.sortRelCoh
        
        info = load(cohfile,'trials');
        
    end
    for ch = 1 : 2
        if ismember(obj.data.Index(inds(ii),[2 9+ch]),indCH,'rows') ==0
            indCH = [indCH; obj.data.Index(inds(ii),[2 9+ch])]; % check if a site/day is already present
            
            
            load(sprintf('spectrogramgroup%04.0fRule1.mat',obj.data.Index(inds(ii),9+ch)))
            for al = 1 : 2
                Sall{al,ch}(c(ch),:,:) = squeeze(mean(S{al},3))'/(5^2);
                Sallnorm{al,ch}(c(ch),:,:) = squeeze(median(S{al},3)) .* repmat(vecr(f),size(S{al},1),1);
%                 Sallnorm{al,ch}(c(ch),:,:) = squeeze(prctile(S{al},95,3))'./ median(median(median(S{al},3)));
                %                 SallnormSTD{al,ch}(c(ch),:,:) = squeeze(std(S{al},[],3))'./ median(median(median(S{al})));
                %             Sall{al}(ch,ii,:,:) = squeeze(nanmean(S{al},3))' ./ mean(mean(mean(S{al})));
                %             me=nanmean(S{al},3);
                %             maxme = repmat(max(me,[],1),[size(S{al},1) 1]);
                %             Sall{al}(ch,ii,:,:) = me ./ maxme;
            end
            c(ch) = c(ch)+ 1;
        end
    end
end




% figure
%
% for ii = 1: size(Sall{1,1},2);
%     subplot(2,1,1)
%     imagesc(t{al},f,squeeze(Sallnorm{1,1}(ii,:,:)))
%     colorbar
%     ylim([0 50]);
%     caxis([0 50])
%     subplot(2,1,2)
%     imagesc(t{al},f,squeeze(Sallnorm{1,2}(ii,:,:)))
%     colorbar
%     ylim([0 50]);
%     caxis([0 50])
% %     pause
%
% end

if Args.plot
    figure
    
    chlab = {'PPC' 'PFC'};
    fnorm = cell(2,2);
    for ch = 1 : 2
        for al = 1 : 2
            meanAmp = median(median(median(Sall{al,ch}(:,:,:))));
            
            subplot(4,2,(ch-1)*2+ al)
            imagesc(t{al},f,squeeze(median(Sall{al,ch}(:,:,:),1)))
            ylim([8 50])
            if ch ==1; caxis([1 45]); else caxis([1 10]);end
            colorbar
            set(gca,'TickDir','out')
            
%             fnorm{al,ch} = squeeze(median(Sallfn{al,ch}(:,:,:),1)) .* repmat(vecc(f),1,size(Sallfn{al,ch},3));
            fnorm{al,ch} = squeeze(median(Sallnorm{al,ch},1))';
            title(sprintf('median %s; n=%d',chlab{ch},size(Sallnorm{al,ch},1)))
            if al == 1; ylabel('Absolute power (uV^2)'); end
            subplot(4,2,(ch-1)*2+ al+4)
            imagesc(t{al},f,fnorm{al,ch})
            ylim([8 50])
            colorbar
            if ch ==1; caxis([100 1200]); else caxis([100 450]);end
            set(gca,'TickDir','out')
            title(sprintf('median %s; n=%d',chlab{ch},size(Sallnorm{al,ch},1)))
            if al == 1; ylabel('Norm. power'); end
        end
    end
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    
    varargout{1} = fnorm;
end

function mkcohgram(channels, trials)

params = struct('tapers',Args.tapers,'Fs',Args.Fs,'fpass',Args.fpass,'trialave',0);

[C,phi,S12,S1,S2,t,f] = cohgramc(data1,data2,[0.4 0.05],params);
