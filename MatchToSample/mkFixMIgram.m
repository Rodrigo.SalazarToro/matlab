function mkFixMIgram(varargin)
% at the day level

Args = struct('save',0,'redo',0);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


sdir = pwd;

cd grams
gdir = pwd;



for loc = 1 : 3
    files = nptDir(sprintf('migramg*Rule1IdePerLoc%d.mat',loc));
    
    for fi = 1 : length(files)
        clear S1t S2t S3t
        cd(gdir)
        
        newfile = regexprep(files(fi).name,'IdePer','Fix');
        
        if isempty(nptDir(newfile)) || Args.redo
            
            load(files(fi).name)
            ntrials = cellfun(@length,trials);
            tottrials = sum(ntrials);
            cd(sdir)
            
            cd session01
            gr1 = str2double(files(fi).name(8:11));
            gr2 = str2double(files(fi).name(13:16));
            neuroInfo = NeuronalChAssign;
            
            ch = [find(neuroInfo.groups == gr1) find(neuroInfo.groups == gr2)];
            
            mts{1} = mtstrial('auto');
            interFix = mtsgetTrials(mts{1},'CueObj',55);
            cd ..
            if ~isempty(nptDir('session03'))
                cd session03
                
                mts{2} = mtstrial('auto');
                blockFix = mtsgetTrials(mts{2},'CueObj',55);
            else
                mts{2} = [];
                blockFix = [];
            end
            
            
            if sum(ntrials) < length([blockFix interFix])
                
                if tottrials > length(interFix)
                    
                    nt = floor(length(interFix)/3);
                    
                    if min(ntrials) < nt; nt = min(ntrials); end
                    
                    
                    S1t{1} = interFix(1: nt);
                    S2t{1} = interFix(nt+1: 2*nt);
                    S3t{1} = interFix(2*nt+1: 3*nt);
                    
                    leftover1 = ntrials(1) - length(S1t{1});
                    S1t{2} = blockFix(1:leftover1);
                    leftover2 = ntrials(2) - length(S2t{1});
                    S2t{2} = blockFix(leftover1+1:leftover1+leftover2);
                    leftover3 = ntrials(3) - length(S3t{1});
                    S3t{2} = blockFix(leftover1+leftover2+1:leftover1+leftover2+leftover3);
                    cd(sdir)
                    [migr,f,time,C,phi,phistd,Cerr] = migram({mts mts mts},{S1t S2t S3t},ch,'fromCell','Fix','sessions',[1 3]);
                    
                else
                    newset = interFix(randperm(length(interFix)));
                    
                    S1t = newset(1: ntrials(1));
                    S2t = newset(ntrials(1)+1: ntrials(1) + ntrials(2));
                    S3t = newset(ntrials(1)+ntrials(2)+ 1: ntrials(1) + ntrials(2) + ntrials(3));
                    
                    [migr,f,time,C,phi,phistd,Cerr] = migram(mts{1},{S1t S2t S3t},ch,'fromCell','Fix');
                end
                
                cd(gdir)
                save(newfile,'migr','f','time','C','phi','phistd','Cerr','S1t','S2t','S3t');
                display(['Saving ' sdir '/' newfile]);
                
            else
                
                display([pwd 'not enough fixation trials'])
            end
            
        end
    end
end
cd(sdir)

