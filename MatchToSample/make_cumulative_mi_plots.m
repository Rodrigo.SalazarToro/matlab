function make_cumulative_mi_plots

%run in monkey folder
load mi_crossings

%make all crossings plot
pp = [betty_all_crossings.pp; clark_all_crossings.pp];
n_pp = (betty_all_crossings.pp_npairs + clark_all_crossings.pp_npairs);

pf = [betty_all_crossings.pf; clark_all_crossings.pf];
n_pf = (betty_all_crossings.pf_npairs + clark_all_crossings.pf_npairs);

ff = [betty_all_crossings.ff; clark_all_crossings.ff];
n_ff = (betty_all_crossings.ff_npairs + clark_all_crossings.ff_npairs);


subplot(1,2,1)
plot(sum(pp) ./ n_pp,'r','LineWidth',2);
hold on
plot(sum(pf) ./ n_pf,'g','LineWidth',2);
hold on
plot(sum(ff) ./ n_ff,'k','LineWidth',2);

line([9 9],[0 .45],'Color','b','LineWidth',2)
line([19 19],[0 .45],'Color','b','LineWidth',2)
line([33 33],[0 .45],'Color','b','LineWidth',2)

legend('PP, n = 774','PF, n = 1221','FF, n = 477')



%make sig crossing plot
pp_sig = [betty_crossings.pp; clark_crossings.pp];
n_pp_sig = (betty_crossings.pp_npairs + clark_crossings.pp_npairs);

pf_sig = [betty_crossings.pf; clark_crossings.pf];
n_pf_sig = (betty_crossings.pf_npairs + clark_crossings.pf_npairs);

ff_sig = [betty_crossings.ff; clark_crossings.ff];
n_ff_sig = (betty_crossings.ff_npairs + clark_crossings.ff_npairs);


subplot(1,2,2)
plot(sum(pp_sig) ./ n_pp_sig,'r','LineWidth',2);
hold on
plot(sum(pf_sig) ./ n_pf_sig,'g','LineWidth',2);
hold on
plot(sum(ff_sig) ./ n_ff_sig,'k','LineWidth',2);

line([9 9],[0 .45],'Color','b','LineWidth',2)
line([19 19],[0 .45],'Color','b','LineWidth',2) 
line([33 33],[0 .45],'Color','b','LineWidth',2)

legend('PP, n = 168','PF, n = 242','FF, n = 80')



%make bar chart
figure
 bar([1 2 3],[n_pp_sig/n_pp n_pf_sig/n_pf n_ff_sig/n_ff])







