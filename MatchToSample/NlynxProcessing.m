function NlynxProcessing(varargin)

%used to make streamer files
%run at day level

Args = struct('sessions',[],'redo',0,'bin_minutes',[],'nlynx2streamer_mat',0,'extend_trial',0,'extend_time',1500000,'whole_trial_with_iti',0);
Args.flags = {'redo','nlynx2streamer_mat','extend_trial','whole_trial_with_iti'};
[Args,modvarargin] = getOptArgs(varargin,Args);

daydir = pwd;
%find record "sessions"

%load the list of channels that should be written
load electrode_recorded
channels = [1:259]; %all channels, this is used for indexing purposes and reflects the number of .ncs files

all_sessions = nptDir('session*');
numb_ses = size(all_sessions,1);

fsep = strfind(daydir,filesep);
monkey = daydir((fsep(end-1)+1):(fsep(end)-1));
day = daydir((fsep(end)+1):end);
sessionname = [monkey day];

if isempty(Args.sessions)
    run_sessions = 1 : numb_ses;
else
    run_sessions = Args.sessions;
end

for s = run_sessions
    cd([daydir filesep all_sessions(s).name])
    sesdir = pwd;
    
    %determine session type (mts,freeview,natimage,rfm)
    %get list of .txt files
    txt = nptDir('*.txt');
    for t = 1:size(txt,1)
        if strmatch(txt(t).name,'mts.txt')
            type = 1;
        elseif strmatch(txt(t).name,'rfm.txt')
            type = 2;
        elseif strmatch(txt(t).name,'natimages.txt')
            type = 3;
        elseif strmatch(txt(t).name,'freeview.txt')
            type = 4;
        elseif strmatch(txt(t).name,'movie.txt')
            type = 5;
        elseif strmatch(txt(t).name,'ketamine.txt')
            type = 6;
        end
    end
    
    %find nlynx dir
    nlynx_dir = nptDir('2011*');
    cd(nlynx_dir.name)
    ndir = pwd;
    %delete copy.ncs files
    delete *_copy.ncs
    
    %find CSC files.
    dirlist = nptDir('*.ncs');
    
    %sort them into numerical order
    CSC =struct2cell(dirlist);
    [~,ind]=sortn(CSC(1,:)); %sort into numerical order
    CSC = CSC(:,ind);
    
    %read events file
    FieldSelectionArray = [1 1 1 1 1 ]; %get everything
    ExtractHeaderValue = 1;%get header also
    ExtractionMode = 1;%all times
    ExtractionModeArray = []; %not needed since we are getting all times
    
    if strcmp(computer,'GLNXA64') || strcmp(computer,'MACI64')
        [events.TimeStamp, events.EventIDs, events.Nttls, events.Extras, events.EventStrings, events.NlxHeader] ...
            = Nlx2MatEV_v3('Events.nev', FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
    else
        [events.TimeStamp, events.EventIDs, events.Nttls, events.Extras, events.EventStrings, events.NlxHeader] ...
            = Nlx2MatEV('Events.nev', FieldSelectionArray, ExtractHeaderValue, ExtractionMode, ExtractionModeArray );
    end
    
    %save the events as a matlab structure
    save events events
    
    if type == 4 || type == 5 || type == 6
        filename = [sessionname '0' num2str(s) '.bin'];
        writeNlynxBin('filename',filename,'channels',channels,'recorded',electrode_recorded,'CSC',CSC,'start',events.TimeStamp,'nlynx2streamer_mat',1)
    elseif type == 1 || type == 2 || type == 3
        if type == 1
            %MonkeyLogic uses 9 and 18 as triggers.
            startTriggerNum = 137; %9 on monkey logic, with strobe on bit 8 it is 137
            stopTriggerNum = 146; %18 on monkey logic, with strobe on bit 8 it is 146
            incorrectNum = 148; %20 on monkey logic, with strobe on bit 8 it is 148
            correctNum = 149; %21 on monkey logic, with strobe on bit 8 it is 149
        else
            %MonkeyLogic uses 9 and 18 as triggers.
            startTriggerNum = 137; %9 on monkey logic, with strobe on bit 8 it is 137
            stopTriggerNum = 146; %18 on monkey logic, with strobe on bit 8 it is 146
            %if sample was turned on then process trial
            correctNum = 153; %25 on monkey logic, with strobe on bit 8 it is 153
            incorrectNum = [];
        end
        
        %search for trial triggers
        %determine which trials are correct/incorrect and only process those.
        t = GetNLXTrialTimes(events,startTriggerNum,stopTriggerNum,correctNum,incorrectNum);
        save timing t channels CSC
        
        good_trials = find(t.trial_record); %these are all of the correct and incorrect trials
        ntrials = size(good_trials,2);
        trial_counter = 0;
        for tt = good_trials
            trial_counter = trial_counter + 1;
            fprintf('\nTrial #%i\nChannels ',trial_counter)
            
            cd(sesdir)
            filename = [sessionname '0' num2str(s) '.' num2strpad(trial_counter,4)];
            %if trial already exists then skip it
            if isempty(nptDir(filename)) || Args.redo
                
                cd(ndir)
                channel = 0;
                for cc = electrode_recorded
                    channel = channel + 1;
                    
                    fprintf('%i ',cc)
                    if Args.nlynx2streamer_mat
                        csc_channel = ['CSC' num2str(cc) '.ncs'];
                        
                        if Args.whole_trial_with_iti
                            if tt+1 < ntrials
                                csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.start(tt+1)); %go from start to start to get the full ITI (what happens before pre-sample?!)
                            else
                                csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.stop(tt) + Args.extend_time);  %this is used to get a second and a half more data at the end of each trial
                            end
                        elseif Args.extend_trial
                            csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.stop(tt) + Args.extend_time);  %this is used to get a second and a half more data at the end of each trial
                        else
                            csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.stop(tt));
                        end
                        d = csc.Samples;
                    else
                        if strcmp(computer,'GLNXA64')
                            [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC_v3(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt)-16000 t.stop(tt)+16000]); %add an extra record on to either end to make sure that all data is retreived
                            %                       [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples] = Nlx2MatCSC_v3(CSC{1,channels(cc)},[1 1 1 1 1],0,2,[floor((t.start(tt) - r1) / 16000) ceil((t.stop(tt) - r1) / 16000)]);
                        else
                            [csc.TimeStamps, csc.ChannelNumbers, csc.SampleFrequencies, csc.NumberValidSamples, csc.Samples,csc.NlxHeader] = Nlx2MatCSC(CSC{1,channels(cc)},[1 1 1 1 1],1,4,[t.start(tt)-16000 t.stop(tt)+16000]);
                        end
                        d = reshape(csc.Samples,1,numel(csc.Samples));
                    end
                    sampling_rate = csc.SampleFrequencies(1);
                    
                    %%%INCLUDE CODE TO ADD DATA TO THE END OF CORRECT TRIALS
                    
                    %remove data before start trigger
                    d(1:(round((t.start(tt)-csc.TimeStamps(1))*(sampling_rate/1000000))))=[];
                    
                    if t.start(tt)-csc.TimeStamps(1) < 0
                        fprintf(1,'bad timing');
                    end
                    
                    
                    if Args.whole_trial_with_iti
                        if tt+1 < ntrials
                            d(end - (csc.NumberValidSamples(1) - (round(((t.start(tt+1))-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                        else
                            d(end - (csc.NumberValidSamples(1) - (round(((t.stop(tt)+Args.extend_time)-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                        end
                        
                    elseif Args.extend_trial
                        d(end - (csc.NumberValidSamples(1) - (round(((t.stop(tt)+Args.extend_time)-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                    else
                        %remove data after stop trigger
                        d(end - (csc.NumberValidSamples(1) - (round((t.stop(tt)-csc.TimeStamps(end))*(sampling_rate/1000000)))):end)=[];
                    end
                    
                    
                    if ((t.stop(tt) + Args.extend_time)-csc.TimeStamps(end)) > 0
                        fprintf(1,'bad timing');
                    end
                    
                    if channel == 1
                        numb_samples = size(d,2);
                        data=NaN(size(electrode_recorded,2),numb_samples);
                    end
                    
                    data(channel,:) = d;
                end
                
                if find(isnan(data))
                    warning('preallocation is incorrect')
                end
                
                %save streamer files in session folder
                cd(sesdir)
                nptWriteStreamerFile(filename,sampling_rate,data,electrode_recorded'); %scan order must be a column
                
                data=[];
            end
        end
    end
    
    
    
end

cd(daydir)