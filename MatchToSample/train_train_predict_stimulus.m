function train_train_predict_stimulus


%used to iteratively run train_predict_stimulus to best set of pairs, brute
%force

c = mtscpp('auto','ml','train');
mt = mtstrial('auto','ML','RTfromML');
N = NeuronalHist;

npairs = size(c.data.Index,1) / 2;
bestpredict = 0;
for x = 1 : 100000
    x
    randpairs = randperm(npairs);
    origpairs = [1 : npairs];
    localpredict = 0;
    delpairs = [];
    counter = 0;
    for y = randpairs
        counter = counter + 1;
        pairs = origpairs;
        if counter == 1 || isempty(delpairs)
            pairs(y) = [];
        else
            pairs([delpairs y]) = [];
        end
        size(pairs,2)
        y
        
        predict = train_predict_stimulus('stim_prediction','euclid','pair_list',pairs,'obj1',c,'obj2',mt,'hist_info',N);
        clc 
        predict
        
        if counter == 1;
            localpredict = predict;
        end
        
        if predict <= localpredict
            delpairs = [delpairs y];
        else
            localpredict = predict
        end
        
        if predict > bestpredict
            clc
            x
            bestpredict = predict
            pairs;
            save bestprediction bestpredict pairs
        end
        
    end
    
end