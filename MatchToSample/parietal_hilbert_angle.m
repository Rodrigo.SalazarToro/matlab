function parietal_hilbert_angle

cd('/media/raid/data/monkey/betty/091001/session01')

%run at session level
%plots voltage across single trials

sesdir = pwd;
N = NeuronalHist;
hnumbers = N.number;
depths = N.recordedDepth;
parietalch = N.gridPos(N.gridPos > 32);
nparietal = sum(N.gridPos > 32);
nfrontal = sum(N.gridPos < 33);

sg = sorted_groups;
%kick out lateral groups
[~,igg] = intersect(sg,[36 37 41 42 47 48 53 54 58 59 63 64]);
sg(igg) = [];


mt = mtstrial('auto','ML','RTfromML','redosetNames');
identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

cd('lfp/lfp2/hilbert')
hdir = pwd;

%PPgrid
pp_order = [ ...
    0; 1; 2; 3; 4; 5;
    0; 6; 7; 8; 9;10;
    11;12;13;14;15;16;
    17;18;19;20;21;22;
    0;23;24;25;26;27;
    0;28;29;30;31;32];

rr = [1,1,3,3,2,3];


%get XY coordinates for all channels
xy = [];
cch = 0;
ch = 0;
for x = 1:6
    for xx = 1:6
        ch = ch + 1;
        if pp_order(ch) ~= 0
            cch = cch +1;
            xy(cch,1) = xx;
            xy(cch,2) = x;
        end
    end
end

%go through trials
trials = nptdir('*hilbert*');
ntrials = size(trials,1);
g = [];
for n = identity
    load([trials(n).name])
    data = angle(data);
%     data = unwrap(data,[],2);
 
    son =sample_on(n);
    soff = sample_off(n);
    m = match(n);
    counter = 0;
    all_grids = [];
    for p = soff + 400 : soff + 800
        counter = counter + 1;
        grid = zeros(1,4);
        ngrid = zeros(1,4);
        for ch = 1 : nparietal
            if ~isempty(intersect(sg,parietalch(ch)));
                hn = hnumbers(ch);
                
                channel = parietalch(ch) - 32;%subtract frontal for plotting purposes
                grid(xy(channel,2),xy(channel,1)) = data((nfrontal + ch),p);
                
            end
        end
        grid = flipud(rot90(grid,3));
        gzero = find(grid == 0);
        grid(gzero) = nan;
        [dx dy] = gradient(grid(2,:));
        %         contour(grid), hold on, quiver(dxx,dyy), hold off
        
        
        
        subplot(1,2,1)
        quiver(dx,dy)
        axis([0 7 0 7])
        
        
        subplot(1,2,2)
        imagesc(flipud(grid))
        pause(.05)
    end
end


