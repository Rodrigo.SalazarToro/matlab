function [allRT,allGR,pvalue,varargout] = compareCongrUncongrTrials(dir, days,varargin)

Args = struct('save',0,'redo',0,'ML',0,'addName',[]);
Args.flags = {'save','redo','ML'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

fname = sprintf('CongrUncongrTrialRT%s.mat',Args.addName);
cd(dir)

if isempty(nptDir(fname)) || Args.redo
    allRT = cell(2,length(days));
    allGR = cell(2,length(days));
    pvalue = zeros(2,length(days));
    for rule = 1 : 2
        
        for d = 1 :length(days);
            
            cd(days{d});
            if Args.ML
                cd session01
                mts = mtstrial('auto','redoSetNames');
            else
                cd session02
                mts = mtstrial('auto','redoSetNames');
                if mts.data.Index(1,1) ~= rule
                    cd ..
                    
                    cd session03
                    mts = mtstrial('auto','redoSetNames');
                end
            end
            
            ind = [];
            if rule == 1
                for stim =1 : 3;
                    ind = [ind mtsgetTrials(mts,'iMatchSac',stim,'rule',1,'iCueLoc',stim,'BehResp',1,'stable')];
                end
            else
                allobj = unique(mts.data.CueObj);
                allpos = unique(mts.data.CueLoc);
                for stim =1 : 3;
                    ind1 = mtsgetTrials(mts,'MatchObj1',allobj(stim),'rule',2,'CueObj',allobj(stim),'BehResp',1,'stable');
                    ind2 = mtsgetTrials(mts,'MatchPos1',allpos(stim),'rule',2,'CueLoc',allpos(stim),'BehResp',1,'stable');
                    indm1 = intersect(ind1,ind2);
                    
                    ind1 = mtsgetTrials(mts,'MatchObj2',allobj(stim),'rule',2,'CueObj',allobj(stim),'BehResp',1,'stable');
                    ind2 = mtsgetTrials(mts,'MatchPos2',allpos(stim),'rule',2,'CueLoc',allpos(stim),'BehResp',1,'stable');
                    indm2 = intersect(ind1,ind2);
                    
                    ind = [ind union(indm2,indm1)];
                end
                
            end
            goodtrials = mtsgetTrials(mts,'rule',rule,'BehResp',1,'stable');
            rtsame = vecr(mts.data.FirstSac(ind,:));
            rtelse = vecr(mts.data.FirstSac(setdiff(goodtrials,ind)));
            
            allRT{rule,d} = [rtsame rtelse];
            allGR{rule,d} = [ones(1,length(rtsame)) zeros(1,length(rtelse))];
            [pvalue(rule,d),~,~] = anovan( allRT{rule,d},{allGR{rule,d}},'display','off');
            cd(dir)
        end
    end
    
    if Args.save
        save(fname,'allRT','allGR','pvalue','days')
        display(['saving' pwd '/' fname])
    end
else
    load(fname)
end

varargout{1} = days;