





cd /Volumes/raid/data/monkey

obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]},'delay','cont');

[pair,sind,alldata,tv,f,before] = mkAveCohgram(obj,ind,'ML','ndelay',3);


figure; 
% subplot(1,2,1);imagesc(tv{1,1},f,squeeze(prctile(alldata{1,1},50,1))');
% title('Sample Aligned')
% axis([0.2 3.333 10 32])
% caxis([0 0.18])
% subplot(1,2,2);
imagesc(tv{1,2},f,squeeze(prctile(alldata{1,2},50,1))');
hold on

line([3.3 3.3],[10 35],'Color','b')
title('Match Aligned')
axis([1 3.45 0 50])
caxis([0.06 0.2])

xlabel('Time [sec]')
ylabel('Frequency [Hz]')

colorbar
colormap('Hot')