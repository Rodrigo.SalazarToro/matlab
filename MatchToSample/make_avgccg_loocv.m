function [stimulus_avg_ccg, trial_ccg] = make_avgccg_loocv(varargin)

%run in session directory

Args = struct('ml',0,'leave_trial',[],'pairs',[],'stimulus',[],'real_trials',[]);
Args.flags = {'ml','sorted'};
[Args,modvarargin] = getOptArgs(varargin,Args);

ses_dir = pwd;
[~,~,all_pairs,~] = sorted_groups('ml');

cd ([ses_dir filesep ('lfp') filesep ('lfp2')]);
lfp2 = pwd;

cd(ses_dir)

trial_ccg = cell(1,size(Args.pairs,2));
stimulus_avg_ccg = cell(1,size(Args.pairs,2));
pair_counter = 0;
for all_cp = Args.pairs
    pair_counter = pair_counter + 1;
    stim = 0;
    for stims = 1 : max(Args.stimulus)
        stim = stim + 1;
        
        ind = find(Args.stimulus == stims);
        
        [i,ii] = intersect(ind,Args.leave_trial);
        if ~isempty(ii)
            ind(ii) = [];
        end
        
        cd (lfp2);
        if Args.ml
            load(['channelpair' num2strpad(all_pairs(all_cp,1),2) num2strpad(all_pairs(all_cp,2),2)]);
        else
            load(['channelpair' num2strpad(all_pairs(all_cp,1),2) num2strpad(all_pairs(all_cp,2),2)]);
        end
        
        if ~isempty(ind)
            cumulative_xcorr = zeros(9,101);
            for x = ind
                trial = Args.real_trials(x);
                cumulative_xcorr = cumulative_xcorr + correlograms{trial}((25:33),:);
            end
            cx = cumulative_xcorr' ./ (size(ind,2));
            im{stim} = single(cx); %save a single precision
        else
            im{stim} = []; %save a single precision
        end
    end
    
    %make trial ccg
    trial_ccg{1,pair_counter} = (correlograms{Args.real_trials(Args.leave_trial)}((25:33),:))';
    stimulus_avg_ccg{1,pair_counter} = im;
end




