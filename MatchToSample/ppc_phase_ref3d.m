function ppc_phase_ref3d(varargin)


Args = struct('ref',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

%  c = mtscpp2('auto','ml')
load cpp_b
% cpp_b = c

%PF grid
pf_order = [ ...
    0; 1; 2; 3; 4; 5;
    0; 6; 7; 8; 9;10;
    11;12;13;14;15;16;
    17;18;19;20;21;22;
    0;23;24;25;26;27;
    0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    0;33;34;35;36;37;
    0;38;39;40;41;42;
    43;44;45;46;47;48;
    49;50;51;52;53;54;
    0;55;56;57;58;59;
    0;60;61;62;63;64];

%get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
            if cch == Args.ref
                xyref = xy(cch,:);
            end
        end
    end
end

[npp,pp_pairs] = get(cpp_b,'Number','ml','pp','ips',1);

%find pairs that have ref channel as first channel
r1 = find(cpp_b.data.Index(pp_pairs,23) == Args.ref);

%find pairs that have ref channel as second channel and reverse
r2 = find(cpp_b.data.Index(pp_pairs,24) == Args.ref);
mult(pp_pairs(r1)) = 1;
mult(pp_pairs(r2')) = -1;

pp_pairs = [pp_pairs(r1) pp_pairs(r2)];
npp = size(pp_pairs,2);

rdepth1 = cpp_b.data.Index(pp_pairs,5) * -1;
rdepth2 = cpp_b.data.Index(pp_pairs,6) * -1;

figure
for ch = 33 : 64
    if ch == Args.ref
        scatter(xy(ch,1),xy(ch,2),100,[1 0 0],'filled')
        hold on
    else
        scatter(xy(ch,1),xy(ch,2),100,[0 0 0],'filled')
        hold on
    end
end

hold on
for x = 1 : npp
    g1 = xy(cpp_b.data.Index(pp_pairs(x),23),:);
    g2 = xy(cpp_b.data.Index(pp_pairs(x),24),:);
    
    if size(intersect(g1,xyref),2) == 2
        non_ref = g2;
        non_refdepth= rdepth2(x);
        
        ref = g1;
        refdepth = rdepth1(x);
    else
        non_ref = g1;
        non_refdepth = rdepth1(x);
        
        ref = g2;
        refdepth = rdepth2(x);
    end
    
    ph_angle = cpp_b.data.Index(pp_pairs(x),54); %match locked , also try using the real correlogram lag
    peak = cpp_b.data.Index(pp_pairs(x),61);
    
    %keep in degrees
    ph_angle = ph_angle * mult(pp_pairs(x));
    
    
    scatter3(ref(1),ref(2),refdepth,100,[1 0 0],'filled')
    hold on
    
    %introduce some jitter
    if rand > .5
        jit = rand*.5;
    else
        jit = rand*.5 * -1;
    end
    
    
    if abs(peak) > .1
        if ph_angle > 0%postive ref = lag (green)
            scatter3(non_ref(1),non_ref(2),non_refdepth,abs(ph_angle) * 5,[0 1 0],'filled')
        else%negative = ref lead (blue)
            scatter3(non_ref(1),non_ref(2),non_refdepth,abs(ph_angle) * 5,[0 0 1],'filled')
        end
    end
    hold on
end



