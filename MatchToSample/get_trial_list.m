function [alltr trialorder] = get_trial_list(varargin)


%run at session level, gets a list of the trials for all combinations
Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

if Args.ml
    c = mtscpp2('auto','ml');
    [~,pairs] = get(c,'ml','Number'); %use all pairs
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
    
    %get only the specified trial indices (correct and stable)
    alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY
    alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2); %LOCATION
    alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
    [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55)); %get rid of bad fix trials
    alltr{3}(rej) = [];
    alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
    
% % %     %%%%%%% NEED TO INCORPORATE KICKING OUT BAD ITI TRIALS
% % %     cd([sesdir filesep 'lfp']);
% % % if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
% % %     load('iti_rejectedTrials.mat','rejecTrials')
% % %     iti_reject = rejecTrials;
% % % else
% % %     iti_reject = [];
% % % end

if ~isempty(iti_reject)
    [~,baditi] = intersect(incorrtrials,iti_reject);
    incorrtrials(baditi) = [];
end
    
    %get 9 stims
    counter = 4;
    for objs = 1:3
        for locs = 1:3
            counter = counter + 1;
            alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs,'iCueLoc',locs);
        end
    end
    
    %3 ides
    for objs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs);
    end
    
    %3 locs
    for locs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',locs);
    end
    
else
    c = mtscpp2('auto');
    [~,pairs] = get(c,'Number'); %use all pairs
    mt = mtstrial('auto','redosetNames');
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
    
    %get only the specified trial indices (correct and stable)
    alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1); %IDENTITY
    alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','rule',2); %LOCATION
    alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
    [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55)); %get rid of bad fix trials
    alltr{3}(rej) = [];
    alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','rule',1); %incorrect/identity
    
    %get 9 stims
    counter = 4;
    for objs = 1:3
        for locs = 1:3
            counter = counter + 1;
            alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueObj',objs,'iCueLoc',locs);
        end
    end
    
    %3 ides
    for objs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueObj',objs);
    end
    
    %3 locs
    for locs = 1:3
        counter = counter + 1;
        alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','rule',1,'iCueLoc',locs);
    end
end
