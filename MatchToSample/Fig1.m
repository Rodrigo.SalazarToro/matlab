cd clark/060428/session02/
[data,num_channels,sampling_rate,datatype,points]=nptReadStreamerFile('clark06042802.0579');
[lfps , resample_rate]=nptLowPassFilter(data([8 14],:),30000,2000,2,999);% group 3 16 or n=3308 in the obj
[eyes , resample_rate]=nptLowPassFilter(data([1 4 5],:),30000,2000,0.001,999);
figure; 

for ch = 1 : 2; plot(lfps(ch,:) + 1000*(ch-1)); hold on; end
for ch = 1 : 3; plot(eyes(ch,:) + 1000*(ch+1)); hold on; end


