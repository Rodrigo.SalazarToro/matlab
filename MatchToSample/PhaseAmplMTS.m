function [m_norm,m_norm_length,m_norm_phase,fout,varargout] = PhaseAmplMTS(lfp,varargin)
% amplitude for first channel and phase for second channel

Args = struct('srate',200,'fstep',4,'fend',100,'onlyAmplAmpl',0,'fromHilbert',0,'iterations',1000);
Args.flags = {'onlyAmplAmpl','fromHilbert'};
[Args,~] = getOptArgs(varargin,Args);


ntrials = size(lfp,2);
f = 1:Args.fstep : Args.fend;
fout = f(1:end-1)+ Args.fstep/2;
m_norm = zeros(length(f),length(f));
m_norm_phase = zeros(length(f),length(f));
m_raw = zeros(length(f),length(f));
m_norm_length = zeros(Args.iterations,length(f),length(f));
if Args.fromHilbert
    for f1 = 1:length(f)-1
        for f2 = 1:length(f)-1
            if f1 ~= f2
                %             te = nptLowPassFilter(lfp(:,1,1),Args.srate,Args.srate,f1,f1+Args.fstep);
                %             fftAmpl = nan(ntrials,length(te));
                %             fftPhase = nan(ntrials,length(te));
                %             for tr = 1 : ntrials
                [fftAmpl , ~] = nptLowPassFilter(lfp(:,:,1),Args.srate,Args.srate,f(f1),f(f1+1));
                [fftPhase , ~] = nptLowPassFilter(lfp(:,:,2),Args.srate,Args.srate,f(f2),f(f2+1));
                %             end
                fft1 = reshape(fftAmpl,1,ntrials*size(fftAmpl,1));
                fft2 = reshape(fftPhase,1,ntrials*size(fftPhase,1));
                amplitude = abs(hilbert(fft1)); %% theta analytic phase time series, uses EEGLAB toolbox
                phase = angle(hilbert(fft2));
                amplitude2 = abs(hilbert(fft2));
                if Args.onlyAmplAmpl
                    [m_norm_length(f1,f2),m_raw(f1,f2)] = AmplAmplMod(amplitude,amplitude2); % need some work
                else
                    [m_norm(f1,f2),m_norm_length(:,f1,f2),~,m_raw(f1,f2)] = PhaseAmplitudeMod(amplitude,phase,'numsurrogate',Args.iterations);
                end
            end
        end
    end
    m_norm_phase = prctile(m_norm_length,[95 99 99.9 99.95],1);
    varargout{1} = m_raw;
else
    params = struct('tapers',[2 3],'Fs',200,'trialave',0);
    [S1,f]=mtspectrumc(squeeze(lfp(:,:,1)),params);
    [S2,f]=mtspectrumc(squeeze(lfp(:,:,2)),params);
    m_norm = zeros(length(f),length(f));
    for f1 = 1: length(f)
        for f2 = 1: length(f)
            R = corrcoef(S1(f1,:),S2(f2,:));
            m_norm(f1,f2) = R(1,2);
        end
    end
    
    m_norm_length = zeros(Args.iterations,length(f),length(f));
    for ii = 1 : Args.iterations
        nS1 = S1(:,randperm(size(S1,2)));
        for f1 = 1: length(f)
            for f2 = 1: length(f)
                R = corrcoef(nS1(f1,:),S2(f2,:));
                m_norm_length(ii,f1,f2) = R(1,2);
            end
        end
    end
    m_norm_phase = prctile(m_norm_length,[95 99 99.9 99.95],1);
end
