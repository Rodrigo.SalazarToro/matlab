function power_rule_transition(varargin)


%run at session level

Args = struct('ml',0);
Args.flags = {'ml',};
Args = getOptArgs(varargin,Args);

cd('/Volumes/raid/data/monkey/betty/090903/session01')

sesdir = pwd;

ep = 4;
smoothing = 10;

mt = mtstrial('auto','ML','RTfromML','redosetNames');
r1 = mtsgetTrials(mt,'ML','rule',1,'iCueLoc',1);
r1correct = mtsgetTrials(mt,'ML','rule',1,'BehResp',1,'iCueLoc',1);
r1correctstable = mtsgetTrials(mt,'ML','Stable','rule',1,'BehResp',1,'iCueLoc',1);

r2 = mtsgetTrials(mt,'ML','rule',2,'iCueLoc',1);
r2correct = mtsgetTrials(mt,'ML','rule',2,'BehResp',1,'iCueLoc',1);
r2correctstable = mtsgetTrials(mt,'ML','Stable','rule',2,'BehResp',1,'iCueLoc',1);




if min(r1) < min(r2)
    trials = [r1(end-50:end) r2(1:50)];
else
    trials = [r2(end-50:end) r2(1:50)];
end

ntrials = size(trials,2);
perf = zeros(1,ntrials);

[rc rcc] = intersect(trials,[r1correct r2correct]);
perf(rcc) = 1;
perf = round(smooth(perf,smoothing)*100);

rt = round(smooth(mt.data.FirstSac(trials),smoothing));

N = NeuronalHist('ml');
groups = 1 : N.chnumb;
[g,~,~,~] = sorted_groups('ml');
NN = NeuronalChAssign;
[i ii] =intersect(NN.groups,g);
sortgroups = zeros(1,size(groups,2));
sortgroups(ii) = 1;

noisy = noisy_groups;
[~,ii] =intersect(groups,noisy);
rejectgroups = ones(1,size(groups,2));
rejectgroups(ii) = 0; %these are the REJECTED groups

goodgroups = intersect(find(sortgroups),find(rejectgroups));
ngroups = size(goodgroups,2);

cd('lfp/power/')
powerdir = pwd;
p = nptDir('betty*');

real_p = [];
norm_p = [];
tcounter = 0;
for t = trials
    tcounter = tcounter + 1;
    
    cd(powerdir)
    load(p(t).name)
    
    g_counter = 0;
    for g = goodgroups
        g_counter = g_counter + 1;
        real_p{g_counter}(tcounter,:) = power.S{ep}(g,:) .* power.f{ep}';
        norm_p{g_counter}(tcounter,:) = (power.S{ep}(g,:).* power.f{ep}') ./ (sum(power.S{ep}(g,:).* power.f{ep}'));
    end
end
allfreq = power.f{ep};


allsmf = zeros(ntrials,77);
for x = 1:11
    smf = [];
    for fsmooth = 1:77
        smf(:,fsmooth) = smooth((real_p{x}(:,fsmooth)),5);
    end
    
    
    
        imagesc(flipdim(smf(1:90,9:17)',1))
    allsmf = [allsmf + smf];
    
    pause 
    cla

end




