function [glm_output]=cppStimuli_glm(varargin)


%Used by mtsCPP to view the averages percents for all stimuli for ONE
%CHANNEL PAIR

Args = struct('b1',[],'ts1',[],'c1',[],'c2',[],'threshh',[],'sessionname',[],'chnumb',[],'epoch',[],'ml',0,'rule',[],'pair',[],'model',[]);
Args.flags = {'ml'};
[Args,modvarargin] = getOptArgs(varargin,Args);

rules = [{'identity' 'location'}];

if Args.ml
    cd(rules{Args.rule})
    load threshold threshold
    cd ..
else
    load threshold threshold
end

lfp2 = pwd;
cd ..
lfp = pwd;
cd ..
sesdir = pwd;
if Args.ml
    mt = mtstrial('auto','ML','RTfromML');
    newmt = mt;
    ind_alltrials = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'ml','rule',Args.rule);
else
    mt=mtstrial('auto');
    alltrials = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1);
    %cut_off = alltrials(round(size(alltrials,2) * .75));
end
objs =  unique(mt.data.CueObj);
locs =  unique(mt.data.CueLoc);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% % % % % for object = 1:3
% % % % %     for location = 1:3
% % % % %         mt.data.CueObj(ind_alltrials) = mt.data.CueObj(ind_alltrials-1);
% % % % %         mt.data.CueLoc(ind_alltrials) = mt.data.CueLoc(ind_alltrials-1);
% % % % %     end
% % % % % end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CHANGE BACK%%%%%%%%%%%%%%%%%%





%should be a 9xn matrix
plotpercents = [];
all_number = [];
all_number_ind = [];
%Run for all objects
a = 0;
for object = 1:3
    %Run for all locations
    for location = 1:3
        a=a+1;
        %initial index is determined base on object and location
        if Args.ml
            cd(sesdir)
            ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueObj',objs(object),'CueLoc',locs(location),'ml','rule',Args.rule);
            cd(lfp)
            if isempty(ind)
                %nothing to run
                glm_output.interaction = nan;
                glm_output.stim = nan;
                glm_output.time = nan;
                glm_output.interaction_coefs = nan;
                glm_output.stim_coefs = nan;
                glm_output.time_coefs = nan;
                cd(lfp2)
                return
            end
        else
            cd(sesdir)
            
            ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueObj',objs(object),'CueLoc',locs(location));
            
            % % % % % %                                 [v vv] = find(ind <= cut_off);
            % % % % % %                                 ind = ind(vv);
            
            cd(lfp)
        end
        
        percents = [];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%RUN FOR ONE PAIR%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cd(lfp2)

            channel_pair = ([Args.sessionname(1:end-6) num2strpad(Args.c1,2) num2strpad(Args.c2,2)]);

        if (size(ind,2) == 0)
            load(channel_pair, 'corrcoefs','phases')
            corrcoefs = abs(corrcoefs(:,1));
            phases = phases(:,1);
            Args.threshh = 0;
        else
            load(channel_pair, 'corrcoefs','phases')
            corrcoefs = abs(corrcoefs(:,ind));
            phases = phases(:,ind);
        end
        
        %Index threshold for the correct threshold
        if Args.threshh > 0
            
            th = threshold{Args.pair};
            thresholds = th(:,Args.threshh);
            
        else
            thresholds = 0;
        end
        [rows columns] = size(corrcoefs);  %will be the same for the phases
        number=[];
        for yyy = 1:columns
            x=[];
            ntrials = [];
            for xxx = 1:rows %50ms bins w/ a max at 3000ms
                %creates a row vector that contains the number of trials
                %for each bin
                ntrials = cat(2,ntrials,(sum(isnan(corrcoefs(xxx,:)))));
                %Determine if threshold is crossed
                if  corrcoefs(xxx,yyy) > thresholds
                    %If it is then keep the number
                    bb = corrcoefs(xxx,yyy);
                    pp = phases(xxx,yyy);
                    yy = 1;
                else
                    %Else, input NaN
                    bb = NaN;
                    pp = NaN;
                    yy = 0;
                end
                x = cat(1,x, yy);%%%Determines number of trials by not counting the else
            end
            number = cat(2,number,x);
            
            all_number_ind(ind(yyy),:) = x'; %gets binary data for all correct/stable trials
        end
        s= size(number,2);
        stim_number(a) = s;
        all_number = [all_number;number'];
        
        
        
        
        %tttt = percent of ind trials
        %tttt = ((sum(number')/length(ind))*100);
        n =[];
        c = length(ntrials);
        for z = 1 : c
            n = cat(2,n,length(ind));
        end
        numbertrials = n - ntrials;
        number = number';
        tttt = [];
        for zz = 1 : length(numbertrials)
            if numbertrials(1,zz) == 0
                tt = 0;
            else
                tt = ((sum(number(:,zz))/numbertrials(1,zz))*100);
                %Determines acutal percentage of trials based on trials left
            end
            tttt = cat(2,tttt,tt);
        end
        percents = cat(1,percents,tttt);
        plotpercents = cat(1,plotpercents,percents);
    end
end

%use all_number_ind to get response for each location and each identity

for loc = 1:3
    if Args.ml
        cd(sesdir)
        ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueLoc',locs(loc),'ml','rule',Args.rule);
        cd(lfp)
    else
        cd(sesdir)
        ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueLoc',locs(loc));
        cd(lfp)
    end
    
    [si sii] = size(ind);
    
    bin_resp = all_number_ind(ind,:);
    
    cross_loc(loc,:) = sum(bin_resp,1);
    
    stim_number_loc(loc) = sii;
end

for obj = 1:3
    if Args.ml
        cd(sesdir)
        ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueObj',objs(obj),'ml','rule',Args.rule);
        cd(lfp)
    else
        cd(sesdir)
        ind = mtsgetTrials(mt,'BehResp',Args.b1,Args.ts1,'CueObj',objs(obj));
        cd(lfp)
    end
    
    [si sii] = size(ind);
    
    bin_resp = all_number_ind(ind,:);
    
    cross_obj(obj,:) = sum(bin_resp,1);
    stim_number_obj(obj) = sii;
end






%GLM
if Args.model == 1
    glm_output = glm_delay_full('all_number',all_number,'stim_number',stim_number,'plotpercents',plotpercents,'epoch',Args.epoch,'model',Args.model);
else
    glm_output = glm_delay_loc_ide('cross_loc',cross_loc,'stim_number_loc',stim_number_loc,'cross_obj',cross_obj,'stim_number_obj',stim_number_obj,'epoch',Args.epoch,'model',Args.model);
end







