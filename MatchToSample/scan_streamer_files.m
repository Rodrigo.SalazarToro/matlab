
function scan_streamer_files

cd('/media/raid/data/monkey/clark')
monkeydir = pwd;
days = nptdir('06*');
numdays = size(days,1);

badfiles = {};
bf = 0;
for d = 1: numdays
    cd([monkeydir filesep days(d).name]);
    
    sessions = nptdir('session0*');
    numsessions  = size(sessions,1);
    
    for s = 1 : numsessions
        cd([monkeydir filesep days(d).name filesep sessions(s).name]);
        sesdir = pwd;
        
%         system(['rm' ' clark' days(d).name '0' num2str(sesdir(end)) '.snc*']);
        
        trials = [nptdir(['clark' days(d).name '0' num2str(sesdir(end)) '.0*']);nptdir(['clark' days(d).name '0' num2str(sesdir(end)) '.1*'])];
        numtrials = size(trials,1);
        for t = 1 : numtrials
            [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(trials(t).name);
            if isempty(data.rawdata)
                bf = bf + 1;
                badfiles{bf} = trials(t).name;
            end
        end
    end
end

cd(monkeydir)
save badfiles badfiles
    