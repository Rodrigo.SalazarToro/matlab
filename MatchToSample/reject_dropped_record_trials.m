function reject_dropped_record_trials

%run at day level
%rejects trials that have lost records
%run artifact detection and manual_ch_reject.m
%reads lost_trials.mat from session dir (written from nlyx_lost_records.m)

sesdir = pwd;

if exist('lost_trials.mat') ~=0 && exist('artifact_detection_complete.txt') && exist('manual_channel_rejection_complete.txt')
    load lost_trials
    cd('lfp')
    load rejectedTrials
    rj_trials = union(lost_trials,rejecTrials); %determine if trials need to be added or if they are already rejected
    
    %overwrite original files with the changes
    save rejectedTrials PWTrials rejecTrials rejectCH stdTrials
end


cd(sesdir)









