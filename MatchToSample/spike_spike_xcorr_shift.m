function [shiftpredict] = spike_spike_xcorr_shift(varargin)

%runs at session level
%called from run_STA_SXC
%calculates the PSTH and then uses the cross product to calculte the shift
%predictor
Args = struct('groups',[],'trials',[],'mt_obj',[],'bin_width',1);
Args.flags = {'binary','crosscor'};
[Args,modvarargin] = getOptArgs(varargin,Args);

mt = Args.mt_obj;

sesdir = pwd;
%get trial timing information
sample_on = floor(mt.data.CueOnset);
sample_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);

ntrials = size(Args.trials,2);

%find groups with spikes
groups = nptDir('group*');

cd([sesdir filesep groups(Args.groups(1)).name])
gdir1 = pwd;
%find clusters
clusters1 = nptDir('cluster*');
nclusters1 = size(clusters1,1);

cd([sesdir filesep groups(Args.groups(2)).name])
gdir2 = pwd;
%find clusters
clusters2 = nptDir('cluster*');
nclusters2 = size(clusters2,1);

ccounter = 0;
for c = 1 : nclusters1
    cd([gdir1 filesep clusters1(c).name]);
    %get spikes
    load ispikes.mat
    sp1 = sp;
    for cc = 1: nclusters2
        ccounter = ccounter + 1;
        cd([gdir2 filesep clusters2(cc).name])
        load ispikes.mat
        sp2 = sp;
        
        spike1_psth = cell(1,6);
        spike2_psth = cell(1,6);
        counter = 0;
        for ttrial = Args.trials
            counter = counter + 1;
            s_on = sample_on(ttrial);
            s_off = sample_off(ttrial);
            m = match(ttrial);
            sp1_trial = ceil(sp1.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
            sp2_trial = ceil(sp2.data.trial(ttrial).cluster.spikes); %ceil to avoid spikes a t = 0
            
            %fix
            spike1_psth{1}(counter,:) = hist(sp1_trial(find(sp1_trial > s_on-400 & sp1_trial <= s_on)),[s_on-400:s_on]);
            spike2_psth{1}(counter,:) = hist(sp2_trial(find(sp2_trial > s_on-400 & sp2_trial <= s_on)),[s_on-400:s_on]);
            
            %sample  lock to sample off
            spike1_psth{2}(counter,:) = hist(sp1_trial(find(sp1_trial > (s_off-400) & sp1_trial <= s_off)),[s_off-400:s_off]);
            spike2_psth{2}(counter,:) = hist(sp2_trial(find(sp2_trial > (s_off-400) & sp2_trial <= s_off)),[s_off-400:s_off]);
            
            %early delay (delay400)
            spike1_psth{3}(counter,:) = hist(sp1_trial(find(sp1_trial > s_off & sp1_trial <= s_off+400)),[s_off:s_off+400]);
            spike2_psth{3}(counter,:) = hist(sp2_trial(find(sp2_trial > s_off & sp2_trial <= s_off+400)),[s_off:s_off+400]);
            
            %sample locked delay (delay800)
            spike1_psth{4}(counter,:) = hist(sp1_trial(find(sp1_trial > s_off+400 & sp1_trial <= s_off+800)),[s_off+400:s_off+800]);
            spike2_psth{4}(counter,:) = hist(sp2_trial(find(sp2_trial > s_off+400 & sp2_trial <= s_off+800)),[s_off+400:s_off+800]);
            
            %delay
            spike1_psth{5}(counter,:) = hist(sp1_trial(find(sp1_trial > s_off+200 & sp1_trial <= s_off+800)),[s_off+200:s_off+800]);
            spike2_psth{5}(counter,:) = hist(sp2_trial(find(sp2_trial > s_off+200 & sp2_trial <= s_off+800)),[s_off+200:s_off+800]);
            
            %match locked delay
            spike1_psth{6}(counter,:) = hist(sp1_trial(find(sp1_trial > m-400 & sp1_trial < m)),[m-400:m]);
            spike2_psth{6}(counter,:) = hist(sp2_trial(find(sp2_trial > m-400 & sp2_trial < m)),[m-400:m]);
            
            %whole trial
% % % %             spike1_psth{7}(counter,:) = hist(sp1_trial(find(sp1_trial > s_on-500 & sp1_trial < m)),[s_on-499:m-1]);
% % % %             spike2_psth{7}(counter,:) = hist(sp2_trial(find(sp2_trial >  s_on-500 & sp2_trial < m)),[s_on-499:m-1]);
        end
        %compute cross correlation
        for xx = 1 : 6
            s1 = sum(spike1_psth{xx});
            s2 = sum(spike2_psth{xx}); 
            shiftpredict{ccounter,xx} = xcorr(s1,s2,50) ./ counter; %divide by number of trials
        end
    end
end


cd(sesdir)