function [r,varargout] = get(obj,varargin)
%   mtstrial/get Get function for MTSTRIAL objects
%   [numevents,dataindices] = get(obj,'Number') returns
%   information about the all the trials. the format is column-wise:
%   1. Cummulative session
%   2. Cummulative trial
%   3. Real trial
%
%   The following arguments can be used:
%
%   - SessionBySession: returns the trials belonging to each session.
%
%   - Location: returns all the trials where the cue was at the specificed location
%
%   - Identity: returns all the trials for teh specified cued object
%
%   All the arguments above can be combined with
%
%   - 'ThresholdP',T,'Parameter','P','Condition','C', which enables to select a set of
%     trials according to the threshold T applied to the parameter P and with a certain
%     condition C.
%
%   P has to be one of the following parameter:
%
%      cueobject = obj.data.CueObj;
%      cuelocation = obj.data.CueLoc;
%      matchobject1 = obj.data.MatchObj1;
%      matchposition1 = obj.data.MatchPos1;
%      matchobject2 = obj.data.MatchObj2;
%      matchposition2 = obj.data.MatchPos2;
%      cueonset = obj.data.CueOnset;
%      cueoffset = obj.data.CueOffset;
%      matchonset = obj.data.MatchOnset;
%      behavioralresponses = obj.data.BehResp;
%      firstsac = obj.data.FirstSac;
%      lastsac = obj.data.LastSac;
%      numbersac = obj.data.NumSac;
%      SetIndex = obj.data.Index;
%
%   Object level is session object
%
%
%   Dependencies: @mtstrial
%
Args = struct('Number',0,'SessionBySession',0,'ObjectLevel',0,'ThresholdP',[],'Parameter',[],'Condition','>','Location',[],'Identity',[],'rule',[],'block',[]);
Args.flags = {'Number','SessionBySession','Fix','ObjectLevel'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

cueobject = obj.data.CueObj;
cuelocation = obj.data.CueLoc;
matchobject1 = obj.data.MatchObj1;
matchposition1 = obj.data.MatchPos1;
matchobject2 = obj.data.MatchObj2;
matchposition2 = obj.data.MatchPos2;
cueonset = obj.data.CueOnset;
cueoffset = obj.data.CueOffset;
matchonset = obj.data.MatchOnset;
behavioralresponses = obj.data.BehResp;
firstsac = obj.data.FirstSac;
lastsac = obj.data.LastSac;
numbersac = obj.data.NumSac;
% type = obj.data.Type;
SetIndex = obj.data.Index;


if Args.Number
    
    if Args.SessionBySession
        % return total number of events
        r = length(obj.data.setNames);
        % find the transition point between sessions
        sdiff = diff(SetIndex(:,2));
        stransition = [0; vecc(find(sdiff))];
        stransition = [stransition; length(SetIndex)];
        rind = [];
        cvalue = 0;
        for idx = 1:r
            value = vecc((stransition(idx)+1):stransition(idx+1));
            cvalue = cvalue(end) + value;
            % grab the indices corresponding to session idx
            rind = [rind; [repmat(idx,length(value),1) value cvalue]];
        end
        
    elseif Args.Location
        LocTrial = find(cuelocation == Args.Location);
        rind = SetIndex(LocTrial,[2 3 4]);
        
    elseif Args.Identity
        ObjTrial = find(cueobject == Args.Identity);
        rind = SetIndex(ObjTrial,[2 3 4]);
    else % case where all the data are given in block
        rind = [ones(size(SetIndex,1),1) SetIndex(:,3:4)];
        
    end
    
    if ~isempty(Args.ThresholdP) % needs to be done the idea is that one can use the threshold in addition to the following selection of data.
        measure = eval(Args.Parameter);
        [rawindices,colindices] = eval(sprintf('find(measure %s %d)',Args.Condition,Args.ThresholdP));
        rindtemp = SetIndex(rawindices,[2 3 4]);
        rindlast = [];
        for i = 1 : size(rind,1)
            I = find(rindtemp(:,2) == rind(i,2));
            if ~isempty(I)
                rindlast = [rindlast; rind(i,:)];
            end
        end
        
    else
        rindlast = rind;
    end
    if ~isempty(Args.rule)
        indt = find(SetIndex(:,1) == Args.rule);
        
        [c, ia, ib] = intersect(rindlast(:,2),indt);
        rindlast = rindlast(ia,:);
    end
    
    if ~isempty(Args.block)
        
        jump = [0; find(diff(SetIndex(:,1)) ~= 0); length(SetIndex(:,1))];
        if Args.block + 1 <= length(jump)
            indt = [jump(Args.block) + 1 : jump(Args.block + 1)];
        else
            indt = [];
        end
        [c, ia, ib] = intersect(rindlast(:,2),indt);
        rindlast = rindlast(ia,:);
    end
    
    if Args.SessionBySession
        r = length(unique(rind(:,1)));
    elseif ~Args.SessionBySession
        r = length(rindlast);
    else
        r = 1;
    end
    varargout(1) = {rindlast};
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

