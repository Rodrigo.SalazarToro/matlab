function obj = plot(obj,varargin)
%   @mtstrial/plot plots histograms of reaction times or number of saccades
%   or plots the eye traces over time or on XY from mtstrial object
%
%   OBJ = plot(OBJ,N) plots the eye traces over time of trial N.
%
%   The following optional input arguments are valid:
%
%     - Space : plots the eye traces on XY axes.
%
%     - lastsacOffset: uses the offset of the last saccadic to quantify the
%       reaction time. Otherwise, it uses the saccade onset of the first
%       saccade.
%
%     - percentCR: plots the percent correct responses
%
%     - NumSac : specifies the number of saccades during the behavioral
%                response. To be used with the 'Hist' argument.
%
%     - ObjPos : plots an histogram of reaction times for each object identity
%                and location. If combined with 'percentCR' plots the performance.
%
%     - Hist : plots an histograms of the specified data.
%
%     - bins/binsEdges can be used to characterize the histograms.
%
%   For the 'Hist' and 'ObjPos' arguments, an additional argument can be used to plot session by session
%   ('SessionBySession').
%
%   Dpendencies: mtstrial, @eyemvt, @mstrial/get.
%
%

Args = struct('Space',0,'percentCR',0,'lastsacOffset',0,'NumSac',0,'Hist',0,'ObjPos',0,'bins',20,'binsEdges',[],'performance',0,'ML',0);
Args.flags = {'Space','percentCR','lastsacOffset','Hist','NumSac','ObjPos','performance','ML'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'Space','percentCR','lastsacOffset','Hist','NumSac','ObjPos'});

[numevents,dataindices,Mark] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = find(dataindices(:,1) == n);
    limit = dataindices(ind,2);
    session = obj.data.setNames(unique(obj.data.Index(limit,2)));
    display(session(:))
else
    limit = dataindices(:,3);
end

if Args.Hist % staying here
    
    nCR = find(obj.data.BehResp(limit) == 1);
    nIR = find(obj.data.BehResp(limit) == 0);
    perf = length(nCR) / (length(nCR) + length(nIR)) * 100;
    
    
    if Args.NumSac
        for R = 1: 2
            RESP{R} = find(obj.data.BehResp(limit) == (R-1));
            [H(R,:) N(R,:)] = hist(obj.data.NumSac(RESP{R}),[0:1:4]);
            
        end
        
        bar(N(1,:)+0.2,H(1,:),0.4,'b')
        hold on
        bar(N(2,:)-0.2,H(2,:),0.4,'g')
        legend('Incorrect responses','Correct responses')
        hold off
        
        xlabel('Number of saccadic responses')
        ylabel('#')
        title(sprintf('% CR : %d',perf))
        
        
    elseif Args.ObjPos % needs to be tested
        [ObjLib] = ProcessSessionMTS('ObjectLibrary',varargin2{:});
        pdir = pwd;
        cd(obj.data.setNames{unique(obj.data.Index(limit,2))})
        mt = mtstrial('auto');
        for ObjI = 1 : size(ObjLib,1)
            ObjTrial = find(obj.data.CueObj(limit) == ObjI+3);
            
            
            if ~isempty(ObjTrial)
                
                if (Args.percentCR)
                    tottrial = length(mtsgetTrials(mt,'CueObj',ObjI+3,varargin2{:}));
                    %                     tottrial = length(find(obj.data.BehResp(limit(ObjTrial)) == 0 | obj.data.BehResp(limit(ObjTrial)) == 1));
                    %                     cortrial = length(find(obj.data.BehResp(limit(ObjTrial)) == 1));
                    cortrial = length(mtsgetTrials(mt,'BehResp',1,'CueObj',ObjI+3,varargin2{:}));
                    Objperf(ObjI) = cortrial / tottrial;
                else
                    
                    if (Args.lastsacOffset)
                        RT = obj.data.LastSac;
                    else
                        RT = obj.data.FirstSac;
                    end
                    
                    CRlat = find(obj.data.BehResp(limit(ObjTrial)) == 1);
                    IRlat = find(obj.data.BehResp(limit(ObjTrial)) == 0);
                    RTselCR = RT(limit(ObjTrial(CRlat)));
                    RTselIR = RT(limit(ObjTrial(IRlat)));
                    
                    Objperf((ObjI-1)*2 + 1) = mean(RTselCR(~isnan(RTselCR)));
                    Objperf((ObjI-1)*2 + 2) = mean(RTselIR(~isnan(RTselIR)));
                    
                end
            else
                if (Args.percentCR)
                    Objperf(ObjI) = NaN;
                else
                    Objperf((ObjI-1)*2 + 1) = NaN;
                    Objperf((ObjI-1)*2 + 2) = NaN;
                end
            end
            
        end
        
        for Pos = 0 : 5
            PosTrial = find(obj.data.CueLoc(limit) == Pos);
            if ~isempty(PosTrial)
                if (Args.percentCR)
                    tottrial = length(mtsgetTrials(mt,'CueLoc',Pos,varargin2{:}));
                    %                     tottrial = length(find(obj.data.BehResp(limit(PosTrial)) == 0 | obj.data.BehResp(limit(PosTrial)) == 1));
                    %                     cortrial = length(find(obj.data.BehResp(limit(PosTrial)) == 1));
                    cortrial = length(mtsgetTrials(mt,'BehResp',1,'CueLoc',Pos,varargin2{:}));
                    Posperf(Pos+1) = cortrial / tottrial;
                else
                    
                    if (Args.lastsacOffset)
                        RT = obj.data.LastSac;
                    else
                        RT = obj.data.FirstSac;
                    end
                    
                    CRlat = find(obj.data.BehResp(PosTrial) == 1);
                    IRlat = find(obj.data.BehResp(PosTrial) == 0);
                    RTselCR = RT(limit(PosTrial(CRlat)));
                    RTselIR = RT(limit(PosTrial(IRlat)));
                    Posperf(Pos*2+1) = mean(RTselCR(~isnan(RTselCR)));
                    Posperf(Pos*2+2) = mean(RTselIR(~isnan(RTselIR)));
                end
            else
                if (Args.percentCR)
                    Posperf(Pos+1) = NaN;
                else
                    Posperf(Pos*2+1) = NaN;
                    Posperf(Pos*2+2) = NaN;
                end
            end
        end
        
        if (Args.percentCR)
            tottrial = length(find(obj.data.BehResp(limit) == 0 | obj.data.BehResp(limit) == 1));
            cortrial = length(find(obj.data.BehResp(limit) == 1));
            GeneralPerf = cortrial / tottrial;
            labely = '% Correct Responses';
            legends = 'Mean %CR';
        else
            
            if (Args.lastsacOffset)
                RT = obj.data.LastSac(limit);
                labely = 'LastSacResp [ms]';
            else
                RT = obj.data.FirstSac(limit);
                labely = 'FirstSacResp [ms]';
            end
            
            CRlat = find(obj.data.BehResp(limit) == 1);
            IRlat = find(obj.data.BehResp(limit) == 0);
            RTselCR = RT(CRlat);
            RTselIR = RT(IRlat);
            GeneralPerf = mean(RTselCR(~isnan(RTselCR)));
            GeneralPerf = [GeneralPerf mean(RTselIR(~isnan(RTselIR)))];
            
            legends = {'Correct responses'; 'Incorrect responses'};
        end
        
        allperf = [Objperf Posperf];
        
        
        if (Args.percentCR)
            bar(allperf)
            % axis([0 30 0 1])
        else
            
            bar([1:2:size(allperf,2)-1],allperf(:,[1:2:size(allperf,2)-1]),0.5,'b')
            hold on
            bar([2:2:size(allperf,2)],allperf(:,[2:2:size(allperf,2)]),0.5,'g')
        end
        hold on
        plot(repmat(GeneralPerf,size(allperf,2),1),'--')
        
        legend(legends)
        labelx = sprintf('1 to %d : objects; %d to %d : locations(1-3 270 deg; 4-6 90 deg)',size(Objperf,2),size(Objperf,2)+1,size(Objperf,2)+size(Posperf,2));
        ylabel(labely);
        xlabel(labelx);
        hold off
    else
        
        if (Args.lastsacOffset)
            RT = obj.data.LastSac(limit);
            labelx = 'LastSacResp [ms]';
        else
            
            RT = obj.data.FirstSac(limit);
            labelx = 'FirstSacResp [ms]';
        end
        
        for R = 1: 2
            RESP{R} = find(obj.data.BehResp(limit) == (R-1));
            if (isempty(Args.binsEdges))
                [H(R,:) N(R,:)] = hist(RT(RESP{R}),Args.bins);
            else
                [H{R} N{R}] = hist(RT(RESP{R}),Args.binsEdges);
                H = H';
                N = N';
            end
        end
        if (isempty(Args.binsEdges))
            plot(N',H')
        else
            co = {'b';'g'};
            for R = 1 : 2
                plot(Args.binsEdges,H{R}',co{R})
                hold on
            end
        end
        legend('Incorrect responses','Correct responses')
        xlabel(labelx)
        ylabel('#')
        title(sprintf('Percent CR : %d',perf))
    end
    
elseif Args.performance
    sdir = pwd;
    cd(session{1})
    clark = findstr(session{1},'clark');
    betty = findstr(session{1},'betty');
    
    
    if ~isempty(clark) % || str2num(sdir(end-5:end-4)) < 9
        
        [perf,ti] = getperformance('getThreshCross','plot','fit',varargin2{:});
        
    elseif ~isempty(betty)
        
        [perf,ti] = getperformance('getThreshCross','plot','fit','ML',varargin2{:});
    end
    cd(sdir)
else
    
    
    if (~isempty(Args.NumericArguments))
        trialn = dataindices(Args.NumericArguments{1},2);
        triale = dataindices(Args.NumericArguments{1},3);
    else
        trialn = dataindices(1,2);
        triale = dataindices(1,3);
    end
    
    sessionn = dataindices(Args.NumericArguments{1},1);
    cd(obj.data.setNames{sessionn})
    
    if Args.Space
        
        inifile = dir('*.ini');
        [INI,status] = ReadIniCognitive(inifile.name);
        
        %%%%%%%%%
        screen_width = 40; % fixed and once measured by someone
        screen_height = 30; % fixed and once measured by someone
        if Args.ML; 
            file = nptDir('*.bhv'); bhv = bhv_read(sprintf('%s/%s',pwd,file.name)); 
            INI.ScreenWidth = bhv.ScreenXresolution;
            INI.ScreenHeight = bhv.ScreenYresolution;
            INI.Eccentricity = 5; % quick fix for now
            INI.ObjectWindowSize = 3;% quick fix for now
            INI.Size = 5;% quick fix for now
        end
            
            
        display_width = INI.ScreenWidth;
        display_height = INI.ScreenHeight;
        xFix = display_width / 2;
        yFix = display_height / 2;
        onedeg =  display_width / screen_width;
        
        
        Eccent = INI.Eccentricity * onedeg;
        Swindow = INI.ObjectWindowSize * onedeg;
        Fwindow = INI.Size * onedeg;
        
        eobj = eyes('auto','Pixels');
        plot(eobj,triale,'XY');
        hold on
        
        yC = sqrt(Eccent^2 * (1 - ((sin(deg2rad(60)))^2)));
        xC = sqrt(Eccent^2 * (1 - ((cos(deg2rad(60)))^2)));
        
        
        Cx(5) = xFix + xC;
        Cy(5) = yFix - yC;
        Cx(6) = xFix - xC;
        Cy(6) = yFix - yC;
        Cx(4) = xFix;
        Cy(4) = yFix + Eccent;
        Cx(2) = xFix + xC;
        Cy(2) = yFix + yC;
        Cx(3) = xFix - xC;
        Cy(3) = yFix + yC;
        Cx(1) = xFix;
        Cy(1) = yFix - Eccent;
        
        halfSw = Swindow/2;
        halfFw = Fwindow/2;
        line([xFix - halfFw xFix - halfFw],[yFix-halfFw yFix + halfFw]);
        line([xFix + halfFw xFix + halfFw],[yFix-halfFw yFix + halfFw]);
        line([xFix - halfFw xFix + halfFw],[yFix-halfFw yFix - halfFw]);
        line([xFix - halfFw xFix + halfFw],[yFix+halfFw yFix + halfFw]);
        
        n = 1;
        OI(1) = obj.data.MatchObj1(trialn);
        OI(2) = obj.data.MatchObj2(trialn);
        for  center = [obj.data.MatchPos1(trialn)+1 obj.data.MatchPos2(trialn)+1]
            line([Cx(center) - halfSw Cx(center) - halfSw],[Cy(center)-halfSw Cy(center) + halfSw]);
            line([Cx(center) + halfSw Cx(center) + halfSw],[Cy(center)-halfSw Cy(center) + halfSw]);
            line([Cx(center) - halfSw Cx(center) + halfSw],[Cy(center)-halfSw Cy(center) - halfSw]);
            line([Cx(center) - halfSw Cx(center) + halfSw],[Cy(center)+halfSw Cy(center) + halfSw]);
            
            text(Cx(center),Cy(center),sprintf('Stimulus %d',OI(n)))
            if (obj.data.Index(trialn,1) == 1) & obj.data.CueObj(trialn) == OI(n) % Index(:,1) == 1 if Identity
                plot(Cx(center),Cy(center),'go')
            elseif (obj.data.Index(trialn,1) == 2) & obj.data.CueLoc(trialn) == center % Index(:,1) == 2 if Location
                plot(Cx(center),Cy(center),'go')
            end
            
            n = n + 1;
            
        end
        legend('Eye trace','End of saccade','Windows locations')
        
        text(xFix-20,yFix+40,sprintf('Beh resp: %d',obj.data.BehResp(trialn)))
        hold off
        axis([250 display_width-250 125 700])
    else
        eobj = eyes('auto',varargin{:});
        emvt = eyemvt('auto','redosetNames',varargin2{:});
        plot(emvt,triale,'Degree');
        % plot(eobj,triale,'Degree')
        
        hold on
        x1 = obj.data.CueOnset(trialn);
        x2 = obj.data.CueOffset(trialn);
        x3 = obj.data.MatchOnset(trialn);
        
        line([x1 x1],[-1 1]);
        text(x1,0,'Cue Onset');
        line([x2 x2],[-1 1]);
        text(x2,0,'Cue Offset');
        line([x3 x3],[-1 1]);
        text(x3,0,'Match Onset');
        %         text(0,1,sprintf('Beh resp: %d',obj.data.BehResp(trialn)))
        %         text(x3,-1,sprintf('First sac: %d [ms]',round(obj.data.FirstSac(trialn))))
        %         text(x3,0,sprintf('Last sac: %d [ms]',round(obj.data.LastSac(trialn))))
        xlabel(sprintf('Beh resp: %d;  First sac: %d [ms];  Last sac: %d [ms]',obj.data.BehResp(trialn),round(obj.data.FirstSac(trialn)),round(obj.data.LastSac(trialn))));
        hold off
    end
end

