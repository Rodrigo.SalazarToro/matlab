function obj = mtstrial(varargin)
% @mtstrial Constructor function for MTSTRIAL class
%   OBJ = mtstrial('auto') attempts to create a mtstrial object by
%   using the files *.seq *.mrk *.ini and *timing.mat (which are assumed to be
%   in the current directory). The  object contains all the
%   following fields for each trial:
%
%   Session.CueObj - cue object
%   Session.CueLoc - cue location
%   Session.MatchObj1 - matched object1
%   Session.MatchPos1 - matched position1
%   Session.MatchObj2 - matched object2
%   Session.MatchPos2 - matched position 2
%   Session.CueOnset - Cue onset
%   Session.CueOffset - Cue offset
%   Session.MatchOnset - Match onset
%   Session.BehResp - Behavioral response (2 timed out 1 for correct and 0 for incorrect)
%   Session.FirstSac - First saccadic response onset
%   Session.LastSac - last saccadic response offset
%   Session.NumSac - Number of saccadic responses
%   Session.Index - has all the information about each trial; column wise:
%      1. Rule (identity = 1; location = 2)
%      2. Cummulative session
%      3. Cummulative trial
%      4. Real trial number according to the recordings
%      5. Trial type (Go = 1; NoGo = 0)
%
%   In addition, the following arguments can be used:
%
%   - NumOfSac: Creates the object with the specified number of saccades.
%               Dep. ProcessSessionMTS.
%
%   - RTLimit: Creates the object with reaction times above or equal to the specified threshold.
%              Dep. ProcessSessionMTS.
%
%   The data are raw vectors of all the trials for a given session.
%
%   Dependencies: ProcessSessionMTS,nptdata.

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'redosetNames',0,'Nlynx',0);
Args.flags = {'Auto','redosetNames','Nlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'mtstrial';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'mtst';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
            if Args.redosetNames
                obj.data.setNames = {pwd};
                
            end
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)
deldir = nptDir('delFiles','CaseInsensitive');
bhvfile = nptDir('*.bhv');
markerfile = nptDir('*mrk*','CaseInsensitive');
if Args.Nlynx && ~isempty(bhvfile)
    if isempty(nptDir('mts.txt'))
        dnum = 0;
    else
        dnum = 3;
    end
else
    if ~isempty(bhvfile) && ~isempty(deldir)
        BHV = bhv_read(sprintf('%s%s%s',pwd,filesep,bhvfile.name));
        if strcmp('MTS',BHV.ExperimentName)
            dnum = 3;
        else
            dnum = 0;
        end
    elseif ~isempty(markerfile)
        MTSfiles = [nptDir('*timing.mat','CaseInsensitive') nptDir('*.seq','CaseInsensitive') markerfile(1)];
        dnum = length(MTSfiles);
    else
        dnum = 0;
        
    end
end
% check if the right conditions were met to create object
if(dnum==3)
    %     markerfile = nptDir('*.mrk','CaseInsensitive');
    %     [MARKERS,records] = ReadMarkerFile(markerfile.name);
    %     if (~isempty(records))
    % this is a valid object
    % these are fields that are useful for most objects
    [Session,ObjI] = ProcessSessionMTS(varargin{:});
    data = Session;
    data.numSets = Session.Index(end,3); % nbr of trial
    data.setNames{1} = pwd;
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
    %     else
    %         obj = createEmptyObject(Args);
    %     end
else
    % create empty object
    fprintf('One of the three required files is missing or not identified... Check out!!!!\n');
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% useful fields for most objects


% these are object specific fields

data.CueObj = [];
data.CueLoc = [];
data.MatchObj1 = [];
data.MatchPos1 = [];
data.MatchObj2 = [];
data.MatchPos2 = [];
data.CueOnset = [];
data.CueOffset = [];
data.MatchOnset = [];
data.BehResp = [];
data.FirstSac = [];
data.LastSac = [];
data.NumSac = [];

% data.Type = '';
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
