function dist_p = make_clark_grids(varargin)

%run at session level
Args = struct('plot',0,'hist_pics',0);
Args.flags = {'plot','hist_pics'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.hist_pics
    N = NeuronalHist('pics');
else
    N = NeuronalHist;
end

p = zeros(1,N.chnumb);
f = zeros(1,N.chnumb);
for ch = 1 : N.chnumb
    if strncmpi(N.cortex(ch),'P',1) 
        p(ch) = 1;
    elseif strncmpi(N.cortex(ch),'F',1)
        f(ch) = 1;
    end
end
numb_p = find(p);
numb_f = find(f);

grid_rec = N.gridPos;
cd ..

cd('histology')

load hist_pf
%determine where the sulcus is
sulcus_pf = find(hist_pf == 2);

load hist_pp
%determine where the sulcus is
sulcus_pp = find(hist_pp == 2);

%64 channel grids are the same for parietal and frontal
xy = get_grid_coordinates;

cd(sesdir)

if Args.plot
    %make pp grid
    figure
    for y = 1:64
        scatter(xy(y,1),xy(y,2),'k','filled');
        hold on
    end
end

%draw sulcus
xy1 = xy(sulcus_pp,1);
[s ss] = sort(xy1);
x_reorder = xy1(ss);

xy2 = xy(sulcus_pp,2);
y_reorder = xy2(ss);

if Args.plot
    plot(x_reorder,y_reorder)
end

%color in recording sites
for np = numb_p
    grid = xy(N.gridPos(np),:);
    if Args.plot
        scatter(grid(1),grid(2),'r','filled');
        hold on
        text((grid(1)+.2),grid(2),num2str(np),'FontSize',16,'FontWeight','bold')
    end
    
    %calculate distance of each point from sulcus
    %first find the 2 closests points on sulcus line
    for ns = 1 : size(sulcus_pp,1)
        d(ns) = sqrt( (xy1(ns) - grid(1))^2 + (xy2(ns) - grid(2))^2);
    end
    
    [s ss]=sort(d);
    
    iter = 0;
    wrong = 1;
    while wrong
        iter = iter + 1;
        P = [grid(1) grid(2)]; %recording site
        Q1 = [xy1(ss(1)) xy2(ss(1))]; %point one
        Q2 = [xy1(ss(iter+1)) xy2(ss(iter+1))]; %point two
        
        %make sure Q1 and Q2 span P in the x or y plane
        
        
        if ~isempty(intersect(P(1),[min(Q1(1),Q2(1)):max(Q1(1),Q2(1))])) || ~isempty(intersect(P(2),[min(Q1(2),Q2(2)):max(Q1(2),Q2(2))]))
            if ~isempty(intersect(P(1),[min(Q1(1),Q2(1)):max(Q1(1),Q2(1))]))
                range1 = size([min(Q1(1),Q2(1)):max(Q1(1),Q2(1))],2);
            else
                range1 = 1;
            end
            if ~isempty(intersect(P(2),[min(Q1(2),Q2(2)):max(Q1(2),Q2(2))]))
                range2 = size([min(Q1(2),Q2(2)):max(Q1(2),Q2(2))],2);
            else
                range2 = 1;
            end
            if range1 > 1 || range2 > 1
                wrong = 0;
            end
        end
    end
    dist_p(np) = abs(det([Q2-Q1;P-Q1])) /norm(Q2-Q1); %calculate distance from a point to a line
    
    %this tells you how close each recording site is to the estimate of the sulcus location
end
axis([0 9 0 9])
