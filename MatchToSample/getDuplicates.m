function [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind,varargin)
% output:
% nsites = number of sites sampled multiple times
% pairs: the unique (no duplicates) pairs with group number and depth
% pairNumberInd: redindexing of the pairs according to unique pairs
% grs: group number of all pairs sites
% depths: depths of all pairs sites
Args = struct('obj','mtscohInter');
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

switch Args.obj
    case 'mtscohInter'
        grs = zeros(length(ind),2);
        depths = zeros(length(ind),2);
    case 'psth'
        grs = zeros(length(ind),1);
        depths = zeros(length(ind),1);
        day = zeros(length(ind),1);
        unittype = zeros(length(ind),1);
        nSUA = zeros(length(ind),1);
end
switch Args.obj
    
    case 'mtscohInter'
        
        for ii = 1 : length(ind)
            
            grs(ii,:) = obj.data.Index(ind(ii),10:11);
            cd(obj.data.setNames{ind(ii)})
            if isempty(findstr('clark',obj.data.setNames{ind(ii)}))
                
                cd session01
                
            else
                cd session02
                
            end
            neuroInfo = NeuronalChAssign;
            clear chs
            for c = 1 : 2
                chs(c) = find(ismember(neuroInfo.groups,grs(ii,c)) == 1);
            end
            depths(ii,:) = neuroInfo.recordedDepth(chs);
            if ~isempty(findstr('clark',obj.data.setNames{ind(ii)}))
                % code to make sure that clark data are always independent
                
                grs(ii,:) = grs(ii,:) + obj.data.Index(ind(ii),2)*100;
            end
        end
        
        thegrs = unique(grs);
        % duplicates sites
        % nsites = cell(length(thegrs),1);
        nsites = zeros(64,10);
        for ii = 1 : length(thegrs)
            [row,col] = find(thegrs(ii) == grs);
            col = unique(col);
            if length(col) ~= 1; error; end
            
            [b,~,n] = unique(depths(row,col));
            
            [ns,~] = hist(n,[1:length(b)]);
            
            [nsites(thegrs(ii),:),~] = hist(ns,[1:10]);
        end
        
        [pairs,~,pairNumberInd] = unique([grs depths],'rows');
        
        
        [ns,~] = hist(n,[1:length(pairs)]);
        
        npairs = hist(ns,[1:5]);
        
    case 'psth'
        for ii = 1 : length(ind)
            
            grs(ii) = str2num(obj.data.setNames{ind(ii)}(55:58));
            cd(obj.data.setNames{ind(ii)}(1:49))
            
            neuroInfo = NeuronalChAssign;
            chs = find(neuroInfo.groups == grs(ii));
            
            depths(ii) = neuroInfo.recordedDepth(chs);
            if ~isempty(findstr('clark',obj.data.setNames{ind(ii)}))
            day(ii) = obj.data.Index(ind(ii),1);
            
            end
            unittype(ii) = obj.data.Index(ind(ii),4);
            nSUA(ii) = obj.data.setNames{ind(ii)}(end-1);
        end
        [~,~,pairNumberInd] = unique([grs depths day unittype nSUA],'rows');
        nsites = [];
        pairs = [];
        
        npairs = [];
end