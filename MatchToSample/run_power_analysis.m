function all_peak_frequencies = run_power_analysis(varargin)

%run at monkey level
%calculates mean power finds peaks for for each channel
%uses power trials written with write_power_trials.m

Args = struct('ml',0,'nlynx',0,'days',[],'sessions',[],'ff',0,'pp',0);
Args.flags = {'ml','nlynx','ff','pp'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);
all_peak_frequencies = [];
for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    for s = Args.sessions;
        cd ([daydir filesep sessions(s).name]);
        sesdir = pwd;
        
        if Args.ml
            if Args.nlynx
                if Args.ff
                    
                elseif Args.pp
                    
                else
                    pf = power_analysis('ml','nlynx');
                end
            else
                if Args.ff
                    pf = power_analysis('ml','ff');
                elseif Args.pp
                    pf = power_analysis('ml','pp');
                else
                    pf = power_analysis('ml');
                end
            end
        else
            pf = power_analysis;
        end
        all_peak_frequencies = [all_peak_frequencies pf];
    end
end

cd(monkeydir)