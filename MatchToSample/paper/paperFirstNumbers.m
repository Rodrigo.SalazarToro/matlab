%% LFP

% bands = {'alpha' 'beta' 'gamma' 'theta'};
% cd /Volumes/raid/data/monkey/clark
% clark = loadObject('cohInterIDE.mat')
% 
% [npairs,ind] = get(clark,'Number','snr',99);npairs
% PPC = length(unique(clark.data.Index(ind,[2 10]),'rows'))
% PFC = length(unique(clark.data.Index(ind,[2 11]),'rows'))
% 
% 
% for b = 1 : 4
%     [r,ind] = get(clark,'Number','snr',99,'sigSur',{bands{b} [1:4] [1 3]});
%     sprintf('%s %d; %g perc.',bands{b},r,round(100*r/npairs))
% end
% 
% cd /Volumes/raid/data/monkey/betty/
% 
% allbetty = loadObject('cohInterIDEandLongSorted.mat')
% 
% [npairs,ind] = get(allbetty,'Number','snr',99);npairs
% PPC = length(unique(allbetty.data.Index(ind,[2 10]),'rows'))
% PFC = length(unique(allbetty.data.Index(ind,[2 11]),'rows'))
% for b = 1 : 4
%     [r,ind] = get(allbetty,'Number','snr',99,'sigSur',{bands{b} [1:4] [1 3]});
%     sprintf('%s %d; %g perc.',bands{b},r,round(100*r/npairs))
% end


cd /Volumes/raid/data/monkey/

bothMonkeys = loadObject('cohInterIDEandLongSorted.mat');


areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
for area = [1 : 5 17]
    [npairs,ind] = get(bothMonkeys,'Number','snr',99,'hist',[area area]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(bothMonkeys,ind);
    
    nSites = size(unique([grs(:,2) depths(:,2)],'rows'),1);
    display([areas{area} ' ' num2str(nSites)])
    
%     display([areas{area} ' ' sprintf('%d',length(unique(bothMonkeys.data.Index(ind,[2 11]),'rows')))])
    % old code
end

for area = [8 : 13]
    [npairs,ind] = get(bothMonkeys,'Number','snr',99,'hist',[area area]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(bothMonkeys,ind);
    
    nSites = size(unique([grs(:,1) depths(:,1)],'rows'),1);
    display([areas{area} ' ' num2str(nSites)])
    
    
%     display([areas{area} ' ' sprintf('%d',length(unique(bothMonkeys.data.Index(ind,[2 10]),'rows')))])
    % old code
end

%% Units


cd /Volumes/raid/data/monkey

psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');

for a = [1:5 8:13 17]
    [r,ind] = get(psth,'Number','cortex',a,'unit',{'m'});r
    
    [~,~,~,pairNumberInd,grs,depths] = getDuplicates(psth,ind,'obj','psth');
    
    nSites = max(pairNumberInd);
    
    display([areas{a} ' MUA ' num2str(nSites)])
    
    
    
    [r,ind] = get(psth,'Number','cortex',a,'unit',{'s'});r
    [~,~,~,pairNumberInd,grs,depths] = getDuplicates(psth,ind,'obj','psth');
    
     nSites = max(pairNumberInd);
    
    display([areas{a} ' SUA ' num2str(nSites)])
end


%% SFC

clear sfcall sfcSurall phiall allgroups alldepths
for cc = [1:5 8:13 17]
    [r,ind] = get(psth,'Number','cortex',cc);
    %     ind = intersect(ind,ideTuned);
    [sfcall{cc},sfcSurall{cc},f,phiall{cc},~,~,allgroups{cc},alldepths{cc},alldays{cc},allunittype{cc},allnSUA{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    
end

for cc = [1:5 8:13 17]
    npairs = length(unique([allgroups{cc} alldepths{cc} alldays{cc} allunittype{cc} allnSUA{cc}],'rows'));
    display([areas{cc} ' ' num2str(npairs)])
end

%% Unit tuning 

for cc = [1:5 8:13 17]
    [r,ideTunedD] = get(psth,'Number','LocTuned',1,'epochTuned',2,'cortex',cc); % old object with ide and loc switched
    [r,locTunedD] = get(psth,'Number','ideTuned',1,'epochTuned',2,'cortex',cc);
    [r,ideTuned] = get(psth,'Number','LocTuned',1,'epochTuned',1,'cortex',cc);
    [r,locTuned] = get(psth,'Number','ideTuned',1,'epochTuned',1,'cortex',cc);
    
    ideCellst = union(ideTuned,ideTunedD);
    [~,~,~,pairNumberInd,grs,depths] = getDuplicates(psth,ideCellst,'obj','psth');
    nSites = max(pairNumberInd);
    
    display([areas{cc} ' ide tuned ' num2str(nSites)])
    locCellst = union(locTuned,locTunedD);
    [~,~,~,pairNumberInd,grs,depths] = getDuplicates(psth,locCellst,'obj','psth');
    nSites = max(pairNumberInd);
    
    display([areas{cc} ' loc tuned ' num2str(nSites)])
    
    
end