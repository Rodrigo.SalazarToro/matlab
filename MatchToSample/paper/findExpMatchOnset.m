function [allrdiff,indexes,alltrials,varargout] = findExpMatchOnset(obj,varargin)
% FixTfixSnotCombined option not yet made


Args = struct('plotSig',0,'save',0,'redo',0,'rule',1,'parfor',0,'mintrials',50,'plength',400,'bootstrap',0,'norm',0,'eventJitter',100,'nSTD',3);
Args.flags = {'FixTfixSnotCombined','plotSig','save','redo','parfor','bootstrap','norm'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


cases = {'bin' 'cont'};
indexes= cell(2,1);
figure
for ca = 1 : length(cases)
    [r,ind] = get(obj,'snr',99,'Number','delay',cases{ca},modvarargin{:});
    
    c = 1 ;
    
    params = struct('tapers',[2 3],'Fs',200,'trialave',1);
    
    dir = 'expectEvent';
    for ii = ind
        
        cd(obj.data.setNames{ii})
        pairg = obj.data.Index(ii,10:11);
        if isempty(nptDir(dir))
            mkdir(dir)
        end
        cd(dir)
        if Args.bootstrap
            thefile = sprintf('matchOnset2g%04.0fg%04.0fRule%d%d.mat',pairg(1),pairg(2),Args.rule,Args.plength);
        else
            thefile = sprintf('matchOnsetg%04.0fg%04.0fRule%d%d.mat',pairg(1),pairg(2),Args.rule,Args.plength);
        end
        if Args.redo || isempty(nptDir(thefile))
            cd(obj.data.setNames{ii})
            cd session01
            info = NeuronalCHAssign;
            channels = find(ismember(info.groups,pairg) == 1);
            mts =mtstrial('auto');
            if ca ==1
                delaytimes = unique(mts.data.MatchOnset-mts.data.CueOffset);
                xout(1) = delaytimes(1);
                xt = find(delaytimes > xout(1) & delaytimes < 3000);
                xout(2) = delaytimes(xt(end));
                addtime = Args.plength + Args.eventJitter;
            else
                [n,xout] = hist(mts.data.MatchOnset-mts.data.CueOffset,3);
                
                addtime = ((xout(2) - xout(1)) / 2) + Args.eventJitter;
                xout = xout;
            end
            for del = 1 : 2
                
                trialsF{del} = mtsgetTrials(mts,'delay',[xout(del)+addtime inf],'stable');
                [data,lplength] = lfpPcut(trialsF{del},channels,'EPremoved','epochs',{'CueOffset' [xout(del)-Args.plength+ Args.eventJitter xout(del)+ Args.eventJitter]},modvarargin{:},'plength',Args.plength);
                if Args.bootstrap
                    [dz(del,:,:),vdz(del,:),Adz(del,:),f,C(del,1,:),C(del,2,:),rdiff] = compareCoherence(data{1},data{2},params,modvarargin{:},'diffNtrials');
                else
                    [dz(del,:),vdz(del,:),Adz(del,:),f,C(del,1,:),C(del,2,:),rdiff] = compareCoherence(data{1},data{2},params,modvarargin{:});
                end
                [Cgram(del,:,:),phigram(del,:,:),~,~,~,t,fgram] = MTScohgram(mts,trialsF{del},channels,'reference',{'CueOffset' -xout(del)+Args.plength xout(del)+Args.plength+ Args.eventJitter},'mwind',[0.1 0.01],'tapers',[3 5],modvarargin{:});
                %        imagesc(t,f,squeeze(Cgram)'); colorbar; pause
            end
            if Args.save
                cd(obj.data.setNames{ii})
                cd(dir)
                save(thefile,'dz','vdz','Adz','f','C','data','trialsF','rdiff','Cgram','phigram','t','fgram','Args','modvarargin');
                display(sprintf('saving %s/%s',pwd,thefile))
                cd ..
            end
            
        else
            cd(obj.data.setNames{ii})
            cd(dir)
            load(thefile,'dz','vdz','Adz','f','C','data','trialsF','rdiff','Cgram','phigram','t','fgram');
            cd ..
        end
        
        
        if (length(trialsF{1}) >= Args.mintrials) && (length(trialsF{2}) >= Args.mintrials) && isempty(find(ii == indexes{ca}))
            alltrials{ca}(c,1) = length(trialsF{1});
            alltrials{ca}(c,2) = length(trialsF{2});
            allC{ca}(c,:,:,:) = C;
            allrdiff{ca}(c,:,:) = ~Adz;
            allgram{ca}(c,:,:,:) = Cgram;
            c = c + 1;
            indexes{ca} = [indexes{ca} ii];
        end
        
    end
    
    %% stats comparison with fix
    
    if Args.plotSig
        
        for del = 1 : 2
            onset = find(t == 0.4050);
            %             posrdiff = squeeze(allrdiff{ca}(:,del,:)) .* ((squeeze(allC{ca}(:,del,1,:)) - squeeze(allC{ca}(:,del,2,:))) > 0);
            meanOnset = repmat(mean(squeeze(allgram{ca}(:,del,1:onset,:)),2),[1 81 1]);
            
            stdOnset = repmat(std(squeeze(allgram{ca}(:,del,1:onset,:)),[],2),[1 81 1]);
            thegrams = squeeze(allgram{ca}(:,del,:,:));
            sigdrop{ca,del} = (thegrams < (meanOnset - Args.nSTD*stdOnset));
            
            fractSig = squeeze(sum(sigdrop{ca,del},1)) / size(sigdrop{ca,del},1);
            subplot(4,2,(ca-1)*2 + del);
            imagesc(t,fgram,fractSig');
            colorbar
            hold on
            
            line([Args.plength/1000 Args.plength/1000], [fgram(1) fgram(end)],'Color','r');
            caxis([0 0.15])
            ylim([10 45])
            xlim([0.05 0.75])
            title(sprintf('Sig. decrease %s %d sec',cases{ca},del))
            subplot(4,2,(ca-1)*2 + del + 4);
            
            if Args.norm
                %                 nallgram = thegrams ./ repmat(max(max(thegrams,[],2),[],3),[1 size(thegrams,2) size(thegrams,3)]);
                %                  nallgram = thegrams ./ repmat(std(thegrams,[],2),[1 size(thegrams,2) 1]);
                
                
                nallgram = thegrams .* sigdrop;
            else
                nallgram = thegrams ;
            end
            
            imagesc(t,fgram,squeeze(median(nallgram,1))');
            colorbar
            hold on
            
            line([Args.plength/1000 Args.plength/1000], [fgram(1) fgram(end)],'Color','r');
            title(sprintf('Coh. %s %d sec',cases{ca},del))
            ylim([10 45])
            xlim([0.05 0.75])
        end
        xlabel('Time [sec]')
        ylabel('Frequency [Hz]')
    end
end
varargout{1} = allC;
varargout{2} = allgram;
varargout{3} = t;
varargout{4} = fgram;
varargout{5} = sigdrop;

