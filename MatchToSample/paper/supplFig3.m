

clear all
cd /Volumes/raid/data/monkey
obj = loadObject('cohInterIDEandLongSorted.mat');
 load('migramIdePerLocRule1prctile98DElocSel1Bias.mat')
[r,ind] = get(obj,'Number','snr',99);


load IdePairsUnbiased.mat

IdePairs = uselpairLocs;



list = obj.data.setNames(ind(IdePairs(2,:)))';

clark = strfind(list,'clark');

clarkInd = [];
for ii = 1 : size(clark,1)
    if ~isempty(clark{ii})
    clarkInd =  [clarkInd ii];
    end
end
bettyInd = setdiff([1:465],clarkInd);
 load IdeMugramUnbiased.mat
 
 time = [0.1:0.05:1.8];

figure; 
set(gcf,'Name','Seaprate monkeys')
subplot(4,1,1);
nf = [0 :50];
ntime = [0.1:0.02:1.8];

thedata = squeeze(nanmedian(biasmugram(clarkInd,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata');
colorbar
caxis([-0.04 0.2])
set(gca,'TickDir','out')
ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for Clark''s pairs selective for the object using any object during the delay (10-42 Hz); n=51')
set(gca,'TickDir','out')

subplot(4,1,2);
nf = [0 :50];
ntime = [0.1:0.02:1.8];

thedata = squeeze(nanmedian(biasmugram(bettyInd,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata');
colorbar
caxis([-0.04 0.2])
set(gca,'TickDir','out')
ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for Betty''s pairs selective for the object using any object during the delay (10-42 Hz); n=387')
set(gca,'TickDir','out')
subplot(4,1,3)


plot(time,squeeze(prctile(coherenceStim{3}(1,clarkInd,5:8),[25 50 75],2))')

xlim([0.1 1.8])
title('prtiles for clark best stimullus')
ylabel('Coherence')
ylim([0.07 0.27])
set(gca,'TickDir','out')

subplot(4,1,4)
plot(time,squeeze(prctile(coherenceStim{3}(1,bettyInd,5:8),[25 50 75],2))')

xlim([0.1 1.8])
title('prtiles for betty best stimullus')
ylabel('Coherence')
ylim([0.07 0.27])
set(gca,'TickDir','out')