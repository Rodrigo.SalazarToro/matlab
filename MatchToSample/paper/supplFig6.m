cd /Volumes/raid/data/monkey/clark/060428/session02/
mts = mtstrial('auto','redosetNames');
for ob = 1 : 3; ind{ob} = mtsgetTrials(mts,'stable','BehResp',1,'iCueObj',ob,'iCueLoc',1); end 
[data,lplength] = lfpPcut(ind{3},[3 9],'fromFiles'); 

 params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1,'err',[2 0.341]);

[C,phi,S12,S1,S2,f,confC,phistd,Cerr]=coherencyc(squeeze(data{4}(:,:,1)),squeeze(data{4}(:,:,2)),params);


params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1);
clear Cb phib
for n=1:500
    st=randi(size(data{1},2),1,65);[Cb(n,:),phib(n,:),S12,S1,S2,f]=coherencyc(squeeze(data{4}(:,st,1)),squeeze(data{4}(:,st,2)),params);
end

clear p;for fq = 1 : 65;[h(fq),p(fq),kstat(fq),cv(fq)] = lillietest(Cb(:,fq));end 



figure; 

subplot(3,1,1)
plot(f,C); hold on; 
plot(f,mean(Cb,1),'k')
plot(f,Cerr(2,:)-C','b--')
plot(f,std(Cb,[],1),'k--')

xlim([3 50])
ylim([0 0.3])
ylabel('coherence')
xlabel('Frequency [Hz]')
title('34.1th percentile vs std')
legend('coherence','mean coherence from bootstrap','34.1th prctile from coherence','std from bootstrap') 
subplot(3,1,2)

range = [0:0.005:1];
ff = 16;
[nn,xout]=hist(Cb(:,ff),range);
nn = nn/sum(nn);
bar(xout,nn)


c=1;for rr = range; Gfit(c) = normpdf(rr,mean(Cb(:,ff)),std(Cb(:,ff))); c=c+1;end
Gfit = Gfit/sum(Gfit);
hold on; plot(range,Gfit,'r')
ylabel('P(r/s)')

title(sprintf('histogram at %g Hz',f(ff)))
xlim([0 0.4])
subplot(3,1,3)


ff=26;
[nn,xout]=hist(Cb(:,ff),range);
nn = nn/sum(nn);
bar(xout,nn)
c=1;for rr = range; Gfit(c) = normpdf(rr,mean(Cb(:,ff)),std(Cb(:,ff))); c=c+1;end
Gfit = Gfit/sum(Gfit);
hold on; plot(range,Gfit,'r')

title(sprintf('histogram at %g Hz',f(ff)))
xlim([0 0.4])
ylabel('P(r/s)')
xlabel('Coherence')