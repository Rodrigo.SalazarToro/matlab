cd /Volumes/raid/data/monkey/
clear all
psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');
coh = loadObject('cohInterIDEandLongSorted.mat');
load tunedPairs.mat
% [r,betapos34] = get(coh,'Number','type',{'betapos34' [1 3]});
[r,betapos34] = get(coh,'Number');

[r,all] = get(psth,'Number');
[r,locTuned] = get(psth,'Number','LocTuned',1,'epochTuned',1,'stimOnset',50);
[r,ideTuned] = get(psth,'Number','ideTuned',1,'epochTuned',1,'stimOnset',50);

[r,locTunedD] = get(psth,'Number','LocTuned',2,'epochTuned',2);
[r,ideTunedD] = get(psth,'Number','ideTuned',2,'epochTuned',2);

tunedCells = union(union(union(locTuned,ideTuned),ideTunedD),locTunedD);
nottuned = setdiff(all,tunedCells);

delayCells = union(locTunedD,ideTunedD);
sampleCells = union(locTuned,ideTuned);


mixedSampleCells = intersect(locTuned,ideTuned);
mixedDelayCells = intersect(locTunedD,ideTunedD);

onlySampleIdeCells = setdiff(ideTuned,mixedSampleCells);
onlySampleLocCells = setdiff(locTuned,mixedSampleCells);

onlyDelayIdeCells = setdiff(ideTunedD,mixedDelayCells);
onlyDelayLocCells = setdiff(locTunedD,mixedDelayCells);


[r,stimOnset] = get(psth,'Number','stimOnset',50);
noOnset = setdiff(all,stimOnset);

%% get the SFC
% [sfc,sfcSur] = mkAveSFC(obj,tunedCells,'plot','pSFC','addNameFile','pSFCtunedCells');
% [sfc,sfcSur] = mkAveSFC(obj,nottuned,'plot','pSFC','addNameFile','pSFCnotTuned');

% [sfc,sfcSur] = mkAveSFC(obj,tunedCells,'plot','addNameFile','tunedCells');
% [sfc,sfcSur] = mkAveSFC(obj,nottuned,'plot','addNameFile','notTuned');
%
%
% [sfc,sfcSur] = mkAveSFC(obj,delayCells,'plot','addNameFile','delayCells');
% [sfc,sfcSur] = mkAveSFC(obj,sampleCells,'plot','addNameFile','sampleCells');
%
% [sfc,sfcSur] = mkAveSFC(obj,mixedSampleCells,'plot','addNameFile','mixedSampleCells');
% [sfc,sfcSur] = mkAveSFC(obj,mixedDelayCells,'plot','addNameFile','mixedDelayCells');
%
%
% [sfc,sfcSur] = mkAveSFC(obj,onlyIdeCells,'plot','addNameFile','onlyIdeCells');
% [sfc,sfcSur] = mkAveSFC(obj,onlyLocCells,'plot','addNameFile','onlyLocCells');
%
%
% [sfc,sfcSur] = mkAveSFC(obj,stimOnset,'plot','addNameFile','StimOnset');
% [sfc,sfcSur] = mkAveSFC(obj,noOnset,'plot','addNameFile','noOnset');
%% cortical areas

areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};
allcc = unique(psth.data.Index(:,3));

sfc = cell(1,length(allcc)- 1);
phisfc = cell(1,length(allcc)- 1);
sfcSur = cell(1,length(allcc)- 1);

for cc = 1 : length(allcc)- 1
    [r(cc),ind] = get(psth,'Number','cortex',allcc(cc));
    
    
    %     ind = intersect(ind,delayCells);
    %     [sfc,sfcSur] = mkAveSFC(obj,ind,'plot','pSFC','addNameFile',sprintf('pSFC%s',areas{allcc(cc)}));
    if ~isempty(ind)
        [sfc{cc},sfcSur{cc},f,phisfc{cc},time] = mkAveSFC(psth,ind,'redo');%,'addNameFile',sprintf('%s%sSFCSig2',areas{allcc(cc)},'idePairs'),'sigLevel',2,'cohObj',coh,'cohInd',idePairsObj,'gram','redo','noSur');
        %     [sfc{cc},sfcSur{cc}] = mkAveSFC(obj,intersect(ind,tunedCells),'plot','addNameFile',sprintf('tuned%s',areas{allcc(cc)}));
        %     [sfc{cc},sfcSur{cc}] = mkAveSFC(obj,intersect(ind,nottuned),'plot','addNameFile',sprintf('notTuned%s',areas{allcc(cc)}));
        %     [sfc{cc},sfcSur{cc}] = mkAveSFC(obj,intersect(ind,sampleCells),'plot','addNameFile',sprintf('sampleCells%s',areas{allcc(cc)}));
        %     [sfc{cc},sfcSur{cc}] = mkAveSFC(obj,intersect(ind,delayCells),'plot','addNameFile',sprintf('delayCells%s',areas{allcc(cc)}));
    end
end
cellnumber = zeros(17,2);
units = {'m' 's'};

for ss = 1 : 2
    for cc = 1 : 17
        [~,ind] = get(psth,'Number','cortex',cc,'unit',{units{ss}});
        if ~isempty(ind)
            [ind,cind2] = comparePsthandcohObjs(psth,ind,coh,betapos34);
            cellnumber(cc,ss) = length(ind);
        else
            cellnumber(cc,ss) = 0;
        end
    end
end

for ss = 1 : 2
    for cc = 1 : length(allcc)- 1
        [recordedC(cc,ss),ind] = get(psth,'Number','cortex',allcc(cc),'unit',{units{ss}});
    end
end
sum(recordedC([1:5 12],:))
sum(recordedC([6:11],:))
indC = cell(length(allcc)- 1,1);
for cc = 1 : length(allcc)- 1
    [~,indC{cc}] = get(psth,'Number','cortex',allcc(cc),'stimOnset',10);
end
for ii = 1 : 12; RT(ii) = nanmedian(psth.data.Index(indC{ii},42)); end
areas(allcc(1:end-1))
RT
ncells = sum(cellnumber(allcc(1:end-1),:),2);

epochs = {'pre-sample' 'sample' 'delay 1' 'delay 2'};
f1 = figure;
f2 = figure;
relf = find(f <=50);
betaf = find(f > 12 & f<=22);
count = 1;
for cc = [1 : 5 12]
    sigv = zeros(4,65);
    sigphif =[];
    sigphi = zeros(4,65);
    for p = 1 : 4
        if ~isempty(sfc{cc})
            sigcoh = squeeze(sfc{cc}(:,betaf,p)) > squeeze(sfcSur{cc}(:,betaf,p));
            tphi = phisfc{cc}(:,betaf,p);
            sigphi = tphi .* sigcoh;
            sigphi(sigcoh == 0) = nan; % remove zero values from not significant coherence
            sigphif = [sigphif reshape(sigphi,size(sigphi,1) * size(sigphi,2),1)];
            
            sigv(p,:) = 100 * sum(sfc{cc}(:,:,p) > sfcSur{cc}(:,:,p),1) ./ size(sfc{cc},1);
        end
    end
    figure(f1)
    subplot(2,6,count)
    plot(f,sigv)
    ylim([0 25])
    title(sprintf('%s n= %d',areas{allcc(cc)},size(sfc{cc},1)))
    figure(f2)
    %     sigphip = nanmean(sigphif,2);
    sigphip = reshape(sigphif,size(sigphif,1) * size(sigphif,2),1);
    subplot(2,6,count)
    rose(sigphip)
    
    title(sprintf('%s n= %d',areas{allcc(cc)},sum(~isnan(sigphip))))
    count = count + 1;
end
count = 1;
for cc = [6 : 11]
    sigv = zeros(4,65);
    sigv = zeros(4,65);
    sigphif =[];
    sigphi = zeros(4,65);
    for p = 1 : 4
        if ~isempty(sfc{cc})
            sigcoh = squeeze(sfc{cc}(:,betaf,p)) > squeeze(sfcSur{cc}(:,betaf,p));
            tphi = phisfc{cc}(:,betaf,p);
            sigphi = tphi .* sigcoh;
            sigphi(sigcoh == 0) = nan;
            
            sigphif = [sigphif reshape(sigphi,size(sigphi,1) * size(sigphi,2),1)];
            sigv(p,:) = 100 * sum(sfc{cc}(:,:,p) > sfcSur{cc}(:,:,p),1) ./ size(sfc{cc},1);
        end
    end
    figure(f1)
    subplot(2,6,6+ count)
    plot(f,sigv)
    ylim([0 25])
    title(sprintf('%s n= %d',areas{allcc(cc)},size(sfc{cc},1)))
    figure(f2)
    sigphip = reshape(sigphif,size(sigphif,1) * size(sigphif,2),1);
    subplot(2,6,6+count)
    rose(sigphip)
    title(sprintf('%s n= %d',areas{allcc(cc)},sum(~isnan(sigphip))))
    count = count + 1;
end
%%
figure(f1)
for sb = 1 : 12; subplot(2,6,sb); xlim([0 50]); set(gca,'TickDir','out');end

xlabel('Frequency [Hz]')
ylabel('% significant at p<0.01');
legend(epochs)

units = {'m' 's'};
for cc = 1 : 17
    [~,ind] = get(psth,'Number','cortex',cc);
    if ~isempty(ind)
        [ind,cind2] = comparePsthandcohObjs(psth,ind,coh,betapos34);
        r(1,cc) = length(ind);
    else
        r(1,cc) = 0;
    end
    for ss = 1 : 2
        [rt,ind] = get(psth,'Number','cortex',cc,'unit',{units{ss}});
        if ~isempty(ind)
            [ind,cind2] = comparePsthandcohObjs(psth,ind,coh,betapos34);
            rd(ss,cc) = length(ind);
        end
    end
end
% [n(1,:),xout] = hist(obj.data.Index(mixedSampleCells,3),[1:17]);
% [n(2,:),xout] = hist(obj.data.Index(onlySampleIdeCells,3),[1:17]);
% [n(3,:),xout] = hist(obj.data.Index(onlySampleLocCells,3),[1:17]);
%
% [n(4,:),xout] = hist(obj.data.Index(mixedDelayCells,3),[1:17]);
% [n(5,:),xout] = hist(obj.data.Index(onlyDelayIdeCells,3),[1:17]);
% [n(6,:),xout] = hist(obj.data.Index(onlyDelayLocCells,3),[1:17]);
clear n

[sampleCells,cind2] = comparePsthandcohObjs(psth,sampleCells,coh,betapos34);
[delayCells,cind2] = comparePsthandcohObjs(psth,delayCells,coh,betapos34);

[n(1,:),xout] = hist(psth.data.Index(sampleCells,3),[1:17]);
[n(2,:),xout] = hist(psth.data.Index(delayCells,3),[1:17]);
figure
% bar(xout,(n./repmat(r,6,1))')
bar(xout,(n./repmat(r,2,1))')
set(gca,'XTick',[1:17])
set(gca,'XTickLabels',areas)
set(gca,'TickDir','out');
xlabel('cortical area')
ylabel('% number of cells')
legend('sample tuned cells','delay tuned cells')
% legend('mixed during sample','Ide during sample','Loc during sample','mixed during delay','Ide during delay','Loc during delay')
ylim([0 0.7])
%% example
% obj = loadObject('psthIDEandLong99bin100msIdeBased.mat');

InspectGUI(psth,'ideTuned',2,'stimOnset',10,'unit',{'s'}) % choose n = 31
for sb = 1: 27; subplot(9,3,sb); xlim([-400 1300]); end
% /Volumes/raid/data/monkey/betty/100128/session01/group0048/cluster01s
% area LIP
% area LIP
InspectGUI(nptdata,'multObjs',{psth;psth;psth;psth;psth;psth},'optArgs',{{'plotType','MI','mutualInfo','PSTHLocPerIde1'};{'plotType','MI','mutualInfo','PSTHIdePerLoc1'};{'plotType','MI','mutualInfo','PSTHLocPerIde2'};{'plotType','MI','mutualInfo','PSTHIdePerLoc2'};{'plotType','MI','mutualInfo','PSTHLocPerIde3'};{'plotType','MI','mutualInfo','PSTHIdePerLoc3'}},'SP',[3 2])
InspectGUI(nptdata,'multObjs',{psth;psth;psth;psth;psth;psth},'optArgs',{{'mutualInfo','PSTHLocPerIde1MatchAlign'};{'mutualInfo','PSTHIdePerLoc1MatchAlign'};{'mutualInfo','PSTHLocPerIde2MatchAlign'};{'mutualInfo','PSTHIdePerLoc2MatchAlign'};{'mutualInfo','PSTHLocPerIde3MatchAlign'};{'mutualInfo','PSTHIdePerLoc3MatchAlign'}},'SP',[3 2])
% choose n = 645 for 999
% choose n = 955 for 99
% InspectGUI(nptdata,'multObjs',{psth;psth;psth},'optArgs',{{'mutualInfo','PSTHIdePerLoc1','ideTuned',2,'stimOnset',50};{'mutualInfo','PSTHIdePerLoc2','ideTuned',2,'stimOnset',50};{'mutualInfo','PSTHIdePerLoc3','ideTuned',2,'stimOnset',50}},'SP',[3 1])
