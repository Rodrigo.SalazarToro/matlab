%% Fixation

clear all
cd /Volumes/raid/data/monkey/


load sigCoh&MIPerLoc.mat


load IdePairsUnbiased.mat

mintrials = 15;

obj = loadObject('cohInterIDEandLongSorted.mat');
[r,ind] = get(obj,'Number','snr',99);
idepairsObj = ind(uselpairLocs(2,:));


%%Fixation
pairswithFix = zeros(465,1);
fixgrams = zeros(465,35,17);
sigdiff = zeros(465,35,17);
sigdiffz = zeros(465,35,17);
alldiffSur = zeros(465,35,17);
alldiff = zeros(465,35,17);
cohdata = zeros(465,3);
cohfix = zeros(465,3);
compfixCor = zeros(465,35,17);
for ii = 1 : 465
    
    cd(obj.data.setNames{idepairsObj(ii)})
    cd grams
    file = sprintf('migramg%04.0fg%04.0fRule1IdePerLoc%d.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11),uselpairLocs(1,ii));
    surfile = sprintf('migramSurGenInterRule1IdePerLoc%d.mat',uselpairLocs(1,ii));
    cohfile = sprintf('cohgramg%04.0fg%04.0fRule1Loc%d.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11),uselpairLocs(1,ii));
    
    fixfile = sprintf('migramg%04.0fg%04.0fRule1Fix.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11));
    surfixfile = sprintf('migramSurGenInterRule1Fix.mat');
    cohfixfile = sprintf('cohgramg%04.0fg%04.0fFix.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11));
    if ~isempty(nptDir(fixfile))
        load(fixfile)
        presample = time <= 0.405;
        surfix = load('migramSurGenInterRule1Fix.mat','mutualgram');
        if min(cellfun(@length,trials)) >= mintrials && size(surfix.mutualgram,1) == 1000
            load(file,'mutualgram','C')
            data = mutualgram(1:35,:);
            cohdata(ii,:) = [mean(mean(C{1}(21:33,5:8),2),1) mean(mean(C{2}(21:33,5:8),2),1) mean(mean(C{3}(21:33,5:8),2),1)];
            %             load(cohfile,'C')
            %             cohdata(ii,:) = mean(C{1}(1:35,5:8),2);
            sur = load(surfile,'mutualgram');
            texpv =  getExpValue(sur.mutualgram(:,1:35,:),f);
            alphaData = squeeze(sum(sum(mutualgram(presample,:),1),2)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';
            sigdata = data > squeeze(prctile((alphaData * sur.mutualgram(:,1:35,:)),98,1));
            
            load(fixfile,'mutualgram','C')
            fix = mutualgram(1:35,:);
            %              load(cohfixfile,'C')
            cohfix(ii,:) = [mean(mean(C{1}(21:33,5:8),2),1) mean(mean(C{2}(21:33,5:8),2),1) mean(mean(C{3}(21:33,5:8),2),1)];
            
%             cohfix(ii,:) = mean(C{1}(1:35,5:8),2);
            pairswithFix(ii) = 1;
            texpv =  getExpValue(surfix.mutualgram(:,1:35,:),f);
            alphaFix = squeeze(sum(sum(mutualgram(presample,:),1),2)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';
            %
            alphaExp =  alphaFix*texpv;
            sigfix = fix > squeeze(prctile((alphaFix * surfix.mutualgram(:,1:35,:)),98,1));
            
            fixgrams(ii,:,:) = mutualgram(1:35,:) - alphaExp;
            % stat
            % with z-score
            meansurdata = mean(alphaData*sur.mutualgram(:,1:35,:),1);
            stdsurdata = std(alphaData*sur.mutualgram(:,1:35,:),[],1);
            
            zsur = ((alphaData*sur.mutualgram(:,1:35,:)) - repmat(meansurdata,[1000 1 1])) ./ repmat(stdsurdata,[1000 1 1]);
            
            meansurfix = mean(alphaFix * surfix.mutualgram(:,1:35,:),1);
            stdsurfix = std(alphaFix * surfix.mutualgram(:,1:35,:),[],1);
            
            zsurfix = ((alphaFix*surfix.mutualgram(:,1:35,:)) - repmat(meansurfix,[1000 1 1])) ./ repmat(stdsurfix,[1000 1 1]);
            zdata = (data - squeeze(meansurdata)) ./ squeeze(stdsurdata);
            diffSurrogates = squeeze(prctile( zsur - zsurfix,99,1));
            zfix = (fix - squeeze(meansurfix)) ./ squeeze(stdsurfix);
            sigdiffz(ii,:,:) = ((zdata - zfix) > diffSurrogates) .* (sigfix + sigdata);
            % without zscore
            
          
            diffSurrogates = squeeze(prctile( alphaData*sur.mutualgram(:,1:35,:) - alphaFix*surfix.mutualgram(:,1:35,:),99,1));
           
            sigdiff(ii,:,:) = ((data - fix) > diffSurrogates) .* (sigfix + sigdata);
           
           
            %% charlie sug
            alldiffSur(ii,:,:) = mean(sur.mutualgram(:,1:35,:) - surfix.mutualgram(:,1:35,:));
            alldiff(ii,:,:) = data - fix;
           
          
            %%
            
            %
        end
        
    end
end
fixpairs = find(pairswithFix ==1);


% falsepositiverate = max(sum(sum(sigdiff(fixpairs,1:7,5:14),2),3)) / (7*10);
FDRall = getExpValue(sigdiff(fixpairs,:,:),f);
FDR = max(max(FDRall(1:7,:)));
sigpairs = find((sum(sum(sigdiff(fixpairs,21:32,5:14),2),3)/ (12*10)) > FDR);
display(['number of sig. pair for fix diff ' num2str(length(sigpairs))])

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(uselpairLocs(2,fixpairs)));
nfix = max(pairNumberInd);

time = [0.1:0.05:1.8];
nf = [0 :50];
ntime = [0.1:0.02:1.8];
time = time(1:35);
figure

subplot(4,2,1)

thedata = squeeze(nanmedian(smugram(fixpairs,:,:) - ExpVgram(fixpairs,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([-0.05 0.15])

title(sprintf('Correct trial; n=%d',nfix))
ylabel('Frequency (Hz)')
subplot(4,2,3)

thedata = squeeze(nanmedian(fixgrams(fixpairs,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([-0.05 0.15])
title('Fixation')
ylabel('Frequency (Hz)')
subplot(4,2,7)

% thedata = 100*squeeze(sum(sigdiff(fixpairs,:,:),1)) / nfix;
% thedata = interp2(f,time,thedata,nf,ntime');
% imagesc(time,f,thedata')
% colorbar
% caxis([0 10])


%% charlie suggestion
% clear p
% for ti = 1 : 34
%     for fi = 1 : 17;
%         
%         [h,p(ti,fi)] = ttest2(alldiffSur(fixpairs,ti,fi),alldiff(fixpairs,ti,fi));
%     end
% end
% thedata = p;


%% 


thedata = 100*squeeze(sum(sigdiff(fixpairs,:,:),1)) / nfix; 

thedata = interp2(f,time(1:35),thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([0 7])

ylabel('Frequency (Hz)')

%%
subplot(4,2,7); title('% significant differences at p<0.01')
nsessions = length(unique(obj.data.setNames(idepairsObj(fixpairs)),'rows'));
subplot(4,2,5);
colormap('default')
% me = mean(cohdata(fixpairs,:));
% ste = std(cohdata(fixpairs,:))/sqrt(nfix);
% ste2 = std(cohdata(fixpairs,:))/sqrt(nsessions);

thedata = reshape(cohdata(fixpairs,:),1, length(fixpairs) *3);
thefixdata = reshape(cohfix(fixpairs,:),1, length(fixpairs) *3);

boxplot([thedata' thefixdata'])
set(gca,'XTick',[1 2])
set(gca,'XTickLabel',{'correct' 'fixation'})


% plot(time,me)
% hold on
% 
% plot(time,me + ste,'--')
% plot(time,me - ste,'--')
% 
% plot(time,me + ste2,'--')
% plot(time,me - ste2,'--')
% 
% me = mean(cohfix(fixpairs,:));
% ste = std(cohfix(fixpairs,:))/sqrt(nfix);
% ste2 = std(cohfix(fixpairs,:))/sqrt(nsessions);
% 
% 
% plot(time,me,'r')
% 
% 
% plot(time,me + ste,'r--')
% plot(time,me - ste,'r--')
% 
% plot(time,me + ste2,'r--')
% plot(time,me - ste2,'r--')
% xlim([0.1 1.8])
% ylim([0.1 0.22])
% ylabel('Coherence')
% xlabel('Time (s)')






%% Incorrect
clear all
cd /Volumes/raid/data/monkey/


load sigCoh&MIPerLoc.mat


load IdePairsUnbiased.mat

mintrials = 15;

obj = loadObject('cohInterIDEandLongSorted.mat');
[r,ind] = get(obj,'Number','snr',99);
idepairsObj = ind(uselpairLocs(2,:));


alldiffSur = zeros(465,35,17);
alldiff = zeros(465,35,17);
pairswithincor = zeros(465,1);
incorgrams = zeros(465,35,17);
sigdiff = zeros(465,35,17);
sigdiffz = zeros(465,35,17);
cohdata = zeros(465,3);
cohincor = zeros(465,3);
compIncorCor = zeros(465,35,17);
for ii = 1 : 465
    
    cd(obj.data.setNames{idepairsObj(ii)})
    cd grams
    file = sprintf('migramg%04.0fg%04.0fRule1IdePerLoc%d.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11),uselpairLocs(1,ii));
    surfile = sprintf('migramSurGenInterRule1IdePerLoc%d.mat',uselpairLocs(1,ii));
    cohfile = sprintf('cohgramg%04.0fg%04.0fRule1Loc%d.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11),uselpairLocs(1,ii));
    
    incorfile = sprintf('migramg%04.0fg%04.0fRule1IdePerLoc%dIncor.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11),uselpairLocs(1,ii));
    surincorfile = sprintf('migramSurGenInterRule1IdePerLoc%dIncor.mat',uselpairLocs(1,ii));
    cohincorfile = sprintf('cohgramg%04.0fg%04.0fLoc%dIncor%.mat',obj.data.Index(idepairsObj(ii),10),obj.data.Index(idepairsObj(ii),11));
    
    
    if ~isempty(nptDir(incorfile)) && ~isempty(nptDir(surincorfile))
        load(incorfile)
        presample = time <= 0.405;
        surIncor = load(surincorfile,'mutualgram');
        if min(cellfun(@length,trials)) >= mintrials && size(surIncor.mutualgram,1) == 1000
            load(file,'mutualgram','C')
            data = mutualgram(1:35,:);
            %             for stim = 1 : 3; cohdata(ii,:,stim) = mean(C{stim}(1:35,5:8),2); end
            cohdata(ii,:) = [mean(mean(C{1}(21:33,5:8),2),1) mean(mean(C{2}(21:33,5:8),2),1) mean(mean(C{3}(21:33,5:8),2),1)];
            %             load(cohfile,'C')
            
            %             cohdata(ii,:) = mean(C{1}(1:35,5:8),2);
            sur = load(surfile,'mutualgram');
            texpv =  getExpValue(sur.mutualgram(:,1:35,:),f);
            
            alphaData = squeeze(sum(sum(mutualgram(presample,:),1),2)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';
%             subplot(2,1,1)
%             imagesc(time,f,(data-alphaData*texpv)');
            sigdata = data > squeeze(prctile((alphaData * sur.mutualgram(:,1:35,:)),98,1));
            
            load(incorfile,'mutualgram','C','trials')
            incor = mutualgram(1:35,:);
            %             for stim = 1 : 3; cohincor(ii,:,stim) = mean(C{stim}(1:35,5:8),2); end
            cohincor(ii,:) = [mean(mean(C{1}(21:33,5:8),2),1) mean(mean(C{2}(21:33,5:8),2),1) mean(mean(C{3}(21:33,5:8),2),1)];
            
            %             load(cohincorfile,'C')
            %             cohincor(ii,:) = mean(C{1}(1:35,5:8),2);
            
            pairswithincor(ii) = 1;
            texpv =  getExpValue(surIncor.mutualgram(:,1:35,:),f);
            
           
            
            alphaIncor = squeeze(sum(sum(mutualgram(presample,:),1),2)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';
            %
%              subplot(2,1,2)
%             imagesc(time,f,(incor-alphaIncor*texpv)');
%             title(['n=' num2str(cellfun(@length,trials))]);
%             pause
            alphaExp =  alphaIncor*texpv;
            sigincor = incor > squeeze(prctile((alphaIncor * surIncor.mutualgram(:,1:35,:)),98,1));
            
            incorgrams(ii,:,:) = mutualgram(1:35,:) - alphaExp;
            % stats
            % with z-score
            meansurdata = mean(alphaData*sur.mutualgram(:,1:35,:),1);
            stdsurdata = std(alphaData*sur.mutualgram(:,1:35,:),[],1);
            
            zsur = ((alphaData*sur.mutualgram(:,1:35,:)) - repmat(meansurdata,[1000 1 1])) ./ repmat(stdsurdata,[1000 1 1]);
            
            meansurincor = mean(alphaIncor * surIncor.mutualgram(:,1:35,:),1);
            stdsurincor = std(alphaIncor * surIncor.mutualgram(:,1:35,:),[],1);
            
            zsurincor = ((alphaIncor * surIncor.mutualgram(:,1:35,:)) - repmat(meansurincor,[1000 1 1])) ./ repmat(stdsurincor,[1000 1 1]);
            
            diffSurrogates = squeeze(prctile( zsur - zsurincor,99,1));
            
            zdata = (data - squeeze(meansurdata)) ./ squeeze(stdsurdata);
            zincor = (incor - squeeze(meansurincor)) ./ squeeze(stdsurincor);
            sigdiffz(ii,:,:) = ((zdata - zincor) > diffSurrogates) .* (sigincor + sigdata);
            % without z-score
             diffSurrogates = squeeze(prctile( alphaData*sur.mutualgram(:,1:35,:) - alphaIncor*surIncor.mutualgram(:,1:35,:),99,1));
           
            sigdiff(ii,:,:) = ((data - incor) > diffSurrogates) .* (sigincor + sigdata);
            
            %% charlie sug
            alldiffSur(ii,:,:) = mean(sur.mutualgram(:,1:35,:) - surIncor.mutualgram(:,1:35,:));
            alldiff(ii,:,:) = data - incor;
            %%          
        end
        
    end
end
incorpairs = find(pairswithincor ==1);
falsepositiverate = max(sum(sum(sigdiff(incorpairs,1:7,5:14),2),3) / (7*10));
FDRall = getExpValue(sigdiff(incorpairs,:,:),f);
FDR = max(max(FDRall(1:7,:)));

sigpairs = find((sum(sum(sigdiff(incorpairs,21:32,5:14),2),3)/ (12*10)) > falsepositiverate);
display(['number of sig. pair for incorrect diff ' num2str(length(sigpairs))])

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(uselpairLocs(2,incorpairs)));
nincor = max(pairNumberInd);

time = [0.1:0.05:1.8];
nf = [0 :50];
ntime = [0.1:0.02:1.8];
time = time(1:35);


subplot(4,2,2)

thedata = squeeze(nanmedian(smugram(incorpairs,:,:) - ExpVgram(incorpairs,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([-0.05 0.15])

title(sprintf('Correct trial; n=%d',nincor))
ylabel('Frequency (Hz)')
subplot(4,2,4)

thedata = squeeze(nanmedian(incorgrams(incorpairs,:,:),1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([-0.05 0.15])
title('Incorrect')
ylabel('Frequency (Hz)')
subplot(4,2,8)
%% charlie sugge
% for ti = 1 : 34
%     for fi = 1 : 17;
%         
%         [h,p(ti,fi)] = ttest2(alldiffSur(incorpairs,ti,fi),alldiff(incorpairs,ti,fi));
%     end
% end
% thedata = p;

thedata = 100*squeeze(sum(sigdiff(incorpairs,:,:),1)) / nincor;
thedata = interp2(f,time(1:35),thedata,nf,ntime');
imagesc(time,f,thedata')
colorbar
caxis([0 8])

ylabel('Frequency (Hz)')
subplot(4,2,7); title('% significant differences at p<0.01')
nsessions = length(unique(obj.data.setNames(idepairsObj(incorpairs)),'rows'));
subplot(4,2,6);
% for stim = 1 : 3
%     me = squeeze(mean(cohdata(incorpairs,:,stim)));
%     ste = squeeze(std(cohdata(incorpairs,:,stim))/sqrt(nincor));
%     ste2 = squeeze(std(cohdata(incorpairs,:,stim))/sqrt(nsessions));
%     plot(time,me)
%     hold on
%
%     plot(time,me + ste,'--')
%     plot(time,me - ste,'--')
%
%     plot(time,me + ste2,'--')
%     plot(time,me - ste2,'--')
%
%     me(stim,:) = squeeze(mean(cohincor(incorpairs,:,stim)));
%     ste(stim,:) = squeeze(std(cohincor(incorpairs,:,stim))/sqrt(nincor));
%     ste2(stim,:) = squeeze(std(cohincor(incorpairs,:,stim))/sqrt(nsessions));
%     plot(time,me,'r')
%
%
%     plot(time,me + ste,'r--')
%     plot(time,me - ste,'r--')
%
%     plot(time,me + ste2,'r--')
%     plot(time,me - ste2,'r--')
% end
thedata = reshape(cohdata(incorpairs,:),1, length(incorpairs) *3);
theincordata = reshape(cohincor(incorpairs,:),1, length(incorpairs) *3);

boxplot([thedata' theincordata'])
set(gca,'XTick',[1 2])
set(gca,'XTickLabel',{'correct' 'incorrect'})



% me = mean(cohdata(fixpairs,:));
% ste = std(cohdata(fixpairs,:))/sqrt(nfix);
% ste2 = std(cohdata(fixpairs,:))/sqrt(nsessions);



% me = mean(cohdata(incorpairs,:));
% ste = std(cohdata(incorpairs,:))/sqrt(nincor);
% ste2 = std(cohdata(incorpairs,:))/sqrt(nsessions);
% 
% 
% plot(time,me)
% hold on
% 
% plot(time,me + ste,'--')
% plot(time,me - ste,'--')
% 
% plot(time,me + ste2,'--')
% plot(time,me - ste2,'--')
% 
% me = mean(cohincor(incorpairs,:));
% ste = std(cohincor(incorpairs,:))/sqrt(nincor);
% ste2 = std(cohincor(incorpairs,:))/sqrt(nsessions);
% 
% 
% plot(time,me,'r')
% 
% 
% plot(time,me + ste,'r--')
% plot(time,me - ste,'r--')
% 
% plot(time,me + ste2,'r--')
% plot(time,me - ste2,'r--')
% 
% xlim([0.1 1.8])
% ylim([0.1 0.22])
% ylabel('Coherence')
% xlabel('Time (s)')
% subplot(4,2,6); title('Averaged coherence between the 3 objects; red: incorrect')


