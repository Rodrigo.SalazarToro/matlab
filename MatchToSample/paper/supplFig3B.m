cd /Volumes/raid/data/monkey/clark/060412/
load GCcohInter.mat
if Day.session(1).rule == 1
    s = 1;
else
    s = 2;
end


Fx2y = squeeze(Day.session(s).Fx2y(:,16,:));
Fy2x = squeeze(Day.session(s).Fy2x(:,16,:));
load GCgeneralSur.mat
Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
Fy2xsur = squeeze(Day.session(s).Fy2x(:,3,:));
siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);


cd GCdirStat/

% obj = loadObject('GCInterIDE.mat')
%  n = 63; group 6 and 16 pair # 16

data = load('GCinterg0006g0016Rule1.mat');
sur = load('GCintergeneralRule1.mat');
epoch = {'pre-sample' 'sample' 'delay1' 'delay2'};

figure;
for sb = 1 : 4;
    subplot(2,4,sb);
    plot(sur.f,squeeze(Fx2ysur(:,4)),'r--');
    hold on;
    plot(sur.f,squeeze(Fy2xsur(:,4)),'b--');
    plot(sur.f,squeeze(Fy2x(:,4)),'b');
    plot(sur.f,squeeze(Fx2y(:,4)),'r');
    xlim([0 50])
    ylim([0 0.25])
    title(epoch{sb})
     set(gca,'TickDir','out');
    
    subplot(2,4,sb+4);
    plot(sur.f,squeeze(sur.pvalues(:,[2 8],sb)),'b--');
    hold on;
    plot(sur.f,squeeze(data.Fdiff(:,sb)) .* siggc(:,sb),'k');
    xlim([0 50])
    ylim([-0.15 0.15])
    title(epoch{sb})
     set(gca,'TickDir','out');
end
xlabel('Frequency [Hz]')
ylabel('GC difference')
