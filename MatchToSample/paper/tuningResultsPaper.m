cd /Volumes/raid/data/monkey/
bothMonkeysL = loadObject('cohInterIDEL.mat');

[r,stable] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0},'sigSur',{'beta' [3 4] [1 3]});r


[r,Loc2] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0;'IDELoc' 2 '>=' 1},'sigSur',{'beta' 2 [1 3]});r
[r,Loc3] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 3 '>=' 1},'sigSur',{'beta' 3 [1 3]});r
[r,Loc4] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 4 '>=' 1},'sigSur',{'beta' 4 [1 3]});r

Loc = union(Loc3,Loc4);

[r,Obj2] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 2 '>=' 1},'sigSur',{'beta' 2 [1 3]});r
[r,Obj3] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 3 '>=' 1},'sigSur',{'beta' 3 [1 3]});r
[r,Obj4] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 4 '>=' 1},'sigSur',{'beta' 4 [1 3]});r

Obj = union(Obj3,Obj4);

bins = [0:0.01:0.24];
figure;
for p = 3 : 4
    
    mixedP{p-2} = intersect(eval(sprintf('Obj%d',p)),eval(sprintf('Loc%d',p)));
    %     [distLocC,distObjC,f] = mkDistanceStimuli(bothMonkeysL,mixedP{p-2},'sortedStim',p);
    subplot(3,2,4+p-2);
    
    hist([bothMonkeysL.data.IDELocInt(mixedP{p-2},p,3)/5- bothMonkeysL.data.IDELocInt(mixedP{p-2},p,1)/5 bothMonkeysL.data.IDEObjInt(mixedP{p-2},p,3)/5- bothMonkeysL.data.IDEObjInt(mixedP{p-2},p,1)/5],bins)
    
    %      plot(f,prctile(squeeze(distLocC(:,:,p,1)),[10 25 50 75 90],2)); hold on;  plot(f,prctile(squeeze(distObjC(:,:,p,1)),[10 25 50 75 90],2),'--');
    title(sprintf('delay %d; n=%d',p-2,length(mixedP{p-2})))
    if p == 3; ylabel('mixed tuned'); end
    ylim([0 6])
    
    ObjP{p-2} = setdiff(eval(sprintf('Obj%d',p)),mixedP{p-2});
    %     [distLocC,distObjC,f] = mkDistanceStimuli(bothMonkeysL,ObjP{p-2},'sortedStim',p);
    
    subplot(3,2,p-2);
    hist(bothMonkeysL.data.IDEObjInt(ObjP{p-2},p,3)/5 - bothMonkeysL.data.IDEObjInt(ObjP{p-2},p,1)/5,bins)
    
    %      plot(f,prctile(squeeze(distObjC(:,:,p,1)),[10 25 50 75 90],2));
    title(sprintf('delay %d; n=%d',p-2,length(ObjP{p-2})))
    if p == 3; ylabel('Obj. tuned'); end
    ylim([0 25])
    
    LocP{p-2} = setdiff(eval(sprintf('Loc%d',p)),mixedP{p-2});
    %     [distLocC,distObjC,f] = mkDistanceStimuli(bothMonkeysL,LocP{p-2},'sortedStim',p);
    
    subplot(3,2,p);
    hist(bothMonkeysL.data.IDELocInt(LocP{p-2},p,3)/5- bothMonkeysL.data.IDELocInt(LocP{p-2},p,1)/5,bins)
    
    %      plot(f,prctile(squeeze(distLocC(:,:,p,1)),[10 25 50 75 90],2));
    title(sprintf('delay %d; n=%d',p-2,length(LocP{p-2})))
    if p == 3; ylabel('Loc tuned'); end
    ylim([0 35])
end
% for sb = 1: 6; subplot(3,2,sb); axis([2 50 -0.1 0.17]); end
for sb = 1: 6; subplot(3,2,sb); xlim([0 0.25]); end
ylabel('Number of pairs')
xlabel('prefer. stim - unpref. stim')
legend('Loc tuned','Obj tuned')

mixed = union(mixedP{1},mixedP{2});
Obj = union(ObjP{1},ObjP{2});
Loc = union(LocP{1},LocP{2});
tuned = union(union(Loc,Obj),mixed); length(tuned)

xlabel('Frequency [Hz]')
ylabel('pref. stim - unprefer. stim [coherence]')

length(setdiff(Loc3,Obj3))
length(setdiff(Loc4,Obj4))

length(setdiff(Obj3,Loc3))
length(setdiff(Obj4,Loc4))

[r,beta] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDEObj' 1 '==' 0; 'IDELoc' 1 '==' 0},'type',{'betapos34' [1 3]});r


length(intersect(tuned,beta)) / length(beta)

length(intersect(tuned,beta)) / length(tuned)
figure

subplot(3,1,1);
[n,xout]=hist(bothMonkeysL.data.hist(Obj,:),[0:18]);
[tot,xout] = hist(bothMonkeysL.data.hist(stable,:),[0:18]);
bar(100*n./tot)
title('Object tuned')

subplot(3,1,2);[n,xout]=hist(bothMonkeysL.data.hist(Loc,:),[0:18])
bar(100*n./tot)
title('loc tuned')
subplot(3,1,3);[n,xout]=hist(bothMonkeysL.data.hist(mixed,:),[0:18])
bar(100*n./tot)
title('mixed')


%% Analysis for long delays plus fixation



[r,Loc3] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 3 '>=' 1},'delay','cont');
[r,Loc4] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDELoc' 4 '>=' 1},'delay','cont');

Loc = union(Loc3,Loc4);


[r,Obj3] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 3 '>=' 1},'delay','cont');
[r,Obj4] = get(bothMonkeysL,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0; 'IDEObj' 1 '==' 0; 'IDEObj' 4 '>=' 1},'delay','cont');

Obj = union(Obj3,Obj4);

tuned = union(Loc,Obj);

%% cohgrams

tune = {'Obj' 'Loc'};
limm = [1.8950 1.795];
c = 1 ;
indexes= [];
params = struct('tapers',[2 3],'Fs',200,'trialave',1);
clear fixS coh
for tu = 1 : 2
    for p = [4 3]
        sel = eval(sprintf('%s%d',tune{tu},p));
        for i = sel
            if isempty(find(i == indexes))
                cd(bothMonkeysL.data.setNames{i})
                cd grams
                stim = eval(sprintf('bothMonkeysL.data.IDE%sSeq(i,p,3)',tune{tu}));
                pairg = bothMonkeysL.data.Index(i,10:11);
                file = sprintf('cohgramg%04.0fg%04.0fRule1%s%d.mat',pairg(1),pairg(2),tune{tu},stim);
                fixfile = sprintf('cohgramg%04.0fg%04.0fFixTS.mat',pairg(1),pairg(2));
                
                if isempty(nptDir(file)) || isempty(nptDir(fixfile))
                    keyboard
                else
                    for al = 1 : 2
                        load(file)
                        ti = find(t{al} == limm(al));
                        coh{al}(:,:,c) = C{al}(1:ti,:);
                      
                        
                        load(fixfile)
                        ti = find(t{al} == limm(al));
                        fixS{al}(:,:,c) = C{al}(1:ti,:);
%                         imagesc(t{al},f,C{al}'); colorbar; pause; clf
                    end
                    c = c + 1;
                    indexes = [indexes i];
                end
                cd ../..
            end
        end
    end
end
tiles = [10 25 50 75 90];
figure;
% for ti = 1 : 5
for sb = 1 : 2;
    %     subplot(1,2,(ti-1)*2 + sb);
    %     imagesc(t{sb},f,squeeze(prctile(mts{sb}-fix{sb},tiles(ti),1))');
    subplot(1,2,sb)
    imagesc(t{sb}(1:size(coh{sb},1)),f,squeeze(median(coh{sb}-fixS{sb},3))',[-0.02 0.04]);
    ylim([10 32]);
    %     xlim(tlim(sb,:));
    colorbar;
end
% end

xlabel('Time [sec]; 1.8 sec Match')
ylabel('Frequency [Hz]')
title('MTS - Fix; Match aligned')

subplot(1,2,1)
xlabel('Time [sec]; 1.0 sec Sample off')
ylabel('Frequency [Hz]')
title('MTS - Fix; Sample aligned')

tune = {'Obj' 'Loc'};
limm = [1.9950 1.795];
c = 1 ;
indexes= [];
params = struct('tapers',[2 3],'Fs',200,'trialave',1);
clear allrdiff allpvalues
for tu = 1 : 2
    for p = [4 3]
        sel = eval(sprintf('%s%d',tune{tu},p));
        for i = sel
            if isempty(find(i == indexes))
                cd(bothMonkeysL.data.setNames{i})
                
                stim = eval(sprintf('bothMonkeysL.data.IDE%sSeq(i,p,3)',tune{tu}));
                pairg = bothMonkeysL.data.Index(i,10:11);
                
                cd tuning
                clear rdiff pvalues
                thefile = sprintf('compg%04.0fg%04.0fRule1%s%dvsFixTS.mat',pairg(1),pairg(2),tune{tu},stim);
                if ~isempty(nptDir(thefile))
                    load(thefile);
                    
                    allrdiff(c,:,:) = rdiff;
                    allpvalues(c,:,:,:) = pvalues;
                    c = c + 1;
                    indexes = [indexes i];
                end
                cd ..
            end
            
        end
    end
end




%% stats comparison with fix
allrdiff =filtfilt(repmat(1/2,1,2),1,allrdiff);
allpvalues = filtfilt(repmat(1/2,1,2),1,allpvalues);
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
figure
for p = 1 : 4
    
    subplot(1,4,p)
    plot(f,-100*sum(squeeze(allrdiff(:,:,p)) > squeeze(allpvalues(:,:,5,p)),1) / size(allrdiff,1))
    hold on
    subplot(1,4,p)
    plot(f,100*sum(squeeze(allrdiff(:,:,p)) < squeeze(allpvalues(:,:,1,p)),1) / size(allrdiff,1))
    axis([10 32 -5 20])
    title(epochs{p})
end

xlabel('Frequency [Hz]')
ylabel('% sign. different with fix')

fbeta = find(f>=13 & f<30);

for p = 3 : 4
    sigdiff = squeeze(allrdiff(:,:,p)) < squeeze(allpvalues(:,:,1,p));
    
    [row,col] = find(sigdiff(:,fbeta) == 1);
    nsig{p-2} = unique(row);
    
end
length(union(nsig{1},nsig{2}))




