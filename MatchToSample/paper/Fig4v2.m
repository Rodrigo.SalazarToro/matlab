clear all

cd /Volumes/raid/data/monkey
obj = loadObject('cohInterIDEandLongSorted.mat');
load tunedPairs
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L' 'all'};
labelsc = {                                         'b'   'r'  'k' 'y'   'g'   'c'   'm'};
[r,all] = get(obj,'Number');
[Fx2ygram,Fy2xgram,t,f] = mkAveGCgram(obj,all);
relFreq = [12:22];
figure;

cd /Volumes/raid/data/monkey/
load('IdePairsUnbiased.mat')
[r,ind] = get(obj,'Number');
idePairsObj = ind(uselpairLocs(2,:));
[Fx2ygram,Fy2xgram,t,f] = mkAveGCgram(obj,idePairsObj);
for cc = 1 : 18
    
    if cc <18
        [r,ind] = get(obj,'Number','hist',[cc cc]);
    else
        [r,ind] = get(obj,'Number');
    end
    [ind,ia,ib] = intersect(ind,idePairsObj);
    
    if cc ==18
        
        subplot(5,1,1)
        
        
        imagesc(t,f,squeeze(median(Fx2ygram(idePairs(ib),:,:),1))');
        colorbar;
        caxis([0 0.05])
        subplot(5,1,2);
        imagesc(t,f,squeeze(median(Fy2xgram(idePairs(ib),:,:),1))');
        colorbar
        caxis([0 0.05])
        
        clear betaFx2y betaFy2x
        for ii = 1 : length(ib)
            betaFx2y(ii,:) = squeeze(mean(Fx2ygram(idePairs(ib(ii)),:,relFreq),3));
            betaFy2x(ii,:) = squeeze(mean(Fy2xgram(idePairs(ib(ii)),:,relFreq),3));
        end
        [p,table,stats] = anova2([betaFx2y; betaFy2x],size(betaFx2y,2));
        subplot(5,1,3)
        me(1,:) = mean(betaFx2y,1);
        sem = std(betaFx2y,1) / sqrt(length(idePairs));
        me(2,:) = me(1,:) - sem;
        me(3,:) = me(1,:) + sem;
        
        plot(t(1:size(me,2)),me','b')
        
        hold on
        me(1,:) = mean(betaFy2x,1);
        sem = std(betaFy2x,1) / sqrt(length(idePairs));
        me(2,:) = me(1,:) - sem;
        me(3,:) = me(1,:) + sem;
        
        plot(t(1:size(me,2)),me','r')
        
        legend('PPC->PFC','PFC->PPC')
        
        xlabel('Time [sec]')
        ylabel('GC')
        xlim([0.11 1.8])
        ylim([0.02 0.05])
        set(gca,'TickDir','out');
        subplot(5,1,4)
        
        me(1,:) = mean(betaFx2y - betaFy2x,1);
        sem = std(betaFx2y - betaFy2x,1) / sqrt(length(idePairs));
        h = ttest(betaFx2y - betaFy2x,0.00026);
        me(2,:) = me(1,:) - sem;
        me(3,:) = me(1,:) + sem;
        me = me .* repmat(h,3,1);
        plot(t(1:size(me,2)),me)
        hold on
        line([0 1.8],[0 0])
        xlabel('Time [sec]')
        ylabel('GC')
        xlim([0.11 1.8])
        ylim([0 0.01])
        
    elseif cc >= 8 && cc < 14
        for ii = 1 : length(ib)
            betaFx2y(ii,:) = squeeze(mean(Fx2ygram(idePairs(ib(ii)),:,relFreq),3));
            betaFy2x(ii,:) = squeeze(mean(Fy2xgram(idePairs(ib(ii)),:,relFreq),3));
        end
        subplot(5,1,5)
        me(1,:) = mean(betaFx2y,1);
        sem = std(betaFx2y,1) / sqrt(length(idePairs));
        %              me(1,:) = mean(betaFx2y - betaFy2x,1);
        %             sem = std(betaFx2y - betaFy2x,1) / sqrt(length(idePairs));
        me(2,:) = me(1,:) - sem;
        me(3,:) = me(1,:) + sem;
        plot(t(1:size(me,2)),me,labelsc{cc-7})
        hold on
        xlim([0.11 1.8])
    end
    
end


%% MI on psth

clear all
cd /Volumes/raid/data/monkey

% psth = loadObject('psthObjIDEandLong999RT20ms.mat');

% psth = loadObject('psthIDEandLong99bin100ms.mat');
psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');

areasName = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

cortex{1} = [1:7 17];
cortex{2} = [8:16];
% cortex{3} = [11 13];

clear tuning sigtuning FR ind
for area = 1 : 2
    [r,ind1] = get(psth,'Number','cortex',cortex{area},'ideTuned',1,'epochTuned',1);
    [r,ind2] = get(psth,'Number','cortex',cortex{area},'ideTuned',1,'epochTuned',2);
    ind{area} = union(ind1,ind2);
%     [tuning{area},time,sigtuning{area}] = mkPopPSTHMI(psth,ind,'type','IdePerLoc','biasCorr','redo','binSize',100,'periodSelection','both','sigLevel',99);

%      [tuning{area},time,sigtuning{area}] = mkPopPSTHMI(psth,ind{area},'type','IdePerLoc','biasCorr','redo','binSize',100,'periodSelection','both','sigLevel',99);
%      [FR{area},time] = mkPopPSTHMI(psth,ind{area},'type','PSTH','redo','binSize',100,'periodSelection','both','sigLevel',99);

     [tuning{area},time,sigtuning{area}] = mkPopPSTHMI(psth,ind{area},'type','Ide','biasCorr','redo','binSize',100,'periodSelection','both','sigLevel',99);

end
figure
lab = {'r' 'b' 'k'};

for a = 1 : 2
    subplot(2,1,1)
    me = 100*sum(sigtuning{a},1)/size(sigtuning{a},1);
    time = [-400:100:2500];
    plot(time,me,lab{a})
    hold on
    xlim([-500 1300])
    ylabel('% significance for ide')
    legend(sprintf('PFC; n=%d',size(tuning{1},1)),sprintf('PPC; n=%d',size(tuning{2},1))); %,sprintf('PPCl; n=%d',size(tuning{2},1)))
    subplot(2,1,2)
    me = mean(tuning{a},1);
    ste = std(tuning{a},1) /sqrt(size(tuning{a},1));
    plot(time,me,lab{a})
    hold on
    plot(time,me + ste,lab{a})
    plot(time,me - ste,lab{a})
    xlim([-500 1300])
    ylabel('MI [bits]')
    
 set(gca,'TickDir','out');
end
xlabel('Time [msec]')

figure
subplot(3,1,1)
[n,xout] = hist(psth.data.Index([ind{1} ind{2}],3),[1:17]);
bar(n)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areasName)
ylabel('Number of ide tuned cells')


for area = 1 : 17
[totcells(area),~] = get(psth,'Number','cortex',area);
text(area,50,sprintf('%d',totcells(area)))
end

ylim([0 60])
subplot(3,1,2)

bar(n./totcells)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areasName)
ylabel('% ide tuned cells')


for area = 1 : 2
    [r,ind1] = get(psth,'Number','cortex',cortex{area},'locTuned',1,'epochTuned',1);
    [r,ind2] = get(psth,'Number','cortex',cortex{area},'locTuned',1,'epochTuned',2);
    ind{area} = union(ind1,ind2);
    [tuning{area},time,sigtuning{area}] = mkPopPSTHMI(psth,ind{area},'type','Ide','biasCorr','redo','binSize',100,'periodSelection','both','sigLevel',99);

end
subplot(3,1,3)
[n,xout] = hist(psth.data.Index([ind{1} ind{2}],3),[1:17]);
bar(n)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areasName)
ylabel('Number of loc tuned cells')
% /Volumes/raid/data/monkey/betty/100128/session01/group0048/cluster01s
% area LIP
% area LIP
cd /Volumes/raid/data/monkey/betty/100128/session01/group0048/cluster01s
load nineStimPSTH1.mat

objA = cell2mat(A([1:3],1)); objB = cell2mat(A([4:6],1));objC = cell2mat(A([7:9],1));


for bins = 1 : 18
ObjFR(1,bins) = 10*sum(sum(objA(:,(bins-1)*100+1: bins*100),1),2)/ size(objA,1);
ObjFR(2,bins) = 10*sum(sum(objB(:,(bins-1)*100+1: bins*100),1),2)/ size(objB,1);
ObjFR(3,bins) = 10*sum(sum(objC(:,(bins-1)*100+1: bins*100),1),2)/ size(objC,1);
end
figure
plot([0.1:0.1:1.8],ObjFR')

%% SFC

cd /Volumes/raid/data/monkey/
psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');
obj = loadObject('cohInterIDEandLongSorted.mat');
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

[r,locTuned1] = get(psth,'Number','LocTuned',1,'epochTuned',1);
[r,locTuned2] = get(psth,'Number','LocTuned',1,'epochTuned',2);
locTuned = union(locTuned1,locTuned2);
[r,ideTuned1] = get(psth,'Number','ideTuned',1,'epochTuned',1);
[r,ideTuned2] = get(psth,'Number','ideTuned',1,'epochTuned',2);
ideTuned = union(ideTuned1,ideTuned2);

[r,allCells] = get(psth,'Number');
notTunedCells = setdiff(allCells,union(locTuned, ideTuned));
tuned = union(ideTuned,locTuned);
[ide,xout] = hist(psth.data.Index(locTuned,3),[1:18]);
[loc,xout] = hist(psth.data.Index(ideTuned,3),[1:18]);

%% options
% all data

[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,notTunedCells);
[PPCsfc,PPCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,notTunedCells);
[PFCsfc,PFCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);

[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,ideTuned);
[PPCsfcTi,PPCsfcSurTi,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,ideTuned);
[PFCsfcTi,PFCsfcSurTi,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);

[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,locTuned);
[PPCsfcTl,PPCsfcSurTl,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,locTuned);
[PFCsfcTl,PFCsfcSurTl,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);

[r,ind] = get(psth,'Number','cortex',[8:13]);

[PPCsfcalll,PPCsfcSurTl,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);
[r,ind] = get(psth,'Number','cortex',[1:5 17]);

[PFCsfcTl,PFCsfcSuralll,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2);

% only betapos34
% [r,betaPairsObj] = get(obj,'Number','type',{'betapos34' [1 3]});
% [r,ind] = get(psth,'Number','cortex',[8:13]);
% [PPCsfc,PPCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',betaPairsObj,'redo');
% [r,ind] = get(psth,'Number','cortex',[1:5 17]);
% [PFCsfc,PFCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',betaPairsObj,'redo');

% only MI pairs
load tunedPairs
[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,notTunedCells);
[PPCsfc,PPCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,notTunedCells);
[PFCsfc,PFCsfcSur,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');

[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,ideTuned);
[PPCsfcTi,PPCsfcSurTi,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,ideTuned);
[PFCsfcTi,PFCsfcSurTi,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');

[r,ind] = get(psth,'Number','cortex',[8:13]);
ind = intersect(ind,locTuned);
[PPCsfcTl,PPCsfcSurTl,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');
[r,ind] = get(psth,'Number','cortex',[1:5 17]);
ind = intersect(ind,locTuned);
[PFCsfcTl,PFCsfcSurTl,f,~] = mkAveSFC(psth,ind,'redo','sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');

%%

figure
sigv = 100 * sum(PFCsfc > PFCsfcSur,1) ./ size(PFCsfc,1);
plot(f,sigv)
hold on
sigv = 100 * sum(PFCsfcTi > PFCsfcSurTi,1) ./ size(PFCsfcTi,1);
plot(f,sigv,'--')
sigv = 100 * sum(PFCsfcTl > PFCsfcSurTl,1) ./ size(PFCsfcTl,1);
plot(f,sigv,'-.')
sigv = 100 * sum(PPCsfc > PPCsfcSur,1) ./ size(PPCsfc,1);
plot(f,sigv,'r')
sigv = 100 * sum(PPCsfcTi > PPCsfcSurTi,1) ./ size(PPCsfcTi,1);
plot(f,sigv,'r--')
sigv = 100 * sum(PPCsfcTl > PPCsfcSurTl,1) ./ size(PPCsfcTl,1);
plot(f,sigv,'r-.')
xlabel('Frequency [Hz]')
ylabel('% pair')
legend(sprintf('n=%d PFC unsel. cells',size(PFCsfc,1)),sprintf('n=%d PFC ide cells',size(PFCsfcTi,1)),sprintf('n=%d PFC loc cells',size(PFCsfcTl)),sprintf('n=%d PPC unsel. cells',size(PPCsfc,1)),sprintf('n=%d PPC ide cells',size(PPCsfcTi,1)),sprintf('n=%d PPC loc cells',size(PPCsfcTl,1)))


relFreq = [12:22];
betaf = find(f >= 12 & f <= 22);
 load tunedPairs
clear sfc sfcSur sfcT sfcSurT
for cc = 1 : 17
    [r,ind] = get(psth,'Number','cortex',cc);
    ind = intersect(ind,notTunedCells);
    if ~isempty(ind)
        [sfc{cc},sfcSur{cc},f,phi{cc},~,~,allgroups{cc},alldepths{cc},alldays{cc},allunittype{cc},allnSUA{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    end
end

for cc = 1 : 17
    [r,ind] = get(psth,'Number','cortex',cc);
    %     ind = intersect(ind,ideTuned);
    ind = intersect(ind,ideTuned);
    if ~isempty(ind)
        [sfcT{cc},sfcSurT{cc},f,phiT{cc},~,~,allgroupsT{cc},alldepthsT{cc},alldaysT{cc},allunittypeT{cc},allnSUAT{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    end
end

for cc = 1 : 17
    [r,ind] = get(psth,'Number','cortex',cc);
    %     ind = intersect(ind,ideTuned);
    ind = intersect(ind,locTuned);
    if ~isempty(ind)
        [sfcTl{cc},sfcSurTl{cc},f,phiTl{cc},~,~,allgroupsl{cc},alldepthsl{cc},alldaysl{cc},allunittypel{cc},allnSUAl{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    end
end
for cc = 1 : 17
    [r,ind] = get(psth,'Number','cortex',cc);
    %     ind = intersect(ind,ideTuned);
    
    if ~isempty(ind)
        [sfcall{cc},sfcSurall{cc},f,phiall{cc},~,~,allgroupsall{cc},alldepthsall{cc},alldaysall{cc},allunittypeall{cc},allnSUAall{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    end
end
clear betaprcsig totpairs
for cc = 1 : 17
    [allpairs,~,~]  = unique([allgroupsall{cc} alldepthsall{cc} alldaysall{cc} allunittypeall{cc} allnSUAall{cc}],'rows');
    totpairs{cc} = length(allpairs);
end
for cc = 1 : 17;
    if ~isempty(sfc{cc});
        %         prctsigt = 100*sum(sfc{cc} > sfcSur{cc},1)./size(sfcall{cc},1);
        [thepairs,~,ccInd]  = unique([allgroups{cc} alldepths{cc} alldays{cc},allunittype{cc},allnSUA{cc}],'rows');
        allnsig = zeros(1,65);
        for pp = 1 : size(thepairs,1)
            duplicates = find(pp == ccInd);
            
            
            rv = nanmean(sfc{cc}(duplicates,:),1);
            rvsur = nanmean(sfcSur{cc}(duplicates,:),1);
            nsig = rv > rvsur;
            
            allnsig = allnsig + nsig;
        end
        prctsigt = 100*allnsig ./ totpairs{cc};
        betaprcsig(1,cc) = mean(prctsigt(betaf),2);
    end
    if ~isempty(sfcT{cc});
        %         prctsigt = 100*sum(sfcT{cc} > sfcSurT{cc},1)./size(sfcall{cc},1);
        [thepairs,~,ccInd]  = unique([allgroupsT{cc} alldepthsT{cc} alldaysT{cc},allunittypeT{cc},allnSUAT{cc}],'rows');
        allnsig = zeros(1,65);
        for pp = 1 : size(thepairs,1)
            duplicates = find(pp == ccInd);
            
            
            rv = nanmean(sfcT{cc}(duplicates,:),1);
            rvsur = nanmean(sfcSurT{cc}(duplicates,:),1);
            nsig = rv > rvsur;
            
            allnsig = allnsig + nsig;
        end
        prctsigt = 100*allnsig ./ totpairs{cc};
        betaprcsig(2,cc) = mean(prctsigt(betaf),2);
    end
    if ~isempty(sfcTl{cc});
        %         prctsigt = 100*sum(sfcTl{cc} > sfcSurTl{cc},1)./size(sfcall{cc},1);
        [thepairs,~,ccInd]  = unique([allgroupsl{cc} alldepthsl{cc} alldaysl{cc},allunittypel{cc},allnSUAl{cc}],'rows');
        allnsig = zeros(1,65);
        for pp = 1 : size(thepairs,1)
            duplicates = find(pp == ccInd);
            
            
            rv = nanmean(sfcTl{cc}(duplicates,:),1);
            rvsur = nanmean(sfcSurTl{cc}(duplicates,:),1);
            nsig = rv > rvsur;
            
            allnsig = allnsig + nsig;
        end
        prctsigt = 100*allnsig ./ totpairs{cc};
        betaprcsig(3,cc) = mean(prctsigt(betaf),2);
    end
end

figure; bar(betaprcsig([1 3],:)')
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areas)
set(gca,'TickDir','out');
legend('nonsel.','ide sel.')

for cc = 1 : 17
    [r,ind] = get(psth,'Number','cortex',cc);
    %     ind = intersect(ind,ideTuned);
    
    if ~isempty(ind)
        [sfcall{cc},sfcSurall{cc},f,phiall{cc},~,~,allgroups{cc},alldepths{cc},alldays{cc},allunittype{cc},allnSUA{cc}] = mkAveSFC(psth,ind,'redo','sigLevel',2);%,'cohObj',obj,'cohInd',idePairsObj,'redo');
    end
end
clear thepairs prctsig
for cc = [1:5 8:13 17]
    [thepairs,~,ccInd]  = unique([allgroups{cc} alldepths{cc} alldays{cc} allunittype{cc} allnSUA{cc}],'rows');
    allnsig = zeros(1,65);
    for pp = 1 : size(thepairs,1)
        duplicates = find(pp == ccInd);
        
        
        rv = mean(sfcall{cc}(duplicates,:),1);
        rvsur = mean(sfcSurall{cc}(duplicates,:),1);
        nsig = rv > rvsur;
        
        allnsig = allnsig + nsig;
    end
    prctsig(cc,:) = 100*allnsig ./ length(thepairs); 
end




% for cc = 1 : 17; if ~isempty(sfcall{cc});prctsig(cc,:) = 100*sum(sfcall{cc} > sfcSurall{cc},1)./size(sfcall{cc},1); end; end

betaprcsig = mean(prctsig(:,betaf),2);
figure; bar(betaprcsig)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areas)
set(gca,'TickDir','out');
% for sb = 1 : 17;
%     if ~isempty(sfc{sb});
%         subplot(6,3,sb);
%         plot(f,mean(sfc{sb},1));
%         hold on
%         plot(f,mean(sfc{sb},1) + std(sfc{sb},1)/sqrt(size(sfc{sb},1)));
%         plot(f,mean(sfc{sb},1) - std(sfc{sb},1)/sqrt(size(sfc{sb},1)));
%         xlim([0 50])
%         ylim([0 0.05])
%         title(areas{sb});
%     end;
% end


PFCsfc = [sfc{1}; sfc{2}; sfc{3}; sfc{4}; sfc{5}; sfc{17}];
PFCsfcSur = [sfcSur{1}; sfcSur{2}; sfcSur{3}; sfcSur{4}; sfcSur{5}; sfcSur{17}];
PPCsfc = [sfc{8}; sfc{9}; sfc{10}; sfc{11}; sfc{12}; sfc{13}];
PPCsfcSur = [sfcSur{8}; sfcSur{9}; sfcSur{10}; sfcSur{11}; sfcSur{12}; sfcSur{13}];


figure
sigv = 100 * sum(PFCsfc > PFCsfcSur,1) ./ size(PFCsfc,1);
plot(f,sigv)
ylim([0 25])
hold on

sigv = 100 * sum(PPCsfc > PPCsfcSur,1) ./ size(PPCsfc,1);
plot(f,sigv,'r')
ylim([0 25])

xlabel('Frequency [Hz]')
ylabel('% pairs')


PFCsfcT = [sfcT{1}; sfcT{2}; sfcT{3}; sfcT{4}; sfcT{5}; sfcT{17}];
PFCsfcSurT = [sfcSurT{1}; sfcSurT{2}; sfcSurT{3}; sfcSurT{4}; sfcSurT{5}; sfcSurT{17}];
PPCsfcT = [sfcT{8}; sfcT{9}; sfcT{10}; sfcT{11}; sfcT{12}; sfcT{13}];
PPCsfcSurT = [sfcSurT{8}; sfcSurT{9}; sfcSurT{10}; sfcSurT{11}; sfcSurT{12}; sfcSurT{13}];



sigv = 100 * sum(PFCsfcT > PFCsfcSurT,1) ./ size(PFCsfcT,1);
plot(f,sigv,'--')
ylim([0 25])
hold on

sigv = 100 * sum(PPCsfcT > PPCsfcSurT,1) ./ size(PPCsfcT,1);
plot(f,sigv,'r--')
ylim([0 25])

xlabel('Frequency [Hz]')
ylabel('% pairs')

legend(sprintf('PFC not Tuned cells; n= %d',size(PFCsfc,1)),sprintf('PPC not Tuned cells; n= %d',size(PPCsfc,1)),sprintf('PFC ide cells; n= %d',size(PFCsfcT,1)),sprintf('PPC ide cells; n= %d',size(PPCsfcT,1)))
set(gca,'TickDir','out');
betaf = find(f >=12 & f <= 22);


for cc = 1 : 17
    if ~isempty(sfcT{cc})
        sigcoh = squeeze(sfcT{cc}(:,betaf)) > squeeze(sfcSurT{cc}(:,betaf));
        prctsig(cc) =  mean(100*sum(sigcoh,1)/size(sigcoh,1));
    end
end

figure;
subplot(2,1,1)
bar(prctsig)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areas)



[r,ind] = get(psth,'Number');
ind = intersect(ind,ideTuned);
if ~isempty(ind)
    [sfcLFP,sfcLFPSur,~,~,~,sfcLFPc] = mkAveSFC(psth,ind,'redo','sigLevel',3);%,'sigLevel',2,'cohObj',obj,'cohInd',idePairsObj,'redo');
end
clear prctsig
for cc = 1 : 17
    theind = find(sfcLFPc == cc);
    sigcoh = squeeze(sfcLFP(theind,betaf)) > squeeze(sfcLFPSur(theind,betaf));
    prctsig(cc) =  mean(100*sum(sigcoh,1)/size(sigcoh,1));
end

subplot(2,1,2)
bar(prctsig)
set(gca,'XTick',[1:17])
set(gca,'XTickLabel',areas)


%% medial lateral split
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

PFCsfcall = [sfc{1}; sfc{2}; sfc{3}; sfc{4}; sfc{5}; sfc{17}];
% PFCsfcSur = [sfcSur{1}; sfcSur{2}; sfcSur{3}; sfcSur{4}; sfcSur{5}; sfcSur{17}];
PFCsfcTall = [sfcT{1}; sfcT{2}; sfcT{3}; sfcT{4}; sfcT{5}; sfcT{17}];
PFCsfcTlall = [sfcTl{1}; sfcTl{2}; sfcTl{3}; sfcTl{4}; sfcTl{5}; sfcTl{17}];
% PFCsfcSurT = [sfcSurT{1}; sfcSurT{2}; sfcSurT{3}; sfcSurT{4}; sfcSurT{5}; sfcSurT{17}];
PPCsfcall = [sfc{8}; sfc{9}; sfc{10}; sfc{11}; sfc{12}; sfc{13}];
PPCsfcTall = [sfcT{8}; sfcT{9}; sfcT{10}; sfcT{11}; sfcT{12}; sfcT{13}];
PPCsfcTlall = [sfcTl{8}; sfcTl{9}; sfcTl{10}; sfcTl{11}; sfcTl{12}; sfcTl{13}];
figure
c = 1;
clear sigvv sigvvT sigvvTl nareas nareasT
for thearea = [8 10 12]
    if ~isempty(sfc{thearea}) && ~isempty(sfcT{thearea})
        PPCsfc = sfc{thearea};
        PPCsfcSur = sfcSur{thearea};
        PPCsfcT = sfcT{thearea};
        PPCsfcSurT = sfcSurT{thearea};
         PPCsfcTl = sfcTl{thearea};
        PPCsfcSurTl = sfcSurTl{thearea};
        
        sigvv(c,:) = 100 * sum(PPCsfc > PPCsfcSur,1) ./ size(PPCsfc,1);
        sigvvT(c,:) = 100 * sum(PPCsfcT > PPCsfcSurT,1) ./ size(PPCsfc,1);
        sigvvTl(c,:) = 100 * sum(PPCsfcTl > PPCsfcSurTl,1) ./ size(PPCsfc,1);
        mag(c,:) = mean(PPCsfc,1);
        nareas(c) = size(PPCsfc,1);
        nareasT(c) = size(PPCsfcT,1);
    else
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        
    end
    c=c+1;
end
subplot(2,1,1)
plot(f,sigvv)
hold on
plot(f,sigvvT,'--')
hold on
plot(f,sigvvTl,'-.')
ylim([0 20])
legend(areas([8 10 12]))
title(nareas)

c = 1;
clear sigvv sigvvT clear sigvvTl nareas nareasT
for thearea = [11 13]
    if ~isempty(sfc{thearea}) && ~isempty(sfcT{thearea})
        PPCsfc = sfc{thearea};
        PPCsfcSur = sfcSur{thearea};
        PPCsfcT = sfcT{thearea};
        PPCsfcSurT = sfcSurT{thearea};
        PPCsfcTl = sfcTl{thearea};
        PPCsfcSurTl = sfcSurTl{thearea};
        
        sigvv(c,:) = 100 * sum(PPCsfc > PPCsfcSur,1) ./ size(PPCsfc,1);
        sigvvT(c,:) = 100 * sum(PPCsfcT > PPCsfcSurT,1) ./ size(PPCsfc,1);
        sigvvTl(c,:) = 100 * sum(PPCsfcTl > PPCsfcSurTl,1) ./ size(PPCsfc,1);
        mag(c,:) = mean(PPCsfc,1);
        nareas(c) = size(PPCsfc,1);
        nareasT(c) = size(PPCsfcT,1);
    else
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        sigvv(c,:) = zeros(1,size(sigvv,2));
        
    end
    c=c+1;
end
subplot(2,1,2)
plot(f,sigvv)
hold on
plot(f,sigvvT,'--')
hold on
plot(f,sigvvTl,'-.')
xlabel('Frequency [Hz]')
ylabel('% pairs')
ylim([0 20])
legend(areas([11 13]))
title(nareas)
%%



f1 = figure;
f2 = figure;

relf = find(f <=50);
betaf = find(f >= 12 & f<=22);
count = 1;
for cc = [1 : 5 17]
    sigv = zeros(1,65);
    sigphif =[];
    
    
    if ~isempty(sfcT{cc})
        sigcoh = squeeze(sfcT{cc}(:,betaf)) > squeeze(sfcSurT{cc}(:,betaf));
        
        
        %         tphi = phi{cc}(:,betaf);
        %         sigphi = tphi .* sigcoh;
        %         sigphi(sigcoh == 0) = nan; % remove zero values from not significant coherence
        %         sigphif = reshape(sigphi,size(sigphi,1) * size(sigphi,2),1);
        
        sigv = 100 * sum(sfcT{cc} > sfcSurT{cc},1) ./ size(sfcT{cc},1);
        
        
        figure(f1)
        subplot(2,6,count)
        plot(f,sigv)
        ylim([0 25])
        title(sprintf('%s n= %d',areas{cc},size(sfcT{cc},1)))
    end
    figure(f2)
    %     sigphip = nanmean(sigphif,2);
    sigphip = reshape(sigphif,size(sigphif,1) * size(sigphif,2),1);
    subplot(2,6,count)
    rose(sigphip)
    
    title(sprintf('%s n= %d',areas{cc},sum(~isnan(sigphip))))
    count = count + 1;
end
count = 1;
for cc = [8 : 13]
    sigv = zeros(1,65);
    
    sigphif =[];
    sigphi = zeros(1,65);
    
    if ~isempty(sfcT{cc})
        sigcoh = squeeze(sfcT{cc}(:,betaf)) > squeeze(sfcSurT{cc}(:,betaf));
        %         tphi = phi{cc}(:,betaf);
        %         sigphi = tphi .* sigcoh;
        %         sigphi(sigcoh == 0) = nan;
        %
        %         sigphif = reshape(sigphi,size(sigphi,1) * size(sigphi,2),1);
        sigv = 100 * sum(sfcT{cc} > sfcSurT{cc},1) ./ size(sfcT{cc},1);
        
        
        figure(f1)
        subplot(2,6,6+ count)
        plot(f,sigv)
        ylim([0 25])
        title(sprintf('%s n= %d',areas{cc},size(sfcT{cc},1)))
    end
    figure(f2)
    sigphip = reshape(sigphif,size(sigphif,1) * size(sigphif,2),1);
    subplot(2,6,6+count)
    rose(sigphip)
    title(sprintf('%s n= %d',areas{cc},sum(~isnan(sigphip))))
    count = count + 1;
end

%% GC
%% data split according to areas and not areas pair
cd /Volumes/raid/data/monkey/
obj = loadObject('cohInterIDEandLongSorted.mat');
[r,ind] = get(obj,'Number','snr',99);
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

% [r,ind] = get(obj,'Number','snr',99,'type',{'gammaneg14' [1 3]});r
areasN = unique(obj.data.hist(ind,:));
nareas = [];
clear nareasN
for a = 1 : length(areasN)
    %         [r,ind] = get(obj,'Number','snr',99,'type',{type selec},'hist',[areasN(a) nan]);
    [r,ind] = get(obj,'Number','snr',99,'hist',[areasN(a) nan]);
    nareasN(a) = r;
    if r >= 40
        nareas = [nareas; areasN(a)];
    end
end
figure
dirL = {'-' '--'};

load tunedPairs.mat
for dir = [-1 1]
    for a = 1 : length(nareas)
        %         [r,ind] = get(obj,'Number','snr',99,'type',{type selec},'hist',[nareas(a) nan]);
        [r,ind] = get(obj,'Number','snr',99,'hist',[nareas(a) nan]);
        ind = intersect(ind,idePairsObj);
        r = length(ind);
        clear sigdiff
        
        
        % testfig = figure;
        for i = 1 : r;
            
            cd(obj.data.setNames{ind(i)});
            cmb = obj.data.Index(ind(i),1);
            load GCcohInter.mat
            if Day.session(1).rule == 1
                s = 1;
            else
                s = 2;
            end
            
            
            Fx2y = squeeze(Day.session(s).Fx2y(:,cmb,:));
            Fy2x = squeeze(Day.session(s).Fy2x(:,cmb,:));
            load GCgeneralSur.mat
            Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
            Fy2xsur = squeeze(Day.session(s).Fx2y(:,3,:));
            siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);
            
            cd GCdirStat
            pairg = obj.data.Index(ind(i),10:11);
            pwd;
            file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),1);
            load(file,'Fdiff')
            Fdiff = Fdiff .* siggc;
            sfile = sprintf('GCintergeneralRule%d.mat',1);
            load(sfile,'pvalues','levels')
            %     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
            
            pind = find(0.01 == levels);
            % Fdiff = Fx2y - Fy2x;
            
            
            
            if ((nareas(a) <= 7 || nareas(a) == 17) && dir == -1) || (~(nareas(a) <= 7 || nareas(a) == 17) && dir == 1)
                sigdiff(i,:,:) = (Fdiff <= squeeze(pvalues(:,pind(1),:))) ; % negative means PFC dominate
            else
                sigdiff(i,:,:) = (Fdiff >= squeeze(pvalues(:,pind(2),:))) ; % positive means PPC dominates
            end
        end
        %     alldiff(a,:,:) = 100*squeeze(sum(sigdiff,1))/r;
        perDir = 100*squeeze(sum(sigdiff,1))/r;
        
        if dir == 1
            receiver(a) = mean(perDir(relFreq));
        else
            sender(a) = mean(perDir(relFreq));
        end
        
        subplot(2,ceil(length(nareas)/2),a);
        
        plot(-dir * perDir);
        hold on
        ylim([-60 60]);xlim([0 50]);
        if nareas(a) ~= 99;title(sprintf('n=%d; %s',r,areas{nareas(a)}));end
        
        set(gca,'TickDir','out');
    end
end

figure
subplot(2,1,1)
bar(sender)
set(gca,'XTickLabel',areas(nareas))
title('Senders')
xlabel('cortical areas')
ylabel('% pairs')
ylim([0 40])
set(gca,'TickDir','out');

subplot(2,1,2)
bar(receiver)
set(gca,'XTickLabel',areas(nareas))
title('Receivers')
xlabel('cortical areas')
ylabel('% pairs')
ylim([0 40])
set(gca,'TickDir','out');