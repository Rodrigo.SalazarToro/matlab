cd /Volumes/raid/data/monkey/

bothMonkeysL = loadObject('cohInterIDEL.mat');

[allrdiff,indexes,alltrials,allC,allgram,t,fgram,sigdrop] = findExpMatchOnset(bothMonkeysL,'plotSig','nSTD',4);

n = length(allrdiff{2});

[allrdiff,indexes,alltrials,allC,allgram,t,fgram,sigdrop] = findExpMatchOnset(bothMonkeysL,'type',{'betapos34' [1 3]},'plotSig','nSTD',4);

trange = find(t > 0.55 & t < 0.8);
fbeta = find(fgram > 13 & fgram < 30);

for d = 1 : 2; sig = sum(sum(sigdrop{1,d}(:,trange,fbeta),2),3) ~=0; ind{d} = find(sig == 1); end;

length(union(ind{1},ind{2})) / length(allrdiff{1})
% figure
% for c = 1 : 123;
%     imagesc(t,f,squeeze(allgram{1}(c,1,:,:))');
%     ylim([10 45]); xlim([0.05 0.75]);
%     line([Args.plength/1000 Args.plength/1000], [fgram(1) fgram(end)],'Color','r');
%     pause; 
% end
InspectGUI(bothMonkeysL,'plotType','longDelays','coh','delay','bin','type',{'betapos34' [1 3]},'frange',[12 32])

% ex 16