cd /Volumes/raid/data/monkey/clark


psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');
coh = loadObject('cohInterIDEandLongSorted.mat');
[r,betapos34] = get(coh,'Number');
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

[r,ind] = get(psth,'Number','unit',{'m'},'cortex',[1:7 17]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPFCm = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'m'},'cortex',[8: 16]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPPCm = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'m'});
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 ntot = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'s'},'cortex',[1:7 17]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPFCs = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'s'},'cortex',[8: 16]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPPCs = length(ind)
 
 
 cd /Volumes/raid/data/monkey/betty


psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');
coh = loadObject('cohInterIDEandLongSorted.mat');
[r,betapos34] = get(coh,'Number');
areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

[r,ind] = get(psth,'Number','unit',{'m'},'cortex',[1:7 17]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPFCm = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'m'},'cortex',[8: 16]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPPCm = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'m'});
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 ntot = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'s'},'cortex',[1:7 17]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPFCs = length(ind)
 
 [r,ind] = get(psth,'Number','unit',{'s'},'cortex',[8: 16]);
 [ind,cind2] = comparePsthandcohObjs(obj,ind,coh,betapos34);
 nPPCs = length(ind)
