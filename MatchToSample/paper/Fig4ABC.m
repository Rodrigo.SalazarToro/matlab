clear all

cd /Volumes/raid/data/monkey/

obj = loadObject('cohInterIDEandLongSorted.mat');
[r,ind] = get(obj,'Number','snr',99);

areas = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

areas{99} = [];


sigdiffn = zeros(r,100,4);
sigdiffp = zeros(r,100,4);
plevel = [0.01];
% testfig = figure;


% sig. coherence pairs
cd /Volumes/raid/data/monkey
load sigCoh&MIPerLoc.mat
DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCoh));
uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = ind(DEsigCoh(duplicates(1)));
end

pairtypes = unique(obj.data.hist(uniquePairs,:),'rows');

histo = zeros(3,size(pairtypes,1));
for ii = 1 : size(pairtypes,1)
    count = ismember(obj.data.hist(uniquePairs,:),pairtypes(ii,:),'rows');
    histo(3,ii) = sum(count);
    
end

% sig. cohMI
cd /Volumes/raid/data/monkey/
load('IdePairsUnbiased.mat')
idepairs = ind(uselpairLocs(2,:));
locs = uselpairLocs(1,:);

%% GC
sdir = pwd;

PPCtoPFC = zeros(35,length(idepairs));
PFCtoPPC = zeros(35,length(idepairs));
for ii = 1 : length(idepairs)
    
    cd(obj.data.setNames{idepairs(ii)});
    grs = obj.data.Index(idepairs(ii),10:11);
    cd grams
    
    load(sprintf('migramg%04.0fg%04.0fRule1IdePerLoc%d.mat',grs(1),grs(2),locs(ii)),'C');
    mcoh = zeros(3,1);
    for ide = 1 : 3
        mcoh(ide) = mean(mean(C{ide}(21:33,5:8)));
    end
    [~,ic] = max(mcoh);
    load(sprintf('gcgramg%04.0fg%04.0fRule1Loc%dIde%d.mat',grs(1),grs(2),locs(ii),ic));
    
    PPCtoPFC(:,ii) = mean(Fx2y(1:35,12:22),2);
    PFCtoPPC(:,ii) = mean(Fy2x(1:35,12:22),2);
    
end

p = anova2([PPCtoPFC';PFCtoPPC'],465);
time = [0.1:0.05:1.8];
figure; 
subplot(3,1,1)
me = mean(PPCtoPFC,2);
ste = std(PPCtoPFC,[],2) / sqrt(438);
ste2 = std(PPCtoPFC,[],2) / sqrt(65);

plot(time,me)
hold on
plot(time,me+ste,'--')
plot(time,me+ste2,'--')
plot(time,me-ste,'--')
plot(time,me-ste2,'--')


me = mean(PFCtoPPC,2);
ste = std(PFCtoPPC,[],2) / sqrt(438);
ste2 = std(PFCtoPPC,[],2) / sqrt(65);

plot(time,me,'r')
hold on
plot(time,me+ste,'r--')
plot(time,me+ste2,'r--')
plot(time,me-ste,'r--')
plot(time,me-ste2,'r--')

xlim([0.1 1.8])
ylim([0.02 0.045])

xlabel('time (s)')
ylabel('WGC')

relFreq = 12:22;
%% senders and receivers analysis
plevels = 0.01;

transmitter = zeros(length(idepairs),1);
for ii = 1 : length(idepairs)
    
    cd(obj.data.setNames{idepairs(ii)});
    grs = obj.data.Index(idepairs(ii),10:11);
    cmb = obj.data.Index(idepairs(ii),1);
    load GCcohInter.mat
    if Day.session(1).rule == 1
        s = 1;
    else
        s = 2;
    end
    
    
    Fx2y = squeeze(Day.session(s).Fx2y(:,cmb,:));
    Fy2x = squeeze(Day.session(s).Fy2x(:,cmb,:));
    load GCgeneralSur.mat
    Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
    Fy2xsur = squeeze(Day.session(s).Fx2y(:,3,:));
    siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);
    
    cd GCdirStat
    pairg = obj.data.Index(idepairs(ii),10:11);
    pwd;
    file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',grs(1),grs(2),1);
    load(file,'Fdiff')
    Fdiff = Fdiff .* siggc;
    sfile = sprintf('GCintergeneralRule%d.mat',1);
    load(sfile,'pvalues','levels')
    %     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
    
    pind = find(plevel == levels);
    
    receiving = sum(Fdiff(relFreq,4) < squeeze(pvalues(relFreq,pind(1),4))); 
    
    sending  = sum(Fdiff(relFreq,4) > squeeze(pvalues(relFreq,pind(2),4))); 
    
    if receiving > sending
        transmitter(ii) = 1;
    elseif receiving < sending
        transmitter(ii) = 2;
    end
end

allsenders = find(transmitter == 2);
allreceivers = find(transmitter == 1);


%% for Steve
for ter = 1 : 2
tarease =unique(obj.data.hist(idepairs(transmitter == ter),:),'rows');

[areas(tarease(:,1))' areas(tarease(:,2))'];


allpairs = unique(obj.data.hist(idepairs,:),'rows');
[areas(allpairs(:,1))' areas(allpairs(:,2))'];

noselparis = setdiff(allpairs,tarease,'rows');

[areas(noselparis(:,1))' areas(noselparis(:,2))']
end
% get teh idepairs for each area
nAreas = zeros(10,1);
cd /Volumes/raid/data/monkey/

load sigCoh&MIPerLoc.mat
DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCoh));
uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = ind(DEsigCoh(duplicates(1)));
end

for ar = [1:5] ; nAreas(ar) = length(find(obj.data.hist(uniquePairs,2) == ar)); end
c = 6; for ar = [8 10:13] ; nAreas(c) = length(find(obj.data.hist(uniquePairs,1) == ar)); c= c + 1; end


% [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,idepairs(allsenders));


[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,idepairs(allsenders));
uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = idepairs(allsenders(duplicates(1)));
end

[n,xout] = hist(obj.data.hist(uniquePairs,:),[1:18]);
n = sum(n,2);
send(6:10)=n([8 10:13]);
receive(1:5) = n(1:5);
% [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,idepairs(allsenders));

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,idepairs(allreceivers));
uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = idepairs(allreceivers(duplicates(1)));
end

[n,xout] = hist(obj.data.hist(uniquePairs,:),[1:18]);
n = sum(n,2);
send(1:5)=n(1:5);
receive(6:10) = n([8 10:13]);

subplot(3,1,2)
bar([1:10],100*send ./ nAreas)
PFC = mean(100*send(1:5) ./ nAreas(1:5))
PPC = mean(100*send(6:10) ./ nAreas(6:10))
100*send ./ nAreas

title('senders')

subplot(3,1,3)
bar([1:10],100*receive ./ nAreas)
title('receivers')
ylim([0 30])
100*receive ./ nAreas
            %% old code
% areasN = unique(obj.data.hist(idepairs,:));
% 
% nareas = [];
% clear nareasN
% for a = 1 : length(areasN)
% %         [r,ind] = get(obj,'Number','snr',99,'type',{type selec},'hist',[areasN(a) nan]);
%     [r,ind] = get(obj,'Number','snr',99,'hist',[areasN(a) nan]);
%     nareasN(a) = r;
%     if r >= 10
%         nareas = [nareas; areasN(a)];
%     end
% end
% figure
% dirL = {'-' '--'};
% relFreq = [12:22];
% clear sigdiff receiver sender
% for dir = [-1 1]
%     for a = 1 : size(nareas,1)
%         %         [r,ind] = get(obj,'Number','snr',99,'type',{type selec},'hist',[nareas(a) nan]);
%         indt = [];
%         for iarea = 1 : size(nareas,2)
%             [r,ind] = get(obj,'Number','snr',99,'hist',[nareas(a,iarea) nan]);
%             indt = union(ind,indt);
%         end
%         ind = intersect(indt,idepairs);
%         r = length(ind);
%         clear sigdiff
%         
%       
%         % testfig = figure;
%         for i = 1 : r
%             
%             cd(obj.data.setNames{ind(i)});
%             cmb = obj.data.Index(ind(i),1);
%             load GCcohInter.mat
%             if Day.session(1).rule == 1
%                 s = 1;
%             else
%                 s = 2;
%             end
%             
%             
%             Fx2y = squeeze(Day.session(s).Fx2y(:,cmb,:));
%             Fy2x = squeeze(Day.session(s).Fy2x(:,cmb,:));
%             load GCgeneralSur.mat
%             Fx2ysur = squeeze(Day.session(s).Fx2y(:,3,:));
%             Fy2xsur = squeeze(Day.session(s).Fx2y(:,3,:));
%             siggc = ((Fx2y >= Fx2ysur) + (Fy2x >= Fy2xsur) ~= 0);
%             
%             cd GCdirStat
%             pairg = obj.data.Index(idepairs(i),10:11);
%             pwd;
%             file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),1);
%             load(file,'Fdiff')
%             Fdiff = Fdiff .* siggc;
%             sfile = sprintf('GCintergeneralRule%d.mat',1);
%             load(sfile,'pvalues','levels')
%             %     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
%             
%             pind = find(plevel == levels);
%             % Fdiff = Fx2y - Fy2x;
%             
%             
%             
% %             if ((nareas(a) <= 7 || nareas(a) == 17) && dir == -1) || (~(nareas(a) <= 7 || nareas(a) == 17) && dir == 1)
%                  if (a ==1 && dir == -1) || (a ~=1 && dir == 1)
%                 sigdiff(i,:,:) = (Fdiff <= squeeze(pvalues(:,pind(1),:))) ; % negative means PFC dominate
%             else
%                 sigdiff(i,:,:) = (Fdiff >= squeeze(pvalues(:,pind(2),:))) ; % positive means PPC dominates
%             end
%         end
%         %     alldiff(a,:,:) = 100*squeeze(sum(sigdiff,1))/r;
%          perDir = 100*squeeze(sum(sigdiff,1))/r;
%         
%         if dir == 1
%             receiver(a) = mean(perDir(relFreq));
%         else
%             sender(a) = mean(perDir(relFreq));
%         end
%         
%     end
% end