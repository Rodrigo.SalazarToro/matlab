function supplFig10

cd /Volumes/raid/data/monkey/


obj = loadObject('cohInterIDEandLongSorted.mat');


[r,ind] = get(obj,'Number','type',{'betapos34' [1 3]},'hist',[13 13]);
cc = 1;

for ii = 1 : r
    
    cd(obj.data.setNames{ind(ii)})
    
   
    c = 1;
    for item = obj.data.IDEObjSeq(ii,4,:)
         cd grams
        load(sprintf('cohgramg%04.0fg%04.0fRule1IDEObj%d',obj.data.Index(ii,10),obj.data.Index(ii,11),item),'C','f','t')
       
        Ccomb(c,cc,:,:) = C{1}(1:176,:);
        
        for ch = 1 : 2
        cd(obj.data.setNames{ind(ii)})
        if ii > 4 % betty
           cd session01
            cd(sprintf('group%04.0f/cluster01m',obj.data.Index(ii,9+ch)))
            load nineStimPSTH1.mat
            
        else
            
        end
        
        end
        c = c + 1;
    end
    cc = cc + 1;
    
end