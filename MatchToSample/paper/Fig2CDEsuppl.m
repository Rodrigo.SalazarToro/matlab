%% only for teh whol edelay

clear all
% obj = loadObject('cohInterIDEandLongSorted.mat');

% [r,ind] = get(obj,'Number','snr',99);


locs = {'max' 'inter' 'min'};


nsigCoh = zeros(1,1);
nsigCohMI = zeros(1,1);
sigCoh = cell(1,3);
sigCohMI = cell(1,3);


    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramIdePerLocRule1prctile98DElocSel%dBias.mat',lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{lc} =  thepairs;
        
    end
    nsigCohMI = length(unique(tMI));
    nsigCoh = length(unique([sigCoh{1}; sigCoh{2}; sigCoh{3}]));


DEsigCoh = unique([sigCoh{1}; sigCoh{2}; sigCoh{3}]);
DEsigCohMI = unique([sigCohMI{1}; sigCohMI{2}; sigCohMI{3}]);
DEsigMI{1} = [sigCohMI{1}' sigCohMI{2}' sigCohMI{3}'];
DEsigMI{2} = [sigCohMI{1}' sigCohMI{2}' sigCohMI{3}'];
thelocs{1} = [zeros(1,length(sigCohMI{1})) + 1 zeros(1,length(sigCohMI{2})) + 2 zeros(1,length(sigCohMI{3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{1})) + 1 zeros(1,length(sigCohMI{2})) + 2 zeros(1,length(sigCohMI{3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramIdePerLocRule1prctile98DElocSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))


smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramIdePerLocRule1prctile98DElocSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    for stim = 1 : 3
    coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
    VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
end
% time = time - 0.005;
biasmugram = smugram - ExpVgram; 


figure; set(gcf,'Name','Object selectivity whole delay')
subplot(4,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])


subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the object at any location during the delay (10-42 Hz); n=484')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')


%% for location
%% only for teh whol edelay

clear all
% obj = loadObject('cohInterIDEandLongSorted.mat');

% [r,ind] = get(obj,'Number','snr',99);


locs = {'max' 'inter' 'min'};


nsigCoh = zeros(1,1);
nsigCohMI = zeros(1,1);
sigCoh = cell(1,3);
sigCohMI = cell(1,3);


    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramLocPerIdeRule1prctile98DEideSel%dBias.mat',lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{lc} =  thepairs;
        
    end
    nsigCohMI = length(unique(tMI));
    nsigCoh = length(unique([sigCoh{1}; sigCoh{2}; sigCoh{3}]));


DEsigCoh = unique([sigCoh{1}; sigCoh{2}; sigCoh{3}]);
DEsigCohMI = unique([sigCohMI{1}; sigCohMI{2}; sigCohMI{3}]);
DEsigMI{1} = [sigCohMI{1}' sigCohMI{2}' sigCohMI{3}'];
DEsigMI{2} = [sigCohMI{1}' sigCohMI{2}' sigCohMI{3}'];
thelocs{1} = [zeros(1,length(sigCohMI{1})) + 1 zeros(1,length(sigCohMI{2})) + 2 zeros(1,length(sigCohMI{3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{1})) + 1 zeros(1,length(sigCohMI{2})) + 2 zeros(1,length(sigCohMI{3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramLocPerIdeRule1prctile98DEideSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))


smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramLocPerIdeRule1prctile98DEideSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    for stim = 1 : 3
    coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
    VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
end
% time = time - 0.005;
biasmugram = smugram - ExpVgram; 


figure; set(gcf,'Name','Location selectivity whole delay')
subplot(4,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])


subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the lcoation with any object during the delay (10-42 Hz); n=432')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')


%% for DE1 and DE2 only location
clear all
cd /Volumes/raid/data/monkey
% obj = loadObject('cohInterIDEandLongSorted.mat');
% 
% [r,ind] = get(obj,'Number','snr',99);

epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramLocPerIdeRule1prctile98%sideSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end

DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);
DEsigCohMI = unique([sigCohMI{3,1}; sigCohMI{3,2}; sigCohMI{3,3}; sigCohMI{4,1}; sigCohMI{4,2}; sigCohMI{4,3}]);
DEsigMI{1} = [sigCohMI{3,1}' sigCohMI{3,2}' sigCohMI{3,3}'];
DEsigMI{2} = [sigCohMI{4,1}' sigCohMI{4,2}' sigCohMI{4,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{3,1})) + 1 zeros(1,length(sigCohMI{3,2})) + 2 zeros(1,length(sigCohMI{3,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{4,1}),1) + 1 zeros(1,length(sigCohMI{4,2})) + 2 zeros(1,length(sigCohMI{4,3})) + 3]; 

[bothDE,ia,ib] = intersect(DEsigMI{1},DEsigMI{2});

selpairLocs = [thelocs{1}(ia); bothDE ];



 load('migramLocPerIdeRule1prctile98DEideSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));
display(sprintf('only %d pairs selected',length(m)))

uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))
smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramLocPerIdeRule1prctile98DEideSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    
    for stim = 1 : 3
        coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
        VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
    
end

biasmugram = smugram - ExpVgram; 
figure; set(gcf,'Name','location selectivity')
subplot(4,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])


subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the object at any location during both DE1 and DE2 (10-42 Hz); n=78')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')
%% object selectivity for the sample
clear all
% obj = loadObject('cohInterIDEandLongSorted.mat');
% 
% [r,ind] = get(obj,'Number','snr',99);

epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramIdePerLocRule1prctile98%slocSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end

DEsigCoh = unique([sigCoh{2,1}; sigCoh{2,2}; sigCoh{2,3}]);
DEsigCohMI = unique([sigCohMI{2,1}; sigCohMI{2,2}; sigCohMI{2,3}]);
DEsigMI{1} = [sigCohMI{2,1}' sigCohMI{2,2}' sigCohMI{2,3}'];
DEsigMI{2} = [sigCohMI{2,1}' sigCohMI{2,2}' sigCohMI{2,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{2,1})) + 1 zeros(1,length(sigCohMI{2,2})) + 2 zeros(1,length(sigCohMI{2,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{2,1})) + 1 zeros(1,length(sigCohMI{2,2})) + 2 zeros(1,length(sigCohMI{2,3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramIdePerLocRule1prctile98SAlocSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))

smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramIdePerLocRule1prctile98SAlocSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    for stim = 1 : 3
    coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
    VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
end
% time = time - 0.005;
biasmugram = smugram - ExpVgram; 
figure; set(gcf,'Name','Object selectivity during the sample')
subplot(4,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])


subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the object at any location during the delay (10-42 Hz); n=207')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')

