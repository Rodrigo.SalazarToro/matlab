clear all
cd /Volumes/raid/data/monkey

obj = loadObject('cohInterIDEandLongSorted.mat');
Flim = [12 22];
[r,ind] = get(obj,'Number','snr',99);
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind);

[idemugram,time,f,ntrials,sig,idePairs,presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile','DelaylocSelmax','getPrefStim');

[locmugram,time,f,ntrials,sig,locPairs,presample,msur] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile','DelayideSelmax','getPrefStim');

upairs = unique(pairNumberInd);
clear nidemugram nlocmugram
for pp = 1 : length(upairs)
    duplicates = find(pairNumberInd == upairs(pp));
    nidemugram(pp,:,:) = mean(idemugram(duplicates,:,:),1);
    nlocmugram(pp,:,:) = mean(locmugram(duplicates,:,:),1);
end

medidemugram = squeeze(nanmedian(nidemugram,1));
medidemugram = interp2(f,time,medidemugram,[0 :50],[0.11:.02:1.8]');

medlocmugram = squeeze(nanmedian(nlocmugram,1));
medlocmugram = interp2(f,time,medlocmugram,[0 :50],[0.11:.02:1.8]');
figure

subplot(5,1,1)
imagesc([0.11:.02:1.8],[0 :50],medidemugram',[0.8 1.3])
colorbar
set(gca,'TickDir','out')
title(['identity of the sample object; n= ' num2str(size(nidemugram,1))])
ylabel('Frequency (Hz)')
subplot(5,1,2)
imagesc([0.11:.02:1.8],[0 :50],medlocmugram',[0.8 1.3])
colorbar
set(gca,'TickDir','out')
title(['Location of the sample object; n= ' num2str(size(nidemugram,1))]) 
ylabel('Frequency (Hz)')

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(idePairs));

uidepairs = unique(pairNumberInd);

relf = find(f> Flim(1) & f< Flim(2));
clear miide miloc cohStim phaseStim
for pp = 1 : length(uidepairs)
    duplicates = find(pairNumberInd == uidepairs(pp));
    miide(pp,:) = nanmean(nanmean(idemugram(idePairs(duplicates),:,relf),3),1);
   
    
    for stim = 1 : 3
        cohStim(stim,pp,:) = nanmean(nanmean(prefide(stim,idePairs(duplicates),:,relf),4),2);
        phaseStim(stim,pp,:) = circ_mean(circ_mean(prefidePhaseSTD(stim,idePairs(duplicates),:,relf),[],4),[],2);
    end
    
end
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(locPairs));
uilocpairs = unique(pairNumberInd);
for pp = 1 : length(uilocpairs)

 duplicates = find(pairNumberInd == uilocpairs(pp));
      miloc(pp,:) = nanmean(nanmean(locmugram(locPairs(duplicates),:,relf),3),1);
end
nsessions = length(unique(obj.data.setNames(ind(idePairs))));

subplot(5,1,3)

me = mean(miide,1);
ste = std(miide) / sqrt(size(miide,1));
ste2 = std(miide) / sqrt(nsessions);
plot(time,me)
hold on
plot(time,me + ste,'--')
plot(time,me - ste,'--')
plot(time,me + ste2,'--')
plot(time,me - ste2,'--')

me = mean(miloc,1);
ste = std(miloc) / sqrt(size(miloc,1));
ste2 = std(miloc) / sqrt(nsessions);
plot(time,me,'r')
hold on
plot(time,me + ste,'r--')
plot(time,me - ste,'r--')
plot(time,me + ste2,'r--')
plot(time,me - ste2,'r--')
xlim([0.1 1.8])
title(['ide; n= ' num2str(size(miide,1)) ' loc; n= ' num2str(size(miloc,1))])
ylabel('norm. MI')
stimlabels = {'r' 'b' 'k'};
for stim = 1 : 3
    
    me = squeeze(mean(cohStim(stim,:,:),2));
    ste = squeeze(std(cohStim(stim,:,:),[],2)) / sqrt(size(cohStim,2));
    ste2 = squeeze(std(cohStim(stim,:,:),[],2)) / sqrt(nsessions);
    subplot(5,1,4)
    plot(time,me,stimlabels{stim})
    hold on
    plot(time,me + ste,[stimlabels{stim} '--'])
    plot(time,me - ste,[stimlabels{stim} '--'])
    plot(time,me + ste2,[stimlabels{stim} '--'])
    plot(time,me - ste2,[stimlabels{stim} '--'])
    xlim([0.1 1.8])
    title('coherence for the 3 stimuli')
    subplot(5,1,5)
    me = squeeze(circ_mean(phaseStim(stim,:,:),[],2));
    ste = squeeze(circ_std(phaseStim(stim,:,:),[],[],2)) / sqrt(size(phaseStim,2));
    ste2 = squeeze(circ_std(phaseStim(stim,:,:),[],[],2)) / sqrt(nsessions);
    
    plot(time,me,stimlabels{stim})
    hold on
    plot(time,me + ste,[stimlabels{stim} '--'])
    plot(time,me - ste,[stimlabels{stim} '--'])
    plot(time,me + ste2,[stimlabels{stim} '--'])
    plot(time,me - ste2,[stimlabels{stim} '--'])
    xlim([0.1 1.8])
    title('STD of the phase for the 3 stimuli')
    xlabel('time (sec)')
end

mixed = intersect(uidepairs, uilocpairs);
% idePairs = setdiff(idePairs,mixed);
% locPairs = setdiff(locPairs,mixed);
cellnames = {'mixed' 'idePairs' 'locPairs'};



delay = find(time > 1.1 & time <1.7);
% prefidePhase = mod(prefidePhase+2*pi,2*pi);
% preflocPhase = mod(preflocPhase+2*pi,2*pi);
%% coherence and phse for 3 obj
figure
stimlab = {'r' 'b' 'k'};
for stim = 1 : 3
    subplot(5,1,1)
    me = squeeze(mean(mean(prefide(stim,idePairs,:,relf),4),2));
    ste = squeeze(std(mean(prefide(stim,idePairs,:,relf),4),[],2)) / sqrt(npairs);
     ste2 = squeeze(std(mean(prefide(stim,idePairs,:,relf),4),[],2)) / sqrt(nsessions);
    plot(time,me,stimlab{stim})
    hold on
    plot(time,me + ste,sprintf('%s--',stimlab{stim}))
    plot(time,me - ste,sprintf('%s--',stimlab{stim}))
    plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
    plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
    set(gca,'TickDir','out')
    ylim([0.13 0.22])
    xlim([0.11 1.8])
    title('mean coherence for the 3 ides')
    xlabel('Time [sec.]')
    ylabel('Coherency')
    
    
    subplot(5,1,2)
    me = squeeze(circ_mean(circ_mean(prefidePhaseSTD(stim,idePairs,:,relf),[],4),[],2));
    ste = squeeze(circ_std(circ_mean(prefidePhaseSTD(stim,idePairs,:,relf),[],4),[],[],2)) / sqrt(npairs);
    ste2 = squeeze(circ_std(circ_mean(prefidePhaseSTD(stim,idePairs,:,relf),[],4),[],[],2)) / sqrt(nsessions);
    plot(time,me,sprintf('%s',stimlab{stim}))
    hold on
    plot(time,me + ste,sprintf('%s--',stimlab{stim}))
    plot(time,me - ste,sprintf('%s--',stimlab{stim}))
    plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
    plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
    set(gca,'TickDir','out')
%     ylim([0.13 0.22])
    xlim([0.11 1.8])
    title('mean phase variance for the 3 ides')
    xlabel('Time [sec.]')
    ylabel('Phase variance')
    
   
    
    for area = 1 : 2
        subplot(5,1,2+area)
        me = squeeze(mean(mean(prefideS{area}(stim,idePairs,:,relf),4),2));
        ste = squeeze(std(mean(prefideS{area}(stim,idePairs,:,relf),4),[],2)) / sqrt(npairs);
        ste2 = squeeze(std(mean(prefideS{area}(stim,idePairs,:,relf),4),[],2)) / sqrt(nsessions);
        plot(time,me,sprintf('%s',stimlab{stim}))
        hold on
        plot(time,me + ste,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste,sprintf('%s--',stimlab{stim}))
        plot(time,me + ste2,sprintf('%s--',stimlab{stim}))
        plot(time,me - ste2,sprintf('%s--',stimlab{stim}))
    end
    set(gca,'TickDir','out')
%     ylim([0.13 0.22])
    xlim([0.11 1.8])
    title('mean power for the 3 ides')
    xlabel('Time [sec.]')
    ylabel('power')
end

subplot(5,1,5)%      set(gcf,'Name',cellnames{ct})

% plot(time,prctile(squeeze(mean(idemugram(idePairs,:,relf),3)),[25 50 75],1))
me = mean(squeeze(mean(idemugram(idePairs,:,relf),3)),1);
ste = std(squeeze(mean(idemugram(idePairs,:,relf),3)),[],1) / sqrt(length(idePairs));
ste2 = std(squeeze(mean(idemugram(idePairs,:,relf),3)),[],1) / sqrt(nsessions);
plot(time,me)
hold on
plot(time,me+ ste,'--')
plot(time,me- ste,'--')
plot(time,me+ ste2,'--')
plot(time,me- ste2,'--')

me = mean(squeeze(mean(locmugram(locPairs,:,relf),3)),1);
ste = std(squeeze(mean(locmugram(locPairs,:,relf),3)),[],1) / sqrt(length(locPairs));
ste2 = std(squeeze(mean(locmugram(locPairs,:,relf),3)),[],1) / sqrt(nsessions);
plot(time,me,'r')
hold on
plot(time,me+ ste,'r--')
plot(time,me- ste,'r--')
plot(time,me+ ste2,'r--')
plot(time,me- ste2,'r--')

ylabel('MI [bits]')
xlabel('Time [sec]')
ylim([0.3 1.3])
set(gca,'TickDir','out')
xlim([0.11 1.8])

    %%
figure
medLab = {'r' 'b' 'k'};
titless = {'unPref. obj' 'inter. obj.' 'pref. obj'};
for stim = 1 : 3
    
    subplot(3,3,stim)
    
    mePhi = squeeze(prefidePhase(stim,idePairs,:,relf));
    mePhi = circ_mean(mePhi,ones(size(mePhi)),3);
    [n,xout] = hist(mePhi,100);
    imagesc(time,xout,n);
    caxis([0 60])
    ylim([-pi pi])
    
    subplot(3,3,7)
    plot(time,rad2deg(circ_mean(mePhi)),medLab{stim})
    hold on
    xlim([time(1) 1.8])
    legend(titless)
    set(gca,'TickDir','out')
    
    title('mean')
     subplot(3,3,8)
    plot(time,rad2deg(circ_std(mePhi)),medLab{stim})
    hold on
    xlim([time(1) 1.8])
    legend(titless)
    title('std')
    set(gca,'TickDir','out')
    subplot(3,3,stim+3)
    
    mePhi = mod(mePhi+2*pi,2*pi);
    
    [nm,xout] = hist(mePhi,100);
    imagesc(time,[pi/2:2*pi/100:3*pi/2],nm);
    
    ylim([2.2 4.3])
    caxis([0 15])
    set(gca,'TickDir','out')
end


xlabel('time (s)')
ylabel('Angle (radians)')





%%
for ct = 1 : 3
    figure
    cells = eval(cellnames{ct});
    set(gcf,'Name',cellnames{ct})
    
    subplot(3,2,1)
    me = squeeze(mean(mean(prefide(cells,:,relf),3),1));
    ste = std(mean(prefide(cells,:,relf),3),[],1) / sqrt(length(cells));
    
    plot(time,me)
    hold on
    plot(time,me + ste)
    plot(time,me - ste)
    set(gca,'TickDir','out')
    ylim([0.14 0.22])
    xlim([0.11 1.8])
    title('mean coherence for pref. ide.')
    xlabel('Time [sec.]')
    ylabel('Coherency')
    
    subplot(3,2,2)
    me = squeeze(mean(mean(prefloc(cells,:,relf),3),1));
    ste = std(mean(prefloc(locPairs,:,relf),3),[],1) / sqrt(length(cells));
    
    set(gca,'TickDir','out')
    plot(time,me)
    hold on
    plot(time,me + ste)
    plot(time,me - ste)
    ylim([0.14 0.22])
    xlim([0.11 1.8])
    
    title('mean coherence for pref. loc.')
    xlabel('Time [sec.]')
    ylabel('Coherency')
    
    
    
    subplot(3,2,4)
    hist(diffide(cells),[0:0.01:0.15])
    title('difference of coherences bewtween pref. and not pref. ide')
    set(gca,'TickDir','out')
    xlabel('coherence')
    ylabel('Number of pair')
    xlim([-0.005 0.155])
    subplot(3,3,4)
    hist(diffloc(cells),[0:0.01:0.15])
    title('difference of coherences bewtween pref. and not pref. loc')
    xlabel('coherence')
    ylabel('Number of pair')
    set(gca,'TickDir','out')
    xlim([-0.005 0.155])
    
    
    subplot(3,2,5)
    me = shiftdim(prefidePhase(cells,:,relf),1);
    me = reshape(me,size(me,1),size(me,2)*size(me,3));
    clear histphase
    for ti = 1 : size(me,1)
        [histphase(:,ti),xout] = hist(me(ti,:),[0:pi/36:2*pi]);
    end
    ntime = [0.11:0.02:1.8];
    nf = [0:pi/72:2*pi];
    % figure
    % subplot(2,1,1)
    histphasei = interp2(time,xout',histphase,ntime,nf');
    imagesc(ntime,nf,histphasei)
    xlim([0.11 1.8])
    set(gca,'TickDir','out')
    ylim([pi-pi/2 pi+pi/3])
    caxis([0 60])
    colorbar
    % subplot(2,1,2)
    clear histphase
    me = mod(me,pi) - pi*(me>pi);
    for ti = 1 : size(me,1)
        [histphase(:,ti),xout] = hist(me(ti,:),[-pi:pi/36:pi]);
    end
    nf = [-pi:pi/72:pi];
    histphasei = interp2(time,xout',histphase,ntime,nf');
    imagesc(ntime,nf,histphasei)
    xlim([0.11 1.8])
    set(gca,'TickDir','out')
    ylim([-pi/3 pi/2])
    caxis([0 250])
    colorbar
    title('mean phase for pref. ide.')
    xlabel('Time [sec.]')
    ylabel('Phase [rad.]')
    %
    % figure
    
    me = prefidePhase(cells,delay,relf);
    me = reshape(me,1,size(me,1)*size(me,2)*size(me,3));
    rose(me,24)
    title('delay phases distribution for pref. ide')
    
    subplot(3,2,6)
    me = shiftdim(preflocPhase(cells,:,relf),1);
    me = reshape(me,size(me,1),size(me,2)*size(me,3));
    clear histphase
    for ti = 1 : size(me,1)
        [histphase(:,ti),xout] = hist(me(ti,:),[0:pi/36:2*pi]);
    end
    % figure
    imagesc(time,xout,histphase)
    xlim([0.11 1.8])
    set(gca,'TickDir','out')
    ylim([0 2*pi])
    
    title('mean phase for pref. ide.')
    xlabel('Time [sec.]')
    ylabel('Phase [rad.]')
    
    me = preflocPhase(cells,delay,relf);
    me = reshape(me,1,size(me,1)*size(me,2)*size(me,3));
    rose(me,24)
    title('delay phases distribution for pref. loc')
end 
    %% mutual info
    
    
    
    % stats
    
    popIde = squeeze(mean(idemugram(idePairs,:,relf),3));
    popLoc = squeeze(mean(locmugram(locPairs,:,relf),3));
    
    for ti = 1 : 36
    [h(ti),p(ti)] = kstest2(popIde(:,ti),popLoc(:,ti),0.05/36);
    end
    % for all ide and loc min(p) = 0.002 or 0.07 with Bonneforri
    % for pure ide and loc min(p) = 0.0078 or 0.28 with Bonneforri at time
    % = 0.7 sec
% end



% figure
% subplot(2,1,1)
% boxplot(squeeze(mean(idemugram(idePairs,:,relf),3)))
% ylabel('MI [bits]')
% xlabel('Time [sec]')
% title('ide tuned')
% subplot(2,1,2)
% boxplot(squeeze(mean(locmugram(locPairs,:,relf),3)))
% ylabel('MI [bits]')
% xlabel('Time [sec]')
% title('loc tuned')
%
% legend('ide tuned mean','loc tuned')
% length(intersect(tuned, betapos))
%% histology distr
areas = {'6DR'; '8AD' ;'8B'; 'dPFC' ;'vPFC'; 'PS'; 'AS'; 'PEC'; 'PGM'; 'PE'; 'PG'; 'MIP'; 'LIP'; 'PEcg'; 'IPS' ;'WM'; '9L'};
pairt = unique(obj.data.hist(ind,:),'rows');
for pp = 1 : size(pairt,1); if pairt(pp,1) ~= 99 & pairt(pp,2) ~= 99; pareas{pp} = sprintf('%s %s',areas{pairt(pp,1)},areas{pairt(pp,2)}); else pareas{pp} = '??';end;end
idePairsObj = ind(idePairs);
locPairsObj = ind(locPairs);
mixedPairsObj = ind(mixed);
% save('tunedPairs.mat','idePairs','idePairsObj','locPairs','locPairsObj')
for pp = 1 : size(pairt,1)
    
    tunedpairs(pp,1) = sum(ismember(obj.data.hist(idePairsObj,:),pairt(pp,:),'rows') == 1);
    tunedpairs(pp,2) = sum(ismember(obj.data.hist(locPairsObj,:),pairt(pp,:),'rows') == 1);
     tunedpairs(pp,3) = sum(ismember(obj.data.hist(mixedPairsObj,:),pairt(pp,:),'rows') == 1);
    tunedpairs(pp,4) = sum(ismember(obj.data.hist(ind,:),pairt(pp,:),'rows') == 1);
end

figure
subplot(2,1,1)
bar(tunedpairs)
legend('ide pairs','loc pairs','mixed','all')
set(gca,'XTick',[1:length(pareas)]);
set(gca,'XTickLabel',pareas)
subplot(2,1,2)
ptpairs = [100*tunedpairs(:,1) ./ tunedpairs(:,4) 100*tunedpairs(:,2) ./ tunedpairs(:,4) 100*tunedpairs(:,3) ./ tunedpairs(:,4)];
bar(ptpairs)
set(gca,'XTick',[1:length(pareas)]);
set(gca,'XTickLabel',pareas)
legend('ide pairs','loc pairs','mixed','all')

ylabel('Number of pairs')
xlim([0 length(pareas)+1])




%%
% cd /Volumes/raid/data/monkey/betty
% obj = loadObject('cohInterLongSorted.mat');
%
% [r,ind] = get(obj,'Number');
% [mugram,time,f,ntrials] = mkAveMugram(obj,ind,'type',types{3},'plot','redo','save');

%% only for betapos34
[r,ind] = get(obj,'Number','type',{'betapos34' [1 3]},'snr',99);


[mugram,time,f,ntrials,sig,idePairs,presample] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','delayRange',[1.3 1.8],'Interpol','addNameFile','betapos34','biasCorr','50thSur');
for sb = 1 : 2; subplot(2,1,sb); xlim([0.11 1.8]); end; caxis([0 0.2])
[mugram,time,f,ntrials,sig,LocPairs,presample] = mkAveMugram(obj,ind,'type','LocPerIde','plot','delayRange',[1.3 1.8],'Interpol','addNameFile','betapos34','biasCorr','50thSur');
for sb = 1 : 2; subplot(2,1,sb); xlim([0.11 1.8]); end; caxis([0 0.2])
[r,betapos] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});
tuned = union(ind(idePairs),ind(LocPairs));
length(intersect(tuned, betapos))


for sb = 1 : 2; subplot(2,1,sb); xlim([0.11 1.8]); end
subplot(2,1,2)
caxis([-0.05 0.12])

%% ex

cd /Volumes/raid/data/monkey/clark/060502/grams

load migramg0001g0014Rule1IdePerLoc3.mat
sur = load('migramSurGenInterRule1IdePerLoc3.mat');
for sb =1 : 3;nC{sb} = interp2(f,time,C{sb},[0 :50],[0:0.02:2]');end
figure; for sb = 1 : 3; subplot(5,1,sb); imagesc([0:0.02:2],[0:50],nC{sb}',[0 0.5]); end

presample = time <= 0.405;
texpv =  getExpValue(sur.mutualgram,f);
alpha = squeeze(sum(sum(mutualgram(presample,:),1),2)) ./ squeeze(sum(sum(texpv(presample,:),1),2))';

thesur = squeeze(prctile(alpha*sur.mutualgram,98,1));

sigMI = mutualgram(1:end-1,:) > thesur;
mutualgram = mutualgram(1:43,:) - alpha * texpv;
% mutualgram = mutualgram .* sigMI;
sigMI = interp2(f,time(1:43),sigMI,[0 :50],[0:0.02:2]');
mutualgram = interp2(f,time(1:size(mutualgram,1)),mutualgram,[0 :50],[0:0.02:2]');
subplot(5,1,4)
imagesc([0:0.02:2],[0:50],mutualgram');
subplot(5,1,5)

contour([0:0.02:2],[0:50],sigMI')
for sb =1 : 5;subplot(5,1,sb); xlim([0.11 2]); set(gca,'TickDir','out');colorbar;end
xlabel('Time [sec.]')
ylabel('Frequency [Hz]')
