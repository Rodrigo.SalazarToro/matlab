cd clark/060428/session02/
[data,num_channels,sampling_rate,datatype,points]=nptReadStreamerFile('clark06042802.0579');
[lfps , resample_rate]=nptLowPassFilter(data([8 14],:),30000,2000,2,999);% group 3 16 or n=125 in the obj
[eyes , resample_rate]=nptLowPassFilter(data([1 4 5],:),30000,2000,0.001,999);
figure;

for ch = 1 : 2; plot(lfps(ch,:) + 1000*(ch-1)); hold on; end
for ch = 1 : 3; plot(eyes(ch,:) + 1000*(ch+1)); hold on; end

cd /Volumes/raid/data/monkey

obj = loadObject('cohInterIDEandLongSorted.mat');
InspectGUI(obj,'plotType','TimeFreq','gram','coh','frange',[0 50])

cd clark/060428/grams

load cohgramg0003g0016Rule1.mat

theC = interp2(f,t{1},C{1},[0 :50],[0:0.02:2]');
thephi = interp2(f,t{1},phi{1},[0 :50],[0:0.02:2]');
figure;
subplot(2,1,1)
imagesc([0:0.02:2],[0 :50],theC')
colorbar
xlim([0.11 2])
caxis([0 0.2])
xlabel('Time [sec]')
set(gca,'TickDir','out')

subplot(2,1,2)
imagesc([0:0.02:2],[0 :50],thephi')
colorbar
xlim([0.11 2])
caxis([-pi pi])
xlabel('Time [sec]')
set(gca,'TickDir','out')

%%
load migramg0003g0016Rule1IdePerLoc2.mat; 
for stim = 1 : 3; 
    subplot(3,1,stim); 
    theC = interp2(f,time,C{stim},[0 :50],[0:0.02:2.2]');
    imagesc([0:0.02:2.2],[0 :50],theC'); 
    colorbar;
    xlim([0.11 2.1])
    caxis([0 0.3])
    set(gca,'TickDir','out')
end




%%
cd 060428/session02/group0016/cluster01m/

load nineStimPSTH1.mat

mfr = cell2mat(A);
mfr = mfr(:,1:3001);
binSize = 100;
Mbins = 20;
time = [-450:100:1450];
for bi = 1 : Mbins
    Mspikefr(bi) = sum(sum(mfr(:,(bi-1)*binSize +1 : bi*binSize ),2),1)*1000/(binSize*size(mfr,1));
    
end % rebin the 1mse matraix
figure; plot(time,Mspikefr)

%% last exdample

cd /Volumes/raid/data/monkey/clark/060427/grams
a=load('cohgramg0002g0015Rule1.mat');

cd /Volumes/raid/data/monkey/clark/060427/session03

cd /Volumes/raid/data/monkey/clark/060428/grams
a=load('cohgramg0002g0009Rule1.mat');

cd /Volumes/raid/data/monkey/clark/060428/session03

mts = mtstrial('auto');
[C,phi,S12,S1,S2,t,f] = MTScohgram(mts,a.trials,[1 7],'mwind',[0.4 0.05]); % [1 7]
theC = interp2(f,t,squeeze(C),[0 :50],[0:0.02:2]');
thephi = interp2(f,t,squeeze(phi),[0 :50],[0:0.02:2]');

figure; subplot(3,1,1);
imagesc([0:0.02:2],[0 :50],theC',[0 0.35]);
colorbar
xlim([0.21 2]) 
set(gca,'TickDir','out');

subplot(3,1,2);
imagesc([0:0.02:2],[0 :50],thephi')
colorbar
xlim([0.21 2]) 
subplot(3,1,3)
plot([0:0.02:2],rad2deg(mean(wrapTo180(thephi(:,15:25)),2)))
xlim([0.21 2]) 
set(gca,'TickDir','out');