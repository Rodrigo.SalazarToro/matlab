clear all
cd /Volumes/raid/data/monkey
load('migramIdePerLocRule1prctile98DElocSel1Bias.mat')

obj = loadObject('cohInterIDEandLongSorted.mat');
 
[r,ind] = get(obj,'Number','snr',99);

% sig coherence

cd /Volumes/raid/data/monkey
load sigCoh&MIPerLoc.mat
DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCoh));

uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = ind(DEsigCoh(duplicates(1)));
end

pairtypes = unique(obj.data.hist(uniquePairs,:),'rows');

histo = zeros(3,size(pairtypes,1));
for ii = 1 : size(pairtypes,1)
    count = ismember(obj.data.hist(uniquePairs,:),pairtypes(ii,:),'rows');
    histo(3,ii) = sum(count);
    
end

% object selective
cd /Volumes/raid/data/monkey/
load('IdePairsUnbiased.mat')
idepairs = ind(uselpairLocs(2,:));
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,idepairs);

uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = idepairs(duplicates(1));
end


for ii = 1 : size(pairtypes,1)
    count = ismember(obj.data.hist(uniquePairs,:),pairtypes(ii,:),'rows');
    histo(1,ii) = sum(count);
    
end
% location selective
cd /Volumes/raid/data/monkey/
load('LocPairsUnbiased.mat')
locpairs = ind(uselpairLocs(2,:));
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,locpairs);

uniquePairs = zeros(max(pairNumberInd),1);
for ii = 1 : max(pairNumberInd);
    duplicates = find(pairNumberInd == ii);
    uniquePairs(ii) = locpairs(duplicates(1));
end




for ii = 1 : size(pairtypes,1)
    count = ismember(obj.data.hist(uniquePairs,:),pairtypes(ii,:),'rows');
    histo(2,ii) = sum(count);
    
end


areasName = {'6DR' '8AD' '8B' 'dPFC' 'vPFC' 'PS' 'AS' 'PEC' 'PGM' 'PE' 'PG' 'MIP' 'LIP' 'PEcg' 'IPS' 'WM' '9L'};

[eliminatepair,col] = find(pairtypes == 99 | pairtypes == 6 | pairtypes ==7 | pairtypes == 6 | pairtypes == 9 | pairtypes == 15 | pairtypes == 16 | pairtypes == 17);
keeppairs = setdiff([1: size(pairtypes,1)],eliminatepair);
realpairtypes = pairtypes(keeppairs,:);

shisto = histo(:,keeppairs);
figure
subplot(2,1,1)
bar(shisto')

set(gca,'XTick',[1:25])

a = areasName(realpairtypes);
labels = cell(25,1);
for ii = 1 : 24; labels{ii} = [a{ii,1} a{ii,2}]; end 
set(gca,'XTickLabel',labels)
legend('location sel.','object selec.','Sig. coh')
xlim([0 26])
set(gca,'TickDir','out')
ylabel('counts')
subplot(2,1,2)
perchisto = zeros(2,size(shisto,2));
for ty = 1 : 2; perchisto(ty,:) = 100*shisto(ty,:) ./ shisto(3,:); end

bar(perchisto')
set(gca,'XTick',[1:25])
set(gca,'XTickLabel',labels)
legend('location sel.','object selec.')
ylim([0 55])
xlim([0 26])
set(gca,'TickDir','out')
ylabel('%')

%% stats

nSigcoh = shisto(3,:);
totpairs = sum(shisto(3,:));
totIdepairs = sum(shisto(1,:));
totLocpairs = sum(shisto(2,:));
bagIde = [ones(totIdepairs,1); zeros(totpairs-totIdepairs,1)];
bagLoc = [ones(totLocpairs,1); zeros(totpairs-totLocpairs,1)];

randHIstoIde = zeros(1000,24);
randHIstoLoc = zeros(1000,24);
for ii = 1 : 1000
   norderIde = randperm(totpairs);
    norderLoc = randperm(totpairs);
   % initiation
   randHIstoIde(ii,1) = sum(bagIde(norderIde(1 : shisto(3,1))));
   
   randHIstoLoc(ii,1) = sum(bagLoc(norderLoc(1 : shisto(3,1))));
   %
   for h = 2 : 24
       randHIstoIde(ii,h) = sum(bagIde(norderIde(sum(shisto(3,1:h-1),2) +1 : sum(shisto(3,1:h),2))));
   end
   for h = 2 : 24
       randHIstoLoc(ii,h) = sum(bagLoc(norderLoc(sum(shisto(3,1:h-1),2) +1 : sum(shisto(3,1:h),2))));
   end
end

nIdepairRand = prctile(randHIstoIde,95,1);
nLocpairRand = prctile(randHIstoLoc,95,1);
pcPairs = 100*nIdepairRand ./ shisto(3,:);

hold on

plot(pcPairs,'+')

pcPairs = 100*nLocpairRand ./ shisto(3,:);

hold on

plot(pcPairs,'r+')
