locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [PREmugramLcos{lc},time,f,ntrials,sig,PREPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('PRElocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.1 0.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [SAmugramLcos{lc},time,f,ntrials,sig,SAPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('SAlocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.6 0.9],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE1mugramLcos{lc},time,f,ntrials,sig,DE1PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE1locSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE2mugramLcos{lc},time,f,ntrials,sig,DE2PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE2locSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.405 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DEmugramLcos{lc},time,f,ntrials,sig,DEPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DElocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [PREmugramLcos{lc},time,f,ntrials,sig,PREPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('PREideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.1 0.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [SAmugramLcos{lc},time,f,ntrials,sig,SAPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('SAideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.6 0.9],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE1mugramLcos{lc},time,f,ntrials,sig,DE1PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE1ideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE2mugramLcos{lc},time,f,ntrials,sig,DE2PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE2ideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.405 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DEmugramLcos{lc},time,f,ntrials,sig,DEPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DEideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.705],'redo','save');
end

load migramIdePerLocRule1prctile98PRElocSel1Bias.mat
length(find(sum(sigcondStim,2) ~= 0))

%% object selectivity
clear all
obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number','snr',99);

epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramIdePerLocRule1prctile98%slocSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end
% save sigCoh&MIPerLoc.mat sigCoh sigCohMI
DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);
DEsigCohMI = unique([sigCohMI{3,1}; sigCohMI{3,2}; sigCohMI{3,3}; sigCohMI{4,1}; sigCohMI{4,2}; sigCohMI{4,3}]);
DEsigMI{1} = [sigCohMI{3,1}' sigCohMI{3,2}' sigCohMI{3,3}'];
DEsigMI{2} = [sigCohMI{4,1}' sigCohMI{4,2}' sigCohMI{4,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{3,1})) + 1 zeros(1,length(sigCohMI{3,2})) + 2 zeros(1,length(sigCohMI{3,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{4,1})) + 1 zeros(1,length(sigCohMI{4,2})) + 2 zeros(1,length(sigCohMI{4,3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramIdePerLocRule1prctile98DElocSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))



%% check for the duplicates
EPOCHCOH = cell(4,1);
EPOCHCOHMI = cell(4,1);
for ep =1 : 4
    EPOCHCOHMI{ep} = unique([sigCohMI{ep,1}; sigCohMI{ep,2}; sigCohMI{ep,3}]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(EPOCHCOHMI{ep}));
    display(size(pairs,1))
    
    EPOCHCOH{ep} = unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(EPOCHCOH{ep}));
    display(size(pairs,1))
end


[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCohMI));
display(size(pairs,1))
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCoh));
display(size(pairs,1))

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(uselpairLocs(2,:)));
% in progress
%%
smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
phaseStimAngle = cell(3,1);
powerStimPPC = cell(3,1);
powerStimPFC = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramIdePerLocRule1prctile98DElocSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    for stim = 1 : 3
    coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
    VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
     phaseStimAngle{stim} = cat(2,phaseStimAngle{stim},circ_mean(prefStimPhase(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
     powerStimPPC{stim} = cat(1,powerStimPPC{stim},squeeze(mean(prefStimS{1}(stim,uselpairLocs(2,locpairs),:,5:8),4)));
      powerStimPFC{stim} = cat(1,powerStimPFC{stim},squeeze(mean(prefStimS{2}(stim,uselpairLocs(2,locpairs),:,5:8),4)));
    end
end
% save('IdePairsUnbiased.mat','uselpairLocs','smugram','ExpVgram','coherenceStim','VcoherenceStim','phaseStimAngle')
% time = time - 0.005;
biasmugram = smugram - ExpVgram; 
time = time -0.005;
figure; set(gcf,'Name','Object selectivity')
subplot(4,1,1);

nf = [0 :50];
ntime = [0.1:0.02:1.8];

thedata = squeeze(nanmedian(biasmugram,1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata');
colorbar
caxis([-0.04 0.14])
set(gca,'TickDir','out')
ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the object at any location during the delay (10-42 Hz); n=438')

subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(438);
ste2 = thestd / sqrt(65);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
set(gca,'TickDir','out')


subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(438);
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
set(gca,'TickDir','out')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(438);
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')
set(gca,'TickDir','out')
for ti = 1 : 35; [pval(ti), k, K] = circ_kuipertest(squeeze(VcoherenceStim{1}(1,:,ti)),squeeze(VcoherenceStim{3}(1,:,ti))); end
for ti = 1 : 35; pval(ti) =  kruskalwallis([squeeze(VcoherenceStim{1}(1,:,ti))' squeeze(VcoherenceStim{3}(1,:,ti))' squeeze(VcoherenceStim{2}(1,:,ti))'],{'min', 'max', 'inter'},'off'); end
time(find(pval < 0.05/35))-0.005


%extrafig

figure;
stimlabel = {'r' 'b' 'k'};
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(uselpairLocs(2,:)));

nPPC = length(unique([grs(:,1) depths(:,1)],'rows'));
nPFC = length(unique([grs(:,2) depths(:,2)],'rows'));
for stim =1 : 3 
    subplot(1,2,1);
    me = mean(powerStimPFC{stim},1) / (4^2); % includes the amplifier multiplification
    
    
    ste = (std(powerStimPFC{stim},1)/ (4^2)) / sqrt(nPFC);
    plot(time(1:35),me,[stimlabel{stim} '-']); 
    
    hold on
    
    plot(time(1:35),me+ste,[stimlabel{stim} '--']); 
      plot(time(1:35),me-ste,[stimlabel{stim} '--']); 
    
%     axis([0.1 1.8 16 24])


    subplot(1,2,2);
    me = mean(powerStimPPC{stim},1) / (4^2);
    ste = (std(powerStimPPC{stim},1) / (4^2)) / sqrt(nPPC);
    plot(time(1:35),me,[stimlabel{stim} '-']); 
    
    hold on
    
    plot(time(1:35),me+ste,[stimlabel{stim} '--']); 
      plot(time(1:35),me-ste,[stimlabel{stim} '--']);
    
%     axis([0.1 1.8 40 70])
end
subplot(1,2,2)
xlabel('Time (s)')
ylabel('Power (uV^2)')
title(sprintf('PPC; n = %d',nPPC))
set(gca,'TickDir','out')
subplot(1,2,1); title(sprintf('PFC; n = %d',nPFC))
set(gca,'TickDir','out')

stimcomb = [1 3; 1 2; 2 3];
allp = [];c=1;for stim = 1 : 3; for ti = 1 : 35; [h,pval(ti)] = kstest2(squeeze(powerStimPPC{stimcomb(stim,1)}(:,ti)),squeeze(powerStimPPC{stimcomb(stim,2)}(:,ti))); end; allp = [allp pval]; end
min(allp)

allp = [];c=1;for stim = 1 : 3; for ti = 1 : 35; [h,pval(ti)] = kstest2(squeeze(powerStimPFC{stimcomb(stim,1)}(:,ti)),squeeze(powerStimPFC{stimcomb(stim,2)}(:,ti))); end; allp = [allp pval]; end

clear pval
for ti = 1 : 35; pval(ti) = kruskalwallis([squeeze(powerStimPFC{1}(:,ti)) squeeze(powerStimPFC{2}(:,ti)) squeeze(powerStimPFC{3}(:,ti))],{'min', 'inter', 'max'},'off'); end;
time(find(pval < 0.05/35))-0.005

%extrafig

figure; 
c=1;for s = [1 3 5]; 
    subplot(3,2,s);
    me = mean(powerStimPFC{c},1);
    ste = std(powerStimPFC{c},1) / sqrt(438);
    plot(time(1:35),me); 
    
    hold on
    
    plot(time(1:35),me+ste,'--'); 
      plot(time(1:35),me-ste,'--'); 
    c=c+1;
    axis([0.1 1.8 260 360])
end

c=1;for s = [2 4 6]; 
    subplot(3,2,s);
    me = mean(powerStimPPC{c},1);
    ste = std(powerStimPPC{c},1) / sqrt(438);
    plot(time(1:35),me); 
    
    hold on
    
    plot(time(1:35),me+ste,'--'); 
      plot(time(1:35),me-ste,'--');
    c=c+1;
    axis([0.1 1.8 700 1050])
end

min(allp)




%% location selectivity
clear all
cd /Volumes/raid/data/monkey
obj = loadObject('cohInterIDEandLongSorted.mat');

[r,ind] = get(obj,'Number','snr',99);

epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramLocPerIdeRule1prctile98%sideSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end

DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);
DEsigCohMI = unique([sigCohMI{3,1}; sigCohMI{3,2}; sigCohMI{3,3}; sigCohMI{4,1}; sigCohMI{4,2}; sigCohMI{4,3}]);
DEsigMI{1} = [sigCohMI{3,1}' sigCohMI{3,2}' sigCohMI{3,3}'];
DEsigMI{2} = [sigCohMI{4,1}' sigCohMI{4,2}' sigCohMI{4,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{3,1})) + 1 zeros(1,length(sigCohMI{3,2})) + 2 zeros(1,length(sigCohMI{3,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{4,1}),1) + 1 zeros(1,length(sigCohMI{4,2})) + 2 zeros(1,length(sigCohMI{4,3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramLocPerIdeRule1prctile98DEideSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs),[],2);
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))


%% check for the duplicates
EPOCHCOH = cell(4,1);
EPOCHCOHMI = cell(4,1);
for ep =1 : 4
    EPOCHCOHMI{ep} = unique([sigCohMI{ep,1}; sigCohMI{ep,2}; sigCohMI{ep,3}]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(EPOCHCOHMI{ep}));
    display(size(pairs,1))
    
    EPOCHCOH{ep} = unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]);
    [nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(EPOCHCOH{ep}));
    display(size(pairs,1))
end


[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCohMI));
display(size(pairs,1))
[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(DEsigCoh));
display(size(pairs,1))

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(uselpairLocs(2,:)));
%need to work on the duplicates for selct pairs
%% get result for 12-22 Hz
smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
phaseStimAngle = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramLocPerIdeRule1prctile98DEideSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    
    for stim = 1 : 3
        coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefStim(stim,uselpairLocs(2,locpairs),:,5:8),4));
        VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefStimPhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
        phaseStimAngle{stim} = cat(2,phaseStimAngle{stim},circ_mean(prefStimPhase(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
    
end
save('LocPairsUnbiased.mat','uselpairLocs','smugram','ExpVgram','coherenceStim','VcoherenceStim','phaseStimAngle')

time = time -0.005;
biasmugram = smugram - ExpVgram; 
figure; set(gcf,'Name','location selectivity')
subplot(4,1,1);
nf = [0 :50];
ntime = [0.1:0.02:1.8];

thedata = squeeze(nanmedian(biasmugram,1));
thedata = interp2(f,time,thedata,nf,ntime');
imagesc(time,f,thedata');
colorbar
caxis([-0.04 0.14])
set(gca,'TickDir','out')
ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the location using any object during the delay (10-42 Hz); n=409')

subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1 1.8])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1 1.8])
title('MI for pairs selective for the object at any location during the delay (10-42 Hz); n=428')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(65);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1 1.8])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')

%% check the pairs for which selectivity
load LocPairsUnbiased.mat

LocPairs = uselpairLocs;

load IdePairsUnbiased.mat

IdePairs = uselpairLocs;
[mixedPairs,ia,ib] = intersect(IdePairs(2,:),LocPairs(2,:));

[nsites,pairs,npairs,pairNumberInd,grs,depths] = getDuplicates(obj,ind(mixedPairs));
display(size(pairs,1))


%% test compare identity and location 
 load IdeMugramUnbiased.mat
 
 ideMugram = biasmugram;
 load LocMugramUnbiased.mat
 
 locMugram = biasmugram;
 
 betaIdeMI = mean(ideMugram(:,:,5:8),3);
 betaLocMI = mean(locMugram(:,:,5:8),3);
 
 
 for ti = 1 : 35
     [h(ti),p(ti)] = kstest2(betaIdeMI(:,ti),betaLocMI(:,ti),0.05/35);
 end
 0.05*min(p) /(0.05/35)
 for ti = 1 : 35
     [h(ti),p(ti)] = kstest2(betaIdeMI(setdiff(1:465,ia),ti),betaLocMI(setdiff(1:428,ib),ti),0.05/35);
 end
0.05*min(p) /(0.05/35)
%  figure; plot(prctile(betaIdeMI,[25 75]),'r'); hold on; plot(prctile(betaLocMI,[25 75]),'b'); plot(p,'*')

