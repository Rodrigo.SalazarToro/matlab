

clear all
cd /Volumes/raid/data/monkey

load('migramIdePerLocRule1prctile98DElocSel1Bias.mat')



load IdePairsUnbiased.mat

stimuli = {'minimum' 'intermediate' 'maximum'};
stimlab = {'r' 'b' 'k'};
figure
for stim = 1 : 3
    bestStimphase = rad2deg(squeeze(phaseStimAngle{stim}(1,:,:)));
    
    
    [N,C] = hist(bestStimphase,[-180:5:180]);
    
    
    subplot(2,3,stim)
    imagesc(time,[-180:5:180],N)
    title(stimuli{stim})
    caxis([0 35])
    colorbar
    hold on
    
    line([0.1 1.8],[0 0],'Color','w')
    
    line([0.5 0.5],[-180 180],'Color','w')
    line([1 1],[-180 180],'Color','w')
    
    subplot(2,3,4)
    bestStimphase = squeeze(phaseStimAngle{stim}(1,:,:));
    plot(time,rad2deg(circ_mean(bestStimphase,[],1)),stimlab{stim})
    hold on
end
xlim([0.1 1.8])
xlabel('Time (s)')

ylabel('Angle (deg)')