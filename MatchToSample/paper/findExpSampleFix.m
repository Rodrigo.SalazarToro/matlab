function [allrdiff,indexes,alltrials,varargout] = findExpSampleFix(obj,varargin)
% FixTfixSnotCombined option not yet made


Args = struct('plotSig',0,'save',0,'redo',0,'rule',1,'parfor',0,'mintrials',70,'eventJitter',0,'plength',400);
Args.flags = {'FixTfixSnotCombined','plotSig','save','redo','parfor'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

[r,ind] = get(obj,'snr',99,'Number','delay','long',modvarargin{:});

c = 1 ;
indexes= [];
params = struct('tapers',[2 3],'Fs',200,'trialave',1);

dir = 'expectEvent';
for ii = ind
    
    cd(obj.data.setNames{ii})
    pairg = obj.data.Index(ii,10:11);
    if isempty(nptDir(dir))
        mkdir(dir)
    end
    cd(dir)
    
    thefile = sprintf('fixSampleg%04.0fg%04.0fRule%d%d.mat',pairg(1),pairg(2),Args.rule,Args.plength);
    
    if Args.redo || isempty(nptDir(thefile))
        cd(obj.data.setNames{ii})
        cd session01
        info = NeuronalCHAssign;
        channels = find(ismember(info.groups,pairg) == 1);
        mts =mtstrial('auto');
        
        trialsF = mtsgetTrials(mts,'CueObj',55);
        [data,lplength] = lfpPcut(trialsF,channels,'EPremoved','epochs',{'CueOnset' [-Args.plength+Args.eventJitter Args.eventJitter]},'plength',Args.plength);
        
        [dz,vdz,Adz,f,C{1},C{2},rdiff] = compareCoherence(data{1},data{2},params,modvarargin{:});
        [Cgram,phigram,~,~,~,t,fgram] = MTScohgram(mts,trialsF,channels,'reference',{'CueOnset' Args.plength+Args.eventJitter Args.plength+Args.eventJitter},'mwind',[0.1 0.01],'tapers',[3 5],modvarargin{:});
        %        imagesc(t,f,squeeze(Cgram)'); colorbar; pause
        if Args.save
            cd(obj.data.setNames{ii})
            cd(dir)
            save(thefile,'dz','vdz','Adz','f','C','data','trialsF','rdiff','Args','modvarargin','Cgram','phigram','t','fgram');
            display(sprintf('saving %s/%s',pwd,thefile))
            cd ..
        end
    else
        cd(obj.data.setNames{ii})
        cd(dir)
        load(thefile,'dz','vdz','Adz','f','C','trialsF','rdiff','Cgram','phigram','t','fgram');
        cd ..
    end
    %      plot(f,filtfilt(repmat(1/2,1,2),1,C{1})); hold on; plot(f,filtfilt(repmat(1/2,1,2),1,C{2}),'--'); plot(f,~Adz.*filtfilt(repmat(1/2,1,2),1,C{1}),'rx');plot(f,~Adz.*filtfilt(repmat(1/2,1,2),1,C{2}),'rx');xlim([0 50]);pause; clf
    n = length(trialsF);
    if (n >= Args.mintrials) && isempty(find(ii == indexes))
        alltrials(c) = length(trialsF);
        allrdiff(c,:) = ~Adz;
        allC(c,1,:) = C{1};
        allC(c,2,:) = C{2};
        allgram(c,:,:) = Cgram;
        c = c + 1;
        indexes = [indexes ii];
    end
    
end

%% stats comparison with fix
varargout{1} = allC;
varargout{2} = allgram;
varargout{3} = t;
varargout{4} = fgram;
if Args.plotSig
    figure
    
    subplot(2,1,1)
    onset = find(t == 0.4050);
    
    meanOnset = repmat(mean(squeeze(allgram(:,1:onset,:)),2),[1 size(allgram,2) 1]);
    
    stdOnset = repmat(std(squeeze(allgram(:,1:onset,:)),[],2),[1 size(allgram,2) 1]);
    thegrams =allgram;
    sigdrop = (thegrams < (meanOnset - 2*stdOnset));
    
    fractSig = squeeze(sum(sigdrop,1)) / size(sigdrop,1);
  
    imagesc(t,fgram,fractSig');
    colorbar
    hold on
    
    line([Args.plength/1000 Args.plength/1000], [fgram(1) fgram(end)],'Color','r');
    caxis([0 0.4])
    ylim([10 45])
    title('Sig. decrease sec')
    
    
    
    subplot(2,1,2);
    imagesc(t,fgram,squeeze(median(allgram,1))');
    hold on
    line([Args.plength/1000 Args.plength/1000], [fgram(1) fgram(end)],'Color','r');
    xlabel('Time [sec]')
    ylabel('Frequency [Hz]')
    %     subplot(2,1,2)
    %     fbeta = find(f>=13 & f<30);
    %     [row,col] = find(allrdiff(:,fbeta) == 1);
    %     sigdiff = unique(row);
    %     plot(f, prctile(Cdiff(sigdiff,:),[10 25 50 75 90],1))
    
end
