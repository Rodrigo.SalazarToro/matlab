function threshold_coherence(varargin)

%currently uses global surrogate at 99% for 4 epochs

Args = struct('global',0,'pairwise',0);
Args.flags = {'global','pairwise'};
Args = getOptArgs(varargin,Args);


cohdir = pwd;
%get list of pairs
cd ../..
[~,~,pairs] = bmf_groups;
cd(cohdir)
nfiles = size(pairs,1);

freq_range = [5 11;12 25;26 40;41 70];
nfreq = size(freq_range,1);
%100:500, 600:1000, 1000:1400, 1400:1800
epochs = [200 400; 700 900;1100 1300; 1500 1700];
nepochs = size(epochs,1);

if Args.global
    %global surrogate threshold
    load globalsurcohgram_surrogate.mat
    gthresh = fitthresh{3}; %(1) 10^-3, (2) 10^-4, (3) 10^-5

    for f = 1 : nfiles
        load(['cohgram' num2strpad(pairs(f,1),3) num2strpad(pairs(f,2),3)])

        %get groups
        g1 = cohgram.groups{1}(1);
        g2 = cohgram.groups{1}(2);
        
        cgram = cohgram.C{1}(1:41,:)'; %cohgram, low frequencies are at the top
        freqs = cohgram.f{1};
        times = floor(cohgram.t{1}*100)*10;
        
        %split sigim into frequencies. determine the number of significant bins
        %and the average coh value for those significant bins
        sigim = zeros(size(cgram,1),size(cgram,2));
        sigim(find(cgram >= gthresh)) = 1; %find values above the surrogate threshold
        
        for e = 1 : nepochs
            for nf = 1 : nfreq
                fs = find(freqs >= freq_range(nf,1) & freqs <= freq_range(nf,2));
                t = find(times >= epochs(e,1) & times <= epochs(e,2));
                sig.fcount(e,nf) = size(find(sigim(fs,t)),1);
                sig.fmean(e,nf) = mean(mean(cgram(fs,t))); 
                sig.ftotalbins(e,nf) = size(fs,2)*size(t,2);
            end
        end
        
        write_info;
        fname = [ 'epochthreshglobal' num2strpad(g1,3) num2strpad(g2,3)];
        save(fname,'sig','sigim','write_info');
    end
end

if Args.pairwise
    for f = 1 : nfiles
        load(['cohgram' num2strpad(pairs(f,1),3) num2strpad(pairs(f,2),3)])
        %get groups
        g1 = cohgram.groups{1}(1);
        g2 = cohgram.groups{1}(2);
        
        load(['threshsurcohgram' num2strpad(pairs(f,1),3) num2strpad(pairs(f,2),3)])
        thresh = all_thresh{6};
        
        
        cgram = cohgram.C{1}(1:41,:)'; %cohgram, low frequencies are at the top
        freqs = cohgram.f{1};
        times = floor(cohgram.t{1}*100)*10;
        thresh3 = thresh{3}; %(1) 10^-3, (2) 10^-4, (3) 10^-5
        
        %split sigim into frequencies. determine the number of significant bins
        %and the average coh value for those significant bins
        sigim = zeros(size(cgram,1),size(cgram,2));
        sigim(find(cgram >= thresh3)) = 1; %find values above the surrogate threshold
        
        for e = 1 : nepochs
            for nf = 1 : nfreq
                fs = find(freqs >= freq_range(nf,1) & freqs <= freq_range(nf,2));
                t = find(times >= epochs(e,1) & times <= epochs(e,2));
                sig.fcount(e,nf) = size(find(sigim(fs,t)),1);
                sig.fmean(e,nf) = mean(mean(cgram(fs,t)));
                sig.ftotalbins(e,nf) = size(fs,2)*size(t,2);
            end
        end
        
        write_info;
        fname = [ 'epochthresh' num2strpad(g1,3) num2strpad(g2,3)];
        save(fname,'sig','sigim','write_info');
    end
end

