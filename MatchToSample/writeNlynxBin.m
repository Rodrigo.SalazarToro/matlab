function writeNlynxBin(varargin)

%writes streamer files for neuralynx data on sessions that were recorded continuously
%use for freeview sessions

Args = struct('filename',[],'channels',[],'recorded',[],'CSC',[],'start',[],'stop',[],'bin_minutes',.0833,'nlynx2streamer_mat',1);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

nlxdir = pwd;

%determine if multiple bins need to be written
% if Args.nlynx2streamer_mat
%     csc = write_nlynx2streamer_files(Args.CSC{1,Args.channels(Args.recorded(1))},0,0);
%     d = csc.Samples;
% else
    [csc.SampleFrequencies, csc.Samples] = Nlx2MatCSC_v3(Args.CSC{1,Args.channels(Args.recorded(1))},[0 0 1 0 1],0,1);
    d = reshape(csc.Samples,1,numel(csc.Samples));
% end
numb_samples = size(d,2)

%number of bins
nbins = ceil(numb_samples / (Args.bin_minutes * 1920000)); %samples per min = 1920000 at 32kHz sampling rate (if bin_minutes = .0167, trials will be 1 second)

bins = 0:(Args.bin_minutes * 1920000):(nbins * Args.bin_minutes * 1920000);

for nb = 1 : nbins
    nbins
    nb
    cd(nlxdir)
    channel = 0;
    for cc = Args.recorded
        channel = channel + 1;
        
        
%         if Args.nlynx2streamer_mat
%             csc_channel = ['CSC' num2str(cc) '.ncs'];
%             csc = write_nlynx2streamer_files(csc_channel,t.start(tt),t.stop(tt)); 
%             d = csc.Samples;
%         else
            [csc.SampleFrequencies, csc.Samples] = Nlx2MatCSC_v3(Args.CSC{1,Args.channels(cc)},[0 0 1 0 1],0,1);
            d = reshape(csc.Samples,1,numel(csc.Samples));
%         end
        
        %select data for bins
        if bins(nb+1) < size(d,2)
            db = d((bins(nb)+1):(bins(nb+1)));
        else
            db = d((bins(nb)+1):size(d,2));
        end
        
        numb_samples = size(db,2);
        if channel == 1
            data=NaN(size(Args.recorded,2),numb_samples);
        end
        data(channel,:) = db;
    end
    sampling_rate = csc.SampleFrequencies(1);
    
    %if there is only one file then a bin file is written otherwise streamer files are written.
    if nbins > 1 
        filename = ([Args.filename(1:end-3) num2strpad(nb,4)]);
    else
        filename = Args.filename;
    end
    cd ..
    nptWriteStreamerFile(filename,sampling_rate,data,Args.recorded'); %scan order must be a column
end