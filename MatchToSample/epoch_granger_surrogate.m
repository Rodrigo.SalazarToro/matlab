function epoch_granger_surrogate(varargin)

%computes granger for 7 different time periods
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]
% 8: ititrial [(match + 201) : (match + 800)], for incorrect, betty trials only

Args = struct('ml',0,'bmf',0,'rules',[],'perms',100);
Args.flags = {'ml','bmf'};
[Args,modvarargin] = getOptArgs(varargin,Args);

movingwin = [.2 .05]; %this is in seconds
params.Fs = 200;

sesdir = pwd;
if Args.ml
    if Args.bmf
        N = NeuronalHist('bmf');
        [~,chpairlist,~,~,sortedpairs] = bmf_groups('ml');
    else
        N = NeuronalHist('ml');
        [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
    end
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end

cd([sesdir filesep 'lfp']);
lfpdir = pwd;
lfptrials = nptDir('*_lfp.*');
numpairs = nchoosek(N.chnumb,2);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)

sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1, 4];
        else
            rules = [1 2 3 4]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    cd(lfpdir)
    if Args.ml
        if rule == 1
            trials = identity;
            granger_epochs_surrogates = cell(7,numpairs); %seven combinations
        elseif rule == 2
            trials = location;
            granger_epochs_surrogates = cell(7,numpairs); %seven combinations
        elseif rule == 3
            trials = fixtrials;
            granger_epochs_surrogates = cell(7,numpairs); %seven combinations
        elseif rule == 4
            trials = incorrtrials;
            granger_epochs_surrogates = cell(8,numpairs); %seven combinations
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        ntrials = size(trials,2);
        counter = 0;
        
        for sp = sortedpairs
            if Args.bmf
                counter = counter + 1;
                c1 = chpairlist(counter,1);
                c2 = chpairlist(counter,2);
            else
                c1 = chpairlist(sp,1);
                c2 = chpairlist(sp,2);
            end
            tcounter = 0;
            data1 = zeros(1000,ntrials);
            data2 = zeros(1000,ntrials);
            for x = trials
                tcounter = tcounter + 1;
                s_on = sample_on(x);
                s_off = sample_off(x);
                m = match(x);
                
                [data.rawdata,~,data.samplingRate]=nptReadStreamerFile(lfptrials(x).name);
                p1t = data.rawdata(c1,:);
                p2t = data.rawdata(c2,:);
                
                %cut, get rid of 60! and detrend
                p1t = preprocessinglfp(p1t(s_on-500:end),'detrend')';
                p2t = preprocessinglfp(p2t(s_on-500:end),'detrend')';
                
                data1((1:size(p1t,2)),tcounter) = p1t;
                data2((1:size(p2t,2)),tcounter) = p2t;
            end
            
                        %%%%%%TIMING IS ALL WRONG, NEED TO CUT TRIAL BEFORE PUTTING
            %%%%%%THEM TOGETHER!!!! REWRITE, USE EPOCH GRANGER
            
            fix = (round(s_on/5) - round(399/5)) : round(s_on/5);
            sample = (round(s_off/5) - round(399/5)) : round(s_off/5);
            delay400 = round(s_off/5) : (round(s_off/5) + round(399/5));
            delay800 = (round(s_off/5) + round(401/5)) : (round(s_off/5) + round(800/5));
            delay = (round(s_off/5) + round(201/5)) : (round(s_off/5) + round(800/5));
            
            if rule == 4
                iti = (round(match/5) + round(701/5)) : (round(match/5) + round(1300/5));
            end
            
            for all_perms = 1 : Args.perms
                
                perms1 = randperm(ntrials);
                perms2 = randperm(ntrials);
                
                permdata1 = data1(:,perms1);
                permdata2 = data2(:,perms2);
                
                [f.fx2y(all_perms,:),f.fy2x(all_perms,:),f.fxy(all_perms,:),f.freq] = GCepoch(permdata1(fix,:),permdata2(fix,:),params,20); %100:500ms, or 0:80samples @200Hz
                
                [s.fx2y(all_perms,:),s.fy2x(all_perms,:),s.fxy(all_perms,:),s.freq] = GCepoch(permdata1(sample,:),permdata2(sample,:),params,20);
                
                [d400.fx2y(all_perms,:),d400.fy2x(all_perms,:),d400.fxy(all_perms,:),d400.freq] = GCepoch(permdata1(delay400,:),permdata2(delay400,:),params,20);
                
                [d800.fx2y(all_perms,:),d800.fy2x(all_perms,:),d800.fxy(all_perms,:),d800.freq] = GCepoch(permdata1(delay800,:),permdata2(delay800,:),params,20);
                
                [d.fx2y(all_perms,:),d.fy2x(all_perms,:),d.fxy(all_perms,:),d.freq] = GCepoch(permdata1(delay,:),permdata2(delay,:),params,20);
                
                
                if rule == 4
                    [iti.fx2y(all_perms,:),iti.fy2x(all_perms,:),iti.fxy(all_perms,:),iti.freq] = GCepoch(permdata1(iti,:),permdata2(iti,:),params,20);
                end
            end
            
            f.fx2y = prctile(f.fx2y,[95,99,99.9,99.95]);
            f.fy2x = prctile(f.fy2x,[95,99,99.9,99.95]);
            f.fxy = prctile(f.fxy,[95,99,99.9,99.95]);
            
            s.fx2y = prctile(s.fx2y,[95,99,99.9,99.95]);
            s.fy2x = prctile(s.fy2x,[95,99,99.9,99.95]);
            s.fxy = prctile(s.fxy,[95,99,99.9,99.95]);
            
            d400.fx2y = prctile(d400.fx2y,[95,99,99.9,99.95]);
            d400.fy2x = prctile(d400.fy2x,[95,99,99.9,99.95]);
            d400.fxy = prctile(d400.fxy,[95,99,99.9,99.95]);
            
            d800.fx2y = prctile(d800.fx2y,[95,99,99.9,99.95]);
            d800.fy2x = prctile(d800.fy2x,[95,99,99.9,99.95]);
            d800.fxy = prctile(d800.fxy,[95,99,99.9,99.95]);
            
            d.fx2y = prctile(d.fx2y,[95,99,99.9,99.95]);
            d.fy2x = prctile(d.fy2x,[95,99,99.9,99.95]);
            d.fxy = prctile(d.fxy,[95,99,99.9,99.95]);
            
            if rule == 4
                iti.fx2y = prctile(iti.fx2y,[95,99,99.9,99.95]);
                iti.fy2x = prctile(iti.fy2x,[95,99,99.9,99.95]);
                iti.fxy = prctile(iti.fxy,[95,99,99.9,99.95]);
            end

            granger_epochs_surrogates{1,sp} =  f;
            granger_epochs_surrogates{2,sp} =  s;
            granger_epochs_surrogates{3,sp} =  d400;
            granger_epochs_surrogates{4,sp} =  d800;
            granger_epochs_surrogates{5,sp} =  d;
            granger_epochs_surrogates{6,sp} =  [];
            granger_epochs_surrogates{7,sp} =  [];
            if rule == 4
                granger_epochs_surrogates{8,sp} =  iti;
            end
        end
    end
    cd([lfpdir filesep 'lfp2'])
    write_info = writeinfo(dbstack);
    if Args.ml
        if rule == 1;
            mkdir('identity')
            cd('identity')
            save granger_epochs_surrogates granger_epochs_surrogates
        elseif rule == 2;
            mkdir('location')
            cd('location')
            save granger_epochs_surrogates granger_epochs_surrogates
        elseif rule == 3
            mkdir('fixation')
            cd('fixation')
            save granger_epochs_surrogates granger_epochs_surrogates
        elseif rule == 4
            mkdir('incorrect')
            cd('incorrect')
            save granger_epochs_surrogates granger_epochs_surrogates
        end
    else
        save granger_epochs_surrogates granger_epochs_surrogates
    end
end

cd(sesdir)

fprintf(1,'\n')
fprintf(1,'Epoch Granger Done.\n')