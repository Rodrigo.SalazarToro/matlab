function [sigPosgr1,sigNeggr1,varargout] = xcorrSpikeSummary(obj,ind,varargin)

Args = struct('name',[],'plot',0,'thresh',.01,'iterations',500,'threshcomp',.05);
Args.flags = {'plot'};
[Args,modvarargin]  = getOptArgs(varargin,Args);

epochs = {'presample' 'sample' 'delay1' 'delay2'};
pv = cell(2,1);
xc = cell(2,1);
compxc = nan(length(ind),4);
histo = nan(length(ind),2);
for ii = 1 : length(ind)
    rind = ind(ii);
    name1 = sprintf('g%04g%02g%s',obj.data.Index(rind,3),obj.data.Index(rind,9),obj.data.Index(rind,7));
    name2 = sprintf('g%04g%02g%s',obj.data.Index(rind,4),obj.data.Index(rind,10),obj.data.Index(rind,8));
    for r = 1 : 2
        pvaluefile1 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name1 name2 'pvaluesRule' num2str(r) '.mat'];
        xcfile1 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name1 name2 'Rule' num2str(r) '.mat'];
        pvaluefile2 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name2 name1 'pvaluesRule' num2str(r) '.mat'];
        xcfile2 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name2 name1 'Rule' num2str(r) '.mat'];
        
        cd(obj.data.setNames{rind})
        try
            load(pvaluefile1);
        catch
            load(pvaluefile2);
        end
        pv{r}(ii,1:3) = pvalues(1,1:3);
        pv{r}(ii,4) = pvalues(2,3);
        try
            load(xcfile1)
        catch
            load(xcfile2);
        end
        xc{r}(ii,1:3) = rc(1,1:3);
        xc{r}(ii,4) = rc(2,3);
        
    end
    histo(ii,:) = sort(obj.data.hist(rind,:));
    
    % rule comparisons
    pvaluefile1 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name1 name2 'RuleComp500mspvalues.mat'];
    pvaluefile2 = [obj.data.setNames{ind(ii)} filesep 'xccorr' name2 name1 'RuleComp500mspvalues.mat'];
    cd(obj.data.setNames{rind})
    try
        load(pvaluefile1);
    catch
        load(pvaluefile2);
    end
    compxc(ii,1:3) = pvalues(1,1:3);
    compxc(ii,4) = pvalues(2,3);
end

% fdr = nan(length(ind),4);
fdrn = cell(2,1);
fdrp = cell(2,1);

ufdr = cat(2,reshape(pv{1},1,size(pv{1},1) * size(pv{1},2)),reshape(pv{2},1,size(pv{2},1) * size(pv{2},2)));
ufdrcomp = reshape(compxc,1,size(compxc,1)*size(compxc,2));

ufdrn = mafdr(ufdr,'BHFDR',true);
ufdrp = mafdr(1 - ufdr,'BHFDR',true);

ufdrcompn = mafdr(ufdrcomp,'BHFDR',true);
ufdrcompp = mafdr(1 - ufdrcomp,'BHFDR',true);

fdrn{1} = ufdrn(1:size(pv{1},1) * size(pv{1},2));
fdrn{2} = ufdrn(size(pv{1},1) * size(pv{1},2) +1 : end);

fdrp{1} = ufdrp(1:size(pv{1},1) * size(pv{1},2));
fdrp{2} = ufdrp(size(pv{1},1) * size(pv{1},2) +1 : end);

fdrcompn = reshape(ufdrcompn,size(compxc,1),size(compxc,2));

fdrcompp = reshape(ufdrcompp,size(compxc,1),size(compxc,2));

for r = 1 : 2
    fdrn{r} = reshape(fdrn{r},size(pv{1},1),size(pv{1},2));
    fdrp{r} = reshape(fdrp{r},size(pv{1},1),size(pv{1},2));
end
sigPosgr1 = fdrp;
sigNeggr1 = fdrn;
% for r = 1 : 2
%     for ep = 1 : 4
%         fdrn{r}(:,ep) = mafdr(pv{r}(:,ep),'BHFDR',true);
%         fdrp{r}(:,ep) = mafdr(1-pv{r}(:,ep),'BHFDR',true);
%         fdrncomp(:,ep) = mafdr(compxc(:,ep),'BHFDR',true);
%          fdrpcomp(:,ep) = mafdr(1-compxc(:,ep),'BHFDR',true);
%     end
% end

lthres = Args.thresh;
rulexcorr = (fdrcompn < Args.threshcomp) + (fdrcompp < Args.threshcomp);
varargout{1} = rulexcorr;
% hthres = 1 - (Args.thresh /2);
allep = zeros(size(xc{1},1),1);
if Args.plot
    f1 = figure;
    f2 = figure;
    for ep = 1 : 4
        figure(f1)
        subplot(1,4,ep)
        plot(xc{1}(:,ep),xc{2}(:,ep),'o')
        hold on
        sonesn = (fdrn{1}(:,ep) < lthres) | (fdrn{2}(:,ep) < lthres);
        plot(xc{1}(sonesn,ep),xc{2}(sonesn,ep),'go')
        sonesp = (fdrp{1}(:,ep) < lthres) | (fdrp{2}(:,ep) < lthres);
        plot(xc{1}(sonesp,ep),xc{2}(sonesp,ep),'ro')
        xlim([-1 1])
        ylim([-1 1])
        title(sprintf('%s; n = %d positive & n = %d negative',epochs{ep},sum(sonesp),sum(sonesn)))
        set(gca,'TickDir','out')
        figure(f2)
        subplot(4,1,ep)
        
        diffxc = xc{1}(sonesp,ep) - xc{2}(sonesp,ep);
        hist(diffxc,[-1:.05:1])
        xlabel('IDErsc - LOCrsc')
        ylabel('Counts')
        title(sprintf('%s; n = %d positive',epochs{ep},sum(sonesp)))
        line([median(diffxc) median(diffxc)],[0 50],'linestyle','--','color','r')
        stpvalue = signtest(diffxc);
        text(0,50,sprintf('%d sign test',stpvalue))
        allep = allep + sonesp;
    end
    figure
    
    areas = {'6DR ' '8AD ' '8B  '  'dPFC '  'vPFC '  'PS  '  'AS  '  'PEC '  'PGM '  'PE  '  'PG  '  'MIP '  'LIP '  'PEcg'  'IPS '  'WM ' '9L  '};
    upairs = unique(histo(allep ~=0,:),'rows');
    
    npairs = zeros(size(upairs,1),1);
    totpairs = zeros(size(upairs,1),1);
    pareas = cell(size(upairs,1),1);
    rpairs = nan(Args.iterations,size(upairs,1));
    for ii = 1 : Args.iterations
        for ip = 1 : size(upairs,1)
            npairs(ip) = sum(ismember(histo(allep ~=0,:),upairs(ip,:),'rows'));
            rp = randperm(length(allep));
            rpairs(ii,ip) =  nansum(ismember(histo(allep(rp) ~=0,:),upairs(ip,:),'rows'));
            totpairs(ip) = sum(ismember(histo,upairs(ip,:),'rows'));
            pareas{ip} = [areas{upairs(ip,1)} areas{upairs(ip,2)}];
        end
    end
    ppairs = 100*npairs./totpairs;
    
    [b,ix] = sort(ppairs);
    totpairs = totpairs(ix);
    kpairs = totpairs > 30;
    
    subplot(2,1,1)
    bar(b(kpairs),'w')
    set(gca,'xtick',[1:length(kpairs)])
    set(gca,'xtickLabel',pareas(ix(kpairs)))
    ikpairs = find(kpairs);
    for ip = 1 : sum(kpairs)
        text(ip,60,num2str(totpairs(ikpairs(ip))))
    end
    ylim([0 max(b)])
    hold on
    
    prpairs = 100*rpairs(:,ix)./ repmat(totpairs',Args.iterations,1);
    
    %     line([0 sum(kpairs)],[max(prctile(prpairs(:,kpairs),.975)) max(prctile(prpairs(:,kpairs),.975))],'LineStyle','--')
    %     line([0 sum(kpairs)],[min(prctile(prpairs(:,kpairs),.025)) min(prctile(prpairs(:,kpairs),.025))],'LineStyle','--')
    plot(prctile(prpairs(:,kpairs),97.5),'r--')
    plot(prctile(prpairs(:,kpairs),2.5),'r--')
    
    subplot(2,1,2)
    upairs = upairs(ix,:);
    for ip = 1 : sum(kpairs)
        sp = allep ~= 0 & ismember(histo,upairs(ip,:),'rows');
        plot(ip,mean(xc{1}(sp,1:4),2),'ro');
        hold on
        plot(ip,mean(xc{2}(sp,1:4),2),'go');
    end
    set(gca,'xtick',[1:length(kpairs)])
    set(gca,'xtickLabel',pareas(ix(kpairs)))
    
    figure;
    
    
end

np =length(find(sum(sigPosgr1{1} < 0.005 | sigPosgr1{2} < (lthres/2),2)));
display(['Number of significant pairs positive' num2str(np)])

nn =length(find(sum(sigNeggr1{1} < 0.005 | sigNeggr1{2} < (lthres/2),2)));
display(['Number of significant pairs negative' num2str(nn)])

display(['Numer of significant rule diff ' num2str(sum(rulexcorr ~= 0))])

