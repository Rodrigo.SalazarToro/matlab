function run_hilbertphase_entropy(varargin)

%run at session level
%computes Shannon Entropy and normalized to get the synchronization index
%in tass_1998
Args = struct('ml',0,'redo',0,'plot',0,'bmf',0);
Args.flags = {'ml','redo','plot','bmf'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
% bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
window = 300;
stepsize = 50;
nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?

bins = linspace(0,1,nbins);

steps = [window/2:stepsize:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
nw = size(steps,2);
sesdir = pwd;

%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

if Args.ml
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
        %         c = mtscpp2('auto','ml');
        N = NeuronalHist('ml','bmf');
        g = bmf_groups; %these are the non-noisy groups
        s = sorted_groups; %these are the sorted groups
        ggroups = intersect(g,s);
        [alltr trialorder] = get_trial_list('ml','bmf');
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
        c = mtscpp2('auto','ml');
        N = NeuronalHist('ml');
        [alltr trialorder] = get_trial_list('ml');
    end
else
    c = mtscpp2('auto');
    N = NeuronalHist;
    mt = mtstrial('auto','redosetNames');
    [alltr trialorder] = get_trial_list;
end

sample_on = floor(mt.data.CueOnset); %lock to sample on

pair_list = [];
for g1 = 1 : size(ggroups,2);
    for g2 = g1+1 : size(ggroups,2)
        pair_list = [pair_list; ggroups(g1) ggroups(g2)];
    end
end

npairs = size(pair_list,1);

%run through all the frequencies for each pair
all_freq = [5 : 5 : 85];
nfreq = size(all_freq,2);

cd([sesdir filesep 'lfp' filesep 'lfp2'])

for p = 1 : npairs
    entfile = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2) '.mat'];
    if ~exist(entfile,'file') || Args.redo
        load(['relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)])
        for alltypes = 1 : 19
            tr = alltr{alltypes};
            ntrials = size(tr,2);
            hentropy = single(zeros(nfreq,nw,ntrials));
            mphase = single(zeros(nfreq,nw,ntrials));
            
            counter = 0;
            for t = tr
                counter = counter + 1;
                fprintf(1,['Pair ' num2str(p) ' of ' num2str(npairs) ', Trials to go = ' num2str(ntrials-counter) '\n']);
                for nf = 1 : nfreq
                    d = relative_phases(nf,:,t);
                    
                    max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                    %get data ready for hist (hist is aparently the most
                    %time consuming function, found using profile
                    cc = 0;
                    allh = zeros(window,nw);
                    for ww = steps
                        cc = cc + 1;
                        allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
                    end
                    
                    allhist = hist(allh,bins) ./ window; %calculate response distribution
                    re  = -1 * nansum(allhist.*log2(allhist));
                    resp_ent = (max_ent - re) ./ max_ent;
                    mp = circ_mean((allh * (2*pi)));
                    
                    hentropy(nf,:,counter) = single(resp_ent);
                    mphase(nf,:,counter) = single(mp);
                    
                    
                    
                    %                 %run sliding window for sample locked
                    %                 c = 0;
                    %                 max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                    %                 for w = steps
                    %                     c = c+1;
                    %
                    %                     h = hist(d(w-((window/2)-1):w+(window/2)),bins) ./ window;%calculate response distribution
                    %                     mp(c) = circ_mean((d(w-((window/2)-1):w+(window/2)) * (2*pi))');
                    %
                    %                     re = -1 * nansum(h.*log2(h)); %response entropy (aka shannon entropy) see panzeri_2007
                    %                     resp_ent(c) = (max_ent - re) / max_ent; %normalize so that response is between 0 and 1 with 1 = perfect synchrony (tass_1998)
                    %
                    %                     if w <= (1000 - (window/2))
                    %                         hsacc = hist(dsacc(w-((window/2)-1):w+(window/2)),bins) ./ window;
                    %                         mpsacc(c) = circ_mean((dsacc(w-((window/2)-1):w+(window/2)) * (2*pi))');
                    %
                    %                         resacc = -1 * nansum(hsacc.*log2(hsacc));
                    %                         resp_entsacc(c) = (max_ent - resacc) / max_ent;
                    %                     end
                    %                 end
                    %                 hentropy(nf,:,counter) = single(resp_ent);
                    %                 mphase(nf,:,counter) = single(mp);
                    %
                    %                 hentropysacc(nf,:,counter) = single(resp_entsacc);
                    %                 mphasesacc(nf,:,counter) = single(mpsacc);
                end
                
                
                if Args.plot
                    imagesc(interp2(flipud(hentropy(:,1:36,counter))));colorbar
                    set(gca,'XTick',[1:4:72])
                    set(gca,'XTickLabel',steps([1:2:36]))
                    
                    set(gca,'YTick',[1:2:(16*2)])
                    set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
                    %sample on
                    hold on
                    plot([17, 17],[0 33],'color','k')
                    %sample off
                    hold on
                    plot([37, 37],[0 33],'color','k')
                    %earliest match
                    hold on
                    plot([69, 69],[0 33],'color','k')
                    hold on
                    colorbar
%                     pause
                end
                
            end
            hilbertentropy{alltypes} = hentropy;
            mean_phases{alltypes} = mphase;
            
        end
        groups = pair_list(p,:);
        channel_pairs = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
        save(channel_pairs,'hilbertentropy','mean_phases','all_freq','steps','window','trialorder','groups')
    end
    
end
cd(sesdir)















