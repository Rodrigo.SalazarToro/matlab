function [corrcoef phase] = find_correlogram_peak(varargin)

%finds peak closest to zero

Args = struct('correlogram',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

cc = Args.correlogram;
phaseSize = floor(size(Args.correlogram,2)/2);
lags = [(-1*phaseSize): phaseSize];

max_lag = size(cc,2);
%Find index of central positive peak
pp = findpeaks(cc);
for ppp = 1 : size(pp.loc,1)
    if (cc(pp.loc(ppp)) < 0)
        %set to max phase index
        pp.loc(ppp) = max_lag;
    end
end
[phase, pos_index] = (min(abs(lags(pp.loc))));
%get real phase value by indexing the location in the lags
pos_phase = lags(pp.loc(pos_index));
pos_corrcoef = cc(pp.loc(pos_index));
%find index of central negative peak (inverts correlogram)
cc = cc*-1;
np = findpeaks(cc);
for nnn = 1 : size(np.loc,1)
    if (cc(np.loc(nnn)) < 0)
        %set to max phase index
        np.loc(nnn) = max_lag;
    end
end
[phase, neg_index] = (min(abs(lags(np.loc))));
neg_phase = lags(np.loc(neg_index));
neg_corrcoef = cc(np.loc(neg_index));
%determines if the closest peak to zero is positive or negative.
%stores the correlation coef and lag position.
if ~isempty(pos_phase) && ~isempty(neg_phase)
    if abs(pos_phase) <= abs(neg_phase)
        phase = pos_phase;
        corrcoef = pos_corrcoef;
    else
        phase = neg_phase;
        corrcoef = (-1 * neg_corrcoef); %make negative
    end
else
    phase = nan;
    corrcoef = nan;
end

