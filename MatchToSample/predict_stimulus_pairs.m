function [stim_percents loc_percents obj_percents] = predict_stimulus_pairs(varargin)

%run at session level
Args = struct('fits',0,'stim_prediction',0,'stims',0,'p_cutoff',0,'logits',0,'rule',1,'xcor',0,'euclid',0,'train',0);
Args.flags = {'fits','stim_prediction','stims','logits','xcor','euclid','train'};
Args = getOptArgs(varargin,Args);

%rule = 1(identity), rule = 0(location)
if Args.rule
    rule = 'identity';
else
    rule = 'location';
end

N = NeuronalHist;
mt = mtstrial('auto','ML','RTfromML');
if Args.train
    c = mtscpp('auto','ml','train');
else
    c = mtscpp('auto','ml');
end
cd(['lfp' filesep 'lfp2']);

lfp2dir = pwd;

chnumb = N.chnumb;
total_pairs = nchoosek(chnumb,2);

combs = [];
cc = 0;
for c1 = 1 : chnumb
    for c2 = (c1+1) : chnumb
        cc = cc + 1;
        combs(cc,1) = N.gridPos(c1);
        combs(cc,2) = N.gridPos(c2);
    end
end

if Args.fits
    for pair_num = 1 : total_pairs
        total_pairs - pair_num
        int_pvals = [];
        cd(lfp2dir)
        
        chpair = (['channelpair' num2strpad(combs(pair_num,1),2) num2strpad(combs(pair_num,2),2)]);
        
        cd(rule)
        load threshold threshold
        cd(lfp2dir)
        t = threshold{pair_num}(1);
        
        load(chpair)
        
        binary_corrcoefs = [];
        for r = 1 : size(corrcoefs,1)
            for ccc = 1: size(corrcoefs,2)
                if abs(corrcoefs(r,ccc)) >= t
                    binary_corrcoefs(r,ccc) = 1;
                else
                    binary_corrcoefs(r,ccc) = 0;
                end
            end
        end
        
        cd ../..
        time(:,1) = [1:13]';
        
        counter = 0;
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        for q = total_trials
            counter = counter + 1;
            x = binary_corrcoefs(:,q);
            response(:,1) = x((22:34));
            response(:,2) = ones(1,13);
            
            B = glmfit(time,response,'binomial','link','logit');
            input = (B(1) + time * (B(2)));
            
            %convert to probability
            
            if ~Args.logits
                input = 1 ./ (1 + exp(-input));
            end
            
            trialFits(counter,:) = input;
        end
        trial_fits{pair_num} = trialFits;
        
        cell_coefs = c.data.interaction_coefs{pair_num};
        cell_pvals = c.data.interaction_pval{pair_num};
        i_pval = c.data.Index(pair_num,30);
        
        for all_coefs = 1:size(cell_coefs,1)
            coefs(all_coefs,1) = str2double(cell_coefs(all_coefs));
            pvals(all_coefs,1) = str2double(cell_pvals(all_coefs));
        end
        
        res(:,1) = coefs(1) + time*coefs(2);
        res(:,2) = coefs(1) + time*coefs(2) + coefs(3) + time* coefs(11);
        res(:,3) = coefs(1) + time*coefs(2) + coefs(4) + time *coefs(12);
        res(:,4) = coefs(1) + time*coefs(2) + coefs(5) + time *coefs(13);
        res(:,5) = coefs(1) + time*coefs(2) + coefs(6) + time * coefs(14);
        res(:,6) = coefs(1) + time*coefs(2) + coefs(7) + time * coefs(15);
        res(:,7) = coefs(1) + time*coefs(2) + coefs(8) + time * coefs(16);
        res(:,8) = coefs(1) + time*coefs(2) + coefs(9) + time *coefs(17);
        res(:,9) = coefs(1) + time*coefs(2) + coefs(10) + time* coefs(18);
        
        if ~Args.logits
            res = 1 ./ (1 + exp(-res));
        end
        
        pair_fits{pair_num} = res';
    end
    
    int_pvals = c.data.Index(:,30);
    %saved as probabilities
    
    if Args.train
        trial_fits_train = trial_fits;
        pair_fits_train = pair_fits;
        save glm_fits_train pair_fits_train trial_fits_train int_pvals
    else
        save glm_fits pair_fits trial_fits int_pvals
    end
else
    cd ../..
    mt = mtstrial('auto','ML','RTfromML');
    if Args.train
        load glm_fits_train
        pair_fits = pair_fits_train;
        trial_fits = trial_fits_train;
    else
        load glm_fits
    end
end

if Args.stims
    %get stimulus info
    locs = unique(mt.data.CueLoc);
    objs = unique(mt.data.CueObj);
    stim = 0;
    for object = 1:3
        %Run for all locations
        for location = 1:3
            stim = stim+1;
            ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object));
            stimulus(ind) = stim;
        end
    end
    [i ii iii] = find(stimulus);
    stimulus = iii;
    save stimulus stimulus
else
    load stimulus stimulus
end

if Args.stim_prediction
    
        npairs = size(pair_fits,2);
        numb_trials = size(trial_fits{1},1);

    for t = 1:numb_trials
        cross_cors = [];
        if Args.euclid
            for timepoints = 1 : 13
                %find pairs with sig interaction
                sigpairs = find(int_pvals(1:npairs))'; %only look at identity trials (int_pvals contains both rules)
                for p = sigpairs
                    pair_timepoints(p) = trial_fits{p}(t,timepoints); %get each trial time point for all pairs
                    pfits_timepoints(:,p) = pair_fits{p}(:,timepoints); %get 9 stims for each time point for all pairs
                end
                for allstims = 1:9
                    stim_euclid(allstims,timepoints) = sqrt(sum(((pair_timepoints - pfits_timepoints(allstims,:)).^2))); %get euclidean distance of each trial from all pairs for 9 stims
                end
            end
            
            s = sqrt(sum(stim_euclid.^2,2));
            
            [i ii] = min(s);
            stim_prediction(t) = ii;
            stim_corr(t) = i;
            
        end
        
        if Args.xcor
            for p = 1 : npairs
                
                all_trial = trial_fits{p};
                trial = all_trial(t,:);
                
                for s = 1:9
                    pfits = pair_fits{p};
                    
                    if int_pvals(p) > Args.p_cutoff
                        cross_cors(s,p) = 0;
                    else
                        cross_cors(s,p) = xcorr(trial',pfits(s,:)',0,'coef');
                    end
                end
            end
            
            [i ii] = max(nansum(cross_cors,2));
            stim_prediction(t) = ii;
            stim_corr(t) = i;
        end
        
        %get second guess
        %         cross_cors(ii,:) = 0;
        %         [i ii] = max(nansum(cross_cors,2));
        %         second_stim_prediction(t) = ii;
        %         second_stim_corr(t) = i;
        %
        %         %get third guess
        %         cross_cors(ii,:) = 0;
        %         [i ii] = max(nansum(cross_cors,2));
        %         third_stim_prediction(t) = ii;
        %
    end
    if Args.train
        stim_prediction_train = stim_prediction;
        stim_corr_train = stim_corr;
        save stim_prediction_train stim_prediction_train stim_corr_train
    else
        save stim_prediction stim_prediction stim_corr
    end
else
    if Args.train
        load stim_prediction_train
        stim_prediction = stim_prediction_train
        stim_corr = stim_corr_train
    else
        load stim_prediction
    end
end

numb_trials = size(trial_fits{1},1);

total_matches = zeros(1,9);
matches_loc = 0;
matches_obj = 0;
ch_matches = 0;
total_stims = zeros(1,9);
matches = 0;
m = zeros(1,numb_trials);

% %                 cut_off = (round(numb_trials * .75));
% %                 cut_off:numb_trials

for x = (round(numb_trials*.7)) : numb_trials
    
    locs = [1,2,3,1,2,3,1,2,3];
    objs = [1,1,1,2,2,2,3,3,3];
    
    predict_loc(x) = locs(stim_prediction(x)); %predicted location
    predict_obj(x) = objs(stim_prediction(x));
    
    stim_loc(x) = locs(stimulus(x));%actual location
    stim_obj(x) = objs(stimulus(x));
    
    stx = stimulus(x);
    if stimulus(x) == stim_prediction(x)
        matches = matches + 1;
        total_matches(stx) = total_matches(stx) + 1;
        m(x) = 1;
    end
    total_stims(stx) = total_stims(stx) + 1;
    % % %     %second prediction
    % %         if (stimulus(x) == second_stim_prediction(x))
    % %             matches = matches + 1;
    % %             total_matches(stx) = total_matches(stx) + 1;
    % %         end
    if predict_loc(x) == stim_loc(x)
        matches_loc = matches_loc + 1;
    end
    if predict_obj(x) == stim_obj(x)
        matches_obj = matches_obj + 1;
    end
    c_stims = randperm(9);
    if stimulus(x) == c_stims(1)
        ch_matches = ch_matches + 1;
    end
end


stim_percents = (matches / numb_trials) * 100;
loc_percents = (matches_loc / numb_trials) * 100;
obj_percents = (matches_obj / numb_trials) * 100;
chance = (ch_matches / numb_trials) * 100








