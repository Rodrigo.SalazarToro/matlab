function reject_filter

%run this after:
%-Artifact rejection (do not reject any channels)
%-Sorting is complete (only sorted groups will be conisdered)
%-Power below 5Hz has been calculated (lowfreq_power.m). 
%-nlynx_lost_records.m

%This function will rewrite rejectedTrials.mat to only reject trials from
%channels that are sorted and have a 5Hz power below the established cut off without any lost records.
%A copy of the original rejectedTrials.mat will be saved as
%rejectedTrials_old.mat.

%run at session level

sesdir = pwd;
N = NeuronalHist('bmf');

%load trials that have lost records;
load lost_trials %created with nlynx_lost_records.m

%get list of sorted groups
g = nptDir('group*');

cd([sesdir filesep 'lfp'])
load rejectedTrials PWTrials rejecTrials rejectCH stdTrials transTrials
save rejectedTrials_old PWTrials rejecTrials rejectCH stdTrials transTrials

load POWER_channels %this is the power for below 5Hz, written with lowfreq_power
p = channel_power_list(:,2) ./ 1000000;

%edit rejectCH to reflect the sorting and 5Hz power
good_ch = [];
for ng = 1 : size(g,1)
    [~,ii]= intersect(single(N.gridPos),single(str2double(g(ng).name(end-3:end))));
    if p(ii) < .5
        good_ch = [good_ch ii];
    end
end
total_ch = [1 : size(N.gridPos,2)];

rej = setdiff(total_ch,good_ch)';

rejectCH = sort(unique([rej ; rejectCH])); %new rejectCH is a combination of noisy and unsorted channels

%only reject trials from good channels and when lost records are present
rejecTrials = sort(unique([cell2mat(PWTrials(good_ch)') cell2mat(stdTrials(good_ch)') cell2mat(transTrials(good_ch)') lost_trials]));

save rejectedTrials PWTrials rejecTrials rejectCH stdTrials transTrials


cd(sesdir)








