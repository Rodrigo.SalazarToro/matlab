function [NeuroInfo,varargout] = NeuronalChAssign(varargin)
% to be run in the session directory
% output: structure NeuroInfo with field:
%
% groups : groups #
% channels : streamer channels #
% recordedDepth : depth into the cortex
% gridPos : location # in the grid
% cortex : either P for parietal or F for prefrontal based
%          on the logic groups <= 8 parietal
%          groups > 8 prefronnal
%
% Dependencies: nptDir, ReadDescriptor.

descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file(end).name);

neuronalCh = find(descriptor_info.group ~= 0);


NeuroInfo.groups = descriptor_info.group(neuronalCh);
NeuroInfo.channels = descriptor_info.channel(neuronalCh);

NeuroInfo.gridPos = descriptor_info.grid(neuronalCh);

if strmatch('pres_trig',descriptor_info.description)
    
    chsep = 8;% for streamer data
    NeuroInfo.recordedDepth = descriptor_info.recdepth(neuronalCh) - descriptor_info.startdepth(neuronalCh);
    bor = {'<=' '>'};
else
    chsep = 32;% for DAQ data
    NeuroInfo.recordedDepth = descriptor_info.recdepth(neuronalCh);
     bor = {'>' '<='};
end
for ch = 1 : length(neuronalCh)

    if eval(sprintf('NeuroInfo.groups(ch) %s chsep',bor{1}))
        NeuroInfo.cortex(ch) = 'P';
    elseif eval(sprintf('NeuroInfo.groups(ch) %s chsep',bor{2}))
        NeuroInfo.cortex(ch) = 'F';
    end

end

PPch = findstr('P',NeuroInfo.cortex); if ~isempty(PPch) & length(PPch) > 1; PPcomb = nchoosek(PPch,2); else PPcomb = []; end
PFch = findstr('F',NeuroInfo.cortex); if ~isempty(PFch) & length(PFch) > 1; PFcomb = nchoosek(PFch,2); else PFcomb = []; end

for out = 1 : size(varargin,2)
    if strcmp(varargin{out},'onlyPP'); varargout{out} = PPch; end

    if strcmp(varargin{out},'onlyPF'); varargout{out} = PFch; end
    if strcmp(varargin{out},'combPP'); varargout{out} = PPcomb; end

    if strcmp(varargin{out},'combPF'); varargout{out} = PFcomb; end
    if strcmp(varargin{out},'combInter')
        if isempty(PPch) | isempty(PFch)
            varargout{out} = [];
        else
            allcomb = nchoosek([PPch PFch],2);
            if ~isempty(PPcomb)
                varargout{out} = setdiff(allcomb,PPcomb,'rows');
            else
                varargout{out} = allcomb;
            end
            if ~isempty(PFcomb)
                varargout{out} = setdiff(varargout{out},PFcomb,'rows');
            end
        end
    end
end
