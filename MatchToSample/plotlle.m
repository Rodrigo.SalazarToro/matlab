function plotlle(k,stims)

load all_pfits_timepoints
load all_t_timepoints

all_t = all_t_timepoints';
y_all_t = lle(all_t,k,3);

points = 13;
colors = [0 0 1;1 0 0; .75 .5 1; .5 1 1;1 0 1; 1 0 .5; .5 0 0; 0 .5 0; 0 1 .75];
lwidths = [1:points].*1.1;

%c = 0;
for xx = stims 
    %c = c + 1;
    %subplot(2,2,c)
    pf = all_pfits_timepoints(:,:,xx)';
    y = lle(pf,k,3)
    for qq = 1 : (points - 1)
        
        plot3(y(1,(qq:qq+1)),y(2,(qq:qq+1)),y(3,(qq:qq+1)),'linewidth',lwidths(qq),'color',colors(xx,:));
        hold on
    end
    
    for q = 1:13
        scatter3(y(1,q),y(2,q),y(3,q),30,'k','filled');
        hold on
    end
    
    xlabel('lle1')
    ylabel('lle2')
    zlabel('lle3')
    axis square
    
    
    xc(xx) = sum(sum((y-y_all_t).^2));
end
% 
% hold on
% 
% for qq = 1 : (points - 1)
%     
%     plot3(y_all_t(1,(qq:qq+1)),y_all_t(2,(qq:qq+1)),y_all_t(3,(qq:qq+1)),'color','k');
%     hold on
% end
% 
% for q = 1:13
%     scatter3(y(1,q),y(2,q),y(3,q),30,'k','filled');
%     hold on
% end

xlabel('lle1')
ylabel('lle2')
zlabel('lle3')
axis square

xc