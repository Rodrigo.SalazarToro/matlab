function depth_phase_correlation


%run at session level
sesdir = pwd;

%notes: consider with respect to 1 channel.

N = NeuronalHist;
%cpp = mtscpp('ml','auto');
load cpp

cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'identity'])

load avg_correlograms_epochs

%get phase information
p = cell2mat(avg_phase_epochs);
all_phase = p(4,:); %get epoch4 phases

%determine if negative or positive peak
c = cell2mat(avg_corrcoef_epochs);
all_coef = c(4,:);

%find identity parietal-parietal pairs
pp_pairs = intersect(find(cpp.data.Index(:,1)),find(cpp.data.Index(:,4) == 1))'


all_phase(find(all_coef < 0)) = all_phase(find(all_coef < 0)) + 20; %add 180 to all negative coefs

%radians = deg * 180/pi
%all_phase = all_phase * (180/pi);
phases = all_phase(pp_pairs);

%get chnumbs
ch_pairs = cpp.data.Index(pp_pairs,(23:24));
ch1 = ch_pairs(:,1)';

%find hist number
h_numbers = cpp.data.Index(pp_pairs,(7:8))
%find depths
depths = cpp.data.Index(pp_pairs,(5:6));
%calculate euclidean distance of each pair
numbpairs = size(depths,1);
medial = [8 9 10 12 14];
lateral = [11 13];

for x = 1 : numbpairs
    hnumbs = h_numbers(x,:);
    deps = depths(x,:);
    if ~isempty(intersect(hnumbs(1),medial)) && ~isempty(intersect(hnumbs(2),medial)) %both medial
        dist(x) = abs(diff(deps));
    elseif ~isempty(intersect(hnumbs(1),lateral)) && ~isempty(intersect(hnumbs(2),lateral)) %both lateral
        dist(x) = abs(diff(deps));
    else
        %subgract max depth
        deps = max(max(depths)) - deps;
        %add 1 mm for sulcus
        dist(x) = sum(deps) + 1; 
    end
    if dist(x) < 0
        x
    end
    
end


scatter(dist,phases)

cd(sesdir)

% % uch1 = unique(ch1);
% % 
% % numbch = size(uch1,2);
% % 
% % for x = 1 : numbch
% %     subplot(3,4,x)
% %     n = find(ch1 == uch1(x));
% %     scatter(dist(n), phases(n))
% % end
% %     

