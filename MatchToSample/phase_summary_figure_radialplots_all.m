function phase_summary_figure_radialplots_all

%plots bar graph for all major combinations

cd('/media/raid/data/monkey/betty')

xcorr_th = .99; % %xcthresh = unimodal_thresh_corrcoef
npairs = 0;

load cpp_b


% allareas = [1 : 5, 8 : 13];
% c = 0;
% for x = allareas
%     c = c + 1;
%     counter = 0;
%     allrose = [];
%     area1 = cell(1);
%     area2 = cell(1);
%     for xx = allareas
%         [total,ii] = get(cpp_b,'Number','ml','avg_corr_pval',.0001,'cross_corr',xcorr_th,'hist_numbers',[x xx]);
%
%         if total > npairs
%             counter = counter + 1;
%             allrose{counter} = cpp_b.data.Index(ii,54) .* (pi/180); %get phase angles for delay 200 : 800
%             h1 = reverse_categorizeNeuronalHist(x);
%             h2 = reverse_categorizeNeuronalHist(xx);
%             area1{counter} = sprintf('%s \\ %s %d',h1{1},h2{1},total);
%         end
%     end
%     nplots = size(allrose,2);
%     figure
%     for np = 1 : nplots
%         subplot(3,4,np)
%         rose(allrose{np})
%         title(area1{np})
%     end
% end


%pool parietal areas for each frontal area
for x = 1:5
    allrose = [];
    area1 = cell(1);
    total = 0;
    ii = [];
    for xx = 8:13
        [t,i] = get(cpp_b,'Number','ml','avg_corr_pval',.0001,'cross_corr',xcorr_th,'hist_numbers',[x xx]);
        total = total + t;
        ii = [ii i];
    end
    
    if total > npairs
        allrose = cpp_b.data.Index(ii,54) .* (pi/180); %get phase angles for delay 200 : 800
        h1 = reverse_categorizeNeuronalHist(x);
        area1 = sprintf('%s \\ %s %d',h1{1},'parietal',total);
    end
    
    figure
    rose(allrose)
    title(area1)
end


%pool frontal areas for each parietal area
for x = 8:13
    allrose = [];
    area1 = cell(1);
    total = 0;
    ii = [];
    for xx = 1:5
        [t,i] = get(cpp_b,'Number','ml','avg_corr_pval',.0001,'cross_corr',xcorr_th,'hist_numbers',[x xx]);
        total = total + t;
        ii = [ii i];
    end
    
    if total > npairs
        allrose = cpp_b.data.Index(ii,54) .* (pi/180); %get phase angles for delay 200 : 800
        h1 = reverse_categorizeNeuronalHist(x);
        area1 = sprintf('%s \\ %s %d',h1{1},'frontal',total);
    end
    
    figure
    rose(allrose)
    title(area1)
end





