function [cind1,cind2,varargout] = comparePsthandcohObjs(psth,ind1,coh,ind2,varargin)

Args = struct();
Args.flags = {};


c1 = 1;
for ii = ind1
    
    daydir = psth.data.setNames{ii}(1:findstr('session0',psth.data.setNames{ii}) -2);
    
    starst = findstr(psth.data.setNames{ii},'group00');
    grn = str2double(psth.data.setNames{ii}(starst+6:starst+8));
    
    cohinds = find(cellfun(@isempty,strfind(coh.data.setNames(ind2),daydir)) == 0);
    [grinds,col] = find(coh.data.Index(ind2(cohinds),[10 : 11]) == grn);
    
    if ~isempty(grinds)
    cind1(c1) = ii;
    cind2{c1} = cohinds(grinds);
    c1 = c1 + 1;
    else
        
%         keyboard
    end
    
end
    