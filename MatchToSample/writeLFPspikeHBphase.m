function writeLFPspikeHBphase(varargin)
% in the cluster folder

Args = struct('iterations',1000,'maxjitter',125,'timeFrame','saccade','redo',0,'reduce',0,'shiftPredictor',[],'addFile',[]);
Args.flags = {'redo','reduce'};
[Args,modvarargin] = getOptArgs(varargin,Args);

cldir = pwd;

load ispikes
unit = ['g' sp.data.groupname 'c' sp.data.cellname];

mkdir('HBphase')
cd HBphase
sdir = pwd;

cd ../../..

mts = mtstrial('auto');
switch Args.timeFrame
    case 'presample'
        timing = [mts.data.CueOnset - 400 mts.data.CueOnset];
    case 'sample'
        timing = [mts.data.CueOffset - 400 mts.data.CueOffset];
    case 'delay1'
        timing = [mts.data.CueOffset mts.data.CueOffset + 400];
    case 'delay2'
        timing = [mts.data.MatchOnset - 400 mts.data.MatchOnset];
        
    case 'saccade'
        timing = [mts.data.MatchOnset mts.data.MatchOnset + mts.data.FirstSac];
end


cd lfp/mband
cdir = pwd;
files = nptDir('*mband.*');

beta = load(files(1).name,'-mat');
rateSR = 1000 / beta.SR;
f = beta.f;
jitters = [-round(Args.maxjitter/rateSR):1:round(Args.maxjitter/rateSR)];
if  ~isempty(Args.shiftPredictor)
    cd(sdir)
    matfile = regexprep(sp.data.sessionname,'highpass',[Args.timeFrame 'ShiftPre' Args.addFile '.mat']);
    if isempty(nptDir(matfile)) || Args.redo
        phases = [];
        for ii = 1 : Args.iterations
            
            randff = randperm(length(Args.shiftPredictor));
            
            if isempty(nptDir(matfile)) || Args.redo
                for ff = 1 : length(Args.shiftPredictor)
                    
                    spikes = sp.data.trial(Args.shiftPredictor(ff)).cluster.spikes;
                    
                    selspikes = find(spikes > timing(Args.shiftPredictor(randff(ff)),1) & spikes < timing(Args.shiftPredictor(randff(ff)),2)); % the limit is set on th erandomized trials
                    cd(sdir)
                    
                    if ~isempty(selspikes)
                        
                        cd(cdir)
                        beta = load(files(Args.shiftPredictor(randff(ff))).name,'-mat');
                        phases = cat(2,phases,single(angle(beta.hb(:,floor(spikes(selspikes)/rateSR),:))));
                    end
                end
            end
        end
        cd(sdir)
        save(matfile,'phases','f','unit')
        display(['saving' pwd '/' matfile])
    end
else
    for ff = 1 : length(files)
        matfile = regexprep(files(ff).name,'mband',[Args.timeFrame 'HBphase']);
        spikes = sp.data.trial(ff).cluster.spikes;
        
        selspikes = find(spikes > timing(ff,1) & spikes < timing(ff,2));
        cd(sdir)
        if isempty(nptDir(matfile)) || Args.redo
            if isempty(selspikes)
                phases = nan;
                f=nan;
            else
                cd(cdir)
                
                
                
                beta = load(files(ff).name,'-mat');
                phaset = single(angle(beta.hb(:,ceil(spikes(selspikes)/rateSR),:)));
                phases = zeros(Args.iterations+1,size(phaset,1),length(selspikes),size(phaset,3));
                for ii = 1 : Args.iterations
                    randt = randi(length(jitters),[length(selspikes),1]);
                    iispikes = ceil(spikes(selspikes)/rateSR) + jitters(randt);
                    while max(iispikes) > size(beta.hb,2) || min(iispikes) <= 0
                        randt = randi(length(jitters),[length(selspikes),1]);
                        iispikes = ceil(spikes(selspikes)/rateSR) + jitters(randt);
                    end
                    phases(ii,:,:,:) = single(angle(beta.hb(:,iispikes,:)));
                end
                phases(ii+1,:,:,:) = phaset;
                
                %                         subplot(2,1,1);
                %                         plot(beta.prelfp(ch,:))
                %                         hold on
                %                         plot(ceil(spikes(selspikes)/5),100*ones(length(selspikes),1),'r*')
                %                         subplot(2,1,2)
                %
                %                         plot(angle(beta.hb(ch,:)))
                %                         hold on
                %                         plot(ceil(spikes(selspikes)/5),peaks{obj,loc}(1,:),'+')
                %                         plot(ceil(spikes(selspikes)/5),peaks{obj,loc}(2,:),'+')
                %                         pause
                %                         clf
            end
            cd(sdir)
            save(matfile,'phases','f','unit')
            display(['saving' pwd '/' matfile])
        elseif Args.reduce && ~isempty(nptDir(matfile))
            load(matfile,'-mat')
            if ~isreal(phases)
                phases = single(angle(phases));
                save(matfile,'phases','f','unit')
                display(['saving' pwd '/' matfile])
            end
        end
    end
end
cd(cldir)