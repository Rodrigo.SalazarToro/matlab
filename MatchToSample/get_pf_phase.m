function get_pf_phase

monkeydir = pwd;
load idedays

all_pf_phases = [];
all_pp_phases = [];
all_ff_phases = [];
for x = 1 : 27
    cd([monkeydir filesep idedays{x} filesep 'session01'])
    
    c = mtscpp('auto','ml','ide_only');
    
    [i,ii]=get(c,'Number','identity','corrrect','stable','pf');
    h = c.data.Index(ii,27);
    h(isnan(h)) = [];
    all_pf_phases = [all_pf_phases h'];
    
    [i,ii]=get(c,'Number','identity','corrrect','stable','ff');
    h = c.data.Index(ii,27);
    h(isnan(h)) = [];
    all_ff_phases = [all_ff_phases h'];
    
    [i,ii]=get(c,'Number','identity','corrrect','stable','pp');
    h = c.data.Index(ii,27);
    h(isnan(h)) = [];
    all_pp_phases = [all_pp_phases h'];
end

cd(monkeydir)

save all_phases all_pf_phases all_pp_phases all_ff_phases