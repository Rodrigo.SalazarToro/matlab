function writeClusterPhases(trials,varargin)
% to be run into the HBphases dir

Args = struct('redo',0,'type','saccade','addFile',[],'prctile',[95 99 99.9 99.99]);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);



matfile = [Args.type 'Phases' Args.addFile '.mat'];




if isempty(nptDir(matfile)) || Args.redo
    files = nptDir(['*_' Args.type '*']);
    nch = 1;
    ct = 1;
    phases = ones(2,1);
    while nch == 1 && size(phases,1) ~= 1
        load(files(trials(ct)).name,'-mat')
        nch = size(phases,2);
        ct = ct +1;
    end
    
    thr =nan(nch,10,80,4);
    rout = nan(nch,10,80);
    tout = nan;
    for ch = 1 : nch
        for fi =1 : 10
            allphases = [];
            for tr = 1 : length(trials)
                load(files(trials(tr)).name,'-mat')
                % size of phases iteration x channels x Nspikes x freq
                c=1;
                
                if size(phases,3) > 1
                    
                    allphases = cat(2,allphases,squeeze(phases(:,ch,:,fi)));
                elseif size(phases,3) == 1 && unique(isnan(phases)) == 0
                    tphases = zeros(size(phases,1),1);
                    tphases(:,1) = squeeze(phases(:,ch,:,fi));
                    allphases = cat(2,allphases,tphases);
                end
                
            end
            
            if ~isempty(allphases)
                
                
                [tout,rout(ch,fi,:)] = rose(allphases(1001,:));
                surrogate = zeros(1000,80);
                for ii = 1 : 1000
                    [~,surrogate(ii,:)] = rose(allphases(ii,:));
                    
                end
                clear allphases phases
                for pv = 1 : length(Args.prctile)
                    thr(ch,fi,:,pv) = prctile(surrogate,Args.prctile(pv),1);
                end
                %                 sig = rout > thr;
                %                 allrout(ch,fi,:) = allrout(fi,:) + sig;
            end
        end
        
        %             for fi =1 : 10
        %                 subplot(2,5,fi)
        %                 rose(allphases(ch,:,fi))
        % %                 title(['freq ' num2str(f(fi))])
        %             end
        %             set(gcf,'Name',[pwd 'LFPgr' num2str(neuroinfo.groups(ch))])
        %             pause
        %             clf
        
        
    end
    save(matfile,'rout','thr','tout')
    display(['saving ' pwd '/' matfile])
    
end