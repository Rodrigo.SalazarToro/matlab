
% 1:'6DR ' 2:'8AD ' 3:'8B  ' 4:'dPFC ' 5:'vPFC ' 6:'PS  ' 7:'AS  ' 8:'PEC ' 9:'PGM ' 10:'PE  ' 11:'PG  ' 12:'MIP ' 13:'LIP ' 14:'PEcg' 15:'IPS ' 16:'WM ' 17:'9L  '};

cd D:\workingmemory\data\monkey
clear all

LOCobj = loadObject('mtsXCobjLOC.mat');
IDEobj = loadObject('mtsXCobjIDE.mat');
[sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);
obj1 = IDEobj;

pairs = unique(obj1.data.hist(sindIDE,:),'rows');

gr1 = [4 11 13];
gr2 = [2 3 8 10];
allgr = [2 3 4 8 10 11 13];
srow = find(sum(ismember(pairs,gr1),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);

% [sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins] = plotIncXcorrSpikes(obj1,ind,'plot','append','epoch');
% sigPos(rule x count x align x epochs)
[sigPosgr1,sigNeggr1] = xcorrSpikeSummary(obj1,ind,'name','rule network');

srow = find(sum(ismember(pairs,gr2),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
[sigPosgr2,sigNeggr2] = xcorrSpikeSummary(obj1,ind,'name','non-specific network');
clear sigPRule sigPNon
% sigPRule{1} = squeeze(sum(sigPosgr1,2));
% sigPRule{2} = squeeze(sum(sigNeggr1,2));
% sigPNon{1} = squeeze(sum(sigPosgr2,2));
% sigPNon{2} = squeeze(sum(sigNeggr2,2));

sigPRule = sigPosgr1;

sigPNon = sigPosgr2;



t1 = sigPRule{1};
% p = anova2(squeeze(sigNRule(:,ref,:)))
t2 = sigPRule{2};
nd1 = reshape(t1,1,size(t1,1)*size(t1,2));
nd2 = reshape(t2,1,size(t2,1)*size(t2,2));

t1 = sigPNon{1};
% p = anova2(squeeze(sigNRule(:,ref,:)))
t2 = sigPNon{2};
nd3 = reshape(t1,1,size(t1,1)*size(t1,2));
nd4 = reshape(t2,1,size(t2,1)*size(t2,2));

grs{1} = [repmat(1,1,length(nd1)) repmat(2,1,length(nd2)) repmat(1,1,length(nd3)) repmat(2,1,length(nd4))];% rule
grs{2} = repmat([1 :4],1,length(nd1)/2+ length(nd3)/2);% epoch
grs{3} = [ones(1,2*length(nd1)) ones(1,2*length(nd3))+1]; % network
p = anovan([nd1 nd2 nd3 nd4],grs,'display','off');
pp{1} = p;


% tc = squeeze(sum(sigPosgr1,1));
% nsigSpikeCountSpecN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
% 100*nsigSpikeCountSpecN/size(tc,1)
%
% tc = squeeze(sum(sigPosgr2,1));
% nsigSpikeCountNoN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
% 100*nsigSpikeCountNoN/size(tc,1)
%
% tc = squeeze(sum(sigNeggr1,1));
% nsigSpikeCountSpecN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
% 100*nsigSpikeCountSpecN/size(tc,1)
%
% tc = squeeze(sum(sigNeggr2,1));
% nsigSpikeCountNoN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
% 100*nsigSpikeCountNoN/size(tc,1)

srow = find(sum(ismember(pairs,allgr),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
[sigPosall,sigNegall] = xcorrSpikeSummary(obj1,ind,'name','all areas','plot');
%% coherence distribution
srow = find(sum(ismember(pairs,allgr),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
[sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins,newind] = plotIncXcorrSpikes(obj1,ind,'plot','append','epoch','falseRate',[0 0],'prctileCoh',[.05 99.95],'prctileRule',[.5 99.5],'pvalueCoh',10^-10);
% sigPos(rule,ind,align,epoch)
al = [1 1; 1 2; 1 3; 2 3];
cohspec = cell(2,4);
cohsig = cell(2,4);
sfr = cell(2,4);
pvalues = cell(2,4);
for rrr = 1 : 2
    for ep = 1 : 4
        %         ssind = find(sigPos(rrr,:,al(ep,1),al(ep,2)) == 1);
        ssind = find(sigPosall{rrr}(:,ep) < 0.05);
        for sii = 1 : length(ssind)
            %             sssind = newind(ssind(sii));
            sssind = ind(ssind(sii));
            thefile = sprintf('xcorrg%04.0f0%d%sg%04.0f0%d%sRule%d.mat', obj1.data.Index(sssind,3),obj1.data.Index(sssind,9),obj1.data.Index(sssind,7),obj1.data.Index(sssind,4),obj1.data.Index(sssind,10),obj1.data.Index(sssind,8),rrr);
            if isempty(nptDir([obj1.data.setNames{sssind} filesep thefile]))
                thefile = sprintf('xcorrg%04.0f0%d%sg%04.0f0%d%sRule%d.mat', obj1.data.Index(sssind,4),obj1.data.Index(sssind,10),obj1.data.Index(sssind,8),obj1.data.Index(sssind,3),obj1.data.Index(sssind,9),obj1.data.Index(sssind,7),rrr);
                if isempty(nptDir([obj1.data.setNames{sssind} filesep thefile]))
                    thefile =  thefile;
                    if isempty(nptDir([regexprep(obj1.data.setNames{sssind},'session99','session01') filesep thefile]))
                        thefile = sprintf('xcorrg%04.0f0%d%sg%04.0f0%d%sRule%d.mat', obj1.data.Index(sssind,3),obj1.data.Index(sssind,9),obj1.data.Index(sssind,7),obj1.data.Index(sssind,4),obj1.data.Index(sssind,10),obj1.data.Index(sssind,8),rrr);
                        
                    end
                end
            end
            try
                d=load([obj1.data.setNames{sssind} filesep thefile],'S');
            catch
                
                try
                    d=load([regexprep(obj1.data.setNames{sssind},'session99','session01') filesep thefile],'S');
                catch
                    display([regexprep(obj1.data.setNames{sssind},'session99','session01') filesep thefile])
                end
            end
            if ~isempty(d.S)
                try
                    surfile = [thefile(1:end-4) 'Surtrial.mat'];
                    sur=load([obj1.data.setNames{sssind} filesep surfile],'S');
                    cohspec{rrr,ep} = [cohspec{rrr,ep}; d.S{ep}];
                    if ~isempty(sur.S{ep}) && sum(isnan(sur.S{ep})) < 5
                        cohsig{rrr,ep} = [cohsig{rrr,ep}; d.S{ep} > prctile(sur.S{ep},99)];
                        thepvalue = ones(100,1);
                        for fq = 1 : 100
                            thepvalue(fq) = pvalueEstimateFromShuffle(sur.S{ep}(:,fq),d.S{ep}(fq));
                        end
                        pvalues{rrr,ep} = [pvalues{rrr,ep}; thepvalue];
                        
                    end
                    if rrr == 1
                        sfr{rrr,ep} = [sfr{rrr,ep}; IDEobj.data.fr(sssind,:)];
                        
                    else
                        sfr{rrr,ep} = [sfr{rrr,ep}; LOCobj.data.fr(sssind,:)];
                    end
                catch
                    try
                        surfile = [thefile(1:end-4) 'Surtrial.mat'];
                        sur=load([regexprep(obj1.data.setNames{sssind},'session99','session01') filesep surfile],'S');
                    catch
                        
                        [pwd '/' surfile]
                    end
                    cohspec{rrr,ep} = [cohspec{rrr,ep}; d.S{ep}];
                    if ~isempty(sur.S) && ~isempty(sur.S{ep})
                        cohsig{rrr,ep} = [cohsig{rrr,ep}; d.S{ep} > prctile(sur.S{ep},99)];
                        thepvalue = ones(100,1);
                        for fq = 1 : 100
                            if ~isnan(d.S{ep}(fq)) && sum(isnan(sur.S{ep}(:,fq))) == 0
                                thepvalue(fq) = pvalueEstimateFromShuffle(sur.S{ep}(:,fq),d.S{ep}(fq));
                            end
                        end
                        pvalues{rrr,ep} = [pvalues{rrr,ep}; thepvalue'];
                        
                    end
                    if ~isempty(sur.S); cohsig{rrr,ep} = [cohsig{rrr,ep}; d.S{ep} > prctile(sur.S{ep},99)];end
                    if rrr == 1
                        sfr{rrr,ep} = [sfr{rrr,ep}; IDEobj.data.fr(sssind,:)];
                        
                    else
                        sfr{rrr,ep} = [sfr{rrr,ep}; LOCobj.data.fr(sssind,:)];
                    end
                end
            end
        end
        
    end
end

%% false discovery rate
fdrp = cell(2,4);

clb = {'r' 'b' 'k' 'g'};

cpvalues = cell(2,4);
for rrr = 1 : 2
    for ep = 1 : 4
        [row,col] = find(sum(pvalues{rrr,ep} ==1,2) ~= 100);
        cpvalues{rrr,ep}= pvalues{rrr,ep}(row,:);
    end
end
% figure
% for rrr = 1 : 2
%     for ep = 1 : 4
%         rpvalues = reshape(1-cpvalues{rrr,ep},size(cpvalues{rrr,ep},1)*size(cpvalues{rrr,ep},2),1);
%         tfdrp = mafdr(rpvalues','BHFDR',true);
%         fdrp{rrr,ep} = reshape(tfdrp',size(cpvalues{rrr,ep},1),size(cpvalues{rrr,ep},2));
%         %         tfdrn = mafdr(1-rpvalues,'BHFDR',true);
%         %         fdrn{rrr,ep} = reshape(tfdrn,size(pvalues{rrr,ep},1),size(pvalues{rrr,ep},2));
%         subplot(2,1,rrr)
%         plot([0:99],100* sum(fdrp{rrr,ep} < 0.01) /size(fdrp{rrr,ep},1),clb{ep})
%         hold on
%     end
% end

fdrp = cell(2,4);

for fq = 1 : 100
    fpvalues = [];
   
    for rrr = 1 : 2
        for ep = 1 : 4
            fpvalues = [fpvalues 1-cpvalues{rrr,ep}(:,fq)'];
            
        end
    end
    tfdrp = mafdr(fpvalues,'BHFDR',true);
     laste = 1;
    for rrr = 1 : 2
        for ep = 1 : 4
            fdrp{rrr,ep}(:,fq) = tfdrp(laste : laste + size(cpvalues{rrr,ep},1) -1);
             laste = laste + size(cpvalues{rrr,ep},1);
        end
    end
end
% tfdrp = mafdr(fpvalues,'BHFDR',true);
% laste = 1;
% for fq = 1 : 100
%     for rrr = 1 : 2
%         for ep = 1 : 4
%             fdrp{rrr,ep}(:,fq) = tfdrp(laste : laste + size(pvalues{rrr,ep},1) -1);
%             laste = laste + size(pvalues{rrr,ep},1);
%         end
%     end
% end

clb = {'r' 'b' 'k' 'g'};
figure
for rrr = 1 : 2
    for ep = 1 : 4
        subplot(2,1,rrr)
        plot([1:100],100* sum(fdrp{rrr,ep} < 0.001) /size(fdrp{rrr,ep},1),clb{ep})
       hold on
        text(50,80-ep*10,['n=' num2str(size(cohsig{rrr,ep},1))])
    end
end
xlabel('Frequency (Hz)')
ylabel('% pairs')
legend('pre-sample','sample','delay1','delay2')
ruleslab = {'IDE','LOC'};
for rrr = 1 : 2
    subplot(2,1,rrr)
    title(ruleslab{rrr})
    line([7 7],[0 100])
    xlim([0 100])
    ylim([0 85])
    set(gca,'TickDir','out')
end
%%
figure
clb = {'r' 'b' 'k' 'g'};
for rrr = 1 : 2
    for ep = 1 : 4
        subplot(2,1,rrr)
        plot([0:100],100*sum(cohsig{rrr,ep})/size(cohsig{rrr,ep},1),clb{ep})
        hold on
        text(50,80-ep*10,['n=' num2str(size(cohsig{rrr,ep},1))])
    end
end
xlabel('Frequency (Hz)')
ylabel('% pairs')
legend('pre-sample','sample','delay1','delay2')
ruleslab = {'IDE','LOC'};
for rrr = 1 : 2
    subplot(2,1,rrr)
    title(ruleslab{rrr})
    line([7 7],[0 100])
    xlim([0 100])
    ylim([0 85])
    set(gca,'TickDir','out')
end
allfr = cell(2,1);
tilesb = cell(2,1);
for rrr = 1 : 2
    for ep = 1 : 4
        allfr{rrr} =[sfr{rrr,ep}(:,3:4); sfr{rrr,ep}(:,3:4)];
        
    end
    allfr{rrr} = median(allfr{rrr},2);
    tilesb{rrr} = prctile(allfr{rrr},[0 33 66 100],1);
end
tiles = [0 33; 33 66; 66 100];
for tile = 1 : 3
    figure
    set(gcf, 'Name',sprintf('Percentile %d of fr',tile))
    clb = {'r' 'b' 'k' 'g'};
    for rrr = 1 : 2
        for ep = 1 : 4
            subplot(2,1,rrr)
            sind = find((median(sfr{rrr,ep}(:,3:4),2) >= tilesb{rrr}(tile)) & (median(sfr{rrr,ep}(:,3:4),2) <= tilesb{rrr}(tile+1)));
            if ep ==4; sind = sind(1:end-1); end
%             plot([0:100],100*sum(cohsig{rrr,ep}(sind,:))/size(cohsig{rrr,ep}(sind,:),1),clb{ep})
             plot([0:100],100*sum(fdrp{rrr,ep}(sind,:))/size(fdrp{rrr,ep}(sind,:),1),clb{ep})
            hold on
            text(50,80-ep*10,['n=' num2str(size(cohsig{rrr,ep},1)) 'nind = ' num2str(length(sind))])
        end
    end
    xlabel('Frequency (Hz)')
    ylabel('% pairs')
    legend('pre-sample','sample','delay1','delay2')
    ruleslab = {'IDE','LOC'};
    for rrr = 1 : 2
        subplot(2,1,rrr)
        title(ruleslab{rrr})
        line([7 7],[0 100])
        xlim([0 100])
        ylim([0 100])
        set(gca,'TickDir','out')
    end
    
end


%%
tc = squeeze(sum(sigNegall,1));
nsigSpikeCountSpecN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
100*nsigSpikeCountSpecN/size(tc,1)

tc = squeeze(sum(sigPosall,1));
nsigSpikeCountNoN = sum(squeeze(sum(tc(:,1,1:3),3)) + squeeze(tc(:,2,2)) >0)
100*nsigSpikeCountNoN/size(tc,1)

keyboard

%% ide or loc tuning for cells
cd /Volumes/raid/data/monkey/
LOCpsth = loadObject('psthLOCswitchObj.mat');
IDEpsth = loadObject('psthIDEswitchObj.mat');

[~,LOCind] = get(LOCpsth,'number');

[~,IDEind] = get(IDEpsth,'number');
ttind = find(ismember(LOCpsth.data.setNames(LOCind),IDEpsth.data.setNames(IDEind),'rows') == 1);
tttind = find(ismember(IDEpsth.data.setNames(IDEind),LOCpsth.data.setNames(LOCind),'rows') == 1);
LOCind = LOCind(ttind);
IDEind = IDEind(tttind);

loccells = [];
idecells = [];
c=1;
for ep = 1 : 2;
    [r,tind]= get(LOCpsth,'Number','locTuned',1,'epochTuned',ep);
    tind  = find(ismember(LOCind,tind) ==1);
    loccells = [loccells tind];
    
    [r,tind]= get(IDEpsth,'Number','locTuned',1,'epochTuned',ep);
    tind  = find(ismember(IDEind,tind) ==1);
    loccells = [loccells tind];
    
    [r,tind]= get(LOCpsth,'Number','ideTuned',1,'epochTuned',ep);
    tind  = find(ismember(LOCind,tind) ==1);
    idecells = [idecells tind];
    
    [r,tind]= get(IDEpsth,'Number','ideTuned',1,'epochTuned',ep);
    tind  = find(ismember(IDEind,tind) ==1);
    idecells = [idecells tind];
end

loccells = unique(loccells);size(loccells);
idecells = unique(idecells);size(idecells);
tunedcells = union(loccells,idecells);


srow = find(sum(ismember(pairs,gr1),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
[xcorrindout1,xcorrindout2] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(tunedcells),obj1,ind);
% [sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins] = plotIncXcorrSpikes(obj1,ind,'plot','append','epoch');
xcorrindout = union(xcorrindout1,xcorrindout2);
xcorrSpikeSummary(obj1,ind(xcorrindout),'name','rule network + tuned cells')


% spN = [1 2 3 10];
% srow = find(sum(ismember(pairs,spN),2) == 2);
% spairs = pairs(srow,:);
% [~,ind] = get(obj1,'Number','hist',spairs);
% ind = intersect(ind,sindIDE);
% [xcorrindout] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(tunedcells),obj1,ind);
% xcorrSpikeSummary(obj1,ind(xcorrindout),'name','non specific network + tuned cells')


% spN = [4 8 11 13];
% srow = find(sum(ismember(pairs,spN),2) == 2);
% spairs = pairs(srow,:);
% [~,ind] = get(obj1,'Number','hist',spairs);
% ind = intersect(ind,sindIDE);
% [xcorrindout] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(loccells),obj1,ind);
% % [sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins] = plotIncXcorrSpikes(obj1,ind,'plot','append','epoch');
% xcorrSpikeSummary(obj1,ind(xcorrindout),'name','rule network + location cells')
%
% spN = [1 2 3 10];
% srow = find(sum(ismember(pairs,spN),2) == 2);
% spairs = pairs(srow,:);
% [~,ind] = get(obj1,'Number','hist',spairs);
% ind = intersect(ind,sindIDE);
% [xcorrindout] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(loccells),obj1,ind);
% xcorrSpikeSummary(obj1,ind(xcorrindout),'name','non specific network + location cells')
%
% spN = [4 8 11 13];
% srow = find(sum(ismember(pairs,spN),2) == 2);
% spairs = pairs(srow,:);
% [~,ind] = get(obj1,'Number','hist',spairs);
% ind = intersect(ind,sindIDE);
% [xcorrindout] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(idecells),obj1,ind);
%
% xcorrSpikeSummary(obj1,ind(xcorrindout),'name','rule network + identity cells')
%
% spN = [1 2 3 10];
% srow = find(sum(ismember(pairs,spN),2) == 2);
% spairs = pairs(srow,:);
% [~,ind] = get(obj1,'Number','hist',spairs);
% ind = intersect(ind,sindIDE);
% [xcorrindout] = findIndObjPSTHinIndObjXCorr(IDEpsth,IDEind(idecells),obj1,ind);
% xcorrSpikeSummary(obj1,ind(xcorrindout),'name','non-specifc network + identity cells')







keyboard
%% Granger causality

keyboard
cd /Volumes/raid/data/monkey/
clear all

LOCobj = loadObject('mtsXCobjLOC.mat');
IDEobj = loadObject('mtsXCobjIDE.mat');
[sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);
obj1 = IDEobj;

pairs = unique(obj1.data.hist(sindIDE,:),'rows');

spN = [4 8 11 13];
%  spN = [1 2 3 10];
srow = find(sum(ismember(pairs,spN),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);


[sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins] = plotIncXcorrSpikes(obj1,ind,'plot');

allsigCor = unique(cat(2,sigCor{1,1},sigCor{2,1}));
inc = zeros(2,4,length(allsigCor),128);
prefix = 'gccorr';
for ii = 1 : length(allsigCor)
    name1 = sprintf('%04g%02g%s',obj1.data.Index(ind(allsigCor(ii)),3),obj1.data.Index(ind(allsigCor(ii)),9),obj1.data.Index(ind(allsigCor(ii)),7));
    name2 = sprintf('%04g%02g%s',obj1.data.Index(ind(allsigCor(ii)),4),obj1.data.Index(ind(allsigCor(ii)),10),obj1.data.Index(ind(allsigCor(ii)),8));
    if obj1.data.Index(ind(allsigCor(ii)),3) ~= obj1.data.Index(ind(allsigCor(ii)),4)
        cd(obj1.data.setNames{ind(allsigCor(ii))})
        for rule =1 : 2
            load(sprintf('%sg%sg%sRule%d.mat',prefix,name1,name2,rule));
            for ep = 1 : 4
                inc(rule,ep,ii,:) = ((Fx2y{ep}- Fy2x{ep}) > prctile(Fdiff{ep},97.5,1)) | ((Fx2y{ep}- Fy2x{ep}) < prctile(Fdiff{ep},2.5,1));
            end
        end
    end
end

for r = 1 : 2; for ep=1:4;subplot(4,2,(ep-1)*2+r);plot(f,squeeze(sum(inc(r,ep,:,:),3))/length(ind)); xlim([0 50]);end; end

%% firing rate spike counts normality test

cd /Volumes/raid/data/monkey/
clear all

LOCobj = loadObject('mtsXCobjLOC.mat');
IDEobj = loadObject('mtsXCobjIDE.mat');
[sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);
obj1 = IDEobj;

pairs = unique(obj1.data.hist(sindIDE,:),'rows');

spN = [4 8 11 13];
%  spN = [1 2 3 10];
srow = find(sum(ismember(pairs,spN),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
allh1 = zeros(length(ind),2);
allh2 = zeros(length(ind),2);
mfr1 = zeros(2,length(ind),3);
mfr2 = zeros(2,length(ind),3);
allcoef =zeros(2,length(ind),3);
binSize = 500;
for ii = 1 : length(ind)
    cd(obj1.data.setNames{ind(ii)})
    cd ..
    cd(sprintf('./group%04g/cluster%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7)))
    %     name1 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7));
    %     name2 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8));
    allsp = cell(2,2);
    if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
        for rule = 1 : 2
            load(sprintf('nineStimPSTH%d',rule))
            allspt=cat(1,A{:,1});
            
            
            for b = 1 : 3;allsp{1,rule}(:,b) = sum(allspt(:,(b-1)*binSize+1:(b-1)*binSize+binSize),2); end
            mfr1(rule,ii,:) = 2* sum(allsp{1,rule},1)/ size(allsp{1,rule},1);
            %             p = ones(3,1);
            %             for nb = 1:3; [~,p(nb)] = lillietest(allsp(:,nb)); end%;plot(hist(allsp(:,nb)));pause; clf;end
            %             allh1(ii,rule) = sum(p<(0.05/(3))) > 0;
        end
    end
    cd ../..
    
    cd(sprintf('./group%04g/cluster%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8)))
    if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
        for rule =1 : 2
            load(sprintf('nineStimPSTH%d',rule))
            allspt=cat(1,A{:,1});
            
            for b = 1 : 3;allsp{2,rule}(:,b) = sum(allspt(:,(b-1)*binSize+1:(b-1)*binSize+binSize),2); end
            mfr2(rule,ii,:) = 2* sum(allsp{2,rule},1)/ size(allsp{2,rule},1);
            %             p = ones(3,1);
            %             for nb = 1:3; [~,p(nb)] = lillietest(allsp(:,nb));end
            %             allh2(ii,rule) = sum(p<(0.05/(3))) > 0;
        end
    end
    for rule = 1 : 2
        for b = 1 : 3
            R = corrcoef(allsp{1,rule}(:,b),allsp{2,rule}(:,b));
            allcoef(rule,ii,b) = R(1,2);
        end
    end
    
    cd ../..
end
%% rule specific rsc for 500 ms epochs
cd /Volumes/raid/data/monkey/
clear all

LOCobj = loadObject('mtsXCobjLOC.mat');
IDEobj = loadObject('mtsXCobjIDE.mat');
[sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);
obj1 = IDEobj;

pairs = unique(obj1.data.hist(sindIDE,:),'rows');

gr1 = [4 11 13];
gr2 = [2 3 8 10];
allgr = [2 3 4 8 10 11 13];
srow = find(sum(ismember(pairs,gr1),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
srow = find(sum(ismember(pairs,allgr),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
[sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins,newind] = plotIncXcorrSpikes(obj1,ind,'plot','append','epoch','falseRate',[0 0],'prctileCoh',[.05 99.95],'prctileRule',[.5 99.5],'pvalueCoh',10^-10);
% sigPos(rule,ind,align,epoch)
al = [1 1; 1 2; 1 3; 2 3];

rsc = cell(4,1);
sur = cell(4,1);
selind = cell(4,1);
for ep = 1 : 4
    ssind = find(sigPos(1,:,al(ep,1),al(ep,2)) == 1 | sigPos(2,:,al(ep,1),al(ep,2)) == 1);
    for sii = 1 : length(ssind)
        sssind = newind(ssind(sii));
        try
            thefile = sprintf('xccorrg%04.0f0%d%sg%04.0f0%d%sRuleComp500ms.mat', obj1.data.Index(sssind,3),obj1.data.Index(sssind,9),obj1.data.Index(sssind,7),obj1.data.Index(sssind,4),obj1.data.Index(sssind,10),obj1.data.Index(sssind,8));
            d=load([obj1.data.setNames{sssind} '/' thefile]);
            
        catch
            thefile = sprintf('xccorrg%04.0f0%d%sg%04.0f0%d%sRuleComp500ms.mat', obj1.data.Index(sssind,4),obj1.data.Index(sssind,10),obj1.data.Index(sssind,8),obj1.data.Index(sssind,3),obj1.data.Index(sssind,9),obj1.data.Index(sssind,7));
            d=load([obj1.data.setNames{sssind} '/' thefile]);
        end
        rsc{ep}(sii) = d.diffrc(al(ep,1),al(ep,2));
        sur{ep}(sii) = prctile(d.diffrcSur(:,al(ep,1),al(ep,2)),99);
    end
    selind{ep} = ssind;
end

sigP = zeros(4,1);
allsEP = [];
for ep = 1 : 4;
    
    sigP(ep) = 100*sum(rsc{ep} > sur{ep}) / length(rsc{ep});
    allsEP = [allsEP  selind{ep}(rsc{ep} > sur{ep})];
end

allEP = unique([selind{1} selind{2} selind{3} selind{4}]);
allsEP = unique(allsEP);

100*length(allsEP)/length(allEP)


