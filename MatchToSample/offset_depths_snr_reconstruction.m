function offset_depths_snr_reconstruction(max_offset,spike_thresh,redo)

%redo: 1 = save new channel_offsets, 0 = no save

%max_offset is entered in mm

max_o = max_offset/.5;


%calculates optimal offset

load depths_snr_reconstruction2 %from make_depths_snr_reconstruction

load betty_reconstruction

d = depths_snr_reconstruction.depths;
s = depths_snr_reconstruction.snr;
deps = floor(d);

r = betty_reconstruction;

days = size(d,1);
%days = 20;

for a = 1 : 64
    for aa = 1 : days
        ch_depth = d(aa,a);
        ch_floor_depth = deps(aa,a);
        if (ch_depth>(ch_floor_depth + .75))
            all_depths(aa,a) = ch_floor_depth + 1;
        elseif (ch_depth<(ch_floor_depth + .25))
            all_depths(aa,a) = ch_floor_depth;
        else
            all_depths(aa,a) = ch_floor_depth + .5;
        end
    end
end

depths = (all_depths./.5);
%maximum offset is 6mm
scores = nan(max_o,64);
consistency_gray = nan(64,40,max_o);
max_snrs = nan(64,40,max_o);
for x = 1 : 64
    
    f = find(depths(:,x));
    m = min(depths(f,x));
    if isempty(m)
        m=0;
    end
    if m > (max_o+1)
        m = max_o+1;
    end
    
    for all_offsets = 1:(m-1) %.5 - max_o
        ddepths = depths - all_offsets; %equivalent to subtracting .5 mm
        ddepths(find(ddepths < 0)) = 0; %necessary for unique
        
        %only use the maximum snr for each depths
        u_depths = unique(ddepths(:,x));
        u_depths = u_depths((2:end))'; %get rid of 0
        
        %get the day and the maximum snr for the independent depths
        ids= 0;
        all_days = [];
        for id = u_depths
            ids = ids + 1;
            id_days = find(ddepths(:,x) == id)';
            [max_s max_ss]= max(s(id_days,x));
            all_days(ids) = id_days(max_ss); %use the day with the max snr
        end
        
        sss = 0; %keep track of how many miss alignments occur
        for y = all_days
            
            if ~strncmpi(betty_reconstruction(x,ddepths(y,x)),'WM',2) && (s(y,x) > spike_thresh)
                sss = sss + 1;  % this criteria uses largest score
            end
            
            if s(y,x) > spike_thresh %indicates it should be gray matter
                consistency_gray(x,ddepths(y,x),all_offsets) = 1;
                max_snrs(x,ddepths(y,x),all_offsets) = s(y,x);
            end
            
        end
        scores(all_offsets,x) = sss;
    end
end


[i ii] = max(scores((3:end),:)); %min offset set at 1.5

channel_offsets = ((ii+2) / 2);

%channels X depths, starts at channel 1
white_gray_matter = []; for x = 1:64;  for y = 1:40; if strncmpi(betty_reconstruction(x,y),'WM',2), white_gray_matter(x,y) = nan; else white_gray_matter(x,y) = .5;end;end;end;

if redo
    save channel_offsets2 channel_offsets scores consistency_gray white_gray_matter max_snrs
end

%load channel_offsets channel_offsets

w= white_gray_matter;
w(find(isnan(w))) = 0;

c = consistency_gray;
c(find(isnan(c))) = 0;

m = max_snrs;
m(find(isnan(m))) = 0;

% % % figure
% % % for x=1:8;subplot(2,4,x);imagesc(w+c(:,:,x),[0 1.5]);end
% % % figure
% % % imagesc(w,[0 1.5]);
% % % figure
% % % hist(channel_offsets)

%make optimal plot
offsets = channel_offsets/.5;
for qqq = 1 : 64;
    optimal(qqq,:) = c(qqq,:,offsets(qqq));
    optimal_snr(qqq,:) = m(qqq,:,offsets(qqq));
end
ww= w;
%find first recorded depths
for f = 1:64
    try
        i = find(d(:,f));
        df = diff(i);
        dff = find(df~=1);
        dff_minus_offset = dff(1) - offsets(f);
        ww(f,(1 :dff_minus_offset)) = 2;
    catch
        
    end
end

figure
imagesc(w+optimal,[0 1.5])
set(gca,'YTick',[1:64])
set(gca,'XTick',[1:40])

figure
imagesc(ww+optimal,[0 2])
set(gca,'YTick',[1:64])
set(gca,'XTick',[1:40])
figure

%make max snr 5
optimal_snr(find(optimal_snr > 10)) = 5;

w(find(optimal_snr>0)) = 0;
ww = w + optimal_snr;

%make a scale bar
spt = spike_thresh / .25;
qqq= 0;
for q = spt:20
    qqq = qqq+1;
    ww(qqq,40) = (q/4);
end

imagesc(ww,[0 5])
set(gca,'YTick',[1:64])
set(gca,'XTick',[1:40])


depths = [1:40];
dd = depths_snr_reconstruction.depths;
dd(find(dd==0))=nan;
ss = depths_snr_reconstruction.snr;
ss(find(ss==0))=nan;
white_gray_matter(find(~isnan(white_gray_matter))) = 0;

% % for q = 1:8
% %     figure
% %     dd = dd - (q/2);
% %     for x = 1:64;subplot(8,8,x);d = dd(:,x);s = ss(:,x);scatter((d/.5),s);hold on;m = white_gray_matter(x,:);scatter(depths,m);hold on;plot([1 40],[1.5 1.5],'r');end
% %
% % end
