function lfp3d_task(varargin)


%run at session level
%fix
%sample
%delay400
%delay800


Args = struct('ml',0);
Args.flags = {'ml',};
Args = getOptArgs(varargin,Args);

cd('/Volumes/raid/data/monkey/betty/091001/session01')

mt = mtstrial('auto','ML','RTfromML','redosetNames');


cd('lfp/lfp2/')
lfp2dir = pwd;
t = nptDir('betty*');

sample_on = floor(mt.data.CueOnset); %sample on
% sample_off = floor(mt.data.CueOffset); %sample off
% match = floor(mt.data.MatchOnset);    %match

figure
plotcounter = 0;
for ide = 1:3
    for loc = 1:3
        plotcounter = plotcounter + 1;
        
        trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',ide,'iCueLoc',loc);
        
        trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        cd(lfp2dir)
        
        tcounter = 0;
        presampled = single(zeros(63,63,63));
        sampled = single(zeros(63,63,63));
        delay1d = single(zeros(63,63,63));
        delay2d = single(zeros(63,63,63));
        alld = zeros(63,63,63);
        for nt = trials;
            tcounter = tcounter + 1;
            
            s_on = round(sample_on(nt));
            %     s_off = round(sample_off(nt));
            
            load(t(nt).name);
            d = [];
            for hx = 1 : size(data,1)
                d(hx,:) = hilbert(data(hx,:)); %%%%%MAKE SURE THE HILBERT TRANSFORM IS WORKING PROPERLY, IT MIGHT BE INVERTING THINGS
            end
            data = d;
            
            
            
            data = data(:,(s_on - 499) : (s_on + 1300));
            d1 = round(angle(data(39,:))* 10) + 32;
            d2 = round(angle(data(43,:))* 10) + 32;
            d3 = round(angle(data(29,:))* 10) + 32; %need all positive numbers
            
            for dp = 1 : 400
                presampled(d1(100+dp),d2(100+dp),d3(100+dp)) = presampled(d1(100+dp),d2(100+dp),d3(100+dp)) + 1;
                sampled(d1(600+dp),d2(600+dp),d3(600+dp)) = sampled(d1(600+dp),d2(600+dp),d3(600+dp)) + 1;
                delay1d(d1(1000+dp),d2(1000+dp),d3(1000+dp)) = delay1d(d1(1000+dp),d2(1000+dp),d3(1000+dp)) + 1;
                delay2d(d1(1400+dp),d2(1400+dp),d3(1400+dp)) = delay2d(d1(1400+dp),d2(1400+dp),d3(1400+dp)) + 1;
            end
            
            for dpall = 1 : 1800
                alld(d1(dpall),d2(dpall),d3(dpall)) = alld(d1(dpall),d2(dpall),d3(dpall)) + 1;
            end
        end
        
        
        pr = find(presampled);
        s = find(sampled);
        de1 = find(delay1d);
        de2 = find(delay2d);
        
        ad = find(alld);
        
        
        
        pprct =  prctile(presampled(pr),99.99);
        sprct =  prctile(sampled(s),99.99);
        de1prct =  prctile(delay1d(de1),99.99);
        de2prct =  prctile(delay2d(de2),99.99);
        
        th = max([pprct sprct de1prct de2prct]);
        
        [prx pry prz] = ind2sub(size(presampled),pr(find(presampled(pr) >= th)));
        
        [sx sy sz] = ind2sub(size(sampled),s(find(sampled(s) >=  th)));
        
        [de1x de1y de1z] = ind2sub(size(delay1d),de1(find(delay1d(de1) >=  th)));
        
        [de2x de2y de2z] = ind2sub(size(delay2d),de2(find(delay2d(de2) >=  th)));
        
        
        figure
        scatter3(prx,pry,prz,50,'g','fill','s')
        hold on
        scatter3(sx-.25,sy+.25,sz+.25,50,'r','fill','s')
        hold on
        scatter3(de1x-.25,de1y-.25,de1z-.25,50,'k','fill','s')
        hold on
        scatter3(de2x+.25,de2y+.25,de2z+.25,50,'b','fill','s')
        hold on
        

        
        
        subplot(3,3,plotcounter)
        adrct =  prctile(alld(ad),99.9);
        [ax ay az] = ind2sub(size(alld),ad(find(alld(ad) > adrct)));
        scatter3(ax,ay,az,50,'k','fill','s')
        
        hold on
        
    end
end





