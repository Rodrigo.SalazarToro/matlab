function spike_hsfpp_relationship


cd /media/raid/data/monkey/betty/090930/session01/lfp/lfp2/

spikehilbert = nptDir('spikehilbertentropy*');
sp = 0;
figure
for pairs = 1%1:36
    
    load(spikehilbert(pairs).name)
    
    sph = spikehilbertentropy{1}{1};
    
    ntrials = sph.ntrials;
    
    all_spikesmax = [];
    all_spikesrand = [];
    all_spikes = [];
    for nt = 1 : ntrials
        plvs = sph.plv{1,nt};
        spikes = sph.cross{1,nt};
        nspikes = size(spikes,2);
        
        if nspikes > 0
        
           rand_sp = randperm(nspikes);
           
        plv_sp = spikes(find(plvs < .9));
        
        all_spikes = [all_spikes spikes];
        
        nplv_sp = size(plv_sp,2);
        
        
        
        all_spikesmax = [all_spikesmax plv_sp];
        all_spikesrand = [all_spikesrand spikes(rand_sp(1:nplv_sp))];
        end


    end
%     sp = sp + 1;
%     if sp == 9;
%         figure;
%         sp = 1;
%     end
%     subplot(4,2,sp)
%     rose(all_spikes);
% subplot(6,6,pairs)

    rt = round(circ_rtest(all_spikesmax) * 100) /100;
    cm = round(circ_mean(all_spikesmax') * (180/pi));
    hold on
    title(['rtest: ' num2str(rt) '  cmean: ' num2str(cm)]) 
    
    bar(hist(all_spikesmax,[-pi:.5:pi]))
    hold on
    plot(hist(all_spikesrand,[-pi:.5:pi]),'r')
    
    figure
    bar(hist(all_spikes,[-pi:.5:pi]))
end









