function make_all_pair_probabilities

%run at session level
% % % %mt = mtstrial('auto','ML','RTfromML');
% % % %index = mtsgetTrials(mt,'BehResp',1,'stable','ML');
% % % %type = mt.data.Index(:,1);
% % % %trials_identity = (find(type == 1));
% % % %trials_location = (find(type == 2));

mt = mtstrial('auto');
ind = mtsgetTrials(mt,'BehResp',1,'stable');

% % % %ind = intersect(index,trials_identity);
num_trials = size(ind,2);
% % % %cd(['lfp' filesep 'lfp2' filesep 'identity'])
cd(['lfp' filesep 'lfp2'])
load threshold
load all_pairs
% % % % cd ..
lfp2dir = pwd;
numb_pairs = size(all_pairs,1);

all_pair_probabilities = zeros(numb_pairs,60);

for x = 1 : numb_pairs
    thresh = threshold{x}(1); % 95%
    numb_pairs
    x
    
    
% % % %     load(['channelpair' num2strpad(all_pairs(x,1),2) num2strpad(all_pairs(x,2),2)])
load(['channelpair' num2str(all_pairs(x,1)) num2str(all_pairs(x,2))])
    prob = [];
    t = 0;
    for y = ind
        for z = 1:60 %60 bins on each trial
            t = t + 1;
            if corrcoefs(z,y) > thresh
               prob(t,z) = 1;
            else
                prob(t,z) = 0;
            end
        end
    end
    
    pair_prob = sum(prob) ./ num_trials;
    
    all_pair_probabilities(x,:) = pair_prob;
    
    save all_pair_probabilities all_pair_probabilities
   
end



