function image_luminance_gradient_color

%calculated from monitor: .2272*R + .7014*G + .0714*B
%adjusted based on monitor in use
r_coef = .2503;
g_coef = .6824;
b_coef = .0672;

%set total desired luminance
ss = 80 * 80 * 3;
desiredlum = 800000;

gradient = [0:.05:1];
hdir = pwd;

mkdir color

%get list of images
all_images = [nptDir('*.bmp') ; nptDir('*.jpg')];
nimages = size(all_images,1);

for ni = 1 : nimages
    cd(hdir)
    im = imread(all_images(ni).name);
    
    im(find(single(im) < 150)) = 0; %get rid of borders
    im(find(single(im) >= 150)) = 255;
    
    
    %make red green and blue images for all luminances
    %     colors = [110 0 0;0 77 0;25 25 220];
    colors = [115 0 0;0 70 0;25 25 220];
    coefs = [r_coef g_coef b_coef];
    for c = 1 : 3
        im1 = im(:,:,1);
        im2 = im(:,:,2);
        im3 = im(:,:,3);
        im1(find(im1 == 255)) = colors(c,1);
        im2(find(im2 == 255)) = colors(c,2);
        im3(find(im3 == 255)) = colors(c,3);
        
        iim = zeros(80,80,3);
        iim = uint8(iim);
        iim(:,:,1) = im1;
        iim(:,:,2) = im2;
        iim(:,:,3) = im3;
        
        for ng = gradient
            newii = iim * ng;
            
            cd([hdir filesep 'color'])
            if c == 1
                imwrite(newii,[all_images(ni).name(1 : end-4) 'red' num2str(ng*100) all_images(ni).name(end-3 : end)])
            elseif c == 2
                imwrite(newii,[all_images(ni).name(1 : end-4) 'green' num2str(ng*100) all_images(ni).name(end-3 : end)])
            else
                imwrite(newii,[all_images(ni).name(1 : end-4) 'blue' num2str(ng*100) all_images(ni).name(end-3 : end)])
            end
        end
    end
end

cd(hdir)



