function mi = mutual_info_pairs(varargin)

Args = struct('posterior_distributions',[],'prob_response',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

time_points = size(Args.posterior_distributions,2);
num_stims = size(Args.posterior_distributions,1);

mi = zeros(num_stims,time_points);
for st = 1 : num_stims
    for x = 1 : time_points
        % mi = sum( P(r)*P(s|r) log2(P(s|r) / P(s)) )
        mi(st,x) = nansum(Args.prob_response{x} .* Args.posterior_distributions{st,x} .* log2(Args.posterior_distributions{st,x} ./ (1/num_stims)));
    end
end


mi = sum(mi);