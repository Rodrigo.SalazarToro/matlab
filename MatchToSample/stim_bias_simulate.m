function stim_bias_simulate


b = simulate_ml




%get condition number
conditions = unique(b.ConditionNumber)';
num_conditions = size(conditions,2);
correct = find(b.TrialError == 0);
incorrect = find(b.TrialError == 6);



total_correct_incorrect = size(correct,1) + size(incorrect,1);

fprintf(1,['total number of correct and incorrect trials = ' num2str(total_correct_incorrect) '\n'])

total_trials = size(b.ConditionNumber,1);



for tt = 1:total_trials
    for nc = 1 : num_conditions
        trials_cond = find(b.ConditionNumber((1:tt),1) == conditions(nc));
        num_correct = size(intersect(trials_cond,correct),1);
        num_incorrect = size(intersect(trials_cond,incorrect),1);
        
        all_performance(nc,tt) = (num_correct / (num_correct + num_incorrect));
        all_conditions(nc,tt) = size(trials_cond,1) / tt;
    end
end






%make bar chart for percent correct on each condition
for nc = 1 : num_conditions
    trials_cond = find(b.ConditionNumber == conditions(nc));
    num_correct = size(intersect(trials_cond,correct),1);
    num_incorrect = size(intersect(trials_cond,incorrect),1);
    sdata(nc) = (num_correct / (num_correct + num_incorrect)) * 100;
    
    correct_totals(nc) = num_correct;
    
    trial_totals(nc) = num_correct + num_incorrect;
    count = 0;
% % %     for xx = 201 :100: total_trials
% % %         count = count + 1;
% % %         bc = find(b.ConditionNumber((xx-200):xx) == conditions(nc));
% % %         
% % %         xx_correct = size(intersect(bc,correct),1);
% % %         xx_incorrect = size(intersect(bc,incorrect),1);
% % %         xx_total =  xx_correct / (xx_correct + xx_incorrect);
% % %         
% % %         resp = size(bc,1) / 200;
% % %         
% % %         if resp == 0
% % %            resp = nan; 
% % %         end
% % %         if xx_total == 0
% % %             xx_total = nan;
% % %         end
% % %       
% % %         
% % %         total_response(nc,count) = resp; %percentage of total trials for each condition
% % %         total_percentage(nc,count) =  xx_total; %percentage of correct trials
% % %     end
end

% % % figure
% % % for y = 1 : num_conditions
% % %     num_points = size(total_percentage,2);
% % %     subplot(3,4,y)
% % %     scatter([1:num_points],total_response(y,:),'.')
% % %     %plot(total_response(y,:))
% % %     hold on
% % %     scatter([1:num_points],total_percentage(y,:),'g','.')
% % %     %plot(total_percentage(y,:),'g')
% % %     hold on
% % %     axis([0 num_points 0 1])
% % % end


figure
for y = 1 : num_conditions
    subplot(3,4,y)
    plot(all_performance(y,:),'g')
    hold on
    plot(all_conditions(y,:))
    hold on
    axis([0 total_trials 0 1])
end



figure
bar(sdata)
axis([0 (nc+1) 0 100])


plotperformance(b)





