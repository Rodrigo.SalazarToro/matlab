function make_bar_graphs_mtscpp

ml = 0;

o = 1;
options = {'glm_delay' 'in_phase' 'glm_fix' 'anti_phase'}

comb = {'mPPC Vs lPPC', 'mPPC Vs mPPC', 'lPPC Vs lPPC', 'mPPC Vs PFC', 'lPPC Vs PFC', 'PFC Vs PFC'}

% if ~ml
    load clark_mtscpp
    
    for ww = 1 : 2
        if ww == 1;
            opt = [1,2];
        else
            opt = [1,4];
        end
        
        y = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[0 1],options{o});
        yy = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[0 1],options{opt});
        
        percentages(1) = (yy/y) * 100;
        
        y = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 0],options{o});
        yy = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 0],options{opt});
        
        percentages(2) = (yy/y) * 100;
        
        y = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 1],options{o});
        yy = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pp','hist',[1 1],options{opt});
        
        percentages(3) = (yy/y) * 100;
        
        
        
        
        
        xd = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 0],options{o});
        xxd = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 0],options{opt});
        
        xv = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 1],options{o});
        xxv = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[0 1],options{opt});
        
        percentages(4) = ((xxv+xxd)/(xv+xd)) * 100;
        
        
        
        
        xd = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 0],options{o});
        xxd = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 0],options{opt});
        
        xv = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 1],options{o});
        xxv = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','pf','hist',[1 1],options{opt});
        
        percentages(5) = ((xxv+xxd)/(xv+xd)) * 100;
        
        
        
        
        
        z = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','ff',options{o});
        zz = get(clark_mtscpp,'Number','threshold',1,'correct','stable','identity','ff',options{opt});
        
        percentages(6) = (zz/z) * 100;
        
        if ww == 1;
            per([1,3,5,7,9,11]) = percentages;
        else
            per([2,4,6,8,10,12]) = percentages;
        end
        
        
    end
    figure
    subplot(2,1,1)
    bar1 =bar(per);
    set(bar1,'BarWidth',.75)
    hold on
    ylim([0 100])
    set(gca,'XTickLabel',comb)
    
    
    % %
    % % figure
    % % for pies = 1:6
    % %     p(1) = percentages(pies)/100;
    % %     p(2) = 1 - p(1);
    % %     subplot(2,3,pies)
    % %     pie(p,[0 1])
    % %     title(comb{pies})
    % % end
    
    
% else
    b
    cd betty
    load betty_mtscpp
    
    for ww = 1:2
        if ww == 1;
            opt = [1,2];
        else
            opt = [1,4];
        end
        
        y = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[0 1],options{o});
        yy = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[0 1],options{opt});
        
        percentages(1) = (yy/y) * 100;
        
        y = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[1 0],options{o});
        yy = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[1 0],options{opt});
        
        percentages(2) = (yy/y) * 100;
        
        y = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[1 1],options{o});
        yy = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pp','hist',[1 1],options{opt});
        
        percentages(3) = (yy/y) * 100;
        
        
        
        
        
        xd = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[0 0],options{o});
        xxd = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[0 0],options{opt});
        
        xv = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[0 1],options{o});
        xxv = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[0 1],options{opt});
        
        percentages(4) = ((xxv+xxd)/(xv+xd)) * 100;
        
        
        
        
        xd = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[1 0],options{o});
        xxd = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[1 0],options{opt});
        
        xv = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[1 1],options{o});
        xxv = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','pf','hist',[1 1],options{opt});
        
        percentages(5) = ((xxv+xxd)/(xv+xd)) * 100;
        
        
        
        
        
        z = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','ff',options{o});
        zz = get(betty_mtscpp,'Number','ml','threshold',1,'correct','stable','identity','ff',options{opt});
        
        percentages(6) = (zz/z) * 100;
        if ww == 1;
            per([1,3,5,7,9,11]) = percentages;
        else
            per([2,4,6,8,10,12]) = percentages;
        end
        
    end
    
          subplot(2,1,2)
        bar1 =bar(per);
        set(bar1,'BarWidth',.75)
        hold on
        ylim([0 100])
       set(gca,'XTickLabel',comb)
    
    
    
% % %     figure
% % %     for pies = 1:6
% % %         p(1) = percentages(pies)/100;
% % %         p(2) = 1 - p(1);
% % %         subplot(2,3,pies)
% % %         pie(p,[0 1])
% % %         title(comb{pies})
% % %     end
    
% end

