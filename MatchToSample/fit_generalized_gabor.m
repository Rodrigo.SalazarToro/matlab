function fit_generalized_gabor(varargin)

%run at session level
Args = struct('ml',0,'rule',1);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args);

sesdir = pwd;
cd([sesdir filesep 'lfp' filesep 'lfp2'])
lfp2dir = pwd;

if Args.ml
    if Args.rule == 1
        cd identity
    else
        cd location
    end
end


load avg_correlograms_epochs
numb_pairs = size(avg_correlograms_epochs,2);

for x = 1 : numb_pairs
    corr_pair = avg_correlograms_epochs{x};
    %run through all 4 epochs
    for e = 1 : 4
        std_pair = epoch_correlograms_stds{e,x};
        %get the lag of the peak
        corr_peak = avg_corrcoef_epochs{e,x};
        corr_lag = avg_phase_epochs{e,x};
        [cf, estimates, sse, p_angle_deg] = generalized_gabor_fmin('correlogram',corr_pair(e,:),'stds',std_pair,'peak',corr_peak,'lag',corr_lag,'plot');

        gen_gabor.epoch_correlogram = corr_pair(e,:);
        gen_gabor.cf = cf;
        gen_gabor.frequency = estimates(3) * 1000; %Hz
        gen_gabor.phase_shift = estimates(4); %ms
        gen_gabor.sse = sse;
        gen_gabor.phase_angle_deg = p_angle_deg; %degrees
        gen_gabor
        %pause
        gabor{e} = gen_gabor;
    end
    epoch_gabors{x} = gabor;
end

write_info = writeinfo(dbstack);
save epoch_gabors epoch_gabors write_info

cd(sesdir)




