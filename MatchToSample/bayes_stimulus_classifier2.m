function bayes_stimulus_classifier2(varargin)

Args = struct('calc_joint',0,'new_rand',0,'ml',0,'threshold',0,'locations',0,'select_pairs',0);
Args.flags = {'calc_joint','new_rand','ml','locations','select_pairs'};
Args = getOptArgs(varargin,Args);

%run at lfp2 session level
sesdir = pwd;


if ~isempty(Args.select_pairs)
    c = mtscpp('auto','ide_only','ml');
    [~,sp]=get(c,'Number','sorted','identity','hist_numbers',[4 10]);
end

cd([sesdir filesep 'lfp' filesep 'lfp2']);

load glm_fits_train trial_corrcoefs trial_phases
load stimulus

if Args.locations
    %determine stimulus locations
    locs = zeros(1,size(stimulus,2));
    locs([find(stimulus==1) find(stimulus==4) find(stimulus==7)]) = 1;
    locs([find(stimulus==2) find(stimulus==5) find(stimulus==8)]) = 2;
    locs([find(stimulus==3) find(stimulus==6) find(stimulus==9)]) = 3;
    
    %USE LOCATIONS INSTEAD OF INDIVIDUAL STIMULUS COMBINATIONS
    stimulus = locs;
end

if Args.ml
    if Args.select_pairs
        pair_list = sp;
    else
        pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    end
    calc_pair_list = find(cellfun(@isempty,trial_corrcoefs) == 0);
    num_pairs = size(pair_list,2);
    [num_trials,num_points] = size(trial_corrcoefs{pair_list(1)});
    cd identity
    load threshold
    cd ..
else
    num_pairs = size(trial_corrcoefs,2);
    [num_trials,num_points] = size(trial_corrcoefs{1});
    pair_list = [1 : num_pairs];
end



%percent of trials to withold
per = round(num_trials * .30);
if Args.new_rand
    Args.calc_joint = 1; %must recalculate
    %select trials to withold
    r = randperm(num_trials);
    r = r(1:per); %trials to withold
    trials = 1:num_trials;
    trials(r) = [];
    save train_test trials r
else
    load train_test
end

cor_range = [0:.1:1];
cor_range = single(cor_range);
ph_range = [-10:1:10];
ph_range = single(ph_range);
max_phase = max(ph_range);
s_phase = size(ph_range,2);
if Args.calc_joint
    
    %make distributions of conditional probabilities P(r|s)
    for p = calc_pair_list
        if Args.locations
            joint_cond_dist = cell(3,9);
            joint_cum_dist = cell(1,9);
        else
            joint_cond_dist = cell(9,9);
            joint_cum_dist = cell(1,9);
        end
        for t = trials
            for tp = 1 : num_points
                point = single(abs(round(trial_corrcoefs{p}(t,tp)*10)/10));
                [~,po] = intersect(cor_range,point);
                
                phase_point = single(trial_phases{p}(t,tp));
                [~,ph] = intersect(ph_range,phase_point);
                
                if isempty(joint_cond_dist{stimulus(t),tp})
                    joint_cond_dist{stimulus(t),tp} = zeros(s_phase,11);
                end
                
                if isempty(joint_cum_dist{1,tp})
                    joint_cum_dist{1,tp} = zeros(s_phase,11);
                end
                
                if Args.threshold == 0
                    t_threshold = 0;
                else
                    t_threshold = threshold{p}(Args.threshold);
                end
                if ~isempty(ph) && point > t_threshold%threshold
                    joint_cond_dist{stimulus(t),tp}(ph,po) = joint_cond_dist{stimulus(t),tp}(ph,po) + 1;
                end
            end
        end
        
        for stims = 1 : max(stimulus) % 3 or 9
            for tp = 1 : num_points
                ajc = joint_cond_dist{stims,tp};
                xs=[];
                ys=[];
                zs=[];
                count = 0;
                for xx = 1 : 11
                    for yy = 1 : s_phase
                        if ajc(yy,xx) > 0
                            count = count + 1;
                            xs(count,:) = xx;
                            ys(count,:) = yy;
                            zs(count,:) = ajc(yy,xx);
                        end
                    end
                end
                
                if ~isempty(zs);
                    F = TriScatteredInterp(xs,ys,zs,'natural');
                    
                    ti = 1:1:11;
                    tti = 1:1:s_phase;
                    [qx,qy] = meshgrid(ti,tti);
                    qz = F(qx,qy);
                    %             mesh(qx,qy,qz);
                    %             hold on;
                    %             plot3(xs,ys,zs,'o');
                    
                    joint_cond_dist{stims,tp} = qz ./ (sum(nansum(qz)));
                    
                    %get rid of nan's
                    jcd = joint_cond_dist{stims,tp};
                    jcd(isnan(jcd)) = 0;
                    joint_cond_dist{stims,tp} = jcd;
                    if isempty(jcd)
                        joint_cond_dist{stims,tp} = ajc ./ sum(nansum(ajc));
                    end
                end
                
                joint_cum_dist{1,tp} = joint_cum_dist{1,tp} + joint_cond_dist{stims,tp};
            end
        end
        
        for tp = 1 : num_points
            joint_cum_dist{1,tp} = joint_cum_dist{1,tp} ./ sum(sum(joint_cum_dist{1,tp}));
        end
        
        all_joint_cond{p} = joint_cond_dist;
        all_joint_cum{p} = joint_cum_dist;
    end
    save bayes_joint_distributions  all_joint_cond all_joint_cum
else
    load bayes_joint_distributions
end

gp = 0;
good_p = []
pair_predictions = [];
pair_predictions_mag = [];
% all_stim_probs = zeros(size(r,2),num_pairs,9);
all_probabilities = cell(1,num_points);
pair_counter = 0;
all_mutual_info = cell(1,size(pair_list,1));
mmi = zeros(size(r,2),num_points);
pair_counter = 0;
for nump = pair_list
    pair_counter = pair_counter + 1;
    num_per = per;
    match = 0;
    counter = 0;
    predict = [];
    
    if Args.threshold == 0
        t_threshold = 0;
    else
        t_threshold = threshold{nump}(Args.threshold);
    end
    mutual_info = zeros(size(r,2),9);
    for numt = r
        counter = counter + 1;
        if Args.locations
            stim_probabilities = zeros(3,9);
        else
            stim_probabilities = zeros(9,9);
        end
        for ntime = 1 : num_points
            phase = single(trial_phases{nump}(numt,ntime));
            [~,ph] = intersect(ph_range,phase);
            
            corr_coef = single(abs(trial_corrcoefs{nump}(numt,ntime)));
            corr_coef = round(corr_coef*10)/10; %round to tenths place
            [~,po] = intersect(cor_range,corr_coef);
            
            cum_prob = all_joint_cum{nump}{1,ntime}(ph,po);
            for stims = 1 : max(stimulus)
                cond_prob = all_joint_cond{nump}{stims,ntime}(ph,po);
                
                if Args.locations
                    prob_sample = 1/3;
                else
                    %probability of the sample is 1/9
                    prob_sample = 1/9;
                end
                
                %bayes formula P(s|r) = (P(r|s)*P(s))  /  P(r)
                if ~isempty(cum_prob) && ~isempty(cond_prob) && ~isempty(ph) && corr_coef > t_threshold %threshold
                    stim_probabilities(stims,ntime) = (cond_prob * prob_sample) / cum_prob;
                    % %
                    % %                     subplot(1,2,1)
                    % %                     surface(flipdim(all_joint_cond{nump}{stims,ntime},1))
                    % %                     hold on;scatter3(po,(s_phase-ph),.1,100,[.5 0 0],'filled')
                    % %                     subplot(1,2,2)
                    % %                     surface(flipdim(all_joint_cum{nump}{1,ntime},1))
                    % %                     hold on;scatter3(po,(s_phase-ph),.1,100,[.5 0 0],'filled')
                    % %                     (cond_prob * prob_sample) / cum_prob;
                    % %                     pause
                    % %                     close all
                    
                end
            end
            %get rid of nans
            stim_probabilities(isnan(stim_probabilities)) = 0;
            
            if ~isempty(cum_prob) && ~isempty(cond_prob) && ~isempty(ph) && corr_coef > t_threshold %threshold
                post_probs = stim_probabilities(:,ntime)';
                mutual_info(counter,ntime) = mutual_info_stims('posterior_probs',post_probs,'prob_response',cum_prob);
            end
            
        end
        
        mm = nanmean(mutual_info(counter,:));
        
        [mm,best] = max(sum(stim_probabilities'));
        
        
        %         best
        %         stimulus(numt)
        
        if mm > 2
            predict(numt) = best;
            if best == stimulus(numt);
                match = match + 1;
            end
        else
            predict(numt) = 0;
            num_per = num_per - 1;
        end
        predict_mag = 1;
    end
    
    nump
    %     perf = (match / per)*100
    perf = (match / num_per)*100
    num_per
    if perf >=40
        gp = gp + 1;
        good_p(gp) = pair_counter;
    end
    
    pair_predictions(:,pair_counter) = predict';
    pair_predictions_mag(:,nump) = predict_mag';
    all_mutual_info{pair_counter} = mutual_info;

    
    mmi =mmi+ mutual_info;
end

% ss=[];for x = 1:78;ss(x) = max(mean(all_mutual_info{x}));end
% good_p = find(ss>=.001)
%


m=0;
for x = r,
    s=zeros(1,max(stimulus));
    for xx = 1:max(stimulus)
        s(xx) = size(find(pair_predictions(x,good_p) == xx),2);
    end
    [~,best] = max(s);
    if best == stimulus(x)
        m = m+1;
    end
end
total_perf = (m/per) * 100



%plot mean mutual information for each pair
npairs = size(pair_list,2);
ss=[];for x = 1:npairs;ss(x,:) = mean(all_mutual_info{x});end
surface(ss)



cd(sesdir)










