function b = simulate_ml

numb_conditions = 12;
performance = .60;
break_fix = .25;
total_trials = 2500;

TrialRecord.ConditionsThisBlock = [1:numb_conditions];

%get first condition
perm = randperm(numb_conditions);
new_condition = perm(1);
plot_on = 0;


if plot_on
    figure('Position',[100 1000 1000 1000])
end

for x = 1 : total_trials
    
    TrialRecord.ConditionsPlayed(x) = new_condition;
    
    TrialRecord.CurrentTrialNumber = x;
    
    
    
    r = rand;
    
    if r >= break_fix
        
        %get another random number
        r = rand;
        
        %determine probability for a specific condition
        if new_condition <= 2 && x <= 1000
            if r >= .2
                TrialRecord.TrialErrors(x) = 6; %incorrect
            else
                TrialRecord.TrialErrors(x) = 0; %correct
            end
        elseif new_condition <= 2 && x > 1000
            
            if r >= .4
                TrialRecord.TrialErrors(x) = 6; %incorrect
            else
                TrialRecord.TrialErrors(x) = 0; %correct
            end
            
        elseif new_condition >=10
            if r >= .8
                TrialRecord.TrialErrors(x) = 6; %incorrect
            else
                TrialRecord.TrialErrors(x) = 0; %correct
            end
        else
            if r >= performance
                TrialRecord.TrialErrors(x) = 6; %incorrect
            else
                TrialRecord.TrialErrors(x) = 0; %correct
            end
        end
        
    else
        TrialRecord.TrialErrors(x) = 3; %break fixation
        
    end
    [new_condition in_performance new_prob] = simulate_condition_prob_weighted(TrialRecord);
    
    
    if plot_on
        if new_prob ~= 0
            if rem(x,20) == 0
                
                for nc = 1 : numb_conditions
                    last_conds(nc) = size(find(TrialRecord.ConditionsPlayed((end-19):end) == nc),2);
                end
                subplot(2,1,1)
                bar(in_performance)
                %axis([0 (numb_conditions+1) 0 1])
                % % %             subplot(3,1,2)
                % % %             bar(new_prob)
                subplot(2,1,2)
                bar(last_conds)
                %axis([0 (numb_conditions+1) 0 1])
                pause(1)
            end
        end
    end
    clc
    x
    
    b.ConditionNumber = TrialRecord.ConditionsPlayed';
    b.TrialError = TrialRecord.TrialErrors';
    
end
