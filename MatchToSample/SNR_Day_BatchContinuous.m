function SNR_Day_BatchContinuous(varargin)

%       Run at day level
%
%           Arguments:
%
%           'sessions'   -  [session indices]
%           'chunks'     -  [chunk size in seconds]
%           'redo'       -  recompute SNR for specific sessions
%
%        SNR = range(Signal)/range(Noise)
%
%        The function will return a structure called SNR containing many SNR's and
%        signal and noise values.      signal and noise values.

Args = struct('sessions',[],'chunks',[],'redo',0);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

daydir = pwd;

for s = Args.sessions
    cd([daydir filesep 'session0' num2str(s)])
    
    descriptor_file = nptDir('*_descriptor.txt');
    descriptor_info = ReadDescriptor(descriptor_file.name);
    groups = descriptor_info.group(descriptor_info.group ~= 0);
    
    channel_snr_list = zeros(size(groups,2),2);
    channel_snr_list(:,1) = groups;
    
    if isempty(nptDir('skip.txt'))
        cd('highpass');
        if ~exist('SNR_channels.mat','file') || Args.redo
            
            
            if ~isempty(Args.chunks)
                bin_list = nptDir('*.bin');
                bin_name = bin_list.name;
                %get recording informations
                dtype = DaqType(bin_name);
                if strcmp(dtype,'Streamer')
                    [num_channels,sampling_rate,scan_order] = nptReadStreamerFileHeader(bin_name);
                    headersize = 73;
                elseif strcmp(dtype,'UEI')
                    data = ReadUEIFile('FileName',bin_name,'Header');
                    sampling_rate = data.samplingRate;
                    num_channels = data.numChannels;
                    headersize = 90;
                else
                    error('unknown file type')
                end
                
                number_chunks = ceil((bin_list.bytes-headersize)/2/num_channels/(Args.chunks*sampling_rate)); %number of chunks
                total_data_points = (bin_list.bytes-headersize)/2;
                total_data_channel = total_data_points / num_channels;
                
                %make a list of data points to acquire for each chunk
                sample_size = sampling_rate * Args.chunks;
                all_chunks = zeros(number_chunks,2);
                for nc = 1 : number_chunks
                    if nc == 1
                        all_chunks(nc,:) = [1 sample_size];
                    else
                        all_chunks(nc,:) = [(sample_size*(nc-1)+1) sample_size*nc];
                    end
                end
                all_chunks(number_chunks,2) = total_data_channel;
            end
            
            %run one channel at a time
            for ch = 1 : num_channels
                fprintf(1,['Computing SNR for channel ' num2str(ch) ' of ' num2str(num_channels)  '\n'])
                if isempty(Args.chunks)
                    SNR = SignalNoiseMUATrial;%%%%%%%%%%%%%%%%%%%%%
                else
                    SNR = SignalNoiseMUAContinuous('FileName',bin_name,'channel',ch,'chunk_points',all_chunks,'total_points',total_data_channel);
                end
                channel_snr_list(ch,2) = SNR;
            end
            save SNR_channels channel_snr_list
        else
            fprintf(1,[pwd '  SNR complete\n'])
        end
    end
end
cd(daydir)

