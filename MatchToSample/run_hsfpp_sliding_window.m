function run_hsfpp_sliding_window(varargin)

%run at session level
Args = struct('ml',0,'redo',0);
Args.flags = {'ml','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;
cd ..;daydir = pwd;
cd(sesdir)
%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);
bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
window = 200;
steps = [window/2:50:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
stepsacc = [window/2:50:1000-(window/2)];
nw = size(steps,2);

if Args.ml
    c = mtscpp2('auto','ml');
    mt = mtstrial('auto','ML','RTfromML','redosetNames')
%     
%     trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
%     
%     %get only the specified trial indices (correct and stable)
%     alltr{1} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1); %IDENTITY
%     alltr{2} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2); %LOCATION
%     alltr{3} = find(mt.data.CueObj == 55)'; %FIXTRIAL
%     [~,rej] = intersect(get_reject_trials,find(mt.data.CueObj == 55));
%     fixtrials(rej) = [];
%     alltr{4} = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
%     
%     %get 9 stims
%     counter = 4;
%     for objs = 1:3
%         for locs = 1:3
%             counter = counter + 1;
%             alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs,'iCueLoc',locs);
%         end
%     end
%     
%     %3 ides
%     for objs = 1:3
%         counter = counter + 1;
%         alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueObj',objs);
%     end
%     
%     %3 locs
%     for locs = 1:3
%         counter = counter + 1;
%         alltr{counter} = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1,'iCueLoc',locs);
%     end
    
else
    c = mtscpp2('auto');
    mt = mtstrial('auto','redosetNames');
    tr = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
end

[~,pairs] = get(c,'ml','Number'); %use all pairs
pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

for p = 1 : npairs
    cd(daydir)
    
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    for alltypes = 1 : 19
        tr = alltr{alltypes};
        ntrials = size(tr,2);
        hentropy = single(zeros(nfreq,nw,ntrials));
        mphase = single(zeros(nfreq,nw,ntrials));
        
        hentropy_sacc = single(zeros(nfreq,nw,ntrials));
        mphase_sacc = single(zeros(nfreq,nw,ntrials));
        
        counter = 0;
        for t = tr
            counter = counter + 1;
            fprintf(1,['Pair ' num2str(p) ' of ' num2str(npairs) ', Trials to go = ' num2str(ntrials-counter) '\n']);
            for nf = 1 : nfreq
                d = relative_phases(nf,:,t);
                dsacc = relative_phases_sacc(nf,:,t);
                
                %run sliding window for sample locked
                c = 0;
                max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
                for w = steps
                    c = c+1;
                    
                    h = hist(d(w-((window/2)-1):w+(window/2)),bins) ./ 100;%calculate response distribution
                    mp(c) = circ_mean((d(w-((window/2)-1):w+(window/2)) * (2*pi))');
                    
                    re = -1 * nansum(h.*log2(h)); %response entropy (aka shannon entropy) see panzeri_2007
                    resp_ent(c) = (max_ent - re) / max_ent; %normalize so that response is between 0 and 1 with 1 = perfect synchrony (tass_1998)
                    
                    if w <= 900
                        hsacc = hist(dsacc(w-((window/2)-1):w+(window/2)),bins) ./ 100;
                        mpsacc(c) = circ_mean((dsacc(w-((window/2)-1):w+(window/2)) * (2*pi))');
                        
                        resacc = -1 * nansum(hsacc.*log2(hsacc));
                        resp_entsacc(c) = (max_ent - resacc) / max_ent;
                    end
                end
                hentropy(nf,:,counter) = single(resp_ent);
                mphase(nf,:,counter) = single(mp);
                
                hentropysacc(nf,:,counter) = single(resp_entsacc);
                mphasesacc(nf,:,counter) = single(mpsacc);
            end
        end
        hilbertentropy{alltypes} = hentropy;
        mean_phases{alltypes} = mphase;
        
        hilbertentropy_sacc{alltypes} = hentropysacc;
        mean_phases_sacc{alltypes} = mphasesacc;
    end
    groups = pair_list(p,:);
    channel_pairs = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    save(channel_pairs,'hilbertentropy','mean_phases','hilbertentropy_sacc','mean_phases_sacc','all_freq','steps','window','trialorder','groups')
end
cd(sesdir)





















%determine which channels the group number corresponds to
g2ch = str2double(groups(g).name(6:end));
[~,ii] = intersect(NeuroInfo.groups,g2ch);
channels(1) = ii; %these are the channels that the groups correspond to (using NeuroInfo)

%determine if pair has already been written
if Args.ml
    h_pairs = [ 'hilbertpair' num2strpad(g2ch,2) num2strpad(lfpg,2)];
else
    h_pairs = [ 'hilbertpair' num2strpad(channels(1),2) num2strpad(channels(2),2)];
end
cd([sesdir filesep 'lfp' filesep 'lfp2'])
spair = nptdir([h_pairs '.mat']);
cd(sesdir)


cd([sesdir filesep groups(g).name])
%find clusters
clusters = nptDir('cluster*');
nclusters = size(clusters,1);

for c = 1 : nclusters
    cd([sesdir filesep groups(g).name])
    cd(clusters(c).name);
    cluster_info{1,c} = clusters(c).name;
    
    %get spikes
    load ispikes.mat
    %get lfp group number
    lfp_group = channels(2);
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
    %get trials
    lfp = nptDir('*_lfp2*');
    
    s_angle = cell(1,7);
    for ttrial = total_trials
        load(lfp(ttrial).name);
        
        lfp_data = data(lfp_group,:);
        
        %filter 10 : 25 Hz
        [LFP] = nptLowPassFilter(lfp_data,1000,Args.filter_low,Args.filter_high);
        
        spike_data = sp.data.trial(ttrial).cluster.spikes;
        s = ceil(spike_data); %ceil because a zero spike occured when using "round"
        
        %use hilbert transform to determine instantaneous phase angle for each spike
        h = hilbert(LFP);
        ha = angle(h);
        sha = ha(s);
        
        %determine what epoch each spike is in
        s_on = sample_on(ttrial);
        s_off = sample_off(ttrial);
        m = match(ttrial);
        
        % 1: fixation    [(sample_on - 399) : (sample_on)]
        % 2: sample      [(sample_off - 399) : (sample_off)]
        % 3: early delay [(sample_off) : (sample_off + 399)]
        % 4: late delay  [(sample_off + 401) : (sample_off + 800)]
        % 5: delay       [(sample_off + 201) : (sample_off + 800)]
        % 6: delay match [(match - 399) : (match)]
        % 7: full trial  [(sample_on - 500) : (match)]
        
        s_angle{1} = [s_angle{1}, sha(s >= (s_on - 399) & s <= s_on)];
        s_angle{2} = [s_angle{2}, sha(s >= (s_off - 399) & s <= s_off)];
        s_angle{3} = [s_angle{3}, sha(s >= s_off & s < (s_off + 399))];
        s_angle{4} = [s_angle{4}, sha(s > (s_off+400) & s <= (s_off + 800))];
        s_angle{5} = [s_angle{5}, sha(s > (s_off+200) & s <= (s_off + 800))];
        s_angle{6} = [s_angle{6}, sha(s >= (m - 399) & s <= m)];
        %                                 s_angle{7} = [s_angle{7}, sha(s >= (s_on - 500) & s <= m)];
        
    end
    
    for sa = 1 : 7
        if size(s_angle{sa} > 1)
            %                                     c_median(sa) = circ_median(s_angle{sa}');
            c_mean(sa) = circ_mean(s_angle{sa}');
            c_rtest(sa) = circ_rtest(s_angle{sa});
            nspikes(sa) = size(s_angle{sa},2);
        else
            %                                     c_median(sa) = nan;
            c_mean(sa) = nan;
            c_rtest(sa) = nan;
            nspikes(sa) = nan;
        end
    end
    %                             cluster_med{1,c} = c_median;
    cluster_mean{1,c} = c_mean;
    cluster_rtest{1,c} = c_rtest;
    cluster_nspikes{1,c} = nspikes;
end
%                         hilbert_groups.median = cluster_med;
hilbert_groups.mean = cluster_mean;
hilbert_groups.rtest = cluster_rtest;
hilbert_groups.nspikes = cluster_nspikes;
if Args.ml
    hilbert_groups.channels = [g2ch lfpg];
else
    hilbert_groups.channels = channels;
end
hilbert_groups.cluster_info = cluster_info;

write_info = writeinfo(dbstack);
save(h_pairs,'hilbert_groups','write_info')




cd(sesdir)




