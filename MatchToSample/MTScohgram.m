function [C,phi,S12,S1,S2,t,f,varargout] = MTScohgram(mts,trials,pairs,varargin)
% pairs in channels number
% surrogate is generalized only and
Args = struct('tapers',[2 3],'Fs',200,'fpass',[0 50],'matchAlign',0,'reference',[],'mwind',[0.2 0.05],'err',[2 0.05],'forTuning',0,'removeEP',0,'beforeMatch',1800,'surrogate',0,'nrep',1000,'Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2,'sessions',[],'sesOrder',[],'ML',0,'fakeses',[],'GCgram',0,'appendS',0);
Args.flags = {'matchAlign','forTuning','removeEP','surrogate','ML','GCgram','appendS'};
[Args,modvarargin] = getOptArgs(varargin,Args);

params = struct('tapers',Args.tapers,'Fs',Args.Fs,'fpass',Args.fpass,'trialave',1,'err',Args.err);

%%
if isempty(Args.sessions)
    rightdir = true;
    nses = 1;
    ses = 1;
    
else
    rightdir = false;
    ses = [];
    for s = 1 : length(Args.sessions)
        if ~isempty(nptDir(sprintf('session0%d',Args.sessions(s))))
            
            ses = [ses Args.sessions(s)];
        end
    end
    nses = length(ses);
end

if ~isempty(Args.sesOrder)
    if Args.ML; ses = [1 1]; else ses = [2 3]; end
    nses = 1;
    for ss = 1 : 2; sessions{ss} = sprintf('session0%d',ses(ss)); end
end
if isempty(Args.reference)
    if Args.matchAlign
        
        before = Args.beforeMatch;
        theref = 'MatchOnset';
    else
        
        before = 1000;
        theref = 'CueOffset';
    end
    
else
    theref = Args.reference{1};
    before = Args.reference{2};
    after1 = Args.reference{3};
end
channels = unique(unique(pairs));


ddir = pwd;

ltrial = [];
saccades = [];
if exist('ses')
    for s = 1 : nses
        if ~rightdir
            cd(sprintf('session0%d',ses(s)))
        end
        
        
        if isempty(Args.sesOrder)
            cd lfp
            files = nptDir('*_lfp.*');
        else
            for ss = 1 : 2
                cd(ddir)
                cd(sprintf('session0%d',ses(ss)))
                cd lfp
                thefiles = struct2cell(nptDir('*_lfp.*'));
                sesfiles{ss} = cell2mat(thefiles(1,:)');
            end
        end
        c = 1;
        newpairs = zeros(size(pairs));
        if iscell(trials)
            
            thetrials = trials{s};
            themts =mts{s};
            
        else
            if isempty(Args.sesOrder)
                
                themts = mts;
            end
            thetrials = trials;
        end
        if ~isempty(thetrials)
            for ch = vecr(channels)
                if s == 1 || ~iscell(trials)
                    tt = 1;
                else
                    tt = length(trials{s-1}) + 1;
                end
                indc = 1;
                for t = vecr(thetrials)
                    if isempty(Args.sesOrder)
                        [lfp,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(files(t).name,ch);
                    else
                        cd(ddir)
                        cd(sessions{Args.sesOrder{Args.fakeses}(indc)})
                        cd lfp
                        [lfp,num_channels,sampling_rate,scan_order,points] = nptReadStreamerChannel(sesfiles{Args.sesOrder{Args.fakeses}(indc)}(t,:),ch);
                        themts =mts{Args.sesOrder{Args.fakeses}(indc)};
                    end
                    
                    ref = eval(sprintf('themts.data.%s(t)',theref));
                    
                    if exist('after1');after = ref+ after1;else; after = length(lfp); end
                    [dlfp{tt}(c,:),SR] = preprocessinglfp(lfp(round(ref-before) : after),modvarargin{:},'detrend');
                    ltrial = [ltrial size(dlfp{tt},2)];
                    tt = tt + 1;
                    if Args.matchAlign; toadd = 0; else toadd = themts.data.MatchOnset(t) - themts.data.CueOffset(t);end
                    saccades = [saccades round((toadd + before + themts.data.FirstSac(t))/(sampling_rate/SR))];
                    indc = indc + 1;
                end
                
                newpairs = newpairs + (pairs == ch).*c; %(a == 4).* 1 + (a == 7).*2 + (a == 8).*3 + (a == 10).*4 + (a == 11).*5
                c = c + 1;
            end
            varargout{1} = before;
        end
        cd ..
        if ~rightdir
            cd ..
        end
    end
    minlength = min(ltrial);
    for t = 1 : size(dlfp,2)
        data(:,:,t) = dlfp{t}(:,1:minlength);
        
    end
    %% removal of the EP
    if Args.removeEP
        for c = 1 : length(channels)
            data(c,:,:) = squeeze(data(c,:,:)) - repmat(squeeze(mean(data(c,:,:),3))',1,size(data,3));
        end
    end
    
    %%
    if Args.forTuning
        params.trialave = 1;
        params.err = [2 0.05];
        for np = 1 :size(pairs,1)
            [C(np,:,:),phi(np,:,:),S12,S1,S2,t,f,confC(np),phistd(np,:,:,:),Cerr(np,:,:,:)] = cohgramc(squeeze(data(newpairs(np,1),:,:)),squeeze(data(newpairs(np,2),:,:)),Args.mwind,params);
            if nargout > 7
                params.trialave = 0;
                for ch = 1 : 2;[S(np,ch,:,:,:),t,f] = mtspecgramc(squeeze(data(newpairs(np,ch),:,:)),Args.mwind,params); end
                
            end
        end
        varargout{2} = S;
        varargout{3} = squeeze(phistd);
        varargout{4} = squeeze(Cerr);
        
    elseif Args.surrogate
        ntrials = size(dlfp,2);
        
        for rep = 1 : Args.nrep
            ordT1 = randperm(ntrials);
            ordT2 = randperm(ntrials);
            thech = [1; 1];
            while diff(thech) == 0
                thech = randi(size(data,1),[2,1]);
            end
            sur1 = data(thech(1),:,ordT1);
            sur2 = data(thech(2),:,ordT2);
            [C(rep,:,:),phi(rep,:,:),S12,S1,S2,t,f] = cohgramc(squeeze(sur1),squeeze(sur2),Args.mwind,params);
        end
        for ti = 1 : size(C,2)
            [~,~,thresh(ti,:,:),~] = fitSurface(squeeze(C(:,ti,:)),f,'Prob',Args.Prob,'FSpread',Args.FSpread,modvarargin{:});
        end
        varargout{2} = thresh;
    else
        
        if Args.GCgram
            for np = 1 :size(pairs,1)
                [C(np,:,:),phi(np,:,:),S12(np,:,:),t,f] = GCgram(squeeze(data(newpairs(np,1),:,:)),squeeze(data(newpairs(np,2),:,:)),params,Args.mwind,20);
            end
            C = squeeze(C);
            phi = squeeze(phi);
            S12 = squeeze(S12);
            S1  = [];
            S2 = [];
        else
            for np = 1 :size(pairs,1)
                if params.err(1,1) == 0
                   [C(np,:,:),phi(np,:,:),S12,S1,S2,t,f] = cohgramc(squeeze(data(newpairs(np,1),:,:)),squeeze(data(newpairs(np,2),:,:)),Args.mwind,params);
                 confC= [];phistd=[];Cerr=[];
                else
                    if Args.appendS
                         [C(np,:,:),phi(np,:,:),S12,S1,S2,t,f] = cohgramc(squeeze(data(newpairs(np,1),:,:)),squeeze(data(newpairs(np,2),:,:)),Args.mwind,params);
                   
                    else
                        [C(np,:,:),phi(np,:,:),S12,S1,S2,t,f,confC(np),phistd(np,:,:,:),Cerr(np,:,:,:)] = cohgramc(squeeze(data(newpairs(np,1),:,:)),squeeze(data(newpairs(np,2),:,:)),Args.mwind,params);
                    end
                end
            end
            varargout{2} = confC;
            varargout{3} = squeeze(phistd);
            varargout{4} = squeeze(Cerr);
        end
        
    end
end
cd(ddir)

