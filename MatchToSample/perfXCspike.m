function [allR,P] = perfXCspike(alltrials,sp1,sp2,mts,rc,varargin)

Args = struct('nTrials',50,'iterations',10000,'not1trialstep',0);
Args.flags = {'not1trialstep'};
[Args,~] = getOptArgs(varargin,Args);


tcomp = [1 1;1 2; 1 3; 2 2];

perf = nan(length(alltrials) - Args.nTrials,1);
for tbin = 1 : length(alltrials) - Args.nTrials
    strials = alltrials(tbin : Args.nTrials  + tbin);
    perf(tbin) = 100*sum(mts.data.BehResp(strials)) / Args.nTrials;
end

allR = nan(length(tcomp),Args.iterations+1);
P = ones(length(tcomp),1);
for tc = 1 : length(tcomp)
    for ii = 1 : Args.iterations + 1
        if ii == Args.iterations + 1
            if Args.not1trialstep
                [Rt,Pt]=corrcoef(perf(1:Args.nTrials:end),rc(1:Args.nTrials:end,tcomp(tc,1),tcomp(tc,2)));
            else
                [Rt,Pt]=corrcoef(perf,rc(:,tcomp(tc,1),tcomp(tc,2)));
            end
            allR(tc,ii) = Rt(1,2);
            P(tc) = Pt(1,2);
        else
            iperf = perf(randperm(length(perf)));
            if Args.not1trialstep
                [Rt,~]=corrcoef(iperf(1:Args.nTrials:end),rc((1:Args.nTrials:end),tcomp(tc,1),tcomp(tc,2)));
            else
                [Rt,~]=corrcoef(iperf,rc(:,tcomp(tc,1),tcomp(tc,2)));
            end
            allR(tc,ii) = Rt(1,2);
            
        end
    end
end
