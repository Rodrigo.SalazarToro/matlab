function SNR = SignalNoiseMUA(varargin)

%     Run at session level
%     Reads highpass files
%     SignalNoise('P1', V1, 'P2', V2,...)
%        Valid Property Names (P1, P2,...) and Property Values (V1, V2,...)
%        include:
%           P-Values      V-Values
%           FileName   -  'FileName.bin' or leave blank to cue user
%           Channels   -  [channel indices] or type {'Select'} to manual select indivdual channels
%           Units      -  'Volts', 'MilliVolts', {'MicroVolts'},  or  'Daqunits'
%           AmpGain    -  {5000} or specify the total amplification, MCC main amp is 500X and the MCS Head Stage is 10X
%           HighRange  -  {[500 10000]} frequency band for filtering
%           std        -  indicates the standard deviation to be used
%           mt         -  if true then the mts trial object is being passed
%           concat     -  concatenates all trials and then runs SNR
%
%        The default values for the Units properties are indicated by braces {}.
%
%        SNR = range(Signal)/range(Noise)
%
%        The function will return a structure called SNR containing many SNR's and
%        signal and noise values.

global mt
% Get Input Arguments
Args = struct('FileName',[],'Channels',[],'AmpGain',5000,'samplingRate',30000,'std',4,'concat',0,'monkey','betty','annie_semichronic',0,'chunk_points',[]);
Args = getOptArgs(varargin,Args,'flags',{'concat','annie_semichronic'});

%determine if their are fixation trials and find their indices
%CueObj 55 is the fixation cue object
fix_trials = find(mt.data.CueObj == 55);

if Args.concat
    trial_list = nptDir([Args.monkey '*']);
    data = [];
    numb_trials = size(trial_list,1);
    time = [];
    for t = 1 : numb_trials
        numb_trials - t
        filename = trial_list(t).name;
        t_start = (mt.data.CueOnset(t) - 500)/1000; %Cue - 500ms
        t_end = (mt.data.MatchOnset(t) + mt.data.FirstSac(t))/1000;% Match + FirstSac
        d = single(nptReadStreamerFile(filename)) / Args.AmpGain; %convert to single
        %Cue - 500ms : Match + FirstSac
        data = [data d(:,(floor((Args.samplingRate*t_start)):floor((Args.samplingRate*t_end))))];
    end
else
    %get filename
    filename = Args.FileName;
    trial_num =  str2double(filename(end-3:end));
    
    if ~Args.annie_semichronic
        t_start = (mt.data.CueOnset(trial_num) - 500)/1000; %Cue - 500ms
        if isempty(intersect(fix_trials, trial_num))
            t_end = (mt.data.MatchOnset(trial_num) + mt.data.FirstSac(trial_num))/1000;% Match + FirstSac
        else
            t_end = (mt.data.MatchOnset(trial_num))/1000;% Match
        end
        data = nptReadStreamerFile(filename) / Args.AmpGain;
        %Cue - 500ms : Match + FirstSac
        try
            data = data(:,(floor((Args.samplingRate*t_start)):floor((Args.samplingRate*t_end))));
        catch
            fprintf(1,['Warning: Unable to use trial number ' num2str(trial_num) '\n']);
        end
    else
        if isempty(Args.chunk_points)
            data = nptReadStreamerFile(filename) ;%/ Args.AmpGain;
        else
            data = nptReadStreamerFileChunk(filename,Args.chunk_points);% / Args.AmpGain;
        end
    end
end

%all_noise=detrend(data,'constant');
all_noise = data;

if size(data,1) ~= size(Args.Channels,2) 
   fprintf(1,'INCORRECT NUMBER OF CHANNELS')
end

for c = 1 : size(data,1)                                       
    
    noise = all_noise(c,:);
    signal = [];
    ind = 1;
    while ~isempty(ind)
        [ind] = find(noise>(nanmean(noise)+Args.std*nanstd(noise)) | noise<(nanmean(noise)-Args.std*nanstd(noise)));
        if ~isempty(ind)
            signal = [signal noise(ind)];
            noise(ind) = [];
        end
    end
    
    
    % SNR is the Ratio of the RMS or standard deviation of the signal, in
    % this case the spike amplitudes, to the RMS or standard deviation of
    % the noise signal.
    
    %%% Calculate the Noise Values %%%%
    SNR.Positive_Noise(c) = max(noise);
    SNR.Negative_Noise(c) = min(noise);
    SNR.Mean_Noise_Level(c) = nanmean(noise);
    SNR.Positive_Noise_99(c) = SNR.Mean_Noise_Level(c)+(Args.std*nanstd(noise));
    SNR.Negative_Noise_99(c) = SNR.Mean_Noise_Level(c)-(Args.std*nanstd(noise));
    %%% Calculate the Signal Values %%%%
    SNR.Positive_Signal_99(c) = prctile(signal(signal>SNR.Mean_Noise_Level(c)),99);
    SNR.Negative_Signal_99(c) = prctile(signal(signal<SNR.Mean_Noise_Level(c)),1);
    %%% Calculate the SNR Values %%%%
    SNR.SNR_99_99(c) = (range([SNR.Positive_Signal_99(c) SNR.Negative_Signal_99(c)]))/(range([SNR.Positive_Noise_99(c) SNR.Negative_Noise_99(c)]));
    SNR.SNR_99_Max_Min(c) = (range([SNR.Positive_Signal_99(c) SNR.Negative_Signal_99(c)]))/(range([SNR.Positive_Noise(c) SNR.Negative_Noise(c)]));
    SNR.Signal_Values{c} = signal;
end

