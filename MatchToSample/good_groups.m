function [groups chpairs gpairs] = good_groups(varargin)

%Makes a list of good groups
%criteria:
%       -Not noisy
%       -SNR is above a specified threshold
%run at day level

Args = struct('snr',1.8);
Args.flags = {};
Args = getOptArgs(varargin,Args);

daydir = pwd;
sessions = nptDir('session*');

cd(sessions(1).name) %cd to first session (sometimes this is session02)

[channels,comb,CHcomb,g] = checkChannels('cohInter'); %these are the groups that are not noisy

num_groups = size(g,2);

cd('highpass')
load SNR_channels
snr_groups = channel_snr_list(:,1);
snrs = channel_snr_list(:,2);

keep = 0;
for x = 1 : num_groups
    i = find(g(x) == snr_groups);
    s = snrs(i);
    if s >= Args.snr
        keep = keep + 1;
        groups(keep) = g(x);
        channels(keep) = x;
    end
end

ngroups = size(groups,2);
p = 0;
for g1 = 1 : ngroups
    for g2 = (g1+1) : ngroups
        p = p + 1;
        gpairs(p,:) = [groups(g1) groups(g2)];
        chpairs(p,:) = [channels(g1) channels(g2)];
    end
end


cd(daydir)

