function average_correlograms(varargin)

%computes average correlogram for 7 different time periods
%
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]
% 8: ititrial [(match + 501) : (match + 900)], for incorrect, betty trials only
% 9: ititrial2 [(match + 901) : (match + 1300)], for incorrect, betty trials only

Args = struct('ml',0,'bmf',0,'rules',[],'correct_iti',0,'bettynlynx',0);
Args.flags = {'ml','bmf','correct_iti','bettynlynx'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.ml
    if Args.bmf
        N = NeuronalHist('bmf');
        [~,chpairlist,~,~,sortedpairs] = bmf_groups('ml');
    else
        if Args.bettynlynx
            N = NeuronalHist('ml','bettynlynx');
            [~,sortedpairs,~,chpairlist] = sorted_groups('ml','bettynlynx');
        else
            N = NeuronalHist('ml');
            [~,sortedpairs,~,chpairlist] = sorted_groups('ml');
        end
    end
    
else
    N = NeuronalHist;
    [~,sortedpairs,~,chpairlist] = sorted_groups;
end
chnumb = N.chnumb;



cd([sesdir filesep 'lfp']);
if exist('iti_rejectedTrials.mat') %this is currently only run on incorrect trials for betty days
    load('iti_rejectedTrials.mat','rejecTrials')
    iti_reject = rejecTrials;
else
    iti_reject = [];
end

if exist([sesdir filesep 'lfp' filesep 'lfp2'])
    cd([sesdir filesep 'lfp' filesep 'lfp2'])
else
    cd([sesdir filesep 'lfp'])
    mkdir lfp2
end

lfp2dir = pwd;

%get index of trials
lfpdata = nptDir('*_lfp2.*');

cd(sesdir)
numpairs = nchoosek(N.chnumb,2);
all_correlograms = cell(7,numpairs); %seven combinations
all_auto_correlograms1 = cell(7,numpairs); %seven combinations
all_auto_correlograms2 = cell(7,numpairs); %seven combinations

if Args.ml
    
    if Args.bmf
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx'); %incorrect/identity
    else
        mt = mtstrial('auto','ML','RTfromML','redosetNames','save','redo');
        %get only the specified trial indices (correct and stable)
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',1);
        location = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',2);
        fixtrials = find(mt.data.CueObj == 55)';
        [~,rej] = intersect(find(mt.data.CueObj == 55),get_reject_trials);
        fixtrials(rej) = [];
        incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','rule',1); %incorrect/identity
        
        %get rid of incorrect trials with bad itis if the artifact
        %detection has been run on it
        if ~isempty(iti_reject)
            [~,baditi] = intersect(incorrtrials,iti_reject);
            incorrtrials(baditi) = [];
        end
    end
else
    mt=mtstrial('auto','redosetNames');
    %get only the specified trial indices (correct and stable)
    tr = mtsgetTrials(mt,'BehResp',1,'stable');
end

%get trial timing information
%all trials are aligned to sample off (sample off is 1000)
sample_on = floor(mt.data.CueOnset);%computes the surrogate thresholds for the average correlogramsoor(mt.data.CueOnset);   %sample on
sample_off = floor(mt.data.CueOffset); %sample off
match = floor(mt.data.MatchOnset);    %match

if isempty(Args.rules)
    if Args.ml
        if Args.bmf
            rules = [1 4];
        else
            rules = [2 3]; %3 and 4 are used for the fixation trials and the incorrect trials
        end
    else
        rules = [1];
    end
else
    rules = Args.rules;
end

for rule = rules
    if Args.ml
        if rule == 1
            trials = identity;
        elseif rule == 2
            trials = location;
        elseif rule == 3
            trials = fixtrials;
        elseif rule == 4
            trials = incorrtrials;
        end
    else
        trials = tr;
    end
    
    if ~isempty(trials)
        cd(lfp2dir)
        tcounter = 0;
        for x = trials
            load ([lfpdata(x).name],'data');
            tcounter = tcounter + 1;
            s_on = sample_on(x);
            s_off = sample_off(x);
            m = match(x);
            
            fix = data(:,[(s_on - 399) : (s_on)]);
            sample = data(:,[(s_off - 399) : (s_off)]);
            delay400 = data(:,[(s_off) : (s_off + 399)]);
            delay800 = data(:,[(s_off + 401) : (s_off + 800)]);
            delay = data(:,[(s_off + 201) : (s_off + 800)]);
            delaymatch = data(:,[(m - 399) : (m)]);
            fulltrial = data(:,[(s_on - 500) : (m)]);
            
            if rule == 4 || Args.bmf
                %                 ititrial = data(:,[(m+701) : (m+1300)]);
                ititrial = data(:,[(m+501) : (m+900)]);
                ititrial2 = data(:,[(m+901) : (m+1300)]);
            elseif rule == 1 && Args.correct_iti
                ititrial = data(:,[(m+1001) : (m+1600)]); %add 300 ms to account for the match fixating period
            end
            
            counter = 0;
            for sp = sortedpairs
                if Args.bmf
                    counter = counter + 1;
                    c1 = chpairlist(counter,1);
                    c2 = chpairlist(counter,2);
                else
                    c1 = chpairlist(sp,1);
                    c2 = chpairlist(sp,2);
                end
                
                [f,lags] = xcorr(fix(c1,:),fix(c2,:),50,'coef');
                [s,lags] = xcorr(sample(c1,:),sample(c2,:),50,'coef');
                [d400,lags] = xcorr(delay400(c1,:),delay400(c2,:),50,'coef');
                [d800,lags] = xcorr(delay800(c1,:),delay800(c2,:),50,'coef');
                [d,lags] = xcorr(delay(c1,:),delay(c2,:),50,'coef');
                [dm,lags] = xcorr(delaymatch(c1,:),delaymatch(c2,:),50,'coef');
                [full,lags] = xcorr(fulltrial(c1,:),fulltrial(c2,:),50,'coef');
                
                all_correlograms{1,sp}(tcounter,:) =  f;
                all_correlograms{2,sp}(tcounter,:) =  s;
                all_correlograms{3,sp}(tcounter,:) =  d400;
                all_correlograms{4,sp}(tcounter,:) =  d800;
                all_correlograms{5,sp}(tcounter,:) =  d;
                all_correlograms{6,sp}(tcounter,:) =  dm;
                all_correlograms{7,sp}(tcounter,:) =  full;
                
                if rule == 4 || (rule == 1 && Args.correct_iti) || Args.bmf
                    [iti,lags] = xcorr(ititrial(c1,:),ititrial(c2,:),50,'coef');
                    all_correlograms{8,sp}(tcounter,:) =  iti;
                    
                    [iti2,lags] = xcorr(ititrial2(c1,:),ititrial2(c2,:),50,'coef');
                    all_correlograms{9,sp}(tcounter,:) =  iti2;
                end
                
                
                %AUTO
                %calculate autocorrelograms to get the main frequency
                [f,lags] = xcorr(fix(c1,:),fix(c1,:),50,'coef');
                [s,lags] = xcorr(sample(c1,:),sample(c1,:),50,'coef');
                [d400,lags] = xcorr(delay400(c1,:),delay400(c1,:),50,'coef');
                [d800,lags] = xcorr(delay800(c1,:),delay800(c1,:),50,'coef');
                [d,lags] = xcorr(delay(c1,:),delay(c1,:),50,'coef');
                [dm,lags] = xcorr(delaymatch(c1,:),delaymatch(c1,:),50,'coef');
                [full,lags] = xcorr(fulltrial(c1,:),fulltrial(c1,:),50,'coef');
                
                all_auto_correlograms1{1,sp}(tcounter,:) =  f;
                all_auto_correlograms1{2,sp}(tcounter,:) =  s;
                all_auto_correlograms1{3,sp}(tcounter,:) =  d400;
                all_auto_correlograms1{4,sp}(tcounter,:) =  d800;
                all_auto_correlograms1{5,sp}(tcounter,:) =  d;
                all_auto_correlograms1{6,sp}(tcounter,:) =  dm;
                all_auto_correlograms1{7,sp}(tcounter,:) =  full;
                
                if rule == 4 || (rule == 1 && Args.correct_iti) || Args.bmf
                    [iti,lags] = xcorr(ititrial(c1,:),ititrial(c1,:),50,'coef');
                    all_auto_correlograms1{8,sp}(tcounter,:) =  iti;
                    
                    [iti2,lags] = xcorr(ititrial2(c1,:),ititrial2(c1,:),50,'coef');
                    all_auto_correlograms1{9,sp}(tcounter,:) =  iti2;
                end
                
                
                [f,lags] = xcorr(fix(c2,:),fix(c2,:),50,'coef');
                [s,lags] = xcorr(sample(c2,:),sample(c2,:),50,'coef');
                [d400,lags] = xcorr(delay400(c2,:),delay400(c2,:),50,'coef');
                [d800,lags] = xcorr(delay800(c2,:),delay800(c2,:),50,'coef');
                [d,lags] = xcorr(delay(c2,:),delay(c2,:),50,'coef');
                [dm,lags] = xcorr(delaymatch(c2,:),delaymatch(c2,:),50,'coef');
                [full,lags] = xcorr(fulltrial(c2,:),fulltrial(c2,:),50,'coef');
                
                all_auto_correlograms2{1,sp}(tcounter,:) =  f;
                all_auto_correlograms2{2,sp}(tcounter,:) =  s;
                all_auto_correlograms2{3,sp}(tcounter,:) =  d400;
                all_auto_correlograms2{4,sp}(tcounter,:) =  d800;
                all_auto_correlograms2{5,sp}(tcounter,:) =  d;
                all_auto_correlograms2{6,sp}(tcounter,:) =  dm;
                all_auto_correlograms2{7,sp}(tcounter,:) =  full;
                
                if rule == 4 || (rule == 1 && Args.correct_iti) || Args.bmf
                    [iti,lags] = xcorr(ititrial(c2,:),ititrial(c2,:),50,'coef');
                    all_auto_correlograms2{8,sp}(tcounter,:) =  iti;
                    
                    [iti2,lags] = xcorr(ititrial2(c2,:),ititrial2(c2,:),50,'coef');
                    all_auto_correlograms2{9,sp}(tcounter,:) =  iti2;
                end
            end
        end
        
        n = size(all_correlograms,1);
        all_phase = cell(n,numpairs);
        all_corrcoef = cell(n,numpairs);
        avg_correlograms = cell(n,numpairs); %this is the mean
        std_correlograms = cell(n,numpairs);
        pvals_correlograms = cell(n,numpairs);
        
        all_auto_phase1 = cell(n,numpairs);
        all_auto_corrcoef1 = cell(n,numpairs);
        avg_auto_correlograms1 = cell(n,numpairs); %this is the mean
        std_auto_correlograms1 = cell(n,numpairs);
        pvals_auto_correlograms1 = cell(n,numpairs);
        
        all_auto_phase2 = cell(n,numpairs);
        all_auto_corrcoef2 = cell(n,numpairs);
        avg_auto_correlograms2 = cell(n,numpairs); %this is the mean
        std_auto_correlograms2 = cell(n,numpairs);
        pvals_auto_correlograms2 = cell(n,numpairs);
        
        for npairs = sortedpairs
            for epochs = 1 : n
                %                 subplot(1,7,epochs)
%                 boxplot(all_correlograms{epochs,npairs})
%                 
                %average correlograms by the number of trials
                acorr = mean(all_correlograms{epochs,npairs});
                avg_correlograms{epochs,npairs} = acorr;
                std_correlograms{epochs,npairs} = std(all_correlograms{epochs,npairs});
                [~,allpvals] = ttest(all_correlograms{epochs,npairs});
                
                pvals_correlograms{epochs,npairs} = allpvals;
                
                %Find central peak
                [peak_corrcoef peak_phase] = find_correlogram_peak('correlogram',acorr);
                
                all_phase{epochs,npairs} = peak_phase;
                all_corrcoef{epochs,npairs} = peak_corrcoef;
                
                
                %AUTO
                %calculate peak and phase for autocorrelograms
                %average correlograms by the number of trials
                acorr = mean(all_auto_correlograms1{epochs,npairs});
                avg_auto_correlograms1{epochs,npairs} = acorr;
                std_auto_correlograms1{epochs,npairs} = std(all_auto_correlograms1{epochs,npairs});
                [~,allpvals] = ttest(all_auto_correlograms1{epochs,npairs});
                pvals_auto_correlograms1{epochs,npairs} = allpvals;
                %Find central peak
                [peak_corrcoef peak_phase] = find_correlogram_peak('correlogram',acorr);
                all_auto_phase1{epochs,npairs} = peak_phase;
                all_auto_corrcoef1{epochs,npairs} = peak_corrcoef;
                
                %average correlograms by the number of trials
                acorr = mean(all_auto_correlograms2{epochs,npairs});
                avg_auto_correlograms2{epochs,npairs} = acorr;
                std_auto_correlograms2{epochs,npairs} = std(all_auto_correlograms2{epochs,npairs});
                [~,allpvals] = ttest(all_auto_correlograms2{epochs,npairs});
                pvals_auto_correlograms2{epochs,npairs} = allpvals;
                %Find central peak
                [peak_corrcoef peak_phase] = find_correlogram_peak('correlogram',acorr);
                all_auto_phase2{epochs,npairs} = peak_phase;
                all_auto_corrcoef2{epochs,npairs} = peak_corrcoef;
            end
        end
        
        write_info = writeinfo(dbstack);
        if Args.ml
            if rule == 1;
                mkdir('identity')
                cd('identity')
                save all_correlograms avg_correlograms std_correlograms pvals_correlograms all_phase all_corrcoef write_info
                save all_auto_correlograms1 avg_auto_correlograms1 std_auto_correlograms1 pvals_auto_correlograms1 all_auto_phase1 all_auto_corrcoef1 write_info
                save all_auto_correlograms2 avg_auto_correlograms2 std_auto_correlograms2 pvals_auto_correlograms2 all_auto_phase2 all_auto_corrcoef2 write_info
            elseif rule == 2;
                mkdir('location')
                cd('location')
                save all_correlograms avg_correlograms std_correlograms pvals_correlograms all_phase all_corrcoef write_info
                save all_auto_correlograms1 avg_auto_correlograms1 std_auto_correlograms1 pvals_auto_correlograms1 all_auto_phase1 all_auto_corrcoef1 write_info
                save all_auto_correlograms2 avg_auto_correlograms2 std_auto_correlograms2 pvals_auto_correlograms2 all_auto_phase2 all_auto_corrcoef2 write_info
            elseif rule == 3
                mkdir('fixation')
                cd('fixation')
                save all_correlograms avg_correlograms std_correlograms pvals_correlograms all_phase all_corrcoef write_info
                save all_auto_correlograms1 avg_auto_correlograms1 std_auto_correlograms1 pvals_auto_correlograms1 all_auto_phase1 all_auto_corrcoef1 write_info
                save all_auto_correlograms2 avg_auto_correlograms2 std_auto_correlograms2 pvals_auto_correlograms2 all_auto_phase2 all_auto_corrcoef2 write_info
            elseif rule == 4
                mkdir('incorrect')
                cd('incorrect')
                save all_correlograms avg_correlograms std_correlograms pvals_correlograms all_phase all_corrcoef write_info
                save all_auto_correlograms1 avg_auto_correlograms1 std_auto_correlograms1 pvals_auto_correlograms1 all_auto_phase1 all_auto_corrcoef1 write_info
                save all_auto_correlograms2 avg_auto_correlograms2 std_auto_correlograms2 pvals_auto_correlograms2 all_auto_phase2 all_auto_corrcoef2 write_info
            end
        else
            save all_correlograms avg_correlograms std_correlograms pvals_correlograms all_phase all_corrcoef write_info
            save all_auto_correlograms1 avg_auto_correlograms1 std_auto_correlograms1 pvals_auto_correlograms1 all_auto_phase1 all_auto_corrcoef1 write_info
                save all_auto_correlograms2 avg_auto_correlograms2 std_auto_correlograms2 pvals_auto_correlograms2 all_auto_phase2 all_auto_corrcoef2 write_info
        end
    end
end


cd(sesdir)

fprintf(1,'\n')
fprintf(1,'Delay Correlograms Done.\n')


