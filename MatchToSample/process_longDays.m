load longdays.mat
alldays = [lbdays lcdays];

MTSlfp3('redo','save','cohInter','days',{alldays{:}},'ML','BehResp',1,'stable','RTfromML','bandP',[5 90],'separateDelays',1,'delay',[900 1666])

MTSlfp3('redo','save','cohInter','days',{alldays{:}},'ML','BehResp',1,'stable','RTfromML','bandP',[5 90],'separateDelays',2,'delay',[1667 2333])

MTSlfp3('redo','save','cohInter','days',{alldays{:}},'ML','BehResp',1,'stable','RTfromML','bandP',[5 90],'separateDelays',3,'delay',[2334 3100])
