function r = plus(p,q,varargin)

% get name of class
classname = mfilename('class');

% check if first input is the right kind of object
if(~isa(p,classname))
	% check if second input is the right kind of object
	if(~isa(q,classname))
		% both inputs are not the right kind of object so create empty
		% object and return it
		r = feval(classname);
	else
		% second input is the right kind of object so return that
		r = q;
	end
else
	if(~isa(q,classname))
		% p is the right kind of object but q is not so just return p
		r = p;
    elseif(isempty(p))
        % p is right object but is empty so return q, which should be
        % right object
        r = q;
    elseif(isempty(q))
        % p are q are both right objects but q is empty while p is not
        % so return p
        r = p;
	else
		% both p and q are the right kind of objects so add them 
        % together
        % assign p to r so that we can be sure we are returning the right
        % object
        r = p;
        
        r.data.means = [p.data.means; q.data.means];
        r.data.se = [p.data.se; q.data.se];
        
        r.data.Index = [p.data.Index; q.data.Index];
        
        r.data.global_surrogates = [p.data.global_surrogates; q.data.global_surrogates];
        r.data.sig_stas = [p.data.sig_stas; q.data.sig_stas];
        r.data.peak_sign = [p.data.peak_sign; q.data.peak_sign];
        
        
        
        r.data.setNames = [p.data.setNames; q.data.setNames];
        
        % useful fields for most objects
        r.data.numSets = p.data.numSets + q.data.numSets;
        
		% add nptdata objects as well
		r.nptdata = plus(p.nptdata,q.nptdata);
	end
end
