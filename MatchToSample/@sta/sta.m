function obj = sta(varargin)

%creates sta object
%run at session level

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'ml',0,'ide_only',1);
Args.flags = {'Auto','ml'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'sta';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'sta';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        
        %loads objecet if it already exists
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end



function obj = createObject(Args,varargin)
sdir = pwd;

%make mts trial object
if Args.ml
    mtst = mtstrial('auto','ML','RTfromML','redosetNames');
else
    mtst = mtstrial('auto','redosetNames');
end


if ~isempty(mtst)
    if Args.ml %could have a session without any correct/stable trials (clark 060406 session03)
        if Args.ide_only
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML','rule',1);
        else
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','ML');
        end
    else
        if Args.ide_only
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable','rule',1);
        else
            all_trials = mtsgetTrials(mtst,'BehResp',1,'stable');
        end
    end
end

%for monkeylogic sessions, only session01 should be used
match2sample = 0;
if ((sdir(end) == '1') && (Args.ml)) || ~Args.ml
    match2sample = 1;
end

sets = 0;
if ~isempty(mtst) && match2sample && ~isempty(all_trials)
    
    N = NeuronalHist;
    %go to lfp2 dir
    cd([sdir filesep 'lfp' filesep 'lfp2'])
    lfp2dir = pwd;
    %get list of stapairs
    
%     !rm lfpstapair_stand*
    
    sta_pairs = nptDir('lfpstapair*.mat');
    
    numb_pairs = size(sta_pairs,1);
    %run through all pairs
    for sp = 1:numb_pairs
        
        load(sta_pairs(sp).name)
       
        ch1 = sta_groups.channels(1);
        ch2 = sta_groups.channels(2);
        c = size(sta_groups.cluster_info,2);
        %EACH CLUSTER GETS ITS OWN ENTRY
        %run through the clusters for each group
        for x = 1 : c; %this is the number of clusters the reference channel has
            sets = sets + 1;

            data.means{sets,1} = sta_groups.mean{x};
            data.se{sets,1} = sta_groups.se{x};
            
            data.Index(sets,1) = ch1;
            data.Index(sets,2) = ch2;
            
            %INDEX 3:determine if cluster is multi or single (multi = 1, single = 2,3 etc.)
            if strmatch(sta_groups.cluster_info{x},'cluster01m')
                data.Index(sets,3) = 1;
            else
                data.Index(sets,3) = x; %single units
            end
            
            %INDEX 16-22: spike count
            spike_counts = sta_groups.spike_counts{x};
            if isempty(spike_counts)
                spike_counts = zeros(1,7);
            end
            data.Index(sets,16) = spike_counts(1);
            data.Index(sets,17) = spike_counts(2);
            data.Index(sets,18) = spike_counts(3);
            data.Index(sets,19) = spike_counts(4);
            data.Index(sets,20) = spike_counts(5);
            data.Index(sets,21) = spike_counts(6);
            data.Index(sets,22) = spike_counts(7);
            
            
            peaks = sta_groups.peak{x};
            %peaks
            data.Index(sets,23) = peaks(1);
            data.Index(sets,24) = peaks(2);
            data.Index(sets,25) = peaks(3);
            data.Index(sets,26) = peaks(4);
            data.Index(sets,27) = peaks(5);
            data.Index(sets,28) = peaks(6);
            data.Index(sets,29) = peaks(7);
            
            lags = sta_groups.lags{x};
            %lags
            data.Index(sets,30) = lags(1);
            data.Index(sets,31) = lags(2);
            data.Index(sets,32) = lags(3);
            data.Index(sets,33) = lags(4);
            data.Index(sets,34) = lags(5);
            data.Index(sets,35) = lags(6);
            data.Index(sets,36) = lags(7);
            
            %mean instantaneous phase (from hilbert transform)
            instph = sta_groups.mean_inst_angle{x};
            data.Index(sets,30) = instph(1);
            data.Index(sets,31) = instph(2);
            data.Index(sets,32) = instph(3);
            data.Index(sets,33) = instph(4);
            data.Index(sets,34) = instph(5); %delay 200 : 800
            data.Index(sets,35) = instph(6);
            data.Index(sets,36) = instph(7);
            
            
            glob_sur = zeros(7,2);
            sig_peaks = zeros(1,7);
            pos_neg_peak = zeros(1,7);
            %calculate global surrogates with spike information and determine if peak crosses threshold
            for gs = 1 : 7
                [u l] = fit_sta_globalsurrogate('num_spikes',spike_counts(gs),'upper',99.99,'lower',.01);
                glob_sur(gs,:) = [u l];
                if peaks(gs) < 0
                    pos_neg_peak(gs) = -1;
                    if peaks(gs) < u
                        sig_peaks(gs) = 1;
                    end
                else
                    pos_neg_peak(gs) = 1;
                    if peaks(gs) > l
                        sig_peaks(gs) = 1;
                    end
                end
            end
            data.global_surrogates{sets,1} = glob_sur;
            data.sig_stas{sets,1} = sig_peaks;
            data.peak_sign{sets,1} = pos_neg_peak;
            %INDEX 5-6: cortex (parietal = 1, frontal = 2)
            if strcmp(N.cortex(ch1),'P')
                data.Index(sets,5) = 1;
            elseif strcmp(N.cortex(ch1),'F')
                data.Index(sets,5) = 2;
            end
            
            if strcmp(N.cortex(ch2),'P')
                data.Index(sets,6) = 1;
            elseif strcmp(N.cortex(ch2),'F')
                data.Index(sets,6) = 2;
            end
            
            %INDEX 7-8: histology number
            data.Index(sets,7) = N.number(ch1);
            data.Index(sets,8) = N.number(ch2);
            
            %INDEX 9-10: grid position
            data.Index(sets,9) = N.gridPos(ch1);
            data.Index(sets,10) = N.gridPos(ch2);
            
            %set name 1: sta
            data.setNames{sets,1} = [lfp2dir filesep sta_pairs(sp).name];
        end
    end
    data.numSets = sets;
    %     load global_sta
    %     data.global_surrogates_sta{1,1} = global_sta';
    %     data.global_surrogates_sta{1,2} = increments;
    
    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end
else
    obj = createEmptyObject(Args);
end


function obj = createEmptyObject(Args)
data.Index = [];
data.means = {};
data.se = {};
data.setNames = {};
data.sig_stas = {};
data.peak_sign = {};
data.numSets = 0;
%data.global_surrogates_sta = {};
data.global_surrogates = {};
data.channel_std = {};
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
