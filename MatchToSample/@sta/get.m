function [r,varargout] = get(obj,varargin)

%get function for sta
Args = struct('Number',0,'ObjectLevel',0,'multi',0,'single',0,'frontal',0,'parietal',0,'hist_numbers',[],'peak',[],'lag',[],'window',[],'sig',0,'self',0);
Args.flags = {'Number','ObjectLevel','multi','single','frontal','parietal','sig','self'};
Args = getOptArgs(varargin,Args);
varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    all = [1 : length(obj.data.Index(:,1))]; 
    
    all_peaks = [23:29];
    all_lags = [30:36];
    
    window = Args.window;
    peakind = all_peaks(window);
    lagind = all_lags(window);
    nstas =size(obj.data.Index,1);
    ind1 = [];
    if Args.sig
        for s = 1 : nstas
            if obj.data.sig_stas{s}(Args.window)
                ind1 = [ind1,s];
            end
        end
    else
        ind1 = all;
    end

    %INDEX 3: single, multi, or all
    if Args.multi
        ind2 = find(obj.data.Index(:,3) == 1);
    elseif Args.single
        ind2 = find(obj.data.Index(:,3) ~= 1);
    else
        ind2 = all;
    end
    

        

    
    
    %spike channel is first, then lfp channel
    %rtemp5
    if ~isempty(Args.hist_numbers)
        ind4 = [];
        %histology information is in Index 7,8
        for hn = 1 : size(Args.hist_numbers,1)
            histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,1));
            histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,2));
            ind4 = [ind4 intersect(histi,histii)'];
            
        %keep this commented out if you only want ch1_spike -> ch2_field
%             histi = find(obj.data.Index(:,7) == Args.hist_numbers(hn,2));
%             histii = find(obj.data.Index(:,8) == Args.hist_numbers(hn,1));
%             ind4 = [ind4 intersect(histi,histii)'];
        end
        ind4 = sort(ind4);
    else
        ind4 = [1 : size(obj.data.Index,1)];
    end
    
    
    
    %peak is a range (greater than and then less than)
    if ~isempty(Args.peak)
        %if a specific epoch is specified look at only it, else look through all
        ind3 = [];
        for x = all
            p = obj.data.Index(x,(22 + Args.window));
            if p > Args.peak(1) && p < Args.peak(2)
                ind3 = [ind3 x];
            end
        end
    else
        ind3 = all;
    end
    
    %lag is a range
    if ~isempty(Args.lag)
        %if a specific epoch is specified look at only it, else look through all
        lags = obj.data.Index(:,all_lags(Args.window),:);
        ind6 = intersect(find(lags > Args.lag(1)),find(lags < Args.lag(2)))';
     else
        ind6 = all;
    end
    
    if Args.self
        selfs = obj.data.Index(:,1) - obj.data.Index(:,2);
        ind5 = find(selfs == 0);
    else
    ind5 = all;
    end
    
    

    
    varargout{1} = intersect(ind1,intersect(ind2,intersect(ind3,intersect(ind4,intersect(ind5,ind6)))));
    r = length(varargout{1});
    
    
    fprintf(1,['Number of Channel Pairs: ',num2str(r),'\n']);
    
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
end