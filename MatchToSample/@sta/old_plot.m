function obj = plot(obj,varargin)

%used to plot sta objects
Args = struct('epoch',4,'surrogate_lfp',0,'avgcor_surrogate',0,'spikeplotmulti',1,'smooth',0);
Args.flags = {'avgcor_surrogate','smooth'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work on
    ind = dataindices(n);
end

%determine if the cluster from the first channel is multi or single
cluster_numb = obj.data.Index(ind,3);

lfp_pair = obj.data.setNames{ind,1};
spike_pair = obj.data.setNames{ind,2};

s = findstr('lfpsta',lfp_pair);
cd(lfp_pair(1:(s-1)));

%get spike spike autocorrelation for first channel
g = obj.data.Index(ind,(1:2));
sp = findstr('spikesta',spike_pair);
load([spike_pair(sp:(end-8)) num2strpad(g(1),2) num2strpad(g(1),2) '.mat'])
auto_spike1 = spikebins;

%get spike spike autocorrelation for second channel
g = obj.data.Index(ind,(1:2));
sp = findstr('spikesta',spike_pair);
load([spike_pair(sp:(end-8)) num2strpad(g(2),2) num2strpad(g(2),2) '.mat'])
auto_spike2 = spikebins;

%load spike data
load(spike_pair(sp:end))

%load lfp data for pair
load(lfp_pair(s:end))
sta_mean = sta_groups_mean{cluster_numb}(Args.epoch,:);
sta_se = sta_groups_se{cluster_numb}(Args.epoch,:);
sta_peak = sta_groups_peak{cluster_numb}(Args.epoch);
sta_time = sta_groups_time{cluster_numb}(Args.epoch);
if Args.surrogate_lfp
    pair_sur5 = obj.data.surrogateslfpsta{ind,cluster_numb}(1,Args.epoch);
    pair_sur99_5 = obj.data.surrogateslfpsta{ind,cluster_numb}(2,Args.epoch);
end

%get self1 MULTI get spike-lfp data for each group (self)
load([lfp_pair(s:(end-8)) num2strpad(g(1),2) num2strpad(g(1),2) '.mat'])
self1multi_mean = sta_groups_mean{1}(Args.epoch,:);
self1multi_se = sta_groups_se{1}(Args.epoch,:);
self1multi_peak = sta_groups_peak{1}(Args.epoch);
self1multi_time = sta_groups_time{1}(Args.epoch);
if Args.surrogate_lfp
    load(['surrogate' lfp_pair(s:(end-14)) num2strpad(g(1),2) num2strpad(g(1),2) '.mat'])
    p1 = prctile(surrogate_sta_groups{1}',[.05 99.95]); %always multi
    
    self1multi_sur5 = p1(1,Args.epoch);
    self1multi_sur99_5 = p1(2,Args.epoch);
end

%self1 get spike-lfp data for each group (self)
load([lfp_pair(s:(end-8)) num2strpad(g(1),2) num2strpad(g(1),2) '.mat'])
self1_mean = sta_groups_mean{cluster_numb}(Args.epoch,:);
self1_se = sta_groups_se{cluster_numb}(Args.epoch,:);
self1_peak = sta_groups_peak{cluster_numb}(Args.epoch);
self1_time = sta_groups_time{cluster_numb}(Args.epoch);
if Args.surrogate_lfp
    load(['surrogate' lfp_pair(s:(end-14)) num2strpad(g(1),2) num2strpad(g(1),2) '.mat'])
    p1 = prctile(surrogate_sta_groups{1,cluster_numb}',[.05 99.95]); %multi and single
    
    self1_sur5 = p1(1,Args.epoch);
    self1_sur99_5 = p1(2,Args.epoch);
end

%self2 Always show MULTI unit sta for second channel
load([lfp_pair(s:(end-8)) num2strpad(g(2),2) num2strpad(g(2),2) '.mat'])
self2_mean = sta_groups_mean{1}(Args.epoch,:);
self2_se = sta_groups_se{1}(Args.epoch,:);
self2_peak = sta_groups_peak{1}(Args.epoch);
self2_time = sta_groups_time{1}(Args.epoch);
if Args.surrogate_lfp
    load(['surrogate' lfp_pair(s:(end-14)) num2strpad(g(2),2) num2strpad(g(2),2) '.mat'])
    p1 = prctile(surrogate_sta_groups{1}',[.05 99.95]); %always multiunit
    
    self2_sur5 = p1(1,Args.epoch);
    self2_sur99_5 = p1(2,Args.epoch);
end

%get average correlogram and surrogate
avg_cor_epoch = zeros(1,101);
cor_epoch = 0;
p_epoch = 0;
if g(1) ~= g(2)
    epoch = Args.epoch;
    if Args.epoch == 5
        epoch = 4;
    end
    avg_g = sort(g); %this makes it ascending
    load avg_correlograms_epochs
    load all_pairs
    [i ii] = intersect(all_pairs,avg_g,'rows'); %ii is the pair number
    avg_cor_epoch = avg_correlograms_epochs{ii}(epoch,:);
    cor_epoch = avg_corrcoef_epochs{epoch,ii};
    p_epoch = avg_phase_epochs{epoch,ii};
    
    if Args.avgcor_surrogate
        load epoch_surrogates
        if epoch == 1
            e = epoch1;
        elseif epoch == 2
            e = epoch2;
        elseif epoch == 3
            e = epoch3;
        else
            e = epoch4;
        end
        acp = prctile(e(ii,:),[.05 99.95]); %always multiunit
        acsur05 = acp(1);
        acsur99_95 = acp(2);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%make self 1 multi plot
subplot(2,4,1)

plot([-100:100],self1multi_mean)
hold on
plot([-100:100],(self1multi_mean + self1multi_se),'r')
hold on
plot([-100:100],(self1multi_mean - self1multi_se),'r')

if Args.surrogate_lfp
    %plot the surrogates
    hold on
    plot([-100 100],[self1multi_sur5 self1multi_sur5],'g')
    hold on
    plot([-100 100],[self1multi_sur99_5 self1multi_sur99_5],'g')
end

%plot peak and time info

hold on
plot([-100 100],[self1multi_peak self1multi_peak],':b')
hold on
plot([self1multi_time self1multi_time],[-1 1],':b')
hold on
plot([-100 100],[0 0],'k')
hold on
plot([0 0],[-1 1],'k')


%always show the multi
type = 'multi';


%sname = obj.data.setNames{ind};
l1 = obj.data.level1{ind,1};
l2 = obj.data.level2{ind,1};
%fseps = strfind(obj.data.setNames{ind},filesep);
group_number1 = num2str(obj.data.Index(ind,1));
%t = sname(fseps(end-2)+1:end);

title(gca,[group_number1 ' ' l1 ' ' l2 ' ' type] ,'fontsize',12,'fontweight','b');
ylabel('z-score')
axis([-100 100 -1 1])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%make self 2 plot
subplot(2,4,2)

plot([-100:100],self2_mean)
hold on
plot([-100:100],(self2_mean + self2_se),'r')
hold on
plot([-100:100],(self2_mean - self2_se),'r')

if Args.surrogate_lfp
    %plot the surrogates
    hold on
    plot([-100 100],[self2_sur5 self2_sur5],'g')
    hold on
    plot([-100 100],[self2_sur99_5 self2_sur99_5],'g')
end

%plot peak and time info

hold on
plot([-100 100],[self2_peak self2_peak],':b')
hold on
plot([self2_time self2_time],[-1 1],':b')
hold on
plot([-100 100],[0 0],'k')
hold on
plot([0 0],[-1 1],'k')


%always show the multi sta for second channel
type = 'multi';


l1 = obj.data.level1{ind,2};
l2 = obj.data.level2{ind,2};
%fseps = strfind(obj.data.setNames{ind},filesep);
group_number2 = num2str(obj.data.Index(ind,2));
%t = sname(fseps(end-2)+1:end);

title(gca,[group_number2 ' ' l1 ' ' l2 ' ' type] ,'fontsize',12,'fontweight','b');

ylabel('z-score')
axis([-100 100 -1 1])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%make self 1 plot
subplot(2,4,3)

plot([-100:100],self1_mean)
hold on
plot([-100:100],(self1_mean + self1_se),'r')
hold on
plot([-100:100],(self1_mean - self1_se),'r')

if Args.surrogate_lfp
    %plot the surrogates
    hold on
    plot([-100 100],[self1_sur5 self1_sur5],'g')
    hold on
    plot([-100 100],[self1_sur99_5 self1_sur99_5],'g')
end

%plot peak and time info

hold on
plot([-100 100],[self1_peak self1_peak],':b')
hold on
plot([self1_time self1_time],[-1 1],':b')
hold on
plot([-100 100],[0 0],'k')
hold on
plot([0 0],[-1 1],'k')


if obj.data.Index(ind,3) == 1
    type = 'multi';
else
    type = 'single';
end

%sname = obj.data.setNames{ind};
l1 = obj.data.level1{ind,1};
l2 = obj.data.level2{ind,1};
%fseps = strfind(obj.data.setNames{ind},filesep);
group_number1 = num2str(obj.data.Index(ind,1));
%t = sname(fseps(end-2)+1:end);

title(gca,[group_number1 ' ' l1 ' ' l2 ' ' type] ,'fontsize',12,'fontweight','b');
ylabel('z-score')
axis([-100 100 -1 1])



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%make pair plot
subplot(2,4,4)

plot([-100:100],sta_mean)
hold on
plot([-100:100],(sta_mean + sta_se),'r')
hold on
plot([-100:100],(sta_mean - sta_se),'r')

if Args.surrogate_lfp
    %plot the surrogates
    hold on
    plot([-100 100],[pair_sur5 pair_sur5],'g')
    hold on
    plot([-100 100],[pair_sur99_5 pair_sur99_5],'g')
end

%plot peak and time info

hold on
plot([-100 100],[sta_peak sta_peak],':b')
hold on
plot([sta_time sta_time],[-1 1],':b')
hold on
plot([-100 100],[0 0],'k')
hold on
plot([0 0],[-1 1],'k')


title(gca,[group_number1 ' spike  VS  ' group_number2 ' LFP'] ,'fontsize',12,'fontweight','b');

ylabel('z-score')
axis tight
%axis([-100 100 -10 10])




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%auto spike ch1
subplot(2,4,5)

if Args.epoch == 1
    bins = auto_spike1.epoch1{1}; %multi unit vs multi unit
    spikecount1 = num2str(auto_spike1.spikecount1{1}); 
elseif Args.epoch == 2
    bins = auto_spike1.epoch2{1};
    spikecount1 = num2str(auto_spike1.spikecount2{1}); 
elseif Args.epoch == 3
    bins = auto_spike1.epoch3{1};
    spikecount1 = num2str(auto_spike1.spikecount3{1}); 
elseif Args.epoch == 4
    bins = auto_spike1.epoch4{1};
    spikecount1 = num2str(auto_spike1.spikecount4{1});
else
    bins = auto_spike1.all{1};
    spikecount1 = num2str(auto_spike1.spikecount_all{1});
end
if ~isempty(bins)
    if Args.smooth
        bar([-100:100],smooth(bins,10),'r')
    else
        bar([-100:100],bins,'r')
    end
    
    axis tight
    %axis([-100 100 0 50])
    hold on
end

title(gca,['  ' group_number1 ' ' l1 ' ' l2 ' ' type] ,'fontsize',12,'fontweight','b');

hold on
title(gca,[group_number1 ' auto-correlation' ' : spike count: ' spikecount1] ,'fontsize',12,'fontweight','b');
%text(-100,55,num2str(numbspikes),'FontSize',10,'FontWeight','bold')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%auto spike ch2
subplot(2,4,6)

if Args.epoch == 1
    bins = auto_spike2.epoch1{1}; %multi unit vs multi unit
    spikecount2 = num2str(auto_spike2.spikecount1{1}); 
elseif Args.epoch == 2
    bins = auto_spike2.epoch2{1};
    spikecount2 = num2str(auto_spike2.spikecount2{1});
elseif Args.epoch == 3
    bins = auto_spike2.epoch3{1};
    spikecount2 = num2str(auto_spike2.spikecount3{1});
elseif Args.epoch == 4
    bins = auto_spike2.epoch4{1};
    spikecount2 = num2str(auto_spike2.spikecount4{1});
else
    bins = auto_spike2.all{1};
    spikecount2 = num2str(auto_spike2.spikecount_all{1});
end
if ~isempty(bins)
    if Args.smooth
        bar([-100:100],smooth(bins,10),'r')
    else
        bar([-100:100],bins,'r')
    end
    
    axis tight
    %axis([-100 100 0 50])
    hold on
end

title(gca,['  ' group_number2 ' ' l1 ' ' l2 ' ' type] ,'fontsize',12,'fontweight','b');

hold on
title(gca,[group_number2 ' auto-correlation' ' : spike count: ' spikecount2] ,'fontsize',12,'fontweight','b');
%text(-100,55,num2str(numbspikes),'FontSize',10,'FontWeight','bold')





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot spike
subplot(2,4,7)
colors = {'r','k','g','m','y','b'};

%use cluster_numb from first channel to plot all spike combinations with second channel

num_cluster_secondchannel = size(spikebins.all,2);

clusts = find(spikebins.clusters(:,1) == cluster_numb)';

if Args.spikeplotmulti
    clusts2 = find(spikebins.clusters(:,2) == 1)'; %only use multi from second channel
    clusts = intersect(clusts,clusts2);
end

numbspikes = obj.data.Index(ind,4);

numbcs = 0;
for ncs = clusts
    numbcs = numbcs + 1;
    if Args.epoch == 1
        bins = spikebins.epoch1{ncs};
    elseif Args.epoch == 2
        bins = spikebins.epoch2{ncs};
    elseif Args.epoch == 3
        bins = spikebins.epoch3{ncs};
    elseif Args.epoch == 4
        bins = spikebins.epoch4{ncs};
    else
        bins = spikebins.all{ncs};
    end
    if ~isempty(bins)
        if Args.smooth
            bar([-100:100],smooth(bins,10),colors{numbcs});
        else
            bar([-100:100],bins,colors{numbcs});
        end
%         axis tight
        axis([-100 100 0 (max(bins) +100)])
        hold on
    end
    hold on
    legend(num2str(spikebins.clusters(clusts,:)))
    title(gca,[] ,'fontsize',12,'fontweight','b');
    title(gca,[group_number1 ' spike  VS  ' group_number2 ' spike' ' : spike count: ' num2str(numbspikes)] ,'fontsize',12,'fontweight','b');
    %text(-100,55,num2str(numbspikes),'FontSize',10,'FontWeight','bold')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%plot average correlogram
subplot(2,4,8)
plot([-50:50],avg_cor_epoch,'r')
hold on
plot([-50 50],[cor_epoch cor_epoch],':b')
hold on
plot([p_epoch p_epoch],[-1 1],':b')
hold on
plot([-50 50],[0 0],'k')
hold on
plot([0 0],[-1 1],'k')
hold on

if g(1) ~= g(2)
    if Args.avgcor_surrogate
        %plot the surrogates
        hold on
        plot([-50 50],[acsur05 acsur05],'g')
        hold on
        plot([-50 50],[acsur99_95 acsur99_95],'g')
    end
end

if Args.epoch == 5
    title(gca,[group_number1 ' Vs ' group_number2 ' epoch ' num2str(epoch) ' average correlogram'] ,'fontsize',12,'fontweight','b');
else
    title(gca,[group_number1 ' Vs ' group_number2 ' epoch ' num2str(Args.epoch) ' average correlogram'] ,'fontsize',12,'fontweight','b');
end