function obj = plot(obj,varargin)

%used to plot sta objects
%Arguments
%   stable = stable trials
%   transition = transition trials
%   correct = correct trials
%   incorrect = correct trials
%Must enter a combination of performance and behavioral response
%ie: 'stable','correct'

Args = struct('ml',0,'window',[]);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


%ind a vector containing the trials that meet criterion
[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(Args.NumericArguments)
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
end
%loads the channelpair specified in the index

cluster_pair = obj.data.setNames{ind};

%get histology information
hn1 = obj.data.Index(ind,7);
hn2 = obj.data.Index(ind,8);
[hist1] = reverse_categorizeNeuronalHist(hn1);
[hist2] = reverse_categorizeNeuronalHist(hn2);

if Args.window == 1
    sta_mean = (obj.data.means{ind}(1,:));
    sta_se = (obj.data.se{ind}(1,:));
    surs = obj.data.global_surrogates{ind}(1,:);
elseif Args.window == 2
    sta_mean = (obj.data.means{ind}(2,:));
    sta_se = (obj.data.se{ind}(2,:));
    surs = (obj.data.global_surrogates{ind}(2,:));
elseif Args.window == 3
    sta_mean = (obj.data.means{ind}(3,:));
    sta_se = (obj.data.se{ind}(3,:));
    surs = (obj.data.global_surrogates{ind}(3,:));
elseif Args.window == 4
    sta_mean = (obj.data.means{ind}(4,:));
    sta_se = (obj.data.se{ind}(4,:));
    surs = (obj.data.global_surrogates{ind}(4,:));
elseif Args.window == 5
    sta_mean = (obj.data.means{ind}(5,:));
    sta_se = (obj.data.se{ind}(5,:));
    surs = (obj.data.global_surrogates{ind}(5,:));
elseif Args.window == 6
    sta_mean = (obj.data.means{ind}(6,:));
    sta_se = (obj.data.se{ind}(6,:));
    surs = (obj.data.global_surrogates{ind}(6,:));
elseif Args.window == 7
    sta_mean = (obj.data.means{ind}(7,:));
    sta_se = (obj.data.se{ind}(7,:));
    surs = (obj.data.global_surrogates{ind}(7,:));
end

plot([-50:50],sta_mean);
hold on
plot([-50:50],sta_mean + sta_se,'r')
hold on
plot([-50:50],sta_mean - sta_se,'r')
hold on
plot([-50 50],[surs(1) surs(1)],'r')
hold on
plot([-50 50],[surs(2) surs(2)],'r')

hold on
day_ses = cell2mat(cellstr(obj.data.setNames{ind}(end-49:end-28)));

title(([day_ses '       spike ch' num2str(obj.data.Index(ind,1)) ' ' cell2mat(hist1) '    field ch' num2str(obj.data.Index(ind,2)) ' ' cell2mat(hist2)]))


