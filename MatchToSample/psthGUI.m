function varargout = psthGUI(varargin)
% PSTHGUI M-file for psthGUI.fig
%      PSTHGUI, by itself, creates a new PSTHGUI or raises the existing
%      singleton*.
%
%      H = PSTHGUI returns the handle to a new PSTHGUI or the handle to
%      the existing singleton*.
%
%      PSTHGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PSTHGUI.M with the given input arguments.
%
%      PSTHGUI('Property','Value',...) creates a new PSTHGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before psthGUI_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to psthGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help psthGUI

% Last Modified by GUIDE v2.5 24-Feb-2009 14:25:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @psthGUI_OpeningFcn, ...
    'gui_OutputFcn',  @psthGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before psthGUI is made visible.
function psthGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to psthGUI (see VARARGIN)

% Choose default command line output for psthGUI
handles.output = hObject;

Args = struct('CueObj',[],'CueLoc',[],'MatchObj1',[],'MatchPos1',[],'MatchObj2',[],'MatchPos2',[],'BehResp',[],'stable',0,'transition',0,'raster',0);

handles.Args = Args;

% obj = mtsispike('auto');
% 
% mtsobj = mtstrial('auto');
% 
% cues = unique(mtsobj.data.CueObj);
% locs = unique(mtsobj.data.CueLoc);
% 
% handles.obj = obj;
% handles.cues = cues;
% handles.locs = locs;
% handles.nobj = 0;
% Update handles structure
guidata(hObject, handles);



% UIWAIT makes psthGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = psthGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in identity_cue1.
function identity_cue1_Callback(hObject, eventdata, handles)
% hObject    handle to identity_cue1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_cue1



if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueObj = [handles.Args.CueObj handles.cues(1)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end

guidata(hObject,handles)

% --- Executes on button press in identity_cue3.
function identity_cue3_Callback(hObject, eventdata, handles)
% hObject    handle to identity_cue3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_cue3

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueObj = [handles.Args.CueObj handles.cues(3)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in correct.
function correct_Callback(hObject, eventdata, handles)
% hObject    handle to correct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of correct

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.BehResp = [handles.Args.BehResp 1];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in incorrect.
function incorrect_Callback(hObject, eventdata, handles)
% hObject    handle to incorrect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of incorrect

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.BehResp = [handles.Args.BehResp 0];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on selection change in reference.
function reference_Callback(hObject, eventdata, handles)
% hObject    handle to reference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns reference contents as cell array
%        contents{get(hObject,'Value')} returns selected item from reference


val = get(hObject,'Value');
switch val

    case 1
        handles.Args.alignement = 'CueOnset';% User selected the first item
    case 2
        handles.Args.alignement = 'CueOffset';% User selected the second item
    case 3
        handles.Args.alignement = 'MatchOnset';% User selected the second item
    case 4
        handles.Args.alignement = 'FirstSac';% User selected the second item
    case 5
        handles.Args.alignement = 'LastSac';% User selected the second item

end
% Proceed with callback...
guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function reference_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cdir.
function cdir_Callback(hObject, eventdata, handles)
% hObject    handle to cdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cwd=pwd;


path = uigetdir(pwd,'Select Session Folder');
if(path~=0)
    cd(path)
end
% handles.output = hObject;


obj = mtsispike('auto');

mtsobj = mtstrial('auto');

cues = unique(mtsobj.data.CueObj);
locs = unique(mtsobj.data.CueLoc);

handles.obj = obj;
handles.cues = cues;
handles.locs = locs;
handles.nobj = 0;
% Update handles structure
guidata(hObject, handles);
% --- Executes on button press in transition.
function transition_Callback(hObject, eventdata, handles)
% hObject    handle to transition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of transition

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.transition =  1;% Checkbox is checked-take approriate action
else
    handles.Args.transition =  0;% Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in stable.
function stable_Callback(hObject, eventdata, handles)
% hObject    handle to stable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stable

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.stable = 1;% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
    handles.Args.stable = 0;
end
guidata(hObject, handles);
% --- Executes on button press in create_obj.
function create_obj_Callback(hObject, eventdata, handles)
% hObject    handle to create_obj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.nobj = handles.nobj + 1;
Args = handles.Args;
nfield = fieldnames(Args);
vfield = struct2cell(Args);
flags = {'stable','transition','raster'};
count = 1;
for n = 1 : length(nfield)
    if ~isempty(strmatch(nfield{n},flags))
        if eval(sprintf('Args.%s == 1;',nfield{n}))
            var{count} = nfield{n};
            count = count + 1;
        end
    else
        var{count} = nfield{n};
        count = count + 1;
        var{count} = vfield{n};
        count = count + 1;
    end
end

handles.optArgs(handles.nobj).var = var;

guidata(hObject, handles);


% --- Executes on button press in plot.
function plot_Callback(hObject, eventdata, handles)
% hObject    handle to plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

obj = handles.obj;

if handles.nobj > 1
    
    strobj = 'obj';
    if handles.nobj > 2
        for n =  1 : handles.nobj-2
            strobj = cat(2,strobj,',obj');
        end

    end

    eval(sprintf('InspectGUI(obj,''addObjs'',{%s},''optArgs'',{handles.optArgs.var},''SP'',[handles.nobj 1]);',strobj));
else
    InspectGUI(obj,handles.optArgs(1).var{:})
end

handles.optArgs = [];

handles.nobj = 0;


guidata(hObject, handles);


% --- Executes on button press in identity_cue2.
function identity_cue2_Callback(hObject, eventdata, handles)
% hObject    handle to identity_cue2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_cue2

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueObj = [handles.Args.CueObj handles.cues(2)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_cue1.
function location_cue1_Callback(hObject, eventdata, handles)
% hObject    handle to location_cue1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_cue1

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueLoc = [handles.Args.CueLoc handles.locs(1)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_cue3.
function location_cue3_Callback(hObject, eventdata, handles)
% hObject    handle to location_cue3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_cue3

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueLoc = [handles.Args.CueLoc handles.locs(3)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_cue2.
function location_cue2_Callback(hObject, eventdata, handles)
% hObject    handle to location_cue2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_cue2

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.CueLoc = [handles.Args.CueLoc handles.locs(2)];% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in identity_match1.
function identity_match1_Callback(hObject, eventdata, handles)
% hObject    handle to identity_match1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_match1

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchObj1 = [handles.Args.MatchObj1 handles.cues(1)];% Checkbox is checked-take approriate action
    handles.Args.MatchObj2 = [handles.Args.MatchObj2 handles.cues(1)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in identity_match2.
function identity_match2_Callback(hObject, eventdata, handles)
% hObject    handle to identity_match2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_match2

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchObj1 = [handles.Args.MatchObj1 handles.cues(2)];% Checkbox is checked-take approriate action
    handles.Args.MatchObj2 = [handles.Args.MatchObj2 handles.cues(2)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in identity_match3.
function identity_match3_Callback(hObject, eventdata, handles)
% hObject    handle to identity_match3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of identity_match3

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchObj1 = [handles.Args.MatchObj1 handles.cues(3)];% Checkbox is checked-take approriate action
    handles.Args.MatchObj2 = [handles.Args.MatchObj2 handles.cues(3)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_match1.
function location_match1_Callback(hObject, eventdata, handles)
% hObject    handle to location_match1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_match1

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchPos1 = [handles.Args.MatchPos1 handles.locs(1)];% Checkbox is checked-take approriate action
    handles.Args.MatchPos2 = [handles.Args.MatchPos2 handles.locs(1)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_match2.
function location_match2_Callback(hObject, eventdata, handles)
% hObject    handle to location_match2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_match2

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchPos1 = [handles.Args.MatchPos1 handles.locs(2)];% Checkbox is checked-take approriate action
    handles.Args.MatchPos2 = [handles.Args.MatchPos2 handles.locs(2)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes on button press in location_match3.
function location_match3_Callback(hObject, eventdata, handles)
% hObject    handle to location_match3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of location_match3

if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.MatchPos1 = [handles.Args.MatchPos1 handles.locs(3)];% Checkbox is checked-take approriate action
    handles.Args.MatchPos2 = [handles.Args.MatchPos2 handles.locs(3)];
else
    % Checkbox is not checked-take approriate action
end
guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Raster.
function Raster_Callback(hObject, eventdata, handles)
% hObject    handle to Raster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Raster
if (get(hObject,'Value') == get(hObject,'Max'))
    handles.Args.raster = 1;% Checkbox is checked-take approriate action
else
    % Checkbox is not checked-take approriate action
    handles.Args.raster = 0;
end
guidata(hObject, handles);

% --- Executes on button press in checkbox29.
function nine_stimuli_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox29


% --- Executes on button press in Reset.


