function mvpa_spike_rule_analysis_figure


cd('/Volumes/raid/data/monkey/betty')
load switchdays;
ndays = size(days,2);
mdir=pwd;

figure
dcounter = 0;
for x = 1:24%ndays
    
    
    cd([mdir filesep days{x} filesep 'session01']);
    load mvpa_perf
    
    if size(pfc_areas,2) >= 3  && size(ppc_areas,2) >= 3
        dcounter = dcounter + 1;
        for ee = 1 : 6
            all_perfs_pfc(ee,dcounter) = perf_pfc(ee) - mean(perf_pfc(1:4));
            all_perfs_ppc(ee,dcounter) = perf_ppc(ee) - mean(perf_ppc(1:4));
        end
        plot(perf_pfc(1:4),'r')
        hold on
        plot(perf_ppc(1:4),'b')
    end
end

figure
subplot(2,1,1)
boxplot(all_perfs_pfc(1:4,:)')
hold on
subplot(2,1,2)
boxplot(all_perfs_ppc(1:4,:)')



figure
shadeplot('bins',all_perfs_pfc(1:5,:)')
hold on
shadeplot('bins',all_perfs_ppc(1:5,:)','line','b')


