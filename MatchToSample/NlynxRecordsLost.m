function NlynxRecordsLost

load timing.mat
trial_starts = t.start;
num_trials = size(trial_starts,2);
%manually input fields below
start_time = 187997424;%check 'CheetahLogFile.txt' to find the time in microseconds that corresponds to the start time in the 'CheetahLostADRecords.txt'

DataSectionsLost = 1;

%time is in microseconds from the beginning of the recording
ADRecordsLostFirstTS = [8419212693];
ADRecordsLostLastTS = [8419844755];

losttrials = [];
for dsl = 1 : DataSectionsLost
    
    start_lost = ADRecordsLostFirstTS(dsl);
    stop_lost = ADRecordsLostLastTS(dsl);
    
    %loop through trials
    for nt = 1 : num_trials
        if trial_starts(nt) >= start_lost  && trial_starts(nt) <= stop_lost
            losttrials = [losttrials nt];
        end
    end 
end

lost_trials.number = size(losttrials,2);
lost_trials.list = losttrials;

save lost_trials lost_trials