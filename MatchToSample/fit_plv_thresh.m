function fit_plv_thresh(varargin)


%run at session level
%fits the plv surrogates to get the probabilities on the tails

Args = struct('ml',0);
Args.flags = {'ml'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


sesdir = pwd;
cd([sesdir filesep 'lfp' filesep 'lfp2'])

sur = nptDir('surrogate_hilbertentropy*');
nump = size(sur,1);
for np = 1 : nump
    nump-np
    load(sur(np).name)
    all_bias = [];
    for p = 1 %only fitting the correct stable ide trials (all stimuli combined)
        sh = surrogate_hilbertentropy(p,:);
        %get mean for each permutation
        allmeans = cellfun(@(pmean) mean(pmean,3),surrogate_hilbertentropy(p,:),'UniformOutput',0);
        allmeans = reshape(allmeans,1,1,size(allmeans,2));
        allsur = cell2mat(allmeans);
        
        bias = mean(allsur,3);
        all_bias{p} = bias(:,1:36);
        
        fitthresh = [];
        for as = 1 : 36%size(allsur,2)
            %time/freq/perm
            fitsur = squeeze(allsur(:,as,:))';
            [datafit,xx,thresh,rsquare] = fitSurface(fitsur,all_freq,'Prob',[.999 .9999 .99999]);
            for th = 1 : size(thresh,2)
                fitthresh{th}(:,as) = thresh(:,th);
            end
        end
        all_thresh{p} = fitthresh;
    end
    sur_pairs = [ 'thresh' sur(np).name];
    save(sur_pairs,'all_thresh','all_bias');
end
cd(sesdir)



