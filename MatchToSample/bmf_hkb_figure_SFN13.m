function bmf_hkb_figure_SFN13(varargin)


%run at session level
%computes Shannon Entropy and normalized to get the synchronization index
%in tass_1998
Args = struct('ml',1,'plot',0);
Args.flags = {};
Args = getOptArgs(varargin,Args);


cd('/media/bmf_raid/data/monkey/ethyl/110714/session01')

%phase_paper_iti_figures.m, investigage data from this figure

% bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
window = 300;
stepsize = 50;
nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
bins = linspace(0,1,nbins);
steps = [window/2:stepsize:5000-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
nw = size(steps,2);

bandpasslow = 8;
bandpasshigh = 25;
%
% bandpasslow = 1;
% bandpasshigh = 8;

sesdir = pwd;
%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

N = NeuronalHist('bmf','ml');

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
%Get trials
tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
sample_off = floor(mt.data.CueOffset); %lock to sample off

cd([sesdir filesep 'lfp'])
lfpdata = nptDir('*_lfp*');

cd(sesdir)

ch1 = find(N.gridPos == 201);
ch2 = find(N.gridPos == 248);

counter = 0;
for t = tr
    counter = counter + 1;
    trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];

    [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
    
    %lowpass
    hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,bandpasslow,bandpasshigh);
    
    %get data ranges
    datarange = [sample_off(t) - 999 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
    bpdata = hdata(:,datarange);
    
    bpd1 = bpdata(ch1,:);
    bpd2 = bpdata(ch2,:);
    
    data = bpdata'; %need to do this before and after using hilbert, don't include during the transform
    %compute hilbert transform
    data = hilbert(data);
    
    %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
    %COMPENT IS FLIPPED
    %find instantaneous phase angles
    data = angle(data);
    data = data';
    
    d1 = unwrap(data(ch1,:)) ./ (2*pi);
    d2 = unwrap(data(ch2,:)) ./ (2*pi);
    
    %cyclic relative phase
    d = mod(d1 - d2,1);
    
    if size(d,2) < 5000
        d = single(padarray(d,[0 5000-size(d,2)],'post'));
        bpd1 = single(padarray(bpd1,[0 5000-size(bpd1,2)],'post'));
        bpd2 = single(padarray(bpd2,[0 5000-size(bpd2,2)],'post'));
    end
    
    
    max_ent = log2(size(bins,2)); %this is the maximum entropy (the entropy when all responses are equal)
    %get data ready for hist (hist is aparently the most
    %time consuming function, found using profile
    cc = 0;
    allh = zeros(window,nw);
    for ww = steps
        cc = cc + 1;
        allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
        
        %calculate the fft for each window
        [power1(cc,:),freq1(cc,:),angles1(cc,:)] = get_fft(bpd1(ww-((window/2)-1):ww+(window/2))',1000);
        [power2(cc,:),freq2(cc,:),angles2(cc,:)]  = get_fft(bpd2(ww-((window/2)-1):ww+(window/2))',1000);
        
    end
    
    allhist = hist(allh,bins) ./ window; %calculate response distribution
    re  = -1 * nansum(allhist.*log2(allhist));
    plv = (max_ent - re) ./ max_ent;
    mp = circ_mean((allh * (2*pi)));
    
    allplv(:,counter) = single(plv);
    allmphase(:,counter) = single(mp);
    
    f1 = 5; %~8Hz
    f2 = 14; %~25Hz
    allfreq = freq1(1,f1:f2);
    
    meanpower1(:,counter) = mean(power1(:,f1:f2)');
    meanpower2(:,counter) = mean(power2(:,f1:f2)');
    
    allangles1(:,counter) = circ_mean(angles1(:,f1:f2)');
    allangles2(:,counter) = circ_mean(angles2(:,f1:f2)');
    
    
    [m1 mm1] = max(power1(:,f1:f2)');
    [m2 mm2] =max(power2(:,f1:f2)');
    
    maxpower1(:,counter) = m1;
    maxpower2(:,counter) = m2;
    
    for nc = 1 : cc
        peakf1(nc,counter) = allfreq(mm1(nc));
        peakf2(nc,counter) = allfreq(mm2(nc));
        
        if allfreq(mm1(nc)) > allfreq(mm2(nc))
            peak_ratio(nc,counter) = allfreq(mm1(nc)) / allfreq(mm2(nc));
        else
            peak_ratio(nc,counter) = allfreq(mm2(nc)) / allfreq(mm1(nc));
        end
    end
end

plv_phase_distributions{1} = allmphase(1:70,:); %all
plv_phase_distributions{2} = allmphase(26:34,:); %delay 
plv_phase_distributions{3} = allmphase(60:68,:); %iti

plv_f1_distributions{1} = peakf1(1:70,:);
plv_f1_distributions{2} = peakf1(26:34,:);
plv_f1_distributions{3} = peakf1(60:68,:);

plv_f2_distributions{1} = peakf2(1:70,:);
plv_f2_distributions{2} = peakf2(26:34,:);
plv_f2_distributions{3} = peakf2(60:68,:);

plvs = mean(allplv(1:70,:)');

time_steps = steps(1:70);

save plv_phase_distributions plv_phase_distributions time_steps
save plv_f1_distributions plv_f1_distributions time_steps
save plv_f2_distributions plv_f2_distributions time_steps
save plvs plvs


save plv_phase_distributions plv_phase_distributions time_steps
save plv_f1_distributions plv_f1_distributions time_steps
save plv_f2_distributions plv_f2_distributions time_steps
save plvs plvs


load plv_phase_distributions
load plv_f1_distributions
load plv_f2_distributions
load plvs plvs




delayt = 18:30; %1000:1600
itit = 54:62; %2800:3200


ab_ratio = [];
pcounter = 0;
for x = 1 : 70
    [hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',plv_phase_distributions{1}(x,:));
    
%     if mod(x,2) == 0
%         pcounter = pcounter + 1;
%         subplot(4,8,pcounter)
%         plot([-pi:(2*pi)/20:pi],empirical_hkb,'b')
%         hold on
%         plot([-pi:(2*pi)/20:pi],hkb_function,'r')
%         axis([-pi pi -.2 .2])
%         axis square
%     end
    ab_ratio(x) = estimates(2)/estimates(1);
    
    ea(x) = estimates(1);
    eb(x) = estimates(2);
    eoffset(x) = estimates(3);
end


figure
subplot(4,1,1)
plot(time_steps,smooth((ab_ratio)),'b')
hold on
plot([0 3500],[0 0])
subplot(4,1,2)
plot(time_steps,mode(plv_f1_distributions{1}'),'r')
hold on
plot(time_steps,mode(plv_f2_distributions{1}'),'k')
subplot(4,1,3)
plot(time_steps,plvs)
hold on
plot([0 3500],[.15 .15])
subplot(4,1,4)
plot(time_steps,circ_mean(abs(plv_phase_distributions{1}')))

 


figure
allphi = linspace(-.5,1.5*pi,100);
phi_counter = 0;
for phi = allphi
    phi_counter = phi_counter + 1;
    for pd = 1:70
        phi_dot(pd,phi_counter) = eoffset(pd) -ea(pd)*sin(phi) - 2*eb(pd)*sin(2*(phi));
    end
end

plot(allphi*180/pi,phi_dot(30,:),'b')
hold on
plot(allphi*180/pi,phi_dot(62,:),'r')
axis tight


figure
subplot(3,1,1)
plot(time_steps,smooth(ea),'r')
hold on;plot(time_steps,smooth(eb),'b')
axis([0 3500 -.03 .03])
hold on
plot([0 3500],[0 0],'k')
subplot(3,1,2)
plot(time_steps,smooth((ab_ratio)),'k')
axis([0 3500 -1 1])
hold on
plot([0 3500],[0 0],'k')
subplot(3,1,3)
plot(time_steps,circ_mean(plv_phase_distributions{1}')*180/pi)
axis([0 3500 -40 180])



figure
subplot(2,2,1)
[hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',plv_phase_distributions{1}(30,:));
plot([-pi*180/pi:(2*pi*180/pi)/20:pi*180/pi],hkb_function,'b')
axis([-180 180 -.03 .03])
axis square
hold on

subplot(2,2,2)
[hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',plv_phase_distributions{1}(60,:));
plot([-pi*180/pi:(2*pi*180/pi)/20:pi*180/pi],hkb_function,'r')
axis([-180 180 -.03 .03])
axis square
hold on

subplot(2,2,3)
plot(allphi*180/pi,phi_dot(30,:),'b')
hold on
plot([0 0],[-.04 .04],'k')
hold on
plot([180 180],[-.04 .04],'k')
axis([-20 270 -.04 .04])
axis square

subplot(2,2,4)
plot(allphi*180/pi,phi_dot(62,:),'r')
hold on
plot([0 0],[-.04 .04],'k')
hold on
plot([180 180],[-.04 .04],'k')
axis([-20 270 -.04 .04])
axis square


%%

cd('/media/bmf_raid/data/monkey/ethyl/110714/session01')
sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx');
incorrtrials = mtsgetTrials(mt,'BehResp',0,'stable','ML','Nlynx');
corrtrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','Nlynx');


cd([sesdir '/lfp/lfp2'])
chpairs = nptdir('channelpair*')
npairs = size(chpairs,1);

load('channelpair201248.mat')

ntrials = size(incorrtrials,2);
%     ntrials = size(corrtrials,2);

allccgs = zeros(71,101);
for t = incorrtrials %corrtrials%incorrtrials
    ccgs = correlograms{t}(1:71,:);
    
    [power1(cc,:),freq1(cc,:),angles1(cc,:)] = get_fft(bpd1(ww-((window/2)-1):ww+(window/2))',1000);
    
end
allccgs = allccgs ./ ntrials; %make mean
%     imagesc(allccgs',[-.3 .3])




