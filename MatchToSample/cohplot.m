load cohInter.mat
sur = load('cohInterSur.mat');

sessions = nptDir('session*');

for s = 2 : length(sessions); skip = nptDir('skip.txt'); if isempty(skip); cd(sessions(s).name); mts = mtstrial('auto'); rule(s-1) = unique(mts.data.Index(:,1)); cd ..; end; end
ruleC = {'b' 'k'};
ruleN = {'IDE','LOC'};
f1 = figure;

set(f1,'Name',pwd)
for comb =  1 : size(Day.comb,1)
    % comb

    for s = 2 : 3%length(sessions)
        skip = nptDir('skip.txt'); 
        if isempty(skip) & ~isempty(Day.session(s-1).C)
            for p = 1 : 4; subplot(4,1,p); plot(f,Day.session(s-1).C(:,comb,p),ruleC{rule(s-1)}); hold on; end


            for p = 1 : 4; subplot(4,1,p); plot(f,sur.Day.session(s-1).Prob(:,2,comb,p),sprintf('%s--',ruleC{rule(s-1)}));  end
            legend(ruleN{rule(s-1)},'sur');
        end
    end
    % for p =1 : 4; subplot(4,1,p); title(titre{p}); end
%     for p = 1 : 4; subplot(4,1,p); legend(ruleN{rule(s-1)},'sur'); end
    for p =1 : 4; subplot(4,1,p); ylabel('Coherence'); end
    for p =1 : 4; subplot(4,1,p); axis([0 100 0 0.5]); end
    subplot(4,1,4); xlabel('Frequency [Hz]')
    title(Day.comb(comb,:))
    pause;
    clf
end