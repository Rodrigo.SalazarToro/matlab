function compare_global_pairwise_cohsurrogates

%run in cohgram dir

load globalsurcohgram
for s = 1:1000
    allgs(:,:,s) = globalsurcohgram.C{s,6};
end
globalp = prctile(allgs,99,3);

surs = nptDir('surcohgram*');
nsurs = size(surs,1);
alldif = zeros(33,41);
for ns = 1 %: nsurs
    load(surs(ns).name);
    
    load('surcohgram080228.mat')
    
    for s = 1:1000
        alls(:,:,s) = surcohgram.C{s,6};
    end
    ps = prctile(alls,99,3);
    
    
        subplot(3,1,1)
        imagesc(flipud(globalp(1:41,:)'));colorbar
        subplot(3,1,2)
        imagesc(flipud(ps(1:41,:)'));colorbar
        subplot(3,1,3)
        imagesc(flipud(globalp(1:41,:)')-flipud(ps(1:41,:)'));colorbar
        pause
        clf
    
    alldif = alldif + [flipud(globalp(1:41,:)')-flipud(ps(1:41,:)')];
end

alldif = alldif ./ nsurs;

imagesc(alldif);colorbar