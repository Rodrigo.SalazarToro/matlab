function [varargout] = ReadSequenceFile(filename)
% ReadSequenceFile reads the sequence file generated for each match to sample task. 
% This file contains all of the information pertaining to what stimuli were displayed.
% This function outputs a matrix called sequence that is separated by trial
% The coding of the sequence for the Match to sample task is
% 
% sequence is a matrix with the raws as trials and the columns as
% following:
% 
% CO CP MO1 MP1 MO2 MP2
%
% CO: cue object; CP: cue location; MO1: matched object1; MO2 matched
% object 2; MP1: matched position 1; MP2: matched position 2

list = textread(filename,'%d','headerlines',5);                     % read every number 1 at a time
sequence = (reshape(list,6,[]))';
% then reshape to necessary size for each value
varargout{1} = sequence;

SessionType = textread(filename,'%s',1,'headerlines',2);
varargout{2} = SessionType;