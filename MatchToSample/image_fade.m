function image_fade



cd('/media/jump/match_fade')
imdir = pwd;
cd('/media/jump/match_fade/match_fade_images')
new_imdir = pwd;

cd(imdir)
all_images = nptDir('*.jpg');
nimages = size(all_images,1);

for n = 1 : nimages
    cd(imdir)
    image_name = all_images(n).name;
    
    im = imread(image_name);
    cd(new_imdir)
    for x = 0:.1:1
        im2 = im .* x;
        imwrite(im2,[image_name(1:(end-4)) '_' num2str(x * 100) '.jpg'])
    end
end









