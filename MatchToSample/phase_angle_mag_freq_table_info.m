function phase_angle_mag_freq_table_info(varargin)


%make summary figure for ips phase relationship
%currently parietal only

Args = struct('ml',0,'acpval',.00001,'xc',.99); %xcthresh = unimodal_thresh_corrcoef
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);


if Args.ml
    cd('/media/raid/data/monkey/betty/')
    
    % cd(['/media/raid/data/monkey/betty/091001/session01'])
    %
    % cpp_b = mtscpp2('auto','ml');
    
    %
    % load alldays
    % cpp_b = processDays(mtscpp2,'days',alldays,'ml','NoSites')
    % save cpp_b cpp_b
    load cpp_b
    
    
else
    
    cd('/media/raid/data/monkey/clark/')
%     load idedays
%     cpp_c = processDays(mtscpp2,'days',idedays,'NoSites');
%     save cpp_c cpp_c
    load cpp_c cpp_c
    
    cpp_b = cpp_c;
end

minpairs = 5;
rtestp = .01;
disttestp = .05;
%
% epoch1 = 54; %identity delay
% % epoch2 = 153; %incorrect iti
% epoch2 = 205; %location delay



epoch1 = 52;
epoch2 = 55;

% epoch1 = 54;%600ms delay
% epoch2 = 153;%600ms iti

figure
subplot(3,1,1)
rtests = [];
mangles = [];
sangles = [];

rtests2 = [];
mangles2 = [];
sangles2 = [];

combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
for mlh1 = 1 : 10
    for mlh2 = mlh1 : 10
        
        
        if Args.ml
            
            [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        else
            [i ii] = get(cpp_b,'Number','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        end
        
        pangles = cpp_b.data.Index(ii,epoch1);
        pangles2 = cpp_b.data.Index(ii,epoch2);
        if i >= minpairs
            if circ_rtest(pangles*(pi/180)) <= rtestp && circ_rtest(pangles2*(pi/180)) <= rtestp
                
                
                counter = counter + 1;
                
                rtests(counter) = circ_rtest(pangles.*(pi/180));
                rtests2(counter) = circ_rtest(pangles2.*(pi/180));
                
                mangles(counter) = circ_mean(pangles.*(pi/180)) * (180/pi);
                mangles2(counter) = circ_mean(pangles2.*(pi/180)) * (180/pi);
                
                sangles(counter) = circ_std(pangles.*(pi/180)) * (180/pi);
                sangles2(counter) = circ_std(pangles2.*(pi/180)) * (180/pi);
                
                combon{counter} = [allmlhist(mlh1) allmlhist(mlh2)];
                comboarea{counter} = reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)]);
                reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)])
                
                
                errorbar(counter,mangles(counter),sangles(counter),'xb')
                hold on
                errorbar(counter,mangles2(counter),sangles2(counter),'xr')
                hold on
                if circ_wwtest(pangles*(pi/180),pangles2*(pi/180)) <= disttestp
                    text(counter,200,comboarea{counter},'Color',[1 0 0])
                else
                    text(counter,200,comboarea{counter},'Color',[0 0 0])
                end
                axis([0 counter + 1 -250 250])
            end
        end
    end
end


subplot(3,1,2)
epoch1 =80;
epoch2 = 82;

% epoch1 =82;%600ms delay
% epoch2 = 185;%600ms iti

%%plot frequency information
combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
for mlh1 = 1 : 10
    for mlh2 = mlh1 : 10
        if Args.ml
            
            [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        else
            [i ii] = get(cpp_b,'Number','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        end
        freq1 = cpp_b.data.Index(ii,epoch1);
        freq2 = cpp_b.data.Index(ii,epoch2);
        if i >= minpairs
            
            counter = counter + 1;
            
            mfreq1(counter) = mean(freq1);
            mfreq2(counter) = mean(freq2);
            
            sfreq1(counter) = std(freq1);
            sfreq2(counter) = std(freq2);
            
            combon{counter} = [allmlhist(mlh1) allmlhist(mlh2)];
            comboarea{counter} = reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)]);
            reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)])
            
            
            errorbar(counter,mfreq1(counter),sfreq1(counter),'xb')
            hold on
            errorbar(counter,mfreq2(counter),sfreq2(counter),'xr')
            hold on
            if ttest2(freq1,freq2)
                text(counter,22,comboarea{counter},'Color',[1 0 0])
            else
                text(counter,22,comboarea{counter},'Color',[0 0 0])
            end
            axis([0 counter + 1 0 25])
        end
    end
end










subplot(3,1,3)
epoch1 = 59;
epoch2 = 62;

% epoch1 = 61;%600ms delay
% epoch2 = 161;%600ms iti

%%plot magnitude information
combon = [];
comboarea = [];
counter = 0;
allmlhist = [1,2,3,4,5,8,10,11,12,13];
for mlh1 = 1 : 10
    for mlh2 = mlh1 : 10
        if Args.ml
            
            [i ii] = get(cpp_b,'Number','ml','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        else
            [i ii] = get(cpp_b,'Number','mlhist',[allmlhist(mlh1) allmlhist(mlh2)],'avg_corr_pval',Args.acpval,'cross_corr',Args.xc);
        end
        peak1 = cpp_b.data.Index(ii,epoch1);
        peak2 = cpp_b.data.Index(ii,epoch2);
        if i >= minpairs
            
            counter = counter + 1;
            
            mpeak1(counter) = mean(peak1);
            mpeak2(counter) = mean(peak2);
            
            speak1(counter) = std(peak1);
            speak2(counter) = std(peak2);
            
            combon{counter} = [allmlhist(mlh1) allmlhist(mlh2)];
            comboarea{counter} = reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)]);
            reverse_categorizeNeuronalHist([allmlhist(mlh1) allmlhist(mlh2)])
            
            
            errorbar(counter,mpeak1(counter),speak1(counter),'xb')
            hold on
            errorbar(counter,mpeak2(counter),speak2(counter),'xr')
            hold on
            if ttest2(peak1,peak2)
                text(counter,1.5,comboarea{counter},'Color',[1 0 0])
            else
                text(counter,1.5,comboarea{counter},'Color',[0 0 0])
            end
            axis([0 counter + 1 -1 2])
        end
    end
end



