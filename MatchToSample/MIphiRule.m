function MIphiRule(phiDiff1,phiDiff2,groupComb,varargin)
% in the xcorr folder

Args = struct('save',0','redo',0,'addName',[],'iterations',500,'lockP',[]);
Args.flags = {'save','redo'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

if ~isempty(Args.lockP)
    phiDiff1 = mod(abs(phiDiff1 - repmat(circ_median(phiDiff1(:,Args.lockP(1):Args.lockP(1)),2)',1,size(phiDiff1,2))),pi);
    phiDiff2 = mod(abs(phiDiff2 - repmat(circ_median(phiDiff2(:,Args.lockP(1):Args.lockP(1)),2)',1,size(phiDiff2,2))),pi);
    matfile = ['MIphiRule' groupComb sprintf('%sLock.mat',Args.addName)];
    lockP = Args.lockP;
else
    matfile = ['MIphiRule' groupComb sprintf('%s.mat',Args.addName)];
    lockP = [];
end



nbins = 2000;
if isempty(nptDir(matfile)) || Args.redo
    pool = cat(1,phiDiff1,phiDiff2);
    n1 = size(phiDiff1,1);
    n2 = size(phiDiff2,1);
    ntot = size(pool,1);
    su = zeros(Args.iterations,nbins);
    for ii = 1 : Args.iterations
        ord1 = randperm(ntot,n1);
        ord2 = setdiff([1:ntot],ord1);
        for ti = 1 : (nbins+1)
            distr =cat(1,pool(ord1,ti),pool(ord2,ti));
            ru = cat(1,ones(n1,1),1+ones(n2,1));
            su(ii,ti) = mutualinfo(distr,ru);
        end
    end
    
    
    mu = zeros(1,nbins);
    for ti = 1 : (nbins+1)
        
        distr =cat(1,phiDiff1(:,ti),phiDiff2(:,ti));
        ru = cat(1,ones(n1,1),1+ones(n2,1));
        mu(ti) = mutualinfo(distr,ru);
        
    end
    time = [-1800 : nbins-1800];
    if Args.save
        save(matfile,'su','mu','time','lockP')
        display([pwd '/' matfile])
    end
end
