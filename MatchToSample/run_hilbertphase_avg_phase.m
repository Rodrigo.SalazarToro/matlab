function run_hilbertphase_avg_phase(varargin)

%run at session level
% computes avg phase and avg entropy for 7 epochs
% 1: fixation    [(sample_on - 399) : (sample_on)]
% 2: sample      [(sample_off - 399) : (sample_off)]
% 3: early delay [(sample_off) : (sample_off + 399)]
% 4: late delay  [(sample_off + 401) : (sample_off + 800)]
% 5: delay       [(sample_off + 201) : (sample_off + 800)]
% 6: delay match [(match - 399) : (match)]
% 7: full trial  [(sample_on - 500) : (match)]

Args = struct('ml',0,'redo',0);
Args.flags = {'ml','redo'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
if Args.ml
    c = mtscpp2('auto','ml');
    mt = mtstrial('auto','ML','RTfromML','redosetNames');
    
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
else
    c = mtscpp2('auto');
    mt = mtstrial('auto','redosetNames');
    trialorder = {'identity','location','fixtrial','incorrect/identity','9stimuli','3identities','3locations'};
end

if Args.ml
    [~,pairs] = get(c,'ml','Number'); %use all pairs
else
    [~,pairs] = get(c,'ml','Number'); %use all pairs
end
pair_list = c.data.Index(pairs,(23:24));
npairs = size(pairs,2);

%run through all the frequencies for each pair
all_freq = [5 : 3 : 50];
nfreq = size(all_freq,2);
cd([sesdir filesep 'lfp' filesep 'lfp2'])

for p = 1 : npairs
    load(['relative_phase_pair' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)])
    for alltypes = 1 : 19
        tr = alltr{alltypes};
        ntrials = size(tr,2);
        hentropy = single(zeros(nfreq,nw,ntrials));
        mphase = single(zeros(nfreq,nw,ntrials));
        
        hentropysacc = single(zeros(nfreq,nw_sacc,ntrials));
        mphasesacc = single(zeros(nfreq,nw_sacc,ntrials));
        

        hilbertentropy{alltypes} = hentropy;
        mean_phases{alltypes} = mphase;
        
        hilbertentropy_sacc{alltypes} = hentropysacc;
        mean_phases_sacc{alltypes} = mphasesacc;
    end
    groups = pair_list(p,:);
    channel_pairs = [ 'hilbertentropy' num2strpad(pair_list(p,1),2) num2strpad(pair_list(p,2),2)];
    save(channel_pairs,'hilbertentropy','mean_phases','hilbertentropy_sacc','mean_phases_sacc','all_freq','steps','window','trialorder','groups')
end
cd(sesdir)















