function test_coherencycpt


%used to test coherencycpt
%reference questions


params.tapers = [5 9];
params.Fs = 200;
params.fpass = [0 50];
params.trialave = 1;


%lfp is sample at 200Hz (time X trials)
%spike times are in seconds in a structure ie. trial.times where
%trial(1).times is a column vector of spike times in seconds

ntrials = 100;

%simulate LFP
f = 20; %20Hz
t = [0:.005:.4];
lfp = 1 * cos(2*pi*f*t);

fields = repmat(lfp,ntrials,1)';

for increment = [-.05:.005:.05];
    
    %simulate spikes
    sptimes = [.05:.05:.35]';
    sptimes = sptimes + increment;
    for tr =  1 : ntrials
        spikes(tr).times = sptimes;
    end
    
    [C,phi,~,~,~,f] = coherencycpt(fields,spikes,params);
    
    subplot(1,2,1)
    rose(phi(14))
    
    subplot(1,2,2)
    plot(t(1:22),lfp(1:22))
    hold on
    [~,ii] = intersect(single(t),single(spikes(1).times(1)));
    scatter(spikes(1).times(1),lfp(ii),100,[.5 0 0],'filled')
    axis tight
    
    pause
    close all
    
end
