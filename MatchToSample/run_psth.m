function run_psth(varargin)

Args = struct('days',[],'sessions',[],'plot',0,'binsize',50,'match_locked',0);
Args.flags = {'plot','match_locked'};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')
monkeydir = pwd;
binsize = Args.binsize;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            rejected_trials = get_reject_trials;
            %this gets the groups that pass reject_filter.m
            [g] = bmf_groups;
            numgroups = size(g,2);
            trials = {};
            mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
            for ides = 1 : 5
                trials{ides} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'iCueObj',ides,'notfromMTStrial','Nlynx');
            end
            trials{6} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
            trials{7} = mtsgetTrials(mt,'BehResp',0,'Nlynx'); %incorrect
            
            %get interleaved fixation trials
            fixtrials = find(mt.data.CueObj == 56)'; %see ProcessSessionMTS
            [~,rej] = intersect(fixtrials,rejected_trials);
            fixtrials(rej) = [];
            trials{8} = fixtrials;
            
            %get the fixation block (if it exists)
            fixblocktrials = find(mt.data.CueObj == 57)';
            [~,rej] = intersect(fixblocktrials,rejected_trials);
            fixblocktrials(rej) = [];
            trials{9} = fixblocktrials;
            fixblock = 1;
            if isempty(trials{9})
                fixblock = 0;
            end
            
            %get the blank trials
            blanktrials = find(mt.data.CueObj == 55)';
            [~,rej] = intersect(blanktrials,rejected_trials);
            blanktrials(rej) = [];
            trials{10} = blanktrials;
            blankblock = 1;
            if isempty(trials{10})
                blankblock = 0;
            end
            
            %get sample and match onset times
            sample_on = floor(mt.data.CueOnset);
            match_on = floor(mt.data.MatchOnset);
            
            cd([sesdir filesep 'highpass']);
            highpassdir = pwd;
            
            for p = 1 : numgroups
                cd([sesdir filesep 'group' num2strpad(g(p),4)])
                gdir = pwd;
                if Args.match_locked
                    psth_pairs = [ 'matchlockedpsth' num2strpad(g(p),4)];
                else
                    psth_pairs = [ 'psth' num2strpad(g(p),4)];
                end
                
                %determine number of clusters
                nc = nptdir('cluster*');
                numnc = size(nc,1);
                
                %run for 5 ides, all trials, incorrect
                for all_ides = 1 : 10
                    idetrials = trials{all_ides};
                    
                    for n = 1 : numnc
                        cd([gdir filesep nc(n).name])
                        load ispikes.mat
                        %get all spike times
                        all_times = [];
                        c = 0;
                        
                        if Args.match_locked
                            binx = [-2200+(binsize/2):binsize:200-(binsize/2)];
                        else
                            binx = [-500+(binsize/2):binsize:1500-(binsize/2)];
                        end
                        nbins = size(binx,2);
                        
                        binned_times = [];
                        for t = idetrials
                            c = c + 1;
                            if Args.match_locked
                                %lock to the match time
                                corrected_sp_times = sp.data.trial(t).cluster.spikes - match_on(t);
                                corrected_sp_times(find(corrected_sp_times > 200)) = [];
                                corrected_sp_times(find(corrected_sp_times < -2200)) = [];
                                binned_times(c,:) = hist(corrected_sp_times,binx); %leave as counts
                                all_times = [all_times corrected_sp_times];
                            else
                                %subtract sample on time so that spikes are aligned to sample on
                                corrected_sp_times = sp.data.trial(t).cluster.spikes - sample_on(t);
                                corrected_sp_times(find(corrected_sp_times > 1500)) = [];
                                corrected_sp_times(find(corrected_sp_times < -499)) = [];
                                binned_times(c,:) = hist(corrected_sp_times,binx); %leave as counts
                                all_times = [all_times corrected_sp_times];
                            end
                        end
                        psth{n,all_ides} = all_times;
                        psthbins{n,all_ides} = binned_times;
                        trial_count{n,all_ides} = size(idetrials,2);
                        average_rate{n,all_ides} = sum(sum(psthbins{n,all_ides})) / (trial_count{n,all_ides} * 2); %trial lengths are 2 seconds
                    end
                end
                
                correct_avgfixation_pvals = {}; %these are pvalues for an averaged portion of the interleaved fixation trials
                
                correct_fixation_pvals = {};
                correct_fixblock_pvals = {};
                fixation_fixblock_pvals = {};
                correct_blank_pvals = {};
                correct_fixation_resp = zeros(numnc,nbins);
                ide_pvals = {};
                for n = 1 : numnc
                    for nb = 1 : nbins %just multi unit for now
                        
                        %                         correct_avgfixation_pvals{n}(nb) = ranksum(psthbins{n,6}(:,nb),(sum(psthbins{n,8}(:,50:89) * .02) ./ (40 * .02)')); %this uses a delay baseline
                        %                         correct_avgfixation_pvals{n}(nb) = ranksum(log(psthbins{n,6}(:,nb)+1),log((sum(psthbins{n,8}(:,61:70) * .02,2) ./ (10 * .02))+1)); %this uses a delay baseline
                        
                        
                        correct_fixation_pvals{n}(nb) = ranksum(psthbins{n,8}(:,nb),psthbins{n,6}(:,nb));
                        if (sum((psthbins{n,6}(:,nb))) ./ (size(psthbins{n,6},1))) >= (sum((psthbins{n,8}(:,nb))) ./ (size(psthbins{n,8},1)))
                            correct_fixation_resp(n,nb) = 1;
                        else
                            correct_fixation_resp(n,nb) = -1;
                        end
                        
                        if fixblock
                            correct_fixblock_pvals{n}(nb) = ranksum(psthbins{n,9}(:,nb),psthbins{n,6}(:,nb));
                            fixation_fixblock_pvals{n}(nb) = ranksum(psthbins{n,9}(:,nb),psthbins{n,8}(:,nb));
                        end
                        
                        if blankblock
                            correct_blank_pvals{n}(nb) = ranksum(psthbins{n,10}(:,nb),psthbins{n,6}(:,nb));
                        end
                        %setup data for kw test
                        idegroups = [];
                        idevals = [];
                        for idex = 1:5
                            idegroups = [idegroups (ones(1,size(psthbins{n,idex},1))*idex)];
                            idevals = [idevals psthbins{n,idex}(:,nb)'];
                        end
                        ide_pvals{n}(nb) = kruskalwallis(idevals,idegroups,'off');
                    end
                end
                if Args.plot
                    
                    if Args.match_locked
                        subplot(6,1,1)
                        h1 = hist(psth{1,6},binx) ./ (trial_count{1,6} * ((2400/nbins)/1000));
                        h2 = hist(psth{1,8},binx) ./ (trial_count{1,8} * ((2400/nbins)/1000));
                        plot(h1,'b');hold on
                        plot(h2,'r');hold on
                        
                        for nn = 1 : nbins
                            if correct_fixation_pvals{1}(nn) < .001
                                scatter(nn,h1(nn),'r','filled');hold on
                            end
                        end
                    else
                        subplot(6,1,1)
                        h1 = hist(psth{1,6},binx) ./ (trial_count{1,6} * ((2000/nbins)/1000));
                        h2 = hist(psth{1,8},binx) ./ (trial_count{1,8} * ((2000/nbins)/1000));
                        plot(h1,'b');hold on
                        plot(h2,'r');hold on
                        
                        for nn = 1 : nbins
                            if correct_fixation_pvals{1}(nn) < .001
                                scatter(nn,h1(nn),'r','filled');hold on
                            end
                        end
                        
                        if fixblock
                            subplot(6,1,2)
                            h1 = hist(psth{1,6},binx) ./ (trial_count{1,6} * ((2000/nbins)/1000));
                            h2 = hist(psth{1,9},binx) ./ (trial_count{1,9} * ((2000/nbins)/1000));
                            plot(h1,'b');hold on
                            plot(h2,'r');hold on
                            
                            for nn = 1 : nbins
                                if correct_fixblock_pvals{1}(nn) < .001
                                    scatter(nn,h1(nn),'r','filled');hold on
                                end
                            end
                            
                            subplot(6,1,3)
                            h1 = hist(psth{1,8},binx) ./ (trial_count{1,8} * ((2000/nbins)/1000));
                            h2 = hist(psth{1,9},binx) ./ (trial_count{1,9} * ((2000/nbins)/1000));
                            plot(h1,'b');hold on
                            plot(h2,'r');hold on
                            
                            for nn = 1 : nbins
                                if fixation_fixblock_pvals{1}(nn) < .001
                                    scatter(nn,h1(nn),'r','filled');hold on
                                end
                            end
                        end
                        
                        %blank block
                        if blankblock
                            subplot(6,1,4)
                            h1 = hist(psth{1,6},binx) ./ (trial_count{1,6} * ((2000/nbins)/1000));
                            h2 = hist(psth{1,10},binx) ./ (trial_count{1,10} * ((2000/nbins)/1000));
                            plot(h1,'b');hold on
                            plot(h2,'r');hold on
                            
                            for nn = 1 : nbins
                                if correct_blank_pvals{1}(nn) < .001
                                    scatter(nn,h1(nn),'r','filled');hold on
                                end
                            end
                        end
                        
                        %IDE plot
                        subplot(6,1,5)
                        for ad = 1 : 5
                            hh = hist(psth{1,ad},binx) ./ (trial_count{1,ad} * ((2000/nbins)/1000));
                            plot(hh);hold on
                            
                        end
                        for nn = 1 : nbins
                            if ide_pvals{1}(nn) < .001
                                scatter(nn,hh(nn),'r','filled');hold on
                            end
                        end
                        
                        %                     %correct vs a delay baseline
                        %                     subplot(6,1,6)
                        %
                        %                     h1 = hist(psth{1,6},binx) ./ (trial_count{1,6} * ((2000/nbins)/1000));
                        %                     h2 = hist(psth{1,8},binx) ./ (trial_count{1,8} * ((2000/nbins)/1000));
                        %                     plot(h1,'b');hold on
                        %                     plot(h2,'r');hold on
                        %
                        %                     for nn = 1 : nbins
                        %                         if correct_avgfixation_pvals{1}(nn) < .001
                        %                             scatter(nn,h1(nn),'r','filled');hold on
                        %                         end
                        %                     end
                        %
                        
                    end
                    pause
                    clf
                end
                
                cd(highpassdir)
                if ~exist('psth','dir')
                    mkdir('psth')
                end
                cd([highpassdir filesep 'psth']);
                
                write_info = writeinfo(dbstack);
                save(psth_pairs,'psth','psthbins','trial_count','correct_fixation_pvals','correct_fixblock_pvals','fixation_fixblock_pvals','correct_blank_pvals','correct_avgfixation_pvals','ide_pvals','correct_fixation_resp','binsize','average_rate','numnc','write_info');
            end
        end
    end
end

cd(monkeydir)












