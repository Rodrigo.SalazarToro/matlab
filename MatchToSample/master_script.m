function master_script(varargin)


%run at monkey dir
%majority of code from:
%-sliding_window_xcorr_sript.m
%-run_hilbert_phase.m,

%NOTES:
%-clarks data uses channel number and not group number. With parietal channels first and then frontal channels.
%-bettys data uses GROUP numbers with original orientation 1:32 for Frontal and 33:64 for Parietal

Args = struct('ml',0,'ide_only',0,'days',[],'sessions',[],'monkey','betty','windowSize',200,'windowStep',50,'lag',50,'lowpass',0,...
    'channelpairs',0,'redo_channelpairs',0,'threshold',0,'fit_gabors',0,...
    'glm',0,'average_corr',0,'average_corr_surrogate',0,'hilbert',0,'fixation',0,...
    'hsfpp',0,'redo_hsfpp',0,'bmf',0,'bandpass_filtering',0,'redo_bandpass_filtering',0,...
    'relative_phase_angle',0,'redo_relative_phase_angle',0,'hilbert_entropy',0,'redo_hilbert_entropy',0,'hilbert_entropy_surrogate',0,...
    'redo_hilbert_entropy_surrogate',0,'plv_thresh',0,'run_thresh',0,'hilbert_mi',0,...
    'hilbert_entropy_hsfpp',0,'hilbert_mi_surrogate',0,'hsfpp_sliding_window',0,...
    'spike_hilbert',0,'granger',0,'power_epoch',0,'power_tf',0,'bettynlynx',0,'writepwtrials',0,'spikexc',0,'spikecoh',0,'spikecoh_redo',0,'spikecoh_surrogate',0);

Args.flags = {'ml','ide_only','windowSize','windowStep','lag','lowpass','channelpairs',...
    'redo_channelpairs','threshold','fit_gabors', 'glm','average_corr','average_corr_surrogate',...
    'hilbert','fixation','bmf','hsfpp','redo_hsfpp','bandpass_filtering','redo_bandpass_filtering',...
    'relative_phase_angle','redo_relative_phase_angle','hilbert_entropy','redo_hilbert_entropy','redo_hilbert_entropy_surrogate',...
    'hilbert_entropy_surrogate','plv_thresh','run_thresh','hilbert_entropy_hsfpp','hilbert_mi','hilbert_mi_surrogate',...
    'hsfpp_sliding_window','spike_hilbert','granger','power_epoch','power_tf','bettynlynx','writepwtrials','spikexc','spikecoh','spikecoh_redo','spikecoh_surrogate'};

Args = getOptArgs(varargin,Args);

monkeylevel = pwd;
cd([monkeylevel filesep Args.monkey]);
monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    for s = Args.sessions;
        cd(daydir)
        if exist(['session0' num2str(s)],'dir')
            cd([daydir filesep 'session0' num2str(s)])
            sesdir = pwd;
            if Args.ml
                if Args.bmf
                    mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
                    r = 1;
                    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
                else
                    mt = mtstrial('auto','ML','RTfromML','redosetNames');
                    r = 1; %only identity need to save in identity dir
                    total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ML','rule',r);
                end
            else
                mt = mtstrial('auto','redosetNames');
                %determine rule
                r = mt.data.Index(1,1);
                total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',r);
            end
            %if ide only then skip session if it is location(2)
            if ((Args.ide_only && r == 1) || (~Args.ide_only)) && ~isempty(total_trials)
                
                %% SLIDING WINDOW CROSS-CORRELATION ANALYSIS
                %LOWPASS CHANNELS FOR SLIDING WINDOW CROSS-CORRELATION ANALYSIS
                if Args.lowpass
                    if Args.ml
                        if Args.bmf
                            lowpass_xcorr_analysis('ml','bmf','lowpasslow',8,'lowpasshigh',25)
                        elseif Args.bettynlynx
                            lowpass_xcorr_analysis('ml','lowpasslow',8,'lowpasshigh',25,'bmf') %run like bmf data
                        else
                            lowpass_xcorr_analysis('ml','lowpasslow',8,'lowpasshigh',25)
                        end
                    else
                        lowpass_xcorr_analysis('lowpasslow',8,'lowpasshigh',25)
                    end
                end
                %MAKE AVERAGE CORRELOGRAMS
                if Args.average_corr
                    cd(sesdir)
                    if Args.ml
                        if Args.bmf
                            average_correlograms('ml','bmf')
                        else
                            if Args.bettynlynx
                                average_correlograms('ml','bettynlynx','correct_iti')
                            else
                                average_correlograms('ml','rules',4)
                            end
                        end
                    else
                        average_correlograms('rules',1)
                    end
                end
                %CURRENTLY NOT USED
                %                 if Args.average_corr_surrogate
                %                     cd(sesdir)
                %                     if Args.ml
                %                         average_correlograms_surrogate('ml')
                %                     else
                %                         average_correlograms_surrogate
                %                     end
                %                 end
                %FIT GENERALIZED GABORS TO THE AVERAGE CORRELOGRAMS
                if Args.fit_gabors
                    if Args.ml
                        if Args.bmf
                            fit_generalized_gabor_all('ml','bmf','rule',1)
                            fit_generalized_gabor_all('ml','bmf','rule',4)
                        elseif Args.bettynlynx
                            fit_generalized_gabor_all('ml','rule',1,'bettynlynx','correct_iti')
                            fit_generalized_gabor_all('ml','rule',4,'bettynlynx')
                        else
%                             fit_generalized_gabor_all('ml','rule',1)
%                             fit_generalized_gabor_all('ml','rule',2)
%                             fit_generalized_gabor_all('ml','rule',3)
                            fit_generalized_gabor_all('ml','rule',4)
                        end
                    else
                        fit_generalized_gabor_all('rule',1)
                    end
                end
                %CHANNELPAIRS(SLIDING WINDOWCROSS-CORRELATION ANALYSIS
                if Args.channelpairs
                    cd(sesdir)
                    if Args.ml
                        if Args.bmf
                            sliding_window_channelpairs('ml','redo_channelpairs','bmf')
                        else
                            sliding_window_channelpairs('ml','redo_channelpairs')
                        end
                    else
                        sliding_window_channelpairs('redo_channelpairs')
                    end
                end
                %WRITE THRESHOLDS FOR CHANNELPAIRS(SLIDING WINDOW CROSS-CORRELATION ANALYSIS)
                if Args.threshold
                    cd(sesdir)
                    if Args.ml
                        sliding_window_thresholds('ml','ide_only',Args.ide_only)
                    else
                        sliding_window_thresholds('ide_only',Args.ide_only)
                    end
                end
                %RUN GENERALIZED LINEAR MODELS FOR PROBABILITY OF SIGNIFICANT CROSS-CORRELATION
                if Args.glm
                    if Args.ml
                        glm_analysis('ml');
                    else
                        glm_analysis;
                    end
                end
                %CURRENTLY NOT USED
                %                 if Args.hilbert
                %                     if Args.ml
                %                         write_hilbert_trials('ml')
                %                     else
                %                         write_hilbert_trials
                %                     end
                %                 end
                
                %% HILBERT SPIKE-FIELD PREFERRED PHASE ANALYSIS
                %RUN HILBERT SPIKE-FIELD PREFERRED PHASE
                if Args.hsfpp % previously know as run_hilbert_phase.m
                    if Args.ml
                        
                        run_hsfpp('ml','incorrect')
                        
%                         if Args.redo_hsfpp
%                             run_hsfpp('ml','redo')
%                         else
%                             run_hsfpp('ml')
%                         end
                    else
                        if Args.redo_hsfpp
                            run_hsfpp('redo')
                        else
                            run_hsfpp
                        end
                    end
                end
                
                %% SLIDING WINDOW HILBERT PHASE ANALYSIS
                %run the bandpass and hilbert transform (saves file for
                %each bandpass)  ****NO LONGER USING THIS, TAKES UP TO MUCH
                %SPACE!!!
                if Args.bandpass_filtering
                    if Args.redo_bandpass_filtering
                        if Args.ml
                            run_bandpass_filtering('ml','redo')
                        else
                            run_bandpass_filtering('redo')
                        end
                    else
                        if Args.ml
                            run_bandpass_filtering('ml')
                        else
                            run_bandpass_filtering
                        end
                        
                    end
                end
                cd(sesdir)
                
                
                %calculate the relative phase angles
                if Args.relative_phase_angle
                    if Args.redo_relative_phase_angle
                        if Args.ml
                            run_relative_phase_angle('ml','redo')
                        else
                            run_relative_phase_angle('redo')
                        end
                    else
                        if Args.ml
                            run_relative_phase_angle('ml')
                        else
                            run_relative_phase_angle
                        end
                    end
                end
                cd(sesdir)
                %calculate the normalized shannon entropy
                if Args.hilbert_entropy
                    if Args.redo_hilbert_entropy
                        if Args.ml
                            run_hilbertphase_entropy('ml','redo')
                        else
                            run_hilbertphase_entropy('redo')
                        end
                        
                    else
                        if Args.ml
                            run_hilbertphase_entropy('ml')
                        else
                            run_hilbertphase_entropy
                        end
                    end
                end
                cd(sesdir)
                %calculate the normalized shannon entropy
                if Args.hilbert_entropy_surrogate
                    if Args.redo_hilbert_entropy_surrogate
                        if Args.ml
                            run_hilbertphase_entropy_surrogate('ml','redo')
                        else
                            run_hilbertphase_entropy_surrogate('redo')
                        end
                    else
                        if Args.ml
                            run_hilbertphase_entropy_surrogate('ml')
                        else
                            run_hilbertphase_entropy_surrogate
                        end
                    end
                end
                cd(sesdir)
                
                
                %caculate the bias and threshold for the plv surrogates
                if Args.plv_thresh
                    if Args.ml
                        fit_plv_thresh('ml')
                    else
                        fit_plv_thresh
                    end
                end
                cd(sesdir)
                
                
                %threshold data and save results
                if Args.run_thresh
                    threshold_plv
                end
                cd(sesdir)
                
                
                
                if Args.hilbert_entropy_hsfpp
                    run_hilbertphase_entropy_hsfpp
                end
                
                %calculate mi based on the normalized shannon entropies
                if Args.hilbert_mi
                    if Args.ml
                        run_hilbertphase_mi('ml')
                    else
                        run_hilbertphase_mi
                    end
                end
                cd(sesdir)
                %calculate mi surrogates based on the normalized shannon entropies
                if Args.hilbert_mi_surrogate
                    if Args.ml
                        run_hilbertphase_mi_surrogate('ml')
                    else
                        run_hilbertphase_mi_surrogate
                    end
                end
                cd(sesdir)
                %%
                if Args.hsfpp_sliding_window
                    if Args.ml
                        run_hsfpp_sliding_window('ml')
                    else
                        run_hsfpp_sliding_window
                    end
                end
                    
                
                
                %run spike_hilbert_analysis (results used to make spikeplvhsfpp object)
                if Args.spike_hilbert
                spike_hilbert_analysis
                    
                end
                
                
                if Args.granger
                    
                    epoch_granger('ml','rules',1)
                    
                end
                
                if Args.writepwtrials
                    if Args.ml
                        write_power_trials('ml') %object is made with power_obj.m (saved in lfp/power)
                    else
                        write_power_trials;
                    end
                end
                
                if Args.power_epoch
                    epoch_power('ml')
                end
                
                if Args.power_tf
                    if Args.ml
                        if Args.bmf
                            tf_power('ml','rules',[1 4],'bmf')
                        elseif Args.bettynlynx
                            tf_power('ml','rules',[1 4],'bettynlynx')
                        end
                    else
                        tf_power
                    end
                end
                
                
                
                
                %spike-spike cross correlations
                if Args.spikexc
                    if Args.ml
                        spikexcorr('ml')
                    else
                        spikexcorr
                    end
                    
                end
                
                
                %spike-spike coherence
                if Args.spikecoh
                    if Args.spikecoh_redo
                        if Args.ml
                            epoch_spike_spike_coh('ml','rules',[1],'redo')
                        else
                            epoch_spike_spike_coh('rules',[1],'redo')
                        end
                    else
                        if Args.ml
                            epoch_spike_spike_coh('ml','rules',[1])
                        else
                            epoch_spike_spike_coh('rules',[1])
                        end
                    end
                end
                
                                %spike-spike coherence
                if Args.spikecoh_surrogate
                    if Args.ml
                        epoch_spike_spike_coh_surrogate('ml','rules',[1])
                    else
                        epoch_spike_spike_coh_surrogate('rules',[1])
                    end
                end
            end
        end
    end
end


cd(monkeylevel)

