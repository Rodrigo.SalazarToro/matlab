function get_mi_results


%run at day level
cd('/media/raid/data/monkey/clark/')
load idedays
days = idedays;

mdir = pwd;
%determine which sessions are IDE for clark
% ses = cell(1,29);
% for x = 1:29
%     cd([mdir filesep idedays{x} filesep 'session02']);
%     m = mtstrial('auto')
%     if m.data.Index(1) == 1;ses{x} = 'session02';
%     else
%         ses{x} = 'session03'
%     end
% end

load ideses



allc = zeros(3,33);
miloc = cell(1,3);
miloc_bias = cell(1,3);

for x = 1 : size(days,2)
    
    cd([mdir filesep days{x} filesep ses{x}])
    
    [c mi_bias mi_no_bias] = mi_single_pair_summary;
    
    allc = allc + c;
    
    for xx = 1:3
        if isempty(miloc{xx})
            miloc{xx} = mi_no_bias{xx};
            miloc_bias{xx} = mi_bias{xx};
        else
            miloc{xx} = [miloc{xx}; mi_no_bias{xx}];
            miloc_bias{xx} = [miloc_bias{xx}; mi_bias{xx}];
            
        end
    end
end
cd(mdir)
allc