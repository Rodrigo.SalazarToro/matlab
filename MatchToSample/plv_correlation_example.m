function plv_correlation_example


% cd('/media/raid/data/monkey/clark/060328/session02/lfp/lfp2')
% load hilbertentropy0102.mat

% cd('/media/raid/data/monkey/clark/060502/session02/lfp/lfp2')
% load hilbertentropy0204.mat

cd('/media/raid/data/monkey/clark/060511/session02/lfp/lfp2')

he = nptdir('hilbertentropy*');
nhe = size(he,1);
for allhe = 1 : nhe
    load(he(allhe).name)
    
    hentropy = hilbertentropy{1};
    
%     for c = 9%5:13
% hentropy = hilbertentropy{c};
        
        
        
        f = 6; %frequency(20Hz,6)
        t = 30;
        cutoff = 0.90 ^2;
        
        % figure;plot(squeeze(hentropy(f,t,:)))
        %
        % figure;for x = 1:36;subplot(6,6,x);scatter(squeeze(hentropy(6,x,:)),squeeze(hentropy(6,25,:)));lsline;end
        %
        %     figure;corrs = [];for x = 1:36;corrs(x) = xcorr(squeeze(hentropy(6,x,:)),squeeze(hentropy(6,25,:)),0,'coef');end;plot(corrs)
        
        
        
        %show time and frequency correlation
        figure
        counter = 0;
        allmin = 1;
        for allx = 1:2:36
            counter = counter + 1;
            for x = 1:36;
                for y = 1:16
                    allcorrs{counter}(y,x) = xcorr(squeeze(hentropy(f,x,:)),squeeze(hentropy(y,allx,:)),0,'coef') ^2;
                    
                    %             run 100 surrogates
%                                 surr = [];
%                                 for p = 1:100
%                                     h1 = squeeze(hentropy(f,x,:))';
%                                     h2 = squeeze(hentropy(y,allx,:))';
%                                     p1 = randperm(size(h1,2));
%                                     p2 = randperm(size(h2,2));
%                                     surr(p) = xcorr(h1(p1),h2(p2),0,'coef') ^2;
%                                 end
%                                 if allcorrs{counter}(y,x) > prctile(surr,99.9)
%                                     allcorrsthresh{counter}(y,x) = 1;
%                                 else
%                                     allcorrsthresh{counter}(y,x) = 0;
%                                 end
                    
                end
            end
            m = min(min(allcorrs{counter}));
            if m < allmin
                allmin = m;
            end
        end
        for c = 1 : counter
            subplot(3,6,c)
            corrs = allcorrs{c};
            corrs(find(corrs > cutoff)) = nan;
            
            imagesc((flipud(corrs)))
            set(gca,'XTick',[1:2:36])
            
            set(gca,'XTickLabel',steps([1:2:36]))
            
            set(gca,'YTick',[1:16])
            set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
            %sample on
            hold on
            plot([8, 8],[0 33],'color','k')
            %sample off
            hold on
            plot([18, 18],[0 33],'color','k')
            %earliest match
            hold on
            plot([34, 34],[0 33],'color','k')%         figure
%         for c = 1 : counter
%             subplot(3,6,c)
%             corrs = allcorrsthresh{c};
%         
%             imagesc((flipud(corrs)))
%             set(gca,'XTick',[1:2:36])
%         
%             set(gca,'XTickLabel',steps([1:2:36]))
%         
%             set(gca,'YTick',[1:16])
%             set(gca,'YTickLabel',fliplr(all_freq([1:size(all_freq,2)])))
%             %sample on
%             hold on
%             plot([8, 8],[0 33],'color','k')
%             %sample off
%             hold on
%             plot([18, 18],[0 33],'color','k')
%             %earliest match
%             hold on
%             plot([34, 34],[0 33],'color','k')
%             hold on
%         
%         end

            hold on
            
            set(gca, 'CLim', [allmin cutoff+.01]);
            
            allsums(c) = sum(sum(allcorrs{c}));
        end
        
        
        
        % close all
%     end
    close all
end
hentropy;