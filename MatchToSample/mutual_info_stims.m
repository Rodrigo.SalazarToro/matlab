function mi = mutual_info_stims(varargin)

Args = struct('posterior_probs',[],'prob_response',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

num_stims = size(Args.posterior_probs,2);

m = [];
for x = 1 : num_stims
    % mi = sum( P(r)*P(s|r) log2(P(s|r) / P(s)) )
    m(x) = Args.prob_response * Args.posterior_probs(x) * log2(Args.posterior_probs(x) / (1/num_stims));
end

mi = nansum(m);
