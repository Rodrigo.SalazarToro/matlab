function threshold_plv(varargin)


%run at session level
%uses the fit surrogates to threshold the data

Args = struct('ml',0);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

sesdir = pwd;
cd([sesdir filesep 'lfp' filesep 'lfp2'])

hentropy = nptDir('hilbertentropy*');
nfiles = size(hentropy,1);

freq_range = [5 11;12 25;26 40;41 70];
nfreq = size(freq_range,1);
%100:500, 600:1000, 1000:1400, 1400:1800
epochs = [200 400; 700 900;1100 1300; 1500 1700];
nepochs = size(epochs,1);

for f = 1 : nfiles
    load(hentropy(f).name)
    
    meanent = mean(hilbertentropy{1},3);
    meanent = meanent(:,1:36);
    
    meanph = circ_mean(mean_phases{1},[],3);
    meanph = meanph(:,1:36);
    
    load(['threshsurrogate_' hentropy(f).name])
    thresh = all_thresh{1};
    
    %get groups
    g1 = groups(1);
    g2 = groups(2);
    
    freqs = all_freq;
    times = steps;
    thresh3 = thresh{3}; %(1) 10^-3, (2) 10^-4, (3) 10^-5
    
    %split sigim into frequencies. determine the number of significant bins
    %and the average coh value for those significant bins
    sigim = zeros(size(meanent,1),size(meanent,2));
    sigim(find(meanent >= thresh3)) = 1; %find values above the surrogate threshold
    
    for e = 1 : nepochs
        for nf = 1 : nfreq
            fs = find(freqs >= freq_range(nf,1) & freqs <= freq_range(nf,2));
            t = find(times >= epochs(e,1) & times <= epochs(e,2));
            sig.fcount(e,nf) = size(find(sigim(fs,t)),1);
            sig.fmean(e,nf) = mean(mean(meanent(fs,t)));
            sig.ftotalbins(e,nf) = size(fs,2)*size(t,2);
            
            sig.fphase(e,nf) = circ_mean(circ_mean(meanph(fs,t)),[],2);
        end
    end

    fname = [ 'epochthresh_hilbertentropy' num2strpad(g1,3) num2strpad(g2,3)];
    save(fname,'sig','sigim','groups','all_freq','steps','thresh3','meanent');
    
% %     figure;subplot(3,1,1);imagesc(meanent),colorbar;subplot(3,1,2);imagesc(all_thresh{1}{1}),colorbar;subplot(3,1,3);imagesc(sigim),colorbar;
% %     pause
% %     close all
end

cd(sesdir)
