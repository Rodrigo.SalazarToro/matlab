function [spect,sig,f] = supplFig1(obj)


spect = cell(2,1);

for sel = 1 : 2
    if sel ==1
    [r,ind] = get(obj,'Number','snr',99);
    else
        [r,ind] = get(obj,'Number','snr',99,'type',{'betapos34' [1 3]});
    end
    for ii = 1 : r
        
        cd(obj.data.setNames{ind(ii)})
        pair = obj.data.Index(ind(ii),1);
        
        load cohInter.mat
        if Day.session(1).rule == 1
            spect{sel}(ii,:,:) = squeeze(Day.session(1).C(:,pair,:));
        else
            spect{sel}(ii,:,:) = squeeze(Day.session(2).C(:,pair,:));
        end
        
        load generalSur.mat
        
        if Day.session(1).rule == 1
            sig(ii,:,:) = squeeze(spect(ii,:,:)) > squeeze(Day.session(1).Prob(:,4,1,:));
        else
            sig(ii,:,:) = squeeze(spect(ii,:,:)) > squeeze(Day.session(2).Prob(:,4,1,:));
        end
        
    end
end

epoch = {'pre-sample' 'sample' '1st delay' '2nd delay'};
figure
for p = 1 : 4
    
    subplot(2,1,p)
    d = squeeze(prctile(spect{1},[10 25 50 75 90],1));
    plot(f,squeeze(d(:,:,p)))
    title(epoch{p})
    
    subplot(2,1,p+4)
    d = squeeze(prctile(spect{2},[10 25 50 75 90],1));
    plot(f,squeeze(d(:,:,p)))
    title(epoch{p})
end

xlabel('Frequency [Hz]')
ylabel('Coherency')
legend('10th percentile','25th percentile','50th percentile','75th percentile','90th percentile')
% cd /Volumes/raid/data/monkey/clark
% obj = loadObject('cohInterIDE.mat');
% [clark,sigC,f] = supplFig1(obj);
% cd /Volumes/raid/data/monkey/betty
% obj = loadObject('cohInterIDEandLong.mat');
% [betty,sigB,f] = supplFig1(obj);
% figure
% epoch = {'pre-sample' 'sample' '1st delay' '2nd delay'};
% for p = 1 : 4
%
%     subplot(4,4,p)
%     d = squeeze(prctile(clark,[10 25 50 75 90],1));
%     plot(f,squeeze(d(:,:,p)))
%     title(epoch{p})
%
%     subplot(4,4,p+4)
%     d = 100*sum(sigC,1) / size(sigC,1);
%     plot(f,squeeze(d(:,:,p)))
%     title(epoch{p})
%
%
%     subplot(4,4,p+8)
%     d = squeeze(prctile(betty,[10 25 50 75 90],1));
%     plot(f,squeeze(d(:,:,p)))
%     subplot(4,4,p+12)
%     d = 100*sum(sigB,1) / size(sigB,1);
%     plot(f,squeeze(d(:,:,p)))
%
%
% end
%
% xlabel('Frequency [Hz]')
% ylabel('Coherency')
% for sb = 1 : 16;  subplot(4,4,sb); xlim([2 45]); end
% for sb = [1 2 3 4 9 10 11 12];  subplot(4,4,sb); ylim([0 0.4]); end
% for sb = [5 6 7 8 13 14 15 16];  subplot(4,4,sb); ylim([0 85]); end
% subplot(4,4,1); ylabel('clark')
% subplot(4,4,9); ylabel('betty')
% subplot(4,4,8); ylabel('% sig.')
% legend('10th percentile','25th percentile','50th percentile','75th percentile','90th percentile')