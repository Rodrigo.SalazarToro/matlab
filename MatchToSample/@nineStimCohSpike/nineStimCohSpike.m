function obj = nineStimPsth(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'selectedCells',[]);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'nineStimCohSpike';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'nineStCo';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

mtst = mtstrial('auto',varargin{:});
combs = nptDir('comb*');
sdir = pwd;
rules = {'I','L'};
if  ~isempty(combs) & ~isempty(mtst)
    count = 1 ;
    data.Index = [];

    for cm = 1 : size(combs,1)
        cd(combs(cm).name)
        if ~isempty(Args.selectedCells)
            indg = findstr(combs(cm).name,'g');
            thegroups = {[sdir '/group' combs(cm).name(indg(1)+1 : indg(1) + 4) '/cluster' combs(cm).name(indg(1) + 5: indg(1) + 7)] [sdir '/group' combs(cm).name(indg(2)+1 : indg(2) + 4) '/cluster' combs(cm).name(indg(2) + 5: indg(2) + 7)]};
            if ~isempty(strmatch(thegroups{1},Args.selectedCells)) | ~isempty(strmatch(thegroups{2},Args.selectedCells))
                select = true;
            else
                select = false;
            end
        else
            select = true;
        end

        nSfile = nptDir('nineStimCohSPIKES.mat');
        if ~isempty(nSfile) & select
            % Index column wise: day,rule,cortex,unit,count

            load(nSfile.name)
            bad = []; for c = 1 : 9; if length(xcova{c}) == 1 | isempty(xcova{c}) | (length(unique(isnan(squeeze(xcova{c})))) == 1 && unique(isnan(squeeze(xcova{c}))) == 1) ; bad = [bad 1]; else bad = [bad 0]; end; end;
            if length(unique(bad)) > 1 |unique(bad) == 0
                data.Index(count,1) = 1;
                data.Index(count,2) = rules{unique(mtst.data.Index(:,1))};

                if strcmp(info.cortex,'PP')
                    data.Index(count,3) = 'P';
                elseif strcmp(info.cortex,'FF')
                    data.Index(count,3) = 'F';
                elseif strcmp(info.cortex,'PF') | strcmp(info.cortex,'FP')
                    data.Index(count,3) = 'T';
                end

                if strcmp(info.unit,'mm')
                    data.Index(count,4) = 'm';
                elseif strcmp(info.unit,'ss')
                    data.Index(count,4) = 's';
                elseif strcmp(info.unit,'ms') | strcmp(info.unit,'sm')
                    data.Index(count,4) = 'H';
                end

                data.Index(count,5) = count;
                nt = []; for c = 1 : 9; nt = [nt length(trials{c})]; end
                data.Index(count,6) = min(nt);

                ne = []; for c = 1 : 9; ne = [ne min(nevent{c})]; end
                data.Index(count,7) = min(ne);

                data.Index(count,8) = maxc;

                pop = []; for c = 1 : 9; pop = [ne max(popout{c})]; end
                data.Index(count,9) = max(ne);

                %                 aT = []; for c = 1 : 9; aT = [aT; unique((yfit{c} > repmat(thresh{c},1,length(yfit{c}))))]; end
                %                 data.Index(count,9) = max(aT);

                data.setNames{count} = pwd;
                count = count + 1;
            else
                obj = createEmptyObject(Args);
            end


        end %c = 1 : size(clusters,1)
        cd(sdir)
    end %g = 1 : size(group,1)

    data.numSets = count-1; % nbr of trial

    % create nptdata so we can inherit from it
    n = nptdata(data.numSets,0,pwd);
    d.data = data;
    obj = class(d,Args.classname,n);
    if(Args.SaveLevels)
        fprintf('Saving %s object...\n',Args.classname);
        eval([Args.matvarname ' = obj;']);
        % save object
        eval(['save ' Args.matname ' ' Args.matvarname]);
    end


else
    % create empty object
    fprintf('The mtstrial object is empty or the group directory does not exist \n');
    obj = createEmptyObject(Args);

end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
