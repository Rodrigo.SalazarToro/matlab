function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'day',[],'rule',[],'cortex',[],'unit',[],'mintrials',[],'minEvent',[],'maxC',[],'popout',[]);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    
    if ~isempty(Args.day)
        rtemp1 = find(obj.data.Index(:,1) == Args.day);
    else
        rtemp1 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.rule)
        rtemp2 = find(obj.data.Index(:,2) == cell2mat(Args.rule));
        else
        rtemp2 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.cortex)
        rtemp3 = find(obj.data.Index(:,3) == cell2mat(Args.cortex));
        else
        rtemp3 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.unit)
        rtemp4 = find(obj.data.Index(:,4) == cell2mat(Args.unit));
        else
        rtemp4 = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.mintrials)
        rtemp5 = find(obj.data.Index(:,6) >= Args.mintrials);
        else
        rtemp5 = [1 : size(obj.data.Index,1)];
    end
     if ~isempty(Args.minEvent)
        rtemp6 = find(obj.data.Index(:,7) >= Args.minEvent);
        else
        rtemp6 = [1 : size(obj.data.Index,1)];
     end
     if ~isempty(Args.maxC)
        rtemp7 = find(obj.data.Index(:,8) >= Args.maxC);
        else
        rtemp7 = [1 : size(obj.data.Index,1)];
     end
    
     if ~isempty(Args.popout)
        rtemp8 = find(obj.data.Index(:,9) >= Args.popout);
        else
        rtemp8 = [1 : size(obj.data.Index,1)];
     end
    
    varargout{1} = intersect(rtemp1,intersect(rtemp2,intersect(rtemp3,intersect(rtemp4,intersect(rtemp5,intersect(rtemp6,intersect(rtemp7,rtemp8)))))));
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});

end

