function obj = plot(obj,varargin)


Args = struct('raster',0,'TSappend',0);
Args.flags = {'raster','TSappend'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'raster','TSappend'});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

cueComb = [1 2 3 1 2 3 1 2 3;1 1 1 2 2 2 3 3 3];



c = 1; for cue = 1 : 9; labels{c} = sprintf('ide %d; loc %d',cueComb(2,c),cueComb(1,c)); c = c + 1; end

if ~isempty(Args.NumericArguments)

    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else

end
cd(obj.data.setNames{ind})

load nineStimCohSPIKES.mat

if Args.TSappend

else

end

if Args.raster
    % needs some work
    %     for cue = 1 : 9
    %         for al = 1 : 3
    %             subplot(9,3,(cue-1) * 3 + al);
    %
    %             imagesc(A{cue,al} == 0)
    %             colormap(gray)
    %
    %             hax = gca;
    %             set(hax,'Xtick',[1 : abs(reference{cue,al}) : size(A{cue,al},2)]);
    %             set(hax,'XTickLabel',[reference{cue,al} : abs(reference{cue,al}) : size(A{cue,al},2)+reference{cue,al}]);
    %             if al == 1; ylabel(labels{cue}); end
    %         end
    %     end
    %
    %     fprintf('%s%s%s \n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2))
else

    allymin = [];
    allymax = [];
    for cue = 1 : 9
        for al = 1 : 5
            subplot(9,5,(cue-1) * 5 + al);
            if Args.TSappend; plot(lags,append{cue}.cor(al,:));else; plot(lags,xcova{cue}(al,:));end
            if length(lags) > 1 & ~isnan(minc) & ~isnan(maxc)
                axis([lags(1) lags(end) minc maxc])
            end

            hold on
            line([0 0],[minc maxc],'Color','r')
            if Args.TSappend; plot(lags,append{cue}.yfit(al,:),'k'); else; plot(lags,yfit{cue}(al,:),'k'); end
            %             plot(lags,repmat(thresh{cue}(al),1,length(lags)),'r--')
            %             [ampl,lag] = findCntPeak(yfit{cue}(al,:),lags);
            %             plot(lag,ampl,'gx')
            %             plot(lag,squeeze(thresh(al,:,1:2)),'r--')

            if Args.TSappend; plot(append{cue}.cPeak(al,2),append{cue}.cPeak(al,1),'gx'); else; plot(cPeak{cue}(al,2),cPeak{cue}(al,1),'gx'); end
            if al == 1; text(0,maxc,sprintf('n=%d',length(trials{cue}))); end
            hold off

            if al == 1 ; ylabel(labels{cue}); end
        end
    end
    warning off all
    sprintf('%s%s%s \n',obj.data.setNames{ind},obj.data.Index(ind,3),obj.data.Index(ind,2))

end
