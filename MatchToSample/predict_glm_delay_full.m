function glm_stim_predict = predict_glm_delay_full(varargin)
%prepares model for R
Args = struct('all_number',[],'stim_number',[],'plotpercents',[],'epoch',[],'model',[],'pair',[]);
Args.flags = {};
Args = getOptArgs(varargin,Args);

if Args.epoch == 0
    delay = [22:34]; %full delay period (18-34). cut out any sample off evoked potential (22:34) (first 100ms)
elseif Args.epoch == 1
    delay = [1:8];
end

points = size(delay,2);
time = [1:points];

all_responses = Args.all_number(:,delay);
all_percents = Args.plotpercents(:,delay);

[s1 s2] = size(all_responses);
% %
% % response = reshape(all_responses,1,s1*s2)';
all_times = [];
for yy = 1 : 9

    all_times = time';
    all_stim_numbers((1:13),1) = Args.stim_number(yy);
    stims((1:13),1) = yy;
    cross((1:13),1)  = (all_percents(yy,:)/100) * Args.stim_number(yy);
    
    glm_stimuli(:,1) = all_times;
    glm_stimuli(:,2) = stims;
    glm_stimuli(:,3) = cross;
    glm_stimuli(:,4) = all_stim_numbers;
    
    gsp = predicts_glm('gstimuli',glm_stimuli,'fits','pair',Args.pair)
    
    glm_stim_predict(yy,:) = gsp;
    
end


