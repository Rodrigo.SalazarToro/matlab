function [SEQUENCE,INI] = SeqFromML(BHV,varargin)
%CO CP MO1 MP1 MO2 MP2 between 0 and 2
% assumes 5 deg excentricirty


Args = struct('Nlynx',0,'nlynx_betty',0);
Args.flags = {'Nlynx','nlynx_betty'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


nobj =  BHV.Stimuli.NumPics;
for ob = 1 : nobj
    eval(sprintf('INI.Object%dFileName = BHV.Stimuli.PIC(ob).Name; allobj{ob} = BHV.Stimuli.PIC(ob).Name;',ob));
end
comma = findstr(',',BHV.TaskObject{1,2});

if Args.Nlynx
    
    if Args.nlynx_betty
        a  = str2num(BHV.TaskObject{1,2}(comma(1):end-2));
        if isequal(a,[0 5]) || isequal(a,[4.330 -2.500]) || isequal(a,[-4.330 -2.500])
            
            INI.Position = 90; % 90 deg
            allpos = [0.000 5.000; 4.330 -2.500; -4.330 -2.500;]; %90 deg
        else
            allpos = [0.000 -5.000; -4.330 2.500; 4.330 2.500;]; %270 deg
            INI.Position = 270; % 270 deg
        end
    else
        INI.Position = 0; % 0 deg
        allpos = [0.000 0.000; 5.000 0.000; -5.000 0.000]; %0 deg
    end
    
else
    a  = str2num(BHV.TaskObject{1,2}(comma(1):end-2));
    if isequal(a,[0 5]) || isequal(a,[4.330 -2.500]) || isequal(a,[-4.330 -2.500])
        
        INI.Position = 90; % 90 deg
        allpos = [0.000 5.000; 4.330 -2.500; -4.330 -2.500;]; %90 deg
    else
        allpos = [0.000 -5.000; -4.330 2.500; 4.330 2.500;]; %270 deg
        INI.Position = 270; % 270 deg
    end
end
oid = [1 3 5];
opos = [2 4 6];
for t = 1 : length(BHV.ConditionNumber)
    cond = BHV.ConditionNumber(t);
    
    for ev = 1 :  3
        event = BHV.TaskObject{cond,ev+1};
        comma = findstr(',',event);
        startObj = findstr('(',event) +1;
        if isempty(startObj)
            SEQUENCE(t,oid(ev)) = nan;
            SEQUENCE(t,opos(ev)) = nan;
        else
            
            if Args.Nlynx
                SEQUENCE(t,oid(ev)) = strmatch(event(startObj:comma(1)-1),allobj);
            else
                SEQUENCE(t,oid(ev)) = strmatch(event(startObj:comma(1)-1),allobj) - 1 ;
            end
            thepos = str2num(event(comma(1):end-2));
            
            
            if isequal(thepos,allpos(1,:))
                
                objpos = 0;
            elseif isequal(thepos,allpos(2,:))
                objpos = 1;
            elseif isequal(thepos,allpos(3,:))
                objpos = 2;
            end
            
            SEQUENCE(t,opos(ev)) = objpos;
        end
        
        
    end
end

