function [euclidxy euclidxyz euclidx euclidy euclidz] = euclid_dist(gridpos1,gridpos2,depths)


%currently only works for Betty

euclidxy = zeros(1,size(depths,1));
euclidxyz = zeros(1,size(depths,1));

pp1 = find(gridpos1 > 32)';
pp2 = find(gridpos2 > 32)';

pairs = intersect(pp1,pp2);

%PF grid
pf_order = [ ...
    0; 1; 2; 3; 4; 5;
    0; 6; 7; 8; 9;10;
    11;12;13;14;15;16;
    17;18;19;20;21;22;
    0;23;24;25;26;27;
    0;28;29;30;31;32];

%PP grid
pp_order = [ ...
    0;33;34;35;36;37;
    0;38;39;40;41;42;
    43;44;45;46;47;48;
    49;50;51;52;53;54;
    0;55;56;57;58;59;
    0;60;61;62;63;64];

%get XY coordinates for all channels
xy = [];
cch = 0;
for grids = 1:2
    ch = 0;
    for x = [6 5 4 3 2 1]
        for xx = 1:6
            ch = ch + 1;
            if grids == 1
                if pf_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx;
                    xy(cch,2) = x;
                end
            else
                if pp_order(ch) ~= 0
                    cch = cch +1;
                    xy(cch,1) = xx+7;
                    xy(cch,2) = x;
                end
            end
        end
    end
end

for p = 1 : size(gridpos1,1)%pairs
    c1x = xy(gridpos1(p),1);
    c1y = xy(gridpos1(p),2);
    d1 = depths(p,1);
    d2 = depths(p,2);
    
    c2x = xy(gridpos2(p),1);
    c2y = xy(gridpos2(p),2);
    
    %grid holes are 860 microns apart
    euclidxyz(p) = sqrt( ((c1x*.86)-(c2x*.86))^2 + ((c1y*.86) - (c2y*.86))^2 + (d1 - d2)^2 );
    euclidxy(p) = sqrt( ((c1x*.86)-(c2x*.86))^2 + ((c1y*.86) - (c2y*.86))^2 );
    euclidx(p) = sqrt( ((c1x*.86)-(c2x*.86))^2 );
    euclidy(p) = sqrt( ((c1y*.86) - (c2y*.86))^2 );
    euclidz(p) = sqrt( (d1 - d2)^2 );
end
