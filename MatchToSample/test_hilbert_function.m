function test_hilbert_function



figure
f = 20;
%time
t = [0:.001:.5];
%make sine wave 1
s1 = 1 * sin(2*pi*f*t);
s2 = 1 * sin(2*pi*f*t + 4.5);
% s2 = 1 * sin(2*pi*f*t + 3); %anti-phase

subplot(2,1,1)
plot(s1);hold on;plot(s2,'r')

alls(1,:) = s1;
alls(2,:) = s2;

alls = alls';

h = hilbert(alls);

alls = alls';
h = h';

hangle = angle(h);

plot(hangle(1,:),'g');hold on
plot(hangle(2,:),'k');hold on


d1 = unwrap(hangle(1,:)) ./ (2*pi);
d2 = unwrap(hangle(2,:)) ./ (2*pi);

%cyclic relative phase
d = mod(d1 - d2,1);

subplot(2,1,2)
plot(d1);hold on;plot(d2,'r')

d(1)



