function [data,mi,time,ntrial,cstim] = psthRuleMI(varargin)
% to run in the day folder


Args = struct('save',0,'redo',0,'iterations',1000,'binSize',50);
Args.flags = {'save','redo'};
Args = getOptArgs(varargin,Args);

references = [1:Args.binSize:3000];


res = cell(2,1);
endfolder = cell(2,1);
bfolder = cell(2,1);
for r = 1 : 2
    baseFile = sprintf('nineStimPSTH%d.mat',r);
    
    [~,res{r}] = unix(sprintf('find . -name ''%s'' -ipath ''*cluster0*'' -ipath ''*session99*''',baseFile));
    
    endfolder{r} = strfind(res{r},'cluster0') + 10;
    bfolder{r} = strfind(res{r},'./');
    
end

if length(bfolder{1}) ~= length(bfolder{2}); error('Different number of MUA for both rules'); end
nfiles = length(bfolder{1});


sdir = pwd;
for ff =1 : nfiles
    cd(sdir)
    cd(res{2}(bfolder{2}(ff):endfolder{2}(ff)))
    matfile = sprintf('PSTHrulesMI.mat');
    
    if isempty(nptDir(matfile)) || Args.redo
        mi = cell(3,1);
        time = cell(3,1);
        bdata = cell(3,1);
        for al = 1 : 3
            rA = [];
            cstim = [];
            for r = 1 : 2
                cd(sdir)
                cd(res{r}(bfolder{r}(ff):endfolder{r}(ff)))
                load(sprintf('nineStimPSTH%d.mat',r))
                data = cell2mat(A(:,al));
                cstim = [cstim ones(1,size(data,1)) * r];
                rA = [rA; data];
            end
            
            
            bdata{al} = zeros(size(rA,1),length(references));
            
            for bin = 1 : length(references)-1;
                bdata{al}(:,bin) = sum(rA(:,references(bin): references(bin+1) -1),2);
                
            end % rebin the 1mse matraix
            time{al} = [bins{1,al}(1)+Args.binSize/2  : Args.binSize : bins{1,al}(end)];
            
            mi{al} = zeros(Args.iterations,size(bdata{al},2));
            for it = 1 : Args.iterations + 1
                
                if it < Args.iterations + 1
                    randseq = randperm(size(rA,1));
                    data = bdata{al}(randseq,:);
                else
                    data = bdata{al};
                end
                
                
                for ti = 1 : size(data,2); mi{al}(it,ti) = mutualinfo(cstim,data(:,ti)); end
                
            end
        end
        if Args.save
            
            save(matfile,'bdata','time','cstim','Args','mi')
            display([pwd '/' matfile])
        end

    end
end


cd(sdir)
