function find_ch_recordings(varargin)

%Gets number of times each channel was recorded
%run at monkey level

Args = struct('days',[],'channels',64);
Args.flags = {};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;
num_days = size(Args.days,2);

record = zeros(num_days,Args.channels);

for x = 1 : num_days
    cd([monkeydir filesep Args.days{x}])
    g = good_groups;
    record(x,g) = 1;
end


cd(monkeydir)
save record record


%determine number of interarial pairs
p = 0;
for x = 1:size(record,1)
   ch = find(record(x,:));
   num_ch = size(ch,2);
   pairs = [];
   for c1 = 1:num_ch
       for c2 = (c1 +1) : num_ch
          if (ch(c1) < 33) && (ch(c2) >32)
            [ch(c1) ch(c2)]
            p = p + 1;
          end

       end 
   end  
end


p


