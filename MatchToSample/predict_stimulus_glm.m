function predict_stimulus_glm(varargin)


%run at session level
Args = struct('fits',0,'stim_prediction',0,'stims',0,'p_cutoff',1,'logits',0,'rule',1,'ml',0,'glm',0);
Args.flags = {'fits','stim_prediction','stims','logits','ml','glm'};
Args = getOptArgs(varargin,Args);


%rule = 1(identity), rule = 0(location)
if Args.rule
    rule = 'identity';
else
    rule = 'location';
end
sesdir = pwd;
N = NeuronalHist;
cd(['lfp' filesep 'lfp2']);
lfp2dir = pwd;
chnumb = N.chnumb;

total_pairs = nchoosek(chnumb,2);
%get actual group number
groups = N.gridPos;
%Get number of channels

lfp2 = pwd;
pair = 0;

if Args.glm
for c1 = 1 : chnumb
    for c2 = (c1+1) : chnumb
        pair = pair + 1;
        br = 1;
        res = 'correct';
        tss1= 'stable';
        sessionname = 'channelpair12.mat';
        thresh = 1;
        cd(lfp2)
        
        glm_stim_predict = cppStimuli_glm_predict('b1',br,'ts1',tss1,'c1',groups(c1),'c2',groups(c2),'threshh',thresh,'sessionname',sessionname,'chnumb',chnumb,'epoch',0,'ml',Args.ml,'rule',Args.rule,'pair',pair,'model',1);
        
        glm_stim_prediction{pair} = glm_stim_predict;
    end
end

cd ..

save glm_stim_prediction glm_stim_prediction
else
    cd(sesdir)
    load glm_stim_prediction
end



if Args.stims
        mt = mtstrial('auto','ml');
    %get stimulus info
    locs = unique(mt.data.CueLoc);
    objs = unique(mt.data.CueObj);
    stim = 0;
    for object = 1:3
        %Run for all locations
        for location = 1:3
            stim = stim+1;
            ind = mtsgetTrials(mt,'ML','BehResp',1,'stable','CueLoc',locs(location),'CueObj',objs(object));
            stimulus(ind) = stim;
        end
    end
    
    [i ii iii] = find(stimulus);
    
    stimulus = iii;
    save stimulus stimulus
    
else
    cd(sesdir)
    load stimulus stimulus
end











p=[];
for x = 1:size(glm_stim_prediction,2)
    p(:,:,x) = glm_stim_prediction{x};
end

 [i prediction]=max(nanmedian(p,3));


prediction
stimulus(1:size(stimulus,2))
%[i prediction] = max(glm_stim_prediction{6})

m=0;
for y = 1:size(stimulus,2)
    if prediction(y) == stimulus(y)
        m = m+1;
    end
end 
m













