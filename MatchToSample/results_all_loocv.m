function results_all_loocv(varargin)


%run at monkey level
%gets results for bayes_stimulus_classifier_LOOCV

Args = struct('days',[],'ml',0,'sorted',1);
Args.flags = {'ml'};
Args = getOptArgs(varargin,Args);

monkeydir = pwd;

num_days = size(Args.days,2);

n_pairs = zeros(1,num_days);
n_trials = zeros(1,num_days);
all_results = zeros(1,num_days);
loc_results = zeros(1,num_days);
ide_results = zeros(1,num_days);
all_pvals = zeros(1,num_days); 
ide_pvals = zeros(1,num_days);
loc_pvals = zeros(1,num_days);

for d = 1 : num_days
    cd([monkeydir filesep Args.days{d} filesep 'session01'])
    
    [~,total_pairs,combs,~] = sorted_groups('ml');

    n_pairs(d) = size(total_pairs,2);
    
    if Args.ml
        mt = mtstrial('auto','ML','RTfromML','redosetNames');
    else
        mt = mtstrial('auto','redosetNames');
    end
    
    if Args.ml
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
    else
        total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
    end
    
    n_trials(d) = size(total_trials,2);
    
    %get results
    cd(['lfp' filesep 'lfp2'])
    
    try
        load bsc_loocv_performance_ide_all performance 
        ide_results(d) = performance;
        
        %calculate p-value (see Quiroga_2009, pg 176 Box 2)
        n = size(total_trials,2);
        K = 3;
        p = 1 / K;
        c_trials = round((n * (performance / 100))); %number of correct trials
        pvalue = 0;
        for k = c_trials : n
            P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
            pvalue = pvalue + P;
        end
        
        ide_pvals(d) = pvalue;
    catch
        fprintf(['skipped   ' pwd])
    end
    
    try
        load bsc_loocv_performance_loc_all performance 
        loc_results(d) = performance;
        
        %calculate p-value
        n = size(total_trials,2);
        K = 3;
        p = 1 / K;
        c_trials = round((n * (performance / 100))); %number of correct trials
        pvalue = 0;
        for k = c_trials : n
            P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
            pvalue = pvalue + P;
        end
        
        loc_pvals(d) = pvalue;
    catch
        fprintf(['skipped   ' pwd])
    end
    
    
    
    try
        load bsc_loocv_performance_all performance 
        all_results(d) = performance;
        
        %calculate p-value
        n = size(total_trials,2);
        K = 9;
        p = 1 / K;
        c_trials = round((n * (performance / 100))); %number of correct trials
        pvalue = 0;
        for k = c_trials : n
            P = nchoosek(n,k) * (p)^k * (1-p)^(n-k);
            pvalue = pvalue + P;
        end
        
        all_pvals(d) = pvalue;
    catch
        fprintf(['skipped   ' pwd])
    end
    
end

cd(monkeydir)

save bsc_loocv ide_results loc_results all_results n_pairs n_trials ide_pvals loc_pvals all_pvals

p_cutoff = .05;

figure
subplot(1,3,1)
scatter(n_trials,loc_results)
hold on
scatter(n_trials(loc_pvals < p_cutoff),loc_results(loc_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,2)
scatter(n_trials,ide_results)
hold on
scatter(n_trials(ide_pvals < p_cutoff),ide_results(ide_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,3);
scatter(n_trials,all_results)
hold on
scatter(n_trials(all_pvals < p_cutoff),all_results(all_pvals < p_cutoff),50,[.5 0 0],'filled')

figure

subplot(1,3,1)
scatter(n_pairs,loc_results)
hold on
scatter(n_pairs(loc_pvals < p_cutoff),loc_results(loc_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,2)
scatter(n_pairs,ide_results)
hold on
scatter(n_pairs(ide_pvals < p_cutoff),ide_results(ide_pvals < p_cutoff),50,[.5 0 0],'filled')

subplot(1,3,3);
scatter(n_pairs,all_results)
hold on
scatter(n_pairs(all_pvals < p_cutoff),all_results(all_pvals < p_cutoff),50,[.5 0 0],'filled')


