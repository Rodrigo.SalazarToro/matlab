function detect_cross_channel_transients


cd('/media/raid/data/monkey/betty/090708/session01')

sesdir = pwd;

% if Args.ml
mt = mtstrial('auto','ML','RTfromML','redosetNames');
r = 1; %only identity need to save in identity dir
total_trials = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1);
ntrials = size(total_trials,2);
% else
%     mt=mtstrial('auto','redosetNames');
%     %determine rule
%     r = mt.data.Index(1,1);
%     total_trials = mtsgetTrials(mt,'BehResp',1,'stable','rule',1);
% end

cue_on = floor(mt.data.CueOnset);
cue_off = floor(mt.data.CueOffset);
match = floor(mt.data.MatchOnset);


allgroups = nptdir('group*');
ngroups = size(allgroups,1);

%try only correct trials
allcounts = [];
alltimes = [];
allisi = zeros(1,2500);
allhist = zeros(1,3000);
for t = total_trials
    sptimes = [];
    for ng = 1 : ngroups
        cd([sesdir filesep allgroups(ng).name filesep 'cluster01m'])
        %get spike times for each group
        load ispikes

        stimes = unique(round(sp.data.trial(t).cluster.spikes));
        stimes(stimes < (cue_on(t) - 500)) = [];
        stimes(stimes > match(t)) = [];
        
        allhist = allhist + hist(stimes,[1:3000]);
        
        sptimes = [sptimes stimes]; %mua, so +1 spike can occur per ms
        hisi = hist(diff(stimes),[1:2500]);
        allisi = allisi + hisi;
    end
    utimes = sort(unique(sptimes));
    counts = hist(sptimes,utimes);
    %     hist(sptimes,utimes)
    %     axis tight
    allcounts = [allcounts max(counts)/ngroups];
    
    [~,ii] = max(counts);
    alltimes = [alltimes utimes(ii)];
    
end
allcounts;
alltimes;


at = alltimes;
at(allcounts < .5) = nan;
ac = allcounts;
ac(allcounts < .5) = nan;

[hc ihc] = hist(allcounts,[0:.025:1])
subplot(1,3,1);bar(ihc,hc./ntrials);axis tight;subplot(1,3,2);hist(alltimes,[0:10:3000]); axis tight
subplot(1,3,3);bar(allisi)



NFFT = 2^nextpow2(size(allhist,2));
f = fft(allhist,NFFT);
power = abs(f(1:NFFT/2+1)).^2;
freq = 1000/2 * linspace(0,1,NFFT/2+1);



