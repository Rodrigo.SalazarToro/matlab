function make_bmf_epoch_correlograms_summary_figure

cd('/media/bmf_raid/data/monkey/ethyl/')
load alldays
monkeydir = pwd;

all_freq1 = [];
all_phase1 = [];
all_corrs1 = [];
all_freq2 = [];
all_phase2 = [];
all_corrs2 = [];
counter = 0;
for x = 1:25%18

    cd([monkeydir filesep alldays{x} filesep 'session01'])
    sesdir = pwd;
    
    [~,~,gg,~,sortedpairs] = bmf_groups;
    cd([sesdir filesep 'lfp' filesep 'lfp2' filesep 'identity'])
    
    load all_gabors
    
    
    for n = sortedpairs
        counter = counter + 1;
        a1 = all_gabors{5,n};
        a2 = all_gabors{5,n};
        if a1.cross_corr >= .99 && abs(a1.peak) > .05 && a2.cross_corr >= .99 && abs(a2.peak) > .05
            all_phase1 = [all_phase1 a1.phase_angle_deg];
            all_corrs1 = [all_corrs1 a1.peak];
            all_freq1 = [all_freq1 a1.frequency];
            
            all_phase2 = [all_phase2 a2.phase_angle_deg];
            all_corrs2 = [all_corrs2 a2.peak];
            all_freq2 = [all_freq2 a2.frequency];
        end
    end
end
% figure
% subplot(1,2,1)
% hist(abs(all_phase),50)
% axis tight
% subplot(1,2,2)
% hist(all_freq,50)
% axis tight

% figure;hist(all_time_lags_pos)
% figure;hist(all_time_lags_neg)
    figure
    hist(all_phase,100)
    axis tight
    axis square
   figure
   scatter(abs(all_corrs1),abs(all_corrs2),'fill','s','k');hold on;plot([0 1],[0 1],'r');axis square;hold on;set(gca, 'TickDir', 'out');
    signtest(all_corrs1,all_corrs2)

    figure;hist(abs(all_corrs2)-abs(all_corrs1),100);hold on;plot([0 0],[0 260],'k');axis([-.25 .25 0 260]);hold on;set(gca, 'TickDir', 'out');10
cd(monkeydir)
