function [fband,table,tableN,band] = summary_table(test,varargin)
% has to be run for both rules separately
mainfile = test.data.mainfile;
surfile = test.data.surfile;
Args = struct('prctile',[10 25 50 75 90],'frange',[0  50],'ruleCode',[1 3],'onlyTable',0,'export',0,'prctileP',[25 50 75],'types',[1:4],'bands',[1:4],'save',0,'redo',0,'combineAll',0,'phaseRange',[301 120; 121 300],'files',{{sprintf('%s.mat',mainfile), sprintf('%s.mat',mainfile) ,'powerPP.mat', 'powerPF.mat'}},'sigLevel',4,'addName',[],'comparison',[]);
Args.flags = {'onlyTable','export','save','redo','combineAll'};
[Args,modvarargin] = getOptArgs(varargin,Args);

bands = {'theta','alpha','beta','gamma'};
fbands = [2 7;7 13; 14 30; 30 42];
odir = pwd;
dir = {'pos','neg'};
files = Args.files;
areaC = {'r' 'b'};
lwind = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];% missing
% [tot,ind] = get(test,'Number');
epochs = {'presample' 'sample' 'delay1' 'delay2'};
legs = { 'md' 'mv' 'ld' 'lv' 'sd' 'sv' 'ms' 'ls' 'other'};
cor = {'PPCm-PFCd' 'PPCl-PPFCd' 'other'};
%% coherence
if ~isempty(find(Args.ruleCode ==1))
    rule = 1;
else
    rule = 2;
end
for b = 1 : size(fbands,1)
    for d = 1 : 2
        for comp = 1 : 6
            [tot(b,comp),ind] =get(test,'Number','sigSur',{sprintf('%s',bands{b}) lwind(comp,:) Args.ruleCode },'snr',99);
            [r,ind] =get(test,'Number','type',{sprintf('%s%s%d%d',bands{b},dir{d},lwind(comp,1),lwind(comp,2)) Args.ruleCode},'snr',99);
            table{b}(d,comp) = 100*r/tot(b,comp);
            tableN{b}(d,comp) = r;
            indexes{b,d,comp} = ind;
        end
    end
end
for b = 1 : size(fbands,1)
    if isempty(Args.comparison)
        band(b).perc = max(max(table{b}));
        [row,col]  = find(table{b} == band(b).perc);
    else
        row = Args.comparison(1); % 1 == pos; 2 == neg
        col = Args.comparison(2); % index according to lwind
    end
    for i = 1 : length(row)
        band(b).type{i} = sprintf('%s%d%d',dir{row(i)},lwind(col(i),1),lwind(col(i),2));
        band(b).hist{i} = hist(test.data.Index(indexes{b,row(i),col(i)},9),[1:9]);
        for h = 1 : 9;[thist(h),ind] =get(test,'Number','sigSur',{sprintf('%s',bands{b}) lwind(col(i),:) [1 3]},'pairhist',legs{h},'rudeHist','snr',99);end
        band(b).histP{i} = band(b).hist{i} ./ thist * 100;
    end
end
if ~Args.onlyTable
    tit = {'distribution of coherences Main coherence effects' 'Distribution of Phases for the main coherence effects' 'Distribution of power for the main coherence effects' ' Distribution of power for the coherence main effects'};
    pdir = pwd;
    filename = sprintf('%s%stempObj.mat',pdir,filesep);
    save(filename,'test')
    
    % cohInter phase powerPP powerPF
    
    for type = Args.types
        
        for b = Args.bands
            fname = sprintf('%s%s%d%s%s.mat',mainfile,files{type}(1:end-4),type,bands{b},Args.addName);
            if type < 4
                h(b) = figure;
                set(h(b),'Name',sprintf('%s; %s',bands{b},tit{type}));
            else
                figure(h(b));
            end
            load(filename)
            
            band(b).legs= legs;
            alldata = cell(4,3);
            cortex{b} = [];
            allpeaks = cell(4,3);
            if isempty(Args.comparison)
                band(b).perc = max(max(table{b}));
                [row,col]  = find(table{b} == band(b).perc);
            else
                row = Args.comparison(1); % 1 == pos; 2 == neg
                col = Args.comparison(2); % index according to lwind
            end
            allind = indexes{b,row,col};
            if ~isempty(allind)
                thefile = nptDir(fname);
                if isempty(thefile) || Args.redo
                    clear listCH
                    count = 1;
                    ac = 1;
                    for ind = allind
                        cmb = test.data.Index(ind,1);
                        
                        cortex{b} = [cortex{b} test.data.Index(ind,9)];
                        %             pairg = coh.data.Index(ind,10:11);
                        cd(test.data.setNames{ind})
                        load(sprintf('%s.mat',mainfile));
                        sur = load(sprintf('%sSur.mat',surfile));
                        for bt = 1 : size(fbands,1); flimB{bt} = find(f>=fbands(bt,1) & f <= fbands(bt,2)); end
                        ch = Day.comb(cmb,:);
                        load(files{type});
                        if type == 3
                            ch(1) = find(ch(1) == Day.channels);
                            newCH = sprintf('%s%d',test.data.setNames{ind},ch(1));
                        elseif type == 4
                            ch(2) = find(ch(2) == Day.channels);
                            newCH = sprintf('%s%d',test.data.setNames{ind},ch(2));
                        end
                        
                        thes = Day.session(1).rule;
                        if thes <= size(Day.session,2)
                            for p = 1 : 4
                                %                 if length(find(Day.session(thes).C(fband,cmb,3) > sur.Day.session(thes).Prob(fband,1,cmb,3))) > 1 || length(find(Day.session(thes).C(fband,cmb,4) > sur.Day.session(thes).Prob(fband,1,cmb,4))) > 1
                                if b == 99 % previously 3 to split the beta band into dorsal and ventral
                                    if sum(test.data.hist(ind,1) == [8 10]) ~= 0 && sum(test.data.hist(ind,2) == [3 4]) ~= 0
                                        area = 1; % mPPC
                                    elseif sum(test.data.hist(ind,1) == [11 13]) ~= 0 && sum(test.data.hist(ind,2) == [3 4]) ~= 0
                                        area = 2; % lPPC
                                    else
                                        area = 3;
                                    end
                                elseif b == 99 && (type == 3 || type == 4)
                                    
                                    if test.data.betaPhase(ind,4) >= deg2rad(Args.phaseRange(1,1)) || test.data.betaPhase(ind,4) <= deg2rad(Args.phaseRange(1,2))
                                        area = 1;
                                    elseif test.data.betaPhase(ind,4) >= deg2rad(Args.phaseRange(2,1)) && test.data.betaPhase(ind,4) <= deg2rad(Args.phaseRange(2,2))
                                        area = 2;
                                    else
                                        area = 3;
                                    end
                                    
                                else
                                    % 1 '6DR'
                                    %   2'8AD'
                                    %   3'8B'
                                    %    4'dPFC'
                                    %   5'vPFC'
                                    %  6'PS'
                                    %   7'AS'
                                    %   8'PEC'
                                    %   9'PGM'
                                    %   10'PE'
                                    %    11'PG'
                                    %   12'MIP'
                                    %   13'LIP'
                                    %   14'PEcg'
                                    %   15'IPS'
                                    %   16'WM'
                                    
                                    if Args.combineAll
                                        area =1;
                                    else
                                        if sum(test.data.hist(ind,1) == [8 10 11 13]) ~= 0 && sum(test.data.hist(ind,2) == [3 4]) ~= 0
                                            area = 1;
                                        else
                                            area = 3;
                                        end
                                    end
                                end
                                switch type
                                    case 1
                                        
                                        alldata{p,area} = [alldata{p,area} single(Day.session(thes).C(:,cmb,p))];
                                        
                                        
                                        for bb = 1 : 4;allpeaks{p,area} = [allpeaks{p,area} eval(sprintf('test.data.%sPeak(ind,p + (rule-1) * 4)',bands{bb}))];end
%                                         if length(find(allpeaks{p,area} >= 14 & allpeaks{p,area} <= 30)) ~= ac; keyboard; end
                                    case 2
                                        sigcoh = Day.session(thes).C(:,cmb,p) > squeeze(sur.Day.session(thes).Prob(:,Args.sigLevel,1,p));
                                        alldata{p,area} = [alldata{p,area} sigcoh.*mod(2*pi + single(Day.session(thes).phi(:,cmb,p)),2*pi)];
                                        allpeaks{p,area} = [allpeaks{p,area} eval(sprintf('test.data.%sPeak(ind,p + (rule-1) * 4)',bands{b}))];
                                    case {3 4}
                                        if count ==1 ||isempty(find(strcmp(newCH,listCH) == 1))
                                            % norm(count,nch,[me st])
                                            adddata = mean(Day.session(thes).S(:,:,ch(type-2),p),2);
                                            normax = max(max(squeeze(mean(Day.session(thes).S(flimB{b},:,ch(type-2),:),2))));
                                            alldata{p,area} = [alldata{p,area} single(adddata / normax)];
                                            
                                        end
                                end
                            end
                            ac =ac+1;
                            if type == 3 || type == 4
                                listCH{count} = newCH;
                                
                                count = count + 1;
                                
                            end
                        end
                        %             pause
                        clear Day areaP
                    end
                else
                    load(fname)
                end
                clear test
                cd(odir)
                if Args.save;  if type == 1; save(fname,'alldata','allpeaks','allind','f');else save(fname,'alldata','allind','f'); end;end
                for bt = 1 : size(fbands,1);
                    %                     flimB{bt} = [1: length(f)];
                    flimB{bt} = find(f>=fbands(bt,1) & f <= fbands(bt,2));
                end
                for p = 1 : 4
                    for c = 1 : 3
                        if ~isempty(alldata{p,c})
                            subplot(3,4,(c-1)*4 + p)
                            
                            switch type
                                case 1
                                    
                                    [n,xout] = hist(allpeaks{p,c},f);
                                    for pp = 1 : 4; [nn,xoutn] = hist(allpeaks{pp,c},f); maxn(pp) = max(nn(flimB{b})); end; maxn = max(maxn) + 1;
                                    [AX,H1,H2] = plotyy(f,prctile(alldata{p,c},Args.prctile,2),xout,n);
                                    hold on
                                    if b == size(fbands,1); set(AX(1),'YLIM',[0 0.5]);  else ylim([0 0.4]); end
                                    %                                     grid on
                                    set(AX(2),'YLIM',[0 maxn]);
                                    set(AX(1),'XLIM',Args.frange,'TickDir','out');
                                    set(AX(2),'XLIM',Args.frange,'TickDir','out');
                                    if p == 4 & c == 3; ylabel('coherence'); end
                                    yn = 0.01;
                                    
                                case 2
                                    %                                     [n,phaseh] = hist(mean(alldata{p,c}(flimB{b},:),1),[0: (pi/6):2*pi]);
                                    %                                     compass(cos(phaseh) .* n,sin(phaseh) .* n)
                                    reardata = alldata{p,c}(flimB{b},:);
                                    gooddata = reardata ~= 0; % assume that it is highly not probable to have a phase excatly of zero
                                    rose(sum(reardata,1)./sum(gooddata,1),12)
                                    
                                    %                                     phases = [];for ii = 1 : length(allpeaks{p,c}); if allpeaks{p,c}(ii) ~= 0; phases = [phases alldata{p,c}(find(allpeaks{p,c}(ii) == f), ii)]; end;end
                                    %                                     rose(phases)
                                    if p == 4 & c == 3; ylabel('Phase [deg]'); end
                                    yn = 0.01;
                                case {3 4}
                                    
                                    %                                     plot(f,prctile(alldata{p,c},Args.prctileP,2))
                                    plot(f,mean(alldata{p,c},2),areaC{type-2})
                                    hold on
                                    plot(f,mean(alldata{p,c},2) + std(alldata{p,c},[],2)/sqrt(size(alldata{p,c},2)),sprintf('%s--',areaC{type-2}))
                                    plot(f,mean(alldata{p,c},2) - std(alldata{p,c},[],2)/sqrt(size(alldata{p,c},2)),sprintf('%s--',areaC{type-2}))
                                    %                                                                         shadeplot('bins',alldata{p,c}','x',f);
                                    if p == 4 & c == 3; ylabel('norm. Power'); end
                                    %                                     axis([Args.frange -2.5 -1]) % for
                                    %                                     clark
                                    xlim(Args.frange)
                                    ylim([0.2 1])
                                    yn = 0.1;
                                    %                                     grid on
                            end
                            set(gca,'TickDir','out');
                            if c == 1; title(epochs{p}); end
                            if c == 3 & p == 4 & type ~= 2; xlabel('Frequency [Hz]'); title(sprintf('%s %s',bands{b},band(b).type{1}));end
                            if type ~= 2; text(35, 0.3* type,sprintf('n=%d',size(alldata{p,c},2))); end
                            if p ==1; ylabel(cor{c}); end
                            
                        end
                        
                    end
                end
                switch type
                    case {1}
                        legend(num2str(Args.prctile'));
                        %                     case {3 4}
                        %                         legend(num2str(Args.prctileP'));
                end
            end
        end
        
    end
    
    %     delete filename
end
%%make the table
%b = 3;[12 13 14 23 24 34; table{b}; tableN{b};tot(b,:)]
for b = 1 : size(fbands,1)
    
    for col = 2 : 7
        fband(b).table{1,1} = 'epoch comp';
        fband(b).table{1,col} = num2str(lwind(col-1,:));
        epoch_comp(col-1) = str2num(sprintf('%d%d',lwind(col-1,1),lwind(col-1,2)));
        fband(b).table{2,1} = '% pos';
        fband(b).table{2,col} = table{b}(1,col-1);
        perc_pos(col-1) = round(table{b}(1,col-1));
        fband(b).table{3,1} = '% neg';
        fband(b).table{3,col} = table{b}(2,col-1);
        perc_neg(col-1) = round(table{b}(2,col-1));
        fband(b).table{4,1} = 'n pos';
        fband(b).table{4,col} = tableN{b}(1,col-1);
        npos(col-1) = round(tableN{b}(1,col-1));
        fband(b).table{5,1} = 'n neg';
        fband(b).table{5,col} = tableN{b}(2,col-1);
        nneg(col-1) = round(tableN{b}(2,col-1));
        fband(b).table{6,1} = 'total';
        fband(b).table{6,col} = tot(b,col-1);
        total(col-1) = round(tot(b,col-1));
    end
    if Args.export;
        if b ==1; epoch_comp = epoch_comp';perc_pos=perc_pos';perc_neg=perc_neg';npos=npos';nneg=nneg';total=total';
        end;
        A = dataset(epoch_comp,perc_pos,perc_neg,npos,nneg,total); export(A,'file',sprintf('%s%s%s.txt',mainfile,bands{b},Args.addName));
    end
end
fband(:).table
%% power
