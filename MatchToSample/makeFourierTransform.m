function makeFourierTransform(varargin)
% in the sessions folder

Args = struct('redo',0,'Fs',200,'tapers',[2 3],'fpass',[0 100],'plength',400);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

Fs = Args.Fs;
tapers = Args.tapers;
fpass = Args.fpass;
NeuroInfo = NeuronalChAssign;


if isempty(nptDir('skip.txt')) && length(NeuroInfo.groups) > 1
    gcomb = nchoosek(NeuroInfo.groups,2);
    ccomb = nchoosek([1 : length(NeuroInfo.groups)],2);
    sdir = pwd;
    cd lfp/
    ldir = pwd;
    ndir = 'fourierTrans';
    
    
    if isempty(nptDir(ndir))
        unix(sprintf('mkdir %s',ndir));
    end
    cd(ndir)
    
    surdir = pwd;
    [st,result] = unix('find . -name ''*_fft_*.mat''');
    cd(ldir)
    files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
    
    [~,num_channels,~,~,~]=nptReadStreamerFile(files(1).name);
    cd ..
    
    if Args.redo; startfile = 1; else startfile = length(strfind(result,'.mat')) + 1; end;
    clear result
    for nf = startfile : length(files)
        cd(ldir)
        fname = regexprep(files(nf).name, 'lfp.', 'fft_');
        
        cd(sdir)
        [data,~,~,~] = lfpPcut(nf,[1 : num_channels],'plength',Args.plength,modvarargin{:},'EPnotRemoved','fromFiles');
        N = size(data{1},1);
        nfft=max(2^(nextpow2(N)),N);
        [f,findx]=getfgrid(Fs,nfft,fpass);
        tapers=dpsschk(tapers,N,Fs);
        for p = 1 : 4
            data{p} = squeeze(data{p});
            
            J = mtfftc(data{p},tapers,nfft,Fs);
            J = J(findx,:,:);
            S = squeeze(mean(conj(J).*J,2));
            Jdata{p} = J;
            Sdata{p} = S;
            for nc = 1 : size(ccomb,1)
                S12{p}(:,nc) = squeeze(mean(conj(J(:,:,ccomb(nc,1))).*J(:,:,ccomb(nc,2)),2));
                
            end
        end
        cd(surdir)
        save(sprintf('%s.mat',fname),'Jdata','Sdata','f','S12','ccomb','gcomb')
        sprintf('saving %s/%s \n',pwd,fname)
       
    end
    
    
end