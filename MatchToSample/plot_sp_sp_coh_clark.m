function plot_sp_sp_coh_clark

cd /media/raid/data/monkey/clark/
mdir = pwd;
load idedays
load ideses
figure
for d = 21 : 35
    cd([mdir filesep idedays{d} filesep ses{d}])
    N = NeuronalHist('ml');
    Nch = NeuronalChAssign;
    
    cd([mdir filesep idedays{d} filesep ses{d} filesep 'lfp' filesep 'lfp2' filesep 'identity'])
    
    
    sp = nptDir('spikecoh*');
    nsp = size(sp,1);
    
    
    counter = 0;
    for n = 1 : nsp
        counter = counter + 1;
        load(sp(n).name)
        idedays{d}
        
        a1 = N.level1(find(Nch.groups == spikecoh.groups(1,1)));
        a2 = N.level1(find(Nch.groups == spikecoh.groups(1,2)));
        
        f = spikecoh.delay_f{1};
        phi = spikecoh.delay_phi{1};
        
        phi = ((abs(phi) / pi) ./10);
        
        interval = find(f > 3 & f < 50);
        sp_sp = spikecoh.delay_c{1};
        if counter > 20
            figure
            counter = 1;
        end
        
        subplot(4,5,counter)
        plot(f(interval),sp_sp(interval))
        hold on
        plot(phi(interval),'r')
        title([a1{:} ' ' a2{:}])
        axis([3 50 0 .4])
    end
    
    pause
    close all
    
end