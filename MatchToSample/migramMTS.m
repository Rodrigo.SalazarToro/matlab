function varargout = migramMTS(obj,varargin)


Args = struct('save',0,'redo',0,'rule',1,'surrogate',0,'iterations',1000,'ML',0,'type','IdePerLoc','nind',[],'BehResp',1,'IdePer',1,'days',[],'matchAlign',0,'minTrials',10,'IncorCorrected',0,'FixCorrected',0,'addName',[],'appendS',0,'notGenerelized',0);
Args.flags = {'save','redo','surrogate','ML','matchAlign','IncorCorrected','FixCorrected','appendS','notGenerelized'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'surrogate'}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );


switch Args.type
    case 'Ide'
        nstim = 3;
        stims1 = 'stim';
        stims2 = '[1:3]';
    case 'Loc'
        nstim = 3;
        stims1 = '[1:3]';
        stims2 = 'stim';
    case 'Nine'
        nstim = 9;
        cueComb = [1 2 3 1 2 3 1 2 3; 1 1 1 2 2 2 3 3 3];% first row == loc
        stims1 = 'cueComb(2,stim)';
        stims2 = 'cueComb(1,stim)';
    case 'IdePerLoc'
        nstim = 3;
        loc = Args.IdePer;
        cueComb = [loc loc loc; 1 2 3];% first row == loc
        stims1 = 'cueComb(2,stim)';
        stims2 = 'cueComb(1,stim)';
        Args.type = sprintf('%s%d',Args.type,loc);
    case 'LocPerIde'
        nstim = 3;
        ide = Args.IdePer;
        cueComb = [ 1 2 3;ide ide ide];% first row == loc
        stims1 = 'cueComb(2,stim)';
        stims2 = 'cueComb(1,stim)';
        Args.type = sprintf('%s%d',Args.type,ide);
    case 'Fix'
        nstim = 3;
        
end

sdir = pwd;

[r,ind] = get(obj,'Number','snr',99);

if isempty(Args.nind); st = 1 ; se = r; else st = Args.nind(1); se = Args.nind(2); end
if ~isempty(Args.days); st = 1 ; se = length(Args.days); for d = 1 : length(Args.days); Args.days{d} = sprintf('%s/%s/',sdir,Args.days{d});end; end
count = 1;
for pair = st : se
    if isempty(Args.days)
        if Args.notGenerelized
            cd(obj.data.setNames{pair})
        else
            cd(obj.data.setNames{ind(pair)})
        end
    else
        cd(Args.days{pair})
    end
    if Args.ML; ses = 1; else rules=load('rules.mat'); ses = find(rules.r == Args.rule) + 1;end
    if ~isempty(ses)
        cd(sprintf('session0%d',ses))
        if isempty(Args.days)
            
            neuroinfo = NeuronalChAssign;
            if Args.notGenerelized
                gr = obj.data.Index(pair,10:11);
            else
                gr = obj.data.Index(ind(pair),10:11);
            end
            for c = 1 : 2;ch(c) = find(neuroinfo.groups == gr(c)); end
        end
        if Args.matchAlign; align = 'MatchAlign'; else align = []; end
        if Args.surrogate
            if Args.BehResp == 1
                matfile = sprintf('migramSurGenInterRule%d%s%s%s.mat',Args.rule,Args.type,align,Args.addName);
                if Args.notGenerelized
                    matfile = sprintf('migramSurg%04.0fg%04.0fRule%d%s%s%s.mat',gr(1),gr(2),Args.rule,Args.type,align,Args.addName);
                else
                    matfile = sprintf('migramSurGenInterRule%d%s%s%s.mat',Args.rule,Args.type,align,Args.addName);
                end
            else
                matfile = sprintf('migramSurGenInterRule%d%sIncor%s%s.mat',Args.rule,Args.type,align,Args.addName);
            end
        else
            
            if Args.BehResp == 1
                if Args.IncorCorrected
                    matfile = sprintf('migramg%04.0fg%04.0fRule%d%s%ssameTIncor%s.mat',gr(1),gr(2),Args.rule,Args.type,align,Args.addName);
                elseif Args.FixCorrected
                    matfile = sprintf('migramg%04.0fg%04.0fRule%d%s%ssameTFix%s.mat',gr(1),gr(2),Args.rule,Args.type,align,Args.addName);
                else
                    matfile = sprintf('migramg%04.0fg%04.0fRule%d%s%s%s.mat',gr(1),gr(2),Args.rule,Args.type,align,Args.addName);
                end
            else
                matfile = sprintf('migramg%04.0fg%04.0fRule%d%sIncor%s%s.mat',gr(1),gr(2),Args.rule,Args.type,align,Args.addName);
            end
        end
        cd ..
        cd grams
        if isempty(nptDir(matfile)) || Args.redo || Args.appendS
            if isempty(Args.days)
                if Args.notGenerelized
                    cd(obj.data.setNames{pair});
                else
                    
                    cd(Args.days{pair})
                end
            else
                cd(Args.days{pair})
            end
            cd(sprintf('session0%d',ses))
            mts = mtstrial('auto','redoSetNames');
            trials = cell(nstim,1);
            
            %             if strcmp(Args.type,'Fix') || Args.FixCorrected
            if Args.FixCorrected
                interTrials = mtsgetTrials(mts,'CueObj',55,modvarargin{:});
                randT1 = interTrials(randperm(length(interTrials)));
                tmt1 = mts;
                clear mts
                mts{1} = tmt1;
                nselec1 = floor(length(interTrials)/3);
                cd ..
                if isempty(nptDir('session03'))
                    theses = 1;
                    nselec2 =0;
                    for stim = 1 : nstim
                        trials{stim} = randT1((stim-1)*nselec1+1:stim*nselec1);
                    end
                else
                    cd session03
                    tmts = mtstrial('auto','redoSetNames');
                    sesTrials = [1 : tmts.data.numSets];
                    randT2 = sesTrials(randperm(length(sesTrials)));
                    nselec2 = floor(length(sesTrials)/3);
                    
                    mts{2} = tmts;
                    for stim = 1 : nstim
                        trials{stim,1} = randT1((stim-1)*nselec1+1:stim*nselec1);
                        trials{stim,2} = randT2((stim-1)*nselec2+1:stim*nselec2);
                    end
                    theses = [1 3];
                end
                
                emp = (nselec1+ nselec2) == 0;
                
                if Args.FixCorrected
                    cd ..
                    cd session01
                    mts = mtstrial('auto','redoSetNames');
                    clear trials
                    for stim = 1 : nstim
                        ntrialsFix = nselec1+nselec2;
                        
                        Tcor = mtsgetTrials(mts,'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'stable','BehResp',Args.BehResp,modvarargin{:});
                        Tcor = Tcor(randperm(length(Tcor)));
                        minT = min([ntrialsFix length(Tcor)]);
                        trials{stim} = Tcor(1:minT);
                    end
                end
                
            else
                for stim = 1 : nstim;
                    if Args.BehResp == 1
                        if Args.IncorCorrected
                            ntrialsIncor = length(mtsgetTrials(mts,'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'BehResp',0,modvarargin{:}));
                            Tcor = mtsgetTrials(mts,'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'stable','BehResp',Args.BehResp,modvarargin{:});
                            Tcor = Tcor(randperm(length(Tcor)));
                            minT = min([ntrialsIncor length(Tcor)]);
                            trials{stim} = Tcor(1:minT);
                        else
                            if strcmp(Args.type,'Fix')
                                trials{stim} =  mtsgetTrials(mts,'CueObj',55,modvarargin{:});
                                
                            else
                                trials{stim} =  mtsgetTrials(mts,'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'stable','BehResp',Args.BehResp,modvarargin{:});
                            end
                        end
                    else
                        trials{stim} =  mtsgetTrials(mts,'iCueObj',eval(stims1),'iCueLoc',eval(stims2),'BehResp',0,modvarargin{:});
                    end
                    emp(stim) = isempty(trials{stim}) + ~(length(trials{stim}) > Args.minTrials);
                end
                if strcmp(Args.type,'Fix')
                    ntrials = floor(length(trials{stim}) / 3);
                    newtrialseq = trials{1}(randperm(length(trials{1})));
                    for stim = 1 : 3; trials{stim} = newtrialseq((stim-1)*ntrials + 1:stim*ntrials); end
                end
            end
            if sum(emp) == 0
                %% surrogate
                if Args.surrogate
                    if Args.notGenerelized
                        rtrials = randTrials(trials,nstim);
                        [mt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:});
                        mutualgram = zeros(Args.iterations,size(mt,1)-2,size(mt,2));
                        for ii = 2 : Args.iterations
                            rtrials = randTrials(trials,nstim);
                            
                            [mutualgramt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:});
                            
                            mutualgram(ii,:,:) = single(mutualgramt(1:size(mutualgram,2),:));
                        end
                    else
                        if isempty(Args.days);
                            if Args.notGenerelized
                                theday = obj.data.Index(pair,2);
                            else
                                theday = obj.data.Index(ind(pair),2);
                            end
                        else
                            [b,m,~] = unique(obj.data.setNames);
                            % the next two lines are to reset the sorting
                            % produced by th e'unique fct
                            [~,ix] = sort(m);
                            b = b(ix);
                            theday = strfind(b,Args.days{pair}(end-7:end-1));
                            
                            d = 1;
                            while iscell(theday) && d <= length(theday)
                                if ~isempty(theday{d});
                                    clear theday; theday = d;
                                end;
                                d = d +1;
                            end;
                            if iscell(theday) && d > length(theday)
                                break
                            end
                        end
                        [~,goodpairs] = get(obj,'Number','snr',99);
                        daypairs = find(obj.data.Index(:,2) == theday);
                        pairss = intersect(daypairs,goodpairs);
                        grPP = unique(obj.data.Index(pairss,10),'rows');
                        grPF = unique(obj.data.Index(pairss,11),'rows');
                        neuroinfo = NeuronalChAssign;
                        chPP = find(ismember(neuroinfo.groups,grPP) == 1);
                        chPF = find(ismember(neuroinfo.groups,grPF) == 1);
                        
                        rtrials = randTrials(trials,nstim);
                        if isempty(Args.days);
                            [mt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:});
                        else
                            
                            [mt,f,time,~,~,~,~] = migram(mts,rtrials,[chPP(1) chPF(1)],modvarargin{:});
                            
                        end
                        mutualgram = zeros(Args.iterations,size(mt,1)-2,size(mt,2));
                        mutualgram(1,:,:) = mt(1:size(mt,1)-2,:); % -2 is to avoid random selection of short trials that decrease the max time in mugram
                        
                        for ii = 2 : Args.iterations
                            
                            clear ch
                            ch(1) = chPP(randi(length(chPP)));
                            ch(2) = chPF(randi(length(chPF)));
                            rtrials = randTrials(trials,nstim);
                            
                            [mutualgramt,f,time,~,~,~,~] = migram(mts,rtrials,ch,modvarargin{:});
                            
                            mutualgram(ii,:,:) = single(mutualgramt(1:size(mutualgram,2),:));
                        end
                        time = time(1:size(mt,1)-1);
                        gr = [vecr(grPP) vecr(grPF)];
                    end
                else
                    %% data
                    if Args.matchAlign
                        [mutualgram,f,time,C,phi,phistd,Cerr,before] = migram(mts,trials,ch,modvarargin{:});
                        %                         if Args.matchAlign
                        %                             varargout{5} = before;
                        %                         end
                        %
                        %                         varargout{6} = S12;
                        %                         varargout{7} = S1;
                        %                         varargout{8} = S2;
                        time = time -before/1000;
                    else
                        if Args.appendS
                            [mutualgram,f,time,C,phi,~,~,~,S12,S1,S2] = migram(mts,trials,ch,modvarargin{:});
                        else
                            [mutualgram,f,time,C,phi,phistd,Cerr,~,S12,S1,S2] = migram(mts,trials,ch,modvarargin{:});
                        end
                    end
                end
                if Args.save
                    if isempty(Args.days)
                        if Args.notGenerelized
                            cd(obj.data.setNames{pair})
                        else
                            cd(obj.data.setNames{ind(pair)})
                        end
                    else
                        cd(Args.days{pair})
                        
                    end
                    cd grams
                    if Args.surrogate
                        if Args.notGenerelized
                            save(matfile,'mutualgram','f','time','trials','Args')
                        else
                            save(matfile,'mutualgram','f','time','trials','grPP','grPF','Args')
                        end
                    else
                        if Args.appendS
                            save(matfile,'S12','S1','S2','-append')
                        else
                            save(matfile,'mutualgram','f','time','trials','gr','C','phi','phistd','Cerr','Args')
                        end
                    end
                    display(['saving' ' ' matfile])
                end
            else
                mes = 'not enough trials';
                %                 keyboard
                %                 save(matfile,'mes')
                %                 pwd
                display(mes)
            end
        else
            cd ..
            cd grams
            try
                load(matfile,'mutualgram','f','time')
            catch
                unix(sprintf('rm %s',matfile))
                display('removing corrupted file')
            end
        end
        if nargout > 0
            allmi{count} = mutualgram;
            count = count + 1;
        end
    end
end
if nargout > 0
    varargout{1} = allmi;
    varargout{2} = f;
    varargout{3} = time;
end
cd(sdir)
%%
function rtrials = randTrials(trials,nstim)

stimN = zeros(nstim,size(trials,2));
rtrials = cell(nstim,size(trials,2));
alltrials = cat(2,trials{:});
randtrial= alltrials(randperm(length(alltrials)));
for ss = 1 : size(trials,2)
    for stim = 1: nstim;
        stimN(stim,ss) = length(trials{stim});
        if stim == 1
            rtrials{stim,ss} = randtrial(1:stimN(stim));
        else
            rtrials{stim,ss} = randtrial(stimN(stim-1)+ 1 :stimN(stim-1)+stimN(stim));
        end
    end
end