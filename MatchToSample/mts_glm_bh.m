function mts_glm_bh

%bh corrects p-values from mts_glm_data

load glm_delay glm_delay
s = size(glm_delay,1);

for x = 1 : s
    p(x) = str2double(glm_delay(x).interaction);
    t(x) = str2double(glm_delay(x).time_coefs_pval(2)); %this is the time p-val from a model with time only, NOT DND TEST FROM INTERACTION MODEL
end

[h_delay, crit_p_delay] = fdr_bh(p,.05);
%h: 1 indicates that p-value <= crit_p
save bh_delay h_delay crit_p_delay 


%run for time p-val
[h_time,crit_p_time] = fdr_bh(t,.05);
save bh_time h_time crit_p_time


load glm_fix glm_fix
s = size(glm_fix,1);

for x = 1 : s
    p(x) = str2double(glm_fix(x).interaction);
end

[h_fix, crit_p_fix] = fdr_bh(p,.05);
%h: 1 indicates that p-value <= crit_p
save bh_fix h_fix crit_p_fix 






