function make_depths_snr_reconstruction(redo)

%redo: 1 = save new channel_offsets, 0 = no save


monkey = pwd;
%run at monkey level

load betty_reconstruction
load idedays

days = alldays;

%get depths
%get snrs
%days = switchDay('ML','combNtrials',150);


num_days = size(days,2);

daydir = pwd;
dd = 0;

for d = 1 : num_days

    if isempty(nptdir('skip.txt'))
        dd = dd + 1;
        try
            
            cd([daydir filesep days{d} filesep 'session01'])
            
            descriptor = nptdir('*descriptor.txt');
            descriptor_info = ReadDescriptor(descriptor(1).name);
            groups = descriptor_info.group;
            channel_depths = descriptor_info.recdepth;
            pwd
            [i ii]=find(groups); %groups recorded from
            
            cd('highpass');
            
            load SNR_channels
            
            channels_list = channel_snr_list(:,1);
            snr_list = channel_snr_list(:,2);
            snrs(dd,channels_list) = snr_list;
            
            
            for c = ii
                day_depths(dd,groups(c)) = channel_depths(c);
                
            end
            
        catch
            dd = dd-1
        end
        
    end
    
    
end

num_days = size(day_depths,1);



%depths
realdepth = (day_depths) / 1000; %mm
f=find(realdepth <= 0);
realdepth(f) = 0;
depths = round(realdepth/.5);

% % % % % %snr
% % % % % for d = 1 : num_days
% % % % %     cc = 0;
% % % % %     for c = find(snrs(d,:))
% % % % %         cc = cc + 1;
% % % % %         if depths(d,c) ~= 0
% % % % %             reconstruction(d,c) = betty_reconstruction(c,depths(d,c));
% % % % %         end
% % % % %     end
% % % % % end


depths_snr_reconstruction.depths = realdepth;
depths_snr_reconstruction.snr = snrs;
% % % % % depths_snr_reconstruction.reconstruction = reconstruction;
%reconstruction is determined using make_depths_snr_reconstruction
cd(monkey)


    save depths_snr_reconstruction2 depths_snr_reconstruction


%% run --> offset_depths_snr_reconstruction
