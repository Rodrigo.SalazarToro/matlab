function rejected_trial_info(varargin)

%run at session level
%
% Enter a channel number (this correponds to grid position).
%   -enter std/pwr/trans to get only the trials for that category
%
%   OR
%
% Enter a trial to find out which channels it was rejected for.
%   -enter std/pwr/trans to get only channels for that category
%
%   OR
%
% Enter a combination of std/pwr/trans without a channel or trial to get a
% list of all the trials rejected for those categories.
%
%   OR
%
% Enter 'rejected_trials' or 'rejected_channels' to get a list of rejected
% trials or channels.
%
Args = struct('channel',[],'trial',[],'std',0,'pwr',0,'trans',0,'rejected_trials',0,'rejected_channels',0);
Args.flags = {'std','pwr','trans','rejected_trials','rejected_channels'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sesdir = pwd;

N = NeuronalHist('bmf');

cd([sesdir filesep 'lfp'])
load rejectedTrials
cd(sesdir)

trials = [];
if ~isempty(Args.channel)
    ch = find(N.gridPos == Args.channel);
    
    if intersect(rejectCH,ch)
        fprintf(1,'\n Rejected Channel \n')
    end
    
    %get list of trials for each art method
    trans_trials = transTrials{ch};
    pwr_trials = PWTrials{ch};
    std_trials = stdTrials{ch};
    
    if ~Args.std && ~Args.pwr && ~Args.trans
        trials = [trials std_trials pwr_trials trans_trials];
    else
        if Args.std
            trials = [trials std_trials];
        end
        if Args.pwr
            trials = [trials pwr_trials];
        end
        if Args.trans
            trials = [trials trans_trials];
        end
    end
elseif ~isempty(Args.trial)
    for x = 1 : size(stdTrials,1)
        if ~isempty(intersect(transTrials{x},Args.trial))
            fprintf(1,[num2str(N.gridPos(x)) '  rejected for transient \n'])
        end
        if ~isempty(intersect(PWTrials{x},Args.trial))
            fprintf(1,[num2str(N.gridPos(x)) '  rejected for power \n'])
        end
        if ~isempty(intersect(stdTrials{x},Args.trial))
            fprintf(1,[num2str(N.gridPos(x)) '  rejected for std \n'])
        end
    end
elseif Args.rejected_trials || Args.rejected_channels
    if Args.rejected_trials
        rejecTrials
    else
        N.gridPos(rejectCH)
    end
    
else
    if Args.std
        trials = cell2mat(stdTrials');
    end
    if Args.pwr
        trials = cell2mat(PWTrials');
    end
    if Args.trans
        trials = cell2mat(transTrials');
    end
end

if ~isempty(trials)
    trials = sort(unique(trials))
end


