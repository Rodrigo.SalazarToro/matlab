pairs = {[8 3;10 3] [8 4;10 4] [11 3] [11 4] [13 3] [13 4]};

lab = {'PE/PEC - 8B' 'PE/PEC - dPFC' 'PG - 8B' 'PG - dPFC' 'LIP - 8B' 'LIP - dPFC'};

[r,ind] = get(obj,'Number','SNR',[],'type',{'betapos34' [1 3]});
[r,tot] = get(obj,'Number','SNR',[],'sigSur',{'beta' [1:4] [1 3]});

for p = 1 : 6
    aind{p} = [];
    atot{p} = [];
    for i = 1 : size(pairs{p},1)
        atot{p} = [atot{p}; find(obj.data.hist(tot,1) == pairs{p}(i,1) & obj.data.hist(tot,2) == pairs{p}(i,2))];
        aind{p} = [aind{p}; find(obj.data.hist(ind,1) == pairs{p}(i,1) & obj.data.hist(ind,2) == pairs{p}(i,2))];
    end
end

for p = 1 : 6; htot(p,1) = length(atot{p}); htot(p,2) = length(aind{p}); summarizeGC(obj,aind{p},'figTit',lab{p});end

figure; bar(htot)
set(gca,'xTickLabel',lab)
ylabel('number of pairs')
legend('total','betapos34')

for ch = 1 : size(value.C,3)
    for p = 1 : 4;
        for fq = 1 : 65;
            [r,pv] = corrcoef(perf,squeeze(value.C(:,fq,ch,p)));
            xcor(fq,ch,p) = r(1,2);
            sigxcor(fq,ch,p) = pv(1,2);
        end;
    end;
end

for ch = 1 : size(value.C,3)
    subplot(5,2,1); [ax,h1,h2] = plotyy(ti,perf,ti,RT(:,1));%axis([50 450 50 100]);
    set(ax(2),'YLIM',[80 250])
    
    for p =1 : 4;
        sigcoh = max(squeeze(value.C(:,:,ch,p)),[],1) > 0.2;
        subplot(5,2,(p-1)*2 + 3); imagesc(tr,fliplr(f),rot90(squeeze(value.C(:,:,ch,p))),[0.2 0.5]); ylim([10 40]);
        subplot(5,2,(p-1)*2 + 4); plot((xcor(:,ch,p) .* (sigxcor(:,ch,p) <0.000001)) .* sigcoh',f ); axis ij;xlim([-1 1]); ylim([10 40]);
    end;
    pause; clf;
end

for nw = 1 : 500
    RT(nw,1) = mean(mt.data.FirstSac([nw : nw +100]));
    RT(nw,2) = std(mt.data.FirstSac([nw : nw +100]));
end


for s = 1 : 70;
    
    cd(mt.data.setNames{s});
    mts = mtstrial('auto');
    rules(s) = mts.data.Index(1,1);
    [aperf{s},ati{s}] = getperformance;
end


ide = find(rules == 1);
loc = find(rules == 2);
str{1} = ide; str{2} = loc;

for r = 1 : 2
    cperf{r} = [];
    swperf{r} = [];
    for i = str{r}
        if length(aperf{i}) > 300 && str2num(mt.data.setNames{i}(end-1 : end)) == 2
            cperf{r} = [cperf{r}; aperf{i}(1: 300)];
        
        elseif length(aperf{i}) > 300 && str2num(mt.data.setNames{i}(end-1 : end)) == 3
            swperf{r} = [swperf{r}; aperf{i}(1: 300)];
        end
    end
    
    
end
lb = {'r' 'k'};
for r = 1 : 2
    subplot(2,1,1)
    plot(mean(cperf{r}),lb{r});
    hold on
    plot(mean(cperf{r}) + std(cperf{r}),sprintf('%s--',lb{r}));
    plot(mean(cperf{r}) - std(cperf{r}),sprintf('%s--',lb{r}));
    
    subplot(2,1,2)
    plot(mean(swperf{r}),lb{r});
    hold on
    plot(mean(swperf{r}) + std(swperf{r}),sprintf('%s--',lb{r}));
    plot(mean(swperf{r}) - std(swperf{r}),sprintf('%s--',lb{r}));
end


%% betty
for s = 1 : 75;
    
    cd(mt.data.setNames{s});
    mts = mtstrial('auto');
    rules(s) = unique(mts.data.Index(1,1));
    [aperf{s},ati{s},b,switches{s}] = getperformance('ML','ruleSwitch');
end


ide = find(rules == 1);
loc = find(rules == 2);
str{1} = ide; str{2} = loc;

for r = 1 : 2
    cperf{r} = [];
    swperf{r} = [];
    for i = str{r}
        if length(aperf{i}) > 300 && str2num(mt.data.setNames{i}(end-1 : end)) == 2
            cperf{r} = [cperf{r}; aperf{i}(1: 300)];
        
        elseif length(aperf{i}) > 300 && str2num(mt.data.setNames{i}(end-1 : end)) == 3
            swperf{r} = [swperf{r}; aperf{i}(1: 300)];
        end
    end
    
    
end
lb = {'r' 'k'};
for r = 1 : 2
    subplot(2,1,1)
    plot(mean(cperf{r}),lb{r});
    hold on
    plot(mean(cperf{r}) + std(cperf{r}),sprintf('%s--',lb{r}));
    plot(mean(cperf{r}) - std(cperf{r}),sprintf('%s--',lb{r}));
    
    subplot(2,1,2)
    plot(mean(swperf{r}),lb{r});
    hold on
    plot(mean(swperf{r}) + std(swperf{r}),sprintf('%s--',lb{r}));
    plot(mean(swperf{r}) - std(swperf{r}),sprintf('%s--',lb{r}));
end