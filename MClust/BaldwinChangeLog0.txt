2/8/03
ShowWaveforms		Changed limits on axii

2/12/03
ShowWaveforms		Changed code to plot only one channel for electrodes
nptLoadingEngine.m	created function(added to MClust Directory)


2/13/03
MclustCallbacks		added *waveforms.bin file to uigetfile

2/28/03
GeneralizedCutterCallbacks	added EditWaveforms case
EditWaveforms			new function
GeneralizedCutterCallbacks	added EditWaveforms to pulldown box @lines 991,997,1004

3/7/03
new folder editwaveforms
bbconvexhull				changed constructor because editwaveforms required bbconvexhull to have new fields
@bbconvexhull/subtract_exceptions	new method 
MClust callbacks			added functionality to convert older files to current bbconvexhull objects

3/10/03
nptloadingEngine		changed time resolution to 1/10 of a millisecond 

3/11/03
EditWaveforms			added editbox, previous and next functionality
EditWaveformsGUI			added editbox, previous and next functionality

3/13/03
bbconvexhull			added precluster case to constructor !!should revise!!

3/17/03 
merge				added subtract_exceptions functionality

3/18/03	
PutOnTop			new function used when plotting multiple lines

3/24/03				
MClustCallbacks			write overlap file
ProcessCluster			" and fixed -1 bug
WriteOverlapFile		"

3/26/03
feature_valleyIndex		new feature
feature_triggerValue		new feature
editwaveforms			changed
highlight 			new function
editwaveformsGUI		changed
editwaveformskeyPress		changed

4/28/03
Generalized Cutter		added Mean Waveforms button


**************************************
File Changed List from original MCLust
**************************************

all of editwaveforms folder
@bbconvexhull/bbconvexhull
@bbconvexhull/subtract_exceptions
MClustcallbacks
GeneralizedCutterCallbacks
ShowWaveforms
nptLoadingEngine
@bbconvexhull/merge
@bbconvexhull/DeleteMembers
ProcessClusters
WriteOverlapFile
GeneralizedCutter



8/6/03
HistISI		removed if TS<1e7 etc...
