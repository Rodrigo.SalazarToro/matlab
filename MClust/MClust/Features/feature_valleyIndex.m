function [PeakData, PeakNames,PeakPars] = feature_valleyIndex(V, ttChannelValidity, Params)

% MClust
% [PeakData, PeakNames] = feature_valleyIndex(V, ttChannelValidity)
% Returns index of valley point for each channel
%
% INPUTS
%    V = TT tsd
%    ttChannelValidity = nCh x 1 of booleans
%
% OUTPUTS
%    Data - nSpikes x nCh peak values
%    Names - "ValleyIndex: Ch"
%



TTData = Data(V);

[nSpikes, nCh, nSamp] = size(TTData);


f = find(ttChannelValidity);


PeakData = zeros(nSpikes, length(f));

PeakNames = cell(length(f), 1);
PeakPars = {};

for iCh = 1:length(f)
 [m,ix] = min(squeeze(TTData(:, f(iCh), :)), [], 2);
 PeakData(:, iCh) = ix + rand(size(ix))-0.5;

 PeakNames{iCh} = ['valleyIndex: ' num2str(f(iCh))];
end
