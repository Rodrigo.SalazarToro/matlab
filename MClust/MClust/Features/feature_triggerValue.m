function [PeakData, PeakNames,PeakPars] = feature_triggerValue(V, ttChannelValidity, Params)

% MClust
% [PeakData, PeakNames] = feature_triggerValue(V, ttChannelValidity)
% Returns value at trigger point for each channel
%
% INPUTS
%    V = TT tsd
%    ttChannelValidity = nCh x 1 of booleans
%    Params - trigger index (default is 11th point)
%
%
% OUTPUTS
%    Data - nSpikes x nCh trigger values 
%    Names - "triggerValue: Ch"
%

if nargin < 3
     index=11;
else
     index=Params{1};
end
PeakPars={index};

TTData = Data(V);

[nSpikes, nCh, nSamp] = size(TTData);


f = find(ttChannelValidity);


PeakData = zeros(nSpikes, length(f));
PeakNames = cell(length(f), 1);


for iCh = 1:length(f)
 ix = TTData(:, f(iCh), index);
 PeakData(:, iCh) = ix;

 PeakNames{iCh} = ['triggerValue: ' num2str(f(iCh))];
end
