function [WaveletData, WaveletNames, WaveletPars] = feature_WAVELET1(V, ttChannelValidity, Params)

%changes from trial4, it couldn't work with batch commands...because
%       WaveletData(:,iCh) but savecoef(iCh,:,1)
%change to savecoef(:,iCh,1/2/3) see how
%
% MClust
% [Data, Names] = feature_wavelet1(V, ttChannelValidity,Params)
% Calculate wavelet feature of each spike in each channel
%
% INPUTS
%    V = TT tsd
%    ttChannelValidity = nCh x 1 of booleans
%    Params.g = groupname of datafile
%    params.b = current block no. of datafile
%
% OUTPUTS
%    Data - nCh x nSpikes of 1st wavelet coefficient store in file during
%    execution
%    Names - "SW: Ch"
%
%0. checks whether resulting file exits or not, if yes, copy result, or ->
%1. wavelet calculation for each spike in each channel
%2. lillietest for coefficients of each spike
%3. grab the most confident 3 coefficients describing non-gaussian
%charateristeric
%4. 1st--->wavelet1
%5. 2nd --->wavelet2
%6. 3rd --->wavelet3
%7. save in a file 'wavecoeffs.mat'

WAVEPTS = 32;
NWAVELETCOEFS = 3;
WAVELETNUM = 1;

TTData = Data(V);
[nSpikes, nCh, nSamp] = size(TTData);
f = find(ttChannelValidity);
fname = strcat(Params.g,'_coeffs_block_',Params.b,'.mat');

WaveletData = zeros(nSpikes, length(f));
WaveletNames = cell(length(f), 1);
WaveletPars = {};

chlength = length(f);

% wcoeffs = zeros(size(TTData,1),chlength,1);

if(~isempty(dir(fname)))
    % load savecoef
    load(fname);

    for iCh = 1:chlength
        wcoeffs = savecoef(:,iCh,WAVELETNUM);
        WaveletData(:,iCh) = wcoeffs;
        WaveletNames{iCh} = ['wavelet1: ' num2str(f(iCh))];
    end
else
    colidx = 1:WAVEPTS;
    cfidx = zeros(1,NWAVELETCOEFS);
    savecoef = zeros(nSpikes,chlength,NWAVELETCOEFS);
    cf = zeros(nSpikes,WAVEPTS);

    for iCh = 1:chlength
        w = squeeze(TTData(:, f(iCh), :));

        for jspike = 1 : nSpikes
            [C,L]=wavedec(w(jspike,:),3,'db4');
            cf(jspike,colidx)=C(colidx);
        end

        sd = zeros(1,WAVEPTS);
        for j=colidx                                 % lillietest test for coefficient selection
            [hh,pp,lstat,cv]=lillietest(cf(:,j));
            if hh==1
                sd(j)=lstat;
            else
                sd(j)=0;
            end
        end
        [max,ind]=sort(sd);
        cfidx = ind(WAVEPTS:-1:(WAVEPTS-NWAVELETCOEFS+1));

        savecoef(:,iCh,:) = cf(:,cfidx(1:NWAVELETCOEFS));

        wcoeffs = cf(:,cfidx(WAVELETNUM));
        WaveletData(:,iCh) = wcoeffs;
        WaveletNames{iCh} = ['wavelet1: ' num2str(f(iCh))];
    end

    save(fname,'savecoef');
end
