function WriteNPTFiles(overlap,numClusters,MClust_TTdn, MClust_fn)

cd(MClust_TTdn)
ind = strfind(lower(MClust_fn),'g');
sessiongroupname = MClust_fn(1:ind+4);
group = MClust_fn(ind+1:ind+4);

%writeOverlapFile
WriteOverlapFile([fullfile(MClust_TTdn, MClust_fn) '_overlap.mat'], overlap);


%get the cluster names and types
cn = ClusterNames(numClusters);
filename = ['ClusterNamesg' group '.txt'];
WriteClusterNamesFile(filename,cn)

%write group ispikes files
fprintf('Creating ispikes for Group %s\n',group)
obj = ispikes('Group',group,'save','redo');

%Create cell directories and cell ispikes
separate(obj,'CellNames',cn,'save');

