function multiCopy(iClust)

numCopies = str2num(char(inputdlg('','How many Copies to make?',1,{'2'})))

if ~isempty(numCopies)
    GeneralizedCutterStoreUndo('multiCopy');
    global MClust_Clusters MClust_Hide
    global MClust_ClusterFileNames
    figHandle = ParentFigureHandle(gcbo);  % control window figure handle
    
    for ii = 1:numCopies
        MClust_ClusterFileNames{end+1} = ['Copy of cluster ' num2str(iClust)];
        MClust_Clusters{end+1} = MClust_Clusters{iClust};
        MClust_Hide(end+1) = 0;
    end   
    
    MClust_Clusters{iClust} = DeleteMembers(MClust_Clusters{iClust});
%     GeneralizedCutterClearClusterKeys(figHandle);
%     GeneralizedCutterRedrawClusterKeys(figHandle, max(0,length(MClust_Clusters)-16));
    
set(gcbo,'Tag','Pack Clusters')
GeneralizedCutterCallbacks


    warndlg(['Cluster ' num2str(iClust) ' copied to cluster(s) ' num2str((length(MClust_Clusters)-numCopies+1):length(MClust_Clusters)) ' and then deleted and packed.'], 'Copy successful');
end