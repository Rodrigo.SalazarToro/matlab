function BringToTop(iClust)

% BringToTop
% Written by Baldwin Goodell
% Montana State University
% Charlie Gray Lab
% 30 September 2003
%
% Put this in the ClusterOptions directory
%
%moves the current cluster to the top of the list while shifting all other
%clusters down.  All colors and names are conserved.

figHandle = gcbf;

global MClust_Clusters
global MClust_Colors
global MClust_ClusterFileNames
GeneralizedCutterStoreUndo('Bring to Top');
nClust = length(MClust_Clusters);
MClust_Clusters = [MClust_Clusters(iClust) MClust_Clusters(1:iClust-1) MClust_Clusters(iClust+1:nClust)];
MClust_Colors = [MClust_Colors(1,:) ; MClust_Colors(iClust+1,:) ; MClust_Colors(2:iClust,:) ;  MClust_Colors(iClust+2:size(MClust_Colors,1),:)];
MClust_ClusterFileNames = [MClust_ClusterFileNames(iClust) MClust_ClusterFileNames(1:iClust-1) MClust_ClusterFileNames(iClust+1:nClust)];
GeneralizedCutterClearClusterKeys(figHandle);
GeneralizedCutterRedrawClusterKeys(figHandle, max(0,length(MClust_Clusters)-16));

