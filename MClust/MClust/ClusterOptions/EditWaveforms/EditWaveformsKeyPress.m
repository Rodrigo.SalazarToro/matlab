function EditWaveformsKeyPress()

a=get(gcbf,'currentkey');
switch a
case 'leftarrow'
   EditWaveformsGUI('SelectPrevious_Callback',gcf,[],[])
case 'downarrow'
  EditWaveformsGUI('PreviousButton_Callback',gcf,[],[])
case 'uparrow'
 EditWaveformsGUI('NextButton_Callback',gcf,[],[])
case 'rightarrow'
    EditWaveformsGUI('SelectNext_Callback',gcf,[],[])
case 'backspace' 
    EditWaveformsGUI('removewaveform_Callback',gcf,[],[])
case 'delete' 
    EditWaveformsGUI('removewaveform_Callback',gcf,[],[])
end
