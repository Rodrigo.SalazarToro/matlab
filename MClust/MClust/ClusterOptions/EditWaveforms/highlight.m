function highlight(lineh)

if nargin==0
    lineh = gcbo;
end

UserData = get(gcf,'UserData');

%defines
Hlw=5;
lw=.5;
Color = UserData.Color;
newcolor = abs((1-Color));

%get already highlighted signals
selecthandles = findobj(gcf,'LineWidth',Hlw);
%turn off other highlighted lines
set(selecthandles,'LineWidth',lw,'Color',Color);
%turn on this signal
linehandle = UserData.linehandle;
for ii=1:length(lineh)
    [row(ii) col] = ind2sub(size(linehandle),find(linehandle==lineh(ii)));
end
sel = linehandle(row,:);
sel = reshape(sel,1,(size(sel,1)*size(sel,2)));
set(sel,'Color',newcolor,'LineWidth',Hlw)
PutOnTop([sel UserData.means])  %put these lines on top

%update UserData
UserData.currentline = row;
set(gcf,'UserData',UserData)
gca;
title(['Waveforms: Cluster ' num2str(UserData.iClust) ' -- Number(s) ' num2str(UserData.currentline) ' of ' num2str(UserData.startpoint) ' through ' num2str(UserData.startpoint + length(UserData.Pts)-1) ' of ' num2str(UserData.maxPts) ' total waveforms shown']);
















