function RunEditWaveforms(iClust)


global MClust_TTData MClust_Clusters MClust_FeatureData
global MClust_xlbls
global MClust_ylbls
global MClust_Colors

%which spikes in this cluster?
f = FindInCluster(MClust_Clusters{iClust}, MClust_FeatureData);
%get data of the spikes in this cluster
clustTT  = ExtractClusterALL(MClust_TTData, f);

waveformpts = 32;
waveformpts2 = waveformpts + 2;
triggeridx = 11;
trigrefractpts = 3;

WVD = Data(clustTT);    %waveform data for this cluster
mWV = squeeze(mean(WVD,1));
sWV = squeeze(std(WVD,1));

if isempty(find(mWV(2:4,:))) 
    cha=1;
elseif isempty(find(mWV(3:4,:)))
    cha=2;
else
    cha=size(mWV,1);
    % cha = 4;
end

mWV = zeros(cha,waveformpts);

maxPts = length(WVD(:,1,1));
fig_handle = findobj('Tag','EditWaveforms');
if isempty(fig_handle)
    fig_handle = EditWaveformsGUI;
end

%draw keys
CallbackString = 'EditWaveformsGUI(''removewaveform_Callback'',gcf,[],[],num)';
width = 0.10;
height = .8/(length(MClust_Clusters)+1);
XLoc=.9;
YLoc=.9;
for iC = 0:length(MClust_Clusters)
    YLoc = YLoc - height;
    CallbackFunc = strrep(CallbackString,'num',num2str(iC));
    drawButtons(fig_handle,XLoc,YLoc,iC,width,height,CallbackFunc)
end



edithandle = findobj(fig_handle,'Tag','EditNumber');
num_waves = str2num(get(edithandle,'String'));
UserData = get(fig_handle,'UserData');
if isfield(UserData,'startpoint')
    start = UserData.startpoint;
else
    start=1;
end

if maxPts > start + num_waves-1
    Pts = start : start + num_waves-1;
    numwaveplot = num_waves;
else
    Pts = start : maxPts;
    numwaveplot = maxPts-start+1;
end

axeshandle = findobj(fig_handle,'Type','Axes');
axes(axeshandle);
% need to clear the axes since line objects are not removed by overplots
cla
xpts = 1:waveformpts;


% tic
% create matrix of x values
mat1 = [xpts' ones(waveformpts,1)];
chanidx = (0:(cha-1))*waveformpts2;
mat2 = [ones(1,cha); chanidx];
mat3 = mat1 * mat2;
xmat = repmat(mat3,1,numwaveplot);
% rearrange the data into columns
y1 = squeeze(permute(WVD(Pts,1:cha,:),[3 2 1]));
ymat = reshape(y1,waveformpts,[]);
% draw the waveforms
lh = line(xmat,ymat,'Color',MClust_Colors(iClust+1,:),'ButtonDownFcn','highlight');
% reshape to format for the old code
linehandle = reshape(lh,cha,[])';
hold on
% draw the mean waveforms
% add transpose to save the handles in the same format as the old code
mean_handle = plot(mat3,mWV','b','LineWidth',3)';
% draw the trigger and refractory periods
a = axis;
cmat = repmat(chanidx+triggeridx,2,1);
dmat = repmat(a(3:4)',1,cha);
line(cmat,dmat,'LineStyle','--','Color','k');
line(cmat+trigrefractpts,dmat,'LineStyle','--','Color','r');
line(cmat-trigrefractpts,dmat,'LineStyle','--','Color','r');
hold off
% toc

% tic
% linehandle = zeros(numwaveplot,cha);
% mean_handle = zeros(1,cha);
% for it = 1:cha
%     xrange = (waveformpts2 * (it-1)) + xpts; 
%     hold on
%     linehandle(:,it)=line(xrange,squeeze(WVD(Pts,it,:))','color',MClust_Colors(iClust + 1,:),'ButtonDownFcn','highlight');
%     mean_handle(it) = plot(xrange, mWV(it,:),'LineWidth',3);
% end
% a = axis;
% for it = 1:cha
%     line((it-1)*waveformpts2+11*[1 1] , [a(3) a(4)],'LineStyle','--','Color','k');
%     line((it-1)*waveformpts2+8*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
%     line((it-1)*waveformpts2+14*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
% end
% hold off
% toc

set(gca,'XTick',[]);
%set(gca,'Ygrid','on');
%set(gca,'Color','none');


%set UserData to keep track of data
UserData.linehandle=linehandle;
UserData.means = mean_handle;
UserData.iClust=iClust;
UserData.maxPts=maxPts;
UserData.Pts=Pts;
UserData.startpoint = start;
UserData.Color=MClust_Colors(iClust + 1,:);
if ~isfield(UserData,'currentline')
    UserData.currentline = 0;
end
set(fig_handle,'UserData',UserData);
title(['Waveforms: Cluster ' num2str(iClust) ', '  num2str(start) ' through ' num2str(start + length(Pts)-1) ' of ' num2str(maxPts) ' total waveforms shown']);
hold off



