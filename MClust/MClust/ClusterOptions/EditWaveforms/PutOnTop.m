function PutOnTop(sel)
    sel=reshape(sel,1,prod(size(sel)));
    c=get(gca,'Children');
    try
    c = [sel setdiff(c,sel)];  %put selected waveforms on top
    catch
        c = [sel setdiff(c,sel)']; % rod 
    end
    set(gca,'Children',c);