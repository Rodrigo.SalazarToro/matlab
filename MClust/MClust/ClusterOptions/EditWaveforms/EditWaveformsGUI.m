function varargout = EditWaveformsGUI(varargin)
% EDITWAVEFORMSGUI M-file for EditWaveformsGUI.fig
%      EDITWAVEFORMSGUI, by itself, creates a new EDITWAVEFORMSGUI or raises the existing
%      singleton*.
%
%      H = EDITWAVEFORMSGUI returns the handle to a new EDITWAVEFORMSGUI or the handle to
%      the existing singleton*.
%
%      EDITWAVEFORMSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EDITWAVEFORMSGUI.M with the given input arguments.
%
%      EDITWAVEFORMSGUI('Property','Value',...) creates a new EDITWAVEFORMSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before EditWaveformsGUI_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to EditWaveformsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help EditWaveformsGUI

% Last Modified by GUIDE v2.5 06-Apr-2007 01:22:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @EditWaveformsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @EditWaveformsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before EditWaveformsGUI is made visible.
function EditWaveformsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to EditWaveformsGUI (see VARARGIN)

% Choose default command line output for EditWaveformsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes EditWaveformsGUI wait for user response (see UIRESUME)
% uiwait(handles.EditWaveforms);


% --- Outputs from this function are returned to the command line.
function varargout = EditWaveformsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in removewaveform.
function removewaveform_Callback(hObject, eventdata, handles,ncluster)
%removes the selected waveform from the present cluster and adds it to
%another cluster.  default is cluster 0.

global MClust_TTData MClust_Clusters MClust_FeatureData
h=gcf;
UserData = get(h,'UserData');
linehandle = UserData.linehandle;
currentline = UserData.currentline;
if currentline==0
    return
end
Pts = UserData.Pts;
iClust = UserData.iClust;

if nargin<4
    %Move to which cluster?
    a = inputdlg('To which Cluster should this point be merged?','Merge With',1,{'0'});
    a = str2num(char(a));
    if isempty(a)
        figure(gcf)
        return
    end
    if (~(a<=size(MClust_Clusters,2))) 
        figure(gcf)
        return
    end
else 
    a=ncluster;
end
    
f = FindInCluster(MClust_Clusters{iClust}, MClust_FeatureData);
MClust_Clusters{iClust} = subtract_exception(MClust_Clusters{iClust},f(Pts(currentline)));
if a
    MClust_Clusters{a} = add_exception(MClust_Clusters{a},f(Pts(currentline)));
end
UserData.currentline=currentline-1;
set(h,'UserData',UserData)
EditWaveforms(iClust)



% --- NextButton.
function NextButton_Callback(hObject, eventdata, handles)
h=gcbf;
UserData=get(h,'UserData');
edithandle = findobj(h,'Tag','EditNumber');
num_waves = str2num(get(edithandle,'String'));
maxPts = UserData.maxPts;
start = UserData.startpoint;

if start + num_waves < maxPts
    UserData.startpoint = start + num_waves;
    UserData.currentline=0;
    set(h,'UserData',UserData)
end
iClust = UserData.iClust;
EditWaveforms(iClust)
figure(gcf)

% --- PreviousButton.
function PreviousButton_Callback(hObject, eventdata, handles)
h=gcbf;
UserData=get(h,'UserData');
edithandle = findobj(h,'Tag','EditNumber');
num_waves = str2num(get(edithandle,'String'));
maxPts = UserData.maxPts;
start = UserData.startpoint;
UserData.currentline=0;

if start - num_waves > 0
    UserData.startpoint = start - num_waves;
else
    UserData.startpoint = 1;
end
set(h,'UserData',UserData)
iClust = UserData.iClust;
EditWaveforms(iClust)
figure(gcf)

function EditNumber_Callback(hObject, eventdata, handles)
h=gcbf;
UserData=get(h,'UserData');
iClust = UserData.iClust;

UserData.currentline=0;
set(h,'UserData',UserData)
EditWaveforms(iClust)
figure(gcf)

% --- SelectNext.
function SelectNext_Callback(hObject, eventdata, handles)
h=gcbf;
UserData = get(h,'UserData');
currentline = UserData.currentline(1);
linehandle = UserData.linehandle;
maxPts = UserData.maxPts;
startpoint = UserData.startpoint;

sz1 = size(linehandle,1);
if currentline==sz1 & startpoint+sz1 < maxPts
    NextButton_Callback
    UserData = get(h,'UserData');
    currentline = UserData.currentline;
    linehandle = UserData.linehandle;
    currentline=currentline+1;
elseif currentline==sz1
else
    currentline=currentline+1;
end
highlight(linehandle(currentline,1))
figure(h)


% --- SelectPrevious.
function SelectPrevious_Callback(hObject, eventdata, handles)
h=gcbf;
UserData = get(h,'UserData');
currentline = UserData.currentline(1);
linehandle = UserData.linehandle;
startpoint = UserData.startpoint;

if currentline==1 & startpoint > 1
    PreviousButton_Callback
    UserData = get(h,'UserData');
    linehandle = UserData.linehandle;
    currentline = size(UserData.linehandle,1);
    currentline = currentline;
elseif currentline<=1
    currentline=1;
else
    currentline = currentline-1;
end
highlight(linehandle(currentline,1))
figure(gcf)


% --- Executes on button press in SelectMultiple.
function SelectMultiple_Callback(hObject, eventdata, handles)
% hObject    handle to SelectMultiple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h=gcbf;
UserData = get(h,'UserData');
linehandle = UserData.linehandle;
startpoint = UserData.startpoint;

[x,y] = ginput(2); %draw horizontal or vertical line

%determine if horizontal or vertical line
if (x(2)-x(1))==0
    vertical=1;
else
    m=(y(2)-y(1))/(x(2)-x(1));
    a=axis;
    AR = (a(4)-a(3))/(a(2)-a(1));
    m=m/AR;
    if round(abs(atan(m))) %vertical
        vertical=1;
    else
        vertical=0;
    end
end
%which channel are we on?
cha = ceil(mean(x)/33);
lines = linehandle(:,cha);
for ii=1:length(lines)
    ydata(ii,:) = get(lines(ii),'YData');  
end

if vertical
    index = mod(mean(x),34);
    ydata = ydata(:,floor(index):floor(index)+1);
    ydata = (index-floor(index))*(ydata(:,2)-ydata(:,1)) + ydata(:,1);

    %is each line within the points?
    currentline = find(ydata(:,1)>min(y) & ydata(:,1)<max(y));    
else
    
    xdata = get(lines(1),'XData');  %all lines have the same Xdata
    start = min(find(xdata>min(x)));  %index of start of horiz line
    finish =  max(find(xdata<max(x))); %index of end of horiz line
    
    ydata = ydata(:,start:finish);
    crossings = nptThresholdCrossings(ydata',y(1),'ignorefirst');
    currentline = unique(crossings(:,2));
    
    
end%if vertical
if ~isempty(currentline)
    highlight(linehandle(currentline,1))
end










% --- Executes on button press in nextISI.
function nextISI_Callback(hObject, eventdata, handles)
% hObject    handle to nextISI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global MClust_TTData MClust_Clusters MClust_FeatureData MClust_FeatureTimestamps

h=gcbf;
UserData = get(h,'UserData');
%which lines are currently displayed
if UserData.currentline
    c=UserData.Pts(UserData.currentline);
else
    c=0;
end

f  = FindInCluster(MClust_Clusters{UserData.iClust}, MClust_FeatureData);
epsilon = 1e-100;
ISI = diff(Data(ts(MClust_FeatureTimestamps(f)))/10) + epsilon;
handle = findobj(h,'Tag','ISITime');
ISItime = str2num(get(handle,'String'));
ind=find(ISI<ISItime);    %less than 1 msec

if isempty(ind)
    warndlg('No spikes with small ISI.')
else
    
    %now we want to select the next pair of waveforms with small ISI
    ind = ind(find(ind>c(1)));
    if isempty(ind)
        warndlg('Last spike pair with small ISI is shown.')
        return
    end
    waveforms = [ind(1) ind(1)+1];
    %how many are we displaying right now
    if ~ismember(waveforms,UserData.Pts)
        UserData.startpoint = ind(1);
        iClust = UserData.iClust;
        set(h,'UserData',UserData)
        EditWaveforms(iClust)
        h=figure(gcf);
        UserData = get(h,'UserData');
        UserData.currentline = [1 2];
        
    else
        UserData.currentline(1) = find(waveforms(1)==UserData.Pts)
        UserData.currentline(2) = find(waveforms(2)==UserData.Pts)
    end
    linehandle = UserData.linehandle;
    highlight(linehandle(UserData.currentline,1))
end


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.axes11)
[x,y] = ginput(2);
xlim(sort(x))


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.axes11)
axis auto



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton14.
function pushbutton14_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton15.
function pushbutton15_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


