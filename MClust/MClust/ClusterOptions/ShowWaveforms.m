function ShowWaveforms(iClust,varargin)

% ShowWaveforms(iClust)
%
%
% INPUTS
%    wv -- tsd of tt or waveform data
%
% OUTPUTS
%
% NONE
% TO USE WITH MCLUST, put this in the MClust/ClusterOptions folder

% NCST 2002
%
% Status: PROMOTED (Release version) 
% See documentation for copyright (owned by original authors) and warranties (none!).
% This code released as part of MClust 3.0.
% Version control M3.0.
% Extensively modified by ADR to accomodate new ClusterOptions methodology
% Modififed by YSC to allow overplotting of all waveforms.

global MClust_TTData MClust_Clusters MClust_FeatureData
global MClust_Colors
        
[f MClust_Clusters{iClust}] = FindInCluster(MClust_Clusters{iClust});
if length(f) == 0
	msgbox('No points in cluster.')
	return
end
[clustTT was_scaled] = ExtractCluster(MClust_TTData, f); 
if was_scaled == 0
	nSpikes = size(Data(clustTT),1);
else
	nSpikes = was_scaled;
end
DoPlotYN= 'yes';
WV = clustTT;
Color = MClust_Colors(iClust + 1,:);

% added options to allow overplotting of all waveforms in selected clusters
PlotAllYN = 'no';
NewFigure = 'yes';

extract_varargin;

WVD = Data(WV);

% mWV = zeros(4,size(WVD,3));
% sWV = zeros(4,size(WVD,3));

mWV = squeeze(mean(WVD,1));
sWV = squeeze(std(WVD,1));

totalwaveforms = length(WVD(:,1,1));
if ( (totalwaveforms > 1000) && (strcmp(PlotAllYN,'no')) )
    maxPts = 1000;
    Pts = randperm(size(WVD,1));
    Pts = Pts(1:1000);
else
    maxPts = size(WVD,1);
    Pts = 1:maxPts;
end

if nargout == 0 || strcmp(DoPlotYN,'yes')  % no output args, plot it
    if ( strcmp(DoPlotYN,'yes') & strcmp(NewFigure,'yes') )
        ShowWVFig = figure;
    end
    if isempty(find(mWV(2:4,:))) 					%BG
        cha=1;										%BG
    elseif isempty(find(mWV(3:4,:)))				%BG
        cha=2;										%BG
    else											%BG
        cha=size(mWV,1);										%BG
    end												%BG
    
    % tic
    wavepts = size(WVD,3);
    % create x values with a gap between channels
    wp1 = wavepts + 2;
    xr1 = reshape(1:(cha*wp1),wp1,[]);
    xr2 = xr1(1:wavepts,:);
    xv1 = repmat(xr2,maxPts,1);
    xv2 = reshape(xv1,wavepts,[]);
    wvd2 = squeeze(permute(WVD(1:maxPts,1:cha,:),[3 1 2]));
    wvd3 = reshape(wvd2,wavepts,[]);
    plot(xv2,wvd3,'Color',Color);
    hold on
    plot(xr2,mWV','w');
    hold off
    % toc % new code is 13 times faster for polytrodes!
    
%     tic
%     for it = 1:cha									%BG
%         xrange = ((size(WVD,3) + 2) * (it-1)) + (1:size(WVD,3)); 
%         if ( strcmp(DoPlotYN,'yes') & strcmp(NewFigure,'yes') ) 
%             figure(ShowWVFig);
%         end
%         hold on;
%         plot(xrange,squeeze(WVD(Pts,it,:))','color',Color);
%         plot(xrange, mWV(it,:),'w');
%     end
%     toc
    
    %axis off										%BG
    %axis([0 4*(size(WVD,3) + 2) -2100 2100])		%BG
    set(gca,'XTick',[])		                        %BG
    set(gca,'Ygrid','on')	                        %BG
    set(gca,'Color','none')	                        %BG
    title(['Waveforms -- ' num2str(maxPts) ' of ' num2str(totalwaveforms) ' waveforms shown']);
    hold off
end