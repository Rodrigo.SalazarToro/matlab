function MCC = PreCut(data, flag)

% MCC = PreCut(data, flag)
% Already cut Cluster 
%
% Status: PROMOTED (Release version) 
% See documentation for copyright (owned by original authors) and warranties (none!).
% This code released as part of MClust 3.0.
% Version control M3.0.

switch nargin
   
case 0
   MCC.idx = [];

case 2
   switch flag
   case 'indices',
      MCC.idx = data;
  otherwise
      error('Unknown flag.');
  end

otherwise
   error('Unknown number of arguments');
end

MCC = class(MCC, 'precut');