function AddUnaccountedPoints

global MClust_ClusterFileNames MClust_FeatureData MClust_Clusters

%create new cluster
CCWh = findobj('Tag','ClusterCutWindow');
if ~isempty(CCWh)
    cboHandle = findobj(CCWh,'Tag','Add Cluster');
    MClust_ClusterFileNames{end + 1} = 'UnaccountedPoints';
    GeneralizedCutterCallbacks(cboHandle)
else
    errordlg('can''t find ClusterCutWindow')
end

%get unaccountedpoints
MClust_ClusterIndex = ProcessClusters(MClust_FeatureData, MClust_Clusters);
f = find(MClust_ClusterIndex == 0);

%put in new cluster
MClust_Clusters{end} = add_Points(MClust_Clusters{end},f);
