function superpositions = SortLatencies(wave,superpositions)
%superpositions = SortLatencies(wave,superpositions)
%
%Function to rank the match between an actual waveform and every
%possible overlap.  This done by simply calculating the Least Squares
%Difference betwwen each possible overlap and the actual waveform.  The
%correlation coeffecient is calculated between the best match and the actual
%waveform to be used as a score.
%
%wave - real waveform
%superpositions - structure containing all possible overlaps

channels=0;
for it=1:4
    xrange = (32 * (it-1)) + (1:32);
    if find(wave(xrange))
        channels=channels+1;
    end
end

sc=zeros(size(superpositions.waveform,1),1);
wave = repmat(wave,size(superpositions.waveform,1),1);
sw=superpositions.waveform;
%then calculate score for each channel
for it=1:channels
    xrange = (32 * (it-1)) + (1:32); 
    sc(:,it) = sum(((sw(:,xrange)-wave(:,xrange)).^2),2);
end
sc=round(sqrt(sc));
[y,ind] = sort(sum(sc,2));


 %bookkeeping
 superpositions.waveform = superpositions.waveform(ind,:);
 superpositions.clus1 = superpositions.clus1(ind);
 superpositions.clus2 = superpositions.clus2(ind);
 superpositions.trigger = superpositions.trigger(ind);
 superpositions.shift1 = superpositions.shift1(ind);
 superpositions.shift2 = superpositions.shift2(ind);
 superpositions.score = y;
 
 %%%%%%%%%%%%%%%PValue-shape
 [r,p] = corrcoef([wave(1,:)' superpositions.waveform(1,:)']);
 sc= r(1,2);
 superpositions.R(1) = sc;
     g = normpdf(1:128,11,2) + normpdf(1:128,11+32,2) + normpdf(1:128,64+11,2) + normpdf(1:128,96+11,2);
 [r,p] = corrcoef([transpose(wave(1,:).*g) transpose(superpositions.waveform(1,:).*g)]);
 WR = r(1,2);
 superpositions.WR(1) = WR;
 
 %%%%%%%%%%%%%%???? - amplitude
 %normalize to largest abs value for each channel 
for it=1:channels
    xrange = (32 * (it-1)) + (1:32); 
    mx = max([abs(wave(1,xrange)) abs(superpositions.waveform(1,xrange))]);
    w(it,:) = wave(1,xrange)/mx;
    s(it,:) = superpositions.waveform(1,xrange)/mx;
end
 %normalize sum products
 nsp = sum(sum(w.*s));
superpositions.NSP(1) = nsp;
%try gaussain
g = repmat(normpdf(1:32,11,2),channels,1);
WSP = sum(sum(w.*s.*g));
superpositions.WSP(1) = WSP;

