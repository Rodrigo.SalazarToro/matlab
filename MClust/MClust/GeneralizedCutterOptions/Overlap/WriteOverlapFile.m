function WriteOverlapFile(fn, overlap)
%
% WriteOverlapFile(fn, overlap)
%
%MClust writes a -1 in the cut file if an overlap occurs.
%But since we do not want to throw away this information, we write this
%file instead.  It contains a sparse matrix (#timestamps x #clusters) with 
%zeros except where an overlap occurs
%timestamp written here instead
%
global MClust_FeatureTimestamps


lt = length(MClust_FeatureTimestamps);
t = spdiags(MClust_FeatureTimestamps,0,lt,lt);
overlap = sparse(t*overlap);

save(fn,'overlap')
