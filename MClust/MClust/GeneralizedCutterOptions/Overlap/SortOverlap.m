function SortOverlaps(templates,overlaps,threshold,guiflag)

global MClust_TTData MClust_Clusters MClust_FeatureData
global MClust_xlbls
global MClust_ylbls
global MClust_Colors

numClusters = length(MClust_Clusters);

if guiflag
    h=overlapGUI;
    hout = findobj(h,'Tag','OutputBox');
    set(hout,'String',templates)
else    %create mirror clusters
    global MClust_ClusterFileNames
    cboHandle = findobj(gcbf,'Tag','Add Cluster');
    for ii=1:length(templates)
        MClust_ClusterFileNames{end + 1} = ['Overlap ' num2str(templates(ii))];
        GeneralizedCutterCallbacks(cboHandle)
    end
end

for i=1:length(templates)
    %which spikes in this cluster?
    f = FindInCluster(MClust_Clusters{templates(i)}, MClust_FeatureData);
    %get data of the spikes in this cluster
    [clustTT was_scaled] = ExtractCluster(MClust_TTData, f); 
    WVD = Data(clustTT);    %waveform data for this cluster
    WVD = permute(WVD,[3 2 1]);
    WVD = transpose(reshape(WVD,[128 size(WVD,3)]));
    mWV(i,:) = mean(WVD);
    %cWV(i,:) = diag(cov(WVD))';
end

%get all of the pairwise cluster overlaps at all latencies
superpositions = TemplateOverlap(mWV);
%superpositions.cov = cWV;

wvd=[];
for kk=1:length(overlaps)
    iClust = overlaps(kk);
    if ~guiflag
        string = ['Analyzing Cluster ' num2str(iClust)  ' please wait...'];
        hw = waitbar(0,string);
    end
    %get waveforms for overlap cluster
    %which spikes in this cluster?
    f = FindInCluster(MClust_Clusters{iClust}, MClust_FeatureData);
    %get data of the spikes in this cluster
    [clustTT was_scaled] = ExtractCluster(MClust_TTData, f); 
    
    WVD = Data(clustTT);    %waveform data for this cluster
    WVD = permute(WVD,[3 2 1]);
    WVD = transpose(reshape(WVD,[128 size(WVD,3)]));
    
    if guiflag
        wvd = [wvd ; WVD];               
    else
        for ii=1:size(WVD,1)
            Superpositions = SortLatencies(WVD(ii,:),superpositions);   %sort 
            out_cluster = ParseWave(1,Superpositions,threshold,WVD(ii,:));%ParseWave(1,Superpositions);        %parse best(1) match to out
            for jj = 1:length(out_cluster)                              %put in new cluster(s)
                MClust_Clusters{numClusters + out_cluster(jj)} = add_Points(MClust_Clusters{numClusters + out_cluster(jj)},f(ii));
            end
            waitbar(ii/size(WVD,1),hw)
        end
        close(hw)
    end
end %loop over overlaps


if guiflag
    UserData.WVD= wvd;                          %overlaps
    UserData.mWV=mWV;                           %templates
    UserData.CurrentWave=1;
    UserData.CurrentLatency=1;
    UserData.Superpositions=superpositions;     %templates
    UserData.Threshold = threshold;
    %UserData.covariance = cWV;
    set(h,'UserData',UserData) 
end
