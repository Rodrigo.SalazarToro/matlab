OverLap ReadMe

The SortOverlap programs allows the comparison of multiple clusters
for the determination of cases of superpositions.  



SETUP
The user first determines the template clusters and the overlap clusters.
It helps to use all the clean clusters as templates and not just the large
signal to noise ratio clusters as templates.  The program then calculates 
all possible superpositions of all template clusters at all latencies.  Each actual 
waveform is compared with all possible superpositions by the least squares 
difference.  A correlation coefficent is then calculated for the best match.
The overlap waveform is then added to one or both of the nearest template clusters 
based on a refractory period rule.  

USE




OUTPUT
Since MClust currently writes a -1 in the .cut file if a waveform belongs
to multiple clusters, a seperate '*_overlap.mat' output file is written.  The 
overlap file contains a sparse matrix (#timestamps x #clusters) with 
zeros except where an overlap occurs.  The timestamp is written here instead.




RESTRICTIONS
32 points
+/- 3 point refractory period
Works better with tetrodes




ADDITIONS

