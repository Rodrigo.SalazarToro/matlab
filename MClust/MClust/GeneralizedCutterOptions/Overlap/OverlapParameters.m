function varargout = OverlapParameters(varargin)
% OVERLAPPARAMETERS M-file for OverlapParameters.fig
%      OVERLAPPARAMETERS, by itself, creates a new OVERLAPPARAMETERS or raises the existing
%      singleton*.
%
%      H = OVERLAPPARAMETERS returns the handle to a new OVERLAPPARAMETERS or the handle to
%      the existing singleton*.
%
%      OVERLAPPARAMETERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OVERLAPPARAMETERS.M with the given input arguments.
%
%      OVERLAPPARAMETERS('Property','Value',...) creates a new OVERLAPPARAMETERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OverlapParameters_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OverlapParameters_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OverlapParameters

% Last Modified by GUIDE v2.5 07-Apr-2005 08:30:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @OverlapParameters_OpeningFcn, ...
    'gui_OutputFcn',  @OverlapParameters_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before OverlapParameters is made visible.
function OverlapParameters_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OverlapParameters (see VARARGIN)

% Choose default command line output for OverlapParameters
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OverlapParameters wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OverlapParameters_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in ProcessSorting.
function ProcessSorting_Callback(hObject, eventdata, handles)
% hObject    handle to ProcessSorting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MClust_TTData MClust_Clusters MClust_FeatureData
global MClust_ClusterFileNames

numClusters = length(MClust_Clusters);

templates  = get(handles.TemplateListbox,'Value');
overlaps = get(handles.OverlapListbox,'Value');

threshold.SD = str2num(get(handles.SD,'String'));
threshold.R = str2num(get(handles.R,'String'));
threshold.WR = str2num(get(handles.WR,'String'));
threshold.NSP = str2num(get(handles.NSP,'String'));
threshold.WSP = str2num(get(handles.WSP,'String'));

%create mirror clusters
CCWh = findobj('Tag','ClusterCutWindow');
if ~isempty(CCWh)
    cboHandle = findobj(CCWh,'Tag','Add Cluster');
    for ii=1:length(templates)
        MClust_ClusterFileNames{end + 1} = ['Overlap ' num2str(templates(ii))];
        GeneralizedCutterCallbacks(cboHandle)
    end
else
    errordlg('can''t find ClusterCutWindow')
end

UserData = get(gcbf,'UserData');
if isempty(UserData)
   [Superpositions,mWV] = GetTemplateData(templates);
else
    Superpositions = UserData.Superpositions;
end


for kk=1:length(overlaps)
    iClust = overlaps(kk);
    string = ['Analyzing Cluster ' num2str(iClust)  ' please wait...'];
    hw = waitbar(0,string);
    %get waveforms for overlap cluster
    f = FindInCluster(MClust_Clusters{iClust}, MClust_FeatureData);
    %get data of the spikes in this cluster
    clustTT = ExtractClusterALL(MClust_TTData, f);

    WVD = Data(clustTT);    %waveform data for this cluster
    WVD = permute(WVD,[3 2 1]);
    WVD = transpose(reshape(WVD,[128 size(WVD,3)]));

    for ii=1:size(WVD,1)
        Superpositions = SortLatencies(WVD(ii,:),Superpositions);   %sort
        out_cluster = ParseWave(1,Superpositions,threshold,WVD(ii,:));        %parse best(1) match to out
        for jj = 1:length(out_cluster)                              %put in new cluster(s)
            MClust_Clusters{numClusters + out_cluster(jj)} = add_Points(MClust_Clusters{numClusters + out_cluster(jj)},f(ii));
        end
        waitbar(ii/size(WVD,1),hw)
    end
    close(hw)
end %loop over overlaps

close(gcbf)
close(findobj('Name','Thresholds'))



% --- Executes on button press in ShowHist.
function ShowHist_Callback(hObject, eventdata, handles)
% hObject    handle to ShowHist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
templates  = get(handles.TemplateListbox,'Value');
overlaps = get(handles.OverlapListbox,'Value');

UserData = get(gcbf,'UserData');
if isempty(UserData)
    [Superpositions,mWV] = GetTemplateData(templates);
    WVD = GetOverlapData(overlaps);
    
    %set this info to UserData so it does not have to be calculated again.
    UserData.Superpositions = Superpositions;
    UserData.WVD=WVD;
    UserData.mWV=mWV;
    set(gcbf,'UserData',UserData)
else
    Superpositions = UserData.Superpositions;
    WVD = UserData.WVD;
    mWV = UserData.mWV;
end

CW=1;
score=[];
nsp=[];
gnsp=[];
corrcoefs=[];
corrcoefsg=[];
hw = waitbar(0,'Please wait...');
while CW<size(WVD,1)
    Superpositions = SortLatencies(WVD(CW,:),Superpositions);
    score = [score; Superpositions.score(1)];
    corrcoefs = [corrcoefs Superpositions.R(1)];
    corrcoefsg = [corrcoefsg Superpositions.WR(1)];
    nsp = [nsp Superpositions.NSP(1)];
    gnsp = [gnsp Superpositions.WSP(1)];
    CW = CW+1;
    waitbar(CW/size(WVD,1),hw)
end
close(hw)
h= figure;
subplot(4,2,1)
hist(score,80)
title('Least Squares')
subplot(4,2,2)
hist(corrcoefs,80)
title('Correlation Coeffecients')
subplot(4,2,3)
hist(corrcoefsg,80)
title('Wieghted Correlation Coeffecients')    
subplot(4,2,4)
hist(nsp,80)
title('Normalized Sum of Products')
subplot(4,2,5)
hist(gnsp,80)
title('Wieghted NSP')
subplot(4,2,6)    
plot(nsp,corrcoefs,'.');
xlabel('nsp');ylabel('R');
subplot(4,2,7)
plot(gnsp,corrcoefs,'.')
xlabel('wsp');ylabel('R');
set(h,'Name','Thresholds')

% --- Executes on button press in TemplateAll.
function TemplateAll_Callback(hObject, eventdata, handles)
% hObject    handle to TemplateAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.TemplateListbox,'value',1:length(get(handles.TemplateListbox,'String')));

% --- Executes on button press in OverlapAll.
function OverlapAll_Callback(hObject, eventdata, handles)
% hObject    handle to OverlapAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.OverlapListbox,'value',1:length(get(handles.OverlapListbox,'String')));


% --- Executes on button press in InspectSorting.
function InspectSorting_Callback(hObject, eventdata, handles)
% hObject    handle to InspectSorting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
templates  = get(handles.TemplateListbox,'Value');
overlaps = get(handles.OverlapListbox,'Value');

threshold.SD = str2num(get(handles.SD,'String'));
threshold.R = str2num(get(handles.R,'String'));
threshold.WR = str2num(get(handles.WR,'String'));
threshold.NSP = str2num(get(handles.NSP,'String'));
threshold.WSP = str2num(get(handles.WSP,'String'));

UserData = get(gcbf,'UserData');
if isempty(UserData)
    [Superpositions,mWV] = GetTemplateData(templates);
    WVD = GetOverlapData(overlaps);
    %set this info to UserData so it does not have to be calculated again.
    UserData.Superpositions = Superpositions;
    UserData.WVD=WVD;
    UserData.mWV=mWV;
    set(gcbf,'UserData',UserData)
else
    Superpositions = UserData.Superpositions;
    WVD = UserData.WVD;
    mWV = UserData.mWV;
end


h=overlapGUI;
hout = findobj(h,'Tag','OutputBox');
set(hout,'String',templates)
UserData.WVD= WVD;                          %overlaps
UserData.mWV=mWV;                           %templates
UserData.CurrentWave=1;
UserData.CurrentLatency=1;
UserData.Superpositions=Superpositions;     %templates
UserData.Threshold = threshold;
%UserData.covariance = cWV;
set(h,'UserData',UserData) 
hSDt = findobj(h,'Tag','SDthreshold');
set(hSDt,'String',num2str(UserData.Threshold.SD))
hRt = findobj(h,'Tag','Rthreshold');
set(hRt,'String',num2str(UserData.Threshold.R))
hNSPt = findobj(h,'Tag','NSPthreshold');
set(hNSPt,'String',num2str(UserData.Threshold.NSP))
hWRt = findobj(h,'Tag','WRthreshold');
set(hWRt,'String',num2str(UserData.Threshold.WR))
hWSPt = findobj(h,'Tag','WSPthreshold');
set(hWSPt,'String',num2str(UserData.Threshold.WSP))


% --- Executes during object creation, after setting all properties.
function WR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function WR_Callback(hObject, eventdata, handles)
% hObject    handle to WR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of WR as text
%        str2double(get(hObject,'String')) returns contents of WR as a double


% --- Executes on button press in ShoiwSurfaces.
function ShoiwSurfaces_Callback(hObject, eventdata, handles)
% hObject    handle to ShoiwSurfaces (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShoiwSurfaces


% --- Executes on button press in ScalingTemplates.
function ScalingTemplates_Callback(hObject, eventdata, handles)
% hObject    handle to ScalingTemplates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ScalingTemplates

global Scaling;
Scaling.Template = get(hObject,'Value');
Scaling.Min = str2num(get(handles.MinScaling,'String'));
Scaling.Max = str2num(get(handles.MaxScaling,'String'));
Scaling.Step = str2num(get(handles.StepScaling,'String'));


