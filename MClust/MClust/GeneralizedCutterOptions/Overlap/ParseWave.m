function out_cluster = ParseWave(CL,Superpositions,threshold,realwaveform)
%out_cluster = ParseWave(CL,Superpositions,threshold)
%
%this function decides if the best overlap match is good enough
%(below the threshold) and if the overlapped waveform should be
%put into both clusters or just the nearest (in time) match.
%The assumption is that a refractory period of +/- 3 datapoints was
%used in the extraction routine.  So if the best match contains two
%waveforms with a latency larger than this refractory period then the
%composite waveform would have been extracted at a different
%spiketime as well.
%
%
%CL -- curent latency
%Superpositions -- structure containing ranked superpositions from
%                  SortLatencies and TemplateOverlap.
%threshold -- specifies the score threshold


%if score is good (above threshold)
%if shift 1 - shift2 <=3
%put into both clusters
%else put into cluster that has smallest abs(shift+11-triggerpt)

% Rodrigo (03.03.05): Improvements for undetected spikes due to below
% threshold or too close to each other but beyond 3 data points

if ~exist('threshold','var')
    threshold.SD=inf;
    threshold.R=0;
    threshold.WR=0;
    threshold.NSP=0;
    threshold.WSP=0;
end

if CL==1
    sS = Superpositions.score(CL,:);
    sR = Superpositions.R(CL,:);
    sWR = Superpositions.WR(CL,:);
    sNSP = Superpositions.NSP(CL,:);
    sWSP = Superpositions.WSP(CL,:);
else
    sS = Superpositions.score(CL,:);
    sR = inf;
    sWR = inf;
    sNSP = inf;
    sWSP = inf;
end


if  sS < threshold.SD ...
        & sR > threshold.R ...
        & sWR > threshold.WR ...
        & sNSP > threshold.NSP ...
        & sWSP > threshold.WSP
    %%%%%%%%%%%%%%%%%%%%%%%%%%% Solution for undetected spikes in overlaps

    clear newwave times NotDetected means threholds
    global MClust_fn
    ind = strfind(lower(MClust_fn),'g00');
    sessiongroupname = MClust_fn(1:ind+4);
    group = MClust_fn(ind+1:ind+4);% To get the group name we are working on

    directories = pwd;
    k = strfind(directories,'sort');% to get the directory

    filename = sprintf('%s%sWaveformsHeader_g%s',directories(1:k+3),filesep,group); % to set teh file name to load


%     [max_duration,min_duration, trials,waves,rawfile,fs,channels,means,thresholds,numChunks,chunkSize] = nptReadSorterHdr(filename); % gets teh thresholds use during the extraction of spikes from the raw data
% 
%     if size(means,1) > 1 % means and thresholds have to be put as a row vector for the nptExtructor fct thus...
%         means = means';
%         thresholds = thresholds';
%     end

    x1 = (21+Superpositions.shift1(CL)-Superpositions.trigger(CL)); % teh x value of the template 1
    x2 = (21+Superpositions.shift2(CL)-Superpositions.trigger(CL)); % the x value of the template 2

    if 0<x1 & x1<=32 & 0<x2 & x2<=32 % if the x value are within the values of the overlap waveforms

        for i = 1 : 4

            if size(realwaveform,2) == 128
                newwave(i,:) = realwaveform(1+32*(i-1):32+32*(i-1)); % Superpositions.waveform(CL,1+32*(i-1):32+32*(i-1)); % reshape into the format of tetrode
            else % this else is if the user changes the latencies it cannot recover the real waveform?????
                newwave(i,:) = Superpositions.waveform(CL,1+32*(i-1):32+32*(i-1));
            end
        end
        
        add = zeros(4,32);% just some zeros to pad 
        newwave = [add newwave add]; % zero padded the data to run it again through the extraction procedure
%         [waveforms,times]=nptExtructor(newwave,means(1:4),thresholds(1:4),30000); % Extract the spikes

%         if length(times) == 1 % the logic is that we know that there are 2 spikes inthe overlaps (see below if statement) thus there should be two spikes extracted through the extraction and if not then size(times,1) == 1
            NotDetected = 1;
%         else
%             NotDetected = 0;
%         end

    else
        NotDetected = 0;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%% 

    if NotDetected % it means tha a real spike is missing (Salazar, 03.02.05) Previously  abs(Superpositions.shift1(CL)- Superpositions.shift2(CL)) > 3
        out_cluster = [(Superpositions.clus1(CL)) Superpositions.clus2(CL)];
    else
        match1 = Superpositions.shift1(CL)+11-Superpositions.trigger(CL);
        match2 = Superpositions.shift2(CL)+11-Superpositions.trigger(CL);
        if abs(match1)<abs(match2)
            out_cluster = [Superpositions.clus1(CL)];
        else
            out_cluster = [Superpositions.clus2(CL)];
        end
    end
else
    out_cluster=[];
end