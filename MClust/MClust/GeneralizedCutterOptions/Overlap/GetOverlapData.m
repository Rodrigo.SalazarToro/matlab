function WVD = GetOverlapData(overlaps)
%WVD = GetOverlapData(overlaps)
%
%returns only the waveforms from the overlap clusters.



global MClust_TTData MClust_Clusters MClust_FeatureData

WVD=[];
for kk=1:length(overlaps)
    iClust = overlaps(kk);
    %get waveforms for overlap cluster
    %which spikes in this cluster?
    f = FindInCluster(MClust_Clusters{iClust}, MClust_FeatureData);
    %get data of the spikes in this cluster
    clustTT  = ExtractClusterALL(MClust_TTData, f); 
    
    wvd = Data(clustTT);    %waveform data for this cluster
    wvd = permute(wvd,[3 2 1]);
    wvd = transpose(reshape(wvd,[128 size(wvd,3)]));
    WVD = [WVD ; wvd];
end %loop over overlaps
