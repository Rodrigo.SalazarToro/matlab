function [superpositions,mWV,WVD] = GetTemplateData(templates)
%[superpositions,mWV,WVD] = GetTemplateData(templates)
%
%returns the superpositions and mean waveforms

global MClust_TTData MClust_Clusters MClust_FeatureData

for i=1:length(templates)
    %which spikes in this cluster?
    f = FindInCluster(MClust_Clusters{templates(i)}, MClust_FeatureData);
    %get data of the spikes in this cluster
    [clustTT was_scaled] = ExtractCluster(MClust_TTData, f); 
    WVD = Data(clustTT);    %waveform data for this cluster
    WVD = permute(WVD,[3 2 1]);
    WVD = transpose(reshape(WVD,[128 size(WVD,3)]));
    mWV(i,:) = mean(WVD);
    %cWV(i,:) = diag(cov(WVD))';
end

%get all of the pairwise cluster overlaps at all latencies
superpositions = TemplateOverlap(mWV);
%superpositions.cov = cWV;
