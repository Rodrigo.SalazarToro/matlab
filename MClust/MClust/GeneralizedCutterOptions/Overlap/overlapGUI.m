function varargout = overlapGUI(varargin)
% OVERLAPGUI M-file for overlapGUI.fig
%      OVERLAPGUI, by itself, creates a new OVERLAPGUI or raises the existing
%      singleton*.
%
%      H = OVERLAPGUI returns the handle to a new OVERLAPGUI or the handle to
%      the existing singleton*.
%
%      OVERLAPGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OVERLAPGUI.M with the given input arguments.
%
%      OVERLAPGUI('Property','Value',...) creates a new OVERLAPGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before overlapGUI_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to overlapGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help overlapGUI

% Last Modified by GUIDE v2.5 28-Oct-2003 11:55:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @overlapGUI_OpeningFcn, ...
    'gui_OutputFcn',  @overlapGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before overlapGUI is made visible.
function overlapGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to overlapGUI (see VARARGIN)

% Choose default command line output for overlapGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes overlapGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = overlapGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in NextWave.
function NextWave_Callback(hObject, eventdata, handles)
% hObject    handle to NextWave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MClust_Colors
checkH  = findobj(gcbf,'Tag','DetailDisplay');
set(checkH,'Value',0)

UserData = get(gcbf,'UserData');
CL = 1;
CW = UserData.CurrentWave;

if UserData.CurrentWave<size(UserData.WVD,1)
    CW = CW+1;
    Superpositions = SortLatencies(UserData.WVD(CW,:),UserData.Superpositions);
    plotOverlap(CW,CL,UserData,Superpositions);
    
    out_cluster = ParseWave(CL,Superpositions,UserData.Threshold,UserData.WVD(CW,:));
    
    %update OutBox
    hout = findobj(gcbf,'Tag','OutputBox');
    set(hout,'Value',out_cluster)
    
    %update template axis
    haxes = findobj(gcbf,'Tag','axes1');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus1(CL),:),'Color',MClust_Colors(Superpositions.clus1(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes1');
    title(['Cluster ' num2str(Superpositions.clus1(CL))])
    if ~ismember(Superpositions.clus1(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    haxes = findobj(gcbf,'Tag','axes2');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus2(CL),:),'Color',MClust_Colors(Superpositions.clus2(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes2');
    title(['Cluster ' num2str(Superpositions.clus2(CL))])
    if ~ismember(Superpositions.clus2(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
        
    UserData.CurrentWave = CW;
    UserData.CurrentLatency = CL;
    UserData.Superpositions = Superpositions;
    set(gcbf,'UserData',UserData)
    hSDs = findobj(gcbf,'Tag','SDscore');
    set(hSDs,'String',num2str(Superpositions.score(CL)))
    hRs = findobj(gcbf,'Tag','Rscore');
    set(hRs,'String',num2str(Superpositions.R(CL)))
    hNSPs = findobj(gcbf,'Tag','NSPscore');
    set(hNSPs,'String',num2str(Superpositions.NSP(CL)))
    hWRs = findobj(gcbf,'Tag','WRscore');
    set(hWRs,'String',num2str(Superpositions.WR(CL)))
    hWSPs = findobj(gcbf,'Tag','WSPscore');
    set(hWSPs,'String',num2str(Superpositions.WSP(CL)))
end %less than size


% --- Executes on button press in PreviousWave.
function PreviousWave_Callback(hObject, eventdata, handles)
% hObject    handle to PreviousWave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MClust_Colors
checkH  = findobj(gcbf,'Tag','DetailDisplay');
set(checkH,'Value',0)

UserData = get(gcbf,'UserData');
CL = 1;
CW = UserData.CurrentWave;

if UserData.CurrentWave>1
    CW = CW-1;
    
    Superpositions = SortLatencies(UserData.WVD(CW,:),UserData.Superpositions);
    plotOverlap(CW,CL,UserData,Superpositions);
    
    out_cluster = ParseWave(CL,Superpositions,UserData.Threshold,UserData.WVD(CW,:));
    %update OutBox
    hout = findobj(gcbf,'Tag','OutputBox');
    set(hout,'Value',out_cluster)
    
    %update template axis
    haxes = findobj(gcbf,'Tag','axes1');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus1(CL),:),'Color',MClust_Colors(Superpositions.clus1(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes1');
    title(['Cluster ' num2str(Superpositions.clus1(CL))])
    if ~ismember(Superpositions.clus1(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    haxes = findobj(gcbf,'Tag','axes2');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus2(CL),:),'Color',MClust_Colors(Superpositions.clus2(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes2');
    title(['Cluster ' num2str(Superpositions.clus2(CL))])
    if ~ismember(Superpositions.clus2(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    
   UserData.CurrentWave = CW;
    UserData.CurrentLatency = CL;
    UserData.Superpositions = Superpositions;
    set(gcbf,'UserData',UserData)
    hSD = findobj(gcbf,'Tag','SDscore');
    set(hSD,'String',num2str(Superpositions.score(CL)))
    hR = findobj(gcbf,'Tag','Rscore');
    set(hR,'String',num2str(Superpositions.R(CL)))
    hNSP = findobj(gcbf,'Tag','NSPscore');
    set(hNSP,'String',num2str(Superpositions.NSP(CL)))
    hWR = findobj(gcbf,'Tag','WRscore');
    set(hWR,'String',num2str(Superpositions.WR(CL)))
    hWSP = findobj(gcbf,'Tag','WSPscore');
    set(hWSP,'String',num2str(Superpositions.WSP(CL)))
end 


% --- Executes on button press in NextLatency.
function NextLatency_Callback(hObject, eventdata, handles)
% hObject    handle to NextLatency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MClust_Colors
checkH  = findobj(gcbf,'Tag','DetailDisplay');
set(checkH,'Value',0)

UserData = get(gcbf,'UserData');
CL = UserData.CurrentLatency;
CW = UserData.CurrentWave;

if CL<size(UserData.Superpositions.waveform,1)
    CL = CL + 1;
    
    Superpositions = UserData.Superpositions;
    plotOverlap(CW,CL,UserData,Superpositions);
    
    
    out_cluster = ParseWave(CL,Superpositions,UserData.Threshold,CW);
    
    %update OutBox
    hout = findobj(gcbf,'Tag','OutputBox');
    set(hout,'Value',out_cluster)
        
    %update template axis
    haxes = findobj(gcbf,'Tag','axes1');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus1(CL),:),'Color',MClust_Colors(Superpositions.clus1(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes1');
    title(['Cluster ' num2str(Superpositions.clus1(CL))])
    if ~ismember(Superpositions.clus1(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    haxes = findobj(gcbf,'Tag','axes2');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus2(CL),:),'Color',MClust_Colors(Superpositions.clus2(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes2');
    title(['Cluster ' num2str(Superpositions.clus2(CL))])
    if ~ismember(Superpositions.clus2(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    
    UserData.CurrentWave = CW;
    UserData.CurrentLatency = CL;
    UserData.Superpositions = Superpositions;
    set(gcbf,'UserData',UserData)
    hSD = findobj(gcbf,'Tag','SDscore');
    set(hSD,'String',num2str(Superpositions.score(CL)))
    hR = findobj(gcbf,'Tag','Rscore');
    set(hR,'String','')
    hNSP = findobj(gcbf,'Tag','NSPscore');
    set(hNSP,'String','')
    hWR = findobj(gcbf,'Tag','WRscore');
    set(hWR,'String','')
    hWSP = findobj(gcbf,'Tag','WSPscore');
    set(hWSP,'String','')
end %less than size


% --- Executes on button press in PreviousLatency.
function PreviousLatency_Callback(hObject, eventdata, handles)
% hObject    handle to PreviousLatency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MClust_Colors
checkH  = findobj(gcbf,'Tag','DetailDisplay');
set(checkH,'Value',0)

UserData = get(gcbf,'UserData');
CL = UserData.CurrentLatency;
CW = UserData.CurrentWave;

if CL>1
    CL = CL-1;
    
    Superpositions = UserData.Superpositions;
    plotOverlap(CW,CL,UserData,Superpositions);
    
    out_cluster = ParseWave(CL,Superpositions,UserData.Threshold,CW);
    
    %update OutBox
    hout = findobj(gcbf,'Tag','OutputBox');
    set(hout,'Value',out_cluster)
    
    %update template axis
    haxes = findobj(gcbf,'Tag','axes1');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus1(CL),:),'Color',MClust_Colors(Superpositions.clus1(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes1');
    title(['Cluster ' num2str(Superpositions.clus1(CL))])
    if ~ismember(Superpositions.clus1(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    haxes = findobj(gcbf,'Tag','axes2');
    axes(haxes);
    plot(UserData.mWV(Superpositions.clus2(CL),:),'Color',MClust_Colors(Superpositions.clus2(CL)+1,:),'lineWidth',3)
    set(haxes,'Tag','axes2');
    title(['Cluster ' num2str(Superpositions.clus2(CL))])
    if ~ismember(Superpositions.clus2(CL),out_cluster)
        set(haxes,'Color',[0.501961 0.501961 0.501961])
    end
    
    UserData.CurrentWave = CW;
    UserData.CurrentLatency = CL;
    UserData.Superpositions = Superpositions;
    set(gcbf,'UserData',UserData)
    hSD = findobj(gcbf,'Tag','SDscore');
    set(hSD,'String',num2str(Superpositions.score(CL)))
    hR = findobj(gcbf,'Tag','Rscore');
    set(hR,'String','')
    hNSP = findobj(gcbf,'Tag','NSPscore');
    set(hNSP,'String','')
    hWR = findobj(gcbf,'Tag','WRscore');
    set(hWR,'String','')
    hWSP = findobj(gcbf,'Tag','WSPscore');
    set(hWSP,'String','')
end %less than size


% --- Executes on button press in ShowSurface.
function ShowSurface_Callback(hObject, eventdata, handles)
% hObject    handle to ShowSurface (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

UserData = get(gcbf,'UserData');
Superpositions = UserData.Superpositions;
CW = UserData.CurrentWave;

cluster1 = UserData.mWV(Superpositions.clus1(1),:);
cluster2 = UserData.mWV(Superpositions.clus2(1),:);

%create overlap clusters
zz1=convmtx(cluster1(1:32),63);
zz2=convmtx(cluster1(33:64),63);
zz3=convmtx(cluster1(65:96),63);
zz4=convmtx(cluster1(97:128),63);
zz = [zz1 zz2 zz3 zz4];
% figure
% for ii=1:size(zz1,1)
%     hold on
%     plot(zz1(ii,:))
% end

qq1 = [zeros(1,31) cluster2(1:32) zeros(1,31)];
qq2 = [zeros(1,31) cluster2(33:64) zeros(1,31)];
qq3 = [zeros(1,31) cluster2(65:96) zeros(1,31)];
qq4 = [zeros(1,31) cluster2(97:128) zeros(1,31)];
qq = [qq1 qq2 qq3 qq4];
qq = repmat(qq,63,1);

ss1=zz+qq;
% figure
% surf(ss1)
% figure
ss1= [ss1(:,32:63) ss1(:,126:157) ss1(:,220:251) ss1(:,314:345)];
%surf(ss1)


%create overlap clusters
zz1=convmtx(cluster2(1:32),63);
zz2=convmtx(cluster2(33:64),63);
zz3=convmtx(cluster2(65:96),63);
zz4=convmtx(cluster2(97:128),63);
zz = [zz1 zz2 zz3 zz4];

qq1 = [zeros(1,31) cluster1(1:32) zeros(1,31)];
qq2 = [zeros(1,31) cluster1(33:64) zeros(1,31)];
qq3 = [zeros(1,31) cluster1(65:96) zeros(1,31)];
qq4 = [zeros(1,31) cluster1(97:128) zeros(1,31)];
qq = [qq1 qq2 qq3 qq4];
qq = repmat(qq,63,1);

ss2=zz+qq;
% figure
% surf(ss2)
ss2= [ss2(:,32:63) ss2(:,126:157) ss2(:,220:251) ss2(:,314:345)];
% figure
% surf(ss2)

ss = [ss1;ss2];
 figure
  subplot(1,2,1)
  surf(ss)
   

  colormap('hsv')
  shading('interp')
  grid off
   v =get(gca,'View')
   set(gca,'XLim',[1 128])
   set(gca,'XTick',[1 32  64  96 128])
   set(gca,'XTickLabel',[1 32  32  32  32])
   set(gca,'YLim',[0 size(ss,1)])
   set(gca,'YTick',[0 32 62 67 96 size(ss,1) ]) 
set(gca,'YTickLabel',[-31 0 31 -31 0 31])
 set(gca,'Color',get(gcf,'Color'))
ylabel('Phase')
zlabel('Amplitude')
xlabel('Data points')
wave = repmat(UserData.WVD(CW,:),size(ss,1),1);
for it=1:4
    xrange = (32 * (it-1)) + (1:32); 
    sc(:,it) = sum(((ss(:,xrange)-wave(:,xrange)).^2),2);
end
subplot(1,2,2)
sc=sum(round(sqrt(sc)),2);
plot3(zeros(length(sc)),1:length(sc),sc,'LineWidth',2)
 set(gca,'XLim',[0 1])
 set(gca,'Color',get(gcf,'Color'))
 set(gca,'XColor',get(gcf,'Color'))
 set(gca,'YLim',[0 128]);
 set(gca,'YTick',[0 32 62 67 96 128])
 set(gca,'YTickLabel',[-31 0 31 -31 0 31])
 set(gca,'View',v)
 zlabel('Least Squares Difference')
 ylabel('Phase')

 subplot(1,2,1)
 [v,ind] = min(sc);
 hold on
  plot3([1:128],ind*ones(1,128),ss(ind,:),'LineWidth',3,'Color','k')
plot3([1:128],[1:4:size(ss,1)]'*ones(1,128),ss(1:4:end,:),'LineWidth',.1,'Color','k')





















%     cindex = intersect(find(Superpositions.clus1 == cluster1) , find(Superpositions.clus2 == cluster2));
%     [p,pindex] = sort(Superpositions.shift1(cindex)-Superpositions.shift2(cindex));
% %[pu, puindex]= unique(p);
% ind = cindex(pindex);
% 
% counter=1;  
% b=[];
% while counter<length(p)
% %     figure
%     howmany = length(find(p == p(counter)))
%     if howmany>2
%         warndlg('more than two waveforms extracted for this phase')
%     end
%     if howmany>1
%         %1st half get larger trigger %%2nd half get smaller trigger
%         if counter>.5*length(p)
%             if Superpositions.trigger(ind(counter))>Superpositions.trigger(ind(counter+1))
%                 b=[b ind(counter)];
% %                 plot(Superpositions.waveform(ind(counter),:))
% %                 hold on
% %                 plot(Superpositions.waveform(ind(counter+1),:),'r')
%             else
%                 b=[b ind(counter+1)];
% %                 plot(Superpositions.waveform(ind(counter+1),:))
% %                 hold on
% %                 plot(Superpositions.waveform(ind(counter),:),'r')
%             end
%         end
%         if counter<.5*length(p)
%             if Superpositions.trigger(ind(counter))>Superpositions.trigger(ind(counter+1))
%                 b=[b ind(counter+1)];
% %                 plot(Superpositions.waveform(ind(counter+1),:))
% %                 hold on
% %                 plot(Superpositions.waveform(ind(counter),:),'r')
%             else
%                 b=[b ind(counter)];
% %                 plot(Superpositions.waveform(ind(counter),:))
% %                 hold on
% %                 plot(Superpositions.waveform(ind(counter+1),:),'r')
%             end
%         end
% %         str=['Shift1=' num2str(Superpositions.shift1(ind(counter),:)) '  Shift2=' num2str(Superpositions.shift2(ind(counter),:)) ' Trigger= ' num2str(Superpositions.trigger(ind(counter))) ];
% %         str=[str ' ***  Shift1=' num2str(Superpositions.shift1(ind(counter+1),:)) '  Shift2=' num2str(Superpositions.shift2(ind(counter+1),:)) ' Trigger= ' num2str(Superpositions.trigger(ind(counter+1))) ];
% %         title(str)
%         counter=counter+howmany;
%     else
%         b=[b ind(counter)];
% %          plot(Superpositions.waveform(ind(counter),:))
%         counter=counter+1;
%     end
% end
%     
%         
% 
% 
%     
%    %tindex = find(Superpositions.trigger(cindex) > -inf);
%     
%     figure
%      subplot(1,2,1)
%    surf(Superpositions.waveform(b,:))
%    v =get(gca,'View')
%    set(gca,'XLim',[1 128])
%    set(gca,'XTick',[32 64 96 128])
%    set(gca,'YLim',[0 64])
%    set(gca,'YTick',[0 32 64]) 
% set(gca,'YTickLabel',[-32 0 32])
% ylabel('Phase')
% zlabel('Amplitude')
% xlabel('Data points')
%       %  surf(Superpositions.waveform(cindex,:))
% title(['Superpositions for Cluster ' num2str(cluster1) ' & Cluster ' num2str(cluster2)'])
%      subplot(1,2,2)
%     %figure
% plot3(zeros(length(b)),1:length(b),Superpositions.score(b),'LineWidth',2)
% set(gca,'XLim',[0 1])
% set(gca,'Color',get(gcf,'Color'))
% set(gca,'XColor',get(gcf,'Color'))
% set(gca,'YTick',[0 32 64])
% set(gca,'YTickLabel',[-32 0 32])
%     set(gca,'View',v)
%       %  plot(Superpositions.score(cindex))
% title(['Least Squares Difference'])
%     %subplot(1,3,3)
% %     figure
% %     plot((Superpositions.shift1(b)-Superpositions.shift2(b)),'k.')
%    % title(['Phase for Cluster ' num2str(cluster1) ' & Cluster ' num2str(cluster2) ' ranked by phase (-32:32)'])
%     %end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in DetailDisplay.
function DetailDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to DetailDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DetailDisplay

OAxesBIG = findobj(gcbf,'Tag','OverlapAxesBIG');
for it=1:4
    eval(['OAxes' num2str(it)  '= findobj(gcbf,''Tag'',[''OverlapAxes'' num2str(it)]);'])
end

checkH  = findobj(gcbf,'Tag','DetailDisplay');

if get(checkH,'Value')
    hBIG = allchild(OAxesBIG);
    set([hBIG ; OAxesBIG],'Visible','off')
    hLITTLE = allchild([OAxes1 OAxes2 OAxes3 OAxes4]);
    for k=1:size(hLITTLE,1)
        eval(['set([hLITTLE{k};[OAxes' num2str(k) ']   ],''Visible'',''on'')'])
    end
else
    hBIG = allchild(OAxesBIG);
    set([hBIG ; OAxesBIG],'Visible','on')
    hLITTLE = allchild([OAxes1 OAxes2 OAxes3 OAxes4]);
    for k=1:size(hLITTLE,1)
        eval(['set([hLITTLE{k};[OAxes' num2str(k) ']   ],''Visible'',''off'')'])
    end
end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
% --- Executes on button press in ShowHist.
function ShowHist_Callback(hObject, eventdata, handles)
% hObject    handle to ShowHist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%loop through all waves and create histogram of scores
UserData = get(gcbf,'UserData');

CW=1;
score=[];
nsp=[];
gnsp=[];
corrcoefs=[];
corrcoefsg=[];
hw = waitbar(0,'Please wait...');
while CW<size(UserData.WVD,1)
    Superpositions = SortLatencies(UserData.WVD(CW,:),UserData.Superpositions);
    score = [score; Superpositions.score(1)];
    corrcoefs = [corrcoefs Superpositions.corrcoef(1)];
    corrcoefsg = [corrcoefsg Superpositions.corrcoefg(1)];
    nsp = [nsp Superpositions.nsp(1)];
    gnsp = [gnsp Superpositions.gnsp(1)];
    CW = CW+1;
    waitbar(CW/size(UserData.WVD,1),hw)
end
close(hw)
figure
subplot(4,2,1)
    hist(score,80)
    title('Least Squares')
subplot(4,2,2)
    hist(corrcoefs,80)
    title('Correlation Coeffecients')
subplot(4,2,3)
    hist(corrcoefsg,80)
    title('Gaussain Correlation Coeffecients')    
subplot(4,2,4)
    hist(nsp,80)
    title('Normalized Sum of Products')
subplot(4,2,5)
    hist(gnsp,80)
    title('Gaussian NSP')
subplot(4,2,6)    
    plot(nsp,corrcoefs,'.');
    xlabel('nsp');ylabel('R');
subplot(4,2,7)
    plot(gnsp,corrcoefs,'.')
    xlabel('gnsp');ylabel('R');




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotOverlap(CW,CL,UserData,Superpositions)



OAxesBIG = findobj(gcbf,'Tag','OverlapAxesBIG');
axes(OAxesBIG);
hold off

for it = 1:4
    xrange = (34 * (it-1)) + (1:32); 
    plot(xrange,UserData.WVD(CW,xrange-2*(it-1)),'r','lineWidth',3)
    hold on
    plot(xrange,Superpositions.waveform(CL,xrange-2*(it-1)),'lineWidth',3)
    a=axis;  
    line((it-1)*34+11*[1 1] , [a(3) a(4)],'LineStyle','--','Color','k');
    line((it-1)*34+8*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
    line((it-1)*34+14*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
end
for it=1:4
    a=axis;  
    line((it-1)*34+11*[1 1] , [a(3) a(4)],'LineStyle','--','Color','k');
    line((it-1)*34+8*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
    line((it-1)*34+14*[1 1] , [a(3) a(4)],'LineStyle','--','Color','r');
end
legend('Real Waveform', 'Possible Superposition')
title(['Wave: ' num2str(CW) '   Overlap: ' num2str(CL)  '   Clusters:' num2str(Superpositions.clus1(CL)) ' & ' num2str(Superpositions.clus2(CL)) '    Latency: ' num2str(Superpositions.shift1(CL)-Superpositions.shift2(CL))])
set(OAxesBIG,'Tag','OverlapAxesBIG')



a=[];
for it = 1:4
    
    eval(['OAxes' num2str(it)  '= findobj(gcbf,''Tag'',[''OverlapAxes'' num2str(it)]);'])
    eval(['axes(OAxes' num2str(it) ');'])
    hold off
    xrange1 = (34 * (it-1)) + (1:32); 
    wave1 = UserData.mWV(Superpositions.clus1(1),xrange1-2*(it-1));
    wave2 = UserData.mWV(Superpositions.clus2(1),xrange1-2*(it-1));
    
    xrange3 = (Superpositions.trigger(1) - 10:Superpositions.trigger(1)+21);
    plot(xrange3,Superpositions.waveform(CL,xrange1-2*(it-1)),'b','lineWidth',3)
    hold on
    plot(xrange3,UserData.WVD(CW,xrange1-2*(it-1)),'r','lineWidth',3)
    xrange4 = Superpositions.shift1(CL):Superpositions.shift1(CL)+31;
    xrange5 = Superpositions.shift2(CL):Superpositions.shift2(CL)+31;
    plot(xrange4,wave1,'lineWidth',.5,'Color','cy')
    plot(xrange5,wave2,'lineWidth',.5,'Color','k')
    
    zoom on
    a(it,:)=axis;
    eval(['set(OAxes' num2str(it) ',''Tag'',[''OverlapAxes'' num2str(it)]);'])
    
end
for it=1:4
    eval(['OAxes' num2str(it)  '= findobj(gcbf,''Tag'',[''OverlapAxes'' num2str(it)]);'])
    eval(['axes(OAxes' num2str(it) ');'])
    anew = [0 64 min(a(:,3)) max(a(:,4))]; 
    axis(anew)
    line(Superpositions.trigger(CL)*[1 1] , [anew(3) anew(4)],'LineStyle','--','Color','k');
    line((Superpositions.trigger(CL)+3)*[1 1] , [anew(3) anew(4)],'LineStyle','--','Color','r');
    line((Superpositions.trigger(CL)-3)*[1 1] , [anew(3) anew(4)],'LineStyle','--','Color','r');
    if it==1
        title(['Wave: ' num2str(CW) '   Overlap: ' num2str(CL)  '   Clusters:' num2str(Superpositions.clus1(CL)) ' & ' num2str(Superpositions.clus2(CL)) '  Latency: ' num2str(Superpositions.shift1(CL)-Superpositions.shift2(CL))])
    else
        title(['Channel: ' num2str(it)])
    end
    eval(['set(OAxes' num2str(it) ',''Tag'',[''OverlapAxes'' num2str(it)]);'])
end

checkH  = findobj(gcbf,'Tag','SingleDisplay');
if get(checkH,'Value')
    hBIG = allchild(OAxesBIG);
    set([hBIG ; OAxesBIG],'Visible','off')
    hLITTLE = allchild([OAxes1 OAxes2 OAxes3 OAxes4]);
    for k=1:size(hLITTLE,1)
        eval(['set([hLITTLE{k};[OAxes' num2str(k) ']   ],''Visible'',''on'')'])
    end
else
    hBIG = allchild(OAxesBIG);
    set([hBIG ; OAxesBIG],'Visible','on')
    hLITTLE = allchild([OAxes1 OAxes2 OAxes3 OAxes4]);
    for k=1:size(hLITTLE,1)
        eval(['set([hLITTLE{k};[OAxes' num2str(k) ']   ],''Visible'',''off'')'])
    end
end




% --- Executes during object creation, after setting all properties.
function OutputBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OutputBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



% --- Executes during object creation, after setting all properties.
function SDthreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SDthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



% --- Executes during object creation, after setting all properties.
function Rthreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Rthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to Rthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rthreshold as text
%        str2double(get(hObject,'String')) returns contents of Rthreshold as a double


% --- Executes during object creation, after setting all properties.
function WRthreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WRthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function WRthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to WRthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of WRthreshold as text
%        str2double(get(hObject,'String')) returns contents of WRthreshold as a double


% --- Executes during object creation, after setting all properties.
function NSPthreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NSPthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function NSPthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to NSPthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NSPthreshold as text
%        str2double(get(hObject,'String')) returns contents of NSPthreshold as a double


% --- Executes during object creation, after setting all properties.
function WSPthreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WSPthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function WSPthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to WSPthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of WSPthreshold as text
%        str2double(get(hObject,'String')) returns contents of WSPthreshold as a double



function SDthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to SDthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SDthreshold as text
%        str2double(get(hObject,'String')) returns contents of SDthreshold as a double


