function MergeAllPoints

% MergeAllPoints
% Deletes all clusters and puts all points into one new cluster.
% Written by Baldwin Goodell
% Montana State University
% Charlie Gray Lab
% 08 December 2006
%
% Put this in the GeneralizedCutterOptions directory


global MClust_ClusterFileNames MClust_FeatureData MClust_Clusters


%delete all clusters
 numClusters = length(MClust_Clusters);
    nClust=1:numClusters;
    GeneralizedCutterStoreUndo('Group Delete');
    for ii=1:length(nClust)
        iClust = nClust(ii);      
        [GetInfoMSG ClusterLimits] = GetInfo(MClust_Clusters{iClust});
        if ~isempty(ClusterLimits)
            for iX = 1:length(ClusterLimits(:,1))
                MClust_Clusters{iClust} = DeleteLimit(MClust_Clusters{iClust}, ClusterLimits(iX,1), ClusterLimits(iX,2));
            end
        end
        MClust_Clusters{iClust} = DeleteMembers(MClust_Clusters{iClust});
    end
 
%create new cluster containing all points
CCWh = findobj('Tag','ClusterCutWindow');
if ~isempty(CCWh)
    cboHandle = findobj(CCWh,'Tag','Add Cluster');
    MClust_ClusterFileNames{end + 1} = 'AllPoints';
    GeneralizedCutterCallbacks(cboHandle)
else
    errordlg('can''t find ClusterCutWindow')
end

%put all pointsin new cluster
MClust_Clusters{end} = add_Points(MClust_Clusters{end},[1 : size(MClust_FeatureData,1)]);

%clean up
GeneralizedCutterCallbacks(findobj('Tag', 'Pack Clusters'));
SetDefaultColors
GeneralizedCutterCallbacks('RedrawAxes');
