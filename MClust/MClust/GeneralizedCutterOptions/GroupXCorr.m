function GroupXCorr

global MClust_Clusters 

numClusters = length(MClust_Clusters);
c = 1:numClusters;
[clusters,status] = listdlg('PromptString','Select Clusters:','SelectionMode','multiple',...
    'ListString',cellstr(num2str(c')));
if status~=1
    return;
end


numXClusters = length(clusters);

 prompt={'bin size (msec):','Window width (msec):'};
        def={'1','500'};
        dlgTitle='X Corr';
        lineNo=1;
        answer=inputdlg(prompt,dlgTitle,lineNo,def);
        if ~isempty(answer)
            figure;
            for i=1:numXClusters
                for j=i:numXClusters
                    % if ismember(i,clusters) & ismember(j,clusters)
                    subplot(numXClusters,numXClusters,((i-1)*numXClusters+j))
                    MClustXcorr(clusters(i), clusters(j),  str2num(answer{1}),  str2num(answer{2}));
                    drawnow
                end
                end
            end
        end




