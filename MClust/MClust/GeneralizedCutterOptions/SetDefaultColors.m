function SetDefaultColors(varargin)

if nargin == 0
   figHandle = gcbf; 
else
    figHandle = varargin{1};
    end


global MClust_Clusters
global MClust_Colors
global MClust_ClusterFileNames

GeneralizedCutterStoreUndo('Set Default Colors');
nClust = length(MClust_Clusters);
colors = nptDefaultColors(1:nClust);
MClust_Colors = [MClust_Colors(1,:) ; colors];
GeneralizedCutterClearClusterKeys(figHandle);
GeneralizedCutterRedrawClusterKeys(figHandle, max(0,length(MClust_Clusters)-16));

