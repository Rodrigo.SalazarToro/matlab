function GroupShowWaveforms


global MClust_Clusters 

numClusters = length(MClust_Clusters);
c = 1:numClusters;
[clusters,status] = listdlg('PromptString','Select Clusters:','SelectionMode','multiple',...
    'ListString',cellstr(num2str(c')));
if status~=1
    return;
end

%plot ShowWaveforms
figure;
amin=0;amax=0;  %initialize y axis limits
for i=1:length(clusters)
    %which spikes in this cluster?
    
    
    nptSubplot(length(clusters),i)
    DoPlotYN='no';
    ShowWaveforms(clusters(i),'DoPlotYN',DoPlotYN)
    title([ 'Cluster ' num2str(clusters(i)) ]) 
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function nptSubplot(n,p)
%nptSubplot(n,p)
%subplot with n being total number of plots
%and p being the current plot.
%this function decides how to split up 
%the axis space.  Useful when calling subplot with 
%a variable number of subplots.

switch n
    case 1
        r=1;c=1;
    case 2 
        r=1;c=2;
    case 3
        r=1;c=3;
    case 4
        r=2;c=2;
    case {5,6}
        r=2;c=3;
    case {7,8,9}
        r=3;c=3;
    case {10,11,12}
        r=3;c=4;
    case {13,14,15,16}
            r=4;c=4;
    case {17,18,19,20}
        r=4;c=5;
    otherwise
        r=5;
        c=ceil(n/5);
end

        
       
subplot(r,c,p)