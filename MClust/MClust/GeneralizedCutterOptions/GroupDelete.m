function GroupDelete

% GroupDelete
% Written by Baldwin Goodell
% Montana State University
% Charlie Gray Lab
% 30 September 2003
%
% Put this in the GeneralizedCutterOptions directory

    global MClust_Clusters
    figHandle = gcf;
    
    numClusters = length(MClust_Clusters);
    c=1:numClusters;
    [nClust,status] = listdlg('PromptString','Delete Clusters:','SelectionMode','multiple',...  %TEMPLATES
        'ListString',cellstr(num2str(c')),'ListSize',[160 (numClusters)*18]);
    if status~=1
        return;
    end
    GeneralizedCutterStoreUndo('Group Delete');
    for ii=1:length(nClust)
        iClust = nClust(ii);      
        [GetInfoMSG ClusterLimits] = GetInfo(MClust_Clusters{iClust});
        if ~isempty(ClusterLimits)
            for iX = 1:length(ClusterLimits(:,1))
                MClust_Clusters{iClust} = DeleteLimit(MClust_Clusters{iClust}, ClusterLimits(iX,1), ClusterLimits(iX,2));
            end
        end
        MClust_Clusters{iClust} = DeleteMembers(MClust_Clusters{iClust});
    end
    
    %GeneralizedCutterCallbacks(findobj('Tag', 'Pack Clusters'));
    GeneralizedCutterCallbacks('RedrawAxes');
 