function CllusterWaveforms(varargin)

% ClusterWaveforms - Plots all waveforms from selected clusters on top
% of each other. Allows easy inspection of cluster separation when there
% are only a few clusters. Clusters with higher numbers are plotted first
%
% Put this in the ClusterOptions directory
%
% Author: Shih-Cheng Yen

global MClust_TTData MClust_Clusters MClust_FeatureData
global MClust_Colors

numClusters = length(MClust_Clusters);
c = 1:numClusters;
[templates,status] = listdlg('PromptString','Select clusters:','SelectionMode','multiple',...
    'ListString',cellstr(num2str(c')),'ListSize',[160 (numClusters-1)*18]);
if status~=1
    return;
end

%plot waveforms
figure;
% set hold on so new plots don't erase old plots
hold on

% amin=0;amax=0;  %initialize y axis limits
% get number of clusters selected
nclusters = length(templates);
for i=nclusters:-1:1
	ShowWaveforms(templates(i),'PlotAllYN','yes','NewFigure','no');
end
