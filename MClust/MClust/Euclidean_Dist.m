function Y = Mahalanobis_Dist(target,X)
%Mahalanobis_Dist: Mahalanobis Distance between a target point and observations.
%   Y = Mahalanobis_Dist(target,X) returns a vector Y containing the Mahalanobis distances
%   between the target and each observation in the M-by-N data matrix X.  Rows of
%   X correspond to observations, columns correspond to variables.  
%   (Modified from Matlab's pdist.m)
%

% ncst
% 08 Nov 02

% X = [target; X];

distfun = @distcalc;
distargs = {};
distargs{end+1} = inv(cov(X));

[m, n] = size(X);
if any(imag(X(:))) & ~isequal(s,'usr')
	error('Mahalanobis_Dist does not accept complex inputs.');
end


if m < 2
	% Degenerate case, just return an empty of the proper size.
	Y = zeros(1,0);
	return;
end

% Create J 
J = ones((m),1);
J = cumsum(J);

% For large matrices, process blocks of rows as a group
n = length(J);
ncols = size(X,2);
blocksize = 1e4;                     % # of doubles to process as a group
M = max(1,ceil(blocksize/ncols));    % # of rows to process as a group
nrem = rem(n,M);
if nrem==0, nrem = min(M,n); end

Y = zeros(1,n);
ii = 1:nrem;
try
	Y(ii) = feval(distfun,repmat(target,size(ii,2),1),X(J(ii),:),distargs{:})';
catch
	if isa(distfun, 'inline')
		error(['The inline distance function generated the following ', ...
				'error:\n%s'], lasterr);
	elseif strfind(lasterr, ...
			sprintf('Undefined function ''%s''', func2str(distfun)))
		error('The distance function ''%s'' was not found.', func2str(distfun));
	else
		error(['The distance function ''%s'' generated the following ', ...
				'error:\n%s'], func2str(distfun),lasterr);
	end
end;

for j=nrem+1:M:n
	ii = j:j+M-1;
	try
		Y(ii) = feval(distfun,repmat(target,size(ii,2),1),X(J(ii),:),distargs{:})';
	catch
		if isa(distfun, 'inline')
			error(['The inline distance function generated the following ', ...
					'error:\n%s'], lasterr);
		else
			error(['The distance function ''%s'' generated the following', ...
					'error:\n%s'], func2str(distfun),lasterr);
		end;
	end;
end

% ----------------------------------------------
function d = distcalc(XI,XJ,arg)
Y = XI - XJ;
d = sqrt(sum((Y).*Y,2));    