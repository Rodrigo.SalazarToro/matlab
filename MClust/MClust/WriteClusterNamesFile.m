function WriteClusterNamesFile(filename,cn)


fid = fopen(filename,'w','ieee-le');
for ii=1:size(cn,2)
    fprintf(fid,'%s\r\n',char(cn(ii)));
end
fclose(fid);