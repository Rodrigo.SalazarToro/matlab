function MCC = add_Points(MCC,index)
%MCC = add_Points(MCC,index)
%adds points to a new mcconvexhull object

MCC.recalc=1;
MCC.myOrigPoints = unique([MCC.myOrigPoints ; index]);   
MCC.myPoints = unique([MCC.myPoints ; index]); 