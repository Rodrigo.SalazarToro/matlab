function MCC = subtract_exception(MCC,index)
%MCC = subtract_exception(MCC,index)
%adds points to the subtract_exceptions field in a mcconvexhull object

MCC.recalc=1;
MCC.add_exceptions = setdiff(MCC.add_exceptions,index);        
MCC.subtract_exceptions= [MCC.subtract_exceptions ; index];