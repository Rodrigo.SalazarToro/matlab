function [clusterIndex, overlap]  = ProcessClusters(data, clusters)

% clusterIndex = ProcessClusters(data, clusters)
%
% INPUT:
%   data - MClust feature data
%   clusters - MClust cluster objects
% OUTPUT:
%   index showing to which cluster each spike belongs
%   spikes belonging to multiple clusters get assigned the error signal -1
%   Also an overlap matrix is returned showing which of the multiple clusters
%   a waveform belongs.
%
% ADR 1999
%
% Status: PROMOTED (Release version) 
% See documentation for copyright (owned by original authors) and warranties (none!).
% This code released as part of MClust 3.0.
% Version control M3.0.



[nSamps, nDims] = size(data);
clusterIndex = zeros(nSamps,1);

overlap = sparse(zeros(nSamps,length(clusters)));
f3 = [];
for iC = 1:length(clusters)
    f = FindInCluster(clusters{iC},data);
    f2 = find(clusterIndex(f)>0);
    f3 = [f3; f(f2)];
    if ~isempty(f2)
        ind = sub2ind(size(overlap),f(f2),clusterIndex(f(f2)));
        overlap(ind) = 1;
    end
    overlap(f(f2),iC) = 1;
    clusterIndex(f) = iC;
    
end

clusterIndex(f3) = -1;



