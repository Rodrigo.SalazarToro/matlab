function [DoubleTriggerTimes] = RemoveDoubleTriggers(WF,SpikeTimes,ISI_Threshold)
% This function takes the multiunit ispikes data and removes the double
% triggers resulting from large spikes with large AHP's that cause double
% triggers.

numSpikes=size(WF,1);
numWF=size(WF,2);
peakInd=3; %32 datapoints per waveform, double triggers typically contain
% peaks within the first 3 points. Some funky overlapping waveforms and
% waveforms close to the noise will have an early positive peak.

% Find the double trigger waveforms based on the peakIndex Points
count=1;
for a = 1:numSpikes
    wf = squeeze(WF(a,:,:))';
    [pV,pI] = max(wf);
    pI=find(pI<=peakInd);
    [A,pV]=max(pV);
    ind=intersect(pV,pI);
    if ~isempty(ind)
        pInd(count,1)=a;
        pInd(count,2)=ind(1);
        pInd(count,3)=A(1);
        count=count+1;
    end
end

% for b = 1:size(pInd,1)
%     wf = squeeze(WF(pInd(b,1),:,:))';
%     plot(wf,'-*')
%     title(['WaveForm: ' num2str(pInd(b,1)) '  Channel: ' num2str(pInd(b,2)) '  PeakValue: ', num2str(pInd(b,3))])
%     waitforbuttonpress
% end

DoubleTriggerTimes=pInd(:,1);

% Remove the Subsequent waveforms after the double trigger, based on the
% ISI threshold, typically .3-.5ms works the best
new_dtt=[];
for b = 1:length(DoubleTriggerTimes)
    if DoubleTriggerTimes(b)~=numSpikes
        if diff([SpikeTimes(DoubleTriggerTimes(b)) SpikeTimes(DoubleTriggerTimes(b)+1)]) <= ISI_Threshold
            new_dtt=[new_dtt;DoubleTriggerTimes(b)+1];
        end
    end
end
DoubleTriggerTimes = sort(unique([DoubleTriggerTimes;new_dtt]));

new_dtt=[];
for b = 1:length(DoubleTriggerTimes)
    if DoubleTriggerTimes(b)~=numSpikes
        if diff([SpikeTimes(DoubleTriggerTimes(b)) SpikeTimes(DoubleTriggerTimes(b)+1)]) <= ISI_Threshold
            new_dtt=[new_dtt;DoubleTriggerTimes(b)+1];
        end
    end
end
DoubleTriggerTimes = sort(unique([DoubleTriggerTimes;new_dtt]));

new_dtt=[];
for b = 1:length(DoubleTriggerTimes)
    if DoubleTriggerTimes(b)~=numSpikes
        if diff([SpikeTimes(DoubleTriggerTimes(b)) SpikeTimes(DoubleTriggerTimes(b)+1)]) <= ISI_Threshold
            new_dtt=[new_dtt;DoubleTriggerTimes(b)+1];
        end
    end
end
DoubleTriggerTimes = sort(unique([DoubleTriggerTimes;new_dtt]));

