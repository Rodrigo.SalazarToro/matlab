function clustTT = ExtractClusterALL(TT, clusterIndex)
% MClust/ExtractCluster
%
% INPUTS
%    TT -- TT tsd
%    clusterIndex - output of FindInCluster
%  
% OUTPUTS
%    clustTT -- TT tsd containing only those points in cluster
%    returns a non-zero value of only a subset of the available points were retrieved.
%     the value is the number of points in the REAL CLUSTER, not the scaled cluster. The 
%     number in the scaled cluster can be gathered by just counting the rows clustTT.
%
% ADR 1999
% Version M1.0
% RELEASED as part of MClust 2.0
% See standard disclaimer in Contents.m
%
% cowen 2001
%  Major modification:  If TT is empty, lookup the waveforms in the original TTfile.
%    this necessitated importing the global TTFileName variable.
% cowen 2001
%  If the TT data from a cluster is to be loaded into memory, and the cluster has over
%  100,000 points, subsample the data (random subsample without replacement). This allows 
%  for the display of data that cannot fit into available memory. The smart way would be to 
%  scale the size dependent on free memory-- Smart is hard so I am not going to do it.
%
% 
%
% Status: PROMOTED (Release version) 
% See documentation for copyright (owned by original authors) and warranties (none!).
% This code released as part of MClust 3.0.
% Version control M3.0.

global MClust_TTfn MClust_TTdn
global MClust_TTData
global MClust_FeatureTimestamps
if isempty(MClust_TTData)
    if ~isempty(MClust_FeatureTimestamps)
        n_in_cluster = length(clusterIndex);
        
        %[t,wv] = LoadTT0_nt(MClust_TTfn, MClust_FeatureTimestamps(clusterIndex),1); 
        % REPLACED ADR 14 May 2002
        pushdir(MClust_TTdn);
        
        [t,wv] = MClust_LoadNeuralData(MClust_TTfn, MClust_FeatureTimestamps(clusterIndex),1);
        
        popdir;
        % ncst
        % check to see if the timestamps you get out match those you put in
        if ~isempty(t) & sum(t~=MClust_FeatureTimestamps(clusterIndex)) ~= 0
            disp(' ')
            disp(['Error: Timestamp mismatch in ' MClust_TTfn])
            GoodPoints = t~=MClust_FeatureTimestamps(clusterIndex);
            t = t(find(GoodPoints == 0));
            wv = wv(find(GoodPoints == 0),:,:);
            disp(['Removed ' num2str(length(find(GoodPoints ~= 0))) ' of ' num2str(length(t)) ' points'])
            disp(' ')
        end
        disp(['Loaded ' num2str(length(t)) ' spikes.'])
        
        clustTT = tsd(t,wv);
    else
        msgbox('No spikes in this cluster')
    end
else
    TTtimestamps = Range(TT, 'ts');
    TTdata = Data(TT);
    clustTT = tsd(TTtimestamps(clusterIndex), TTdata(clusterIndex,:,:));
end
