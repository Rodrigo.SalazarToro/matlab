function cn = ClusterNames(numClusters)

fig_handle = UnitType(numClusters);
p = get(fig_handle,'Position');
%set figure hieght based on numClusters
figHieght  = 1;
%figHieght  = .1*numClusters + .2;
%set(fig_handle,'Units','normalized','Position',[p(1) p(2) p(3) 1]);


for ii=1:numClusters
    sspace = .2;
    espace = .05;
    spacing = (figHieght-sspace-espace)/numClusters;
    vert = figHieght-sspace - ii*spacing;
    if ii==numClusters
        typeString = 'multi';
    else
        typeString = 'single';
    end
    
    uicontrol('Parent', fig_handle,'Units','normalized','Position',[.111 vert .089 .046], ...
        'FontUnits','normalized','FontSize',.85, ...
        'FontWeight','bold', ...
        'String',num2str(ii),'Style','text');
    
    handlestr = ['hnum' num2str(ii)];
    cstr1 = ' = uicontrol(''Parent'', fig_handle,''Units'',''normalized'',''Position'',[.33 vert .222 .046],';
    cstr2 = '''FontUnits'',''normalized'',''FontSize'',.561,';
    cstr3 = '''FontWeight'',''normal'', ''BackgroundColor'',[1 1 1],'; 
    cstr4 = '''String'',num2str(ii),''Style'',''edit'',';
    cstr5 = '''Tag'',[''CellName'' num2str(ii)],''TooltipString'',';
    cstr6 = '''Number associated with a cell throughout multiple sessions at a single site.'');';
    eval([handlestr cstr1 cstr2 cstr3 cstr4 cstr5 cstr6])
    
    handlestr = ['htype' num2str(ii)];
    cstr1 = ' =  uicontrol(''Parent'', fig_handle,''Units'',''normalized'',''Position'',[.694 vert .166 .046],';
    cstr2 = '''FontUnits'',''normalized'',''FontSize'',.368,';
    cstr3 = '''FontWeight'',''normal'',''BackgroundColor'',[1 1 1],';
    cstr4 = '''String'',typeString,''Style'',''edit'',';
    cstr5 = '''Tag'',[''UnitType'' num2str(ii)],''TooltipString'',';
    cstr6 = '''Type single or multi.'');';
    eval([handlestr cstr1 cstr2 cstr3 cstr4 cstr5 cstr6])
    
end


uiwait(fig_handle)


if ishandle(fig_handle)
    %get parameters from gui
    mNum=0;
    sNum=0;
    for ii=1:numClusters
        
        eval(['num = str2num(get(hnum' num2str(ii) ',''String''));'])
        eval(['typeStr = get(htype' num2str(ii) ',''String'');'])
        if(strcmp(lower(typeStr(1)),'s'))
        	type='s';
            sNum = sNum+1;
            cn{ii} = [num2strpad(sNum,2) type];
        elseif(strcmp(lower(typeStr(1)),'m'))
        	type='m';
            mNum = mNum+1;
            cn{ii} = [num2strpad(mNum,2) type];
        end
    end
    
    
else
    warndlg('CellName and Type file not saved')
end

close(fig_handle)
