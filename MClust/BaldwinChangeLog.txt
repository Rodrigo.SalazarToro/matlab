Changes to MClust 3.2
baldwin@cns.montana.edu


**New features added to feature folder


**RunClustBatch
	added ==0 @ line 437
	added new diary functionality to start and end of file

**Bring to Top function
	changed GeneralizedCutter.m
	added 'Bring to Top' case @ 713
	added 'Bring to Top' to Keys @ 1088
	%Added 'Bring To Top' within the permament functions because 
	%Cluster Options does not have acess to the StoreUndo function. 


**EditWaveforms
	new function in ClusterOptions which uses EditWaveforms folder
	EditWaveforms Folder - new functions


**DefineOverlap
	new function in ClusterOptions which uses Overlap folder
	Overlap Folder - new functions
	also added nonGui version via 
	"SortOverlap" in GeneralizedCutter @ line 184
	and in GeneralizedCutterCallbacks @ line 178


**ProcessCluster
	added overlap functionality and fixed bug


**MClustCallbacks
	in case 'WriteFiles' added new overlap file on lines 972 & 980


**@mcconvexhull class
	1. mcconvexhull constructor
		MCC.add_exceptions = [];
		MCC.subtract_exceptions = [];
	2. merge method
	3. add_Points
	4. add_exceptions
	5. subtract_exceptions
	6. FindInCluster
	

**bbconvexhull constructor
	MCC.add_exceptions = [];
	MCC.subtract_exceptions = [];
	%added for backcompatibility b/c MClust3.0 used bbconvexhull as default.
	

**GroupDelete
	new function via Group Delete Button
	in GeneralizedCutter @ line 190 and 
	in GeneralizedCutterCallbacks @line 182

**MeanWaveforms
	new function via Group Delete Button
	in GeneralizedCutter @ line 196 and 
	in GeneralizedCutterCallbacks @line 212
	also change button defualts in GeneralizedCutter@ 37



***** UNIX fixes
	Capitalization of words and filenames:
	Extract_varargin
	FindFiles
	DisplayProgress
	mcconvexhull
	Data (methods)
	pushdir([MClust_Directory filesep 'ClusterOptions' filesep]); @1070 GeneralizedCutterCallbacks		
	added mexglx and mexmac files of dll files








9/17/03
RunClustBatch			removed diary code in WriteFeatureData2TextFile subfunction


9/19/03			
nptSubplot				added to MClust/Matlab dir
GeneralizedCutterCallbacks		changed limits in meanWaveforms

10/1/03
batch.txt		added KKwikMaxPossibleClusters
ParseBatch			"
RunClustBatch			"

10/2/03
RunClustBatch		added ShihCheng's changes regarding KlustaKwik on different platforms
			and made KlustaKwik-1.5 the default

10/7/03
OverlapGUI.m/.fig		changed to show only templates that are in out_cluster
RunDefineOverlap		"

10/23/03
RunClustBatch 		displays output of Klustakwik to get permission denied message
overlap folder 		added overlap parameters gui, etc...

12/10/03
klustakwik-1.6 		windows, linus and mac versions
Batch_KKwikEE.txt	added penaltyMix value - undercluster for electrode
Batch_KKwik.txt         added penaltyMix value  - overcluster for tetrode
RunClustBatch		klustakwik-1.6 is default
RunClustBatch		changed case to extract_varargin
GeneralizedCutter 	changed case to displayprogress
GeneralizedCutterCallbacks	removed redundant AddLimits

1/23/04
New functions:
		multiCopy
		GroupHistISI	   (changed ShowHistISI)
		GroupShowWaveforms (changed ShowWaveforms)
		GroupXCorr
		SetDefaultColors (uses nptDefaultColors)

added multipleSelect to Edit Waveforms
	which required changes in highlight, EditWaveformsGUI, BringToTop
added nextISI to editWaveforms

made a small change to GeneralizedCutterCallbacks @line 691 and 723.

Changed max default num of clustes from 64 to 100
	MClust_Hide = zeros(64,1); -> MClust_Hide = zeros(100,1);
	BBMClustCallbacks.m 		line 611
	MClust.m			line 61
	MClustCallbacks.m 		line 818


April 15,04
OverlappingWaveforms  use ExtractClusterAll when loading overlap clusters.

5/18/04
EditWaveformsGUI   		vertical or horizontal multi select line.

5/24/04
GetOverlapData		only pertains to the overlap clusters now.
GetTemplateDAta		moved old functionality of getOverlapDAta to this function.
ExtractClusterALL 	moved to MClust/Mclust directory

6/7/04
EditWaveformsGUI   	added aspectratio to vetical vs. horiz decision

6/8/04			
@MCConvexhull/Merge	account for exceptions before merging.
         "   /subtract_exception	changed comment

6/9/04
ProcessClusters		removed the WriteOverlapFile subfunction
WriteOverlapFile	new file
MClustCallbacks		added call to WriteOverlapFile
AddUnaccountedPoints	new function

6/16/04
WriteNPTFiles		new wrapper function to write overlap and ispikes files
MClustCallbacks		added writenptfiles to write case
UnitType		new .fig and .m gui to specify cluster ispikes filenames
ClusterNames		callback for unittypr function

6/16/04
GetOverlapData		changed to get only data for overlaps 
GetTemplateData		new function to only get template data rather than using get overlapdata
OverlapParameters	calls gettemplatedata now

6/24/04	
writenptfiles

7/8/04
writeNPTFiles		write cluster names file.
writeclsternamefile	new file
readclusternamefile	new file

7/29/04
Changed max default num of clustes from 100 to 150
	MClust_Hide = zeros(100,1); -> MClust_Hide = zeros(150,1);
	BBMClustCallbacks.m 		line 611
	MClust.m			line 61
	MClustCallbacks.m 		line 818