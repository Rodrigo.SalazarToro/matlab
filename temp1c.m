% load switchdays.mat
%
% coh = ProcessDays(mtscohInter,'days',days,'sessions',{'session01'},'NoSites');
% index = coh.data.Index;

Args = struct('redo',0','save',0);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{});

for anat = [1 3]
    for rule = 0 : 3
        for effect = 3 : 6
            ind = find(index(:,effect) == rule & index(:,9) == anat);
            if ~isempty(ind)
                
                for i = vecr(ind)
                    cd(coh.data.setNames{i}); % day directory
                    pairg = index(i,10:11);
                    matfile = sprintf('cohgramg%04.0fg%04.0f.mat',pairg(1),pairg(2));
                    file = nptDir(matfile);
                    if isempty(file) || Args.redo
                        for s = 2 : 3
                            %index(:,[10 11])
                            cd(sprintf('session0%d',s))
                            NeuroInfo = NeuronalChAssign;
                            for c =1  : 2; ch(c) = find(pairg(c) == NeuroInfo.groups); end
                            mts = mtstrial('auto');
                            rules = unique(mts.data.Index(:,1));
                            trials{s-1} = mtsgetTrials(mts,'stable','BehResp',1);
                            
                            
                            [Ct,phit,S12,S1,S2,t{s-1,1},f] = MTScohgram(mts,trials{s-1},ch);
                            C{rules,1} = squeeze(Ct);
                            phi{rules,1} = squeeze(phit);
                            
                            [Ct,phit,S12,S1,S2,t{s-1,2},f] = MTScohgram(mts,trials{s-1},ch,'matchAlign');
                            C{rules,2} = squeeze(Ct);
                            phi{rules,2} = squeeze(phit);
                            
                            cd ..
                        end
                        
                        display(matfile)
                        save(matfile,'C','phi','t','f','trials')
                        
                    else
                       
                        load(matfile)
                        loc(anat).rule(rule+1).effect(effect).pair(i).C = C;
                        
                        loc(anat).rule(rule+1).effect(effect).pair(i).phi = phi;
                       
                    end
                    
                end
            end
        end
    end
end

effect = 3;
for anat = [1 3]
    
    for r = 1 : 4
        figure
        mil = [];
        n = size(loc(anat).rule(r).effect(effect).pair,2);
        for cond = 1 : 4
        for pa =  1 : n; mil = [mil size(loc(anat).rule(r).effect(effect).pair(pa).C{cond},1)]; end;
        mil = min(mil);
        for pa =1 : n; thedata(n,:,:) = loc(anat).rule(r).effect(effect).pair(pa).C{cond}(1:mil,:); end;
        subplot(2,2,cond)
        
        end
    end
end
