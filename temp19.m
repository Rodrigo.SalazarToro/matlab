
ncombName = {'md' 'ld' 'sd'};
realName = {'PPCm-PFCd' 'PPCl-PFCd' 'PPCs-PFCd'};
% fband = find(f > 12 & f < 60);
% ncombName = {'54' '56' '58' 'm4' 'm6' 'm8' 'l4' 'l6' 'l8' '74' '76' '78'};
% realName = {'PPC5-PFC46' 'PPC5-PFC6' 'PPC5-PFC8' 'PPCmip-PFC46' 'PPCmip-PFC6' 'PPCmip-PFC8' 'PPClip-PFC46' 'PPClip-PFC6' 'PPC7a-PFC8' 'PPC7-PFC46' 'PPC7a-PFC6' 'PPC7a-PFC8'};
figure
for ncomb = 1 : length(ncombName)
    alldata = cell(4,1);
%         [r,allind] = get(cohn,'snr',1.8,'Number','pairhist',ncombName{ncomb});
    
    [r,allind] = get(cohn,'snr',1.8,'Number','pairhist',ncombName{ncomb},'rudeHist');
    if ~isempty(allind)
        for ind = allind
            cmb = cohn.data.Index(ind,1);
            %             pairg = coh.data.Index(ind,10:11);
            cd(cohn.data.setNames{ind})
            %             load cohInterDelay1.mat
            load cohInter.mat
            %             sur = load('cohInterSur.mat');
            
            thes = Day.session(1).rule;
            if thes <= size(Day.session,2) && length(Day.session(thes).trials) >=140
                for p = 1 : 4
                    %                 if length(find(Day.session(thes).C(fband,cmb,3) > sur.Day.session(thes).Prob(fband,1,cmb,3))) > 1 || length(find(Day.session(thes).C(fband,cmb,4) > sur.Day.session(thes).Prob(fband,1,cmb,4))) > 1
                    alldata{p} = [alldata{p} single(Day.session(thes).C(:,cmb,p))];
                    %                     clf;subplot(2,1,2); plot(Day.session(thes).C(fband,cmb,4)); hold on; plot( sur.Day.session(thes).Prob(fband,1,cmb,4))
                    %                     subplot(2,1,1);plot(Day.session(thes).C(fband,cmb,3)); hold on; plot( sur.Day.session(thes).Prob(fband,1,cmb,3))
                    %                 end
                end
            end
            %             pause
        end
        
        
        % figure; plot(f,prctile(alldata(:,max(alldata,[],1)< 0.5),[25 50 75 90 95],2))
        for p = 1 : 4
            subplot(length(ncombName),4,(ncomb-1) * 4 + p)
            plot(f,prctile(alldata{p},[50 75 90 95 99],2))
%             if ncomb == 1; title(sprintf('delay %d',p)); end
            if p == 1; ylabel(sprintf('%s',realName{ncomb})); text(50, 0.4,sprintf('n=%d',size(alldata{p},2)));end
            axis([8 60 0 0.6])
            grid on
        end
    end
end
xlabel('Frequency [Hz]')
ylabel('Coherence')
legend('50th prctile','75th prctile','90th prctile','95th prctile','99th prctile')