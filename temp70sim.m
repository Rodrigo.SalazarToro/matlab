
function [S,t,f,lfp]=temp70sim(files,oinds,oboth,ostrt)
%files, list of file names to analyze
%oinds= optional boolian to draw individual power spectra
    
    if nargin<2 | isempty(oinds)
        inds=1;
    else
        inds = oinds;
    end
     if nargin<3 | isempty(oboth)
        both=1;
    else
        both = oboth;
     end
    if nargin<4 | isempty(ostrt)
        st = 501;
    else
        st = ostrt;
    end
    
    ndnum=3500-st;
    params = struct('Fs',1000,'fpass',[1 90],'tapers',[3 5],'trialave',0);
    c=1;
    if both
    lfp = zeros(ndnum,length(files)*2);
    else
        lfp = zeros(ndnum,length(files));
    end
    
    for ff = 1 : length(files)
        [LFP_column_1,LFP_column_2]=read_a_run(files{ff},0);
        if both
            nlfp = (LFP_column_2(st:end)- mean(LFP_column_2(st:end))) / std(LFP_column_2(st:end));
            [lfpt , rs] = nptLowPassFilter(nlfp,1000,5,120);
            lfp(:,c) = lfpt(1:ndnum);
            c=c+1;
        end
        nlfp = (LFP_column_1(st:end)- mean(LFP_column_1(st:end))) / std(LFP_column_1(st:end));
         [lfpt , rs] = nptLowPassFilter(nlfp,1000,5,120);
         try
          lfp(:,c) = lfpt(1:ndnum);
         catch
             1
         end
        c=c+1;
    end

    figure
    [S,t,f]=mtspecgramc(lfp,[0.5 0.02],params);
     imagesc(t,f,squeeze(mean(S,3))')
     
     if inds
         see_individuals(S,lfp,t,f)
     end




function see_individuals(S,lfp,t,f)
figure
 for ff =1 : size(S,3)
     subplot(2,1,1)
     plot(lfp(:,ff))
     subplot(2,1,2)
  imagesc(t,f,squeeze(S(:,:,ff)'))
  colorbar
  title(ff)
  pause
  clf
 end