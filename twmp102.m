clear all
cd F:\salazar\data\Electrophy
nunit = 1;
unit = cell(50,4);
raster = cell(50,4);
unitname = cell(50,1);
days = {'20150604' '20150623'};
sdir = pwd;
ntrials = zeros(50,4);
for d = 1 : length(days)
    cd(days{d});
    sessions = nptDir('session0*');
    ddir = pwd;
    for s = 1 : length(sessions)
        cd(sessions(s).name)
        sesdir = pwd;
        seqfile = nptDir('*newSeq.mat');
        load(seqfile.name,'-mat');
        ntrial = length(nseq);
        
        light =  ismember(nseq,find(ismember(conds,{'Air + Light'})));
        
        odor = ismember(nseq,find(ismember(conds,{'3-Hexanone' 'Amyl acetate' 'Ethyl butyrate'})));
        sound = ismember(nseq,find(ismember(conds,{'Sound'})));
        both = ismember(nseq,find(ismember(conds,{'Sound+3-Hexanone' 'Sound+Amyl acetate' 'Sound+Ethyl butyrate'})));
        if ~isempty(nptDir('sort'))
            cd sort
            groups = nptDir('group0*');
            
            for g = 1 : length(groups)
                cd(groups(g).name)
                gdir = pwd;
                clusters = nptDir('cluster0*');
                for cl = 1 : length(clusters)
                    cd(clusters(cl).name)
                    
                    load ispikes
                    for tr = 1 : ntrial
                        if tr <= sp.data.numTrials
                            if ismember(tr,find(light))
                                unit{nunit,1} = cat(2,unit{nunit,1},sp.data.trial(tr).cluster.spikes);
                                raster{nunit,1} = [raster{nunit,1}; hist(sp.data.trial(tr).cluster.spikes,[0:20:5000])];
                                ntrials(nunit,1) = ntrials(nunit,1) + 1;
                            elseif ismember(tr,find(sound))
                                unit{nunit,2} = cat(2,unit{nunit,2},sp.data.trial(tr).cluster.spikes);
                                ntrials(nunit,2) = ntrials(nunit,2) + 1;
                            elseif ismember(tr,find(odor))
                                unit{nunit,3} = cat(2,unit{nunit,3},sp.data.trial(tr).cluster.spikes);
                                ntrials(nunit,3) = ntrials(nunit,3) + 1;
                            elseif ismember(tr,find(both))
                                unit{nunit,4} = cat(2,unit{nunit,4},sp.data.trial(tr).cluster.spikes);
                                ntrials(nunit,4) = ntrials(nunit,4) + 1;
                            end
                        end
                    end
                    unitname{nunit} = pwd;
                    cd(gdir)
                    allsp{nunit} = sp.data;
                    nunit = nunit + 1;
                end
                cd ..
            end
            
            cd(ddir)
        end
    end
    cd(sdir)
end

%%
save('twmp102.mat')
%% plotting
cats = { 'sound' 'odor' 'odor + sound'};
figure
labs = { ' ' 'r' 'b' 'k'};

bins = [10 200 200 200];
for nu = 1 : nunit - 1
    set(gcf,'Name',unitname{nu})
    
    sb = 1;
    subplot(3,1,sb)
    [n,xtick] = hist(unit{nu,sb},[0:bins(sb):5100]);
    plot(xtick,(1000 /bins(sb))* n/ntrials(nu,sb))
    title('light')
    xlim([0 5000])
    
    subplot(3,1,2)
    imagesc(~raster{nu,1})
    colormap('gray')
    
    subplot(3,1,3)
    for sb = 2 : 4
        [n,xtick] = hist(unit{nu,sb},[0:bins(sb):5100]);
        plot(xtick,(1000 /bins(sb))* n/ntrials(nu,sb),labs{sb})
        hold on
    end
    legend(cats)
    ylabel('Firing rate (sp/s)')
    xlabel('time (ms)')
    
    pause
    clf
end

%% xcor

bins = [-201:201];
pcomb = [nchoosek([1:7],2); nchoosek([8:14],2); nchoosek([15:21],2); nchoosek([22:27],2); nchoosek([28:37],2)];
cumcoi = zeros(size(pcomb,1),length(bins));
for ncomb = 1 : size(pcomb,1)
    for tr = 1 : allsp{pcomb(ncomb,1)}.numTrials
        for nspike = 1 : length(allsp{pcomb(ncomb,1)}.trial(tr).cluster.spikes)
            if ~isempty(allsp{pcomb(ncomb,2)}.trial(tr).cluster.spikes)
            cumcoi(ncomb,:) = cumcoi(ncomb,:) + hist(allsp{pcomb(ncomb,2)}.trial(tr).cluster.spikes - allsp{pcomb(ncomb,1)}.trial(tr).cluster.spikes(nspike),bins);
            end
        end
    end
end

figure
for ncomb = 1 : size(pcomb,1)
    plot(cumcoi(ncomb,2:end-1))
    title([unitname{pcomb(ncomb,1)} unitname{pcomb(ncomb,2)}])
    pause
end

