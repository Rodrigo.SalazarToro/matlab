for d = 1 : 16
    cd(days{d})
    cd session01/lfp
    file = nptDir('flatGrOld.mat');
    if ~isempty(file)
    load(file.name)
%      movefile('flatGr.mat','flatGrOld.mat')
    else
        flatGr = [];
    end
    ind = find(nanmedian(allSNR{d}) < 1.5);
%     maybe = find(SNR{d}(:,2) > 1.5 & SNR{d}(:,2) < 1.8);
%     SNR{d}(maybe,:)
    
   
    thegr = unique(sort([SNR{d}(ind,1)' flatGr]));
   
    flatGr = thegr;
    save flatGr.mat flatGr
    cd ../../..
end
    