
obj = loadObject('cohInterIDE.mat');
[r,ind] = get(obj,'Number','snr',99);r
cPPC = 1;
cPFC = 1;
csPPC = 1;
csPFC = 1;
clear PFCmua PPCmua PFCsua PPCsua PFCmuaSur PPCmuaSur PFCsuaSur PPCsuaSur
for i = 1 : length(ind)
    ngr = obj.data.Index(ind(i),10:11);
    cd(obj.data.setNames{ind(i)});
    if isempty(strfind(pwd,'clark'))
        sessions(1).name = 'session01';
        ns = 1;
    else
        sessions = nptDir('session0*');
        ns = 2;
    end
    
    
    for s = ns : length(sessions)
        
        cd(sessions(s).name)
        sdir = pwd;
        ppc = sprintf('%04.0f',ngr(1));
        pfc = sprintf('%04.0f',ngr(2));
        if ~isempty(nptDir(sprintf('group%s',ppc))) && ~isempty(nptDir(sprintf('group%s',pfc)))
            cd(sprintf('group%s',ppc))
            cluster = nptDir('cluster*');
            gdir = pwd;
            for cl = 1 : length(cluster)
                cd(cluster(cl).name)
                if ~isempty(nptDir(sprintf('SFCall1g%sRule1Sur.mat',pfc)))
                    load(sprintf('SFCall1g%sRule1.mat',pfc))
                    
                    load(sprintf('SFCall1g%sRule1Sur.mat',pfc))
                    if cluster(cl).name(end) == 'm'
                        PPCmua(cPPC,:,:) = C;
                        PPCmuaSur(cPPC,:,:,:) = Prob;
                        cPPC = cPPC + 1;
                    else
                        PPCsua(cPPC,:,:) = C;
                        PPCsuaSur(cPPC,:,:,:) = Prob;
                        csPPC = csPPC + 1;
                    end
               
                end
                cd(gdir)
            end
            cd(sdir)
            
            cd(sprintf('group%s',pfc))
            gdir = pwd;
            cluster = nptDir('cluster*');
            for cl = 1 : length(cluster)
                cd(cluster(cl).name)
                if ~isempty(nptDir(sprintf('SFCall1g%sRule1Sur.mat',ppc)))
                    load(sprintf('SFCall1g%sRule1.mat',ppc))
                    
                    load(sprintf('SFCall1g%sRule1Sur.mat',ppc))
                    if cluster(cl).name(end) == 'm'
                        PFCmua(cPFC,:,:) = C;
                        PFCmuaSur(cPFC,:,:,:) = Prob;
                        cPFC = cPFC + 1;
                    else
                        PFCsua(cPFC,:,:) = C;
                        PFCsuaSur(cPFC,:,:,:) = Prob;
                        csPFC = csPFC + 1;
                    end
               
                end
                
                cd(gdir)
            end
            cd(sdir)
        else
            keyboard
        end
        cd(obj.data.setNames{ind(i)});
    end
end



sigPPC = PPCmua > squeeze(PPCmuaSur(:,:,2,:));
sigPFC = PFCmua > squeeze(PFCmuaSur(:,:,2,:));

sigPPCsua = PPCsua > squeeze(PPCsuaSur(:,:,1,:));
sigPFCsua = PFCsua > squeeze(PFCsuaSur(:,:,1,:));

epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
figure
for p = 1 : 4
    subplot(4,4,p); plot(f,sum(sigPPC(:,:,p))/size(sigPPC,1))
    ylim([0 0.3])
    %     xlim([0 40])
    title(epochs{p})
    subplot(4,4,4+p); plot(f,sum(sigPFC(:,:,p))/size(sigPFC,1))
    ylim([0 0.3])
    %     xlim([0 40])
    subplot(4,4,8+p); plot(f,sum(sigPPCsua(:,:,p))/size(sigPPCsua,1))
    ylim([0 0.2])
    %     xlim([0 40])
    title(epochs{p})
    subplot(4,4,12+p); plot(f,sum(sigPFCsua(:,:,p))/size(sigPFCsua,1))
    ylim([0 0.2])
    %     xlim([0 40])
end

xlabel('Frequency [Hz]')
ylabel('Spike Field Coherence')
subplot(4,4,1); ylabel('PPC mua PFC lfp')
subplot(4,4,5); ylabel('PFC mua PPC lfp')
subplot(4,4,9); ylabel('PPC sua PFC lfp')
subplot(4,4,13); ylabel('PFC sua PPC lfp')




