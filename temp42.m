

a = load('testgr0002gr0004.mat');


PP300 = squeeze(a.C(:,1,:));
f = a.f;
a = load('testgr0002gr0011.mat');
a = load('testgr0002gr0009.mat');
a = load('testgr0004gr0009.mat');
a = load('testgr0004gr0011.mat');
inter300 = squeeze(a.C(:,1,:));

a = load('testgr0009gr0011.mat');
PF300 = squeeze(a.C(:,1,:));


[datafit,xx,threshPF,rsquare] = fitSurface(PF',a.f,'showPlots','Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2);
[datafit,xx,threshInter,rsquare] = fitSurface(inter',a.f,'showPlots','Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2);
[datafit,xx,threshPP,rsquare] = fitSurface(PP',a.f,'showPlots','Prob',[0.95 0.99 0.999 0.9999 0.99999 0.999999],'FSpread',2);

figure; 
for p = 1 : 6
    subplot(6,1,p);
    plot(f,threshPP(:,p),'r'); 
    hold on; 
    plot(f,threshPF(:,p),'b'); 
    plot(f,threshInter(:,p),'k'); 
    ylim([0.05 0.3]); 
end