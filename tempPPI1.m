function plotStartle(startle,infoTrial,defaultParams,chamberid,blockid,varargin)
%
%
Args = struct('block',2);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


limit = max(max(abs([startle.Null startle.Prepulse startle.Startle])));

%% plot the 3 periods
periods = {'Null' 'Prepulse' 'Startle'};
figure
set(gcf,'Name','Startles for the diferent periods');
subplot(1,3,1); plot(startle.Null')
subplot(1,3,2); plot(startle.Prepulse')
subplot(1,3,3); plot(startle.Startle')
for sb = 1 : 3; subplot(1,3,sb);ylim([-limit limit]); title(periods{sb});end

xlabel('Time (ms)')
ylabel('startle (arb. unit)')
%% plot the different intensities of prepulse

raw = find(strcmp('Prepulse Level',defaultParams) ==1);
for ch = 1 : max(chamberid)
    figure
    set(gcf,sprintf('Different prepulse intensities for Chamber #%d',ch));
    c=1;
    for int = unique(infoTrial(raw,:))
        prepulse = double2str(int);
        ind = intersect(intersect(find(infoTrial(raw,:) == int),find(chamberid == ch)),find(blockid == Args.block));
        subplot(5,1,c)
        plot(startle.Startle(ind,:)')
        xlabel('Time (ms)')
        ylabel('startle (arb. unit)')
        ylim([-limit limit])
        title(['prepulse at ' prepulse 'dB'])
        c=c+1;
    end
end