cd /Volumes/raid/data/monkey/
bothMonkeys = loadObject('cohInterIDEL.mat');
[r,ind] = get(bothMonkeys,'Number','snr',99);r
sigdiffn = zeros(3,r,100,4);
sigdiffp = zeros(3,r,100,4);
plevels = [0.0001 0.00001 0.000001];
% testfig = figure;
for i = 1 : r;
    
    cd(bothMonkeys.data.setNames{ind(i)});
    cd GCdirStat
    pairg = bothMonkeys.data.Index(ind(i),10:11);
    pwd;
    file = sprintf('GCinterg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),1);
    load(file,'Fdiff')
    sfile = sprintf('GCintergeneralRule%d.mat',1);
    load(sfile,'pvalues','levels')
%     figure(testfig); for p = 1 : 4; subplot(1,4,p); plot(Fdiff(:,p)); hold on; plot(pvalues(:,:,p),'--'); end; pause; clf
    pv = 1;
    for plevel =  plevels
        pind = find(plevel == levels);
        % Fdiff = Fx2y - Fy2x;
            sigdiffn(pv,i,:,:) = (Fdiff <= squeeze(pvalues(:,pind(1),:))) ; % negative means PFC dominate
            sigdiffp(pv,i,:,:) = (Fdiff >= squeeze(pvalues(:,pind(2),:))) ; % positive means PPC dominates
        
        pv = pv + 1;
    end
    %     figure(testfig)
    %     for pv = 1 : 3;
    %         for p =1 : 4;
    %             subplot(3,4,(pv-1) * 4 + p);
    %             plot(sum(squeeze(sigdiffn(pv,:,:,p))/r,1));
    %             hold on
    %             plot(sum(squeeze(sigdiffp(pv,:,:,p))/r,1),'r');
    %             ylim([0 1]);xlim([0 50]);
    %         end;
    %     end
    %     pause
    %     clf
end
figure
for pv = 1 : 3;
    for p =1 : 4;
        subplot(3,4,(pv-1) * 4 + p);
        plot(sum(squeeze(sigdiffn(pv,:,:,p))/r,1));
        hold on
        plot(sum(squeeze(sigdiffp(pv,:,:,p))/r,1),'r');
        ylim([0 1]);xlim([0 50]);
    end;
end


xlabel('Frequency [Hz]')
ylabel('Fraction of significant')
legend('PFC->PPC dom.','PPC->PFC dom.');
epoch = {'pre-sample' 'sample' 'delay1' 'delay2'};
for sb = 1 : 4; subplot(3,4,sb); title(epoch{sb}); end
c = 1 ;for sb = [1 5 9];subplot(3,4,sb); ylabel(sprintf('p < %d',plevels(c))); c = c + 1;end


% clark PPC = x, PFC = y;
% betty PFC = x, PPC = y