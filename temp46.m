obj = loadObject; % choose  cohInterIDE
[r,ind] = get(obj,'Number','snr',99);
parfor item = 1 : 3
  mkAveCohgram(obj,ind,'save','iCueLoc',item,'stim',sprintf('Loc%d',item)); 
   mkAveCohgram(obj,ind,'save','iCueObj',item,'stim',sprintf('Obj%d',item));
end
mkAveCohgram(obj,ind,'save');
mkAveCohgram(obj,ind,'save','InCor');
mkAveCohgram(obj,ind,'save','tuning');

%%% longdays

obj = loadObject; % choose  cohInterLong
[r,ind] = get(obj,'Number','snr',99);
for item = 1 : 3
  mkAveCohgram(obj,ind,'save','iCueLoc',item,'stim',sprintf('Loc%d',item),'noCharlie'); 
   mkAveCohgram(obj,ind,'save','iCueObj',item,'stim',sprintf('Obj%d',item),'noCharlie');
end