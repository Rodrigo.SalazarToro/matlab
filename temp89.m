cd /Volumes/raid/data/monkey/
clear all
if isempty(nptDir('temp89.mat'))
    LOCobj = loadObject('mtsXCobjLOC.mat');
    IDEobj = loadObject('mtsXCobjIDE.mat');
    [sindLOC,sindIDE] = getSameIndRules(LOCobj,IDEobj);
    obj1 = IDEobj;
    
    pairs = unique(obj1.data.hist(sindIDE,:),'rows');
    spN = [4 8 11 13];
    % spN = [1 2 3 4 8 10 11 13];
    %  spN = [1 2 3 10];
    srow = find(sum(ismember(pairs,spN),2) == 2);
    spairs = pairs(srow,:);
    [~,ind] = get(obj1,'Number','hist',spairs);
    ind = intersect(ind,sindIDE);
    % [sigPos,sigNeg,sigPosDiff,sigNegDiff,mCor,sigCor,sigRule,allsigCor,bins] = plotIncXcorrSpikes(obj1,ind,'plot');
    %
    % allsigCor = unique(cat(2,sigCor{1,1},sigCor{2,1}));
    % ind = ind(allsigCor);
    allh1 = zeros(length(ind),2);
    allh2 = zeros(length(ind),2);
    mfr1 = zeros(2,474,4);
    mfr2 = zeros(2,474,4);
    allcoef =zeros(2,474,4);
    sigallcoef =zeros(2,474,4,1000);
    binSize = 500;
    allhisto = zeros(474,2);
    c=1;
    for ii = 1 : length(ind)
        cd(obj1.data.setNames{ind(ii)})
        cd ..
        cd(sprintf('./group%04g/cluster%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7)))
        %     name1 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),3),obj1.data.Index(ind(ii),9),obj1.data.Index(ind(ii),7));
        %     name2 = sprintf('%04g%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8));
        allsp = cell(2,2);
        if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
            for rule = 1 : 2
                load(sprintf('nineStimPSTH%d',rule))
                allspt=cat(1,A{:,1});
                allspt2=cat(1,A{:,2});
                
                for b = 1 : 3;allsp{1,rule}(:,b) = sum(allspt(:,(b-1)*binSize+1:(b-1)*binSize+binSize),2); end
                allsp{1,rule}(:,4) = sum(allspt2(:,1300:1800),2);
                mfr1(rule,c,:) = 2* sum(allsp{1,rule},1)/ size(allsp{1,rule},1);
                %             p = ones(3,1);
                %             for nb = 1:3; [~,p(nb)] = lillietest(allsp(:,nb)); end%;plot(hist(allsp(:,nb)));pause; clf;end
                %             allh1(ii,rule) = sum(p<(0.05/(3))) > 0;
            end
        end
        cd ../..
        
        cd(sprintf('./group%04g/cluster%02g%s',obj1.data.Index(ind(ii),4),obj1.data.Index(ind(ii),10),obj1.data.Index(ind(ii),8)))
        if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
            for rule =1 : 2
                load(sprintf('nineStimPSTH%d',rule))
                allspt=cat(1,A{:,1});
                allspt2=cat(1,A{:,2});
                for b = 1 : 3;allsp{2,rule}(:,b) = sum(allspt(:,(b-1)*binSize+1:(b-1)*binSize+binSize),2); end
                allsp{2,rule}(:,4) = sum(allspt2(:,1300:1800),2);
                mfr2(rule,c,:) = 2* sum(allsp{2,rule},1)/ size(allsp{2,rule},1);
                %             p = ones(3,1);
                %             for nb = 1:3; [~,p(nb)] = lillietest(allsp(:,nb));end
                %             allh2(ii,rule) = sum(p<(0.05/(3))) > 0;
            end
        end
        if obj1.data.Index(ind(ii),3) ~= obj1.data.Index(ind(ii),4)
            for rule = 1 : 2
                for b = 1 : 4
                    R = corrcoef(allsp{1,rule}(:,b),allsp{2,rule}(:,b));
                    allcoef(rule,c,b) = R(1,2);
                    nn = size(allsp{1,rule}(:,b),1);
                    parfor it = 1 : 1000
                        R = corrcoef(allsp{1,rule}(randperm(nn),b),allsp{2,rule}(randperm(nn),b));
                        sigallcoef(rule,c,b,it) = R(1,2);
                        
                    end
                end
            end
            allhisto(c,:) = obj1.data.hist(ind(ii),:);
            selind(c) = ii;
            c=c+1;
        end
        cd ../..
        ii
    end
    cd /Volumes/raid/data/monkey/
    save temp89.mat
else
    cd /Volumes/raid/data/monkey/
    load temp89.mat
end
f1 = figure;
f2 = figure;
f3 = figure;

areas = {'6DR ' '8AD ' '8B  ' 'dFC ' 'vFC ' 'PS  ' 'AS  ' 'PEC ' 'PGM ' 'PE  ' 'PG  ' 'MIP ' 'LIP ' 'PEcg' 'IPS ' 'WM ' '9L  '};
%           1       2   3       4       5                   8            10
%                                                                               11     12      13
%
pairsh = {[4 4]        [4 8]    [8 8]    [4 11]  [11 8]   [11 11]  [4 13]    [8 13]      [2 3]    [3 3]   [2 10]    [3 10] [10 10]};
pairsL = {'dFC-dFC' 'dFC-PEC' 'PEC-PEC' 'dFC-PG' 'PEC-PG' 'PG-PG' 'dFC-LIP'   'PEC-LIP' '8AD-8B' '8B-8B'  '8AD-PE' '8B-PE' 'PE-PE'};
conf = prctile(sigallcoef,99.5,4);
sigcoef =  allcoef > squeeze(conf(:,:,:));
allsigCor=selind(sum(sum(sigcoef,1),3) > 0);

for np = 1 : 8
    nind = ismember(allhisto,pairsh{np},'rows') | ismember(allhisto,fliplr(pairsh{np}),'rows');
    sind = find(nind==1);
    
    for rule = 1 : 2
        for b = 1 : 4
            y = allcoef(rule,sind,b);
            ysig = sigcoef(rule,sind,b);
            
            figure(f2)
            subplot(2,4,(rule-1)*4+b)
            plot(2*np+rand([1 length(y(~ysig))]),y(~ysig),'b.')
            hold on
            plot(2*np+rand([1 length(y(ysig))]),y(ysig),'r.')
            %             line([np np],[mean(y) - std(y)/sqrt(length(y)) mean(y) + std(y)/sqrt(length(y))])
            %             boxplot(yy,gr)
            ylim([-0.4 0.8])
            figure(f1)
            subplot(2,4,(rule-1)*4+b)
            bar(np,sum(ysig)/length(y));
            hold on
            ylim([0 0.6])
        end
    end
    
    figure(f3)
    subplot(2,1,1)
    sigp = sum(sigcoef(:,sind,:),3);
    bar([np-0.2],sum(sigp(1,:) > 1,2)/length(y),0.3,'b')
    hold on
    bar([np+0.2],sum(sigp(2,:) > 1,2)/length(y),0.3,'r')
    subplot(2,1,2)
    sigp = sum(sigcoef(:,sind,:),1);
    bar([np-0.2:0.2:np+0.4],squeeze(sum(sigp > 0,2)/length(y))',0.4,'b')
    hold on
    
end

figure(f2)
epochs = {'presample' 'sample' 'delay1' 'delay2'};
for sb = [5 6 7 8], subplot(2,4,sb); xlabel(epochs{sb-4}); end
subplot(2,4,1); ylabel('Identity');
subplot(2,4,5); ylabel('Location');


figure(f3)
for sb = 1 : 2
    subplot(2,1,sb)
    set(gca,'XTick',[1:8])
    
    set(gca,'XTickLabel',pairsL(1:8))
    xlabel('Cortical area pairs')
    
end

%% performance

spN = [4 8 11 13];
% spN = [1 2 3 4 8 10 11 13];
%  spN = [1 2 3 10];
srow = find(sum(ismember(pairs,spN),2) == 2);
spairs = pairs(srow,:);
[~,ind] = get(obj1,'Number','hist',spairs);
ind = intersect(ind,sindIDE);
pairs = obj1.data.hist(ind(allsigCor),:);
pairs = unique(pairs,'rows');
ii=1;
while ii < size(pairs,1)
    if sum(ismember(fliplr(pairs),pairs(ii,:),'rows')) > 0 && diff(pairs(ii,:)) ~= 0
        pairs(ii,:) = [];
    else
        ii = ii + 1;
    end
    
end
areas = {'6DR ' '8AD ' '8B  ' 'dFC ' 'vFC ' 'PS  ' 'AS  ' 'PEC ' 'PGM ' 'PE  ' 'PG  ' 'MIP ' 'LIP ' 'PEcg' 'IPS ' 'WM ' '9L  '};
pairsArea = cell(size(pairs,1),1);
n = zeros(2,size(pairs,1));
for up = 1 : size(pairs,1)
    n(1,up) = sum(ismember(obj1.data.hist(ind,:),pairs(up,:),'rows')) + sum(ismember(obj1.data.hist(ind,:),fliplr(pairs(up,:)),'rows'));
end
[allNegsig,allPossig,allR] = getPerfXcorrSpike(obj1,ind(allsigCor),'minCoef',0,'addName','epoch');
ind = ind(allsigCor);
figure

for r = 1 : 2
    for sb = 1 : 4
        pairsArea = cell(size(pairs,1),1);
        sn = zeros(2,size(pairs,1));
        tot = zeros(2,size(pairs,1));
        sigPerf{1} = find(squeeze(allNegsig(r,:,sb)) >0);
        sigPerf{2} = find(squeeze(allPossig(r,:,sb)) >0);
        %         sigPerf{1} = intersect(sigCor{r,1},sigPerf{1});
        %         sigPerf{2} = intersect(sigCor{r,1},sigPerf{2});
        for up = 1 : size(pairs,1)
            tot(1,up) = sum(ismember(obj1.data.hist(ind,:),pairs(up,:),'rows')) + sum(ismember(obj1.data.hist(ind,:),fliplr(pairs(up,:)),'rows'));
            tot(2,up) = tot(1,up);
            
            sn(1,up) = sum(ismember(obj1.data.hist(ind(sigPerf{1}),:),pairs(up,:),'rows') | ismember(obj1.data.hist(ind(sigPerf{1}),:),fliplr(pairs(up,:)),'rows'));
            
            sn(2,up) = sum(ismember(obj1.data.hist(ind(sigPerf{2}),:),pairs(up,:),'rows') | ismember(obj1.data.hist(ind(sigPerf{2}),:),fliplr(pairs(up,:)),'rows'));
            
            if n(1,up) <30
                sn(:,up) =zeros(2,1);
            end
            pairsArea{up} = [areas{pairs(up,1)} areas{pairs(up,2)}];
        end
        subplot(4,2,(sb-1)*2+r)
        
        bar(100*(sn./repmat(n(1,:),2,1))')
        hold on
        %         line([0 11],[3.3 3.3],'LineStyle','--')
        
        set(gca,'XTick',[1:size(pairs,1)])
        
        set(gca,'XTickLabel',pairsArea(:))
        ylim([0 25])
        xlim([0 size(pairs,1)+1])
    end
end
rules = {'IDE' 'LOC'};
for sb = 7 : 8
    subplot(4,2,sb)
    xlabel(['Cortical area pairs ' rules{sb-6}])
end
ylabel('% pairs ')
c=1;
epochs = {'presample' 'sample' 'delay1' 'delay2'};
for sb = [1:2:7]
    subplot(4,2,sb)
    title(epochs{c})
    c=c+1;
end
legend('Negative corr.','Positive corr.')
for up = 1 : size(pairs,1)
    
    text(up,5,['n=' num2str(tot(1,up))])
end

%%
alln = zeros(2,2,size(pairs,1));
ualln = zeros(2,size(pairs,1));
for r = 1 : 2
    [sigPerf{1},~] = find(squeeze(allNegsig(r,:,:)) >0);
    [sigPerf{2},~] = find(squeeze(allPossig(r,:,:)) >0);
    sigPerf{1} = unique(sigPerf{1});
    sigPerf{2} = unique(sigPerf{2});
    for up = 1 : size(pairs,1)
        alln(r,1,up) = sum(ismember(obj1.data.hist(ind(sigPerf{1}),:),pairs(up,:),'rows') | ismember(obj1.data.hist(ind(sigPerf{1}),:),fliplr(pairs(up,:)),'rows'));
        
        alln(r,2,up) = sum(ismember(obj1.data.hist(ind(sigPerf{2}),:),pairs(up,:),'rows') | ismember(obj1.data.hist(ind(sigPerf{2}),:),fliplr(pairs(up,:)),'rows'));
    end
    
    
end

figure;
for r = 1 : 2
    subplot(2,2,r)
    nn = squeeze(alln(r,:,:));
    
    bar(cat(1,n(1,:),nn)')
    set(gca,'XTick',[1:size(pairs,1)])
    
    set(gca,'XTickLabel',pairsArea(:))
    ylim([0 155])
    xlim([0 size(pairs,1)+1])
    title(rules{r})
    ylabel('# pairs ')
    
    subplot(2,2,2+r)
    nn = squeeze(alln(r,:,:));
    %     nn(repmat(n(1,:),2,1) <30) = 0;
    %     bar(100*(sum(nn,1)./n(1,:))','g')
    
    hold on
    
    bar(100*(nn ./repmat(n(1,:),2,1))')
    nnStat{r} = 100*(nn ./repmat(n(1,:),2,1));
    set(gca,'XTick',[1:size(pairs,1)])
    
    set(gca,'XTickLabel',pairsArea(:))
    ylim([0 60])
    xlim([0 size(pairs,1)+1])
    title(rules{r})
    ylabel('% pairs ')
    
end
subplot(2,2,2)
legend('Sample size','Negative corr.','Positive corr.')
subplot(2,2,4)
legend('sig. corr','Negative corr.','Positive corr.')
anovad = cat(2,reshape(nnStat{1},1,size(nnStat{1},2)*2),reshape(nnStat{2},1,size(nnStat{1},2)*2));
grs{1} = cat(2,[1:size(nnStat{1},2)],[1:size(nnStat{1},2)],[1:size(nnStat{1},2)],[1:size(nnStat{1},2)]);
grs{2} = cat(2,ones(1,size(nnStat{1},2)*2),ones(1,size(nnStat{1},2)*2)+1);
grs{3} = cat(2,ones(1,size(nnStat{1},2)),ones(1,size(nnStat{1},2))+1,ones(1,size(nnStat{1},2)),ones(1,size(nnStat{1},2))+1);
p = anovan(anovad,grs);


figure
x = cell(4,1);
y = cell(4,1);
for ep = 1 : 4;
    x{ep} = squeeze(allR(1,logical(squeeze(allNegsig(1,:,ep))),ep));
    y{ep} = squeeze(allR(2,logical(squeeze(allNegsig(1,:,ep))),ep));
    subplot(1,4,ep);plot(squeeze(allR(1,logical(squeeze(allNegsig(1,:,ep))),ep)),squeeze(allR(2,logical(squeeze(allNegsig(1,:,ep))),ep)),'.');
    hold on;
end

for ep = 1 : 4;
    x{ep} = cat(2,x{ep},squeeze(allR(1,logical(squeeze(allPossig(1,:,ep))),ep)));
    y{ep} = cat(2,y{ep},squeeze(allR(2,logical(squeeze(allPossig(1,:,ep))),ep)));
    subplot(1,4,ep);plot(squeeze(allR(1,logical(squeeze(allPossig(1,:,ep))),ep)),squeeze(allR(2,logical(squeeze(allPossig(1,:,ep))),ep)),'.');
    hold on;
end

for ep = 1 : 4;
    x{ep} = cat(2,x{ep},squeeze(allR(1,logical(squeeze(allNegsig(2,:,ep))),ep)));
    y{ep} = cat(2,y{ep},squeeze(allR(2,logical(squeeze(allNegsig(2,:,ep))),ep)));
    subplot(1,4,ep);plot(squeeze(allR(1,logical(squeeze(allNegsig(2,:,ep))),ep)),squeeze(allR(2,logical(squeeze(allNegsig(2,:,ep))),ep)),'.');
    hold on;
end

for ep = 1 : 4;
    
    x{ep} = cat(2,x{ep},squeeze(allR(1,logical(squeeze(allPossig(2,:,ep))),ep)));
    y{ep} = cat(2,y{ep},squeeze(allR(2,logical(squeeze(allPossig(2,:,ep))),ep)));
    subplot(1,4,ep);plot(squeeze(allR(1,logical(squeeze(allPossig(2,:,ep))),ep)),squeeze(allR(2,logical(squeeze(allPossig(2,:,ep))),ep)),'.');
    hold on;
end

for ep = 1 : 4; subplot(1,4,ep);axis([-1 1 -1 1]); end
xlabel('IDE')
ylabel('LOC')

mdl = cell(4,1);
for ep = 1 : 4;
    
    
    subplot(1,4,ep)
    [r,m,b] = regression(x{ep},y{ep});
    line([-1 1],[-1*m+b m+b]);
    mdl{ep} = LinearModel.fit(x{ep},y{ep});
    title([epochs{ep} '; slope ' num2str(m) '; p=' num2str(mdl{ep}.anova.pValue(1))]);
end



