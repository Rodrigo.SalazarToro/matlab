clear all
cd /Volumes/raid/data/monkey

% psth = loadObject('psthObjIDEandLong999RT20ms.mat');
psth = loadObject('psthIDEandLong99bin100msIdeBased.mat');
cortex{1} = [1:5 17];
cortex{2} = [8:13];
[r,ind2] = get(psth,'Number','cortex',cortex{2});%,'ideTuned',1,'epochTuned',1);
list = psth.data.setNames(ind2);
clear sfc
c = 1;

clear sfcPFC sfcPFCSur 

for ii = 1 : length(ind2) % PPC
    cd(list{ii})
%     files = nptDir('SFConlyDelayall1g00*Rule1.mat');
    files = nptDir('SFCgramall1g00*Rule1.mat');
%     filesS = nptDir('SFConlyDelayall1g00*Rule1Sur.mat');
    clear groups
    if ~isempty(files) %&& ~isempty(filesS)
        
         for ff = 1 : length(files);groups(ff) = str2num(files(ff).name(14:16)); end
        
        if isempty(strfind(pwd,'betty'))% clark
            interSFC = find(groups >8);
        else
            interSFC = find(groups <=32);
        end
        
        for ff = 1 : length(interSFC)
            load(files(interSFC(ff)).name)
%              load(filesS(interSFC(ff)).name)
             
            if ~isempty(C)
                sfcPPC(c,:,:) = C;
%                 sfcPPCSur(c,:,:) = squeeze(prctile(Prob,99.9,3));
                c = c + 1;
            end
        end
    end
    
end

[r,ind2] = get(psth,'Number','cortex',cortex{1});%,'ideTuned',1,'epochTuned',1);
list = psth.data.setNames(ind2);
c = 1;
clear sfcPFC sfcPFCSur
for ii = 1 : length(ind2) % PFC
    cd(list{ii})
%     files = nptDir('SFConlyDelayall1g00*Rule1.mat');
     files = nptDir('SFCgramall1g00*Rule1.mat');
%     filesS = nptDir('SFCgramall1g00*Rule1Sur.mat');
    clear groups
    if ~isempty(files) % && ~isempty(filesS)
        
         for ff = 1 : length(files);groups(ff) = str2num(files(ff).name(14:16)); end
        
        if isempty(strfind(pwd,'betty'))% clark
            interSFC = find(groups <=8);
        else
            interSFC = find(groups >32);
        end
        
        for ff = 1 : length(interSFC)
            load(files(interSFC(ff)).name)
%              load(filesS(interSFC(ff)).name)
             
            if ~isempty(C)
                sfcPFC(c,:,:) = C;
%                 sfcPFCSur(c,:,:) = squeeze(prctile(Prob,99.9,3));
                c = c + 1;
            end
        end
    end
    
end
figure; 

subplot(2,1,1);
% plot(f,squeeze(prctile(sfcPPC,[10 25 50 75 90],1))) ;%colorbar
% caxis([0 0.04])
title('PPC cells')
ylim([0 0.07])
% subplot(4,1,2);
% 
% sigSFC = squeeze(sum(sfcPPC > sfcPPCSur,1)) / size(sfcPPC,1);
imagesc(time,f,squeeze(nanmean(sfcPPC,1))') ;colorbar
% caxis([0 0.3])


subplot(2,1,2);
% plot(f,squeeze(prctile(sfcPFC,[10 25 50 75 90],1))) ;%colorbar
ylim([0 0.07])

% caxis([0 0.04])
title('PFC cells')
% subplot(4,1,4);

% sigSFC = squeeze(sum(sfcPFC > sfcPFCSur,1)) / size(sfcPFC,1);
imagesc(time,f,squeeze(nanmean(sfcPFC,1))') ;colorbar
% caxis([0 0.3])
% xlabel('time [sec]')
% ylabel('% sig')

for ii = 1 : size(sfcPFC,1)
    imagesc(time,f,squeeze(sfcPFC(ii,:,:))') ;colorbar
    pause
end
    