% files1 = nptDir('Two_Column_Results_2013__8_14_19*.mat');
% files2 = nptDir('Two_Column_Results_2013__8_14_20*.mat');
% files = [files1; files2];
% lfp = zeros(1001,length(files)*2);

runs = {'nostims_nolearn' 'nostims_learn'};
params = struct('Fs',1000,'fpass',[1 120],'tapers',[3 5],'trialave',0);
clear sumsp
for ty = 1 : 2
    fig(ty) = figure;
    files = eval(runs{ty});
    
    c=1;
    clear lfp
    st = 1;
    for ff = 1 : length(files)
        [LFP_column_1,LFP_column_2,rr]=read_a_run(files{ff},0);
        %     nlfp = (LFP_column_1(st:end)- mean(LFP_column_1(st:end))) / std(LFP_column_1(st:end));
        %     [lfpt , rs] = nptLowPassFilter(nlfp,1000,5,90);
        %     lfp(:,c) = lfpt(1:1001);
        %     c=c+1;
        %                nlfp = (LFP_column_1(st:end)- mean(LFP_column_1(st:end))) / std(LFP_column_1(st:end));
%         nlfp = LFP_column_1(1:end);
%         %         [lfpt , rs] = nptLowPassFilter(nlfp,1000,1,120);
%         lfpt =nlfp;
%         lfp(:,c) = lfpt(1:end);
        sumsp(ty,c,:) = sum(full(rr.rastlist.Pool_1_E_rast),2);
        c=c+1;
    end
    
%     [S,t,f]=mtspecgrampb(lfp,[0.3 0.02],params);
%     subplot(2,1,1)
%     imagesc(t,f,log10(squeeze(mean(S,3)))')
%     colorbar
%     title('Mean')
%     ylabel('Frequency (Hz)')
%     
%     subplot(2,1,2)
%     imagesc(t,f,log10(squeeze(std(S,[],3)))')
%     colorbar
%     title('Standard Deviation')
%     set(fig(ty),'Name',runs{ty})
%     xlabel('Time (sec)')
%     ylabel('Frequency (Hz)')
%     subplot(2,1,1); caxis([3 4.5])
%     subplot(2,1,2); caxis([3 4.5])
end



files = eval(runs{1});
c=1;
clear lfp
st = 1;
for ff = 1 : length(files)
    [LFP_column_1,LFP_column_2]=read_a_run(files{ff},0);
    %     nlfp = (LFP_column_1(st:end)- mean(LFP_column_1(st:end))) / std(LFP_column_1(st:end));
    %     [lfpt , rs] = nptLowPassFilter(nlfp,1000,5,90);
    %     lfp(:,c) = lfpt(1:1001);
    %     c=c+1;
    %                nlfp = (LFP_column_1(st:end)- mean(LFP_column_1(st:end))) / std(LFP_column_1(st:end));
    nlfp = LFP_column_1(1:end);
    %         [lfpt , rs] = nptLowPassFilter(nlfp,1000,1,120);
    lfpt =nlfp;
    lfp(:,c) = lfpt(1:end);
    c=c+1;
end

[S,t,f]=mtspecgrampb(lfp,[0.3 0.02],params);
figure
for ff =1 : size(S,3)
    subplot(2,1,1)
    plot(lfp(:,ff))
    subplot(2,1,2)
    imagesc(t,f,log10(squeeze(S(:,:,ff))'))
    caxis([3 4.5])
    colorbar
    title(ff)
    pause
    clf
end