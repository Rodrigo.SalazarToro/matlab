load 20150803prctPPI.mat
gr = [1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1];
labs = {'k' 'r'};

conds = {'CONT','SALB'};
subplot(3,1,1)
for dr = 0:1
   plot(mean(ppi(:,gr ==dr),2) - std(ppi(:,gr ==dr),[],2)/sqrt(sum(gr ==dr)),labs{dr+1}); 
   hold on; 
   plot(mean(ppi(:,gr ==dr),2) + std(ppi(:,gr ==dr)/sqrt(sum(gr ==dr)),[],2),labs{dr+1})
    
   ylim([-20 80])
end
set(gca,'XTick',[1 2 3 4])

legend('CONT','CONT','SALB','SALB')


for dr = 0:1
    subplot(3,1,2+ dr)
    plot(ppi(:,gr ==dr)); 
    ylim([-20 80])
    set(gca,'XTick',[1 2 3 4])
    title(conds{dr+1})
end

set(gca,'XTickLabel',[5 10 15 20])
xlabel('Prepulse intensities (dB)')
ylabel('% PPI')
