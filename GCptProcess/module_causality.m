% We have the spectrum evaluated from the binned data using multitaper 
% estimation or any other method. Using these estimates, we will find transfer function
% and error covariance matrix using Wilson's algorithm. Then evaluate
% Granger causality for different pairs of channels.
%INPUT:- Sp: spectral density matrix where Sp(i,j,k) is cross spectral
%density between ith and jth channel/neuron at kth frequency.
% f: array containing frequency at which spectrums are evaluated.
% dt: Sampling period in seconds.
% OUTPUT:-  causality is a matrix such that causality(i,j)contains Granger causality
% from ith channel/neuron to jth channel/neuron summed over all frequency. Note that for
% convenience we put -1 at all diagonal entries.

function [causality]= module_causality(Sp,f,dt)
[s1,s2,s3]=size(Sp);
% Sampling frequency
Fs = 1/dt;
for i=1:(s1-1)
    causality(i,i)=-1;
    for j=i+1:s1
        [Snew,Hnew,Znew] = sfactorization_wilson1(Sp([i,j],[i,j],:),f(1:s3),Fs);
        [Fx2y,Fy2x,pp1,pp2]=pwcausalr_c(Snew,Hnew,Znew,f(1:s3),Fs);
        causality(i,j)= sum(Fx2y); 
        causality(j,i)= sum(Fy2x);
    end
end
causality(s1,s1)=-1;
