function [pp,cohe,Fxy,Fyx]=pttcausal(x,Nr,Nl,porder,fs,freq)
% Causality analysis using our partition matrix method
% x input data with L row for channels, N column for Nr trials with Nl points per trial
% porder is the model order of AR fitting
% fs is the sampling frequency
% freq is frequency range you want to get
% cohe is output coherence spectrum, cohe(i,j,:) is the coherence between i and j channels
% Ixy is the causality from x channel to y channel, Iyx is from y to x
% the order of Ixy/Iyx is 1 to 2:L, 2 to 3:L,....,L-1 to L.  That is,
%  1st column: 1&2; 2nd: 1&3; ...; (L-1)th: 1&L; ...; (L(L-1))th: (L-1)&L.

% Finished July 2003 by Yonghong Chen

[L,N]=size(x);
[A,Z]=armorf(x,Nr,Nl,porder);  %using my own code

f_ind  = 0;
    for f  = freq
        f_ind = f_ind + 1;
        % Spectrum Matrix
        [S,H] = spectrum(A,Z,porder,f,fs); 
        % Power spectrum (N.B.: this is the two-sided power spectrum. For 1-sided multiply S by 2).
        
        pp(:,f_ind)   = real(diag(S)) .*2 ;
        
        % Coherence spectrum
        
        num              = abs(S).^2;
        denum            = repmat(diag(S),1,L) .* repmat(diag(S),1,L)';
        cohe(:,:,f_ind) = real( num ./ denum );
        
        %DTF measure of causality
        for i=1:L
            for j=1:L
                theta(i,j,f_ind) = abs(H(i,j))^2;
            end
        end
        
        
        index=0;
        for i=1:L-1
            for j=i+1:L
                index=index+1
                cohall(index,f_ind)=squeeze(cohe(i,j,f_ind));     
                [H1,H2,H3,H4]=pttmatrx(H,i,j);
                [Z1,Z2,Z3,Z4]=pttmatrx(Z,i,j);
                BB=inv(H1)*H2;
                ZZ=Z1+2*real(BB*Z3)+BB*Z4*BB';
                eyx=ZZ(2,2)-abs(ZZ(1,2))^2/ZZ(1,1);
                exy=ZZ(1,1)-abs(ZZ(2,1))^2/ZZ(2,2);
                Iyx(index,f_ind)=abs(H(i,j))^2*real(eyx)/abs(S(i,i))/fs; %measure within [0,1]
                Ixy(index,f_ind)=abs(H(j,i))^2*real(exy)/abs(S(j,j))/fs;
                Fyx(index,f_ind)=log(abs(S(i,i))/abs(S(i,i)-H(i,j)*eyx*conj(H(i,j))/fs));%Geweke's original measure
                Fxy(index,f_ind)=log(abs(S(j,j))/abs(S(j,j)-H(j,i)*exy*conj(H(j,i))/fs));
            end
        end        
    end