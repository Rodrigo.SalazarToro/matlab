% Here we demostrate the causality and conditional causality analysis of a
% toy_data. The toy_data consists of 3 channels, 100 trials and 101 bins
% with sampling interval 10ms.

clear;
clc;
dt =0.01;
fs = 1/dt;
% Here channel 1 drives 2 and 2 drives 3.
load toy_data1.mat;
data = toy_data1;

% Here channel 1 drives 2 and 3.
% load toy_data.mat;
% data = toy_data;


chns = (1:3);
bins = (2:101);
% Evaluation of spectrum
[Sp,f]= module_spectrum(data,dt,chns,bins);
[s1,s2,s3]=size(Sp);
% Evaluation of G. causality
[causality]= module_causality(Sp,f,dt);
disp('causality')
disp(causality)
freq =f(1:s3);
% Evaluation of conditional G. causality
[Snew,Hnew,Znew] = sfactorization_wilson1(Sp,freq,fs);
% j drives i conditional on k;
j=1;
i=2;
k=3;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

j=2;
i=1;
k=3;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

j=1;
i=3;
k=2;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

j=3;
i=1;
k=2;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

j=2;
i=3;
k=1;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

j=3;
i=2;
k=1;

Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq);
cdcausal(j,i)=sum(Cj2i);

for i = 1:3
    cdcausal(i,i)= -1;
end
disp('Condn G.causality')
disp(cdcausal)