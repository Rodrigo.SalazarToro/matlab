function Cj2i=cdcausalr1(Snew,Hnew,Znew,i,j,k,fs,freq)
%  Using Geweke's method to get conditional causality (j drives i conditional k)
%  Partition matrix is employed to modify the method

% Finished in Sept. 2003 by Yonghong Chen

% xxx=[x(i,:);x(j,:);x(k,:)];
% [A3,Z3]=armorf(xxx,Nr,Nl,porder);  %using my own code
f_ind  = 0;
        Z3 = [Znew(i,i) Znew(i,j) Znew(i,k)
              Znew(j,i) Znew(j,j) Znew(j,k)
              Znew(k,i) Znew(k,j) Znew(k,k)];

    for f  = freq
        f_ind = f_ind + 1;
        % Spectrum Matrix
        S2 = Snew(:,:,f_ind);
        H2 = Hnew(:,:,f_ind);
        S3 = [S2(i,i) S2(i,j) S2(i,k)
              S2(j,i) S2(j,j) S2(j,k)
              S2(k,i) S2(k,j) S2(k,k)];
        H3 = [H2(i,i) H2(i,j) H2(i,k)
              H2(j,i) H2(j,j) H2(j,k)
              H2(k,i) H2(k,j) H2(k,k)];
              
        %[S3,H3] = spectrum(A3,Z3,porder,f,fs);
        cc=Z3(1,1)*Z3(2,2)-Z3(1,2)^2;
        P=[1,0,0;-Z3(1,2)/Z3(1,1),1,0;(Z3(1,2)*Z3(2,3)-Z3(2,2)*Z3(1,3))/cc,(Z3(1,2)*Z3(1,3)-Z3(1,1)*Z3(2,3))/cc,1];
%         P1=[1,0,0;-Z3(1,2)/Z3(1,1),1,0;-Z3(1,3)/Z3(1,1),0,1];
%         P2=[1,0,0;0,1,0;0,-(Z3(1,1)*Z3(2,3)-Z3(1,2)*Z3(1,3))/cc,1];
%         P=P2*P1;
        ZZ=P*Z3*P';
        HH=H3*inv(P);%inv(P*inv(H3));
        
        [g1,g2,g3,g4]=pttmatrx(H3,1,3); %using partition matrix to get fitted model for two time series
        [e1,e2,e3,e4]=pttmatrx(Z3,1,3);
        gg=inv(g1)*g2;
        Z2=e1+2*real(gg)*e3+gg*e4*gg';
        H2=g1;
        S2=H2*Z2*H2';
        Q=[1,0;-Z2(1,2)/Z2(1,1),1];
        B=Q*inv(H2);
        BB=[B(1,1),0,B(1,2);0,1,0;B(2,1),0,B(2,2)];
        FF=BB*HH;
        Cj2i(f_ind)=log(abs(Z2(1,1))/abs(FF(1,1)*Z3(1,1)*conj(FF(1,1)))); %Geweke's original measure
        Ij2i(f_ind)=abs((Z2(1,1)-FF(1,1)*Z3(1,1)*conj(FF(1,1))))/abs(Z2(1,1)); %measure within [0,1]
        
    end



