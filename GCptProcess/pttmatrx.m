function [B11,B12,B21,B22]=pttmatrx(H,i,j)
% partition matrix into 2*2 blocks
L=length(H);
B1=H;
B2=H;
B11=reshape([H(i,i),H(j,i),H(i,j),H(j,j)],2,2);
B1(i,:)=[];
if j<i
    B1(j,:)=[];
else
    B1(j-1,:)=[];
end
B21=[B1(:,i),B1(:,j)];
B1(:,i)=[];
if j<i
    B1(:,j)=[];
else
    B1(:,j-1)=[];
end
B2(:,i)=[];
if j<i
    B2(:,j)=[];
else
    B2(:,j-1)=[];
end
B12=[B2(i,:);B2(j,:)];
B22=B1;
