% Here using the binned data, we evaluate spectrum, cross spectrum for
% different channels. 
% INPUT: data in the format channels x trial x bins i.e in case of spike train data, data(i,j,k)
% contains number of spikes by ith neuron in jth trial in kth bin. 
% dt: bin size in seconds
% chns: array containing channel numbers to be analysed.
% bins: array containing bin number to be analysed.
% here insted of neurons, one can consider it as channels and use the code.
% NOTE that here we are taking array of channel numbers and bin number to be
% analysed because this method of evaluation of spectrum is only valid for
% stationary data and so for analysis we generally take data from short
% time window. The method is not valid for analysis of data from large time
% window as stationarity condition might not be true.

% OUTPUT:- Sp is the spectral density matrix where Sp (i,j,k) gives the
% cross spectral density between ith and jth channels/neurons from chns at kth
% frequency.
% f: array of frequencies at which spectral densities are evaluated.

% IN THIS CODE, WE HAVE ADAPTED THE CODE FOR CALCULATING SPECTRUM USING 
% MULTITAPER METHOD FOR BINNED DATA FROM CHRONUX.


function [Sp,f]= module_spectrum(data,dt,chns,bins)
data = data(chns,:,bins);
[nnrns, ntrials, nbins] = size(data);
data = data(:,2:ntrials,:);
ntrials = ntrials-1;
% Sampling frequency
Fs = 1/dt;
% No of points at which spectrum has to be evaluated.
nfft = 2^(nextpow2(nbins)+1);
% frequencies at which spectrum is evaluated.
df = Fs/nfft;
f = 0:df:Fs;
% evaluation of tapers.
[tapers,eigs] = dpss(nbins,3,8);
tapers = tapers*sqrt(Fs);
% make no copies of tapers equal to ntrials.
tapers = tapers(:,:,ones(1,ntrials));
% fft of tapers.
H = fft(tapers,nfft,1);
% evaluation of autospectrums and cross spectrums.
for i = 1:nnrns
    dat = squeeze(data(i,:,:));
    dat = permute(dat,[2,1]);
%   total spikes.
    Total_sp = squeeze(sum(dat,1));
%   average spiking rate.  
    A_sp = squeeze((Total_sp)'./nbins);
    clear Total_sp;
%   product of mean spiking rate and fft of tapers.  
    Avg_sp = A_sp(:,ones(1,8),ones(1,nfft));
    Avg_sp = permute(Avg_sp,[3,2,1]);
    H1 = H.*Avg_sp;
    clear Avg_sp, A_sp;
%   8 copies of data.  
    dat = dat(:,:,ones(1,8));
%   product of data and tapers.
    dat = permute(dat,[1,3,2]);
    dat_tap = dat.*tapers;
    clear dat;
%   fft of dat.tapers
    J1 = fft(dat_tap,nfft,1);
    clear dat_tap;
%   evaluation of fft of multi taper estimate for each neuron.
    J1 = J1 - H1;
    clear H1;
    J(:,:,:,i) = J1;
    clear J1;
end
    
for i = 1:nnrns
    for j = 1:nnrns
        I1 = squeeze(mean(J(:,:,:,i).*conj(J(:,:,:,j)),2));
        Sp(i,j,:) = squeeze(mean(I1,2));
        clear I1;
    end
end
clear dat;
