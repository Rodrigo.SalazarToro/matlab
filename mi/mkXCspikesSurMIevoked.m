function [crosscorr,timelag,S,S1,S2,phi,varargout] = mkXCspikesSurMIevoked(sp2,sp1,trials,mts,varargin)
% output crosscorr(epoch,timelag,trials,iterations);
Args = struct('save',0,'redo',0,'endtrial','MatchOnset','addName',[],'timeWindow',120,'step',6,'epochBased',0,'epochLength',500,'iterations',500);
Args.flags = {'save','redo','epochBased'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );


matfile = sprintf('xcorrg%s%sg%s%s%sSurMIevoked.mat',sp1.data.groupname,sp1.data.cellname,sp2.data.groupname,sp2.data.cellname,Args.addName);


if Args.epochBased
    nep = 4;
else
    nep = 1;
end
params = struct('Fs',10000,'fpass',[0 100],'tapers',[2 3],'trialave',1);
bins = [(-Args.timeWindow -Args.step ) [-Args.timeWindow  : Args.step : Args.timeWindow ] (Args.timeWindow  + Args.step)];


if isempty(nptDir(matfile)) || Args.redo
    crosscorr  = zeros(nep,length(bins),length(trials),Args.iterations);
    S = cell(4,1);
    S1 = cell(4,1);
    S2 = cell(4,1);
    phi = cell(4,1);
    for ii = 1 : Args.iterations
        trials1 = trials(randperm(length(trials)));
        trials2 = trials(randperm(length(trials)));
        cti = 1;
        clear allepochs1 allepochs2
        for ti = 1 : length(trials)
            if ~isempty(sp1.data.trial(trials1(ti)).cluster(1).spikes) && ~isempty(sp2.data.trial(trials2(ti)).cluster(1).spikes)
                if Args.epochBased
                    tlim(1,1) = mts.data.CueOnset(trials1(ti));
                    tlim(2,1) = mts.data.CueOffset(trials1(ti));
                    tlim(3,1) = mts.data.CueOffset(trials1(ti)) + Args.epochLength;
                    tlim(4,1) = mts.data.MatchOnset(trials1(ti));
                    
                    tlim(1,2) = mts.data.CueOnset(trials2(ti));
                    tlim(2,2) = mts.data.CueOffset(trials2(ti));
                    tlim(3,2) = mts.data.CueOffset(trials2(ti)) + Args.epochLength;
                    tlim(4,2) = mts.data.MatchOnset(trials2(ti));
                else
                    tlim(1) = mts.data.MatchOnset(trials(ti));
                end
                
                for ep = 1 : size(tlim,1)
                    selSpikes1 = find(sp1.data.trial(trials1(ti)).cluster(1).spikes > (tlim(ep,1) - Args.epochLength + Args.timeWindow) & sp1.data.trial(trials1(ti)).cluster(1).spikes < (tlim(ep,1) - Args.timeWindow));
                    selSpikes2 = find(sp2.data.trial(trials2(ti)).cluster(1).spikes > (tlim(ep,2) - Args.epochLength + Args.timeWindow) & sp2.data.trial(trials2(ti)).cluster(1).spikes < (tlim(ep,2) - Args.timeWindow));
                    if ~isempty(selSpikes1) && ~isempty(selSpikes2) && length(selSpikes1) > 1 && length(selSpikes2) > 1
                       
                        xct = zeros(1,length(bins));
                        for spi =  selSpikes1
                            rtimes = sp2.data.trial(trials2(ti)).cluster(1).spikes(selSpikes2) - sp1.data.trial(trials1(ti)).cluster(1).spikes(spi);
                            
                            if ~isempty(rtimes)
                                [n,~] = hist(rtimes,bins);
                                xct = xct + n;
                            end
                        end
                        
                        crosscorr(ep,:,ti,ii) =xct;
                        allepochs1(ep).trial(cti).times = vecc(sp1.data.trial(trials1(ti)).cluster(1).spikes(selSpikes1)/1000);
                        allepochs2(ep).trial(cti).times = vecc(sp2.data.trial(trials2(ti)).cluster(1).spikes(selSpikes2)/1000);
                        
                    end
                    
                end
                cti = cti + 1;
            end
        end
        
        f = [0:100];
        if exist('allepochs1','var') && exist('allepochs2','var')
            
            cc = 1;
            while isempty(allepochs1(cc).trial) && isempty(allepochs2(cc).trial)
                
                cc = cc + 1;
            end
            %             [St,freq,~]=mtspectrumpt(allepochs(cc).trial,params,0,[]);
            try 
            [St,phit,~,S1t,S2t,~,~,freq]=coherencypt(allepochs1(cc).trial,allepochs2(cc).trial,params);
            catch
                freq = [];
            end
            if length(freq) > 1
                S{cc}(ii,:) = interp1(freq,St,f);
                S1{cc}(ii,:) = interp1(freq,S1t,f);
                S2{cc}(ii,:) = interp1(freq,S2t,f);
                phi{cc}(ii,:) = interp1(freq,phit,f);
            else
                S{cc}(ii,:) = nan(1,length(f));
                S1{cc}(ii,:) = nan(1,length(f));
                S2{cc}(ii,:) = nan(1,length(f));
                phi{cc}(ii,:) = nan(1,length(f));
            end
            
            for ep = cc: length(tlim)
                if size(allepochs1,2) >= ep && ~isempty(allepochs1(cc).trial) && size(allepochs2,2) >= ep && ~isempty(allepochs2(cc).trial)
                    try
                        %                         [Stt,freq,R(ep)]=mtspectrumpt(allepochs(ep).trial,params,0,[]);
                        [Stt,phitt,~,S1t,S2t,~,~,freq]=coherencypt(allepochs1(ep).trial,allepochs2(ep).trial,params);
                        S{ep}(ii,:) = interp1(freq,Stt,f);
                        S1{ep}(ii,:) = interp1(freq,S1t,f);
                        S2{ep}(ii,:) = interp1(freq,S2t,f);
                        phi{ep}(ii,:) = interp1(freq,phitt,f);
                    end
                end
            end
        end
    end
    crosscorr = crosscorr(:,[2: end-1],:,:);
    
    %     crosscorr = crosscorr ./ sum(crosscorr);
    timelag = bins(2:end-1);
    crosscorr = single(crosscorr);
    
    if Args.save
        save(matfile,'crosscorr','timelag','trials','S','f','S1','S2','phi')
        display(sprintf('Saving %s %s',pwd,matfile))
    end
    
else
    load(matfile)
    
end





