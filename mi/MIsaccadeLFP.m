function varargout = MIsaccadeLFP(obj,ind,varargin)
% always saccade align

Args = struct('save',0,'redo',0,'surrogate',0,'iterations',1000,'ML',0,'minTrials',10,'type','saccade','beforeSac',200);
Args.flags = {'save','redo','surrogate','ML'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'surrogate'}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            );

params = struct('tapers',[2 3],'Fs',200,'fpass',[0 90],'err',[2 0.341],'trialave',1);

switch Args.type
    case 'saccade'
        nstim = 3;
    case 'rules'
        nstim = 2;
end
%%
sdir = pwd;

rn = length(ind);

for pair = 1 : rn
    
    cd(obj.data.setNames{ind(pair)})
    if Args.ML || isempty(findstr(obj.data.setNames{ind(pair)},'clark')); cd session01; nst = 1; nend = 1; else cd session02; nst = 2; nend = 3;end
    neuroinfo = NeuronalChAssign;
    if Args.surrogate
        theday = obj.data.Index(ind(pair),2);
        [~,goodpairs] = get(obj,'Number','snr',99);
        daypairs = find(obj.data.Index(:,2) == theday);
        pairss = intersect(daypairs,goodpairs);
        grPP = unique(obj.data.Index(pairss,10),'rows');
        grPF = unique(obj.data.Index(pairss,11),'rows');
        chPP = find(ismember(neuroinfo.groups,grPP) == 1);
        chPF = find(ismember(neuroinfo.groups,grPF) == 1);
        ch = [chPP chPF];
        nch = length(chPP) + length(chPF);
    else
        gr = obj.data.Index(ind(pair),10:11);
        for c = 1 : 2;ch(c) = find(neuroinfo.groups == gr(c)); nch = 2; end
        
    end
    if Args.surrogate
        matfile = sprintf('MIrespSurGen%s.mat',Args.type);
    else
        matfile = sprintf('MIrespg%04.0fg%04.0f%s.mat',gr(1),gr(2),Args.type);
    end
    cd ..
    if isempty(nptDir('MIsac')); mkdir MIsac; end
    cd MIsac
    if isempty(nptDir(matfile)) || Args.redo
        cd(obj.data.setNames{ind(pair)})
        
        tt=ones(1,nstim);
        dlfp = cell(1,nstim);
        c = 1;
        
        for ses = nst : nend;
            cd(sprintf('session0%d',ses))
            mts = mtstrial('auto','redoSetNames');
            seltrials =  mtsgetTrials(mts,'stable','BehResp',1,modvarargin{:});
            minRT(c) = round(min(mts.data.FirstSac(seltrials)));
            c = c+1;
            cd(obj.data.setNames{ind(pair)})
        end
        if isempty(Args.beforeSac)
            minRT = min(minRT);
        else
            minRT = Args.beforeSac;
        end
        for ses = nst : nend
            cd(sprintf('session0%d',ses))
            mts = mtstrial('auto','redoSetNames');
            
            
            switch Args.type
                case 'saccade'
                    
                    for sac = 1 : nstim
                        trials =  mtsgetTrials(mts,'iMatchSac',sac,'stable','BehResp',1,modvarargin{:});
                        cd lfp
                        files = nptDir('*_lfp.*');
                        
                        
                        for t = trials
                            [lfp,~,~,~,~] = nptReadStreamerChannel(files(t).name,ch);
                            if round(mts.data.FirstSac(t)) < 300
                                
                                
                                startSac = round(mts.data.MatchOnset(t)) + round(mts.data.FirstSac(t));
                                
                                for thec = 1 : nch
                                    [dlfp{sac}(thec,:,tt(sac)),~] = preprocessinglfp(lfp(thec,startSac - minRT : startSac),modvarargin{:},'detrend');
                                end
                                tt(sac) = tt(sac) + 1;
                            end
                        end
                        
                    end
                    
                case 'rules'
                    for r = 1 : nstim
                        trials =  mtsgetTrials(mts,'rule',r,'stable','BehResp',1,modvarargin{:});
                        if ~isempty(trials)
                            cd lfp
                            files = nptDir('*_lfp.*');
                            
                            for t = trials
                                [lfp,~,~,~,~] = nptReadStreamerChannel(files(t).name,ch);
                                if round(mts.data.FirstSac(t)) < 300
                                    startSac = round(mts.data.MatchOnset(t)) + round(mts.data.FirstSac(t));
                                    for thec = 1 : nch
                                        [dlfp{r}(thec,:,tt(r)),~] = preprocessinglfp(lfp(thec,startSac - minRT : startSac),modvarargin{:},'detrend');
                                    end
                                    tt(r) = tt(r) + 1;
                                end
                            end
                        end
                    end
            end
            cd(obj.data.setNames{ind(pair)})
        end
        C = cell(1,length(dlfp));
        Cerr = cell(1,length(dlfp));
        ProbStim = cell(1,length(dlfp));
        
        if Args.surrogate
            tt = tt -1;
            if length(dlfp) == 3
            for stim = 1 : length(dlfp); ndlfp = cat(3,dlfp{1},dlfp{2},dlfp{3});  end
            indtrial = {[1:tt(1)] [tt(1)+1:tt(1)+tt(2)] [tt(1)+tt(2)+1:tt(1)+tt(2)+tt(3)]};
            else
                for stim = 1 : length(dlfp); ndlfp = cat(3,dlfp{1},dlfp{2}); end
                indtrial = {[1:tt(1)] [tt(1)+1:tt(1)+tt(2)]};
            end
            [~,~,~,~,~,f,~,~,~] = coherencyc(squeeze(dlfp{stim}(1,:,:)),squeeze(dlfp{stim}(2,:,:)),params);
            muInfo = zeros(Args.iterations,length(f));
            for ii = 1 : Args.iterations
                
                clear rch
                rch(1) = randi(length(chPP));
                rch(2) = randi(length(chPF));
                randtrials = randperm(size(ndlfp,3));
                
                for stim = 1 : length(dlfp)
                   
                    [C{stim},~,~,~,~,f,~,~,Cerrt] = coherencyc(squeeze(ndlfp(rch(1),:,randtrials(indtrial{stim}))),squeeze(ndlfp(length(chPP)+rch(2),:,randtrials(indtrial{stim}))),params);
                    C{stim} = C{stim}';
                    Cerr{stim} = (diff(Cerrt,[],1)/2);
                    ProbStim{stim} = repmat(1/nstim,1,length(f));
                end
                
                muInfo(ii,:) = MIcont(C,Cerr,ProbStim,modvarargin{:});
            end
        else
            for stim = 1 : length(dlfp)
                [C{stim},~,~,~,~,f,~,~,Cerrt] = coherencyc(squeeze(dlfp{stim}(1,:,:)),squeeze(dlfp{stim}(2,:,:)),params);
                C{stim} = C{stim}';
                Cerr{stim} = (diff(Cerrt,[],1)/2);
                ProbStim{stim} = repmat(1/nstim,1,length(f));
            end
            
            muInfo = MIcont(C,Cerr,ProbStim,modvarargin{:});
        end
        
        if Args.save
            
            cd(obj.data.setNames{ind(pair)})
            
            cd MIsac
            if Args.surrogate
                save(matfile,'muInfo','f','grPP','grPF','Args')
            else
                save(matfile,'muInfo','f','gr','C','Cerr','Args')
            end
            display(['saving' ' ' matfile])
        end
    end
    
end
if nargout > 0
    varargout{1} = allmi;
    varargout{2} = f;
    varargout{3} = time;
end
cd(sdir)
%%
function rtrials = randTrials(trials,nstim)

stimN = zeros(nstim,size(trials,2));
rtrials = cell(nstim,size(trials,2));
alltrials = cat(2,trials{:});
randtrial= alltrials(randperm(length(alltrials)));
for ss = 1 : size(trials,2)
    for stim = 1: nstim;
        stimN(stim,ss) = length(trials{stim});
        if stim == 1
            rtrials{stim,ss} = randtrial(1:stimN(stim));
        else
            rtrials{stim,ss} = randtrial(stimN(stim-1)+ 1 :stimN(stim-1)+stimN(stim));
        end
    end
end