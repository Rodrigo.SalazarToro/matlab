function [mi,timelag,varargout] = mkXCspikesMI(unit1,unit2,varargin)
% in the xcorr folder
% output mi{1} for location tuning and mi{2} for identity tuning

Args = struct('save',0,'redo',0,'iterations',500,'rule',1,'SurMIevoked',0,'appendNewNCoi',0,'Incor',0);
Args.flags = {'save','redo','SurMIevoked','appendNewNCoi','Incor'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

basename = sprintf('xcorrg%sg%sRule%d',unit1, unit2,Args.rule);

if Args.SurMIevoked
    if Args.Incor
        filename = sprintf('%sIncorSurMIevoked.mat',basename);
    else
        filename = sprintf('%sSurMIevoked.mat',basename);
    end
else
    if Args.Incor
        filename = sprintf('%sIncorMI.mat',basename);
    else
        filename = sprintf('%sMI.mat',basename);
    end
end
if isempty(nptDir(filename)) || Args.redo
    
    allxc = [];
    indexes = [];% first column is objs and 2nd locs
    
    for objs = 1 : 3
        for locs = 1 : 3
            if Args.SurMIevoked
                if Args.Incor
                    lfile = sprintf('%sIde%dLoc%dIncorSurMIevoked.mat',basename,objs,locs);
                else
                    lfile = sprintf('%sIde%dLoc%dSurMIevoked.mat',basename,objs,locs);
                end
            else
                if Args.Incor
                    lfile = sprintf('%sIde%dLoc%dIncor.mat',basename,objs,locs);
                else
                    lfile = sprintf('%sIde%dLoc%d.mat',basename,objs,locs);
                end
            end
            if ~isempty(nptDir(lfile))
                load(lfile,'crosscorr','timelag')
                %                 if size(crosscorr,4) ~= 500
                %                    unix(['rm ' lfile])
                %                    display([pwd '/' lfile])
                %                 end
                %                 try
                allxc = cat(3,allxc,crosscorr);
                %                 catch
                %                    display(pwd)
                %                 end
                indexes = cat(1,indexes,[objs*ones(size(crosscorr,3),1) locs*ones(size(crosscorr,3),1)]);
            end
        end
    end
    
    mi = cell(2,1);% mi{1} = location tuning for each of the three objs
    
    if size(indexes,1) ~= 0
        ncoi = zeros(2,3,3);
        for tune = 1 : 2
            if Args.SurMIevoked
                
                mi{tune} = zeros(size(allxc,4),3,4,size(allxc,2));
            else
                mi{tune} = zeros(Args.iterations+1,3,4,size(allxc,2));
            end
            for item = 1 : 3
                sitem = find(indexes(:,tune) == item);
                if length(sitem) > 2
                    cstim = indexes(sitem,setdiff([1 2],tune));
                    if Args.appendNewNCoi
                        for items2 = 1: 3
                            inds = find(cstim == items2);
                            ncoi(tune,item,items2) = nansum(nansum(nansum(allxc(:,:,sitem(inds)))));
                            
                        end
                    else
                        for ep = 1 : 4
                            for tl = 1 : size(allxc,2)
                                
                                if Args.SurMIevoked
                                    for ii = 1 : size(crosscorr,4)
                                        sxc = double(squeeze(allxc(ep,tl,sitem,ii)));
                                        mi{tune}(ii,item,ep,tl) = mutualinfo(cstim,sxc);
                                    end
                                else
                                    sxc = squeeze(allxc(ep,tl,sitem));
                                    for ii = 1 : Args.iterations
                                        cstimr = cstim(randperm(length(cstim)));
                                        mi{tune}(ii,item,ep,tl) = mutualinfo(cstimr,sxc);
                                        
                                    end
                                    mi{tune}(ii+1,item,ep,tl) = mutualinfo(cstim,sxc);
                                    
                                    
                                end
                            end
                        end
                        for items2 = 1: 3
                            inds = find(cstim == items2);
                            ncoi(tune,item,items2) = nansum(nansum(nansum(allxc(:,:,sitem(inds)))));
                            
                        end
                    end
                end
                %                 ncoi(tune,item) = nansum(nansum(nansum(allxc(:,:,sitem))));
            end
            
        end
    else
        timelag = [];
        ncoi = [];
    end
    if Args.appendNewNCoi
        save(filename,'ncoi','-append')
        display([pwd '/' filename])
    else
        if Args.save
            if Args.SurMIevoked
                save(filename,'mi','timelag')
                display([pwd '/' filename])
            else
                save(filename,'mi','timelag','ncoi')
                display([pwd '/' filename])
            end
        end
    end
else
    load(filename)
    
end