function [migr,f,time,varargout] = migram(mts,ind,ch,varargin)
% assumes equal probability for the different stimuli
% to be run in the session folder


Args = struct('mwind',[0.2 0.05],'measure','C','fromCell',0,'matchAlign',0,'Fix',0,'Rules',0,'sessions',[],'surrogate',0,'fromJackknife',0,'appendS',0);
Args.flags = {'fromCell','matchAlign','Fix','Rules','surrogate','fromJackknife','appendS'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'surrogate','fromJackknife'});

nstim = length(ind);
C = cell(nstim,1);
phi = cell(nstim,1);
S12 = cell(nstim,1);
S1 = cell(nstim,1);
S2 = cell(nstim,1);

t = cell(nstim,1);
phistd = cell(nstim,1);
Cerr = cell(nstim,1);
tlimt = zeros(nstim,1);
mu = zeros(nstim,1);
sigma = zeros(nstim,1);

if Args.fromJackknife
    Pr = 0;
    Pr_s = cell(nstim,1);
    time = cell(nstim,1);
    for stim = 1 : nstim
        ntrials = length(ind{stim});
        C = zeros(500,37,17);
        for ii = 1 : 500
            orde=randi(ntrials,1,ntrials);
            [Ct,~,~,~,~,time{stim},f] = MTScohgram(mts,ind{stim}(orde),ch,'mwind',Args.mwind,'err',[0 0.05],modvarargin{:});
            C(ii,:,:) = Ct(1,1:37,:);
            
        end
        for fi = 1 : 17
            Pr_s{stim}(:,:,fi) = hist(squeeze(C(:,:,fi)),[0 : 0.01 : 1]); %Pr_s is r x freq x time
        end
        Pr_s{stim} =  Pr_s{stim} ./  repmat(sum(Pr_s{stim},1),size(Pr_s{stim},1),1);
        Pr = Pr + Pr_s{stim};
    end
    ProbStim = repmat(1/nstim,size(Pr_s{stim},2),size(Pr_s{stim},3));
    migr = 0;
    Pr = Pr ./ repmat(sum(Pr,1),size(Pr,1),1); % normalization
    for stim = 1 : nstim
        for r = 1:length([0 : 0.01 : 1])
            TM  = log2(squeeze(Pr_s{stim}(r,:,:)) ./ squeeze(Pr(r,:,:)));
            TM(isnan(TM)) = 0; % to avoid nans
            TM(isinf(TM)) = 0; % to avoid nans
            migr = migr + squeeze(Pr_s{stim}(r,:,:)) .* ProbStim .* TM;
        end
    end
    
    phi= [];
    phistd =[];
    Cerr =[];
else
    
    for stim = 1 : nstim
        if Args.matchAlign && ~Args.Rules
            [Ct,phit,~,~,~,t{stim},f,before,~,phistdt,Cerrt] = MTScohgram(mts,ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
        elseif Args.Fix
            if isempty(Args.sessions)
                %             [Ct,phit,~,~,~,t{stim},f,~,~,phistdt,Cerrt] = MTScohgram(mts,ind(stim,:),ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
                [Ct,phit,~,~,~,t{stim},f,~,~,phistdt,Cerrt] = MTScohgram(mts,ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
            else
%                 cd(sprintf('session0%d',Args.sessions(stim)))
                [Ct,phit,~,~,~,t{stim},f,~,~,phistdt,Cerrt] = MTScohgram(mts{stim},ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
%                 cd ..
            end
        elseif Args.Rules
            if Args.surrogate
                [Ct,phit,~,~,~,t{stim},f,before,~,phistdt,Cerrt] = MTScohgram(mts,ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],'fakeses', stim,modvarargin{:});
            else
                cd(Args.sessions{stim})
                [Ct,phit,~,~,~,t{stim},f,before,~,phistdt,Cerrt] = MTScohgram(mts{stim},ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
            end
        else
            if Args.appendS
                 [Ct,phit,S12t,S1t,S2t,t{stim},f] = MTScohgram(mts,ind{stim},ch,'mwind',Args.mwind,'err',[0 0.341],Args.mwind,modvarargin{:});
          
            else
                [Ct,phit,S12t,S1t,S2t,t{stim},f,~,~,phistdt,Cerrt] = MTScohgram(mts,ind{stim},ch,'mwind',Args.mwind,'err',[2 0.341],modvarargin{:});
            end
        end
        
        if Args.appendS
            C{stim} = squeeze(Ct);
            phi{stim} = squeeze(phit);
            S12{stim} = squeeze(S12t);
            S1{stim} = squeeze(S1t);
            S2{stim} = squeeze(S2t);
        else
            C{stim} = squeeze(Ct);
            phi{stim} = squeeze(phit);
            phistd{stim} = squeeze(phistdt);
            Cerr{stim} = squeeze(Cerrt);
            S12{stim} = squeeze(S12);
            S1{stim} = squeeze(S1);
            S2{stim} = squeeze(S2);
        end
        tlimt(stim) = length(t{stim});
        if Args.fromCell
            ProbStim{stim} = repmat(1/nstim,size(Ct,2),size(Ct,3));
            Cerr{stim} = diff(Cerrt,[],1)/2;
        end
    end
    
    [tlim,sstim] = min(tlimt);
    time = t{sstim};
    
    if Args.appendS
        migr = [];
    else 
    if Args.fromCell
        for stim = 1 : nstim; C{stim} = C{stim}(1:tlim,:); Cerr{stim} = Cerr{stim}(:,1:tlim,:); ProbStim{stim} = ProbStim{stim}(1:tlim,:);end
    else
        ProbStim = repmat(1/nstim,nstim,1);
    end
    
    if Args.fromCell
        
        migr = MIcont(C,Cerr,ProbStim,modvarargin{:});
    else
        
        migr = zeros(tlim,length(f));
        for fb = 1 : length(f)
            for ti = 1 : tlim
                for stim = 1 : nstim
                    if Args.measure == 'C'
                        mu(stim) = C{stim}(ti,fb);
                        sigma(stim) = diff(Cerr{stim}(:,ti,fb),[],1)/2;
                    else
                        mu(stim) = phi{stim}(ti,fb);
                        mu(stim) = mod(mu(stim) + 2*pi,2*pi) ;
                        sigma(stim) = phistd{stim}(ti,fb);
                        sigma(stim) = mod(sigma(stim) + 2*pi,2*pi) ;
                    end
                end
                migr(ti,fb) = MIcont(mu,sigma,ProbStim,modvarargin{:});
            end
        end
        
    end
    end
end
varargout{1} = C;
varargout{2} = phi;
varargout{3} = phistd;
varargout{4} = Cerr;

if Args.matchAlign
    varargout{5} = before;
else
    varargout{5} = [];
end

varargout{6} = S12;
varargout{7} = S1;
varargout{8} = S2;