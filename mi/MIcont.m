function [muInfo,varargout] = MIcont(mu,sigma,ProbStim,varargin)
% mutual information calculation for coherence values knowing the estimate
% coherence and its standard deviation (from bootstrap or chronux jacknifing).
%
% Input:
%
% mu: coherence values for the different stimuli (vector or cell array)
% if vector should be 1xC 
% sigma: standard deviation of teh coherence (vector or cell array)
% ProbStim: Probability of the stimulus should be a vector having nstim element
% 
% assume a gaussian distribution of the estimates
% from von Heimendahl & al. 2007 with modification with the Baye
% theorem Ps_r = (Pr_s * Ps) / Pr


Args = struct('responseRange',[0 : 0.01 : 1]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

nstim = length(mu);
%% case for input being in cell format more optimal for computation. Each
%% element in the cell is a matrix (i.e time x frequency) for each stimulus
if iscell(mu)
   
    Pr_s = cell(nstim,1);
    Pr = zeros(length(Args.responseRange),size(mu{1},1),size(mu{1},2));
    for stim = 1 : nstim % loop through all the stimuli to get their conditional probabilities P(r/s)
        count = 1;
        for X = Args.responseRange
            
            Pr_s{stim}(count,:,:) =  normpdf(repmat(X,size(mu{stim},1),size(mu{stim},2)),mu{stim},squeeze(sigma{stim}));
            count = count +1;
        end
         Pr_s{stim} =  Pr_s{stim} ./  repmat(sum(Pr_s{stim},1),size(Pr_s{stim},1),1);
        Pr = Pr + Pr_s{stim};
    end
    
    Pr = Pr ./ repmat(sum(Pr,1),size(Pr,1),1); % estimate the probability of the response by summing and normalizing all the P(r/s)
    
    muInfo = zeros(size(mu{1},1),size(mu{1},2));
    %% calculation of the mutual information 
    for stim = 1 : length(mu)
        for r = Args.responseRange
            iel = find(r==Args.responseRange);
            if Pr(iel) ~= 0
                if isvector(mu{1})
                     newMu = squeeze(Pr_s{stim}(iel,:)) .* ProbStim{stim} .* log2(squeeze(Pr_s{stim}(iel,:)) ./ squeeze(Pr(iel,:)));
                tmi(1,:) = muInfo;
                tmi(2,:) = newMu;
                else
                newMu = squeeze(Pr_s{stim}(iel,:,:)) .* ProbStim{stim} .* log2(squeeze(Pr_s{stim}(iel,:,:)) ./ squeeze(Pr(iel,:,:)));
                tmi(1,:,:) = muInfo;
                tmi(2,:,:) = newMu;
                end
                muInfo = squeeze(nansum(tmi,1));
                
                %from von Heimendahl & al. 2007 with modification with the Baye
                % theorem Ps_r = (Pr_s * Ps) / Pr
            end
        end
    end
    %%
    %% case for single values for each stimulus
else
    Pr_s = zeros(length(mu),length(Args.responseRange));
    
    
    for stim = 1 : nstim
        count = 1;
        for X = Args.responseRange
            
            Pr_s(stim,count) =  normpdf(X,mu(stim),sigma(stim));
            count = count +1;
        end
    end
    Pr_s = Pr_s ./ repmat(sum(Pr_s,2),1,size(Pr_s,2)); % normalize to have the sum == 1
    Pr = sum(Pr_s,1);
    
    Pr = Pr / sum(Pr);% normalize to have the sum == 1
    
    muInfo = 0;
    for stim = 1 : length(mu)
        for r = Args.responseRange
            iel = find(r==Args.responseRange);
            if Pr(iel) ~= 0
                
                muInfo = muInfo + Pr_s(stim,iel) * ProbStim(stim) * log2(Pr_s(stim,iel) / Pr(iel));
                
                %from von Heimendahl & al. 2007 with modification with the Baye
                % theorem Ps_r = (Pr_s * Ps) / Pr
            end
        end
    end
end
varargout{1} = Pr_s;
varargout{2} = Pr;