%set up the independent pools
%needs 'sub_pools' as defined in 'model_driver_1'

%independent parameters as I see them
gmaxE_ext = 0.02; %Lifted from Heinzel
pool_size=100;

gmaxI_ext = 3*gmaxE_ext;


% Add populations in Pool1.

name='Pool_1_E';type='exc';poolsize=pool_size;nretpos=subpools;bgE=0.472;bgI=0.34;

add_population;

name='Pool_1_I';type='inh';poolsize=pool_size/4;nretpos=subpools ;bgE=0.46;bgI=0.4;

add_population;

% Add populations in Pool2.

name='Pool_2_E';type='exc';poolsize=pool_size;nretpos=subpools;bgE=0.472;bgI=0.34;

add_population;

name='Pool_2_I';type='inh';poolsize=pool_size/4;nretpos=subpools;bgE=0.46;bgI=0.4;

add_population;
