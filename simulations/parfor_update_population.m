% update_population updates the all populations. The code is adapted from 

% (Salinas, NeuralComputation,15,1439ff).  

function holdvar=parfor_update_population(holdvar,numb,inputs,cons,tstepEc,tstepEc1,tstepEc2,tstepIc,tstepIc1,tstepIc2,VeqI,VeqE,tauME,tauMI,V_th,V_reset,trefE,trefI,dt,t)%[holdvar,anothervar]

%unpack input structure
% fldnms = fieldnames(updatestruct);
% for m=1:numel(fldnms)
%     eval([fldnms{m},' = ',num2str(updatestruct.(fldnms{m})),';']);
% end
anothervar=cell(1,numb);
for k=1:numb
   % update input conductances first
   GE=zeros(holdvar{k}.n_neurons,1);% preallocating
   GI=zeros(holdvar{k}.n_neurons,1);
   INP=zeros(holdvar{k}.n_neurons,1);
   INPNOISE=zeros(holdvar{k}.n_neurons,1);
   
   %add in all the conductance inputs from other populations
   for uu=holdvar{k}.input_exc
      GE=GE+cons.connection{uu}.G;
   end
   for uu=holdvar{k}.input_inh
      GI=GI+cons.connection{uu}.G;
   end

   
   % update external inputs
   for uu=holdvar{k}.input_external
      if t>inputs.external{uu}.t_off
         continue%dont add external input.
      elseif t>inputs.external{uu}.t_trans_off
         INP=INP+inputs.external{uu}.sustained_level*inputs.external{uu}.ExtInp;
         INPNOISE=INPNOISE+inputs.external{uu}.sustained_level*inputs.external{uu}.NoiseExtInp.*randn(holdvar{k}.n_neurons,1);
      elseif t>inputs.external{uu}.t_on
         INP=INP+inputs.external{uu}.ExtInp;% so this is a constant firing while the input is on?  More like an electrode than a population of neurons
         INPNOISE=INPNOISE+inputs.external{uu}.NoiseExtInp.*randn(holdvar{k}.n_neurons,1);
      end
   end

   holdvar{k}.inEAux = holdvar{k}.inEAux*tstepEc + ...                                           %decay the current conductances
                       (holdvar{k}.bgExc + INP)*tstepEc1 + ...                                   %add 'leak' input conductance and external inputs, * 1- time constant 
                       (holdvar{k}.NoiseExc*randn(holdvar{k}.n_neurons,1) + INPNOISE)*tstepEc2;  %add noise conductances, decayed.  THE DECAY CONSTANT HERE SEEMS WRONG IF WE WANT NOISE TO BE A VARIATION OF INPUT AND BACKGROUND CURRENTS. HOWEVER THE NETWORK NEVER GETS STARTED WITHOUT THIS.
%                      (holdvar{k}.NoiseExc*randn(holdvar{k}.n_neurons,1) + INPNOISE)*tstepEc1;  %add noise conductances, decayed.
    anothervar{k}=holdvar{k}.NoiseExc*randn(holdvar{k}.n_neurons,1);

   holdvar{k}.inIAux = holdvar{k}.inIAux*tstepIc + ...                                           %as above for excitatory currents
                       (holdvar{k}.bgInh)*tstepIc1 + ... 
                       (holdvar{k}.NoiseInh*randn(holdvar{k}.n_neurons,1))*tstepIc2;
%                      (holdvar{k}.NoiseInh*randn(holdvar{k}.n_neurons,1))*tstepIc1;

   temp = 1./(1 + GE + GI + holdvar{k}.inEAux + holdvar{k}.inIAux);% total 1/g, or R
   Iplus = (GE + holdvar{k}.inEAux)*VeqE;  %the whole positive conductance
   Iminus = (GI + holdvar{k}.inIAux)*VeqI; %negative conductance
   Vinf = ( Iplus + Iminus).*temp;         %V=IR
   
   holdvar{k}.refrac = rectify(holdvar{k}.refrac - dt); %step refractory period
   iiref = (holdvar{k}.refrac == 0);%check if cell is out of "hard" refractory period
     
   % calculate the total conductances of the neurons, the membrane voltage
   % and the spikes for excitatory neurons.
   if strcmp(holdvar{k}.type,'exc') %different membrane conductances
      tauV = tauME*temp; %tau = RC, ie tauME is actually a capacitance?
   elseif strcmp(holdvar{k}.type,'inh')
      tauV = tauMI*temp;
   else
       raise('fail')
   end
   
   %set spikes and associated values
   holdvar{k}.Vm(iiref) = Vinf(iiref) +   (holdvar{k}.Vm(iiref) - Vinf(iiref)).*exp(-dt./tauV(iiref));%this suggests that nothing changes in the underlying membrane voltage while in ther refractory period.  As with the idea of a backpropogating spike 'clearing' the membrane potential
   holdvar{k}.spikes = holdvar{k}.Vm > V_th;
   if strcmp(holdvar{k}.type,'exc') %different refractory periods
      holdvar{k}.refrac(holdvar{k}.spikes) = trefE;
   elseif strcmp(holdvar{k}.type,'inh')
      holdvar{k}.refrac(holdvar{k}.spikes) = trefI;
   end
   holdvar{k}.Vm(holdvar{k}.spikes) = V_reset;
end



%--------------------------------------------------------------

%       holdvar{k}.inEAux = holdvar{k}.inEAux*tstepEc +   
%                             (holdvar{k}.bgExc + INP)*tstepEc1 +  
%                             (holdvar{k}.NoiseExc*randn(holdvar{k}.n_neurons,1)+INPNOISE)*tstepEc2;
%       holdvar{k}.inIAux = holdvar{k}.inIAux*tstepIc +  
%                             (holdvar{k}.bgInh)*tstepIc1 +  
%                             (holdvar{k}.NoiseInh*randn(holdvar{k}.n_neurons,1))*tstepIc2;
%       temp = 1./(1 + GE + GI + holdvar{k}.inEAux + holdvar{k}.inIAux);     
%       Vinf = ((GE + holdvar{k}.inEAux)*VeqE + (GI + holdvar{k}.inIAux)*VeqI).*temp;
%       % Calculate new values (!! refractory period !!)
%       holdvar{k}.refrac = rectify(holdvar{k}.refrac - dt);
%       iiref = (holdvar{k}.refrac == 0);






