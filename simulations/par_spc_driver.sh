#! /usr/bin/env bash
cd /media/raid/data/sim/

for m in {0,.02} #,0.056,.08,.1}
do for n in {30,.1,.3} #,.5}
do  matlab -nodisplay -r "path(/media/raid/data/sim/);Two_column_driver_for_local_HPC(1,{3,$m,$n,$n},{'size_factor', 'inputstruct.Pool_1_E.External.MeanInp','inputstruct.Pool_2_E.Pool_1_E.coincidence_power','inputstruct.Pool_1_E.Pool_2_E.coincidence_power'}); quit" &
done
done

