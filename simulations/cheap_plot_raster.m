function cheap_plot_raster(inpt, oshape, ocolor, oneg)
%takes a sparse raster of spikes and optionally a color and shape for each
%mark, then plots a raster of spikes against time


%spy() should pretty much do it, just needs some resizing
figure
if nargin<2 || isempty(oshape)
    oshape='+';
end

if nargin<3 || isempty(ocolor)
    ocolor='b';
end

dt=.1;
%figure
%hold on
%[tt,rr]=find(inpt);
[tt,rr,s]=find(inpt);
subplot(2,1,1)
if nargin<4 || isempty(oneg) || ~oneg
    plot(rr*dt,tt,[oshape,ocolor])
else
    plot(rr*dt,-tt,[oshape,ocolor])
end
title(strrep(inputname(1),'_','  '))
