Pools = ['Pool_1_E','Pool_1_I','Pool_2_E','Pool_2_I']
%WANT TO MAKE A SET OF EDITABLE SUBDIAGONAL MATRICES SHOWING FOR [TAU, WEIGHT, SPARSENESS] OF EACH POOL TO POOL CONNECTION.  
%BETTER YET, MAKE THESE A SEARCHABLE VARIABLE SPACE


%for m=1:length(Pools)
%    for n=1:length(Pools)
%	if strfind('E', Pools(m)) 
%            type='exc';
%	elseif strfind('I', Pools(m))
%	    type='inh';
%	end 
%        from=Pools(m);
%        to=Pools(n);
%        weight=0.008;
%        tau=5;
%        sparseness=0.5;
%        small_matrix=diag(ones(nfac,1),0)+0.05*(diag(ones(nfac-1,1),1)+diag(ones(nfac-1,1),-1));%NOT TOTALLY CERTAIN WHAT THIS IS DOING…
%        add_connection; %GET IN HERE TO ADD VARIABILITY TO THE CONNECTIONS
%    end
%end

% General parameters

w_dist=1;  % distribution of weights w_dist=0:no distribution, 1:uniform, 2:gaussian (not yet implemented)

sigma_dist=0.5; % sigma of distribution, needs to be <=1



% Within pools 
%excite to excite

type='exc';from='Pool_1_E';to='Pool_1_E';weight=0.008;tau=5;sparseness=0.5;

small_matrix=diag(ones(nfac,1),0)+0.05*(diag(ones(nfac-1,1),1)+diag(ones(nfac-1,1),-1));

add_connection;

% inhib to excite

type='inh';from='Pool_1_I';to='Pool_1_E';weight=0.06;tau=3;sparseness=0.5;

small_matrix=diag(ones(nfac,1),0);

add_connection;
% from excite to inhib

type='exc';from='Pool_1_E';to='Pool_1_I';weight=0.0025;tau=5;sparseness=0.25;

small_matrix=ones(nfac,nfac);

add_connection;
%THERE IS NO INHIB->INHIB CONNECTION IN HEINZEL'S WORK.  IS THAT 'CORRECT'?


%Across pools
type='exc';from='Pool_1_E';to='Pool_2_E';
weight=0.008;%IS THIS THE WEIGHT OF A GIVEN SYNAPSE OR THE AVERAGE WEIGHT (AND SPARSENESS IS ABOUT PROBABILITY OF TWO NEURONS CONTACTING THE SAME TARGET))?
tau=5;
sparseness=0.1;%PRESUMABLY THIS IS THE THING THAT WE WANT TO REALLY CHANGE BETWEEN THE POOLS

small_matrix=diag(ones(nfac,1),0)+0.05*(diag(ones(nfac-1,1),1)+diag(ones(nfac-1,1),-1));

add_connection;

