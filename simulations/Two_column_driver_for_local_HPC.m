function Two_column_driver(inh,newvars, newnames)
%Runs a 

%clear all;
rand('state',sum(100*clock));

if nargin<1 || isempty(inh)
    inh = 1;
end

%get parallelizing ready, making sure to use the local conformation
if ~matlabpool('size')
    matlabpool(3)
end 

%all the global parameters that are fit to save
ParameterSpaceNames = {'size_factor',...  %The number of neurons in an inhibitory pool, multiply by (1+excite_factor) to get total number of neurons in a pool
                    'excite_factor',... %the multiple by which excitatory cells outnumber inhibitory cells.
                    'bckgrnd_excite_to_Excit_1',... %background injected currents, essentially.  Keeping the thing running
                    'bckgrnd_inhib_to_Excit_1',...
                    'bckgrnd_excite_to_Inhib_1',...
                    'bckgrnd_inhib_to_Inhib_1',...
                    'nretpos',... %currently a legacy variable set to 1, can be used to create subpopulations in a pool
                    'inarray1',... %currently a legacy variable set to 1, can be used to selectively excite subpopulations
                    'gmaxE_ext',... %maximal excitatory current from external inputs (?)
                    'gmaxI_ext',... %maximal excitatory current from external inputs (?)
                    'w_dist',...  %distribution of synaptic weights w_dist=0:no distribution, 1:uniform, 2:gaussian (not yet implemented) from makeall_connections
                    'sigma_dist',...%standard dev as fraction of mean for w_dist
                    'V_reset',... %Voltage which must be reached befor a neuron can spike again.  All voltages are mV with 0=resting                   
                    'V_reset_var',... %Variability in V_reset.  Not recomended 
                    'tauMI',...
                    'V_th',... %Threshold voltage
                    'V_th_var',... %Variability in V_th
                    'taucorrE',...
                    'taucorrI',...                   
                    'VeqE',...                    
                    'VeqE_var',...
                    'VeqI',...
                    'VeqI_var',...
                    'tmax',... %length of simulation
                    'addvariability',...
                    'taucorrE',...
                    'taucorrI',... 
                    'inputstruct',... %The structure defining the input populations and their connections
                    };


%What should be saved when all is done?
savepath=['save ','/media/raid/data/sim/Two_Column_Results_',strrep(int2str(clock),'    ','_'),...
    ' rastlist',...
    ' ParameterSpace',... '
    ' inputstruct',...
    ];   

%Defines model wide parameters
DefineMyParameters;%Sets up parameters with 0mV = resting and time step rules

%Defines the pools and the connections between them
inputstruct_builder;



for m=1:length(newvars)
    if strncmp('inarray1',newnames{m},4)
        disp('not ready for this variable to change yet')
    else
        eval([newnames{m},'=',num2str(newvars{m}),';'])
    end
end

%calculate dependent parameters
% number of iterations
iterations = ceil(tmax/dt);
% integration time steps based on channel time constants above
tstepEc = exp(-dt/taucorrE);
tstepEc1= 1 - tstepEc;
tstepEc2= sqrt(1 - tstepEc^2);
tstepIc = exp(-dt/taucorrI);
tstepIc1= 1 - tstepIc;
tstepIc2= sqrt(1 - tstepIc^2);
nfac=length(inarray1);

if exist('addvariability') & addvariability
    varnames = {'VeqE_var','VeqI_var','V_th_var','V_reset_var'};
    for m=1:numel(varnames)
        if ~eval(varnames{m})
            eval([varnames{m},'=5;'])
        end
    end
else
    [VeqE_var, VeqI_var, V_th_var, V_reset_var] = deal(0);
end


% %reset variables that we are going to mess with
% if nargin>0 & ~isempty(variablestruct) & strncmp(class(variablestruct),'struct',4)
%     fldnames = fieldnames(variablestruct);
%     for m=1:numel(fldnames)
%         if strncmp(class(eval(fldnames{m})),'double',4) & length(eval(fldnames{m}))==1
%             disp('Making a change')
%             %there can be an error raised here if you try to change a
%             %single float into an array.  This is a feature for now at
%             %least
%             eval([fldnames{m},' = ',num2str(variablestruct.(fldnames{m}))]) %This appears to fail on the creation of new variables.  Which is great.
%         elseif strncmp(class(eval(fldnames{m})),'struct',4)
%             nm=['.',fldnames{m}];
%             while ~strncmp(class(eval(['variablestruct',nm])),'double',4)
%                 subfields = fieldnames(eval(['variablestruct',nm]));
%                 nm = [nm,'.',subfields{1}];
%             end
%             disp('Making a change')
%             eval([nm(2:end), ' = ',num2str(eval(['variablestruct',nm]))])
%         else
%             varaiblestruct.fldnames{m};
%             raise('Whoa there dude, not a cool input')
%         end
%     end
% end
    
poolnames = fieldnames(inputstruct);

rastlist = struct();
for m=1:numel(poolnames)%just allocating space here
    rastname = [poolnames{m},'_rast'];  
    %get a raster variable for each population
    if ~isempty(strfind(poolnames{m}, 'E')) %get a HZvaraible
        rastlist.(rastname) = sparse(false(size_factor*excite_factor,iterations));
    elseif ~isempty(strfind(poolnames{m}, 'I'))
        rastlist.(rastname) = sparse(false(size_factor,iterations));
    else
        raise([poolnames{m}, 'not defined as inhibitory or excitatory']);
    end
end

initialize_network;% creates empty "pops cons inputs"

%preallocate the populations
nretpos=nfac;
for m=1:numel(poolnames)% don't parfor, too fast
    name=poolnames{m}; %for each population, resets all the variables needed for the add_population script
    if ~isempty(strfind(poolnames{m},'E'))
        type='exc';poolsize=size_factor*excite_factor;
        bgE=bckgrnd_excite_to_Excit_1;
        bgI=bckgrnd_inhib_to_Excit_1;
    elseif ~isempty(strfind(poolnames{m},'I'))
        type='inh';poolsize=size_factor;
        bgE=bckgrnd_excite_to_Inhib_1;
        bgI=bckgrnd_inhib_to_Inhib_1;
    else
        raise([poolnames{m}, 'not defined as inhibitory or excitatory']);
    end
    add_population;%needs nretpos, bgE,bgI
end


%The below is clunky and perhaps not the best way to accieve the goal, but
%is a product of other failed ways to deal with parfor loops
pp=pwd;
qq=1;
rr=1;
connectiondescription={};%preallocate; small so no need to overallocate and truncate
externalindeces={};
for m=1:numel(poolnames) %'line up' all the connections that need to be made to avoid double for loops of unknown and variable length
    fldnames = fieldnames(inputstruct.(poolnames{m}));
    for n=1:numel(fldnames)
        if ~isempty(strfind(fldnames{n},'Extern'))
            %have to dynamically reset time on and off since it is
            %dependent on variables that may have changed with input
            onval = min(warmuptime+.2*tmax,warmuptime+500);
            offval = min(warmuptime+.4*tmax,warmuptime+1000);
            eval(['inputstruct.',poolnames{m},'.',fldnames{n},'.t_on = ',num2str(onval)])
            eval(['inputstruct.',poolnames{m},'.',fldnames{n},'.t_off = ',num2str(offval)])
            externalindeces{rr}=[poolnames{m},fldnames(n)];
            rr=rr+1;
            continue
        end
        connectiondescription{qq}=[poolnames{m},'_onto_',fldnames{n}];
        qq=qq+1;
    end
end

%use the ordered connection sets in separate parfor functions
cons.connection = connection_maker(inputstruct, connectiondescription, pops,w_dist,sigma_dist,dt);
inputs.external = input_maker(externalindeces, inputstruct,pops,gmaxE_ext);
cons.ncons=numel(cons.connection);
inputs.ninputs=numel(inputs.external);


%get a structure of the variables used in step by step model updating.
%This makes passing easier and gives a chance to check that we are saving
%all these variables
updatestruct = struct();
updatenames = {'tstepEc','tstepEc1','tstepEc2','tstepIc','tstepIc1','tstepIc2','VeqI','VeqE','tauME','tauMI','V_th','V_reset','trefE','dt'};
for m=1:numel(updatenames)
    updatestruct.(updatenames{m})=eval(updatenames{m});
end



%Collect all the relevant parameters.  Down here since all is defined by
%this point
for m=1:numel(updatenames)
    if ~ismember(updatenames{m},ParameterSpaceNames)
        ParameterSpaceNames{end+1}=updatenames{m}; 
    end
end
ParameterSpace=struct();
for m=1:length(ParameterSpaceNames)
    nm=ParameterSpaceNames{m};
    ParameterSpace.(nm)=eval(nm);
end


%main driving loop.  All parforing is done in functions, these are
%non-independent itterations.
for j=1:iterations
   t=t+dt;
%    recdt=recdt+dt;
   
   %used for debugging   
   for k=1:pops.npops
       if (sum(pops.population{k}.spikes)>1)
           foo
       end
   end
   cons.connection = parfor_update_conductance(cons.connection,rastlist,pops,cons.ncons,j,dt);
   pops.population = parfor_update_population(pops.population,pops.npops,inputs,cons,tstepEc,tstepEc1,tstepEc2,tstepIc,tstepIc1,tstepIc2,VeqI,VeqE,tauME,tauMI,V_th,V_reset,trefE,dt);

   % save the spike patterns
   for m=1:pops.npops
       rastlist.([pops.population{m}.name,'_rast'])(:,j) = pops.population{m}.spikes;
   end
   
   if tmax<t
      break;
   end
end

eval(savepath);







%-----------------DEPRICATED CODE--------------------------------

% [Pool_1_E, Pool_1_I, Pool_2_E, Pool_2_I] = deal();
% [Pool_1_E_HZ, Pool_2_E_HZ]=deal();


%preallocate.  Note that other variables are preallocated above, and may
%need to be for sub-functions
%[Pool_1_E_rast, Pool_2_E_rast]=deal(sparse(logical(zeros(size_factor*4,iterations,'uint8'))));%zeros preallocates, uint8 makes creation faster, logical needed for sparse, which reduces memory needs
%[Pool_1_I_rast, Pool_2_I_rast]=deal(sparse(logical(zeros(size_factor,iterations,'uint8'))));


%initialize a series of counters counting the total amount
%of spikes for each population.

%[countPool_1_E, countPool_1_I, countPool_2_E, countPool_2_I]=deal(0);

% name='Pool_1_I';type='inh';poolsize=size_factor;nretpos=nfac;
% bgE=bckgrnd_excite_to_Inhib_1;
% bgI=bckgrnd_inhib_to_Inhib_1;
% add_population;
% 
% name='Pool_2_E';type='exc';poolsize=size_factor*4;
% bgE=bckgrnd_excite_to_Excit_2;
% bgI=bckgrnd_inhib_to_Excit_2;
% add_population;
% 
% name='Pool_2_I';type='inh';poolsize=size_factor;nretpos=nfac;
% bgE=bckgrnd_excite_to_Inhib_2;
% bgI=bckgrnd_inhib_to_Inhib_2;
% add_population;

% % from I4 to E4
% type='inh';from='Pool_1_I';to='Pool_1_E';weight=0.06;tau=3;sparseness=0.5;
% small_matrix=diag(ones(nfac,1),0);
% add_connection;
% % from E4 to I4
% type='exc';from='Pool_1_E';to='Pool_1_I';weight=0.0025;tau=5;sparseness=0.25;
% small_matrix=ones(nfac,nfac);
% add_connection;
% %CURRENTLY LACKING I TO I
% 
% type='exc';from='Pool_2_E';to='Pool_2_E';weight=0.008;tau=5;sparseness=0.5;
% small_matrix=diag(ones(nfac,1),0)+0.05*(diag(ones(nfac-1,1),1)+diag(ones(nfac-1,1),-1));
% add_connection;
% % from I4 to E4
% type='inh';from='Pool_2_I';to='Pool_2_E';weight=0.06;tau=3;sparseness=0.5;
% small_matrix=diag(ones(nfac,1),0);
% add_connection;
% % from E4 to I4
% type='exc';from='Pool_2_E';to='Pool_2_I';weight=0.0025;tau=5;sparseness=0.25;
% small_matrix=ones(nfac,nfac);
% add_connection;
% 
% %Connect the two populations
% type='exc';from='Pool_1_E';to='Pool_2_E';weight=0.0025;tau=5;sparseness=0.05;
% small_matrix=ones(nfac,nfac);
% add_connection;
% type='exc';from='Pool_2_E';to='Pool_1_E';weight=0.0025;tau=5;sparseness=0.05;
% small_matrix=ones(nfac,nfac);
% add_connection;
% longdelay = 1;
% longdelay_var = .1;

% t_trans_off = sustained_level = inputstruct.Pool_1_E.External.t_trans_off;
% name='Column1inpt';type='exc';to='Pool_1_E';inarray=inarray1;retinotopic=0;
% MeanInp=0.056;NoiseLevel=.5;t_on=.4*tmax;t_off=.6*tmax;sustained_level=0.5;
% t_trans_off=10;%STILL NOT CERTAIN WHAT THIS ONE IS DOING
% add_input;

% parfor somecounter=1:numel(poolnames)
%     tic
% %    from=poolnames{somecounter}
%     fldnames = fieldnames(inputstruct.(poolnames{somecounter}));
%     locnm = poolnames{somecounter};
%     for n=1:numel(fldnames)
%         instruct = inputstruct.(locnm).(fldnames{n});
%         if ~isempty(strfind(locnm,'E'))
%             instruct.type='exc';
%         elseif ~isempty(strfind(locnm,'I'))
%             instruct.type='inh';
%         else    
%             raise([locnm, 'not defined as inhibitory or excitatory']);    
%         end
%         if ~isempty(strfind(fldnames{n},'External'))
%             continue
%         end
%         instruct.to = fldnames{n}
%         instruct.from = locnm;
%         instruct
% %         to=fldnames{n};
% %         weight=inputstruct.(poolnames{somecounter}).(fldnames{n}).weight;
% %         tau=inputstruct.(poolnames{somecounter}).(fldnames{n}).tau;
% %         sparseness=inputstruct.(poolnames{somecounter}).(fldnames{n}).sparseness;
% %         small_matrix=inputstruct.(poolnames{somecounter}).(fldnames{n}).small_matrix;
% %         longdelay=inputstruct.(poolnames{somecounter}).(fldnames{n}).longdelay;
% %         longdelay_var=inputstruct.(poolnames{somecounter}).(fldnames{n}).longdelay_var;
%         evalparfor_add_connection(instruct,fldnames,pops,cons);
%     end
%     toc
% end

%clear longdelay longdelay_var %the existance of these variables means that
%they will be used in all subsequent connections.  But they are legitimate
%parameters and should be saved in the parameter structure.  Clear if
%necessary and remove from the ParameterSpaceNames.

   %rastlist = {Pool_1_E_rast,Pool_2_E_rast};%THESE GUYS PROBABLY AREN'T GOING TO BENIFIT FROM A PARFOR LOOP.  ASSIGNING THESE LISTS FURTHER UP (IN ORDER) SHOULD MAKE FEWER VARIABLE CHANGES NECESSARY WHEN ADDING AND REMOVING POPULATIONS
   %varlist = {Pool_1_E,Pool_1_I,Pool_2_E,Pool_2_I};
   %HZlist = {Pool_1_E_HZ, Pool_1_E_HZ};
   
   
          %        if strmatch(pops.population{m}.name,poolnames{1})
%            Pool_1_E_rast(:,j)=pops.population{m}.spikes; %FIX HARDCODED VARIABLE NAMES HERE?
%            Pool_1_E(countPool_1_E+1:countPool_1_E+length(find(pops.population{m}.spikes)),:)=[t*ones(length(find(pops.population{m}.spikes)),1) find(pops.population{m}.spikes)]; 
%            countPool_1_E=countPool_1_E
%            Pool_1_E_HZ(:,ind)=Pool_1_E_HZ(:,ind)+sum(reshape(pops.population{m}.spikes,pops.population{m}.poolsize,nfac))'*facE;
%        elseif strmatch(pops.population{m}.name,poolnames{2})
%            Pool_1_I_rast(:,j)=pops.population{m}.spikes;
%            Pool_1_I(countPool_1_I+1:countPool_1_I+length(find(pops.population{m}.spikes)),:)=[t*ones(length(find(pops.population{m}.spikes)),1) find(pops.population{m}.spikes)]; 
%            countPool_1_I=countPool_1_I+length(find(pops.population{m}.spikes));
%        elseif strmatch(pops.population{m}.name,poolnames{3})
%            Pool_2_E_rast(:,j)=pops.population{m}.spikes;
%            Pool_2_E(countPool_2_E+1:countPool_2_E+length(find(pops.population{m}.spikes)),:)=[t*ones(length(find(pops.population{m}.spikes)),1) find(pops.population{m}.spikes)]; 
%            countPool_2_E=countPool_2_E+length(find(pops.population{m}.spikes));
%            Pool_2_E_HZ(:,ind)=Pool_2_E_HZ(:,ind)+sum(reshape(pops.population{m}.spikes,pops.population{m}.poolsize,nfac))'*facE;
%        elseif strmatch(pops.population{m}.name,poolnames{4})
%            Pool_2_I_rast(:,j)=pops.population{m}.spikes;
%            Pool_2_I(countPool_2_I+1:countPool_2_I+length(find(pops.population{m}.spikes)),:)=[t*ones(length(find(pops.population{m}.spikes)),1) find(pops.population{m}.spikes)]; 
%            countPool_2_I=countPool_2_I+length(find(pops.population{m}.spikes));
%        end


%FIX THIS to work in the new structures where they are held.
% for m=1:numel(rastlist)
%     varlist{m}=varlist{m}(1:find(varlist{m}(:,1),'last'),:);
% end
% Pool_1_E=Pool_1_E(1:find(Pool_1_E(:,1),'last'),:);%AND USE THIS FORMAT
% Pool_2_I=Pool_2_I(1:max(find(Pool_2_I(:,1))),:);
% Pool_2_E=Pool_2_E(1:max(find(Pool_2_E(:,1))),:);
% Pool_1_I=Pool_1_I(1:max(find(Pool_1_I(:,1))),:);
% 
% Pool_1_E_rast=sparse(logical(Pool_1_E_rast));
% Pool_1_I_rast=sparse(logical(Pool_1_I_rast));
% Pool_2_E_rast=sparse(logical(Pool_2_E_rast));
% Pool_2_I_rast=sparse(logical(Pool_2_I_rast));

   %update recdt and ind 
%    if recdt>1
%       recdt=dt/2;
%       ind=ind+1;
%    end

%       startind = countlist.(['count',pops.population{m}.name]) + 1;
%       endin = countlist.(['count',pops.population{m}.name]) + length(find(pops.population{m}.spikes));
%       varlist.(pops.population{m}.name)(startind:endin,:) = [t*ones(length(find(pops.population{m}.spikes)),1) find(pops.population{m}.spikes)];
%       countlist.(['count',pops.population{m}.name]) = countlist.(['count',pops.population{m}.name]) + length(find(pops.population{m}.spikes));

       %HZnames.([pops.population{m}.name,'_HZ'])(:,ind) = HZlist{m}(:,ind) + sum(reshape(pops.population{m}.spikes,pops.population{m}.poolsize,nfac))'*facE;

%ReadOutVariables;%initializes a series fo readout variables for each class of neurons, STILL NOT SURE ON FORMAT
%varlist=struct(); %varlist is essentially a sparse matrix for spike times.  But it takes too much memory by requiring double precission, so its been repalced
%HZnames=struct();
%countlist=struct();

%    varlist.(poolnames{m}) = zeros(tmax*10,2);%primary data structure
%    countname = ['count',poolnames{m}]; %counter for each
%    countlist.(countname) = 0;

%        namelocal = [poolnames{m},'_HZ'];
%        HZnames.(namelocal) = zeros(nfac,tmax);

% bckgrnd_excite_to_Excit_2=bckgrnd_excite_to_Excit_1;%building identical columns

% bckgrnd_excite_to_Inhib_2=bckgrnd_excite_to_Inhib_1;

% bckgrnd_inhib_to_Excit_2=bckgrnd_inhib_to_Excit_1;
 
% bckgrnd_inhib_to_Inhib_2=bckgrnd_inhib_to_Inhib_1;
%%%%%%%

%repatriate all the seperately saved connections into the single cons
%structure
% for m=1:numel(connectiondescription)
%     holdvar=load([pp,'tempfile_', num2str(m),'.mat']);  
%     cons.connection{m}=holdvar.conout;
%     if ~(m==holdvar.conout.n)
%         raise('We have a BIG parforing problem')
%     end
%     system(['rm ',pp,'tempfile_', num2str(m),'.mat']);
% end

% for m=1:numel(externalindeces)
%     holdvar=load([pp,'tempfile_', num2str(m),'.mat']);  
%     inputs.external{m}=holdvar.extrn;
%     if ~(m==holdvar.extrn.n)
%         raise('We have a BIG parforing problem')
%     end
%     system(['rm ',pp,'tempfile_', num2str(m),'.mat']);
% end
% inputs.ninputs = numel(inputs.external);

% cons.ncons = numel(cons.connection);
%    ' varlist',... %    ' HZnames',...
