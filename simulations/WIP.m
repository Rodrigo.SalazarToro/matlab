
% cd ~/Cortex_grant
% addpath('/home/msu/sep/Cortex_grant')
% 
% %check effects of population size
% size_factor = [25,100,250,500,1000,2500];
% placenow = load('~/Desktop/Cortex_grant/where_am_I.mat');
% parfor qq=1:length(size_factor)
%    Two_column_driver({size_factor(qq)},{'ParameterSpace.size_factor'});
%    %fldnms= fieldnames(placenow);for m=1:length(fldnms), eval([fldnms{m}, '=', num2str(placenow.(fldnms{m}))]), end
%    %save('~/Desktop/Cortex_grant/where_am_I.mat','learn_value','sparse_value','stim_value','size_now')
%    size_now
% end   
if ~matlabpool('size')
    matlabpool
end

%checking parameters
EE=[.001,.002,.003,.004,.005];
EI=[.0015,.002,.0025,.003,.0035];
IE=[.04,.03,.02,.01,.005];
for ei_value=1:length(ei)
for ie_value=1:length(IE)
parfor ee_value=1:length(EE)
    nms = {'inputstruct.Pool_2_E.Pool_2_E.weight',...
          'inputstruct.Pool_1_E.Pool_1_E.weight',...
          'inputstruct.Pool_2_E.Pool_2_I.weight',...
          'inputstruct.Pool_1_E.Pool_1_I.weight',...
          'inputstruct.Pool_2_I.Pool_2_E.weight',...
          'inputstruct.Pool_1_I.Pool_1_E.weight',...
          };
    vls = {EE(ee_value),...
           EE(ee_value),...
           EI(ei_value),...
           EI(ei_value),...
           IE(ie_value),...
           IE(ie_value),...
           };
    Two_column_driver(vls,nms); %use the two independent columns as reps        
end
end
end

%checking if we can get coherence between two populations
sparsevals= [0,.05,.1,.2,.5,.7];
parfor sparse_val=1:length(sparsevals)
    nms = {'inputstruct.Pool_2_E.Pool_1_E.sparseness',...
          'inputstruct.Pool_1_E.Pool_2_E.sparseness',...
          };
    vls = {sparsevals(sparse_val),...
           sparsevals(sparse_val),...
           };
     Two_column_driver(vls,nms);
end

% 
% 
% 
% placenow=load('/home/msu/sep/Cortex_grant/where_am_I.mat');
% sparseness = [.01,.05,.1,.5];
% stimstrength = [0,.02,0.056,.08,.1];
% learnpower = [0,.1,.3,.5];
% for sparse_value=placenow.sparse_value:length(sparseness)
%     nowsparse = sparseness(sparse_value);
%     for learn_value=placenow.learn_value:length(learnpower)%will redo whatever run it was in the middle of most recently
%         nowlearn = learnpower(learn_value);
% 	startpar = placenow.stim_value;
% 	endpar = length(stimstrength);
%         parfor stim_value=startpar:endpar
%             nms = {'inputstruct.Pool_1_E.Pool_2_E.sparseness',...
%                 'inputstruct.Pool_2_E.Pool_1_E.sparseness',...
%                 'inputstruct.Pool_2_E.Pool_1_E.coincidence_power',...
%                 'inputstruct.Pool_1_E.Pool_2_E.coincidence_power',...
%                 'inputstruct.Pool_1_E.External.MeanInp'};
%             vls = {nowsparse,nowsparse,...
%                 nowlearn,nowlearn,...
%                 stimstrength(stim_value)};
%             Two_column_driver(vls,nms);         
%         end
%     	fldnms= fieldnames(placenow);for m=1:length(fldnms), eval([fldnms{m}, '=', num2str(placenow.(fldnms{m}))]), end
%         save('/home/msu/sep/Cortex_grant/where_am_I.mat','learn_value','sparse_value','stim_value','size_now')
% 	through_one = 1
%     end
% end



% fltr=[0,.1,.3,.85,1,.85,.3,.1,0];
% stable = 75/.1+1; %things start to look OK 75ms in, which needs to be divided by dt
% 
% betarange = [70,12];
% pictures = 1;
% params = struct('Fs',1000,'fpass',[0 150],'trialave',1);
% for m=1:numel(tt)
%     %NEED TO GET THIS GUY FIXED
%     %jj=tt{m}; 
% end   
    
%Measures
%1) Each population shows beta power
%2) Beta range is less during the stimulus

