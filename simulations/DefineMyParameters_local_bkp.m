%Define the general parameters for the simulation.  This should be all the
%parameters needed, and only those used.  Ranges also given where
%appropriate

%Arbitrary running variables
dt    = 0.10;  %integration time step in ms
tmax  = 1500;  %Total time to simulate in ms
size_factor=300;% A factor representing the number of the neurons in a column.  Total in column is 5*size_factor
w_dist=1;  % distribution of weights w_dist=0:no distribution, 1:uniform, 2:gaussian (not yet implemented) from makeall_connections
sigma_dist=0.25; % sigma of distribution, needs to be <=1
addvariability = 0;%Boolian for the addition of variability in several Parameters.  See DefineParameters
inarray1=1; %legacy variable allowing for subpopulations within a population.
inh=1;%long range inhibitory connections?

%=====================================================================

% Cell parameters; here rest is V=0. Adapted from Salinas, Neural

% Computation

%=====================================================================


%Channel properties.  Should be assigned on a PER MODEL basis as they are an effect of the genetic background 
taucorrE= 3.0;      % time constant for background E input?
taucorrI= 3.0;      % time constant for background I input
gmaxE_ext = 0.02; %Synaptic weights; from makeall_populations
gmaxI_ext = 3*gmaxE_ext;
tauME   = 20;       % in milliseconds; 20 from McCormick; Presumably this makes up for the larger size (capacitance) of excitatory neurons. Direct measurement of specific membrane capacitance in neurons.
tauMI   = 12;       % in milliseconds; 12 from McCormick
trefE   = 1.8;      % refractory period in ms for excitatory
trefI   = 1.2;      % refractory period in ms




%Cellular properties.  Should be defined on a PER CELL basis



%Network properties.  Only set once per run.  Good to check for robustness
%against variation here
bckgrnd_excite_to_Excit_1=.472;%.435; %Tonic background excitation, FEF numbers from originial paper
bckgrnd_excite_to_Inhib_1=.46;
bckgrnd_inhib_to_Excit_1=.34; 
bckgrnd_inhib_to_Inhib_1=.4;
excite_factor = 4; %the multiple by which excitatory cells outnumber inhibitory cells.  4 inherited from Heinzle



%Not quite sure which place they should go
V_th    = 20;       % spike threshold
V_reset = 10;       % -60 from Troyer and Miller, 1997
VeqE    = 74;       % Equilibrium potential for excitatory conductances (74 mV for AMPA)
VeqI    = -10;      % Eq. pot. for inhibitory conductances; from -20 (K) to 0 (Cl, shunting)

%other semi-biological parameters

if exist('addvariability') & addvariability
    VeqE_var = 5;           %added parameter, CAN I BACK THIS UP?
    VeqI_var = 5;     %added parameter, CAN I BACK THIS UP?
    V_th_var = 5;      %added parameter, CAN I BACK THIS UP?
    V_reset_var = 5;     %added parameter, GET REAL VALUE FROM Troyer and Miller, 1997

else
    [VeqE_var, VeqI_var, V_th_var, V_reset_var] = deal(0);
end




%Depricated Variables------------------------------------------------------

% gfac  = 2;              % factor for weights calculated as poolsize*0.02, WHERE DOES THIS SHOW UP AGAIN 
% att_goal=11;
% max_sac=0; 
% sac_goal=0; 
% tlast=0; 
% fov=0; 
% pos=0;
% jskip=skip+1; jspkE=0; jspkI=0; % parameters used for checking for saccade and for graphics update.
% recdt=-dt/2; ind=1; 
% facE=1000/100;  facI=facE*4; % auxiliary values used to store rates of the neurons. WHAT ARE THESE?  NEED TO RENAME IN TERMS OF SOMETHING MORE MEANINGFUL
% nretpos=1; %This is the number of retinotopic positions.  Comes in from time to time while trying to shoehorn old code
%skip=0;%This should be meaningless
