% update_conductance updates the conductances in the network according

% to (Salinas, NeuralComputation,15,1439ff). 


function holdvar = parfor_update_conductance(holdvar,rastlist,pops,numb,ind,dt)



for k=1:numb 
   %check if we need to worry about delays
   if isfield(holdvar{k},'dlays')
       minback = max(1,ind-holdvar{k}.longestdlay/dt);
       rastname = [holdvar{k}.from,'_rast'];
       latest = rastlist.(rastname)(:,minback:ind);
       [neuronid,timeback]=find(latest);
       if ~isempty(neuronid) %only run if there are spikes to be bothered with
           nowarriving = neuronid(holdvar{k}.dlays(neuronid)==timeback);
           holdvar{k}.G = an_update(holdvar,nowarriving,k,ind); %use local function below
       end
   else
       if (sum(pops.population{holdvar{k}.n_from}.spikes)~=0) %only run if there are spikes to be bothered with
           nowarriving = pops.population{holdvar{k}.n_from}.spikes;
           holdvar{k}.G = an_update(holdvar,nowarriving,k,ind); %use local function below
       end
   end
   holdvar{k}.G = holdvar{k}.G*holdvar{k}.tstep;%decay the conductance

   %all the learning stuff
   if isfield(holdvar{k},'coincidence_power')
       pre_spiking = find(pops.population{holdvar{k}.n_from}.spikes);%all the spiking cells in the pre-synaptic population
       post_spiking = find(pops.population{holdvar{k}.n_to}.spikes);%all the spiking cells in the post-synaptic population
       if ~isempty(pre_spiking)
           for m=1:length(pre_spiking)%for all the cells firing in the pre-synaptic population
               %need to see if there were recent firings in the
               %post-synaptic partners so that we know which synapses to
               %inhibit
               try
             post_cells = find(holdvar{k}.matrix(:,pre_spiking(m)));%this gives all the post-synaptic cells that recieve a synapse from the spiking neuron in the pre_synaptic population
             post_rastname = [holdvar{k}.to,'_rast'];%the name of the spike raster for the post_synaptic population
             minback = max(1,ind-2*holdvar{k}.coincidence_tau/dt); %the relevant time period where a pre-synaptic firing might be relevant
             latest = rastlist.(post_rastname)(post_cells,minback:ind);%get the matrix just of recent spikes from relevant cells
             [neuronid,timeback]=find(latest);
                            catch
                   foo
               end
             for n=1:length(neuronid)
  %A BETTER WAY TO CALCULATE THE EXPONENTIAL
                 potent = 1+holdvar{k}.coincidence_power*exp(-timeback(n)/(holdvar{k}.coincidence_tau/dt));
                 localto = post_cells(neuronid(n));
                 localfrom = pre_spiking(m);
                 holdvar{k}.coincidence_matrix(localto,localfrom) = holdvar{k}.coincidence_matrix(localto,localfrom) * potent;        %update coincidence_matrix
             end
           end
       end
       if ~isempty(post_spiking)
           for m=1:length(post_spiking)%need to find which synapses to enhance if there was a pre-synaptic spike before the post
             pre_firing = find(holdvar{k}.matrix(post_spiking(m),:));            
             rastname = [holdvar{k}.from,'_rast'];
             minback = max(1,ind-2*holdvar{k}.coincidence_tau/dt);
             latest = rastlist.(rastname)(pre_firing,minback:ind);
             [neuronid,timeback]=find(latest);
             for n=1:length(neuronid)
                 potent = 1-holdvar{k}.coincidence_power*exp(-timeback(n)/(holdvar{k}.coincidence_tau/dt));
                 localto = post_spiking(m);
                 localfrom = pre_firing(neuronid(n));
                 holdvar{k}.coincidence_matrix(localto,localfrom) = holdvar{k}.coincidence_matrix(localto,localfrom) * potent;
             end
           end
       end
      holdvar{k}.coincidence_matrix =  holdvar{k}.coincidence_matrix * holdvar{k}.coincidence_decay_tstep;%decay the conductance 
   end
end


function conductnce = an_update(holdvar,nowarriving,k,ind)
           if isfield(holdvar{k},'coincidence_power')
               try
                   current_power = holdvar{k}.matrix.*holdvar{k}.coincidence_matrix;
               catch
                   foo
               end
           else
               current_power = holdvar{k}.matrix;
            end
           conductnce= calcg2(holdvar{k}.G,current_power,nowarriving);


%It is possible to want other kinds of potentiation added in here
