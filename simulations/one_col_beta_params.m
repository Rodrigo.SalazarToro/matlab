function [EE,EI,IE,bgEE,bgEI,bgIE,bgII,rast,beta_power,relative_beta_power,gamma_power,relative_gamma_power,peak_freq] = one_col_beta_params(crs,spc) % pwr,tau,dec_tau
%GET THIS TO ACCEPT A SET OF VARIABLE NAMES AND A TEST PARAMETER

beta_freq = [10,20];
gamma_freq = [30 80];
full_range = [1, 150];

cd ~/Desktop/Cortex_grant/MUS_mini-grant/model_results
jj=ls;%matlab's dir appears to produce a troublesome format

strts=strfind(jj,'Two_Column_Results_');
nds=strfind(jj,'.mat')+3;
nam=cell(1,length(strts));
for m=1:length(strts)
    nam{m}=jj(strts(m):nds(find(nds>strts(m),1,'first')));
end
nam = nam(~cellfun('isempty',nam));
number_of_runs = numel(nam);

%preallocate parameters we might want
if spc==1
    [EE,EI,IE] = deal(zeros(1,number_of_runs*2)); %multipy by 2 since it is possible to get two columns per run
    [bgEE,bgEI,bgIE,bgII] = deal(zeros(1,number_of_runs*2));
    sparseness_value = zeros(1,number_of_runs*2);
elseif spc==2 
    [pwr,tau,dec_tau] = deal(zeros(1,number_of_runs*2)); %multipy by 2 since it is possible to get two columns per run
end

%preallocate the dependent measures
[beta_power,relative_beta_power] = deal(zeros(1,number_of_runs*2));%beta power is integral within 'classic' beta frequencies, 'relative' is the same divided by the total power in (0,150]Hz
[gamma_power, relative_gamma_power] = deal(zeros(1,number_of_runs*2));% as above, but for gamma with the idea that this are the parameters we want for the 'stimulus on' period
peak_freq= zeros(2,number_of_runs*2);

%preallocate a cell array for the whole raster, which underlies everything else, in case we want to see it later
rast=cell(1,number_of_runs*2);
% for m=1:numel(rast)
%     rast{m}=sparse(ones(1000,1000));%just an aproximate size really, but sould be a start
% end

counter=1;
for m=1:number_of_runs
    rr=load(nam{m});
    if isfield(rr,'inputstruct')%check that these are in the 'standard' format
        %only run if the two columns are disconnected
        condtn = (~isfield(rr.inputstruct.Pool_1_E,'Pool_2_E') || rr.inputstruct.Pool_1_E.Pool_2_E.weight==crs);
        if spc==2
            condtn = condtn & rr.inputstruct.Pool_1_E.Pool_1_E.weight==.0035 & isfield(rr.inputstruct.Pool_1_E.Pool_1_E,'coincidence_power');
        end
        if condtn   
            %get the raster
            rast{counter} = [rr.rastlist.Pool_1_E_rast;rr.rastlist.Pool_1_I_rast];
            if isempty(find(rast{counter},1))
                [beta_power(counter),relative_beta_power(counter),gamma_power(counter),relative_gamma_power(counter),peak_freq(counter,:)]=deal(NaN); 
                counter=counter+1;
            else

                %parameters

                if spc==1
                    EE(counter)=rr.inputstruct.Pool_1_E.Pool_1_E.weight;
                    EI(counter)=rr.inputstruct.Pool_1_E.Pool_1_I.weight;
                    IE(counter)=rr.inputstruct.Pool_1_I.Pool_1_E.weight;
                    bgEE(counter)=rr.ParameterSpace.bckgrnd_excite_to_Excit_1;
                    bgEI(counter)=rr.ParameterSpace.bckgrnd_excite_to_Inhib_1;
                    bgIE(counter)=rr.ParameterSpace.bckgrnd_inhib_to_Excit_1;
                    bgII(counter)=rr.ParameterSpace.bckgrnd_inhib_to_Inhib_1;
                    sparseness_value(counter) = rr.inputstruct.Pool_1_I.Pool_1_E.sparseness;
                elseif spc==2
                    pwr(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_power;
                    tau(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_tau;
                    dec_tau(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_decay_tau;
                end
                %measures
                [~, ~, PS_1, PS_2, post_PS_1, post_PS_2,freqs,post_f] = read_a_run(nam{m},0);
                %get just the post stimulus values if a stimulus is applied
                if isfield(rr.inputstruct.Pool_1_E, 'External') && ~rr.inputstruct.Pool_1_E.External.MeanInp==0
                    PS_1=post_PS_1;
                    PS_2=post_PS_2;
                    freqs=post_f;
                end
%                 if counter>=9
%                     foo
%                 end
                beta_indeces = find(freqs>=beta_freq(1) & freqs<=beta_freq(2));
                beta_power(counter) = sum(PS_1(beta_indeces));
                gamma_indeces = find(freqs>=gamma_freq(1) & freqs<=gamma_freq(2));
                gamma_power(counter) = sum(PS_1(gamma_indeces));
                full_indeces = find(freqs>=full_range(1) & freqs<=full_range(2));
                mean_other_power = sum(PS_1(full_indeces))/length(full_indeces);%the mean per_frequency power
                relative_beta_power(counter) = beta_power(counter)/(mean_other_power*length(beta_indeces));
                relative_gamma_power(counter) = gamma_power(counter)/(mean_other_power*length(gamma_indeces));
                smooth_PS = max(smooth(PS_1,3));
                peak_freq(counter,:) = max(smooth_PS);
                counter=counter+1;
            end
           
            rast{counter} = [rr.rastlist.Pool_2_E_rast;rr.rastlist.Pool_2_I_rast];
            if isempty(find(rast{counter},1))
                [beta_power(counter),relative_beta_power(counter),gamma_power(counter),relative_gamma_power(counter),peak_freq(counter,:)]=deal(NaN); 
                counter=counter+1;
            else
                %get the same measurements for the second column

                if spc==1
                    EE(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.weight;
                    EI(counter)=rr.inputstruct.Pool_2_E.Pool_2_I.weight;
                    IE(counter)=rr.inputstruct.Pool_2_I.Pool_2_E.weight;
                    bgEE(counter)=rr.ParameterSpace.bckgrnd_excite_to_Excit_1;
                    bgEI(counter)=rr.ParameterSpace.bckgrnd_excite_to_Inhib_1;
                    bgIE(counter)=rr.ParameterSpace.bckgrnd_inhib_to_Excit_1;
                    bgII(counter)=rr.ParameterSpace.bckgrnd_inhib_to_Inhib_1;
                    sparseness_value(counter) = rr.inputstruct.Pool_2_I.Pool_2_E.sparseness;
                elseif spc==2
                    pwr(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_power;
                    tau(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_tau;
                    dec_tau(counter)=rr.inputstruct.Pool_2_E.Pool_2_E.coincidence_decay_tau;
                end
                %measures
                beta_indeces = find(freqs>=beta_freq(1) & freqs<=beta_freq(2));
                beta_power(counter) = sum(PS_2(beta_indeces));
                gamma_indeces = find(freqs>=gamma_freq(1) & freqs<=gamma_freq(2));
                gamma_power(counter) = sum(PS_2(gamma_indeces));
                full_indeces = find(freqs>=full_range(1) & freqs<=full_range(2));
                mean_other_power = sum(PS_2(full_indeces))/length(full_indeces);
                relative_beta_power(counter) = beta_power(counter)/(mean_other_power*length(beta_indeces));
                relative_gamma_power(counter) = gamma_power(counter)/(mean_other_power*length(gamma_indeces));
                smooth_PS = max(smooth(PS_2,3));
                peak_freq(counter,:) = max(smooth_PS);
                counter=counter+1;
            end
        end       
    end
end
if spc==1
    EE(counter:number_of_runs*2)=[];
    EI(counter:number_of_runs*2)=[];
    IE(counter:number_of_runs*2)=[];
    bgEE(counter:number_of_runs*2)=[];
    bgEI(counter:number_of_runs*2)=[];
    bgIE(counter:number_of_runs*2)=[];
    bgII(counter:number_of_runs*2)=[];
elseif spc==2
    pwr(counter:number_of_runs*2)=[];
    tau(counter:number_of_runs*2)=[];
    dec_tau(counter:number_of_runs*2)=[];
end

beta_power(counter:number_of_runs*2)=[];
relative_beta_power(counter:number_of_runs*2)=[];
relative_gamma_power(counter:number_of_runs*2)=[];
gamma_power(counter:number_of_runs*2)=[];
peak_freq(counter:number_of_runs*2)=[];
rast(counter:number_of_runs*2)=[];

figure
tt=colormap;
a_var=relative_beta_power;
inds=find(a_var>0);
if spc==1
    ind1=EE;
    ind2=EI;
    ind3=IE;
elseif spc==2
    ind1=pwr;
    ind2=tau;
    ind3=dec_tau;
end

%scatter3(ind1(inds),ind2(inds),ind3(inds),a_var(inds)/10,tt(ceil(a_var(inds)/max(a_var)*length(tt)),:))
scal = max(a_var(inds));
%scal = 5.5;
scatter3(ind1(inds),ind2(inds),ind3(inds),20+20*a_var(inds)./scal,tt(ceil(a_var(inds)./scal*length(tt)),:))

if spc==1
    xlabel('EE')
    ylabel('EI')
    zlabel('IE')
    xlim([0 .008])
    ylim([0,.005])
    zlim([0,.08])
elseif spc==2
    xlabel('maximum percent increase in synaptic conductance')
    ylabel('time constant of learning rule')
    zlabel('time constant of decay of altered conductance')
end
    


