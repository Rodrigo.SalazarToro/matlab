cd ~/Cortex_grant
addpath('/home/msu/sep/Cortex_grant')

%check effects of population size
size_factor = [25,250,1000,2000];
placenow = load('/home/msu/sep/Cortex_grant/where_am_I.mat');
for qq=placenow.size_now:length(size_factor)
   Two_column_driver({size_factor(qq)},{'ParameterSpace.size_factor'});
   fldnms= fieldnames(placenow);
   for m=1:length(fldnms)
       eval([fldnms{m}, '=', num2str(placenow.(fldnms{m}))])
   end
   save('/home/msu/sep/Cortex_grant/where_am_I.mat','learn_value','sparse_value','stim_value','size_now')
   size_now
end

