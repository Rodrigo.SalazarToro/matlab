% add_input adds a new input to the network. 

% The following parameters need to be defined before.

% 'name': name of the input

% type: 'exc' (excitatory) or 'inh' (inhibitory)

% to: name of afferent population

% inarray: retinotopic inputarray.

% retinotopic: 1 if input is retinotopic, 0 else

% MeanInp: average input conductance

% NoiseLevel: ranges from 0 ot 1 and defines how much noise is injected.

% t_on: onset time of input

% t_trans_off: offset of phasic transient

% t_off: offset of input

% sustained_level: relative strength of sustained input to phasic input.

%

% created: Jakob Heinzle 01/07



function extrn = parfor_add_input(instruct,counter,pops,gmaxE_ext)





n=counter;

to = instruct.to;



n_to=0;

for k=1:pops.npops; 

   name_pop=pops.population{k}.name;

   if length(name_pop)==length(to)*strfind(name_pop,instruct.to)

    n_to=k;

   end

end

if ~n_to

   error('Invalid target population.');

end

nfac_tmp=pops.population{n_to}.nretpos;

if length(instruct.inarray)~=nfac_tmp

   error('inarray must have as many retinotopic positions as the target population.');

end



% general information about the external.

extrn.name = instruct.name;

extrn.retinotopic = instruct.retinotopic;

extrn.type = instruct.type;

extrn.to = instruct.to;

extrn.inarray = instruct.inarray;

extrn.MeanInp = instruct.MeanInp;

extrn.NoiseLevel = instruct.NoiseLevel;

extrn.t_on = instruct.t_on;

extrn.t_off = instruct.t_off;

extrn.t_trans_off = instruct.t_trans_off;

extrn.sustained_level = instruct.sustained_level;

extrn.sparseness = instruct.sustained_level;

extrn.n=n;



% auxiliary variables.

nperpop=pops.population{n_to}.poolsize;

InpH=zeros(nperpop*nfac_tmp,1);

for k=1:nfac_tmp

   InpH((k-1)*nperpop+1:k*nperpop)=instruct.inarray(k);

end



extrn.ExtInp = instruct.MeanInp*InpH.*rand(size(InpH))<extrn.sparseness;

extrn.NoiseExtInp = instruct.NoiseLevel*sqrt(gmaxE_ext*InpH/2);



pops.population{n_to}.input_external=[pops.population{n_to}.input_external, n];



 

 

 


