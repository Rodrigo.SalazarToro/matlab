function plot_param_space(ind1,ind2,ind3,a_var)

tt=colormap;
%a_var=relative_beta_power;
inds=find(a_var>0);
% ind1=EE;
% ind2=EI;
% ind3=IE;
%scatter3(ind1(inds),ind2(inds),ind3(inds),a_var(inds)/10,tt(ceil(a_var(inds)/max(a_var)*length(tt)),:))
scatter3(ind1(inds),ind2(inds),ind3(inds),20+20*a_var(inds)/max(a_var(inds)),tt(ceil(a_var(inds)/max(a_var)*length(tt)),:))
xlabel(inputname(1)),
ylabel(inputname(2)),
zlabel(inputname(3)),