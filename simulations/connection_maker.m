function connectionstruction = connection_maker(inputstruct,connectiondescription, pops,w_dist,sigma_dist,dt)

instruct = cell([numel(connectiondescription),1]);
%get the data all sorted out to avoid passing too much in the parfor loop
for m=1:numel(connectiondescription)
   instruct{m}=struct();
   frmnm = connectiondescription{m}(1:strfind(connectiondescription{m},'_onto_')-1);%get the name of the structure parts we want to work with
   tonm = connectiondescription{m}(strfind(connectiondescription{m},'_onto_')+6:end);
   instruct{m} = inputstruct.(frmnm).(tonm);
   instruct{m}.to = tonm;
   instruct{m}.from = frmnm;
   if ~isempty(strfind(frmnm,'E'))
        instruct{m}.type='exc';
   elseif ~isempty(strfind(frmnm,'I'))
        instruct{m}.type='inh';
   else    
        raise([frmnm, 'has something wrong']);    
   end
end
clear inputstruct
parfor m=1:numel(connectiondescription)
%    frmnm = connectiondescription{m}(1:strfind(connectiondescription{m},'_onto_')-1);%get the name of the structure parts we want to work with
%    tonm = connectiondescription{m}(strfind(connectiondescription{m},'_onto_')+6:end);
%    instruct = inputstruct.(frmnm).(tonm);
%    instruct.to = tonm;
%    instruct.from = frmnm;
%     if ~isempty(strfind(frmnm,'E'))
%         instruct{m}.type='exc';
%     elseif ~isempty(strfind(frmnm,'I'))
%         instruct{m}.type='inh';
%     else
%         raise([frmnm, 'has something wrong']);
%     end
    if isfield(instruct{m},'weight') & instruct{m}.weight
        connectionstruction{m} = parfor_add_connection(instruct{m},m,pops,w_dist,sigma_dist,dt);
    end
   %eval([jj{m},'= holdvar'])
end
connectionstruction=connectionstruction(~cellfun('isempty',connectionstruction));
