function out = compare_model_parameters(rr,tt)
%A function that finds all the differences between two model runs and
%prints these out to the command line

%get all the parameter names in the first run
rr_children = get_structure_tree(rr.inputstruct);
for m=1:numel(rr_children)
    rr_children{m} = ['rr.inputstruct',rr_children{m}];
end
rr_child2 = get_structure_tree(rr.ParameterSpace);
for m=1:numel(rr_child2)
    rr_child2{m} = ['rr.ParameterSpace',rr_child2{m}];
end
rr_children = [rr_children; rr_child2];

%get all the parameter names in the second run
tt_children = get_structure_tree(tt.inputstruct);
for m=1:numel(tt_children)
    tt_children{m} = ['tt.inputstruct',tt_children{m}];
end
tt_child2 = get_structure_tree(tt.ParameterSpace);
for m=1:numel(tt_child2)
    tt_child2{m} = ['tt.ParameterSpace',tt_child2{m}];
end
tt_children = [tt_children; tt_child2];

%working here, need to collect the diffs into a structure to print
out = cell(1,1); 
%diffs = [];
for m=1:numel(rr_children)
    tt_str=['tt.',rr_children{m}(4:end)];
    loc = find(ismember(tt_children,tt_str),1,'first');
    if ~loc %check this first to avoid errors futher down
%        diffs = [diffs, m];
        out{end+1}=rr_children{m}(4:end);
        continue
    end
    %indx = find(ismember(tt_children,rr_children{m}));
    if ~(eval(tt_children{loc}) == eval(rr_children{m}))
%        diffs = [diffs, m];
        out{end+1}=rr_children{m}(4:end);
    end
end
out(1)=[];

%format the output