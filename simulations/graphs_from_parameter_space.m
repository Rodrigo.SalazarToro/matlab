function graphs_from_parameter_space()

[jj.inputstruct.Pool_1_E.Pool_1_E.weight,jj.inputstruct.Pool_1_E.Pool_1_I.weight,jj.inputstruct.Pool_1_I.Pool_1_E.weight] = getCurrent3DPoint();
%[jj.inputstruct.Pool_1_E.Pool_1_E.coincidence_power,jj.inputstruct.Pool_1_E.Pool_1_I.coincidence_tau,jj.inputstruct.Pool_1_I.Pool_1_E.coincidence_decay_tau] = getCurrent3DPoint();
the_nms = pull_up_runs(jj);
for m=1:numel(the_nms)
    read_a_run(the_nms{m},1);
    curfig = gcf;
    for n=0:4
        figure(curfig-n)
        title(['Run ',num2str(m)])
    end        
    figure(curfig)
end

%something that compares the two