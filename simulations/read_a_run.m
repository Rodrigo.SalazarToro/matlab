function [LFP_column_1, LFP_column_2,rr] = read_a_run(somename,pictures, vrble1)


    if ischar(somename)
        rr=load(somename);
    else
        rr=somename;
    end
    if nargin<3
        vrble1='';
    end
    dt=0.1;
    stable = 75/dt+1; %things start to look OK 75ms in, which needs to be divided by dt
    binsize = 10;
    params = struct('Fs',10000/binsize,'fpass',[0 150],'trialave',1);
    
    %plot the raster for population 1
    if pictures
        figure
        cheap_plot_raster(rr.rastlist.Pool_1_I_rast,'r','+',1); 
        hold on
        cheap_plot_raster(rr.rastlist.Pool_1_E_rast);
        %gg=['bkground = ',num2str(rr.ParameterSpace.bckgrnd_excite_to_Excit_1), ' learnpower=', num2str(rr.inputstruct.Pool_1_E.Pool_2_E.coincidence_power), ' dlay=',num2str( rr.inputstruct.Pool_1_E.Pool_2_E.longdelay)]; 
        if ~isempty(vrble1)
            gg=[vrble1,' = ',num2str(eval(['rr.',vrble1]))]; 
            title(['Column 1 ', gg]);
        end
        xlabel('time (ms)')
        ylabel('neuron id')
        
        figure
        cheap_plot_raster(rr.rastlist.Pool_2_I_rast,'r','+',1); 
        hold on
        cheap_plot_raster(rr.rastlist.Pool_2_E_rast);
        %gg=['bkground = ',num2str(rr.ParameterSpace.bckgrnd_excite_to_Excit_1), ' learnpower=', num2str(rr.inputstruct.Pool_1_E.Pool_2_E.coincidence_power), ' dlay=',num2str( rr.inputstruct.Pool_1_E.Pool_2_E.longdelay)]; 
        %gg=['sparseness = ',num2str(rr.inputstruct.Pool_2_E.Pool_2_E.sparseness),];
        if ~isempty(vrble1)
            gg=[vrble1,' = ',num2str(eval(['rr.',vrble1]))]; 
            title(['Column 2 ', gg]);
        end
        xlabel('time (ms)')
        ylabel('neuron id')
    end
    
    %Compute the LFP for column 1
    jj=full(sum([rr.rastlist.Pool_1_E_rast;rr.rastlist.Pool_1_I_rast],1));
    jj=jj(stable:end);
    jconsume = jj;
    timeofspikes=[];maxval=max(jconsume); for n=1:maxval, timeofspikes=[timeofspikes,find(jconsume>n-1)]; end
    LFP_column_1 = hist(timeofspikes,.5*binsize:binsize:length(jj));% turn this into a ms time scale
    
    %Compute the LFP for column 2
    jj_2=full(sum([rr.rastlist.Pool_2_E_rast;rr.rastlist.Pool_2_I_rast],1));
    jj_2=jj_2(stable:end);
    jconsume = jj_2;
    timeofspikes_2=[];maxval=max(jconsume); for n=1:maxval, timeofspikes_2=[timeofspikes_2,find(jconsume>n-1)]; end
    LFP_column_2 = hist(timeofspikes_2,.5*binsize:binsize:length(jj_2));
    
    LFP_column_1=LFP_column_1-mean(LFP_column_1);
    LFP_column_2=LFP_column_2-mean(LFP_column_2);
    LFP_total = (LFP_column_1+LFP_column_2)/2 - mean((LFP_column_1+LFP_column_2)/2);
    
    if pictures
        figure, plot(smooth(LFP_column_1)), hold on, plot(smooth(LFP_column_2),'r'), %plot(LFP_total,'g')
        xlabel('time (ms)')
        ylabel('normalized firing rate (spikes/ms)')
        legend('Column 1','Column 2')
    end

    %get the coherence and spectra
%     [total_coh,total_phi,~,total_PS_1,total_PS_2,total_f]=coherencyc(LFP_column_1,LFP_column_2,params);%this needs the toolbox from Rodrigo's lab to work    
%     [~,~,~,total_PS_1,total_PS_total,total_f]=coherencyc(LFP_column_1,LFP_total,params);%this needs the toolbox from Rodrigo's lab to work    
% 
%     [~,~,~,slide_PS_1,slide_PS_2,slide_t,slide_f]=cohgramc(LFP_column_1',LFP_total',[.200 .020],params);%this needs the toolbox from Rodrigo's lab to work    

    
    
    somevar = mscohere(LFP_column_2,LFP_column_1);
    [tt,lags]=xcorr(LFP_column_1,LFP_column_2, 'coeff');
    if isfield(rr.inputstruct.Pool_1_E,'External')
        stimon = [rr.inputstruct.Pool_1_E.External.t_on, rr.inputstruct.Pool_1_E.External.t_off];
        post = [rr.inputstruct.Pool_1_E.External.t_off, length(LFP_column_1)];
        [stim_coh,stim_phi,~,stim_PS_1,stim_PS_2,stim_f]=coherencyc(LFP_column_1(stimon(1):stimon(2)),LFP_column_2(stimon(1):stimon(2)),params);
        [post_coh,post_phi,~,post_PS_1,post_PS_2,post_f]=coherencyc(LFP_column_1(post(1):post(2)),LFP_column_2(post(1):post(2)),params);
    else
        post_PS_1=[];
        post_PS_2=[];
        stim_coh=[];
        post_coh=[];
        post_f=[];
    end
    
    if pictures
        figure
        hold on
        plot(total_f,total_PS_1,'b:')
        plot(total_f,total_PS_2,'r:')
        %plot(total_f,total_coh,'k')
        %plot(total_f,total_PS_total,'g:')
        ylim([0,max(total_coh)])
        plot(somevar,'g')
        xlim(params.fpass)
        xlabel('Frequency (Hz)')
        legend('Power Column 1','Power Column 2')%,'Coherency','Joint Power')
        
        figure
        plot(lags,tt)
        xlim([-200 200])
        xlabel('time (ms)')
        ylabel('Cross Correlation')
%         figure,hold on, plot(stim_f,smooth(stim_coh),'k'),plot(stim_f,smooth(stim_PS_1),'b:'),plot(stim_f,smooth(stim_PS_2),'r:'),title('During Stimulus'),%ylim([0,max(stim_coh)])
%         figure,hold on, plot(post_f,smooth(post_coh),'k'),plot(post_f,smooth(post_PS_1),'b:'),plot(post_f,smooth(post_PS_2),'r:'),title('Post Stimulus'),%ylim([0,max(post_coh)])
    end
        
    
