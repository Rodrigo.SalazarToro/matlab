#!/bin/sh
#PBS -l nodes=4:ppn=12
#PBS -N Sizes
#PBS -d /home/msu/sep/Cortex_grant
#PBS -S /bin/bash
#PBS -j oe
cd ~/Cortex_grant
matlab -nodesktop -nosplash -r "a_size_test;quit;"
