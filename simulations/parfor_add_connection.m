% add_connection adds a new connection to the FEF network 

% The following parameters need to be defined before running this script:

% type: either 'exc' (for excitatory synapses) or 'inh' (for inhibitory

% synapses)

% from: name of the efferent population of neurons.

% to: name of the afferent population of neurons.

% weight: average synaptic weight of the connection (for full connectivity)

% weights are scaled, if a connection is sparsened.

% sparseness: percentage of active connections (between 0 and 1).  

% tau: time constant of the synapse

% small_matrix: matrix defining the population connectivity, size is number

% of afferent populations times number of efferent populations 

%

% created: Jakob Heinzle 01/07



function conout = parfor_add_connection(instruct,counter, pops,w_dist,sigma_dist,dt)



from = instruct.from;

to=instruct.to;

weight=instruct.weight;

tau=instruct.tau;

sparseness=instruct.sparseness;

small_matrix=instruct.small_matrix;

longdelay=instruct.longdelay;

longdelay_var=instruct.longdelay_var;

type = instruct.type;

if isfield(instruct, 'coincidence_power')

    coincidence_tau=instruct.coincidence_tau;

    coincidence_power = instruct.coincidence_power;

    coincidence_max = instruct.coincidence_max;

    coincidence_decay_tau = instruct.coincidence_decay_tau;

end



n=counter;

% look for efferent population

n_from=0;

for k=1:pops.npops; 

   name_pop=pops.population{k}.name;

   if length(name_pop)==length(from)*strfind(name_pop,from)

    n_from=k;

   end

end

if ~n_from

    error('Invalid from population.');

end



% look for afferent population

n_to=0;

for k=1:pops.npops; 

   name_pop=pops.population{k}.name;

   if length(name_pop)==length(to)*strfind(name_pop,to)

    n_to=k;

   end

end

if ~n_to

    error('Invalid to population.');

end



 



% link connctions to populations.

if strcmp(type,'exc')

    pops.population{n_to}.input_exc=[pops.population{n_to}.input_exc, n];

elseif strcmp(type,'inh')

    pops.population{n_to}.input_inh=[pops.population{n_to}.input_inh, n];

else

   error('Invalid synapse type');

end



size_to=pops.population{n_to}.nretpos;

poolsize_to=pops.population{n_to}.poolsize;

size_from=pops.population{n_from}.nretpos;

poolsize_from=pops.population{n_from}.poolsize;



if sum(size(small_matrix)==[size_to size_from])<1.6 %presumably less than 1.6 is essentially less than the whole thing (2)? 

    error('Matrix size for connection is incompatible with populations.');

end

% 

% % general parameters the connection.

conout.type=type;

conout.from=from;

conout.n_from=n_from;

conout.to=to;

conout.n_to=n_to;

conout.weight=weight;

conout.tau=tau;

conout.sparseness=sparseness;

conout.small_matrix=small_matrix;

conout.n = n;



%create the full weight matrix

M=zeros(poolsize_to*size_to,poolsize_from*size_from);

for toto=1:size_to;

   for fromfrom=1:size_from;

      M((toto-1)*poolsize_to+1:toto*poolsize_to,(fromfrom-1)*poolsize_from+1:fromfrom*poolsize_from)=weight*small_matrix(toto,fromfrom);

   end

end

M=sparse(M);

M=select_sparse_connect(M,sparseness);

M=distribute_random(M,w_dist,sigma_dist);



if n_to==n_from % take out autapses.

M = M-diag(diag(M));

end

conout.matrix=M;



%create a matrix of delays if delay variable exists.  It is worth noting

%that at this point each cell synapses to all post-synaptic partners with

%the differnt strength, but with variable delay times

if exist('longdelay') & longdelay %if it exists and is non-zero, add a field

    %[ii,jj,zz]=find(M);

    rr=round(repmat(longdelay/dt,poolsize_from*size_from));

    conout.dlays=rr;

    %D=sparse(ii,jj,rr);

    if exist('longdelay_var') & longdelay

        standev=sqrt(longdelay_var/dt);

        devs = round(longdelay/dt+standev*randn(poolsize_from*size_from,1));

        btom = max(0,longdelay-4*standev);%get only reasonable delay values

        devs(rr<=btom)=longdelay/dt;

        conout.dlays=devs;

   

    end

    %rr=round(rr)

    conout.longestdlay=max(conout.dlays);

end





if exist('coincidence_power') & coincidence_power %if it exists and is non-zero, add a field

    conout.coincidence_tau = instruct.coincidence_tau;

    conout.coincidence_power = instruct.coincidence_power;

    conout.coincidence_max = instruct.coincidence_max;

    conout.coincidence_decay_tau = instruct.coincidence_decay_tau;

    conout.coincidence_tstep=exp(-dt/conout.coincidence_tau);

    conout.coincidence_decay_tstep=exp(-dt/conout.coincidence_decay_tau);
    
    %add in a learning matrix for coincidence learning where it is the relative

    %spike time of pre and post synaptic firing that drives everything

    [ii,jj,zz]=find(M);

    rr=ones(size(zz));

    conout.coincidence_matrix = sparse(ii,jj,rr);

    %no option to make this variable at the moment, on the assumption that

    %this is a channel property (sort of)

end



%ALSO CAN ADD IN CA POTENTIATION

%ALSO CAN ADD IN RUNDOWN



% auxiliary variables of the connection

conout.G=zeros(pops.population{n_to}.n_neurons,1);

conout.tstep=exp(-dt/conout.tau);



%save([pp,'tempfile_', num2str(counter),'.mat'],'conout')





%%Depricated code----------------------------------------------------

        %devarray=repmat(devs',size_to*poolsize_to,1);%make a per-from-cell arra

        %D=sparse(ii,jj,ones(size(zz)));

        %D=D.*devarray;

        %rr=rr+standev*randn(length(rr),1);

        %remove negative or silly delay

        %rr(rr<=btom)=longdelay; 


