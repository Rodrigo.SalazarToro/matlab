function extrn = input_maker(externalindeces, inputstruct,pops,gmaxE_ext)
%note that external inputs are only excitatory

%parfor happy format
instruct = cell([numel(externalindeces),1]);
for m=1:numel(externalindeces)
    instruct{m} = inputstruct.(externalindeces{m}{1}).(externalindeces{m}{2});
    instruct{m}.to = externalindeces{m}{1};
end
clear inputstruct

extrn = cell([numel(externalindeces),1]);
parfor m=1:numel(externalindeces)
    extrn{m} = parfor_add_input(instruct{m},m,pops,gmaxE_ext);
end
