addpath('/home/msu/sep/Cortex_grant')

%check effects of population size
size_factor = [25,250,1000,2000];
for qq=1:length(sizefactor)
    Two_column_driver({size_factor(qq)},{'ParameterSpace.size_factor'});
end    


placenow=load('/home/msu/sep/Cortex_grant/where_am_I.mat');
sparseness = [.01,.05,.1,.5];
stimstrength = [[inputstruct.Pool_1_E.External.MeanInp=[0,.02,0.056,.08,.1]];
learnpower = [0,.1,.3,.5];
for sparse_value=placenow.sparse_value:length(sparseness)
    nowsparse = sparseness(sparse_value);
    for learn_value=placenow.learn_value:length(learnpower)%will redo whatever run it was in the middle of most recently
        nowlearn = learnpower(learn_value);
        parfor stim_value=placenow.stim_value:length(stimsrength)
	nms = {'inputstruct.Pool_1_E.Pool_2_E.sparseness',...
                'inputstruct.Pool_2_E.Pool_1_E.sparseness',...
                'inputstruct.Pool_2_E.Pool_1_E.coincidence_power',...
                'inputstruct.Pool_1_E.Pool_2_E.coincidence_power',...
                '[inputstruct.Pool_1_E.External.MeanInp'};
            vls = {nowsparse,nowsparse,...
                nowlearn,nowlearn,...
                stimstrength(stim_value)};
            Two_column_driver(vls,nms);         
        end
        save('~/Desktop/Cortex_grant/MUS_mini-grant/model_results/where_am_I.mat','learn_value','sparse_value')
    end
end



% fltr=[0,.1,.3,.85,1,.85,.3,.1,0];
% stable = 75/.1+1; %things start to look OK 75ms in, which needs to be divided by dt
% 
% betarange = [70,12];
% pictures = 1;
% params = struct('Fs',1000,'fpass',[0 150],'trialave',1);
% for m=1:numel(tt)
%     %NEED TO GET THIS GUY FIXED
%     %jj=tt{m}; 
% end   
    
%Measures
%1) Each population shows beta power
%2) Beta range is less during the stimulus

