function [pths] = get_structure_tree(some_struct)
%takes in a structure (optionally with structures as elements) 
% CLEAN THIS UP AND SUBMIT IT?

nam=inputname(1);

fldnms = fieldnames(some_struct);
pths={};
for m=1:length(fldnms)
    pths=[pths;get_subfields(some_struct,fldnms{m})];
end


%unpack for deeper subfields
cont = 1;
while cont
    cont=0;
    for m=1:length(pths)
        if iscell(pths{m})
            for n=1:length(pths{m})
                pths=[pths;pths{m}{n}];
            end
            pths(m)=[];
            cont=1;
            break
        end
    end
end
    
%since apparently matlab can't grab the top level name of a structure
for m=1:length(pths)
    pths{m}=[nam,'.',pths{m}];
end


function outpt = get_subfields(some_struct,nam)
    if isstruct(eval(['some_struct.',nam]))
        fldnms = fieldnames(eval(['some_struct.',nam]));
        outpt=cell(length(fldnms),1);
        for m=1:length(fldnms)
            outpt{m}=[nam,'.',fldnms{m}];
            if isstruct(eval(['some_struct.',outpt{m}]))
                outpt{m}=get_subfields(some_struct,outpt{m});
            end
        end
    else
        outpt = nam;
    end
    
% ststruc    fldnms = fieldnames(eval(nam));
%     pths = {['']}
%     for m=1:length(fldnms)
%         pths = {pths, [nam,'.',fldnms{m}]};
%         if isstruct(eval(pths{m}))
%             pths=[pths,get_subfields(struct,pths{end})];
%         end
%     end
%         if isstruct(struct.fieldnames)
%             get_structure_tree(