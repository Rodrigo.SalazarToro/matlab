function [datafit,xx,thresh,rsquare] = fitSurface(C,f,varargin)
%*********************************************************************
%
%function [datafit,xx,thresh,rsquare] = fitSurface(C,f,varargin)
%
%
%  ********** Inputs ***********
%  C - Coherence matrix in rep x f
%  f - Vector of frequencies in Hz.
%
%  ********** Outputs **********
%  datafit - yaxis data points from fitted model.
%  xx      - xaxis data points from fitted model.
%  thresh  -  probability threshold.
%  rsquare -  goodness of fit.
%
%  ********* Optional Args *********
% The default 'Distribution' is 'GEV' but the other option is 'Norm'.
%
% 'FJump' is number of frequencies in Hz to jump for the next center frequency.
% Default is 'none' which means just jump one freq point.
%
% 'FSpread' combines this number of frequencies in Hz above and below the center
% frequency.
% The default 'FSpread' is 0.
%
% 'Prob' is the probability that a threshold is calculated at.  This
% variable is passed into the subprogram that models the histogram of a
% frequency as a probability distribution.  The default is .99.  'Prob' can
% be a vector and then a vector thresh is returned.
%
% 'ShowPlots' flag to show a 3d surface histogram plot of all frequencies
% and a surface of the modeled histograms.
%
% 'HistBinNumber' number of bins per freq for histogram of surface plot.
%
% ********** Dependents **************
% The subprogram gevfitSur.m is called.
%
%********************************************************************


Args = struct('ShowPlots',0,'HistBinNumber',100, ...
    'FJump','none','FSpread',0,'Prob',.99,'Distribution','GEV');
Args.flags = {'showPlots'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'FSpread','Prob'});


maxx = max(max(C));
minn=  min(min(C));

if Args.ShowPlots % show surface of all frequencies
    x = linspace(minn,maxx,Args.HistBinNumber);
    for freq = 1 : length(f)
        n(freq,:) = hist(C(:,freq),x);
    end

    figure
    surfl(x,f,n)
    view([50 40])
    xlabel('bin centers')
    ylabel('frequency (Hz)')
    zlabel('number of occurences in bin')
    dx = diff(x);
    title(['All Histograms at binsize of ' num2str((maxx-minn)/Args.HistBinNumber,'%0.3g')])
end

%Change from Hz to points
df = diff(f);
step = round((df-df(1))*10^5)/10^5;

if ~isempty(find(step))
    error('f must be a vector of evenly spaced frequencies')
end
FSpread = round(Args.FSpread/df(1));
if strcmp(Args.FJump,'none')
    FJump=1;
else
    FJump = round(Args.FJump/df(1));
end

%Replicate the Edge
Sindex = length(f)+1;
Eindex = 2*length(f);   
C = [C C C];
freqq = Sindex:FJump:Eindex;

%preallocate variables
thresh = zeros(length(freqq),length(Args.Prob));
rsquare = zeros(length(freqq),length(Args.Prob));

for ii=1:length(freqq)
range = [freqq(ii)-FSpread , freqq(ii)+FSpread];
    data = C(:,range(1):range(2));
    data = reshape(data,size(data,1)*size(data,2),1);
    if strcmp(Args.Distribution,'GEV')
        [datafit(ii,:),xx(ii,:),thresh(ii,:),rsquare(ii,:)] = gevfitSur(data, ...
            Args.Prob,modvarargin{:},'Maxx',maxx,'Minn',minn,'HistBinNumber',Args.HistBinNumber);
    elseif strcmp(Args.Distribution,'Norm')
        [datafit(ii,:),xx(ii,:),thresh(ii,:),rsquare(ii,:)] = normfitSur(data, ...
            Args.Prob,modvarargin{:},'Maxx',maxx,'Minn',minn,'HistBinNumber',Args.HistBinNumber);
    end

end

if Args.ShowPlots & size(datafit,1)>=3
    figure
    surfl(xx(1,:),f(freqq-length(f)),datafit)
    shading interp
    view([50 40])
    colormap copper
    xlabel('bin centers')
    ylabel('frequency (Hz)')
    zlabel('number of occurences in bin')
end
