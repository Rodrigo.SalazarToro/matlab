function [datafit,xx,thresh,rsquare] = normfitSur(data,prob,varargin)
%[datafit,Thresh,rsquare] = gevfitSur(data,prob,varargin)
%
%This function fits a Generalized Extreme Value Distribution to a histogram
%of raw data. 
%
%******** Variables ***********
%
%    data - raw data
%    datafit - fitted data normalized to input data
%    thresh - threshold at (prob)*100% probabilty for the distribution.
%    rsquare - goodness of fit 
%


Args = struct('showPlot',0,'HistBinNumber',100,'OuputResolutionGain',10,'Maxx','calculate','twoSided',0);
Args = getOptArgs(varargin,Args,'flags',{'showPlot','twoSided'});

if Args.twoSided
    minn = min(data);
else
    minn = 0;
end
if strcmp(Args.Maxx,'calculate')
    maxx = max(data);
else
    maxx = Args.Maxx;
end
x =linspace(minn,maxx,Args.HistBinNumber); 

%Histogram of data
hdata = hist(data,x);

%Generalized Extreme Value Distribution
% calculate params for the best fit to this distribution
[params(1),params(2)] = normfit(data);
xx =linspace(minn,maxx,Args.HistBinNumber*Args.OuputResolutionGain); 
datafit = normpdf(xx,params(1),params(2));
%normalize to input data
datafit = datafit/max(datafit)*max(hdata);

%find probability threshold using inververse cumulative distribution function.
thresh = norminv(prob,params(1),params(2));

%rsquared - goodness of fit
datafit1 = normpdf(x,params(1),params(2));
whichstats = {'rsquare'};
stats=regstats(datafit1,hdata,'linear',whichstats);
rsquare = stats.rsquare;

%plotting
if Args.showPlot
    hold off
    bar(x,hdata)
    hold on
    plot(xx,datafit,'r');
    
    %Plot Labels
    title(['Fitted Normal Distribution, r2=' num2str(rsquare)])
    xlabel('bin centers')
    ylabel('Number of Occurences')
    legend('Histogram of data','Fitted Normal Distribution')
end
