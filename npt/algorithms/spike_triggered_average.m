function [waveforms] = spike_triggered_average(varargin)

Args = struct('lfp',[],'spikes',[],'samplingrate',[],'surrogate',0);
Args.flags = {'surrogate'};
[Args,modvarargin] = getOptArgs(varargin,Args);

waveforms = [];
spikes = Args.spikes;

lfp = Args.lfp;
%loop through spikes
count = 0;
for sp = spikes
    
    if Args.surrogate 
        count = count+1;
        waveforms(count,1) = lfp(sp);
    else
        %take 100 data points (100ms) on either side of spike
        if (sp > 50) && (sp < (size(lfp,2) - 50))
            count = count + 1;
            waveforms(count,(1:101)) = lfp(1,((sp-50):(sp+50)));
        end
    end
end




