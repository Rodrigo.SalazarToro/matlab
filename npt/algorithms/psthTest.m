function [sigbin,firstBin,varargout] = psthTest(A,reference,varargin)
% mFR is the mean firing rate over the tested time period

Args = struct('degree',1,'nSTD',3,'binSize',20,'linearTrend',0,'polyTrend',0,'plot',0,'stimOff',500);
Args.flags = {'linearTrend','polyTrend','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args);

al = 1;
Mbins = [reference{1,al} : Args.binSize : size(A{1,al},2)+reference{1,al}] + Args.binSize/2;
mfr = zeros(9,length(Mbins));
for cue = 1: 9
    for bi = 1 : length(Mbins)-1
        mfr(cue,bi) = sum(sum(A{cue,al}(:,(bi-1)*Args.binSize +1 : bi*Args.binSize ),2),1)*1000/(Args.binSize*size(A{cue,al},1));
    end % rebin the 1mse matraix
end

baseBins = find(Mbins <= 0);

baseline(1,:) = mean(mfr(:,baseBins),1) + Args.nSTD*(std(mfr(:,baseBins),1));
baseline(2,:) = mean(mfr(:,baseBins),1) - Args.nSTD*(std(mfr(:,baseBins),1));

standD = std(reshape(mfr(:,baseBins),1,length(baseBins)*9),1);

[p,S] = polyfit(Mbins(baseBins),mean(mfr(:,baseBins),1),Args.degree);
[y,~] = polyval(p,Mbins,S);

sigbin = zeros(9,length(Mbins));
for cue = 1 : 9
    sigbin(cue,:) = mfr(cue,:) > (y + Args.nSTD * standD) + (mfr(cue,:) < (y - Args.nSTD * standD));
end

allcues = sum(sigbin,1);

sampleBins = find(Mbins > 0 & Mbins < 500);
firstBin = find(allcues(sampleBins) ~= 0);
if ~isempty(firstBin)
    firstBin = sampleBins(firstBin(1));
    firstBin = Mbins(firstBin);
end
%% test for linear trend and slope different from zero
% needs works
if Args.linearTrend
    [b,bint,r,rint,stats] = regress(mean(mfr(:,baseBins),1),[ones(length(mean(mfr(:,baseBins),1)),1) [1:length(mean(mfr(:,baseBins),1))]']);
    
    if bint(2,1) < 0 && bint(2,2) > 0
        
        slope = false;
        
    else
        slope = true;
        
    end
    varargout{1} = slope;
end
%% test for the polynome trend
% needs works
if Args.polyTrend
    [p,S] = polyfit(Mbins(baseBins),mean(mfr(:,baseBins),1),Args.degree);
    [y,delta] = polyval(p,baseline,S);
    
    sigbin = (mfr > y + Args.nSTD * delta) - (mfr > y - Args.nSTD * delta);
    
    varargout{2} = sigbin;
    
    % test each bin after baseline for significance
    
end

if Args.plot
    for sb = 1 : 9;
        subplot(9,1,sb);
        plot(Mbins,mfr(sb,:));
        hold on;
        plot(Mbins,sigbin(sb,:),'k');
        plot(Mbins,y + Args.nSTD * standD,'r')
        plot(Mbins,y - Args.nSTD * standD,'r')
    end
    
end
