function trials = stableSpikes(sp,varargin)
% function to deal with non stable units

Args = struct();
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

starttrial = []; 
for t = 1 : sp.data.numTrials
    
    if ~isempty(sp.data.trial(t).cluster.spikes) & isempty(starttrial)
        starttrial = t;
    end
    
    if ~isempty(starttrial) & isempty(sp.data.trial(t).cluster.spikes)
        endtrial = t-1;
    end
end

if ~exist('endtrial') || ~isempty(sp.data.trial(t).cluster.spikes)
    endtrial = t;
end

trials = [starttrial : endtrial];
        
        