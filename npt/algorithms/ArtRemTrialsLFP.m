function [stdTrials,PWTrials,rejectCH,varargout] = ArtRemTrialsLFP(varargin)
% to be run into the session directory
% determine the trials in which any of the channels exceed 12 fold the
% standard deviation in the lfp.
%
% option to use: - 'save' to save the file
%                - 'threshold',x for the x fold STD threshold
%
% Dependencies: getOptArgs, nptDir, getDataDirs, nptReadStreamerFile
%
%   Betty: ArtRemTrialsLFP('save','distr','sigma','Kthresh',9)

Args = struct('threshold',12,'save',0,'redo',0,'distr',0,'sigma',0,'badCH',0.04,'freqThresh',100,'plot3D',0,'resample',200,'bmf',0,'detect_transients',0,'trans_thresh',6.75);%6.8
Args.flags = {'save','redo','distr','plot3D','sigma','bmf','detect_transients'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'distr','plot3D','sigma'});

matfile = 'rejectedTrials.mat';
RS = 1000/ Args.resample;

natimage = 0;
if Args.bmf
    if exist('mts.txt','file')
        tobj = mtstrial('auto');
    elseif exist('natimages.txt','file')
        tobj = natimagetrial('auto');
        natimage = 1;
    end
else
    bhv_file = nptDir('*.bhv');
    if ~isempty(bhv_file) && strncmp('MTS',bhv_file.name,3) || isempty(bhv_file)
        tobj = mtstrial('auto');
    elseif ~isempty(bhv_file) && strncmp('DS',bhv_file.name,2)
        tobj = dstrial('auto');
    end
end
[~,cdir] = getDataDirs('lfp','relative','CDNow');
rejfile = nptDir(matfile);
params = struct('tapers',[6 11],'Fs',Args.resample,'fpass',[0 100],'pad',2);
if isempty(rejfile) || Args.redo
    
    files = nptDir('*_lfp.*');
    
    max_trial = 0;
    totpt = 0;
    for t = 1 : size(files,1)
        [lfp,num_channels,~,~,points]=nptReadStreamerChannel(files(t).name,1);
        
        %only use data that is analyzed
        if Args.bmf
            if natimage
                lfp = lfp(1,(tobj.data.image_on(t)-500):end);
            else
                lfp = lfp(1,(tobj.data.CueOnset(t)-500):(tobj.data.MatchOnset(t))); %just use first channel
            end
        end
        
        lfp = resample(lfp,Args.resample,1000);
        
        max_trial = max(max_trial,length(lfp));
        totpt = totpt + points;
    end
    [St,f] = mtspectrumc(lfp',params);
    mdata = zeros(num_channels,1);
    maxdata = zeros(num_channels, size(files,1));
    S = zeros(size(St,1),num_channels,size(files,1));
    transTrials = cell(num_channels,1);
    for t = 1 : size(files,1)
        params = struct('tapers',[6 11],'Fs',Args.resample,'fpass',[0 100],'pad',2);
        
        [lfp,num_channels,~,~,~,~] = nptReadStreamerFile(files(t).name);
        
        if Args.bmf
            if natimage
                lfp = lfp(:,(tobj.data.image_on(t)-500):end);
            else
                lfp = lfp(:,(tobj.data.CueOnset(t)-500):(tobj.data.MatchOnset(t)));
            end
        end
        lfp = resample(lfp',Args.resample,1000);
        prelfp = lfp_detrend(lfp)';
        
        if Args.sigma;
            mdata = mdata + sum(prelfp.^2,2);
            maxdata(:,t) = max(abs(prelfp),[],2);
        end
        
        %use to detect transients
        %takes first derivative and then z-scores
        %a threshold is set to detect fast deviations (transients)
        if Args.detect_transients
            dd = diff(prelfp')';
            dd = zscore(dd(:,(3:end)),0,2);
            md = min(dd(:,(3:end))'); %cut off first 2 observations
            tran = find(md <= (Args.trans_thresh * -1));
            for tt = tran
                transTrials{tt} = [transTrials{tt} t];
            end
        end
        
        notok = true;
        while notok && params.pad >= 0
            try
                [S(:,:,t),f] = mtspectrumc(prelfp',params);
                notok = false;
            catch
                params.pad = params.pad-1;
                
                if params.pad < 0 && Args.bmf %try more pads
                    params.pad = 3;
                end
            end
        end
        clear prelfp
        
    end
    noisyt = cell(num_channels,1);
    if Args.sigma
        %         chstd = std(data,0,2);
        chstd = sqrt(mdata/totpt);
        badT =  maxdata > repmat(Args.threshold .* chstd,1,size(maxdata,2));
        for ch = 1 : num_channels
            noisyt{ch} = find(badT(ch,:) ==1);
        end
    end
    clear data
    if Args.distr
        rejectCH = zeros(num_channels,1);
        flimit = find(round(f) == Args.freqThresh);
        trials = [1:size(files,1)];
        badT = cell(num_channels,1);
        kdist = cell(num_channels,1);
        for ch = 1 : num_channels
            fprintf(' Analyzing channel #%d',ch);
            
            for freq = 1 : flimit(end)
                %                 trials = setdiff(setdiff([1:size(files,1)],rejectTrials),badT{ch});
                
                if ~isempty(trials)
                    data = squeeze(S(freq,ch,trials));
                    %                     data = squeeze(S(freq+1,ch,trials));
                    [~,outliers,kdist{ch}(freq,:)] = getKurtosisThresh(data,modvarargin{:});
                    
                    badT{ch} = unique([badT{ch} trials(outliers)]);
                else
                    badT{ch} = [1:size(files,1)];
                    break
                end
            end
            if length(badT{ch}) + length(noisyt{ch}) > Args.badCH*(size(files,1))
                rejectCH(ch) = 1;
                %                 badT{ch} = [];
            end
            
        end
        rejectCH = find(rejectCH ==1);
        if Args.plot3D
            for ch = 1 : num_channels
                figure;
                
                surf([101:101+size(kdist{ch},2)],fliplr(f(1:flimit(end))),flipud(kdist{ch}))
            end
            %
        end
        
        
    end
    
    if ~Args.distr
        rejectCH = [];
        for ch = 1 : num_channels
            if length(noisyt{ch}) > Args.badCH*(size(files,1))
                rejectCH(ch) = 1;
            end
        end
        rejectCH = find(rejectCH ==1);
    end
    
    PWTrials = cell(num_channels,1);
    stdTrials = cell(num_channels,1);
    alltrials = cell(num_channels,1);
    if Args.sigma;
        stdTrials = noisyt
    end
    
    if Args.distr
        PWTrials = badT;
    end
    
    for ch = 1 : num_channels
        alltrials{ch} = unique([PWTrials{ch} stdTrials{ch}]);
    end
    varargout{1} = alltrials;
    goodCH = setdiff([1:num_channels],rejectCH);
    rejecTrials = [];
    for nch = 1 : length(goodCH)
        ch = goodCH(nch);
        rejecTrials = [rejecTrials PWTrials{ch} stdTrials{ch}];
    end
    rejecTrials = unique(rejecTrials);
    varargout{2} = rejecTrials;
    if Args.save
        save('rejectedTrials.mat','rejecTrials','stdTrials','rejectCH','PWTrials','transTrials')
    end
    
else
    load(matfile)
end
cd(cdir)


