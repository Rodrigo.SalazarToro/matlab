function [ampl,lag,varargout] = findCntPeak(data,lags)

%Creates a correlogram using the xcorr function with the specfied lag.
%Finds the peak of the correlogram (positive or negative) that is closest to the center.
%Outputs the correlogram and the lags (used for plotting)
%along with the value of the central peaks lag and its correlation coefficent as a 1 X 4 cell.

%Find index of central positive peak
shift = [];
c = 1;
for coef = [-1 1]
    pks(c) = findpeaks(coef*data);
    if ~isempty(pks(c).loc)
        [mlag,lind(c)] = min(abs(lags(pks(c).loc)));
    else
        mlag = nan;
        lind(c) = nan;
    end
    shift = [shift mlag];
    
    c = c + 1;
end

if ~isnan(shift) | ~isnan(lind) | ~isnan(mlag)
    [p,ind] = min(shift);
    
    lag = lags(pks(ind).loc(lind(ind)));
    
    ampl = data(pks(ind).loc(lind(ind)));
else
    
    lag = nan;
    ampl = nan;
end

stdr = nanstd(data);
popout = ampl / stdr;

varargout{1} = popout;



