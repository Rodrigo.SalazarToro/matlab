function [m_norm_length,m_raw] = AmplAmplMod(amplitude1,amplitude2,varargin)
Args = struct('numsurrogate',200,'minskip',200);
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args);



R = corrcoef(amplitude1,amplitude2); %% mean of z over time, prenormalized value
m_raw = R(1,2); %% compute surrogate values
surrogate_m = nan(Args.numsurrogate,1);
Args.maxskip = length(amplitude1) - Args.minskip;
skip = ceil(length(amplitude1) .* rand(Args.numsurrogate*2,1));

skip(find(skip > Args.maxskip)) = [];
skip(find(skip < Args.minskip)) = [];
skip = skip(1:Args.numsurrogate,1);
for s = 1 : Args.numsurrogate
    surrogate_amplitude = [amplitude2(skip(s):end) amplitude2(1:skip(s) - 1)];
    R = corrcoef(amplitude1,surrogate_amplitude);
    surrogate_m(s) = R(1,2);
    %     disp(Args.numsurrogate - s)
end


[surrogate_mean,surrogate_std] = normfit(surrogate_m); %% normalize length using surrogate data (z-score)
m_norm_length = (abs(m_raw) - surrogate_mean) / surrogate_std;

end

