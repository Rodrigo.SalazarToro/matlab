function [rdiff,pvalues,levels,f,varargout] = compareTwoGroupCoh(group1,group2,params,varargin)
% Input:
% group%d = sample x trials x channel(2ch)
% the difference is group2 minus group1

Args = struct('iteration',1000,'gProb',[0.05 0.01 0.001 0.0001],'FSpread',2);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'nrep'});



%% create the surrogates

nt(1) = size(group1,2);
nt(2) = size(group2,2);
pool = [group1 group2];
allt = [1 : sum(nt)];

for i = 1 : Args.iteration + 1
    if i <= Args.iteration
        seq = randperm(length(allt));
        nallt = allt(seq);
    else
        nallt = allt;
        
    end
    %     for g = 1 : 2
    g = 1;
    [C(:,g),phi,S12,S1,S2,f]=coherencyc(squeeze(pool(:,nallt(1:nt(g)),1)),squeeze(pool(:,nallt(1:nt(g)),2)),params);
    g = 2;
    [C(:,g),phi,S12,S1,S2,f]=coherencyc(squeeze(pool(:,nallt(nt(1)+1:end),1)),squeeze(pool(:,nallt(nt(1)+1:end),2)),params);
    %     end
    if i <= Args.iteration
        Cdiff(i,:) = diff(C,1,2);
    else
        rdiff = diff(C,1,2);
    end
    
end

%% fit the surrogate and get the probability

proba = [Args.gProb./2 (1 - Args.gProb./2)];
varargout{1} = C;

[datafit,xx,pvalues,rsquare] = fitSurface(Cdiff,f,'Prob',proba,'FSpread',Args.FSpread,modvarargin{:});

levels = [Args.gProb Args.gProb];


