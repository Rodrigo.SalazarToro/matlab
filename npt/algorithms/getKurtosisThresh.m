function [threshold,outliers,varargout] = getKurtosisThresh(data,varargin)
% function that calculates iteratively the kurtosis value of a population binned
% (hist) and get rid of the last percentile. Then, it assumes that teh rejections
% of all the outliers will produce a constant kurtosis value.
% so far the best parameters for Betty's data are Kthresh of 5 and alpha of
% 0.0001 and bins of 100. For clark, Kthresh is 8. For Betty with
% semi-chronic Kthresh is 9.

Args = struct('iterations',100,'bins',100,'alpha',0.0001,'Kthresh',9,'plot2D',0); %'bins',100,
Args.flags = {'plot2D'};
[Args,modvarargin] = getOptArgs(varargin,Args);

% nlimit = Args.bins*2;
alldata = [1 : length(data)];
outlier{1} = [];
for i = 1 : Args.iterations
    seldata = setdiff(alldata,outlier{i});
    if ~isempty(seldata) %& length(seldata) > nlimit
        [hdata,xout] = hist(data(seldata),Args.bins);
        kdist(i) = kurtosis(hdata);
        %         cl = find(data(seldata) >= xout(end));
        %         kdist(i) = kurtosis(data(seldata));
        [b,ix] = sort(data(seldata));
        cl = ix(end);
        outlier{i+1} = [outlier{i} seldata(cl)];
        
    else
        
        break
    end
end

slope = true;
newsel = 1;

while slope && (length(kdist) >= newsel)
    %     [klimit,kind] = min(kdist);
    x = vecc([newsel:length(kdist)]);
    y = vecc(kdist(newsel : end));
    
    [b,bint,r,rint,stats] = regress(y,[ones(length(x),1) x],Args.alpha);
    
    if  ((rint(1,1) <= 0) && (rint(1,2) >= 0)) || y(1) <= Args.Kthresh
        %     if eval(sprintf('%0.2f',bint(2,1))) <= 0 & eval(sprintf('%0.2f',bint(2,2))) >= 0
        slope = false;
        
    else
        newsel = newsel+1;
    end
    if Args.plot2D
        plot(kdist); hold on; plot(x,b(2)*x+b(1),'r'); plot(x,rint(:,1),'k'); plot(x,rint(:,2),'k');
        pause
        clf
    end
end


if newsel >= length(kdist)
    error.value = 1;
    error.message = 'not enough iterations';
    threshold = kdist(newsel-1); % -1 because of line 42
    outliers = outlier{newsel-1};
    
else
    
    threshold = kdist(newsel);
    outliers = outlier{newsel};
    
    error.value = 0;
end

varargout{1} = kdist;
varargout{2} = error;

