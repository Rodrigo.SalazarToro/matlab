function [trials,varargout] = checkForStability(groupName,varargin)
% to run in the session directory
% for spikes it will look for first and last spike

Args = struct('measure','spike');
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

switch Args.measure
    case 'spike'
        
        cd(groupName)
        cluster = nptDir('cluster*');
        for cl = 1 : size(cluster,1)
            clname = cluster(cl).name;
            %         if clname(end) == 's'
            cd(clname)
            load ispikes.mat
            t = 1;
            while t < length(sp.data.trial) && sp.data.trial(t).cluster.spikecount == 0
                t = t + 1;
            end
            
            if t == length(sp.data.trial)
                cluster(cl).firstspike = [];
                cluster(cl).lastspike = [];
                trials{cl} = [];
            else
                cluster(cl).firstspike = t;
                t = sp.data.numTrials;
                while sp.data.trial(t).cluster.spikecount == 0
                    t = t - 1;
                end
                cluster(cl).lastspike = t;
                cd ..
                varargout{1} = cluster;
                trials{cl} = [cluster(cl).firstspike : cluster(cl).lastspike];
            end
             cd(groupName)
        end
        cd ..
    case 'lfp'
        
end



