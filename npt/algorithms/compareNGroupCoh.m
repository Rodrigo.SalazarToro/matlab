function [rdiff,pvalues,levels,f,varargout] = compareNGroupCoh(groups,params,varargin)
% Input:
% groups = {sample x trials x channel(2ch)} N times
% base don the variance

Args = struct('iteration',1000,'Prob',[0.05 0.01 0.001 0.0001],'FSpread',2);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'nrep'});

%% create the surrogates
for ng = 1 : length(groups)
    nt(ng) = size(groups{ng},2);
    
    pool = cell2mat(groups);
    allt = [1 : sum(nt)];
end

% if Args.anova
%     params.trialave = 0;
%     Cm = [];
%     S12m = [];
%     gv = [];
%     for g = 1 : length(groups)
%
%         [C,phi,S12,S1,S2,f]=coherencyc(squeeze(groups{ng}(:,:,1)),squeeze(groups{ng}(:,:,2)),params);
%         % C frq x trial
%         Cm = [Cm C];
%         S12m = [S12m S12];
%         gv = [gv repmat(g,1,size(C,2))];
%     end
%     for fi = 1 : length(f)
%         pvalues(fi) = anovan(real(S12m(fi,:)),{gv},[],[],[],'off');
%
%     end
%     rdiff = std(Cm,[],2);
% else
for i = 1 : Args.iteration + 1
    if i <= Args.iteration
        seq = randperm(length(allt));
        nallt = allt(seq);
    else
        nallt = allt;
        
    end
    %     for g = 1 : 2
    for g = 1 : length(groups)
        if g ==1
            [C(:,g),phi,S12,S1,S2,f]=coherencyc(squeeze(pool(:,nallt(1:1 + nt(g)),1)),squeeze(pool(:,nallt(1:1+nt(g)),2)),params);
        else
            [C(:,g),phi,S12,S1,S2,f]=coherencyc(squeeze(pool(:,nallt(sum(nt(1:g-1))+1:sum(nt(1:g-1)) + nt(g)),1)),squeeze(pool(:,nallt(sum(nt(1:g-1))+1:sum(nt(1:g-1))+nt(g)),2)),params);
        end
        %         [C(:,g),phi,S12,S1,S2,f]=coherencyc(squeeze(pool(:,nallt(nt(1)+1:end),1)),squeeze(pool(:,nallt(nt(1)+1:end),2)),params);
    end
    %     end
    if i <= Args.iteration
        Cdiff(i,:) = std(C,[],2);
    else
        rdiff = std(C,[],2);
    end
    
end
% end
%% fit the surrogate and get the probability

proba = [1 - Args.Prob];
varargout{1} = C;

[datafit,xx,pvalues,rsquare] = fitSurface(Cdiff,f,'Prob',proba,'FSpread',Args.FSpread,modvarargin{:});

levels = [Args.Prob Args.Prob];


