function [m_norm,m_norm_length,m_norm_phase,m_raw,varargout] = PhaseAmplitudeMod(amplitude,phase,varargin)
Args = struct('numsurrogate',500,'minskip',200,'maxlag',100,'fromCanolty',0);
Args.flags = {'fromCanolty'};
[Args,~] = getOptArgs(varargin,Args);

if Args.fromCanolty
    %% complex-valued composite signal from canolty 2006
    z = amplitude.*exp(i*phase); %% mean of z over time, prenormalized value
    m_raw = mean(z); %% compute surrogate values
    surrogate_m = nan(Args.numsurrogate,1);
    Args.maxskip = length(amplitude) - Args.minskip;
    skip = ceil(length(amplitude) .* rand(Args.numsurrogate*2,1));
    
    skip(find(skip > Args.maxskip)) = [];
    skip(find(skip < Args.minskip)) = [];
    skip = skip(1:Args.numsurrogate,1);
    for s = 1 : Args.numsurrogate
        surrogate_amplitude = [amplitude(skip(s):end) amplitude(1:skip(s) - 1)];
        surrogate_m(s) = abs(mean(surrogate_amplitude.*exp(i*phase)));
        %     disp(Args.numsurrogate - s)
    end
    
    
    [surrogate_mean,surrogate_std] = normfit(surrogate_m); %% normalize length using surrogate data (z-score)
    m_norm_length = (abs(m_raw) - surrogate_mean) / surrogate_std;
    m_norm_phase = angle(m_raw);
    m_norm = m_norm_length*exp(i*m_norm_phase);
    
else
    %% from Tort 2008
    bins = [-pi:pi/9:pi];
    [~,idbins] = histc(phase,bins);
    mA= zeros(length(bins)-1,1); % mean amplitude per phase bin
    m_norm_length =zeros(Args.numsurrogate,1);
    for ii =  1 : Args.numsurrogate + 1
        if ii < Args.numsurrogate + 1
            idbinst = idbins(randperm(length(idbins)));
        else
            idbinst = idbins;
        end
        
        for bi = 1 : length(bins)-1
            mA(bi) = mean(amplitude(idbinst == bi));
        end
        
        totA = sum(mA);
        H=0;
        for bi = 1 : length(bins)-1
            pj = mA(bi) / totA;
            H = H + pj * log(pj);
        end
        
        H = -H;
        Hmax = log(length(bins)-1);
        if ii < Args.numsurrogate + 1
            m_norm_length(ii) = (Hmax-H) / Hmax;
            
        else
            m_raw = H;
            m_norm = (Hmax-H) / Hmax;
        end
        
        m_norm_phase=[];
        
    end

end