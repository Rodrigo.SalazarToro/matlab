function [power] = powerDistr(TimeSerie,varargin)
% in format TimSerie(samples x trials) or (nchannels x samples x trials)

Args = struct('tapers',[2 3],'Fs',1000,'fpass',[0 100],'keepSpectra',0);
Args.flags = {'keepSpectra'};
Args = getOptArgs(varargin,Args);

params = struct('tapers',Args.tapers,'Fs',Args.Fs,'fpass',Args.fpass,'trialave',0);



nch = size(TimeSerie,1);

if length(size(TimeSerie)) == 2
    [S,f] = mtspectrumc(TimeSerie,params);
    power.mean = mean(S,2);
    power.f = f;
    power.std = std(S,[],2);
    power.perc25 = prctile(S',25);
    power.perc50 = prctile(S',50);
    power.perc75 = prctile(S',75);
    if Args.keepSpectra
        power.S = S;
    end

else
    for ch = 1 : nch

        [St,f] = mtspectrumc(squeeze(TimeSerie(ch,:,:)),params);
        power(ch).mean = mean(St,2);
        power(ch).f = f;
        power(ch).std = std(St,[],2);
        power(ch).perc25 = prctile(St',25);
        power(ch).perc50 = prctile(St',50);
        power(ch).perc75 = prctile(St',75);
        if Args.keepSpectra
            S(:,:,ch) = St;
            power(ch).S = S;
        end
    end
end



