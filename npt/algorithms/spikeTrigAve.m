function [stac,ti,varargout] = spikeTrigAve(lfp,spikes,samplingrate,varargin)

Args = struct('range',0.05,'trialSeparation',0,'average',0);
Args.flags = {'trialSeparation','average'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{'range','trialSeparation','average'});

step = 1/samplingrate;

pointRange = Args.range * samplingrate;

ntrial = size(lfp,2);

count = 1;
for t = 1 : ntrial
    triallength = (size(lfp,1)-1) / samplingrate;;

    indx = find(spikes(t).times > (Args.range + eps + step) & spikes(t).times < (triallength-Args.range+eps+step));

    for sp = 1 : length(indx)
        Mpoint = round(spikes(t).times(indx(sp)) * samplingrate);
        limit = [Mpoint - pointRange;  Mpoint + pointRange];


        stac(count,:) = lfp(limit(1) : limit(2),t);
        trialind(count) = t;
        count = count + 1;
    end
end

ti = [-Args.range : step : Args.range];
if exist('stac') & exist('trialind')
    stac = (stac - repmat(mean(stac,1),size(stac,1),1)) ./repmat(std(stac,[],1),size(stac,1),1);
    varargout{1} = trialind;
    if Args.average
        varargout{2} = mean(stac,1);

    end
else
    stac = nan(size(ti));
    varargout{1} = [];
    varargout{2} = nan(size(ti));
end

