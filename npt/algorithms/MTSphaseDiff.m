function phiDiff = MTSphaseDiff(name,raster1,raster2,varargin)
Args = struct('save',0','redo',0,'addName',[],'minSpike',5,'minFRP',[1000 1800]);
Args.flags = {'save','redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


matfile = sprintf('phiDiff%s%s.mat',name,Args.addName);
if isempty(nptDir(matfile)) || Args.redo
    phiDiff = nan(size(raster1));
    rTrials = [];
    for tr = 1 : size(raster1,1);
        if sum(raster1(tr,Args.minFRP(1):Args.minFRP(2))) > Args.minSpike && sum(raster2(tr,Args.minFRP(1):Args.minFRP(2))) > Args.minSpike
            phiDiff(tr,:) = HBphaseSpikeTrain(raster1(tr,:),raster2(tr,:),modvarargin{:});
        else
            rTrials = [rTrials tr];
            
        end
    end
    rnan = sum(isnan(phiDiff),2) > 0;
    phiDiff(rnan,:) = [];
    if Args.save
        save(matfile,'phiDiff','rTrials')
        display([pwd '/' matfile])
    end
else
    load(matfile)
end



%%
function phasediff = HBphaseSpikeTrain(sptrain1,sptrain2,varargin)
% sptrain1 and sptrain2 must be vector of zeros and ones
Args = struct('sigma',50,'range',200,'noDetrend',0,'upperHzrange',6,'plotOneTrial',0);
Args.flags = {'detrend','plot','plotOneTrial'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});



z1 = gaussianConvSpikeTrain(sptrain1,modvarargin(:)); % convolution of spike trains

if Args.plotOneTrial
   
    subplot(5,2,1)
    plot(sptrain1)
    subplot(5,2,2)
    plot(sptrain2,'r')
    subplot(5,2,3)
    plot(z1)
    title('Gaussian convolution of spike train')
end
z1 = nptLowPassFilter(z1,1000,1,Args.upperHzrange); % low pass filtering

z2 = gaussianConvSpikeTrain(sptrain2,modvarargin{:});
if Args.plotOneTrial
    subplot(5,2,5)
    plot(z1)
    title(sprintf('low pass filtered up to %d',Args.upperHzrange))
    
    subplot(5,2,4)
    plot(z2)
    title('Gaussian convolution of spike train')
end
z2 = nptLowPassFilter(z2,1000,1,Args.upperHzrange); % low pass filtering

if Args.plotOneTrial
    subplot(5,2,6)
    plot(z2)
    title(sprintf('low pass filtered up to %d',Args.upperHzrange))
end

if ~Args.noDetrend
    z1 = z1 - mean(z1); % detrend
    z2 = z2 - mean(z2); % detrend
end
hi = hilbert(z1);
iphase1 = unwrap(mod(angle(hi)+2*pi,2*pi));
hi = hilbert(z2);

iphase2 = unwrap(mod(angle(hi)+2*pi,2*pi));
phasediff = iphase1 - iphase2;
phasediff = mod(phasediff,2*pi);

if Args.plotOneTrial
    subplot(5,2,7)
    plot(iphase1)
    title('Hilbert phase')
    subplot(5,2,8)
    plot(iphase2)
    title('Hilbert phase')
    subplot(5,2,9)
    plot(phasediff)
    title('Phase difference')
    pause
    clf
end



%%
function z = gaussianConvSpikeTrain(sptrain,varargin)


Args = struct('sig',50,'range',200);
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args,'remove',{});


x = [-Args.range:Args.range];

k = exp(-(x/Args.sig).^2/2) / (Args.sig*sqrt(2*pi)); % gaussian kernel

z = conv(sptrain,k);

z=z(ceil(length(k)/2):end-floor(length(k)/2)); % Aligned size with the spike train

