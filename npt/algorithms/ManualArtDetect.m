function ManualArtDetect
% To be run in the lfp directory
% assume that the rejectedTrials file with the new format exists

[pdir,cdir] = getDataDirs('lfp','relative','CDNow');
data = load('rejectedTrials.mat');
names = fieldnames(data);
for f = 1 : size(names,1); eval(sprintf('%s = data.%s;',names{f},names{f})); end
% displaydata

% PlotRawData

[pdir,cdir] = getDataDirs('lfp','relative','CDNow');%dirLevel('eye','relative','CDNow');
% check for saved object
display(rejectCH)
v = whos('-file','rejectedTrials.mat');
for s =  1 : length(v);
    t = strfind(v(s).name,'rejec');
    tt = strfind(v(s).name,'Trials');
    ttt = strfind(v(s).name,'PW');
    if ~isempty(t) & ~isempty(tt)   & isempty(ttt)
        rejectTrials = eval(v(s).name);
        clear rejecTrials
        id = strmatch('rejecTrials',names);
        names(id) = [];
    end;
end
display(rejectTrials)

rejectTrials = input('Your selection of bad trials (ex: [1 4 56 256]) = ');


badch = input('Do you want to modify the list of bad channels (y/n)? ','s');

if badch == 'y'

    rejectCH = input('Your selection of bad channels (ex: [1 4]) = ');
end

display(rejectTrials)
display(rejectCH)

savereply = input('Are you sure you want to save your selection (y/n)? ','s');

if savereply == 'y'
    save('rejectedTrials.mat',names{:},'rejectTrials')

    display('Rejected trials modified and saved in rejectedTrials.mat')

else
    display('Rejected trials not modified and not saved in rejectedTrials.mat')
end

cd(cdir)
