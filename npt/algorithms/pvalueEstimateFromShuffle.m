function [pvalue,varargout] = pvalueEstimateFromShuffle(surrogate,realvalue,varargin)

Args = struct('distr','GeneralizedExtremeValue');
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

pd = fitdist(surrogate,Args.distr);
varargout{1} = pd;
pvalue = cdf(pd,realvalue);