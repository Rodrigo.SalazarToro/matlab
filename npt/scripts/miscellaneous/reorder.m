function  outdata  = reorder( indata,scanOrder )
%REORDER 
%   outdata = reorder(indata,scanOrder)
%   Reorders data based on the scanorder and reorder.txt file.
%
%  Assumption -  the first channel acquired is neural data.  Triggers or
%  eyedata if acquired are after the neural data.  Also it is assumed that
%  any non-neural data is not acquired on the microdrive channels.
%  scanorder is always written as increasing numbers.

filelist = nptDir('*reorder.txt');
if ~isempty(filelist)
    ch=textread(filelist(1).name);
    display(['Reordering according to ' filelist(1).name])  
    
    
    %how many possible nueral channels?
    num_channels = length(ch);
    outdata = zeros(num_channels,size(indata,2));
    
    missing=[];
    for ii=1:num_channels
        %was this channel actually acquired?
        so = find(scanOrder==ii);
        if ~isempty(so)
            outdata(find(ch==ii),:)=indata(so,:);
        else
            m=find(ch==ii);
            missing = [missing m];
        end
    end
    outdata(missing,:)=[];
    
    outdata = [outdata ; indata((so+1:end),:)]; %add nonneural data to end.    
    
else
    outdata=indata;
    display('No reorder file found.')
end
    

