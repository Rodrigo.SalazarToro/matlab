function [spiketimes] = getSpikeInPer(sp,periods,t,p,plength)



d = vecc(sp.data.trial(t).cluster.spikes);

ind = (d >= periods(p) & d <= periods(p)+plength);

if isempty(ind)
    spiketimes = [];
   
else
    spiketimes = d(ind)-periods(p);
    
end