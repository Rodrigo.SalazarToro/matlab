function  [numChunks,chunkSize] = getChunkInfo(sessionname,varargin)
%[numChunks,chunkSize] = getChunkInfo(sessionname,chunkSize)
%
%reads the streamer file in the session dir to figure out how many chunks
%of data are in it.
if nargin>1
    chunkSize=varargin{1};
else
    chunkSize=100;
end
if findstr(sessionname,'highpass')
    [p,cwd] = getDataDirs('highpass','CDNow');
elseif findstr(sessionname,'lfp')
    [p,cwd] = getDataDirs('lfp','CDNow');
else
    [p,cwd] = getDataDirs('session','CDNow');
end

filelist = [ ...
    nptDir([sessionname '*.0*']); ...
    nptDir([sessionname '*.1*']); ...
    nptDir([sessionname '*.2*']); ...
    nptDir([sessionname '*.3*']); ...
    nptDir([sessionname '.bin']) ...
    ];
if isempty(filelist)
    warning('Could not find raw data file.')
    numChunks=0;
    chunkSize=0;
else
    dtype = DaqType(filelist(1).name);
    if strcmp(dtype,'Streamer')
        [num_channels,sampling_rate,scan_order,headersize]=nptReadStreamerFileHeader(filelist(1).name);
    elseif strcmp(dtype,'UEI')
        data = ReadUEIFile('FileName',filelist(1).name,'Header');
        sampling_rate = data.samplingRate;
        num_channels = data.numChannels;
        headersize = 90;
    elseif strcmp(dtype,'mat')
        load(filelist(1).name,'-mat');
        if ~exist('sampling_rate','var')
            try
            sampling_rate = samplingRate;
            catch
             sampling_rate =  resample_rate; 
            end
        end
        if ~exist('numChannels','var')
            try
            num_channels = size(broadband,1);
            catch
               num_channels = size(lfp,1); 
            end
        else
            num_channels = numChannels;
        end
        headersize = 90;
    else
        error('unknown file type')
    end
    if length(chunkSize)>1
        error('chunkSize length greater than 1')  %BG when does this happen?  MAybe we can get rid of this section.
        chunkSize = chunkSize(2)/sampling_rate;
    end
    chunkSize = ceil(chunkSize); %round up to the nearest second
    numChunks=[];
    for ii=1:size(filelist,1)
        numChunks = [numChunks ; ceil((filelist(ii).bytes-headersize)/2/num_channels/(chunkSize*sampling_rate))];
    end
end
%cd(cwd)



