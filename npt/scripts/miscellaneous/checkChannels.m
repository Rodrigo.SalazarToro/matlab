function [channels,comb,CHcomb,varargout] = checkChannels(varargin)


Args = struct('powerPP',0,'powerPF',0,'cohPP',0,'cohPF',0,'cohInter',0,'notCareForLFP',0,'flatGr',0,'GCcohInter',0);
Args.flags = {'powerPP','powerPF','cohPP','cohPF','cohInter','notCareForLFP','flatGr','GCcohInter'};
[Args,modvarargin] = getOptArgs(varargin,Args);




[NeuroInfo,PPch,PFch,PPcomb,PFcomb,Intercomb] = NeuronalChAssign('onlyPP','onlyPF','combPP','combPF','combInter');

if Args.notCareForLFP
    nch = [];
else
    %     [status,results] = unix('find . -name ''rejectedTrials.mat'' -print');
    %     load(sprintf('%s',results(1:end-1)));
    cd('lfp')
    skip = nptDir('skip.txt');
    rejfile = nptDir('rejectedTrials.mat');
    if ~isempty(rejfile) && isempty(skip)
        load(rejfile.name)
        if exist('rejectCH')
            nch = rejectCH;
        else
            nch = [];
        end
        if ~isempty(Intercomb)
            rm = [];
            for n = 1 : length(nch)
                [r,c] = find(Intercomb == nch(n));
                rm = [rm;r];
            end
            
            Intercomb(rm,:) = [];
        else
            Intercomb = [];
        end
    else
        nch = [];
    end
    cd ..
    
end

if Args.powerPP
    channels = setdiff(PPch,nch);
    if ~isempty(channels); comb = 1; CHcomb = -1; else comb = -1; CHcomb = -1;end
elseif Args.powerPF
    channels = setdiff(PFch,nch);
    if ~isempty(channels); comb = 1; CHcomb = -1; else comb = -1; CHcomb = -1;end
elseif Args.cohPP
    channels = setdiff(PPch,nch);
    if length(channels) > 1; comb = 2; CHcomb = nchoosek(channels,2); else comb = -1; CHcomb = -1; end
elseif Args.cohPF
    channels = setdiff(PFch,nch);
    if length(channels) > 1; comb = 2; CHcomb = nchoosek(channels,2); else comb = -1; CHcomb = -1; end
elseif Args.cohInter || Args.GCcohInter
    channels = setdiff([PPch PFch],nch);
    if ~isempty(setdiff(PPch,nch)) & ~isempty(setdiff(PFch,nch)); comb = 2; CHcomb = Intercomb; else comb = -1; CHcomb = -1; end
else
    channels = setdiff([PPch PFch],nch); if length(channels) > 1; comb = 2; CHcomb = nchoosek(channels,2); else comb = -1; CHcomb = -1; end
end

groups = NeuroInfo.groups(channels);
varargout{1} = groups;

if Args.flatGr
    cd('lfp')
    %  [status,results] = unix('find . -name ''flatGr.mat'' -print');
    skip = nptDir('skip.txt');
    flatGrFile = nptDir('flatGr.mat');
    flat.ch = [];
    flat.pairs = [];
    if ~isempty(flatGrFile) && isempty(skip)
        %         load(sprintf('%s',results(1:end-1)));
        fGr = load(flatGrFile.name);
        for ngr =  1 : length(groups)
            if ~isempty(find(groups(ngr) == fGr.flatGr))
                flat.ch = [flat.ch ngr];
                [epair,col] = find(ngr == CHcomb);
                flat.pairs = [flat.pairs epair'];
            end
        end
    else
        
        flat.ch = [];
        flat.pairs = [];
    end
    
    cd ..
    varargout{2} = flat;
end