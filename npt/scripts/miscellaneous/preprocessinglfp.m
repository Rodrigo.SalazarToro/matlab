function [prelfp,SR,varargout] = preprocessinglfp(lfp,varargin)
% lfp must be a vector (one channel at a time)
Args = struct('SR',1000,'downSR',200,'tapers60Hz',[5 9],'bandP',[],'No60Hz',0,'Zscore',0,'ZendCut',length(lfp),'detrend',0);
Args.flags = {'No60Hz','Zscore','detrend'};
[Args,modvarargin] = getOptArgs(varargin,Args);

if isempty(Args.bandP)
    [rslfp , resample_rate] = nptLowPassFilter(lfp,Args.SR,Args.downSR,1,round(0.9*Args.downSR/2));
else
    [rslfp , resample_rate] = nptLowPassFilter(lfp,Args.SR,Args.downSR,Args.bandP(1),Args.bandP(2));
end
% rslfp = resample(lfp,Args.downSR,Args.SR);

if ~Args.No60Hz
    flfp = gray_noise_remove(rslfp,Args.tapers60Hz(1),Args.tapers60Hz(2)); % NW = 5 and K = 9 from Craig experience 60 Hz removal
else
    
    flfp = rslfp;
end
%  prelfp = flfp;
if Args.detrend
     prelfp = lfp_detrend(vecc(flfp),modvarargin{:}); % take off teh linear portion of the signal
else
  
     prelfp = flfp;
end
if Args.Zscore
    me = mean(prelfp);
    st = std(prelfp);
    prelfp = (prelfp - me) / st;
else
    me = nan;
    st = nan;
end
prelfp = vecc(prelfp);
varargout{1} = [me st];
SR = Args.downSR;