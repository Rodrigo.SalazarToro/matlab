function stabilityUnits(days)

fdir = pwd;
for d = 1 : length(days)
    cd(days{d})
    cd session99/
    sdir = pwd;
    groups = nptDir('group0*');
    [~,stabFiles] = unix('find . -name ''stability.mat'' -ipath ''*group0*/cluster01m*'' ');
    if ~isempty(stabFiles)
        endf = strfind(stabFiles,'.mat');
        staFiles2 = cell(length(endf),1);
        for nf = 1 : length(endf)
            staFiles2{nf} = [pwd stabFiles(endf(nf)-31 : endf(nf)+3)];
        end
        noFiles2 = false;
    else
        noFiles2 = true;
    end
    cd sort/FD
    fddir = pwd;
    for gr = 1 : length(groups)
        thefile = nptDir(sprintf('*g%04.0fwaveforms_triggerValue.fd',str2num(groups(gr).name(end-1:end))));
        load(thefile(1).name,'-mat')
        plot(FeatureData,'.')
        if ~noFiles2
            thefile2 = ~cellfun(@isempty,strfind(staFiles2,sprintf('group%04.0f',str2num(groups(gr).name(end-1:end)))));
            if ~isempty(find(thefile2==1))
                load(staFiles2{find(thefile2==1)})
                title(sprintf('stability: %d',stability))
            end
        end
        xlabel('Time')
        ylabel('Trigger Value')
        stability = input('Stability: true, false or press ''enter'' for no change? ');
        if ~isempty(stability) && islogical(stability)
            cd(sdir)
            cd(sprintf('%s/group%04.0f/cluster01m/',sdir,str2num(groups(gr).name(end-1:end))))
            save stability.mat stability
            display(['saving ' pwd 'as ' num2str(stability)])
            cd(fddir)
        elseif ~isempty(stability) && ~islogical(stability)
            stability = input('Stability: true, false or press ''enter'' for no change? ');
            cd(sdir)
            cd(sprintf('%s/group%04.0f/cluster01m/',sdir,str2num(groups(gr).name(end-1:end))))
            save stability.mat stability
            display(['saving ' pwd ' as ' num2str(stability)])
            cd(fddir)
        end
    end
    cd(fdir)
end
