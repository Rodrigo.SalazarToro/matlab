function [channel_names,status] = getChannelNames
%   channel_names = getChannelNames
%   This function reads the descriptor file and find the channel names for
%   the data.
%   For electrode data, the group number should be used but for grouped
%   data (tetrode or maybe polytrode) the channel list should be used.
%   Grouped data can be detected when there are multiple channels with the
%   same group number.
%   This program also attempts to read an SNR_channels file from the
%   highpass directory.  It also attempts to get the reconstruction or
%   histology information.


status=1;
%is this raw data?
[session_dir,cwd]=getDataDirs('session','CDNow');
if strcmp(session_dir,cwd)
    raw=1;
else
    raw=0;
end

descriptor = nptDir('*_descriptor.txt');
if isempty(descriptor)
    status=0;
    channel_names=[];
    return
end
descriptor_info = ReadDescriptor(descriptor.name);
neurongroup_info=GroupSignals(descriptor_info);
groups = 1:size(neurongroup_info,2);
t = cellstr(num2str(transpose(descriptor_info.group)));

%get trigger and eye information
zi=find(descriptor_info.group==0);
for ii=1:length(zi)
    t{zi(ii)}=[ t{zi(ii)} ' - ' descriptor_info.description{zi(ii)}];
end


%get reconstruction or histology information
channel_names = {};
hist_flag = 0;
try
    if isempty(nptDir('*.bhv*'));
        N = NeuronalHist;
    else
        N = NeuronalHist('ml');
    end
    
    if isfield(N,'level1')
        hist_flag = 1;
    else
        display('No Histology or Reconstruction information found.')
    end
catch
    %if there is no reconstruction information then do nothing
    display('Error when looking for Histology or Reconstruction info.')
end

%get SNRs
cd(session_dir)
snr_flag=0;
if isdir('highpass') && exist([session_dir filesep 'highpass' filesep 'SNR_channels.mat'])
    snr_flag=1;
    cd('highpass')
    load SNR_channels
    snr = round(channel_snr_list(:,2)*100)/100;
    snr_groups = channel_snr_list(:,1);
    
else
    %if there is no SNR information then do nothing')
    display('No SNR information found.')
end

%get power < 10 Hz
cd(session_dir)
power_flag=0;
if isdir('lfp') && exist([session_dir filesep 'lfp' filesep 'POWER_channels.mat'])
    power_flag = 1;
    cd('lfp')
    load POWER_channels
    power = round((channel_power_list(:,2) ./ 10^6)*100)./100;
    power_groups = channel_snr_list(:,1);
else
    %if there is no power information then do nothing')
    display('No Power information found.')
end


a_groups = transpose(descriptor_info.group);
all_groups = a_groups(find(a_groups));
ni = find(descriptor_info.group~=0);
for q=1:size(ni,2)
    ggg = all_groups(q);
    hist_text='';
    snr_text='';
    power_text = '';
    if hist_flag && exist('snr_groups','var')
        area = N.level1{snr_groups == ggg}; %get area information fron NeuronalHist
        hist_text = [' - ' area];
    end
    if snr_flag
        s = snr(find(snr_groups == ggg)); %match the group to the correct snr
        snr_text = [' - ' num2str(s)];
    end
    if power_flag
        s = power(find(power_groups == ggg)); %match the group to the correct snr
        power_text = [' - ' num2str(s)];
    end
    t{ni(q)}=[ t{ni(q)} hist_text snr_text power_text];
end

%get channel names
if raw
    channel_names = t'; %get actual group names
else
    pg=0;
    for ttt = ni
        pg=pg+1;
        channel_names{pg} = t{ttt,1}; %get actual group names
    end
end

cd(cwd)