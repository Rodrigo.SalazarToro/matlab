function [ich,iCHcomb,iflat,igroups] = getFlatCh(varargin)
%at the day level

Args = struct('cohtype','cohInter','skipSession01',0);
Args.flags = {'skipSession01'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sessions = nptDir('session0*');

if Args.skipSession01
    start = 2;
else
     start = 1;
end
iflat.ch = [];
for s = start : length(sessions)
    
    cd(sessions(s).name)
    if ~isempty(nptDir('lfp'))
        [channels,comb,CHcomb,groups,flat] = checkChannels('flatGr',Args.cohtype,modvarargin{:});
        if ~exist('ich')
            ich = channels;
            iCHcomb = CHcomb;
%             iflat = flat;
            igroups = groups;
        else
            ich = intersect(ich,channels);
            if comb ~= -1 || isempty(CHcomb); iCHcomb = intersect(iCHcomb,CHcomb,'rows');else iCHcomb = []; end
%             iflat.ch = union(iflat.ch,flat.ch);
%             iflat.pairs = union(iflat.pairs,flat.pairs);
            igroups = intersect(igroups,groups);
        end
        iflat.ch = [iflat.ch flat.ch];
    end
    cd ..
end

iflat.pairs = find(sum(ismember(iCHcomb,iflat.ch),2) ~= 0) ;

