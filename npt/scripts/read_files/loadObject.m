function obj = loadObject(varargin)

if nargin == 0
    [objname,PathName,FilterIndex] = uigetfile(pwd,'Select object to load');
else
    PathName = [pwd filesep];
    objname =varargin{1};
    
end

tobj = load(sprintf('%s%s',PathName,objname));
objn = fieldnames(tobj);
eval(sprintf('obj = tobj.%s;',objn{1}));
dirsep = strfind(pwd,filesep);
pa = pwd;
pdir = pa(dirsep(end) + 1 : end);
themonkey = regexp(pa, 'monkey');
basename = pa(1:themonkey + 5);
% level = strfind(obj.data.setNames{1},pdir);

for s = 1 : obj.data.numSets
    theday = regexp(obj.data.setNames{s}, '\d');
    themonkey = regexp(obj.data.setNames{s}, 'clark');
    
    
    if isempty(themonkey); themonkey = regexp(obj.data.setNames{s}, 'betty');end
    if theday(1) - themonkey(end) > 1
        extra = eval(['tobj.' objn{1} '.data.setNames{s}(themonkey+6:theday(1)-2)']);
    else
        extra = [];
    end
    if (theday(1) + 6) < length(obj.data.setNames{s})
        rest = obj.data.setNames{s}(theday(1)+6:end);
    else
        rest = [];
    end
    %     restname = obj.data.setNames{s}(level + 1 + length(pdir): end);
    %     rlevel = strfind(restname,'/');
    %     if isempty(rlevel);  rlevel = strfind(restname,'\'); end
    %
    %     if ~isempty(rlevel) && length(rlevel) > 1;
    %         for cut = 2 : length(rlevel) ; adle{cut} = restname(rlevel(cut) -1 : rlevel(cut)); end;
    %     else
    %         adle{1} = restname;
    %     end
    
    obj.data.setNames{s} = fullfile(basename,obj.data.setNames{s}(themonkey:themonkey+5),extra,obj.data.setNames{s}(theday(1):theday(1)+5),rest);
end