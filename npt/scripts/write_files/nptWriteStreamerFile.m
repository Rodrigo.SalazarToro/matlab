function status=nptWriteStreamerFile(filename,sampling_rate,data,scan_order)
%nptWriteStreamerFile Function to write binary files from Data Streamer
%function status=nptWriteStreamerFile(filename,sampling_rate,data,scan_order)
%scanorder is optional
%bmf should be used for any

num_channels=size(data,1);

if max(scan_order) > 64
    %header_size for 256 channel bmf plus 32 channel DC coupled input 
    header_size = 586; % header_size + num_channels + scan_order + sampling_rate = 4 + 2 + (288 * 2) + 4
else
    header_size=73;
end

fid=fopen(filename,'w','ieee-le');
if fid~=-1
    status=1;
end

if nargin==3
    if max(scan_order) > 64
        scan_order=zeros(1,288);
        pad=[];
    else
        scan_order=zeros(1,64);
        pad=[];
    end
else
    if max(scan_order) > 64
        %pad scan_order to 288 numbers
        pad=zeros(1,288 - size(scan_order,1));
    else
        %pad scan_order to 64 numbers
        pad=zeros(1,64 - size(scan_order,1));
    end
end

fwrite(fid, header_size, 'int32');				% 4 bytes reserved for header size which is 73 bytes

if max(scan_order) > 64
    fwrite(fid,num_channels, 'int16'); % 2 bytes
else
    fwrite(fid,num_channels, 'uchar');			% 1 byte
end

fwrite(fid,sampling_rate, 'uint32');			% 4 bytes

if max(scan_order) > 64
    fwrite(fid,scan_order,'int16');    % 2 bytes for each channel up to 288 channels (256 + 32)
else
    fwrite(fid,scan_order,'uchar');				% 1 byte for each channel up to 64 channels
end

if max(scan_order) > 64
    fwrite(fid,pad,'int16');
else
    fwrite(fid,pad,'int8');
end

fwrite(fid, data, 'int16');
fclose(fid);
