function [vs, varargout] = plot(vs,n,varargin)
%STREAMER/PLOT Plots raw data from Data Streamer
%   OBJ = PLOT(OBJ,N) plots the raw data from the chunk specified N.
%   The streamer files are assumed to be in the current directory.
%           P-Values      V-Values
%           Units      -  {'Volts'}, 'MilliVolts', 'MicroVolts',  or  'Daqunits'
%           AmpGain    -  {5000} or specify the total amplification, MCC main amp is 500X and the MCS Head Stage is 10X
%           Stacked    -  no V-Value needed, Plots the data one channel stacked using an offset, or plots the data one channel on top of another.
%           Filter     -  Filters the Raw data either "High" or "Low" Pass, no V-Value needed
%           FilterType -  Either {'High'} 500-10000 Hz or 'Low' 0.5-150 Hz
%           HighRange  -  {[500 10000]} frequency band for high pass
%           LowRange   -  {[0.5 150]} frequency band for low pass
%           ViewerGain -  Multiply the original signal by an integer eg.'2' or '10', {'1'} is the default
%           Clean      -  Removes common PCA components from each channel
%
%   Dependencies: nptReadStreamerFile, ReadUEIFile, ViewUEIRawData.

Args = struct('showTitle',1,'linkedZoom',0,...
    'AmpGain',5000,'Units','MilliVolts','Stacked',0,'Filter',0,...
    'FilterType','High','HighRange',[500 10000],'LowRange',[0.5 150],...
    'Clean',0,'ViewerGain',1,'EMD',0,'IMF',1,'ReOrder',0);
Args = getOptArgs(varargin,Args,'flags',{'showTitle','linkedZoom',...
    'Filter','Clean'});
%    'Stacked','Filter','Clean','ReOrder'});

gcf;
clist = get(gca,'ColorOrder');
clistsize = size(clist,1);
polytrode54=0;  %This data is plotted differently than everyhting else.

channel = vs.channel;
sessionname = vs.sessionname;
chunkSize= vs.chunkSize;
numChunks = vs.numChunks;

trialbased = 0;
if size(numChunks,1)>1 %trialbased
    trialbased = 1;
    %translate chunk n into a trial
    
    b = n-[0; cumsum(numChunks)];
    
    b(b<=0)=[];
    trial = length(b);
    chunk = b(end);
    filename = [sessionname '.' num2str(trial,'%04i')];
    dtype = DaqType(filename);
else %single bin file
    chunk=n;
    filename = [sessionname '.bin'];
    dtype = DaqType(filename);
end
fprintf('Reading %s\n',filename);

%%%%%%% GET DATA %%%%%%%%%%%%
%%%Streamer
if strcmp(dtype,'Streamer')
    [numChannels,samplingRate,scanOrder]=nptReadStreamerFileHeader(filename);
    points = [chunkSize*samplingRate*(chunk-1)+1   chunkSize*samplingRate*chunk];
    if trialbased
        data = nptReadStreamerFile(filename);
    else
        data = nptReadStreamerFileChunk(filename,points);
    end
    ylab = 'MilliVolts';
    points(2)=points(1)+size(data,2)-1;%incase we did not read entire chunk b/c end of file.
    
    %%%UEI
elseif strcmp(dtype,'UEI')
    d = ReadUEIFile('FileName',filename,'Header');
    samplingRate = d.samplingRate;
    numChannels = d.numChannels;
    scanOrder = d.scanOrder;
    points = [chunkSize*samplingRate*(chunk-1)+1   chunkSize*samplingRate*chunk];
    %d = ReadUEIFile('FileName',filename,'Channels',channel,'Samples',points,'Units',Args.Units);
    d = ReadUEIFile('FileName',filename,'Samples',points,'Units',Args.Units);
    points(2)=d.timevector(end); %incase we did not read the entire chunk b/c end of file.
    
    %Filter Data
    if Args.Filter
        if strcmpi(Args.FilterType,'High')
            data = nptHighPassFilter(d.rawdata,samplingRate,Args.HighRange(1),Args.HighRange(2));
        elseif strcmpi(Args.FilterType,'Low')
            points = points/samplingRate;
            [data,samplingRate]=nptLowPassFilter(d.rawdata,samplingRate,Args.LowRange(1),Args.LowRange(2));
            points = points*samplingRate;
        end
        
        
    else
        data = d.rawdata;
    end
    
    %Apply Gain to signal
    if strcmpi(Args.Units,'daqunits')
        % No need to change to Voltage Values
        ylab = 'DAQ Units';
    elseif strcmpi(Args.Units,'Volts')
        data = data/Args.AmpGain;
        ylab = 'Volts';
    elseif strcmpi(Args.Units,'MilliVolts')
        data = data/Args.AmpGain*10^3;
        ylab = 'MilliVolts';
    elseif strcmpi(Args.Units,'MicroVolts')
        data = data/Args.AmpGain*10^6;
        ylab = 'MicroVolts';
    end
    
    if Args.EMD
        data = calcIMFs(data,Args.IMF);
    end
elseif strcmp(dtype,'mat')
    d = load(filename,'-mat');
    pnames = fieldnames(d);
    if  sum(strcmp(pnames,'lfp')) ~= 0
        data = d.lfp;
    elseif sum(strcmp(pnames,'raw')) ~= 0
        data = d.raw;
    elseif sum(strcmp(pnames,'broadband')) ~= 0
        data = d.broadband;
        elseif sum(strcmp(pnames,'highpass')) ~= 0
        data = d.highpass;
    end
   if  sum(strcmp(pnames,'samplingRate')) ~= 0
       samplingRate = d.samplingRate;
    elseif sum(strcmp(pnames,'sampling_rate')) ~= 0
        samplingRate = d.sampling_rate;
    elseif sum(strcmp(pnames,'resample_rate')) ~= 0
        samplingRate = d.resample_rate;
        elseif sum(strcmp(pnames,'RS')) ~= 0
        samplingRate = d.RS;
    end
      
    points(1) = 1;
    points(2) = size(data,2);
else
    error('unknown file type')
end

%Reorder Data
if Args.ReOrder
    data = reorder(data,scanOrder);
    if ~isempty(nptDir('polytrode54-reorder.txt'))
        polytrode54=1;
    end
end

datalength = size(data,2);
timestep = 1/samplingRate*1000; %convert to milliseconds
timev = points(1)/samplingRate*1000:timestep:points(2)/samplingRate*1000;

if Args.Clean
    data = CleanData(data); data = data'; data = data(1:length(plotchannel),:); %Transpose and take out PCA's for the plotting
end
%%%%%%%% END OF GET DATA %%%%%%%%%%%%%%%%%%%

%%%%%%%% PLOT DATA %%%%%%%%%%%%%%%%%%%%%%%%%
if Args.Stacked
    
    plotchannel = 1:length(channel);
    mx = max(data(channel(plotchannel),:),[],2);
    mn = min(data(channel(plotchannel),:),[],2);
    r = mx-mn;
    offset = 1/(length(plotchannel)+1);
    d=data(channel,:)*offset/max(r);
    % Amplify the Signals
    if Args.ViewerGain
        d = d*Args.ViewerGain;
    end
    for i=1:length(plotchannel)
        if polytrode54==1
            plot(timev(1:datalength),d(i,:)+(i*offset),'Color',clist(mod(i-1,clistsize)+1,:)) %first channel is lowest
        else
            plot(timev(1:datalength),d(i,:)+(1-i*offset),'Color',clist(mod(i-1,clistsize)+1,:))% first channels is highest.
        end
        hold on
    end
    set(gca,'FontSize',11)
    set(gca,'YTick',[offset:offset:(offset*length(channel))])
    
    channel_names = getChannelNames;
    if isempty(channel_names)
        %set(gca,'YTickLabel',plotchannel)
        %set(gca,'YTickLabel',channel(plotchannel))
        set(gca,'YTickLabel',scanOrder(fliplr(channel)))
    else
        if polytrode54==1
            set(gca,'YTickLabel',{channel_names{(channel)}})
        else
            set(gca,'YTickLabel',{channel_names{fliplr(channel)}})
        end
    end
    
    ylimits = [0 1];
    ylabel('Channel Number','FontSize',14)
    xlabel('Time (MilliSeconds)','FontSize',12)
    
else
    for ii=1:length(channel)
        plot(timev(1:datalength),data(channel(ii),:),'Color',clist(mod(ii-1,clistsize)+1,:))
        hold on
    end
    ylimits = [min2(data(channel,:)) max2(data(channel,:))];
    ylabel('spikes','FontSize',14)
    xlabel('Time (msec)','FontSize',12)
    set(gca,'FontSize',12)
end


% Set the Axis Parameters
hold off
axis tight
set(gca,'XLim',[timev(1) timev(end)])
set(gca,'YLim',ylimits)
if isempty(findobj(gcf,'Tag','streamer'))
    set(gca,'Tag','streamer')
end
% set zoom to on
zoom on
varargout{1} = {'Args',Args,'xLimits',[timev(1) timev(end)]};



