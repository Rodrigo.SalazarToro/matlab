function [obj,varargout] = plot(obj,varargin)
%@nptgroup/plot Plot function for nptgroup class
%   OBJ = plot(OBJ,N,'Object',{'CLASS',{'PLOT_OPTIONS'},{'CONSTRUCTOR_
%   OPTIONS'}) loops over the cluster directories contained in OBJ, 
%   instantiates an object of the type CLASS with 'auto' and
%   'CONSTRUCTOR_OPTIONS', and calls the plot function of that object 
%   with 'PLOT_OPTIONS'. 
%
%   e.g. plot(ng,1,'Object',{'ispikes',{'chunkSize',4}});

Args = struct('Object','','GroupEvent',0,'GroupPlotSep','');
[Args,varargin2] = getOptArgs(varargin,Args,'flags',{'GroupEvent'}, ...
    'remove',{'GroupEvent'});

% default values for variables
groupN = 1;
cellN = 1;

% if there are numeric arguments, figure out who it is for
if(~isempty(Args.NumericArguments))
    if(Args.GroupEvent)
        groupN = Args.NumericArguments{1};
        if(groupN>obj.data.numSets)
            groupN = obj.data.numSets;
        end
    else
        cellN = Args.NumericArguments{1};
    end
end

% get relevant directories
startIndex = obj.data.numCellsIndex(groupN) + 1;
endIndex = obj.data.numCellsIndex(groupN + 1);
cellDirs = {obj.data.cellNames{startIndex:endIndex}};
% get number of directories
numDirs = endIndex - startIndex + 1;

plotObject = Args.Object;
% get number of columns in Object cell array
[oRows,oCols] = size(plotObject);
% there should be at least 1 column in plotObject and that should be
% the plot object
pObject = plotObject{1};
% set plotOptions and objOptions to empty cell array by default
plotOptions = {};
objOptions = {};
if(oCols>1)
	% get the plot options
	plotOptions = plotObject{2};
end
if(oCols>2)
	% get the constructor options
	objOptions = plotObject{3};
end

% check if we need to separate the plots
% instantiate empty object so we can query plot properties
emptyObject = feval(pObject);
sepaxis = 'No';
% get plot properties from object
if(numDirs>1)
	if(isempty(Args.GroupPlotSep))
		objPlotProps = get(emptyObject,'GroupPlotProperties',numDirs,plotOptions{:});
		sepaxis = objPlotProps.separate;
    else
        sepaxis = Args.GroupPlotSep;
	end
end
% get axes positions
axesPositions = separateAxis(sepaxis,numDirs);

% save current directory
origDir = pwd;
% go to group directory
cd(obj.data.setNames{groupN})
cwd=pwd;
% check oCols outside loop to optimize performance inside loop
colors = nptDefaultColors(1:numDirs);
h=zeros(numDirs,1);
delete(gca);   %bug in R2006b with subplot requires this.
for i = 1:numDirs
	% change directory to each cell and call plot function for object
	cd(cellDirs{i})
	% instantiate object with arguments in 3rd column of Object
	thisObj= feval(pObject,'auto',objOptions{:});
	h(i) = subplot('Position',axesPositions(i,:));
	% h = [h hc];
	% plot with arguments in 2nd column of Object
% 	try
% 		[thisObj, outputs] = plot(thisObj,cellN,plotOptions{:},'GroupPlots',numDirs, ...
% 			'GroupPlotIndex',i,'Color',colors(i,:));
%	catch
        % pass varargin2 to plot function so that arguments like LabelsOff
        % from nptdata/plot will be handled properly
		plot(thisObj,cellN,plotOptions{:},'GroupPlots',numDirs, ...
			'GroupPlotIndex',i,'Color',colors(i,:),varargin2{:});
        ylabel(thisObj.data.cellname);
%	end
    % reset axis position since Matlab7 changes the dimensions of the axis
    % after plotting.
%     if(strcmp(version('-release'),'14'))
%         set(hc,'Position',axesPositions(i,:));
%     end
	hold on
	cd(cwd)
end
% if exist('outputs','var')
% varargout{1} = {outputs{:},'handle',h};
% end
% change the title
% title(getDataDirs('ShortName',obj.data.setNames{groupN}))
hold off
cd(origDir)
