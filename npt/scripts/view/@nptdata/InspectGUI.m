function fig = InspectGUI(varargin)
%NPTDATA/InspectGUI Inspect object
%   FIG = InspectGUI(OBJ, VARARGIN) displays a graphical user interface to
%   step through data contained in OBJ.
%
%   The optional input arguments are:
%      holdaxis - specifies whether to hold the axis limits constant
%                 across plots.
%      addObjs - specifies that the following cell array contains
%                additional objects that should be plotted at the
%                same time.
%      optArgs - specifies that the following cell array contains
%                optional input arguments for the various objects.
%                If this option is not specified, and there is only
%                one object, the remaining arguments are assumed to
%                be optional input arguments.
%      OverPlot - flag specifying that the plots for the different
%                 objects should be plotted on top of one another.
%   Examples:
%   InspectGUI(rf,'addObjs',{rf},'optArgs',{{},{'recovery'}})
%   InspectGUI(bi,'addObjs',{pi,rt})
%
%   FIG = InspectGUI(OBJ,'holdaxis','addObjs',{OBJ1,OBJ2},'optArgs',{{}})


% This is the machine-generated representation of a Handle Graphics object
% and its children.  Note that handle values may change when these objects
% are re-created. This may cause problems with any callbacks written to
% depend on the value of the handle at the time the object was saved.
%
% To reopen this object, just type the name of the M-file at the MATLAB
% prompt. The M-file and its associated MAT-file must be on your path.

load InspectGUI

h0 = figure('Units','characters', ...
    'Color',[0.8 0.8 0.8], ...
    'Colormap',mat0, ...
    'Position',[64.8 9.692307692307693 102.4 29.46153846153846], ...
    'Tag','Fig1');
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'Callback','InspectCB Previous', ...
    'Position',[0.216796875 0.01 0.134765625 0.05483028720626632], ...
    'String','Previous', ...
    'Tag','Pushbutton1');
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'Callback','InspectCB Next', ...
    'Position',[0.642578125 0.01 0.134765625 0.05483028720626632], ...
    'String','Next', ...
    'Tag','Pushbutton1');
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'Callback','InspectCB PlotOptions', ...
    'Position',[0.85 0.01 0.134765625 0.05483028720626632], ...
    'String','Plot Options', ...
    'Tag','Pushbutton1');
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'Callback','InspectCB Number', ...
    'Position',[0.486328125 0.01 0.115234375 0.05744125326370757], ...
    'Style','edit', ...
    'Tag','EditText1');
h1 = uicontrol('Parent',h0, ...
    'Units','normalized', ...
    'Position',[0.359375 0.01 0.12109375 0.04438642297650131], ...
    'String','Number:', ...
    'Style','text', ...
    'Tag','StaticText1');
h1 = axes('Parent',h0, ...
    'CameraUpVector',[0 1 0], ...
    'CameraUpVectorMode','manual', ...
    'Color',[1 1 1], ...
    'ColorOrder',mat1, ...
    'Position',[0.072265625 0.138 0.8828125 0.7806788511749347], ...
    'Tag','Axes1', ...
    'XColor',[0 0 0], ...
    'YColor',[0 0 0], ...
    'ZColor',[0 0 0]);

if nargout > 0, fig = h0; end

% by default, use the first argument as the object. This can be changed
% with the optional argument 'multipleObjs'.
obj{1} = varargin{1};
s.overplot=0;
s.linkedzoom = 0;
s.removexlabels=0;
% parse input argument to see if we need to overide object's HoldAxis property
% first argument is the object so remove it
[varargin,num_args] = removeargs(varargin,1,1);
i = 1;
while(i <= num_args)
    if ischar(varargin{i})
        switch varargin{i}
            case('HoldAxis')
                % grab value
                val = varargin{i+1};
                if strcmp(val,'true')
                    s.holdaxis = 1;
                else
                    s.holdaxis = 0;
                end
                % remove argument from varargin
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('multObjs')
                obj = varargin{i+1};
                s.ev = event(1,get(obj{1},'Number'));
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('addObjs')
                % objs = varargin{i+1};
                % obj = {obj{1}, objs{:}};
                obj = {obj{1}, varargin{i+1}{:}};
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('Groups')
                s.groups = varargin{i+1};
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('optArgs')
                s.optargs = varargin{i+1};
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('dir')
                s.dir = varargin{i+1};
                [varargin,num_args] = removeargs(varargin,i,2);
                i = i - 1;
            case('OverPlot')
                s.overplot = 1;% varargin{i+1};
                [varargin,num_args] = removeargs(varargin,i,1);
                i = i - 1;
            case('RemoveXLabels')
                s.removexlabels = 1;% varargin{i+1};
                [varargin,num_args] = removeargs(varargin,i,1);
                i = i - 1;
            case('LinkedZoom')
                s.linkedzoom = 1;
                % get the next argument
                if(length(varargin)>=(i+1) && varargin{i+1}==1)
                    [varargin,num_args] = removeargs(varargin,i,2);
                elseif(length(varargin)>=(i+1) && varargin{i+1}==0)
                    [varargin,num_args] = removeargs(varargin,i,2);
                    s.linkedzoom = 0;
                else
                    [varargin,num_args] = removeargs(varargin,i,1);
                end
                i = i - 1;
        end
        i = i + 1;
    else
        % not a character, just skip over it
        i = i + 1;
    end

end


s.dir{1} = nptPWD;

% get total number of objects
nobj = length(obj);
% if there are multiple objects, initialize structure for number of
% objects
if (nobj>1)
    ndir = length(s.dir);
    if (ndir~=nobj)
        % if there are not enough directories, use the first directory,
        % which is the current directory, to fill in the rest
        for i=(ndir+1):nobj
            s.dir{i} = s.dir{1};
        end
    end
    if ~isfield(s,'optargs')
        noptArgs = 0;
    else
        noptArgs = length(s.optargs);
    end
    if (noptArgs~=nobj)
        % there should be the same number of optArgs as objects
        % if an object does not have arguments, an empty cell array
        % should still be present. Need empty cell arrays instead
        % of empty numerica arrays created by cell(n,m) in order for
        % the optional arguments to be passed on properly
        for i=(noptArgs+1):nobj
            s.optargs{i} = {};
        end
    end
else
    if ~isfield(s,'optargs')
        if (num_args>0)
            % if there are remaining arguments, and there is only 1 object with
            % no optArgs, assume they are for the object
            s.optargs  = {{varargin{:}}};
        else
            % set optargs to empty cell array to prevent errors
            s.optargs = {{}};
        end
    end
end

% pass optional arguments for the object as well so that the get
% function can figure events using the optional arguments that
% the plot function is getting
s.ev = event(1,get(obj{1},'Number',s.optargs{1}{:},varargin{:}));
s.holdaxis = get(obj{1},'HoldAxis');

for ii=1:nobj
    cd(s.dir{ii})
    % subplot(nobj,1,ii)
    if(~s.overplot)
        s.subplot = nptSubplot(nobj,ii,varargin{:});
    end
    %pass optional arguments in a form that can be recognized as varargin
    %    try
    %         [s.obj{ii}, outputs] = plot(obj{ii},1,s.optargs{ii}{:});
    %
    %         Args = struct('Args',[],'handle',[],'xLimits',[]);
    %         s.Arg(ii) = getOptArgs(outputs,Args);
    %    catch
    s.obj{ii} = plot(obj{ii},1,s.optargs{ii}{:});
    s.Arg=[];
    %    end
    if(s.overplot)
        hold on
    end
end

if(s.overplot)
    hold off
end

if s.holdaxis
    ax = axis;
    s.lm = limits(ax(3),ax(4));
end


%set all axis to the same x range
h=[];
if(nobj>1 && ~s.overplot)
    for ii=1:size(s.Arg,2)
        if isfield(s.Arg(ii).Args,'linkedZoom') & s.Arg(ii).Args.linkedZoom==1
            h= [h , s.Arg(ii).handle];
            axes(h(1))
        end
    end
end
if(~isempty(h))
%    linkedzoom(h,'onx')
%    linkedzoom('out')
    linkaxes(h,'x')
elseif s.linkedzoom & length(findobj(h0,'Type','axes'))>1
%    linkedzoom(h0,'onx')
%    linkedzoom('out')
    ah = findobj(gcf,'Type','Axes');
    ll=findobj(ah,'Tag','legend');
    [~,loc]= ismember(ll,ah);
    ah(loc)=[];    
    linkaxes(ah,'x') 
    h = zoom;
set(h,'Motion','horizontal')
else
    zoom xon
end

if s.removexlabels
    ah = findobj(h0,'Type','Axes');
    ah = ah(2:end);
    set(ah,'XTick',[]);
    xh = get(ah,'XLabel');
    if length(xh)>1
        xh=cell2mat(xh);
    end
    set(xh,'String','')
end


f=fieldnames(s.obj{1});
if sum(strcmp(f,'title'))==1
    set(gcf,'Name',getfield(s.obj{1},'title'))
end
if sum(strcmp(f,'sessionname'))==1
    set(gcf,'Name',getfield(s.obj{1},'sessionname'))
end



set(h0,'UserData',s);
