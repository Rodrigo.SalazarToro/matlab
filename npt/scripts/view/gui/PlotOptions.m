function varargout = PlotOptions(varargin)
% PLOTOPTIONS M-file for PlotOptions.fig
%      PLOTOPTIONS, by itself, creates a new PLOTOPTIONS or raises the existing
%      singleton*.
%
%      H = PLOTOPTIONS returns the handle to a new PLOTOPTIONS or the handle to
%      the existing singleton*.
%
%      PLOTOPTIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTOPTIONS.M with the given input arguments.
%
%      PLOTOPTIONS('Property','Value',...) creates a new PLOTOPTIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotOptions_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotOptions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotOptions

% Last Modified by GUIDE v2.5 02-Jun-2006 17:52:39

% Begin initialization code - DO NOT EDIT
% 09/06/2006 edit the gui_Singleton to create multiple PlotOptions window
% if multiple figures are created.
if varargin{1} == 1
    gui_Singleton = 1;
else
    gui_Singleton = 0;
end
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PlotOptions_OpeningFcn, ...
                   'gui_OutputFcn',  @PlotOptions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before PlotOptions is made visible.
function PlotOptions_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotOptions (see VARARGIN)


ph = varargin{1};    %parent handle
pud = get(ph, 'UserData');  %parent user data

numObjects = size(pud.obj,2);

vert = 1;
height = 46.15384615384615;
width = 90.0;
sizeRatio = 1;

for ii=1:numObjects
    
    vert = vert - .15;
    vert1 = vert;
    vert2 = vert;
    
    %box
    %title
    if isfield(pud.Arg(ii).Args,'flags')
        flagsCollect{ii} = {pud.Arg(ii).Args.flags};
        pud.Arg(ii).Args = rmfield(pud.Arg(ii).Args,'flags');
    else
        flagsCollect{ii}={};
    end
    optionNames = fieldnames(pud.Arg(ii).Args);
    numOptions = size(optionNames,1);
    
    for jj = 1:numOptions
        %optionName
        if sum(strcmp(optionNames(jj),flagsCollect{ii}{:})) 
            %flag
            vert1 = vert1 - .04;
        else
            %value
            vert2 = vert2 - .04;
            
        end
    end
    
    if vert1>vert2
        vert = vert2;
    else
        vert = vert1;
    end
end
vert = vert - .05;
vert = vert - .05;
if vert >= 0
    height = (1-vert)*height;
    set (gcf, 'Position', [103.80000000000001 15.153846153846207 width height]);
    sizeRatio = 1/(1-vert);
end

vert = 1;

% initialize the PlotOptions interface
for ii=1:numObjects
    
    if ii == 1
        vert = vert-.05*sizeRatio;
    else
        vert = vert - .1;
    end
    ht{ii} = uicontrol('Parent',gcf,'Units','characters', ...
                       'Position',[0 vert*height 0.2*width .027*height*sizeRatio], ...
                       'FontUnits','normalized','FontSize',.9, ...
                       'FontWeight','bold', ...
                       'String',['Object ', num2str(ii), ':'],'Style','text');
    vert = vert - .05*sizeRatio;
    vert1 = vert;
    vert2 = vert;
    
    %box
    %title
    flags = flagsCollect{ii};

    optionNames = fieldnames(pud.Arg(ii).Args);
    numOptions = size(optionNames,1);
    
    for jj = 1:numOptions
        hf{ii}{jj} = [];
        ho{ii}{jj} = [];
        hv{ii}{jj} = [];
        
        %optionName
        value = eval(['pud.Arg(ii).Args.' optionNames{jj}]);
        if sum(strcmp(optionNames(jj),flags{:})) 
            %flag
            hf{ii}{jj} = uicontrol('Parent',gcf,'Units','characters', ...
                                   'Position',[.03*width vert1*height .35*width .025*height*sizeRatio], ...
                                   'FontUnits','normalized','FontSize',.8, ...
                                   'FontWeight','normal', ...
                                   'String',optionNames(jj), ...
                                   'Style','checkbox','Value',value);
            vert1 = vert1 - .04*sizeRatio;
        else
            testNum = 'c';
            if isnumeric(value)
                value = num2str(value);
                testNum = 'n';
            end
            %value
            ho{ii}{jj} = uicontrol('Parent',gcf,'Units','characters', ...
                                   'Position',[.37*width vert2*height .3*width .025*height*sizeRatio], ...
                                   'FontUnits','normalized','FontSize',.83, ...
                                   'FontWeight','normal', ...
                                   'String',optionNames(jj),'Style','text');
            hv{ii}{jj} = uicontrol('Parent', gcf,'Units','characters', ...
                                   'Position',[.68*width vert2*height .25*width .0285*height*sizeRatio], ...
                                   'FontUnits','normalized','FontSize',.83, ...
                                   'FontWeight','normal', ...
                                   'String',value,'Style','edit', 'Tag', testNum);
            
            vert2 = vert2 - .04*sizeRatio;
            
        end
    end
    
    if vert1>vert2
        vert = vert2;
    else
        vert = vert1;
    end
end

% create a checkbox for PopulationPlot option
vert = vert - .05*sizeRatio;
hff = uicontrol('Parent',gcf,'Units','characters', ...
                'Position',[.03*width vert*height .35*width .025*height*sizeRatio], ...
                'FontUnits','normalized','FontSize',.8, ...
                'FontWeight','normal', ...
                'String','PopulationPlot', ...
                'Style','checkbox', 'Value',pud.PopulationPlot);

% collect the handlesData
handlesData = [];
for ii = 1:numObjects  
    for jj = 1:numOptions
        handlesData = [handlesData, hf{ii}{jj}, ho{ii}{jj}, hv{ii}{jj}];
    end
end
handlesData = [handlesData, hff];

% create a push button to update data
vert = vert - .05*sizeRatio;
updateCB = 'updatebutton_Callback ';
argInCB = ['(gcbo, [], get(gcf, ''UserData''), ' num2str(ph, '%.13f') ')'];
updateCB = [updateCB argInCB];
h_push = uicontrol('Parent',gcf, 'Units','characters', ...
                   'Position',[.75*width vert*height .15*width .04*height*sizeRatio], ...
                   'FontUnits','normalized', 'FontSize',.55, ...
                   'FontWeight','bold', 'Callback',updateCB, ...
                   'String','Update', 'Style','pushbutton', 'Tag','updatebutton');
                   
                  
% edit the callback command of slider
clbk = '';
for ii = 1:numObjects
    temPos = get(ht{ii}, 'Position');
    line = ['set(', num2str(ht{ii}, '%.13f'), ', ''Position'', [', ...
            '0 ', num2str(temPos(2)),'-get(gcbo, ''value'') ', ...
            '.2*', num2str(width), ' ', ...
            '.027*', num2str(height), '*', num2str(sizeRatio), '])'];
    if ii ~= numOptions
        clbk = [clbk, line, ', '];
    else
        clbk = [clbk, line];
    end
    
    for jj = 1:numOptions
        if ~isempty(hf{ii}{jj})
            temPos = get(hf{ii}{jj}, 'Position');
            line = ['set(', num2str(hf{ii}{jj}, '%.13f'), ', ''Position'', [', ...
                    '.03*', num2str(width), ' ', ...
                    num2str(temPos(2)),'-get(gcbo, ''value'') ', ...
                    '.31*', num2str(width), ' ', ...
                    '.025*', num2str(height), '*', num2str(sizeRatio), '])'];
            clbk = [clbk, line, ', '];
        end
        
        if ~isempty(ho{ii}{jj})
            temPos = get(ho{ii}{jj}, 'Position');
            line = ['set(', num2str(ho{ii}{jj}, '%.13f'), ', ''Position'', [', ...
                    '.33*', num2str(width), ' ', ...
                    num2str(temPos(2)),'-get(gcbo, ''value'') ', ...
                    '.29*', num2str(width), ...
                    ' .025*', num2str(height), '*', num2str(sizeRatio), '])'];
            clbk = [clbk, line, ', '];
            temPos = get(hv{ii}{jj}, 'Position');
            line = ['set(', num2str(hv{ii}{jj}, '%.13f'), ', ''Position'', [', ...
                    '.63*', num2str(width), ' ', ...
                    num2str(temPos(2)),'-get(gcbo, ''value'') ', ...
                    '.3*', num2str(width), ...
                    ' .0285*', num2str(height), '*', num2str(sizeRatio), '])'];
            clbk = [clbk, line, ', '];
        end
    end
end

temPos = get(hff, 'Position');
line = ['set(', num2str(hff, '%.13f'), ', ''Position'', [', ...
        '.03*', num2str(width), ' ', ...
        num2str(temPos(2)), '-get(gcbo, ''value'') ', ...
        '.31*', num2str(width), ...
        ' .025*', num2str(height), '*', num2str(sizeRatio), '])'];
clbk = [clbk, line, ', '];
    
temPos = get(h_push, 'Position');
line = ['set(', num2str(h_push, '%.13f'), ', ''Position'', [', ...
        '.75*', num2str(width), ' ', ...
        num2str(temPos(2)), '-get(gcbo, ''value'') ', ...
        '.15*', num2str(width), ...
        ' .04*', num2str(height), '*', num2str(sizeRatio), '])'];
clbk = [clbk, line];

% create a slider at the right side of the PlotOptions window
if vert < 0
    sldrPos = [.95*width, 0, .05*width, height];
    sldrMax = 0;
    sldrMin = (vert-.05)*height;
    h_sldr = uicontrol ('Parent',gcf, 'Style','slider', 'Units','characters', ...
                        'Callback', clbk, ...
                        'Position',sldrPos, 'Min',sldrMin, 'Max',sldrMax, ...
                        'SliderStep',[0.1 0.5], 'Value',0);
end


% Choose default command line output for PlotOptions
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% save the handlesData on gcf
set (gcf, 'UserData', handlesData);

% UIWAIT makes PlotOptions wait for user response (see UIRESUME)
% uiwait(handles.figure1);




% --- Outputs from this function are returned to the command line.
function varargout = PlotOptions_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
set(hObject, 'Visible', 'off');


