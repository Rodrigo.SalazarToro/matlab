function InspectCB(action)

switch(action)
    
    case 'Previous'
        s = get(gcbf,'UserData');
        [n,s.ev] = Decrement(s.ev);
        
    case 'Next'
        s = get(gcbf,'UserData');
        [n,s.ev] = Increment(s.ev);
        
    case 'Number'
        s = get(gcbf,'UserData');
        % get string
        str = get(gcbo,'String');
        % try converting string to number
        n = str2num(str);
        % if not a number, try searching for first index that matches
        if(isempty(n))
            % check if str starts with 's:' which indicates a search string
            if(strncmpi('s:',str,2))
                n = name2index(s.obj{1},sscanf(str,'s: %s'),s.optargs{1}{:});
            else
                n = name2index(s.obj{1},str,s.optargs{1}{:});
            end
        end
        [s.ev,n] = SetEventNumber(s.ev,n);
        
    case 'Quit'
        close(gcbf)
        return
        
    case 'Load'
        [filename,path] = uigetfile('*.*','Select data file');
        if(filename~=0)
            cd(path);
            obj = CreateDataObject(filename);
        end
    case 'PlotOptions'
        H=gcbf;
        s = get(H,'UserData');
        if isfield('s','plotOptions') & ishandle(s.plotOptions)
            figure(s.plotOptions)
        else
            h = PlotOptions(H,s);
            s.plotOptions=h;
            set(H,'UserData',s)
        end
        return
end

% get number of objects
nobj = length(s.obj);
delete(findobj(gcf,'Type','axes'))
% get original directory
cwd = pwd;
for ii=1:nobj
    cd(s.dir{ii})
    if(~s.overplot)
        % get subplot layout
        a = s.subplot;
        subplot(a(1),a(2),ii);
        % need to call subplot a second time to work around problem when
        % using plotyy (see Solution Number: 1-19HLP in Mathworks's tech
        % solution database). This bug is supposed to be fixed in R14.
        subplot(a(1),a(2),ii);
    end
    %pass optional arguments in a form that can be recognized as varargin
    %     try
    %         [s.obj{ii}, outputs] = plot(s.obj{ii},n,s.optargs{ii}{:});
    %         Args = struct('Args',[],'handle',[],'axisLimits',[]);
    %         Arg(ii) = getOptArgs(outputs,Args,'remove',{'axisLimits'});
    %     catch
    s.obj{ii} = plot(s.obj{ii},n,s.optargs{ii}{:});
    Arg=[];
    %     end
    if(s.overplot)
        hold on
    end
end
% return to original directory
cd(cwd)
if(s.overplot)
    hold off
end
s.Arg=Arg;

if s.holdaxis
    ax = axis;
    [s.lm,lmin,lmax] = update(s.lm,ax(3),ax(4));
    axis([ax(1) ax(2) lmin lmax]);
end

h=[];
if(nobj>1 && ~s.overplot)
    for ii=1:size(s.Arg,2)
        if isfield(s.Arg(ii).Args,'linkedZoom') & s.Arg(ii).Args.linkedZoom==1
            h= [h , s.Arg(ii).handle];
            axes(h(1))
        end
    end
end

if s.removexlabels
    ah = findobj(gcbf,'Type','Axes');
    ah = ah(2:end);
    set(ah,'XTick',[]);
    xh = get(ah,'XLabel');
    if length(xh)>1
        xh=cell2mat(xh);
    end
    set(xh,'String','')
end

if s.linkedzoom & length(findobj(gcbf,'Type','axes'))>1
    %linkedzoom(gcbf,'onx')
    %linkedzoom('out')
    ah = findobj(gcbf,'Type','Axes');
    ll=findobj(ah,'Tag','legend');
    [~,loc]= ismember(ll,ah);
    ah(loc)=[];    
    linkaxes(ah,'x')
     h = zoom;
    set(h,'Motion','horizontal')
elseif(~isempty(h))
    % linkedzoom(h,'onx');
    % linkedzoom('out');
    ah = findobj(gcbf,'Type','Axes');
    linkaxes(ah,'x')
    h = zoom;
    set(h,'Motion','horizontal')
else
    zoom xon
end

edithandle = findobj(gcbf,'Tag','EditText1');
set(edithandle,'String',num2str(n))
set(gcbf,'UserData',s);

try
    
    if sum(strcmp('PlayMovie',s.optargs{:}))
        pause(.00001)
        drawnow
        InspectCB('Next')
    end
end


