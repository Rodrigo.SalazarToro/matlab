function deduceFlatCh(varargin)
% To be run at the day level
Args = struct('trustSorting',0,'fromSNR',0,'snrThres',1.8);
Args.flags = {'trustSorting','fromSNR'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sessions = nptDir('session0*');


[ch,allCHcomb,iflat,groups] = getFlatCh('cohtype','cohInter');

flat = [];
for s = 1 : length(sessions)
    
    cd(sessions(s).name)
    
    if Args.fromSNR
        cd highpass
        file = nptDir('SNR_channels.mat');
        if ~isempty(file); 
            load(file.name,'channel_snr_list'); 
        flat = channel_snr_list(channel_snr_list(:,2) < Args.snrThres,1);
        elseif ~exist('flat')
            flat = [];
            
        end
        cd ..
        
    else
        gr = nptDir('group0*');
        
        
        for g = 1 : length(gr); gnum(g) = str2num(gr(g).name(end- 3 : end)); end
        
        if exist('gnum') && ~isempty(gnum) ; flat = [flat setdiff(groups,gnum)];end
    end
    cd ..
end

if ~isempty(flat)
    cd session01/lfp
    if Args.trustSorting || Args.fromSNR
        
        flatGr = unique(flat);
    else
        newCh = setdiff(flat,groups(iflat.ch));
        
        load('flatGr.mat');
        flatGr = unique([flatGr newCh]);
        
    end
    save('flatGr.mat','flatGr')
    cd ../..
end

