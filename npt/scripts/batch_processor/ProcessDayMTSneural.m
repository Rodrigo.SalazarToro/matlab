
diary([pwd filesep 'diary.txt']);
CombineSessionRule % CombineSessionRule('deleteOriSession')
ProcessDay('timing','eye','extraction','threshold',4,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','wintermute','yes'},'lowpass','highpass');
mname = which('moveprocessedfiles.sh');
[status1,w] = system(mname);
% display w so we can see what happened
fprintf('%s\n',w);

% sessions = nptDir('session*'); sesSize = length(sessions);MoveProcessedFiles(sessions,sesSize)
ProcessDay(nptdata,'nptSessionCmd','eyemvt(''auto'',''SacThresh'',60,''save'');'); % mtstrial(''auto'',''save'');');
mts =ProcessDay(mtstrial,'NoSites','auto','save');
% InspectGUI(mts,'ObjPos','percentCR','Hist')
diary off;

% mtsday = ProcessDay(mtstrial,'auto');
% InspectGUI(mtsday,'ObjPos','percentCR')