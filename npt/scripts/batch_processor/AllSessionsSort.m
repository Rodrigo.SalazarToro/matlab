function AllSessionsSort(varargin)
% to be run at the day level
% the processing only works with mac and linux

%MatchSortSessions used after sorting has been done on session99

%keep_groups: keeps old groups. all old groups are deleted by default.

Args = struct('redo',0,'save',0,'dir',0,'process',0,'noFlatCh',0,'MatchSortSessions',0,'keep_groups',0,'onlygroup',[],'nstd',5,'skipSession01',0);
Args.flags = {'redo','save','dir','process','noFlatCh','MatchSortSessions','keep_groups','skipSession01'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

if Args.dir
    dir = uigetdir;
else
    dir = pwd;
end
cd(dir)

filename = 'AllsessionSort.txt';
thefile = nptDir(filename);
ndir = 'session99';
mkdir(ndir)
if Args.skipSession01; startS = 2; else startS = 1; end
sessions = nptDir('session0*');
nses = length(sessions);

if nses > 1
    for s = startS : nses
        cd(sessions(s).name)
        
        files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
        
       
        nfiles(s) = length(files);
        cd(dir)
        %         clear files
    end
    if isempty(thefile) || Args.redo
        if ~Args.redo; mkdir(ndir);end
        for type = 1 : 2 % for raw and highpass data
            for s = startS : nses
                cd(sessions(s).name)
                desc = nptDir('*_descriptor.txt');
                ptd = strfind(desc.name,'_');
                ndesc = desc.name;
                ndesc(ptd-2:ptd-1) = num2str(99);
                if type == 2
                    cd('highpass')
                end
                files = [nptDir('C*~*'); nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
            
                for ff = 1 : length(files); da= nptReadStreamerChannel(files(ff).name,1);if isempty(da) || strcmp(files(ff).name(1),'C'); delete(files(ff).name); display(files(ff).name); end; end
                 files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
                nfiles(s) = length(files);
                if type == 2
                    %             pt = strfind(files(1).name,'_');
                    sesdir = sprintf('%s/%s',sessions(s).name,'highpass');
                else
                    
                    sesdir = sessions(s).name;
                    copyfile(desc.name,sprintf('%s/%s/%s',dir,ndir,ndesc));
                end
                pt = strfind(files(1).name,'.');
                for f = 1 : nfiles(s);
                    if s == startS
                        
                        newfiles{f} = files(f).name;
                        linfiles{f} = sprintf('%s/%s/%s%',dir,sesdir,files(f).name);
                    else
                        newfiles = [newfiles sprintf('%s%04.0f',files(f).name(1:pt),length(newfiles)+1)];
                        linfiles = [linfiles sprintf('%s/%s/%s%',dir,sesdir,files(f).name)];
                    end
                    
                end
                cd(dir)
            end
            
            cd(ndir)
            if type == 2
               if ~isdir('highpass'); mkdir('highpass');end
                cd('highpass')
            end
            
            for f = 1 : length(newfiles)
                if type == 2
                    ptt = strfind(files(1).name,'_');
                    
                else
                    ptt = strfind(files(1).name,'.');
                    
                end
%                 newfiles{f}(ptt-1) = num2str(nses + 1);
                newfiles{f}(ptt-2:ptt-1) = num2str(99);
                if Args.redo
                    eval(sprintf('! rm %s',newfiles{f}));
                end
                eval(sprintf('! ln -s %s %s',linfiles{f},newfiles{f}));
                fprintf('linking %s to %s \n',linfiles{f},newfiles{f});
            end
            clear newfiles linfiles files
            cd(dir)
        end
        sfid = fopen('AllsessionSort.txt','wt');
        fclose(sfid);
    end
    
    %% checking
    
    cd(ndir)
    rfiles = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
    
    cd('highpass')
    hfiles = [nptDir('*_highpass.0*'); nptDir('*_highpass.1*'); nptDir('*_highpass.2*')];
    
    if length(rfiles) == sum(nfiles) && length(hfiles) == sum(nfiles)
        
        proceed = true;
    elseif Args.MatchSortSessions
         proceed = true;
    else
        proceed = false;
        error('different # of trials \n')
    end
    cd(dir)
    
    %% Processing
    
    if proceed && Args.process
        cd(ndir)
        ProcessSession('extraction','threshold',Args.nstd,'highpasshigh',7000,'sort_algo','KK','clustoptions',{'Do_AutoClust','yes','remotekk','no'},modvarargin{:});
        mname = which('moveprocessedfiles.sh');
        
        [status1,w] = system(sprintf('! chmod ugo+x %s ',mname));
        cd(dir)
        [status1,w] = system(mname);
    end
    
    if Args.MatchSortSessions && proceed
        
        curdir = pwd;
        if ~Args.keep_groups %get rid of old groups in each session
            for  s = startS : nses
                cd(sessions(s).name)
                %get groups
                gr = nptDir('group*');
                for g = 1:length(gr)
                    rmdir(gr(g).name,'s')
                end
                cd(curdir)
            end
        end

        cd(ndir)
        %     [status, clusters] = unix('find . -name ''ispikes.mat''');
        if isempty(Args.onlygroup); list = 'group0*'; else list = sprintf('group%04.0f',Args.onlygroup); end
        clusters = tar('tgroups',list);
        is = strfind(clusters{1},'ispikes.mat') - 1;
        
        for  s = startS : nses
            cd(sprintf('/%s/%s',dir,ndir))
            untar('tgroups.tar',sprintf('%s/%s',dir,sessions(s).name));
            cd(dir)
            cd(sessions(s).name)
            sesdir = pwd;
            for cl = 1 : length(clusters)
                cd(clusters{cl}(1:is))
                if s == startS
                    thefiles = [1 : nfiles(s)];
                else
                    start = sum(nfiles(1:s-1)) ;
                    thefiles = [ start + 1: start + nfiles(s)];
                end
                ispikes('redo','fromSession99',thefiles,'save','useSort',0,'ShortName');
                
                cd(sesdir)
            end
            
        end
        
    end
end
