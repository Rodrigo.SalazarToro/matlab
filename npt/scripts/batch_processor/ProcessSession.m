function ProcessSession(varargin)
%ProcessSession		Process session data
%	ProcessSession(ARG1,ARG2,...) performs the calculations specified
%	by the arguments ARG1, ARG2, etc., or all the calculations if there
%	are no arguments. This function should be called from the directory
%	containing all the data from one session. The arguments can be of the
%	following:
%		'eye'			Peforms calculations on only the eye signals.
%		'extraction'	Performs spike extraction on unit signals. If
%		                extraction is redone then the FD folder is deleted.
%                       By default, the extraction algorithm looks only for
%                       local minima, although this behavior can be
%                       modified using the 'PositiveOnly' and
%                       'NegativePositive' options described below.
%		'highpass'		High-pass filters broadband signals only.
%       'highpasslow'   Highpass Low Cutoff Frequency.  Default is 500.
%       'highpasshigh'  Highpass High Cutoff Frequency.  Default is 10000.
%		'redo'			Performs the relevant calculations regardless of
%						of whether the calculations have already been
%						performed.
%		'lowpass'		Low-pass filters broadband signals only. This
%						calculation is not performed unless explicitly
%						requested.
%       'lowpasslow'    Lowpass Low Cutoff Frequency.  Default is 1.
%       'lowpasshigh'   Lowpass High Cutoff Frequency.  Default is 200.
%       'timing'        Checks the duration of the first control trigger
%                       if the Control trigger channel was acquired. Saves
%                       the onset of the Presenter triggers if that channel
%                       was acquired. Saves the datapoints corresponding to
%                       vertical syncs if Presenter's vertical sync channel
%                       was acquired. This option is not performed by
%                       default.
%       'threshold'     Uses the number of standard deviations passed in
%                       after this argument during the extraction process.
%                       6 standard deviations is the default.
%       'sort_algo'     Uses the automatic spike sorting algorithm passed in
%                       following this argument.  Either 'none' , 'BB' or 'KK'.
%                       'KK' is the default.
%       'clustoptions'  Optional input arguments for RunClustBatch.
%                       (e.g. ProcessSession('clustoptions',{'Do_AutoClust',
%                       'no'}).
%       'groups'        Performs processes only on groups passed in.  ie.
%                       'groups',[2 5] would only do processing on groups 2
%                       and 5. Group numbers are the order of the groups in
%                       the descriptor file and not the actual group number.
%       'PositiveOnly'  Extracts waveforms by looking for local maxima.
%       'NegativePositive' Extracts waveforms by looking first for local
%                          minima and then local maxima.
%       'chunkSize'     defines the amount of data to process at a time.
%                       Default is 1 second.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        How do I calculate the SNR?
%        -On .bin (streamer) or .0001 (streamer) use the following command
%         ProcessSession('SNR_highpass','redo')
%
%        -If the .bin file is to large then specify the number of chunks
%         ProcessSession('SNR_highpass','SNR_chunks',10,'redo')
%
%        -To change the number of standard deviations use the SNR_std argument (default is 4)
%         ProcessSession('SNR_highpass','SNR_chunks',10,'SNR_std',3,'redo')
%
%        -If the SNR is already computed then use SNR_redo to recalculate
%         ProcessSession('SNR_highpass','SNR_chunks',10,'SNR_std',3,'SNR_redo','redo')
%
%       SNR Arguments:
%       'SNR_highpass'  Calculates SNR of highpass data.
%       'SNR_chunks'    Number of chunks in seconds for .bin data.
%       'SNR_std'       Number of stadard deviations for calculation (defaults is 4).
%       'SNR_redo'      Re-calculates SNR.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Alternately, some arguments can be passed in as:
%       ['argument type' 'Value'], 'Value'
%       redo, extraction, eye, highpass, lowpass, timing, have this
%       functionality.  ie. 'redoValue','1' is the same as 'redo'.
%
%	Dependencies: nptDir, nptFileParts, nptPWD, nptReadStreamerFile,
%		nptGaussianConv, nptEyeCalibAnalysis, nptWriteStreamerFile,
%		nptLowPassFilter, nptHighPassFilter, ChannelIndex, ExtractorWrapper,
%		nptTetrodeExtractor, nptWriteDat, ReadDescriptor, GroupSignals.

%argument to select what data type to process
eyeflag=0;
extractionflag=0;
lowpassflag=0;
highpassflag=0;
timingflag=0;
threshold_flag=0;
SNR_flag=0;
chunkSize=10;  %10 second default
groups_flag=0;
sort_algo='none';
single_flag=0;
reorderFlag=0;

SNR_highpass = 0;
SNR_chunks = [];
SNR_std = 4;
SNR_redo = 0;

% variable used to check if we are to extract from a broadband signal that has
% been high-pass filtered already
extracthighpass=0;
% variable used to store the directory path to the session directory
% used mainly for broadband signals which have highpass subdirectories
dirprefix = '';
clustoptions = {};

% default value for variable so we can skip extraction if there are no
% relevant signals
sumX = [];

if ~isempty(varargin)
    num_args = nargin;
    
    redo=0;
    for i=1:num_args
        if(ischar(varargin{i}))
            switch varargin{i}
                case('eye')
                    eyeflag=1;
                case('eyeValue')
                    eyeflag = varargin{i+1};
                case('extraction')
                    extractionflag=1;
                case('extractionValue')
                    extractionflag = varargin{i+1};
                case('lowpass')
                    lowpassflag=1;
                case('lowpassValue')
                    lowpassflag = varargin{i+1};
                case('lowpasslow')
                    lowpasslow = varargin{i+1};
                case('lowpasshigh')
                    lowpasshigh = varargin{i+1};
                case('highpass')
                    highpassflag=1;
                case('highpassValue')
                    highpassflag = varargin{i+1};
                case('highpasslow')
                    highpasslow = varargin{i+1};
                case('highpasshigh')
                    highpasshigh = varargin{i+1};
                case('timing')
                    timingflag = 1;
                case('timingValue')
                    timingflag = varargin{i+1};
                case('redo')% if passed redo argument, ignore the marker file
                    % processedsession.txt
                    redo=1;
                case('redoValue')
                    redo = varargin{i+1};
                case('threshold')
                    threshold_flag=1;
                    extract_sigma=varargin{i+1};
                    %extractionflag=1;
                case('SNRExtractionCutoff')
                    SNRExtractionCutoff = varargin{i+1};
                    if ~isempty(SNRExtractionCutoff) & ~isspace(SNRExtractionCutoff)
                        SNR_flag=1;
                    else
                        SNRExtractionCutoff=0;
                    end
                case('SNRSortCutoff')
                    SNRSortCutoff = varargin{i+1};
                    if ~isempty(SNRSortCutoff) & ~isspace(SNRSortCutoff)
                        SNR_flag=1;
                    else
                        SNRSortCutoff=0;
                    end
                case('chunkSize')
                    chunkSize = varargin{i+1};
                    %                     if ~isempty(chunkSize) & ~isspace(chunkSize)
                    %                         chunksize_flag=1;
                    %                     end
                case('sort_algo')
                    sort_algo=varargin{i+1};
                case('clustoptions')
                    clustoptions = varargin{i+1};
                case ('groups')
                    groups=varargin{i+1};
                    if ~isempty(groups) & ~isspace(groups)
                        groups_flag=1;
                        extractionflag=1;
                    end
                case ('UEI')
                    ReadUEI = 1;
                    
                    
                case ('SNR_highpass')
                    SNR_highpass = 1;
                case ('SNR_chunks')
                    SNR_chunks = varargin{i+1};
                case ('SNR_std')
                    SNR_std = varargin{i+1};
                case ('SNR_redo')
                    SNR_redo = 1;
            end % switch
        end % if(ischar)
    end
    %check if redo was the only argument, in which case
    %we need to redo everything
    if redo==1 & num_args==1
        eyeflag=1;
        extractionflag=1;
        lowpassflag=1;
        highpassflag=1;
    end
else % if ~isempty(varargin) & ~isempty(varargin{1})
    eyeflag=1;
    extractionflag=1;
    lowpassflag=1;
    highpassflag=1;
    redo=0;
end % if ~isempty(varargin) & ~isempty(varargin{1})

% should we skip this session
marker = nptDir('skip.txt');
% check for processedsession.txt unless redo is 1
if redo==0
    marker=[marker nptDir('processedsession.txt')];
end

if ~isempty(marker)
    return;
end
seslist = nptDir('*_descriptor.txt','CaseInsensitive');
% make sure seslist not 0, e.g. when we are using a fake
% calibration session
if size(seslist,1)==0
    return;
end
sessionname = seslist(1).name(1:end-15);
fprintf('\t\tPROCESSING SESSION %s ...\n',sessionname);

%%%%%%	Get Descriptor Info	%%%%%
descriptor_info = ReadDescriptor(seslist.name);
neurongroup_info=GroupSignals(descriptor_info);
if groups_flag==0
    groups = 1:size(neurongroup_info,2);
end

ini = nptDir('*.ini','CaseInsensitive');
inisize = size(ini,1);
for j = 1:inisize
    if isempty(findstr(ini(j).name,'_rfs'))
        isCalib=IsEyeCalib(ini(j).name);
        if isCalib==1
            init_info = ReadIniFileWrapper(ini(j).name);
        else
            fprintf('Not Eye Calibration!\n')
        end
        break;
    end
end

for i = 1:descriptor_info.number_of_channels
    if ((strcmp(descriptor_info.description{i},'broadband')) & (strcmp(descriptor_info.state{i},'Active')))
        % check if there are broadband signals in this session which have been
        % high-passed filtered that we just want to run the extraction on.
        if highpassflag==0 & extractionflag==1
            extracthighpass = 1;
        end
    end
end

if extractionflag & redo    %feature files need to be recalculated
    if isdir('sort')
        cd('sort')
        if isdir('FD')
            cd('FD')
            delete('*.fd')
            cd ..
        end
        cd ..
    end
end


durations=[];
num_spikes=[];
tmeans=[];
thresholds=[];
trigDurations=[];
presTrigOnsets=[];
syncOnsets=[];
smSyncs=[];

% If extracthighpass, we want to get the high-passed data from the
% highpass directory instead
if extracthighpass==1
    cd('highpass');
    dirprefix = ['..' filesep];
end

filelist = [nptDir([sessionname '*.0*']); ...
    nptDir([sessionname '*.1*']); ...
    nptDir([sessionname '*.2*']); ...
    nptDir([sessionname '*.3*']); ...
    nptDir([sessionname '.bin']); ...
    ];
numTrials = size(filelist,1);

if isempty(filelist)
    error('Could not find raw data files.')
else
    dtype = DaqType(filelist(1).name);
    [~,rawname] = fileparts(filelist(1).name);  %could be '*_highpass.*' or not.
    if strcmp(dtype,'Streamer')
        [num_channels,sampling_rate,scan_order,headersize] = nptReadStreamerFileHeader(filelist(1).name);
        %         headersize = 73; headersize is larger for neuralynx data
    elseif strcmp(dtype,'UEI')
        data = ReadUEIFile('FileName',filelist(1).name,'Header');
        sampling_rate = data.samplingRate;
        num_channels = data.numChannels;
        headersize = 90;
    elseif strcmp(dtype,'mat')
        load(filelist(1).name,'-mat')
        headersize = 90;
        try
            num_channels = numChannels;
        catch
            num_channels = size(broadband,1);
        end
        try
            sampling_rate = samplingRate;
        catch
            sampling_rate = sampling_rate;
        end
    else
        error('unknown file type')
    end
    chunkSize = ceil(chunkSize); %round up to the nearest second
    numChunks=[];
    for ii=1:numTrials
        nc = ceil((filelist(ii).bytes-headersize)/2/num_channels/(chunkSize*sampling_rate));
        if (nc==0)
            % print warning
            fprintf('Warning: %s is empty!\n',filelist(ii).name);
            % check if empty trial is the last trial present
            if(ii~=numTrials)
                % create skip.txt and break out of loop
                sfid = fopen([dirprefix 'skip.txt'],'wt');
                return;
            end
            % remove this trial from trials
            numTrials = ii - 1;
            % create file to indicate that the last file is incomplete
            incfid = fopen('emptylasttrial.txt','wt');
            fclose(incfid);
            % break out of loop
            break;
        end
        numChunks = [numChunks; nc];
    end
end

if(timingflag==1)
    % get list of sync files
    smdirlist = nptDir([dirprefix '*.snc0*']);
end

if timingflag==1
    smtrials = size(smdirlist,1);
    if (smtrials~=0) & (smtrials~=numTrials)
        fprintf('\t\tWarning: Number of sync monitor files do not match number of trial files!\n');
        fprintf('\t\tWarning: Skipping analysis of sync monitor files!\n');
        smtrials = 0;
    end
end

% initialize data in case we don't enter the loop
eyedata=[];
broadband=[];
lowpass=[];
uunit=[];
unit=[];
eyescanorder=[];
lowpassscanorder=[];
broadbandscanorder=[];

% use shortcut or operator so we don't have to check everything if
% one is true
if eyeflag || extractionflag || lowpassflag || highpassflag || timingflag
    totalChunks = sum(numChunks);
    for n=1:totalChunks  %loop over chunks
        % clear data before processing next trial
        eyedata=[];
        broadband=[];
        lowpass=[];
        uunit=[];
        unit=[];
        eyescanorder=[];
        lowpassscanorder=[];
        broadbandscanorder=[];
        
        if size(numChunks,1)>1
            trialbased=1;
            %translate chunk n into a trial
            b = n-[0;cumsum(numChunks)];
            b(b<=0)=[];
            trial = length(b);
            chunk = b(end);
            filename = [rawname '.' num2str(trial,'%04i')];
            dtype = DaqType(filename);
            fprintf('Reading %s Chunk%i of %i Total-%i.\n',filename,chunk,numChunks(trial),totalChunks);
        else
            trialbased=0;
            chunk=n;
            filename = [rawname '.bin'];
            dtype = DaqType(filename);
        end
        %
        %get data
        if strcmp(dtype,'Streamer')
            [numChannels,samplingRate,scanOrder]=nptReadStreamerFileHeader(filename);
            points = [chunkSize*samplingRate*(chunk-1)+1   chunkSize*samplingRate*chunk];
            data = nptReadStreamerFileChunk(filename,points);
        elseif strcmp(dtype,'UEI')
            d = ReadUEIFile('FileName',filename,'Header');
            samplingRate = d.samplingRate;
            numChannels = d.numChannels;
            scanOrder = d.scanOrder;
            points = [chunkSize*samplingRate*(chunk-1)+1   chunkSize*samplingRate*chunk];
            d = ReadUEIFile('FileName',filename,'Samples',points,'Units','MilliVolts');
            data = d.rawdata;
        elseif strcmp(dtype,'mat')
            load(filename,'-mat');
            try
                data = raw;
                
            catch
                data = broadband;
            end
            try
                samplingRate =  sampling_rate;
            catch
                
            end
            points = size(data,2);
            scanOrder = [1 : size(data,1)];
            numChannels = size(data,1);
        else
            error('unknown file type')
        end
        
        % get sampling_rate in ms since that is used more often
        srms = samplingRate/1000;
        
        % check to see if we are supposed to be extracting from high-pass filtered data
        if extracthighpass==1
            % just put the data in unit since presumably we have checked the
            % Active/Inactive state before
            uunit=data;
        else
            data = reorder(data,scanOrder);
            
            for j=1:numChannels
                des = sprintf('%s',descriptor_info.description{j});
                switch des
                    case ('vertical1')
                        if strcmp(descriptor_info.state{j},'Active')  & eyeflag==1
                            eyedata(1,:)=data(j,:);
                            eyescanorder = [eyescanorder; descriptor_info.channel(j)];
                        end
                    case('horizontal1')
                        if strcmp(descriptor_info.state{j},'Active') & eyeflag==1
                            eyedata(2,:)=data(j,:);
                            eyescanorder = [eyescanorder; descriptor_info.channel(j)];
                        end
                    case{'lfp','lowpass'}			%lfp is an outdated description but still used for backcompatibility.
                        if strcmp(descriptor_info.state{j},'Active') & lowpassflag==1
                            lowpass = [lowpass ; data(j,:)];
                            lowpassscanorder = [lowpassscanorder; descriptor_info.channel(j)];
                        end
                    case('broadband')
                        if strcmp(descriptor_info.state{j},'Active') & highpassflag==1
                            broadband = [broadband ; data(j,:)];
                            broadbandscanorder = [broadbandscanorder; descriptor_info.channel(j)];
                        end
                        if strcmp(descriptor_info.state{j},'Active') & lowpassflag==1
                            lowpass = [lowpass ; data(j,:)];
                            lowpassscanorder = [lowpassscanorder; descriptor_info.channel(j)];
                        end
                    case {'electrode','highpass','tetrode'}		%electrode and tetrode are outdated descriptions but still used for backcompatibility.
                        if strcmp(descriptor_info.state{j},'Active') & extractionflag==1
                            uunit=[uunit ; data(j,:)];
                        end
                        if strcmp(descriptor_info.state{j},'Active') & lowpassflag==1
                            lowpass = [lowpass ; data(j,:)];
                            lowpassscanorder = [lowpassscanorder; descriptor_info.channel(j)];
                        end
                    case ('trigger')
                        if timingflag==1
                            fprintf('extracting trigger duration/');
                            tdur = nptThresholdCrossings(data(j,:),2500,'falling');
                            if length(tdur)>1
                                % print warning
                                fprintf('Warning: Trigger falls below threshold more than once!\n');
                            end
                            % column 1 in trigDurations is in data points and
                            % column 2 is in ms so subtract 1 from data point
                            % since data point 1 is 0 ms.
                            % trigDurations = [trigDurations; tdur(1) (tdur(1)-1)/srms];
                            trigDurations = [trigDurations; tdur(1)];
                        end
                    case ('pres_trig')
                        if timingflag==1
                            fprintf('extracting Presenter trigger onsets/');
                            presTrigOnsets = concatenate(presTrigOnsets,nptThresholdCrossings(data(j,:),2500,'rising'),0,'DiscardEmptyA');
                        end
                    case ('pres_sync')
                        if timingflag==1
                            fprintf('extracting sync onsets/');
                            syncOnsets = concatenate(syncOnsets,nptThresholdCrossings(data(j,:),2500,'rising'),0,'DiscardEmptyA');
                        end
                end % switch des
            end % for j=1:num_channels
        end % if extracthighpass==1
        
        % if processing timing, gets syncs from sync monitor file
        if (timingflag==1) & (smtrials~=0)
            fprintf('reading sync monitor file/');
            syncs = nptReadSyncsFile([dirprefix smdirlist(n).name]);
            % need to transpose syncs before concatenating since it is a column vector
            % pad with -1 since sync points can't be negative
            smSyncs = concatenate(smSyncs,syncs',-1,'DiscardEmptyA');
        end
        
        % indent twice since this is usually called by ProcessDay, which is called by
        % ProcessDays
        % fprintf('\t\t');
        if ~isempty(eyedata)
            if isCalib
                fprintf('creating calibration dxy matrix/');
                [eyefilt , resample_rate, G, sigma, SamplesPerMS] = nptGaussianConv(eyedata,sampling_rate);
                [eyefilt,number_spikes] = nptRemoveNoiseSpike(eyefilt,resample_rate);
                fprintf(['Removing ' num2str(number_spikes) ' Artifacts/']);
                dxy.meanVH(1:2,n) = nptEyeCalibAnalysis(eyefilt , G , sigma, SamplesPerMS);
            end
            %process all eye signals including calibration sessions
            fprintf('subsampling eye/');
            if ~isCalib
                [eyefilt , resample_rate, G, sigma, SamplesPerMS] = nptGaussianConv(eyedata,sampling_rate);
                [eyefilt,number_spikes] = nptRemoveNoiseSpike(eyefilt,resample_rate);
                fprintf(['Removing ' num2str(number_spikes) ' Artifacts/']);
            end
            eyefiltfilename = [filename '_eyefilt.' trialname];
            status=nptWriteDataFile(eyefiltfilename , resample_rate , eyefilt);
        end
        
        if ~isempty(lowpass)
            fprintf('lowpass filtering/');
            if ~exist('lowpasslow','var')
                lowpasslow = 1;
            end
            if ~exist('lowpasshigh','var')
                lowpasshigh = 200;
            end
            [lfp , resample_rate] = nptLowPassFilter(lowpass,sampling_rate,lowpasslow,lowpasshigh);
            
            if trialbased
                lfpfilename = [sessionname '_lfp.' num2str(trial,'%04i')];
            else
                lfpfilename = [sessionname '_lfp.bin'];
            end
            if chunk == 1
                %open new file
                if strcmp(dtype,'mat')
                    save(lfpfilename, 'resample_rate' , 'lfp' , 'lowpassscanorder')
                else
                    nptWriteStreamerFile(lfpfilename , resample_rate , lfp , lowpassscanorder);
                end
            else
                %append to file
                status=nptAppendStreamerFile(lfpfilename , resample_rate , lfp , lowpassscanorder);
            end
        end
        
        if ~isempty(broadband)
            fprintf('highpass filtering broadband/');
            if ~exist('highpasslow','var')
                highpasslow = 500;
            end
            if ~exist('highpasshigh','var')
                highpasshigh = 10000;
            end
            broadband = nptHighPassFilter(broadband,sampling_rate,highpasslow,highpasshigh);
            if trialbased
                highpassfilename = [sessionname '_highpass.' num2str(trial,'%04i')];
            else
                highpassfilename = [sessionname '_highpass.bin'];
            end
            if chunk == 1
                %open new file
                if strcmp(dtype,'mat') && ~exist(highpassfilename,'file')
                    save(highpassfilename,'sampling_rate','broadband','broadbandscanorder')
                else
                    nptWriteStreamerFile(highpassfilename,sampling_rate,broadband,broadbandscanorder);
                end
            else
                %append to file
                status=nptAppendStreamerFile(highpassfilename , sampling_rate,broadband,broadbandscanorder);
            end
            
            %now combine broadband and uunit while maintaining channel sequence order
            if ~isempty(uunit)
                [unitindex bbindex] = ChannelIndex (descriptor_info);
                ucounter=0;
                bbcounter=0;
                for k=1:numChannels
                    a=find(unitindex == k);
                    if ~isempty(a)
                        ucounter=ucounter+1;
                        unit=[unit ; uunit(ucounter,:)];
                    end
                    a=find(bbindex == k);
                    if ~isempty(a)
                        bbcounter=bbcounter+1;
                        unit = [unit ; broadband(bbcounter,:)];
                    end
                end
            else
                unit=[unit ; broadband];
            end
        else
            unit=uunit;
            clear uunit;
        end
        
        if extractionflag
            if ~isempty(unit)
                if threshold_flag==0
                    extract_sigma=6;
                end
                if SNR_flag
                    fprintf('calculating extraction threshold and SNR/');
                    [SNR(:,n),noiseSTD(:,n)] = CalcSNRandThreshold(unit,neurongroup_info,groups);
                else
                    fprintf('unit extracting/');
                    [sumX(:,:,n), sumX2(:,:,n), nn(:,n)] = extractionThreshold(unit,neurongroup_info,extract_sigma,groups);
                end
            end
        end
        fprintf('\n');
        clear data;
    end	%loops over trials
end %if any flags are checked
clear unit;



%%%%%%%%%%%%% EXTRACTION %%%%%%%%%%%%%

if( extractionflag )%& ~isempty(ExtractThreshold) )
    if SNR_flag
        %average noiseSTD and SNR across all trials
        SNR = mean(SNR,2);
        tt = extract_sigma * mean(noiseSTD,2);
        %remove groups that have small SNR.
        for ii=groups
            ch = neurongroup_info(ii).channels;
            threshold(ii,:) = tt(ch);  %change form to [group x channel]
            if sum(SNR(ch) < SNRExtractionCutoff)
                groups = setxor(groups,ii);
            end
        end
        tmean = zeros(size(threshold));    %assume mean was zero after highpass filtering.
    else %do it the original way
        [tmean,stdev]  = calcSTD(sumX, sumX2, nn);
    end
    
    %append extracted waveforms after each trial
    if extractionflag==1
        for i=groups
            groupname = num2strpad(neurongroup_info(i).group,4);
            fidt = ['fidtime' groupname];
            timefilename = [groupname 'time.tmp'];   %just a temp file containing timestamps
            eval([fidt '=fopen(''' dirprefix timefilename ''',''w'',''ieee-le'');'])
            extractfilename = [sessionname 'g' groupname 'waveforms.bin'];   %a waveform temp file that is later added to end of timestamp file
            fidw = ['fidwave' groupname];
            eval([fidw '=fopen(''' dirprefix extractfilename ''',''w'',''ieee-le'');'])
            eval(['fwrite(' fidw ',zeros(1,100),''int8'');'])    %100 bytes reserved for header
        end
    end
    
    
    
    for n=1:totalChunks  %loop over chunks
        
        %unit is obtained from the data file.  If a highpass file
        %exists then we want to get it from there.  The highpass file
        %can exist in two places depending on whether it was just
        %recomputed or already exists in the highpass directory.
        %If no highpass then we need to get the data from the trial
        %file which is already highpassed.  Just need to sort out the
        %correct channels.
        
        %'*_highpass.*' files are always streamer files.
        %Old streamer files were written as 'electrode' or 'highpass' files
        %and were a highpass filtered before entering the DAQ.  Trial files
        %and .bin files exist in this old format.  More recent files
        %written with the StreamerDAQ or MAtlabDAQ (UEI files) were
        %broadband files.  All broadband files are rewritten as highpass
        %files in the streamer file format.  So therefore all highpass
        %files can be read as streamer files.
        
        %and it can be assumed that no reordering needs to be done, b/c all
        %'_highpass.*' files have already been reordered and all electrode
        %files are already in the correct order.
        
        %If processing only select groups then groups is set and we
        %should use this but unit is still all groups.  The logic for
        %seperate groups is within extractorwrapper.
        
        
        highpass_files = nptDir(['*_highpass.*']);
        %translate chunk n into a trial
        if trialbased
            b = n-[0;cumsum(numChunks)];
            b(b<=0)=[];
            trial = length(b);
            chunk = b(end);
            if isempty(highpass_files)
                %read old streamer trial file (rare case- older monkey expirements (Tang))
                filename = [sessionname '.' num2str(trial,'%04i')];
            else
                %read streamer trial file
                filename = [sessionname '_highpass.' num2str(trial,'%04i')];
            end
        elseif trialbased==0
            %read streamer bin file
            chunk=n;
            if isempty(highpass_files)
                %read old streamer bin file (rare case-older cat expirements)
                filename = [sessionname '.bin' ];
            else
                %read streamer trial file
                filename = [sessionname '_highpass.bin' ];
            end
        end
        %         fprintf('Reading %s Chunk%i of %i Total-%i.\n',filename,chunk,numChunks(trial),totalChunks);
        
        %get data
        if strcmp(dtype,'mat')
            d= load(filename,'-mat');
            numChannels = size(d.broadband);
            samplingRate = d.sampling_rate;
            scanOrder = [1 : numChannels];
            unit = d.broadband;
        else
        [numChannels,samplingRate,scanOrder] = nptReadStreamerFileHeader(filename);
        points = [chunkSize*samplingRate*(chunk-1)+1   chunkSize*samplingRate*chunk];
        unit = nptReadStreamerFileChunk(filename,points);
        
        end
        if isempty(highpass_files)
            unitChannel = [];
            for j=1:num_channels
                des = sprintf('%s',descriptor_info.description{j});
                switch des
                    case {'electrode','highpass','tetrode'}		%electrode and tetrode are outdated descriptions but still used for backcompatibility.
                        if strcmp(descriptor_info.state{j},'Active')
                            unitChannel=[unitChannel ; j];
                        end
                end % switch des
            end % for j=1:num_channels
            unit=unit(unitChannel,:);
        end
        
        if SNR_flag
            [unit_extracted,duration] = ExtractorWrapper2(unit,neurongroup_info,sampling_rate,tmean,threshold,groups);
        else
            [unit_extracted,duration,threshold] = ExtractorWrapper(unit,descriptor_info,neurongroup_info,sampling_rate,tmean,stdev,groups,extract_sigma,varargin{:});
        end
        
        %append unit_extracted to dat file immediately
        spikes_per_trial=[];
        for group = groups
            groupname = num2strpad(neurongroup_info(group).group,4);
            fid = ['fidtime' groupname];
            eval(['fwrite(' fid ',unit_extracted(group).times,''uint64'');'])
            fid=['fidwave' groupname];
             eval(['fwrite(' fid ',transpose(unit_extracted(group).waveforms),''int16'');'])% maybe200 fold for gain ampl
            % rod changes
            % eval(['fwrite(' fid ',transpose(unit_extracted(group).waveforms),''int64'');'])
            spikes_per_trial(group) = size(unit_extracted(group).times,1);
        end
        num_spikes = [num_spikes spikes_per_trial'];
        durations = [durations duration];
        if size(tmean,2)==1 %electrode
            tmeans = [tmean zeros(size(tmean,1),3)];
            thresholds = [threshold zeros(size(threshold,1),3)];
        elseif size(tmean,2)==2 %stereotrode
            tmeans = [tmean zeros(size(neurongroup_info,2),2)];
            thresholds = [threshold zeros(size(neurongroup_info,2),2)];
        elseif size(tmean,2)==4 %tetrode
            tmeans = tmean;
            thresholds = threshold;
        elseif size(tmean,2)>4 %polytrode
            tmeans = tmean;
            thresholds = threshold;
        end
        for gr=groups
            extract_info.group(gr).trial(n).means = tmeans(gr,:);
            extract_info.group(gr).trial(n).thresholds = thresholds(gr,:);
        end
        fprintf('\n');
    end%for i = 1:trials
end%if extractionflag


% if we were extracting from high-pass filtered data, we have to go back
% up to the root session directory
if extracthighpass==1
    cd ..
end

% save timing data
if timingflag==1
    % get presTrigOnsets and syncOnsets in ms. Do this here
    % since we know the sampling rate.
    presTrigOnsetsMS = (presTrigOnsets-1)/srms;
    syncOnsetsMS = (syncOnsets-1)/srms;
    trigDurations = [trigDurations (trigDurations-1)/srms];
    save([sessionname 'timing'],'trigDurations','presTrigOnsets','syncOnsets','smSyncs','presTrigOnsetsMS','syncOnsetsMS');
end

%write dat files
if exist('unit_extracted')
    [~, rawfilename, ~] = nptFileParts(filename);
    % round duration up to the next largest integer to make sure
    % that we have a round number of an integer number of data points
    % when we convert durations back to data points
    max_duration = ceil(max(durations));
    % get min duration as well
    min_duration = min(durations);
    % get the exact duration that will be written to the header file and
    % use that precision for all computations
    % durationstr = num2str(duration,'%6f');
    % duration = str2num(durationstr);
    
    for i=groups	%loop over groups
        groupname = num2strpad(neurongroup_info(i).group,4);
        fidt = ['fidtime' groupname];
        timefilename = [groupname 'time.tmp'];   %just a temp file containing timestamps
        fidw = ['fidwave' groupname];
        extractfilename = [sessionname 'g' groupname '_waveforms.bin'];   %file inwhich timestamps are appended.
        
        eval(['s=fclose(' fidt ');'])
        % don't need dirprefix since we would already have done cd .. if extracthighpass==1
        eval([fidt '=fopen(''' timefilename ''',''r'',''ieee-le'');'])
        eval(['time = fread(' fidt ',inf,''uint64'');'])
        eval(['s=fclose(' fidt ');'])
        delete(timefilename);   %just temp file
        time = StretchTimes(time,num_spikes(i,:),max_duration);        %add max duration to each trial
        
        %waveform file
        num_cha=length(neurongroup_info(i).channels);
        sum_spikes=sum(num_spikes(i,:));
        eval(['WriteWaveformsHeader(' fidw ',sum_spikes,num_cha);']) %add header info
        eval(['fseek(' fidw ',100+2*32*num_cha*sum_spikes,''bof'');'])
        eval(['fwrite(' fidw ',time,''uint64'');'])
        eval(['fclose(' fidw ');'])
        
        
        %All of the extracted waveforms for a single group for the entire
        %session are listed together in a single waveforms.bin file.  The
        %times for each waveform are added by chunkSize*chunk.  This is so
        %the spikes will be in sequential order during the sorting
        %(relevant when the data is trial based).  So the waveforms header
        %file is used to generate ispikes objects after
        %sorting with the correct time in a correct trial so that the
        %ispikes can be accurately plotted againast the highpass files
        %and stimulus.
        %The information in the waveforms header is used to plot
        %the extraction thresholds on the highpass data also.
        
        %we are passing in total chunks but this not very useful to
        %destrecthing the spiketimes if there are trial based data files.
        %Instead we should pass in numChunks
        %but this would lead to a new version of the waveforms header file.
        
        %another wierd thing is that we are writing the threshold and mean
        %of each chunk but we have already averaged the threshold and mean
        %over the entire session so they are the same numbers for each chunk.
        %Must be some back compatibility issue.  BG-3/2011
        
        totalwaveforms=sum(num_spikes(i,:));
        if single_flag & strcmp(dtype,'Streamer')  %not tested 3/2011-BG-probably can remove this case and combine it will next if statement
            trials=1;
            sum_max_duration = sum(durations);
            sum_min_duration=0;
            % write raw channel numbers if signals are not broadband
            nptWriteSorterHdr( groupname , sampling_rate , ...
                sum_max_duration , sum_min_duration, trials , totalwaveforms , rawfilename, ...
                neurongroup_info(i).channels, extract_info.group(i),numChunks,chunkSize );
            
            % multiple trial header used with sorter1
        elseif (~isempty(broadband) | extracthighpass==1)  %tested 3/2011-BG
            ng=neurongroup_info(i).channels;
            t=extract_info.group(i).trial(1).thresholds;  %same average for entire session
            m=extract_info.group(i).trial(1).means; %same average for entire session
            
            dd=durations;
            d=zeros(size(numChunks));
            for tt=1:length(numChunks)  %find durations of trials in seconds
                d(tt)=sum(dd(1:numChunks(tt)));
                dd(1:numChunks(tt))=[];
            end
            
            save(['WaveformsHeader_g' groupname],'sampling_rate', 'totalwaveforms', 'rawfilename', ...
                'ng', 't','m','d','numChunks','chunkSize' );
        else    %not tested 3/2011-BG
            % write raw channel numbers if signals are not broadband
            nptWriteSorterHdr( groupname , sampling_rate , ...
                max_duration , min_duration,  trial , totalwaveforms , rawfilename, ...
                neurongroup_info(i).raw_channels, extract_info.group(i) );
        end
        
    end %loop over groups
    
    %             if SNR_flag
    %                 %do not sort groups that have small SNR.
    %                 for ii=groups
    %                     ch = neurongroup_info(ii).channels;
    %                     if sum(SNR(ch) < SNRSortCutoff)
    %                         groups = setxor(groups,ii);
    %                         %write as ispikes
    %                         ispikes('Group',num2strpad(ii,3),'UseSort',0)
    %                         %and then remove them from RunClustBatch somehow.
    %                     end
    %                 end
    %             end
end%if exist unitextracted

if ~strcmp(sort_algo,'none')
    
    sort=0;
    %where are the waveforms.bin files?
    wavelist = nptDir('*waveforms.bin');
    if ~isempty(wavelist)
        sort=1;
    elseif   isempty(wavelist) & isdir('sort')
        cd('sort')
        wavelist = nptDir('*waveforms.bin');
        if ~isempty(wavelist)
            sort=2;
        else
            cd ..
        end
    end
    
    if sort
        [hdrsz , num_spikes, num_cha, gain, ptswv] = ReadWaveformsHeader(wavelist(1).name);
        %look for KK batch file in current directory first
        batchlist = nptDir('Batch_*.txt');
        if ~isempty(batchlist)
            p=pwd;
        else
            p=which('RunClustBatch');
            [p,n,e]=fileparts(p);
        end
        
        % RunClustBatch turns on diary so if diary is already on, we
        % should save the diaryname so we can turn it off now and turn
        % it back on later. This will only work if diary was turned on
        % using: diary([pwd filesep 'diary']).
        diaryname = '';
        if strcmp(get(0,'Diary'),'on')
            diaryname = get(0,'DiaryFile');
            diary off
        end
        
        if strcmp(sort_algo,'BB')
            if num_cha==1
                RunClustBatch([p filesep 'Batch_BBClustEE.txt'],clustoptions{:})
            elseif num_cha==4
                RunClustBatch([p filesep 'Batch_BBClust.txt'],clustoptions{:})
            end
        end
        if strcmp(sort_algo,'KK')
            if num_cha==1
                RunClustBatch([p filesep 'Batch_KKwikEE.txt'],clustoptions{:})
            elseif num_cha==4
                RunClustBatch([p filesep 'Batch_KKwik.txt'],clustoptions{:})
            end
        end
        
        % turn diary back on if it was on before
        % YSC 11/17/03
        if ~isempty(diaryname)
            diary(diaryname)
        end
    end
    if sort==2
        cd ..
    end
end
if ~isempty(eyedata) & isCalib
    %collate all meanVH points by their targets,
    %calculate the mean (x,y) around each target
    NumberOfPoints = init_info.GridCols * init_info.GridRows;
    dxy.avgVH = zeros(2,NumberOfPoints);
    for i=1:NumberOfPoints
        % find(init_info.StimulusSequence==(i-1));
        ind=find(init_info.StimulusSequence==(i-1));
        dxy.avgVH(:,i) = mean(dxy.meanVH(1:2,ind),2);
    end
    %write dxy file
    fid=fopen([sessionname '_dxy.bin'],'w','ieee-le');
    fwrite(fid, init_info.ScreenWidth, 'int32');
    fwrite(fid, init_info.ScreenHeight, 'int32');
    fwrite(fid, init_info.GridRows, 'int32');
    fwrite(fid, init_info.GridCols, 'int32');
    fwrite(fid, init_info.Xsize, 'int32');
    fwrite(fid, init_info.Ysize, 'int32');
    fwrite(fid, init_info.CenterX, 'int32');
    fwrite(fid, init_info.CenterY, 'int32');
    fwrite(fid, init_info.NumBlocks, 'int32');
    fwrite(fid, dxy.meanVH, 'double');
    fwrite(fid, dxy.avgVH, 'double');
    fclose(fid);
end

%run SNR calcuation on highpass data
if SNR_highpass
    SNR_Session_Continuous('chunks_snr',SNR_chunks,'redo_snr',SNR_redo,'std_snr',SNR_std)
end

fprintf('\t\tDone!\n');
%create marker file to show this session has been processed
fid=fopen('processedsession.txt','wt');
fclose(fid);

