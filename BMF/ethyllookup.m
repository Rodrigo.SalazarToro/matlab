function  locations = ethyllookup(session, channels)

%given a session (numerical) and channel (numerical), returns a string with recording location
%given a session (numerical) and a row vector of channels, returns a row
%vector of recording locations corresponding to those channels.
%run in ethyl/histology

table = load('LookUpTable.mat');
first = [table.lut{:,1}];
row = find(first == session); %returns the row of the session 
chancols = channels + 1; %indexes into the channel column
locations = table.lut(row,chancols);