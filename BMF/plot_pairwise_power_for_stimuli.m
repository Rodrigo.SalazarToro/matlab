function plot_pairwise_power_for_stimuli(varargin)


%run at session level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl/110704/session01')


sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');

%get trial for each stimulus
for xt = 1:5
    identity{xt} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','iCueObj',xt);
end

g = bmf_groups;
N = NeuronalHist('bmf');

[~,good_ch] = intersect(N.gridPos,g);

ch = [1 : size(N.gridPos,2)];

chnumb = size(ch,2);

cd([sesdir filesep 'lfp' filesep 'power'])

powertrials = nptDir('*power.*');




allperformance = [];
stimcomb = 0;
for stim1 = 2%1 : 4
    for stim2 = 4%stim1 + 1 : 5
        stimcomb = stimcomb + 1;
        
        alltrials = sort([identity{stim1} identity{stim2}]);
        [~,st1] = intersect(alltrials,identity{stim1});
        [~,st2] = intersect(alltrials,identity{stim2});
        ntrials = size(alltrials,2);
        
        chlist = [];
        clcounter =0;
        for c1 = 8%1:26;
            for c2 = c1+1:26
                clcounter = clcounter +1;
                chlist(clcounter,:) = [c1 c2];
            end
        end
        
        
        for epoch = 2%1:4
            trials = alltrials;
            
            trial_power1 = [];
            trial_power2 = [];
            trial_power3 = [];
            for chl = 1: size(chlist,1)
                chcounter = 0;
                for xxx = 1:2
                    chlist(chl,xxx)
                    chcounter = chcounter +1;
                    %make data matrix
                    tcounter = 0;
                    for nt = trials
                        tcounter = tcounter + 1;
                        load(powertrials(nt).name)
                        normpower = power.S{epoch}(chlist(chl,xxx),1:19) .* power.f{epoch}(1:19)';
                        totalpower = sum(normpower);
                        npower = normpower ./ totalpower;
                        
                        %                         npower = log(power.S{epoch}(chlist(chl,xxx),:));
                        trial_power1(tcounter,chcounter) = sum(npower(2:7));
                        trial_power2(tcounter,chcounter) = sum(npower(8:13));
                        trial_power3(tcounter,chcounter) = sum(npower(14:19));
                    end
                end
                
                subplot(1,3,1)
                scatter(trial_power1(st1,1),trial_power1(st1,2),'fill','r','s');
                hold on
                scatter(trial_power1(st2,1),trial_power1(st2,2),'fill','b','s')
                
                subplot(1,3,2)
                scatter(trial_power2(st1,1),trial_power2(st1,2),'fill','r','s');
                hold on
                scatter(trial_power2(st2,1),trial_power2(st2,2),'fill','b','s')
                
                subplot(1,3,3)
                scatter(trial_power3(st1,1),trial_power3(st1,2),'fill','r','s');
                hold on
                scatter(trial_power3(st2,1),trial_power3(st2,2),'fill','b','s')
                pause(1)
                clf
            end
            
        end
    end
end






