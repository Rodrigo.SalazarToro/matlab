function run_bmf_mi(varargin)

Args = struct('days',[],'sessions',[],'mi',0,'redo_mi',0,'surrogates',0,'redo_surrogates',0);
Args.flags = {'mi','redo_mi','surrogates','redo_surrogates'};
Args = getOptArgs(varargin,Args);

cd('/media/bmf_raid/data/monkey/ethyl')

monkeydir = pwd;
num_days = size(Args.days,2);

for d = 1 : num_days
    cd ([monkeydir filesep Args.days{d}])
    daydir = pwd;
    sessions = nptDir('*session0*');
    
    for s = Args.sessions;
        if s <= str2double(sessions(size(sessions,1)).name(end))
            cd ([daydir filesep sessions(s).name]);
            sesdir = pwd;
            %this gets the groups with SNR > 1.8, POWER < .5 and not Noisy (from rejectCH)
            [~,~,gpairs] = bmf_groups;
            npairs = size(gpairs,1)
            cd([sesdir filesep 'lfp' filesep 'cohgrams']);
            cohdir = pwd;
            if Args.mi
                for np = 1 : npairs
                    cd(cohdir)
                    coh_pairs = [ 'cohgram' num2strpad(gpairs(np,1),3) num2strpad(gpairs(np,2),3)];
                    load(coh_pairs,'cohgram','idecohgram');
                    
                    probstim = [{ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2}]; %1/5
                    
                    %uses theoretical error bars
                    for cs = 1:5
                        cstd{cs} = idecohgram.confC{cs};
                    end
                    [muInfo,varargout] = MIcont(idecohgram.C,cstd,probstim);
                    
                    
                    migram.migram = muInfo;
                    migram.groups = gpairs(np,:);
                    migram.trials = cellfun(@length,idecohgram.trials);
                    migram.t = cohgram.t{1}; %time
                    migram.f = cohgram.f{1}; %frequency
                    
                    mi_pairs = [ 'migram' num2strpad(gpairs(np,1),3) num2strpad(gpairs(np,2),3)];
                    if ~exist('migrams','dir')
                        mkdir('migrams')
                    end
                    
                    cd([cohdir filesep 'migrams']);
                    
                    write_info = writeinfo(dbstack);
                    save(mi_pairs,'migram','write_info');
                    
                end
            end
            
            if Args.surrogates
                for np = 1 : npairs
                    cd(cohdir)
                    coh_pairs = [ 'surcohgram' num2strpad(gpairs(np,1),3) num2strpad(gpairs(np,2),3)];
                    load(coh_pairs,'surcohgram');
                    
                    nperms = size(surcohgram.C,1);
                    
                    probstim = [{ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2} {ones(62,33)*.2}]; %1/5
                    
                    for perms = 1 : nperms
                        %uses theoretical standard deviation
                        ther_std = surcohgram.confC(perms,:);
                        [muInfo(:,:,perms),varargout] = MIcont(surcohgram.C(perms,:),ther_std,probstim);
                    end
                    surmigram.migram = muInfo;
                    surmigram.groups = gpairs(np,:);
                    surmigram.trials = cellfun(@length,surcohgram.trials(np,:));
                    surmigram.t = cohgram.t{np,1}; %time, same for all stims
                    surmigram.f = cohgram.f{np,1}; %frequency, same for all stims
                    
                    mi_pairs = [ 'surmigram' num2strpad(gpairs(np,1),3) num2strpad(gpairs(np,2),3)];
                    cd([cohdir filesep 'migrams']);
                    
                    write_info = writeinfo(dbstack);
                    save(mi_pairs,'surmigram','write_info');
                end
            end
        end
    end
end
%
cd(monkeydir)




















