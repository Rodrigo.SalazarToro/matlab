function mvpa_power_analysis_bmf_epoch(varargin)


%run at session level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl/110810/session01')


sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
alltrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
ntrials = size(alltrials,2);
%get trial for each stimulus

g = bmf_groups;
N = NeuronalHist('bmf');

[~,good_ch] = intersect(N.gridPos,g);
cd([sesdir filesep 'lfp' filesep 'power'])
powertrials = nptDir('*power.*');


ecounter = 0;
for epoch = [1,4]
    ecounter = ecounter + 1;
    chcounter = 0;
    for cch = good_ch
        chcounter = chcounter +1;
        %make data matrix
        tcounter = 0;
        for nta = alltrials
            tcounter = tcounter + 1;
            load(powertrials(nta).name)
            
%             trial_power1_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,2:7);
%             trial_power2_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,8:13);
%             trial_power3_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,14:19);
            
            normpower = power.S{ecounter}(cch,1:19) .* power.f{ecounter}(1:19)';
            totalpower = sum(normpower);
            npower = normpower ./ totalpower;
            trial_power1_norm{ecounter}(tcounter,chcounter) = sum(npower(2:7));
            trial_power2_norm{ecounter}(tcounter,chcounter) = sum(npower(8:13));
            trial_power3_norm{ecounter}(tcounter,chcounter) = sum(npower(14:19));




        end
    end
end


for nt = 1 : ntrials
    
    trials = alltrials;
    test_trial = trials(nt);
    trials(nt) = [];
    

    tp1e1 = trial_power1_norm{1}; 
    tp2e1 = trial_power2_norm{1};
    tp3e1 = trial_power3_norm{1};
    
    tp1e4 = trial_power1_norm{2};
    tp2e4 = trial_power2_norm{2};
    tp3e4 = trial_power3_norm{2};
    
    
    %get test data (epoch 1)
    tp_test1 = tp1e1(nt,:);
    tp_test2 = tp2e1(nt,:);
    tp_test3 = tp3e1(nt,:);
    
    
    %GET RID OF THE CURRENT TRIAL
    tp1e1(nt,:) = [];
    tp2e1(nt,:) = [];
    tp3e1(nt,:) = [];
    
    tp1e4(nt,:) = [];
    tp2e4(nt,:) = [];
    tp3e4(nt,:) = [];
    
    tgroups = [ones(1,ntrials-1) zeros(1,ntrials-1)];
    tp1 = [tp1e1;tp1e4];
    tp2 = [tp2e1;tp2e4];
    tp3 = [tp3e1;tp3e4];
    
   
    tgroups(randperm(size(tgroups,2))) = tgroups;

    try
        SVMstruct1 = svmtrain(tp1(:,:),tgroups,'Kernel_Function','rbf');
        SVMstruct2 = svmtrain(tp2(:,:),tgroups,'Kernel_Function','rbf');
        SVMstruct3 = svmtrain(tp3(:,:),tgroups,'Kernel_Function','rbf');
        
        %1:12
        %13:26
        %37:39
        newClasses1 = svmclassify(SVMstruct1,tp_test1(:,:))
        newClasses2 = svmclassify(SVMstruct2,tp_test2(:,:))
        newClasses3 = svmclassify(SVMstruct3,tp_test3(:,:))
    catch
        newClasses1 = 2
        newClasses2 = 2
        newClasses3 = 2
    end
    
    if newClasses1 == 1
        allperformance1(nt) = 1;
    elseif newClasses1 == 2
        allperformance1(nt) = 2;
    else
        allperformance1(nt) = 3;
    end
    
    if newClasses2 == 1
        allperformance2(nt) = 1;
    elseif newClasses2 == 2
        allperformance2(nt) = 2;
    else
        allperformance2(nt) = 3;
    end
    
    if newClasses3 == 1
        allperformance3(nt) = 1;
    elseif newClasses3 == 2
        allperformance3(nt) = 2;
    else
        allperformance3(nt) = 3;
    end
end

allperformance1;


        f1_1 = size(find(allperformance1 == 1),2);
        f1_2 = size(find(allperformance1 == 2),2);
        f1_3 = size(find(allperformance1 == 3),2);
        
        performance1 = round((f1_1 / (f1_1 + f1_3)) * 100)
        
        f2_1 = size(find(allperformance2 == 1),2);
        f2_2 = size(find(allperformance2 == 2),2);
        f2_3 = size(find(allperformance2 == 3),2);
        
        performance2 = round((f2_1 / (f2_1 + f2_3)) * 100)
        
        f3_1 = size(find(allperformance3 == 1),2);
        f3_2 = size(find(allperformance3 == 2),2);
        f3_3 = size(find(allperformance3 == 3),2);
        
        performance3 = round((f3_1 / (f1_1 + f1_3)) * 100)
        
        
        subplot(1,3,1)
        scatter3(tp1(1:529,37),tp1(1:529,38),tp1(1:529,39),'filled','s','k')
        hold on
        scatter3(tp1(530:1058,37),tp1(530:1058,38),tp1(530:1058,39),'filled','s','r')
        
        subplot(1,3,2)
        scatter3(tp2(1:529,37),tp2(1:529,38),tp2(1:529,39),'filled','s','k')
        hold on
        scatter3(tp2(530:1058,37),tp2(530:1058,38),tp2(530:1058,39),'filled','s','r')
        
        subplot(1,3,3)
        scatter3(tp3(1:529,37),tp3(1:529,38),tp3(1:529,39),'filled','s','k')
        hold on
        scatter3(tp3(530:1058,37),tp3(530:1058,38),tp3(530:1058,39),'filled','s','r')
        


