function power_analysis_bmf_figure(varargin)




%run at monkey level
Args = struct('raw',0);
Args.flags = {'raw'};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl')

if Args.raw
    load allchgrid_low_raw allchgrid_low
    load allchgrid_middle_raw allchgrid_middle
    load allchgrid_high_raw allchgrid_high
    load allchgrid_superhigh_raw allchgrid_superhigh   %RERUN TO GET THIS BAND
else
    load allchgrid_low allchgrid_low
    load allchgrid_middle allchgrid_middle
    load allchgrid_high allchgrid_high
    load allchgrid_superhigh allchgrid_superhigh   %RERUN TO GET THIS BAND
end


maxlow = max(cell2mat(cellfun(@max,cellfun(@max,allchgrid_low,'UniformOutput',0),'UniformOutput',0)));
maxmiddle = max(cell2mat(cellfun(@max,cellfun(@max,allchgrid_middle,'UniformOutput',0),'UniformOutput',0)));
maxhigh = max(cell2mat(cellfun(@max,cellfun(@max,allchgrid_high,'UniformOutput',0),'UniformOutput',0)));

maxsuperhigh = max(cell2mat(cellfun(@max,cellfun(@max,allchgrid_superhigh,'UniformOutput',0),'UniformOutput',0)));


minlow = min(cell2mat(cellfun(@min,cellfun(@min,allchgrid_low,'UniformOutput',0),'UniformOutput',0)));
minmiddle = min(cell2mat(cellfun(@min,cellfun(@min,allchgrid_middle,'UniformOutput',0),'UniformOutput',0)));
minhigh = min(cell2mat(cellfun(@min,cellfun(@min,allchgrid_high,'UniformOutput',0),'UniformOutput',0)));

minsuperhigh = min(cell2mat(cellfun(@min,cellfun(@min,allchgrid_superhigh,'UniformOutput',0),'UniformOutput',0)));



thpercent = .5;

figure
i = imread('ethyl_brainsketch.jpg');

%%
subplot(4,4,1)
imshow(i);
hold on

m = allchgrid_low{1};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minlow) ./ (maxlow-minlow);
m(find(m < thpercent)) = nan;
blue = cat(3, ones(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(1,1) = nansum(nansum(m));
%%
subplot(4,4,2)
imshow(i);
hold on

m = allchgrid_low{2};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minlow) ./ (maxlow-minlow);
m(find(m < thpercent)) = nan;
blue = cat(3, ones(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(1,2) = nansum(nansum(m));
%%
subplot(4,4,3)
imshow(i);
hold on

m = allchgrid_low{3};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minlow) ./ (maxlow-minlow);
m(find(m < thpercent)) = nan;
blue = cat(3, ones(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(1,3) = nansum(nansum(m));
%%
subplot(4,4,4)
imshow(i);
hold on

m = allchgrid_low{4};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minlow) ./ (maxlow-minlow);
m(find(m < thpercent)) = nan;
blue = cat(3, ones(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(1,4) = nansum(nansum(m));


%%
subplot(4,4,5)
imshow(i);
hold on

m = allchgrid_middle{1};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minmiddle) ./ (maxmiddle-minmiddle);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), ones(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(2,1) = nansum(nansum(m));
%%
subplot(4,4,6)
imshow(i);
hold on

m = allchgrid_middle{2};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minmiddle) ./ (maxmiddle-minmiddle);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), ones(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(2,2) = nansum(nansum(m));
%%
subplot(4,4,7)
imshow(i);
hold on

m = allchgrid_middle{3};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minmiddle) ./ (maxmiddle-minmiddle);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), ones(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(2,3) = nansum(nansum(m));
%%
subplot(4,4,8)
imshow(i);
hold on

m = allchgrid_middle{4};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minmiddle) ./ (maxmiddle-minmiddle);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), ones(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(2,4) = nansum(nansum(m));








%high
%%
subplot(4,4,9)
imshow(i);
hold on

m = allchgrid_high{1};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minhigh) ./ (maxhigh-minhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),ones(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(3,1) = nansum(nansum(m));
%%
subplot(4,4,10)
imshow(i);
hold on

m = allchgrid_high{2};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minhigh) ./ (maxhigh-minhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),ones(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(3,2) = nansum(nansum(m));
%%
subplot(4,4,11)
imshow(i);
hold on

m = allchgrid_high{3};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minhigh) ./ (maxhigh-minhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),ones(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(3,3) = nansum(nansum(m));
%%
subplot(4,4,12)
imshow(i);
hold on

m = allchgrid_high{4};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minhigh) ./ (maxhigh-minhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),ones(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(3,4) = nansum(nansum(m));



%superhigh
%%
subplot(4,4,13)
imshow(i);
hold on

m = allchgrid_superhigh{1};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minsuperhigh) ./ (maxsuperhigh-minsuperhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(4,1) = nansum(nansum(m));

%%
subplot(4,4,14)
imshow(i);
hold on

m = allchgrid_superhigh{2};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minsuperhigh) ./ (maxsuperhigh-minsuperhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(4,2) = nansum(nansum(m));
%%
subplot(4,4,15)
imshow(i);
hold on

m = allchgrid_superhigh{3};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minsuperhigh) ./ (maxsuperhigh-minsuperhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(4,3) = nansum(nansum(m));

%%
subplot(4,4,16)
imshow(i);
hold on

m = allchgrid_superhigh{4};
m1 = size(m,1);
m2 = size(m,2);

m = (m-minsuperhigh) ./ (maxsuperhigh-minsuperhigh);
m(find(m < thpercent)) = nan;
blue = cat(3, zeros(m1,m2),zeros(m1,m2), zeros(m1,m2));
hold on
h = imshow(blue);
set(h,'AlphaData',m)
mm(4,4) = nansum(nansum(m));


mm;








