function test_reward_pulse_width

%uses nlynx .csc file with reward pulse recorded
%attach probe (NO GROUND) to solenoid ground
%this should measure the pulse
%run into NLYX and record

cd('~/Desktop/nlynx_test_data/')



fid = fopen('CSC257_copy_ml2.ncs');

first_record = 16384; %16kb, or 16 * 1024
recordsize = 1044;

fseek(fid,first_record,'bof'); %skip 16kb header
firstts = fread(fid,1,'uint64');


counter = 0;
currentts = 0;
csc.Samples = [];
for tts = 1:1000
    counter = counter + 1;
    currenttstamp = fread(fid,1,'uint64');
    if ~isempty(currenttstamp) 
        currentts = currenttstamp;
    else
        currentts = 0;
    end
    csc.TimeStamps(counter) = currentts;
%     csc.ChannelNumbers(counter) = fread(fid,1,'uint32');
%     csc.SampleFrequencies(counter) = fread(fid,1,'uint32'); %samples per second (512 samples per record)
%     csc.NumberValidSamples(counter) = fread(fid,1,'uint32');
    %skip to record
    %     fseek(fid,12,'cof');
    
    csc.Samples = [csc.Samples fread(fid,512,'int16')'];
end

  d = reshape(csc.Samples,1,numel(csc.Samples));

fclose(fid)

csc;
