function mvpa_power_analysis_bmf_figure(varargin)

%run at monkey level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl')
mdir = pwd;
load alldays

recording_category = {'visual','Temporal','PPC','PFC','Motor','Somato','All'};

for x = 18%:25;
    
    cd([mdir filesep alldays{x} filesep 'session01'])
    
    
    
    load performance1_stims
    load performance2_stims
    load performance3_stims
    
    hold on
    for e = 1:4
        subplot(1,4,e)
        for a = 1:7
            scatter(a-.2,performance1_stims(a,e),'r','fill','s')
            hold on
        end
    end
    
    hold on
    for e = 1:4
        subplot(1,4,e)
        for a = 1:7
            scatter(a,performance2_stims(a,e),'b','fill','s')
            hold on
        end
    end
    
    hold on
    for e = 1:4
        subplot(1,4,e)
        for a = 1:7
            scatter(a+.2,performance3_stims(a,e),'g','fill','s')
            hold on
        end
        axis([0 8 0 100])
        hold on
        plot([0 8],[50 50],'k','LineWidth',2)
        hold on
        plot([0 8],[40 40],'--k','LineWidth',1)
        hold on
        plot([0 8],[60 60],'--k','LineWidth',1)
        hold on
        
        set(gca,'XTick',[1:7])
        set(gca,'XTickLabel',recording_category)
        
 
        
    end

end



