function example_power_analysis_bmf_sfn14(varargin)

%run at monkey level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);



cd('/Volumes/bmf_raid/data/monkey/ethyl/110714/session01')
sesdir = pwd;
g = bmf_groups;
N = NeuronalHist('bmf');
[realch,good_ch] = intersect(N.gridPos,g);
ch = [1 : size(N.gridPos,2)];
chnumb = size(ch,2);

cd([sesdir filesep 'lfp' filesep 'power'])
load trial_power trial_power
m = cellfun(@mean,trial_power,'UniformOutput',0);

fspacing = linspace(0,90.6250,59);

spcounter = 0;
for ch = [8,22,27,33,48,53]%1:chnumb

    for e = 1:4
        %normalize power
        
        spcounter = spcounter + 1;
        reverse_categorizeNeuronalHistBMF(N.number(ch))
        ch
        
        subplot(6,4,spcounter)
        hold on
        plot(linspace(0,90.6250,59),(m{ch,e}(1:59) .* linspace(0,90.6250,59)) ./ sum(m{ch,e}(1:59).*linspace(0,90.6250,59)),'k')
        hold on
        area(linspace(0,90.6250,59),(m{ch,e}(1:59) .* linspace(0,90.6250,59)) ./ sum(m{ch,e}(1:59).*linspace(0,90.6250,59)))
        axis([0 90 0 .15])
        hold on
        
        plot([fspacing(8) fspacing(8)],[0 .2],'r')
        hold on
        plot([fspacing(15) fspacing(15)],[0 .2],'b')
        hold on
        plot([fspacing(22) fspacing(22)],[0 .2],'g')
        hold on
        if e == 1
        text(70,.1,reverse_categorizeNeuronalHistBMF(N.number(ch)))
        end
        set(gca, 'TickDir', 'out')
    end
    
end

