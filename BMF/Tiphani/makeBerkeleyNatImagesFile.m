function makeBerkeleyNatImagesFile(taskName,imagedirlist)

% generates conditions file for gabors as passive distractors
% each rotation can be represented in one of 8 locations on the screen

% taskName: string identifying the name of conditions file ie: 'gabors_distractors'
% timingFile: string identifying the name of the timing file ie: 'distractor'

rfreq = 1;
cond = 1;

timingFile = 'pix_nat_images';
filename = sprintf('%s.txt',taskName);
fid = fopen(filename,'wt');
header = generate_condition('header',fid);

%fixation point
taskObject(1).Type = 'fix';
taskObject(1).Arg{1} = 0;
taskObject(1).Arg{2} = 0;

numimages = length(imagedirlist);

%% Block 1

block = 1;

%images
for i = 1:50
        taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;


%% Block 2

block = 2;
rfreq = 1;

%images
for i = 51:100
     taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;    
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;

%% Block 3

block = 3;
rfreq = 1;

%images
for i = 101:150
     taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;    
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;

%% Block 4

block = 4;
rfreq = 1;

%images
for i = 151:200
     taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;    
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;

%% Block 5

block = 5;
rfreq = 1;

%images
for i = 201:250
        taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;    
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;

%% Block 6

block = 6;
rfreq = 1;

%images
for i = 251:300
        taskObject(2).Type = 'pic';
        filename = strrep(imagedirlist(i).name,'.jpg','');
        taskObject(2).Arg{1} = filename;
        taskObject(2).Arg{2} = 0;
        taskObject(2).Arg{3} = 0;
        textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
        cond = cond + 1;    
end

%fixation points
rfreq = 5;
taskObject(2).Type = 'fix';
taskObject(2).Arg{1} = 0;
taskObject(2).Arg{2} = 0;
textline = generate_condition(cond,rfreq,block,timingFile,taskObject,fid);
cond = cond + 1;
