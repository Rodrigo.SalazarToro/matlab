function get_xcorr_hist

%results for the cell conference

xx = load('/media/bmf_raid/data/monkey/ethyl/ethyl_gamma/ethylevents.mat');

sessnames = xx.sessnames;


%% loop through session directories to get cross correlations 

corrvec = -1:0.01:1;  
corrhist = zeros(length(corrvec),1); 
results = zeros(1,35);

for n = 1:length(sessnames)
    n
    session = sessnames{n}; %the name of this session, i.e. 'ethyl11071103'
    folder = session(6:11);
    sessnum = session(12:13);
    cd(['/Volumes/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum]);


    a = load(['epoch_xcorr_sfn2013_ethyl',folder,sessnum,'.mat']);
    cormat = a.correlationdatamatrix;

    numevents = length(cormat(:,1));
    maxcorrs = zeros(numevents,35); 
    
    foramps = load(['fixation_oscepoch_sfn2013_ethyl',folder,sessnum,'.mat']);
    avgrms = foramps.avgrms;
    peaktorms = foramps.peaktorms;
    
    %Now hilberts are the whole length of the file, so eventon+imageon
    %should be sufficient
    burst = load(['burstdata_sfn2013_ethyl',folder,sessnum,'.mat']);
    data = burst.edata;
    
    durations = data.duration;
    absoluteeventon = data.absoluteeventon;
    absoluteeventoff = data.absoluteeventoff;
    saclatency = data.saclatency;
    fixlatency = data.fixlatency;
    saccadecodes = data.saccadecodes;
    visarea = data.visarea;
    trialnumber = data.trialnumber;
   
    
    
    forhils = load(['fixation_hilberts_sfn2013_ethyl',folder,sessnum,'.mat']);
    hilberts = forhils.sessionhilberts;
    
    
    
    
    for g = 1:numevents
        maxcorrs(g,2) = n;              % session #
        maxcorrs(g,1) = cormat{g,1}(2); % ID # in whole data set
        maxcorrs(g,3) = cormat{g,1}(1); % number in session (for indexing data stored by session)
        
        eventon = cormat{g,1}(4);
        maxcorrs(g,4) = cormat{g,1}(4); % event on
        maxcorrs(g,5) = cormat{g,1}(5); % event off
        maxcorrs(g,6) = avgrms(g);      % amplitude
        maxcorrs(g,7) = peaktorms(g);   % peak to avg amplitude ratio  
        maxcorrs(g,12) = durations(g);
        maxcorrs(g,13) = saclatency(g);
        maxcorrs(g,14) = fixlatency(g);
        maxcorrs(g,15) = saccadecodes(g);
        maxcorrs(g,16) = visarea(g);
        maxcorrs(g,17) = trialnumber(g);
        
        
        thesehilberts = hilberts{g};
        for k = 1:length(thesehilberts(:,1)) %for each frequency band
            onehilbert = thesehilberts(:,k);
            oscillationcenter = absoluteeventoff-absoluteeventon;
            maxcorrs(g,17+k) = angle(onehilbert(oscillationcenter));%find each phase at the center of the oscillation            
        end
        
        
        thiseventsxcorr = cormat{g,2};
        maxcorr = -1;
        for i = 1:length(thiseventsxcorr(:,1))
            chanon = cormat{g,2}{i,1};
            chanto = cormat{g,2}{i,2};
            if(chanon ~= chanto)
                maxcorrs(g,8) = chanon; % the channel the event occurred on
                onecorrelation = thiseventsxcorr{i,3};
                for k = 1:length(onecorrelation)
                    if(onecorrelation(k) > maxcorr)
                        maxcorr = onecorrelation(k);
                        maxcorrs(g,11) = k;                % lag number of the maximum correlation
                        maxcorrs(g,10) = maxcorr;          % magnitude of the maximum correlation
                        maxcorrs(g,9)  = cormat{g,2}{i,2}; % the channel the max correlation was with
                    end
                    addtohist = onecorrelation(k);
                    amat = histc(addtohist,corrvec);
                    mat = amat(:);
                    corrhist = corrhist+mat;
                end
            end
        end    
    end
    results = vertcat(results,maxcorrs);
end

results(1,:) = [];

save('/media/bmf_raid/data/monkey/ethyl/ethyl_gamma/gamma_aug_2013.mat','results','corrhist','corrvec','-v7.3')
