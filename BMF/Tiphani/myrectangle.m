function myrectangle(a,b,w,h,theta,col)

X = [-w/2 w/2 w/2 -w/2 -w/2];
Y = [h/2 h/2 -h/2 -h/2 h/2];
P = [X;Y];
ct = cos(theta);
st = sin(theta);
R = [ct -st;st ct];
P = R * P;

if (col == 1) %plot black rectangle
    fill(P(1,:)+a,P(2,:)+b,'k');
elseif (col == 0) %plot white rectangle
    fill(P(1,:)+a,P(2,:)+b,'w');
end