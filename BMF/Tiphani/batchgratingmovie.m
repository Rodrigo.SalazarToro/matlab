function batchgratingmovie(diamindegrees)
 
xsize = diamindegrees*36;
rot1 = 0:30:180;
rot = [rot1,270];
gauss = xsize/4;

for i = 1:length(rot)
    rota = rot(i);
    title = ['rot',num2str(rota),'_',num2str(diamindegrees),'gabors.avi'];
    gratingmovie(title,xsize,rota,gauss)
end