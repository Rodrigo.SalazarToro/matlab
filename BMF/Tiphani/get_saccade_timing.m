function [absolutetimingrelativetoimage,latencyinformation] = get_saccade_timing(sacs,eventonset,imageonset)
   
    saccades = sacs(imageonset:end);                                       % all eyemovement information locked to the image onset for accurate timing
    oculobefore = saccades(1:eventonset);                                  % all eyemovements before the start of the event
    oculoafter = saccades(eventonset:end);
    eventon = eventonset;
    
    lastmovement = find(oculobefore,2,'last'); 
    nextmovement = find(oculoafter,3,'first');
    
    lastsaccade = NaN;
    lastfixation = NaN;
    nextsaccade = NaN;
    nextfixation = NaN; 
    saclatency = NaN;
    fixlatency = NaN;
    imageonprox = NaN;
    
    
    if(numel(nextmovement) ~= 0)
        if (numel(lastmovement)==0) % the event is locked to image onset only         
            imageonprox = eventon;
            lastsaccade = 1; %basically imageonset
            lastfixation = NaN;
            nextsaccade = nextmovement(1) + eventon;
            nextfixation  = nextmovement(2) + eventon;
            saclatency = NaN;
            fixlatency = NaN;       
        elseif (numel(lastmovement) == 1) % the event is locked to the first saccade after image onset, but started before fixation 
            imageonprox = eventon;
            if (lastmovement(1) == 1)
                lastsaccade = lastmovement(1);
                saclatency = eventon - lastsaccade;
                    if(saccades(oculoafter(1)) == -1)
                        fixlatency = -nextmovement(1);
                        lastfixation = nextmovement(1) + eventon;
                                if(numel(nextmovement) > 1)
                                    nextsaccade = nextmovement(2) + eventon; %b/c oculoafter(1) is the last fixation...
                                else
                                    nextsaccade = NaN;
                                end
                                if(numel(nextmovement) > 2)
                                    nextfixation = nextmovement(3) + eventon;
                                else
                                    nextfixation = NaN;
                                end
                    else 
                        'Something went wrong in saccade_timing_script #1'               
                    end
            elseif (lastmovement(1) == -1) %starts between imageonset and a fixation, however that's possible. 
                lastsaccade = 1;
                lastfixation = lastmovement(1);
                saclatency = NaN;
                fixlatency = eventon - lastfixation;
                    if(saccades(oculoafter(1)) == 1)
                    nextsaccade = nextmovement(1) + eventon;
                        if(numel(nextmovement) > 1)
                            nextfixation = nextmovement(2) + eventon;
                        else
                            nextfixation = NaN;
                        end
                    else
                        'Something went wrong in saccade_timing_script #2'
                    end
            end
        elseif (saccades(lastmovement(1)) == 1); % last recorded movement before event was a fixation 
            lastsaccade = lastmovement(1);
            lastfixation = lastmovement(2);
            imageonprox = eventon; % event on is relative to image on, so whatever the latency from event is, it is stored in event on 
            saclatency = eventon - lastsaccade;
            fixlatency = eventon - lastfixation;
            nextsaccade = nextmovement(1) + eventon;
            if(numel(nextmovement) > 1)
            nextfixation = nextmovement(2) + eventon;
            else
            nextfixation = NaN;
            end
        elseif (saccades(lastmovement(1)) == -1);
            lastfixation = nextmovement(1) + eventon; %this is the associated fixation, not necessarily the fixation that caused the gamma, as that would have been part of the previous eyemovement
            lastsaccade = lastmovement(2);
            imageonprox = eventon;
            saclatency = eventon - lastsaccade;
            fixlatency = eventon - lastfixation; %this will read negative
            if(numel(nextmovement) > 1)
                nextsaccade = nextmovement(2) + eventon; %b/c oculoafter(1) is the last fixation...
            else
                nextsaccade = NaN;
            end
            if(numel(nextmovement) > 2)
                nextfixation = nextmovement(3) + eventon;
            else
                nextfixation = NaN;
            end
        else %this case will only happen if oscillation is locked to the onset of the saccade file, which is the image onset
            sprintf('Something else wrong');
        end  
        absolutetimingrelativetoimage = [lastsaccade,lastfixation,nextsaccade,nextfixation];
        latencyinformation = [saclatency,fixlatency,imageonprox];
    else
        absolutetimingrelativetoimage = [NaN,NaN,NaN,NaN];
        latencyinformation = [NaN,NaN,NaN];
    end
    
 