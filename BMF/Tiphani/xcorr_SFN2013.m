function xcorr_SFN2013(threshold,session_number)

%run at the ethyl dir level


xx = load('/media/bmf_raid/data/monkey/ethyl/ethyl_gamma/ethylevents.mat');
fm = xx.fm;
sessnames = xx.sessnames;

goodinds = find(fm(:,7) > threshold); 
fm = fm(goodinds,:);


params = struct('tapers',[2 3],'pad',2,'Fs',1000);                         % mtspectrumc parameters
%eoi = fm; % just run for everything, determine how to divide the data later... 

%% loop through each event

    
    counter = 1; % to hold the absolute index of each event. 
    g = session_number;
    subinds = find(fm(:,1) == g); 
    eoi = fm(subinds,:);
    
    correlationdatamatrix = cell(length(eoi(:,1)),2); % session,channel,eventon,eventoff,thisgamma,correlationresults,fullspectrumofepoch,mtspecfreqs
    rawspectra = cell(length(eoi(:,1)),1);
    spectrafreqs = cell(length(eoi(:,1)),1);
    gammasignal = cell(length(eoi(:,1)),1);
    
    %cd to the session of interest 
    session = sessnames{g}; %the name of this session, i.e. 'ethyl11071103'
    folder = session(6:11);
    sessnum = session(12:13);
    cd(['/media/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum]);
    
        
    for i = 1:length(eoi(:,1)) % for each event in the session 
        i
    %% list the visual channels in this session (those in feature matrix)
    thissess = find(fm(:,1) == eoi(i,1));   
    vischans = unique(fm(thissess,3));   
    

    trialno = eoi(i,2); 
    channel = eoi(i,3);
    visarea = eoi(i,4);
    eventon = eoi(i,5);
    eventoff = eoi(i,6);
    imageon = eoi(i,13);
    
    %getrawdata for fft
    if(trialno < 10)
        trialstring = ['000',num2str(trialno)];
        thistrial = [session,'.000',num2str(trialno)];
    elseif(trialno < 100)
        trialstring = ['00',num2str(trialno)];
        thistrial = [session,'.00',num2str(trialno)];
    else
        trialstring = ['0',num2str(trialno)];
        thistrial = [session,'.0',num2str(trialno)];
    end
    
    %% get data from other channels to do cross-channel analyses
    
    [data,~,sampling_rate,scan_order,~,~] = nptReadStreamerFile(thistrial);
    
    %% get data from other channels to do cross-correlation on the extracted epoch
    
    chanind = find(scan_order == channel); 
    thisrawdata = data(chanind,:);    %find this channel on this trial
 
    [thisepoch,visrs] = nptLowPassFilter(double(thisrawdata),sampling_rate,40,100); %filter
    thisgamma = thisepoch(eventon+imageon:eventoff+imageon); %the event markers are relative to the image on mark, but the trial data isn't. 
    [spectrumofrawepoch,frequenciesforrawepoch] = MYmtspectrumc(thisepoch,params,1024);

        
    %this is just the gamma cross-correlation during the actual burst
    
         correlationresults = cell(length(vischans),4); % channel 1,channel 2,c,lags,othergamma
         for n = 1:length(vischans)
             chan = find(scan_order == vischans(n));
             visrawdata = data(chan,:);
             [gammavislfp,visrs] = nptLowPassFilter(double(visrawdata),sampling_rate,40,100);
             othergamma = gammavislfp(eventon+imageon:eventoff+imageon);

             [c,lags] = xcorr(thisgamma,othergamma,50,'coeff');
             correlationresults(n,1) = {channel};     % channel 1's real name
             correlationresults(n,2) = {vischans(n)}; % channel 2's real name
             correlationresults(n,3) = {single(c)};           % correlation coefficient
             correlationresults(n,4) = {single(lags)};        % lags
         end

     infovector = [i,counter,chanind,eventon,eventoff,imageon];
    
     correlationdatamatrix(i,1) = {infovector};
     correlationdatamatrix(i,2) = {correlationresults}; %primary channel for this correlation
     
     rawspectra(i) = {spectrumofrawepoch};
     gammasignal(i) = {single(thisgamma)};
     spectrafreqs(i) = {single(frequenciesforrawepoch)};
     counter = counter + 1;

    end  
     savetitle1 = ['epoch_xcorr_sfn2013_',sessnames{g},'.mat'];
     savetitle2 = ['epoch_other_sfn2013_',sessnames{g},'.mat'];
       
     save(savetitle1,'threshold','correlationdatamatrix','-v7.3')
     save(savetitle2,'rawspectra','gammasignal','spectrafreqs','-v7.3')
end
     
  