function ethyl_otherfreqs_SFN2013_batchversion(threshold,session_number)

% plots the averaged fft for the raw data from events of interest
% ethyl function, run at the ethyl dir level

xx = load('/media/bmf_raid/data/monkey/ethyl/ethyl_gamma/ethylevents.mat');
fm = xx.fm;
sessnames = xx.sessnames;
goodinds = find(fm(:,7) > threshold); 
fm = fm(goodinds,:);


params = struct('tapers',[2 3],'pad',2,'Fs',1000);                         % mtspectrumc parameters
eoi = fm; % just run for everything, determine how to divide the data later... 
good = find(eoi(:,1) == session_number);
eoi = fm(good,:);

%% loop through each event in this session

sessionhilberts = cell(length(eoi(:,1)),1);
sessionfiltered = cell(length(eoi(:,1)),1);
justtheoscillat = cell(length(eoi(:,1)),1);
oscillationband = cell(length(eoi(:,1)),1);
oscillationhilbert = cell(length(eoi(:,1)),1);
peaktorms = zeros(length(eoi(:,1)),1);
avgrms = zeros(length(eoi(:,1)),1);

for i = 1:length(eoi(:,1))
    i
    %% list the visual channels in this session (those in feature matrix)
    vischans = unique(fm(:,3));    
    
    %cd to the session of interest 
    session = sessnames{session_number}; %the name of this session, i.e. 'ethyl11071103'
    folder = session(6:11);
    sessnum = session(12:13);
    cd(['/media/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum]);
    
    trialno = eoi(i,2); 
    channel = eoi(i,3);
    visarea = eoi(i,4);
    eventon = eoi(i,5);
    eventoff = eoi(i,6);
    imageon = eoi(i,13);

    
    %getrawdata for fft
    if(trialno < 10)
        trialstring = ['000',num2str(trialno)];
        thistrial = [session,'.000',num2str(trialno)];
    elseif(trialno < 100)
        trialstring = ['00',num2str(trialno)];
        thistrial = [session,'.00',num2str(trialno)];
    else
        trialstring = ['0',num2str(trialno)];
        thistrial = [session,'.0',num2str(trialno)];
    end
    
    %% get data from other channels to do cross-channel analyses
    
    [data,~,sampling_rate,scan_order,~,~] = nptReadStreamerFile(thistrial);
    
    %% get data from other channels to do cross-correlation on the extracted epoch
    
    chanind = find(scan_order == channel); 
    therawdata = data(chanind,:); 
    thisrawdata = therawdata*(7.62963e-8); % now the data is in volts
    
    %% these analyses are based on the entire time from last to next saccade
    sacfile = load(['/media/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum,'/gamma/',session,'_saccades..',trialstring,'.mat']);
    sacs = sacfile.saccades;
    
    [timing,latencyinformation] = get_saccade_timing(sacs,eventon,imageon);
    lastsaccade = timing(1);
    lastfixation = timing(2);
    nextsaccade = timing(3);
    nextfixation = timing(4);
    
    realtimestart = eventon + imageon;
    realtimeend = eventoff + imageon;
    
    realtimelastsaccade = lastsaccade + imageon;
    realtimenextsaccade = nextsaccade + imageon;
    
    if(~isnan(nextsaccade) && ~isnan(lastsaccade))
    
      bandinfo = [4,8; 8,12; 12,22; 20,30;30,35;35,40;40,45;45,50;50,55;55,60;60,65;65,70;70,75;75,80;80,85;85,90;90,95;95,100];
        
      %% need to filter raw data and THEN take the data from last saccade to next saccade

      fullgammaband = nptLowPassFilter(thisrawdata,sampling_rate,40,100);
      rawsignals = vertcat(nptLowPassFilter(thisrawdata,sampling_rate,4,8),...
                             nptLowPassFilter(thisrawdata,sampling_rate,8,12),...
                             nptLowPassFilter(thisrawdata,sampling_rate,12,22),...
                             nptLowPassFilter(thisrawdata,sampling_rate,20,30),...
                             nptLowPassFilter(thisrawdata,sampling_rate,30,35),...
                             nptLowPassFilter(thisrawdata,sampling_rate,35,40),...
                             nptLowPassFilter(thisrawdata,sampling_rate,40,45),...
                             nptLowPassFilter(thisrawdata,sampling_rate,45,50),...
                             nptLowPassFilter(thisrawdata,sampling_rate,50,55),...
                             nptLowPassFilter(thisrawdata,sampling_rate,55,60),...
                             nptLowPassFilter(thisrawdata,sampling_rate,60,65),...
                             nptLowPassFilter(thisrawdata,sampling_rate,65,70),...
                             nptLowPassFilter(thisrawdata,sampling_rate,70,75),...
                             nptLowPassFilter(thisrawdata,sampling_rate,75,80),...
                             nptLowPassFilter(thisrawdata,sampling_rate,80,85),...
                             nptLowPassFilter(thisrawdata,sampling_rate,85,90),...
                             nptLowPassFilter(thisrawdata,sampling_rate,90,95),...
                             nptLowPassFilter(thisrawdata,sampling_rate,95,100));
        
        
      
        justtheoscillation = fullgammaband(realtimestart:realtimeend);
        %eventsignals = rawsignals(:,realtimelastsaccade:realtimenextsaccade);     

        %doing filtering and hilberts on whole file
        hilberts = zeros(size(rawsignals));                
        for h = 1:length(rawsignals(:,1)) %for each frequency band
            x = hilbert(rawsignals(h,:));
            hilberts(h,:) = x;       
        end       
        
        sessionhilberts(i) = {single(hilberts)};
        sessionfiltered(i) = {single(rawsignals)};
        oscillationhilbert(i) = {hilbert(fullgammaband)};
        oscillationband(i) = {fullgammaband};
        
        justtheoscillat(i) = {justtheoscillation};
        peaktorms(i) = peak2rms(justtheoscillation);
        avgrms(i) = rms(justtheoscillation);

    else
        sessionhilberts(i) = {NaN};
        sessionfiltered(i) = {NaN};
    end
    
    
end

     savetitle1 = ['fixation_hilberts_sfn2013_',sessnames{session_number},'.mat'];
     savetitle2 = ['fixation_filtered_sfn2013_',sessnames{session_number},'.mat'];
     savetitle3 = ['fixation_oscepoch_sfn2013_',sessnames{session_number},'.mat'];
       
     save(savetitle1,'bandinfo','oscillationhilbert','sessionhilberts','-v7.3','-append')
     save(savetitle2,'bandinfo','oscillationband','sessionfiltered','-v7.3','-append')
     save(savetitle3,'justtheoscillat','peaktorms','avgrms','-v7.3','-append')
