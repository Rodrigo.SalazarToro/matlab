function drawrectangles1(col,gridsize)


widthindeg = 0.1;
heightindeg = 0.6; %6 to 1 aspect ratio


ppd = 40;
w=widthindeg*ppd;               
h=heightindeg*ppd;              

figure('name','Test RF Mapping Images') 
whitebg([0.5 0.5 0.5])


angs = 0:12:180;

range = 0.25*ppd*2;
xoffsets = -0.25*ppd:range/gridsize:0.25*ppd; %x offset from -half a degree to +half a degree (in as many steps as grid size, for now)
yoffsets = -0.25*ppd:range/gridsize:0.25*ppd; %y offset from -half a degree to +half a degree





for i = 1:gridsize % for each column
    for n = 1:gridsize 
        an = randperm(length(angs)); 
        xs = randperm(length(xoffsets));
        ys = randperm(length(yoffsets));
        
        yoffset = yoffsets(ys(1));
        xoffset = xoffsets(xs(1));
        ang = angs(an(1));
        theta = ang*2*pi/360;
        x = (i*ppd)+xoffset;
        y = (n*ppd)+yoffset;
        
        myrectangle(x,y,w,h,theta,col)
        
%         xv=[x x+w x+w x x];
%         yv=[y y y+h y+h y];
% 
%         %rotate angle alpha
%         R(1,:)=xv;
%         R(2,:)=yv;
%     
%         XY=[cos(alpha) -sin(alpha);sin(alpha) cos(alpha)]*R;
% 
%         if (col == 1) %plot black rectangle
%         fill(XY(1,:),XY(2,:),'k');
%         elseif (col == 0) %plot white rectangle
%         fill(XY(1,:),XY(2,:),'w');
%         end
          hold on
    end   
end



