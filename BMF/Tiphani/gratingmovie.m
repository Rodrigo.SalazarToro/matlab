function gratingmovie(title,xsize,rot,gauss)

% want 75 frames per second, and two seconds of video, so 150 frames total
% want tp of 5 cycles per second, so 15 frames per cycle


%im = creategrating(1*36,1*36,0,(5/(1*36)),0.001,1,(1*36)/3);

framerate = 75;
ysize = xsize;

%rot = 30;
sf = 5/xsize; % cycles per degree
tp = 15;
gabor = 1;
%gauss = xsize/4;

fig = figure;
writerObj = VideoWriter(title,'Uncompressed AVI');
%'width',xsize,'height',ysize,'codec','xvid'
open(writerObj);

      
    cm=colormap('gray');
    for n = 1:10 % number of complete cycles
        for i = 1:15 % number of frames per cycle
            phase=(i/tp)*2*pi; % so for i = 1, phase = 30 degrees
            % grating
            [x,y]=meshgrid(-xsize/2:xsize/2,-ysize/2:ysize/2);
            angle=rot*pi/180;
            f=sf*2*pi; % cycles/pixel
            a=cos(angle)*f;
            b=sin(angle)*f;
        if gabor
            m=exp(-((x/gauss).^2)-((y/gauss).^2)).*sin(a*x+b*y+phase);
        else
            m=sin(a*x+b*y+phase);
        end
            m=((m+1)/2)*(length(cm)-1)+1; %movie requires 1<=data<=length(colormap)
            imagesc(m);
            axis off;
            truesize;
            frame = getframe(gca); 
            writeVideo(writerObj,frame);
        end
    end
    
    close(fig)
    close(writerObj);
    
    