function ethyl_latency_SFN2013_batchversion(threshold)


xx = load('/media/bmf_raid/data/monkey/ethyl/ethyl_gamma/ethylevents.mat');
fm = xx.fm;
sessnames = xx.sessnames;
goodinds = find(fm(:,7) > threshold);
fm = fm(goodinds,:);


params = struct('tapers',[2 3],'pad',2,'Fs',1000);                         % mtspectrumc parameters


for n = 1:length(sessnames)
    n
    session_number = n;
    good = find(fm(:,1) == session_number);
    eoi = fm(good,:);
    for i = 1:length(eoi(:,1))
        %cd to the session of interest 
        session = sessnames{eoi(i,1)}; %the name of this session, i.e. 'ethyl11071103'
        folder = session(6:11);
        sessnum = session(12:13);
        cd(['/media/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum]);

        trialno = eoi(i,2); 
        channel = eoi(i,3);
        visarea = eoi(i,4);
        eventon = eoi(i,5);
        eventoff = eoi(i,6);
        imageon = eoi(i,13);

        %getrawdata for fft
        if(trialno < 10)
            trialstring = ['000',num2str(trialno)];
            thistrial = [session,'.000',num2str(trialno)];
        elseif(trialno < 100)
            trialstring = ['00',num2str(trialno)];
            thistrial = [session,'.00',num2str(trialno)];
        else
            trialstring = ['0',num2str(trialno)];
            thistrial = [session,'.0',num2str(trialno)];
        end


        % get saccade data for the event of interest to calculate latency
        sacfile = load(['/media/bmf_raid/data/monkey/ethyl/',folder,'/session',sessnum,'/gamma/',session,'_saccades..',trialstring,'.mat']);
        sacs = sacfile.saccades;
        saccades = sacs(imageon:end);

        saclength = length(saccades);
        oculobefore = saccades(1:eventon);
        lastmovement = find(oculobefore,2,'last');

        % lastmovement(2) is the immediately previous eye movement
        % lastmovement(1) is the second to last

        if(numel(lastmovement)==0)
            sprintf('LockedToImageOn');
            imageonprox = eventon;
            saclatency = NaN;
            fixlatency = NaN;
            code = 1;
        elseif(numel(lastmovement) == 1)
            imageonprox = eventon;
            if(lastmovement(1) == 1)
                lastsaccade = lastmovement(1);
                lastfixation = NaN;
                saclatency = eventon - lastsaccade;
                fixlatency = NaN;
                code = 2;
            elseif(lastmovement(1) == -1)
                lastsaccade = NaN;
                lastfixation = lastmovement(1);
                saclatency = NaN;
                fixlatency = eventon - lastfixation;
                code = 3;
            end
        elseif(saccades(lastmovement(1)) == 1);
            lastsaccade = lastmovement(1);
            lastfixation = lastmovement(2);
            imageonprox = eventon; % event on is relative to image on, so whatever the latency from event is, it is stored in event on 
            saclatency = eventon - lastsaccade;
            fixlatency = eventon - lastfixation;
            code = 4;
        elseif (saccades(lastmovement(1)) == -1);
            lastfixation = lastmovement(1);
            lastsaccade = lastmovement(2);
            imageonprox = eventon;
            saclatency = eventon - lastsaccade;
            fixlatency = eventon - lastfixation;
            code = 5;
        else %this case will only happen if oscillation is locked to the onset of the saccade file, which is the image onset
            sprintf('Something else wrong');
        end

        duration = eoi(i,7);
        kurtosis = eoi(i,8);
        variance = eoi(i,9);
        linelen = eoi(i,11);
        numpeak = eoi(i,12);    

        edata.trialnumber(i) = trialno;
        edata.channel(i) = channel;
        edata.visarea(i) = visarea;
        edata.eventon(i) = eventon;
        edata.eventoff(i) = eventoff;
        edata.imageon(i) = imageon;
        edata.absoluteeventon(i) = imageon + eventon;  % for indexing into the raw data properly
        edata.absoluteeventff(i) = imageon + eventoff; % for indexing into the raw data properly
        edata.imageonprox(i) = imageonprox;
        edata.saclatency(i) = saclatency;
        edata.fixlatency(i) = fixlatency;
        edata.duration(i) = duration;
        edata.kurtosis(i) = kurtosis;
        edata.variance(i) = variance;
        edata.linelen(i) = linelen;
        edata.numpeaks(i) = numpeak;
        edata.visarea(i) = visarea;
        edata.saccadecodes(i) = code;

    end


savetitle1 = ['burstdata_sfn2013_',sessnames{session_number},'.mat'];
save(savetitle1,'edata','-v7.3')
end
    