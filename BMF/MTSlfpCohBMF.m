function [C,f,varargout] = MTSlfpCohBMF(varargin)
% Computes the coherence on defined epochs (500ms; fixation,cue,delay1, delay2).
% The type of coherence can be chosen with the arguments:
% at the session level
% Additional arguments are:
%
% The preprocessiong of data includes 60Hz removal and a linear detrend of
% the data.


Args = struct('redo',0,'save',0,'surrogate',0,'compareWindows',0,'InCor',0,'fixation',0,'fixationS',0,'separateDelays',[]);
Args.flags = {'redo','save','surrogate','compareWindows','fixationS','InCor','fixation'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto','trans'});

rsFs = 200; % downsampling

params = struct('tapers',[3 5],'Fs',rsFs,'fpass',[0 100],'trialave',1);

sdir = pwd;
cd ..
ddir = pwd;
mkdir('EpochCoh')
cd(sdir)
skip = nptDir('skip.txt');


if isempty(skip)
    %% selection of match-to-sample trials
    mtst = mtstrial('auto','redosetNames');
    fftrials = mtsgetTrials(mtst,modvarargin{:},'CueObj',[55 48]); % fixation and free viewing trials
    mtstrials = mtsgetTrials(mtst,modvarargin{:});
    selectTrials = setdiff(mtstrials,fftrials);
    GT = mtsgetTrials(mtst,'BehResp',1,'stable','Nocharlie',modvarargin{:});
    GTfromselectTrials = find(ismember(selectTrials,GT) ==1);
    %%
    
    %% get all the channels
    neuroinfo = NeuronalChAssign;
    allgroups = neuroinfo.groups;
    allCH = [1 : length(allgroups)];
    goodCH = checkChannels;
    %%
    
    %% do the epoch cutting, the down sampling, the detrending and other
    %% preprocessing
    
    % [data,~,~] = lfpPcut(selectTrials,allCH,'savelfpPcut'); shouild be before run
    %%
    cd(ddir)
    cd EpochCoh
    mdir = pwd;
    if Args.surrogate
         ncomb = 1;
    else
        ncomb = nchoosek(allCH,2);
    end
    for cm = 1 : size(ncomb,1)
        cd(mdir)
        if Args.surrogate
            matfile = sprintf('cohMTSgenSur.mat');
        else
            matfile = sprintf('cohMTSg%04.fg%04.f.mat',allgroups(ncomb(cm,1)),allgroups(ncomb(cm,2)));
        end
        prematfile = nptDir(matfile);
        if isempty(prematfile) || Args.redo
            cd(sdir); cd('lfp')
            
            skip = nptDir('skip.txt');
            if isempty(skip)
                if Args.surrogate
                    [data,~,~] = lfpPcut(GTfromselectTrials,goodCH,'fromFiles');
                else
                    [data,~,~] = lfpPcut(GTfromselectTrials,ncomb(cm,:),'fromFiles'); % assumes that the trials in the lfpPcut file are all taken
                end
                C = cell(4,1);
                phi = cell(4,1);
                Prob = cell(4,1);
                for p = 1 : 4
                    if Args.surrogate
                        %                     [Session(scount).Cmean(:,cb,p),Session(scount).phCstd(:,cb,p),Session(scount).phimean(:,cb,p),Session(scount).phistd(:,cb,p),Session(scount).tile99(:,cb,p),Session(scount).Prob(:,:,cb,p),f] = cohsurrogate(data{p},data{p},params,'generalized','saveName',sprintf('generalSurRule%dp%d.mat',allrules(scount),p),'nrep',10000,modvarargin{:});%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                        [~,~,~,~,~,Prob{p},f,C{p}] = cohsurrogate(data{p},data{p},params,'generalized','nrep',10000);
                    else
                        [C{p},phi{p},~,~,~,f] = coherencyc(squeeze(data{p}(:,:,1)),squeeze(data{p}(:,:,2)),params);%coherencyc(data{CHcomb(cb,1)}(lowLim:highLim,:),data{CHcomb(cb,2)}(lowLim:highLim,:),params);
                    end
                end
                if Args.save
                    cd(mdir)
                    if Args.surrogate
                        save(matfile,'C','Prob','f')
                    else
                        save(matfile,'C','phi','f')
                    end
                    display(sprintf('saving %s',matfile))
                end
            end
        end
    end
end
%                                        tf = ismember(ccomb,CHcomb(cb,:),'rows');
%                                         load(sprintf('fourierTrans/Rule%dgr%04.0fgr%04.0f.mat',allrules(scount),gcomb(tf,1),gcomb(tf,2)))
%                                         Session(scount).C(:,cb,:) = C;
%                                         Session(scount).phi(:,cb,:) = phi;

