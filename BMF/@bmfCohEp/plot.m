function obj = plot(obj,varargin)


Args = struct('sigLevel',4,'phase',0,'plotType','default','power',[],'frange',[0 50],'gram',[],'maxTimeScale',0,'minTrials',100,'matchAlign',0,'sortedStim',4);
Args.flags = {'phase','maxTimeScale','matchAlign'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'raster','cues9'});

[numevents,dataindices] = get(obj,'Number',varargin2{:});

if ~isempty(dataindices)
    if ~isempty(Args.NumericArguments)
        
        n = Args.NumericArguments{1}; % to work oon
        ind = dataindices(n);
        
    else
        
    end
    
    epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
    ymax = [];
    ymin = [];
    
    
    align = {'Sample off-set aligned' 'Match aligned'};
    
    pairg = obj.data.groups(ind,:);
    
    info = sprintf('%s',obj.data.setNames{ind});
    fprintf('%s \n',info)
    h = gcf;
    set(h,'Name',info)
    load(obj.data.setNames{ind})
    [pathstr, ~, ~, ~] = fileparts(obj.data.setNames{ind});
    frange = find(f >= Args.frange(1) & f <= Args.frange(2));
    
    %%
    switch Args.plotType
        case 'TimeFreq'
            cd grams
            ref = [1 1.8];
            
            maxc = [];
            for r = 1 : 1
                switch Args.gram
                    case {'coh','phi'}
                        file{2} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%dIncor.mat',pairg(1),pairg(2),r));
                        
                        file{3} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%dFix.mat',pairg(1),pairg(2),r));
                        if ~isempty(file{3}); n = 3; else n = 2; end
                        file{1} = nptDir(sprintf('cohgramg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),r));
                    case {'tunePow','tuneCoh'}
                        file{1} = nptDir(sprintf('tunegramg%04.0fg%04.0fRule%d.mat',pairg(1),pairg(2),r));
                        n = 1;
                    case 'migram'
                        file{1} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dLoc.mat',pairg(1),pairg(2),r));
                        for fff = 1 : 3;
                            file{1+fff} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dIdePerLoc%d.mat',pairg(1),pairg(2),r,fff));
                            file{4+fff} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dIdePerLoc%dIncor.mat',pairg(1),pairg(2),r,fff));
                            file{7+fff} = nptDir(sprintf('migramSurGenInterRule%dIdePerLoc%d.mat',r,fff));
                        end;
                        file{11} = nptDir(sprintf('migramg%04.0fg%04.0fRule%dFix.mat',pairg(1),pairg(2),r));
                        file{12} = nptDir(sprintf('migramSurGenInterRule%dLoc.mat',r));
                        n = 12;
                        nmax = sum(~cellfun(@isempty,file));
                end
                maxtime = cell(2,1);
                count = 1;
                for fi = 1 : n
                    if ~isempty(file{fi})
                        switch Args.gram
                            case 'coh'
                                load(file{fi}.name);
                                thedata = C;
                                lim = [0 0.3];
                            case 'phi'
                                load(file{fi}.name);
                                thedata = phi;
                                lim = [-pi pi];
                            case 'tuneCoh'
                                load(file{fi}.name,'tuneCoh','f','variable');
                                thedata = tuneCoh;
                                lim = [0 0.5];
                                ch = 1;
                            case 'tunePow'
                                load(file{fi}.name,'tunePow','f','variable');
                                thedata = tunePow;
                                lim = [0 0.5];
                                ch = 2;
                            case 'migram'
                                load(file{fi}.name,'trials','f','time','mutualgram');
                                if length(size(mutualgram)) == 2
                                    thedata = mutualgram;
                                else
                                    thedata = squeeze(prctile(mutualgram,98,1));
                                end
                        end
                        
                        if ~isempty(thedata)
                            if ~isempty(findstr(Args.gram,'tune'))
                                for tune = 1 : 2
                                    for c = 1 : ch
                                        subplot(ch * 2,2,(r-1)*2+tune+(c-1) * 4)
                                        
                                        switch Args.gram
                                            case 'tunePow'
                                                pdata =   squeeze(thedata(tune).T(c,:,:));
                                                %                                 imagesc(variable(tune).t{1}(1:size(thedata(tune).T,ch)),f,squeeze(thedata(tune).T(c,:,:))',[lim(1) lim(2)])
                                            case 'tuneCoh'
                                                pdata = thedata(tune).T(:,:);
                                                
                                        end
                                        imagesc(variable(tune).t{1}(1:size(thedata(tune).T,ch)),f,pdata',[lim(1) lim(2)])
                                        maxc = [maxc max(max(pdata))];
                                        if r == 1; title(tuningN{tune}); end
                                        if tune == 1; ylabel(sprintf('%s; %s',MLrules{r},area{c})); end
                                        ylim(Args.frange)
                                        colorbar
                                        set(gca,'TickDir','out');
                                    end
                                end
                            elseif strcmp('migram',Args.gram)
                                
                                subplot(nmax,2,(count-1)*2+1)
                                
                                imagesc(time,f,thedata',[0 1.5])
                                title(file{fi}.name)
                                xlim([0.1 2])
                                
                            else
                                for cond = 1 : 2
                                    if length(trials) > Args.minTrials
                                        maxtime{cond} = [maxtime{cond}; [min(t{cond}) max(t{cond})]];
                                        subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi)
                                        imagesc(t{cond},f,thedata{cond}',[lim(1) lim(2)])
                                        flim = find(f >Args.frange(1) & f < Args.frange(2));
                                        if max(max(thedata{cond}(:,flim))) > 0.4; amaxc = 0.4; else amaxc = max(max(thedata{cond}(:,flim)));end
                                        maxc = [maxc amaxc];
                                        
                                        colorbar
                                        hold on
                                        line([ref(cond) ref(cond)],Args.frange,'Color','r')
                                        ylim(Args.frange)
                                        if cond == 1; title(sprintf('%s; %s; n=%d',fname{fi},align{cond},length(trials)));else title(align{cond}); end
                                        if fi == 1; ylabel(MLrules{r}); end
                                        set(gca,'TickDir','out');
                                    end
                                end
                            end
                            
                        end
                        count = count+1;
                    end
                end
                if strcmp('migram',Args.gram)
                else
                    if ~Args.maxTimeScale && (~isempty(maxtime{1}) || ~isempty(maxtime{2})); for r = 1 : 2; for fi = 1 : n;  for cond = 1 : 2;  subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi); xlim([max(maxtime{cond}(:,1)) min(maxtime{cond}(:,2))]); end; end; end; end
                    if isempty(findstr(Args.gram,'tune')); for r = 1 : 2; for fi = 1 : n;  for cond = 1 : 2;  subplot(4,n,(r-1) * (n*2) + (cond - 1) * n + fi); caxis([lim(1) max(maxc)]); end; end; end;end
                end
            end
            
            xlabel('Time [sec]')
            ylabel('Frequency [Hz]')
            cd ..
            
            %%
        case 'default'
            cd(pathstr)
            sur = load('cohMTSgenSur.mat');
            for p = 1: 4
                subplot(2,4,p)
                
                plot(f,C{p})
                hold on
                ymax = [ymax max(C{p}(frange))];
                
                hold on;
                
                plot(f,sur.Prob{p}(:,Args.sigLevel),'--')
                title(epochs{p})
                subplot(2,4,p+4)
                plot(f,phi{p})
                
            end
            xlabel('Frequency [Hz]')
            ylabel('Phase [rad]')
            
            for sb = 1 : 4; subplot(2,4,sb); axis([Args.frange 0 max(ymax)]); set(gca,'TickDir','out');end
            ylabel('coherency')
            for sb = 5 : 8; subplot(2,4,sb); xlim(Args.frange); set(gca,'TickDir','out');end
            xlabel('Frequency [Hz]')
            
    end
end

