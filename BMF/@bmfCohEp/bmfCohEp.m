function obj = bmfCohEp(varargin)
%
%
Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0);
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'bmfCohEp';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'bmfCoh';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) & isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [pdir,cdir] = getDataDirs('site','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('day','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                & (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

function obj = createObject(Args,varargin)

if ~isempty(nptDir('EpochCoh'))
    ddir = pwd;
    cd session01/highpass
    if isempty(nptDir('SNR_channels.mat'))
        channel_snr_list = [];
    else
        load SNR_channels.mat
        
    end
    
    cd(ddir)
    cd EpochCoh
    
    files = nptDir('cohMTSg0*.mat');
    
    if ~isempty(files)
       
        data = getIndexCohBMF(files);
        filesinfo = struct2cell(files);
        filesinfo = cell2mat(filesinfo(1,:)');
        
        data.groups(:,1) = str2num(filesinfo(:,8:11));
        data.groups(:,2) = str2num(filesinfo(:,13:16));
        filesinfo = cat(2,repmat([pwd '/'],size(filesinfo,1),1),filesinfo);
        data.setNames = mat2cell(filesinfo,ones(size(filesinfo,1),1),size(filesinfo,2));
        if isempty(channel_snr_list)
            data.snr = 99*ones(size(data.groups,1),2);
            for ch = 1 : size(channel_snr_list,1)
                data.snr(data.groups == channel_snr_list(ch,1)) = channel_snr_list(ch,2);
            end
        else
            data.snr = 99*ones(size(data.groups,1),2);
        end
        data.numSets = size(files,1);
        data.Index = [(1: data.numSets)' ones(data.numSets,1)];
       
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('*mat files missing \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('EpochCoh directory missing \n');
    obj = createEmptyObject(Args);
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];
data = struct('betapos34',[],...
    'betapos14',[],...
    'betapos12',[],...
    'betapos23',[],...
    'betaneg34',[],...
    'betaneg14',[],...
    'betaneg12',[],...
    'betaneg23',[],...
    'gammapos34',[],...
    'gammapos14',[],...
    'gammapos12',[],...
    'gammapos23',[],...
    'gammaneg34',[],...
    'gammaneg14',[],...
    'gammaneg12',[],...
    'gammaneg23',[],...
    'alphapos34',[],...
    'alphapos14',[],...
    'alphapos12',[],...
    'alphapos23',[],...
    'alphaneg34',[],...
    'alphaneg14',[],...
    'alphaneg12',[],...
    'alphaneg23',[],...
    'betamodIDE',[],...
    'betamodLOC',[],...
    'gammamodIDE',[],...
    'gammamodLOC',[]);


% useful fields for most objects
data.Index = [];
data.numSets = 0;
data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
