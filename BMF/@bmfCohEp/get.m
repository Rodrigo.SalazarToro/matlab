function [r,varargout] = get(obj,varargin)

Args = struct('Number',0,'ObjectLevel',0,'snr',99,'type',[],'sigSur',[],'sigSurPIntersect',0,'hist',[],'delay',[],'groups',[]);
Args.flags = {'Number','ObjectLevel','sigSurPIntersect',};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

SetIndex = obj.data.Index;

varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    rtemp = [1:size(SetIndex,1)];
    
    if ~isempty(Args.hist)
        rtemph = [];
        for nf = 1 : size(Args.hist,1)
            for area = 1 : 2
                if isnan(Args.hist(nf,area))
                    row1{area} = [1 : size(obj.data.hist,1)];
                else
                    [row1{area},col] = find(obj.data.hist == Args.hist(nf,area));
                end
            end
            row = intersect(row1{1},row1{2});
            rtemph = [rtemph; row];
        end
        rtemp = intersect(rtemp,rtemph);
    end
    
    if ~isempty(Args.snr)
        indsnr = find(obj.data.snr(:,1) >= Args.snr & obj.data.snr(:,2) >= Args.snr);
        rtemp = intersect(rtemp,indsnr) ; % ttem;
    end
    
    
    if ~isempty(Args.type)
        
        for c = 1 : size(Args.type,1)
            tnews = [];
            for e =  1 : length(Args.type{c,2})
                eval(sprintf('tnews = [tnews; vecc(find(obj.data.%s == %d))];',Args.type{c,1},Args.type{c,2}(e)));
            end
            rtemp = intersect(rtemp,tnews);
        end
      
    end
    
    if ~isempty(Args.sigSur)
       
        for c = 1 : size(Args.sigSur,1)
            
            for p = 1: length(Args.sigSur{c,2})
                tnews2{p} = [];
                for e =  1 : length(Args.sigSur{c,3})
                    eval(sprintf('tnews2{p} = [tnews2{p}; vecc(find(obj.data.%s(:,%d) == %d))];',Args.sigSur{c,1},Args.sigSur{c,2}(p),Args.sigSur{c,3}(e)));
                end
                if p > 1; if Args.sigSurPIntersect; tnews2{1} = intersect(tnews2{1},tnews2{p}); else; tnews2{1} = union(tnews2{1},tnews2{p}); end;end
            end
            tnews = tnews2{1};
            rtemp = intersect(rtemp,tnews);
        end
        
    end
    
    if ~isempty(Args.groups)
        
        for ii = 1 : length(Args.groups)
        [indgr,~] = find(obj.data.groups == Args.groups);
        rtemp = intersect(rtemp,indgr) ;
        end
         % ttem;
    end
    
    varargout{1} = rtemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

