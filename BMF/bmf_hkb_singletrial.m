function bmf_hkb_singletrial(varargin)


%run at session level
%computes Shannon Entropy and normalized to get the synchronization index
%in tass_1998
Args = struct('ml',1,'plot',0);
Args.flags = {};
Args = getOptArgs(varargin,Args);


cd('/Volumes/bmf_raid/data/monkey/ethyl/110714/session01')

%phase_paper_iti_figures.m, investigage data from this figure

% bins = [0:.01:1]; %normalized by 2pi and using absolute values so the relative phases are between -0.5 and 0.5 (see tass_1998)
window = 300;
stepsize = 150;
nbins = exp(.626 + (.4*log(window-1))); %see levanquyen_2001, should this be log2 instead of the the natural log?
bins = linspace(0,1,nbins);
max_ent = log2(size(bins,2));

bandpasslow = 8;
bandpasshigh = 25;
%
% bandpasslow = 1;
% bandpasshigh = 8;

sesdir = pwd;
%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

N = NeuronalHist('bmf','ml');

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
%Get trials
tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
sample_on = floor(mt.data.CueOnset); %lock to sample off
match_on = floor(mt.data.MatchOnset);

cd([sesdir filesep 'lfp'])
lfpdata = nptDir('*_lfp*');

cd(sesdir)

chs = [201 248;201 233;201 235;201 209;201 246; 201 255; 233 248; 233 235; 209 246; 5 32; 18 248; 18 201];
for xx = 1:3%12
    % ch1 = find(N.gridPos == 201);
    % ch2 = find(N.gridPos == 248);
    
    ch1 = find(N.gridPos == chs(xx,1));
    ch2 = find(N.gridPos == chs(xx,2));
    
    allallhist = zeros(18,100);
    resp_ent = [];
    allea = [];
    allcm = [];
    allt = [];
    counter = 0;
    ccc = 0;
    for t = tr (1:100)
        counter = counter + 1;
        trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
        
        [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
        
        %lowpass
        hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,bandpasslow,bandpasshigh);
        
        %get data ranges
        % % % %     datarange = [sample_on(t) - 499 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
        bpdata = hdata; %use whole trial!
        
        bpd1 = bpdata(ch1,:);
        bpd2 = bpdata(ch2,:);
        
        data = bpdata'; %need to do this before and after using hilbert, don't include during the transform
        %compute hilbert transform
        data = hilbert(data);
        
        %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
        %COMPENT IS FLIPPED
        %find instantaneous phase angles
        data = angle(data);
        data = data';
        
        d1 = unwrap(data(ch1,:)) ./ (2*pi);
        d2 = unwrap(data(ch2,:)) ./ (2*pi);
        
        %cyclic relative phase
        d = mod(d1 - d2,1);
        
        steps = [window/2:stepsize:size(d,2)-(window/2)]; %sliding window parameters (use 4s for incorrect trials)
        nw = size(steps,2);
        
        
        for ww = steps
            ccc = ccc + 1;
            allcm(ccc) = circ_mean((d(ww-((window/2)-1):ww+(window/2))'.*2*pi));%calculate response distribution
        end
        
        cc = 0;
        allh = zeros(window,nw);
        for ww = steps
            cc = cc + 1;
            allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
        end
        
        allhist = hist(allh,bins) ./ window; %calculate response distribution
        re  = -1 * nansum(allhist.*log2(allhist));
        resp_ent = [resp_ent (max_ent - re) ./ max_ent];
        
        
        
        ea = [];
        eb = [];
        eoffset = [];
        for x = 1 : size(allhist,2)
            [hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',smooth(allhist(:,x),3) .* -1);
            
            ab_ratio(x) = estimates(2)/estimates(1);
            
            ea(x) = estimates(1);
            eb(x) = estimates(2);
            eoffset(x) = estimates(3);
            
%                     plot(hkb_function,'r')
%                     hold on;plot(smooth(allhist(:,x),3).*-1,'b')
%                     pause
%                     cla
        end
        
        allea = [allea ea];
        
     
        
        for xt = 1 : size(allhist,2)
            if steps(xt) < (sample_on(t) - 499)
                allt = [allt 1];
            elseif steps(xt) < sample_on(t) && steps(xt) > (sample_on(t) - 500)
                allt = [allt 2];
            elseif steps(xt) > sample_on(t) && steps(xt) < (sample_on(t) + 500) 
                allt = [allt 3];
            elseif steps(xt) > (sample_on(t) + 700) && steps(xt) < sample_on(t) + 1300 %match
                allt = [allt 4];
            elseif steps(xt) > match_on(t) && steps(xt) < match_on(t) + 800
                allt = [allt 5];
            elseif steps(xt) > match_on(t) + 800 && steps(xt) > match_on(t) + 1400
                allt = [allt 6]; 
            else
                allt = [allt 7]; 
            end
        end

        %     subplot(3,1,1)
        %     imagesc(allhist(:,1:70) ./max(max(allhist)))
        %     hold on
        %     [~,mm] =max(allhist);
        %     plot(mm,'r','LineWidth',2)
        %     subplot(3,1,2)
        %     imagesc(allallhist(:,1:70) ./ max(max(allallhist)))
        %     subplot(3,1,3)
        %     plot(smooth(ea,10),'r')
        %     hold on
        % %     plot(eb,'b')
        % %     hold on
        %     pause(.1)
        % %     cla
        
        
    end
    allallresp_ent{xx} = resp_ent;
    allallea{xx} = allea;
    allallcm{xx} = allcm;
end

for xxx = 1:size(allallea,2)
    alleas(:,xxx) = allallea{xxx};
    
    allresp(:,xxx) = allallresp_ent{xxx};
end

for xxx = 1:size(allallea,2)
    alleas(find(allresp(:,xxx) < .1),xxx) = nan;
end


% [wcoeff,score,latent,tsquared,explained] = pca(alleas,'VariableWeights','variance');
[wcoeff,score,latent,tsquared,explained] = pca(alleas,'Rows','pairwise');



for xc = 1:size(score,1)
    if allt(xc) == 7
        scatter3(score(xc,1),score(xc,2),score(xc,3),50,'r','filled')
    elseif allt(xc) == 5
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'g','filled')
    elseif allt(xc) == 4
        scatter3(score(xc,1),score(xc,2),score(xc,3),50,'b','filled')
    elseif allt(xc) == 3
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'k','filled')
    elseif allt(xc) == 2
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'m','filled')
    else
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'y','filled')
    end
    hold on
end






%%
for xxx = 1:size(allallcm,2)
    allcms(:,xxx) = smooth(allallcm{xxx},10);
    
    allresp(:,xxx) = allallresp_ent{xxx};
end

for xxx = 1:size(allallcm,2)
    
    
    
    allcms(find(allresp(:,xxx) < .2),xxx) = 0;
    
    
    cm1(xxx) = circ_mean(allcms(find(allcms(find(allt == 7),xxx))));
    cm2(xxx) = circ_mean(allcms(find(allcms(find(allt == 4),xxx))));
    
    
end

allcms = (allcms);

% [wcoeff,score,latent,tsquared,explained] = pca(allcms,'VariableWeights','variance');
[wcoeff,score,latent,tsquared,explained] = pca(allcms,'Rows','pairwise');



for xc = 1:size(score,1)
    if allt(xc) == 7
        scatter3(score(xc,1),score(xc,2),score(xc,3),50,'r','filled')
    elseif allt(xc) == 5
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'g','filled')
    elseif allt(xc) == 4
        scatter3(score(xc,1),score(xc,2),score(xc,3),50,'b','filled')
    elseif allt(xc) == 3
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'k','filled')
    elseif allt(xc) == 2
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'m','filled')
    else
%         scatter3(score(xc,1),score(xc,2),score(xc,3),50,'y','filled')
    end
    hold on
end


find(allt == 7)


