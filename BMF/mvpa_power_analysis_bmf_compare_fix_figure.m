function mvpa_power_analysis_bmf_compare_fix_figure(varargin)

%run at monkey level
Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl')
mdir = pwd;
load alldays

recording_category = {'visual','Temporal','PPC','PFC','Motor','Somato','All'};

allperformances1 = zeros(25,4);
allperformances2 = zeros(25,4);
allperformances3 = zeros(25,4);
allperformances4 = zeros(25,4);
for x = 1:25;
    
    cd([mdir filesep alldays{x} filesep 'session01'])
    
    
    
    load performance1
    load performance2
    load performance3
    load performance4
    
    allperformances1(x,:) = performance1(7,:);
    allperformances2(x,:) = performance2(7,:);
    allperformances3(x,:) = performance3(7,:);
    allperformances4(x,:) = performance4(7,:);
    
% %     hold on
% %     for e = 1:4
% %         subplot(1,4,e)
% %         for a = 7%1:7
% %             scatter(a-.3,performance1(a,e),'r','fill','s')
% %             hold on
% %         end
% %     end
% %     
% %     hold on
% %     for e = 1:4
% %         subplot(1,4,e)
% %         for a = 7%1:7
% %             scatter(a-.1,performance2(a,e),'b','fill','s')
% %             hold on
% %         end
% %     end
% %     
% %     hold on
% %     for e = 1:4
% %         subplot(1,4,e)
% %         for a = 7%1:7
% %             scatter(a+.1,performance3(a,e),'g','fill','s')
% %             hold on
% %         end
% %     end
% %     
% %     hold on
% %     for e = 1:4
% %         subplot(1,4,e)
% %         for a = 7%1:7
% %             scatter(a+.3,performance4(a,e),'k','fill','s')
% %             hold on
% %         end
% %         axis([0 8 0 100])
% %         hold on
% %         plot([0 8],[50 50],'k','LineWidth',2)
% %         hold on
% %         plot([0 8],[40 40],'--k','LineWidth',1)
% %         hold on
% %         plot([0 8],[60 60],'--k','LineWidth',1)
% %         hold on
% %         
% %         set(gca,'XTick',[1:7])
% %         set(gca,'XTickLabel',recording_category)
% %     end
end


for e = 1:4
    subplot(1,4,e)
    boxplot([allperformances1(:,e),allperformances2(:,e),allperformances3(:,e),allperformances4(:,e)],'plotstyle','compact','symbol','*')
    set(gca, 'TickDir', 'out')
    axis([0 5 0 100])
    hold on
    plot([0 5],[50 50],'--k','LineWidth',1)
end


