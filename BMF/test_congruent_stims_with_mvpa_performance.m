function test_congruent_stims_with_mvpa_performance


cd('/Volumes/bmf_raid/data/monkey/ethyl')
mdir = pwd;
load alldays

recording_category = {'visual','Temporal','PPC','PFC','Motor','Somato','All'};

for x = 1:25;
    cd([mdir filesep alldays{x} filesep 'session01'])
    
    
    load performance1_stims
    load performance2_stims
    load performance3_stims
    
    
    p1(x) = performance1_stims(7,1);
    p2(x) = performance2_stims(7,1);
    p3(x) = performance3_stims(7,1);
    
    
    mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
    alltrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
    for xt = 1:5
        identity{xt} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','iCueObj',xt);
        [~,ist] = intersect(alltrials,identity{xt});
        stim_list(ist) = xt;
    end
    
    
    rt(x) = size(find(abs(diff(stim_list)) == 0),2)/size(find(abs(diff(stim_list)) == 1),2);
    
end

rt;



