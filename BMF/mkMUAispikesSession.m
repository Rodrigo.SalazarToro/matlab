function mkMUAispikesSession(varargin)
% to run in the session


Args = struct('redo',0);
Args.flags = {'redo'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{'redo'});



% for d = 1 : 50
%     cd(days(d).name)
%     sessions = nptDir('session0*');
%     for s = 1 : length(sessions)
%         cd(sprintf('session0%d',s))
%         if ~isempty(nptDir('mts.txt')) && isempty(nptDir('sort'))
%             ProcessSession('extraction','threshold',4,'redo');
%         end
%         cd ..
%     end
%     ! /media/raid/cvs/npt/scripts/batch_processor/moveprocessedfiles.sh
%     for s = 1 : length(sessions)
%         cd(sprintf('session0%d',s))
%         if ~isempty(nptDir('mts.txt')) && isempty(nptDir('sort'))
%             mkMUAispikesSession
%         end
%         cd ..
%     end
%     cd ..
% end
info =NeuronalChAssign;


for g = 1 : length(info.groups)
    cd sort
    grNum = sprintf('%04.0f',info.groups(g));
    grName = sprintf('group%04.0f',info.groups(g));
    sp = ispikes('Group',grNum,'UseSort',0);
    
    cd ..
    mkdir(grName)
    cd(grName)
    mkdir('cluster01m')
    cd('cluster01m')
    save ispikes.mat sp
    cd ../..
end




