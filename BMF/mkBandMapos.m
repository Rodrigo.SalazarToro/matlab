function mkBandMapos(obj,varargin)

Args = struct('modThres',20,'band','beta','epoch',0,'nbins',4,'coherenceBar',0,'groups',[],'limits',[0 0.5]);
Args.flags = {'epoch','coherenceBar'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

epochs = {'presample' 'sample' 'delay 1' 'delay 2' 'all'};
switch Args.band
    case 'theta'
        limits = [3 7];
    case 'alpha'
        limits = [7 13];
    case 'beta'
        limits = [13 30];
    case 'gamma'
        limits = [30 42];
    case 'highgamma'
        limits = [42 80];
end

if Args.epoch
    eval(sprintf('for p = 1 : 4; sigband{p} = find(obj.data.%sSurMod(:,p) > Args.modThres); end;',Args.band));
else
    eval(sprintf('sigband = find(max(obj.data.%sSurMod,[],2) > Args.modThres);',Args.band));
end

colorcode = -sort(-(gray(10*(Args.nbins+1))));
% colorcode = jet(10*(Args.nbins+1));
if Args.coherenceBar
    
    allvalues = unique(eval(sprintf('obj.data.%sMeanCoh',Args.band)));
    fname = 'MeanCoh';
else
    allvalues = unique(eval(sprintf('obj.data.%sSurMod',Args.band)));
    fname = 'SurMod';
end

if isempty(Args.limits)
    [~,bins] = hist(allvalues,Args.nbins);
    step = bins(1) /2;
    bins = bins +step;
else
    bins = [Args.limits(2)/Args.nbins:Args.limits(2)/Args.nbins: Args.limits(2)];
    
end

for p = 1 : size(sigband,2)
    [~,xy] = bmf_grid;
    
    if iscell(sigband)
        sigbandt = sigband{p};
        ap = 0;
    else
        sigbandt = sigband;
        ap = 4;
    end
    for ii = sigbandt'
        
        thepair = obj.data.groups(ii,:);
        if isempty(Args.groups) || (~isempty(Args.groups) && sum(ismember(thepair,Args.groups)) == 2)
            
            
            if Args.epoch
                
                themod = eval(sprintf('obj.data.%s%s(ii,p);',Args.band,fname));
            else
                themod = eval(sprintf('max(obj.data.%s%s(ii,:));',Args.band,fname));
            end
            
            colorind = find(bins >= themod);
            if isempty(colorind); colorind = Args.nbins; end
            line([xy(thepair(1),2) xy(thepair(2),2)],[11-xy(thepair(1),1) 11-xy(thepair(2),1)],'color',colorcode(10*(colorind(1))+1,:),'LineWidth',3)
        end
    end
    
    clabels = num2str(round([0 1000*bins(1:round(Args.nbins/4):Args.nbins)])'/1000);
    a = colorbar('YTickLabel',clabels,'Location','WestOutside');
    ticks = get(a,'Ylim');
    tickrange = abs(ticks(1) - ticks(end));
    tickstep = tickrange/(Args.nbins);
    colormap(-sort(-gray(10*(Args.nbins+1))))
    %     colormap(jet(10*(Args.nbins+1)));
    set(a,'YTick',[ticks(1):tickstep:ticks(end)]);
    set(gcf,'Name',sprintf('%s %d - %d Hz; %s',Args.band,limits(1),limits(2),epochs{p +ap}))
    
end
