function scatter3dpower


cd('/Volumes/bmf_raid/data/monkey/ethyl/')

load ethyl_power_mat

ee = [];
aa = [];
for x = 1:817
    
    ee(x,:) = (ethyl_power_mat(x,1:59) .* linspace(0,90.6250,59)) ./ sum(ethyl_power_mat(x,1:59) .* linspace(0,90.6250,59));
    
    parea = ethyl_power_areas(x);
    
    if parea < 9
        aa{x} = 'r';
    elseif parea < 14
        aa{x} = 'b';
    elseif parea < 22
        aa{x} = 'r';
    elseif parea < 38
        aa{x} = 'g';
    elseif parea < 44
        aa{x} = 'k';
    else
        aa{x} = 'k';
    end
        
end

for x = 1 : 817
    
    scatter3(ee(x,5),ee(x,12),ee(x,20),'fill','s',aa{x})
hold on
end