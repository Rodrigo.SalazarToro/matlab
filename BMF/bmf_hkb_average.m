function bmf_hkb_average(varargin)


Args = struct();
Args.flags = {};
Args = getOptArgs(varargin,Args);


cd('/Volumes/bmf_raid/data/monkey/ethyl/110714/session01')

nbins = 36;
bins = linspace(0,1,nbins);

max_ent = log2(size(36,2));

bandpasslow = 8;
bandpasshigh = 25;


sesdir = pwd;
%determine which channels are neuronal
descriptor_file = nptDir('*_descriptor.txt');
descriptor_info = ReadDescriptor(descriptor_file.name);
neuronalCh = find(descriptor_info.group ~= 0);

N = NeuronalHist('bmf','ml');

mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
%Get trials
tr = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','Nlynx');
ntrials = size(tr,2);

% steps = floor(linspace(1,ntrials,5));
steps = floor(linspace(1,ntrials,4));
nsteps = size(steps,2);

sample_on = floor(mt.data.CueOnset); %lock to sample off
sample_off = floor(mt.data.CueOffset);
match_on = floor(mt.data.MatchOnset);

cd([sesdir filesep 'lfp'])
lfpdata = nptDir('*_lfp*');

cd(sesdir)
for xxx = 1 : nsteps-1
chs = [201 248;201 233;201 235;201 209;201 246; 201 255; 233 248; 233 235; 209 246; 5 32; 18 248; 18 201];
pair_counter = 0;
allcm = [];
for xx = 1:6%12
    pair_counter = pair_counter + 1;
    
    ch1 = find(N.gridPos == chs(xx,1));
    ch2 = find(N.gridPos == chs(xx,2));
    
    dbins = cell(1,18);
    counter = 0;
    for t = tr(steps(xxx):steps(xxx+1))
        counter = counter + 1;
        trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
        
        [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
        
        %lowpass
        hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,bandpasslow,bandpasshigh);
        
        %get data ranges
        % % % %     datarange = [sample_on(t) - 499 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
        bpdata = hdata; %use whole trial!
        
        data = bpdata'; %need to do this before and after using hilbert, don't include during the transform
        %compute hilbert transform
        data = hilbert(data);
        
        %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
        %COMPENT IS FLIPPED
        %find instantaneous phase angles
        data = angle(data);
        data = data';
        
        d1 = unwrap(data(ch1,:));
        d2 = unwrap(data(ch2,:));
        
        %cyclic relative phase
        d = mod(d1 - d2,2*pi);
        
        
        dbins{1} = [dbins{1} d(1:sample_on(t)-500)]; %prepresample
        
        dbins{2} = [dbins{2} d(sample_on(t)-500:sample_on(t) - 250)]; %presample
        
        dbins{3} = [dbins{3} d(sample_on(t)-250:sample_on(t))]; %presample
        
        dbins{4} = [dbins{4} d(sample_on(t):sample_on(t)+250)]; %sample
        
        dbins{5} = [dbins{5} d(sample_on(t)+250:sample_on(t)+500)]; %sample
        
        dbins{6} = [dbins{6} d(sample_off(t):sample_off(t)+250)];
        
        dbins{7} = [dbins{7} d(sample_off(t)+250:sample_off(t)+500)];
        
        dbins{8} = [dbins{8} d(sample_off(t)+500:sample_off(t)+750)];
        
        dbins{9} = [dbins{9} d(sample_off(t)+750:sample_off(t)+1000)];
        
        dbins{10} = [dbins{10} d(match_on(t):match_on(t)+250)];
        
        dbins{11} = [dbins{11} d(match_on(t)+250:match_on(t)+500)];
        
        dbins{12} = [dbins{12} d(match_on(t)+500:match_on(t)+750)];
        
        dbins{13} = [dbins{13} d(match_on(t)+750:match_on(t)+1000)];
        
        dbins{14} = [dbins{14} d(match_on(t)+1000:match_on(t)+1250)];
        
        dbins{15} = [dbins{15} d(match_on(t)+1250:match_on(t)+1500)];
        
        dbins{16} = [dbins{16} d(match_on(t)+1500:match_on(t)+1750)];
        
        dbins{17} = [dbins{17} d(match_on(t)+1750:1900)];
        
        dbins{18} = [dbins{18} d(match_on(t)+1900:end)];
    end
    
    
    
    for db = 1 : 18
        allcm(pair_counter,db) = circ_mean(dbins{db}');%calculate response distribution
    end
    
    % %         cc = 0;
    % %         allh = zeros(window,nw);
    % %         for ww = steps
    % %             cc = cc + 1;
    % %             allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
    % %         end
    % %
    % %         allhist = hist(allh,bins) ./ window; %calculate response distribution
    % %         re  = -1 * nansum(allhist.*log2(allhist));
    % %         resp_ent = [resp_ent (max_ent - re) ./ max_ent];
    % %
    % %
    % %
    % %          ea = [];
    % %         eb = [];
    % %         eoffset = [];
    % %         for x = 1 : size(allhist,2)
    % %             [hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',smooth(allhist(:,x),3) .* -1);
    % %
    % %             ab_ratio(x) = estimates(2)/estimates(1);
    % %
    % %             ea(x) = estimates(1);
    % %             eb(x) = estimates(2);
    % %             eoffset(x) = estimates(3);
    % %
    % %             %                     plot(hkb_function,'r')
    % %             %                     hold on;plot(smooth(allhist(:,x),3).*-1,'b')
    % %             %                     pause
    % %             %                     cla
    % %         end
    % %
    % %         allea = [allea ea];
    % %
    % %
    % %
    % %     allallresp_ent{xx} = resp_ent;
    % %     allallea{xx} = allea;
    % %     allallcm{xx} = allcm;
end



% [wcoeff,score,latent,tsquared,explained] = pca(alleas,'VariableWeights','variance');
a = allcm';
[wcoeff,score,latent,tsquared,explained] = pca(a);

% % % hold on
% % % scatter3(score(1:3,1),score(1:3,2),score(1:3,3),100,'r','fill','s')
% % % hold on
% % % scatter3(score(4:5,1),score(4:5,2),score(4:5,3),100,'g','fill','s')
% % % hold on
% % % scatter3(score(6:9,1),score(6:9,2),score(6:9,3),100,'b','fill','s')
% % % hold on
% % % scatter3(score(10:11,1),score(10:11,2),score(10:11,3),100,'m','fill','s')
% % % hold on
% % % scatter3(score(12:18,1),score(12:18,2),score(12:18,3),100,'k','fill','s')
% % % hold on
% % % plot3(score(:,1),score(:,2),score(:,3))

allscores{xxx} = score;
allwcoeff{xxx} = wcoeff;
allallcm{xxx} = a;

end

save allscores allscores allwcoeff allallcm







%% All trials

chs = [201 248;201 233;201 235;201 209;201 246; 201 255; 233 248; 233 235; 209 246; 5 32; 18 248; 18 201];
pair_counter = 0;
allcm = [];
for xx = 1:6%12
    pair_counter = pair_counter + 1;
    
    ch1 = find(N.gridPos == chs(xx,1));
    ch2 = find(N.gridPos == chs(xx,2));
    
    dbins = cell(1,18);
    counter = 0;
    for t = tr
        counter = counter + 1;
        trial = [sesdir filesep lfpdata(t).name(1:(end-9)) lfpdata(t).name((end-4):end)];
        
        [orig_data.rawdata,~,orig_data.samplingRate]=nptReadStreamerFile(trial);
        
        %lowpass
        hdata = nptLowPassFilter(orig_data.rawdata(neuronalCh,:),orig_data.samplingRate,bandpasslow,bandpasshigh);
        
        %get data ranges
        % % % %     datarange = [sample_on(t) - 499 : size(hdata,2)]; %%%MAKE SURE TO ACCOUNT FOR THIS IF LOOKING AT SPIKE TIMES!!!!!!!!!!!!!!!1
        bpdata = hdata; %use whole trial!
        
        data = bpdata'; %need to do this before and after using hilbert, don't include during the transform
        %compute hilbert transform
        data = hilbert(data);
        
        %DO NOT USE THE (') TO TRANSPOSE THE DATA BEFORE FIND THE ANGLE, SIGN OF IMAGINARY
        %COMPENT IS FLIPPED
        %find instantaneous phase angles
        data = angle(data);
        data = data';
        
        d1 = unwrap(data(ch1,:));
        d2 = unwrap(data(ch2,:));
        
        %cyclic relative phase
        d = mod(d1 - d2,2*pi);
        
        
        dbins{1} = [dbins{1} d(1:sample_on(t)-500)]; %prepresample
        
        dbins{2} = [dbins{2} d(sample_on(t)-500:sample_on(t) - 250)]; %presample
        
        dbins{3} = [dbins{3} d(sample_on(t)-250:sample_on(t))]; %presample
        
        dbins{4} = [dbins{4} d(sample_on(t):sample_on(t)+250)]; %sample
        
        dbins{5} = [dbins{5} d(sample_on(t)+250:sample_on(t)+500)]; %sample
        
        dbins{6} = [dbins{6} d(sample_off(t):sample_off(t)+250)];
        
        dbins{7} = [dbins{7} d(sample_off(t)+250:sample_off(t)+500)];
        
        dbins{8} = [dbins{8} d(sample_off(t)+500:sample_off(t)+750)];
        
        dbins{9} = [dbins{9} d(sample_off(t)+750:sample_off(t)+1000)];
        
        dbins{10} = [dbins{10} d(match_on(t):match_on(t)+250)];
        
        dbins{11} = [dbins{11} d(match_on(t)+250:match_on(t)+500)];
        
        dbins{12} = [dbins{12} d(match_on(t)+500:match_on(t)+750)];
        
        dbins{13} = [dbins{13} d(match_on(t)+750:match_on(t)+1000)];
        
        dbins{14} = [dbins{14} d(match_on(t)+1000:match_on(t)+1250)];
        
        dbins{15} = [dbins{15} d(match_on(t)+1250:match_on(t)+1500)];
        
        dbins{16} = [dbins{16} d(match_on(t)+1500:match_on(t)+1750)];
        
        dbins{17} = [dbins{17} d(match_on(t)+1750:1900)];
        
        dbins{18} = [dbins{18} d(match_on(t)+1900:end)];
    end
    
    
    
    for db = 1 : 18
        allcm(pair_counter,db) = circ_mean(dbins{db}');%calculate response distribution
    end
    
    % %         cc = 0;
    % %         allh = zeros(window,nw);
    % %         for ww = steps
    % %             cc = cc + 1;
    % %             allh(:,cc) = d(ww-((window/2)-1):ww+(window/2));%calculate response distribution
    % %         end
    % %
    % %         allhist = hist(allh,bins) ./ window; %calculate response distribution
    % %         re  = -1 * nansum(allhist.*log2(allhist));
    % %         resp_ent = [resp_ent (max_ent - re) ./ max_ent];
    % %
    % %
    % %
    % %          ea = [];
    % %         eb = [];
    % %         eoffset = [];
    % %         for x = 1 : size(allhist,2)
    % %             [hkb_function, empirical_hkb, estimates, sse] = hkb_fmin('pp_dist',smooth(allhist(:,x),3) .* -1);
    % %
    % %             ab_ratio(x) = estimates(2)/estimates(1);
    % %
    % %             ea(x) = estimates(1);
    % %             eb(x) = estimates(2);
    % %             eoffset(x) = estimates(3);
    % %
    % %             %                     plot(hkb_function,'r')
    % %             %                     hold on;plot(smooth(allhist(:,x),3).*-1,'b')
    % %             %                     pause
    % %             %                     cla
    % %         end
    % %
    % %         allea = [allea ea];
    % %
    % %
    % %
    % %     allallresp_ent{xx} = resp_ent;
    % %     allallea{xx} = allea;
    % %     allallcm{xx} = allcm;
end



% [wcoeff,score,latent,tsquared,explained] = pca(alleas,'VariableWeights','variance');
a = allcm';
[wcoeff,score,latent,tsquared,explained] = pca(a);

% % % hold on
% % % scatter3(score(1:3,1),score(1:3,2),score(1:3,3),100,'r','fill','s')
% % % hold on
% % % scatter3(score(4:5,1),score(4:5,2),score(4:5,3),100,'g','fill','s')
% % % hold on
% % % scatter3(score(6:9,1),score(6:9,2),score(6:9,3),100,'b','fill','s')
% % % hold on
% % % scatter3(score(10:11,1),score(10:11,2),score(10:11,3),100,'m','fill','s')
% % % hold on
% % % scatter3(score(12:18,1),score(12:18,2),score(12:18,3),100,'k','fill','s')
% % % hold on
% % % plot3(score(:,1),score(:,2),score(:,3))



save allscores_alltrials score wcoeff a

