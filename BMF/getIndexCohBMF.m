function data = getIndexCohBMF(files,varargin)
% Index(:,1) = pair number
% Index(:,2) = day number

%%
Args = struct('fband',[13 30;30 42; 7 13; 3 7; 42 80],'mvAvg',3,'sigLevel',4);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});
sdir = pwd;
warning off
bands = {'beta' 'gamma' 'alpha' 'theta' 'highgamma'};
%% temporary solution
data = struct('betapos34',[],...
    'betapos14',[],...
    'betapos12',[],...
    'betapos23',[],...
    'betaneg34',[],...
    'betaneg14',[],...
    'betaneg12',[],...
    'betaneg23',[],...
    'gammapos34',[],...
    'gammapos14',[],...
    'gammapos12',[],...
    'gammapos23',[],...
    'gammaneg34',[],...
    'gammaneg14',[],...
    'gammaneg12',[],...
    'gammaneg23',[],...
    'alphapos34',[],...
    'alphapos14',[],...
    'alphapos12',[],...
    'alphapos23',[],...
    'alphaneg34',[],...
    'alphaneg14',[],...
    'alphaneg12',[],...
    'alphaneg23',[]);
%%
for bb = 1 : 4
    eval(sprintf('data.%sPeak = nan(size(files,1),4);',bands{bb}));
    eval(sprintf('data.%sPhase = nan(size(files,1),4);',bands{bb}));
end

sdir = pwd;
for ff = 1 : size(files,1)
    cd(sdir)
    load(files(ff).name)
    limitB = find(f>= Args.fband(1,1) & f<= Args.fband(1,2));
    limitG = find(f>= Args.fband(2,1) & f<= Args.fband(2,2));
    limitA = find(f>= Args.fband(3,1) & f<= Args.fband(3,2));
    limitT = find(f>= Args.fband(4,1) & f<= Args.fband(4,2));
     limitH = find(f>= Args.fband(5,1) & f<= Args.fband(5,2));
    Flimit = [limitB(1) limitB(end); limitG(1) limitG(end); limitA(1) limitA(end); limitT(1) limitT(end); limitH(1) limitH(end)];
    sur = load('cohMTSgenSur.mat');
    % the next line simply use the matlab toolbox findpeak fct
    fcts = which('findpeaks','-all');thefct = strfind(fcts,sprintf('toolbox%ssignal%ssignal%sfindpeaks.m',filesep,filesep,filesep)); for nf =  1 : length(thefct); if ~isempty(thefct{nf}); tfn = nf; end; end; pdir = pwd;cd(fcts{tfn}(1:end-11));
    for p = 1 : 4
        
        smCoh = filtfilt(repmat(1/Args.mvAvg,1,Args.mvAvg),1,C{p}); % smoothing of the spectrum
        surmod = 100 * (smCoh - sur.Prob{p}(:,Args.sigLevel)) ./ sur.Prob{p}(:,Args.sigLevel);
        sigcoh = smCoh > sur.Prob{p}(:,Args.sigLevel); % get the significant coherences indices
        smCoh = smCoh .* sigcoh; % only keeps the significant coh
        phase = phi{p} .* sigcoh; % only keeps the phase with significant coh
        surmod = surmod .* sigcoh;
        for b = 1 : size(Flimit,1)
            range = [Flimit(b,1)-1 : Flimit(b,2)+1] ; % freq range
            [lmax,locs] = findpeaks(smCoh(range)); % get the peaks
            
            if length(locs) > 1
                [~,thef] = max(lmax);
                locs = locs(thef);
            end
            if ~isempty(locs)
                eval(sprintf('data.%sPeak(ff,p) = f(range(1) + locs - 2);',bands{b}));
                
                eval(sprintf('data.%sPhase(ff,p) = mod(2*pi + mean(phase(range(2:end-1))),2*pi);',bands{b}));
                eval(sprintf('data.%sSurMod(ff,p) = mean(surmod(range(2:end-1)));',bands{b}));
                eval(sprintf('data.%sMeanCoh(ff,p) = mean(smCoh(range(2:end-1)));',bands{b}));
            end
        end
     end
end
cd(sdir)
data.hist = nan(size(files,1),1);

%% comaprisons between windows
% lwind = [3 4; 1 4; 1 2; 2 3; 1 3; 2 4];
% comparison = {'pos' 'neg'};
% for comp = 1 : 2
%     for w = 1 : size(lwind,1)
%         eval(sprintf('data.%s%s%d%d = zeros(size(CHcomb,1),1);',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
%         for rule = 1 : 2
%             if b == size(Flimit,1)
%                 [spairs,ntot,index] = selectPairs('data',list,'range',{bands{b}},'rule',rule,modvarargin{:});
%                 selpairs{rule} = index.npair;
%                 tf = ismember(CHcomb,index.ch,'rows');
%                 if ~isempty(index.npair) && sum(tf) ~= 0; for row = 1 : length(index.npair); Index(find(tf == 1),26) = rule; end; end
%
%             end
%                                                         else
%             [spairs,ntot,index] = selectPairs('data',list,'comparison',{comparison{comp}},'range',{bands{b}},'windows',lwind(w,:),'rule',rule,modvarargin{:});
%             selpairs{rule} = index.npair;
%             tf = ismember(CHcomb,index.ch,'rows');
%             if ~isempty(index.npair) && sum(tf) ~= 0;
%                 for row = 1 : length(index.npair);
%                     if (b == 1 && w == 1) || (b ==2 && w == 2);  Index(find(tf ==1),(b-1)*2+ comp + 2) = rule; end
%                     eval(sprintf('data.%s%s%d%d(find(tf == 1)) = rule;',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
%                 end;
%             end
%
%                                 end
%         end
%         tracking = [tracking [b; comp; w; (b-1)*8+ (comp-1) *4 + w + 2]];
%         bothrule = intersect(selpairs{1},selpairs{2});
%         if ~isempty(bothrule);
%             for pp = 1 : length(bothrule);
%                 tf = ismember(CHcomb,Day.comb(bothrule(pp),:),'rows');
%                 if sum(tf) ~= 0;
%                     if b < size(Flimit,1) && w < 3;
%                         Index(find(tf == 1),(b-1)*2+ comp + 2) = 3;
%
%                     end;
%                     eval(sprintf('data.%s%s%d%d(find(tf == 1)) = 3;',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
%                 end;
%             end;
%         end
%     end
% end
% end
