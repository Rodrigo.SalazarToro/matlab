function bmf_coherence_distance_power_relationship(varargin)


% cd('/media/bmf_raid/data/monkey/ethyl/')
%
% % allbmf = ProcessDays(bmfCohgram,'days',alldays,'ml','NoSites','Sites','session01');
% % save allbmf allbmf
%
%
% load allbmf allbmf

Args = struct('raw',0,'trialpower',0);
Args.flags = {'raw','trialpower'};
Args = getOptArgs(varargin,Args);

cd('/Volumes/bmf_raid/data/monkey/ethyl')
load alldays
mdir = pwd;
for d = 1:5
    cd([mdir filesep alldays{d} filesep 'session01'])
    
    sesdir = pwd;
    
    if Args.trialpower
        
        sesdir = pwd;
        mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
        identity = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
        
        g = bmf_groups;
        N = NeuronalHist('bmf');
        
        [realch,good_ch] = intersect(N.gridPos,g);
        ch = [1 : size(N.gridPos,2)];
        chnumb = size(ch,2);
        cd([sesdir filesep 'lfp' filesep 'power'])
        
        powertrials = nptDir('*power.*');
        
        %make data matrix
        trial_power = cell(chnumb,4);
        for nt = identity
            load(powertrials(nt).name)
            for ep = 1 : 4 %only run through the first 6 epochs, uneven results for the full trial
                for c = 1 : chnumb
                    if nt == identity(1)
                        trial_power{c,ep} = power.S{ep}(ch(c),:);
                    else
                        trial_power{c,ep} = [trial_power{c,ep}; power.S{ep}(ch(c),:)];
                    end
                end
            end
        end
        save trial_power trial_power
    end
    
    cd([sesdir filesep 'lfp' filesep 'power'])
    load trial_power trial_power
    m = cellfun(@mean,trial_power,'UniformOutput',0);
    cd(sesdir)
    g = bmf_groups;
    N = NeuronalHist('bmf');
    [~,ig] = intersect(N.gridPos,g);
    
    allbmf = bmfCohgram('auto')
    
    amean = allbmf.data.Index(:,23);
    bmean = allbmf.data.Index(:,27);
    gmean = allbmf.data.Index(:,31);
    hgmean = allbmf.data.Index(:,35);
    
    
    gridpos1 = allbmf.data.Index(:,1);
    gridpos2 = allbmf.data.Index(:,2);
    
    grid = [ ...
        
    0, 0,  0,  0,  0,  0,  50, 61, 72, 83, 94,  105, 116, 127, 138, 149, 160, 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0;
    0, 0,  0,  0,  30, 40, 51, 62, 73, 84, 94,  106, 117, 128, 139, 150, 161, 171, 181, 191, 0,   0,   0,   0,   0,   0,   0,   0;
    0, 0,  0,  21, 31, 41, 52, 63, 74, 85, 96,  107, 118, 129, 140, 151, 162, 172, 182, 192, 201, 210, 0,   0,   0,   0,   0,   0;
    0, 0,  13, 22, 32, 42, 53, 64, 75, 86, 97,  108, 119, 130, 141, 152, 163, 173, 183, 193, 202, 211, 219, 227, 0,   0,   0,   0;
    0, 6,  14, 23, 33, 43, 54, 65, 76, 87, 98,  109, 120, 131, 142, 153, 164, 174, 184, 194, 203, 212, 220, 228, 235, 0,   0,   0;
    0, 7,  15, 24, 34, 44, 55, 66, 77, 88, 99,  110, 121, 132, 143, 154, 165, 175, 185, 195, 204, 213, 221, 229, 236, 242, 0,   0;
    1, 8,  16, 25, 35, 45, 56, 67, 78, 89, 100, 111, 122, 133, 144, 155, 166, 176, 186, 196, 205, 214, 222, 230, 237, 243, 248, 0;
    2, 9,  17, 26, 36, 46, 57, 68, 79, 90, 101, 112, 123, 134, 145, 156, 167, 177, 187, 197, 206, 215, 223, 231, 238, 244, 249, 253;
    3, 10, 18, 27, 37, 47, 58, 69, 80, 91, 102, 113, 124, 135, 146, 157, 168, 178, 188, 198, 207, 216, 224, 232, 239, 245, 250, 254;
    4, 11, 19, 28, 38, 48, 59, 70, 81, 92, 103, 114, 125, 136, 147, 158, 169, 179, 189, 199, 208, 217, 225, 233, 240, 246, 251, 255;
    5, 12, 20, 29, 39, 49, 60, 71, 82, 93, 104, 115, 126, 137, 148, 159, 170, 180, 190, 200, 209, 218, 226, 234, 241, 247, 252, 256];


xy = [];
cch = 0;
for column = 1:28
    for row = 1:11
        if grid(row,column) ~= 0
            cch = cch +1;
            xy(cch,1) = row;
            xy(cch,2) = column;
        end
    end
end

[i ii] = get(allbmf,'Number');
scolors = {'r','g','b','k'};
bmfcoh = [19 23 27 31];
for fr  = 1:4
    if fr ==1
        fr_range = 2:8;
    elseif fr == 2
        fr_range = 9:15;
    elseif fr == 3
        fr_range = 16:22;
    else
        fr_range = 23:59;
    end
    for e = 1:4
        counter = 0;
        for x = ii
            counter = counter + 1;
            edist(counter) = sqrt((xy(gridpos1(x),1) - xy(gridpos2(x),1))^2 + (xy(gridpos1(x),2) - xy(gridpos2(x),2))^2);
            meancoh(counter) = allbmf.data.Index(x,bmfcoh(e)+fr);
        end
        
        s = fitoptions('Method','NonlinearLeastSquares','Lower',[0,0],'Upper',[Inf,max(edist)],'Startpoint',[1 1]);
        ft = fittype('a+(x)^-b','options',s);
        [fit1,gof,fitinfo] = fit(edist',meancoh',ft);
        
        counter = 0;
        for x = ii
            counter = counter + 1;
            edist(counter) = sqrt((xy(gridpos1(x),1) - xy(gridpos2(x),1))^2 + (xy(gridpos1(x),2) - xy(gridpos2(x),2))^2);
            meancoh(counter) = allbmf.data.Index(x,bmfcoh(e)+fr);
            %calculate the diff in power percentage
            [~,ig1] = intersect(N.gridPos,gridpos1(x));
            [~,ig2] = intersect(N.gridPos,gridpos2(x));
            
            normp1 =  (m{ig1,e}(1:59) .* linspace(0,90.6250,59)) ./ sum(m{ig1,e}(1:59).*linspace(0,90.6250,59));
            normp1_band =  single(sum(normp1(fr_range)));%12.5 : 21.9
            
            normp2 =  (m{ig2,e}(1:59) .* linspace(0,90.6250,59)) ./ sum(m{ig2,e}(1:59).*linspace(0,90.6250,59));
            normp2_band =  single(sum(normp2(fr_range)));%12.5 : 21.9
            
            spectral_perc(ig1) = normp1_band ;%/ (normp1_band + normp2_band);
            spectral_perc(ig2) = normp2_band;% / (normp1_band + normp2_band);
            
            if edist(counter) > 0
                meancoh_ch{ig1}(counter) = meancoh(counter) - fit1.a+(edist(counter).^-fit1.b);
                meancoh_ch{ig2}(counter) = meancoh(counter) - fit1.a+(edist(counter).^-fit1.b);
%                 
%                 meancoh_ch{ig1}(counter) = meancoh(counter) - (edist(counter).^-1.5);
%                 meancoh_ch{ig2}(counter) = meancoh(counter) - (edist(counter).^-1.5);
            end
        end
        
        
        
        
        for x = 1:size(meancoh_ch,2)
            if ~isempty(meancoh_ch{x})
                mc = meancoh_ch{x};
                mc(find(mc == 0)) = [];
                
                med_mc = median(mc);
                subplot(5,4,fr+(4*(d-1)))
                scatter((spectral_perc(x)),(med_mc),10,'s','fill',scolors{e})
                hold on
            end
        end
    end
end
end

scolors;

