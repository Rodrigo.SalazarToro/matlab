
load idedays
parfor d = 1 : 29;
    cd(idedays{d});
    if isempty(nptDir('skip.txt'));
        %         cd session01
        sessions = nptDir('session0*');
        for s = 2 : length(sessions)
            cd(sessions(s).name)
            [status, result] = unix('find . -name ''skip.txt''');
            
            if isempty(result)
                cd lfp
                files = [nptDir('*.0*'); nptDir('*.1*'); nptDir('*.2*')];
                
                [~,num_channels,~,~,~]=nptReadStreamerFile(files(1).name);
                cd ..
                [data,lplength] = lfpPcut([1:length(files)],[1:num_channels],'savelfpPcut','EPremoved');
                %                 makeFourierTransformSurrogate
            end
            cd ..
        end
    end
    cd ..
end
