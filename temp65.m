locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [PREmugramLcos{lc},time,f,ntrials,sig,PREPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('PRElocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.1 0.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [SAmugramLcos{lc},time,f,ntrials,sig,SAPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('SAlocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.6 0.9],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE1mugramLcos{lc},time,f,ntrials,sig,DE1PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE1locSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE2mugramLcos{lc},time,f,ntrials,sig,DE2PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE2locSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.405 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DEmugramLcos{lc},time,f,ntrials,sig,DEPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','IdePerLoc','plot','NodelaySelection','Interpol','addNameFile',sprintf('DElocSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [PREmugramLcos{lc},time,f,ntrials,sig,PREPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('PREideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.1 0.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [SAmugramLcos{lc},time,f,ntrials,sig,SAPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('SAideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[0.6 0.9],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE1mugramLcos{lc},time,f,ntrials,sig,DE1PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE1ideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.4],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DE2mugramLcos{lc},time,f,ntrials,sig,DE2PairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DE2ideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.405 1.705],'redo','save');
end

locs = {'max' 'inter' 'min'};
for lc = 1 : 3
    [DEmugramLcos{lc},time,f,ntrials,sig,DEPairsLcos{lc},presample,msur,prefide,diffide,prefidePhase,~,prefidePhaseSTD,prefideS] = mkAveMugram(obj,ind,'type','LocPerIde','plot','NodelaySelection','Interpol','addNameFile',sprintf('DEideSel%dBias',lc),'getPrefStim','locSel',locs{lc},'noStimRanked','onlySigCohPairs','delayRange',[1.1 1.705],'redo','save');
end

load migramIdePerLocRule1prctile98PRElocSel1Bias.mat
length(find(sum(sigcondStim,2) ~= 0))

%% object selectivity
epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramIdePerLocRule1prctile98%slocSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end

DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);
DEsigCohMI = unique([sigCohMI{3,1}; sigCohMI{3,2}; sigCohMI{3,3}; sigCohMI{4,1}; sigCohMI{4,2}; sigCohMI{4,3}]);
DEsigMI{1} = [sigCohMI{3,1}' sigCohMI{3,2}' sigCohMI{3,3}'];
DEsigMI{2} = [sigCohMI{4,1}' sigCohMI{4,2}' sigCohMI{4,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{3,1})) + 1 zeros(1,length(sigCohMI{3,2})) + 2 zeros(1,length(sigCohMI{3,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{4,1}),1) + 1 zeros(1,length(sigCohMI{4,2})) + 2 zeros(1,length(sigCohMI{4,3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramIdePerLocRule1prctile98DE1locSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 load('migramIdePerLocRule1prctile98DE2locSel1Bias.mat')
  csigcondStim = csigcondStim + sigcondStim;
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))

smugram = [];
ExpVgram = [];
coherenceStim = cell(3,1);
VcoherenceStim = cell(3,1);
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramIdePerLocRule1prctile98DElocSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    for stim = 1 : 3
    coherenceStim{stim} = cat(2,coherenceStim{stim},mean(prefide(stim,uselpairLocs(2,locpairs),:,5:8),4));
    VcoherenceStim{stim} = cat(2,VcoherenceStim{stim},circ_mean(prefidePhaseSTD(stim,uselpairLocs(2,locpairs),:,5:8),[],4));
    end
end

biasmugram = smugram - ExpVgram; 
figure; 
subplot(4,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])


subplot(4,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),[],1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1050 1.7550])
 xlabel('Time (s)')
ylabel('MI (arb. units)')

title('Mean MI and SEM between 12-22Hz')
subplot(4,1,1)

ylabel('Frequency (Hz)')
xlim([0.1050 1.7550])
title('MI for pairs selective for the object at any location during the delay (10-42 Hz); n=450')

subplot(4,1,3)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(mean(coherenceStim{stim},2));
    ste = squeeze(std(coherenceStim{stim},[],2)) / sqrt(size(coherenceStim{stim},2));
    ste2 = squeeze(std(coherenceStim{stim},[],2)) / sqrt(67);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1050 1.7550])
title('Mean coherence for the 3 stimuli') 
ylabel('Coherence')
subplot(4,1,4)
stimlabel = {'r' 'b' 'k'};
for stim = 1 : 3
    me = squeeze(circ_mean(VcoherenceStim{stim},[],2));
    ste = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(size(VcoherenceStim{stim},2));
    ste2 = squeeze(circ_std(VcoherenceStim{stim},[],[],2)) / sqrt(67);
    plot(time,me,stimlabel{stim})
    hold on
    plot(time,me + ste,[stimlabel{stim} '--'])
    plot(time,me + ste2,[stimlabel{stim} '--'])
    plot(time,me - ste,[stimlabel{stim} '--'])
    plot(time,me - ste2,[stimlabel{stim} '--'])
end
xlim([0.1050 1.7550])
title('Mean std of the relative phase for the 3 stimuli') 
ylabel('STD of the phase')
%% location selectivity
epochs = {'PRE' 'SA' 'DE1' 'DE2'};
locs = {'max' 'inter' 'min'};


nsigCoh = zeros(4,1);
nsigCohMI = zeros(4,1);
sigCoh = cell(4,3);
sigCohMI = cell(4,3);

for ep = 1 : 4
    tMI = [];
    for lc = 1 : 3
        load(sprintf('migramLocPerIdeRule1prctile98%sideSel%dBias.mat',epochs{ep},lc))
        row = find(sigcondStim(:,lc) > 11);
        row = unique(row);
        sigCoh{ep,lc} = row;
        
        tMI = [tMI;  thepairs];
        sigCohMI{ep,lc} =  thepairs;
        
    end
    nsigCohMI(ep) = length(unique(tMI));
    nsigCoh(ep) = length(unique([sigCoh{ep,1}; sigCoh{ep,2}; sigCoh{ep,3}]));
end

DEsigCoh = unique([sigCoh{3,1}; sigCoh{3,2}; sigCoh{3,3}; sigCoh{4,1}; sigCoh{4,2}; sigCoh{4,3}]);
DEsigCohMI = unique([sigCohMI{3,1}; sigCohMI{3,2}; sigCohMI{3,3}; sigCohMI{4,1}; sigCohMI{4,2}; sigCohMI{4,3}]);
DEsigMI{1} = [sigCohMI{3,1}' sigCohMI{3,2}' sigCohMI{3,3}'];
DEsigMI{2} = [sigCohMI{4,1}' sigCohMI{4,2}' sigCohMI{4,3}'];
thelocs{1} = [zeros(1,length(sigCohMI{3,1})) + 1 zeros(1,length(sigCohMI{3,2})) + 2 zeros(1,length(sigCohMI{3,3})) + 3]; 
thelocs{2} = [zeros(1,length(sigCohMI{4,1}),1) + 1 zeros(1,length(sigCohMI{4,2})) + 2 zeros(1,length(sigCohMI{4,3})) + 3]; 

selpairLocs = unique([DEsigMI{1} DEsigMI{2}; thelocs{1} thelocs{2}],'rows');



 load('migramLocPerIdeRule1prctile98DE1ideSel1Bias.mat')
 
 csigcondStim = sigcondStim;
 load('migramLocPerIdeRule1prctile98DE2ideSel1Bias.mat')
  csigcondStim = csigcondStim + sigcondStim;
  
  [indii,m,n] = unique(selpairLocs(2,:));

 uselpairLocs = zeros(2,length(m));
for ii = 1 : length(m)
    
    slocs = selpairLocs(1,find(n == ii));
    [~,thegoodloc] = max(csigcondStim(indii(ii),slocs));
    uselpairLocs(1,ii) = slocs(thegoodloc);
    uselpairLocs(2,ii) = indii(ii);
end

sum(ismember(uselpairLocs',selpairLocs','rows'))

smugram = [];
ExpVgram = [];
for loc = 1 : 3
    locpairs = find(uselpairLocs(1,:) == loc);
    load(sprintf('migramLocPerIdeRule1prctile98DE2ideSel%dBias.mat',loc))
    
    
    smugram = cat(1,smugram,mugram(uselpairLocs(2,locpairs),:,:));
    ExpVgram = cat(1,ExpVgram,alphaExp(uselpairLocs(2,locpairs),:,:));
    
end

biasmugram = smugram - ExpVgram; 
figure; 
subplot(2,1,1);
imagesc(time,f,squeeze(mean(biasmugram,1))');
colorbar
caxis([-0.02 0.17])
title('MI for pairs selective for the location using any object during the delay (10-42 Hz); n=450')
subplot(2,1,2)
themean = squeeze(nanmean(nanmedian(biasmugram(:,:,5:8),3),1));
thestd = squeeze(nanstd(nanmedian(biasmugram(:,:,5:8),3),1));
ste = thestd / sqrt(size(biasmugram,1));
ste2 = thestd / sqrt(67);
plot(time,themean)
hold on
 plot(time,themean+ste,'--')
 plot(time,themean-ste,'--')
plot(time,themean+ste2,'--')
 plot(time,themean-ste2,'--')
 xlim([0.1050 1.7550])
 xlabel('Time (s)')
ylabel('MI (arb. units)')
subplot(2,1,1)

ylabel('Frequency (Hz)')
xlim([0.1050 1.7550])

title('Mean MI and SEM between 12-22Hz')











































%%
figure
 ntime = [0:0.02:1.8];
for lc = 2 : 3
    
    load(sprintf('migramIdePerLocRule1prctile95wTriallocSel%dBias.mat',lc))
    subplot(3,2,(lc-1)*2+1)
     nf = [0 :50];
   mugram = mugram - alphaExp;
   [row,col] = find(sigcondStim(:,lc) > 24);
   thedata = squeeze(nanmean(mugram(row,:,:),1));
   thedata = interp2(f,time,thedata,nf,ntime');
   imagesc(ntime,nf,thedata');
   xlim([0.11 1.81])
   caxis([-0.02 0.06])
   title(sprintf('location %d; n=%d with sign. coh.',lc,length(row)))
   colorbar
   
   betaFreq = find(f >= 10 & f <= 42);
   %
   seltime = find(time >= 0.1 & time <= 1.7);
   sigpair = zeros(size(sig,1),1);
   for pp = 1 : size(sig,1)
       [row,col] = find(squeeze(sig(pp,seltime,betaFreq)) == 1);
       [nu,~] = hist(row,length(unique(row)));
       adjrow = sum(nu(nu > 2));
       [nu,~] = hist(col,length(unique(col)));
       adjcol = sum(nu(nu > 2));
       sigpair(pp) = adjcol + adjrow;
       %             sigpair(pp) = adjrow;
   end
   thepairs = find(sigpair > 0); % get the pairs with higher number of significant bins than expected by chance
   
   thepairs = intersect(thepairs,find(sigcondStim(:,lc) > 24));
   %
   
   
   
   subplot(3,2,(lc-1)*2+2)
   row = intersect(find(sigcondStim(:,lc) > 24),thepairs);
   thedata = squeeze(nanmean(mugram(row,:,:),1));
   thedata = interp2(f,time,thedata,nf,ntime');
   imagesc(ntime,nf,thedata');
   xlim([0.11 1.81])
   caxis([-0.03 0.2])
   title(sprintf('n=%d with sign. MI',length(row)))
   colorbar
end

ylabel('Frequency (Hz)')
xlabel('Time (s)')