% Index(:,1) = pair number
% Index(:,2) = day number
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% all of the above is either 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules

% Index(:,7) = beta differences between the two rules (0 for no, 1 for delay 1, 2 for delay 2 and 3 for both)
% Index(:,8) = gamma differences between the two rules (0 for no, 1 for
% fix, 2 for delay 2 and 3 for both)
% Index(:,9) = histology (1: PPCm-PFCd; 2: PPCm-PFCv; 3 PPCl-PFCd;
% 4 : PPCl-PFCv; 5: PPCs-PFCd; 6: PPCs-PFCv; 7: PPCm-PFCs; 8: PPCl-PFCs)
%
% Index(:,10:11) = group number
% Index(:,12) = IDENTITY rule, no tuning (0), tuning for the
% identity (1) or the location (2) or (3) both location and idenities tuning of the sample only for the beta
% range of course
% Index(:,13) = LOCATION rule, no tuning (0), tuning for the
% identity (1) or the location (2) of the sample only for the beta
% range of course
% Index(:,14) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 3
% Index(:,15) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 4
% Idex(:,16) ide tuning index during IDE rule for window 4 PP
% Idex(:,17) ide tuning index during LOC rule for window 4 PP
% Idex(:,18) loc tuning index during IDE rule for window 4 PP
% Idex(:,19) loc tuning index during LOC rule for window 4 PP
% Idex(:,20) ide tuning index during IDE rule for window 4 PF
% Idex(:,21) ide tuning index during LOC rule for window 4 PF
% Idex(:,22) loc tuning index during IDE rule for window 4 PF
% Idex(:,23) loc tuning index during LOC rule for window 4 PF

load switchdays.mat
days = switchdays;
% cohInter = ProcessDays(mtscohInter,'auto','days',{days{:}},'sessions',{'session01'},'NoSites');
cohInter = ProcessDays(mtscohInter,'auto','days',{days{:}},'sessions',{'session01'},'NoSites','NoHisto');
index = cohInter.data.Index;
clear n
rulesind = {[16 18; 20 22] [17 19; 21 23]};
therules = {'IDENTITY' 'LOCATION'};
tune = {'obj' 'locs'};
maxy = [];
clear h
for r = 1 : 2
    h(r) = figure;
    set(h(r),'Name',therules{r})
    ind = find(index(:,3) ~= 0);%1 | index(:,3) == 2 | index(:,3) == 3);
    %     ind = [1:size(index,1)];
    length(ind);
    for sb = 1 : 2
        subplot(2,1,sb)
        [n(:,1),xout] = hist(index(ind,rulesind{r}(1,sb)),[0:0.05:0.8]);
        hold on
        [n(:,2),xout] = hist(index(ind,rulesind{r}(2,sb)),[0:0.05:0.8]);
        bar(xout,n)
        maxy = [maxy max(max(n))];
        title(sprintf('%s tuning',tune{sb}))
    end
    legend('PP','PF')
    xlabel('tuning index')
    ylabel('Nbr of sites/pair')
end
areas = {'Parietal' 'Frontal'};

for r = 1 : 2;for sb = 1 : 2; figure(h(r));subplot(2,1,sb);axis([ 0 0.8 0 max(maxy)]); end; end
lab = {':' '-.' '--' '-'};

thres = [0.1 -0.1];
dire = {'>' '<'};
for ttt = 1 : 2
    hhh = figure;
    set(hhh,'Name',sprintf('%s preference',tune{ttt}))
    for r = 1 : 2
        for area = 1 : 2
            pref(r,area,:) = index(:,rulesind{r}(area,1)) - index(:,rulesind{r}(area,2));
            %             pref = index(:,rulesind{2}(1,1)) - index(:,rulesind{2}(1,2));
            ind = find(eval(sprintf('pref(r,area,:) %s thres(ttt);',dire{ttt})));
            %             ind = find(index(:,rulesind{r}(area,ttt)) > 0.185 & index(:,rulesind{r}(area,setdiff([1 2],ttt))) < 0.185);
            clear data;for p = 1 : 4; data{p} = [];end
            for i = 1 : length(ind)
                cd(cohInter.data.setNames{ind(i)})
                load cohInter.mat
                mt = ProcessDay(mtstrial,'auto','NoSites');
                if r == mt.data.Index(1,1)
                    s = 1;
                else
                    s = 2;
                end
                for p =  1 : 4; data{p} = [data{p} squeeze(Day.session(s).C(:,index(ind(i),1),:))];end
            end
            subplot(2,2,(r-1)*2 + area)
            if ~isempty(data{4});
                %                 plot(f,prctile(data{4},[10 25 50 75 90],2),lab{4});
                plot(f,mean(data{4},2),lab{4});
                hold on;
                plot(f,mean(data{4},2)+ std(data{4},[],2),'--')
                plot(f,mean(data{4},2)- std(data{4},[],2),'--')
            end
            axis([0 50 0 0.5])
            text(40,0.3,sprintf('n = %d',length(ind))); grid on
            title(areas{area})
            xlabel('Frequency [Hz]')
            ylabel(sprintf('%s; coh',therules{r}))
            
            %         hist(pref(r,area,:),[-0.6: 0.02:0.2])
            %         if r == 1
            %             hist(cohInter.data.betamodIDE(ind),[-100:400]);
            %         else
            %             hist(cohInter.data.betamodLOC(ind),[-100:400]);
            %         end
            
            %         axis([ -0.6 0.2 0 60])
        end
    end
end
legend('10th prctile','25th prctile','50th prctile','75th prctile','90th prctile')

co = {'r' 'k' 'y'};

for in = 1 : 3
figure;
subplot(2,2,1)
[r,ind] = get(coh,'Number','type',{'betapos34' in});
plot(index(:,18),index(:,19),'.'); axis([0 0.8 0 0.8]); hold on; line([0 0.8],[0 0.8]); plot(index(ind,18),index(ind,19),sprintf('%s.',co{in}));
xlabel('IDE rule; loc tuning')
ylabel('LOC rule; loc tuning')
title('Parietal')

subplot(2,2,2)
plot(index(:,22),index(:,23),'.'); axis([0 0.8 0 0.8]); hold on; line([0 0.8],[0 0.8]);plot(index(ind,22),index(ind,23),sprintf('%s.',co{in}));
xlabel('IDE rule; loc tuning')
ylabel('LOC rule; loc tuning')
title('Frontal')

% ind = find(index(:,3) == 1 | index(:,3) == 3);
subplot(2,2,3)
plot(index(:,16),index(:,17),'.'); axis([0 0.8 0 0.8]); hold on; line([0 0.8],[0 0.8]);plot(index(ind,16),index(ind,17),sprintf('%s.',co{in}));
xlabel('IDE rule; ide tuning')
ylabel('LOC rule; ide tuning')
title('Parietal')

subplot(2,2,4)
plot(index(:,20),index(:,21),'.'); axis([0 0.8 0 0.8]); hold on; line([0 0.8],[0 0.8]);plot(index(ind,20),index(ind,21),sprintf('%s.',co{in}));
xlabel('IDE rule; ide tuning')
ylabel('LOC rule; ide tuning')
title('Frontal')

end
figure;
for r = 1 : 2;
    for area = 1 : 2;
        subplot(2,2,(r-1)*2+area); hist(pref(r,area,:),[-0.6 : 0.02 : 0.2]);
        title(areas{area});ylabel(sprintf('%s; # sites',therules{r})); axis([-0.6 0.2 0 60]);
    end;
end


thres = [0.18 -0.18];
dire = {'>' '<'};
revind{1} = [];
revind{2} = [];
for r = 1 : 2
    for area = 1 : 2
        pref = index(:,rulesind{r}(area,1)) - index(:,rulesind{r}(area,2));
        %             pref = index(:,rulesind{2}(1,1)) - index(:,rulesind{2}(1,2));
        revind{1} = [revind{1}; find(eval(sprintf('pref %s thres(r);',dire{r})))];
        revind{2} = [revind{2}; find(eval(sprintf('pref %s thres(setdiff([1 2],r));',dire{setdiff([1 2],r)})))];
        %             ind = find(index(:,rulesind{r}(area,ttt)) > 0.185 & index(:,rulesind{r}(area,setdiff([1 2],ttt))) < 0.185);
    end
end



figure
clear data;for p = 1 : 4; data{p} = [];end
for rv = 1 : 2
    ind = unique(revind{rv});
     
    for i = 1 : length(ind)
        cd(cohInter.data.setNames{ind(i)})
        load cohInter.mat
        mt = ProcessDay(mtstrial,'auto','NoSites');
        if r == mt.data.Index(1,1)
            s = 1;
        else
            s = 2;
        end
        for p =  1 : 4; data{p} = [data{p} squeeze(Day.session(s).C(:,index(ind(i),1),p))];end
    end
    for p = 1 : 4
    subplot(4,1,p)
    if ~isempty(data{p});
        plot(f,prctile(data{p},[50 75 90],2),lab{rv+2});
        %      plot(f,mean(data{4},2),lab{4});
        %      hold on;
        %      plot(f,mean(data{4},2)+ std(data{4},[],2),'--')
        %      plot(f,mean(data{4},2)- std(data{4},[],2),'--')
        hold on
    end
    axis([0 50 0 0.65])
    text(40,0.3*rv,sprintf('n = %d %s',length(ind),lab{rv+2})); grid on
    title(areas{area})
    xlabel('Frequency [Hz]')
    ylabel(sprintf('%s; coh',therules{r}))
    end
end
%         hist(pref(r,area,:),[-0.6: 0.02:0.2])
%         if r == 1
%             hist(cohInter.data.betamodIDE(ind),[-100:400]);
%         else
%             hist(cohInter.data.betamodLOC(ind),[-100:400]);
%         end

%         axis([ -0.6 0.2 0 60])


legend('50th prctile rel','75th prctile rel','90th prctile rel','50th prctile irel','75th prctile irel','90th prctile irel')