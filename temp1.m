% function [distr] = GCdistr(days,varargin)

rules = {'IDENTITY' 'LOCATION'};
epochs = {'Fix' 'Sample' 'Delay 1' 'Delay 2'};
% days = switchDay('ML','combNtrials',150);
for d = 1 : length(days)
    cd(days{d})
    [ch,CHcomb,iflat,groups] = getFlatCh;
    ind = true(size(CHcomb,1),1); ind(iflat.pairs) = false;
    load GCcohInter.mat
    coh = load('cohInter.mat');
    f1 = figure;
    for r = 1 : 2
        for p = 1 : 4
            subplot(2,4,(r-1) * 4 + p)
            %             plot(f,prctile(squeeze(Day.session(r).Fx2y(:,:,p)),[25 50 75 90],2),'b')
            %             hold on
            %             plot(f,prctile(squeeze(Day.session(r).Fy2x(:,:,p)),[25 50 75 90],2),'r')
            %             plot(f,squeeze(Day.session(r).Fx2y(:,:,p)))
            
            
            [AX,H1,H2] = plotyy(f,prctile(squeeze(Day.session(r).Fy2x(:,ind,p)),[50 75 90],2),coh.f,prctile(squeeze(coh.Day.session(r).C(:,ind,p)),[50 75 90],2));
            set(H1,'LineStyle','-')
            set(H2,'LineStyle','--')
            set(get(AX(1),'Ylabel'),'String','F->P GC') 
            set(get(AX(2),'Ylabel'),'String','Coherence')
            hold on
            
            if p == 1; ylabel(sprintf('%s',rules{r})); end
            if r == 1; title(epochs{p}); end
            if r ==2 ; xlabel('Frequency [ Hz]'); end
            %             if p == 4 & r ==2; legend('25th P->F','50th P->F','75th P->F','90th P->F','25th F->P','50th F->P','75th F->P','90th F->P'); end
        end
        legend('50th prctile F->P','75th prctile F->P','90th prctile F->P','50th prctile coh','75th prctile coh','90th prctile coh')
    end
    
    set(f1,'Name',days{d})
    cd ..
end


obj = ProcessDays(mtscohInter,'days',{days{:}},'sessions',{'session01'},'auto','NoSites','NoHisto');

InspectGUI(obj)




windows = {'Fix' 'Sample' 'Delay 1' 'Delay 2'};
mt = mtstrial('auto');
trials = mtsgetTrials(mt,'stable','BehResp',1,'ML','RTfromML');
[data,lplength] = lfpPcut(trials,[1 49]);
tim = [0 :5 : 400];
figure; 
for t = 1 : 345;
    for p = 1 : 4; subplot(4,1,p); 
        plot(tim,squeeze(data{p}(:,t,:))); title(windows{p}); ylabel('Voltage [microVolt]');
    end; xlabel('time [msec]'); legend('Group 1','Group 63');pause; 
end
