[r,ind] = get(GCobj,'Number','snr',99);r
plevel = 3;
count = 1;
clear GC surro
for i = ind
cd(GCobj.data.setNames{i})
load GCcohInter.mat
if Day.session(1).rule == 1
s = 1;
else
s = 2;
end
cmb = GCobj.data.Index(i,1);
GC(1,count,:,:) = Day.session(s).Fx2y(:,cmb,:);
GC(2,count,:,:) = Day.session(s).Fy2x(:,cmb,:);
GC(3,count,:,:) = Day.session(s).Fxy(:,cmb,:);
load GCgeneralSur.mat
surro(1,count,:,:) = Day.session(s).Fx2y(:,plevel,:);
surro(2,count,:,:) = Day.session(s).Fy2x(:,plevel,:);
surro(3,count,:,:) = Day.session(s).Fy2x(:,plevel,:);
count = count + 1;
end
GCsig = GC >= surro;
figure
for dir = 1 : 3; for p = 1 : 4; subplot(3,4,(dir-1)*4+p); plot(sum(squeeze(GCsig(dir,:,:,p)),1)); end; end

epochs = {'presample' 'sample' 'delay 1' 'delay2'};
xlabel('Frequency [Hz]')
ylabel('Number of significant pairs')
subplot(3,4,1); ylabel('PPC -> PFC')
subplot(3,4,5); ylabel('PFC -> PPC')
subplot(3,4,9); ylabel('Inst.')
for sb = 1 : 4; subplot(3,4,sb);title(epochs{sb}); end
for sb = 1 : 12; subplot(3,4,sb);axis([0 50 0 130]); end


figure; 
epoch1 = squeeze(bothMonkeysL.data.IDELocInt(indLoc,1,:))./repmat(max(squeeze(bothMonkeysL.data.IDELocInt(indLoc,1,:)),[],2),1,3);
epoch2 = squeeze(bothMonkeysL.data.IDELocInt(indLoc,4,:))./repmat(max(squeeze(bothMonkeysL.data.IDELocInt(indLoc,4,:)),[],2),1,3);

figure;for d = 1 : size(epoch1,1); R = corrcoef(epoch1(d,:),epoch2(d,:)); cc(d) = R(1,2);end


[r,indObj] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDEObjsig' 1 '>' 0},'sigSur',{'beta' 1 [1 3]});
[r,indLoc] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 0},'sigSur',{'beta' 1 [1 3]});r
unstable = union(indLoc,indObj);length(unstable)/3445
for p = 1 : 4
[r,indObj] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDEObjsig' p '>' 2},'sigSur',{'beta' p [1 3]});
[r,indLoc] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' p '>' 2},'sigSur',{'beta' p [1 3]});

[tot,indtot] = get(bothMonkeysL,'Number','snr',99,'sigSur',{'beta' p [1 3]});

tuned = union(indLoc,indObj);length(tuned)/tot
end

[r,indObj] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 0;'IDEObjsig' 1 '>' 0;'IDEObjsig' [3 4] '>' 1},'sigSur',{'beta' [3 4] [1 3]});r
[r,indLoc] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 0;'IDEObjsig' 1 '>' 0;'IDELocsig' [3 4] '>' 1},'sigSur',{'beta' [3 4] [1 3]});r
[r,indObj2] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 0;'IDEObjsig' 1 '>' 0;'IDEObjsig' [3 4] '>' 1},'sigSur',{'beta' [1] [1 3]});r
[r,indLoc2] = get(bothMonkeysL,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 0;'IDEObjsig' 1 '>' 0;'IDELocsig' [3 4] '>' 1},'sigSur',{'beta' [1] [1 3]});r
indObj = intersect(indObj,indObj2);
indLoc = intersect(indLoc,indLoc2);

betatuned = union(indLoc,indObj);length(betatuned)/3445

[r,indObj] = get(obj,'Number','snr',99,'tuning',{'IDEObjsig' 1 '>' 2});r
[r,indLoc] = get(obj,'Number','snr',99,'tuning',{'IDELocsig' 1 '>' 2});r
unstable = union(indLoc,indObj);length(unstable)/292