cd ~/Desktop/
load example_calcium_data.mat

ncell = size(calcium,2);
starts = [108:39:1631];
ntrials = 40;
combp = nchoosek([1:ncell],2);
neuron = cell(ncell,1);
for c = 1 : ncell
    for t = 1 : ntrials
        neuron{c}(:,t) = (calcium(starts(t) : starts(t) + 38,c) - mean(calcium(starts(t) : starts(t) + 38,c))) / std(calcium(starts(t) : starts(t) + 38,c));
    end
end

params =struct('tapers',[2 3],'Fs',3.91,'trialave',1);

C = cell(size(combp,1),1);
phi = cell(size(combp,1),1);
S1 = cell(size(combp,1),1);
S2 = cell(size(combp,1),1);
R = nan(ncell,ncell);
for p = 1 : size(combp,1)
    [C{p},phi{p},S12,S1{p},S2{p},f] = coherencyc(neuron{combp(p,1)},neuron{combp(p,2)},params);
    Rt = corrcoef(reshape(neuron{combp(p,1)},39*40,1),reshape(neuron{combp(p,2)},39*40,1));
    R(combp(p,1),combp(p,2)) = Rt(1,2);
end
figure; imagesc(R)
colorbar

figure; imagesc(R,[-0.5 0.7]); colorbar