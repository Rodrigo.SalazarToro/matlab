function makeFourierTransform(varargin)
% in the sessions folder

Args = struct('redo','Fs',200,'tapers',[2 3],'fpass',[0 100],'plength',400);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args);

Fs = Args.Fs;
tapers = Args.tapers;
fpass = Args.fpass;

cd lfp/
files = [nptDir('*.0*') nptDir('*.1*') nptDir('*.2*')];

[data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(files(1).name);

! mkdir fourierTrans
cd ..

for f = 1 : length(files)
    cd lfp/
    [data,num_channels,sampling_rate,scan_order,points]=nptReadStreamerFile(files(f).name);
    
    fname = regexprep(files(f).name, 'lfp.', 'fft_');
    cd ..
    [data,~,~,~] = lfpPcut(f,[1 : num_channels],'plength',Args.plength,varargin{:});
    N = size(data{1},1);
    nfft=max(2^(nextpow2(N)),N);
    [f,findx]=getfgrid(Fs,nfft,fpass);
    tapers=dpsschk(tapers,N,Fs);
    for p = 1 : 4
        data{p} = squeeze(data{p});
        
        J=mtfftc(data{p},tapers,nfft,Fs);
        J = tdata(findx,:,:);
        S=squeeze(mean(conj(J).*J,2));
        Jdata{p} = J;
        Sdata{p} = S;
    end
    cd lfp/fourierTrans
    save(sprintf('%s.mat',fname),'Jdata','Sdata','f')
    cd ../..
end