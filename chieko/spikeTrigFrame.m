function spikeTrigFrame(varargin)
% The movie files are *01.mov : last movie; one file per trial
% The neuronal data are *01.mat : last trial; one file per trial
% The spike time are referred to the beginning of the trial
%
Args = struct('nfolder','spikeTrigFrame');
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


nfiles = nptDir('*SpikeData*.mat');
thecells = [];
for ff = 1 : length(nfiles)
    load(nfiles(ff).name)
    thecells = [thecells; unique(newData(:,1))];
end
thecells = unique(thecells);
ncells = length(thecells);
mkdir(Args.nfolder);
pdir = [pwd filesep Args.nfolder];
parfor cc = 1 : ncells
    try
        SUAspikeTframe(thecells(cc),nfiles,pdir,modvarargin{:});
    catch
        display(sprintf('Error in neuron%03.0f.mat',thecells(cc)));
    end
end


end
%%
function SUAspikeTframe(cc,nfiles,pdir,varargin)

Args = struct('movieFrameRate',20,'redo',0,'addName',[],'onlyAmp',0);
Args.flags = {'redo','onlyAmp'};
[Args,~] = getOptArgs(varargin,Args,'remove',{});

sname = sprintf('neuron%03.0f%s.mat',cc,Args.addName);

if isempty(nptDir([pdir filesep sname])) || Args.redo
    ntrials = length(nfiles);
    neuron = [];
    trials = [];
    ampl = [];
    
    mfiles = nptDir('*.mov');
    for tt = 1 : ntrials
        stime = load(nfiles(tt).name);
        
        id = find(stime.newData(:,1) == cc);
        
        cctime = stime.cOnsets(id);
        cctime = cctime(cctime>0);
        if ~isempty(cctime) && ~(strcmp(pwd,'E:\salazar\calciumChronic\20161128\session01') && tt == 4)% problem with trial 4 thus trial 4 eliminated
            if ~Args.onlyAmp
                frames = round(Args.movieFrameRate * cctime); % 20 Hz
                themovie = importdata(mfiles(tt).name);
                themovie = single(squeeze(themovie(:,:,1,:)));
                
                neuron = cat(3,neuron,single(squeeze(themovie(:,:,frames))));
                
                trials = [trials repmat(tt,1,length(frames))];
               
            end
            
            amplt = stime.newData(id,3);
            ampl = [ampl; amplt];
        end
    end
    if Args.onlyAmp
         save([pdir filesep sname],'ampl','-append');
    else
        save([pdir filesep sname],'neuron','trials','ampl');
    end
    display(sprintf('saving %s%s%s',pdir,filesep,sname));
end
end