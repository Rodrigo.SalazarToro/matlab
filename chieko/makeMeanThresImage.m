function makeMeanThresImage(varargin)
% to be run in to the spikeFrame directory

Args = struct('borders',[100 400;220 470],'mouseDetect',[200 2000],'prctileThresh',.8,'strials','(end-7:end)','addVname',[],'withAmp',0,'splitSide',0);
Args.flags = {'withAmp','splitSide'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

files = nptDir('neuron*.mat');
sdir = pwd;
cd ..
nfiles = length(nptDir('*.mov'));

spfiles = nptDir('*AmpSpikeData*.mat');
spdir = pwd;
str = [1:nfiles];
str = eval(sprintf('str%s',Args.strials));
cd(sdir)
for ff = 1 : length(files)
    load(files(ff).name)
    if ~isempty(neuron)
        [~,~,~,cache] = mkFrameCache(neuron,'borders',Args.borders,'extras',[]);
        allimages = nan(size(cache,1),size(cache,2),size(neuron,3));
        strials = find(ismember(trials,str)); %trials are trial number of each spike and str are the chosen selected trials
        if ~isempty(strials)
            for fr = 1 : length(strials)
                
                theimage = squeeze(neuron(:,:,strials(fr)));
                th = prctile(reshape(theimage,[size(theimage,1) * size(theimage,2) 1]),Args.prctileThresh);
                
                spixel = theimage < th;
                BW2 = bwareafilt(logical(spixel),Args.mouseDetect);
                if Args.withAmp
                    allimages(:,:,fr) = cache .*BW2 .* ampl(fr);
                else
                    allimages(:,:,fr) = cache .*BW2;
                end
            end
            if Args.splitSide
                load([spdir filesep 'reinforcedSide.mat'])
                left = strfind(rside,'L');
                right = strfind(rside,'R');
                eval(sprintf('mImageR%s = squeeze(nanmean(allimages(:,:,find(ismember(trials,str(right)))),3));',Args.addVname));
                eval(sprintf('mImageL%s = squeeze(nanmean(allimages(:,:,find(ismember(trials,str(left)))),3));',Args.addVname));
            else
                eval(sprintf('mImage%s = squeeze(nanmean(allimages,3));',Args.addVname));
            end
        else
            if Args.splitSide
                eval(sprintf('mImageR%s = nan;',Args.addVname));
                eval(sprintf('mImageL%s = nan;',Args.addVname));
            else
                eval(sprintf('mImage%s = nan;',Args.addVname));
            end
        end
    else
        if Args.splitSide
            eval(sprintf('mImageR%s = nan;',Args.addVname));
            eval(sprintf('mImageL%s = nan;',Args.addVname));
        else
            eval(sprintf('mImage%s = nan;',Args.addVname));
        end
    end
    if Args.splitSide
        save(files(ff).name,sprintf('mImageR%s',Args.addVname),sprintf('mImageL%s',Args.addVname),'-append')
    else
        save(files(ff).name,sprintf('mImage%s',Args.addVname),'-append')
    end
    display(files(ff).name)
end