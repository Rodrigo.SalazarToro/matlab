clear all
% movies
dirm = {'D:\Chieko\Vglut2_CAR007751_ToRodrigo\BehaviourVideo\CDIDS1' 'D:\Chieko\Vglut2_CAR007751_ToRodrigo\BehaviourVideo\IDS2ED'};


for d = 1 : 2
    cd(dirm{d})
    day(d).trialm = nptDir('Test*.mov');
end
% importing movies : importdata(filename);
% spike timing

cd D:\Chieko\Vglut2_CAR007751_ToRodrigo\CellNum_Time_AmplitudeData
day(1).trialt = [nptDir('CAR007751_AmpSpikeData_20161130_ASS_CD_*.mat'); nptDir('CAR007751_AmpSpikeData_20161130_ASS_IDS1_*.mat')];

day(2).trialt = [nptDir('CAR007751_AmpSpikeData_20161130_ASS_IDS2_*.mat'); nptDir('CAR007751_AmpSpikeData_20161130_ASS_ED_*.mat')];

timec = {'D:\Chieko\Vglut2_CAR007751_ToRodrigo\TrialEndTimeData\CAR007751_ASS_endTime_CDIDS1.mat' 'D:\Chieko\Vglut2_CAR007751_ToRodrigo\TrialEndTimeData\CAR007751_ASS_endTime_IDS2ED.mat'};
varr = {'endTime_ID' 'endTime_ED'};


for d = 1 : 2
    cd D:\Chieko\Vglut2_CAR007751_ToRodrigo
    load(timec{d});
    eval(sprintf('%s = [0; %s];',varr{d},varr{d}));
    
    for cc = 1 : 100
        neuron = [];
        trials = [];
        for tt = 1 : length(day(d).trialt)
            stime = load([day(d).trialt(tt).folder filesep day(d).trialt(tt).name]);
            day(d).trialt(tt).name
            
            id = find(stime.newData(:,1) == cc);
            realt = str2num(day(d).trialt(tt).name(end-5:end-4));
            if isempty(realt); realt = str2num(day(d).trialt(tt).name(end-4:end-4)); end
            cctime = stime.newData(id,2) - eval(sprintf('%s(realt)',varr{d}));
            cctime = cctime(cctime>0);
            if ~isempty(cctime)
                frames = round(20*cctime); % 20 Hz
                themovie = importdata([day(d).trialm(realt).folder filesep day(d).trialm(realt).name]);
                themovie = single(squeeze(themovie(:,:,1,:)));
                neuron = cat(3,neuron,single(squeeze(themovie(:,:,frames))));
                trials = [trials repmat(realt,1,length(frames))];
            end
        end
        save(sprintf('day%dneuron%d.mat',d,cc),'neuron','trials');
        display(sprintf('saving day %d neuron %d', d, cc));
    end
    
end

%% viewing
% cd D:\Chieko\Vglut2_CAR007751_ToRodrigo
% files = nptDir('day1neuron*.mat');
% 
% figure
% for ff = 1 : length(files)
%     load(files(ff).name)
%     
%     
%     if size(neuron,3) > 1
%         subplot(2,2,1)
%         strials = find(trials > 6 & trials < 15);
%         imagesc(squeeze(std(neuron(:,:,strials),[],3)))
%         title(['CD ' files(ff).name ' nspikes =' num2str(length(strials))])
%         subplot(2,2,3)
%         hist(trials(strials),[0:30])
%         xlabel('trial #')
%         ylabel('counts')
%         
%         subplot(2,2,2)
%         strials = find(trials > 20 & trials < 29);
%         imagesc(squeeze(std(neuron(:,:,strials),[],3)))
%         title(['IDS ' files(ff).name ' nspikes =' num2str(length(strials))])
%         subplot(2,2,4)
%         hist(trials(strials),[0:30])
%         xlabel('trial #')
%         ylabel('counts')
%     else
%         imagesc(neuron)
%         title([files(ff).name ' nspikes = 1'])
%     end
%     
%     pause
% end

cd D:\Chieko\Vglut2_CAR007751_ToRodrigo

tlim{1} = [6 15; 20 29];
tlim{2} = [9 18; 30 39];
T1 = cell(2,1);
T2 = cell(2,1);
for d = 1 : 2
    files = nptDir(sprintf('day%dneuron*.mat',d));
    if plotthem; figure;end
    sframes1 = 120:350;%120:402;
    sframes2 = 220:460;
   
    plotthem = false;
    for ff = 1 : length(files)
        load(files(ff).name)
        
        if size(neuron,3) > 1
            
            strials = find(trials > tlim{d}(1,1) & trials < tlim{d}(1,2));
            neuron(110:270,340:370,:) = ones(length(110:270),length(340:370),size(neuron,3));
            me = mean(reshape(neuron(sframes1,sframes2,strials),length(sframes1)*length(sframes2)*length(strials),1));
            stde = std(reshape(neuron(sframes1,sframes2,strials),length(sframes1)*length(sframes2)*length(strials),1));
            nneuron = (neuron(sframes1,sframes2,strials) - me) /stde;
            T1{d}(:,:,ff) = squeeze(std(nneuron,[],3));
            if plotthem
                subplot(2,2,1)
                imagesc(squeeze(std(nneuron,[],3)))
                title(['IDS2 ' files(ff).name ' nspikes =' num2str(length(strials))])
                subplot(2,2,3)
                hist(trials(strials),[0:30])
                xlabel('trial #')
                ylabel('counts')
            end
            
            strials = find(trials > tlim{d}(2,1) & trials < tlim{d}(2,2));
            me = mean(reshape(neuron(sframes1,sframes2,strials),length(sframes1)*length(sframes2)*length(strials),1));
            stde = std(reshape(neuron(sframes1,sframes2,strials),length(sframes1)*length(sframes2)*length(strials),1));
            nneuron = (neuron(sframes1,sframes2,strials) - me) /stde;
            T2{d}(:,:,ff) = squeeze(std(nneuron,[],3));
            if plotthem
                subplot(2,2,2)
                imagesc(squeeze(std(nneuron,[],3)))
                title(['ED ' files(ff).name ' nspikes =' num2str(length(strials))])
                subplot(2,2,4)
                hist(trials(strials),[0:40])
                xlabel('trial #')
                ylabel('counts')
            end
        else
            imagesc(neuron)
            title([files(ff).name ' nspikes = 1'])
        end
        if plotthem;  pause; end
    end
end
%% make a movie

clear nneuron
nneuron(:,:,1,:) = neuron;
mov = immovie(uint8(nneuron),map);
implay(mov)
%% classification
nclass = 5;
tasks = {'T1' 'T2'};

for d = 1 : 2
    T1{d}(:,:,find(sum(sum(isnan(T1{d}))~= 0))) = [];
    T2{d}(:,:,find(sum(sum(isnan(T2{d}))~= 0))) = [];
    figure
    for task = 1 : 2
        thetask = eval(sprintf('%s',tasks{task}));
        rthetask = reshape(shiftdim(thetask{d},2),[size(thetask{d},3) length(sframes1)*length(sframes2)]);
        [idx,Centr] = kmeans(rthetask,nclass,'MaxIter',500,'Distance','sqeuclidean');%correlation
%         for ii = 1 : nclass
%         mCentr = reshape(Centr(ii,:),[length(sframes1) length(sframes2)]);
%         subplot(2,nclass,(task-1)*nclass + ii)
%         imagesc(mCentr)
%         end
        %     figure
        %     for ii = 1 : nclass
        %         ind = find(idx ==ii);
        %         for ni = 1 : length(ind)
        %             imagesc(squeeze(thetask(:,:,ind(ni))))
        %             title(sprintf('class %d',ii))
        %             pause
        %         end
        %     end
        % nclass = 4;
        
        
        for ii = 1 : nclass
            ind = find(idx ==ii);
            subplot(2,nclass,(task-1)*nclass +ii)
            imagesc(squeeze(nanmean(thetask{d}(:,:,ind),3)))
            title(sprintf('%s n=%d',tasks{task},length(ind)))
            
        end
        
    end
    
end
