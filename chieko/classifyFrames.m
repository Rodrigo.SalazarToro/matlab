function [idx,Centr,allframes,varargout] = classifyFrames(obj,varargin)
% 'borders',[100 400;220 470]; 'extras',[]
Args = struct('frameComb','mean','mouseDetect',[200 2000],'nclass',6,'metric','kmeans','Distance','sqeuclidean','MaxIter',1000,'plot',0,'minNspike',5,'prctileThresh',.8,'thevariable','mImage','zscore',0,'normalize',0,'replicates',10,'plotName',[]);
Args.flags = {'plot','zscore','normalize'};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});


[r,ind] = get(obj,'minNspike',Args.minNspike,'Number',varargin2{:});


cd(obj.data.setNames{ind(1)});
neuronn = sprintf('neuron%03.0f.mat',obj.data.Index(ind(1),2));
load(neuronn)

switch Args.frameComb
    case {'std'}
        [nframes,~,~,cache] = mkFrameCache(neuron,varargin2{:});
        theframe = squeeze(std(nframes,[],3));
    case {'mean'}
        [~,~,~,cache] = mkFrameCache(neuron,'extras',[],varargin2{:});
        %         tframe = nan(size(cache,1),size(cache,2),size(neuron,3));
        %         for fr = 1 : size(neuron,3)
        %             theimage = squeeze(neuron(:,:,fr));
        %             th = prctile(reshape(theimage,[size(theimage,1) * size(theimage,2) 1]),Args.prctileThresh);
        %             spixel = theimage < th;
        %             tframe(:,:,fr) = bwareafilt(logical(spixel),Args.mouseDetect);
        %         end
        %         theframe = nanmean(tframe,3);
        if eval(sprintf('unique(isnan(%s)) ~= 1',Args.thevariable))
            eval(sprintf('theframe = %s;',Args.thevariable))
        else
            theframe = nan(size(cache,1), size(cache,2));
        end
end

allframes = nan(size(theframe,1),size(theframe,2),r);
allframes(:,:,1) = theframe;
tpixels = theframe(cache);
cpixels = nan(r,length(tpixels));
cpixels(1,:) = tpixels;
% count = 1;
for ii = 1 : r
    
    cd(obj.data.setNames{ind(ii)});
    neuronn = sprintf('neuron%03.0f.mat',obj.data.Index(ind(ii),2));
    load(neuronn)
    switch Args.frameComb
        case {'std'}
            [nframes,~,~,cache] = mkFrameCache(neuron,varargin2{:});
            theframe = squeeze(std(nframes,[],3));
            
        case {'mean'}
            [~,~,~,cache] = mkFrameCache(neuron,'extras',[],varargin2{:});
            %             tframe = nan(size(cache,1),size(cache,2),size(neuron,3));
            %             for fr = 1 : size(neuron,3)
            %                 theimage = squeeze(neuron(:,:,fr));
            %                 th = prctile(reshape(theimage,[size(theimage,1) * size(theimage,2) 1]),Args.prctileThresh);
            %                 spixel = theimage < th;
            %                 tframe(:,:,fr) = bwareafilt(logical(spixel),Args.mouseDetect);
            %             end
            %             theframe = mean(tframe,3);
            if eval(sprintf('length(unique(%s)) > 1',Args.thevariable))
                 eval(sprintf('theframe = %s;',Args.thevariable))
                allframes(:,:,ii) = theframe;
                cpixels(ii,:) = theframe(cache);
                %                 count = count + 1;
            end
    end
end


npixels = cpixels ./ repmat(nanmax(cpixels,[],2),1,size(cpixels,2));
zpixels = cpixels;
zpixels(:,find(nansum(cpixels) ==0)) = [];
mn = nanmean(cpixels(~isnan(zpixels)));
st =  nanstd(cpixels(~isnan(zpixels)));
zpixels = (zpixels - mn) ./ st;

varargout{1} = cpixels;
varargout{2} = ind;
varargout{3} = cache;
varargout{4} = npixels;
varargout{5} = zpixels;


if Args.normalize
    [idx,Centr] = kmeans(npixels,Args.nclass,'MaxIter',Args.MaxIter,'Distance',Args.Distance,'Replicates',Args.replicates);%correlation
    
else
    [idx,Centr] = kmeans(cpixels,Args.nclass,'MaxIter',Args.MaxIter,'Distance',Args.Distance,'Replicates',Args.replicates);%correlation
    
end
thetasks = {'CD' 'IDS1' 'IDS2' 'ED'};
if Args.plot
    
    for ii = 1 : Args.nclass
        figure
        sind = find(idx ==ii);
        subplot(2,1,1)
        imagesc(squeeze(nanmean(allframes(:,:,sind),3)) .* cache)
        title(sprintf('n=%d',length(sind)))
        hold on;
        line([222 456],[238 238],'Color','w');
        line([222 456],[102 102],'Color','w');
        line([222 222],[102 238],'Color','w');
        line([456 456],[102 238],'Color','w');
        line([222+120 222+120],[102 238],'Color','w');
        viscircles([285 170],30,'Color','w');
        viscircles([408 168 ],30,'Color','w');
        colorbar
        subplot(2,1,2)
        ct = zeros(4,1);
        for ta = 1:4
        ct(ta) = sum(strcmp(obj.data.tasks(ind(sind)),thetasks{ta}));
        end
        bar(ct);
        set(gca,'XTickLabel',thetasks);
        set(gcf,'Name',Args.plotName)
    end
    
end
