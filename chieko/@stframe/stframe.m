function obj = stframe(varargin)
%

Args = struct('RedoLevels',0,'SaveLevels',0,'Auto',0,'dirname','spikeTrigFrame');
Args.flags = {'Auto'};
[Args,modvarargin] = getOptArgs(varargin,Args, ...
    'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});

% variable specific to this class. Store in Args so they can be easily
% passed to createObject and createEmptyObject
Args.classname = 'stframe';
Args.matname = [Args.classname '.mat'];
Args.matvarname = 'stf';

numArgin = nargin;
if(numArgin==0)
    % create empty object
    obj = createEmptyObject(Args);
elseif( (numArgin==1) && isa(varargin{1},Args.classname))
    obj = varargin{1};
else
    % create object using arguments
    if(Args.Auto)
        % change to the proper directory
        [~,cdir] = getDataDirs('session','relative','CDNow');%dirLevel('eye','relative','CDNow');
        % check for saved object
        if(isempty(cdir))
            % if there is an eye subdirectory, we are probably in the session dir
            % so change to the eye subdirectory
            [r,a] = ispresent('session','dir','CaseInsensitive');
            if r
                cdir = pwd;
                cd(a);
            end
        end
        if(ispresent(Args.matname,'file','CaseInsensitive') ...
                && (Args.RedoLevels==0))
            fprintf('Loading saved %s object...\n',Args.classname);
            l = load(Args.matname);
            obj = eval(['l.' Args.matvarname]);
        else
            % no saved object so we will try to create one
            % pass varargin in case createObject needs to instantiate
            % other objects that take optional input arguments
            obj = createObject(Args,modvarargin{:});
        end
        % change back to previous directory if necessary
        if(~isempty(cdir))
            cd(cdir)
        end
    end
end

end
%%
function obj = createObject(Args,varargin)
sdir = pwd;
if ~isempty(nptDir('task.txt'))
    thetask = fileread('task.txt');
else
    thetask = '???';
end
if ~isempty(nptDir(Args.dirname))
    cd(Args.dirname) 
    if ~isempty(nptDir('neuron*.mat'))
        files = nptDir('neuron*.mat');
        tasks = cell(length(files),1);
        Index(:,1) = [1:length(files)]';
        setNames = cell(length(files),1);
        for ff = 1 : length(files)
            Index(ff,2) = str2num(files(ff).name(end-6:end-4));
            ddd = load(files(ff).name);
            Index(ff,3) = length(ddd.trials);
            Index(ff,4) = length(unique(ddd.trials));
            tasks{ff} = thetask;
            setNames{ff} = pwd;
        end
        
        data.Index = Index;
        data.tasks = tasks;
        data.numSets = length(files); % nbr of trial
        data.setNames = setNames;
        n = nptdata(data.numSets,0,pwd);
        d.data = data;
        obj = class(d,Args.classname,n);
        if(Args.SaveLevels)
            fprintf('Saving %s object...\n',Args.classname);
            eval([Args.matvarname ' = obj;']);
            % save object
            eval(['save ' Args.matname ' ' Args.matvarname]);
        end
    else
        % create empty object
        fprintf('empty object \n');
        obj = createEmptyObject(Args);
        
    end
else
    fprintf('empty object \n');
    obj = createEmptyObject(Args);
end
cd(sdir)
end

function obj = createEmptyObject(Args)

% these are object specific fields
% data.spiketimes = [];
% data.spiketrials = [];


% useful fields for most objects
data.Index = [];
data.tasks = [];
data.numSets = 0;

data.setNames = '';
% create nptdata so we can inherit from it
n = nptdata(0,0);
d.data = data;
obj = class(d,Args.classname,n);
end