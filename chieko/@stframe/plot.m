function obj = plot(obj,varargin)


Args = struct('plotType','standard','prctileThresh',.8,'mouseDetect',[200 2000],'borders',[100 400;220 470],'thevariable','mImage');
Args.flags = {};
[Args,varargin2] = getOptArgs(varargin,Args,'remove',{});

[numevents,dataindices] = get(obj,'Number',varargin2{:});
if ~isempty(Args.NumericArguments)
    
    n = Args.NumericArguments{1}; % to work oon
    ind = dataindices(n);
else
    
end
cd(obj.data.setNames{ind})
neuronn = sprintf('neuron%03.0f.mat',obj.data.Index(ind,2));
% display(obj.data.Index(ind,28:37))

%% plotting fct
load(neuronn)
set(gcf,'Name',sprintf('%s %s',obj.data.setNames{ind},neuronn))
switch Args.plotType
    
    case {'standard' }
        
        subplot(1,2,1)
        imagesc(squeeze(std(neuron,[],3)))
        title([neuronn ' nspikes =' num2str(size(neuron,3))])
        subplot(1,2,2)
        hist(trials,[0:50])
        xlabel('trial #')
        ylabel('counts')
    case {'withCache'}
        
        nframes = mkFrameCache(neuron);
        imagesc(squeeze(std(nframes,[],3)),[0 3])
        title([neuronn ' nspikes =' num2str(size(neuron,3))])
    case {'iframes'}
        for fr = 1 : size(neuron,3)
            colormap('gray')
            imagesc(squeeze(neuron(:,:,fr)))
            title([neuronn ' nspikes =' num2str(size(neuron,3))])
            pause(0.3)
            
        end
    case {'threshold'}
        [~,~,~,cache] = mkFrameCache(neuron,'borders',Args.borders,'extras',[]);
         for fr = 1 : size(neuron,3)
            colormap('gray')
            theimage = squeeze(neuron(:,:,fr));
            th = prctile(reshape(theimage,[size(theimage,1) * size(theimage,2) 1]),Args.prctileThresh);
            subplot(1,2,1)
            imagesc(theimage)
            subplot(1,2,2)
            %             spixel = cache.*(theimage < th);
            spixel = theimage < th;
            BW2 = bwareafilt(logical(spixel),[200 2000]);
            imagesc(cache .*BW2)
            pause(1.0)
            
         end
    case {'meanThreshold'}
%         [~,~,~,cache] = mkFrameCache(neuron,'borders',Args.borders,'extras',[]);
%         allimages = nan(size(cache,1),size(cache,2),size(neuron,3));
%          for fr = 1 : size(neuron,3)
%            
%             theimage = squeeze(neuron(:,:,fr));
%             th = prctile(reshape(theimage,[size(theimage,1) * size(theimage,2) 1]),Args.prctileThresh);
%             
%             spixel = theimage < th;
%             BW2 = bwareafilt(logical(spixel),Args.mouseDetect);
%             allimages(:,:,fr) = cache .*BW2;
%          end
        
        eval(sprintf('imagesc(%s);',Args.thevariable))
        title('Mean')
        colorbar
       
end
%%
