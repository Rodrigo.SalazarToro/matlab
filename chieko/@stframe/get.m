function [r,varargout] = get(obj,varargin)
%   mtslfptraces/get Get function for mtslfptraces objects
%
%
%   Object level is session object
%
%
%   Dependencies: getTrials
%
Args = struct('Number',0,'ObjectLevel',0,'minNspike',[],'neuronNumber',[1:100],'day',[],'task',[]);
Args.flags = {'Number','ObjectLevel'};
Args = getOptArgs(varargin,Args);



varargout{1} = {''};
varargout{2} = 0;

if Args.Number
    
    if ~isempty(Args.minNspike)
        ttemp = find(obj.data.Index(:,3) >= Args.minNspike);
    else
        ttemp = [1 : size(obj.data.Index,1)];
    end
    if ~isempty(Args.neuronNumber)
        ttemp = intersect(ttemp,find(ismember(obj.data.Index(:,2),Args.neuronNumber) == 1));
    
    end
    
    if ~isempty(Args.day)
        ttemp = intersect(ttemp,find(~cellfun(@isempty,strfind(obj.data.setNames,Args.day))));
   
    end
    
    if ~isempty(Args.task)
        ttemp = intersect(ttemp,find(~cellfun(@isempty,strfind(obj.data.tasks,Args.task))));
   
    end
    varargout{1} = ttemp;
    r = length(varargout{1});
    
elseif(Args.ObjectLevel)
    r = 'Session';
else
    r = get(obj.nptdata,varargin{:});
    
end

