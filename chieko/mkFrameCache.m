function [nframes,varargout] = mkFrameCache(frames,varargin)
% frames is on xpixel x ypixel x nframes
% Args.extra in x1 x2 y1 y2 format for a rectangle. if more than one x1 x2
% y1 y2; x1 x2 y1 y2
% z-score Normalization is included
Args = struct('frameSize',[480 640],'borders',[120 350;220 460],'extras',[100 290 330 370],'getCacheFrom',[],'normalize',0);
Args.flags = {'normalize'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

if ~isempty(Args.getCacheFrom) && ~isempty(nptDir(Args.getCacheFrom))
    load(Args.getCacheFrom)
    Args.frameSize = frameSize;
    Args.borders = borders;
    Args.extras = extras;
elseif ~isempty(Args.getCacheFrom) && isempty(nptDir(Args.getCacheFrom))
    error('Cache file %s not present',Args.getCacheFrom)
end

cache = ones(Args.frameSize(1),Args.frameSize(2));

cache(1:Args.borders(1,1),:) = zeros(Args.borders(1,1),size(cache,2)); % zeros for cache
cache(Args.borders(1,2) + 1: end,:) = zeros(size(cache,1) - Args.borders(1,2),size(cache,2));
cache(:,1:Args.borders(2,1)) = zeros(size(cache,1),Args.borders(2,1));
cache(:,Args.borders(2,2) +1 : end) = zeros(size(cache,1),size(cache,2) - Args.borders(2,2));

for ne = 1 : size(Args.extras,1)
    cache(Args.extras(ne,1) : Args.extras(ne,2),Args.extras(ne,3) : Args.extras(ne,4)) = zeros(length(Args.extras(ne,1) : Args.extras(ne,2)),length(Args.extras(ne,3) : Args.extras(ne,4)));
end
cache = logical(cache);
tcache = repmat(cache,1,1,size(frames,3));

if Args.normalize
    me = mean(frames(tcache));
    stde = std(frames(tcache));
    frames = (frames - me) /stde;
    
end
nframes = tcache .* frames;
varargout{1} = frames;
varargout{2} = tcache;
varargout{3} = cache;