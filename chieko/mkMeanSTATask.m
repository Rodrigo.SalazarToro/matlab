
tasks = unique(obj.data.tasks);

figure
texture1 = repmat([222:456; ones(1,456-222+1)],238-102+1,1);
texture = zeros(480,640);
texture(102:238,222:456) = 1;
bowls = zeros(480,640);
ccoef = 0.025;
texbol = cell(4,1);
for ro = 0:30
    bowls(170+ro,[round(285-30+ccoef*ro^2) : round(285+30-ccoef*ro^2) round(408-30+ccoef*ro^2) : round(408+30-ccoef*ro^2)]) = 1;
    bowls(170-ro,[round(285-30+ccoef*ro^2) : round(285+30-ccoef*ro^2) round(408-30+ccoef*ro^2) : round(408+30-ccoef*ro^2)]) = 1;
end
mice = {'20161123' '20161128' '20161130' '20161201'};
 mSTA = cell(length(tasks),length(mice));
 
 thevariable = 'mImageAmp';
%  thevariable = 'mImage';
for mouse = 1 : length(mice)
    mSTA{mouse,1} = zeros(480,640);
    mSTA{mouse,2} = zeros(480,640);
    mSTA{mouse,3} = zeros(480,640);
    mSTA{mouse,4} = zeros(480,640);
    figure
    for tt = 1 : length(tasks)
        [r,ind] = get(obj,'Number','task',tasks{tt},'day',mice{mouse});
        c = 0;
        for ii = 1 : r
            cd(obj.data.setNames{ind(ii)})
            neuronn = sprintf('neuron%03.0f.mat',obj.data.Index(ind(ii),2));
            
            load(neuronn)
            if sum(size(mImage) == [480 640]) == 2 && sum(sum(~isnan(mImage))) ~= 0
%                             eval(sprintf('nmSTA = %s/nanmax(nanmax(%s));',thevariable,thevariable));
                eval(sprintf('nmSTA =  %s;',thevariable));
                texbol{tt}(ii,1) = sum(nmSTA(logical(texture)))/sum(sum(texture));
                texbol{tt}(ii,2) =  sum(nmSTA(logical(bowls)))/sum(sum(bowls));
                mSTA{mouse,tt} = mSTA{mouse,tt} + nmSTA;
                c = c + 1;
            end
        end
        mSTA{mouse,tt} = mSTA{mouse,tt} /c;
        subplot(1,4,tt)
        imagesc(mSTA{mouse,tt})
        hold on;
        line([222 456],[238 238],'Color','w');
        line([222 456],[102 102],'Color','w');
        line([222 222],[102 238],'Color','w');
        line([456 456],[102 238],'Color','w');
        line([222+120 222+120],[102 238],'Color','w');
        viscircles([285 170],30,'Color','w');
        viscircles([408 168],30,'Color','w');
        title(tasks{tt})
        colorbar
          xlim([222 456])
        ylim([102 400])
    end
    set(gcf,'Name',mice{mouse})
    figure;
    for tt = 1 : 4
        subplot(1,4,tt);
        boxplot(texbol{tt},'Labels',{'texture' 'bowls'});
        ylim([0 .3]);
        title(tasks{tt});
      
    end
    set(gcf,'Name',mice{mouse})
end
%% for each side separately
