function tf_visualizer(dat,freqrange,fromchan,tochan,cmax,cmin,opt)

%TF_VISUALIZER plots t-f planes
%
% usage: [...]=tf_visualizer(dat,freqrange,fromchan,tochan,cmax,cmin,opt)
%
% Inputs:
%    dat:       time frequency array (GC, Coherence or Power)
%    freqrange: the range of frequencies to be plotted (i.e. [1 100])
%    fromchan:  for GC, this is the sending channel, for coherence
%               fromchan and tochan are interchangeable.  For Power they
%               must be the same
%    tochan:    the target channel
%    cmax:      upper value of the colorbar
%    cmin:      lower value of the colorbar
%    opt:       1 for plotting epoch1 and 2 from plotting epoch2
%
%    ex.        tf_visualizer(dat,[1 100],4,1,.05,0,2)
%
%    NOTE:  Coherence and power are both calculated from the same array
%    that results from processing a *.co file with tf_load_from_clust.m, GC
%    plots are constructed from arrays resulting from *.gc cluster files
%
% September 25 2007
% Craig Richter, Center for Complex Systems &
% Brain Sciences, FAU, Boca Raton, FL 33413


dat = permute(dat,[1 4 2 3]);
datrange = [1 size(dat,1)];
freq = [1 size(dat,2)];
surf(dat(datrange(1):datrange(2),freq(1):freq(2),fromchan,tochan)','FaceColor','interp','LineStyle','none');
view(2);
set(gcf,'renderer','zbuffer');
%xlim([freqrange(1) freqrange(2)]);
axis([datrange(1) datrange(2) freqrange(1) freqrange(2)]);
if opt==1
    % starts from 2 because of stimstart at 505. i.e. point 1 = -505, point 2=
    % -500
    set(gca,'xtick',[2,42,92,122,162,202,242,282,322,362]);
    set(gca,'xticklabel',{-450,-250,0,150,350,550,750,950,1150});
    title('Sample Locked');
end

if opt==2
    set(gca,'xtick',[2,42,82,122,152,202,242,282,322,362]);
    set(gca,'xticklabel',{-750,-550,-350,-150,0,250,450,650,850});
    title('Match Locked');
end

%sets color axis limit (max/min values):  
set(gca,'CLim',[cmin cmax])
xlabel('ms')
ylabel('Hz')
colorbar;
box on
set(gca,'tickdir','out')