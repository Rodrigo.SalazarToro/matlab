function writeCPPdataML(varargin)


%run at monkey level

%NOTE: Clarks data uses channel number and not group number. With parietal
%channels first and then frontal channels.

%Bettys data uses GROUP numbers with original orientation 1:32 for Frontal
%and 33:64 for Parietal. ALL CHANNELS ARE PROCESSED, BAD CHANNELS HAVE TO
%BE ACCOUNTED FOR LATER

%ide_only indicatest that only the identity rule was run


%Clark data was filtered at 10-50 Hz
%Betty data was filtered at 10-50 Hz


%Args.clean will delete lfp2 files and channelmatrix.mat

%Args.lite saves the bare minimum
%Args.bh_glmdelay : runs fdr_bh.m correction on all p-values


Args = struct('ml',0,'ide_only',0,'days',[],'sessions',[],'monkey','betty','windowSize',200,'windowStep',50,'phaseSize',50,...
    'lowpass_normalize',0,'avg_correlograms',0,'epoch_correlograms',0,'avg_correlograms_surrogates',0,...
    'epoch_correlograms_surrogates',0,'cross',0,'threshold',0,'channelpairs',0,'redo_channelpairs',0,...
    'delay_slopes',0,'clean',0,'lite',0,'pair_info',0,'ccg_surrogates',0,'bh_glm',0,'makeneuronalhist',0,...
    'generalized_gabor',0,'global_sta_surrogate',0,'glm_train_predict',0,'global_epoch_correlograms_surrogate',0,'delay_correlograms',0,'delay_correlograms_surrogates',0);

Args.flags = {'ml','ide_only','lowpass_normalize','avg_correlograms','epoch_correlograms','avg_correlograms_surrogates',...
    'epoch_correlograms_surrogates','cross','threshold','channelpairs','redo_channelpairs','delay_slopes','clean','lite',...
    'pair_info','ccg_surrogates','bh_glm','makeneuronalhist','generalized_gabor','global_sta_surrogate','glm_train_predict',...
    'global_epoch_correlograms_surrogate','delay_correlograms','delay_correlograms_surrogates'};
Args = getOptArgs(varargin,Args);

%Xcorr
windowSize = Args.windowSize;
windowStep = Args.windowStep;
phaseSize = Args.phaseSize;

daydir = cd;
%Get all the switch days


run_days = [1:size(Args.days,2)];

for alldays = run_days
    alldays
    cd ([daydir filesep Args.days{alldays}])
    days = pwd;
    numb_sessions = nptDir('*session0*')
    nses = size(numb_sessions,1);
    if ~Args.ml
        if max(Args.sessions) > nses
            Args.sessions(end) = []; %if session03 dne then it will be skipped
        end
    end
    
    for sessions = Args.sessions;
        cd ([days filesep numb_sessions(sessions).name]);
        ses = pwd;
        [NeuroInfo] = NeuronalChAssign;
        
        %GET A LIST OF CHANNELS THAT WERE USED FOR ANALYSIS, THIS WILL
        %INCLUDE "FLAT" CHANNELS.
        
        %MAKE AND INDEX TO SAVE IN RULE FOLDERS
        if Args.ml
            all_channels = NeuroInfo.groups;
        else
            all_channels = (1:size(NeuroInfo.groups,2));
        end
        
        
        %Get number of channels
        chnumb=length(NeuroInfo.channels);
        
        %if there aren't enough channels then do nothing
        if chnumb < 2
            fprintf(1,'ONLY ONE CHANNEL!\n')
            return
        end
        
        %calculate number of pairs (n choose 2)
        pairs = (chnumb*(chnumb-1)) / 2;
        
        %create trial object
        if Args.ml
            mt = mtstrial('auto','ML','RTfromML','redosetNames');
            trials_identity = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',1)';
            trials_location = mtsgetTrials(mt,'BehResp',1,'stable','ml','rule',2)';
        else
            mt=mtstrial('auto','redosetNames');
            trials_identity = mtsgetTrials(mt,'BehResp',1,'stable','rule',1)';
            trials_location = mtsgetTrials(mt,'BehResp',1,'stable','rule',2)';
        end
        
        if ~Args.ml
            cd ([ses filesep 'lfp']);
            load rejectedTrials stdTrials PWTrials rejectCH rejecTrials
        end
        
        %Low Pass and Normalize
        %cuts out everything after first sac on mts trials
        if Args.lowpass_normalize
            %makes lfp2 directory
            %lowpass and normalized trial data in new dir (lfp2)
            %THESE ARE THE CHANNELS AND PAIRS USED!
            
            if Args.ml
                cd([ses filesep 'lfp'])
                
                lowPass4XcorrML(chnumb) %leaves in lfp2 directory
                
                mkdir('identity')
                mkdir('location')
            else
                cd([ses filesep 'lfp'])
                
                lowPass4Xcorr(chnumb) %leaves in lfp2 directory
            end
        end
        
        all_pairs = [];
        ap = 0;
        for ap1 = 1 : chnumb
            for ap2 = (ap1+1) : chnumb
                ap = ap + 1;
                all_pairs(ap,:) = [all_channels(ap1) all_channels(ap2)];
            end
        end
        
        if Args.pair_info
            cd([ses filesep 'lfp' filesep 'lfp2'])
            if Args.ml
                cd('identity')
                save all_channels all_channels
                save all_pairs all_pairs
                cd ..
                cd('location')
                save all_channels all_channels
                save all_pairs all_pairs
                cd ..
                save all_channels all_channels
                save all_pairs all_pairs
            else
                save all_channels all_channels
                save all_pairs all_pairs
            end
            
        end
        
        %Make Average Correlograms
        if Args.avg_correlograms || Args.avg_correlograms_surrogates
            %Calls averageCorrelograms function (makes average correlograms
            %and surrogates
            cd([ses filesep 'lfp'])
            averageCorrelogram('ml',Args.ml,'chnumb',chnumb,'monkey',Args.monkey,'correlograms',Args.avg_correlograms,'surrogate',Args.avg_correlograms_surrogates,'all_channels',all_channels) %leaves in lfp2 directory
        end
        
        %Make Epoch Correlograms
        if Args.epoch_correlograms || Args.epoch_correlograms_surrogates
            cd([ses filesep 'lfp'])
            %makes 4 correlograms for each epoch (400ms each)
            epochCorrelograms('ml',Args.ml,'chnumb',chnumb,'monkey',Args.monkey,'correlograms',Args.epoch_correlograms,'surrogate',Args.epoch_correlograms_surrogates) %leaves in lfp directory
        end
        
        %Make Delay Correlograms
        if Args.delay_correlograms || Args.delay_correlograms_surrogates
            cd([ses filesep 'lfp'])
            %makes 4 correlograms for each epoch (400ms each)
            delayCorrelogram('ml',Args.ml,'chnumb',chnumb,'monkey',Args.monkey,'correlograms',Args.delay_correlograms,'surrogate',Args.delay_correlograms_surrogates,'all_channels',all_channels) %leaves in lfp2 directory
        end
        
        
        if Args.cross
            %checks to see if the average correlogram crosses threshold
            cd([ses filesep 'lfp' filesep 'lfp2'])
            avg_cross(chnumb,Args.ml) %leaves in lfp2 directory
            cd([ses filesep 'lfp'])
        end
        
        if ~Args.ml
            cd([ses filesep 'lfp' filesep 'lfp2'])
            save rejectedTrials stdTrials PWTrials rejectCH rejecTrials %saves index of rejected trials in lfp2 folder
        end
        
        %write thresholds for channelpair files
        if Args.threshold
            
            if Args.ml
                lfp2_dirs = ['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                num_lfp2 = [1 2];
                if Args.ide_only
                    lfp2_dirs = ['identity']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                    num_lfp2 = [1];
                end
            else
                lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
                num_lfp2 = [1 2];
            end
            
            cd([ses filesep 'lfp'])
            for ide_loc = num_lfp2
                type = mt.data.Index(:,1);
                
                trials_all = [{trials_identity'} {trials_location'}];
                
                
                cd([ses filesep 'lfp' filesep 'lfp2'])
                
                %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
                % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
                lfpdata = nptDir([Args.monkey '*']);
                load ([cd filesep lfpdata(1).name],'normdata');
                numchannels = size(normdata,1);
                ddata = cell(numchannels,1);
                if isempty(trials_all{ide_loc})  %if there are no trials then do not run threshold calculation
                    if ide_loc == 1
                        fprintf(1,'No identity trials\n')
                    else
                        fprintf(1,'No location trials\n')
                    end
                else
                    rr_surrogate=cell(1,pairs);
                    pair = 0;
                    for ii=1:chnumb
                        fprintf('\n%0.5g     ',ii)
                        for jj=ii+1:chnumb
                            fprintf(' %0.5g',jj)
                            pair = pair + 1;
                            %concatenate all trials for each channel
                            data1 = [];
                            data2 = [];
                            for x = trials_all{ide_loc}
                                load ([cd filesep lfpdata(x).name],'normdata');
                                ddata1 = [data1 single(normdata(ii,:))];
                                ddata2 = [data2 single(normdata(jj,:))];
                            end
                            
                            if Args.ml
                                [r_surrogate posneg] = randxcorr_peaks('x',ddata1,'y',ddata2,'pair_number',pair,'ml','mts_obj',mt,'ide_loc',ide_loc);
                            else
                                [r_surrogate posneg] = randxcorr_peaks('x',ddata1,'y',ddata2,'pair_number',pair);
                            end
                            
                            
                            rr_surrogate{pair} = r_surrogate;
                            pn(pair) = posneg; %this is used to determine if positive or negative peaks were searched for
                        end
                    end
                    threshold = cell(1,pairs);
                    for p = 1:pairs
                        %Loads 10000 samples for each pairwise combination
                        pp = rr_surrogate{p};
                        if pn(p) == 1
                            threshold{p} = prctile(pp,[95 99 99.9 99.99]);
                        elseif pn(p) == -1
                            tp = prctile(pp,[5 1 .1 .01]) * -1; %this makes the values all positive
                            threshold{p} = tp;
                        end
                    end
                    if Args.ml
                        cd(lfp2_dirs(ide_loc,:))
                    end
                    write_info = writeinfo(dbstack);
                    save threshold threshold write_info
                    
                end
                cd([ses filesep 'lfp' filesep 'lfp2'])
                
            end
        end
        
        %write channelpairs
        if Args.channelpairs
            
            if Args.ml
                lfp2_dirs =['lfp2']; %['identity'; 'location']; %lfp2 data is saved in two folders ide_lfp2 and loc_lfp2
                num_lfp2 = [1]; %[1 2];
                
                N = NeuroInfo;
                groups = N.gridPos;
                
            else
                lfp2_dirs = ['lfp2']; %lfp2 data is saved in one folder
                num_lfp2 = [1];
            end
            
            cd([ses filesep 'lfp'])
            for ide_loc = 1%;num_lfp2
                trials_all = [1:size(mt.data.Index,1)]; %just run for every trial
                cd([ses filesep 'lfp' filesep 'lfp2'])
                lfp2directory = pwd;
                %NEED TO SELECT FOR IDE/LOC TRIALS, THEN SAVE CHANNEL PAIRS IN THEIR
                % RESPECTIVE FOLDERS. USE MTSGETTRIALS FOR THE INDEX
                lfpdata = nptDir([Args.monkey '*']);
                load ([cd filesep lfpdata(1).name],'normdata');
                numchannels = size(normdata,1);
                ddata = cell(numchannels,1);
                
                pair_number = 0;
                total_pairs = pairs;
                for c1=1:chnumb
                    for c2=(c1+1):chnumb
                        
                        pair_number = pair_number + 1;
                        fprintf(1,[num2str(pair_number) '  of  ' num2str(total_pairs)])
                        fprintf(1,'\n')
                        
                        %check to see if the channel_pair has already been written
                        if Args.ml
                            channel_pairs = [ 'channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
                        else
                            channel_pairs = [ 'channelpair' num2strpad(c1,2) num2strpad(c2,2)]
                        end
                        tpz=1;
                        if tpz%isempty(nptDir([channel_pairs '.mat'])) || Args.redo_channelpairs
                            
                            corrcoefs=[]; phases=[]; correlograms={};
                            
                            for x = trials_all
                                cd(lfp2directory)
                                %get avg_correlograms
                                if Args.ml
                                    if mt.data.Index(x,1) == 1
                                        cd('identity');
                                    elseif mt.data.Index(x,1) == 2
                                        cd('location')
                                    end
                                    load avg_correlograms_epochs avg_corrcoef_epochs
                                    cd .. %go back to lfp2 directory
                                else
                                    load avg_correlograms_epochs avg_corrcoef_epochs
                                end
                                load ([cd filesep lfpdata(x).name],'normdata');
                                %calculate number of bins
                                steps = floor((length(normdata)-windowSize)/windowStep);
                                start =  1:windowStep:steps*windowStep+1;
                                endd = start+windowSize-1;
                                r = GrayXxcorr(normdata(c1,:),normdata(c2,:),windowSize,windowStep,phaseSize,start,endd);
                                pos_or_neg = avg_corrcoef_epochs{4,pair_number}; %THIS IS EPOCH 4 (DELAY 2)
                                %if the average correlogram has a negative peak that is closest to zero
                                %then invert the correlograms to find the negative peaks that are
                                %closest to zero
                                if pos_or_neg < 0
                                    r = r * -1;
                                    inverted = 1;
                                else
                                    inverted = 0;
                                end
                                lags = [(-1*phaseSize): phaseSize];
                                
                                %find closest peak to zero lag
                                p=zeros(size(r,1),2);
                                for kk=1:size(r,1)
                                    rr=r(kk,:);
                                    
                                    %Find index of central positive peak
                                    peak = findpeaks(rr);
                                    for ppp = 1 : size(peak.loc,1)
                                        if (rr(peak.loc(ppp)) < 0)
                                            %set to max phase index
                                            peak.loc(ppp) = length(lags);
                                        end
                                    end
                                    [phase, index] = (min(abs(lags(peak.loc))));
                                    %get real phase value by indexing the location in the lags
                                    %get corr coef by indexing the correlogram
                                    peak_phase = lags(peak.loc(index));
                                    peak_corrcoef = rr(peak.loc(index));
                                    
                                    if size(peak.loc,1) > 0
                                        %Stores the correlation coef and lag position.
                                        p(kk,2) = peak_phase;
                                        p(kk,1) = peak_corrcoef;
                                    else
                                        p(kk,1) = nan;
                                        p(kk,1) = nan;
                                    end
                                end
                                %if the correlograms are inverted then make the corrcoefs
                                %negative to correspond to their actual values
                                if inverted == 1
                                    p(:,1) = p(:,1) * -1;
                                    
                                    r = r * -1; %reverse correlogram back
                                end
                                %%coefs = the correlation coefs for the trial pair
                                coefs=(p(:,1));
                                %%phases = the phases
                                phase=(p(:,2));
                                %Find the length of the coefs column.
                                cc = length(coefs);
                                dd = length(phase);
                                %Determines how many buffer NaN's are necessary
                                ccc = (3000/windowStep) - cc; %3000/windowStep determines #of bins
                                ddd = (3000/windowStep) - dd;
                                %make sure there are only 60 bins
                                if ccc >= 0
                                    %Creates NaN buffer
                                    cccc = NaN(1,ccc)';
                                    dddd = NaN(1,ddd)';
                                    %%Concatenates NaN buffer on to the end of the column in order
                                    %%for the matrix dimensions to match
                                    coefs = cat(1,coefs,cccc);
                                    phase = cat(1,phase,dddd);
                                else
                                    
                                    coefs = coefs((1:end+ccc),1);
                                    phase = phase((1:end+ddd),1);
                                end
                                
                                %%makes a matrix with all trials correlation coef for the
                                %%specific pair
                                corrcoefs = cat(2,corrcoefs, coefs);
                                phases = cat(2,phases, phase);
                                
                                %places r in the cell location corresponding to the trial number
                                correlograms{x} = r;
                            end
                            
                            %indexes will be a 4X1 cell
                            %   cell 1: correct,stable
                            %   cell 3: correct,transition
                            %   cell 4: incorrect,stable
                            %   cell 5: incorrect,transition
                            
                            indexes = cell(1,4);
                            lfp2 = cd;
                            cd ..
                            lfp_dir = cd;
                            cd ..
                            
                            if ~Args.ml
                                mt=mtstrial('auto');
                            else
                                mt = mtstrial('auto','ML','RTfromML');
                            end
                            
                            cd(lfp_dir)
                            %correct, stable
                            allTrials = trials_all;
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',1,'stable');
                            else
                                ind = mtsgetTrials(mt,'BehResp',1,'stable','ML');
                            end
                            
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{1} = index;  %index 1 is correct stable trials
                            
                            %correct, transition
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',1,'transition');
                            else
                                ind = mtsgetTrials(mt,'BehResp',1,'transition','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{2} = index;
                            
                            %incorrect, stable
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',0,'stable');
                            else
                                ind = mtsgetTrials(mt,'BehResp',0,'stable','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{3} = index;
                            
                            %incorrect, transtion
                            index = [];
                            if ~Args.ml
                                ind = mtsgetTrials(mt,'BehResp',0,'transition');
                            else
                                ind = mtsgetTrials(mt,'BehResp',0,'transition','ML');
                            end
                            for i = 1 : size(trials_all,2)
                                if ~isempty(intersect(allTrials(i),ind))
                                    index = [index i];
                                end
                            end
                            indexes{4} = index;
                            cd(lfp2)
                            
                            %Write Indexes For Each Threshold
                            plot_data = cell(5,5);
                            
                            %rows are the criteria
                            %   all
                            %   correct/stable
                            %   correct/transition
                            %   incorect/stable
                            %   incorrect/transition
                            
                            %columns are the thresholds (0 - 4)
                            if Args.ml
                                if mt.data.Index(x,1) == 1
                                    cd('identity');
                                elseif mt.data.Index(x,1) == 2
                                    cd('location')
                                end
                                load threshold threshold
                                cd(lfp2) %go back to lfp2 directory
                            else
                                load threshold threshold
                            end
                            
                            for thresh = 0:4
                                for allindexes = 0 : 4
                                    if allindexes == 0
                                        cc = abs(corrcoefs);
                                        ppp = phases;
                                    else
                                        cc = abs(corrcoefs(:,indexes{allindexes}));  %necessary for trials with negative peaks
                                        ppp = phases(:,indexes{allindexes});
                                    end
                                    %Index threshold for the correct threshold
                                    if thresh > 0
                                        th = threshold{pair_number};
                                        thresholds = th(:,thresh);
                                    else
                                        thresholds = 0;
                                    end
                                    [rows columns] = size(cc);  %will be the same for the phases
                                    cors = [];
                                    phase = [];
                                    number=[];
                                    for yyy = 1:columns
                                        corrss = [];
                                        phasess= [];
                                        x=[];
                                        ntrials = [];
                                        for xxx = 1:rows %50ms bins w/ a max at 3000ms
                                            %creates a row vector that contains the number of trials
                                            %for each bin
                                            ntrials = cat(2,ntrials,(sum(isnan(cc(xxx,:)))));
                                            %Determine if threshold is crossed
                                            if  cc(xxx,yyy) > thresholds
                                                %If it is then keep the number
                                                bb = cc(xxx,yyy);
                                                pp = ppp(xxx,yyy);
                                                yy = 1;
                                            else
                                                %Else, input NaN
                                                bb = NaN;
                                                pp = NaN;
                                                yy = 0;
                                            end
                                            x = cat(1,x, yy);%%%Determines number of trials by not counting the elses.
                                            corrss = cat(1,corrss, bb);
                                            phasess = cat(1,phasess,pp);
                                        end
                                        number = cat(2,number,x);
                                        cors = cat(2,cors,corrss);
                                        phase = cat(2,phase,phasess);
                                    end
                                    d.number = number;
                                    d.cors = cors;
                                    d.phase = phase;
                                    d.ntrials = ntrials;
                                    plot_data{(allindexes+1),(thresh+1)} = d;
                                end
                            end
                            %pair_thresholds = threshold{pair_number};   %can not save threshold because it is different for location and identity tasks
                            %cd(lfp2_dirs(ide_loc,:))
                            if Args.ml
                                channel_pairs = [ 'channelpair' num2strpad(groups(c1),2) num2strpad(groups(c2),2)]
                            else
                                channel_pairs = [ 'channelpair' num2strpad(c1,2) num2strpad(c2,2)]
                            end
                            
                            
                            %reduce data size
                            corrcoefs = single(corrcoefs);
                            phases = single(phases);
                            write_info = writeinfo(dbstack);
                            if ~Args.lite
                                save(channel_pairs,'corrcoefs','phases','correlograms','indexes','plot_data','write_info')%,'pair_thresholds')
                            else
                                save(channel_pairs,'corrcoefs','phases')%,'pair_thresholds')
                            end
                            cd ..
                            
                        end
                        
                        
                    end
                end
            end
        end
        
        if (Args.ide_only && mt.data.Index(1,1) ~= 2) || ~Args.ide_only || Args.ml
            
            if Args.generalized_gabor
                curdir = pwd;
                cd(ses)
                if Args.ml
                    fit_generalized_gabor('ml')
                else
                    fit_generalized_gabor
                end
                cd(curdir)
            end
            
            if Args.glm_train_predict
                curdir = pwd;
                cd(ses)
                if Args.ml
                    train_predict_stimulus('fits','stim_prediction','stims','euclid','block',1,'sorted')
                else
                    fprintf(1,'need to edit writeCPPdataML Args.glm_train_predict\n')
                end
                cd(curdir)
            end
            
            if Args.global_epoch_correlograms_surrogate
                curdir = pwd;
                cd(ses)
                if Args.ml
                    epoch_correlograms_GS('ml','monkey','betty')
                else
                    epoch_correlograms_GS
                end
                cd(curdir)
            end
            
            if Args.global_sta_surrogate
                curdir = pwd;
                cd(ses)
                if Args.ml
                    sta_global_surrogate('ml')
                else
                    sta_global_surrogate
                end
                
                cd(curdir)
            end
        end
        
        cd ..
    end
    
    if Args.ccg_surrogates
        avg_ccg_surrogate('ml',Args.ml,'monkey',Args.monkey,'windowSize',Args.windowSize)
    end
    
    if Args.clean
        cd(numb_sessions(Args.sessions).name)
        cd('lfp')
        cd('lfp2')
        !rm *_lfp2*
        % %         !rm channel_matrix.mat
    end
    
    if Args.delay_slopes
        cd ([daydir filesep cell2mat(switchdays(alldays))])
        delaySlopes(Args.ml)
        %euclidPairs
    end
    
    if Args.bh_glm
        
        if Args.ml
            cd([numb_sessions(1).name])
            cd('lfp')
            cd('lfp2')
            cd('identity')
            mts_glm_bh
            cd ..
            % % %             cd('location')
            % % %             mts_glm_bh
        else
            cd('lfp2')
            mts_glm_bh
        end
    end
    
    if Args.makeneuronalhist
        if Args.ml
            cd([numb_sessions(1).name])
            make_hist_info
        else
            cd ..
            make_hist_info
        end
    end
    
end

cd(daydir)

