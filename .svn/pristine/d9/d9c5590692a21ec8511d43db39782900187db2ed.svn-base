 How to do reverse correlation

			  Bruno A. Olshausen
			       12/11/01

The function revcorr_sn will compute a reverse correlation map from
the spike data collected in response to a sparse noise sequence.  The
input and output arguments are as follows:

  [R Pspike] =  revcorr_sn(session, cluster_num, init_info, noise_seq,
                           window_size, bin_size, eyepos);

You should read the help on revcorr_sn for further details on each of
the input and output arguments.  Here I will provide general instructions
on how to use the function and how to interpret the results.


Preparing data:
---------------

In order to run revcorr_sn, you must first generate a session
structure containing the spike data, an init_info structure containing
parameters of the stimulus, a noise_seq array containing the bar
presentation sequence, and an eye position array.  You then need to
decide on a window size, which specifies the amount of time prior to a
spike for computing the reverse correlation kernel, and the bin size,
which specifies the temporal precision.  The function then returns an
array R which gives the conditional probability of each bar
configuration (position, orientation, color) occuring prior to a spike
for each time bin.  It also returns the spike rate as the probability
of a spike occuring within a time bin.  You can display the array R
directly using imagesc, or you can use the functions play_revcorr_sn
or show_revcorr_sn to properly visualize the result.

To generate a session structure, use the function
GenerateSessionSpikeTrains.  For data collected prior to 9/1/01 (i.e.,
where stimulus latency is variable), you should correct the spike
times by first inferring the latency for each trial using the function
infer_latency and then passing the resulting dT array to shift_spikes.
For data collected after 9/1/01 these steps won't be necessary, but
you should make sure that the duration field of your session structure
reflects the *actual* stimulus and data acquisition duration.

To generate the init_info structure and noise_seq array, use the
functions read_init_info and read_seq, respectively.  To generate the
eye position array, use the function read_eyepos.  But note again that
for data collected prior to 9/1/01 you will need to pass in the
latency array dT in order to have the eye position correctly
registered with the spike data and stimulus sequence.

For the window size you will probably want to choose something between
100-200 ms.  For the bin size, probably best to use 1-2 ms.


Visualizing the result:
-----------------------

To quickly see if the results are sensible, you can display the array
R as an image using imagesc(R(1:M-1,:)), where M is the number of rows
of R.  Note that you want to skip the last row because it corresponds
to the bar being outside of the virtual grid (due to eye movements).

To make a static picture of the reverse correlation kernel as a
function of space and time, use the function show_revcorr_sn.  You
will need to pass a step size which specifies how many time bins to
clump into each snapshot of the kernel.  Use something like 10-20 for
this.  The function produces a series of arrays for each orientation
and bar color.  Each row corresponds to a different orientation,
ordered from top to bottom according to init_info.bar_orientation.
The top half are the results for dark bars and the bottom half is the
result for light bars.  Time proceeds from left to right, with the
spike occuring just to the right of the rightmost column.

To make a movie of the resulting kernel, use the function
play_revcorr_sn.  A reasonable threshold value is between 0.25-0.5.


Correcting for uneven coverage:
-------------------------------

Because of eye movements, in addition to the fact that only a fraction
of all bar configurations are presented in the first place, there will
not be an even coverage for each position, orientation, and bar color.
So, the results in R need to be judged with respect to the prior
probability that a bar occured at any given time bin prior to a spike.
The prior probability can be calculated by the function bar_prob,
which returns an array P that can be visualized using the same two
functions above.  By visually comparing P and R, you can tell whether
certain structures in the revcorr map (R) - for example, blank zones -
are caused by uneven coverage of positions/orientations/colors.

If you want to automatically "correct" for the uneven coverage, the
thing to do is divide R by P.  Remember that R gives you the
conditional probability of a bar proceeding a spike, P(b|s).  What we
really want to know is the efficacy of the stimulus, which is the
probability of a spike occuring after the presentation of a bar,
P(s|b).  This may be calculated using Bayes' rule:

                       P(b|s) P(s)
	     P(s|b) = -------------
                           P(b)

P(b) is the prior probability of the bar, which is the array P
returned by bar_prob.  P(s) is the probability of a spike occuring
within a given bin, which is just proportional to the spike rate
averaged over all trials - i.e., it is a constant.  Since we end up
renormalizing the display anyways to fill the colormap range, we can
ignore P(s) and simply divide P(b|s) by P(b) (i.e., divide R by P).
Note however that when P(b) is zero, you should either set the result
to zero, or else somehow visually indicate that it should be ignored.
Also, when P(b) is small, the result will be less significant than
when P(b) is large, so it may be desirable to show a "significance"
map that indicates this.

