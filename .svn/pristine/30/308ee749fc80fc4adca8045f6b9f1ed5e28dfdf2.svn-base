function processDaysPPI(days,svname,labels,varargin)

%enter a name, will be saved as namecombinedppi

% fonction qui lit les fichiers des jours que l'on souhaite �tudier, et
% cr�e trois vecteurs et une matrice :
% mice ID = identit�s des souris pour tous les jours
% doses = doses recues pour tous les jours
% Day = jour ou la souris a �t� test�e
% ppi = ppi des souris pour chaque intensit�

Args = struct('noreactivity',0,'onlyPP',0,'dosereponse',0,'stats', 0,'keepNonStartleMice',0,'medianStartlefold',3,'withLight',0,'sdB',[75 80 85 90]);
Args.flags = {'noreactivity','onlyPP','dosereponse','stats','keepNonStartleMice','withLight'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

miceID=[];
doses=[];
ppi=[];
Day=[];
startleAmp=[];
nullPeriodAmp=[];
pval = [];
ptab = [];
dose1 = [];

sdir=pwd;
% Rentre dans les dossiers des jours
for d=1:length(days)
    cd([sdir filesep days{d}])
    if isempty(nptDir('skip.txt')) % cas ou les donn�es n'ont pas pu �tre sauvergard�es
        processDayPPI('save',modvarargin{:})
        dateExp=str2num(days{d});
        sessions=nptDir('session*');
        % Rentre dans les sessions pour chaque jour
        for ss=1:length(sessions)
            cd(sessions(ss).name);
            if isempty(nptDir('skip.txt')) % cas ou les donn�es n'ont pas pu �tre sauvergard�es
                fnameID=nptDir('*LabID.txt');
                fileID=fopen(fnameID.name,'r');
                a=textscan(fileID,'%s');
                miceID=cat(1,miceID, a{1}(1,1),a{1}(3,1)); % cr�e un vecteur qui prend les identit�s des souris
                doses=cat(1,doses,str2double(a{1}(2,:)),str2double(a{1}(4,:))); % cr�e un vecteur qui prend les doses inject�es
                table=nptDir ('*TableTest.TAB*');
                data=nptDir('*rawdata*');
                [startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(data.name);
                Day=cat(1,Day,dateExp,dateExp);% cr�e un vecteur qui garde le jour de l'exp
                
                if ~Args.noreactivity
                    % Lecture des informations des fichiers
                    [infoTrial,defaultParams] = ReadInfoPPItxtFile(table.name,modvarargin{:});
                    [noPPAmpStartle,noPPAmpNull,intensities2] = getTrialsPPI(startle,infoTrial,defaultParams,chamberid,blockid,modvarargin{:});
                    startleAmp=cat(1,startleAmp,noPPAmpStartle);
                    nullPeriodAmp=cat(1,nullPeriodAmp,noPPAmpNull);
                    % startleAmpPPonly=cat(1,startleAmpPPonly, PPonlyStartle);
                    % %was supposed to be used to create dose response using
                    % PP_induced startles
                   if Args.dosereponse
                        [noPPAmpStartle,noPPAmpNull,intensities] = getTrialsPPI(startle,infoTrial,defaultParams,chamberid,blockid,modvarargin{:});
                    end
                    
                end
           end
            fclose all;
            cd .. ;
        end
    end
    %creates matrix of ppi
    fnamePPI=nptDir('*prctPPI.mat');
    prctPPI=load(fnamePPI.name);
    for k = 1:length(prctPPI.ppi)
        ppi=cat(1,ppi,transpose(prctPPI.ppi(:,k))); % cr�e une matrice avec en colonne les ppi correspondant � chaque intensit�
        cd(sdir)
    end
    
end

% supprime les lignes ou il n'y avait aucune souris dans les chambres

eliminate = find(strcmp('nomice',miceID) == 1);
miceID(eliminate) = [];
doses(eliminate)= [];
%Day(eliminate)=[];
if ~Args.dosereponse
    if Args.withLight
        ppi([eliminate eliminate+ size(ppi,1)/2],:)=[]; 
    else
    ppi(eliminate,:)=[];
    
    end
end
startleAmp(eliminate,:)=[];
nullPeriodAmp(eliminate,:)=[];

if Args.withLight; miceID = [miceID; miceID];doses = [doses; doses + max(doses) + 1];startleAmp = [startleAmp;startleAmp]; nullPeriodAmp = [nullPeriodAmp; nullPeriodAmp];end
% supprime les lignes ou il n'y a aucune donn�e (NaN)
baddata = find(sum(isnan(ppi),2) ~= 0);
miceID(baddata) = [];
doses(baddata)= [];
%Day(baddata)=[];
if ~Args.dosereponse
    if Args.withLight
        ppi([baddata baddata + size(ppi,1)],:)=[];  
    else
    ppi(baddata,:) = [];
    end
end
startleAmp(baddata,:) = [];
nullPeriodAmp(baddata,:) = [];

% excludes animals who do not differ significantly in startles with or w/o
% pulse
miceIDExcl = [];
miceExcl = [];
if ~Args.keepNonStartleMice
   medianStartle =  median(startleAmp./nullPeriodAmp,2);
    
    excl = find(medianStartle < Args.medianStartlefold);
    if Args.withLight
%         rind = [];
%         for pp = 1 : size(ppi,1)/4; rind = [rind (pp-1)*4 + [1 2]]; end
%         for pp = 1 : size(ppi,1)/4; rind = [rind (pp-1)*4 + [3 4]]; end
%         ppi = ppi(rind,:);
%         ppi([excl excl + size(ppi,1)/2],:) = [];
        ppi(excl,:) = [];
        startleAmp(excl, :) = [];
        miceIDExcl = miceID(excl);
        miceID(excl) = [];
        
        doses(excl)= [];
        
        nullPeriodAmp(excl,:)=[];
        
        miceExcl = excl;
    else
        ppi(excl, :) = [];
        startleAmp(excl, :) = [];
        miceIDExcl = miceID(excl);
        miceID(excl) = [];
        doses(excl)= [];
        nullPeriodAmp(excl,:)=[];
        
        miceExcl = excl;
    end
    
end
display('Saving data')
% sauvegarde d'un fichier combin�
if Args.onlyPP
    save(strcat(days{d},'PPonly'),'doses','startleAmpPPonly', 'intensities')
end

if Args.dosereponse
    save(strcat(days{d},'DoseRep'),'doses','startleAmp', 'intensities')
end

sdB = Args.sdB;
save(sprintf('%scombinedDataPPI',svname),'Day','miceID','doses','ppi','startleAmp','nullPeriodAmp','intensities2','ptab','miceIDExcl','miceExcl','labels','sdB')
display(['saving ' pwd sprintf('%scombinedDataPPI',svname)])



