
Baldwin, Charlie,

Here's the scoop on timing for sparse noise trials.  Note that it is
necessary to understand the full interaction between the Control computer
and the Presenter computer in order to make sense of the timing signals
that will result from any latency/stimulus duration combination.  The
steps which I have outlined below are applicable only for sparse noise 
trials using bars with the SyncCounterCheckBox checked, and where data
acquisition begins and ends upon Control's trigger.  Any deviations
from this could completely change the resulting timing signals.  My
recommendation is that we outline these steps for WINDOW_MOVIE trials
as well, especially if we wish to initiate data acquisition using the
Control trigger instead of the Presenter trigger (as we are doing 
currently).  To proceed with collecting further data without a full 
understanding of how the system functions is just asking for more
trouble.

Ok, with that said, here's the situation for sparse noise trials:

On the Control computer, the order of events (from the SessionCode.h
file, under "Fixation Paradigm Instructions") is

  1. Show fixation mark.
  2. Wait for vertical sync.
  3. Send data acquisition trigger.
  4. Start timer with stimulus latency.
  4. Wait for timer to expire (while fixation maintained).
  5. Send SHOW STIMULUS to Presenter computer.
  6. Start timer with stimulus duration.
  7. Wait for acknowledgement from Presenter (from SHOW STIMULUS command).
  8. Wait for timer to expire (while fixation maintained).
  9. Send HIDE STIMULUS to Presenter computer.
 10. Start timer with post-stimulus duration.
 11. Wait for acknowlegement from Presenter (from HIDE STIMULUS command).
 12. Wait for timer to expire (while fixation maintained).
 13. Send data acquisition trigger (stops acquisition)
 14. Hide fixation mark.

Now, on the Presenter computer, the messages from the Control computer
are caught within the main event loop (called the IdleLoop in 
ReverseCorrelation.bpp).  When Presenter receives the SHOW STIMULUS
command (STIM_SHOW_STIM), it does the following (among other things):

  1. Set startDAQnextframe=1.
  2. Set ShowMovie=1.
  3. Decrement FrameNumber (since it is incremented by LoadNextFrame).
  4. Send confirmation to Control.
  5. Call LoadNextFrame().

And from then on, until another message is received from the Control 
computer, the IdleLoop will keep calling LoadNextFrame() as long as 
ShowMovie==1.

LoadNextFrame does the following:

  1. Increment FrameNumber (the frame counter).
  2. If this is the last frame of the sequence, set ShowMovie=0 (among
     other things).
  3. If this is the first frame of the sequence, zero out the vertical sync
     counter (CommModule->ElapsedSyncs).
  4. Set syncs = CommModule->ElapsedSyncs (this also clears sync counter)
  5. Set number_of_refreshes = RefreshesPerFrame - syncs.
  6. If startDAQnextframe==1, wait for [number_of_refreshes-1] vertical 
     syncs and then issue Presenter's data acquisition trigger.
     Otherwise, wait for [number_of_refreshes] vertical syncs.
  7. If FrameNumber!=0, erase old object.
  8. If this frame is -1 -1 -1 -1, wait for 
     [NumSpontaneousTrialsPerBlock*(RefreshesPerFrame + ISI)] vertical 
     syncs and increment FrameNumber.
  9. Blt the frame.
 10. Blt the fixation mark.
 11. Clear sync counter and do accounting of scanlines and skipped syncs.

When Presenter catches the HIDE STIMULUS command from the Control computer
in its IdleLoop (STIM_HIDE_STIM), it does the following:

  1. Set ShowMovie=0.
  2. Call ShowFix() (blanks screen but keeps fixation point).
  3. Issue Presenter's data acquisition trigger (stop acquisition).
  4. Send confirmation to Control.

Ok, so how do all of these parts work together to produce the timing
signals we observe?

Let's say this is the first trial of a sequence.  Then FrameNumber will
equal 0 in LoadNextFrame (after being incremented, since it is decremented
beforehand) and so the sync counter will be cleared.  Thus, 
number_of_refreshes will be set to 3 (the value of RefreshesPerFrame).  
And since startDAQnextframe is 1, we will wait for [number_of_refreshes-1] 
or 2 vertical syncs before issuing the Presenter trigger (which will
actually wait for the next vertical sync to go - taken care of by
the counter/timer board in Presenter).  The frame is then blt'd, and
will actually be displayed in the next vertical sync as well.  The
sync counter is then cleared.  Subsequent calls to LoadNextFrame will 
be made immediately afterwards.  For each subsequent call, we will wait 
for three vertical syncs, minus however many have elapsed since the
end of the last LoadNextFrame, and then quickly erase the last 
frame and blt the current frame.  This will continue until the 
IdleLoop catches Control's HIDE STIMULUS command, upon which it will
issue the Presenter trigger and send an acknowledgement to Control,
allowing it to issue its stop acquisition trigger.  The total
data acquisition period will then be

  [floor(lat/T) + 2 + 1 + ceil((dur-3)/(3T))*3 + 1]*T

and the *actual* stimulus latency will be

  [floor(lat/T) + 2 + 1]*T
  
where 'lat' is the stimulus latency, 'dur' is the stimulus duration,
'T' is the vertical refresh period, and 'floor' and 'ceil' mean 
'round-down' and 'round-up' respectively.

Now, if this is the second trial, there will have been a substantial
number of vertical syncs elapsed since the last LoadNextFrame, and
so number_of_refreshes will most likely be a negative number.  So
now there is no wait before issuing Presenter's trigger, and so it
will occur on the next vertical sync.  The first frame of this trial
will also be displayed upon the next vertical sync.  LoadNextFrame
will then be called again, but this time it will wait three vertical
syncs before displaying the next frame.  This will continue again
until the IdleLoop catches Control's HIDE STIMULUS command, upon
which it issues the Presenter trigger and sends the acknowledgement
to Control allowing it to issue its stop trigger.  The total data
acquistion period is then

  [floor(lat/T) + 1 + ceil((dur-1)/(3T))*3 + 1]*T

and the *actual* stimulus latency will be

  [floor(lat/T) + 1]*T

These two situations are illustrated on timing diagrams which I have
given to Baldwin.

Bruno

-- 
Bruno A. Olshausen              Phone: (530) 757-8749
Center for Neuroscience         Fax:   (530) 757-8827
UC Davis                        Email: baolshausen@ucdavis.edu
1544 Newton Ct.                 WWW:   http://redwood.ucdavis.edu/bruno
Davis, CA 95616


From Aug. 20-Sept. 22:

Center for Computational Biology
1 Lewis Hall
Montana State University
Bozeman, Montana  59717

Phone: (406) 994-7190
Fax:  (406) 994-7438
