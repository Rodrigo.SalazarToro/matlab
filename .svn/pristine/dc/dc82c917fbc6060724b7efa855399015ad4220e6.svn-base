function superposition = TemplateOverlap(means)
%superposition = TemplateOverlap(means)
%
%function to find all pairwise superpositions at every latency for a set of
%templates.  Each template is a 32 datapoint extraction of a waveform as
%used by MClust.

superposition.waveform=[];
superposition.clus1=[];
superposition.clus2=[];
superposition.trigger=[];
superposition.shift1=[];
superposition.shift2=[];

global Scaling
if  isempty(Scaling) || Scaling.Template == 0
    Scaling.Min = 0;
    Scaling.Max = 0;
    Scaling.Step = 100;
    Scaling.Template = 0;
end
stpsz =1;
for clus1 = 1:size(means,1)-1
    for clus2 =clus1+1:size(means,1)
        fprintf('Superimposing Cluster %d and %d ...\n',clus1,clus2)
        %%%%%%%%%%%%for the addition
%         for i = 1: 2
%             stepjitter(i) = 0.3*max(abs(means(i,:))); % step jitter by  addition          
%         end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for jitter1 = (100+Scaling.Min)/100 : Scaling.Step /100 : (100+Scaling.Max)/100 % jitter for the scaling of the template
            for jitter2 =  (100+Scaling.Min)/100 : Scaling.Step /100 : (100+Scaling.Max)/100 % jitter for the scaling of the template
                %                 for jitter1 = -1 : 1 %jitter for teh addition
                %                     for jitter2 = -1 : 1 % jitter for the addition
                meanst = means;
                meanst(clus1,:) = means(clus1,:) * jitter1;
                meanst(clus2,:) = means(clus2,:) * jitter2;
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %Code to jitter the two templates independently by addition
%                 if means(:,33:end) == zeros(size(means(:,33:end)))
%                     meanst = means + [repmat([jitter1*stepjitter(1); jitter2*stepjitter(2)],1,32) zeros(size(means(:,33:end)))];  
%                 else
%                     meanst = means + repmat([jitter1*stepjitter(1); jitter2*stepjitter(2)],1,size(means,2));
%                 end
                %%%%%%%%%%%

                %create overlap clusters
                zz1=convmtx(meanst(clus1,1:32),32);
                zz1=zz1(1:stpsz:32,:);
                zz2=convmtx(meanst(clus1,33:64),32);
                zz2=zz2(1:stpsz:32,:);
                zz3=convmtx(meanst(clus1,65:96),32);
                zz3=zz3(1:stpsz:32,:);
                zz4=convmtx(meanst(clus1,97:128),32);
                zz4=zz4(1:stpsz:32,:);
                zz = [zz1 zz2 zz3 zz4];
                zz=repmat(zz,32/stpsz,1);
                %         figure
                %         surf(zz)
                %         title(['zz - Cluster ' num2str(clus1) ' & ' num2str(clus2)])
                
                qq1=convmtx(meanst(clus2,1:32),32);
                qq1=qq1(1:stpsz:32,:);
                qq2=convmtx(meanst(clus2,33:64),32);
                qq2=qq2(1:stpsz:32,:);
                qq3=convmtx(meanst(clus2,65:96),32);
                qq3=qq3(1:stpsz:32,:);
                qq4=convmtx(meanst(clus2,97:128),32);
                qq4=qq4(1:stpsz:32,:);
                
                qq=[];
                for ii=1:32/stpsz
                    qqlat = [qq1(ii,:) qq2(ii,:) qq3(ii,:) qq4(ii,:)];
                    qq=[qq;repmat(qqlat,32/stpsz,1)];
                end
                
                %            figure
                %            surf(qq)
                %        title(['qq - Cluster ' num2str(clus1) ' & ' num2str(clus2)])
                
                
                
                
                s=zz+qq;
                
                
                
                %(1,1) (1,2)  (1,3) (1,4) ...(1,32)
                %(2,1) (2,2)  (2,3) (2,4)
                %(3,1) (3,2)  (3,3) (3,4)      .
                %(4,1) (4,2)  (4,3) (4,4)      .
                %  .                           .
                %  .
                %  .
                %(32,1)        ...          (32,32)
                %above are all possible latency combinations but we don't 
                %need them all.  Instead we take the 1st row, 1st 
                %column, last row and last column.  This would normally 
                %be overkill (only need 1st row and 1st column) but because
                %Extructor blocks out peaks at the begginning and the end of the
                %data stream the other row and column are needed because they
                %are padded with zeros in different locations.  But in the
                %variable s, each column is concatonated into a single column so
                %the indexing is as follows:
                
                
                % 1st column         %% 1st row                 %                                          
                %                                               %
                %                                               %
                %                                               % last column                %%last row
                d =  [s(1:32,:) ; s(33:32:size(s,1)-32,:) ;  s(size(s,1)-31:size(s,1),:)  ;  s(32*2:32:size(s,1)-32,:)   ];
                dshift1 = [1:32             ones(1,30)                    1:32                            32*ones(1,30)       ]';
                dshift2 = [ ones(1,32)        2:31                       32*ones(1,32)                       2:31             ]';
                %dshift1-dshift2=dlat YES!!!   
                dlat = transpose([(0:31)     (-1:-1:-30)                 (-31:1:0)                          (30:-1:1)   ]);
                
                %        figure
                %        surf(s)
                %        title(['All Superpositions of Clusters ' num2str(clus1) ' & ' num2str(clus2) ' before extraction'])
                
                
                %break up s
                s1=d(:,1:63);
                s2 = d(:,64:126);
                s3 = d(:,127:189);
                s4 = d(:,190:252);
                
                dd=[];
                trigger=[];
                shift1=[];
                shift2=[];        
                for ii=1:size(d,1)
                    data=[s1(ii,:);s2(ii,:);s3(ii,:);s4(ii,:)];
                    [waveforms,times]=nptExtructor(data,[0 0 0 0],abs(.25*min(min(data)))*ones(1,4),30000);
                    times = ceil(times*30000/1000000);  %now this is the trigger point
                    trigger = [trigger; times];
                    dd = [dd ; waveforms];
                    shift1 = [shift1 ; repmat(dshift1(ii),size(waveforms,1),1)];
                    shift2 = [shift2 ; repmat(dshift2(ii),size(waveforms,1),1)];
                    %                  times
                    %                  dlat(ii)
                    %                  display('shift1  ');shift1(ii)
                    %                  display('shift2   ');shift2(ii)
                    %                  figure;plot(data(1,:));hold on;plot(data(2,:),'r');plot(data(3,:),'g');plot(data(4,:),'k');legend('1','2','3','4')
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Rodrigo (04.31.05)
                    % To add waveforms at
                    % the trigger of the templates in order to have them
                    % even if they do not create any bumps
                    
                    for dshift = [dshift1(ii) dshift2(ii)] % to set the trigger on the two peak of the template
                        waveforms = [data(1,dshift:dshift+31) data(2,dshift:dshift+31) data(3,dshift:dshift+31) data(4,dshift:dshift+31)];
                        times = dshift + 10;
                        trigger = [trigger; times];
                        dd = [dd ; waveforms];
                        shift1 = [shift1 ; repmat(dshift1(ii),size(waveforms,1),1)];
                        shift2 = [shift2 ; repmat(dshift2(ii),size(waveforms,1),1)];  
                    end
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                end
                
                
                [dd,i] = unique(dd,'rows');
                trigger = trigger(i);
                shift1 = shift1(i);
                shift2 = shift2(i);
                
                %         figure
                %         surf(dd)
                %         title(['All Superpositions of Clusters ' num2str(clus1) ' & ' num2str(clus2) ' after extraction'])
                
                
                
                superposition.waveform = [superposition.waveform ; dd];
                superposition.clus1 = [superposition.clus1 ; repmat(clus1,size(dd,1),1)];
                superposition.clus2 = [superposition.clus2 ; repmat(clus2,size(dd,1),1)];
                superposition.trigger = [superposition.trigger ;trigger];
                superposition.shift1 = [superposition.shift1 ;shift1];
                superposition.shift2 = [superposition.shift2 ;shift2];
            end
        end
    end
end



