function mvpa_power_analysis_bmf(varargin)


%run at session level
Args = struct('compute_trial_power',0);
Args.flags = {'compute_trial_power'};
Args = getOptArgs(varargin,Args);


sesdir = pwd;
mt = mtstrial('auto','ML','RTfromML','redosetNames','Nlynx','save','redo');
alltrials = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial');
ntrials = size(alltrials,2);
%get trial for each stimulus
for xt = 1:5
    identity{xt} = mtsgetTrials(mt,'BehResp',1,'stable','ML','NoCharlie',1,'notfromMTStrial','iCueObj',xt);
    [~,ist] = intersect(alltrials,identity{xt});
    stim_list(ist) = xt;
end



g = bmf_groups;
N = NeuronalHist('bmf');

[~,good_ch] = intersect(N.gridPos,g);
nch = size(good_ch,2);
cd([sesdir filesep 'lfp' filesep 'power'])
powertrials = nptDir('*power.*');

if Args.compute_trial_power
    ecounter = 0;
    for epoch = 1:4
        ecounter = ecounter + 1;
        chcounter = 0;
        for cch = good_ch
            chcounter = chcounter +1;
            %make data matrix
            tcounter = 0;
            for nta = alltrials
                tcounter = tcounter + 1;
                load(powertrials(nta).name)
                
                %             trial_power1_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,2:7);
                %             trial_power2_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,8:13);
                %             trial_power3_abs{ecounter}(tcounter,chcounter) = power.S{ecounter}(cch,14:19);
                
                normpower = power.S{ecounter}(cch,1:22) .* power.f{ecounter}(1:22)';
                totalpower = sum(normpower);
                npower = normpower ./ totalpower;
                trial_power1_norm_stims{ecounter}(tcounter,chcounter) = sum(npower(2:8)); %1.6:11
                trial_power2_norm_stims{ecounter}(tcounter,chcounter) = sum(npower(9:15)); %12.5 : 21.9
                trial_power3_norm_stims{ecounter}(tcounter,chcounter) = sum(npower(16:22)); %23.4 : 32.8
                
% % %                 trial_power3_norm_stims{ecounter}(tcounter,chcounter) = sum(power.S{ecounter}(cch,53:59)); %23.4 : 32.8
            end
        end
    end
    save trial_power_stims trial_power1_norm_stims trial_power2_norm_stims trial_power3_norm_stims
else
    load trial_power_stims trial_power1_norm_stims trial_power2_norm_stims trial_power3_norm_stims
end

alltgroups = zeros(1,ntrials);

for areas = 1:7
    if ~isempty(find(N.level2(good_ch) == areas)) || areas == 7
        for nt = 1 : ntrials
            
            test_g = stim_list(nt);
            tgroups = alltgroups;
            tgroups(find(stim_list == test_g)) = 1;
            tgroups(nt) = [];
            
            %set test_g to 1
            test_g = 1;
            
            tp1e1 = trial_power1_norm_stims{1};
            tp2e1 = trial_power2_norm_stims{1};
            tp3e1 = trial_power3_norm_stims{1};
            
            tp1e2 = trial_power1_norm_stims{2};
            tp2e2 = trial_power2_norm_stims{2};
            tp3e2 = trial_power3_norm_stims{2};
            
            tp1e3 = trial_power1_norm_stims{3};
            tp2e3 = trial_power2_norm_stims{3};
            tp3e3 = trial_power3_norm_stims{3};
            
            tp1e4 = trial_power1_norm_stims{4};
            tp2e4 = trial_power2_norm_stims{4};
            tp3e4 = trial_power3_norm_stims{4};
            
            
            %get test data (epoch 1)
            testtp1e1 = tp1e1(nt,:);
            testtp2e1 = tp2e1(nt,:);
            testtp3e1 = tp3e1(nt,:);
            
            %get test data (epoch 2)
            testtp1e2 = tp1e2(nt,:);
            testtp2e2 = tp2e2(nt,:);
            testtp3e2 = tp3e2(nt,:);
            
            %get test data (epoch 3)
            testtp1e3 = tp1e3(nt,:);
            testtp2e3 = tp2e3(nt,:);
            testtp3e3 = tp3e3(nt,:);
            
            %get test data (epoch 4)
            testtp1e4 = tp1e4(nt,:);
            testtp2e4 = tp2e4(nt,:);
            testtp3e4 = tp3e4(nt,:);
            
            
            %GET RID OF THE CURRENT TRIAL
            tp1e1(nt,:) = [];
            tp2e1(nt,:) = [];
            tp3e1(nt,:) = [];
            
            tp1e2(nt,:) = [];
            tp2e2(nt,:) = [];
            tp3e2(nt,:) = [];
            
            tp1e3(nt,:) = [];
            tp2e3(nt,:) = [];
            tp3e3(nt,:) = [];
            
            tp1e4(nt,:) = [];
            tp2e4(nt,:) = [];
            tp3e4(nt,:) = [];
            
            
            
            % % % % % % % %     tgroups(randperm(size(tgroups,2))) = tgroups;
            
            if areas == 7
                aa = 1 :  nch;
            else
                aa = find(N.level2(good_ch) == areas);
            end
            
            try
                SVMstruct1e1 = svmtrain(tp1e1(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct2e1 = svmtrain(tp2e1(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct3e1 = svmtrain(tp3e1(:,aa),tgroups,'Kernel_Function','mlp');
                
                SVMstruct1e2 = svmtrain(tp1e2(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct2e2 = svmtrain(tp2e2(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct3e2 = svmtrain(tp3e2(:,aa),tgroups,'Kernel_Function','mlp');
                
                SVMstruct1e3 = svmtrain(tp1e3(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct2e3 = svmtrain(tp2e3(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct3e3 = svmtrain(tp3e3(:,aa),tgroups,'Kernel_Function','mlp');
                
                
                SVMstruct1e4 = svmtrain(tp1e4(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct2e4 = svmtrain(tp2e4(:,aa),tgroups,'Kernel_Function','mlp');
                SVMstruct3e4 = svmtrain(tp3e4(:,aa),tgroups,'Kernel_Function','mlp');
                
                
                %1:12
                %13:26
                %37:39
                newClasses1e1 = svmclassify(SVMstruct1e1,testtp1e1(:,aa));
                newClasses2e1 = svmclassify(SVMstruct2e1,testtp2e1(:,aa));
                newClasses3e1 = svmclassify(SVMstruct3e1,testtp3e1(:,aa));
                
                newClasses1e2 = svmclassify(SVMstruct1e2,testtp1e2(:,aa));
                newClasses2e2 = svmclassify(SVMstruct2e2,testtp2e2(:,aa));
                newClasses3e2 = svmclassify(SVMstruct3e2,testtp3e2(:,aa));
                
                newClasses1e3 = svmclassify(SVMstruct1e3,testtp1e3(:,aa));
                newClasses2e3 = svmclassify(SVMstruct2e3,testtp2e3(:,aa));
                newClasses3e3 = svmclassify(SVMstruct3e3,testtp3e3(:,aa));
                
                newClasses1e4 = svmclassify(SVMstruct1e4,testtp1e4(:,aa));
                newClasses2e4 = svmclassify(SVMstruct2e4,testtp2e4(:,aa));
                newClasses3e4 = svmclassify(SVMstruct3e4,testtp3e4(:,aa));
                
            catch
                newClasses1e1 = 2
                newClasses2e1 = 2
                newClasses3e1 = 2
                
                newClasses1e2 = 2
                newClasses2e2 = 2
                newClasses3e2 = 2
                
                newClasses1e3 = 2
                newClasses2e3 = 2
                newClasses3e3 = 2
                
                newClasses1e4 = 2
                newClasses2e4 = 2
                newClasses3e4 = 2
            end
            
            %epoch 1
            if newClasses1e1 == test_g
                allperformance1(1,nt) = 1;
            elseif newClasses1e1 == 2
                allperformance1(1,nt) = 2;
            else
                allperformance1(1,nt) = 3;
            end
            
            if newClasses2e1 == test_g
                allperformance2(1,nt) = 1;
            elseif newClasses2e1 == 2
                allperformance2(1,nt) = 2;
            else
                allperformance2(1,nt) = 3;
            end
            
            if newClasses3e1 == test_g
                allperformance3(1,nt) = 1;
            elseif newClasses3e1 == 2
                allperformance3(1,nt) = 2;
            else
                allperformance3(1,nt) = 3;
            end
            
            
            
            %epoch 2
            if newClasses1e2 == test_g
                allperformance1(2,nt) = 1;
            elseif newClasses1e2 == 2
                allperformance1(2,nt) = 2;
            else
                allperformance1(2,nt) = 3;
            end
            
            if newClasses2e2 == test_g
                allperformance2(2,nt) = 1;
            elseif newClasses2e2 == 2
                allperformance2(2,nt) = 2;
            else
                allperformance2(2,nt) = 3;
            end
            
            if newClasses3e2 == test_g
                allperformance3(2,nt) = 1;
            elseif newClasses3e2 == 2
                allperformance3(2,nt) = 2;
            else
                allperformance3(2,nt) = 3;
            end
            
            
            %epoch 3
            
            if newClasses1e3 == test_g
                allperformance1(3,nt) = 1;
            elseif newClasses1e3 == 2
                allperformance1(3,nt) = 2;
            else
                allperformance1(3,nt) = 3;
            end
            
            if newClasses2e3 == test_g
                allperformance2(3,nt) = 1;
            elseif newClasses2e3 == 2
                allperformance2(3,nt) = 2;
            else
                allperformance2(3,nt) = 3;
            end
            
            if newClasses3e3 == test_g
                allperformance3(3,nt) = 1;
            elseif newClasses3e3 == 2
                allperformance3(3,nt) = 2;
            else
                allperformance3(3,nt) = 3;
            end
            
            
            %epoch 4
            
            if newClasses1e4 == test_g
                allperformance1(4,nt) = 1;
            elseif newClasses1e4 == 2
                allperformance1(4,nt) = 2;
            else
                allperformance1(4,nt) = 3;
            end
            
            if newClasses2e4 == test_g
                allperformance2(4,nt) = 1;
            elseif newClasses2e4 == 2
                allperformance2(4,nt) = 2;
            else
                allperformance2(4,nt) = 3;
            end
            
            if newClasses3e4 == test_g
                allperformance3(4,nt) = 1;
            elseif newClasses3e4 == 2
                allperformance3(4,nt) = 2;
            else
                allperformance3(4,nt) = 3;
            end
        end
        
        
        
        %performance low
        f1_1 = size(find(allperformance1(1,:) == 1),2);
        f1_2 = size(find(allperformance1(1,:) == 2),2);
        f1_3 = size(find(allperformance1(1,:) == 3),2);
        
        performance1_stims(areas,1) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge1_stims(areas,1) = f1_2;
        
        f1_1 = size(find(allperformance1(2,:) == 1),2);
        f1_2 = size(find(allperformance1(2,:) == 2),2);
        f1_3 = size(find(allperformance1(2,:) == 3),2);
        
        performance1_stims(areas,2) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge1_stims(areas,2) = f1_2;
        
        f1_1 = size(find(allperformance1(3,:) == 1),2);
        f1_2 = size(find(allperformance1(3,:) == 2),2);
        f1_3 = size(find(allperformance1(3,:) == 3),2);
        
        performance1_stims(areas,3) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge1_stims(areas,3) = f1_2;
        
        f1_1 = size(find(allperformance1(4,:) == 1),2);
        f1_2 = size(find(allperformance1(4,:) == 2),2);
        f1_3 = size(find(allperformance1(4,:) == 3),2);
        
        performance1_stims(areas,4) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge1_stims(areas,4) = f1_2;
        
        
        %performance middle
        f1_1 = size(find(allperformance2(1,:) == 1),2);
        f1_2 = size(find(allperformance2(1,:) == 2),2);
        f1_3 = size(find(allperformance2(1,:) == 3),2);
        
        performance2_stims(areas,1) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge2_stims(areas,1) = f1_2;
        
        f1_1 = size(find(allperformance2(2,:) == 1),2);
        f1_2 = size(find(allperformance2(2,:) == 2),2);
        f1_3 = size(find(allperformance2(2,:) == 3),2);
        
        performance2_stims(areas,2) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge2_stims(areas,2) = f1_2;
        
        f1_1 = size(find(allperformance2(3,:) == 1),2);
        f1_2 = size(find(allperformance2(3,:) == 2),2);
        f1_3 = size(find(allperformance2(3,:) == 3),2);
        
        performance2_stims(areas,3) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge2_stims(areas,3) = f1_2;
        
        f1_1 = size(find(allperformance2(4,:) == 1),2);
        f1_2 = size(find(allperformance2(4,:) == 2),2);
        f1_3 = size(find(allperformance2(4,:) == 3),2);
        
        performance2_stims(areas,4) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge2_stims(areas,4) = f1_2;
        
        
        
        %performance high
        f1_1 = size(find(allperformance3(1,:) == 1),2);
        f1_2 = size(find(allperformance3(1,:) == 2),2);
        f1_3 = size(find(allperformance3(1,:) == 3),2);
        
        performance3_stims(areas,1) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge3_stims(areas,1) = f1_2;
        
        f1_1 = size(find(allperformance3(2,:) == 1),2);
        f1_2 = size(find(allperformance3(2,:) == 2),2);
        f1_3 = size(find(allperformance3(2,:) == 3),2);
        
        performance3_stims(areas,2) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge3_stims(areas,2) = f1_2;
        
        f1_1 = size(find(allperformance3(3,:) == 1),2);
        f1_2 = size(find(allperformance3(3,:) == 2),2);
        f1_3 = size(find(allperformance3(3,:) == 3),2);
        
        performance3_stims(areas,3) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge3_stims(areas,3) = f1_2;
        
        f1_1 = size(find(allperformance3(4,:) == 1),2);
        f1_2 = size(find(allperformance3(4,:) == 2),2);
        f1_3 = size(find(allperformance3(4,:) == 3),2);
        
        performance3_stims(areas,4) = round((f1_1 / (f1_1 + f1_3)) * 100)
        noconverge3_stims(areas,4) = f1_2;
    end
end


cd(sesdir)
save performance1_stims performance1_stims noconverge1_stims
save performance2_stims performance2_stims noconverge2_stims
save performance3_stims performance3_stims noconverge3_stims