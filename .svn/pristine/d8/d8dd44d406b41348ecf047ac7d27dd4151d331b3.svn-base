function [sumpop,raster,sumpopsur,thres,sigframe,varargout] = popPsth(onsets,varargin)
%
% output:
% sumpop : time serie of activated cells (blue in the plot)
% raster: 
% sumpopsur: surrogate of sumpop
% thres: threshold which to the max of the surrogates (red in teh plot)
% sigframe : percent number of significant frames.
% sigcell: vector of number of coactivated cells

Args = struct('iterations',10000,'plot',0,'save',0,'redo',0,'savename',[],'savefig',0);
Args.flags = {'plot','save','redo','savefig'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

if isempty(Args.savename)
    savename = 'popPsth.mat';
else
    savename = Args.savename;
end

if isempty(nptDir(savename)) || Args.redo
    ncell = length(onsets);
    maxframe = max(cell2mat(onsets')) + 1;
    
    temp = hist(cell2mat(onsets'),[-1 : 1 : maxframe]);
    sumpop=temp(2:end-1);
    raster = zeros(ncell,length(sumpop));
    for n = 1 : ncell
        raster(n,:) = hist(onsets{n},[0:maxframe - 1]);
        
    end
    %% surrogate
    sumpopsur = zeros(Args.iterations,size(sumpop,2));
    sur = cell(1,ncell);
    for ii = 1 : Args.iterations
        shift = randi(maxframe - 2,ncell,1);
        
        for n = 1 : ncell
            sur{n} = mod(onsets{n} + shift(n),maxframe);
            
        end
        temp = hist(cell2mat(sur'),[-1 : 1 : maxframe]);
        sumpopsur(ii,:) = temp(2:end-1);
    end
    thres = max(sumpopsur,[],1);
    
    
    sigframe = 100*sum(sumpop > thres) / maxframe;
    sigcell = sumpop(sumpop > thres);
    
    varargout{1} = sigcell;
    varargout{2} = sur;
    if Args.save
        
       save(savename,'sumpop','raster','sumpopsur','thres','sigframe','sigcell');
    end
else
    
    load(savename);
    
end


if Args.plot
    f1 = figure;
    subplot(3,1,1);
    imagesc(raster);
    colormap('gray')
    ylabel('cell #')
    
    subplot(3,1,2);
    plot(100*sumpop/ncell)
    hold on
    plot(100*thres/ncell','r')
    
    legend('population activity','random process')
    xlabel('frame #')
    ylabel('% cell')
    
    subplot(3,1,3)
    
    hist(100*sigcell/ncell,[0 :100]);
    
    xlabel('% cell coactivated')
    ylabel('count')
    if Args.savefig
        
       saveas(f1,'popPsth');
       close(f1)
    end
end