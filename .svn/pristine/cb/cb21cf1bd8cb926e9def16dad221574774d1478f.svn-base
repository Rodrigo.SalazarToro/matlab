%PlotEyeMovementHistograms
%This is a SCRIPT that plots histograms of combined eye movement data for a
%number of sessions.  Eye data for EVERY session inside the parent
%directory is included.  In my particular directory structure, I enter the
%animal name directory (which is inside the "paradigm type" folder), and
%run this script.
%The following 8 histograms are created:  
%   a) The Main Sequence
%   b) Saccade and Fixation Durations
%   c) Saccade and Fixation Amplitudes
%   d) Saccade and Fixation Maximum Velocities
%   e) Fixation Mean Velocity

%From reviewing the data of multiple animals, certain limits were
%determined to be useful in comparing data across animals.  In order to use
%these limits, have a variable called "limit" in your workspace prior to
%calling the script.  Here are the appropriate values for it:

%   limit == 0 implies that no limits should be used in plotting the data
%   limit == 1 implies that free-viewing (FV) limits should be used
%   limit == 2 implies that fixational (fix) limits should be used

%The plot limits that are used are as follows:
%   Saccade Amplitudes:  Fix = 2 deg; FV = 30 deg
%   Saccade Velocities:  FV = 1000 deg/sec
%   Saccade Durations:  FV = 100 ms
%   Fixation Amplitudes:  FV = 1 deg
%   Peak Fixation Velocities:  Fix = 10 deg/sec and FV = 15 deg/sec
%   Mean Fixation Velocities:  Fix = 4 deg/sec
%   Fixation Durations:  FV = 1000 ms

%Several variables are also created which contain all of the data.  Each
%will have as many rows as there are sessions in the parent directory.

%Lastly, in calling the script, make sure that you've changed the animal
%names of the plot titles to the correct one.  I simply do a "Find and
%Replace" for the current name and replace with the new one.

nd = ProcessSite(nptdata, 'nptSessionCmd', 'robj = nptdata(0, 0, ''SessionDirs'', {pwd});')
if ~exist('limit', 'var')
    limit = 0;
end

[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_positionrange.mat''); load(a.name); data = concatenate(data, position_range.saccade, ''DiscardEmptyA'');');
sacPRdata = data;
if limit == 2
    [i, j] = find(sacPRdata > 2);
    sacPRdata(i, j) = NaN;
end
[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_positionrange.mat''); load(a.name); data = concatenate(data, position_range.fixation, ''DiscardEmptyA'');');
fixPRdata = data;
fix = reshape(fixPRdata, 1, (size(fixPRdata, 1) * size(fixPRdata, 2)));
sac = reshape(sacPRdata, 1, (size(sacPRdata, 1) * size(sacPRdata, 2)));
fixationPR = fix(~isnan(fix));
saccadePR = sac(~isnan(sac));

[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_velocity.mat''); load(a.name); data = concatenate(data, velocity.fixation, ''DiscardEmptyA'');');
fixMeanVdata = data([1:2:end], :);
fixMaxVdata = data([2:2:end], :);
[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_velocity.mat''); load(a.name); data = concatenate(data, velocity.saccade, ''DiscardEmptyA'');');
sacVdata = data;
if limit == 2
    sacVdata(i, j) = NaN;
end
fixMax = reshape(fixMaxVdata, 1, (size(fixMaxVdata, 1) * size(fixMaxVdata, 2)));
fixMean = reshape(fixMeanVdata, 1, (size(fixMeanVdata, 1) * size(fixMeanVdata, 2)));
sac = reshape(sacVdata, 1, (size(sacVdata, 1) * size(sacVdata, 2)));
fixationMaxV = fixMax(~isnan(fixMax));
fixationMeanV = fixMean(~isnan(fixMean));
saccadeV = sac(~isnan(sac));


[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_duration.mat''); load(a.name); data = concatenate(data, duration_histograms.fixation, ''DiscardEmptyA'');');
fixDurData = data;
[nd,data] = ProcessDirs(nd,'nptDirCmd','cd Eye; a = nptdir(''*_duration.mat''); load(a.name); data = concatenate(data, duration_histograms.saccade, ''DiscardEmptyA'');');
sacDurData = data;
if limit == 2
    sacDurData(i, j) = NaN;
end
fix = reshape(fixDurData, 1, (size(fixDurData, 1) * size(fixDurData, 2)));
sac = reshape(sacDurData, 1, (size(sacDurData, 1) * size(sacDurData, 2)));
fixationDur = fix(~isnan(fix));
saccadeDur = sac(~isnan(sac));

if length(saccadeV) == length(saccadePR)
    figure
    plot(saccadePR, saccadeV, '.')
    title('Tang Fixational Main Sequence')
    xlabel('Magnitude (degrees)')
    ylabel('Velocity (degrees/sec)')
    set(gca,'TickDir', 'out')
end

figure
if limit ~= 2
    n = histc(saccadePR, 0:0.1:(ceil(max(saccadePR))));
    bar(0:0.1:(ceil(max(saccadePR))), n, 'histc')
    title('Tang Fixational Saccade Amplitudes')
    xlabel('Amplitude (degrees)')
    a = axis;
    if limit == 1
        axis([0 30 0 a(4)])
        a = axis;
    end
else
    n = histc(saccadePR, 0:0.01:(ceil(max(saccadePR))));
    bar(0:0.01:(ceil(max(saccadePR))), n, 'histc')
    title('Tang Fixational Saccade Amplitudes')
    xlabel('Amplitude (degrees)')
    a = axis;
    axis([0 2 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(saccadePR),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(saccadePR),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(saccadePR),3)])
set(gca,'TickDir', 'out')

figure
n = histc(saccadeV, 0:1:(ceil(max(saccadeV))));
bar(0:1:(ceil(max(saccadeV))), n, 'histc')
title('Tang Fixational Saccade Velocities')
xlabel('Velocity (degrees/sec)')
a = axis;
if limit == 1
    axis([0 1000 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(saccadeV),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(saccadeV),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(saccadeV),3)])
set(gca,'TickDir', 'out')

figure
n = histc(saccadeDur, 0:1:(ceil(max(saccadeDur))));
bar(0:1:(ceil(max(saccadeDur))), n, 'histc')
title('Tang Fixational Saccade Durations')
xlabel('Duration (ms)')
a = axis;
if limit == 1
    axis([0 100 0 a(4)])
    a = axis;
end
text(.70*a(2),.95*a(4),['Mean = ' num2str(mean(saccadeDur),3)])
text(.70*a(2),.87*a(4),['Median = ' num2str(median(saccadeDur),3)])
text(.70*a(2),.8*a(4),['Std = ' num2str(std(saccadeDur),3)])
set(gca,'TickDir', 'out')

figure
n = histc(fixationPR, 0:0.01:(ceil(max(fixationPR))));
bar(0:0.01:(ceil(max(fixationPR))), n, 'histc')
title('Tang Fixational Fixation Amplitudes')
xlabel('Amplitude (degrees)')
a = axis;
if limit
    axis([0 1 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(fixationPR),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(fixationPR),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(fixationPR),3)])
set(gca,'TickDir', 'out')

figure
n = histc(fixationMaxV, 0:0.01:(ceil(max(fixationMaxV))));
bar(0:0.01:(ceil(max(fixationMaxV))), n, 'histc')
title('Tang Fixational Peak Fixation Velocities')
xlabel('Velocity (degrees/sec)')
a = axis;
if limit == 1
    axis([0 15 0 a(4)])
    a = axis;
elseif limit == 2
    axis([0 10 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(fixationMaxV),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(fixationMaxV),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(fixationMaxV),3)])
set(gca,'TickDir', 'out')

figure
n = histc(fixationMeanV, 0:0.01:(ceil(max(fixationMeanV))));
bar(0:0.01:(ceil(max(fixationMeanV))), n, 'histc')
title('Tang Fixational Mean Fixation Velocities')
xlabel('Velocity (degrees/sec)')
a = axis;
if limit == 1
    axis([0 a(2) 0 a(4)])
    a = axis;
elseif limit == 2
    axis([0 4 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(fixationMeanV),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(fixationMeanV),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(fixationMeanV),3)])
set(gca,'TickDir', 'out')

figure
n = histc(fixationDur, 0:1:(ceil(max(fixationDur))));
bar(0:1:(ceil(max(fixationDur))), n, 'histc')
title('Tang Fixational Fixation Durations')
xlabel('Duration (ms)')
a = axis;
if limit == 1
    axis([0 1000 0 a(4)])
    a = axis;
end
text(.55*a(2),.95*a(4),['Mean = ' num2str(mean(fixationDur),3)])
text(.55*a(2),.87*a(4),['Median = ' num2str(median(fixationDur),3)])
text(.55*a(2),.8*a(4),['Std = ' num2str(std(fixationDur),3)])
set(gca,'TickDir', 'out')