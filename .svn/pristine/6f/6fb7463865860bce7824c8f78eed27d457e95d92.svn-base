function plotperformance(varargin)

% takes input .bhv file and draws a smoothed plot of correct versus incorrect
% trialerror values

Args = struct('bhv',[],'smoothwin',10);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

if isempty(Args.bhv)
    bhv = bhv_read;
else
    bhv = Args.bhv;
end

te = bhv.TrialError;

%get list of correct/incorrect trials
rte = te(te == 0 | te == 6)';
rte(rte == 6) = 1;

lrte = size(rte,2);
colororder = [0 1 0; 1 0 0];
corder(1,1:2,1:3) = colororder;

%get block info for each correct or incorrect trial
blocks = bhv.BlockNumber(te == 0 | te == 6)';

%find where blocks change
block_index = [find(diff(blocks)) lrte];
block_order = [blocks(block_index)];
num_block_change = size(block_order,2) - 1;




yarray1 = zeros(lrte,3);
for i = [0 1]
    r = smooth(double(rte==i), Args.smoothwin, 'gauss')';
    yarray1(:, i+2) = yarray1(:, i+1) + r;
end

xarray1 = (1:lrte)';
xarray1 = repmat(xarray1,1,3);

xarray2 = flipud(xarray1);
yarray2 = flipud(yarray1);

x = cat(1,xarray1(:,1:2), xarray2(:,2:3));
y = cat(1,yarray1(:,1:2), yarray2(:,2:3));


warning off
figure
scrnsz = get(0, 'screensize');
set(gcf, 'color', [1 1 1], 'position', [50 450 scrnsz(3)-100 350], 'numbertitle', 'off', 'name', 'Behavior Summary', 'color', [.85 .85 1]);
patch(x, y, corder);
set(gca, 'xlim', [1 lrte], 'ylim', [0 1], 'position' ,[0.05 0.13 .92 .8], 'box', 'on');
hline(1) = line([0 lrte], [0.5 0.5]);
set(hline(1), 'color', [0.7 0.7 0.7], 'linewidth', 2);
hline(2) = line([0 lrte], [0.25 0.25]);
hline(3) = line([0 lrte], [0.75 0.75]);
set(hline([2 3]), 'color', [0.7 0.7 0.7], 'linewidth', 1);


%make lines for each block and plot text to indicate block number
for x = 1 : num_block_change;
    line([block_index(x) block_index(x)],[0 1]);
    hold on
end
for x = 1 : (num_block_change + 1);
    if x == 1;
        text((block_index(1) / 2),.1,num2str(block_order(x)));
    else
        bindex = block_index(x-1) + ((block_index(x) - block_index(x-1)) / 2);
        text(bindex,.1,num2str(block_order(x)));
    end
    hold on
end




xlabel('Trial number (aborted trial numbers not included)');
ylabel('Fraction correct');
warning on



