function pr = RFproperties(obj,clusters,oversample,gaussianSize,sigma,percent,overlap,durationSigma)
%pr = RFproperties(s.obj,clusters,XTOversample,XTGaussianSize,XTSigma,XTPercentile,XTOverlap,XTDurationSigma)
%
%calculates Receptive Field properties from a kernel.
%
%Inputs:  
%obj  - rc kernel (kernels from all cells in agroup).
%clusters - list of clusters within obj to calculate.
%ssiz - optional spatial fft size
%tsiz - optional temporal fft size
%
%output:
%pr - property structure
%       pr.cluster(ii).center - [vertical , horizontal, time]
%       pr.cluster(ii).area - percentile contour area in pixels^2.
%       pr.cluster(ii).areaM = matrix of abs summed intensities
%       pr.cluster(ii).BoundingBox = bonding box of region of interest
%       pr.cluster(ii).areaThreshold = threshold calculated from percentile
%       pr.cluster(ii).xfreq - 
%       pr.cluster(ii).yfreq - 
%       pr.cluster(ii).orientation - spatial orientation
%       pr.cluster(ii).sfreq - spatial freq (magnitude of x and y componrents)
%       pr.cluster(ii).tfreq - 
%       pr.cluster(ii).vel - 
%       pr.cluster(ii).sphase - 
%       pr.cluster(ii).tphase - 
%       pr.cluster(ii).xt - sampled through time along orientation line
%                           through x,y center bounded by bounding box.
%       pr.cluster(ii).xi - x vector used to sample xt
%       pr.cluster(ii).yi - y vector used to sample xt 
%       pr.cluster(ii).XTsum - collapse xt along time dimension using ...
%       pr.cluster(ii).XTthreshold - to find ... 
%       pr.cluster(ii).latency - 1x2 vector of latency and duration.
%
%   also save algo parameters
%    pr.cluster(ii).Algo.percent 
%    pr.cluster(ii).Algo.overlap 
%    pr.cluster(ii).Algo.oversample 
%    pr.cluster(ii).Algo.GausSiz 
%    pr.cluster(ii).Algo.GausSigma 
%    pr.cluster(ii).Algo.DurationSigma 
% all outputs units are with respect to the kernel (pixels or pages) not degrees.

R=obj.R;

if nargin<3
    oversample = 2;
    gaussianSize = 10;
    sigma=2;
    percent = 99;
    overlap = 4;
    durationSigma =4;
end
%smooth kernel to get better Area estimate
%but everything else is calculated on original data.
smooth=1;   
display=0;
tsiz = size(R,3);           %temporal FFT size
ssiz = size(R,1)*2;       %spatial FFT size

waith = waitbar(0,'Please wait...');


%loop over clusters
for ii=1:length(clusters)
    g = R(:,:,:,clusters(ii));
    g = g-mean(g(:));
    
    if smooth
        ag = imresize(g,oversample);
        f=TwoDimGaussFilter(gaussianSize,sigma);
        ag = imfilter(ag,f,'same');
    else
        oversample=1;
        ag = g;
    end
    
    %find peak location and latency
    [Max,row,col,page] = max3(ag);
    s=abs(ag-mean(ag(:)));
    
    %sum across time
    s=sum(s,3);  
    [Max,row,col] = max2(s);
    
    %area
    threshold = prctile(reshape(s,[1 size(s,1)*size(s,2)]),percent); %set threshold to percentth percentile 
    
    if display
        figure;hist(reshape(s,[1 size(s,1)*size(s,2)]));hold on
        plot(threshold,1,'r*')
    end
    
    bw = roicolor(s,threshold,max(s(:)));                   %Threshold data
    bw = bwselect(bw,col,row);                              %select only maximum Region of Interest
    stats = regionprops(bwlabel(bw),'Area','Centroid','BoundingBox');  %compute stats of binomial mask
    
    bx = 1:size(ag,2)/oversample;    
    by = 1:size(ag,1)/oversample;
    BB = stats.BoundingBox/oversample;
    xind = double((bx>(min(BB(1) - overlap))) & (bx<(max(BB(1) + BB(3) + overlap))));
    yind = double((by>(min(BB(2)  - overlap))) & (by<(max(BB(2) + BB(4)  + overlap))));
    mask = yind'*xind;
    
    if display
        figure;   imagesc(s);hold on; colorbar
        [c,h] = imcontour(s,[threshold threshold],'k');
        figure;   imagesc(mask)
    end
    
    pr.cluster(ii).area = stats.Area/(oversample^2);
    center = [stats.Centroid/oversample page]; %xyz
    pr.cluster(ii).center = center;
    pr.cluster(ii).areaM = s;
    pr.cluster(ii).BoundingBox = BB;
    pr.cluster(ii).areaThreshold = threshold;
    
    %now we need to clean up the image 
    %otherwise the noise messes up the fft.
    %set everything outside of mask to mean
    mask = repmat(mask,[1 1 size(g,3)]);
    g1=mask.*g;
    if display;figure;imagesc(g1(:,:,center(3)));end
    
    f=fftshift(fftn(g1-mean(g1(:)),[ssiz,ssiz,tsiz]));
    f=f(:,:,1:tsiz/2);    %only use one side of the spectrum b/c symmetric and exclude DC!
    fm = abs(f);
    
    [v,r,c,p] = max3(fm);
    if display;figure;imagesc(fm(:,:,p));end
    
    y=r-(ssiz/2+1);
    x=c-(ssiz/2+1);
    t=p-(tsiz/2+1);
    
    %freq measured in cycles/pixel
    pr.cluster(ii).xfreq = abs(x)/ssiz;
    pr.cluster(ii).yfreq = abs(y)/ssiz;
    pr.cluster(ii).orientation = 180/pi*atan(-y/x)+90;  
    
    pr.cluster(ii).sfreq = sqrt(pr.cluster(ii).xfreq^2 + pr.cluster(ii).yfreq^2);
    pr.cluster(ii).tfreq = abs(t)/tsiz;
    pr.cluster(ii).vel = pr.cluster(ii).tfreq/pr.cluster(ii).sfreq;
    
    pr.cluster(ii).sphase = 180/pi*atan(imag(f(r,c,p))/real(f(r,c,p)));
    pr.cluster(ii).tphase = 180/pi*atan(imag(f(r,c,p))/real(f(r,c,p)));
    
    %XT and Duration
    box = [ BB(1) - overlap ...
            , BB(2) + BB(4)  + overlap  ...
            , BB(1) + BB(3)  + overlap  ...
            , BB(2) - overlap];
    
    slope = y/x;
    if isinf(slope)
        slope=100000;
    end
    [xt,xi,yi] = CreateXT(g1,slope,center,box);
    if display;figure;imagesc(xt);end
    
    %latency and duration
    xts = sum(abs(xt),2);
    
    %assume only noise in 1st 20 msec so...
    %threshold = mean(xts(1:20)) + .25*(max(xts) - mean(xts(1:20)));
    threshold = durationSigma*std(xts(1:20)) + mean(xts(1:20));
    if display;figure;plot(xts);hold on;line([0 length(xts)],[threshold threshold]);end
    
    cr = nptThresholdCrossings(xts,threshold,'separate');
    if ~isempty(cr.rising) & ~isempty(cr.falling)
        latency = [cr.rising(1) cr.falling(end)];
    else
        latency = [1 2];
    end
    if latency(2)<latency(1)
        latency(2)=NaN;
    end
    
    pr.cluster(ii).xt = xt;
    pr.cluster(ii).latency = latency;
    pr.cluster(ii).xi = xi; 
    pr.cluster(ii).yi = yi;
    pr.cluster(ii).XTthreshold = threshold;
    pr.cluster(ii).XTsum = xts;
    
    %save algo parameters
    pr.cluster(ii).Algo.percent = percent;
    pr.cluster(ii).Algo.overlap = overlap;
    pr.cluster(ii).Algo.oversample = oversample;
    pr.cluster(ii).Algo.GausSiz = gaussianSize;
    pr.cluster(ii).Algo.GausSigma = sigma;
    pr.cluster(ii).Algo.DurationSigma = durationSigma;
    
    
     waitbar(ii/length(clusters),waith)
end
close(waith)



%%%%%%%%%%%%%%%%%%%%%%%x-t plot
function [xt,xi,yi] = CreateXT(Rc,m,center,box)
%[xt,xi,yi] = CreateXT(Rc,m,center,box)
%
%RC - 3d kernel
%m - slope
%center  - x,y
%box    -  bounding box to sample within

if nargin<4
    box = [ 1 size(Rc,1) size(Rc,2) 1];  %left, top right bottom
end

%find where line intercepts edges of box
yint = center(2) - m*center(1);   %y=m*x+b
yleft = m*box(1)+yint;%left edge
xtop = (box(2)-yint)/m;   %1=mx+b  %top edge
xbottom = (box(4) - yint)/m;  %y=mx+b bottom edge ->x = (y-b)/m
yright = m*box(3) + yint;

x=[];
if yleft>(box(4)) & yleft<box(2)
    x = [x box(1)];
end
if  xtop>=box(1) & xtop<=box(3)
    x = [x xtop];
end
if xbottom>=box(1) & xbottom<=box(3)
    x = [x xbottom];
end
if yright>box(4) & yright<box(2)
    x = [x box(3)];
end
x = sort(x);
%points along orthogonal line
xi=x(1):(x(2)-x(1))/100:x(2);
yi=m*xi+yint; 
width = size(xi,2);
xii = repmat(xi,[1 size(Rc,3)]);
yii = repmat(yi,[1 size(Rc,3)]);
zii=imresize([1:size(Rc,3)],[1 size(xii,2)]);
xt=interp3(Rc,xii,yii,zii);
xt = transpose(reshape(xt,[width,size(Rc,3)]));



