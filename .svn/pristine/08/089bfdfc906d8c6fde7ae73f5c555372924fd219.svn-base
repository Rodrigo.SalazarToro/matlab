function data = getIndexCohInter(datafile,surfile,compfile,rulefile,idefile,locfile,varargin)
% Index(:,1) = pair number
% Index(:,2) = day number
% Index(:,3) = beta increase during delay (plus one above sur)
% Index(:,4) = beta decrease during delay (plus one above sur)
% Index(:,5) = gamma increase between fix and delay 2 (plus one above sur)
% Index(:,6) = gamma decrease between fix and delay 2 (plus one above sur)
% all of the above is either 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules

% Index(:,7) = beta differences between the two rules (0 for no, 1 for delay 1, 2 for delay 2 and 3 for both)
% Index(:,8) = gamma differences between the two rules (0 for no, 1 for
% fix, 2 for delay 2 and 3 for both)
% Index(:,9) = histology crude (1: PPCm-PFCd; 2: PPCm-PFCv; 3 PPCl-PFCd;
% 4 : PPCl-PFCv; 5: PPCs-PFCd; 6: PPCs-PFCv; 7: PPCm-PFCs; 8: PPCl-PFCs)
%
% Index(:,10:11) = group number parietal, frontal
% Index(:,12) = IDENTITY rule, no tuning (0), tuning for the
% identity (1) or the location (2) or (3) both location and idenities tuning of the sample only for the beta
% range of course
% Index(:,13) = LOCATION rule, no tuning (0), tuning for the
% identity (1) or the location (2) of the sample only for the beta
% range of course
% Index(:,14) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 3
% Index(:,15) = difference between the identity (1), the location(2), both(3) or none (0) of the cues between
% the two rules for window 4
% Idex(:,16) ide tuning index during IDE rule for window 4 PP
% Idex(:,17) ide tuning index during LOC rule for window 4 PP
% Idex(:,18) loc tuning index during IDE rule for window 4 PP
% Idex(:,19) loc tuning index during LOC rule for window 4 PP
% Idex(:,20) ide tuning index during IDE rule for window 4 PF
% Idex(:,21) ide tuning index during LOC rule for window 4 PF
% Idex(:,22) loc tuning index during IDE rule for window 4 PF
% Idex(:,23) loc tuning index during LOC rule for window 4 PF
% Idex(:,24) snr for parietal channel
% Idex(:,25) snr for frontal channel
% Index(:,26) alpha peak 0 for no effect, 1 for IDE, 2 for LOC
% and 3 for both rules
% index(:,27) DC 0 or 1
% Index(:,28) histology middle
% Index(:,29) sorted
%%
Args = struct('selectedLFP',[],'nptAbove',1,'NoHisto',0,'flat',0,'Tuning',0,'fband',[14 30;30 42; 7 13; 1 7],'powerTuningWind',4,'noSur',0,'withSNR',1,'pLocal',4,'fromSorting',0);
Args.flags = {'NoHisto','flat','Tuning','noSur','withSNR','fromSorting'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

warning off

load(datafile.name)
%     rcomp = load(compfile);

if ~Args.noSur
    sur = load(surfile.name);
    if size(sur.Day.session(1).Prob,3) ~= size(Day.session(1).C,2)
        for s = 1 : length(sur.Day.session); sur.Day.session(s).Prob = repmat(sur.Day.session(s).Prob,[1 1 size(Day.session(1).C,2) 1]); end
    end
end
if Args.Tuning
    ide = load(idefile.name);
    loc = load(locfile.name);
    load(rulefile.name);
end

dir = pwd;
if ~Args.NoHisto; cd session01; histology = NeuronalHist; cd ..;end

cd(dir)

clark = findstr(dir,'clark');
betty = findstr(dir,'betty');
tuning = {'ide' 'loc'};
tuningN = {'Obj' 'Loc'};

if ~isempty(clark) || str2num(dir(end-5:end-4)) < 9
    
    mt = ProcessDay(mtstrial,'auto','NoSites','sessions',{'session02','session03'});
    rules = [mt.data.Index(1,1) mt.data.Index(end,1)];
    if Args.Tuning
        
        % from the checktuning
        for s = 2 : 3; cd(sprintf('session0%d/lfp/',s));
            for tune = 1 : 2;
                eval(sprintf('load(''%sTuningRule%d.mat'',''powerPP'',''powerPF''); for cu = 1 : 3; %s%d.power(cu).S = [powerPP.power(cu).S; powerPF.power(cu).S]; end;',tuning{tune},rules(s-1),tuning{tune},rules(s-1)));
            end; cd ../..;
        end
    end
elseif ~isempty(betty)
    
    if Args.Tuning
        cd('session01/lfp');
        for tune = 1 : 2;
            for r = 1 : 2;
                
                eval(sprintf('load(''%sTuningRule%d.mat'',''powerPP'',''powerPF'');',tuning{tune},r));
                if isfield(powerPP,'power') && isfield(powerPF,'power')
                    eval(sprintf('for cu = 1 : 3; %s%d.power(cu).S = [powerPP.power(cu).S; powerPF.power(cu).S]; end;',tuning{tune},r));
                end
            end;
        end
        cd ../..;
    end
end
% npairs = size(Day.comb,1);
if isfield(Day,'comb')
    CHcomb = Day.comb;
else
    
    CHcomb = [];
end
if ~isempty(CHcomb)
    cd ..
    if exist('sur'); [list] = mkListPairs('thedays',{dir(end-5: end)},modvarargin{:});f = list.f;end
    cd(dir)
    
    limitB = find(f>= Args.fband(1,1) & f<= Args.fband(1,2));
    limitG = find(f>= Args.fband(2,1) & f<= Args.fband(2,2));
    limitA = find(f>= Args.fband(3,1) & f<= Args.fband(3,2));
    limitT = find(f>= Args.fband(4,1) & f<= Args.fband(4,2));
    Flimit = [limitB(1) limitB(end); limitG(1) limitG(end); limitA(1) limitA(end); limitT(1) limitT(end)];
    comparison = {'pos' 'neg'};
    bands = {'beta' 'gamma' 'alpha' 'theta'};
    therules = {'IDE' 'LOC'};
    
    lwind = [3 4; 1 4; 1 2; 2 3; 1 3; 2 4];
    cd session01; [allch,comb,CHcombt,groups] = checkChannels('cohInter',modvarargin{:}); cd ..;
    % [allch,CHcomb,iflat,groups] = getFlatCh( modvarargin{:});
    
    % [ch,gind] = setdiff(allch,iflat.ch); CHcomb = setdiff(CHcomb,CHcomb(iflat.pairs,:),'rows'); groups = groups(gind);
    ch = allch;
    tracking = [];
    % &&  (exist('Adz') || unique(isnan(Adz)) ~= 1)
    Index = zeros(size(CHcomb,1),26);
    if exist('sur')
        for b = 1 : size(Flimit,1)
            %% mod above sur
            eval(sprintf('data.%s = zeros(size(CHcomb,1),4);',bands{b}));
            eval(sprintf('data.%sPeak = zeros(size(CHcomb,1),8);',bands{b}));
            eval(sprintf('data.%sPhase = zeros(size(CHcomb,1),8);',bands{b}));
            for p = 1 : 4
                for rule = 1 : 2
                    
                    range = Flimit(b,1):Flimit(b,2);
                    [spairs,ntot,index] = selectPairs('data',list,'range',{bands{b}},'rule',rule,'sigSur','windows',p,modvarargin{:});
                    selpairs{rule} = index.npair;
                    tf = ismember(CHcomb,index.ch,'rows');
                    if ~isempty(index.npair) && sum(tf) ~= 0;
                        for row = 1 : length(index.npair);
                            
                            eval(sprintf('data.%s(find(tf == 1),p) = rule;',bands{b}));
                            coher = list.rdata{rule}(:,find(tf == 1),p);
                            fcts = which('findpeaks','-all');thefct = strfind(fcts,sprintf('toolbox%ssignal%ssignal%sfindpeaks.m',filesep,filesep,filesep)); for nf =  1 : length(thefct); if ~isempty(thefct{nf}); tfn = nf; end; end; pdir = pwd;cd(fcts{tfn}(1:end-11));
                            sp = find(tf == 1);
                            for np = 1 : length(sp)
                                [lmax,locs] = findpeaks(coher(Flimit(b,1)- 1:Flimit(b,2)+1,np));
                                
                                if length(locs) > 1
                                    [c,thef] = max(lmax);
                                    locs = locs(thef);
                                end
                                
                                eval(sprintf('data.%sPeak(sp(np),(rule-1) * 4+ p) = f(range(1) + locs - 2);',bands{b}));
                                if Day.session(1).rule == 1; s = 1; else s = 2; end
                                eval(sprintf('data.%sPhase(sp(np),(rule-1) * 4+ p) = mod(2*pi + mean(Day.session(s).phi(range,sp(np),p),1),2*pi);',bands{b}));
                            end
                            
                            cd(pdir)
                        end;
                        
                    end
                end
                
                bothrule = intersect(selpairs{1},selpairs{2});
                if ~isempty(bothrule);
                    for pp = 1 : length(bothrule);
                        tf = ismember(CHcomb,Day.comb(bothrule(pp),:),'rows');
                        if sum(tf) ~= 0;
                            
                            eval(sprintf('data.%s(find(tf == 1),p) = 3;',bands{b}));
                        end;
                    end;
                end
            end
            %% comaprisons between windows
            for comp = 1 : 2
                for w = 1 : size(lwind,1)
                    eval(sprintf('data.%s%s%d%d = zeros(size(CHcomb,1),1);',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
                    for rule = 1 : 2
                        if b == size(Flimit,1)
                            [spairs,ntot,index] = selectPairs('data',list,'range',{bands{b}},'rule',rule,modvarargin{:});
                            selpairs{rule} = index.npair;
                            tf = ismember(CHcomb,index.ch,'rows');
                            if ~isempty(index.npair) && sum(tf) ~= 0; for row = 1 : length(index.npair); Index(find(tf == 1),26) = rule; end; end
                            
                        end
                        %                                             else
                        [spairs,ntot,index] = selectPairs('data',list,'comparison',{comparison{comp}},'range',{bands{b}},'windows',lwind(w,:),'rule',rule,modvarargin{:});
                        selpairs{rule} = index.npair;
                        tf = ismember(CHcomb,index.ch,'rows');
                        if ~isempty(index.npair) && sum(tf) ~= 0;
                            for row = 1 : length(index.npair);
                                if (b == 1 && w == 1) || (b ==2 && w == 2);  Index(find(tf ==1),(b-1)*2+ comp + 2) = rule; end
                                eval(sprintf('data.%s%s%d%d(find(tf == 1)) = rule;',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
                            end;
                        end
                        
                        %                     end
                    end
                    tracking = [tracking [b; comp; w; (b-1)*8+ (comp-1) *4 + w + 2]];
                    bothrule = intersect(selpairs{1},selpairs{2});
                    if ~isempty(bothrule);
                        for pp = 1 : length(bothrule);
                            tf = ismember(CHcomb,Day.comb(bothrule(pp),:),'rows');
                            if sum(tf) ~= 0;
                                if b < size(Flimit,1) && w < 3;
                                    Index(find(tf == 1),(b-1)*2+ comp + 2) = 3;
                                    
                                end;
                                eval(sprintf('data.%s%s%d%d(find(tf == 1)) = 3;',bands{b},comparison{comp},lwind(w,1),lwind(w,2)));
                            end;
                        end;
                    end
                end
            end
            %%
        end
    end
    for count = 1 : size(Day.comb,1)
        pa = count;
        %% DC falg
        Index(count,27) = 0;
        load(datafile.name)
        for s =1 : size(Day.session,2);
            
            if ~isempty(Day.session(s).C)
                ts = squeeze(Day.session(s).C(:,pa,:));
                if max(max(ts(1:3,:),[],1),[],2) > 0.3
                    if ~Args.noSur && Args.pLocal > size(sur.Day.session(s).Prob,2); Args.pLocal = 1; end
                    if ~Args.noSur; Surts = squeeze(sur.Day.session(s).Prob(:,Args.pLocal,pa,:));end
                    ff = repmat(1/3,1,3);
                    for p = 1 : 4; tsf(:,p) = filtfilt(ff,1,ts(:,p));end
                    fcts = which('findpeaks','-all');
                    
                    thefct = strfind(fcts,sprintf('toolbox%ssignal%ssignal%sfindpeaks.m',filesep,filesep,filesep));
                    for nf =  1 : length(thefct); if ~isempty(thefct{nf}); tfn = nf; end; end
                    for nf =  1 : length(thefct); if ~isempty(thefct{nf}); tfn = nf; end; end
                    pdir = pwd;
                    cd(fcts{tfn}(1:end-11));
                    %                      clf;  plot(f,squeeze(Day.session(s).C(:,pa,:)));hold on
                    %                         plot(f,Surts,'--')
                    %                         title(Day.comb(pa,:))
                    for p = 1 : 4;
                        [lmax,locs] = findpeaks(tsf(:,p)); fpeaks{p} = f(locs);
                        if ~isempty(fpeaks{p}) && (fpeaks{p}(1) > Args.fband(2,2)); fpeaks{p} = [];end% Index(count,27) = 0; end
                        if ~Args.noSur
                            if ~isempty(fpeaks{p}) && (fpeaks{p}(1) <= Args.fband(2,2) && fpeaks{p}(1) >= Args.fband(3,1)) && (max(ts(locs(1)-1 : locs(1)+1,p)) <=  max(Surts(locs(1)-1 : locs(1)+1,p))); fpeaks{p} = [];end
                        end
                        
                        %                        if ~isempty(fpeaks{p}) && (fpeaks{p}(1) <= Args.fband(2,2) && fpeaks{p}(1) >= Args.fband(3,1)) && isempty(find(ts(locs(1)-1 : locs(1)+1,p) > Surts(locs(1)-1 : locs(1)+1,p))); nopeak = true;else nopeak = false; end
                        %                         for sp = speaks; if fpeaks{p}(sp) >= Args.fband(3,1) && fpeaks{p}(sp) <= Args.fband(2,2) && max(ts(locs(sp)-1 : locs(sp)+1,p)) <=  max(Surts(locs(sp)-1 : locs(sp)+1,p)); nopeak = true;else nopeak = false; break;end; end
                        if ~exist('nopeak'); nopeak = true; end
                        if ~isempty(fpeaks{p}) && fpeaks{p}(1) < Args.fband(3,1) && (length(fpeaks{p}) == 1 || fpeaks{p}(2) > Args.fband(2,2) ||  nopeak); fpeaks{p} = [];end% Index(count,27) = 0; end
                        
                    end
                    
                    if isempty(fpeaks{1}) && isempty(fpeaks{2}) && isempty(fpeaks{3}) && isempty(fpeaks{4});
                        Index(count,27) = 1 * s + Index(count,27);
                        %                          clf;  plot(f,squeeze(Day.session(s).C(:,pa,:)));hold on
                        %                         plot(f,Surts,'--')
                        
                    end
                    cd(pdir)
                end
            end
            
        end
        %%
        Index(count,1) = pa;
        Index(count,2) = 1;
         clear sigcoh
        for s = 1 : length(Day.session);
            if ~isempty(Day.session(s).C) && ~isempty(sur.Day.session(s).Prob)
                sigcoh(Day.session(s).rule,:,:) = squeeze(Day.session(s).C(:,pa,:)) > squeeze(sur.Day.session(s).Prob(:,Args.pLocal,pa,:));
            
            end
        end
        
        for b = 1 : 2
            if exist('sur') && exist('Adz') && unique(isnan(Adz)) ~= 1
                range = Flimit(b,1):Flimit(b,2);
                if sum(squeeze(~Adz(range,pa,lwind(b,1)))) >= Args.nptAbove & (length(find(Day.session(1).C(range,pa,lwind(b,1)) > sur.Day.session(1).Prob(range,Args.pLocal,pa,lwind(b,1)))) > Args.nptAbove & length(find(Day.session(1).C(range,pa,lwind(b,2)) > sur.Day.session(1).Prob(range,Args.pLocal,pa,lwind(b,2)))) > Args.nptAbove)
                    
                    Index(count,6+b) = 3;
                    
                elseif sum(squeeze(~Adz(range,pa,lwind(b,1)))) >= Args.nptAbove & ~sum(squeeze(~Adz(range,pa,lwind(b,2)))) >= Args.nptAbove & length(find(Day.session(1).C(range,pa,lwind(b,1)) > sur.Day.session(1).Prob(range,Args.pLocal,pa,lwind(b,1)))) > Args.nptAbove
                    Index(count,6+b) = 1;
                elseif  sum(squeeze(~Adz(range,pa,lwind(b,2)))) >= Args.nptAbove & ~sum(squeeze(~Adz(range,pa,lwind(b,1)))) >= Args.nptAbove & length(find(Day.session(1).C(range,pa,lwind(b,2)) > sur.Day.session(1).Prob(range,Args.pLocal,pa,lwind(b,2)))) > Args.nptAbove
                    Index(count,6+b) = 2;
                    
                else
                    Index(count,6+b) = 0;
                end
            else
                Index(count,6+b) = nan;
                
            end
        end
        
        
        if Args.NoHisto
            Index(count,9) = NaN;
            data.hist(count,:) = nan(1,2);
        else
            %             if ~iscell(histology.locs) && isnan(histology.locs)
            %                 Index(count,9) = nan;
            %             else
            
            pairhist =  cell2mat(histology.level3(Day.comb(pa,:)));
            
            switch pairhist
                case 'md'
                    
                    Index(count,9) = 1;
                case 'mv'
                    Index(count,9) = 2;
                case 'ld'
                    Index(count,9) = 3;
                case 'lv'
                    Index(count,9) = 4;
                case 'sd'
                    Index(count,9) = 5;
                case 'sv'
                    Index(count,9) = 6;
                case 'ms'
                    Index(count,9) = 7;
                case 'ls'
                    Index(count,9) = 8;
                otherwise
                    Index(count,9) = 99;
            end
            
            pairhist =  cell2mat(histology.level2(Day.comb(pa,:)));
            
            switch pairhist
                case '54'
                    Index(count,28) = 1;
                case '56'
                    Index(count,28) = 2;
                case '58'
                    Index(count,28) = 3;
                case 'l4'
                    Index(count,28) = 4;
                case 'l6'
                    Index(count,28) = 5;
                case 'l8'
                    Index(count,28) = 6;
                case 'm4'
                    Index(count,28) = 7;
                case 'm6'
                    Index(count,28) = 8;
                case 'm8'
                    Index(count,28) = 9;
                case '74'
                    Index(count,28) = 10;
                case '76'
                    Index(count,28) = 11;
                case '78'
                    Index(count,28) = 12;
                otherwise
                    Index(count,28) = 99;
            end
            pairhist =  histology.number(Day.comb(pa,:));
            
            data.hist(count,:) = pairhist;
            %             end
        end
        
        
        %% tuning
        % Cuedz(:,ncomb,p,comp,r)
        % Index(:,12) = IDENTITY rule, no tuning (0), tuning for the
        % identity (1) or the location (2) or (3) both location and idenities tuning of the sample only for the beta
        % range of course
        % Index(:,13) = LOCATION rule, no tuning (0), tuning for the
        % identity (1) or the location (2) of the sample only for the beta
        % range of course
         Index(count,10:11) = [groups(find(CHcomb(pa,1) == ch)) groups(find(CHcomb(pa,2) == ch))];
         if ~isempty(nptDir('tuning'))
             cd tuning
             for r = 1 : 2
                 for tune = 1 : 2
                     filenames = nptDir(sprintf('iCue%sg%04.0fg%04.0fRule%d.mat',tuningN{tune},Index(count,10),Index(count,11),r));
                     if ~isempty(filenames)
                         load(filenames.name)
                         for p = 1 : 4
                             clear limitBstim
                             if data.betaPeak(pa,p+(r-1)*4) ~= 0
                                 limitBstim = find(data.betaPeak(pa,p+(r-1)*4) == f);
                                 limitBstim = [limitBstim-2 : limitBstim+2];
                                 %                                  limitBstim = limitB;
                                 % option to get sig diff only if main coh
                                 % is sig
                                 sigdata = (filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff) >= filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues(:,1))) .* squeeze(sigcoh(r,:,p))';
                                 
                                 eval(sprintf('data.%s%s(count,p) = sum(sigdata(limitBstim));',therules{r},tuningN{tune}))
                                 intStim = sum(groupN(p).C(limitBstim,:));
                                 [~,ord] = sort(intStim);
                                 eval(sprintf('data.%s%sSeq(count,p,:) = ord;',therules{r},tuningN{tune}))
                                  eval(sprintf('data.%s%sInt(count,p,:) = intStim(ord);',therules{r},tuningN{tune}))
                             else
                                 eval(sprintf('data.%s%s(count,p) = 0;',therules{r},tuningN{tune}))
                                 eval(sprintf('data.%s%sSeq(count,p,:) = [1 2 3];',therules{r},tuningN{tune}))
                                 eval(sprintf('data.%s%sInt(count,p,:) = zeros(1,3);',therules{r},tuningN{tune}))
                             end
                             sigdata = (filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff) > filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues(:,1))) .* squeeze(sigcoh(r,:,p))';
                            
                             eval(sprintf('data.%s%ssig(count,p) = sum(sigdata(limitB));',therules{r},tuningN{tune}))
                             
                         end
                     else
                         eval(sprintf('data.%s%s(count,:) = nan(1,4);',therules{r},tuningN{tune}))
                         eval(sprintf('data.%s%sSeq(count,:,:) = nan(4,3);',therules{r},tuningN{tune}))
                         eval(sprintf('data.%s%sInt(count,p,:) = nan(1,3);',therules{r},tuningN{tune}))
                          eval(sprintf('data.%s%ssig(count,p) = nan;',therules{r},tuningN{tune}))
                     end
                 end
             end
              cd ..
         end
       
        
        if Args.Tuning
            
            for b = 1 : 2
                if isnan(ide.Adz)
                    Index(count,13+b) = nan;
                else
                    
                    
                    range = Flimit(b,1):Flimit(b,2);
                    if ~isempty(find(sum(squeeze(~ide.Adz(range,pa,lwind(b,1),:))) >= Args.nptAbove)) && ~isempty(find(sum(squeeze(~loc.Adz(range,pa,lwind(b,1),:))) >= Args.nptAbove))
                        
                        Index(count,13+b) = 3;
                        
                    elseif ~isempty(find(sum(squeeze(~ide.Adz(range,pa,lwind(b,1),:))) >= Args.nptAbove))
                        Index(count,13+b) = 1;
                    elseif ~isempty(find(sum(squeeze(~loc.Adz(range,pa,lwind(b,1),:))) >= Args.nptAbove))
                        Index(count,13+b) = 2;
                        
                    else
                        Index(count,13+b) = 0;
                    end
                    
                end
            end
           
            %%
            
            if exist('sur')
                if isempty(ide.CueAdz)
                    Index(count,12) = nan;
                    Index(count,13) = nan;
                else
                    for r = 1 : 2
                        
                        range = Flimit(1,1):Flimit(1,2);
                        idedata = squeeze(~ide.CueAdz(range,pa,2,:,r));
                        locdata = squeeze(~loc.CueAdz(range,pa,2,:,r));
                        cueIDEdata{1} = squeeze(ide.C1(range,pa,2,:));
                        cueIDEdata{2} = squeeze(ide.C2(range,pa,2,:));
                        cueLOCdata{1} = squeeze(loc.C1(range,pa,2,:));
                        cueLOCdata{2} = squeeze(loc.C2(range,pa,2,:));
                        
                        if ~isempty(find(sum(idedata,1) >= Args.nptAbove)) && ~isempty(find(sum(locdata,1) >= Args.nptAbove)) &&  length(find(cueIDEdata{r} > repmat(sur.Day.session(1).Prob(range,Args.pLocal,pa,2),1,3))) > Args.nptAbove && length(find(cueLOCdata{r} > repmat(sur.Day.session(1).Prob(range,Args.pLocal,pa,2),1,3))) > Args.nptAbove
                            
                            Index(count,11+r) = 3;
                            
                        elseif ~isempty(find(sum(locdata,1) >= Args.nptAbove)) && length(find(cueLOCdata{r} > repmat(sur.Day.session(1).Prob(range,Args.pLocal,pa,2),1,3))) > Args.nptAbove% sum(squeeze(~Adz(range,pa,lwind(b,1)))) >= Args.nptAbove & ~sum(squeeze(~Adz(range,pa,lwind(b,2)))) >= Args.nptAbove & length(find(Day.session(1).C(range,pa,lwind(b,1)) > sur.Day.session(1).Prob(range,2,pa,lwind(b,1)))) > Args.nptAbove
                            Index(count,11+r) = 2;
                        elseif  ~isempty(find(sum(idedata,1) >= Args.nptAbove)) && length(find(cueIDEdata{r} > repmat(sur.Day.session(1).Prob(range,Args.pLocal,pa,2),1,3))) > Args.nptAbove% sum(squeeze(~Adz(range,pa,lwind(b,2)))) >= Args.nptAbove & ~sum(squeeze(~Adz(range,pa,lwind(b,1)))) >= Args.nptAbove & length(find(Day.session(1).C(range,pa,lwind(b,2)) > sur.Day.session(1).Prob(range,2,pa,lwind(b,2)))) > Args.nptAbove
                            Index(count,11+r) = 1;
                            
                        else
                            Index(count,11+r) = 0;
                        end
                        
                        
                    end
                end
            else
                Index(count,11+r) = nan;
            end
            %
            %% power tuning
            % loc1.power(1).S
            ite = {'ide1' 'ide2' 'loc1' 'loc2'};
            asum = [15 19];
            window = Args.powerTuningWind;
            for cases = 1 : 4
                for a = 1 : 2
                    %                     for p = 2 : 4
                    for cu = 1 : 3
                        thech = find(Day.comb(pa,a) == allch);
                        if exist(ite{cases})
                            thedata = eval(sprintf('squeeze(mean(%s.power(cu).S(thech,limitB,:,Args.powerTuningWind),3));',ite{cases}));
                            tarea(:,cu) = sum(thedata);
                        end
                    end
                    Index(count,asum(a) + cases) = reshape((max(tarea,[],2) - min(tarea,[],2)) ./ min(tarea,[],2),1,size(tarea,1));
                    %                     end
                    %                     Index(count,asum(a) + cases) = max(tIndex);
                end
            end
        end
        %%
        if ~exist('list') || isempty(list.modu{1,6}); data.betamodIDE(count,:) = nan; else data.betamodIDE(count,:) = mean(list.modu{1,6}(Flimit(1,1):Flimit(1,2),count),1);end
        if ~exist('list') ||isempty(list.modu{2,6}); data.betamodLOC(count,:) = nan; else data.betamodLOC(count,:) = mean(list.modu{2,6}(Flimit(1,1):Flimit(1,2),count),1); end%% maybe pa
        
        if ~exist('list') ||isempty(list.modu{1,3}); data.gammamodIDE(count,:) = nan; else data.gammamodIDE(count,:) = mean(list.modu{1,3}(Flimit(2,1):Flimit(2,2),count),1);end
        if ~exist('list') ||isempty(list.modu{2,3}); data.gammamodLOC(count,:) = nan; else data.gammamodLOC(count,:) = mean(list.modu{2,3}(Flimit(2,1):Flimit(2,2),count),1);end
        %         Index(count,10:11) = [groups(find(CHcomb(pa,1) == ch)) groups(find(CHcomb(pa,2) == ch))];
        for g = 1 : 2
            if isempty(clark)
                com = ['find ' sprintf('''./session01/group%04.0f''',Index(count,9+g))];
            else
                load rules.mat
                thes = find(r == 1)+1;
                com = ['find ' sprintf('''./session0%d/group%04.0f''',thes,Index(count,9+g))];
                
            end
            [status(g), result{g}] = unix(com);
        end
        if ~isempty(result{1}) && ~isempty(result{2}) && sum(status) == 0;
            Index(count,29) = 1;
        else
            % temporary solution until the new days of clark are sorted Now
            % corrected all data of clark sorted
%             if isempty(clark)
                Index(count,29) = 0;
%             else
%                 Index(count,29) = 1;
%             end
        end
    end
    
    
    if Args.flat
        
        for rch = sort(iflat.pairs,'descend')'
            Index(rch,:) = [];
            data.betamodIDE(rch,:) = [];
            data.betamodLOC(rch,:) = [];
            data.gammamodIDE(rch,:) = [];
            data.gammamodLOC(rch,:) = [];
        end
    end
    %     for epair = 1 : 2; for nch = 1 : size(CHcomb,1); CHcombm(nch,epair) = find(CHcomb(nch,epair) == ch); end; end
    CHcombm = CHcomb;
    
    
    if Args.withSNR
        pdir = pwd;
        if ~isempty(betty); cd session01/highpass; else cd session02/highpass; end
        if isempty(nptDir('SNR_channels.mat'))
            for tg = 1 : size(CHcombm,1)
                Index(tg,24 : 25) = nan(1,2);
            end
        else
            
            load('SNR_channels.mat')
            
            for tg = 1 : size(CHcombm,1)
                Index(tg,24 : 25) = [channel_snr_list(find(channel_snr_list == Index(tg,10)),2) channel_snr_list(find(channel_snr_list == Index(tg,11)),2)];
            end
        end
        cd(pdir)
    end
    
    
    
else
    Index = [];
    
end
data.Index = Index;


