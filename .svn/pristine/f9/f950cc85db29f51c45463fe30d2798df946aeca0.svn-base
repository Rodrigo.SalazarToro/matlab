function velocity = fvtEyeVelocityHistogram(eye)
%
%velocity = fvtEyeVelocityHistogram(eye)
%This function calculates velocity histograms for the fixations
%and saccades (in degrees per sec) using the same filters as are
%used in determining the beginning and ends of the eye events (see fvtGenerateSessionEyeMovements)
%The input, "eye" is an eye object containing the start and finish times of
%fixations and saccades.
%The output is a structure with two fields:  velocity.fixation and velocity.saccade
%velocity.saccade is a 2 row vector with the first row corresponding to 
%mean velocities, and the second row being maximal velocities.
%the program assumes you have cd to the correct eye directory
%

velocity.sessionname = eye.data.sessionname;
dirlist = nptDir('*_eye.0*');

buffer = 15;        %Buffer (towards the center) from the end of each fixation
order1 = 6;         %Order of filter used on raw data (same as fvtGenerateSessionEyeMovements)
order2 = 15;        %Order of filter used on veloctiy (same as fvtGenerateSessionEyeMovements)
min_length = (buffer * 3) + 1;

fixation = [];
filt_saccade = [];
saccade = [];
for trial = 1:size(eye.data.fixation,2)
    filename = dirlist(trial).name;
    [data, num_channels, samples_per_sec, datatype, points] = nptReadDataFile(filename);
    %Change data to degrees
    [data(1,:) data(2,:)] = pixel2degree(data(1,:), data(2,:));
    b = ones(1, order1)/order1;
    %filter the raw data
    filtered = filtfilt(b, 1, data');	%13th order running average(boxcar) with no delay
    delta_vert = diff(filtered(:,1));
    delta_horiz = diff(filtered(:,2));
    distance = sqrt(delta_vert.^2 + delta_horiz.^2);
    unfilVel = distance * samples_per_sec;
    %filter the velocity signal
    c = ones(1, order2)/order2;         %31st order running average(boxcar) with no delay
    vel = filtfilt(c, 1, unfilVel);
    for event = 1:size(eye.data.fixation(trial).start, 2)
        %If the fixation is long enough to use the appropriate filters on
        start = eye.data.fixation(trial).start(event);
        finish = eye.data.fixation(trial).finish(event);
        if (finish - start) >= min_length
            %Remove the buffer sections at each end of the fixation before
            %finding the max velocity, which will be reported
            fixation_vel = vel((start + buffer):(finish - buffer));
            meanFixVel = mean(fixation_vel);
            maxFixVel = max(fixation_vel);
        else      %If the fixation isn't big enough to filter
            %use the unfiltered velocity
            fixation_vel = unfilVel(start:finish);
            meanFixVel = mean(fixation_vel);
            maxFixVel = max(fixation_vel);
        end
        fixation = concatenate(fixation, [meanFixVel; maxFixVel], 'Columnwise', 'DiscardEmptyA');
    end          %loop through all fixations in this trial
   
   for event = 1:size(eye.data.saccade(trial).start, 2)
      saccade = [saccade eye.data.saccade(trial).max_velocity(2, event)];
   end
end


velocity.fixation = fixation;
velocity.saccade = saccade;