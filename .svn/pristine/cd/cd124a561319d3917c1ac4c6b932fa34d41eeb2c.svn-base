function  makepwgramPolytrode(varargin)
% day = '20170426';
% sessions = nptDir('session0*');
% for ss = 1 : length(sessions)
%     cd(sessions(ss).name);
%     makepwgramPolytrode('name',sprintf('%s%d',day,ss));
%     makepwgramPolytrode('name',sprintf('%s%d',day,ss),'coherency');
%     cd .. ;
% end

Args = struct('redo',0,'name',[],'coherency',0);
Args.flags = {'redo','coherency'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sname = sprintf('%spwsummary.mat',Args.name);

n=1;
ff = nptDir(sprintf('*lfp_group0%0.2g.mat',n));
while ~isempty(ff)
    files(n).name = ff.name;
    n=n+1;
    ff = nptDir(sprintf('*lfp_group0%0.2g.mat',n));
    
end

if Args.redo || isempty(nptDir(sname))
    
    c = 1;
    for ff = 1 : length(files)
        out = load(files(ff).name);
        if isfield(out,'lfp')
            nname = regexprep(files(ff).name,'.mat','');
            [S,t,f] = pwgram(out.lfp,out.resampling,'save',modvarargin{:},'basename',nname,'savefig',0);
            mS(c,:) = squeeze(median(median(S,3),1));
            c = c + 1;
        end
    end
    save(sname,'mS','f','t')
    display(['saving ' pwd '\' sname])
end

%% coherency
cname = sprintf('%scohsummary.mat',Args.name);
if Args.coherency && (Args.redo || isempty(nptDir(cname)))
    grpairs = nchoosek([1:length(files)],2);
    c=1;
    pp = 1;
    gr1 = load(files(grpairs(pp,1)).name);
    gr2 = load(files(grpairs(pp,2)).name);
    bname = files(grpairs(pp,1)).name(1:10);
    aname = [files(grpairs(pp,1)).name(15:21) '_' files(grpairs(pp,2)).name(15:21)];
    [C,~,f,~] = cohgram(gr1.lfp,gr2.lfp,gr1.resampling,'addName',aname,'basename',bname,'save',modvarargin{:},'savefig',1);
    
    mC = nan(size(grpairs,1),length(f));
    %     mphi = nan(size(grpairs,1),length(f));
    mC(c,:) = median(median(median(C,3),1),2);
    %     mphi(c,:) = circ_median(circ_median(phi,3),1);
    c = 2;
    for pp = 2 : size(grpairs,1)
        gr1 = load(files(grpairs(pp,1)).name);
        gr2 = load(files(grpairs(pp,2)).name);
        bname = files(grpairs(pp,1)).name(1:10);
        aname = [files(grpairs(pp,1)).name(15:21) '_' files(grpairs(pp,2)).name(15:21)];
        [C,~,f,~] = cohgram(gr1.lfp,gr2.lfp,gr1.resampling,'addName',aname,'basename',bname,'save',modvarargin{:},'savefig',1);
        mC(c,:) = median(median(median(C,3),1),2);
        %         mphi(c,:) = circ_median(circ_median(phi,1),1);
        c = c + 1;
        
    end
    save(cname,'mC','f')
    display(['saving ' pwd '\' cname])
end

%%
function [S,t,f] = pwgram(lfp,resampling,varargin)

Args = struct('save',0,'redo',0,'savefig',0,'tapers',[3 5],'fpass',[.5 90],'mwindow',[10 2],'basename',[],'addName',[]);
Args.flags = {'save','redo','savefig'};
[Args,modvarargin] = getOptArgs(varargin,Args);

sname = sprintf('%spowergram%s.mat',Args.basename,Args.addName);

if Args.redo || isempty(nptDir(sname))
    
    params = struct('tapers',Args.tapers,'Fs',resampling,'fpass',Args.fpass);
    
    [S,t,f]=mtspecgramc(lfp',Args.mwindow,params);
    
    save(sname,'S','t','f')
    if Args.savefig
        
        for nf = 1 : ceil(size(S,3)/8);
            f1(nf) = figure;
            for sb = 1 : 8
                if size(S,3) >= ((nf-1)*8 + sb)
                    subplot(4,2,sb)
                    imagesc(t,f,squeeze(S(:,:,(nf-1) * 8 + sb))',[0 10^-3]); colorbar
                end
            end
            xlabel('time (s)')
            ylabel('frequency (Hz)')
            
            fname = sprintf('%spowergram%s%d.fig',Args.basename,Args.addName,nf);
            set(gcf,'Name',fname)
            saveas(f1(nf),fname)
            close(f1(nf))
        end
    end
else
    load(sname)
end
%%
function [C,phi,f,t] = cohgram(lfps1,lfps2,resampling,varargin)

Args = struct('save',0,'redo',0,'savefig',0,'tapers',[10 19],'fpass',[.5 90],'mwindow',[10 10],'basename',[],'addName',[]);
Args.flags = {'save','redo','savefig'};
[Args,modvarargin] = getOptArgs(varargin,Args);


sname = sprintf('%scohgram_%s.mat',Args.basename,Args.addName);

if Args.redo || isempty(nptDir(sname))
    
    params = struct('tapers',Args.tapers,'Fs',resampling,'fpass',Args.fpass);
    [Ct,phit,~,~,~,t,f] = cohgramc(lfps1(1,:)',lfps2(1,:)',Args.mwindow,params);
    
    C = single(nan(size(lfps1,1),size(lfps2,1),length(t),length(f)));
    phi = single(nan(size(lfps1,1),size(lfps2,1),length(t),length(f)));
    C(1,1,:,:) = single(Ct);
    phi(1,1,:,:) = single(phit);
    
    for ch1 = 1 : size(lfps1,1)
        for ch2 = 1 : size(lfps2,1)
            [Ct,phit,~,~,~,t,f] = cohgramc(lfps1(ch1,:)',lfps2(ch2,:)',Args.mwindow,params);
            C(ch1,ch2,:,:) = single(Ct);
            phi(ch1,ch2,:,:) = single(phit);
        end
    end
    save(sname,'C','t','f','phi')
    if Args.savefig
        f1 = figure;
        ch1s = randi(size(lfps1,1),[3 1]);
        ch2s = randi(size(lfps2,1),[3 1]);
        c=1;
        for ch1 = ch1s'
            for ch2 =ch2s'
                subplot(3,3,c)
                imagesc(t,f,squeeze(C(ch1,ch2,:,:))',[0 1]); colorbar
                title(sprintf('ch1 = %d; ch2 = %d',ch1,ch2))
                c=c+1;
            end
        end
        xlabel('time (s)')
        ylabel('frequency (Hz)')
        
        fname = sprintf('%scohgram%s.fig',Args.basename,Args.addName);
        set(gcf,'Name',fname)
        saveas(f1,fname)
        close(f1)
        
    end
else
    load(sname)
end










