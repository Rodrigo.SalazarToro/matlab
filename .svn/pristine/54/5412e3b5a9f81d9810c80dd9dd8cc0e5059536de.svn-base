function [varargout] = MTSGC(varargin)
% Computes the coherence on defined epochs (500ms; fixation,cue,delay1, delay2).
% The type of coherence can be chosen with the arguments:
% - cohPP: within the parietal electrodes
% - cohPF: within the frontal electrodes
% - cohInter: between parietal and frontal
% - surrogate: computes the surrogate distribution
%
% Additional arguments are:
% - stable to only analyze teh stable performance phase
% - BehResp to specify correct 1 or incorrect 0
% - 'save' to save the results in the day directory
% - 'redo' to redo the analysis even though the results have already been
% saved.
%
% The preprocessiong of data includes 60Hz removal and a linear detrend of
% the data.
% Nr = length(x)/101;
% fs = 200;
% Nl = 101;
% porder = 11;
% freq = (1:100);


Args = struct('redo',0,'save',0,'remoteName',[],'surrogate',0,'startDay',1,'days',[],'combineSession',0,'ML',0,'cohPP',0,'cohPF',0,'cohInter',0,'porder',22,'freq',[1 : 100],'noFlat',0,'sameNtrials',0,'InCor',0,'fixation',0);
Args.flags = {'redo','save','surrogate','ML','combineSession','noFlat','sameNtrials','InCor','fixation','cohPP','cohPF','cohInter'};
[Args,modvarargin] = getOptArgs(varargin,Args,'subtract',{'RedoLevels','SaveLevels'}, ...
    'shortcuts',{'redo',{'RedoLevels',1}; 'save',{'SaveLevels',1}}, ...
    'remove',{'Auto'});
AICmin = [];
if Args.combineSession; supname = 'cS'; else supname = []; end
if Args.InCor; BehResp = 'InCor';  else BehResp = []; end
if Args.surrogate; matfile = sprintf('%s/GC%s%sSur.mat',pwd,supname,BehResp); else matfile = sprintf('%s/GC%s%s.mat',pwd,supname,BehResp); end

rsFs = 200; % downsampling
plength = 400;
if isempty(Args.days)
    tdays = [nptDir('0*'); nptDir('1*')];
    for d = 1 : length(tdays); days{d} = tdays(d).name; end
else
    days = Args.days;
end
if ~isempty(Args.remoteName) && ~strcmp(Args.remoteName,'local')
    out = findResource('scheduler','type','jobmanager','LookupURL',sprintf('%s.cns.montana.edu',Args.remoteName));
    
end

for d = Args.startDay : length(days)
    
    cd(days{d});
    skip = nptDir('skip.txt');
    if isempty(skip)
        for flag = find(strcmp(Args.flags,'cohPP') == 1) : size(Args.flags,2); if eval(sprintf('Args.%s',Args.flags{flag})); selOpt = flag; end; end;
        if Args.surrogate; matfile = sprintf('%s/GC%s%s%sSur.mat',pwd,Args.flags{selOpt},supname,BehResp); else matfile = sprintf('%s/GC%s%s%s.mat',pwd,Args.flags{selOpt},supname,BehResp);end
        prematfile = nptDir(matfile);
        if isempty(prematfile) || Args.redo
            session = nptDir('session0*');
            scount = 1;
            clear allrules rsession
            if Args.ML
                ses = [];
                for s = 1 : size(session,1);cd(session(s).name);
                    mtsfile = nptDir('*.bhv');
                    skip = nptDir('skip.txt');
                    if strcmp('MTS',mtsfile(1).name(1:3)) && isempty(skip)
                        tmtst = mtstrial('auto');
                        if unique(tmtst.data.Index(:,1)) < 3
                            
                            if Args.combineSession; allrules = unique(tmtst.data.Index(:,1)); else allrules = unique(tmtst.data.Index(:,1));end%[tmtst.data.Index(find(diff(tmtst.data.Index(:,1)) ~= 0),1); tmtst.data.Index(end,1)];
                            for r = 1 : 2; if Args.fixation; minTrials(r) = length(mtsgetTrials(tmtst,modvarargin{:},'rule',r,'CueObj',55)); else; minTrials(r) = length(mtsgetTrials(tmtst,modvarargin{:},'rule',r));end;end; minTrials = min(minTrials);
                            ses = [ses repmat(s,1,length(allrules))];
                        end
                        if length(allrules) >= 2
                            cd ..
                            break
                        end
                    end
                    cd ..;
                end
            else
                for s = 2 : 3 %size(session,1);
                    cd(session(s).name);
                    tmtst = mtstrial('auto'); allrules(s-1) = unique(tmtst.data.Index(:,1));
                    minTrials(s-1) = length(mtsgetTrials(tmtst,modvarargin{:}));
                    cd ..;
                end
                minTrials = min(minTrials);  end
            if Args.combineSession && length(session) > 2; ses = [2 3]; for s = 1 : 2; rsession{s} = find(allrules == allrules(s));end; elseif ~Args.ML; ses = [2 : length(session)]; end
            %             cd(session(1).name); [~,~,CHcomb] = checkChannels(Args.flags{selOpt});cd ..;
            if Args.ML; cd session01; else cd session02; end; [channels,comb,CHcomb] = checkChannels(Args.flags{selOpt}); cd ..
            if Args.noFlat ; [ch,allCHcomb,iflat,groups] = getFlatCh('cohtype',Args.flags{selOpt}); channels = setdiff(ch,iflat.ch); CHcomb = setdiff(allCHcomb,allCHcomb(iflat.pairs,:),'rows');end
            if Args.surrogate
                Session = struct('x2yProb',{zeros(100,4,size(CHcomb,1),4) zeros(100,4,size(CHcomb,1),4)},'y2xProb',{zeros(100,4,size(CHcomb,1),4) zeros(100,4,size(CHcomb,1),4)},'xyProb',{zeros(100,4,size(CHcomb,1),4) zeros(100,4,size(CHcomb,1),4)});
            else
                Session = struct('Fx2y',{zeros(100,size(CHcomb,1),4) zeros(100,size(CHcomb,1),4)},'Fy2x',{zeros(100,size(CHcomb,1),4) zeros(100,size(CHcomb,1),4)},'Fxy',{zeros(100,size(CHcomb,1),4) zeros(100,size(CHcomb,1),4)});
            end
            
            for s = ses
                
                cd(session(s).name)
                
                skip = nptDir('skip.txt');
                
                if isempty(skip)
                    
                    [channels,comb,CHcomb] = checkChannels(Args.flags{selOpt});
                    if Args.noFlat; channels = setdiff(channels,iflat.ch); CHcomb = setdiff(allCHcomb,allCHcomb(iflat.pairs,:),'rows'); end
                    if  comb ~= -1
                        mtst = mtstrial('auto');
                        
                        cd('lfp')
                        skip = nptDir('skip.txt');
                        if isempty(skip)
                            files = nptDir('*_lfp.*');
                            if Args.ML
                                selectTrials = mtsgetTrials(mtst,modvarargin{:},'rule',allrules(scount));
                            else
                                selectTrials = mtsgetTrials(mtst,modvarargin{:});
                            end
                            if Args.sameNtrials; norder = selectTrials(randperm(length(selectTrials))); selectTrials = sort(norder(1:minTrials)); end
                            
                            if size(files,1) == mtst.data.numSets && ~isempty(selectTrials) && ~isempty(mtst)
                                count = 1;
                                [tdata,~,periods] = lfpPcut(selectTrials,channels,'plength',plength,modvarargin{:});
                                Nr = size(tdata{1},2);
                                Nl = size(tdata{1},1);
                                data = cell(4,1);for p = 1 : 4; data{p} = reshape(tdata{p},Nl * Nr,size(tdata{p},3))'; end
                                if Args.combineSession
                                    startd = pwd;
                                    for ns = rsession{s-1}(2:end)
                                        
                                        cd ../..
                                        cd(session(ns).name)
                                        %                                         [channels,comb,CHcomb] = checkChannels(Args.flags{selOpt});
                                        %                                         if Args.noFlat; channels = setdiff(channels,iflat.ch); CHcomb = setdiff(CHcomb,allCHcomb(iflat.pairs,:),'rows'); end
                                        skip = nptDir('skip.txt');
                                        
                                        if comb ~= -1 && isempty(skip)
                                            
                                            nmtst = mtstrial('auto');
                                            
                                            cd('lfp')
                                            skip = nptDir('skip.txt');
                                            if isempty(skip)
                                                nfiles = nptDir('*_lfp.*');
                                                
                                                nselectTrials = mtsgetTrials(nmtst,modvarargin{:});
                                                if size(nfiles,1) == nmtst.data.numSets && ~isempty(nselectTrials) && ~isempty(nmtst)
                                                    
                                                    [ndata,~,periods] = lfpPcut(nselectTrials,channels,'plength',plength,modvarargin{:});
                                                    Nr = size(ndata{1},2);
                                                    Nl = size(ndata{1},1);
                                                    data = cell(4,1);for p = 1 : 4; data{p} = [data{p} reshape(ndata{p},Nl * Nr,size(ndata{p},3))']; end
                                                end
                                                
                                            end
                                            cd ..
                                        end
                                        cd(startd);
                                    end
                                end
                                clear files
                                if comb == 2
                                    
                                    %                                     data2 = cell(length(periods)+(size(CHcomb,1)-1)*length(periods),1);
                                    if ~isempty(Args.remoteName) && ~strcmp(Args.remoteName,'local')
                                        data1 = cell(1,length(periods)+(size(CHcomb,1)-1)*length(periods));
                                        for cb = 1 : size(CHcomb,1)
                                            for p = 1 : length(periods)
                                                ch1 = find(channels == CHcomb(cb,1));
                                                ch2 = find(channels == CHcomb(cb,2));
                                                data1{p+(cb-1)*length(periods)} = single(data{p}([ch1 ch2],:)); % data{CHcomb(cb,1)}(lowLim:highLim,:);
                                                %                                                 data2{p+(cb-1)*length(periods)} = data{p}(ch2,:); % data{CHcomb(cb,2)}(lowLim:highLim,:);
                                            end
                                        end
                                    else
                                        if Args.surrogate;x2yProb = zeros(100,4,size(CHcomb,1),4);y2xProb = zeros(100,4,size(CHcomb,1),4);xyProb = zeros(100,4,size(CHcomb,1),4); else Fx2y = zeros(100,size(CHcomb,1),4); Fy2x = zeros(100,size(CHcomb,1),4); Fxy = zeros(100,size(CHcomb,1),4);end
                                        
                                        p = 1;
                                        for p = 4%1 : 4
                                            for cb = 1 : size(CHcomb,1)
                                                ch1 = find(channels == CHcomb(cb,1));
                                                ch2 = find(channels == CHcomb(cb,2));
                                                if Args.surrogate
                                                    [x2yProb(:,:,cb,p),y2xProb(:,:,cb,p),xyProb(:,:,cb,p)] = GCcohsurrogate(data{p}([ch1 ch2],:),size(data{p},2)/Nl,Nl,Args.porder,rsFs,Args.freq);
                                                else
                                                    bic = zeros(1,30);
                                                    Nr = size(data{p},2)/Nl;
                                                    for porder = 1 : 30
                                                        [A2,Z2] = armorf(data{p}([ch1 ch2],:),Nr,Nl,porder);
%                                                         aic(porder) = AICfromAR(Z2,4,porder,80);
                                                        bic1 = 2*log(det(Z2));
                                                        bic2 = (2*(2^2)*porder*log(Nr*Nl))/(Nr*Nl);
                                                        
                                                        
                                                        bic(porder) = bic1 + bic2;
                                                    end
                                                    
                                                    plot(bic); hold on
                                                    
%                                                     [~,~,Fx2y(:,cb,p),Fy2x(:,cb,p),Fxy(:,cb,p),~]= pwcausalrp(data{p}([ch1 ch2],:),size(data{p},2)/Nl,Nl,Args.porder,rsFs,Args.freq);
                                                end
                                            end
                                        end
                                        if Args.surrogate
                                            Session(scount).x2yProb = x2yProb; Session(scount).y2xProb = y2xProb; Session(scount).xyProb = xyProb;
                                            
                                        else
                                            Session(scount).Fx2y = Fx2y; Session(scount).Fy2x = Fy2x; Session(scount).Fxy = Fxy;
                                        end
                                    end
                                    
                                    clear data
                                    if ~isempty(Args.remoteName)
                                        if ~strcmp(Args.remoteName,'local')
                                            fprintf('Data sent to %s on %g %g %gth at %gh%g %g sec. \n',Args.remoteName,clock)
                                            if Args.surrogate
                                                [x2yProb,y2xProb,xyProb] = dfeval(@GCcohsurrogate,data1,repmat({size(data1{p},2)/Nl},1,length(data1)),repmat({Nl},1,length(data1)),repmat({Args.porder},1,length(data1)),repmat({rsFs},1,length(data1)),repmat({Args.freq},1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                                
                                            else
                                                %                                             [C,phi,S12,S1,S2,ftemp,confC,Phierr,Cerr] = dfeval(@GC,data1,data2,repmat(params,1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                                [~,~,Fx2y,Fy2x,Fxy,~]= dfeval(@pwcausalrp,data1,repmat({size(data1{p},2)/Nl},1,length(data1)),repmat({Nl},1,length(data1)),repmat({Args.porder},1,length(data1)),repmat({rsFs},1,length(data1)),repmat({Args.freq},1,length(data1)),'lookupURL',sprintf('%s.cns.montana.edu',Args.remoteName),'configuration','jobmanager','jobmanager',out(1).Name,'StopOnError',true);
                                            end
                                        end
                                        clear data1 data2
                                        if Args.surrogate; var = {'x2yProb','y2xProb','xyProb'}; else var = {'Fx2y','Fy2x','Fxy'};end
                                        for cb = 1 : size(CHcomb,1)
                                            countV = length(periods);
                                            for p = 1 : countV
                                                for v = 1 : length(var)
                                                    if ~isempty(findstr(var{1},'Prob'))
                                                        eval(sprintf('Session(scount).%s(:,:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                    else
                                                        eval(sprintf('Session(scount).%s(:,cb,p) = %s{p+(cb-1)*length(periods)};',var{v},var{v}));
                                                    end
                                                end
                                                
                                            end
                                        end
                                        
                                        clear pp cohe Fx2y Fy2x Fxy rp
                                    end
                                end
                                
                            else
                                fprintf('!!!!!!Problem: different # of trials in lfp folder than in main folder or no trial selected');
                            end
                            
                        end
                        
                        cd .. % session
                        if exist('Session','var')
                            if Args.ML
                                Session(scount).rule = scount;
                                Session(scount).trials = selectTrials;
                            else
                                Session(scount).rule = unique(mtst.data.Index(:,1));
                            end
                            scount = scount + 1;
                        end
                        
                    end
                    
                    cd .. % day
                else
                    cd ..
                end %s = 2 : size(session,1)
                
                %cd ..
            end
            
        end
    end
    
    if exist('Session','var')
        Day.session = Session;
        Day.comb = CHcomb;
        Day.name = pwd;
        Day.option = varargin;
        clear Session
    end
    if Args.save && exist('Day','var'); fprintf('\n saving file %s \n',matfile); f = Args.freq; save(matfile,'Day','f'); clear Day; end
    
    cd ..
    
end %for d = 1 : size(days,1)
varargout{1} = AICmin;
