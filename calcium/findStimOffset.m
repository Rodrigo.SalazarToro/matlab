function [starts,ends,tduration] = findStimOffset(stimulation,filename,varargin)
% find teh onset and offset of the stimulations TTL
Args = struct('threshold',1,'plot',0,'save',0,'redo',0);
Args.flags = {'plot','save','redo'};
[Args] = getOptArgs(varargin,Args,'remove',{});

figname = regexprep(filename,'stimulation.mat','stimOnOffset.fig');
if isempty(nptDir(figname)) || Args.redo
    
    Args.upthreshold = .5 *max(stimulation);
    stimulation(stimulation > Args.upthreshold) = Args.upthreshold + 10;
    der1 = diff(stimulation);
    
    ends = find(der1(1:end-1) > Args.threshold & der1(2:end) < Args.threshold) + 2;
    
    starts = find(abs(der1(1:end-1)) < Args.threshold & der1(2:end) > Args.threshold) + 1;
    
    for ii = 1 : length(ends)
        try
            if max(stimulation(starts(ii) : ends(ii))) < Args.upthreshold
                starts(ii) = [];
                ends(ii) = [];
            end
            
            if stimulation(starts(ii)) > Args.threshold
                starts(ii) = starts(ii) - 1;
            end
            if stimulation(ends(ii)) > Args.threshold
                ends(ii) = ends(ii) + 1;
            end
            
        end
    end
    tduration = min(diff(starts));
    if Args.save
        save(filename,'starts','ends','tduration')
    end
    if Args.plot
        plot(stimulation)
        hold on
        plot(starts,zeros(length(starts),1),'r*')
        plot(ends,zeros(length(ends),1),'g*')
        hold off
        legend('stim','starts','ends')
        set(gcf,'Name',pwd)
        if Args.save; saveas(gcf,figname); end
    end
else
    load(filename)
end