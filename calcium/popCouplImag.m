function [coupling,zcoupling] = popCouplImag(traces,varargin)
% cell x time in DF/F
% from Okun et al. 2015


ncell = size(traces,1);

stdTraces = std(traces,[],2);
nTraces = traces - repmat(mean(traces,2),1,size(traces,2));

 ctemp = zeros(ncell,1);
for unit = 1 : ncell
    otherunits = setdiff([1:ncell], unit);
    pop = sum(nTraces(otherunits,:));
   
    for ti = 1 : size(traces,2)
      ctemp(unit) = ctemp(unit) + traces(unit,ti) * pop(ti);
    end
    
end

coupling = ctemp ./ stdTraces;

zcoupling = (coupling - mean(coupling)) / std(coupling);