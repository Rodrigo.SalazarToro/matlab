function [neuron,varargout] = zscorecalcium(calcium,starts,tduration,varargin)
%
% input = calcium (time x cells); the first "cell" is teh frame number
% starts = vector of trials starts in data points
% tduration = min duration of all of the trials in data point
%
% output:
% neuron = cell of neurons with time x trials
Args = struct('nofiltering',0,'sampling_rate',10,'resampling_rate',2,'low_freq_limit',.0001,'high_freq_limit',.001,'plot',0,'savefig',0);
Args.flags = {'nofiltering','plot','savefig'};
[Args] = getOptArgs(varargin,Args,'remove',{});


ncell = size(calcium,2)-1; 

ntrials = length(starts);
% lcalcium = zeros(floor(size(calcium,1)/2),size(calcium,2));
% filtering the data
if ~Args.nofiltering
    for cel = 2 : size(calcium,2)
    [lcalcium(:,cel),~] = nptLowPassFilter(calcium(:,cel),Args.sampling_rate,Args.resampling_rate,Args.low_freq_limit,Args.high_freq_limit);
    end
    starts = floor(starts * (Args.resampling_rate / Args.sampling_rate));
    tduration =  floor(tduration * (Args.resampling_rate / Args.sampling_rate));
end
if Args.plot
    
    scells = randi(ncell,6,1);
    
    for cc = 1 : 6
    subplot(6,1,cc)
    try
    plot(lcalcium(:, scells(cc)+1))
    catch
      plot(calcium(:,scells(cc)+1))  
        
    end
    hold on
    plot(starts,0,'r+')
    hold on
    plot(starts + tduration,0,'k+')
    end
    legend('calcium','starts','end')
    linkedzoom onx
    if Args.savefig; saveas(gcf,'Examples preprocessed Traces'); end
end
calcium = lcalcium;
%% z-score of the data
neuron = cell(ncell,1);
for c = 1 : ncell
    for t = 1 : ntrials
        if (starts(t) + tduration) <= size(calcium,1)
            neuron{c}(:,t) = (calcium(starts(t) : starts(t) + tduration,c + 1) - mean(calcium(starts(t) : starts(t) + tduration,c + 1))) / std(calcium(starts(t) : starts(t) + tduration,c + 1));
       
        end
    end
end

varargout{1} = Args.resampling_rate;
