function [threshlow,threshhigh,Rdiff,Rdiffsur,varargout] = compareCorrTables(neuron1,neuron2,starts,tlength,combp,varargin)

Args = struct('iterations',10000,'pvalue',[5 1 0.1 0.01 0.005],'plot',0,'save',[]);
Args.flags = {'plot'};
[Args] = getOptArgs(varargin,Args,'remove',{});


ntrial1 = size(neuron1{1},2);
ntrial2 = size(neuron2{1},2);
alltrial = ntrial1 + ntrial2;
ncells = length(neuron1);


poolneuron = cell(ncells,1);
for nc = 1 : ncells
    poolneuron{nc} = cat(2,neuron1{nc},neuron2{nc});
end
rowdist = size(poolneuron{1},1);
poolneuron = cell2mat(poolneuron);

R1sur = nan(ncells,ncells,Args.iterations);
R2sur = nan(ncells,ncells,Args.iterations);
Rdiffsur = nan(ncells,ncells,Args.iterations);

for ii = 1 : Args.iterations
    rseq = randperm(alltrial);
    st1 = rseq(1:ntrial1);
    st2 = rseq(ntrial1 + 1 : end);
    
    neuront1 = mat2cell(poolneuron(:,st1),repmat(rowdist,size(poolneuron,1)/rowdist,1),length(st1));
    neuront2 = mat2cell(poolneuron(:,st2),repmat(rowdist,size(poolneuron,1)/rowdist,1),length(st2));
    
    R1sur(:,:,ii) = makeCorrTable(neuront1,starts,tlength,combp);
    R2sur(:,:,ii) = makeCorrTable(neuront2,starts,tlength,combp);
    Rdiffsur(:,:,ii) = R1sur(:,:,ii) - R2sur(:,:,ii);
end
threshlow = cell(length(Args.pvalue),1);
threshhigh = cell(length(Args.pvalue),1);

for pp = 1 : length(Args.pvalue);
    threshlow{pp} = prctile(Rdiffsur,Args.pvalue(pp)/2,3);
    threshhigh{pp} = prctile(Rdiffsur,100 - (Args.pvalue(pp))/2,3);
end

R1 = makeCorrTable(neuron1,starts,tlength,combp);
R2 = makeCorrTable(neuron2,starts,tlength,combp);


Rdiff = R1 - R2;
varargout{1} = R1;
varargout{2} = R2;

if ~isempty(Args.save)
    
    save(Args.save,'threshlow','threshhigh','Rdiff','Rdiffsur');
    display([pwd '/' Args.save])
end