function processExp(varargin)

% main summary script that finds teh stim onset and offset and run main
% preprocessing (z-score)
% all the blocs below should be run in sequence
% run this code in the directory where all of the experiments directory is
% a file with format "*stimulation.txt" et "*calcium.txt" doivent etre
% present et tous le format avant doivent etre similaire pour *calcium.txt
% et * *stimulation.txt

% use args 'nofiltering' for no filtering of teh raw data
%
diary('on')
Args = struct('redo',0,'expType','exp*','samplingRate',4,'resamplingRate',4,'TrialDuration',5,'cscale',[-.3 .7]);
Args.flags = {'redo'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

%% find on and offset and loop through all directories
% dirs = nptDir('Control*');
startdir = pwd;
dirs = nptDir(Args.expType);
c = 1;
samplingRate = Args.samplingRate;
resamplingRate = Args.resamplingRate;
secDuration = Args.TrialDuration;
% tduration = min(minTlength) - 4;
nframes =  secDuration * samplingRate;
for d = 1 : length(dirs)
    cd(dirs(d).name)
    stims = nptDir('*stimulation.txt*');
    if ~isempty(stims)
        for ff = 1 : length(stims)
            stimulation = load(stims(ff).name);if size(stimulation,2) == 2; stimulation = stimulation(:,2); end
            filename = sprintf('%s.mat',stims(ff).name(1:end-4));
            [starts,ends, minTlength(c)] = findStimOffset(stimulation,filename,'plot','save');
            
            
            display([pwd '/' filename])
            c = c + 1;
        end
    else
        display('File missing. WATCH OUT')
    end
    cd ..
end

%% z-score for each trial separately and loops through all the directories

cd(startdir)
for d = 1 : length(dirs)
    cd(dirs(d).name)
    stims = nptDir('*stimulation.mat');
    if ~isempty(stims)
        for ff = 1 : length(stims)
            stimulation = load(stims(ff).name);
            calcium = load(regexprep(stims(ff).name,'stimulation.mat','calcium.txt'));
            filename = regexprep(stims(ff).name,'stimulation.mat','neuron.mat');
            if isempty(nptDir(filename)) || Args.redo
                neuron = zscorecalcium(calcium,stimulation.starts,nframes,'sampling_rate',samplingRate,'resampling_rate',resamplingRate,'savefig','plot',modvarargin(:));
                clf
                
                save(filename,'neuron')
                display([pwd '/' filename])
            end
            
            
        end
    end
    cd ..
end
%% make the correlation of all possible pairs of neurons and create teh whole trial correlation matrix and the time-dependent correlations matrices
cd(startdir)
cscale = Args.cscale;
nframes =  secDuration * resamplingRate;
for d = 1 : length(dirs)
    cd([startdir filesep dirs(d).name])
    stims = nptDir('*stimulation.mat');
    if ~isempty(stims)
        for ff = 1 : length(stims)
            nfilename = regexprep(stims(ff).name,'stimulation.mat','corrTable.mat');
            if isempty(nptDir(nfilename)) || Args.redo
                stimulation = load(stims(ff).name);
                filename = regexprep(stims(ff).name,'stimulation.mat','neuron.mat');
                neuron = load(filename);
                neuron = neuron.neuron;
                combp = nchoosek([1:length(neuron)],2);
                
                R = makeCorrTable(neuron,1,2,combp,'surrogate','save',regexprep(filename,'neuron','corrTableSur3bins'));
                corrts = corrTime(neuron,combp,'save','filename',nfilename,'R',R);
             
                display([pwd '/' nfilename])
            else
                out = load(nfilename);
                corrts = out.corrts;
                R = out.R;
            end
            
            subplot(3,3,1)
            imagesc(R,cscale);
            title('Whole trial')
            for sb = 1 : 7
                subplot(3,3,sb+ 1)
                imagesc(squeeze(corrts(:,:,sb)),cscale)
                title(['Frame Binned ' num2str(sb)]);
            end
            
            saveas(gcf,regexprep(stims(ff).name,'stimulation.mat','corrTable.fig'));
            clf
        end
    end
    cd ..
end

%% old code to make a bootstrap comparisons of two correlation matrices Currently not used
% cd(startdir)
% parfor d = 1 : length(dirs)
%     cd([startdir '/' dirs(d).name])
%     neu1 = nptDir('*_BF_neuron.mat');
%     neuron1 = load(neu1.name);
%     
%     neu2 = nptDir('*_AF_neuron.mat');
%     
%     neuron2 = load(neu2.name);
%     combp = nchoosek([1:length(neuron2.neuron)],2);
%     filename = sprintf('%s_%s_comp.mat',neu1.name(1:end -4),neu2.name(1:end -4));
%     [threshlow,threshhigh,Rdiff,Rdiffsur] = compareCorrTables(neuron1.neuron,neuron2.neuron,1,3,combp,'save',filename);
%     
%     
%     cd(sdir)
% end
%% make the correlation matrix and teh significance level for the 3 frames after the stim onset.
% old code. Now the fitting has been integrated in the makeCorrTable.m
% cd(startdir)
% 
% for d = 1 : length(dirs)
%     cd([startdir '/' dirs(d).name])
%     stims = nptDir('*stimulation.mat');
%     if ~isempty(stims)
%         for ff = 1 : length(stims)
%             stimulation = load(stims(ff).name);
%             filename = regexprep(stims(ff).name,'stimulation.mat','neuron.mat');
%             neu = load(filename);
%             combp = nchoosek([1:length(neu.neuron)],2);
%             nfilename = regexprep(stims(ff).name,'stimulation.mat','corrTableSur3bins.mat');
%             
%             makeCorrTable(neu.neuron,1,1,combp,'save',nfilename,'surrogate');
%             
%         end
%     end
%     
% end


%%
cd(startdir)


summaryScript

% sdir = pwd;
% dirs = nptDir('Contr*');
% for d = 1 : length(dirs)
%     cd([sdir '/' dirs(d).name])
%     combineStatsCorrTables
% end



