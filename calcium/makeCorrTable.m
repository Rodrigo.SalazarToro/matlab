function [R,varargout] = makeCorrTable(neuron,starts,tlength,combp,varargin)
%
% makes teh correlation table for apossible pairs of neurons
% Inputs:
%
% neurons: cell{#neurons}{time x trials
% tonset: starts of the trial in data points
% tduration: number of data points after trial onset to be used
%
% outputs:
%
% R : correlation table ncells x ncells

Args = struct('redo',0,'Fs',3.91,'tapers',[2 3],'multitapers',0,'plot',0,'surrogate',0,'iterations',10000,'pvalue',[5 1 0.1 0.01 0.005],'estpvalues',[10^-5 10^-6 10^-7 1-10^-5 1-10^-6 1-10^-7],'save',[]);
Args.flags = {'multitapers','plot','surrogate','redo'};
[Args] = getOptArgs(varargin,Args,'remove',{});

ncell = length(neuron);
Rsur = nan(ncell,ncell,Args.iterations);
if Args.redo || isempty(Args.save) || isempty(nptDir(Args.save))
    
    ncell = length(neuron);
    
    ntrials = size(neuron{1},2);
    
    R = nan(ncell,ncell);
    for p = 1 : size(combp,1)
        
        Rt = corrcoef(reshape(neuron{combp(p,1)}(starts : starts + tlength,:),(tlength +1) * ntrials,1),reshape(neuron{combp(p,2)}(starts : starts + tlength,:),(tlength +1) * ntrials,1));
        R(combp(p,1),combp(p,2)) = Rt(1,2);
    end
    
    Rsur = nan(ncell,ncell,Args.iterations);
    if Args.surrogate
        for ii =  1 : Args.iterations
            ntrials = size(neuron{1},2);
            newtrial1 =  randperm(ntrials);
            newtrial2 =  randperm(ntrials);
            seq = [starts : starts + tlength];
            newseq1 = seq(randperm(tlength +1));
            newseq2 = seq(randperm(tlength +1));
            for p = 1 : size(combp,1)
                
                Rt = corrcoef(reshape(neuron{combp(p,1)}(newseq1,newtrial1),(tlength +1) * ntrials,1),reshape(neuron{combp(p,2)}(newseq2,newtrial2),(tlength +1) * ntrials,1));
                Rsur(combp(p,1),combp(p,2),ii) = Rt(1,2);
            end
        end
        Rsur = single(Rsur);
        varargout{1} = Rsur;
        specvar = whos('Rsur');
        if specvar.bytes >= 10^9
            display('Variable too big cannot be saved!!!!!!!!!!!!!!!!!')
        end
        
        lowprctile = cell(length(Args.pvalue),1);
        highprctile = cell(length(Args.pvalue),1);
        for pp = 1 : length(Args.pvalue);
            lowprctile{pp} = prctile(Rsur,Args.pvalue(pp)/2,3);
            highprctile{pp} = prctile(Rsur,100 - (Args.pvalue(pp))/2,3);
        end
        %% fitting with gaussian and higher p-values estimates
        
        thresh = nan(ncell,ncell,6);
        for cb = 1 : size(combp,1)
            [~,~,thresh(combp(cb,1),combp(cb,2),:),~] = normfitSur(Rsur(combp(cb,1),combp(cb,2),:),Args.estpvalues,'twosided');
        end
        varargout{2} = lowprctile;
        varargout{3} = highprctile;
        varargout{4} = thresh;
    end
    
elseif  ~isempty(nptDir(Args.save))
    load(Args.save)
end



%%
if Args.multitapers
    params =struct('tapers',Args.tapers,'Fs',Args.Fs,'trialave',1);
    
    C = cell(size(combp,1),1);
    phi = cell(size(combp,1),1);
    S1 = cell(size(combp,1),1);
    S2 = cell(size(combp,1),1);
    
    for p = 1 : size(combp,1)
        [C{p},phi{p},S12,S1{p},S2{p},f] = coherencyc(neuron{combp(p,1)},neuron{combp(p,2)},params);
        
    end
end
%%

if Args.plot
    figure;
    imagesc(R)
    colorbar
    
end

if ~isempty(Args.save)
    f1=figure;
    nthres = length(Args.estpvalues)/2;
    for sb = 1 : nthres
        subplot(nthres,1,sb)
        imagesc(R > squeeze(thresh(:,:,nthres + sb)))
        colorbar
        title(sprintf('p < %g',Args.estpvalues( sb)))
        caxis([0 1 ])
    end
    saveas(gcf,regexprep(Args.save,'corrTableSur3bins.mat','corrTableDifpvalues.fig'))
    close(f1)
    
    save(Args.save,'R','Rsur','lowprctile','highprctile','thresh');
    display([pwd '\' Args.save])
end
