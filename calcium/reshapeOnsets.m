function nonsets = reshapeOnsets(onsets,starts,delay)

ncell = length(onsets);
ntrial = length(starts);
triallength = min(diff(starts)) - delay;

nonsets = cell(ncell,ntrial);
for n = 1 : ncell
    for t =  1 : ntrial
        ntimes = onsets{n} - starts(t) + delay;
        nonsets{n,t} = ntimes(ntimes > 0 & ntimes < triallength);
        
        
    end
end