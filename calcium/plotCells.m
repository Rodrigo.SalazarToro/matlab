function plotCells(cell1,cell2,varargin)

Args = struct('fromRaw',0,'withStimulation',0);
Args.flags = {'fromRaw','withStimulation'};
[Args] = getOptArgs(varargin,Args,'remove',{});

if Args.fromRaw
    AFfile = nptDir('*AF_calcium.txt');
    BFfile = nptDir('*BF_calcium.txt');
    AF = load(AFfile.name);
    BF = load(BFfile.name);
    
    subplot(2,1,1)
    plot(BF(:,cell1))
    hold on
    plot(BF(:,cell2),'r')
    title('Before plasticity')
    
    if Args.withStimulation
        stimfile = nptDir('*_BF_stimulation.mat');
        load(stimfile.name)
        
        line([starts'; starts'],[repmat(min(min(BF(:,[cell1,cell2]))),1,length(starts)); repmat(max(max(BF(:,[cell1,cell2]))),1,length(starts))],'Linestyle','--','Color','k'); 
    end
    subplot(2,1,2)
    plot(AF(:,cell1))
    hold on
    plot(AF(:,cell2),'r')
    if Args.withStimulation
        stimfile = nptDir('*_AF_stimulation.mat');
        load(stimfile.name)
        
        line([starts'; starts'],[repmat(min(min(AF(:,[cell1,cell2]))),1,length(starts)); repmat(max(max(AF(:,[cell1,cell2]))),1,length(starts))],'Linestyle','--','Color','k'); 
    end
    title('After plasticity')
    
    xlabel('Time (data points)')
    ylabel('Fluorescence')
else
    thefiles = nptDir('*neuron.mat');
    
    for ff = 1 : length(thefiles)
        load(thefiles(ff).name)
        subplot(2,length(thefiles),ff)
        plot(neuron{cell1})
        title(sprintf('%s',thefiles(ff).name))
        subplot(2,length(thefiles),ff+ length(thefiles))
        plot(neuron{cell2})
        
    end
    
    xlabel('time in bins')
    ylabel(' Fluorescence')
    
end