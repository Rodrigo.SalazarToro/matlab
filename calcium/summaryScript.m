sdir = pwd;
dirs = nptDir('exp*');
sumpairs = zeros(3,1);
totpairs = 0;
for d = 1: length(dirs)
    cd([sdir '/' dirs(d).name])
    [sigpairs,~, categories] =  combineStatsCorrTables;
    totpairs = totpairs + ((size(sigpairs{1},1)^2- size(sigpairs{1},1))/2) ;
    tsumpairs = zeros(3,1);
    for ca = 1 : 3
        tsumpairs(ca) = nansum(nansum(sigpairs{ca}));
        sumpairs(ca) = sumpairs(ca) + tsumpairs(ca);
    end
    f1 = figure;
    bar(tsumpairs)
    set(gca,'XtickLabel',categories)
    ylabel('# of pairs')
    saveas(gcf,'Number of pairs with significant correlations AF-BF')
    close(f1)
    cd(sdir)
    
end

f1=figure;

bar(100*sumpairs/totpairs)
title(sprintf('Total # of pairs: %d',totpairs))
set(gca,'XtickLabel',categories)
ylabel('% pairs')
saveas(gcf,'Summary AF-BF')
close(f1)