function [varargout] = combineStatsCorrTables(varargin)

Args = struct('save',0,'redo',0,'plot',0);
Args.flags = {'save','redo','plot'};
[Args] = getOptArgs(varargin,Args,'remove',{});


fileAF = nptDir('*AF_corrTableSur3bins.mat');
fileBF = nptDir('*BF_corrTableSur3bins.mat');
af = load(fileAF.name);
bf = load(fileBF.name);

thefile = regexprep(fileAF.name,'AF_corrTableSur3bins','sigAFvssigBF');

if isempty(nptDir(thefile)) || Args.redo
    
    sigAF = af.R < af.thresh(:,:,1) | af.R > af.thresh(:,:,4);
    
    
    sigBF = bf.R < bf.thresh(:,:,1) | bf.R > bf.thresh(:,:,4);
    
    % pairs with sig corr on both conditions
    
    sigpairs{1} = sigAF .* sigBF;
    
    
    % pairs with lost corr
    
    sigpairs{2} = sigBF .* ~sigAF;
    
    % pairs with gained corr
    
    sigpairs{3} = sigAF .* ~sigBF;
    
    % pairs with no corr
    
    sigpairs{4} = ~sigAF .* ~sigBF;
    
    celllevel = cell(4,1);
    for sb = 1 : 4
        
        celllevel{sb} = sum(sigpairs{sb},1) + sum(sigpairs{sb},2)';
    end
else
    load(thefile)
end

categories = {'pairs with sig corr on both conditions' 'pairs with lost corr' 'pairs with gained corr' 'pairs with no corr'};

if Args.plot
    figure
    set(gcf,'name',pwd)
    
    for sb = 1 : 4
        
        subplot(4,2,(sb -1 ) * 2 + 1)
        
        imagesc(sigpairs{sb});
        title(categories{sb})
        subplot(4,2,(sb - 1) * 2 + 2);
        
        plot(celllevel{sb})
        xlabel('Cell #')
        ylabel('Pop. weight')
        
    end
    
end

varargout{1} = sigpairs;

varargout{2} = celllevel;

varargout{3} = categories;
% sigCorr = sigAF | sigBF;

%
% filecomp = nptDir('*_comp.mat');
%
% comp = load(filecomp.name);
%
% sigcomp = (comp.Rdiff > comp.threshhigh{1} | comp.Rdiff < comp.threshlow{1});
%
% sigcomp(sigCorr == 0) = 0;
%
% combsig = sigcomp + sigCorr;
if Args.save
    
    save(thefile,'sigpairs','celllevel')
    display([pwd '/' thefile])
    
end