cd H:\stephane&Rod\awake_RWS_dataset

clear ll
files = {'*BF_calcium.txt' '*AF_calcium.txt'};

files2 = {'Fluo_BF_*.txt' 'Fluo_AF_*.txt'};
dirs = nptDir('exp*');
coupling = cell(2,1);

fluo = cell(2,1);
for d = 1 : length(dirs)
    cd(dirs(d).name)
    for ff = 1 : 2
        file = nptDir(files{ff});
        calcium=load(file.name);
        
        traces = calcium(:,2:end)';
        [tcoup,~] = popCouplImag(traces);
        coupling{ff} = [coupling{ff} tcoup'];
        
        file = nptDir(files2{ff});
        fluot = load(file.name);
        fluo{ff} = [fluo{ff} fluot'];
    end
    
    cd ..
end

figure
subplot(2,1,1)
boxplot([coupling{1}; coupling{2}]')
subplot(2,1,2)
boxplot(coupling{2}-coupling{1})

title('coupling diff after-before')


figure
for ff = 1 : 2
    subplot(2,1,ff)
    
    plot(coupling{ff},fluo{ff},'.')
%     ylim([-0.05 0.7])
%     xlim([-6 2])
    ylabel('fluo index')
    xlabel('Pop coupling z_score')
    title(files{ff})
    
end

figure

plot(coupling{2} -coupling{1},fluo{2} - fluo{1},'.') 
xlabel('diff coupling'); ylabel('diff fluo')
title('After minus before')