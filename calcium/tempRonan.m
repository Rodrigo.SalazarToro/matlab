ncells = length(all_boutons);
maxdur = size(all_boutons{1},1);
zscoreB = cell(ncells,1);
for c = 1 : ncells; 
    zscoreB{c} = (all_boutons{c} - repmat(mean(all_boutons{c},1),maxdur,1)) ./ repmat(std(all_boutons{c},1),maxdur,1); 
end


[R] = makeCorrTable(all_boutons,1,size(all_boutons{1},1)-1,nchoosek([1:length(all_boutons)],2),'plot');
set(gcf, 'Name','from raw data')
savefig('correlation matrice from raw data')
[R] = makeCorrTable(zscoreB,1,size(all_boutons{1},1)-1,nchoosek([1:length(all_boutons)],2),'plot');

set(gcf, 'Name','from z-score')
savefig('correlation matrice from z-score')



for sameN = 0 : max(same_axon)
    ind = find(same_axon == sameN);
    [R] = makeCorrTable(all_boutons(ind),1,size(all_boutons{1},1)-1,nchoosek([1:length(ind)],2),'plot');
    set(gcf, 'Name','from raw data')
    savefig(sprintf('correlation matrice from raw data for neurons belonging to neuron %d',sameN))
    [R] = makeCorrTable(zscoreB(ind),1,size(all_boutons{1},1)-1,nchoosek([1:length(ind)],2),'plot');
    set(gcf, 'Name','from z-score')
    savefig(sprintf('correlation matrice from z-score for neurons belonging to neuron %d',sameN))
end