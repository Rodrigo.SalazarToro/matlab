function [mcalcium,tim,cexpV,confI,varargout] = MIcalcium(calcium,categorie,varargin)
%
% calcium in  frame x trial
% categorie in frame x trial or in 1 x trial 
% The fct calculates the mutual information in a sliding window over time 
%
% optional input:
%
% 'nodetrend' : does not apply the default linear detrend
% 'lwind' : [window step] window length and step in frame for the sliding
% window
% 'iterations' : nbr of iterations for the surrogate
% 'nbin' : number of bins to calculate the expected value of the surrogate
% 'pvalues' : pvalues to use for the confidence intervals tested only
% one-side
% 'plot' : makes the plot
% 'title' : put a title to the plot
% 'figure' : put the plot in a specific figure
% 'subplot' : put the plot in a specific subplot (format [2 2 3] for a
% subplot(2,2,3)
%
% outputs:
% mcalcium : raw mutual inforamtion values over time
% tim: time vector in frame (correspond to the middle of the sliding
% window)
% cexpV: mutual information bias
% confI: significance level according to the p-values chosen tested
% one-sided
%
% optional output:
% {1} : surrogate distribution.
Args = struct('nodetrend',0,'lwind',[30 5],'iterations',10000,'nbin',500,'plot',0,'pvalues',[.05 .01 .001 .0001],'title',[],'figure',[],'subplot',[]);
Args.flags = {'nodetrend','plot'};
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});


%% linear detrend
if ~Args.nodetrend
    temp =reshape(calcium,size(calcium,1) * size(calcium,2),1);
    temp = detrend(temp);
    dcalcium = reshape(temp,size(calcium,1),size(calcium,2));
    
end
%%
ntrial = size(calcium,2);
nframe = size(calcium,1);

if Args.lwind(1)==Args.lwind(2)
    nw = fix(nframe/Args.lwind(2));
else
    nw = round((nframe - Args.lwind(1)) / Args.lwind(2));
end



mcalcium = zeros(nw,1);


scalcium = zeros(nw,Args.iterations);

tim = zeros(nw,1);

if ~(sum(size(calcium) == size(categorie)) == 2)
    
  categorie = repmat(categorie,nframe,1);
end

for w = 1 : nw
    wirange = (w-1) * Args.lwind(2) + 1: (w-1)*Args.lwind(2) + 1 + Args.lwind(1);
    mcalcium(w) = mutualinfo(mean(dcalcium(wirange,:)),sum(categorie(wirange,:)));
    
    %tim(w) = median([(w-1)*Args.lwind(2)+1  (w-1)*Args.lwind(2)+1+Args.lwind(1)]);
    tim(w) = mean([(w-1)*Args.lwind(2)+1  (w-1)*Args.lwind(2)+1+Args.lwind(1)]);
    parfor ii = 1 : Args.iterations
        scalcium(w,ii) =  mutualinfo(mean(dcalcium(wirange,:)),sum(categorie(wirange,randperm(size(categorie,2)))));
    end
    
end



%% expected value and prctiles

cexpV = zeros(nw,1);

prctiles = [100 - 100*Args.pvalues];
confI = zeros(nw,length(prctiles));
for w = 1 : nw
    [distr,values] = hist(scalcium(w,:),Args.nbin) ;
    distr = distr / sum(distr);
    cexpV(w) = sum(distr .* values);
    
    confI(w,:) = prctile(scalcium(w,:),prctiles);
end
%% prctiles

varargout{1} = scalcium;
if Args.plot
    if isempty(Args.figure)
        figure
    else
        figure(Args.figure)
    end
    if ~isempty(Args.subplot)
       subplot(Args.subplot(1),Args.subplot(2),Args.subplot(3)) 
    end
    plot(tim,mcalcium - cexpV,'r')
    hold on
    plot(tim,confI - repmat(cexpV,1,length(prctiles)),'k--')
    
    xlabel('Time (frame)')
    
    ylabel('MI (bits)')
    if ~isempty(Args.title); title(Args.title); end
    legend('data','p-values')
end