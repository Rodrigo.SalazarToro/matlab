dirs = nptDir('exp*');

for d = 1 : length(dirs)
    cd(dirs(d).name)
    files = nptDir('*corrTable.mat');
    
    for ff = 1  : length(files)
        load(files(ff).name)
        figure
        set(gcf,'name',files(ff).name)
        
        subplot(4,4,1)
        imagesc(R,[-0.5 0.7])
        title('whole trial')
        for sb = 2 : 12
            subplot(4,4,sb)
            imagesc(squeeze(corrts(:,:,sb-1)),[-0.5 0.7])
            title(sprintf('%d - %d ms from stimulation onset',(sb-2) *3*256,(sb-1)* 3* 256))
        end
        
        subplot(4,4,sb +1 )
        imagesc(squeeze(corrts(:,:,sb-1)),[-0.5 0.7])
        colorbar
        xlabel('cell #')
        ylabel('cell #')
    end
    cd ..
end

for d = 1 : length(dirs)
    cd(dirs(d).name)
    stims = nptDir('*stimulation.mat');
    if ~isempty(stims)
        for ff = 1 : length(stims)
            figure
            filename = regexprep(stims(ff).name,'stimulation.mat','neuron.mat');
            load(filename)
            for ncells = 1 : length(neuron)
                subplot(10,10,ncells)
                plot(neuron{ncells})
            end
            set(gcf,'name',filename)
            
        end
    end
    cd ..
end

load exp4_BF_neuron_exp4_AF_neuron_comp.mat
figure
set(gcf,'name','Stats on differential correlation')
subplot(5,1,1); imagesc(Rdiff);
title('Difference in correlation Before and After palsticity protocol')
colorbar
pvalues = {'p < 0.05' 'p < 0.01' 'p < 0.001' 'p < 0.0001'};
for th = 1 : 4
    
    subplot(5,1,th+1); imagesc(Rdiff > threshhigh{th} | Rdiff < threshlow{th});
    title(pvalues{th})
end

xlabel('cell #')
ylabel('cell #')

bf = load('exp4_BF_corrTableSur3bins.mat');
af = load('exp4_AF_corrTableSur3bins.mat');
figure
set(gcf,'name','Stats on calcium correlation')
subplot(4,2,1); imagesc(bf.R,[-0.5 0.7]);
title('Correlation before plasticity protocol')
colorbar
subplot(4,2,2); imagesc(af.R,[-0.5 0.7]);
title('Correlation after plasticity protocol')

colorbar
pvalues = {'p < 10^-5' 'p < 10^-6' 'p < 10^-7'};
for th = 1 : 3
    
    subplot(4,2,(th - 1) * 2 + 3); imagesc(bf.R < bf.thresh(:,:,th) | bf.R > bf.thresh(:,:,th + 3));
    title(pvalues{th})
    
     subplot(4,2,(th - 1) * 2 + 4); imagesc(af.R < af.thresh(:,:,th) | af.R > af.thresh(:,:,th + 3));
    title(pvalues{th})
end

xlabel('cell #')
ylabel('cell #')