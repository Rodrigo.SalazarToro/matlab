function [corrts,varargout] = corrTime(neuron,combp,varargin)


Args = struct('bin',3,'started',1,'plot',0,'save',0,'filename',[],'R',[]);
Args.flags = {'multitapers','plot','save'};
[Args] = getOptArgs(varargin,Args,'remove',{});


nbins = floor(size(neuron{1}(Args.started:end,:),1) / Args.bin);

ncell = length(neuron);

corrts = nan(ncell,ncell,nbins);

for n = 1 : nbins-1
    ntonset = (n - 1) * Args.bin + Args.started;
    [corrts(:,:,n)] = makeCorrTable(neuron,ntonset,Args.bin,combp);
    
end
if Args.save; R = Args.R; save(Args.filename,'corrts','R'); end