cd D:\Chieko\Matlab\CAR007441\
clear all

offolders = nptDir('OFT*');
parfor ff = 1 : length(offolders)
    cd(offolders(ff).name)
    cd Result_meanRef_std2.0
    files = nptDir('OnsetsData*.mat');
    data = load(files.name);
%     onsets = cell(length(data.onsets),1);
%     for c = 1 : length(data.onsets);onsets{c} =  cell2mat(data.onsets{c});end
    
    [cumcoi,bins,surcoi,thresholds] = xcorrCalcOnsets(data.onsets,'maxlagbin',20,'binsize',2,'save');
    
    cd ../..
end