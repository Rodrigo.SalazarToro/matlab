


dirs = nptDir('exp*');

fff = {'BF' 'AF'};
parfor d = 1 : length(dirs)
    cd(dirs(d).name)
    
    for ff = 1 : 2
        stims = nptDir(sprintf('*%s_stimulation.mat',fff{ff}));
        a=load(stims.name);
        ofile = nptDir(sprintf('*%s_onsets.mat',fff{ff}));
        b = load(ofile.name);
        nonsets = reshapeOnsets(b.onsets,a.starts,10);
        xcorrCalcOnsets(nonsets,'redo','save','binsize',2,'maxlagbin',5,'jitterTrialOnly','iterations',1000,'addName',fff{ff});
        
    end
    cd ..
end

cd('H:\stephane&Rod\analysis\rws')
dirs = nptDir('exp*');

fff = {'BF' 'AF'};
for d = 1 : length(dirs)
    cd(dirs(d).name)
    af = load('xcorrSpikesAF.mat');
    bf = load('xcorrSpikesBF.mat');
    
    f1=figure;
    subplot(2,1,2);
    allaf(d,:) = 100*nansum(af.cumcoi > squeeze((af.surcoi))) / size(af.cumcoi,1);
    bar(af.bins,allaf(d,:))
    title('after plasticity')
    subplot(2,1,1);
    allbf(d,:) = 100*nansum(bf.cumcoi > squeeze((bf.surcoi))) / size(bf.cumcoi,1);
    bar(bf.bins,allbf(d,:))
    title('before plasticity')
    
    xlabel('frame lags')
    ylabel('% significant pairs')
    set(gcf,'Name',pwd)
    saveas(f1,'AF_BFxcorr.fig');
    close(f1)
    
    cd ..
end

figure; 

plot(af.bins,(allbf - allaf)')
