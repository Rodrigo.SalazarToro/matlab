function [m_norm,varargout] = PhaseAmplitudeMod(x1ampl,x2phase,filt1ampl,filt2phase,varargin)
Args = struct('srate',200,'numsurrogate',200);
Args.flags = {};
[Args,~] = getOptArgs(varargin,Args);



numpoints=length(x1ampl); 

minskip=srate; 
maxskip=numpoints-srate; 
skip=ceil(numpoints.*rand(numsurrogate*2,1)); 
skip(find(skip>maxskip))=[]; 
skip(find(skip<minskip))=[]; 
skip=skip(1:numsurrogate,1); 
surrogate_m=zeros(numsurrogate,1);
%% sampling rate used in this study, in Hz %% number of sample points in raw signal %% number of surrogate values to compare to actual value %% time lag must be at least this big %% time lag must be smaller than this
%% HG analytic amplitude time series, uses eegfilt.m from EEGLAB toolbox %% (http://www.sccn.ucsd.edu/eeglab/) 
[fft1 , ~] = nptLowPassFilter(x1ampl,Args.srate,Args.filt1ampl(1),Args.filt1ampl(2));
[fft2 , ~] = nptLowPassFilter(x2phase,Args.srate,Args.filt1amp2(1),Args.filt1amp2(2));

amplitude=abs(hilbert(fft1)); %% theta analytic phase time series, uses EEGLAB toolbox 
phase=angle(hilbert(fft2));
%% complex-valued composite signal 
z=amplitude.*exp(i*phase); %% mean of z over time, prenormalized value 
m_raw=mean(z); %% compute surrogate values
for s=1:numsurrogate 
    surrogate_amplitude=[amplitude(skip(s):end) amplitude(1:skip(s)-1)]; 
    surrogate_m(s)=abs(mean(surrogate_amplitude.*exp(i*phase))); 
    disp(numsurrogate-s)
end %% fit gaussian to surrogate data, uses normfit.m from MATLAB Statistics toolbox 
[surrogate_mean,surrogate_std]=normfit(surrogate_m); %% normalize length using surrogate data (z-score) 
m_norm_length=(abs(m_raw)-surrogate_mean)/surrogate_std; 
m_norm_phase=angle(m_raw); 
m_norm=m_norm_length*exp(i*m_norm_phase);


end

