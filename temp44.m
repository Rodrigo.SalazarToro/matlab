
delays = [1000 1678;1679 2349;2350 3100];

rt = cell(3,1);
for d = 1 : length(lcdays); 
    cd(lcdays{d}); 
    cd session01; 
    mts = mtstrial('auto');
    for dl = 1 : 3
    ind = mtsgetTrials(mts,'stable','delay',delays(dl,:),'BehResp',1,'ML','noCharlie');
    rt{dl} = [rt{dl}; mts.data.FirstSac(ind)];
    end
    cd ../..
end
    
rt = cell(3,1);
for d = 1 : length(lbdays); 
    cd(lbdays{d}); 
    cd session01; 
    mts = mtstrial('auto');
    for dl = 1 : 3
    ind = mtsgetTrials(mts,'stable','delay',delays(dl,:),'BehResp',1,'ML','noCharlie');
    rt{dl} = [rt{dl}; mts.data.FirstSac(ind)];
    end
    cd ../..
end
    