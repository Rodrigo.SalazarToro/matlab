function [varargout] = readFreqRangeTuning(day,varargin)

Args = struct('fband',[]);
Args.flags = {};
[Args,modvarargin] = getOptArgs(varargin,Args);

dir = pwd;

if isempty(Args.fband)
    clark = findstr(dir,'clark');
    betty = findstr(dir,'betty');
    if ~isempty(clark)
        Args.fband = [12 25;26 40];
        
    elseif ~isempty(betty)
        Args.fband = [9 22;23 40];
        
    end
end


idefile = nptDir('ideTuning11.mat'); % from the compareCohRule.m
locfile = nptDir('locTuning11.mat')

ide = load(idefile.name);
loc = load(locfile.name);

f = idefile.name;
limitB = find(f>= Args.fband(1,1) & f<= Args.fband(1,2));

Flimit = [limitB(1) limitB(end)];


%% tuning for identities
sigTuning = find(ide.CueAdz(Flimit(1) : Flimit(2),    ) == 0);





%% tuning for locations

%%