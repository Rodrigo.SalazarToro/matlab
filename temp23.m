bands = {'beta','gamma','alpha'};
bands = {'alpha','beta','gamma'};
dir = {'pos','neg'};
lwind = [3 4; 1 4; 1 2; 2 3; 1 3; 2 4];% missing
lwind = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];% missing
% [tot,ind] = get(test,'Number');
epochs = {'presample' 'sample' 'delay1' 'delay2'};
for b = 1 : 3
    for d = 1 : 2
        for comp = 1 : 6
            [tot(b,comp),ind] =get(test,'Number','sigSur',{sprintf('%s',bands{b}) [1 3] lwind(comp,:)});
            [r,ind] =get(test,'Number','type',{sprintf('%s%s%d%d',bands{b},dir{d},lwind(comp,1),lwind(comp,2)) [1 3]});
            table{b}(d,comp) = 100*r/tot(b,comp);
            tableN{b}(d,comp) = r;
            indexes{b,d,comp} = ind;
        end
    end
end
legs = { 'md' 'mv' 'ld' 'lv' 'sd' 'sv' 'ms' 'ls' 'other'};
for b = 1 : 3
    band(b).perc = max(max(table{b}));
    [row,col]  = find(table{b} == band(b).perc);
    for i = 1 : length(row)
        band(b).type{i} = sprintf('%s%d%d',dir{row(i)},lwind(col(i),1),lwind(col(i),2));
        band(b).hist{i} = hist(test.data.Index(indexes{b,row(i),col(i)},9),[1:9]);
        for h = 1 : 9;[thist(h),ind] =get(test,'Number','sigSur',{sprintf('%s',bands{b}) [1 3] lwind(col(i),:)},'pairhist',legs{h},'rudeHist');end
        band(b).histP{i} = band(b).hist{i} ./ thist * 100;
    end
    
    band(b).legs= legs;
    alldata = cell(4,1);
    allind = indexes{b,row,col};
    if ~isempty(allind)
        for ind = allind
            cmb = test.data.Index(ind,1);
            %             pairg = coh.data.Index(ind,10:11);
            cd(test.data.setNames{ind})
            %             load cohInterDelay1.mat
            load cohInter.mat
            %             sur = load('cohInterSur.mat');
            
            thes = Day.session(1).rule;
            if thes <= size(Day.session,2) && length(Day.session(thes).trials) >=140
                for p = 1 : 4
                    %                 if length(find(Day.session(thes).C(fband,cmb,3) > sur.Day.session(thes).Prob(fband,1,cmb,3))) > 1 || length(find(Day.session(thes).C(fband,cmb,4) > sur.Day.session(thes).Prob(fband,1,cmb,4))) > 1
                    alldata{p} = [alldata{p} single(Day.session(thes).C(:,cmb,p))];
                    %                     clf;subplot(2,1,2); plot(Day.session(thes).C(fband,cmb,4)); hold on; plot( sur.Day.session(thes).Prob(fband,1,cmb,4))
                    %                     subplot(2,1,1);plot(Day.session(thes).C(fband,cmb,3)); hold on; plot( sur.Day.session(thes).Prob(fband,1,cmb,3))
                    %                 end
                end
            end
            %             pause
        end
        
        
        % figure; plot(f,prctile(alldata(:,max(alldata,[],1)< 0.5),[25 50 75 90 95],2))
        h = figure;
        set(h,'Name',sprintf('%s %s',bands{b},band(b).type{1}))
        for p = 1 : 4
            subplot(1,4, p)
            plot(f,prctile(alldata{p},[50 75 90 95 99],2))
            %             if ncomb == 1; title(sprintf('delay %d',p)); end
            if p == 1; ylabel('Coherence');text(50, 0.4,sprintf('n=%d',size(alldata{p},2)));end
            xlabel('Frequency [Hz]')
            title(epochs{p})
            axis([0  60 0 0.6])
            grid on
        end
    end
    
end


%%make the table
%b = 3;[12 13 14 23 24 34; table{b}; tableN{b};tot(b,:)]
for b = 1 : 3
    
    for col = 2 : 7
        fband(b).table{1,1} = 'epoch comp';
        fband(b).table{1,col} = num2str(lwind(col-1,:));
         fband(b).table{2,1} = '% pos';
        fband(b).table{2,col} = table{b}(1,col-1);
         fband(b).table{3,1} = '% neg';
        fband(b).table{3,col} = table{b}(2,col-1);
         fband(b).table{4,1} = 'n pos';
        fband(b).table{4,col} = tableN{b}(1,col-1);
         fband(b).table{5,1} = 'n neg';
        fband(b).table{5,col} = tableN{b}(2,col-1);
         fband(b).table{6,1} = 'total';
        fband(b).table{6,col} = tot(b,col-1);
    end
end
         