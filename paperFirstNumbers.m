bands = {'alpha' 'beta' 'gamma' 'theta'};
cd /clark
clark = loadObject('cohInterIDE.mat')

[npairs,ind] = get(clark,'Number','snr',99);npairs
PPC = length(unique(clark.data.Index(ind,[2 10]),'rows'))
PFC = length(unique(clark.data.Index(ind,[2 11]),'rows'))


for b = 1 : 4
[r,ind] = get(clark,'Number','snr',99,'sigSur',{bands{b} [1:4] [1 3]});
sprintf('%s %d; %g perc.',bands{b},r,round(100*r/npairs))
end

cd betty/

allbetty = loadobject('cohInterIDEandLong.mat')

[npairs,ind] = get(allbetty,'Number','snr',99);npairs
PPC = length(unique(allbetty.data.Index(ind,[2 10]),'rows'))
PFC = length(unique(allbetty.data.Index(ind,[2 11]),'rows'))
for b = 1 : 4
[r,ind] = get(allbetty,'Number','snr',99,'sigSur',{bands{b} [1:4] [1 3]});
sprintf('%s %d; %g perc.',bands{b},r,round(100*r/npairs))
end

cd betty/
