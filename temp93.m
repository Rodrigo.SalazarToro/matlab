cd /Volumes/raid/data/monkey/clark/
clear all
load idedays

c=1;
for d = 1 : length(idedays)
    cd(idedays{d})
    sessions = nptDir('session0*');
    for s = 1 : length(sessions)
        cd(sessions(s).name)
        if isdir('cxFreqMod')
            cd cxFreqMod
            files = nptDir('cx*.mat');
            for ff = 1 : length(files);
                ch1 = str2num(files(ff).name(11:14));
                ch2 = str2num(files(ff).name(16:19));
                if (ch1 <=8 && ch2 >8) || (ch2 <=8 && ch1 >8)
                    a = load(files(ff).name);
                    for ep = 1 : 4;
                        %                     subplot(2,4,ep);
                        %                     imagesc(f,f,squeeze(m_norm(ep,:,:)));
                        %                     colorbar;
                        sig(c,ep,:,:) = squeeze(a.m_norm(ep,:,:)) > squeeze(a.threshold(ep,4,:,:));
                        %                     subplot(2,4,4+ep)
                        %                     imagesc(f,f,sig(c,ep,:,:).*squeeze(m_norm(ep,:,:)));
                        %                     colorbar;
                        
                    end
                    c=c+1;
                end
                %                 set(gcf,'name',[pwd '/' files(ff).name])
                %                 pause(2)
                %                 clf
            end
            cd ..
        end
        cd ..
    end
    cd ..
end

nfreqlag = 4;
nfreqlag*a.f(2)
npol = 3;
figure
for ep = 1 : 4;
    ndata = squeeze(sum(sig(:,ep,:,:),1))  / size(sig,1);
    for ii = 1 : 65; ndata(ii,ii) = 0; end
    for ii = 1 : 65; for nf = 1 : nfreqlag; if ii+nf <= 65; ndata(ii,ii+nf) = nf*0.005; ndata(ii+nf,ii) = nf*0.005; end; end; end
    ZI = interp2(ndata,npol);
    nf = [0 : a.f(2)/npol: 100];
    subplot(2,4,ep);
    imagesc(a.f,a.f,ndata,[0 0.2]);
    axis([0 50 0 50]);
    subplot(2,4,4+ep);
    imagesc(nf,nf,ZI,[0 0.2]);
    axis([0 50 0 50]);
end

%% for betty

cd /Volumes/raid/data/monkey/betty/
clear all
load idedays

c=1;
for d = 1 : length(days)
    cd(days{d})
    
    cd('session01')
    if isdir('cxFreqMod')
        cd cxFreqMod
        files = nptDir('cx*.mat');
        for ff = 1 : length(files);
            ch1 = str2num(files(ff).name(11:14));
            ch2 = str2num(files(ff).name(16:19));
            if (ch1 <=32 && ch2 >32) || (ch2 <=32 && ch1 >32)
                a = load(files(ff).name);
                for ep = 1 : 4;
                    %                     subplot(2,4,ep);
                    %                     imagesc(f,f,squeeze(m_norm(ep,:,:)));
                    %                     colorbar;
                    sig(c,ep,:,:) = squeeze(a.m_norm(ep,:,:)) > squeeze(a.threshold(ep,4,:,:));
                    %                     subplot(2,4,4+ep)
                    %                     imagesc(f,f,sig(c,ep,:,:).*squeeze(m_norm(ep,:,:)));
                    %                     colorbar;
                    
                end
                c=c+1;
            end
            %                 set(gcf,'name',[pwd '/' files(ff).name])
            %                 pause(2)
            %                 clf
        end
        cd ..
    end
    cd ..
    
    cd ..
end

nfreqlag = 7;
nfreqlag*a.f(2)
npol = 3;
for ep = 1 : 4;
    ndata = squeeze(sum(sig(:,ep,:,:),1))  / size(sig,1);
    for ii = 1 : 65; ndata(ii,ii) = 0; end
    for ii = 1 : 65; for nf = 1 : nfreqlag; if ii+nf <= 65; ndata(ii,ii+nf) = nf*0.001; ndata(ii+nf,ii) = nf*0.001; end; end; end
    ZI = interp2(ndata,npol);
    nf = [0 : a.f(2)/npol: 100];
    subplot(2,4,ep);
    imagesc(a.f,a.f,ndata,[0 0.4]);
    axis([0 50 0 50]);
    subplot(2,4,4+ep);
    imagesc(nf,nf,ZI,[0 0.4]);
    axis([0 50 0 50]);
end

%% both animals
cd /Volumes/raid/data/monkey/
sdir = pwd;
obj = loadObject('cohInterIDEandLongSorted.mat');
[n,all] = get(obj,'Number');
% [n,all] = get(obj,'Numbr','type',{'betapos34' [1 3]});
% sig = zeros(n,4,65,65);
sig = zeros(n,4,25,25);
direcs = {'PA' 'AP'};
for d = 1 : 2
    
    parfor ii = 1 : n
        cd(obj.data.setNames{all(ii)})
        if ~isempty(strfind(obj.data.setNames{all(ii)},'clark'))
            b=load('rules.mat');
            cd(['session0' num2str(find(b.r==1)+1)])
        else
            cd session01
        end
        cd cxFreqMod
        try
            file = sprintf('cxFreqModg%04gg%04g%s.mat',obj.data.Index(all(ii),10),obj.data.Index(all(ii),11),direcs{d});
            a = load(file);
            for ep = 1 : 4;
                sig(ii,ep,:,:) = squeeze(a.m_norm(ep,:,:)) > squeeze(a.threshold(ep,4,:,:));
            end
        end
    end
    npol = 2;
    epoch = {'pre-sample' 'sample' 'delay1' 'delay2'};
    f = [3:4:95];
    figure
    for ep = 1 : 4;
        ndata = squeeze(sum(sig(:,ep,:,:),1))  / size(sig,1);
        %     for ii = 1 : length(a.f); ndata(ii,ii) = 0; end
        %     for ii = 1 : length(a.f); for nf = 1 : nfreqlag; if ii+nf <= length(a.f); ndata(ii,ii+nf) = nf*0.001; ndata(ii+nf,ii) = nf*0.001; end; end; end
        ZI = interp2(ndata,npol);
        nf = [0 : 4/npol: 95+1];
        subplot(2,4,ep);
        imagesc(f,f,ndata,[0 1]);
        colorbar
        axis([3 50 3 50]);
        title(epoch{ep})
        subplot(2,4,4+ep);
        imagesc(nf,nf,ZI,[0 1]);
        axis([3 50 3 50]);
        set(gca,'TickDir','out')
        %     colorbar
    end
    if strcmp('PA',direcs{d})
        xlabel('Amplitude in Frontal channel (Hz)')
        ylabel('Phase in Parietal channel (Hz)')
    else
        xlabel('Amplitude in Parietal channel (Hz)')
        ylabel('Phase in Frontal channel (Hz)')
    end
    cd(sdir)
    save(sprintf('cxTemp%d.mat',d))
    
end

cd /Volumes/raid/data/monkey/
sdir = pwd;
obj = loadObject('cohInterIDEandLongSorted.mat');
[n,all] = get(obj,'Number');
% [n,all] = get(obj,'Numbr','type',{'betapos34' [1 3]});
% sig = zeros(n,4,65,65);
mag = nan(2,4,25,25);
direcs = {'PA' 'AP'};
for d = 1 : 2
    ii = 1; 
    cd(obj.data.setNames{all(ii)})
        if ~isempty(strfind(obj.data.setNames{all(ii)},'clark'))
            b=load('rules.mat');
            cd(['session0' num2str(find(b.r==1)+1)])
        else
            cd session01
        end
        cd cxFreqMod
        try
            file = sprintf('cxFreqModg%04gg%04g%s.mat',obj.data.Index(all(ii),10),obj.data.Index(all(ii),11),direcs{d});
            a = load(file);
            mag(d,:,:,:) = a.m_norm;
        end
    for ii = 2 : n
        cd(obj.data.setNames{all(ii)})
        if ~isempty(strfind(obj.data.setNames{all(ii)},'clark'))
            b=load('rules.mat');
            cd(['session0' num2str(find(b.r==1)+1)])
        else
            cd session01
        end
        cd cxFreqMod
        try
            file = sprintf('cxFreqModg%04gg%04g%s.mat',obj.data.Index(all(ii),10),obj.data.Index(all(ii),11),direcs{d});
            a = load(file);
           mag(d,:,:,:) = (mag + a.m_norm) / 2;
        end
    end
end
for d = 1 : 2
    npol = 2;
    epoch = {'pre-sample' 'sample' 'delay1' 'delay2'};
    f = [3:4:95];
    figure
    for ep = 1 : 4;
        ndata = squeeze(mag(d,ep,:,:));
        %     for ii = 1 : length(a.f); ndata(ii,ii) = 0; end
        %     for ii = 1 : length(a.f); for nf = 1 : nfreqlag; if ii+nf <= length(a.f); ndata(ii,ii+nf) = nf*0.001; ndata(ii+nf,ii) = nf*0.001; end; end; end
        ZI = interp2(ndata,npol);
        nf = [0 : 4/npol: 95+1];
        subplot(2,4,ep);
        imagesc(f,f,ndata);
        colorbar
        axis([3 50 3 50]);
        title(epoch{ep})
        subplot(2,4,4+ep);
        imagesc(nf,nf,ZI);
        axis([3 50 3 50]);
        set(gca,'TickDir','out')
        %     colorbar
    end
    if strcmp('PA',direcs{d})
        xlabel('Amplitude in Frontal channel (Hz)')
        ylabel('Phase in Parietal channel (Hz)')
    else
        xlabel('Amplitude in Parietal channel (Hz)')
        ylabel('Phase in Frontal channel (Hz)')
    end
   
end   

