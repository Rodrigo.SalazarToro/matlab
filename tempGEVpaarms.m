figure
xlims = {[-.45 -0.1]; [.005 .02];[-.007 -.002]};
epochs = {'presample' 'sample' 'delay1' 'delay2'};
paramss = {'k' 'sigma' 'mu'};
for ep = 1 : 4; 
    for p= 1 : 3;
        subplot(3,4,(p-1)*4+ep);hist(allparams(:,ep,p),10);
        xlim(xlims{p})
        ylim([0 90]);
        if p == 1 ; title(epochs{ep});end
    if ep == 1; ylabel(paramss{p}); end
    end; 
    
end

xlabel('parameter')
ylabel('counts')