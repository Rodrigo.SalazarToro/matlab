% frepoch = ProcessDays(FRepochs,'auto','sessions',{'session02' 'session03'},'NoSites','days',{switchdays{:}});
% [r,ind] = get(frepoch,'pIde',0.01,'number');
% selCells = frepoch.data.setNames(ind);
% sfc = ProcessDays(threeStimSFC,'auto','sessions',{'session02' 'session03'},'NoSites','days',{switchdays{:}},'selectedCells',selCells);
% sfc = ProcessDays(threeStimSFC,'auto','sessions',{'session02' 'session03'},'NoSites','days',{switchdays{:}});
% Uareas = {'P' 'F'};
% Lareas = {'F' 'P'};
% rules = {'IDE' 'LOC'};
% combl = {'unitP lfp F' 'unitF lfp P'};
% mdir = pwd;
% for rule = 1 : 2
%     for comb = 1 : 2
%         [r,ind] = get(sfc,'unitcortex',{Uareas{comb}},'lfpcortex',{Lareas{comb}},'rule',rule,'number','maxC',0.15);
%         count = 1;
%         for theind = ind
%             cd(sfc.data.setNames{theind})
%             ideSFCfile = nptDir('SFCideTuning11*.mat');
%             locSFCfile = nptDir('SFClocTuning11*.mat');
%             ideSFC = load(ideSFCfile.name);
%             locSFC = load(locSFCfile.name);
%             f = ideSFC.f;
%             fband = find(f > 8 & f < 30);
%             for p = 1 : 4
%                 data = squeeze(ideSFC.C{p}(:,sfc.data.Index(theind,8),:));
%                 tu = max(data(fband,:),[],1);
%                 [v,ord] = sort(tu);
%                 selSFC{rule,comb,1}(p,:,:,count) = data(:,ord);
%
%                 data = squeeze(locSFC.C{p}(:,sfc.data.Index(theind,8),:));
%                 tu = max(data(fband,:),[],1);
%                 [v,ord] = sort(tu);
%                 selSFC{rule,comb,2}(p,:,:,count) = data(:,ord);
%
%             end
%             cd(mdir)
%         end
%     end
% end
%
% for rule = 1 : 2
%
%     for comb = 1 : 2
%         h = figure;
%         set(h,'Name',sprintf('%s %s',rules{rule},combl{comb}));
%         maxv = [];
%         for tune = 1 : 2
%             for p = 1 : 4
%                 subplot(2,4,(tune-1)*4 + p)
%                 mdata = squeeze(mean(selSFC{rule,comb,tune}(p,:,:,:),4));
%                 plot(f,mdata)
%                 maxv = [maxv max(max(mdata))];
%             end
%         end
%         for sb = 1 : 8; subplot(2,4,sb); axis([5 60 0 max(maxv)]); end
%     end
% end
%
%%%

load switchdays.mat
dir = pwd;
rules = {'I' 'L'};
therules = {'IDENTITY' 'LOCATION'};
tit = {'Pre-sample' 'Sample' 'Delay1' 'Delay2'};
% frepoch = ProcessDays(FRepochs,'auto','sessions',{'session02' 'session03'},'NoSites','days',{switchdays{:}});
% frepoch = ProcessDays(nineStimPsth,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}});

clear mSFC
% clear ideCells locCells
allband = [2 8; 8 13;14 30;30 42];
% for ff = 1 : 4
clear mSFC
clear data
clear nsig
fband = find(f >= allband(ff,1) & f <= allband(ff,2));
for ruu = 1 : 2
    [r,ideCellsind] = get(frepoch,'pIde',0.05,'number','rule',{rules{ruu}},'ntrials',10,'mfr',1,'tuningI',{'IDE',0.2},'unit',{'m'},'bothRules'); %,'cortex',{'F'});
    [r,locCellsind] = get(frepoch,'pLoc',0.05,'number','rule',{rules{ruu}},'ntrials',10,'mfr',1,'tuningI',{'LOC',0.2},'unit',{'m'},'bothRules'); %,'cortex',{'F'});
    [r,allcells] = get(frepoch,'number','rule',{rules{ruu}},'ntrials',10,'mfr',1,'unit',{'m'},'bothRules'); %,'cortex',{'F'});
    ideCellst = frepoch.data.setNames(ideCellsind);
    locCellst = frepoch.data.setNames(locCellsind);
    noTuneCell{ruu} = setdiff(frepoch.data.setNames(allcells), [ideCellst locCellst]);
    bothCells{ruu} = intersect(ideCellst,locCellst,'rows');
    ideCells{ruu} = setdiff(ideCellst,locCellst,'rows');
    locCells{ruu} = setdiff(locCellst,ideCellst,'rows');
    %         SFC{ruu,2} = ProcessDays(threeStimSFC,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}},'selectedCells',locCells{ruu}); %,'maxC',0.12);
    %         SFC{ruu,1} = ProcessDays(threeStimSFC,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}},'selectedCells',ideCells{ruu}); %,'maxC',0.12);
    %         SFC{ruu,3} = ProcessDays(threeStimSFC,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}},'selectedCells',bothCells{ruu}); %,'maxC',0.12);
    %         SFC{ruu,4} = ProcessDays(threeStimSFC,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}},'selectedCells',noTuneCell{ruu}); %,'maxC',0.12);
    for sel = 1 : 4
        thesfc = SFC{ruu,sel};
        count = 1;
        countsig = 1;
        for theind = 1 : thesfc.data.numSets
                        if thesfc.data.Index(theind,3) == thesfc.data.Index(theind,4) && thesfc.data.Index(theind,3) == 80
%             if thesfc.data.Index(theind,3) ~= thesfc.data.Index(theind,4)
                if thesfc.data.Index(theind,9) == 0
                    cd(thesfc.data.setNames{theind})
                    
                    sfcfile = nptDir('SFCallCuesComb11*.mat');
                    
                    sfcdata = load(sfcfile.name);
                    for p = 1 : 4
                        aldata = squeeze(sfcdata.C{p}(:,thesfc.data.Index(theind,8)));
                        surrogate = squeeze(sfcdata.SurProb{p}(:,2,thesfc.data.Index(theind,8)));
                        
                        nsig{ruu,sel}(p,count,:) = aldata > surrogate;
                        
                    end
                    if sum(sum(squeeze(nsig{ruu,sel}(:,count,:)))) > 1
                        for p = 1 : 4
                            adata = squeeze(sfcdata.C{p}(:,thesfc.data.Index(theind,8)));
                            mSFC{ruu,sel}(p,:,countsig) = adata;
                        end
                        countsig = countsig + 1;
                    end
                    count = count + 1;
                    cd ..
                end
            end
        end
    end
    cd(dir)
end

if exist('mSFC')
    for ruu = 1 : 2
        h = figure;
        set(h,'Name',sprintf('%s',therules{ruu}))
        maxv  = [];
        for p = 1 : 4
            subplot(4,1, p)
            for stim = 3
                %         mdata = squeeze(nanmean(mSFC{ruu,1}(p,:,:,:),4));
                mdata = squeeze(mSFC{ruu,1}(p,:,:));
                plot(f,prctile(mdata,[50 75 90],2))
                maxv = [maxv max(max(mdata))];
                hold on
                %         mdata = squeeze(nanmean(mSFC{ruu,2}(p,:,:,:),4));
                mdata = squeeze(mSFC{ruu,2}(p,:,:));
                plot(f,prctile(mdata,[50 75 90],2),'--')
                maxv = [maxv max(max(mdata))];
            end
            axis([0 60 0.04 0.3]);
            text(35,0.07,sprintf('n=%d ide Cells; n=%d SFC',length(ideCells{ruu}),size(mSFC{ruu,1},3)))
            text(35,0.09,sprintf('n=%d loc Cells; n=%d SFC',length(locCells{ruu}),size(mSFC{ruu,2},3)))
        end
        tit = {'Pre-sample' 'Sample' 'Delay1' 'Delay2'};
        subplot(4,1,4); xlabel('Frequency [Hz]')
        for sb = 1 : 4;subplot(4,1,sb); title(tit{sb}); end
        for sb = 1 : 4;subplot(4,1,sb); ylabel('SFC'); end
        subplot(4,1,4); legend('ide1','ide2','ide3','loc1','loc2','loc3')
    end
    revSFC = cat(3,mSFC{1,1},mSFC{2,2});
    irevSFC = cat(3,mSFC{1,2},mSFC{2,1});
    bothSFC = cat(3,mSFC{1,3},mSFC{2,3});
    
    revsig = cat(2,nsig{1,1},nsig{2,2});
    irevsig = cat(2,nsig{1,2},nsig{2,1});
    bothsig = cat(2,nsig{1,3},nsig{2,3});
    noTunesig = cat(2,nsig{1,4},nsig{2,4});
    figure;
    for p = 1 : 4;
        subplot(4,1,p);
        plot(f,sum(squeeze(revsig(p,:,:)),1)./size(revsig,2));hold on;
        plot(f,sum(squeeze(irevsig(p,:,:)),1)./size(irevsig,2),'--');
        plot(f,sum(squeeze(bothsig(p,:,:)),1)./size(bothsig,2),'-.');
        plot(f,sum(squeeze(noTunesig(p,:,:)),1)./size(noTunesig,2),'r');
        axis([3 60 0 0.15]);
    end;
    xlabel('Frequency [Hz]');
    ylabel('% significant pairs')
    for p = 1 : 4; subplot(4,1,p);title(tit{p}); end
    
    nrevCells = length(ideCells{1}) +length(locCells{2});
    nirrevCells = length(ideCells{2}) +length(locCells{1});
    bothCellst = length(bothCells{1}) +length(bothCells{2});
    %     legend('SFC from 34 revelant units; n=256','SFC from 27 irrevelant units; n=188','SFC from 23 mixed units; n=171','SFC from 98 not-tuned units; n=676')
    %     legend('SFC from 34 revelant units; n=122','SFC from 27 irrevelant units; n=89','SFC from 23 mixed units; n=77','SFC from 98 not-tuned units; n=329')
    rev = figure;
    set(rev,'Name',sprintf('Revelant,irrelevant mix cells; band %d',ff))
    for p = 1 : 4
        subplot(4,1, p)
        for stim = 3
            mdata = squeeze(revSFC(p,:,:));
            %         stddata = squeeze(nanstd(revSFC(p,:,stim,:),[],4)) ./ sqrt(size(revSFC,4));
            %         mdata = squeeze(nanmean(revSFC(p,:,stim,:),4));
            plot(f,prctile(mdata,[50 75 90 95],2))
            %         plot(f,mdata+stddata)
            hold on
            %         plot(f,mdata-stddata)
            %         plot(f,mdata)
            maxv = [maxv max(max(mdata))];
            mdata = squeeze(irevSFC(p,:,:));
            %         stddata = squeeze(nanstd(irevSFC(p,:,stim,:),[],4)) ./ sqrt(size(irevSFC,4));
            %         mdata = squeeze(nanmean(irevSFC(p,:,stim,:),4));
            plot(f,prctile(mdata,[50 75 90 95],2),'--')
            %         plot(f,mdata+stddata,'r')
            %         hold on
            %         plot(f,mdata-stddata,'r')
            %         plot(f,mdata,'r')
            maxv = [maxv max(max(mdata))];
            mdata = squeeze(bothSFC(p,:,:));
            
            plot(f,prctile(mdata,[50 75 90 95],2),':')
            axis([0.02 60 0 0.15])
        end
        %     for sb = 1 : 4; subplot(4,1,sb); axis([0 60 0 0.2]); end
        
    end
    text(35,0.07,sprintf('n=%d rev Cells; n=%d SFC',length(ideCells{1}) +length(locCells{2}),size(revSFC,3)))
    text(35,0.09,sprintf('n=%d irev Cells; n=%d SFC',length(ideCells{2}) +length(locCells{1}),size(irevSFC,3)))
    text(35,0.12,sprintf('n=%d mixed Cells; n=%d SFC',length(bothCells{1}) +length(bothCells{2}),size(bothSFC,3)))
    
    subplot(4,1,4); xlabel('Frequency [Hz]')
    for sb = 1 : 4;subplot(4,1,sb); title(tit{sb}); end
    for sb = 1 : 4;subplot(4,1,sb); ylabel('SFC'); end
    % subplot(1,4,4); legend('50th prctile rev','75th prctile rev','90th prctile rev','50th prctile irev','75th prctile irev','90th prctile irev','50th prctile mix','75th prctile mix','90th prctile mix')
end
% end
keyboard
%%
clear SFC bothCells mSFC
srule = {'IDE' 'LOC'};
ptune = {'pIde' 'pLoc'};

dir = pwd;
for tune = 1 : 2
    [r,CellsindI] = get(frepoch,ptune{tune},0.05,'number','rule',{'I'},'ntrials',10,'mfr',1,'tuningI',{srule{tune},0.2},'unit',{'m'},'bothRules');
    [r,CellsindL] = get(frepoch,ptune{tune},0.05,'number','rule',{'L'},'ntrials',10,'mfr',1,'tuningI',{srule{tune},0.2},'unit',{'m'},'bothRules');
    
    ideCellst = regexprep(frepoch.data.setNames(CellsindI),'session02','session03');
    locCellst = regexprep(frepoch.data.setNames(CellsindL),'session02','session03');
    
    bothCells = intersect(ideCellst,locCellst,'rows');
    bothCells = [bothCells regexprep(bothCells,'session03','session02')];
    
    for ruu = 1 : 2
        if ruu == 1
            Cellst = intersect(frepoch.data.setNames(CellsindI),bothCells);
        else
            Cellst = intersect(frepoch.data.setNames(CellsindL),bothCells);
        end
        SFC = ProcessDays(threeStimSFC,'auto','sessions',{'session02','session03'},'NoSites','days',{switchdays{:}},'selectedCells',Cellst,'rule',r); %,'maxC',0.12);
        
        thesfc = SFC;
        for theind = 1 : thesfc.data.numSets
            cd(thesfc.data.setNames{theind})
            
            sfcfile = nptDir('SFCallCuesComb11*.mat');
            %
            sfcdata = load(sfcfile.name);
            f = locSFCdata.f;
            for p = 1 : 4
                
                adata = squeeze(sfcdata.C{p}(:,thesfc.data.Index(theind,8)));
                
                mSFC{ruu}(p,:,theind) = adata;
            end
            cd ..
        end
        
        h = figure;
        set(h,'Name',sprintf('tuning %s; rule %s',srule{tune},therules{ruu}))
        maxv  = [];
        for p = 1 : 4
            subplot(4,1, p)
            %         mdata = squeeze(nanmean(mSFC{ruu,1}(p,:,:,:),4));
            mdata = squeeze(mSFC{ruu}(p,:,:));
            plot(f,prctile(mdata,[10 25 50 75 90],2))
            maxv = [maxv max(max(mdata))];
            hold on
            %         mdata = squeeze(nanmean(mSFC{ruu,2}(p,:,:,:),4));
            mdata = squeeze(mSFC{ruu}(p,:,:));
            plot(f,prctile(mdata,[10 25 50 75 90],2),'--')
            maxv = [maxv max(max(mdata))];
            %              axis([0 60 0.04 0.1])
            text(35,0.07,sprintf('n=%d SFC',size(mSFC{ruu},3)))
            xlim([5 50])
        end
        tit = {'Pre-sample' 'Sample' 'Delay1' 'Delay2'};
        subplot(4,1,4); xlabel('Frequency [Hz]')
        for sb = 1 : 4;subplot(4,1,sb); title(tit{sb}); end
        for sb = 1 : 4;subplot(4,1,sb); ylabel('SFC'); end
        
        cd(dir)
    end
end

