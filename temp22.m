dt = load('cohInterTriald.mat');
old = load('cohInterReal.mat');
nod = load('cohInternoD.mat');
qua = load('cohInter.mat');
for cmb = 1 : size(dt.Day.comb,1)
%    if nod.Day.comb(cmb,1) == 40 && nod.Day.comb(cmb,2) == 1
   plot(dt.f,dt.Day.session(1).C(:,cmb,1),'k')
   hold on
   plot(old.f,old.Day.session(1).C(:,cmb,1))
   plot(nod.f,nod.Day.session(1).C(:,cmb,1),'r')
   plot(qua.f,qua.Day.session(1).C(:,cmb,1),'g')
   xlim([0 50])
   title(sprintf('%d %d',nod.Day.comb(cmb,1),nod.Day.comb(cmb,2)))
   pause
   clf
%    end
end