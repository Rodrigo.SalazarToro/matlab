clear sign2 signt
count = 1;
plevel = 1;
load idedays
for d = 1 : length(idedays); idedays{d} = [pwd '/' idedays{d}]; end
% load longdays
% idedays = [lbdays lcdays];

tuningDir = 'tuning1block';
% tuningDir = 'tuning';
tuneArgs = {'iCueLoc' 'iCueObj'};
obj = betty;
% [r,indLoc] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0}); %;'IDELoc' [3 4] '>=' 1;});r
% [r,indObj] = get(obj,'snr',99,'Number','tuning',{'IDELoc' 1 '==' 0;'IDELoc' 1 '==' 0}); %; 'IDEObj' [3 4] '>=' 1;});r
[r,indLoc] = get(obj,'snr',99,'Number');
[r,indObj] = get(obj,'snr',99,'Number');
indtuned = union(indLoc,indObj);
% for d = 1:length(idedays)
tenHz = [];
for n = indtuned
    %     if ~isempty(nptDir(idedays{d}))
    cd(obj.data.setNames{n})
    load('cohInter.mat');
    value = squeeze(Day.session(1).C(:,obj.data.Index(n,1),:));
    load('generalSur.mat');
    sur = squeeze(Day.session(1).Prob(:,4,1,:));
    clear Day
    sigvalues = value >= sur;
    if ~isempty(nptDir(tuningDir))
        cd(tuningDir)
        files = nptDir('iCue*Rule1.mat');
        pairg = obj.data.Index(n,10:11);
        
        files = nptDir(sprintf('iCue*g%04.0fg%04.0fRule1.mat',pairg(1),pairg(2)));
        for i = 1 : length(files)
            
            cd(obj.data.setNames{n})
%             cd grams
%             cfiles = nptDir(sprintf('cohgram%sRule1%s*.mat',files(i).name(8:17),files(i).name(5:7)));
%             clear ntrials
%             for fff = 1 : length(cfiles)
%                 load(cfiles(fff).name)
%                 ntrials(fff) = length(trials);
%             end
            cd(obj.data.setNames{n})
            %             if ~isempty(cfiles) && min(ntrials) > 150
            cd(tuningDir)
            load(files(i).name)
            for p = 1 : 4
                %                 signt(count,p,:) = groupN(p).rdiff >= groupN(p).pvalues(:,plevel);
                signt(count,p,:) = filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff) >= filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues(:,plevel));
                signt(count,p,:) = squeeze(signt(count,p,:)) .* sigvalues(:,p);
                %             subplot(4,1,p)
                %             plot(f,filtfilt(repmat(1/2,1,2),1,groupN(p).rdiff)); hold on;  plot(f,filtfilt(repmat(1/2,1,2),1,groupN(p).pvalues))
                %             plot(f, squeeze(signt(count,p,:))./10,'.')
                %             ylim([0 0.2])
                temp = zeros(65,1);
                for c = 1 :3
                    value = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).rdiff);
                    difVar1 = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues(:,plevel+1));
                    
                    difVar2 = filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues(:,4+plevel+1));
                    
                    temp = temp + (value <= difVar1) + (value >= difVar2);
                    %                     subplot(4,3,(p-1)*3+c)
                    %                     plot(f,value)
                    %                     hold on
                    %                     plot(f,filtfilt(repmat(1/2,1,2),1,group2(p).comp(c).pvalues))
                    %                     ylim([-0.2 0.2])
                end
                if p == 3 && signt(count,p,8) == 1;
                    tenHz = [tenHz n];
                end
                sign2(count,p,:) = (temp ~= 0);
                %                 plot(f, squeeze(sign2(count,p,:))./10,'.')
            end
            count = count + 1;
            cd(obj.data.setNames{n})
            %             end
            %             pause
            %             clf
        end
        cd(obj.data.setNames{n})
        
    end
    
    % end
    cd(obj.data.setNames{n})
end
cd tuning
load(files(i).name)
figure
for p = 1 : 4; subplot(2,4,p);plot(f,squeeze(sum(signt(:,p,:),1))/(count-1)); axis([5 50 0 0.1]);end

for p = 1 : 4; subplot(2,4,p+4);plot(f,squeeze(sum(sign2(:,p,:),1))/(count-1)); axis([5 50 0 0.1]);end
xlabel('Frequency [Hz]')
ylabel('Fraction of pairs with stimulus effect')
subplot(2,4,1); ylabel('STD bootstrap test')
subplot(2,4,5); ylabel('Two groups bootstrap test')
epochs = {'pre-sample' 'sample' 'delay1' 'delay2'};
for sb = 1 : 4; subplot(2,4,sb);title(epochs{sb}); end
set(gcf,'Name',sprintf('betty; idedays; n = %d only beta stim pairs',count-1))