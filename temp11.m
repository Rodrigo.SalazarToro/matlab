
cd('/Volumes/JEFFERSON/Data/Monkey/Working_Memory/clark/060406')
load cohInter.mat
cd session02/lfp
tuning = load('locTuningRule2.mat');
f = tuning.f;
limitB = find(f>= 12 & f<= 25);
cl = {'r' 'b' 'k'};
tit = {'Pre-sample' 'Sample' 'Delay1' 'Delay2'};
figure
for p = 1 : 4
    subplot(4,1,p)
    for c = 1 : 3
        plot(f,mean(squeeze(tuning.powerPP.power(c).S(3,:,:,p)),2),cl{c})
        hold on
    end
end
thecues = [1 3];

for c =1 : 2
    ppdata = sum(squeeze(tuning.powerPP.power(thecues(c)).S(3,limitB,:,2)),1);
    
    [values,tpref] = sort(ppdata);
    trials(c).order = tuning.ntrials{thecues(c)}(tpref);
end
% ppdata = sum(squeeze(tuning.powerPP.power(3).S(3,limitB,:,4)),1);
%
% [values,tpref] = sort(ppdata);
% trials(2).order = tuning.ntrials{3}(tpref);

set{1} = [trials(1).order(end-74:end) trials(2).order(1:75)];
set{2} = [trials(1).order(1:75) trials(2).order(end-74:end)];


params = struct('tapers',[2 3],'Fs',200,'fpass',[0 100],'trialave',1,'err',[2 0.05]);

for se = 1 : 2
    [selection(se).data,lplength] = lfpPcut(set{se},[3 6]);
    for p = 1 : 4
        PP = squeeze(selection(se).data{p}(:,:,1));
        PF = squeeze(selection(se).data{p}(:,:,2));
        [C{p,se},phi{p,se},S12,S1,S2,f,confC,phistd,Cerr]=coherencyc(PP,PF,params);
         [SPP{p,se},f,Serr]=mtspectrumc(PP,params);
         [SPF{p,se},f,Serr]=mtspectrumc(PF,params);
    end
end
figure; for p = 1 : 4; 
    subplot(4,2,(p-1) * 2+ 1);plot(f,C{p,1},'r'); hold on; plot(f,C{p,2}); title(tit{p}); 
    axis([0 60 0 0.35]);
    subplot(4,2,(p-1) * 2+ 2);plot(f,SPP{p,1},'k'); hold on; plot(f,SPP{p,2},'k--'); title(tit{p}); 
%     plot(f,SPF{p,1},'y'); hold on; plot(f,SPF{p,2},'y--'); title(tit{p});
    axis([0 60 0 700]);
end
xlabel('Frequency [Hz]')

legend('PP pref loc strong, non-pref loc weak','PP pref loc weak, non-pref loc strong')
subplot(4,2,7)
legend('pref loc strong, non-pref loc weak','pref loc weak, non-pref loc strong')
ylabel('coherence')
%   for p = 1 : 4; subplot(2,4,4+p);plot(f,phi{p,1},'r'); hold on; plot(f,phi{p,2}); end


