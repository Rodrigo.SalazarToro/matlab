

clear all

cd F:\salazar\data\Electrophy\20150804\session02
seqfile = nptDir('*newSeq.mat');
load(seqfile.name,'-mat');
cd lfp\
files = nptDir('*lfp.0*');

sound = 5;
odors = {1, 3, 4};
soundOdors = {6, 7, 8};
sc = 1;
oc = 1;
soc = 1;
slfp = cell(2,1);
olfp = cell(2,1);
solfp = cell(2,1);
for ff = 1 : length(files)
    if ff >= 181; drug = 1; elseif ff == 182; sc = 1; oc = 1;soc = 1; drug = 2;else drug = 2; end
    load(files(ff).name,'-mat')
    
    [prelfp,SR] = preprocessinglfp(lfp(24,:),'detrend','No60Hz');
    
    switch nseq(ff)
        
        case sound
            slfp{drug}(:,sc) = prelfp(1:1001);
            sc = sc + 1;
        case odors
            olfp{drug}(:,oc) = prelfp(1:1001);
            oc = oc + 1;
        case soundOdors
            solfp{drug}(:,soc) = prelfp(1:1001);
            soc = soc + 1;
    end
    
end
params =struct('tapers',[2 3],'Fs',SR,'trialave',1,'fpass',[0 30]);
Stsound = cell(2,1);
Stodor =  cell(2,1);
Stsoundodor = cell(2,1);
for dr = 1 : 2
%     [Stsound{dr},t,ft]=mtspecgramc(squeeze(slfp{dr}),[.8 .02],params);
%     [Stodor{dr},t,ft]=mtspecgramc(squeeze(olfp{dr}),[.8 .02],params);
%     [Stsoundodor{dr},t,ft]=mtspecgramc(squeeze(solfp{dr}),[.8 .02],params);
[Ssound{dr},f]=mtspectrumc(squeeze(slfp{dr}),params);
    [Sodor{dr},f]=mtspectrumc(squeeze(olfp{dr}),params);
    [Ssoundodor{dr},f]=mtspectrumc(squeeze(solfp{dr}),params);
end


%%
% figure;
% for dr = 1 : 2
%     subplot(3,2,dr )
%     imagesc(t,ft,Stsound{dr}');
%     colorbar
%     ylabel('sound')
%     if dr == 1; title('before CNO'); else title('after CNO'); end
%     subplot(3,2,2 +dr)
%     imagesc(t,ft,Stodor{dr}');
%     colorbar
%      ylabel('odor')
%      
%      subplot(3,2,4+dr)
%     imagesc(t,ft,Stsoundodor{dr}');
%     colorbar
%      ylabel('sound+odor')
%     
%     xlabel('time (s)')
%     if dr == 2
%     ylabel('frequency (Hz)')
%     
%    
%     end
%    
% end

% for sb = 1 : 6; subplot(3,2,sb); caxis([0 80000]); end

lab = {'-' '--'};
figure
for dr = 1 : 2
   plot(f,cat(2,Ssound{dr},Sodor{dr}, Ssoundodor{dr}),lab{dr})
   hold on
end
   legend('sound', 'odor', 'sound & odor', 'sound after CNO', 'odor after CNO', 'sound & odor after CNO')
