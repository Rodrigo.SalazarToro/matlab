% for pp = 1: 2
%     for r = 1 : 2;
%         for p = 1 : 4;
%             allnind{pp,r,p} = [];
%             allrind{pp,r,p} = [];
%             allvarind{pp,r,p} = [];
%         end;
%     end;
% end
% for d = 1 : 21;
%     cd(days{d});
%     [nind,rind,varind] = LFPtuning('tuningType','allCuesTuning');
%     %     spairs = tind{1,1} <= 0.2 & tind{2,1} <= 0.2;
%     for pp =1 : 2
%         for r = 1 : 2;
%             for p = 1 : 4;
%                 allnind{pp,r,p} = [allnind{pp,r,p} nind{pp,r,p}];
%                 allrind{pp,r,p} = [allrind{pp,r,p} rind{pp,r,p}];
%                 allvarind{pp,r,p} = [allvarind{pp,r,p} varind{pp,r,p}];
%             end;
%         end;
% 
%     end
%     cd ..;
% end
% 
% for pp = 1 : 2; f1 = figure; set(f1,'name',sprintf('ref window == %d',pp+2));for r = 1 : 2; for p = 1 : 4; subplot(2,4,(r-1)*4+p); hist(allnind{r,p},40); end; end; end
% for pp = 1 : 2; f1 = figure; set(f1,'name',sprintf('ref window == %d',pp+2));for r = 1 : 2; for p = 1 : 4; subplot(2,4,(r-1)*4+p); boxplot(allrind{pp,r,p}'); end; end; end
% for pp = 1 : 2; f1 = figure; set(f1,'name',sprintf('ref window == %d',pp+2)); for r = 1 : 2; for p = 1 : 4; subplot(2,4,(r-1)*4+p); hist(allvarind{pp,r,p},40); end; end; end
% for sb = 1 : 8; subplot(2,4,sb); axis([-0.2 0.5 0 15]); end;

% areas = {'P' 'F'};
% chanj{1} = 'nan';
% allch = {'chani' 'chanj'};
% for area = 1 : 2
%     chan = eval(allch{area});
%     nn = strfind(chan,areas{area});
%     ch{area} = [];
%     for n = 1 : length(nn); if ~isempty(nn{n}); ch{area} = [ch{area} n]; end; end
% end
%


%% independent and paired analysis analysis
load MVAR_Results/clark_4epoch_specs.mat
for pp = 1: 4
    for r = 1 : 2;
        for p = 1 : 4;
            for direc = 1 : 2
                allnind{pp,r,p,direc} = [];
                allrind{pp,r,p,direc} = [];
                allvarind{pp,r,p,direc} = [];
                allcorp{pp,r,direc} = [];
                allindex{pp,r,direc} = [];
            end
        end;
    end;
end
%%selection of specific pairs
for r = 1 : 2
    [Bspairs{r,2},uindex{r,2}] = selectPairs('rule',r,'windows',[3 4],'comparison',{'pos'},'range',{'beta'});

    [Bspairs{r,1},uindex{r,1}] = selectPairs('rule',r,'windows',[1 4],'comparison',{'neg'},'range',{'gamma'});
end
band = 2;
% for band = 1 : 2
    bothr = unique([Bspairs{1,band};Bspairs{2,band}],'rows');
%     bothr = intersect(Bspairs{1,band},Bspairs{2,band},'rows');
    
    for d = 1 : size(bothr,1)
        theindex.days{d} = sprintf('06%04.0f',bothr(d,1));
        theindex.npair(d) = bothr(d,4);
        theindex.ch(d,:) = bothr(d,2:3);
    end
    for r = 1 : 2; [data,surro] = summaryGC('list',theindex,'r',r,'plot'); end
%     for r = 1 : 2; [data,surro] = summaryGC('list',uindex{r,2},'r',r,'plot'); end
% end
%%
alldays = unique(theindex.days);
cd MVAR_Results/
for d = 1 : length(alldays) % 21
    rd = strmatch(alldays{d},date);
    repdays = strmatch(alldays{d},theindex.days);
    %     [nind,rind,varind,corp,index] = tuningGC(date{rd},'type','FF','allpairs',theindex.npair(repdays));
    %         [nind,rind,varind,corp,index] = tuningGC(date{d},'pairedAnalysis','type','PF','allpairs',theindex.npair(repdays));
    %         spairs = tind{1,1} <= 0.2 & tind{2,1} <= 0.2;
    [nind,rind,varind,corp,index] = tuningGC(date{d},'pairedAnalysis','type','PF');
    for pp =1 : 4 % reference window
        for r = 1 : 2;
            
            for direc = 1 : 2
                if ~isempty(corp{pp,r,direc})
                    %                     sig = find(corp{pp,r,direc}(1,:) > 0);
                    sig = [1 : size(corp{pp,r,direc},2)];
                    for p = 1 : 4;
                        allnind{pp,r,p,direc} = [allnind{pp,r,p,direc} nind{pp,r,p,direc}(:,sig)];
                        allrind{pp,r,p,direc} = [allrind{pp,r,p,direc} rind{pp,r,p,direc}(:,sig)];
                        allvarind{pp,r,p,direc} = [allvarind{pp,r,p,direc} varind{pp,r,p,direc}(:,sig)];
                    end
                    allindex{pp,r,direc} = [allindex{pp,r,direc} index{pp,r,direc}(:,sig)];
                end
            end
        end;
    end;
end


direction = {'Post to Fronto' 'Fronto to Post'};
% for pp = 1 : 2;for direc = 1 : 2; f1 = figure; set(f1,'name',sprintf('ref window %d; direction %s',pp+2,direction{direc}));for r = 1 : 2; for p = 1 : 4;  subplot(2,4,(r-1)*4+p); hist(allnind{pp,r,p,direc},40); end; end; end; end
for pp = 1 : 4;
    for direc = 1 : 2;
        f1 = figure;  set(f1,'name',sprintf('ref window %d; direction %s',pp,direction{direc}));
        for sb = 1 : 8; subplot(2,4,sb); axis([0 10 0 1.05]); end;
        for sb = 5 : 8; subplot(2,4,sb); xlabel('stimulus type'); end
        wind = {'pre-sample' 'sample' 'delay1' 'delay2'}; for sb = 1 : 4; subplot(2,4,sb); title(wind{sb}); end
        rulesN = {'IDENTITY' 'LOCATION'};c =1; for sb = [1 5]; subplot(2,4,sb); ylabel(rulesN{c}); c = c + 1;end
        for r = 1 : 2; for p = 1 : 4; subplot(2,4,(r-1)*4+p); boxplot(allrind{pp,r,p,direc}'); if p~=pp; hold on; [b,bint,rr,rint,stats] = regress(median(allrind{pp,r,p,direc},2),[1 1;1 2;1 3;1 4;1 5;1 6;1 7;1 8;1 9]); y = b(1) + b(2)*[1:9]; if stats(3)<0.01; plot([1:9],y,'r');else; plot([1:9],y,'r--');end; end;text(1,0.8,sprintf('n=%d',size(allrind{pp,r,p,direc},2)));end; end;
       
    end;
end

for pp = 1 : 4;
    for direc = 1 : 2;maxx = []; minx = []; maxy = [];
        f1 = figure; set(f1,'name',sprintf('ref window %d; direction %s',pp,direction{direc})); for r = 1 : 2; for p = 1 : 4; subplot(2,4,(r-1)*4+p); theD = allvarind{pp,r,p,direc}; [n,xout] = hist(theD,40); bar(xout,n); text(0.1,2,sprintf('mean %.3f std %.3f n=%d',mean(theD),std(theD),length(theD)));maxx = [maxx max(xout)]; minx = [minx min(xout)]; maxy = [maxy max(n)];  end; end;
        for sb = 1 : 8; subplot(2,4,sb); axis([min(minx) max(maxx) 0 max(maxy)]); end;
        for sb = 5 : 8; subplot(2,4,sb); xlabel('stimulus type'); end
        wind = {'pre-sample' 'sample' 'delay1' 'delay2'}; for sb = 1 : 4; subplot(2,4,sb); title(wind{sb}); end
        rulesN = {'IDENTITY' 'LOCATION'};c =1; for sb = [1 5]; subplot(2,4,sb); ylabel(rulesN{c}); c = c + 1;end
    end;
end

allcomp = [1 2 4; 1 2 3];

for pp = 1 : 2
    for direc = 1 : 2
        for r = 1 : 2
            for comp = 1 : 3
                p(pp,direc,r,comp) = signrank(allvarind{pp,r,pp+2,direc},allvarind{pp,r,allcomp(pp,comp),direc});
            end
        end
    end
end
%% paired analysis




