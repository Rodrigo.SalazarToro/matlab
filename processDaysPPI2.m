function processDaysPPI2(days,varargin)
% fonction qui lit les fichiers des jours que l'on souhaite �tudier, et
% cr�e trois vecteurs et une matrice :
% mice ID = identit�s des souris pour tous les jours
% doses = doses recues pour tous les jours
% Day = jour ou la souris a �t� test�e
% ppi = ppi des souris pour chaque intensit�

%launch processdaysPPI - curly brackets! to get startle amplitude, add
%reactivity as argument. to be launched in folder containing day folders.
% processDaysPPI({'20150807', 'othername', 'etc'}, 'reactivity')

 
Args = struct('reactivity',0);
Args.flags = {'reactivity'}; 
[Args,modvarargin] = getOptArgs(varargin,Args,'remove',{});

miceID=[];
doses=[];
ppi=[];
Day=[];
startleAmp=[];
nullPeriodAmp=[];

sdir=pwd;
% Rentre dans les dossiers des jours
for d=1:length(days)
  cd(days{d})
  dateExp=str2num(days{d});
  sessions=nptDir('session*');
  % Rentre dans les sessions pour chaque jour
  for ss=1:length(sessions)
      cd(sessions(ss).name);
      if isempty(nptDir('skip.txt')) % cas ou les donn�es n'ont pas pu �tre sauvergard�es
      fnameID=nptDir('*LabID.txt');
      fileID=fopen(fnameID.name,'r');
      a=textscan(fileID,'%s');
      miceID=cat(1,miceID, a{1}(1,1),a{1}(3,1)); % cr�e un vecteur qui prend les identit�s des souris
      doses=cat(1,doses,str2double(a{1}(2,:)),str2double(a{1}(4,:))); % cr�e un vecteur qui prend les doses inject�es
      Day=cat(1,Day,dateExp,dateExp);% cr�e un vecteur qui garde le jour de l'exp
        
      if Args.reactivity
            % Lecture des informations des fichiers
            table=nptDir ('*TableTest.TAB*') 
            data=nptDir('*rawdata*') 
            [infoTrial,defaultParams] = ReadInfoPPItxtFile(table.name);
            [startle,chamberid,blockid,trialid] = ReadDataPPItxtFile(data.name);
            [noPPAmpStartle,noPPAmpNull] = getTrialsPPI(startle,infoTrial,defaultParams,chamberid,blockid);   
            startleAmp=cat(1,startleAmp,noPPAmpStartle);
            nullPeriodAmp=cat(1,nullPeriodAmp,noPPAmpNull);   
        end  
      
         
      fclose(fileID);
      end
      cd .. ;
  end
  
  fnamePPI=nptDir('*prctPPI.mat');
  prctPPI=load(fnamePPI.name);
  for k = 1:length(prctPPI.ppi)
      ppi=cat(1,ppi,transpose(prctPPI.ppi(:,k))); % cr�e une matrice avec en colonne les ppi correspondant � chaque intensit�
      cd(sdir)
  end    

end

% supprime les lignes ou il n'y avait aucune souris dans les chambres
eliminate = find(strcmp('nomice',miceID) == 1);
miceID(eliminate) = [];
doses(eliminate)= [];
Day(eliminate)=[];
ppi(eliminate,:)=[];
startleAmp(eliminate,:)=[];
nullPeriodAmp(eliminate,:)=[];

% supprime les lignes ou il n'y a aucune donn�e (NaN)
baddata = find(sum(isnan(ppi),2) ~= 0);
miceID(baddata) = [];
doses(baddata)= [];
Day(baddata)=[];
ppi(baddata,:) = [];
startleAmp(baddata,:) = [];
nullPeriodAmp(baddata,:) = [];
display('Saving data')

% sauvegarde d'un fichier combin�
save(strcat(days{d},'combinedDataPPI'),'Day','miceID','doses','ppi','startleAmp','nullPeriodAmp')

%sauvegarde plot ppi
ppi2 = ppi.'; % transpose the matrix to be in the same sense as the rest
   
   figure1 = figure;% plot the SEM 
   subplot(3,1,1)
   plot(mean(ppi2(:,doses ==0),2) - std(ppi2(:,doses ==0),[],2)/sqrt(sum(doses(:) == 0)),'k') 
   hold on; 
   plot(mean(ppi2(:,doses ==0),2) + std(ppi2(:,doses ==0)/sqrt(sum(doses(:) == 0)),[],2),'k')
   
   plot(mean(ppi2(:,doses ==1),2) - std(ppi2(:,doses ==1),[],2)/sqrt(sum(doses(:) == 1)),'r')
   hold on; 
   plot(mean(ppi2(:,doses ==1),2) + std(ppi2(:,doses ==1)/sqrt(sum(doses(:) == 1)),[],2),'r')
   
   ylim([-20 80])
   set(gca,'XTick',[5 10 15 20])
   legend('CONT','CONT','Treatment','Treatment')
   
ppicont = ppi2(:,doses==0)
ppitreat = ppi2(:,doses==1)

    subplot(3,1,2)
    plot(ppi2(:,doses==0)); %plot ppi for all animal of each condition
    ylim([min(ppi2(:))-10 max(ppi2(:))+10])
    title('CONT')
    
    set(gca,'XTickLabel',[5 10 15 20])
    xlabel('Prepulse intensities (dB)')
    ylabel('% PPI')


    subplot(3,1,3)
    plot(ppi2(:,doses==1)); %plot ppi for all animal of each condition
    ylim([min(ppi2(:)-10) max(ppi2(:))+10])
    title('Treatment')

    set(gca,'XTickLabel',[5 10 15 20])
    xlabel('Prepulse intensities (dB)')
    ylabel('% PPI')

    
figure2 = figure; % plot the SD

   plot(mean(ppi2(:,doses ==0),2) - std(ppi2(:,doses ==0),[],2),'k')
   hold on; 
   plot(mean(ppi2(:,doses ==0),2) + std(ppi2(:,doses ==0),[],2),'k')
   
   plot(mean(ppi2(:,doses ==1),2) - std(ppi2(:,doses ==1),[],2),'r')
   hold on; 
   plot(mean(ppi2(:,doses ==1),2) + std(ppi2(:,doses ==1),[],2),'r')
   
   ylim([-20 80])
   set(gca,'XTick',[5 10 15 20])
   legend('CONT','CONT','Treatment','Treatment')
   
   
figure3 = figure; %plot Amplitude

subplot(2,1,1);plot(startleAmp(doses == 0,:),'o');axis([ 0 13 0 3000]); title('Control'); ylabel('Amplitude'); subplot(2,1,2); plot(startleAmp(doses == 1,:),'o');axis([ 0 13 0 3000]); title('Treatment');ylabel('Amplitude');

    
   saveas(figure1, strcat(days{d},'PPIplotSEM'))
   saveas(figure2, strcat(days{d},'PPIplotSD'))
   saveas(figure3, strcat(days{d},'AmpPlot'))

 
